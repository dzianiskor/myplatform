Highcharts.theme = {
	colors: ['#F29583', '#ABDBCE', '#7CB5EC', '#FFFF66'],
	title: {
		text:'',
		style: {
			color: '#717C95',
			font: '16px Helvetica, Arial, sans-serif'
		}
	},
	subtitle: {
		style: {
			color: '#b1bacf',
			font: '12px Helvetica, Arial, sans-serif'
		}
	},
	xAxis: {
		gridLineWidth: 0,
		lineColor: '#717C95',
		tickColor: '#717C95',
		labels: {
			style: {
				color: '#717C95',
				font: '11px Helvetica, Arial, sans-serif'
			}
		},
		title: {
			style: {
				color: '#717C95',
				fontWeight: 'bold',
				fontSize: '12px',
				fontFamily: 'Helvetica, Arial, sans-serif'

			}
		}
	},
	yAxis: {
		minorTickInterval: 'auto',
		lineColor: '#717C95',
		lineWidth: 0,
		tickWidth: 0,
		tickColor: '#717C95',
		labels: {
			style: {
				color: '#717C95',
				font: '11px Helvetica, Arial, sans-serif'
			}
		},
		title: {
			style: {
				color: '#717C95',
				fontWeight: 'bold',
				fontSize: '12px',
				fontFamily: 'Helvetica, Arial, sans-serif'
			}
		}
	},
	legend: {
		itemStyle: {
			font: '9pt Helvetica, Arial, sans-serif',
			color: '#717C95'

		},
		itemHoverStyle: {
			color: '#039'
		},
		itemHiddenStyle: {
			color: 'gray'
		},
		borderRadius:0,
		borderWidth:0
	},
	labels: {
		style: {
			color: '#99b'
		}
	}
};

// Apply the theme
var highchartsOptions = Highcharts.setOptions(Highcharts.theme);
