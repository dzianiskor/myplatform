// New Cattranslation
    $(".btn-new-prodmodeltranslation").on("click", function(e) {
        e.preventDefault();

        _this = $(this);

        $.get('/dashboard/prodmodeltranslation/new/'+_this.data('prodmodel_id'), function(html) {
            showDialogWithContents(html, function(){
                                 
            });
        });
    });

    // Edit Cattranslation
    $(document).on('click', 'a.edit-prodmodeltranslation', function(e){
        e.preventDefault();

        _this = $(this);

        $.get(_this.attr('href'), function(html) {
            showDialogWithContents(html, function(){
                                 
            });
        });
    });

    // Delete Cattranslation
    $(document).on('click', 'a.delete-prodmodeltranslation', function(e){
        e.preventDefault();

        _this = $(this);

        if(confirm("Are you sure you want to delete this prodmodeltranslation?")) {
            $.get(_this.attr('href'), function(resp) {
                if(!resp.failed) {
                    _this.closest('tr').hide().remove();
                }
            });
        }
    });
    
    // Save Cattranslation
    $(document).on('click', 'a.save-new-prodmodeltranslation', function(e){
        e.preventDefault();

        _this = $(this);
        _frm  = $('#new-prodmodeltranslation-form');

        _this.prop('disabled', true);
        _this.html('Loading...');

        $.post(_frm.attr('action'), _frm.serializeArray(), function(resp) {
            if(resp.failed) {
                alert(resp.errors);
            } else {
                
                $.get(resp.view_url, function(html) {
                    $('#prodmodeltranslation-list tbody').html(html);
                    _this.prop('disabled', false);
                    _this.html('Save');
                    closeDialog();
                });
            }
        });            
        
    });