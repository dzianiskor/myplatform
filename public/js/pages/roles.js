$(document).ready(function() {
    // New Store
    $("#select-all-permissions").on("click", function(e) {
        e.preventDefault();
        var checkBoxes = $("input:checkbox");
        checkBoxes.prop("checked", !checkBoxes.prop("checked"));
    });

    $(".select-group-permissions").on("click", function(e) {
        e.preventDefault();
        var checkBoxes = $(this).closest('tbody').find('input:checkbox');
        checkBoxes.prop("checked", !checkBoxes.prop("checked"));
    });
});