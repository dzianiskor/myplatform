function populateNetworks(sender) {
    _this = $(sender);
    if (_this.data("draft") == 1) {
        _selected = _this.val();
        _id = _this.data("id");
        $.get("/dashboard/suppliers/resetnetworks/" + _id + "/" + _selected, function(html) {
            $("#network-listing").html(html);
        });
    }
}
$(document).ready(function() {
     //when selecting the parent supplier,
    //assign the same networks

    populateNetworks($("#parent_id"));

    $("#parent_id").on("change", function(e){
        populateNetworks(this);
    });

    $("#parent_id").on("blur", function(e){
        populateNetworks(this);
    });

    // New Store
    $(".btn-new-supplier-store").on("click", function(e) {
        e.preventDefault();

        _this = $(this);

        $.get('/dashboard/supplierstore/new/'+_this.data('supplier_id'), function(html) {
            showDialogWithContents(html, function(){
                var loc = new google.maps.LatLng($('#supplierstore_latitude').val(), $('#supplierstore_longitude').val());

                var map = new google.maps.Map(document.getElementById("map"), {
                    center: loc,
                    zoom: 11
                });

                var marker = new google.maps.Marker({
                    position: loc,
                    map: map,
                    title:"Drag to location",
                    draggable:true
                });

                google.maps.event.addListener(marker, 'dragend', function(evt) {
                    $('#supplierstore_latitude').val(evt.latLng.lat().toFixed(6));
                    $('#supplierstore_longitude').val(evt.latLng.lng().toFixed(6));
                });                 
            });
        });
    });

    // Edit Store
    $(document).on('click', 'a.edit-supplier-store', function(e){
        e.preventDefault();

        _this = $(this);

        $.get(_this.attr('href'), function(html) {
            showDialogWithContents(html, function(){
                var loc = new google.maps.LatLng($('#supplierstore_latitude').val(), $('#supplierstore_longitude').val());

                var map = new google.maps.Map(document.getElementById("map"), {
                    center: loc,
                    zoom:11
                });

                var marker = new google.maps.Marker({
                    position: loc,
                    map: map,
                    title:"Drag to location",
                    draggable:true
                });

                google.maps.event.addListener(marker, 'dragend', function(evt) {
                    $('#supplierstore_latitude').val(evt.latLng.lat().toFixed(6));
                    $('#supplierstore_longitude').val(evt.latLng.lng().toFixed(6));
                });                 
            });
        });
    });

    // Delete Store
    $(document).on('click', 'a.delete-supplier-store', function(e){
        e.preventDefault();

        _this = $(this);

        if(confirm("Are you sure you want to delete this store?")) {
            $.get(_this.attr('href'), function(resp) {
                if(!resp.failed) {
                    _this.closest('tr').hide().remove();
                }
            });
        }
    });

    

    // Save Store
    $(document).on('click', 'a.save-new-supplier-store', function(e){
        e.preventDefault();

        _this = $(this);
        _frm  = $('#new-supplier-store-form');

        var failed = false;


        if(failed == true) {
            alert("Please make sure you have give a valid name to each Point of Sale");
        } else {

            _this.prop('disabled', true);
            _this.html('Loading...');

            $.post(_frm.attr('action'), _frm.serializeArray(), function(resp) {

                if(resp.failed) {
                    alert(resp.errors);
                } else {
                    $.get(resp.view_url, function(html) {
                        $('#supplier-store-list tbody').html(html);
                        _this.prop('disabled', false);
                        _this.html('Save');
                        closeDialog();
                    });
                }
            });            
        }
    });
});