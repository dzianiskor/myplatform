$(document).ready(function() {
    // New estatementtranslation
    $(".btn-new-estatementtranslation").on("click", function(e) {
        e.preventDefault();

        _this = $(this);

        $.get('/dashboard/estatementtranslation/new/'+_this.data('estatement_id'), function(html) {
            showDialogWithContents(html, function(){
                $('div#dialog-wrapper').css("width", "800");
            });
        });
    });

    // Edit estatementtranslation
    $(document).on('click', 'a.edit-estatementtranslation', function(e){
        e.preventDefault();

        _this = $(this);

        $.get(_this.attr('href'), function(html) {
            showDialogWithContents(html, function(){
                                 
            });
        });
    });

    // Delete estatementtranslation
    $(document).on('click', 'a.delete-estatementtranslation', function(e){
        e.preventDefault();

        _this = $(this);

        if(confirm("Are you sure you want to delete this estatementtranslation?")) {
            $.get(_this.attr('href'), function(resp) {
                if(!resp.failed) {
                    _this.closest('tr').hide().remove();
                }
            });
        }
    });
    
    // Save estatementtranslation
    $(document).on('click', 'a.save-new-estatementtranslation', function(e){
        e.preventDefault();

        _this = $(this);
        _frm  = $('#new-estatementtranslation-form');

        _this.prop('disabled', true);
        _this.html('Loading...');

        $.post(_frm.attr('action'), _frm.serializeArray(), function(resp) {
            if(resp.failed) {
                alert(resp.errors);
                _this.prop('disabled', false);
                _this.html('Save');
            } else {
                
                $.get(resp.view_url, function(html) {
                    $('#estatementtranslation-list tbody').html(html);
                    _this.prop('disabled', false);
                    _this.html('Save');
                    closeDialog();
                });
            }
        });            
        
    });
    
    //E-Statement Items
    // New estatementitem
    $(".btn-new-estatementitem").on("click", function(e) {
        e.preventDefault();

        _this = $(this);

        $.get('/dashboard/estatementitem/new/'+_this.data('estatement_id'), function(html) {
            showDialogWithContents(html, function(){
                $('div#dialog-wrapper').css("width", "800");
            });
        });
    });

    // Edit estatementitem
    $(document).on('click', 'a.edit-estatementitem', function(e){
        e.preventDefault();

        _this = $(this);

        $.get(_this.attr('href'), function(html) {
            showDialogWithContents(html, function(){
                                 
            });
        });
    });

    // Delete estatementitem
    $(document).on('click', 'a.delete-estatementitem', function(e){
        e.preventDefault();

        _this = $(this);

        if(confirm("Are you sure you want to delete this estatementitem?")) {
            $.get(_this.attr('href'), function(resp) {
                if(!resp.failed) {
                    _this.closest('tr').hide().remove();
                }
            });
        }
    });
    
    // Save estatementitem
    $(document).on('click', 'a.save-new-estatementitem', function(e){
        e.preventDefault();

        _this = $(this);
        _frm  = $('#new-estatementitem-form');

        _this.prop('disabled', true);
        _this.html('Loading...');

        $.post(_frm.attr('action'), _frm.serializeArray(), function(resp) {
            console.log(resp);
            if(resp.failed) {
                alert(resp.errors);
                _this.prop('disabled', false);
                _this.html('Save');
            } else {
                
                $.get(resp.view_url, function(html) {
                    $('#estatementitem-list tbody').html(html);
                    _this.prop('disabled', false);
                    _this.html('Save');
                    closeDialog();
                });
            }
        });            
        
    });
    
    //Product Search
    $(document).on('click', '#products-search', function(event) {
        event.preventDefault();
            if($("#products-autocomplete").val().length>=3) {
                event.preventDefault();
                $('select#product_id').empty();
                $.get('/dashboard/loyalty/products?term='+$('#products-autocomplete').val(), function(resp){
                    $.each(resp, function(index, value){
                        jQuery('<option/>', {
                            value: value.id,
                            html: value.value
                            }).appendTo('select#product_id');//appends to select if parent div has id dropdown
                            $('input[name=product_id]').val(value.id);
                        });
                });
                return false;
            }else{
                event.preventDefault();
                return false;
            }
     });       
        $(document).bind('keypress', '#products-autocomplete', function(event) {
                if(event.keyCode == 13) {
                    if($("#products-autocomplete").length >=3) {
                        event.preventDefault();
                        $('select#product_id').empty();
                        $.get('/dashboard/loyalty/products?term='+$('#products-autocomplete').val(), function(resp){
                            $.each(resp, function(index, value){
                                jQuery('<option/>', {
                                    value: value.id,
                                    html: value.value
                                    }).appendTo('select#product-id');//appends to select if parent div has id dropdown
                                    $('input[name=product_id]').val(value.id);
                                });
                        });
                        return false;
                    }else{
                        event.preventDefault();
                        return false;
                    }
                 }
            // Turn on the global items search autocomplete function
                $("#products-autocomplete").autocomplete({
                  source: "/dashboard/loyalty/products",
                  minLength: 3,
                  select: function( event, ui ) {
                    jQuery('<option/>', {
                        value: ui.item.id,
                        html: ui.item.value
                        }).appendTo('select#product_id').attr('selected', true); //appends to select if parent div has id dropdown
                    $('input[name=product_id]').val(ui.item.id);
                  }
                });

        });
        $(document).on('change', 'select#product_id', function() {

            $('input[name=product_id]').val($(this).val());

        });
        $(document).on('change', 'select#estatementitem_type', function() {
            var type = $(this).val();
            if(type == 'product'){
                $('#Product').css('display', '');
                $('#Offer').css('display', 'none');
            }
            else{
                $('#Product').css('display', 'none');
                $('#Offer').css('display', '');
            }
        });
        
        $(document).on('click', 'button#send_now', function() {
            var estatementId = $(this).data('estatement_id');
            //var user_id = $("#entity_id").val();
            console.log('/dashboard/estatement/sendnow/' + estatementId + '/en');
            if(confirm('The email will be sent out, are you sure you would like to Proceed?')){
                $.get('/dashboard/estatement/sendnow/' + estatementId + '/en', function(resp) {
                    if(resp == 1){
                        alert('The email has been successfully sent');
                    }
                    console.log(resp);
                });
            }
        });

        // Turn on the global member search autocomplete function
            $(document).bind('keypress', '#members-autocomplete', function(event) {
            if($('#members-autocomplete').length > 0) {

                $("#members-autocomplete").autocomplete({
                  source: "/dashboard/reports/members",
                  minLength: 2,
                  select: function( event, ui ) {
                    $('input[name="user_id"]').val(ui.item.id);
                  }
});
            }
        });
        
        $('#partner-list').on('change', function() {
            var partner = $(this).val();
            $.get('/dashboard/estatement/tiersbypartner/' + partner, function(resp) {
                   $('select[name=tier_id]').html(resp);
                });
        });
});