// New Cattranslation
    $(".btn-new-prodsubmodeltranslation").on("click", function(e) {
        e.preventDefault();

        _this = $(this);

        $.get('/dashboard/prodsubmodeltranslation/new/'+_this.data('prodsubmodel_id'), function(html) {
            showDialogWithContents(html, function(){
                                 
            });
        });
    });

    // Edit Cattranslation
    $(document).on('click', 'a.edit-prodsubmodeltranslation', function(e){
        e.preventDefault();

        _this = $(this);

        $.get(_this.attr('href'), function(html) {
            showDialogWithContents(html, function(){
                                 
            });
        });
    });

    // Delete Cattranslation
    $(document).on('click', 'a.delete-prodsubmodeltranslation', function(e){
        e.preventDefault();

        _this = $(this);

        if(confirm("Are you sure you want to delete this prodsubmodeltranslation?")) {
            $.get(_this.attr('href'), function(resp) {
                if(!resp.failed) {
                    _this.closest('tr').hide().remove();
                }
            });
        }
    });
    
    // Save Cattranslation
    $(document).on('click', 'a.save-new-prodsubmodeltranslation', function(e){
        e.preventDefault();

        _this = $(this);
        _frm  = $('#new-prodsubmodeltranslation-form');

        _this.prop('disabled', true);
        _this.html('Loading...');

        $.post(_frm.attr('action'), _frm.serializeArray(), function(resp) {
            if(resp.failed) {
                alert(resp.errors);
            } else {
                
                $.get(resp.view_url, function(html) {
                    $('#prodsubmodeltranslation-list tbody').html(html);
                    _this.prop('disabled', false);
                    _this.html('Save');
                    closeDialog();
                });
            }
        });            
        
    });