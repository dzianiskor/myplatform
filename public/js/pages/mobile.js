function scan()
{
    cordova.plugins.barcodeScanner.scan(
          function (result) {
              $("#scan").val(result.text);
          },
          function (error) {
              alert("Scanning failed: " + error);
          }
       );
}

function s4()
{
    return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
}

function guid()
{
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
         s4() + '-' + s4() + s4() + s4();
}

function configureCookie()
{
    _key = 'mobile_uid';

    var _cookie = $.fn.cookie(_key);

    if (!_cookie) {

        _cookie = localStorage.getItem(_key);

        if (!_cookie || _cookie == "undefined") {
            _cookie = guid();
            localStorage.setItem(_key, _cookie);
        }
        $.fn.cookie(_key,_cookie);
    }
}

function partnerChange(sender)
{
    NProgress.start();

    _this = $(sender);

    _selected = _this.val();

    $.get("/mobile/storesforpartner/" + _selected, function(html) {
        $("#store-wrapper").html(html);

        storeChange($("#store"));

        NProgress.done();
    });
}

function bindPartnerListener()
{
    $('#partner').on('change', function(e){
        partnerChange(this);
    });

    _partner = $("#partner")[0];

    if (_partner) {
        _partner = _partner.options[_partner.selectedIndex].value;
        if (!_partner) {
            $("#partner").trigger('change');
        }
    }

}

function storeChange(sender)
{
    NProgress.start();

    _this = $(sender);

    _selected = _this.val();

    if (_selected == "") {
        $("#pos-wrapper").html("");
        NProgress.done();
    } else {
        $.get("/mobile/posforstore/" + _selected, function(html) {
            $("#pos-wrapper").html(html);
            NProgress.done();
        });
    }
}

function bindStoreListener()
{
//    $('#store').on('change', function(e){
//        storeChange(this);
//    });
		$(document).on('change', '#store', function(e){
        storeChange(this);
    });
}

function bindListeners()
{
    bindPartnerListener();

    bindStoreListener();

    $("#doscan").on('click', function(e){
        e.preventDefault();
        scan();
    });
}

/**
 * Explicitly load the specified page script
 *
 * @param string src
 */
function enqueue_cordova_script(src)
{
    var script = document.createElement( 'script' );

    script.src = "/js/cordova/"+src+"/cordova.js";

    $("body").append( script );
}

function loadPhoneGap()
{
    var _name;

    if (navigator.userAgent.match(/Android/i)) {
        _name = 'android';
    } else if (navigator.userAgent.match(/iPhone|iPad|iPod/i)) {
        _name = 'ios';
    } else if (navigator.userAgent.match(/IEMobile/i)) {
        _name = 'windows';
    }

    if(typeof _name != 'undefined') {
        enqueue_cordova_script(_name);
    }
}

function isPhoneGap()
{
    if(/ios|iphone|ipod|ipad|android/i.test(navigator.userAgent)) {
        return true;
    } else {
        return false;
    }
}

$(document).ready(function()
{
    bindListeners();

    configureCookie();

    loadPhoneGap();

    $(document).on('keypress', 'input.enforceNumeric', function(e) {
        if(e.which == 13) {
            e.preventDefault();
        }

       if(!(e.which >= 48 && e.which <= 57) && !(e.which == 8)){
            e.preventDefault();
        }
    });

    $('a.doProceed').one('click',function(e){
        e.stopPropagation();
        e.preventDefault();
        $(this).attr('disabled',true);
        $(this).closest('form').submit();
    });
});