// New Brandtranslation
    $(".btn-new-brandtranslation").on("click", function(e) {
        e.preventDefault();

        _this = $(this);

        $.get('/dashboard/brandtranslation/new/'+_this.data('brand_id'), function(html) {
            showDialogWithContents(html, function(){
                                 
            });
        });
    });

    // Edit Brandtranslation
    $(document).on('click', 'a.edit-brandtranslation', function(e){
        e.preventDefault();

        _this = $(this);

        $.get(_this.attr('href'), function(html) {
            showDialogWithContents(html, function(){
                                 
            });
        });
    });

    // Delete Brandtranslation
    $(document).on('click', 'a.delete-brandtranslation', function(e){
        e.preventDefault();

        _this = $(this);

        if(confirm("Are you sure you want to delete this brandtranslation?")) {
            $.get(_this.attr('href'), function(resp) {
                if(!resp.failed) {
                    _this.closest('tr').hide().remove();
                }
            });
        }
    });
    
    // Save Brandtranslation
    $(document).on('click', 'a.save-new-brandtranslation', function(e){
        e.preventDefault();

        _this = $(this);
        _frm  = $('#new-brandtranslation-form');

        _this.prop('disabled', true);
        _this.html('Loading...');

        $.post(_frm.attr('action'), _frm.serializeArray(), function(resp) {
            if(resp.failed) {
                alert(resp.errors);
            } else {
                
                $.get(resp.view_url, function(html) {
                    $('#brandtranslation-list tbody').html(html);
                    _this.prop('disabled', false);
                    _this.html('Save');
                    closeDialog();
                });
            }
        });            
        
    });