$(document).ready(function() {
    // New Langkey
    $(".btn-new-key").on("click", function(e) {
        e.preventDefault();

        _this = $(this);

        $.get('/dashboard/langkey/new/'+_this.data('lang_id'), function(html) {
            showDialogWithContents(html, function(){
                                 
            });
        });
    });

    // Edit Langkey
    $(document).on('click', 'a.edit-langkey', function(e){
        e.preventDefault();

        _this = $(this);

        $.get(_this.attr('href'), function(html) {
            showDialogWithContents(html, function(){
                                 
            });
        });
    });

    // Delete Langkey
    $(document).on('click', 'a.delete-langkey', function(e){
        e.preventDefault();

        _this = $(this);

        if(confirm("Are you sure you want to delete this langkey?")) {
            $.get(_this.attr('href'), function(resp) {
                if(!resp.failed) {
                    _this.closest('tr').hide().remove();
                }
            });
        }
    });

    

    // Save Langkey
    $(document).on('click', 'a.save-new-langkey', function(e){
        e.preventDefault();

        _this = $(this);
        _frm  = $('#new-langkey-form');

        var failed = false;

        

            _this.prop('disabled', true);
            _this.html('Loading...');

            $.post(_frm.attr('action'), _frm.serializeArray(), function(resp) {
                if(resp.failed) {
                    alert(resp.errors);
                } else {
                    console.log(resp);
                    $.get(resp.view_url, function(html) {
                        $('#langkey-list tbody').html(html);
                        _this.prop('disabled', false);
                        _this.html('Save');
                        closeDialog();
                    });
                }
            });            
        
    });
});