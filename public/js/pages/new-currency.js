// New Currency Tranlation
    $(".btn-new-currencytranslation").on("click", function(e) {
        e.preventDefault();

        _this = $(this);

        $.get('/dashboard/currencytranslation/new/'+_this.data('currency'), function(html) {
            showDialogWithContents(html, function(){
                                 
            });
        });
    });

    // Edit Currency Tranlation
    $(document).on('click', 'a.edit-currencytranslation', function(e){
        e.preventDefault();

        _this = $(this);

        $.get(_this.attr('href'), function(html) {
            showDialogWithContents(html, function(){
                                 
            });
        });
    });

    // Delete Currency Tranlation
    $(document).on('click', 'a.delete-currencytranslation', function(e){
        e.preventDefault();

        _this = $(this);

        if(confirm("Are you sure you want to delete this currency translation?")) {
            $.get(_this.attr('href'), function(resp) {
                if(!resp.failed) {
                    _this.closest('tr').hide().remove();
                }
            });
        }
    });
    
    // Save Currency Tranlation
    $(document).on('click', 'a.save-new-currencytranslation', function(e){
        e.preventDefault();

        _this = $(this);
        _frm  = $('#new-currencytranslation-form');

        _this.prop('disabled', true);
        _this.html('Loading...');

        $.post(_frm.attr('action'), _frm.serializeArray(), function(resp) {
            if(resp.failed) {
                alert(resp.errors);
            } else {
                
                $.get(resp.view_url, function(html) {
                    $('#currencytranslation-list tbody').html(html);
                    _this.prop('disabled', false);
                    _this.html('Save');
                    closeDialog();
                });
            }
        });            
        
    });