$(document).ready(function() {
    // New Store
    $(".btn-verify").on("click", function(e) {
        e.preventDefault();

        _frm    = $('#coupon-form');
        _action = _frm.attr('action');
        _coupon_code = $(document).find('input[name="coupon_code"]').val();

        $.get(_action + '/' + _coupon_code, function(html) {
            showDialogWithContents(html);
        });
    });


});