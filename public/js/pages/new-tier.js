
$(document).ready(function() {
     

    // New Store
    $(".btn-new-tier-criteria").on("click", function(e) {
        e.preventDefault();

        _this = $(this);

        $.get('/dashboard/tiercriteria/new/'+_this.data('tier_id'), function(html) {
            showDialogWithContents(html, function(){
                   
            });
        });
    });

    // Edit Store
    $(document).on('click', 'a.edit-tier-criteria', function(e){
        e.preventDefault();

        _this = $(this);

        $.get(_this.attr('href'), function(html) {
            showDialogWithContents(html, function(){
                                
            });
        });
    });

    // Delete Store
    $(document).on('click', 'a.delete-tier-criteria', function(e){
        e.preventDefault();

        _this = $(this);

        if(confirm("Are you sure you want to delete this criteria?")) {
            $.get(_this.attr('href'), function(resp) {
                if(!resp.failed) {
                    _this.closest('tr').hide().remove();
                }
            });
        }
    });

    

    // Save Store
    $(document).on('click', 'a.save-new-tier-criteria', function(e){
        e.preventDefault();

        _this = $(this);
        _frm  = $('#new-tier-criteria-form');

        var failed = false;




        _this.prop('disabled', true);
        _this.html('Loading...');
        //alert(_frm.serializeArray());
        $.post(_frm.attr('action'), _frm.serializeArray(), function(resp) {
            //alert(resp.toSource());

            if(resp.failed) {
                alert(resp.errors);
            } else {
                $.get(resp.view_url, function(html) {
                    $('#tier-criteria-list tbody').html(html);
                    _this.prop('disabled', false);
                    _this.html('Save');
                    closeDialog();
                });
            }
        });            
        
    });
});