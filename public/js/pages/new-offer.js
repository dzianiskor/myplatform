$(document).ready(function() {
    // New Category
    $(".btn-new-category").on("click", function(e) {
        e.preventDefault();

        $.get('/dashboard/categories/newPopover/', function(html) {
            showDialogWithContents(html);
        });
    });

    //save category from popover
    $(document).on('click', 'a.category-save', function(e){
        e.preventDefault();

        _this = $(this);
        _frm  = $('#new-category-form');

        _this.prop('disabled', true);
        _this.html('Loading...');

        $.post(_frm.attr('action'), _frm.serializeArray(), function(resp) {
            if(resp.failed) {
                alert(resp.errors);
            } else {
                $.get(resp.view_url, function(html) {
                    $('#category-list').html(html);
                    _this.prop('disabled', false);
                    _this.html('Save');
                    closeDialog();
                });
            }
        });
    });

    //add new row for image upload
    $(document).on('click', 'a.btn-new-file', function(e){
        e.preventDefault();
        $('table.file-upload').append($('table.file-upload tr:last').clone());
    });

    //delete existing image
    $(document).on('click', 'a.delete-image', function(e){
        if (!confirm('Are you sure you want to delete this image?')) {
            return;
        }
        e.preventDefault();
        _this = $(this);
        _id = _this.data('id');
        _anchor = $(event.target);
        _tr = $(_anchor).closest('tr');
        $.get('/media/delete/'+_id, function(resp) {
            if(resp.failed) {
                alert(resp.errors);
            } else {
                //remove the element from DOM
                _tr.remove();
            }
        });
    });

    // New offertranslation
    $(".btn-new-offertranslation").on("click", function(e) {
        e.preventDefault();

        _this = $(this);

        $.get('/dashboard/offertranslation/new/'+_this.data('offer_id'), function(html) {
            showDialogWithContents(html, function(){
                $('div#dialog-wrapper').css("width", "800");
            });
        });
    });

    // Edit offertranslation
    $(document).on('click', 'a.edit-offertranslation', function(e){
        e.preventDefault();

        _this = $(this);

        $.get(_this.attr('href'), function(html) {
            showDialogWithContents(html, function(){
                                 
            });
        });
    });

    // Delete offertranslation
    $(document).on('click', 'a.delete-offertranslation', function(e){
        e.preventDefault();

        _this = $(this);

        if(confirm("Are you sure you want to delete this offertranslation?")) {
            $.get(_this.attr('href'), function(resp) {
                if(!resp.failed) {
                    _this.closest('tr').hide().remove();
                }
            });
        }
    });
    
    // Save offertranslation
    $(document).on('click', 'a.save-new-offertranslation', function(e){
        e.preventDefault();

        _this = $(this);
        _frm  = $('#new-offertranslation-form');

        _this.prop('disabled', true);
        _this.html('Loading...');

        $.post(_frm.attr('action'), _frm.serializeArray(), function(resp) {
            if(resp.failed) {
                alert(resp.errors);
            } else {
                
                $.get(resp.view_url, function(html) {
                    $('#offertranslation-list tbody').html(html);
                    _this.prop('disabled', false);
                    _this.html('Save');
                    closeDialog();
                });
            }
        });            
        
    });

    $(document).on('click', ".form-buttons .pure-button-primary", function(e){
        if ($('ul#partner-listing li.empty').html() == 'No Partner Associated') {
            e.preventDefault();
            $('ul#partner-listing').next().css('display','block');
            $('ul#partner-listing').before().get(0).scrollIntoView();
            return;
        }

        if ($('ul#network-listing li.empty').html() == 'No Networks Associated') {
            e.preventDefault();
            $('ul#network-listing').next().css('display','block');
            $('ul#network-listing').before().get(0).scrollIntoView();
            return;
        }
    })
});