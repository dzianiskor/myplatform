$(document).ready(function() {
    // New Brand
    $(".btn-new-brand").on("click", function(e) {
        e.preventDefault();

        $.get('/dashboard/brands/newPopover/', function(html) {
            showDialogWithContents(html);
        });
    });

    // New Category
    $(".btn-new-category").on("click", function(e) {
        e.preventDefault();

        $.get('/dashboard/categories/newPopover/', function(html) {
            showDialogWithContents(html);
        });
    });

    $('input[name="price_in_usd"]').on('keyup', function(e){
        _price_in_points = $('input[name="price_in_points"]');
        _price_in_usd = $(this).val();
        _price_in_points.val( Math.round(_price_in_usd / 0.01) );
    });

    //save brand from popover
    $(document).on('click', 'a.brand-save', function(e){
        e.preventDefault();

        _this = $(this);
        _frm  = $('#new-brand-form');

        _this.prop('disabled', true);
        _this.html('Loading...');

        $.post(_frm.attr('action'), _frm.serializeArray(), function(resp) {
            if(resp.failed) {
                alert(resp.errors);
                _this.prop('disabled', false);
                _this.html('Save');
            } else {
                $.get(resp.view_url, function(html) {
                    $('#brand-list').html(html);
                    _this.prop('disabled', false);
                    _this.html('Save');
                    closeDialog();
                });
            }
        });
    });

    //save category from popover
    $(document).on('click', 'a.category-save', function(e){
        e.preventDefault();

        _this = $(this);
        _frm  = $('#new-category-form');

        _this.prop('disabled', true);
        _this.html('Loading...');

        $.post(_frm.attr('action'), _frm.serializeArray(), function(resp) {
            if(resp.failed) {
                alert(resp.errors);
            } else {
                $.get(resp.view_url, function(html) {
                    $('#category-list').html(html);
                    _this.prop('disabled', false);
                    _this.html('Save');
                    closeDialog();
                });
            }
        });
    });

    //add new row for image upload
    $(document).on('click', 'a.btn-new-file', function(e){
        e.preventDefault();
        $('table.file-upload').append($('table.file-upload tr:last').clone());
    });

    //delete existing image
    $(document).on('click', 'a.delete-image', function(e){
        if (!confirm('Are you sure you want to delete this image?')) {
            return;
        }
        e.preventDefault();
        _this = $(this);
        _id = _this.data('id');
        _anchor = $(event.target);
        _tr = $(_anchor).closest('tr');
        $.get('/media/delete/'+_id, function(resp) {
            if(resp.failed) {
                alert(resp.errors);
            } else {
                //remove the element from DOM
                _tr.remove();
            }
        });
    });

    // New Prodtranslation
    $(".btn-new-prodtranslation").on("click", function(e) {
        e.preventDefault();

        _this = $(this);

        $.get('/dashboard/prodtranslation/new/'+_this.data('product_id'), function(html) {
            showDialogWithContents(html, function(){
                $('div#dialog-wrapper').css("width", "800");
            });
        });
    });

    // Edit Prodtranslation
    $(document).on('click', 'a.edit-prodtranslation', function(e){
        e.preventDefault();

        _this = $(this);

        $.get(_this.attr('href'), function(html) {
            showDialogWithContents(html, function(){
                                 
            });
        });
    });

    // Delete Prodtranslation
    $(document).on('click', 'a.delete-prodtranslation', function(e){
        e.preventDefault();

        _this = $(this);

        if(confirm("Are you sure you want to delete this prodtranslation?")) {
            $.get(_this.attr('href'), function(resp) {
                if(!resp.failed) {
                    _this.closest('tr').hide().remove();
                }
            });
        }
    });
    
    // Save Prodtranslation
    $(document).on('click', 'a.save-new-prodtranslation', function(e){
        e.preventDefault();

        _this = $(this);
        _frm  = $('#new-prodtranslation-form');

        _this.prop('disabled', true);
        _this.html('Loading...');

        $.post(_frm.attr('action'), _frm.serializeArray(), function(resp) {
            if(resp.failed) {
                alert(resp.errors);
            } else {
                
                $.get(resp.view_url, function(html) {
                    $('#prodtranslation-list tbody').html(html);
                    _this.prop('disabled', false);
                    _this.html('Save');
                    closeDialog();
                });
            }
        });            
        
    });

    $(document).on('click', ".form-buttons .pure-button-primary", function(e){
        if ($('ul#partner-listing li.empty').html() == 'No Partner Associated') {
            e.preventDefault();
            $('ul#partner-listing').next().css('display','block');
            $('ul#partner-listing').before().get(0).scrollIntoView();
            return;
        }

        if ($('ul#network-listing li.empty').html() == 'No Networks Associated') {
            e.preventDefault();
            $('ul#network-listing').next().css('display','block');
            $('ul#network-listing').before().get(0).scrollIntoView();
            return;
        }
    });
    
    // Edit Prodtranslation
    $(document).on('click', 'a.edit-reward_partner', function(e){
        e.preventDefault();

        _this = $(this);

        $.get(_this.attr('href'), function(html) {
            showDialogWithContents(html, function(){
                                 
            });
        });
    });
    
    // Edit Prodtranslation
    $(document).on('click', 'a.edit-upc', function(e){
        e.preventDefault();

        _this = $(this);

        $.get(_this.attr('href'), function(html) {
            showDialogWithContents(html, function(){
                                 
            });
        });
    });
    
    // Edit country details
    $(document).on('click', 'a.edit-country', function(e){
        e.preventDefault();

        _this = $(this);

        $.get(_this.attr('href'), function(html) {
            showDialogWithContents(html, function(){
                                 
            });
        });
    });
    
    // Edit country details
    $(document).on('click', 'a.edit-redemption-partner', function(e){
        if(!confirm('By leaving this page witout saving, any changes will be discarded!')){
            e.preventDefault();
            return false;
        }

    });
    
    // get prefix by partners
    $(document).on('change', 'select[name="partner_id"]', function(e){
        var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9+/=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/rn/g,"n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}

       $.get('/dashboard/productpricing/get_prefix/'+Base64.encode($(this).val()), function(html) {
            var $el = $('select[name="partner_prefix"]');
            $el.empty(); // remove old options
            $.each(html, function(key,value) {
              $el.append($("<option></option>")
                 .attr("value", key).text(value));
            });
        });

    });
});
