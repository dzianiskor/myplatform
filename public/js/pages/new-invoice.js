$(document).ready(function() {
    //when selecting the parent partner,
    //assign the same networks

    // Edit User Invoice
    $(document).on('click', 'a.edit-userinvoice', function(e){
        e.preventDefault();

        _this = $(this);

        $.get(_this.attr('href'), function(html) {
            showDialogWithContents(html, function(){
                console.log("html");            
            });
        });
    });

    // Delete Store
    $(document).on('click', 'a.delete-userinvoice', function(e){
        e.preventDefault();

        _this = $(this);

        if(confirm("Are you sure you want to delete this user invoice?")) {
            $.get(_this.attr('href'), function(resp) {
                if(!resp.failed) {
                    _this.closest('tr').hide().remove();
                }
            });
        }
    });

    // Save Store
    $(document).on('click', 'a.save-new-userinvoice', function(e){
        e.preventDefault();

        _this = $(this);
        _frm  = $('#new-userinvoice-form');

        var failed = false;

            _this.prop('disabled', true);
            _this.html('Loading...');

            $.post(_frm.attr('action'), _frm.serializeArray(), function(resp) {
                console.log(resp);
                if(resp.failed) {
                    alert(resp.errors);
                } else {
                    
                    window.location = resp.view_url;
//                    $.get(resp.view_url, function(html) {
//                        console.log(html);
//                        return;
//                        $('#userinvoice-list tbody').html(html);
//                        _this.prop('disabled', false);
//                        _this.html('Save');
//                        closeDialog();
//                    });
                }
            });            
        
    });
});