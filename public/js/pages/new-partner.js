function populateNetworks(sender) {
    _this = $(sender);
    if (_this.data("draft") == 1) {
        _selected = _this.val();
        _id = _this.data("id");
        $.get("/dashboard/partners/resetnetworks/" + _id + "/" + _selected, function(html) {
            $("#network-listing").html(html);
        });
    }
}

function checkPOSMappingIds(sender) {
    
    $.get("/dashboard/store/check-pos-mapping-id/" + $(".btn-new-store").data('partner_id') + "/" + sender, function(html) {
        console.log(html);
            failed =  html.failed ;
            console.log(failed);
            return failed;
    });
}

$(document).ready(function() {
    //when selecting the parent partner,
    //assign the same networks

    populateNetworks($("#parent_id"));

    $("#parent_id").on("change", function(e){
        populateNetworks(this);
    });

    $("#parent_id").on("blur", function(e){
        populateNetworks(this);
    });

    // New Store
    $(".btn-new-store").on("click", function(e) {
        e.preventDefault();

        _this = $(this);

        $.get('/dashboard/store/new/'+_this.data('partner_id'), function(html) {
            showDialogWithContents(html, function(){
                var loc = new google.maps.LatLng($('#store_latitude').val(), $('#store_longitude').val());

                var map = new google.maps.Map(document.getElementById("map"), {
                    center: loc,
                    zoom: 11
                });

                var marker = new google.maps.Marker({
                    position: loc,
                    map: map,
                    title:"Drag to location",
                    draggable:true
                });

                google.maps.event.addListener(marker, 'dragend', function(evt) {
                    $('#store_latitude').val(evt.latLng.lat().toFixed(6));
                    $('#store_longitude').val(evt.latLng.lng().toFixed(6));
                });
            });
        });
    });

    // Edit Store
    $(document).on('click', 'a.edit-store', function(e){
        e.preventDefault();

        _this = $(this);

        $.get(_this.attr('href'), function(html) {
            showDialogWithContents(html, function(){
                var loc = new google.maps.LatLng($('#store_latitude').val(), $('#store_longitude').val());

                var map = new google.maps.Map(document.getElementById("map"), {
                    center: loc,
                    zoom:11
                });

                var marker = new google.maps.Marker({
                    position: loc,
                    map: map,
                    title:"Drag to location",
                    draggable:true
                });

                google.maps.event.addListener(marker, 'dragend', function(evt) {
                    $('#store_latitude').val(evt.latLng.lat().toFixed(6));
                    $('#store_longitude').val(evt.latLng.lng().toFixed(6));
                });
            });
        });
    });

    // Delete Store
    $(document).on('click', 'a.delete-store', function(e){
        e.preventDefault();

        _this = $(this);

        if(confirm("Are you sure you want to delete this store?")) {
            $.get(_this.attr('href'), function(resp) {
                if(!resp.failed) {
                    _this.closest('tr').hide().remove();
                }
            });
        }
    });

    // Remove POS
    $(document).on('click', '#pos-table a', function(e){
        e.preventDefault();

        if($('#pos-table').find('tr').length == 1) {
            $('#pos-table input').val('');
        } else {
            $(this).closest('tr').fadeOut().remove();
        }
    });

    // Add POS
    $(document).on('click', 'a.add-pos', function(e){
        e.preventDefault();

        $('#pos-table').append($('#pos-table tr').first().clone());

		$('input[name="pos_name[]"]').last().val("");
		$('input[name="pos_id[]"]').last().val("");
		$('input[name="pos_mapping[]"]').last().val("");

        adaptInterface();
    });

    // Save Store
    $(document).on('click', 'a.save-new-store', function(e){
        e.preventDefault();

        _this = $(this);
        _frm  = $('#new-store-form');

        var failed = false;
        var failed1 = false;
        var msg = '';
        $.each($('input[name="pos_name[]"]'), function(i, el){
            if($(this).val() == '') {
                failed = true;
                msg = "Please make sure you have give a valid name to each Point of Sale";
            }
        });

        var mapping_ids = [];
        if($(this).val() != '') {
            mapping_ids.push($(this).val());
        }

        var sorted_mapping_ids = mapping_ids.slice().sort();
        var results = [];
        for (var i = 0; i < mapping_ids.length - 1; i++) {
            if (sorted_mapping_ids[i + 1] == sorted_mapping_ids[i]) {
                results.push(sorted_mapping_ids[i]);
            }
        }

        if (failed == true) {

            alert("Please make sure you have give a valid name to each Point of Sale");

        } else if(results.length > 0) {

            alert("Please make sure you use the mapping id only one time.");

        } else {
            $.each($('input[name="pos_mapping[]"]'), function(i, el){
                if($(this).val() != '') {
                    $.ajax({
                        url : "/dashboard/store/check-pos-mapping-id/" + $(".btn-new-store").data('partner_id') + "/" + $(this).val() + "/" + $('#store_id').val(),
                        success : function(resp){
                            failed1 =  resp.failed ;
                        },
                        async: false
                    });
                    if (failed1 == true){
                        return false;
                    }
                }
            });
                
            if (failed1 == true) {
                alert("the mapping id is already used");
            } else {
                _this.prop('disabled', true);
                _this.html('Loading...');

                $.post(_frm.attr('action'), _frm.serializeArray(), function(resp) {
                    if(resp.failed) {
                        alert(resp.errors);
                    } else {
                        $.get(resp.view_url, function(html) {
                            $('#store-list tbody').html(html);
                            _this.prop('disabled', false);
                            _this.html('Save');
                            closeDialog();
                        });
                    }
                });
            }
        }
    });
    
    // Save Store
    $(document).on('change', 'input[name="name"]', function(e){
        if($('input[name="draft"]').val() != 1){
            return false;
        }
        var partner_id = $('input[name="partner_id"]').val();
        var name = $(this).val();
        $.post("/dashboard/partners/update_name/" + partner_id + "/" + name, function(response) {
            $('select[name="issued_by"]').empty();
            $.each(response, function(key,value) {
            $('select[name="issued_by"]').append($("<option></option>").val(key).text(value));
            });
        });
    });
});