$(document).ready(function() {

    // New segment user
    $(".btn-new-segmentuser").on("click", function(e) {
        e.preventDefault();

        _this = $(this);

        $.get('/dashboard/segmentuser/new/'+_this.data('segment_id'), function(html) {
            showDialogWithContents(html, function() {
                $('div#dialog-wrapper').css("width", "800");
            });
        });
    });

    // Delete segmentuser
    $(document).on('click', 'a.delete-segmentuser', function(e){
        e.preventDefault();

        _this = $(this);

        if(confirm("Are you sure you want to delete this user?")) {
            $.get(_this.attr('href'), function(resp) {
                if(!resp.failed) {
                    _this.closest('tr').hide().remove();
                }
            });
        }
    });
    
    // Save segmentuser
    $(document).on('click', 'a.save-new-segmentuser', function(e) {
        e.preventDefault();

        _this = $(this);
        _frm  = $('#new-segmentuser-form');

        _this.prop('disabled', true);
        _this.html('Loading...');

        $.post(_frm.attr('action'), _frm.serializeArray(), function(resp) {
            if(resp.failed) {

                alert(resp.errors);
                _this.prop('disabled', false);
                _this.html('Save');

            } else {

                $.get(resp.view_url, function(html) {
                    $('#segmentuser-list tbody').html(html);
                    _this.prop('disabled', false);
                    _this.html('Save');
                    closeDialog();
                });
            }
        });

    });

    // User Search
    $(document).on('click', '#users-search', function(event) {
        event.preventDefault();

        if ($("#users-autocomplete").val().length >= 2) {

            event.preventDefault();
            $('select#user_id').empty();

            $.get('/dashboard/reports/members?term='+$('#users-autocomplete').val(), function(resp) {
                $.each(resp, function(index, value) {
                    jQuery('<option/>', {
                        value: value.id,
                        html: value.value
                    }).appendTo('select#user_id'); //appends to select if parent div has id dropdown
                    $('select#user_id').val(value.id);
                });
            });

            return false;

        } else {
            event.preventDefault();
            return false;
        }
    });


    $(document).bind('keypress', '#users-autocomplete', function(event) {
        if(event.keyCode == 13) {
            if($("#users-autocomplete").val().length >=2 ) {

                event.preventDefault();
                $('select#user_id').empty();

                $.get('/dashboard/reports/members?term='+$('#users-autocomplete').val(), function(resp){
                    $.each(resp, function(index, value){
                        jQuery('<option/>', {
                            value: value.id,
                            html: value.value
                        }).appendTo('select#user_id');//appends to select if parent div has id dropdown
                        $('input[name=user_id]').val(value.id);
                    });
                });
                return false;

            } else {
                event.preventDefault();
                return false;
            }
        }

        // Turn on the global items search autocomplete function
        $("#users-autocomplete").autocomplete({
            source: "/dashboard/reports/members",
            minLength: 3,
            select: function( event, ui ) {
                jQuery('<option/>', {
                    value: ui.item.id,
                    html: ui.item.value
                }).appendTo('select#user_id').attr('selected', true); //appends to select if parent div has id dropdown
                $('input[name=user_id]').val(ui.item.id);
            }
        });
    });

    $(document).on('change', 'select#user_id', function() {
        $('input[name=user_id]').val($(this).val());
    });
});