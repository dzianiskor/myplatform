$(document).ready(function() {
   // New Brand
    $(".member-sell").on("click", function(e) {
        e.preventDefault();

        _this = $(this);
        _member_id = _this.data('member_id')

        $.get('/dashboard/call_center/sell_item_popup/'+_member_id, function(html) {
            showDialogWithContents(html);
        });
    });

    $(document).on("click", ".sell-item", function(e) {
        e.preventDefault();

        _this = $(this);
        _member_id = _this.data('member_id')
        _product_ucid = $(document).find('input[name="product_ucid"]').val();


        $.get('/dashboard/call_center/sell_item/'+_product_ucid+'/'+_member_id, function(resp) {
            if(resp.failed) {
                alert(resp.errors);
            } else {
                closeDialog();
            }
        });
    });

    // dropdown to select partner or member
    $("#query_source").on("change", function(e) {
        e.preventDefault();
        _this = $(this);
        _selected = _this.val();
        // clear out form on right hand side
        $('#source').html('');
        $.get('/dashboard/call_center/get_list/'+_selected, function(html) {
            $('#source_id').html(html);
        });
    });

    // different partner or member selected
    $("#source_id").on("change", function(e) {
        e.preventDefault();

        _type = $("#query_source").val();
        _this = $(this);
        _selected = _this.val();
        $.get('/dashboard/call_center/get_form/'+_type+'/'+_selected, function(html) {
            $('#source').html(html);
        });
    });

    //save member button clicked
    $(".member-save").on("click", function(e){
        e.preventDefault();

        _frm  = $('#member-form');
        _action = _frm.attr('action');

        $.post(_action, _frm.serializeArray(), function(resp) {
            if(resp.failed) {
                alert(resp.errors);
            } else {
                alert('Member saved');
            }
        });
    });

    //save partner button clicked
    $(".partner-save").on("click", function(e){
        e.preventDefault();

        _frm  = $('#partner-form');
        _action = _frm.attr('action');

        $.post(_action, _frm.serializeArray(), function(resp) {
            if(resp.failed) {
                alert(resp.errors);
            } else {
                alert('Partner saved');
            }
        });
    });

});