/**
 * Adjust the interface bindings on size changes
 *
 * @return void
 */
function adaptInterface() {
    if (!$('#main-container')[0]) {
        return;
    }
    $('#main-container').height($(window).outerHeight());
    $('#main-container').width($(window).outerWidth());
    $('#left-navigation').height($(window).outerHeight(true));
    $('#scroll-rail').height($('#left-navigation').outerHeight() - $('#left-navigation .pure-form').outerHeight());
    $('#scroll-rail').css('top', $('#left-navigation .pure-form').outerHeight());
    $('#scroll-rail').width($('#left-navigation').outerWidth());
    $('#content-area').height($(window).outerHeight());
    $('#content-area').width($(window).outerWidth() - $('#left-navigation').outerWidth(true));
    $('div#dialog-container').height($(window).outerHeight(true));
    $('div#dialog-container').width($(window).outerWidth(true));
    $('#content-area #content').width($('#content-area').outerWidth());
    $('#content-area #content').height($('#content-area').outerHeight() - $('#content-area header').outerHeight());
    $('#chart-canvas').width($('#dashboard-chart').outerWidth());
    $('#chart-canvas').height($('#dashboard-chart').outerHeight());

    centerDialog();
}

/**
 * Center the global dialog
 *
 * @return void
 */
function centerDialog()
{
    container = $('#dialog-container');
    container.width($(document).outerWidth(true));
    container.height($(document).outerHeight(true));

    wrapper = $('#dialog-wrapper');
    wrapper.css('left', ($(window).outerWidth(true) / 2) - (wrapper.outerWidth(true) / 2));
    wrapper.css('top', 100);

    $( "#tabs" ).tabs();
}

/**
 * Bind the dialog close button
 *
 * @return void
 */
function bindDialogButtons()
{
    $(document).on("click", "#dialog-wrapper a.close", function(e) {
        e.preventDefault();
        closeDialog();
        adaptInterface();
    });
}

/**
 * Display the global dialog with a content ID
 *
 * @param string elID
 */
function showDialog(elID, cb)
{
    $('#dialog-wrapper').width($(elID).outerWidth());
    $('div#dialog-wrapper .body').html($(elID).html());
    $('div#dialog-container').fadeIn('fast');

    if (cb && typeof(cb) === "function") {
      cb();
    }

    adaptInterface();
}

/**
 * Display the global dialog passing content directly
 *
 * @param string elID
 */
function showDialogWithContents(html, cb)
{
    // set a default width;
    $('div#dialog-wrapper .body').html(html);
    $('div#dialog-container').fadeIn('fast');

    if (cb && typeof(cb) === "function") {
      cb();
    }

    adaptInterface();
}

/**
 * Display the global dialog with a content ID
 *
 * @param string elID
 */
function closeDialog()
{
    $('div#dialog-container').fadeOut('fast', function(){
        $('div#dialog-wrapper .body').html('');
    });
}

/**
 * Display all elements within a form
 *
 * @param string frm
 */
function disableForm(frm)
{
    $(frm).find('*').prop('disabled', true);
}

/**
 * Set the main menu active state
 *
 * @param string opt
 * @return void
 */
function set_menu(opt) {
    $('#left-navigation ul li').removeClass('active');
    $('#'+opt).addClass('active');
}

/**
 * Explicitly load the specified page script
 *
 * @param string src
 */
function enqueue_script(src) {
    document.write('<script src="/js/pages/'+src+'.js"><\/script>');
}

function configureValidate() {
    $(".required").prev().append("<span class='asterix'>*</span>");
    $(".required").attr("required",'');
    var _rules = {rules:
                    {confirm_password:{equalTo:"#password"},
                             password:{minlength:3},
                                email:{email:true},
                           valid_from:{date:true},
                             valid_to:{date:true}
                    }
                 };
    if ($("form")[0] && $("#form").validate) {
        $("form").validate(_rules);
        $("form").on('submit', function(e){
            var _validator = $(this).validate(_rules);
            var _valid = $(this).valid();
            if(!_valid){
                e.preventDefault();
            }
        });
    }
}
function isFloat(){
    if ((event.which !== 46 || $(this).val().indexOf('.') !== -1) && (event.which < 48 || event.which > 57)) {
      event.preventDefault();
    }
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode !== 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

var operatorArray = [];
var typeArray = [];
var comparisonArray = [];
var valueArray = [];
var valueCurrencyArray = [];

function addExceptionRuleEvents(){
    $('select.rule-item').off();
    $('#dialog-wrapper .type-forms').each(function(i){
        var index = i;
        $('select.rule-item', this).change(function() {
            $(this).parent().next('.property-forms').find('form').hide();
            $(this).parent().next('.property-forms').find('.form_x').fadeIn();
            $(this).parent().next('.property-forms').find($(this).find('option:selected').data('index')).fadeIn();
            $('#dialog-wrapper form.form_hidden_attributes').fadeIn();
            typeArray[index] = $(this).val();
            $('input[name=rule_type]').val(JSON.stringify(typeArray));
            $('input[name=rule_value]').val(JSON.stringify(valueArray));
            $('input[name=rule_comparison]').val(JSON.stringify(comparisonArray));
            $('input[name=rule_operator]').val(JSON.stringify(operatorArray));
        });

        // Set the rule comparison in the post form
        $('select#rule_comparison', this).change(function() {
            comparisonArray[index] = $(this).val();
            $('input[name=rule_comparison]').val(JSON.stringify(comparisonArray));
        });

        // Set the rule value in the post form
        $('select#rule_value', this).change(function() {
            valueArray[index] = $(this).val();
            $('input[name=rule_value]').val(JSON.stringify(valueArray));
            console.log(typeArray);
            console.log(comparisonArray);
            console.log(valueArray);
            console.log(operatorArray);
        });

        $('select#rule_value_items', this).change(function() {
            valueArray[index] = $(this).val();
            $('input[name=rule_value]').val(JSON.stringify(valueArray));
            console.log(typeArray);
            console.log(comparisonArray);
            console.log(valueArray);
            console.log(operatorArray);
        });

        // Set the rule operator in the post form
        $('select#rule_operator', this).change(function() {
            operatorArray[index] = $(this).val();
            $('input[name=rule_operator]').val(JSON.stringify(operatorArray));
        });

        $('.items-search', this).click(function(event) {
            if($(this).parent().find(".items-autocomplete").val().length>=3) {

                event.preventDefault();
                $(this).parent().find('select.rule_value_items').empty();
                $.get('/dashboard/loyalty/products?term='+$(this).parent().find('.items-autocomplete').val(), function(resp){
                      console.log(resp);
                    $.each(resp, function(index, value){
                        jQuery('<option/>', {
                            value: value.id,
                            html: value.value
                        }).appendTo($(this).parent().find('select.rule_value_items'));//appends to select if parent div has id dropdown
                        $(this).parent().find('select.rule_value_items').trigger('change');
                        $('input[name=rule_value_items]').val(value.id);
                    });
                });
                return false;
            }else{
                event.preventDefault();
                return false;
            }
        });
        $('.items-autocomplete', this).keypress(function(event) {
            console.log("here");
            if(event.keyCode == 13) {
                if($(this).parent().find(".items-autocomplete").val().length>=3) {
                    event.preventDefault();
                    $(this).parent().find('select.rule_value_items').empty();
                    $.get('/dashboard/loyalty/products?term='+$(this).parent().find('.items-autocomplete').val(), function(resp){
                        $.each(resp, function(index, value){
                            jQuery('<option/>', {
                                value: value.id,
                                html: value.value
                            }).appendTo($(this).parent().find('select.rule_value_items'));//appends to select if parent div has id dropdown
                            $(this).parent().find('select.rule_value_items').trigger('change');
                            $('input[name=rule_value_items]').val(value.id);
                            });
                        });
                    return false;
                }else{
                    event.preventDefault();
                    return false;
                }
            }
            // Turn on the global items search autocomplete function
            $(this).parent().find(".items-autocomplete").autocomplete({
                source: "/dashboard/loyalty/products",
                minLength: 3,
                select: function( event, ui ) {
                    console.log(ui);
                    $('input[name="rule_value"]').val(ui.item.id);
                    jQuery('<option/>', {
                        value: ui.item.id,
                        html: ui.item.value
                    }).appendTo($(this).parent().find('select.rule_value_items')).attr('selected', true); //appends to select if parent div has id dropdown
                    $(this).parent().find('select.rule_value_items').trigger('change');
                    $('input[name=rule_value_items]').val(ui.item.id);
                }
            });
        });

        $('.mcc-search', this).click(function(event) {
            if($(this).parent().find(".mcc-autocomplete").val().length>=3) {
                event.preventDefault();
                $(this).parent().find('select.rule_value_items').empty();
                $.get('/dashboard/loyalty/mcccodes?term='+$(this).parent().find('.mcc-autocomplete').val(), function(resp){
                    $.each(resp, function(index, value){
                        jQuery('<option/>', {
                            value: value.id,
                            html: value.value
                        }).appendTo($(this).parent().find('select.rule_value_items'));//appends to select if parent div has id dropdown
                        $(this).parent().find('select.rule_value_items').trigger('change');
                        $('input[name=rule_value_items]').val(value.id);
                    });
                });
                return false;
            }else{
                event.preventDefault();
                return false;
            }
        });
        $('.mcc-autocomplete', this).keypress(function(event) {
            if(event.keyCode == 13) {
                if($(this).parent().find(".items-autocomplete").val().length>=3) {
                    event.preventDefault();
                    $(this).parent().find('select.rule_value_items').empty();
                    $.get('/dashboard/loyalty/mcccodes?term='+$(this).parent().find('.mcc-autocomplete').val(), function(resp){
                        $.each(resp, function(index, value){
                            jQuery('<option/>', {
                                value: value.id,
                                html: value.value
                            }).appendTo($(this).parent().find('select.rule_value_items'));//appends to select if parent div has id dropdown
                            $(this).parent().find('select.rule_value_items').trigger('change');
                            $('input[name=rule_value_items]').val(value.id);
                        });
                    });
                        return false;
                }else{
                    event.preventDefault();
                    return false;
                }
            }
            // Turn on the global items search autocomplete function
            $(this).parent().find(".mcc-autocomplete").autocomplete({
                source: "/dashboard/loyalty/mcccodes",
                minLength: 3,
                select: function( event, ui ) {
                    console.log(ui);
                    $('input[name="rule_value"]').val(ui.item.id);
                    jQuery('<option/>', {
                        value: ui.item.id,
                        html: ui.item.value
                    }).appendTo($(this).parent().find('select.rule_value_items')).attr('selected', true); //appends to select if parent div has id dropdown
                    $(this).parent().find('select.rule_value_items').trigger('change');
                    $('input[name=rule_value_items]').val(ui.item.id);
                }
            });
        });

        $('.trxType-search', this).click(function(event) {
            if($(this).parent().find(".trxType-autocomplete").val().length>=1) {
                event.preventDefault();
                $(this).parent().find('select.rule_value_items').empty();
                $.get('/dashboard/loyalty/trxtypes?term='+$(this).parent().find('.trxType-autocomplete').val(), function(resp){
                    $.each(resp, function(index, value){
                        jQuery('<option/>', {
                            value: value.id,
                            html: value.value
                        }).appendTo($(this).parent().find('select.rule_value_trxType'));//appends to select if parent div has id dropdown
                        $(this).parent().find('select.rule_value_trxType').trigger('change');
                        $('input[name=rule_value_items]').val(value.id);
                    });
                });
                return false;
            }else{
                event.preventDefault();
                return false;
            }
        });

        $('.trxType-autocomplete', this).keypress(function(event) {
            if(event.keyCode == 13) {
                if($(this).parent().find(".trxType-autocomplete").val().length>=3) {
                    event.preventDefault();
                    $(this).parent().find('select.rule_value_items').empty();
                    $.get('/dashboard/loyalty/trxtypes?term='+$(this).parent().find('.trxType-autocomplete').val(), function(resp){
                        $.each(resp, function(index, value){
                            jQuery('<option/>', {
                                value: value.id,
                                html: value.value
                            }).appendTo($(this).parent().find('select.rule_value_trxType'));//appends to select if parent div has id dropdown
                            $(this).parent().find('select.rule_value_trxType').trigger('change');
                            $('input[name=rule_value_items]').val(value.id);
                        });
                    });
                        return false;
                }else{
                    event.preventDefault();
                    return false;
                }
            }
            // Turn on the global Transation Types search autocomplete function
            $(this).parent().find(".trxType-autocomplete").autocomplete({
                source: "/dashboard/loyalty/trxtypes",
                minLength: 1,
                select: function( event, ui ) {
                    console.log(ui);
                    valueArray[index] = ui.item.id;
                    $('input[name=rule_value]').val(JSON.stringify(valueArray));
//                    $('input[name="rule_value"]').val(ui.item.id);
                    jQuery('<option/>', {
                        value: ui.item.id,
                        html: ui.item.value
                    }).appendTo($(this).parent().find('select.rule_value_trxType')).attr('selected', true); //appends to select if parent div has id dropdown
                    $(this).parent().find('select.rule_value_trxType').trigger('change');
                    $('input[name=rule_value_items]').val(ui.item.id);
                }
            });
        });

        // Set the rule value in the post form
        $('input#rule_value_amount', this).keyup(function() {
            valueArray[index] = $(this).val();
            $('input[name=rule_value]').val(JSON.stringify(valueArray));
        });

        // Set the rule value currency in the post form
        $('select#rule_value_currency', this).change(function() {
            valueCurrencyArray[index] = $(this).val();
            $('input[name=rule_value_currency]').val(JSON.stringify(valueCurrencyArray));
        });
    });
}

function setupAjax() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
}

// Off we go...
$(function() {
    setupAjax();
    adaptInterface();
    bindDialogButtons();
    configureValidate();


    if($('#redemption-order-table').length){
        console.log('data table');
        $('#redemption-order-table').DataTable({
            "paging":   false,
            "ordering": false,
            "info":     false,
            "searching": false,
        });
    }

    $('.dynamic-off-state').each(function(){
        if($(this).val() == '') {
            $(this).addClass('disabled');
        }
    });

    if ($("#main-container")[0]) {
        $("#main-container").stop().animate({
            opacity: 1.0,
        }, 200, function() {
            $('body').removeClass('loading');
        });
    }

    $(window).resize(function(){
        adaptInterface();
    });

    $(document).on('click', 'a.close-dialog', function(e){
        e.preventDefault();
        closeDialog();
    });

    $(document).on('click', '.dialog', function(e){
        e.preventDefault();
        showDialog($(this).attr('href'));
        if($(this).attr('href') == '#dialog-add-rule'){
            typeArray = [];
            valueArray = [];
            comparisonArray = [];
            operatorArray = [];
            // Show the correct rule table
            addExceptionRuleEvents();
            //add new exception rule type
            $('a.btn-new-type').click(function(e){
                e.preventDefault();
                $('#dialog-wrapper .exception-type-wrapper').append($('#dialog-wrapper .type-forms:first').clone());
                addExceptionRuleEvents();
            });
        }
    });

    $(document).on('click', 'a.delete', function(e) {
        if(!confirm('Are you sure you want to delete this item?')) {
            e.preventDefault();
        }
    });

    if($('input.datetime-now').length > 0) {
        $('input.datetime-now').datetimepicker({
            timepicker:true,
            formatDate:'Y/m/d',
            minDate:'0'
        });
    }

    // Email WhiteLabeling
    $('a.btn-new-lang-name').click(function(e){
        e.preventDefault();
        var clone = $('#program_name_div').clone();
        clone.find('.chosen-container-single').remove();
        clone.find('.pure-input-1').val('');
        clone.find('.remove-cloned').css('display', '');
        $('#program_name_section').append(clone);
        $('#program_name_section:last select').chosen();
    });

    $('a.btn-new-lang-name-in-subject').click(function(e){
        e.preventDefault();
        var clone = $('#program_subject_div').clone();
        clone.find('.chosen-container-single').remove();
        clone.find('.pure-input-1').val('');
        clone.find('.remove-cloned').css('display', '');
        $('#program_subject_section').append(clone);
        $('#program_subject_section:last select').chosen();
    });

    $('a.btn-new-lang-name-in-content').click(function(e){
        e.preventDefault();
        var clone = $('#program_content_div').clone();
        clone.find('.chosen-container-single').remove();
        clone.find('.pure-input-1').val('');
        clone.find('.remove-cloned').css('display', '');
        $('#program_content_section').append(clone);
        $('#program_content_section:last select').chosen();
    });

    $(document).on('click', 'a.remove-cloned', function(e){
        e.preventDefault();
        $(this).parent().remove();
    });



    // Expand Lists
    $(document).on('click', 'a.plus', function(e){
        e.preventDefault();
        $(this).closest('.checkbox-wrap').find('label.entity-name').fadeIn().css('display', 'block');
        $(this).find('i').removeClass('fa-caret-right').addClass('fa-caret-down');
        $(this).removeClass('plus').addClass('minus');
    });

    // Collapse Lists
    $(document).on('click', 'a.minus', function(e){
        e.preventDefault();
        $(this).closest('.checkbox-wrap').find('label.entity-name').fadeOut().hide();
        $(this).find('i').removeClass('fa-caret-down').addClass('fa-caret-right');
        $(this).removeClass('minus').addClass('plus');
    });

    if($('.balance-tooltip').length > 0) {
        $('.balance-tooltip').tooltipster({
            contentAsHTML: true,
            content: 'Loading...',
            updateAnimation: false,
            theme:'tooltipster-light',
            functionBefore: function(origin, continueTooltip) {

                continueTooltip();

                if (origin.data('ajax') !== 'cached') {
                    $.ajax({
                        type: 'GET',
                        url: '/dashboard/members/balances/'+origin.data('id'),
                        success: function(data) {
                            origin.tooltipster('content', data).data('ajax', 'cached');
                        }
                    });
                }
            }
        });
    }

    // Bind Esc Key
    $(document).keyup(function(e) {
        if(e.keyCode === 27) {
            if($('div#dialog-container').is(':visible')) {
                closeDialog();
            }
        }
    });

    // Dynamically add on/off states based on input
    $(document).on('keyup', '.dynamic-off-state', function(){
        if($(this).val() != '') {
            $(this).removeClass('disabled');
        } else {
            $(this).addClass('disabled');
        }
    });

    // Handle the navigation search
    $(document).on('input', '#nav-search', function(e) {
        var q = $(this).val();

        var regex = new RegExp(q, "gi");

        if(q == '') {
            $('#left-navigation li').show();
        } else {
             $('#left-navigation li').each(function(){
                _this = $(this);

                if(!_this.find('a').html().match(regex)) {
                    _this.fadeOut();
                }
            });
        }
    });

    /*** Handle the dynamic dropdowns ***/

    // Bind the timepickers
    $(document).on('click', 'input.timepicker', function(event) {
        $(this).timepicker({'timeFormat': 'H:i', 'step': 60}).focus();
    });

    // Bind the datepickers
    $(document).on('click', 'input.datepicker', function(event) {
        $(this).datepicker({
            showOn: 'focus',
			minDate: 0
        }).focus();
    });

    $(document).on('click', 'input.datepicker-year', function(event) {
        $(this).datepicker({
            showOn: 'focus',
            changeMonth: true,
            changeYear: true,
			minDate: 0
        }).focus();
    });

    $(document).on('click', 'input.birthdatepicker', function(event) {
        $(this).datepicker({
            showOn: 'focus',
            changeMonth: true,
            changeYear: true,
			maxDate: 0
        }).focus();
    });
    $(document).on('click', 'input.earndatepicker', function(event) {
        $(this).datepicker({
            showOn: 'focus',
            changeMonth: true,
            changeYear: true,
			maxDate: 0
        }).focus();
    });
    $(document).on('click', 'input.datepicker-report', function(event) {
        $(this).datepicker({
            showOn: 'focus',
            changeMonth: true,
            changeYear: true,
            //dateFormat: 'd/m/Y'
        }).focus();
    });

	// Load partnercountries in coupon
    $(document).on('change', '#partner_id', function(){
//        $.get('/dashboard/coupons/partnercountries/'+$(this).val(), function(resp){
//            $('#country_id').html(resp);
//        });
    });

    // Load areas
    $(document).on('change', '[data-country]', function(){
        $.get('/dashboard/locale/areas/'+$(this).val(), function(resp){
            $('[data-area]').html(resp);

            if( resp.indexOf('All Areas') >= 0){
                $('[data-city]').html('<option>All Cities</option>');
            } else {
                $('[data-area]').change();
            }
        });
    });

    // Load Cities
    $(document).on('change', '[data-area]', function(){
        $.get('/dashboard/locale/cities/'+$(this).val(), function(resp){
            $('[data-city]').html(resp);
        });
    });

    // Add Attributes
    $(document).on('click','.attribute-action', function(e){
        e.preventDefault();

        _this = $(this);

        _href = $(this).attr('href');
        _data = $(this).closest('form').serialize();
        $.post(_href, _data, function(data, status, xhr) {
            if(xhr.getResponseHeader('content-type') == 'application/json') {
                alert(data.message);
            } else {
                if(_this.data('target') == '#channel-listing' && _this.data('product_redemption_id') != undefined){
                    $.get('/dashboard/productpricing/channels_segments/'+_this.data('product_redemption_id'), _data, function(data1, status, xhr) {
                        var $el = $("#segment_list");
                        $el.empty(); // remove old options
                        $.each(data1, function(key,value) {
                          $el.append($("<option></option>")
                             .attr("value", key).text(value));
                        });
                    });
                }
                $(_this.data('target')).html(data);

                closeDialog();
            }
        });
    });

    // Submit an action-reload form (reports)
    $(document).on('click','.attribute-action-reload', function(e){
        e.preventDefault();

        var data = {};
        if($('.bypartner:checked')){
            data.bypartner = $('.bypartner:checked').val();
        }
        if($('.zerobalance:checked')){
            data.zerobalance = $('.zerobalance:checked').val();
        }
        if($('input[name=member_id]').length > 0){
            data.member_id = $('input[name=member_id]').val();
            data.reference_number = $('input[name=reference_number]').val();
        }
        // Verify that we have networks
        if($('.network_ids').length > 0) {

            $(".network_ids").each(function () {
                if($(this).is("input") && $(this).is(":checked")){
                    if(!data.network_ids) {
                        data.network_ids = $(this).val();
                    } else {
                        data.network_ids += "," + $(this).val();
                    }
                }

                if($(this).is('select')) {
                    if(!data.network_ids) {
                        data.network_ids = $(this).val();
                    } else {
                        data.network_ids += "," + $(this).val();
                    }
                }

                if($(this).is('input[type="hidden"]') && $(this).val() != '') {
                    if(!data.network_ids) {
                        data.network_ids = $(this).val();
                    } else {
                        data.network_ids += "," + $(this).val();
                    }
                }
            });

            if(!data.network_ids) {
                alert("Please select Network for this report.");
                return false;
            }
        }


        // Verify that we have entities
        if($('.parent_ids').length > 0) {

            $(".parent_ids").each(function () {
                if($(this).is("input") && $(this).is(":checked")){
                    if(!data.parent_ids) {
                        data.parent_ids = $(this).val();
                    } else {
                        data.parent_ids += "," + $(this).val();
                    }
                }

                if($(this).is('select')) {
                    if(!data.parent_ids) {
                        data.parent_ids = $(this).val();
                    } else {
                        data.parent_ids += "," + $(this).val();
                    }
                }

                if($(this).is('input[type="hidden"]') && $(this).val() != '') {
                    if(!data.parent_ids) {
                        data.parent_ids = $(this).val();
                    } else {
                        data.parent_ids += "," + $(this).val();
                    }
                }
            });

            if(!data.parent_ids) {
                alert("Please select entities for this report.");
                return false;
            }
        }

        if($('#network_currency').length > 0) {
            if($('#report_number').val() == '40') {
                var network_currency_value = $('#network_currency').val();
                if(network_currency_value == 0) {
                    alert("Please select Network for this report.");
                    return false;
                }
            }
        }

        // Verify that we have entities
        if($('.entity_id').length > 0) {

            $(".entity_id").each(function () {
                if($(this).is("input") && $(this).is(":checked")){
                    if(!data.entity) {
                        data.entity = $(this).val();
                    } else {
                        data.entity += "," + $(this).val();
                    }
                }

                if($(this).is('select')) {
                    if(!data.entity) {
                        data.entity = $(this).val();
                    } else {
                        data.entity += "," + $(this).val();
                    }
                }

                if($(this).is('input[type="hidden"]') && $(this).val() != '') {
                    if(!data.entity) {
                        data.entity = $(this).val();
                    } else {
                        data.entity += "," + $(this).val();
                    }
                }
            });

            if(!data.entity && $('#report_number').val() != '31' && $('#report_number').val() != '41' && $('#report_number').val() != '42') {
                alert("Please select entities for this report.");
                return false;
            }
        }

        if($('.filter1').length > 0){
            $('.filter1').each(function(){
                if($(this).is("input") && $(this).is(":checked")){
                    if(!data.filter1) {
                        data.filter1 = $(this).val();
                    } else {
                        data.filter1 += "," + $(this).val();
                    }
                }
            });
        }

        // Verify that we have a start date
        if($('input[name=start_date]').length > 0) {
            data.start_date = $('input[name=start_date]').val();
			var start		= new Date(data.start_date);
            if(!data.start_date) {
                alert("Please select a start date for this report period.");
                return false;
            }
        }

        // Verify that we have an end date
        if($('input[name=end_date]').length > 0) {
            data.end_date = $('input[name=end_date]').val();
			var end		= new Date(data.end_date);
            if(!data.end_date) {
                alert("Please select an end date for this report period.");
                return false;
            }
        }

		if(start > end) {
                alert("End date should be greater than Start date.");
                return false;
            }
        // Verify that we have an end date
        if($('input[name=range]').length > 0) {

            data.range = $('input[name=range]:checked').val();

            if(!data.range) {
                alert("Please select a range for this report.");
                return false;
            }
        }

        if($('input[name=monyearrange]').length > 0) {

            data.monyeardaterange = $('input[name=monyearrange]:checked').val();

            if(!data.monyeardaterange) {
                alert("Please select a range for this report.");
                return false;
            }
        }

        if($('input[name=trxtype]').length > 0) {

            data.trxtype = $('input[name=trxtype]:checked').val();

            if(!data.trxtype) {
                alert("Please select a Data Type for this report.");
                return false;
            }
        }

		// Verify that we have suppliers
        if($('.supplier_id').length > 0) {

            $(".supplier_id").each(function () {
                if($(this).is("input") && $(this).is(":checked")){
                    if(!data.supplier) {
                        data.supplier = $(this).val();
                    } else {
                        data.supplier += "," + $(this).val();
                    }
                }

                if($(this).is('select')) {
                    if(!data.supplier) {
                        data.supplier = $(this).val();
                    } else {
                        data.supplier += "," + $(this).val();
                    }
                }

                if($(this).is('input[type="hidden"]') && $(this).val() != '') {
                    if(!data.supplier) {
                        data.supplier = $(this).val();
                    } else {
                        data.supplier += "," + $(this).val();
                    }
                }
            });
			if(!data.supplier && $('#report_number').val() != '31') {
			alert("Please select suppliers for this report.");
			return false;
			}
        }

		if($('#currencies option:selected').val() > 0){
			 data.currencies = $('#currencies option:selected').val();
		}

		if($('#network_currency option:selected').val() > 0){
			 data.network_currency = $('#network_currency option:selected').val();
		}

        if($('#network_ids option:selected').val() > 0){
            data.network_ids = $('#network_ids option:selected').val();
        }

		// Verify that we have travels
        if($('.travel_id').length > 0) {

            $(".travel_id").each(function () {
                if($(this).is("input") && $(this).is(":checked")){
                    if(!data.travels) {
                        data.travels = $(this).val();
                    } else {
                        data.travels += "," + $(this).val();
                    }
                }

                if($(this).is('select')) {
                    if(!data.travels) {
                        data.travels = $(this).val();
                    } else {
                        data.travels += "," + $(this).val();
                    }
                }

                if($(this).is('input[type="hidden"]') && $(this).val() != '') {
                    if(!data.travels) {
                        data.travels = $(this).val();
                    } else {
                        data.travels += "," + $(this).val();
                    }
                }
            });
			if(!data.travels && $('#report_number').val() != '31') {
			alert("Please select travel for this report.");
			return false;
			}
        }

		// Verify that we have Redemption Categories
        if($('.redemptioncategories').length > 0) {

            $(".redemptioncategories").each(function () {
                if($(this).is("input") && $(this).is(":checked")){
                    if(!data.redemptioncategories) {
                        data.redemptioncategories = $(this).val();
                    } else {
                        data.redemptioncategories += "," + $(this).val();
                    }

                }

                if($(this).is('select')) {
                    if(!data.redemptioncategories) {
                        data.redemptioncategories = $(this).val();
                    } else {
                        data.redemptioncategories += "," + $(this).val();
                    }
                }

                if($(this).is('input[type="hidden"]') && $(this).val() != '') {
                    if(!data.redemptioncategories) {
                        data.redemptioncategories = $(this).val();
                    } else {
                        data.redemptioncategories += "," + $(this).val();
                    }
                }
            });
			if(!data.redemptioncategories) {
			alert("Please select Redemption Category for this report.");
			return false;
			}
        }

		if(!data.entity && !data.supplier && !data.travels && !data.filter1 && !data.network_ids && !data.member_id) {
			alert("Please select alt least one entity or suppliers for this report.");
			return false;
		}

        if($('input[name=user_id]').length > 0) {
            data.user_id = $('input[name=user_id]').val();
        }

        data.admin_id = $('#admin_id').val();
        window.location.href = $(this).closest('form').attr('action') + "?" + jQuery.param(data);
    });

	$(document).on('change', '#currencies', function() {
		var loaded =  $('#loaded').val();
		if( loaded > 0 ){
			$('.attribute-action-reload').click()
		}
	  });

    // Handle multiple checkbox sections
    $(document).on('change', 'input.parent', function () {
        var store_id = $(this).data('store_id') | 0;
        if ($(this).is(':checked')) {
            if (store_id > 0) {
                $(this).closest('div').find("[data-store_id='" + store_id + "']").prop('checked', true);
            } else {
                $(this).closest('div').find('input.child').prop('checked', true);
            }
        } else {
            if (store_id > 0) {
                $(this).closest('div').find("[data-store_id='" + store_id + "']").prop('checked', false);
            } else {
                $(this).closest('div').find('input.child').prop('checked', false);
            }
        }
        $(this).parents('.store-wrap').find('[value=' + $(this).val() + ']').prop('checked', $(this).is(':checked'));
    });

    // Handle multiple checkbox sections
    $(document).on('change', 'input.child', function () {
        var store_id = $(this).data('store_id') | 0;
        if ($(this).is(':checked')) {
            if (store_id > 0) {
                $(this).closest('div').find("input[value='" + store_id + "']").prop('checked', true);
            }
            else {
                $(this).closest('div').find('input.parent').prop('checked', true);
            }
        }
        $(this).parents('.store-wrap').find('[value=' + $(this).val() + ']').prop('checked', $(this).is(':checked'));
    });

    // Toggle the report charts
    $(document).on('click','a.report-type', function(e){
        e.preventDefault();

        $('#table').hide();
        $('#chart').hide();

        $('a.report-type').removeClass('active');

        $(this).addClass('active');

        $($(this).attr('href')).fadeIn();
    });

    // Global Delete
    $(document).on('click','.delete-ajax', function(e){
        e.preventDefault();

        _this = $(this);

        if(confirm("Are you sure you want to delete this item?")) {
            $.get(_this.attr('href'), function(data, status, xhr) {
                if(_this.data('target')) {
                    $(_this.data('target')).html(data);
                }
            });
        }
    });

    // Only allow numeric input
    $(document).on('keypress', 'input.enforceNumeric', function(e) {
        if(e.which == 13) {
            e.preventDefault();
        }

        if(!(e.which >= 48 && e.which <= 57) && !(e.which == 8)) {
            e.preventDefault();
        }
    });

    // Don't disable element, but don't allow input
    $(document).on('keypress', 'input.noInput', function(e) {
        e.preventDefault();
    });

    // Don't disable element, but don't allow input
    $(document).on('click', 'input.selectAll', function(e) {
        //e.preventDefault();
        $(this).closest('div').find('input.parent').prop('checked', true);
        $(this).closest('div').find('input.child').prop('checked', true);
        $(this).addClass('deselectAll').removeClass('selectAll');

    });

    $(document).on('click', 'input.deselectAll', function(e) {
        //e.preventDefault();
        $(this).closest('div').find('input.parent').prop('checked', false);
        $(this).closest('div').find('input.child').prop('checked', false);
        $(this).addClass('selectAll').removeClass('deselectAll');
    });

    $(document).on('change', 'input[data-select-all]', function(e) {
        if ($('input[data-select-item="' + $(this).data('select-all') + '"]:not(:checked)').length > 0) {
            $('input[data-select-item="' + $(this).data('select-all') + '"]').prop('checked', true);
        } else {
            $('input[data-select-item="' + $(this).data('select-all') + '"]').prop('checked', false);
        }
    });
    $(document).on('change', 'input[data-select-item]', function(e) {
        if ($('input[data-select-item="' + $(this).data('select-item') + '"]:not(:checked)').length > 0) {
            $('input[data-select-all="' + $(this).data('select-item') + '"]').prop('checked', false);
        } else {
            $('input[data-select-all="' + $(this).data('select-item') + '"]').prop('checked', true);
        }
    });
    $('input[data-select-all]').each(function() {
        if ($('input[data-select-item="' + $(this).data('select-all') + '"]:not(:checked)').length > 0) {
            $(this).prop('checked', false);
        } else {
            $(this).prop('checked', true);
        }
    });

    $(document).on('click', 'input.deselectAll', function(e) {
        //e.preventDefault();
        $(this).closest('div').find('input.parent').prop('checked', false);
        $(this).closest('div').find('input.child').prop('checked', false);
        $(this).addClass('selectAll').removeClass('deselectAll');
    });

    $(document).on('click', 'input.selectAllSup', function(e) {
        //e.preventDefault();
        $(this).closest('div').find('input.supplier_id').prop('checked', true);
        $(this).addClass('deselectAllSup').removeClass('selectAllSup');

    });

    $(document).on('click', 'input.deselectAllSup', function(e) {
        //e.preventDefault();
        $(this).closest('div').find('input.supplier_id').prop('checked', false);
        $(this).addClass('selectAllSup').removeClass('deselectAllSup');
    });

    $(document).on('click', 'input.selectAllRedCat', function(e) {
        //e.preventDefault();
        $(this).closest('div').find('input.redemptioncategories').prop('checked', true);
        $(this).addClass('deselectAllRedCat').removeClass('selectAllRedCat');

    });

    $(document).on('click', 'input.deselectAllRedCat', function(e) {
        //e.preventDefault();
        $(this).closest('div').find('input.redemptioncategories').prop('checked', false);
        $(this).addClass('selectAllRedCat').removeClass('deselectAllRedCat');
    });

    // Turn on the global member search autocomplete function
    if($('#members-autocomplete').length > 0) {

        $("#members-autocomplete").autocomplete({
          source: "/dashboard/reports/members",
          minLength: 2,
          select: function( event, ui ) {
            $('input[name="entity_id"]').val(ui.item.id);
          }
        });
    }
    // Turn on the global member search autocomplete function
    if($('#members-autocomplete-new').length > 0) {

        $("#members-autocomplete-new").autocomplete({
            source: "/dashboard/reports/members",
            minLength: 2,
            select: function( event, ui ) {
                $('input[name="member_id"]').val(ui.item.id);
            }
        });

        $('#members-autocomplete-new').on('change',  function(){
            if ($('#members-autocomplete-new').val() == '') {
                $('input[name="member_id"]').val('');
            }
        });
    }
    if($('#members-autocomplete-name').length > 0) {

        $("#members-autocomplete-name").autocomplete({
            source: "/dashboard/reports/members?search-type=name",
            minLength: 2,
            select: function (event, ui) {
                $('input[name="member_id"]').val(ui.item.id);
                $('#members-autocomplete-reference').val(ui.item.reference);
                $('.member-balance .member-balance-value').text(ui.item.balance).parents('.member-balance').removeClass('hidden');
            }
        });

        $('#members-autocomplete-name').on('change', function () {
            if ($('#members-autocomplete-name').val() == '') {
                $('input[name="member_id"]').val('');
                $('#members-autocomplete-reference').val('');
                $('.member-balance').addClass('hidden');
            }
        });
    }
    if ($('#members-autocomplete-reference').length > 0) {

        $("#members-autocomplete-reference").autocomplete({
            source: "/dashboard/reports/references",
            minLength: 2,
            select: function (event, ui) {
                $('input[name="member_id"]').val(ui.item.id);
                $('#members-autocomplete-name').val(ui.item.name);
                $('.member-balance .member-balance-value').text(ui.item.balance).parents('.member-balance').removeClass('hidden');
            }
        });

        $('#members-autocomplete-reference').on('change', function () {
            if ($('#members-autocomplete-reference').val() == '') {
                $('input[name="member_id"]').val('');
                $('#members-autocomplete-name').val('');
                $('.member-balance').addClass('hidden');
            }
        });
    }

    function matchStart (term, text) {
        if (text.toUpperCase().indexOf(term.toUpperCase()) == 0) {
            return true;
        }
        return false;
    }

    if ($('.select2').length > 0) {
        $('.select2').select2({
            tags: true
        });
    }

    $('select[data-filter-catalogue]').each(function () {
        var select = $(this);
        var input = $('input[type=hidden][data-filter-catalogue="' + select.data('filter-catalogue') + '"]');

        $.fn.select2.amd.require(['select2/compat/matcher'], function (oldMatcher) {
            select.select2({
                placeholder: select.data('filter-catalogue-title'),
                minimumResultsForSearch: -1,
                matcher: oldMatcher(matchStart),
                allowClear: true
            })
        });

        select.val(input.val().split(','));
        select.trigger('change');
    });

    if ($('#partner_list_select').length) {
        $.fn.select2.amd.require(['select2/compat/matcher'], function (oldMatcher) {
            $("#partner_list_select").select2({
                placeholder: "Select a Partner",
                minimumResultsForSearch: -1,
                matcher: oldMatcher(matchStart),
                allowClear: true
            })
        });

        $("#partner_list_select").val($("#filterPartners").val().split(','));
        $("#partner_list_select").trigger('change');
    }
    if ($('#parent_list_select').length) {
        $.fn.select2.amd.require(['select2/compat/matcher'], function (oldMatcher) {
            $("#parent_list_select").select2({
                placeholder: "Select a Partner",
                minimumResultsForSearch: -1,
                matcher: oldMatcher(matchStart),
                allowClear: true
            })
        });

        $("#parent_list_select").val($("#filterPartners").val().split(','));
        $("#parent_list_select").trigger('change');
    }
    if ($('#network_list_select').length) {
        $.fn.select2.amd.require(['select2/compat/matcher'], function (oldMatcher) {
            $("#network_list_select").select2({
                placeholder: "Select a Network",
                minimumResultsForSearch: -1,
                matcher: oldMatcher(matchStart),
                allowClear: true
            })
        });

        $("#network_list_select").val($("#filterNetworks").val().split(','));
        $("#network_list_select").trigger('change');
    }
    if ($('#category_list_select').length) {
        $.fn.select2.amd.require(['select2/compat/matcher'], function (oldMatcher) {
            $("#category_list_select").select2({
                placeholder: "Select a Category",
                minimumResultsForSearch: -1,
                matcher: oldMatcher(matchStart),
                allowClear: true
            })
        });

        $("#category_list_select").val($("#filterCategories").val().split(','));
        $("#category_list_select").trigger('change');
    }

    $('#filter-catalogue').on('submit', function(e) {
        e.preventDefault();

        var url = '';
        if ($('#filter_route').val()) {
            url = $('#filter_route').val() + '?';
        }

        $('select[data-filter-catalogue]').each(function () {
            if ($(this).val()) {
                url += '&' + $(this).data('filter-catalogue') + '=' + $(this).val().join(',');
            }
        });
        if ($('#partner_list_select').val()) {
            url += '&partner_list=' + $('#partner_list_select').val().join(',');
        }
        if ($('#parent_list_select').val()) {
            url += '&parent_list=' + $('#parent_list_select').val().join(',');
        }
        if ($('#network_list_select').val()) {
            url += '&network_list=' + $('#network_list_select').val();
        }
        if ($('#category_list_select').val()) {
            url += '&category_list=' + $('#category_list_select').val();
        }
        if ($('#search_q').val()) {
            url += '&q=' + $('#search_q').val();
        }

        window.location = url;
    });

	// banners Logged In/
		$('#logged_in').on('change',  function(){
			var logged_in	= $('#logged_in').is(":checked");
			var logged_out	= $('#logged_out').is(":checked");

			if(!logged_in && !logged_out){
				$('#logged_out').prop('checked', true);
				alert('At least one option should be selected.');
			}

			if(logged_in){
				$('#segments_dialog').click();
			}
});

		$('#logged_out').on('change',  function(){
			var logged_in	= $('#logged_in').is(":checked");
			var logged_out	= $('#logged_out').is(":checked");

			if(!logged_in && !logged_out){
				$('#logged_in').prop('checked', true);
				alert('At least one option should be selected.');
			}
        });

        // Partner color picker
        if($('#partnerForm #color').length){
            $("#partnerForm #color").spectrum({
                allowEmpty:true,
                color: "#000",
                showInput: true,
                containerClassName: "full-spectrum",
                showInitial: true,
                showPalette: true,
                showSelectionPalette: true,
                showAlpha: true,
                maxPaletteSize: 10,
                preferredFormat: "hex",
                localStorageKey: "spectrum.blu",
                move: function (color) {

                },
                show: function () {

                },
                beforeShow: function () {

                },
                hide: function (color) {

                },

                palette: [
                    ["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)", "rgb(204, 204, 204)", "rgb(217, 217, 217)", "rgb(255, 255, 255)"],
                    ["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)",
                    "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"],
                    ["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)",
                    "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)",
                    "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)",
                    "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)",
                    "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)",
                    "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)",
                    "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)",
                    "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)",
                    "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)",
                    "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
                ]
            });

            $("#partnerForm #secondary_color").spectrum({
                allowEmpty:true,
                color: "#000",
                showInput: true,
                containerClassName: "full-spectrum",
                showInitial: true,
                showPalette: true,
                showSelectionPalette: true,
                showAlpha: true,
                maxPaletteSize: 10,
                preferredFormat: "hex",
                localStorageKey: "spectrum.blu",
                move: function (color) {

                },
                show: function () {

                },
                beforeShow: function () {

                },
                hide: function (color) {

                },

                palette: [
                    ["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)", "rgb(204, 204, 204)", "rgb(217, 217, 217)", "rgb(255, 255, 255)"],
                    ["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)",
                    "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"],
                    ["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)",
                    "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)",
                    "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)",
                    "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)",
                    "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)",
                    "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)",
                    "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)",
                    "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)",
                    "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)",
                    "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
                ]
            });
        }

        //Input number validation
        $('#affiliate_contact_ext').addClass('numeric-field number-field');
        $('#reward_points').addClass('numeric-field number-field');
        $('#num_children').addClass('numeric-field number-field');
        $('#cashback_validity').addClass('numeric-field number-field');
        $('#reward_pts').addClass('numeric-field number-field');
        $('#number_of_items').addClass('numeric-field number-field');
        $('#qty').addClass('numeric-field number-field');
        $('#price_in_points').addClass('numeric-field number-field');
        $('#timer').addClass('numeric-field number-field');
        $('#max_points_to_grant').addClass('numeric-field number-field');
        $('#max_points_per_user').addClass('numeric-field number-field');

        $('#value_in_currency').addClass('numeric-field float-field');
        $('#network_point').addClass('numeric-field float-field');
        $('#value_in_currency').addClass('numeric-field float-field');
        $('#original_reward').addClass('numeric-field float-field');
        $('#reward_usd').addClass('numeric-field float-field');
        $('#price').addClass('numeric-field float-field');
        $('#price_in_usd').addClass('numeric-field float-field');
        $('#retail_price_in_usd').addClass('numeric-field float-field');
        $('#sales_tax_in_usd').addClass('numeric-field float-field');
        $('#original_price').addClass('numeric-field float-field');
        $('#original_retail_price').addClass('numeric-field float-field');
        $('#original_sales_tax').addClass('numeric-field float-field');
        $('#weight').addClass('numeric-field float-field');
        $('#volume').addClass('numeric-field float-field');
        $('#delivery_charges').addClass('numeric-field float-field');
        $('#latest_rate').addClass('numeric-field float-field');

        $('#percentage').addClass('numeric-field float-field');

        $('.number-field').keypress(function(){
            return isNumber();
        });

        $('.float-field').keypress(function(){
            return isFloat();
        });
});
