<?php

return array(
        'admin_email'           => env('BLU_INFO_EMAIL', 'info@bluloyalty.com'),
        'email_from'           =>  'info@blupoints.com',
        'stock_level_warning'   => 3,
        'enable_debugging'      => true,
        'points_rate'           => 0.01,
        'transfer_cost'         => 5,
        'mobile_version'        => 0.7,
        'website_URL'           => 'http://54.191.130.14',
        'cc_number'             => '4074482183077380',
        'cc_cvv'                => '916',
        'cc_exp_month'          => '08',
        'cc_exp_year'           => '17',
        'cc_type'               => 'VI',
        'cc_name'               => 'Antoine Gougassian Ste. BLU Solutions Limited'
);

