<?php

namespace Tests\Unit;

use App\Admin;
use App\Brand;
use App\Language;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BrandTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    protected $user;

    public function setUp()
    {
        parent::setUp();

        $user = Admin::where('email', 'info@bluloyalty.com')->first();
        $this->user = $user;
        $this->actingAs($user)
             ->session([
                 'user_roles' => [
                     'BLU Admin',
                 ],
                 'user_permissions' => [
                     'create_brands',
                     'view_brands',
                     'edit_brands',
                     'delete_brands',
                 ],
             ]);
    }

    /** @test */
    public function can_create_new_brand_page_with_permissions()
    {
        $response = $this->get('/dashboard/brands/new');

        $response->assertStatus(200)
                 ->assertViewIs('brand.new');
    }

    /** @test */
    public function can_not_create_new_brand_page_without_permissions()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $response = $this->get('/dashboard/brands/new');

        $response->assertStatus(302)
                 ->assertRedirect()
                 ->assertSeeText('/dashboard');
    }

    /** @test */
    public function can_open_edit_brand_with_permissions()
    {
        $brand = factory(\App\Brand::class)->create();

        $response = $this->get('/dashboard/brands/view/' . base64_encode($brand->id));

        $response->assertStatus(200)
                 ->assertViewIs('brand.view');
    }

    /** @test */
    public function can_not_open_brand_page_without_permissions()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $brand = factory(\App\Brand::class)->create();

        $response = $this->get('/dashboard/brands/view/' . base64_encode($brand->id));

        $response->assertStatus(302)
                 ->assertRedirect()
                 ->assertSeeText('/dashboard');
    }

    /** @test */
    public function can_not_open_brand_page_which_is_not_exists()
    {
        $brand = factory(\App\Brand::class)->create();

        $response = $this->get('/dashboard/brands/view/' . base64_encode($brand->id + 10));

        $response->assertStatus(404);
    }

    /** @test */
    public function can_update_brand_with_permission()
    {
        $brand = factory(\App\Brand::class)->create();

        $data = [
            'name' => uniqid(),
        ];

        $response = $this->post('/dashboard/brands/update/' . base64_encode($brand->id), $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/brands');

        $this->assertDatabaseHas('brand', [
            'id' => $brand->id,
            'name' => $data['name'],
        ]);
    }

    /** @test */
    public function can_not_update_brand_without_permission()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $brand = factory(\App\Brand::class)->create();

        $data = [
            'name' => uniqid(),
        ];

        $response = $this->post('/dashboard/brands/update/' . base64_encode($brand->id), $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard');

        $this->assertDatabaseMissing('brand', [
            'id' => $brand->id,
            'name' => $data['name'],
        ]);
    }

    /** @test */
    public function can_not_update_brand_which_is_not_exists()
    {
        $brand = factory(\App\Brand::class)->create();

        $data = [
            'name' => uniqid(),
        ];

        $response = $this->post('/dashboard/brands/update/' . base64_encode($brand->id + 10), $data);

        $response->assertStatus(404);

        $this->assertDatabaseMissing('brand', [
            'id' => $brand->id,
            'name' => $data['name'],
        ]);
    }

    /** @test */
    public function can_not_update_brand_without_required_fields()
    {
        $brand = factory(\App\Brand::class)->create();

        $data = [];
        $response = $this->post('/dashboard/brands/update/' . base64_encode($brand->id), $data);

        $response->assertStatus(200)
                 ->assertJson([
                     'failed' => true,
                     'errors' => "Please ensure that you provide a valid brand name"
                 ]);
    }

    /** @test */
    public function can_delete_brand_with_permission()
    {
        $brand = factory(\App\Brand::class)->create();

        $response = $this->get('/dashboard/brands/delete/' . base64_encode($brand->id));

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/brands');

        $this->assertDatabaseMissing('brand', [
            'id' => $brand->id,
        ]);
    }

    /** @test */
    public function can_not_delete_brand_without_permission()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $brand = factory(\App\Brand::class)->create();

        $response = $this->get('/dashboard/brands/delete/' . base64_encode($brand->id));

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard');

        $this->assertDatabaseHas('brand', [
            'id' => $brand->id,
        ]);
    }

    /** @test */
    public function can_not_delete_brand_which_is_not_exists()
    {
        $brand = factory(\App\Brand::class)->create();

        $response = $this->get('/dashboard/brands/delete/' . base64_encode($brand->id + 10));

        $response->assertStatus(404);

        $this->assertDatabaseHas('brand', [
            'id' => $brand->id,
        ]);
    }

    /** @test */
    public function can_open_brands_page()
    {
        factory(\App\Brand::class)->create();

        $response = $this->get('/dashboard/brands');

        $response->assertStatus(200)
                 ->assertViewIs('brand.list')
                 ->assertViewHas('brands');
    }

    /** @test */
    public function can_not_open_brands_page_without_permission()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        factory(\App\Brand::class)->create();

        $response = $this->get('/dashboard/brands');

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard');
    }

    /** @test */
    public function can_filter_brands_by_search_query()
    {
        $brand = factory(\App\Brand::class)->create();

        $response = $this->get('/dashboard/brands?q=' . uniqid());

        $response->assertStatus(200)
                 ->assertViewIs('brand.list')
                 ->assertDontSee($brand->name);

        $response = $this->get('/dashboard/brands?q=' . $brand->name);

        $response->assertStatus(200)
                 ->assertViewIs('brand.list')
                 ->assertSee($brand->name);
    }

    /** @test */
    public function can_add_brand_translation()
    {
        $brand = factory(\App\Brand::class)->create();

        $response = $this->get('/dashboard/brandtranslation/new/' . $brand->id);

        $response->assertStatus(200)
                 ->assertViewIs('brandtranslation.form');
    }

    /** @test */
    public function can_save_brand_translation()
    {
        $brand = factory(\App\Brand::class)->create();
        $brandTranslation = factory(\App\Brandtranslation::class)->create([
            'brand_id' => $brand->id,
        ]);
        $lang = factory(Language::class)->create();

        $data = [
            'brandtranslation_lang' => $lang->id,
            'brandtranslation_name' => uniqid(),
        ];

        $response = $this->post('/dashboard/brandtranslation/update/' . base64_encode($brandTranslation->id), $data);

        $response->assertStatus(200)
                 ->assertJson([
                     'failed' => false,
                     'view_url' => url('dashboard/brandtranslation/list/' . base64_encode($brandTranslation->brand_id)),
                 ]);

        $this->assertDatabaseHas('brand_translation', [
            'id' => $brandTranslation->id,
            'lang_id' => $data['brandtranslation_lang'],
            'name' => $data['brandtranslation_name'],
        ]);
    }

    /** @test */
    public function can_not_save_brand_translation_without_required_fields()
    {
        $brand = factory(\App\Brand::class)->create();
        $brandTranslation = factory(\App\Brandtranslation::class)->create([
            'brand_id' => $brand->id,
        ]);

        $data = [];

        $response = $this->post('/dashboard/brandtranslation/update/' . base64_encode($brandTranslation->id), $data);

        $response->assertStatus(200)
                 ->assertJsonFragment([
                     'failed' => true,
                 ]);
    }

    /** @test */
    public function can_get_brand_translation_list()
    {
        $brand = factory(\App\Brand::class)->create();
        $brandTranslation = factory(\App\Brandtranslation::class)->create([
            'brand_id' => $brand->id,
        ]);

        $response = $this->get('/dashboard/brandtranslation/list/' . base64_encode($brand->id));

        $response->assertStatus(200)
                 ->assertViewIs('brandtranslation.list')
                 ->assertSee($brandTranslation->name);
    }

    /** @test */
    public function can_delete_brand_translation()
    {
        $brand = factory(\App\Brand::class)->create();
        $brandTranslation = factory(\App\Brandtranslation::class)->create([
            'brand_id' => $brand->id,
        ]);

        $response = $this->get('/dashboard/brandtranslation/delete/' . base64_encode($brandTranslation->id));

        $response->assertStatus(200)
                 ->assertJson([
                     'failed' => false,
                 ]);

        $this->assertDatabaseMissing('brand_translation', [
            'id' => $brandTranslation->id,
            'name' => $brandTranslation->name,
        ]);
    }

    /** @test */
    public function can_view_pagination_page()
    {
        $perPage = 15;
        $pageCount = ceil(Brand::where('draft', 0)->count() / $perPage);

        if ($pageCount > 1) {
            $response = $this->get('/dashboard/brands?page=' . $pageCount);

            $response->assertStatus(200)
                     ->assertViewIs('brand.list')
                     ->assertSee('brands?page=1')
                     ->assertDontSee('No Brands Found');
        }
    }

    /** @test */
    public function can_not_view_pagination_page_if_one_page()
    {
        $perPage = 15;
        $page = ceil(Brand::where('draft', 0)->count() / $perPage) + 1;

        $response = $this->get('/dashboard/brands?page=' . $page);

        $response->assertStatus(200)
                 ->assertViewIs('brand.list')
                 ->assertDontSee('brands?page=' . $page)
                 ->assertSee('No Brands Found');
    }
}
