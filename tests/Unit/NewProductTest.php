<?php

namespace Tests\Feature;

use App\Admin;
use App\ProductPartnerRedemption;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class NewProductTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    protected $user;

    public function setUp()
    {
        parent::setUp();

        $user = Admin::where('email', 'info@bluloyalty.com')->first();
        $this->user = $user;
        $this->actingAs($user)
             ->session([
                 'user_roles' => [
                     'BLU Admin',
                 ],
                 'user_permissions' => [
                     'view_products',
                     'edit_products',
                     'create_products',
                     'delete_products'
                 ],
             ]);
    }

    /** @test */
    public function can_view_product_list_page_with_permission()
    {
        factory(\App\Product::class)->create();

        $response = $this->get('/dashboard/productpricing');

        $response->assertStatus(200)
                 ->assertViewIs('productpricing.list')
                 ->assertViewHas('products');
    }

    /** @test */
    public function can_not_view_product_list_page_without_permission()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        factory(\App\Product::class)->create();

        $response = $this->get('/dashboard/productpricing');

        $response->assertStatus(302)
                 ->assertRedirect()
                 ->assertRedirect('/dashboard');
    }

    /** @test */
    public function can_filter_product_by_search_query()
    {
        $product = factory(\App\Product::class)->create();

        $response = $this->get('/dashboard/productpricing?q=' . uniqid());

        $response->assertStatus(200)
                 ->assertViewIs('productpricing.list')
                 ->assertDontSee($product->name);

        $response = $this->get('/dashboard/productpricing?q=' . $product->name);

        $response->assertStatus(200)
                 ->assertViewIs('productpricing.list')
                 ->assertSee($product->name);
    }

    /** @test */
    public function can_filter_product_by_partner()
    {
        $product  = factory(\App\Product::class)->create();
        $partner1 = factory(\App\Partner::class)->create();
        $partner2 = factory(\App\Partner::class)->create();
        $partner3 = factory(\App\Partner::class)->create();

        $response = $this->get('/dashboard/productpricing?partner_list[]=' . $partner1->id . '&partner_list[]=' . $partner2->id);

        $response->assertStatus(200)
                 ->assertViewIs('productpricing.list')
                 ->assertDontSee($product->name);

        $product->partners()->save($partner3);

        $response = $this->get('/dashboard/productpricing?partner_list[]=' . $partner3->id);

        $response->assertStatus(200)
                 ->assertViewIs('productpricing.list')
                 ->assertSee($product->name);
    }

    /** @test */
    public function can_filter_product_by_supplier()
    {
        $product = factory(\App\Product::class)->create();
        $supplier1 = factory(\App\Supplier::class)->create();
        $supplier2 = factory(\App\Supplier::class)->create();
        $supplier3 = factory(\App\Supplier::class)->create();

        $response = $this->get('/dashboard/productpricing?supplier_list[]=' . $supplier1->id . '&partner_list[]=' . $supplier2->id);

        $response->assertStatus(200)
                 ->assertViewIs('productpricing.list')
                 ->assertDontSee($product->name);

        $product->supplier_id = $supplier3->id;
        $product->save();

        $response = $this->get('/dashboard/productpricing?supplier_list[]=' . $product->supplier_id);

        $response->assertStatus(200)
                 ->assertViewIs('productpricing.list')
                 ->assertSee($product->name);
    }

    /** @test */
    public function can_filter_product_by_country()
    {
        $product = factory(\App\Product::class)->create();
        $country1 = factory(\App\Country::class)->create();
        $country2 = factory(\App\Country::class)->create();
        $country3 = factory(\App\Country::class)->create();

        $response = $this->get('/dashboard/productpricing?country_list[]=' . $country1->id . '&country_list[]=' . $country2->id);

        $response->assertStatus(200)
                 ->assertViewIs('productpricing.list')
                 ->assertDontSee($product->name);

        $product->countries()->save($country3);

        $response = $this->get('/dashboard/productpricing?country_list[]=' . $country3->id);

        $response->assertStatus(200)
                 ->assertViewIs('productpricing.list')
                 ->assertSee($product->name);
    }

    /** @test */
    public function can_filter_product_by_channel()
    {
        $product = factory(\App\Product::class)->create();
        $channel1 = factory(\App\ProductChannel::class)->create();
        $channel2 = factory(\App\ProductChannel::class)->create();
        $channel3 = factory(\App\ProductChannel::class)->create([
            'product_id' => $product->id
        ]);

        $response = $this->get('/dashboard/productpricing?displaychannel_list[]=' . $channel1->id . '&displaychannel_list[]=' . $channel2->id);

        $response->assertStatus(200)
                 ->assertViewIs('productpricing.list')
                 ->assertDontSee($product->name);

        $response = $this->get('/dashboard/productpricing?displaychannel_list[]=' . $channel3->channel_id);

        $response->assertStatus(200)
                 ->assertViewIs('productpricing.list')
                 ->assertSee($product->name);
    }

    /** @test */
    public function can_filter_product_by_brand()
    {
        $product = factory(\App\Product::class)->create();
        $brand1 = factory(\App\Brand::class)->create();
        $brand2 = factory(\App\Brand::class)->create();
        $brand3 = factory(\App\Brand::class)->create();

        $response = $this->get('/dashboard/productpricing?brand_list[]=' . $brand1->id . '&brand_list[]=' . $brand2->id);

        $response->assertStatus(200)
                 ->assertViewIs('productpricing.list')
                 ->assertDontSee($product->name);

        $product->brand_id = $brand3->id;
        $product->save();

        $response = $this->get('/dashboard/productpricing?brand_list[]=' . $product->brand_id);

        $response->assertStatus(200)
                 ->assertViewIs('productpricing.list')
                 ->assertSee($product->name);
    }

    /** @test */
    public function can_filter_product_by_category()
    {

        $category1 = factory(\App\Category::class)->create();
        $category2 = factory(\App\Category::class)->create();
        $category3 = factory(\App\Category::class)->create();
        $product   = factory(\App\Product::class)->create(['category_id' => $category3->id]);

        $response = $this->get('/dashboard/productpricing?cat_list[]=' . $category1->id . '&cat_list[]=' . $category2->id);

        $response->assertStatus(200)
                 ->assertViewIs('productpricing.list')
                 ->assertDontSee($product->name);

        $response = $this->get('/dashboard/productpricing?cat_list[]=' . $category3->id);

        $response->assertStatus(200)
                 ->assertViewIs('productpricing.list')
                 ->assertSee($product->name);
    }

    /** @test */
    public function can_view_pagination_page()
    {
        $partner = Auth::User()->getTopLevelPartner()->id;
        $product = \App\Product::select('product.*');
        $products = $product->whereHas('Partners', function($q) use($partner) {
            $q->where('partner_id', $partner);
        });

        $perPage = 15;
        $pageCount = ceil($products->count() / $perPage);

        if ($pageCount > 1) {
            $response = $this->get('/dashboard/productpricing?page=' . $pageCount);

            $response->assertStatus(200)
                     ->assertViewIs('productpricing.list')
                     ->assertSee('productpricing?page=1')
                     ->assertDontSee('No Items Found');
        }
    }

    /** @test */
    public function can_not_view_pagination_page_which_does_not_exist()
    {
        $partner = Auth::User()->getTopLevelPartner()->id;
        $product = \App\Product::select('product.*');
        $products = $product->whereHas('Partners', function($q) use($partner) {
            $q->where('partner_id', $partner);
        });

        $perPage = 15;
        $page = ceil($products->count() / $perPage) + 1000;

        $response = $this->get('/dashboard/productpricing?page=' . $page);

        $response->assertStatus(200)
                 ->assertViewIs('productpricing.list')
                 ->assertDontSee('productpricing?page=' . $page)
                 ->assertSeeText('No Items Found');
    }

    /** @test */
    public function can_create_new_product_with_permission()
    {
        $response = $this->get('/dashboard/productpricing/new');

        $response->assertStatus(302)
                 ->assertRedirect()
                 ->assertSeeText('/dashboard/productpricing/view/');
    }

    /** @test */
    public function can_not_create_new_product_without_permission()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $response = $this->get('/dashboard/productpricing/new');

        $response->assertStatus(302)
                 ->assertRedirect()
                 ->assertSeeText('/dashboard');
    }

    /** @test */
    public function can_view_edit_page_with_permission()
    {
        $product = factory(\App\Product::class)->create();

        $response = $this->get('/dashboard/productpricing/view/' . base64_encode($product->id));

        $response->assertStatus(200)
                 ->assertViewIs('productpricing.view')
                 ->assertSee($product->name);
    }

    /** @test */
    public function can_not_view_edit_page_without_permission()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $product = factory(\App\Product::class)->create();

        $response = $this->get('/dashboard/productpricing/view/' . base64_encode($product->id));

        $response->assertStatus(302)
                 ->assertRedirect()
                 ->assertSeeText('/dashboard');
    }

    /** @test */
    public function can_not_view_edit_page_if_partner_incorrect()
    {
        $product = factory(\App\Product::class)->create();

        $response = $this->get('/dashboard/productpricing/view/' . base64_encode($product->id + 10));

        $response->assertStatus(404)
                 ->assertDontSee($product->name);
    }

    /** @test */
    public function can_delete_product_with_permission()
    {
        $product = factory(\App\Product::class)->create();
        $response = $this->get('/dashboard/productpricing/delete/' . base64_encode($product->id));

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/productpricing');

        $this->assertDatabaseMissing('product', [
            'id' => $product->id,
        ]);
    }

    /** @test */
    public function can_not_delete_coupon_without_permission()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $product = factory(\App\Product::class)->create();
        $response = $this->get('/dashboard/productpricing/delete/' . base64_encode($product->id));

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard');

        $this->assertDatabaseHas('product', [
            'id' => $product->id,
        ]);
    }

    /** @test */
    public function can_not_delete_coupon_with_incorrect_id()
    {
        $product = factory(\App\Product::class)->create();
        $response = $this->get('/dashboard/productpricing/delete/' . base64_encode($product->id + 10));

        $response->assertStatus(404);

        $this->assertDatabaseHas('product', [
            'id' => $product->id,
        ]);
    }

    /** @test */
    public function can_update_product_with_permission()
    {
        $product = factory(\App\Product::class)->create();
        $currency = factory(\App\Currency::class)->create();
        factory(\App\CurrencyPricing::class)->create(['currency_id' => $currency->id]);

        $stub = __DIR__ . '\\..\\stubs\\test.png';
        $name = str_random(8) . '.png';
        $path = sys_get_temp_dir() . '\\' . $name;

        copy($stub, $path);

        $file = new UploadedFile($path, $name, filesize($path), 'image/png', null, true);

        $data = [
            'name'                  => uniqid(),
            'brand_id'              => factory(\App\Brand::class)->create()->id,
            'category_id'           => factory(\App\Category::class)->create()->id,
            'draft'                 => 0,
            'start_date'            => Carbon::today()->subDays(10),
            'end_date'              => Carbon::today()->addDays(10),
            'qty'                   => random_int(1, 10),
            'currency_id'           => $currency->id,
            'original_price'        => random_int(1, 10),
            'original_retail_price' => random_int(1, 10),
            'original_sales_tax'    => random_int(1, 10),
            'weight'                => random_int(1, 10),
            'volume'                => random_int(1, 10),
            'delivery_charges'      => random_int(1, 10),
            'status'                => 'active'
        ];

        $response = $this->call('POST', '/dashboard/productpricing/update/' . base64_encode($product->id), $data, [], ['cover_image' => $file], ['Accept' => 'application/json']);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/productpricing/view/' . base64_encode($product->id));

        $this->assertDatabaseHas('product', [
            'id' => $product->id,
            'name' => $data['name']
        ]);

        $this->assertFileExists(public_path(\App\Http\Controllers\MediaController::getImageFromId($product->cover_image)));

        @unlink(public_path(\App\Http\Controllers\MediaController::getImageFromId($product->cover_image)));
    }

    /** @test */
    public function can_not_update_without_permission()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $product = factory(\App\Product::class)->create();
        $currency = factory(\App\Currency::class)->create();
        factory(\App\CurrencyPricing::class)->create(['currency_id' => $currency->id]);

        $data = [
            'name'                  => uniqid(),
            'brand_id'              => factory(\App\Brand::class)->create()->id,
            'category_id'           => factory(\App\Category::class)->create()->id,
            'draft'                 => 0,
            'end_date'              => Carbon::today()->addDays(10),
            'qty'                   => random_int(1, 10),
            'currency_id'           => $currency->id,
            'original_price'        => random_int(1, 10),
            'original_retail_price' => random_int(1, 10),
            'original_sales_tax'    => random_int(1, 10),
            'weight'                => random_int(1, 10),
            'volume'                => random_int(1, 10),
            'delivery_charges'      => random_int(1, 10),
            'status'                => 'active'
        ];

        $response = $this->call('POST', '/dashboard/productpricing/update/' . base64_encode($product->id), $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard');

        $this->assertDatabaseMissing('product', [
            'id' => $product->id,
            'name' => $data['name']
        ]);
    }

    /** @test */
    public function can_not_update_product_which_is_not_exists()
    {
        $product = factory(\App\Product::class)->create();
        $currency = factory(\App\Currency::class)->create();
        factory(\App\CurrencyPricing::class)->create(['currency_id' => $currency->id]);

        $data = [
            'name' => uniqid(),
        ];

        $response = $this->call('POST', '/dashboard/productpricing/update/' . base64_encode($product->id + 10), $data);

        $response->assertStatus(404);

        $this->assertDatabaseMissing('product', [
            'id' => $product->id,
            'name' => $data['name']
        ]);
    }

    /** @test */
    public function can_validate_errors()
    {
        $product = factory(\App\Product::class)->create();
        $currency = factory(\App\Currency::class)->create();
        factory(\App\CurrencyPricing::class)->create(['currency_id' => $currency->id]);

        $data = [
            'name' => '',
            'brand_id' => '',
            'category_id' => '',
            'start_date' => random_int(1, 10),
            'end_date' => random_int(1, 10),
            'qty' => 'i',
            'original_price' => 'i',
            'original_retail_price' => 'i',
            'original_sales_tax' => 'i',
            'weight' => 'i',
            'volume' => 'i',
            'delivery_charges' => 'i',
        ];

        $response = $this->post('/dashboard/productpricing/update/' . base64_encode($product->id), $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/productpricing/view/' . base64_encode($product->id))
                 ->assertSessionHasErrors('name')
                 ->assertSessionHasErrors('brand_id')
                 ->assertSessionHasErrors('category_id')
                 ->assertSessionHasErrors('start_date')
                 ->assertSessionHasErrors('end_date')
                 ->assertSessionHasErrors('qty')
                 ->assertSessionHasErrors('original_price')
                 ->assertSessionHasErrors('original_retail_price')
                 ->assertSessionHasErrors('original_sales_tax')
                 ->assertSessionHasErrors('weight')
                 ->assertSessionHasErrors('volume')
                 ->assertSessionHasErrors('delivery_charges');
    }

    /** @test */
    public function can_attach_partner()
    {
        $product = factory(\App\Product::class)->create();
        $partner = factory(\App\Partner::class)->create();

        $response = $this->post('/dashboard/productpricing/link_partner/' . base64_encode($product->id), ['partner_id' => $partner->id]);

        $response->assertStatus(200)
                 ->assertViewIs('productpricing.list.partnerList')
                 ->assertSeeText($partner->name);

        $this->assertDatabaseHas('partner_product', [
            'partner_id' => $partner->id,
            'product_id' => $product->id,
        ]);
    }

    /** @test */
    public function can_not_attach_partner_if_product_or_partner_do_not_exist()
    {
        $product = factory(\App\Product::class)->create();
        $partner = factory(\App\Partner::class)->create();

        $response = $this->post('/dashboard/productpricing/link_partner/' . base64_encode($product->id + 10), ['partner_id' => $partner->id]);

        $response->assertStatus(404);

        $this->assertDatabaseMissing('partner_product', [
            'partner_id' => $partner->id,
            'product_id' => $product->id,
        ]);

        $response = $this->post('/dashboard/productpricing/link_partner/' . base64_encode($product->id), ['partner_id' => $partner->id + 10]);

        $response->assertStatus(404);

        $this->assertDatabaseMissing('partner_product', [
            'partner_id' => $partner->id,
            'product_id' => $product->id,
        ]);
    }

    /** @test */
    public function can_not_attach_duplicate_partner()
    {
        $product = factory(\App\Product::class)->create();
        $partner = factory(\App\Partner::class)->create();

        $response = $this->post('/dashboard/productpricing/link_partner/' . base64_encode($product->id), ['partner_id' => $partner->id]);

        $response->assertStatus(200)
                 ->assertViewIs('productpricing.list.partnerList')
                 ->assertSeeText($partner->name);

        $this->assertDatabaseHas('partner_product', [
            'partner_id' => $partner->id,
            'product_id' => $product->id,
        ]);

        $response = $this->post('/dashboard/productpricing/link_partner/' . base64_encode($product->id), ['partner_id' => $partner->id]);

        $response->assertStatus(200)
                 ->assertViewIs('productpricing.list.partnerList')
                 ->assertSeeText($partner->name);

        $this->assertEquals(1, $product->partners()->count());
    }

    /** @test */
    public function can_detach_partner()
    {
        $product = factory(\App\Product::class)->create();
        $partner = factory(\App\Partner::class)->create();

        $response = $this->post('/dashboard/productpricing/link_partner/' . base64_encode($product->id), ['partner_id' => $partner->id]);

        $response->assertStatus(200)
                 ->assertViewIs('productpricing.list.partnerList')
                 ->assertSeeText($partner->name);

        $this->assertDatabaseHas('partner_product', [
            'partner_id' => $partner->id,
            'product_id' => $product->id,
        ]);

        $partner1 = factory(\App\Partner::class)->create();

        $response = $this->post('/dashboard/productpricing/link_partner/' . base64_encode($product->id), ['partner_id' => $partner1->id]);

        $response->assertStatus(200)
                 ->assertViewIs('productpricing.list.partnerList')
                 ->assertSeeText($partner1->name);

        $this->assertDatabaseHas('partner_product', [
            'partner_id' => $partner1->id,
            'product_id' => $product->id,
        ]);

        $response = $this->get('/dashboard/productpricing/unlink_partner/' . base64_encode($product->id) . '/' . base64_encode($partner->id));

        $response->assertStatus(200)
                 ->assertDontSeeText($partner->name);

        $this->assertDatabaseMissing('partner_product', [
            'partner_id' => $partner->id,
            'product_id' => $product->id,
        ]);
    }

    /** @test */
    public function can_not_detach_partner_if_product_or_partner_do_not_exist()
    {
        $product = factory(\App\Product::class)->create();
        $partner = factory(\App\Partner::class)->create();

        $response = $this->post('/dashboard/productpricing/link_partner/' . base64_encode($product->id), ['partner_id' => $partner->id]);

        $response->assertStatus(200)
                 ->assertViewIs('productpricing.list.partnerList')
                 ->assertSeeText($partner->name);

        $this->assertDatabaseHas('partner_product', [
            'partner_id' => $partner->id,
            'product_id' => $product->id,
        ]);

        $response = $this->get('/dashboard/productpricing/unlink_partner/' . base64_encode($product->id + 10) . '/' . base64_encode($partner->id));

        $response->assertStatus(404);

        $this->assertDatabaseHas('partner_product', [
            'partner_id' => $partner->id,
            'product_id' => $product->id,
        ]);

        $response = $this->get('/dashboard/productpricing/unlink_partner/' . base64_encode($product->id) . '/' . base64_encode($partner->id + 10));

        $response->assertStatus(404);

        $this->assertDatabaseHas('partner_product', [
            'partner_id' => $partner->id,
            'product_id' => $product->id,
        ]);
    }

    /** @test */
    public function can_attach_redemption_partner()
    {
        $product = factory(\App\Product::class)->create();
        $partner = factory(\App\Partner::class)->create();

        $response = $this->post('/dashboard/productpricing/link_redemption_partner/' . base64_encode($product->id), ['partner_id' => $partner->id]);

        $response->assertStatus(200)
                 ->assertViewIs('productpricing.list.redemptionPartnersList')
                 ->assertSeeText($partner->name);

        $this->assertDatabaseHas('product_partner_redemption', [
            'partner_id' => $partner->id,
            'product_id' => $product->id,
        ]);
    }

    /** @test */
    public function can_not_attach_redemption_partner_if_product_or_partner_do_not_exist()
    {
        $product = factory(\App\Product::class)->create();
        $partner = factory(\App\Partner::class)->create();

        $response = $this->post('/dashboard/productpricing/link_redemption_partner/' . base64_encode($product->id + 10), ['partner_id' => $partner->id]);

        $response->assertStatus(404);

        $this->assertDatabaseMissing('product_partner_redemption', [
            'partner_id' => $partner->id,
            'product_id' => $product->id,
        ]);

        $response = $this->post('/dashboard/productpricing/link_redemption_partner/' . base64_encode($product->id), ['partner_id' => $partner->id + 10]);

        $response->assertStatus(404);

        $this->assertDatabaseMissing('product_partner_redemption', [
            'partner_id' => $partner->id,
            'product_id' => $product->id,
        ]);
    }

    /** @test */
    public function can_not_attach_duplicate_redemption_partner()
    {
        $product = factory(\App\Product::class)->create();
        $partner = factory(\App\Partner::class)->create();

        $response = $this->post('/dashboard/productpricing/link_redemption_partner/' . base64_encode($product->id), ['partner_id' => $partner->id]);

        $response->assertStatus(200)
                 ->assertViewIs('productpricing.list.redemptionPartnersList')
                 ->assertSeeText($partner->name);

        $this->assertDatabaseHas('product_partner_redemption', [
            'partner_id' => $partner->id,
            'product_id' => $product->id,
        ]);

        $response = $this->post('/dashboard/productpricing/link_redemption_partner/' . base64_encode($product->id), ['partner_id' => $partner->id]);

        $response->assertStatus(200)
                 ->assertViewIs('productpricing.list.redemptionPartnersList')
                 ->assertSeeText($partner->name);

        $this->assertEquals(1, $product->ProductPartnerRedemption()->count());
    }

    /** @test */
    public function can_detach_redemption_partner()
    {
        $product = factory(\App\Product::class)->create();
        $partner = factory(\App\Partner::class)->create();
        $redemptioProduct = factory(\App\ProductPartnerRedemption::class)->create(
            [
                'product_id' => $product->id,
                'partner_id' => $partner->id
            ]
        );

        $response = $this->get('/dashboard/productpricing/unlink_redemption_partner/' . base64_encode($product->id) . '/' . base64_encode($redemptioProduct->id));

        $response->assertStatus(200)
                 ->assertViewIs('productpricing.list.redemptionPartnersList')
                 ->assertDontSeeText($partner->name);

        $this->assertDatabaseMissing('product_partner_redemption', [
            'partner_id' => $partner->id,
            'product_id' => $product->id,
        ]);
    }

    /** @test */
    public function can_not_detach_redemption_partner_if_product_or_partner_do_not_exist()
    {
        $product = factory(\App\Product::class)->create();
        $partner = factory(\App\Partner::class)->create();
        $redemptioProduct = factory(\App\ProductPartnerRedemption::class)->create(
            [
                'product_id' => $product->id,
                'partner_id' => $partner->id
            ]
        );

        $response = $this->get('/dashboard/productpricing/unlink_redemption_partner/' . base64_encode($product->id + 10) . '/' . base64_encode($redemptioProduct->id));

        $response->assertStatus(404);

        $this->assertDatabaseHas('product_partner_redemption', [
            'partner_id' => $partner->id,
            'product_id' => $product->id,
        ]);

        $response = $this->get('/dashboard/productpricing/unlink_redemption_partner/' . base64_encode($product->id) . '/' . base64_encode($redemptioProduct->id + 10));

        $response->assertStatus(404);

        $this->assertDatabaseHas('product_partner_redemption', [
            'partner_id' => $partner->id,
            'product_id' => $product->id,
        ]);
    }

    /** @test */
    public function can_attach_reward_partner()
    {
        $product = factory(\App\Product::class)->create();
        $partner = factory(\App\Partner::class)->create();

        $response = $this->post('/dashboard/productpricing/link_reward_partner/' . base64_encode($product->id),
            [
                'partner_id' => $partner->id,
                'partner_prefix' => uniqid(),
                'partner_mapping_id' => uniqid(),
                'sku' => uniqid(),
            ]
        );

        $response->assertStatus(200)
                 ->assertViewIs('productpricing.list.rewardPartnersList')
                 ->assertSeeText($partner->name);

        $this->assertDatabaseHas('product_partner_reward', [
            'partner_id' => $partner->id,
            'product_id' => $product->id,
        ]);
    }

    /** @test */
    public function can_not_attach_reward_partner_if_product_or_partner_or_sku_do_not_exist()
    {
        $product = factory(\App\Product::class)->create();
        $partner = factory(\App\Partner::class)->create();

        $response = $this->post('/dashboard/productpricing/link_reward_partner/' . base64_encode($product->id + 10),
            [
                'partner_id' => $partner->id,
                'partner_prefix' => uniqid(),
                'partner_mapping_id' => uniqid(),
                'sku' => uniqid(),
            ]
        );

        $response->assertStatus(404);

        $this->assertDatabaseMissing('product_partner_reward', [
            'partner_id' => $partner->id,
            'product_id' => $product->id,
        ]);

        $response = $this->post('/dashboard/productpricing/link_reward_partner/' . base64_encode($product->id),
            [
                'partner_id' => $partner->id + 10,
                'partner_prefix' => uniqid(),
                'partner_mapping_id' => uniqid(),
                'sku' => uniqid(),
            ]
        );

        $response->assertStatus(404);

        $this->assertDatabaseMissing('product_partner_reward', [
            'partner_id' => $partner->id,
            'product_id' => $product->id,
        ]);
    }

    /** @test */
    public function can_not_attach_duplicate_reward_partner()
    {
        $product = factory(\App\Product::class)->create();
        $partner = factory(\App\Partner::class)->create();

        $response = $this->post('/dashboard/productpricing/link_reward_partner/' . base64_encode($product->id), [
                'partner_id' => $partner->id,
                'partner_prefix' => uniqid(),
                'partner_mapping_id' => uniqid(),
                'sku' => uniqid(),
        ]);

        $response->assertStatus(200)
                 ->assertViewIs('productpricing.list.rewardPartnersList')
                 ->assertSeeText($partner->name);

        $this->assertDatabaseHas('product_partner_reward', [
            'partner_id' => $partner->id,
            'product_id' => $product->id,
        ]);

        $response = $this->post('/dashboard/productpricing/link_reward_partner/' . base64_encode($product->id), [
            'partner_id' => $partner->id,
            'partner_prefix' => uniqid(),
            'partner_mapping_id' => uniqid(),
            'sku' => uniqid(),
        ]);

        $response->assertStatus(200)
                 ->assertViewIs('productpricing.list.rewardPartnersList')
                 ->assertSeeText($partner->name);

        $this->assertEquals(1, $product->ProductPartnerReward()->count());
    }

    /** @test */
    public function can_not_attach_reward_partner_if_sku_already_exists()
    {
        $product = factory(\App\Product::class)->create();
        $partner = factory(\App\Partner::class)->create();
        $sku = uniqid();

        $response = $this->post('/dashboard/productpricing/link_reward_partner/' . base64_encode($product->id),
            [
                'partner_id' => $partner->id,
                'partner_prefix' => uniqid(),
                'partner_mapping_id' => uniqid(),
                'sku' => $sku,
            ]
        );

        $response->assertStatus(200)
                 ->assertViewIs('productpricing.list.rewardPartnersList')
                 ->assertSeeText($partner->name);

        $this->assertDatabaseHas('product_partner_reward', [
            'partner_id' => $partner->id,
            'product_id' => $product->id,
        ]);

        $response = $this->post('/dashboard/productpricing/link_reward_partner/' . base64_encode($product->id),
            [
                'partner_id' => $partner->id,
                'partner_prefix' => uniqid(),
                'partner_mapping_id' => uniqid(),
                'sku' => $sku,
            ]
        );

        $response->assertStatus(200)
                 ->assertViewIs('productpricing.list.rewardPartnersList')
                 ->assertSeeText($partner->name);

        $this->assertEquals(1, $product->ProductPartnerReward()->count());
    }

    /** @test */
    public function can_not_attach_reward_partner_without_sku()
    {
        $product = factory(\App\Product::class)->create();
        $partner = factory(\App\Partner::class)->create();
        $sku = uniqid();

        $response = $this->post('/dashboard/productpricing/link_reward_partner/' . base64_encode($product->id),
            [
                'partner_id' => $partner->id,
                'partner_prefix' => uniqid(),
                'partner_mapping_id' => uniqid(),
                'sku' => $sku,
            ]
        );

        $response->assertStatus(200)
                 ->assertViewIs('productpricing.list.rewardPartnersList')
                 ->assertSeeText($partner->name);

        $this->assertDatabaseHas('product_partner_reward', [
            'partner_id' => $partner->id,
            'product_id' => $product->id,
        ]);

        $response = $this->post('/dashboard/productpricing/link_reward_partner/' . base64_encode($product->id),
            [
                'partner_id' => $partner->id,
                'partner_prefix' => uniqid(),
                'partner_mapping_id' => uniqid(),
                'sku' => $sku,
            ]
        );

        $response->assertStatus(200)
                 ->assertViewIs('productpricing.list.rewardPartnersList')
                 ->assertSeeText($partner->name);

        $this->assertEquals(1, $product->ProductPartnerReward()->count());
    }

    /** @test */
    public function can_detach_reward_partner()
    {
        $product = factory(\App\Product::class)->create();
        $partner = factory(\App\Partner::class)->create();

        $rewardProduct = factory(\App\ProductPartnerReward::class)->create(
            [
                'partner_id' => $partner->id,
                'product_id' => $product->id,
                'partner_mapping_id' => uniqid(),
                'sku' => uniqid(),
            ]
        );

        $response = $this->get('/dashboard/productpricing/unlink_reward_partner/' . base64_encode($product->id) . '/' . base64_encode($rewardProduct->id));

        $response->assertStatus(200)
                 ->assertViewIs('productpricing.list.rewardPartnersList')
                 ->assertDontSeeText($partner->name);

        $this->assertDatabaseMissing('product_partner_reward', [
            'partner_id' => $partner->id,
            'product_id' => $product->id,
            'sku' => $rewardProduct->sku,
        ]);
    }

    /** @test */
    public function can_not_detach_reward_partner_if_product_or_partner_do_not_exist()
    {
        $product = factory(\App\Product::class)->create();
        $partner = factory(\App\Partner::class)->create();

        $rewardProduct = factory(\App\ProductPartnerReward::class)->create(
            [
                'partner_id' => $partner->id,
                'product_id' => $product->id,
                'partner_mapping_id' => uniqid(),
                'sku' => uniqid(),
            ]
        );

        $response = $this->get('/dashboard/productpricing/unlink_reward_partner/' . base64_encode($product->id + 1) . '/' . base64_encode($rewardProduct->id));

        $response->assertStatus(404);

        $this->assertDatabaseHas('product_partner_reward', [
            'partner_id' => $partner->id,
            'product_id' => $product->id,
            'sku' => $rewardProduct->sku,
        ]);

        $response = $this->get('/dashboard/productpricing/unlink_reward_partner/' . base64_encode($product->id) . '/' . base64_encode($rewardProduct->id + 1));

        $response->assertStatus(404);

        $this->assertDatabaseHas('product_partner_reward', [
            'partner_id' => $partner->id,
            'product_id' => $product->id,
            'sku' => $rewardProduct->sku,
        ]);
    }

    /** @test */
    public function can_create_new_translation()
    {
        $product = factory(\App\Product::class)->create();
        $response = $this->get('/dashboard/prodtranslation/new/' . base64_encode($product->id));

        $response->assertStatus(200)
                 ->assertViewIs('prodtranslation.new');
    }

    /** @test */
    public function can_edit_translation()
    {
        $language = factory(\App\Language::class)->create();
        $product = factory(\App\Product::class)->create();
        $prodTransalation = factory(\App\Prodtranslation::class)->create([
            'product_id' => $product->id,
            'lang_id' => $language->id
        ]);

        $response = $this->get('/dashboard/prodtranslation/edit/' . $prodTransalation->id);

        $response->assertStatus(200)
                 ->assertViewIs('prodtranslation.form');
    }

    /** @test */
    public function can_add_translation()
    {
        $data = [
            'prodtranslation_name' => uniqid(),
            'prodtranslation_pid' => factory(\App\Product::class)->create()->id,
            'prodtranslation_lang' => factory(\App\Language::class)->create()->id,
        ];

        $response = $this->call('POST', '/dashboard/prodtranslation/create', $data);

        $response->assertStatus(200)
                 ->assertJson([
                     'failed' => false,
                 ]);

        $this->assertDatabaseHas('product_translation', [
            'product_id' => $data['prodtranslation_pid'],
            'lang_id' => $data['prodtranslation_lang'],
        ]);
    }

    /** @test */
    public function can_not_add_translation_if_product_does_not_exist()
    {
        $data = [
            'prodtranslation_name' => uniqid(),
            'prodtranslation_pid' => factory(\App\Product::class)->create()->id + 1,
            'prodtranslation_lang' => factory(\App\Language::class)->create()->id,
        ];

        $response = $this->call('POST', '/dashboard/prodtranslation/create', $data);

        $response->assertStatus(404);

        $this->assertDatabaseMissing('product_translation', [
            'product_id' => $data['prodtranslation_pid'],
            'lang_id' => $data['prodtranslation_lang'],
        ]);
    }

    /** @test */
    public function can_update_translation()
    {
        $language = factory(\App\Language::class)->create();
        $product = factory(\App\Product::class)->create();

        $prodTranslation = factory(\App\Prodtranslation::class)->create([
            'product_id' => $product->id,
            'lang_id' => $language->id,
            'name' => 'test',
        ]);

        $data = [
            'prodtranslation_name' => uniqid(),
        ];

        $response = $this->call('POST', '/dashboard/prodtranslation/update/' . $prodTranslation->id, $data);

        $response->assertStatus(200)
                 ->assertJson([
                     'failed' => false,
                 ]);
    }

    /** @test */
    public function can_not_update_translation_if_translation_is_incorrect()
    {
        $language = factory(\App\Language::class)->create();
        $product = factory(\App\Product::class)->create();

        $prodTranslation = factory(\App\Prodtranslation::class)->create([
            'product_id' => $product->id,
            'lang_id' => $language->id,
            'name' => 'test',
        ]);

        $data = [
            'prodtranslation_name' => uniqid(),
        ];

        $response = $this->call('POST', '/dashboard/prodtranslation/update/' . $prodTranslation->id + 10, $data);

        $response->assertStatus(404);
    }

    /** @test */
    public function check_validation()
    {
        $language = factory(\App\Language::class)->create();
        $product = factory(\App\Product::class)->create();

        $prodTranslation = factory(\App\Prodtranslation::class)->create([
            'product_id' => $product->id,
            'lang_id' => $language->id,
        ]);

        $data = [
            'prodtranslation_name' => '',
        ];

        $response = $this->call('POST', '/dashboard/prodtranslation/update/' . $prodTranslation->id, $data);

        $response->assertStatus(200)
                 ->assertJson([
                     'failed' => true,
                     'errors' => "Please ensure that you provide a valid prodtranslation name"
                 ]);
    }

    /** @test */
    public function can_delete_translation()
    {
        $prodTranslation = factory(\App\Prodtranslation::class)->create([
            'product_id' => factory(\App\Product::class)->create()->id,
            'lang_id' => factory(\App\Language::class)->create()->id,
            'name' => uniqid(),
        ]);

        $response = $this->get('/dashboard/prodtranslation/delete/' . $prodTranslation->id);

        $response->assertStatus(200)
                 ->assertJson([
                     'failed' => false
                 ]);

        $this->assertDatabaseMissing('product_translation', [
            'product_id' => $prodTranslation->product_id,
            'lang_id' => $prodTranslation->lang_id,
        ]);
    }

    /** @test */
    public function can_not_delete_translation_if_translation_is_incorrect()
    {
        $prodTranslation = factory(\App\Prodtranslation::class)->create([
            'product_id' => factory(\App\Product::class)->create()->id,
            'lang_id' => factory(\App\Language::class)->create()->id,
            'name' => uniqid(),
        ]);

        $response = $this->get('/dashboard/prodtranslation/delete/' . $prodTranslation->id + 1);

        $response->assertStatus(404);

        $this->assertDatabaseHas('product_translation', [
            'product_id' => $prodTranslation->product_id,
            'lang_id' => $prodTranslation->lang_id,
        ]);
    }

    /** @test */
    public function can_attach_upc()
    {
        $product = factory(\App\Product::class)->create();
        $upc = uniqid();

        $response = $this->post('/dashboard/productpricing/link_upc/' . base64_encode($product->id), ['upc' => $upc]);

        $response->assertStatus(200)
                 ->assertViewIs('productpricing.list.upcList')
                 ->assertSeeText($upc);

        $this->assertDatabaseHas('product_upc', [
            'product_id' => $product->id,
            'upc' => $upc,
        ]);
    }

    /** @test */
    public function can_not_attach_upc_if_product_or_upc_do_not_exist()
    {
        $product = factory(\App\Product::class)->create();
        $upc = uniqid();

        $response = $this->post('/dashboard/productpricing/link_upc/' . base64_encode($product->id + 1), ['upc' => $upc]);

        $response->assertStatus(404);

        $this->assertDatabaseMissing('product_upc', [
            'product_id' => $product->id,
            'upc' => $upc,
        ]);

        $response = $this->post('/dashboard/productpricing/link_upc/' . base64_encode($product->id), ['upc' => '']);

        $response->assertStatus(404);

        $this->assertDatabaseMissing('product_upc', [
            'product_id' => $product->id,
            'upc' => $upc,
        ]);
    }

    /** @test */
    public function can_not_attach_duplicate_upc()
    {
        $product = factory(\App\Product::class)->create();
        $upc = uniqid();

        $response = $this->post('/dashboard/productpricing/link_upc/' . base64_encode($product->id), ['upc' => $upc]);

        $response->assertStatus(200)
                 ->assertViewIs('productpricing.list.upcList')
                 ->assertSeeText($upc);

        $this->assertDatabaseHas('product_upc', [
            'product_id' => $product->id,
            'upc' => $upc,
        ]);

        $response = $this->post('/dashboard/productpricing/link_upc/' . base64_encode($product->id), ['upc' => $upc]);

        $response->assertStatus(200)
                 ->assertViewIs('productpricing.list.upcList')
                 ->assertSeeText($upc)
                 ->assertSeeText('UPC already in use for other product.');

        $this->assertEquals(1, $product->upcs()->count());
    }

    /** @test */
    public function can_detach_upc()
    {
        $product = factory(\App\Product::class)->create();
        $upc = uniqid();

        $upcProduct = factory(\App\ProductUpc::class)->create(
            [
                'product_id' => $product->id,
                'upc' => $upc
            ]
        );

        $response = $this->get('/dashboard/productpricing/unlink_upc/' . base64_encode($product->id) . '/' . base64_encode($upcProduct->id));

        $response->assertStatus(200)
                 ->assertViewIs('productpricing.list.upcList')
                 ->assertDontSeeText($upc);

        $this->assertDatabaseMissing('product_upc', [
            'product_id' => $product->id,
            'upc' => $upc,
        ]);
    }

    /** @test */
    public function can_not_detach_upc_if_product_or_upc_do_not_exist()
    {
        $product = factory(\App\Product::class)->create();
        $upc = uniqid();

        $upcProduct = factory(\App\ProductUpc::class)->create(
            [
                'product_id' => $product->id,
                'upc' => $upc
            ]
        );

        $response = $this->get('/dashboard/productpricing/unlink_upc/' . base64_encode($product->id + 1) . '/' . base64_encode($upcProduct->id));

        $response->assertStatus(404);

        $this->assertDatabaseHas('product_upc', [
            'product_id' => $product->id,
            'upc' => $upc
        ]);

        $response = $this->get('/dashboard/productpricing/unlink_upc/' . base64_encode($product->id) . '/' . base64_encode($upcProduct->id + 1));

        $response->assertStatus(404);

        $this->assertDatabaseHas('product_upc', [
            'product_id' => $product->id,
            'upc' => $upc
        ]);
    }

    /** @test */
    public function can_view_edit_upc()
    {
        $product = factory(\App\Product::class)->create();
        $upc = uniqid();

        $upcProduct = factory(\App\ProductUpc::class)->create(
            [
                'product_id' => $product->id,
                'upc' => $upc
            ]
        );

        $response = $this->get('/dashboard/productpricing/edit_upc/' . base64_encode($upcProduct->id));

        $response->assertStatus(200)
                 ->assertViewIs('productpricing.dialog.edit_upc');
    }

    /** @test */
    public function can_not_view_edit_upc_if_id_does_not_exist()
    {
        $product = factory(\App\Product::class)->create();
        $upc = uniqid();

        $upcProduct = factory(\App\ProductUpc::class)->create(
            [
                'product_id' => $product->id,
                'upc' => $upc
            ]
        );

        $response = $this->get('/dashboard/productpricing/edit_upc/' . base64_encode($upcProduct->id + 1));

        $response->assertStatus(404);
    }

    /** @test */
    public function can_update_upc()
    {
        $product = factory(\App\Product::class)->create();

        $upcProduct = factory(\App\ProductUpc::class)->create(
            [
                'product_id' => $product->id,
                'upc' => uniqid()
            ]
        );

        $upc = uniqid();

        $response = $this->post('/dashboard/productpricing/update_upc/' . base64_encode($upcProduct->id)  . '/' . base64_encode($product->id), ['upc' => $upc]);

        $response->assertStatus(200)
                 ->assertViewIs('productpricing.list.upcList');

        $this->assertDatabaseHas('product_upc', [
            'product_id' => $product->id,
            'upc' => $upc
        ]);
    }

    /** @test */
    public function can_not_update_upc_if_id_does_not_exist()
    {
        $product = factory(\App\Product::class)->create();
        $upc = uniqid();

        $upcProduct = factory(\App\ProductUpc::class)->create(
            [
                'product_id' => $product->id,
                'upc' => uniqid()
            ]
        );

        $response = $this->post('/dashboard/productpricing/update_upc/' . base64_encode($upcProduct->id + 1)  . '/' . base64_encode($product->id));

        $response->assertStatus(404);

        $this->assertDatabaseHas('product_upc', [
            'product_id' => $product->id,
            'upc' => $upcProduct->upc
        ]);

        $response = $this->post('/dashboard/productpricing/update_upc/' . base64_encode($upcProduct->id) . '/' . base64_encode($product->id + 1));

        $response->assertStatus(404);

        $this->assertDatabaseHas('product_upc', [
            'product_id' => $product->id,
            'upc' => $upcProduct->upc
        ]);
    }
}