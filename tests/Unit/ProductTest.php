<?php

namespace Tests\Unit;

use App\Admin;
use Carbon\Carbon;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ProductTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    protected $user;

    public function setUp()
    {
        parent::setUp();

        $user = Admin::where('email', 'info@bluloyalty.com')->first();
        $this->user = $user;
        $this->actingAs($user)
             ->session([
                 'user_roles' => [
                     'BLU Admin',
                 ],
                 'user_permissions' => [
                     'view_products',
                     'edit_products',
                     'create_products',
                     'delete_products'
                 ],
             ]);
    }

    /** @test */
    public function can_view_product_list_page_with_permission()
    {
        factory(\App\Product::class)->create();

        $response = $this->get('/dashboard/catalogue');

        $response->assertStatus(200)
                 ->assertViewIs('product.list')
                 ->assertViewHas('products');
    }

    /** @test */
    public function can_not_view_product_list_page_without_permission()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        factory(\App\Product::class)->create();

        $response = $this->get('/dashboard/catalogue');

        $response->assertStatus(302)
                 ->assertRedirect()
                 ->assertRedirect('/dashboard');
    }

    /** @test */
    public function can_filter_product_by_search_query()
    {
        $product = factory(\App\Product::class)->create();

        $response = $this->get('/dashboard/catalogue?q=' . uniqid());

        $response->assertStatus(200)
                 ->assertViewIs('product.list')
                 ->assertDontSee($product->name);

        $response = $this->get('/dashboard/catalogue?q=' . $product->name);

        $response->assertStatus(200)
                 ->assertViewIs('product.list')
                 ->assertSee($product->name);
    }

    /** @test */
    public function can_filter_product_by_partner()
    {
        $product = factory(\App\Product::class)->create();
        $product->partners()->save((\App\Partner::find(1)->first()));

        $partner1 = factory(\App\Partner::class)->create();
        $partner2 = factory(\App\Partner::class)->create();
        $partner3 = factory(\App\Partner::class)->create();

        $response = $this->get('/dashboard/catalogue?partner_list[]=' . $partner1->id . '&partner_list[]=' . $partner2->id);

        $response->assertStatus(200)
                 ->assertViewIs('product.list')
                 ->assertDontSee($product->name);

        $product->partners()->save($partner3);

        $response = $this->get('/dashboard/catalogue?partner_list[]=' . $partner3->id);

        $response->assertStatus(200)
                 ->assertViewIs('product.list')
                 ->assertSee($product->name);
    }

    /** @test */
    public function can_filter_product_by_supplier()
    {
        $product = factory(\App\Product::class)->create();
        $product->partners()->save((\App\Partner::find(1)->first()));

        $supplier1 = factory(\App\Supplier::class)->create();
        $supplier2 = factory(\App\Supplier::class)->create();
        $supplier3 = factory(\App\Supplier::class)->create();

        $response = $this->get('/dashboard/catalogue?supplier_list[]=' . $supplier1->id . '&supplier_list[]=' . $supplier2->id);

        $response->assertStatus(200)
                 ->assertViewIs('product.list')
                 ->assertDontSee($product->name);

        $product->supplier_id = $supplier3->id;
        $product->save();

        $response = $this->get('/dashboard/catalogue?supplier_list[]=' . $product->supplier_id);

        $response->assertStatus(200)
                 ->assertViewIs('product.list')
                 ->assertSee($product->name);
    }

    /** @test */
    public function can_filter_product_by_country()
    {
        $product = factory(\App\Product::class)->create();
        $product->partners()->save((\App\Partner::find(1)->first()));

        $country1 = factory(\App\Country::class)->create();
        $country2 = factory(\App\Country::class)->create();
        $country3 = factory(\App\Country::class)->create();

        $response = $this->get('/dashboard/catalogue?country_list[]=' . $country1->id . '&country_list[]=' . $country2->id);

        $response->assertStatus(200)
                 ->assertViewIs('product.list')
                 ->assertDontSee($product->name);

        $product->countries()->save($country3);

        $response = $this->get('/dashboard/catalogue?country_list[]=' . $country3->id);

        $response->assertStatus(200)
                 ->assertViewIs('product.list')
                 ->assertSee($product->name);
    }

    /** @test */
    public function can_filter_product_by_channel()
    {
        $product = factory(\App\Product::class)->create();
        $product->partners()->save((\App\Partner::find(1)->first()));

        $channel1 = factory(\App\ProductChannel::class)->create();
        $channel2 = factory(\App\ProductChannel::class)->create();
        $channel3 = factory(\App\ProductChannel::class)->create([
            'product_id' => $product->id
        ]);

        $response = $this->get('/dashboard/catalogue?displaychannel_list[]=' . $channel1->id . '&displaychannel_list[]=' . $channel2->id);

        $response->assertStatus(200)
                 ->assertViewIs('product.list')
                 ->assertDontSee($product->name);

        $response = $this->get('/dashboard/catalogue?displaychannel_list[]=' . $channel3->channel_id);

        $response->assertStatus(200)
                 ->assertViewIs('product.list')
                 ->assertSee($product->name);
    }

    /** @test */
    public function can_filter_product_by_brand()
    {
        $product = factory(\App\Product::class)->create();
        $product->partners()->save((\App\Partner::find(1)->first()));

        $brand1 = factory(\App\Brand::class)->create();
        $brand2 = factory(\App\Brand::class)->create();
        $brand3 = factory(\App\Brand::class)->create();

        $response = $this->get('/dashboard/catalogue?brand_list[]=' . $brand1->id . '&brand_list[]=' . $brand2->id);

        $response->assertStatus(200)
                 ->assertViewIs('product.list')
                 ->assertDontSee($product->name);

        $product->brand_id = $brand3->id;
        $product->save();

        $response = $this->get('/dashboard/catalogue?brand_list[]=' . $product->brand_id);

        $response->assertStatus(200)
                 ->assertViewIs('product.list')
                 ->assertSee($product->name);
    }

    /** @test */
    public function can_view_pagination_page()
    {
        $partner = Auth::User()->getTopLevelPartner()->id;
        $product = \App\Product::select('product.*');
        $products = $product->whereHas('Partners', function($q) use($partner) {
            $q->where('partner_id', $partner);
        });

        $perPage = 15;
        $pageCount = ceil($products->count() / $perPage);

        if ($pageCount > 1) {
            $response = $this->get('/dashboard/catalogue?page=' . $pageCount);

            $response->assertStatus(200)
                     ->assertViewIs('product.list')
                     ->assertSee('catalogue?page=1')
                     ->assertDontSee('No Items Found');
        }
    }

    /** @test */
    public function can_not_view_pagination_page_which_does_not_exist()
    {
        $partner = Auth::User()->getTopLevelPartner()->id;
        $product = \App\Product::select('product.*');
        $products = $product->whereHas('Partners', function($q) use($partner) {
            $q->where('partner_id', $partner);
        });

        $perPage = 15;
        $page = ceil($products->count() / $perPage) + 1000;

        $response = $this->get('/dashboard/catalogue?page=' . $page);

        $response->assertStatus(200)
                 ->assertViewIs('product.list')
                 ->assertDontSee('catalogue?page=' . $page)
                 ->assertSeeText('No Items Found');
    }

    /** @test */
    public function can_create_new_product_with_permission()
    {
        $response = $this->get('/dashboard/catalogue/new');

        $response->assertStatus(302)
                 ->assertRedirect()
                 ->assertSeeText('/dashboard/catalogue/view/');
    }

    /** @test */
    public function can_not_create_new_product_without_permission()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $response = $this->get('/dashboard/catalogue/new');

        $response->assertStatus(302)
                 ->assertRedirect()
                 ->assertSeeText('/dashboard');
    }

    /** @test */
    public function can_view_edit_page_with_permission()
    {
        $product = factory(\App\Product::class)->create();
        $product->partners()->save((\App\Partner::find(1)->first()));

        $response = $this->get('/dashboard/catalogue/view/' . base64_encode($product->id));

        $response->assertStatus(200)
                 ->assertViewIs('product.view')
                 ->assertSee($product->name);
    }

    /** @test */
    public function can_not_view_edit_page_without_permission()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $product = factory(\App\Product::class)->create();
        $product->partners()->save((\App\Partner::find(1)->first()));

        $response = $this->get('/dashboard/catalogue/view/' . base64_encode($product->id));

        $response->assertStatus(302)
                 ->assertRedirect()
                 ->assertSeeText('/dashboard');
    }

    /** @test */
    public function can_not_view_edit_page_if_partner_incorrect()
    {
        $product = factory(\App\Product::class)->create();
        $product->partners()->save((\App\Partner::find(1)->first()));

        $response = $this->get('/dashboard/catalogue/view/' . base64_encode($product->id + 10));

        $response->assertStatus(404)
                 ->assertDontSee($product->name);
    }

    /** @test */
    public function can_delete_product_with_permission()
    {
        $product = factory(\App\Product::class)->create();
        $response = $this->get('/dashboard/catalogue/delete/' . base64_encode($product->id));

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/catalogue');

        $this->assertDatabaseMissing('product', [
            'id' => $product->id,
        ]);
    }

    /** @test */
    public function can_not_delete_product_without_permission()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $product = factory(\App\Product::class)->create();
        $response = $this->get('/dashboard/catalogue/delete/' . base64_encode($product->id));

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard');

        $this->assertDatabaseHas('product', [
            'id' => $product->id,
        ]);
    }

    /** @test */
    public function can_not_delete_product_with_incorrect_id()
    {
        $product = factory(\App\Product::class)->create();
        $response = $this->get('/dashboard/catalogue/delete/' . base64_encode($product->id + 10));

        $response->assertStatus(404);

        $this->assertDatabaseHas('product', [
            'id' => $product->id,
        ]);
    }

    /** @test */
    public function can_update_product_with_permission()
    {
        $product = factory(\App\Product::class)->create();
        $currency = factory(\App\Currency::class)->create();
        factory(\App\CurrencyPricing::class)->create(['currency_id' => $currency->id]);

        $stub = __DIR__ . '\\..\\stubs\\test.png';
        $name = str_random(8) . '.png';
        $path = sys_get_temp_dir() . '\\' . $name;

        copy($stub, $path);

        $file = new UploadedFile($path, $name, filesize($path), 'image/png', null, true);

        $data = [
            'name'                  => uniqid(),
            'brand_id'              => factory(\App\Brand::class)->create()->id,
            'category_id'           => factory(\App\Category::class)->create()->id,
            'draft'                 => 0,
            'end_date'              => Carbon::today()->addDays(10),
            'qty'                   => random_int(1, 10),
            'currency_id'           => $currency->id,
            'original_price'        => random_int(1, 10),
            'original_retail_price' => random_int(1, 10),
            'original_sales_tax'    => random_int(1, 10),
            'weight'                => random_int(1, 10),
            'volume'                => random_int(1, 10),
            'delivery_charges'      => random_int(1, 10),
            'status'                => 'active'
        ];

        $response = $this->call('POST', '/dashboard/catalogue/update/' . base64_encode($product->id), $data, [], ['cover_image' => $file], ['Accept' => 'application/json']);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/catalogue/');

        $this->assertDatabaseHas('product', [
            'id' => $product->id,
            'name' => $data['name']
        ]);

        $this->assertFileExists(public_path(\App\Http\Controllers\MediaController::getImageFromId($product->cover_image)));

        @unlink(public_path(\App\Http\Controllers\MediaController::getImageFromId($product->profile_image)));

        $stub1 = __DIR__ . '\\..\\stubs\\test.png';
        $name1 = str_random(8) . '.png';
        $path1 = sys_get_temp_dir() . '\\' . $name1;

        copy($stub1, $path1);

        $file1 = new UploadedFile($path1, $name1, filesize($path1), 'image/png', null, true);

        $response = $this->call('POST', '/dashboard/catalogue/update/' . base64_encode($product->id), $data, [], ['image' => $file1], ['Accept' => 'application/json']);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/catalogue/');

        $this->assertDatabaseHas('product', [
            'id' => $product->id,
            'name' => $data['name']
        ]);
    }

    /** @test */
    public function can_not_update_without_permission()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $product = factory(\App\Product::class)->create();
        $currency = factory(\App\Currency::class)->create();
        factory(\App\CurrencyPricing::class)->create(['currency_id' => $currency->id]);

        $data = [
            'name'                  => uniqid(),
            'brand_id'              => factory(\App\Brand::class)->create()->id,
            'category_id'           => factory(\App\Category::class)->create()->id,
            'draft'                 => 0,
            'end_date'              => Carbon::today()->addDays(10),
            'qty'                   => random_int(1, 10),
            'currency_id'           => $currency->id,
            'original_price'        => random_int(1, 10),
            'original_retail_price' => random_int(1, 10),
            'original_sales_tax'    => random_int(1, 10),
            'weight'                => random_int(1, 10),
            'volume'                => random_int(1, 10),
            'delivery_charges'      => random_int(1, 10),
            'status'                => 'active'
        ];

        $response = $this->call('POST', '/dashboard/catalogue/update/' . base64_encode($product->id), $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard');

        $this->assertDatabaseMissing('product', [
            'id' => $product->id,
            'name' => $data['name']
        ]);
    }

    /** @test */
    public function can_not_update_product_which_is_not_exists()
    {
        $product = factory(\App\Product::class)->create();
        $currency = factory(\App\Currency::class)->create();
        factory(\App\CurrencyPricing::class)->create(['currency_id' => $currency->id]);

        $data = [
            'name' => uniqid(),
        ];

        $response = $this->call('POST', '/dashboard/catalogue/update/' . base64_encode($product->id + 10), $data);

        $response->assertStatus(404);

        $this->assertDatabaseMissing('product', [
            'id' => $product->id,
            'name' => $data['name']
        ]);
    }

    /** @test */
    public function can_validate_errors()
    {
        $product = factory(\App\Product::class)->create();
        $currency = factory(\App\Currency::class)->create();
        factory(\App\CurrencyPricing::class)->create(['currency_id' => $currency->id]);

        $data = [
            'name' => '',
            'brand_id' => '',
            'category_id' => ''
        ];

        $response = $this->post('/dashboard/catalogue/update/' . base64_encode($product->id), $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/catalogue/view/' . base64_encode($product->id))
                 ->assertSessionHasErrors('name')
                 ->assertSessionHasErrors('brand_id')
                 ->assertSessionHasErrors('category_id');

        $data = [
            'end_date' => random_int(1, 10),
        ];

        $response = $this->post('/dashboard/catalogue/update/' . base64_encode($product->id), $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/catalogue/view/' . base64_encode($product->id))
                 ->assertSessionHasErrors('end_date');

        $data = [
            'qty' => 'i',
            'original_price' => 'i',
            'original_retail_price' => 'i',
            'original_sales_tax' => 'i',
            'weight' => 'i',
            'volume' => 'i',
            'delivery_charges' => 'i',
        ];

        $response = $this->post('/dashboard/catalogue/update/' . base64_encode($product->id), $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/catalogue/view/' . base64_encode($product->id))
                 ->assertSessionHasErrors('qty')
                 ->assertSessionHasErrors('original_price')
                 ->assertSessionHasErrors('original_retail_price')
                 ->assertSessionHasErrors('original_sales_tax')
                 ->assertSessionHasErrors('weight')
                 ->assertSessionHasErrors('volume')
                 ->assertSessionHasErrors('delivery_charges');
    }

    /** @test */
    public function can_attach_partner()
    {
        $product = factory(\App\Product::class)->create();
        $partner = factory(\App\Partner::class)->create();

        $response = $this->post('/dashboard/catalogue/link_partner/' . base64_encode($product->id), ['partner_id' => $partner->id]);

        $response->assertStatus(200)
                 ->assertViewIs('product.list.partnerList')
                 ->assertSeeText($partner->name);

        $this->assertDatabaseHas('partner_product', [
            'partner_id' => $partner->id,
            'product_id' => $product->id,
        ]);
    }

    /** @test */
    public function can_not_attach_partner_if_product_or_partner_do_not_exist()
    {
        $product = factory(\App\Product::class)->create();
        $partner = factory(\App\Partner::class)->create();

        $response = $this->post('/dashboard/catalogue/link_partner/' . base64_encode($product->id + 10), ['partner_id' => $partner->id]);

        $response->assertStatus(404);

        $this->assertDatabaseMissing('partner_product', [
            'partner_id' => $partner->id,
            'product_id' => $product->id,
        ]);

        $response = $this->post('/dashboard/catalogue/link_partner/' . base64_encode($product->id), ['partner_id' => $partner->id + 10]);

        $response->assertStatus(404);

        $this->assertDatabaseMissing('partner_product', [
            'partner_id' => $partner->id,
            'product_id' => $product->id,
        ]);
    }

    /** @test */
    public function can_not_attach_duplicate_partner()
    {
        $product = factory(\App\Product::class)->create();
        $partner = factory(\App\Partner::class)->create();

        $response = $this->post('/dashboard/catalogue/link_partner/' . base64_encode($product->id), ['partner_id' => $partner->id]);

        $response->assertStatus(200)
                 ->assertViewIs('product.list.partnerList')
                 ->assertSeeText($partner->name);

        $this->assertDatabaseHas('partner_product', [
            'partner_id' => $partner->id,
            'product_id' => $product->id,
        ]);

        $response = $this->post('/dashboard/catalogue/link_partner/' . base64_encode($product->id), ['partner_id' => $partner->id]);

        $response->assertStatus(200)
                 ->assertViewIs('product.list.partnerList')
                 ->assertSeeText($partner->name);

        $this->assertEquals(1, $product->partners()->count());
    }

    /** @test */
    public function can_detach_partner()
    {
        $product = factory(\App\Product::class)->create();
        $partner = factory(\App\Partner::class)->create();

        $response = $this->post('/dashboard/catalogue/link_partner/' . base64_encode($product->id), ['partner_id' => $partner->id]);

        $response->assertStatus(200)
                 ->assertViewIs('product.list.partnerList')
                 ->assertSeeText($partner->name);

        $this->assertDatabaseHas('partner_product', [
            'partner_id' => $partner->id,
            'product_id' => $product->id,
        ]);

        $response = $this->get('/dashboard/catalogue/unlink_partner/' . base64_encode($product->id) . '/' . base64_encode($partner->id));

        $response->assertStatus(200)
                 ->assertDontSeeText($partner->name);

        $this->assertDatabaseMissing('partner_product', [
            'partner_id' => $partner->id,
            'product_id' => $product->id,
        ]);
    }

    /** @test */
    public function can_not_detach_partner_if_product_or_partner_do_not_exist()
    {
        $product = factory(\App\Product::class)->create();
        $partner = factory(\App\Partner::class)->create();

        $response = $this->post('/dashboard/catalogue/link_partner/' . base64_encode($product->id), ['partner_id' => $partner->id]);

        $response->assertStatus(200)
                 ->assertViewIs('product.list.partnerList')
                 ->assertSeeText($partner->name);

        $this->assertDatabaseHas('partner_product', [
            'partner_id' => $partner->id,
            'product_id' => $product->id,
        ]);

        $response = $this->get('/dashboard/catalogue/unlink_partner/' . base64_encode($product->id + 10) . '/' . base64_encode($partner->id));

        $response->assertStatus(404);

        $this->assertDatabaseHas('partner_product', [
            'partner_id' => $partner->id,
            'product_id' => $product->id,
        ]);

        $response = $this->get('/dashboard/catalogue/unlink_partner/' . base64_encode($product->id) . '/' . base64_encode($partner->id + 10));

        $response->assertStatus(404);

        $this->assertDatabaseHas('partner_product', [
            'partner_id' => $partner->id,
            'product_id' => $product->id,
        ]);
    }

    /** @test */
    public function can_attach_country()
    {
        $product = factory(\App\Product::class)->create();
        $country = factory(\App\Country::class)->create();

        $response = $this->post('/dashboard/catalogue/link_country/' . base64_encode($product->id), ['country_id' => $country->id]);

        $response->assertStatus(200)
                 ->assertViewIs('product.list.countryList')
                 ->assertSeeText($country->name);

        $this->assertDatabaseHas('country_product', [
            'country_id' => $country->id,
            'product_id' => $product->id,
        ]);
    }

    /** @test */
    public function can_not_attach_country_if_product_or_country_do_not_exist()
    {
        $product = factory(\App\Product::class)->create();
        $country = factory(\App\Country::class)->create();

        $response = $this->post('/dashboard/catalogue/link_country/' . base64_encode($product->id + 10), ['country_id' => $country->id]);

        $response->assertStatus(404);

        $this->assertDatabaseMissing('country_product', [
            'country_id' => $country->id,
            'product_id' => $product->id,
        ]);

        $response = $this->post('/dashboard/catalogue/link_country/' . base64_encode($product->id), ['country_id' => $country->id + 10]);

        $response->assertStatus(404);

        $this->assertDatabaseMissing('country_product', [
            'country_id' => $country->id,
            'product_id' => $product->id,
        ]);
    }

    /** @test */
    public function can_not_attach_duplicate_country()
    {
        $product = factory(\App\Product::class)->create();
        $country = factory(\App\Country::class)->create();

        $response = $this->post('/dashboard/catalogue/link_country/' . base64_encode($product->id), ['country_id' => $country->id]);

        $response->assertStatus(200)
                 ->assertViewIs('product.list.countryList')
                 ->assertSeeText($country->name);

        $this->assertDatabaseHas('country_product', [
            'country_id' => $country->id,
            'product_id' => $product->id,
        ]);

        $response = $this->post('/dashboard/catalogue/link_country/' . base64_encode($product->id), ['country_id' => $country->id]);

        $response->assertStatus(200)
                 ->assertViewIs('product.list.countryList')
                 ->assertSeeText($country->name);

        $this->assertEquals(1, $product->countries()->count());
    }

    /** @test */
    public function can_detach_country()
    {
        $product = factory(\App\Product::class)->create();
        $country = factory(\App\Country::class)->create();

        $response = $this->post('/dashboard/catalogue/link_country/' . base64_encode($product->id), ['country_id' => $country->id]);

        $response->assertStatus(200)
                 ->assertViewIs('product.list.countryList')
                 ->assertSeeText($country->name);

        $this->assertDatabaseHas('country_product', [
            'country_id' => $country->id,
            'product_id' => $product->id,
        ]);

        $response = $this->get('/dashboard/catalogue/unlink_country/' . base64_encode($product->id) . '/' . base64_encode($country->id));

        $response->assertStatus(200)
                 ->assertDontSeeText($country->name);

        $this->assertDatabaseMissing('country_product', [
            'country_id' => $country->id,
            'product_id' => $product->id,
        ]);
    }

    /** @test */
    public function can_not_detach_country_if_product_or_country_do_not_exist()
    {
        $product = factory(\App\Product::class)->create();
        $country = factory(\App\Country::class)->create();

        $response = $this->post('/dashboard/catalogue/link_country/' . base64_encode($product->id), ['country_id' => $country->id]);

        $response->assertStatus(200)
                 ->assertViewIs('product.list.countryList')
                 ->assertSeeText($country->name);

        $this->assertDatabaseHas('country_product', [
            'country_id' => $country->id,
            'product_id' => $product->id,
        ]);

        $response = $this->get('/dashboard/catalogue/unlink_country/' . base64_encode($product->id + 10) . '/' . base64_encode($country->id));

        $response->assertStatus(404);

        $this->assertDatabaseHas('country_product', [
            'country_id' => $country->id,
            'product_id' => $product->id,
        ]);

        $response = $this->get('/dashboard/catalogue/unlink_country/' . base64_encode($product->id) . '/' . base64_encode($country->id + 10));

        $response->assertStatus(404);

        $this->assertDatabaseHas('country_product', [
            'country_id' => $country->id,
            'product_id' => $product->id,
        ]);
    }

    /** @test */
    public function can_attach_channel()
    {
        $product = factory(\App\Product::class)->create();
        $channel = factory(\App\Partner::class)->create();

        $response = $this->post('/dashboard/catalogue/link_channel/' . base64_encode($product->id), ['partner_id' => $channel->id]);

        $response->assertStatus(200)
                 ->assertViewIs('product.list.channelList')
                 ->assertSeeText($channel->name);

        $this->assertDatabaseHas('channel_product', [
            'product_id' => $product->id,
            'channel_id' => $channel->id,
        ]);
    }

    /** @test */
    public function can_not_attach_channel_if_product_or_channel_do_not_exist()
    {
        $product = factory(\App\Product::class)->create();
        $channel = factory(\App\Partner::class)->create();

        $response = $this->post('/dashboard/catalogue/link_channel/' . base64_encode($product->id + 10), ['partner_id' => $channel->id]);

        $response->assertStatus(404);

        $this->assertDatabaseMissing('channel_product', [
            'product_id' => $product->id,
            'channel_id' => $channel->id,
        ]);

        $response = $this->post('/dashboard/catalogue/link_channel/' . base64_encode($product->id), ['partner_id' => $channel->id + 10]);

        $response->assertStatus(404);

        $this->assertDatabaseMissing('channel_product', [
            'product_id' => $product->id,
            'channel_id' => $channel->id,
        ]);
    }

    /** @test */
    public function can_not_attach_duplicate_channel()
    {
        $product = factory(\App\Product::class)->create();
        $channel = factory(\App\Partner::class)->create();

        $this->post('/dashboard/catalogue/link_channel/' . base64_encode($product->id), ['partner_id' => $channel->id]);

        $this->post('/dashboard/catalogue/link_channel/' . base64_encode($product->id), ['partner_id' => $channel->id]);

        $this->assertEquals(1, $product->channels()->count());
    }

    /** @test */
    public function can_detach_channel()
    {
        $product = factory(\App\Product::class)->create();
        $channel = factory(\App\Partner::class)->create();

        $this->post('/dashboard/catalogue/link_channel/' . base64_encode($product->id), ['partner_id' => $channel->id]);

        $response = $this->get('/dashboard/catalogue/unlink_channel/' . base64_encode($product->id) . '/' . base64_encode($channel->id));

        $response->assertStatus(200)
                 ->assertDontSeeText($channel->name);

        $this->assertDatabaseMissing('channel_product', [
            'product_id' => $product->id,
            'channel_id' => $channel->id,
        ]);
    }

    /** @test */
    public function can_not_detach_channel_if_channel_or_product_do_not_exist()
    {
        $product = factory(\App\Product::class)->create();
        $channel = factory(\App\Partner::class)->create();

        $this->post('/dashboard/catalogue/link_channel/' . base64_encode($product->id), ['partner_id' => $channel->id]);

        $response = $this->get('/dashboard/catalogue/unlink_channel/' . base64_encode($product->id + 10) . '/' . base64_encode($channel->id));

        $response->assertStatus(404);

        $this->assertDatabaseHas('channel_product', [
            'product_id' => $product->id,
            'channel_id' => $channel->id,
        ]);

        $response = $this->get('/dashboard/catalogue/unlink_channel/' . base64_encode($product->id) . '/' . base64_encode($channel->id + 10));

        $response->assertStatus(404);

        $this->assertDatabaseHas('channel_product', [
            'product_id' => $product->id,
            'channel_id' => $channel->id,
        ]);
    }

    /** @test */
    public function can_attach_segment()
    {
        $product = factory(\App\Product::class)->create();
        $segment = factory(\App\Segment::class)->create();

        $response = $this->post('/dashboard/catalogue/link_segment/' . base64_encode($product->id), ['segment_id' => $segment->id]);

        $response->assertStatus(200)
                 ->assertViewIs('product.list.segmentList')
                 ->assertSeeText($segment->name);

        $this->assertDatabaseHas('product_segment', [
            'product_id' => $product->id,
            'segment_id' => $segment->id,
        ]);
    }

    /** @test */
    public function can_not_attach_segment_if_product_or_segment_do_not_exist()
    {
        $product = factory(\App\Product::class)->create();
        $segment = factory(\App\Segment::class)->create();

        $response = $this->post('/dashboard/catalogue/link_segment/' . base64_encode($product->id + 10), ['segment_id' => $segment->id]);

        $response->assertStatus(404);

        $this->assertDatabaseMissing('product_segment', [
            'product_id' => $product->id,
            'segment_id' => $segment->id,
        ]);

        $response = $this->post('/dashboard/catalogue/link_segment/' . base64_encode($product->id), ['segment_id' => $segment->id + 10]);

        $response->assertStatus(404);

        $this->assertDatabaseMissing('product_segment', [
            'product_id' => $product->id,
            'segment_id' => $segment->id,
        ]);
    }

    /** @test */
    public function can_not_attach_duplicate_segment()
    {
        $product = factory(\App\Product::class)->create();
        $segment = factory(\App\Segment::class)->create();

        $this->post('/dashboard/catalogue/link_segment/' . base64_encode($product->id), ['segment_id' => $segment->id]);

        $this->post('/dashboard/catalogue/link_segment/' . base64_encode($product->id), ['segment_id' => $segment->id]);

        $this->assertEquals(1, $product->segments()->count());
    }

    /** @test */
    public function can_detach_segment()
    {
        $product = factory(\App\Product::class)->create();
        $segment = factory(\App\Segment::class)->create();

        $this->post('/dashboard/catalogue/link_segment/' . base64_encode($product->id), ['segment_id' => $segment->id]);

        $response = $this->get('/dashboard/catalogue/unlink_segment/' . base64_encode($product->id) . '/' . base64_encode($segment->id));

        $response->assertStatus(200)
                 ->assertDontSeeText($segment->name);

        $this->assertDatabaseMissing('product_segment', [
            'product_id' => $product->id,
            'segment_id' => $segment->id,
        ]);
    }

    /** @test */
    public function can_not_detach_segment_if_segment_or_product_do_not_exist()
    {
        $product = factory(\App\Product::class)->create();
        $segment = factory(\App\Segment::class)->create();

        $response = $this->post('/dashboard/catalogue/link_segment/' . base64_encode($product->id), ['segment_id' => $segment->id]);

        $response->assertStatus(200)
                 ->assertViewIs('product.list.segmentList')
                 ->assertSeeText($segment->name);

        $this->assertDatabaseHas('product_segment', [
            'product_id' => $product->id,
            'segment_id' => $segment->id,
        ]);

        $response = $this->get('/dashboard/catalogue/unlink_segment/' . base64_encode($product->id + 10) . '/' . base64_encode($segment->id));

        $response->assertStatus(404);

        $this->assertDatabaseHas('product_segment', [
            'product_id' => $product->id,
            'segment_id' => $segment->id,
        ]);

        $response = $this->get('/dashboard/catalogue/unlink_channel/' . base64_encode($product->id) . '/' . base64_encode($segment->id + 10));

        $response->assertStatus(404);

        $this->assertDatabaseHas('product_segment', [
            'product_id' => $product->id,
            'segment_id' => $segment->id,
        ]);
    }

    /** @test */
    public function can_create_new_translation()
    {
        $product = factory(\App\Product::class)->create();
        $response = $this->get('/dashboard/prodtranslation/new/' . base64_encode($product->id));

        $response->assertStatus(200)
                 ->assertViewIs('prodtranslation.new');
    }

    /** @test */
    public function can_edit_translation()
    {
        $language = factory(\App\Language::class)->create();
        $product = factory(\App\Product::class)->create();
        $prodTransalation = factory(\App\Prodtranslation::class)->create([
            'product_id' => $product->id,
            'lang_id' => $language->id
        ]);

        $response = $this->get('/dashboard/prodtranslation/edit/' . $prodTransalation->id);

        $response->assertStatus(200)
                 ->assertViewIs('prodtranslation.form');
    }

    /** @test */
    public function can_add_translation()
    {
        $data = [
            'prodtranslation_name' => uniqid(),
            'prodtranslation_pid' => factory(\App\Product::class)->create()->id,
            'prodtranslation_lang' => factory(\App\Language::class)->create()->id,
        ];

        $response = $this->call('POST', '/dashboard/prodtranslation/create', $data);

        $response->assertStatus(200)
                 ->assertJson([
                     'failed' => false,
                 ]);

        $this->assertDatabaseHas('product_translation', [
            'product_id' => $data['prodtranslation_pid'],
            'lang_id' => $data['prodtranslation_lang'],
        ]);
    }

    /** @test */
    public function can_not_add_translation_if_product_does_not_exist()
    {
        $data = [
            'prodtranslation_name' => uniqid(),
            'prodtranslation_pid' => factory(\App\Product::class)->create()->id + 1,
            'prodtranslation_lang' => factory(\App\Language::class)->create()->id,
        ];

        $response = $this->call('POST', '/dashboard/prodtranslation/create', $data);

        $response->assertStatus(404);

        $this->assertDatabaseMissing('product_translation', [
            'product_id' => $data['prodtranslation_pid'],
            'lang_id' => $data['prodtranslation_lang'],
        ]);
    }

    /** @test */
    public function can_update_translation()
    {
        $language = factory(\App\Language::class)->create();
        $product = factory(\App\Product::class)->create();

        $prodTranslation = factory(\App\Prodtranslation::class)->create([
            'product_id' => $product->id,
            'lang_id' => $language->id,
            'name' => 'test',
        ]);

        $data = [
            'prodtranslation_name' => uniqid(),
        ];

        $response = $this->call('POST', '/dashboard/prodtranslation/update/' . $prodTranslation->id, $data);

        $response->assertStatus(200)
                 ->assertJson([
                     'failed' => false,
                 ]);
    }

    /** @test */
    public function can_not_update_translation_if_translation_is_incorrect()
    {
        $language = factory(\App\Language::class)->create();
        $product = factory(\App\Product::class)->create();

        $prodTranslation = factory(\App\Prodtranslation::class)->create([
            'product_id' => $product->id,
            'lang_id' => $language->id,
            'name' => 'test',
        ]);

        $data = [
            'prodtranslation_name' => uniqid(),
        ];

        $response = $this->call('POST', '/dashboard/prodtranslation/update/' . $prodTranslation->id + 10, $data);

        $response->assertStatus(404);
    }

    /** @test */
    public function check_validation()
    {
        $language = factory(\App\Language::class)->create();
        $product = factory(\App\Product::class)->create();

        $prodTranslation = factory(\App\Prodtranslation::class)->create([
            'product_id' => $product->id,
            'lang_id' => $language->id,
        ]);

        $data = [
            'prodtranslation_name' => '',
        ];

        $response = $this->call('POST', '/dashboard/prodtranslation/update/' . $prodTranslation->id, $data);

        $response->assertStatus(200)
                 ->assertJson([
                     'failed' => true,
                     'errors' => "Please ensure that you provide a valid prodtranslation name"
                 ]);
    }

    /** @test */
    public function can_delete_translation()
    {
        $prodTranslation = factory(\App\Prodtranslation::class)->create([
            'product_id' => factory(\App\Product::class)->create()->id,
            'lang_id' => factory(\App\Language::class)->create()->id,
            'name' => uniqid(),
        ]);

        $response = $this->get('/dashboard/prodtranslation/delete/' . $prodTranslation->id);

        $response->assertStatus(200)
                 ->assertJson([
                     'failed' => false
                 ]);

        $this->assertDatabaseMissing('product_translation', [
            'product_id' => $prodTranslation->product_id,
            'lang_id' => $prodTranslation->lang_id,
        ]);
    }

    /** @test */
    public function can_not_delete_translation_if_translation_is_incorrect()
    {
        $prodTranslation = factory(\App\Prodtranslation::class)->create([
            'product_id' => factory(\App\Product::class)->create()->id,
            'lang_id' => factory(\App\Language::class)->create()->id,
            'name' => uniqid(),
        ]);

        $response = $this->get('/dashboard/prodtranslation/delete/' . $prodTranslation->id + 1);

        $response->assertStatus(404);

        $this->assertDatabaseHas('product_translation', [
            'product_id' => $prodTranslation->product_id,
            'lang_id' => $prodTranslation->lang_id,
        ]);
    }
}
