<?php

namespace Tests\Unit;

use App\Admin;
use App\Carrier;
use App\Language;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CarrierTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    protected $user;

    public function setUp()
    {
        parent::setUp();

        $user = Admin::where('email', 'info@bluloyalty.com')->first();
        $this->user = $user;
        $this->actingAs($user)
             ->session([
                 'user_roles' => [
                     'BLU Admin',
                 ],
             ]);
    }

    /** @test */
    public function can_create_new_carrier_page()
    {
        $response = $this->get('/dashboard/carriers/new');

        $response->assertStatus(200)
                 ->assertViewIs('carrier.new');
    }

    /** @test */
    public function can_open_edit_carrier()
    {
        $carrier = factory(\App\Carrier::class)->create();

        $response = $this->get('/dashboard/carriers/view/' . $carrier->id);

        $response->assertStatus(200)
                 ->assertViewIs('carrier.view');
    }

    /** @test */
    public function can_not_open_carrier_page_which_is_not_exists()
    {
        $carrier = factory(\App\Carrier::class)->create();

        $response = $this->get('/dashboard/carriers/view/' . ($carrier->id + 10));

        $response->assertStatus(404);
    }

    /** @test */
    public function can_update_carrier()
    {
        $carrier = factory(\App\Carrier::class)->create();

        $data = [
            'name' => uniqid(),
            'country' => uniqid()
        ];

        $response = $this->post('/dashboard/carriers/update/' . $carrier->id, $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/carriers');

        $this->assertDatabaseHas('carriers', [
            'id' => $carrier->id,
            'name' => $data['name'],
            'country' => $data['country'],
        ]);
    }

    /** @test */
    public function can_not_update_carrier_which_is_not_exists()
    {
        $carrier = factory(\App\Carrier::class)->create();

        $data = [
            'name' => uniqid(),
            'country' => uniqid()
        ];

        $response = $this->post('/dashboard/carriers/update/' . ($carrier->id + 10), $data);

        $response->assertStatus(404);

        $this->assertDatabaseMissing('carriers', [
            'id' => $carrier->id,
            'name' => $data['name'],
            'country' => $data['country'],
        ]);
    }

    /** @test */
    public function can_not_update_carrier_without_required_fields()
    {
        $carrier = factory(\App\Carrier::class)->create();

        $data = [];
        $response = $this->post('/dashboard/carriers/update/' . $carrier->id, $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/carriers/view/' . $carrier->id)
                 ->assertSessionHasErrors(['name', 'country']);
    }

    /** @test */
    public function can_delete_carrier_with()
    {
        $carrier = factory(\App\Carrier::class)->create();

        $response = $this->get('/dashboard/carriers/delete/' . $carrier->id);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/carriers');

        $this->assertDatabaseMissing('carriers', [
            'id' => $carrier->id,
        ]);
    }

    /** @test */
    public function can_not_delete_carrier_which_is_not_exists()
    {
        $carrier = factory(\App\Carrier::class)->create();

        $response = $this->get('/dashboard/carriers/delete/' . ($carrier->id + 10));

        $response->assertStatus(404);

        $this->assertDatabaseHas('carriers', [
            'id' => $carrier->id,
        ]);
    }

    /** @test */
    public function can_open_carriers_page()
    {
        factory(\App\Carrier::class)->create();

        $response = $this->get('/dashboard/carriers');

        $response->assertStatus(200)
                 ->assertViewIs('carrier.list')
                 ->assertViewHas('carriers');
    }

    /** @test */
    public function can_filter_carriers_by_search_query()
    {
        $carrier = factory(\App\Carrier::class)->create();

        $response = $this->get('/dashboard/carriers?q=' . uniqid());

        $response->assertStatus(200)
                 ->assertViewIs('carrier.list')
                 ->assertDontSee($carrier->name);

        $response = $this->get('/dashboard/carriers?q=' . $carrier->name);

        $response->assertStatus(200)
                 ->assertViewIs('carrier.list')
                 ->assertSee($carrier->name);
    }

    /** @test */
    public function can_view_pagination_page()
    {
        $perPage = 15;
        $pageCount = ceil(Carrier::count() / $perPage);

        if ($pageCount > 1) {
            $response = $this->get('/dashboard/carriers?page=' . $pageCount);

            $response->assertStatus(200)
                     ->assertViewIs('carrier.list')
                     ->assertSee('carriers?page=1')
                     ->assertDontSee('No Carriers Found');
        }
    }

    /** @test */
    public function can_not_view_pagination_page_if_one_page()
    {
        $perPage = 15;
        $page = ceil(Carrier::count() / $perPage) + 1;

        $response = $this->get('/dashboard/carriers?page=' . $page);

        $response->assertStatus(200)
                 ->assertViewIs('carrier.list')
                 ->assertDontSee('carriers?page=' . $page)
                 ->assertSee('No Carriers Found');
    }
}
