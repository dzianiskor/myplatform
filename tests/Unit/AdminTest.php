<?php

namespace Tests\Unit;

use App\Admin;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AdminTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    protected $user;

    public function setUp()
    {
        parent::setUp();

        $user = Admin::where('email', 'info@bluloyalty.com')->first();
        $this->user = $user;
        $this->actingAs($user)
             ->session([
                 'user_roles' => [
                     'BLU Admin',
                 ],
                 'user_role_ids' => [
                     1,
                 ],
                 'user_permissions' => [
                     'create_admins',
                     'view_admins',
                     'edit_admins',
                     'delete_admins',
                     'view_roles',
                 ],
             ]);
    }

    /** @test */
    public function can_create_new_admin_page_with_permissions()
    {
        $response = $this->get('/dashboard/admins/new');

        $response->assertStatus(302)
                 ->assertRedirect()
                 ->assertSeeText('/dashboard/admins/view/');
    }

    /** @test */
    public function can_not_create_new_admin_page_without_permissions()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $response = $this->get('/dashboard/admins/new');

        $response->assertStatus(302)
                 ->assertRedirect()
                 ->assertSeeText('/dashboard');
    }

    /** @test */
    public function can_open_edit_admin()
    {
        $admin = factory(\App\Admin::class)->create();
        $admin->each(function ($a) {
            $a->partners()->save((\App\Partner::find(1))->first());
        });

        $response = $this->get('/dashboard/admins/view/' . base64_encode($admin->id));

        $response->assertStatus(200)
                 ->assertViewIs('admin.view');
    }

    /** @test */
    public function can_not_open_admin_page_which_is_not_exists()
    {
        $admin = factory(\App\Admin::class)->create();
        $admin->each(function ($a) {
            $a->partners()->save((\App\Partner::find(1))->first());
        });

        $response = $this->get('/dashboard/admins/view/' . base64_encode($admin->id + 10));

        $response->assertStatus(404);
    }

    /** @test */
    public function can_not_open_admin_page_if_role_is_not_blu_admin()
    {
        $role = factory(\App\Role::class)->create();

        $this->session([
            'user_roles' => ['test'],
            'user_role_ids' => [$role->id],
            'user_permissions' => [
                'create_admins',
                'view_admins',
                'edit_admins',
                'delete_admins',
                'view_roles',
            ],
        ]);

        $admin = factory(\App\Admin::class)->create();
        $admin->each(function ($a) use ($role) {
            $a->roles()->save($role);
        });

        $response = $this->get('/dashboard/admins/view/' . base64_encode($admin->id));

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard');
    }

    /** @test */
    public function can_update_admin_with_permission()
    {
        $admin = factory(\App\Admin::class)->create();
        $admin->each(function ($a) {
            $a->partners()->save((\App\Partner::find(1))->first());
        });

        $stub = __DIR__ . '\\..\\stubs\\test.png';
        $name = str_random(8) . '.png';
        $path = sys_get_temp_dir() . '\\' . $name;

        copy($stub, $path);

        $file = new UploadedFile($path, $name, filesize($path), 'image/png', null, true);

        $data = [
            'first_name' => uniqid(),
            'last_name' => uniqid(),
        ];

        $response = $this->call('POST', '/dashboard/admins/update/' . base64_encode($admin->id), $data, [], ['image' => $file], ['Accept' => 'application/json']);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/admins');

        $this->assertDatabaseHas('admins', [
            'id' => $admin->id,
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
        ]);

        $admin1 = \App\Admin::find(['id' => $admin->id])->first();

        $this->assertFileExists(public_path(\App\Http\Controllers\MediaController::getImageFromId($admin1->profile_image)));

        @unlink(public_path(\App\Http\Controllers\MediaController::getImageFromId($admin1->profile_image)));
    }

    /** @test */
    public function can_not_update_admin_without_permission()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $admin = factory(\App\Admin::class)->create();
        $admin->each(function ($a) {
            $a->partners()->save((\App\Partner::find(1))->first());
        });

        $data = [
            'first_name' => uniqid(),
            'last_name' => uniqid(),
        ];

        $response = $this->post('/dashboard/admins/update/' . base64_encode($admin->id), $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard');

        $this->assertDatabaseMissing('admins', [
            'id' => $admin->id,
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
        ]);
    }

    /** @test */
    public function can_not_update_admin_which_is_not_exists()
    {
        $admin = factory(\App\Admin::class)->create();
        $admin->each(function ($a) {
            $a->partners()->save((\App\Partner::find(1))->first());
        });

        $data = [
            'first_name' => uniqid(),
            'last_name' => uniqid(),
        ];

        $response = $this->post('/dashboard/admins/update/' . base64_encode($admin->id + 10), $data);

        $response->assertStatus(404);

        $this->assertDatabaseMissing('admins', [
            'id' => $admin->id,
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
        ]);
    }

    /** @test */
    public function can_not_update_admin_without_required_fields()
    {
        $admin = factory(\App\Admin::class)->create();
        $admin->each(function ($a) {
            $a->partners()->save((\App\Partner::find(1))->first());
        });

        $data = [];
        $response = $this->post('/dashboard/admins/update/' . base64_encode($admin->id), $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/admins/view/' . base64_encode($admin->id))
                 ->assertSessionHasErrors(['first_name', 'last_name']);

        $data = [
            'first_name' => 'q',
            'last_name' => 'w',
        ];
        $response = $this->post('/dashboard/admins/update/' . base64_encode($admin->id), $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/admins/view/' . base64_encode($admin->id))
                 ->assertSessionHasErrors(['first_name', 'last_name']);
    }

    /** @test */
    public function can_open_admins_page_with_permission()
    {
        $admin = factory(\App\Admin::class)->create();
        $admin->each(function ($a) {
            $a->partners()->save((\App\Partner::find(1))->first());
        });

        $response = $this->get('/dashboard/admins');

        $response->assertStatus(200)
                 ->assertViewIs('admin.list')
                 ->assertViewHas('admins');
    }

    /** @test */
    public function can_not_open_admins_page_without_permission()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $admin = factory(\App\Admin::class)->create();
        $admin->each(function ($a) {
            $a->partners()->save((\App\Partner::find(1))->first());
        });

        $response = $this->get('/dashboard/admins');

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard');
    }

    /** @test */
    public function can_delete_admin_with_permission()
    {
        $admin = factory(\App\Admin::class)->create();
        $admin->each(function ($a) {
            $a->partners()->save((\App\Partner::find(1))->first());
        });

        $response = $this->get('/dashboard/admins/delete/' . base64_encode($admin->id));

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/admins');

        $this->assertDatabaseMissing('admins', [
            'id' => $admin->id,
            'deleted_at' => null
        ]);
    }

    /** @test */
    public function can_not_delete_admin_without_permission()
    {
        $this->session(['user_roles' => ['BLU Admin'], 'user_permissions' => []]);

        $admin = factory(\App\Admin::class)->create();
        $admin->each(function ($a) {
            $a->partners()->save((\App\Partner::find(1))->first());
        });

        $response = $this->get('/dashboard/admins/delete/' . base64_encode($admin->id));

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard');

        $this->assertDatabaseHas('admins', [
            'id' => $admin->id,
            'deleted_at' => null
        ]);
    }

    /** @test */
    public function can_not_delete_admin_if_role_is_not_blu_admin()
    {
        $this->session([
            'user_roles' => [],
            'user_role_ids' => [],
            'user_permissions' => [
                'create_admins',
                'view_admins',
                'edit_admins',
                'delete_admins',
                'view_roles',
            ],
        ]);

        $admin = factory(\App\Admin::class)->create();
        $admin->each(function ($a) {
            $a->partners()->save((\App\Partner::find(1))->first());
        });

        $response = $this->get('/dashboard/admins/delete/' . base64_encode($admin->id));

        $response->assertStatus(400);

        $this->assertDatabaseHas('admins', [
            'id' => $admin->id,
            'deleted_at' => null
        ]);
    }

    /** @test */
    public function can_not_delete_admin_which_is_not_exists()
    {
        $admin = factory(\App\Admin::class)->create();
        $admin->each(function ($a) {
            $a->partners()->save((\App\Partner::find(1))->first());
        });

        $response = $this->get('/dashboard/admins/delete/' . base64_encode($admin->id + 10));

        $response->assertStatus(404);

        $this->assertDatabaseHas('admins', [
            'id' => $admin->id,
            'deleted_at' => null
        ]);
    }

    /** @test */
    public function can_attach_type_to_admin()
    {
        $admin = factory(\App\Admin::class)->create();
        $admin->each(function ($a) {
            $a->partners()->save((\App\Partner::find(1))->first());
        });

        $role = factory(\App\Role::class)->create();

        $response = $this->post('/dashboard/roles/attachadmin/' . base64_encode($admin->id), ['role_id' => $role->id]);

        $response->assertStatus(200)
                 ->assertSeeText($role->name);

        $this->assertDatabaseHas('role_admin', [
            'role_id' => $role->id,
            'admin_id' => $admin->id,
        ]);
    }

    /** @test */
    public function can_not_attach_duplicate_type_to_admin()
    {
        $admin = factory(\App\Admin::class)->create();
        $role = factory(\App\Role::class)->create();
        $admin->each(function ($a) use ($role) {
            $a->partners()->save((\App\Partner::find(1))->first());
            $a->roles()->save($role);
        });

        $this->post('/dashboard/roles/attachadmin/' . base64_encode($admin->id), ['role_id' => $role->id]);

        $this->assertEquals(1, $admin->roles()->count());
    }

    /** @test */
    public function can_not_attach_type_to_admin_which_is_not_exists()
    {
        $admin = factory(\App\Admin::class)->create();
        $admin->each(function ($a) {
            $a->partners()->save((\App\Partner::find(1))->first());
        });

        $role = factory(\App\Role::class)->create();

        $response = $this->post('/dashboard/roles/attachadmin/' . base64_encode($admin->id + 10), ['role_id' => $role->id]);

        $response->assertStatus(404);

        $this->assertDatabaseMissing('role_admin', [
            'role_id' => $role->id,
            'admin_id' => $admin->id,
        ]);
    }

    /** @test */
    public function can_not_attach_type_which_is_not_exists_to_admin()
    {
        $admin = factory(\App\Admin::class)->create();
        $admin->each(function ($a) {
            $a->partners()->save((\App\Partner::find(1))->first());
        });

        $role = factory(\App\Role::class)->create();

        $response = $this->post('/dashboard/roles/attachadmin/' . base64_encode($admin->id), ['role_id' => $role->id + 10]);

        $response->assertStatus(404);

        $this->assertDatabaseMissing('role_admin', [
            'role_id' => $role->id,
            'admin_id' => $admin->id,
        ]);
    }

    /** @test */
    public function can_unlink_type()
    {
        $admin = factory(\App\Admin::class)->create();
        $admin->each(function ($a) {
            $a->partners()->save((\App\Partner::find(1))->first());
            $a->roles()->save(factory(\App\Role::class)->create());
        });

        $role = $admin->roles->first();

        $response = $this->get('/dashboard/roles/unlinkadmin/' . base64_encode($admin->id) . '/' . base64_encode($role->id));

        $response->assertStatus(200)
                 ->assertDontSeeText($role->name);

        $this->assertDatabaseMissing('role_admin', [
            'role_id' => $role->id,
            'admin_id' => $admin->id,
        ]);
    }

    /** @test */
    public function can_attach_partner_to_admin()
    {
        $admin = factory(\App\Admin::class)->create();
        $admin->each(function ($a) {
            $a->partners()->save((\App\Partner::find(1))->first());
        });

        $partner = factory(\App\Partner::class)->create();

        $response = $this->post('/dashboard/partners/attachadmin/' . base64_encode($admin->id), ['partner_id' => $partner->id]);

        $response->assertStatus(200)
                 ->assertViewIs('partner.adminlist')
                 ->assertSeeText($partner->name);

        $this->assertDatabaseHas('partner_admin', [
            'partner_id' => $partner->id,
            'admin_id' => $admin->id,
        ]);
    }

    /** @test */
    public function can_not_attach_duplicate_partner_to_admin()
    {
        $admin = factory(\App\Admin::class)->create();
        $admin->each(function ($a) {
            $a->partners()->save((\App\Partner::find(1))->first());
        });

        $this->post('/dashboard/partners/attachadmin/' . base64_encode($admin->id), ['partner_id' => 1]);

        $this->assertEquals(1, $admin->partners()->count());
    }

    /** @test */
    public function can_not_attach_partner_to_admin_which_is_not_exists()
    {
        $admin = factory(\App\Admin::class)->create();
        $admin->each(function ($a) {
            $a->partners()->save((\App\Partner::find(1))->first());
        });

        $partner = factory(\App\Partner::class)->create();

        $response = $this->post('/dashboard/partners/attachadmin/' . base64_encode($admin->id + 10), ['partner_id' => $partner->id]);

        $response->assertStatus(404);

        $this->assertDatabaseMissing('partner_admin', [
            'partner_id' => $partner->id,
            'admin_id' => $admin->id,
        ]);
    }

    /** @test */
    public function can_not_attach_partner_which_is_not_exists_to_admin()
    {
        $admin = factory(\App\Admin::class)->create();
        $admin->each(function ($a) {
            $a->partners()->save((\App\Partner::find(1))->first());
        });

        $partner = factory(\App\Partner::class)->create();

        $response = $this->post('/dashboard/partners/attachadmin/' . base64_encode($admin->id), ['partner_id' => $partner->id + 10]);

        $response->assertStatus(404);

        $this->assertDatabaseMissing('partner_admin', [
            'partner_id' => $partner->id,
            'admin_id' => $admin->id,
        ]);
    }

    /** @test */
    public function can_detach_partner()
    {
        $admin = factory(\App\Admin::class)->create();
        $admin->each(function ($a) {
            $a->partners()->save((\App\Partner::find(1))->first());
        });

        $partner = $admin->partners->first();

        $response = $this->get('/dashboard/partners/unlinkadmin/' . base64_encode($admin->id) . '/' . base64_encode($partner->id));

        $response->assertStatus(200)
                 ->assertDontSeeText($partner->name);

        $this->assertDatabaseMissing('partner_admin', [
            'partner_id' => $partner->id,
            'admin_id' => $admin->id,
        ]);
    }

    /** @test */
    public function can_filter_admins_by_search_query()
    {
        $admin = factory(\App\Admin::class)->create();
        $admin->each(function ($a) {
            $a->partners()->save((\App\Partner::find(1))->first());
        });

        $response = $this->get('/dashboard/admins?q=' . uniqid());

        $response->assertStatus(200)
                 ->assertViewIs('admin.list')
                 ->assertDontSee($admin->last_name);

        $response = $this->get('/dashboard/admins?q=' . $admin->last_name);

        $response->assertStatus(200)
                 ->assertViewIs('admin.list')
                 ->assertSee($admin->last_name);
    }

    /** @test */
    public function can_view_pagination_page()
    {
        $perPage = 15;
        $pageCount = ceil(Admin::where('draft', 0)->count() / $perPage);

        if ($pageCount > 1) {
            $response = $this->get('/dashboard/admins?page=' . $pageCount);

            $response->assertStatus(200)
                     ->assertViewIs('admin.list')
                     ->assertSee('admins?page=1')
                     ->assertDontSee('No Members Found');
        }
    }

    /** @test */
    public function can_not_view_pagination_page_if_one_page()
    {
        $perPage = 15;
        $page = ceil(Admin::where('draft', 0)->count() / $perPage) + 1;

        $response = $this->get('/dashboard/admins?page=' . $page);

        $response->assertStatus(200)
                 ->assertViewIs('admin.list')
                 ->assertDontSee('admins?page=' . $page)
                 ->assertSee('No Members Found');
    }
}
