<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;
use App\Admin;
use App\Network;

class NetworkTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    protected $user;

    public function setUp()
    {
        parent::setUp();

        $user = Admin::where('email', 'info@bluloyalty.com')->first();
        $this->user = $user;
        $this->actingAs($user)
             ->session([
                 'user_roles' => [
                     'BLU Admin',
                 ],
                 'user_permissions' => [
                     'view_network',
                     'create_network'
                 ],
             ]);
    }

    /** @test */
    public function can_view_network_list_page_with_permission_and_blu_admin_role()
    {
        $response = $this->get('/dashboard/network');

        $response->assertStatus(200);
    }

    /** @test */
    public function can_view_network_list_page_with_permission_and_with_non_blu_admin_role()
    {
        $this->session(['user_roles' => ['Non Admin Role']]);
        $response = $this->get('/dashboard/network');

        $response->assertStatus(200);
    }

    /** @test */
    public function can_not_view_network_list_page_without_permission()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $response = $this->get('/dashboard/network');

        $response->assertStatus(302)
                 ->assertRedirect()
                 ->assertRedirect('/dashboard');
    }

    /** @test */
    public function can_create_new_network_with_permission_or_role()
    {
        $response = $this->get('/dashboard/network/new');

        $response->assertStatus(302)
                 ->assertRedirect()
                 ->assertSeeText('/dashboard/network/view/');
    }

    /** @test */
    public function can_not_create_new_network_without_permission_or_role()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $response = $this->get('/dashboard/network/new');

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard');
    }

    /** @test */
    public function can_blu_admin_or_admin_with_permission_view_network()
    {
        $network = factory(Network::class)->create();
        $response = $this->get('/dashboard/network/view/' . base64_encode($network->id));

        $response->assertStatus(200)
                 ->assertViewIs('network.view');
    }

    /** @test */
    public function can_not_not_blu_admin_or_without_permission_view_network()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $network = factory(Network::class)->create();

        $response = $this->get('/dashboard/network/view/' . base64_encode($network->id));

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard');
    }

    /** @test */
    public function can_not_view_network_which_does_not_exist()
    {
        $network = factory(Network::class)->create();
        $response = $this->get('/dashboard/network/view/' . base64_encode($network->id + 10));

        $response->assertStatus(404);
    }

    /** @test */
    public function can_not_view_draft_network_without_role()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $network = factory(Network::class)->create();
        $network->draft = false;

        $response = $this->get('/dashboard/network/view/' . base64_encode($network->id));

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard');
    }

    /** @test */
    public function can_not_update_network_which_does_not_exist()
    {
        $network = factory(Network::class)->create();
        $data = [];

        $response = $this->call('POST', '/dashboard/network/update/' . base64_encode($network->id + 10), $data);

        $response->assertStatus(404);
    }

    /** @test */
    public function can_validate_errors()
    {
        $network = factory(\App\Network::class)->create();
        $country = factory(\App\Country::class)->create();
        $area = factory(\App\Area::class)->create([
            'country_id' => $country->id,
        ]);
        $city = factory(\App\City::class)->create([
            'area_id' => $area->id,
        ]);

        $data = ['name' => ""];
        $response = $this->post('/dashboard/network/update/' . base64_encode($network->id), $data);
        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/network/view/' . base64_encode($network->id))
                 ->assertSessionHasErrors('name');

        $data = ['currency' => ""];
        $response = $this->post('/dashboard/network/update/' . base64_encode($network->id), $data);
        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/network/view/' . base64_encode($network->id))
                 ->assertSessionHasErrors('currency');

        $data = ['affiliate_name' => ""];
        $response = $this->post('/dashboard/network/update/' . base64_encode($network->id), $data);
        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/network/view/' . base64_encode($network->id))
                 ->assertSessionHasErrors('affiliate_name');


        $data = [
            'affiliate_country' => $country->id + 10
        ];
        $response = $this->post('/dashboard/network/update/' . base64_encode($network->id), $data);
        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/network/view/' . base64_encode($network->id))
                 ->assertSessionHasErrors('affiliate_country');

        $data = [
            'affiliate_city' => $city->id + 10
        ];
        $response = $this->post('/dashboard/network/update/' . base64_encode($network->id), $data);
        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/network/view/' . base64_encode($network->id))
                 ->assertSessionHasErrors('affiliate_city');

        $data = [
            'affiliate_area' => $area->id + 10,
        ];
        $response = $this->post('/dashboard/network/update/' . base64_encode($network->id), $data);
        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/network/view/' . base64_encode($network->id))
                 ->assertSessionHasErrors('affiliate_area');

        $data = [
            'value_in_currency' => "qwe",
        ];
        $response = $this->post('/dashboard/network/update/' . base64_encode($network->id), $data);
        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/network/view/' . base64_encode($network->id))
                 ->assertSessionHasErrors('value_in_currency');

        $data = [
            'value_in_currency' => "-1",
        ];
        $response = $this->post('/dashboard/network/update/' . base64_encode($network->id), $data);
        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/network/view/' . base64_encode($network->id))
                 ->assertSessionHasErrors('value_in_currency');
    }

    /** @test */
    public function can_not_update_network_without_permission()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $data = [
            'currency' => 'Test Currency',
            'currencies' => 6,
            'draft' => false,
            'name' => "Test Updated Network",
            'affiliate_name' => "Test Aff. name",
            'affiliate_country' => 9,
            'affiliate_area' => 76,
            'affiliate_city' => 4138,
            'value_in_currency' => 200
        ];

        $network = factory(Network::class)->create();
        $response = $this->call('POST', '/dashboard/network/update/' . base64_encode($network->id), $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard');
    }

    /** @test */
    public function can_update_network_with_permission()
    {
        $network = factory(Network::class)->create();

        $stub = __DIR__ . '\\..\\stubs\\test.png';
        $name = str_random(8) . '.png';
        $path = sys_get_temp_dir() . '\\' . $name;

        copy($stub, $path);

        $file = new UploadedFile($path, $name, filesize($path), 'image/png', null, true);

        $data = [
            'currency' => 'Test Currency',
            'currencies' => 6,
            'draft' => false,
            'name' => "Test Updated Network",
            'affiliate_name' => "Test Aff. name",
            'affiliate_country' => 9,
            'affiliate_area' => 76,
            'affiliate_city' => 4138,
            'value_in_currency' => 200
        ];

        $response = $this->call('POST', '/dashboard/network/update/' . base64_encode($network->id), $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/network');

        $this->assertDatabaseHas('network', [
            'id' => $network->id,
            'draft' => $data['draft'],
            'name' => $data['name'],
            'affiliate_name' => $data['affiliate_name'],
        ]);

        $checkNetwork = Network::find(['id' => $network->id])->first();

        $this->assertFileExists(public_path(\App\Http\Controllers\MediaController::getImageFromId($checkNetwork->image)));

        @unlink(public_path(\App\Http\Controllers\MediaController::getImageFromId($checkNetwork->image)));
    }

    /** @test */
    public function can_delete_network_with_permission()
    {
        $network = factory(Network::class)->create();
        $response = $this->get('/dashboard/network/delete/' . base64_encode($network->id));

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/network');

        $this->assertDatabaseMissing('network', [
            'id' => $network->id
        ]);
    }

    /** @test */
    public function can_not_delete_network_without_permission()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $network = factory(Network::class)->create();

        $response = $this->get('/dashboard/network/delete/' . base64_encode($network->id));

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard');
    }

    /** @test */
    public function can_not_delete_network_which_does_not_exist()
    {
        $network = factory(Network::class)->create();
        $response = $this->get('/dashboard/network/delete/' . base64_encode($network->id + 10));

        $response->assertStatus(404);
    }

    /** @test */
    public function can_filter_networks_by_search_filer()
    {
        $network = factory(\App\Network::class)->create();
        $response = $this->get('/dashboard/network?q=' . uniqid());

        $response->assertStatus(200)
                 ->assertViewIs('network.list')
                 ->assertDontSee($network->name);

        $response = $this->get('/dashboard/network?q=' . $network->name);

        $response->assertStatus(200)
                 ->assertViewIs('network.list')
                 ->assertSee($network->name);
    }

    /** @test */
    public function can_view_networks_paginated_list_page()
    {
        $result = Network::where('draft', false)->count();
        $response = $this->get('/dashboard/network?page=1');

        if ($result > 0) {
            $response->assertStatus(200)
                     ->assertViewIs('network.list')
                     ->assertDontSeeText('No Networks Found');
        } else {
            $response->assertStatus(200)
                     ->assertViewIs('network.list')
                     ->assertSeeText('No Networks Found');
        }
    }

    /** @test */
    public function can_not_view_networks_paginated_list_page()
    {
        $result = Network::where('draft', false)->count();
        $page = round($result / 15, 0, PHP_ROUND_HALF_UP) + 1;

        $response = $this->get('/dashboard/network?page=' . $page);

        $response->assertStatus(200)
                 ->assertViewIs('network.list')
                 ->assertDontSee('network?page=' . $page)
                 ->assertSeeText('No Networks Found');
    }
}
