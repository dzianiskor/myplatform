<?php

namespace Tests\Unit;

use App\Admin;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PartnerTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    protected $user;

    public function setUp()
    {
        parent::setUp();

        $user = Admin::where('email', 'info@bluloyalty.com')->first();
        $this->user = $user;
        $this->actingAs($user)
             ->session([
                 'user_roles' => [
                     'BLU Admin',
                 ],
                 'user_permissions' => [
                     'create_partners',
                     'view_partners',
                     'edit_partners',
                     'delete_partners',
                 ],
             ]);
    }

    /** @test */
    public function can_open_view_partner_list_page_with_permission()
    {
        factory(\App\Partner::class)->create();

        $response = $this->get('/dashboard/partners');

        $response->assertStatus(200)
                 ->assertViewIs('partner.list')
                 ->assertViewHas('partners');
    }

    /** @test */
    public function can_not_view_partner_list_page_without_permission()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $response = $this->get('/dashboard/partners');

        $response->assertStatus(302)
                 ->assertRedirect()
                 ->assertRedirect('/dashboard');
    }

    /** @test */
    public function can_create_new_partner_with_permission()
    {
        $response = $this->get('/dashboard/partners/new');

        $response->assertStatus(302)
                 ->assertRedirect()
                 ->assertSeeText('/dashboard/partners/view/');
    }

    /** @test */
    public function can_not_create_new_partner_without_permission()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $response = $this->get('/dashboard/partners/new');

        $response->assertStatus(302)
                 ->assertRedirect()
                 ->assertSeeText('/dashboard');
    }

    /** @test */
    public function can_view_partner_with_permission()
    {
        $partner = factory(\App\Partner::class)->create();

        $response = $this->get('/dashboard/partners/view/' . base64_encode($partner->id));

        $response->assertStatus(200)
                 ->assertViewIs('partner.view');
    }

    /** @test */
    public function can_not_view_partner_without_permission()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $partner = factory(\App\Partner::class)->create();

        $response = $this->get('/dashboard/partners/view/' . base64_encode($partner->id));

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard');
    }

    /** @test */
    public function can_not_view_partner_which_does_not_exist()
    {
        $partner = factory(\App\Partner::class)->create();

        $response = $this->get('/dashboard/partners/view/' . base64_encode($partner->id + 10));

        $response->assertStatus(404);
    }


    /** @test */
    public function can_update_partner_with_permission()
    {
        $partner = factory(\App\Partner::class)->create();
        $partnerCategory = factory(\App\PartnerCategory::class)->create();

        $data = [
            'name' => uniqid(),
            'category_id' => $partnerCategory->id,
        ];

        $response = $this->post('/dashboard/partners/update/' . base64_encode($partner->id), $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/partners');

        $this->assertDatabaseHas('partner', [
            'id' => $partner->id,
            'name' => $data['name'],
            'category_id' => $data['category_id'],
        ]);
    }

    /** @test */
    public function can_not_update_partner_without_permission()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $partner = factory(\App\Partner::class)->create();
        $partnerCategory = factory(\App\PartnerCategory::class)->create();

        $data = [
            'name' => uniqid(),
            'category_id' => $partnerCategory->id,
        ];

        $response = $this->post('/dashboard/partners/update/' . base64_encode($partner->id), $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard');

        $this->assertDatabaseMissing('partner', [
            'id' => $partner->id,
            'name' => $data['name'],
            'category_id' => $data['category_id'],
        ]);
    }

    /** @test */
    public function can_not_update_partner_which_does_not_exist()
    {
        $partner = factory(\App\Partner::class)->create();

        $data = [
            'name' => uniqid(),
            'category_id' => uniqid(),
        ];

        $response = $this->post('/dashboard/partners/update/' . base64_encode($partner->id + 10), $data);

        $response->assertStatus(404);

        $this->assertDatabaseMissing('partner', [
            'id' => $partner->id,
            'name' => $data['name'],
            'category_id' => $data['category_id'],
        ]);
    }

    /** @test */
    public function can_validate_errors_when_update_partner()
    {
        $partner = factory(\App\Partner::class)->create();
        $partnerCategory = factory(\App\PartnerCategory::class)->create();

        $data = [
            'name' => '',
            'category_id' => $partnerCategory->id,
        ];

        $response = $this->post('/dashboard/partners/update/' . base64_encode($partner->id), $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/partners/view/' . base64_encode($partner->id))
                 ->assertSessionHasErrors('name');

        $data = ['name' => "I"];
        $response = $this->post('/dashboard/partners/update/' . base64_encode($partner->id), $data);
        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/partners/view/' . base64_encode($partner->id))
                 ->assertSessionHasErrors('name');

        $data = ['category_id' => ''];
        $response = $this->post('/dashboard/partners/update/' . base64_encode($partner->id), $data);
        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/partners/view/' . base64_encode($partner->id))
                 ->assertSessionHasErrors('category_id');

        $data = ['category_id' => $partnerCategory->id + 10];
        $response = $this->post('/dashboard/partners/update/' . base64_encode($partner->id), $data);
        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/partners/view/' . base64_encode($partner->id))
                 ->assertSessionHasErrors('category_id');
    }

    /** @test */
    public function can_delete_partner_with_permission()
    {
        $partner = factory(\App\Partner::class)->create();
        $response = $this->get('/dashboard/partners/delete/' . base64_encode($partner->id));

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/partners');

        $this->assertDatabaseMissing('partner', [
            'id' => $partner->id,
        ]);
    }

    /** @test */
    public function can_not_delete_partner_without_permission()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $partner = factory(\App\Partner::class)->create();
        $response = $this->get('/dashboard/partners/delete/' . base64_encode($partner->id));

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard');

        $this->assertDatabaseHas('partner', [
            'id' => $partner->id
        ]);
    }

    /** @test */
    public function can_not_delete_partner_which_is_not_exists()
    {
        $partner = factory(\App\Partner::class)->create();

        $data = [
            'name' => uniqid(),
            'category_id' => uniqid(),
        ];

        $response = $this->get('/dashboard/partners/delete/' . base64_encode($partner->id + 10), $data);

        $response->assertStatus(404);

        $this->assertDatabaseMissing('partner', [
            'id' => $partner->id,
            'name' => $data['name'],
            'category_id' => $data['category_id'],
        ]);

        $this->assertDatabaseHas('partner', [
            'id' => $partner->id
        ]);
    }

    /** @test */
    public function can_attach_network_to_partner()
    {
        $partner = factory(\App\Partner::class)->create();
        $network = factory(\App\Network::class)->create();

        $response = $this->post('/dashboard/partners/attach/network/' . base64_encode($partner->id) . '?network_id=' . $network->id);

        $response->assertStatus(200)
                 ->assertSeeText($network->name);

        $this->assertDatabaseHas('network_partner', [
            'partner_id' => $partner->id,
            'network_id' => $network->id,
        ]);
    }

    /** @test */
    public function can_not_attach_network_with_partner_which_does_not_exist()
    {
        $partner = factory(\App\Partner::class)->create();
        $network = factory(\App\Network::class)->create();

        $response = $this->post('/dashboard/partners/attach/network/' . base64_encode($partner->id + 10) . '?network_id=' . $network->id);

        $response->assertStatus(404)
                 ->assertDontSeeText($network->name);

        $this->assertDatabaseMissing('network_partner', [
            'partner_id' => $partner->id,
            'network_id' => $network->id,
        ]);
    }

    /** @test */
    public function can_not_attach_network_without_partner()
    {
        $partner = factory(\App\Partner::class)->create();
        $network = factory(\App\Network::class)->create();

        $response = $this->post('/dashboard/partners/attach/network/?network_id=' . $network->id);

        $response->assertStatus(500)
                 ->assertDontSeeText($network->name);

        $this->assertDatabaseMissing('network_partner', [
            'partner_id' => $partner->id,
            'network_id' => $network->id,
        ]);
    }

    /** @test */
    public function can_not_attach_network_with_duplicate_id()
    {
        $partner = factory(\App\Partner::class)->create();
        $network = factory(\App\Network::class)->create();

        $response = $this->post('/dashboard/partners/attach/network/' . base64_encode($partner->id) . '?network_id=' . $network->id);

        $response->assertStatus(200)
                 ->assertSeeText($network->name);

        $responseDuplicate = $this->post('/dashboard/partners/attach/network/' . base64_encode($partner->id) . '?network_id=' . $network->id);

        $responseDuplicate->assertStatus(200)
                          ->assertJson([
                              'failed' => true,
                              'message' => "Network already linked to partner"
                          ]);
    }

    /** @test */
    public function can_unlink_network()
    {
        $partner = factory(\App\Partner::class)->create();
        $network = factory(\App\Network::class)->create();

        $response = $this->post('/dashboard/partners/attach/network/' . base64_encode($partner->id) . '?network_id=' . $network->id);

        $response->assertStatus(200)
                 ->assertSeeText($network->name);

        $responseUnlink = $this->get('/dashboard/partners/detach/network/' . base64_encode($partner->id) . '/' . base64_encode($network->id));

        $responseUnlink->assertStatus(200)
                       ->assertDontSeeText($network->name);

        $this->assertDatabaseMissing('network_partner', [
            'partner_id' => $partner->id,
            'network_id' => $network->id
        ]);
    }

    /** @test */
    public function can_not_unlink_network_without_partner()
    {
        $partner = factory(\App\Partner::class)->create();
        $network = factory(\App\Network::class)->create();

        $response = $this->post('/dashboard/partners/attach/network/' . base64_encode($partner->id) . '?network_id=' . $network->id);

        $response->assertStatus(200)
                 ->assertSeeText($network->name);

        $responseUnlink = $this->get('/dashboard/partners/detach/network/' . base64_encode($partner->id + 10) . '/' . base64_encode($network->id));

        $responseUnlink->assertStatus(404);

        $this->assertDatabaseHas('network_partner', [
            'partner_id' => $partner->id,
            'network_id' => $network->id
        ]);
    }

    /** @test */
    public function can_attach_to_display_channel()
    {
        $partner = factory(\App\Partner::class)->create();
        $channel = factory(\App\Partner::class)->create();

        $response = $this->post('/dashboard/partners/attach/channel/' . base64_encode($partner->id) . '?channel_id=' . $channel->id);

        $response->assertStatus(200)
                 ->assertSeeText($channel->name);

        $this->assertDatabaseHas('channel_partner', [
            'partner_id' => $partner->id,
            'channel_id' => $channel->id
        ]);
    }

    /** @test */
    public function can_not_attach_to_display_channel_without_partner_and_without_channel()
    {
        $partner = factory(\App\Partner::class)->create();
        $channel = factory(\App\Partner::class)->create();

        $response = $this->post('/dashboard/partners/attach/channel/' . base64_encode($partner->id + 10) . '?channel_id=' . $channel->id);

        $response->assertStatus(404)
                 ->assertDontSeeText($channel->name);

        $this->assertDatabaseMissing('channel_partner', [
            'partner_id' => $partner->id,
            'channel_id' => $channel->id
        ]);

        $response = $this->post('/dashboard/partners/attach/channel/' . base64_encode($partner->id) . '?channel_id=' . $channel->id + 10);

        $response->assertStatus(404)
                 ->assertDontSeeText($channel->name);

        $this->assertDatabaseMissing('channel_partner', [
            'partner_id' => $partner->id,
            'channel_id' => $channel->id
        ]);
    }

    /** @test */
    public function can_not_attach_display_channel_with_duplicate_id()
    {
        $partner = factory(\App\Partner::class)->create();
        $channel = factory(\App\Partner::class)->create();

        $response = $this->post('/dashboard/partners/attach/channel/' . base64_encode($partner->id) . '?channel_id=' . $channel->id);

        $response->assertStatus(200)
                 ->assertSeeText($channel->name);

        $this->assertDatabaseHas('channel_partner', [
            'partner_id' => $partner->id,
            'channel_id' => $channel->id
        ]);

        $duplicateResponse = $this->post('/dashboard/partners/attach/channel/' . base64_encode($partner->id) . '?channel_id=' . $channel->id);

        $duplicateResponse->assertStatus(200)
                          ->assertDontSeeText($channel->name);

        $duplicateResponse->assertJson([
            'failed' => true,
            'message' => "Channel already linked to partner"
        ]);
    }

    /** @test */
    public function can_unlink_display_channel()
    {
        $partner = factory(\App\Partner::class)->create();
        $channel = factory(\App\Partner::class)->create();

        $response = $this->post('/dashboard/partners/attach/channel/' . base64_encode($partner->id) . '?channel_id=' . $channel->id);

        $response->assertStatus(200)
                 ->assertSeeText($channel->name);

        $responseUnlink = $this->get('/dashboard/partners/detach/channel/' . base64_encode($partner->id) . '/' . base64_encode($channel->id));

        $responseUnlink->assertStatus(200)
                       ->assertDontSeeText($channel->name);

        $this->assertDatabaseMissing('channel_partner', [
            'partner_id' => $partner->id,
            'channel_id' => $channel->id
        ]);
    }

    /** @test */
    public function can_not_unlink_display_channel_without_partner()
    {
        $partner = factory(\App\Partner::class)->create();
        $channel = factory(\App\Partner::class)->create();

        $response = $this->post('/dashboard/partners/attach/channel/' . base64_encode($partner->id) . '?channel_id=' . $channel->id);

        $response->assertStatus(200)
                 ->assertSeeText($channel->name);

        $response = $this->get('/dashboard/partners/detach/channel/' . base64_encode($partner->id + 10) . '/' . base64_encode($channel->id));

        $response->assertStatus(404);

        $this->assertDatabaseHas('channel_partner', [
            'partner_id' => $partner->id,
            'channel_id' => $channel->id
        ]);
    }

    /** @test */
    public function can_attach_to_linked_partner()
    {
        $partner = factory(\App\Partner::class)->create();
        $linkedPartner = factory(\App\Partner::class)->create();

        $response = $this->post('/dashboard/partners/attach/partnerlink/' . base64_encode($partner->id) . '?partner_link_id=' . $linkedPartner->id);

        $response->assertStatus(200)
                 ->assertSeeText($linkedPartner->name);

        $this->assertDatabaseHas('partner_link', [
            'partner_id' => $partner->id,
            'partner_linked_id' => $linkedPartner->id
        ]);
    }

    /** @test */
    public function can_not_attach_to_linked_partner_if_partner_does_not_exist()
    {
        $partner = factory(\App\Partner::class)->create();
        $linkedPartner = factory(\App\Partner::class)->create();

        $response = $this->post('/dashboard/partners/attach/partnerlink/' . base64_encode($partner->id + 10) . '?partner_link_id=' . $linkedPartner->id);

        $response->assertStatus(404)
                 ->assertDontSeeText($linkedPartner->name);

        $this->assertDatabaseMissing('partner_link', [
            'partner_id' => $partner->id,
            'partner_linked_id' => $linkedPartner->id
        ]);
    }

    /** @test */
    public function can_not_attach_to_linked_partner_with_duplicate_id()
    {
        $partner = factory(\App\Partner::class)->create();
        $linkedPartner = factory(\App\Partner::class)->create();

        $response = $this->post('/dashboard/partners/attach/partnerlink/' . base64_encode($partner->id) . '?partner_link_id=' . $linkedPartner->id);

        $response->assertStatus(200)
                 ->assertSeeText($linkedPartner->name);

        $this->assertDatabaseHas('partner_link', [
            'partner_id' => $partner->id,
            'partner_linked_id' => $linkedPartner->id
        ]);

        $duplicateResponse = $this->post('/dashboard/partners/attach/partnerlink/' . base64_encode($partner->id) . '?partner_link_id=' . $linkedPartner->id);

        $duplicateResponse->assertStatus(200)
                          ->assertDontSeeText($linkedPartner->name);

        $duplicateResponse->assertJson([
            'failed' => true,
            'message' => "Linked Partner already linked to partner"
        ]);
    }

    /** @test */
    public function can_unlink_linked_partner()
    {
        $partner = factory(\App\Partner::class)->create();
        $linkedPartner = factory(\App\Partner::class)->create();

        $response = $this->post('/dashboard/partners/attach/partnerlink/' . base64_encode($partner->id) . '?partner_link_id=' . $linkedPartner->id);

        $response->assertStatus(200)
                 ->assertSeeText($linkedPartner->name);

        $responseUnlink = $this->get('/dashboard/partners/detach/partnerlink/' . base64_encode($partner->id) . '/' . base64_encode($linkedPartner->id));

        $responseUnlink->assertStatus(200)
                       ->assertDontSeeText($linkedPartner->name);

        $this->assertDatabaseMissing('partner_link', [
            'partner_id' => $partner->id,
            'partner_linked_id' => $linkedPartner->id
        ]);
    }

    /** @test */
    public function can_not_unlink_linked_partner_if_partner_does_not_exist()
    {
        $partner = factory(\App\Partner::class)->create();
        $linkedPartner = factory(\App\Partner::class)->create();

        $response = $this->post('/dashboard/partners/attach/partnerlink/' . base64_encode($partner->id) . '?partner_link_id=' . $linkedPartner->id);

        $response->assertStatus(200)
                 ->assertSeeText($linkedPartner->name);

        $response = $this->get('/dashboard/partners/detach/partnerlink/' . base64_encode($partner->id + 10) . '/' . base64_encode($linkedPartner->id));

        $response->assertStatus(404);

        $this->assertDatabaseHas('partner_link', [
            'partner_id' => $partner->id,
            'partner_linked_id' => $linkedPartner->id
        ]);
    }

    /** @test */
    public function can_attach_to_country()
    {
        $partner = factory(\App\Partner::class)->create();
        $country = factory(\App\Country::class)->create();

        $response = $this->post('/dashboard/partners/link_country/' . base64_encode($partner->id) . '?country_id=' . $country->id);

        $response->assertStatus(200)
                 ->assertSeeText($country->name);

        $this->assertDatabaseHas('country_partner', [
            'partner_id' => $partner->id,
            'country_id' => $country->id
        ]);
    }

    /** @test */
    public function can_not_attach_to_country_if_partner_or_country_do_not_exist()
    {
        $partner = factory(\App\Partner::class)->create();
        $country = factory(\App\Country::class)->create();

        $response = $this->post('/dashboard/partners/link_country/' . base64_encode($partner->id + 10) . '?country_id=' . $country->id);

        $response->assertStatus(404)
                 ->assertDontSeeText($country->name);

        $this->assertDatabaseMissing('country_partner', [
            'partner_id' => $partner->id,
            'country_id' => $country->id
        ]);

        $response = $this->post('/dashboard/partners/link_country/' . base64_encode($partner->id) . '?country_id=' . $country->id + 10);

        $response->assertStatus(404)
                 ->assertDontSeeText($country->name);

        $this->assertDatabaseMissing('country_partner', [
            'partner_id' => $partner->id,
            'country_id' => $country->id
        ]);
    }

    /** @test */
    public function can_not_attach_country_with_duplicate_id()
    {
        $partner = factory(\App\Partner::class)->create();
        $country = factory(\App\Country::class)->create();

        $response = $this->post('/dashboard/partners/link_country/' . base64_encode($partner->id) . '?country_id=' . $country->id);

        $response->assertStatus(200)
                 ->assertSeeText($country->name);

        $this->assertDatabaseHas('country_partner', [
            'partner_id' => $partner->id,
            'country_id' => $country->id
        ]);

        $duplicateResponse = $this->post('/dashboard/partners/link_country/' . base64_encode($partner->id) . '?country_id=' . $country->id);

        $duplicateResponse->assertStatus(200)
                          ->assertDontSeeText($country->name);

        $duplicateResponse->assertJson([
            'failed' => true,
            'message' => "Country already linked to partner"
        ]);
    }

    /** @test */
    public function can_unlink_country()
    {
        $partner = factory(\App\Partner::class)->create();
        $country = factory(\App\Country::class)->create();

        $response = $this->post('/dashboard/partners/link_country/' . base64_encode($partner->id) . '?country_id=' . $country->id);

        $response->assertStatus(200)
                 ->assertSeeText($country->name);

        $this->assertDatabaseHas('country_partner', [
            'partner_id' => $partner->id,
            'country_id' => $country->id
        ]);

        $responseUnlink = $this->get('/dashboard/partners/unlink_country/' . base64_encode($partner->id) . '/' . base64_encode($country->id));

        $responseUnlink->assertStatus(200)
                       ->assertDontSeeText($country->name);

        $this->assertDatabaseMissing('country_partner', [
            'partner_id' => $partner->id,
            'country_id' => $country->id
        ]);
    }

    /** @test */
    public function can_not_unlink_country_if_partner_or_country_do_not_exist()
    {
        $partner = factory(\App\Partner::class)->create();
        $country = factory(\App\Country::class)->create();

        $response = $this->post('/dashboard/partners/link_country/' . base64_encode($partner->id) . '?country_id=' . $country->id);

        $response->assertStatus(200)
                 ->assertSeeText($country->name);

        $this->assertDatabaseHas('country_partner', [
            'partner_id' => $partner->id,
            'country_id' => $country->id
        ]);

        $responseUnlink = $this->get('/dashboard/partners/unlink_country/' . base64_encode($partner->id + 10) . '/' . base64_encode($country->id));

        $responseUnlink->assertStatus(404)
                       ->assertDontSeeText($country->name);

        $this->assertDatabaseHas('country_partner', [
            'partner_id' => $partner->id,
            'country_id' => $country->id
        ]);

        $responseUnlink = $this->get('/dashboard/partners/unlink_country/' . base64_encode($partner->id) . '/' . base64_encode($country->id + 10));

        $responseUnlink->assertStatus(404)
                       ->assertDontSeeText($country->name);

        $this->assertDatabaseHas('country_partner', [
            'partner_id' => $partner->id,
            'country_id' => $country->id
        ]);
    }

    /** @test */
    public function can_attach_to_auto_generated_report()
    {
        $partner = factory(\App\Partner::class)->create();
        $agReport = factory(\App\AutoGeneratedReports::class)->create();
        $frequency = 'daily';

        $response = $this->post('/dashboard/partners/attach/agreport/' . base64_encode($partner->id) . '?agr_id=' . $agReport->id . '&frequency=' . $frequency);

        $response->assertStatus(200)
                 ->assertSeeText($agReport->name);

        $this->assertDatabaseHas('auto_generated_report_partner', [
            'partner_id' => $partner->id,
            'agr_id' => $agReport->id,
            'frequency' => $frequency
        ]);
    }

    /** @test */
    public function can_not_attach_to_auto_generated_report_if_partner_or_report_or_frequency_do_not_exist()
    {
        $partner = factory(\App\Partner::class)->create();
        $agReport = factory(\App\AutoGeneratedReports::class)->create();
        $frequency = 'daily';

        $response = $this->post('/dashboard/partners/attach/agreport/' . base64_encode($partner->id + 10) . '?agr_id=' . $agReport->id . '&frequency=' . $frequency);

        $response->assertStatus(404)
                 ->assertDontSeeText($agReport->name);

        $this->assertDatabaseMissing('auto_generated_report_partner', [
            'partner_id' => $partner->id,
            'agr_id' => $agReport->id,
            'frequency' => $frequency
        ]);

        $response = $this->post('/dashboard/partners/attach/agreport/' . base64_encode($partner->id) . '?agr_id=' . $agReport->id + 10 . '&frequency=' . $frequency);

        $response->assertStatus(404)
                 ->assertDontSeeText($agReport->name);

        $this->assertDatabaseMissing('auto_generated_report_partner', [
            'partner_id' => $partner->id,
            'agr_id' => $agReport->id,
            'frequency' => $frequency
        ]);

        $response = $this->post('/dashboard/partners/attach/agreport/' . base64_encode($partner->id) . '?agr_id=' . $agReport->id . '&frequency=');

        $response->assertStatus(404)
                 ->assertDontSeeText($agReport->name);

        $this->assertDatabaseMissing('auto_generated_report_partner', [
            'partner_id' => $partner->id,
            'agr_id' => $agReport->id,
            'frequency' => $frequency
        ]);
    }

    /** @test */
    public function can_not_attach_to_auto_generated_report_with_duplicate_id()
    {
        $partner = factory(\App\Partner::class)->create();
        $agReport = factory(\App\AutoGeneratedReports::class)->create();
        $frequency = 'daily';

        $response = $this->post('/dashboard/partners/attach/agreport/' . base64_encode($partner->id) . '?agr_id=' . $agReport->id . '&frequency=' . $frequency);

        $response->assertStatus(200)
                 ->assertSeeText($agReport->name);

        $this->assertDatabaseHas('auto_generated_report_partner', [
            'partner_id' => $partner->id,
            'agr_id' => $agReport->id,
            'frequency' => $frequency
        ]);

        $duplicateResponse = $this->post('/dashboard/partners/attach/agreport/' . base64_encode($partner->id) . '?agr_id=' . $agReport->id . '&frequency=' . $frequency);

        $duplicateResponse->assertStatus(200)
                          ->assertDontSeeText($agReport->name);

        $duplicateResponse->assertJson([
            'failed' => true,
            'message' => "Report already linked to partner"
        ]);
    }

    /** @test */
    public function can_unlink_auto_generated_report()
    {
        $partner = factory(\App\Partner::class)->create();
        $agReport = factory(\App\AutoGeneratedReports::class)->create();
        $frequency = 'daily';

        $response = $this->post('/dashboard/partners/attach/agreport/' . base64_encode($partner->id) . '?agr_id=' . $agReport->id . '&frequency=' . $frequency);

        $response->assertStatus(200)
                 ->assertSeeText($agReport->name);

        $this->assertDatabaseHas('auto_generated_report_partner', [
            'partner_id' => $partner->id,
            'agr_id' => $agReport->id,
            'frequency' => $frequency
        ]);

        $responseUnlink = $this->get('/dashboard/partners/detach/agreport/' . base64_encode($partner->id) . '/' . base64_encode($agReport->id));

        $responseUnlink->assertStatus(200)
                       ->assertDontSeeText($agReport->name);

        $this->assertDatabaseMissing('auto_generated_report_partner', [
            'partner_id' => $partner->id,
            'agr_id' => $agReport->id
        ]);
    }

    /** @test */
    public function can_not_unlink_auto_generated_report_if_data_does_not_exist()
    {
        $partner = factory(\App\Partner::class)->create();
        $agReport = factory(\App\AutoGeneratedReports::class)->create();
        $frequency = 'daily';

        $response = $this->post('/dashboard/partners/attach/agreport/' . base64_encode($partner->id) . '?agr_id=' . $agReport->id . '&frequency=' . $frequency);

        $response->assertStatus(200)
                 ->assertSeeText($agReport->name);

        $this->assertDatabaseHas('auto_generated_report_partner', [
            'partner_id' => $partner->id,
            'agr_id' => $agReport->id,
            'frequency' => $frequency
        ]);

        $responseUnlink = $this->get('/dashboard/partners/detach/agreport/' . base64_encode($partner->id + 10) . '/' . base64_encode($agReport->id));

        $responseUnlink->assertStatus(404)
                       ->assertDontSeeText($agReport->name);

        $this->assertDatabaseHas('auto_generated_report_partner', [
            'partner_id' => $partner->id,
            'agr_id' => $agReport->id
        ]);

        $responseUnlink = $this->get('/dashboard/partners/detach/agreport/' . base64_encode($partner->id) . '/' . base64_encode($agReport->id + 10));

        $responseUnlink->assertStatus(404)
                       ->assertDontSeeText($agReport->name);

        $this->assertDatabaseHas('auto_generated_report_partner', [
            'partner_id' => $partner->id,
            'agr_id' => $agReport->id
        ]);
    }

    /** @test */
    public function can_filter_partners_by_search_query()
    {
        $partner = factory(\App\Partner::class)->create();

        $response = $this->get('/dashboard/partners?q=' . uniqid());

        $response->assertStatus(200)
                 ->assertViewIs('partner.list')
                 ->assertDontSee($partner->name);

        $response = $this->get('/dashboard/partners?q=' . $partner->name);

        $response->assertStatus(200)
                 ->assertViewIs('partner.list')
                 ->assertSee($partner->name);
    }

    /** @test */
    public function can_load_partners_store_location_form()
    {
        $partner = factory(\App\Partner::class)->create();

        $response = $this->get('/dashboard/store/new/' . $partner->id);

        $response->assertStatus(200)
                 ->assertViewIs('store.form');
    }

    /**
     * @return Address
     */
    protected function createAddress()
    {
        $country = factory(\App\Country::class)->create();
        $area = factory(\App\Area::class)->create([
            'country_id' => $country->id,
        ]);
        $city = factory(\App\City::class)->create([
            'area_id' => $area->id,
        ]);

        $address = factory(\App\Address::class)->create([
            'country_id' => $country->id,
            'area_id' => $area->id,
            'city_id' => $city->id,
        ]);

        return $address;
    }

    /** @test */
    public function can_save_partner_store_location()
    {
        $partner = factory(\App\Partner::class)->create();
        $store = factory(\App\Store::class)->create([
            'partner_id' => $partner->id
        ]);

        $address = $this->createAddress();
        $store->address()->save($address);

        $data = [
            'store_name' => uniqid(),
            'display_online' => 1,
            'store_country' => $address['country_id'],
            'store_area' => $address['area_id'],
            'store_city' => $address['city_id'],
            'store_street' => '',
            'store_floor' => '',
            'store_building' => '',
            'mapping_id' => 0,
            'store_latitude' => 33.904250,
            'store_longitude' => 35.577072,
        ];

        $response = $this->post('/dashboard/store/update/' . base64_encode($store->id), $data);

        $response->assertStatus(200)
                 ->assertJson([
                     'failed' => false,
                     'view_url' => url('dashboard/store/list/' . $partner->id),
                 ]);

        $this->assertDatabaseHas('store', [
            'name' => $data['store_name'],
            'partner_id' => $partner->id,
        ]);
    }

    /** @test */
    public function can_not_save_partner_store_location_with_incorrect_store()
    {
        $partner = factory(\App\Partner::class)->create();
        $store = factory(\App\Store::class)->create([
            'partner_id' => $partner->id
        ]);

        $address = $this->createAddress();
        $store->address()->save($address);

        $data = [
            'store_name' => uniqid(),
            'display_online' => 1,
            'store_country' => $address['country_id'],
            'store_area' => $address['area_id'],
            'store_city' => $address['city_id'],
            'store_street' => '',
            'store_floor' => '',
            'store_building' => '',
            'mapping_id' => 0,
            'store_latitude' => 33.904250,
            'store_longitude' => 35.577072,
        ];

        $response = $this->post('/dashboard/store/update/' . base64_encode($store->id + 10), $data);

        $response->assertStatus(404);

        $this->assertDatabaseMissing('store', [
            'name' => $data['store_name'],
            'partner_id' => $partner->id,
        ]);
    }

    /** @test */
    public function can_get_list_partner_store_locations()
    {
        $partner = factory(\App\Partner::class)->create();
        $store = factory(\App\Store::class)->create([
            'partner_id' => $partner->id
        ]);

        $store->address()->save($this->createAddress());

        $response = $this->get('/dashboard/store/list/' . $partner->id);

        $response->assertStatus(200)
                 ->assertViewIs('store.list')
                 ->assertSee($store->name);
    }

    /** @test */
    public function can_not_get_list_partner_store_locations_with_incorrect_store()
    {
        $partner = factory(\App\Partner::class)->create();
        $store = factory(\App\Store::class)->create([
            'partner_id' => $partner->id
        ]);

        $store->address()->save($this->createAddress());

        $response = $this->get('/dashboard/store/list/' . $partner->id + 10);

        $response->assertStatus(404)
                 ->assertDontSee($store->name);
    }

    /** @test */
    public function can_delete_partner_store_location()
    {
        $partner = factory(\App\Partner::class)->create();
        $store = factory(\App\Store::class)->create([
            'partner_id' => $partner->id
        ]);

        $store->address()->save($this->createAddress());

        $response = $this->get('/dashboard/store/delete/' . base64_encode($store->id));

        $response->assertStatus(200)
                 ->assertJson([
                     'failed' => false,
                 ]);

        $this->assertDatabaseMissing('store', [
            'name' => $store->name,
        ]);
    }

    /** @test */
    public function can_not_delete_partner_store_location_with_incorrect_store()
    {
        $partner = factory(\App\Partner::class)->create();
        $store = factory(\App\Store::class)->create([
            'partner_id' => $partner->id
        ]);

        $store->address()->save($this->createAddress());

        $response = $this->get('/dashboard/store/delete/' . base64_encode($store->id + 10));

        $response->assertStatus(404);

        $this->assertDatabaseHas('store', [
            'name' => $store->name,
        ]);
    }

    /** @test */
    public function can_view_pagination_page()
    {
        $perPage = 15;
        $pageCount = ceil(\App\Partner::where('draft', 0)->count() / $perPage);

        if ($pageCount > 1) {
            $response = $this->get('/dashboard/partners?page=' . $pageCount);

            $response->assertStatus(200)
                     ->assertViewIs('partner.list')
                     ->assertSee('partners?page=1')
                     ->assertDontSee('No Partners Found');
        }
    }

    /** @test */
    public function can_not_view_pagination_page_which_does_not_exist()
    {
        $perPage = 15;
        $page = ceil(\App\Partner::where('draft', 0)->count() / $perPage) + 1;

        $response = $this->get('/dashboard/partners?page=' . $page);

        $response->assertStatus(200)
                 ->assertViewIs('partner.list')
                 ->assertDontSee('partners?page=' . $page)
                 ->assertSee('No Partners Found');
    }
}
