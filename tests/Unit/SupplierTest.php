<?php

namespace Tests\Unit;

use App\Address;
use App\Admin;
use App\Supplier;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SupplierTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    protected $user;

    public function setUp()
    {
        parent::setUp();

        $user = Admin::where('email', 'info@bluloyalty.com')->first();
        $this->user = $user;
        $this->actingAs($user)
             ->session([
                 'user_roles' => [
                     'BLU Admin',
                 ],
                 'user_permissions' => [
                     'create_suppliers',
                     'view_suppliers',
                     'edit_suppliers',
                     'delete_suppliers',
                 ],
             ]);
    }

    /** @test */
    public function can_create_new_supplier_page_with_permissions()
    {
        $response = $this->get('/dashboard/suppliers/new');

        $response->assertStatus(302)
                 ->assertRedirect()
                 ->assertSeeText('/dashboard/suppliers/view/');
    }

    /** @test */
    public function can_not_create_new_supplier_page_without_permissions()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $response = $this->get('/dashboard/suppliers/new');

        $response->assertStatus(302)
                 ->assertRedirect()
                 ->assertSeeText('/dashboard');
    }

    /**
     * @return Supplier
     */
    protected function createSupplier()
    {
        $country = factory(\App\Country::class)->create();
        $area = factory(\App\Area::class)->create([
            'country_id' => $country->id,
        ]);
        $city = factory(\App\City::class)->create([
            'area_id' => $area->id,
        ]);
        $category = factory(\App\PartnerCategory::class)->create();

        $supplier = factory(\App\Supplier::class)->create([
            'country_id' => $country->id,
            'area_id' => $area->id,
            'city_id' => $city->id,
            'category_id' => $category->id,
        ]);
        $supplier->partners()->save((\App\Partner::find(1))->first());

        return $supplier;
    }

    /**
     * @return Address
     */
    protected function createAddress()
    {
        $country = factory(\App\Country::class)->create();
        $area = factory(\App\Area::class)->create([
            'country_id' => $country->id,
        ]);
        $city = factory(\App\City::class)->create([
            'area_id' => $area->id,
        ]);

        $address = factory(\App\Address::class)->create([
            'country_id' => $country->id,
            'area_id' => $area->id,
            'city_id' => $city->id,
        ]);

        return $address;
    }

    /** @test */
    public function can_open_edit_supplier_with_permissions()
    {
        $supplier = $this->createSupplier();

        $response = $this->get('/dashboard/suppliers/view/' . base64_encode($supplier->id));

        $response->assertStatus(200)
                 ->assertViewIs('supplier.view');
    }

    /** @test */
    public function can_not_open_supplier_page_which_is_not_exists()
    {
        $supplier = $this->createSupplier();

        $response = $this->get('/dashboard/suppliers/view/' . base64_encode($supplier->id + 10));

        $response->assertStatus(404);
    }

    /** @test */
    public function can_not_open_supplier_page_which_is_not_draft_and_partner_can_not_view()
    {
        $supplier = factory(\App\Supplier::class)->create();
        $partner = factory(\App\Partner::class)->create();
        $supplier->partners()->save($partner);

        $response = $this->get('/dashboard/suppliers/view/' . base64_encode($supplier->id));

        $response->assertStatus(302)
                 ->assertRedirect()
                 ->assertSeeText('/dashboard');
    }

    /** @test */
    public function can_not_open_edit_supplier_without_permissions()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $supplier = $this->createSupplier();

        $response = $this->get('/dashboard/suppliers/view/' . base64_encode($supplier->id));

        $response->assertStatus(302)
                 ->assertRedirect()
                 ->assertSeeText('/dashboard');
    }

    /** @test */
    public function can_update_supplier_with_permissions()
    {
        $supplier = $this->createSupplier();

        $category = factory(\App\PartnerCategory::class)->create();

        $data = [
            'name' => uniqid(),
            'category_id' => $category->id,
        ];

        $response = $this->post('/dashboard/suppliers/update/' . base64_encode($supplier->id), $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/suppliers')
                 ->assertSessionMissing('alert_error');

        $this->assertDatabaseHas('supplier', [
            'id' => $supplier->id,
            'name' => $data['name'],
        ]);
    }

    /** @test */
    public function can_not_update_supplier_without_permission()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $supplier = $this->createSupplier();

        $data = [
            'name' => uniqid(),
        ];

        $response = $this->post('/dashboard/suppliers/update/' . base64_encode($supplier->id), $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard');

        $this->assertDatabaseMissing('supplier', [
            'id' => $supplier->id,
            'name' => $data['name'],
        ]);
    }

    /** @test */
    public function can_not_update_supplier_which_is_not_exists()
    {
        $supplier = $this->createSupplier();

        $data = [
            'name' => uniqid(),
        ];

        $response = $this->post('/dashboard/suppliers/update/' . base64_encode($supplier->id + 10), $data);

        $response->assertStatus(404);

        $this->assertDatabaseMissing('supplier', [
            'id' => $supplier->id,
            'name' => $data['name'],
        ]);
    }

    /** @test */
    public function can_not_update_supplier_without_required_fields()
    {
        $supplier = $this->createSupplier();

        $data = [];
        $response = $this->post('/dashboard/suppliers/update/' . base64_encode($supplier->id), $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/suppliers/view/' . $supplier->id)
                 ->assertSessionHasErrors(['name', 'category_id']);

        $data = [
            'name' => 'q',
            'category_id' => 999999,
        ];
        $response = $this->post('/dashboard/suppliers/update/' . base64_encode($supplier->id), $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/suppliers/view/' . $supplier->id)
                 ->assertSessionHasErrors(['name', 'category_id']);
    }

    /** @test */
    public function can_delete_supplier_with_permission()
    {
        $supplier = $this->createSupplier();

        $response = $this->get('/dashboard/suppliers/delete/' . base64_encode($supplier->id));

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/suppliers');

        $this->assertDatabaseMissing('supplier', [
            'id' => $supplier->id,
        ]);
    }

    /** @test */
    public function can_not_delete_supplier_without_permission()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $supplier = $this->createSupplier();

        $response = $this->get('/dashboard/suppliers/delete/' . base64_encode($supplier->id));

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard');

        $this->assertDatabaseHas('supplier', [
            'id' => $supplier->id,
        ]);
    }

    /** @test */
    public function can_not_delete_supplier_which_is_not_exists()
    {
        $supplier = $this->createSupplier();

        $data = [
            'name' => uniqid(),
        ];

        $response = $this->get('/dashboard/suppliers/delete/' . base64_encode($supplier->id + 10));

        $response->assertStatus(404);

        $this->assertDatabaseMissing('supplier', [
            'id' => $supplier->id,
            'name' => $data['name'],
        ]);

        $this->assertDatabaseHas('supplier', [
            'id' => $supplier->id,
        ]);
    }

    /** @test */
    public function can_filter_suppliers_by_search_query()
    {
        $supplier = $this->createSupplier();

        $response = $this->get('/dashboard/suppliers?q=' . uniqid());

        $response->assertStatus(200)
                 ->assertViewIs('supplier.list')
                 ->assertDontSee($supplier->name);

        $response = $this->get('/dashboard/suppliers?q=' . $supplier->name);

        $response->assertStatus(200)
                 ->assertViewIs('supplier.list')
                 ->assertSee($supplier->name);
    }

    /** @test */
    public function can_load_suppliers_store_location_form()
    {
        $supplier = $this->createSupplier();

        $response = $this->get('/dashboard/supplierstore/new/' . $supplier->id);

        $response->assertStatus(200)
                 ->assertViewIs('supplierstore.form');
    }

    /** @test */
    public function can_save_suppliers_store_location()
    {
        $supplier = $this->createSupplier();
        $supplierStore = factory(\App\Supplierstore::class)->create([
            'supplier_id' => $supplier->id,
        ]);
        $supplierStore->address()->save($this->createAddress());

        $data = [
            'supplierstore_name' => uniqid(),
            'supplierstore_country' => '',
            'supplierstore_area' => 76,
            'supplierstore_city' => 4138,
            'supplierstore_street' => '',
            'supplierstore_floor' => '',
            'supplierstore_building' => '',
            'supplierstore_latitude' => 33.904250,
            'supplierstore_longitude' => 35.577072,
        ];

        $response = $this->post('/dashboard/supplierstore/update/' . $supplierStore->id, $data);

        $response->assertStatus(200)
                 ->assertJson([
                     'failed' => false,
                     'view_url' => url('dashboard/supplierstore/list/' . $supplierStore->supplier_id),
                 ]);

        $this->assertDatabaseHas('supplier_store', [
            'name' => $data['supplierstore_name'],
            'supplier_id' => $supplier->id,
        ]);
    }

    /** @test */
    public function can_not_save_suppliers_store_location_without_required_fields()
    {
        $supplier = $this->createSupplier();
        $supplierStore = factory(\App\Supplierstore::class)->create([
            'supplier_id' => $supplier->id,
        ]);
        $supplierStore->address()->save($this->createAddress());

        $data = [];

        $response = $this->post('/dashboard/supplierstore/update/' . $supplierStore->id, $data);

        $response->assertStatus(200)
                 ->assertJsonFragment([
                     'failed' => true,
                 ]);
    }

    /** @test */
    public function can_get_list_suppliers_store_locations()
    {
        $supplier = $this->createSupplier();
        $supplierStore = factory(\App\Supplierstore::class)->create([
            'supplier_id' => $supplier->id,
        ]);
        $supplierStore->address()->save($this->createAddress());

        $response = $this->get('/dashboard/supplierstore/list/' . $supplier->id);

        $response->assertStatus(200)
                 ->assertViewIs('supplierstore.list')
                 ->assertSee($supplierStore->name);
    }

    /** @test */
    public function can_delete_suppliers_store_location()
    {
        $supplier = $this->createSupplier();
        $supplierStore = factory(\App\Supplierstore::class)->create([
            'supplier_id' => $supplier->id,
        ]);
        $supplierStore->address()->save($this->createAddress());

        $response = $this->get('/dashboard/supplierstore/delete/' . $supplierStore->id);

        $response->assertStatus(200)
                 ->assertJson([
                     'failed' => false,
                 ]);

        $this->assertDatabaseMissing('supplier_store', [
            'name' => $supplierStore->name,
        ]);
    }

    /** @test */
    public function can_attach_partner()
    {
        $supplier = $this->createSupplier();
        $partner = factory(\App\Partner::class)->create();

        $data = [
            'partner_id' => $partner->id,
        ];

        $response = $this->post('/dashboard/partners/attachsupplier/' . base64_encode($supplier->id), $data);

        $response->assertStatus(200)
                 ->assertViewIs('partner.supplierlist')
                 ->assertSee($partner->name);
    }

    /** @test */
    public function can_not_attach_partner_if_supplier_not_found()
    {
        $supplier = $this->createSupplier();
        $partner = factory(\App\Partner::class)->create();

        $data = [
            'partner_id' => $partner->id,
        ];

        $response = $this->post('/dashboard/partners/attachsupplier/' . base64_encode($supplier->id + 10), $data);

        $response->assertStatus(404);
    }

    /** @test */
    public function can_not_attach_partner_if_partner_not_found()
    {
        $supplier = $this->createSupplier();
        $partner = factory(\App\Partner::class)->create();

        $data = [
            'partner_id' => $partner->id + 10,
        ];

        $response = $this->post('/dashboard/partners/attachsupplier/' . base64_encode($supplier->id), $data);

        $response->assertStatus(404);
    }

    /** @test */
    public function can_detach_partner()
    {
        $supplier = $this->createSupplier();
        $partner = factory(\App\Partner::class)->create();

        $supplier->partners()->save($partner);

        $response = $this->get('/dashboard/partners/unlinksupplier/' . base64_encode($supplier->id) . '/' . base64_encode($partner->id));

        $response->assertStatus(200)
                 ->assertViewIs('partner.supplierlist')
                 ->assertDontSee($partner->name);

        $this->assertDatabaseMissing('supplier_partner', [
            'supplier_id' => $supplier->id,
            'partner_id' => $partner->id,
        ]);
    }

    /** @test */
    public function can_not_detach_partner_if_supplier_not_found()
    {
        $supplier = $this->createSupplier();
        $partner = factory(\App\Partner::class)->create();

        $supplier->partners()->save($partner);

        $response = $this->get('/dashboard/partners/unlinksupplier/' . base64_encode($supplier->id + 10) . '/' . base64_encode($partner->id));

        $response->assertStatus(404);
    }

    /** @test */
    public function can_not_detach_partner_if_partner_not_found()
    {
        $supplier = $this->createSupplier();
        $partner = factory(\App\Partner::class)->create();

        $supplier->partners()->save($partner);

        $response = $this->get('/dashboard/partners/unlinksupplier/' . base64_encode($supplier->id) . '/' . base64_encode($partner->id + 10));

        $response->assertStatus(404);
    }

    /** @test */
    public function can_attach_network()
    {
        $supplier = $this->createSupplier();
        $network = factory(\App\Network::class)->create();

        $data = [
            'network_id' => $network->id,
        ];

        $response = $this->post('/dashboard/suppliers/attach/network/' . base64_encode($supplier->id), $data);

        $response->assertStatus(200)
                 ->assertViewIs('supplier.networklist')
                 ->assertSee($network->name);
    }

    /** @test */
    public function can_not_attach_network_if_supplier_not_found()
    {
        $supplier = $this->createSupplier();
        $network = factory(\App\Network::class)->create();

        $data = [
            'network_id' => $network->id,
        ];

        $response = $this->post('/dashboard/suppliers/attach/network/' . base64_encode($supplier->id + 10), $data);

        $response->assertStatus(404);
    }

    /** @test */
    public function can_not_attach_network_if_network_not_found()
    {
        $supplier = $this->createSupplier();
        $network = factory(\App\Network::class)->create();

        $data = [
            'network_id' => $network->id + 10,
        ];

        $response = $this->post('/dashboard/suppliers/attach/network/' . base64_encode($supplier->id), $data);

        $response->assertStatus(404);
    }

    /** @test */
    public function can_not_attach_network_twice()
    {
        $supplier = $this->createSupplier();
        $network = factory(\App\Network::class)->create();

        $supplier->networks()->save($network);

        $data = [
            'network_id' => $network->id,
        ];

        $response = $this->post('/dashboard/suppliers/attach/network/' . base64_encode($supplier->id), $data);

        $response->assertStatus(200)
                 ->assertJson([
                     'failed' => true,
                     'message' => 'Network already linked to supplier',
                 ]);
    }

    /** @test */
    public function can_detach_network()
    {
        $supplier = $this->createSupplier();
        $network = factory(\App\Network::class)->create();

        $supplier->networks()->save($network);

        $response = $this->get('/dashboard/suppliers/detach/network/' . base64_encode($supplier->id) . '/' . base64_encode($network->id));

        $response->assertStatus(200)
                 ->assertViewIs('supplier.networklist')
                 ->assertDontSee($network->name);

        $this->assertDatabaseMissing('supplier_network', [
            'supplier_id' => $supplier->id,
            'network_id' => $network->id,
        ]);
    }

    /** @test */
    public function can_not_detach_network_if_supplier_not_found()
    {
        $supplier = $this->createSupplier();
        $network = factory(\App\Network::class)->create();

        $supplier->networks()->save($network);

        $response = $this->get('/dashboard/networks/unlinksupplier/' . base64_encode($supplier->id + 10) . '/' . base64_encode($network->id));

        $response->assertStatus(404);
    }

    /** @test */
    public function can_not_detach_network_if_network_not_found()
    {
        $supplier = $this->createSupplier();
        $network = factory(\App\Network::class)->create();

        $supplier->networks()->save($network);

        $response = $this->get('/dashboard/networks/unlinksupplier/' . base64_encode($supplier->id) . '/' . base64_encode($network->id + 10));

        $response->assertStatus(404);
    }

    /** @test */
    public function can_view_pagination_page()
    {
        $perPage = 15;
        $pageCount = ceil(Supplier::where('draft', 0)->count() / $perPage);

        if ($pageCount > 1) {
            $response = $this->get('/dashboard/suppliers?page=' . $pageCount);

            $response->assertStatus(200)
                     ->assertViewIs('supplier.list')
                     ->assertSee('suppliers?page=1')
                     ->assertDontSee('No Suppliers Found');
        }
    }

    /** @test */
    public function can_not_view_pagination_page_if_one_page()
    {
        $perPage = 15;
        $page = ceil(Supplier::where('draft', 0)->count() / $perPage) + 1;

        $response = $this->get('/dashboard/suppliers?page=' . $page);

        $response->assertStatus(200)
                 ->assertViewIs('supplier.list')
                 ->assertDontSee('suppliers?page=' . $page)
                 ->assertSee('No Suppliers Found');
    }
}
