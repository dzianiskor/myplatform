<?php

namespace Tests\Unit;

use App\Admin;
use App\AffiliateProgram;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AffiliateProgramTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    protected $user;

    public function setUp()
    {
        parent::setUp();

        $user = Admin::where('email', 'info@bluloyalty.com')->first();
        $this->user = $user;
        $this->actingAs($user)
            ->session([
                'user_roles' => [
                    'BLU Admin',
                ],
                'user_permissions' => [
                    'view_affiliate_program',
                    'add_affiliate_program',
                    'edit_affiliate_program',
                    'delete_affiliate_program',
                ],
            ]);
    }

    /**
     * @return Affiliate
     */
    protected function createAffiliateProgram()
    {
        $affiliate = factory(\App\Affiliate::class)->create();

        $affiliateProgram = factory(\App\AffiliateProgram::class)->create([
            'affiliate_id' => $affiliate->id,
        ]);

        return $affiliateProgram;
    }

    /** @test */
    public function can_create_new_affiliate_program_page_with_permission()
    {
        $response = $this->get('/dashboard/affiliate_program/new');

        $response->assertStatus(302)
                 ->assertRedirect()
                 ->assertSeeText('/dashboard/affiliate_program/view/');
    }

    /** @test */
    public function can_not_create_new_affiliate_program_page_without_permission()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $response = $this->get('/dashboard/affiliate_program/new');

        $response->assertStatus(302)
                 ->assertRedirect()
                 ->assertSeeText('/dashboard');
    }

    /** @test */
    public function can_open_edit_affiliate_program_with_permission()
    {
        $affiliateProgram = $this->createAffiliateProgram();

        $response = $this->get('/dashboard/affiliate_program/view/' . base64_encode($affiliateProgram->id));

        $response->assertStatus(200)
                 ->assertViewIs('affiliate_program.view');
    }

    /** @test */
    public function can_not_open_edit_affiliate_program_without_permission()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $affiliateProgram = $this->createAffiliateProgram();

        $response = $this->get('/dashboard/affiliate_program/view/' . base64_encode($affiliateProgram->id));

        $response->assertStatus(302)
                 ->assertRedirect()
                 ->assertSeeText('/dashboard');
    }

    /** @test */
    public function can_not_open_affiliate_program_page_which_is_not_exists()
    {
        $affiliateProgram = $this->createAffiliateProgram();

        $response = $this->get('/dashboard/affiliate_program/view/' . base64_encode($affiliateProgram->id + 10));

        $response->assertStatus(404);
    }

    /** @test */
    public function can_update_affiliate_program_with_permission()
    {
        $affiliateProgram = $this->createAffiliateProgram();

        $country = factory(\App\Country::class)->create();

        $stub = __DIR__.'\\..\\stubs\\test.png';
        $name = str_random(8).'.png';
        $path = sys_get_temp_dir().'\\'.$name;

        copy($stub, $path);

        $file = new UploadedFile($path, $name, filesize($path), 'image/png', null, true);

        $data = [
            'name' => uniqid(),
            'country_id' => $country->id,
        ];

        $response = $this->call('POST', '/dashboard/affiliate_program/update/' . base64_encode($affiliateProgram->id), $data, [], ['image' => $file], ['Accept' => 'application/json']);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/affiliate_program')
                 ->assertSessionMissing('alert_error');

        $this->assertDatabaseHas('affiliate_program', [
            'id' => $affiliateProgram->id,
            'name' => $data['name'],
        ]);

        $affiliateProgram1 = \App\AffiliateProgram::find(['id' => $affiliateProgram->id])->first();

        $this->assertFileExists(public_path(\App\Http\Controllers\MediaController::getImageFromId($affiliateProgram1->image)));

        @unlink(public_path(\App\Http\Controllers\MediaController::getImageFromId($affiliateProgram1->image)));
    }

    /** @test */
    public function can_not_update_affiliate_program_without_permission()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $affiliateProgram = $this->createAffiliateProgram();

        $data = [
            'name' => uniqid(),
        ];

        $response = $this->post('/dashboard/affiliate_program/update/' . base64_encode($affiliateProgram->id), $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard');

        $this->assertDatabaseMissing('affiliate_program', [
            'id' => $affiliateProgram->id,
            'name' => $data['name'],
        ]);
    }

    /** @test */
    public function can_not_update_affiliate_program_which_is_not_exists()
    {
        $affiliateProgram = $this->createAffiliateProgram();

        $data = [
            'name' => uniqid(),
        ];

        $response = $this->post('/dashboard/affiliate_program/update/' . base64_encode($affiliateProgram->id + 10), $data);

        $response->assertStatus(404);

        $this->assertDatabaseMissing('affiliate_program', [
            'id' => $affiliateProgram->id,
            'name' => $data['name'],
        ]);
    }

    /** @test */
    public function can_not_update_affiliate_program_without_required_fields()
    {
        $affiliateProgram = $this->createAffiliateProgram();

        $data = [];
        $response = $this->post('/dashboard/affiliate_program/update/' . base64_encode($affiliateProgram->id), $data);

        $response->assertStatus(302)
            ->assertRedirect('/dashboard/affiliate_program/view/' . base64_encode($affiliateProgram->id))
            ->assertSessionHasErrors(['name']);

        $data = [
            'name' => 'q',
        ];
        $response = $this->post('/dashboard/affiliate_program/update/' . base64_encode($affiliateProgram->id), $data);

        $response->assertStatus(302)
            ->assertRedirect('/dashboard/affiliate_program/view/' . base64_encode($affiliateProgram->id))
            ->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function can_delete_affiliate_program_with_permission()
    {
        $affiliateProgram = $this->createAffiliateProgram();

        $response = $this->get('/dashboard/affiliate_program/delete/' . base64_encode($affiliateProgram->id));

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/affiliate_program');

        $this->assertDatabaseMissing('affiliate_program', [
            'id' => $affiliateProgram->id,
        ]);
    }

    /** @test */
    public function can_not_delete_affiliate_program_without_permission()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $affiliateProgram = $this->createAffiliateProgram();

        $response = $this->get('/dashboard/affiliate_program/delete/' . base64_encode($affiliateProgram->id));

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard');

        $this->assertDatabaseHas('affiliate_program', [
            'id' => $affiliateProgram->id,
        ]);
    }

    /** @test */
    public function can_not_delete_affiliate_program_which_is_not_exists()
    {
        $affiliateProgram = $this->createAffiliateProgram();

        $response = $this->get('/dashboard/affiliate_program/delete/' . base64_encode($affiliateProgram->id + 10));

        $response->assertStatus(404);

        $this->assertDatabaseHas('affiliate_program', [
            'id' => $affiliateProgram->id,
        ]);
    }

    /** @test */
    public function can_attach_channel()
    {
        $affiliateProgram = $this->createAffiliateProgram();
        $partner = factory(\App\Partner::class)->create();

        $data = [
            'partner_id' => $partner->id,
        ];

        $response = $this->post('/dashboard/affiliate_program/attachpartner/' . base64_encode($affiliateProgram->id), $data);

        $response->assertStatus(200)
                 ->assertViewIs('affiliate_program.partnerlist')
                 ->assertSee($partner->name);
    }

    /** @test */
    public function can_not_attach_channel_if_affiliate_not_found()
    {
        $affiliateProgram = $this->createAffiliateProgram();
        $partner = factory(\App\Partner::class)->create();

        $data = [
            'partner_id' => $partner->id,
        ];

        $response = $this->post('/dashboard/affiliate_program/attachpartner/' . base64_encode($affiliateProgram->id + 10), $data);

        $response->assertStatus(404);
    }

    /** @test */
    public function can_not_attach_channel_if_partner_not_found()
    {
        $affiliateProgram = $this->createAffiliateProgram();
        $partner = factory(\App\Partner::class)->create();

        $data = [
            'partner_id' => $partner->id + 10,
        ];

        $response = $this->post('/dashboard/affiliate_program/attachpartner/' . base64_encode($affiliateProgram->id), $data);

        $response->assertStatus(404);
    }

    /** @test */
    public function can_detach_channel()
    {
        $affiliateProgram = $this->createAffiliateProgram();
        $partner = factory(\App\Partner::class)->create();

        $affiliateProgram->partners()->save($partner);

        $response = $this->get('/dashboard/affiliate_program/unlinkpartner/' . base64_encode($affiliateProgram->id) . '/' . base64_encode($partner->id));

        $response->assertStatus(200)
                 ->assertViewIs('affiliate_program.partnerlist')
                 ->assertDontSee($partner->name);

        $this->assertDatabaseMissing('affiliate_program_partner', [
            'affiliate_program_id' => $affiliateProgram->id,
            'partner_id' => $partner->id,
        ]);
    }

    /** @test */
    public function can_not_detach_channel_if_affiliate_not_found()
    {
        $affiliateProgram = $this->createAffiliateProgram();
        $partner = factory(\App\Partner::class)->create();

        $affiliateProgram->partners()->save($partner);

        $response = $this->get('/dashboard/affiliate_program/unlinkpartner/' . base64_encode($affiliateProgram->id + 10) . '/' . base64_encode($partner->id));

        $response->assertStatus(404);
    }

    /** @test */
    public function can_not_detach_channel_if_partner_not_found()
    {
        $affiliateProgram = $this->createAffiliateProgram();
        $partner = factory(\App\Partner::class)->create();

        $affiliateProgram->partners()->save($partner);

        $response = $this->get('/dashboard/affiliate_program/unlinkpartner/' . base64_encode($affiliateProgram->id) . '/' . base64_encode($partner->id + 10));

        $response->assertStatus(404);
    }

    /** @test */
    public function can_filter_affiliate_programs_by_search_query()
    {
        $affiliateProgram = $this->createAffiliateProgram();

        $response = $this->get('/dashboard/affiliate_program?q=' . uniqid());

        $response->assertStatus(200)
                 ->assertViewIs('affiliate_program.list')
                 ->assertDontSee($affiliateProgram->name);

        $response = $this->get('/dashboard/affiliate_program?q=' . $affiliateProgram->name);

        $response->assertStatus(200)
                 ->assertViewIs('affiliate_program.list')
                 ->assertSee($affiliateProgram->name);
    }

    /** @test */
    public function can_filter_affiliate_programs_by_affiliate()
    {
        $affiliateProgram = $this->createAffiliateProgram();

        $affiliate1 = factory(\App\Affiliate::class)->create();
        $affiliate2 = factory(\App\Affiliate::class)->create();

        $response = $this->get('/dashboard/affiliate_program?affiliate_list[]=' . $affiliate1->id . '&affiliate_list[]=' . $affiliate2->id);

        $response->assertStatus(200)
                 ->assertViewIs('affiliate_program.list')
                 ->assertDontSee($affiliateProgram->name);

        $response = $this->get('/dashboard/affiliate_program?affiliate_list[]=' . $affiliateProgram->affiliate_id . '&affiliate_list[]=' . $affiliate2->id);

        $response->assertStatus(200)
                 ->assertViewIs('affiliate_program.list')
                 ->assertSee($affiliateProgram->name);
    }

    /** @test */
    public function can_filter_affiliate_programs_by_channel()
    {
        $affiliateProgram = $this->createAffiliateProgram();
        $partner = factory(\App\Partner::class)->create();
        $affiliateProgram->partners()->save($partner);

        $partner1 = factory(\App\Partner::class)->create();
        $partner2 = factory(\App\Partner::class)->create();

        $response = $this->get('/dashboard/affiliate_program?affiliate_program_channel_list[]=' . $partner1->id . '&affiliate_program_channel_list[]=' . $partner2->id);

        $response->assertStatus(200)
                 ->assertViewIs('affiliate_program.list')
                 ->assertDontSee($affiliateProgram->name);

        $response = $this->get('/dashboard/affiliate_program?affiliate_program_channel_list[]=' . $partner->id . '&affiliate_program_channel_list[]=' . $partner2->id);

        $response->assertStatus(200)
                 ->assertViewIs('affiliate_program.list')
                 ->assertSee($affiliateProgram->name);
    }

    /** @test */
    public function can_view_pagination_page()
    {
        $perPage = 15;
        $pageCount = ceil(AffiliateProgram::where('draft', 0)->count() / $perPage);

        if ($pageCount > 1) {
            $response = $this->get('/dashboard/affiliate_program?page=' . $pageCount);

            $response->assertStatus(200)
                     ->assertViewIs('affiliate_program.list')
                     ->assertSee('affiliate_program?page=1')
                     ->assertDontSee('No Affiliate Programs Found');
        }
    }

    /** @test */
    public function can_not_view_pagination_page_if_one_page()
    {
        $perPage = 15;
        $page = ceil(AffiliateProgram::where('draft', 0)->count() / $perPage) + 1;

        $response = $this->get('/dashboard/affiliate_program?page=' . $page);

        $response->assertStatus(200)
                 ->assertViewIs('affiliate_program.list')
                 ->assertDontSee('affiliate_program?page=' . $page)
                 ->assertSee('No Affiliate Programs Found');
    }
}
