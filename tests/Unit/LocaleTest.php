<?php

namespace Tests\Unit;

use App\Admin;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class LocaleTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    protected $user;

    public function setUp()
    {
        parent::setUp();

        $user = Admin::where('email', 'info@bluloyalty.com')->first();
        $this->user = $user;
        $this->actingAs($user)
             ->session([
                'user_roles' => [
                    'BLU Admin',
                ],
                'user_permissions' => [
                    'create_admins',
                    'view_admins',
                    'edit_admins',
                    'delete_admins',
                    'view_roles',
                ],
            ]);
    }

    /** @test */
    public function can_get_areas_options_by_country_id()
    {
        $country = factory(\App\Country::class)->create();

        $area = factory(\App\Area::class)->create([
            'country_id' => $country->id,
        ]);

        $response = $this->get('/dashboard/locale/areas/' . $country->id);

        $response->assertStatus(200)
                 ->assertSeeText($area->name);
    }

    /** @test */
    public function can_get_cities_options_by_area_id()
    {
        $country = factory(\App\Country::class)->create();

        $area = factory(\App\Area::class)->create([
            'country_id' => $country->id,
        ]);

        $city = factory(\App\City::class)->create([
            'area_id' => $area->id,
        ]);

        $response = $this->get('/dashboard/locale/cities/' . $area->id);

        $response->assertStatus(200)
                 ->assertSeeText($city->name);
    }
}
