<?php

namespace Tests\Unit;

use App\Admin;
use App\Currency;
use App\Language;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CurrencyTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    protected $user;

    public function setUp()
    {
        parent::setUp();

        $user = Admin::where('email', 'info@bluloyalty.com')->first();
        $this->user = $user;
        $this->actingAs($user)
             ->session([
                 'user_roles' => [
                     'BLU Admin',
                 ],
                 'user_permissions' => [
                     'create_currencies',
                     'view_currencies',
                     'edit_currencies',
                     'delete_currencies',
                 ],
             ]);
    }

    /** @test */
    public function can_create_new_currency_page_with_permissions()
    {
        $response = $this->get('/dashboard/currencies/create');

        $response->assertStatus(302)
                 ->assertRedirect()
                 ->assertSeeText('/dashboard/currencies/edit/');
    }

    /** @test */
    public function can_not_create_new_currency_page_without_permissions()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $response = $this->get('/dashboard/currencies/create');

        $response->assertStatus(302)
                 ->assertRedirect()
                 ->assertSeeText('/dashboard');
    }

    /** @test */
    public function can_open_edit_currency_with_permissions()
    {
        $currency = factory(\App\Currency::class)->create();

        $response = $this->get('/dashboard/currencies/edit/' . base64_encode($currency->id));

        $response->assertStatus(200)
                 ->assertViewIs('currency.view');
    }

    /** @test */
    public function can_not_open_currency_page_without_permissions()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $currency = factory(\App\Currency::class)->create();

        $response = $this->get('/dashboard/currencies/edit/' . base64_encode($currency->id));

        $response->assertStatus(302)
                 ->assertRedirect()
                 ->assertSeeText('/dashboard');
    }

    /** @test */
    public function can_not_open_currency_page_which_is_not_exists()
    {
        $currency = factory(\App\Currency::class)->create();

        $response = $this->get('/dashboard/currencies/edit/' . base64_encode($currency->id + 10));

        $response->assertStatus(404);
    }

    /** @test */
    public function can_update_currency_with_permission()
    {
        $currency = factory(\App\Currency::class)->create();

        $data = [
            'name' => uniqid(),
            'short_code' => 'TES',
            'latest_rate' => 0.00,
        ];

        $response = $this->post('/dashboard/currencies/update/' . base64_encode($currency->id), $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/currencies');

        $this->assertDatabaseHas('currency', [
            'id' => $currency->id,
            'name' => $data['name'],
            'short_code' => $data['short_code'],
        ]);
    }

    /** @test */
    public function can_not_update_currency_without_permission()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $currency = factory(\App\Currency::class)->create();

        $data = [
            'name' => uniqid(),
            'short_code' => 'TES',
        ];

        $response = $this->post('/dashboard/currencies/update/' . base64_encode($currency->id), $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard');

        $this->assertDatabaseMissing('currency', [
            'id' => $currency->id,
            'name' => $data['name'],
            'short_code' => $data['short_code'],
        ]);
    }

    /** @test */
    public function can_not_update_currency_which_is_not_exists()
    {
        $currency = factory(\App\Currency::class)->create();

        $data = [
            'name' => uniqid(),
            'short_code' => 'TES',
        ];

        $response = $this->post('/dashboard/currencies/update/' . base64_encode($currency->id + 10), $data);

        $response->assertStatus(404);

        $this->assertDatabaseMissing('currency', [
            'id' => $currency->id,
            'name' => $data['name'],
            'short_code' => $data['short_code'],
        ]);
    }

    /** @test */
    public function can_not_update_currency_without_required_fields()
    {
        $currency = factory(\App\Currency::class)->create();

        $data = [];
        $response = $this->post('/dashboard/currencies/update/' . base64_encode($currency->id), $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/currencies/edit/' . base64_encode($currency->id))
                 ->assertSessionHasErrors(['name']);

        $data = [];
        $response = $this->post('/dashboard/currencies/update/' . base64_encode($currency->id), $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/currencies/edit/' . base64_encode($currency->id))
                 ->assertSessionHasErrors(['name', 'short_code']);
    }

    /** @test */
    public function can_delete_currency_with_permission()
    {
        $currency = factory(\App\Currency::class)->create();

        $response = $this->get('/dashboard/currencies/delete/' . base64_encode($currency->id));

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/currencies');

        $this->assertDatabaseMissing('currency', [
            'id' => $currency->id,
        ]);
    }

    /** @test */
    public function can_not_delete_currency_without_permission()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $currency = factory(\App\Currency::class)->create();

        $response = $this->get('/dashboard/currencies/delete/' . base64_encode($currency->id));

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard');

        $this->assertDatabaseHas('currency', [
            'id' => $currency->id,
        ]);
    }

    /** @test */
    public function can_not_delete_currency_which_is_not_exists()
    {
        $currency = factory(\App\Currency::class)->create();

        $response = $this->get('/dashboard/currencies/delete/' . base64_encode($currency->id + 10));

        $response->assertStatus(404);

        $this->assertDatabaseHas('currency', [
            'id' => $currency->id,
        ]);
    }

    /** @test */
    public function can_open_currencies_page()
    {
        factory(\App\Currency::class)->create();

        $response = $this->get('/dashboard/currencies');

        $response->assertStatus(200)
                 ->assertViewIs('currency.list')
                 ->assertViewHas('currencies');
    }

    /** @test */
    public function can_not_open_currencies_page_without_permission()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        factory(\App\Currency::class)->create();

        $response = $this->get('/dashboard/currencies');

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard');
    }

    /** @test */
    public function can_filter_currencies_by_search_query()
    {
        $currency = factory(\App\Currency::class)->create();

        $response = $this->get('/dashboard/currencies?q=' . uniqid());

        $response->assertStatus(200)
                 ->assertViewIs('currency.list')
                 ->assertDontSee($currency->name);

        $response = $this->get('/dashboard/currencies?q=' . $currency->name);

        $response->assertStatus(200)
                 ->assertViewIs('currency.list')
                 ->assertSee($currency->name);
    }

    /** @test */
    public function can_add_currency_translation()
    {
        $currency = factory(\App\Currency::class)->create();

        $response = $this->get('/dashboard/currencytranslation/new/' . $currency->id);

        $response->assertStatus(200)
                 ->assertViewIs('currencytranslation.form');
    }

    /** @test */
    public function can_save_currency_translation()
    {
        $currency = factory(\App\Currency::class)->create();
        $currencyTranslation = factory(\App\CurrencyTranslation::class)->create([
            'currency_id' => $currency->id,
        ]);
        $lang = factory(Language::class)->create();

        $data = [
            'currencytranslation_lang' => $lang->id,
            'currencytranslation_name' => uniqid(),
        ];

        $response = $this->post('/dashboard/currencytranslation/update/' . base64_encode($currencyTranslation->id), $data);

        $response->assertStatus(200)
                 ->assertJson([
                     'failed' => false,
                     'view_url' => url('dashboard/currencytranslation/list/' . base64_encode($currencyTranslation->currency_id)),
                 ]);

        $this->assertDatabaseHas('currency_translation', [
            'id' => $currencyTranslation->id,
            'lang_id' => $data['currencytranslation_lang'],
            'name' => $data['currencytranslation_name'],
        ]);
    }

    /** @test */
    public function can_not_save_currency_translation_without_required_fields()
    {
        $currency = factory(\App\Currency::class)->create();
        $currencyTranslation = factory(\App\CurrencyTranslation::class)->create([
            'currency_id' => $currency->id,
        ]);

        $data = [];

        $response = $this->post('/dashboard/currencytranslation/update/' . base64_encode($currencyTranslation->id), $data);

        $response->assertStatus(200)
                 ->assertJsonFragment([
                     'failed' => true,
                 ]);
    }

    /** @test */
    public function can_get_currency_translation_list()
    {
        $currency = factory(\App\Currency::class)->create();
        $currencyTranslation = factory(\App\CurrencyTranslation::class)->create([
            'currency_id' => $currency->id,
        ]);

        $response = $this->get('/dashboard/currencytranslation/list/' . base64_encode($currency->id));

        $response->assertStatus(200)
                 ->assertViewIs('currencytranslation.list')
                 ->assertSee($currencyTranslation->name);
    }

    /** @test */
    public function can_delete_currency_translation()
    {
        $currency = factory(\App\Currency::class)->create();
        $currencyTranslation = factory(\App\CurrencyTranslation::class)->create([
            'currency_id' => $currency->id,
        ]);

        $response = $this->get('/dashboard/currencytranslation/delete/' . base64_encode($currencyTranslation->id));

        $response->assertStatus(200)
                 ->assertJson([
                     'failed' => false,
                 ]);

        $this->assertDatabaseMissing('currency_translation', [
            'id' => $currencyTranslation->id,
            'name' => $currencyTranslation->name,
        ]);
    }

    /** @test */
    public function can_view_pagination_page()
    {
        $perPage = 15;
        $pageCount = ceil(Currency::where('draft', 0)->count() / $perPage);

        if ($pageCount > 1) {
            $response = $this->get('/dashboard/currencies?page=' . $pageCount);

            $response->assertStatus(200)
                     ->assertViewIs('currency.list')
                     ->assertSee('currencies?page=1')
                     ->assertDontSee('No Currencies Found');
        }
    }

    /** @test */
    public function can_not_view_pagination_page_if_one_page()
    {
        $perPage = 15;
        $page = ceil(Currency::where('draft', 0)->count() / $perPage) + 1;

        $response = $this->get('/dashboard/currencies?page=' . $page);

        $response->assertStatus(200)
                 ->assertViewIs('currency.list')
                 ->assertDontSee('currencies?page=' . $page)
                 ->assertSee('No Currencies Found');
    }
}
