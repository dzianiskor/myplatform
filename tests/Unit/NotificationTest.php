<?php

namespace Tests\Unit;

use App\Admin;
use App\Notification;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class NotificationTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    protected $user;

    public function setUp()
    {
        parent::setUp();

        $user = Admin::where('email', 'info@bluloyalty.com')->first();
        $this->user = $user;
        $this->actingAs($user)
             ->session([
                 'user_roles' => [
                     'BLU Admin',
                 ],
                 'user_permissions' => [
                     'create_notifications',
                     'view_notifications',
                     'edit_notifications',
                     'delete_notifications',
                 ],
             ]);
    }

    protected function createNotification($data = [])
    {
        $notification = factory(\App\Notification::class)->create(array_merge([
            'partner_id' => $this->user->partners()->first()->id,
            'created_by' => $this->user->id,
        ], $data));

        return $notification;
    }

    /** @test */
    public function can_create_new_notification_page_with_permissions()
    {
        $response = $this->get('/dashboard/notifications/new');

        $response->assertStatus(302)
                 ->assertRedirect()
                 ->assertSeeText('/dashboard/notifications/view/');
    }

    /** @test */
    public function can_not_create_new_notification_page_without_permissions()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $response = $this->get('/dashboard/notifications/new');

        $response->assertStatus(302)
                 ->assertRedirect()
                 ->assertSeeText('/dashboard');
    }

    /** @test */
    public function can_open_edit_notification_with_permissions()
    {
        $notification = $this->createNotification();

        $response = $this->get('/dashboard/notifications/view/' . base64_encode($notification->id));

        $response->assertStatus(200)
                 ->assertViewIs('notification.view');
    }

    /** @test */
    public function can_not_open_notification_page_which_is_not_exists()
    {
        $notification = $this->createNotification();

        $response = $this->get('/dashboard/notifications/view/' . base64_encode($notification->id + 10));

        $response->assertStatus(404);
    }

    /** @test */
    public function can_not_open_notification_page_which_is_not_draft_and_partner_can_not_view()
    {
        $partner = factory(\App\Partner::class)->create();
        $member = factory(\App\User::class)->create();
        $notification = $this->createNotification([
            'partner_id' => $partner->id,
            'created_by' => $member,
        ]);

        $response = $this->get('/dashboard/notifications/view/' . base64_encode($notification->id));

        $response->assertStatus(302)
                 ->assertRedirect()
                 ->assertSeeText('/dashboard');
    }

    /** @test */
    public function can_not_open_edit_notification_without_permissions()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $notification = $this->createNotification();

        $response = $this->get('/dashboard/notifications/view/' . base64_encode($notification->id));

        $response->assertStatus(302)
                 ->assertRedirect()
                 ->assertSeeText('/dashboard');
    }

    /** @test */
    public function can_update_notification_with_permissions()
    {
        $notification = $this->createNotification();

        $data = [
            'name' => uniqid(),
            'partner_id' => $notification->partner->id,
            'start_date' => Carbon::create()->addDay(),
        ];

        $response = $this->post('/dashboard/notifications/update/' . base64_encode($notification->id), $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/notifications')
                 ->assertSessionMissing('alert_error');

        $this->assertDatabaseHas('notification', [
            'id' => $notification->id,
            'name' => $data['name'],
        ]);
    }

    /** @test */
    public function can_not_update_notification_without_permission()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $notification = $this->createNotification();

        $data = [
            'name' => uniqid(),
            'partner_id' => $notification->partner->id,
            'start_date' => Carbon::create()->addDay(),
        ];

        $response = $this->post('/dashboard/notifications/update/' . base64_encode($notification->id), $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard');

        $this->assertDatabaseMissing('notification', [
            'id' => $notification->id,
            'name' => $data['name'],
        ]);
    }

    /** @test */
    public function can_not_update_notification_which_is_not_exists()
    {
        $notification = $this->createNotification();

        $data = [
            'name' => uniqid(),
            'partner_id' => $notification->partner->id,
            'start_date' => Carbon::create()->addDay(),
        ];

        $response = $this->post('/dashboard/notifications/update/' . base64_encode($notification->id + 10), $data);

        $response->assertStatus(404);

        $this->assertDatabaseMissing('notification', [
            'id' => $notification->id,
            'name' => $data['name'],
        ]);
    }

    /** @test */
    public function can_not_update_notification_without_required_fields()
    {
        $notification = $this->createNotification();

        $data = [];
        $response = $this->post('/dashboard/notifications/update/' . base64_encode($notification->id), $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/notifications/view/' .  base64_encode($notification->id))
                 ->assertSessionHasErrors(['name', 'partner_id', 'start_date']);

        $data = [
            'name' => uniqid(),
            'partner_id' => 'test',
            'start_date' => Carbon::create()->addDay(),
        ];
        $response = $this->post('/dashboard/notifications/update/' . base64_encode($notification->id), $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/notifications/view/' .  base64_encode($notification->id))
                 ->assertSessionHasErrors(['partner_id']);
    }

    /** @test */
    public function can_delete_notification_with_permission()
    {
        $notification = $this->createNotification();

        $response = $this->get('/dashboard/notifications/delete/' . base64_encode($notification->id));

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/notifications');

        $this->assertDatabaseMissing('notification', [
            'id' => $notification->id,
        ]);
    }

    /** @test */
    public function can_not_delete_notification_without_permission()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $notification = $this->createNotification();

        $response = $this->get('/dashboard/notifications/delete/' . base64_encode($notification->id));

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard');

        $this->assertDatabaseHas('notification', [
            'id' => $notification->id,
        ]);
    }

    /** @test */
    public function can_not_delete_notification_which_is_not_exists()
    {
        $notification = $this->createNotification();

        $data = [
            'name' => uniqid(),
        ];

        $response = $this->get('/dashboard/notifications/delete/' . base64_encode($notification->id + 10));

        $response->assertStatus(404);

        $this->assertDatabaseMissing('notification', [
            'id' => $notification->id,
            'name' => $data['name'],
        ]);

        $this->assertDatabaseHas('notification', [
            'id' => $notification->id,
        ]);
    }

    /** @test */
    public function can_filter_notifications_by_search_query()
    {
        $notification = $this->createNotification();

        $response = $this->get('/dashboard/notifications?q=' . uniqid());

        $response->assertStatus(200)
                 ->assertViewIs('notification.list')
                 ->assertDontSee($notification->name);

        $response = $this->get('/dashboard/notifications?q=' . $notification->name);

        $response->assertStatus(200)
                 ->assertViewIs('notification.list')
                 ->assertSee($notification->name);
    }

    /** @test */
    public function can_attach_segment()
    {
        $notification = $this->createNotification();
        $segment = factory(\App\Segment::class)->create();

        $data = [
            'segment_id' => base64_encode($segment->id),
        ];

        $response = $this->post('/dashboard/notifications/link/' . base64_encode($notification->id), $data);

        $response->assertStatus(200)
                 ->assertViewIs('notification.segments_list')
                 ->assertSee($segment->name);
    }

    /** @test */
    public function can_not_attach_segment_if_notification_not_found()
    {
        $notification = $this->createNotification();
        $segment = factory(\App\Segment::class)->create();

        $data = [
            'segment_id' => $segment->id,
        ];

        $response = $this->post('/dashboard/notifications/link/' . base64_encode($notification->id + 10), $data);

        $response->assertStatus(404);
    }

    /** @test */
    public function can_not_attach_segment_if_segment_not_found()
    {
        $notification = $this->createNotification();
        $segment = factory(\App\Segment::class)->create();

        $data = [
            'segment_id' => $segment->id + 10,
        ];

        $response = $this->post('/dashboard/notifications/link/' . base64_encode($notification->id), $data);

        $response->assertStatus(404);
    }

    /** @test */
    public function can_detach_segment()
    {
        $notification = $this->createNotification();
        $segment = factory(\App\Segment::class)->create();

        $notification->segments()->save($segment);

        $response = $this->get('/dashboard/notifications/unlink/' . base64_encode($notification->id) . '/' . base64_encode($segment->id));

        $response->assertStatus(200)
                 ->assertViewIs('notification.segments_list')
                 ->assertDontSee($segment->name);

        $this->assertDatabaseMissing('notification_segment', [
            'notification_id' => $notification->id,
            'segment_id' => $segment->id,
        ]);
    }

    /** @test */
    public function can_not_detach_segment_if_notification_not_found()
    {
        $notification = $this->createNotification();
        $segment = factory(\App\Segment::class)->create();

        $notification->segments()->save($segment);

        $response = $this->get('/dashboard/notifications/unlink/' . base64_encode($notification->id + 10) . '/' . base64_encode($segment->id));

        $response->assertStatus(404);
    }

    /** @test */
    public function can_not_detach_segment_if_segment_not_found()
    {
        $notification = $this->createNotification();
        $segment = factory(\App\Segment::class)->create();

        $notification->segments()->save($segment);

        $response = $this->get('/dashboard/notifications/unlink/' . base64_encode($notification->id) . '/' . base64_encode($segment->id + 10));

        $response->assertStatus(404);
    }

    /** @test */
    public function can_get_users_list_by_firstname()
    {
        $user = factory(\App\User::class)->create();

        $response = $this->get('/dashboard/reports/members?term=' . $user->first_name);

        $response->assertStatus(200)
                 ->assertSee($user->first_name);
    }

    /** @test */
    public function can_attach_user()
    {
        $notification = $this->createNotification();
        $user = factory(\App\User::class)->create();

        $data = [
            'user_id' => $user->id,
        ];

        $response = $this->post('/dashboard/notifications/linkuser/' . base64_encode($notification->id), $data);

        $response->assertStatus(200)
                 ->assertViewIs('notification.users_list')
                 ->assertSee($user->first_name);
    }

    /** @test */
    public function can_not_attach_user_if_notification_not_found()
    {
        $notification = $this->createNotification();
        $user = factory(\App\User::class)->create();

        $data = [
            'user_id' => $user->id,
        ];

        $response = $this->post('/dashboard/notifications/linkuser/' . base64_encode($notification->id + 10), $data);

        $response->assertStatus(404);
    }

    /** @test */
    public function can_not_attach_user_if_user_not_found()
    {
        $notification = $this->createNotification();
        $user = factory(\App\User::class)->create();

        $data = [
            'user_id' => $user->id + 10,
        ];

        $response = $this->post('/dashboard/notifications/linkuser/' . base64_encode($notification->id), $data);

        $response->assertStatus(404);
    }

    /** @test */
    public function can_detach_user()
    {
        $notification = $this->createNotification();
        $user = factory(\App\User::class)->create();

        $notification->users()->save($user);

        $response = $this->get('/dashboard/notifications/unlinkuser/' . base64_encode($notification->id) . '/' . base64_encode($user->id));

        $response->assertStatus(200)
                 ->assertViewIs('notification.users_list')
                 ->assertDontSee($user->first_name);

        $this->assertDatabaseMissing('notification_user', [
            'notification_id' => $notification->id,
            'user_id' => $user->id,
        ]);
    }

    /** @test */
    public function can_not_detach_user_if_notification_not_found()
    {
        $notification = $this->createNotification();
        $user = factory(\App\User::class)->create();

        $notification->users()->save($user);

        $response = $this->get('/dashboard/notifications/unlinkuser/' . base64_encode($notification->id + 10) . '/' . base64_encode($user->id));

        $response->assertStatus(404);
    }

    /** @test */
    public function can_not_detach_user_if_user_not_found()
    {
        $notification = $this->createNotification();
        $user = factory(\App\User::class)->create();

        $notification->users()->save($user);

        $response = $this->get('/dashboard/notifications/unlinkuser/' . base64_encode($notification->id) . '/' . base64_encode($user->id + 10));

        $response->assertStatus(404);
    }

    /** @test */
    public function can_view_pagination_page()
    {
        $perPage = 15;
        $pageCount = ceil(Notification::where('draft', 0)->count() / $perPage);

        if ($pageCount > 1) {
            $response = $this->get('/dashboard/notifications?page=' . $pageCount);

            $response->assertStatus(200)
                     ->assertViewIs('notification.list')
                     ->assertSee('notifications?page=1')
                     ->assertDontSee('No Notifications Found');
        }
    }

    /** @test */
    public function can_not_view_pagination_page_if_one_page()
    {
        $perPage = 15;
        $page = ceil(Notification::where('draft', 0)->count() / $perPage) + 1;

        $response = $this->get('/dashboard/notifications?page=' . $page);

        $response->assertStatus(200)
                 ->assertViewIs('notification.list')
                 ->assertDontSee('notifications?page=' . $page)
                 ->assertSee('No Notifications Found');
    }
}
