<?php

namespace Tests\Unit;

use App\Admin;
use App\Affiliate;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AffiliateTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    protected $user;

    public function setUp()
    {
        parent::setUp();

        $user = Admin::where('email', 'info@bluloyalty.com')->first();
        $this->user = $user;
        $this->actingAs($user)
             ->session([
                 'user_roles' => [
                     'BLU Admin',
                 ],
                 'user_permissions' => [
                     'add_affiliates',
                     'view_affiliates',
                     'edit_affiliates',
                     'delete_affiliates',
                 ],
             ]);
    }

    /**
     * @return Affiliate
     */
    protected function createAffiliate()
    {
        $country = factory(\App\Country::class)->create();
        $area = factory(\App\Area::class)->create([
            'country_id' => $country->id,
        ]);
        $city = factory(\App\City::class)->create([
            'area_id' => $area->id,
        ]);
        $category = factory(\App\Category::class)->create();

        $affiliate = factory(\App\Affiliate::class)->create([
            'country_id' => $country->id,
            'area_id' => $area->id,
            'city_id' => $city->id,
            'category_id' => $category->id,
        ]);
        $affiliate->partners()->save((\App\Partner::find(1))->first());

        return $affiliate;
    }

    /** @test */
    public function can_create_new_affiliate_page_with_permissions()
    {
        $response = $this->get('/dashboard/affiliates/new');

        $response->assertStatus(302)
                 ->assertRedirect()
                 ->assertSeeText('/dashboard/affiliates/view/');
    }

    /** @test */
    public function can_not_create_new_affiliate_page_without_permissions()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $response = $this->get('/dashboard/affiliates/new');

        $response->assertStatus(302)
                 ->assertRedirect()
                 ->assertSeeText('/dashboard');
    }

    /** @test */
    public function can_open_edit_affiliate_with_permission()
    {
        $affiliate = $this->createAffiliate();

        $response = $this->get('/dashboard/affiliates/view/' . base64_encode($affiliate->id));

        $response->assertStatus(200)
                 ->assertViewIs('affiliates.view');
    }

    /** @test */
    public function can_not_open_edit_affiliate_without_permissions()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $affiliate = $this->createAffiliate();

        $response = $this->get('/dashboard/affiliates/view/' . base64_encode($affiliate->id));

        $response->assertStatus(302)
                 ->assertRedirect()
                 ->assertSeeText('/dashboard');
    }

    /** @test */
    public function can_not_open_affiliate_page_which_is_not_exists()
    {
        $affiliate = $this->createAffiliate();

        $response = $this->get('/dashboard/affiliates/view/' . base64_encode($affiliate->id + 10));

        $response->assertStatus(404);
    }

    /** @test */
    public function can_update_affiliate_with_permission()
    {
        $affiliate = $this->createAffiliate();

        $country = factory(\App\Country::class)->create();

        $stub = __DIR__ . '\\..\\stubs\\test.png';
        $name = str_random(8) . '.png';
        $path = sys_get_temp_dir() . '\\' . $name;

        copy($stub, $path);

        $file = new UploadedFile($path, $name, filesize($path), 'image/png', null, true);

        $data = [
            'name' => uniqid(),
            'country_id' => $country->id,
        ];

        $response = $this->call('POST', '/dashboard/affiliates/update/' . base64_encode($affiliate->id), $data, [], ['image' => $file], ['Accept' => 'application/json']);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/affiliates')
                 ->assertSessionMissing('alert_error');

        $this->assertDatabaseHas('affiliate', [
            'id' => $affiliate->id,
            'name' => $data['name'],
        ]);

        $affiliate1 = \App\Affiliate::find(['id' => $affiliate->id])->first();

        $this->assertFileExists(public_path(\App\Http\Controllers\MediaController::getImageFromId($affiliate1->image)));

        @unlink(public_path(\App\Http\Controllers\MediaController::getImageFromId($affiliate1->image)));
    }

    /** @test */
    public function can_not_update_affiliate_without_permission()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $affiliate = $this->createAffiliate();

        $data = [
            'name' => uniqid(),
        ];

        $response = $this->post('/dashboard/affiliates/update/' . base64_encode($affiliate->id), $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard');

        $this->assertDatabaseMissing('affiliate', [
            'id' => $affiliate->id,
            'name' => $data['name'],
        ]);
    }

    /** @test */
    public function can_not_update_affiliate_which_is_not_exists()
    {
        $affiliate = $this->createAffiliate();

        $data = [
            'name' => uniqid(),
        ];

        $response = $this->post('/dashboard/affiliates/update/' . base64_encode($affiliate->id + 10), $data);

        $response->assertStatus(404);

        $this->assertDatabaseMissing('affiliate', [
            'id' => $affiliate->id,
            'name' => $data['name'],
        ]);
    }

    /** @test */
    public function can_not_update_affiliate_without_required_fields()
    {
        $affiliate = $this->createAffiliate();

        $data = [];
        $response = $this->post('/dashboard/affiliates/update/' . base64_encode($affiliate->id), $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/affiliates/view/' . base64_encode($affiliate->id))
                 ->assertSessionHasErrors(['name', 'country_id']);

        $data = [
            'name' => 'q',
        ];
        $response = $this->post('/dashboard/affiliates/update/' . base64_encode($affiliate->id), $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/affiliates/view/' . base64_encode($affiliate->id))
                 ->assertSessionHasErrors(['name', 'country_id']);
    }

    /** @test */
    public function can_delete_affiliate_with_permission()
    {
        $affiliate = $this->createAffiliate();

        $response = $this->get('/dashboard/affiliates/delete/' . base64_encode($affiliate->id));

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/affiliates');

        $this->assertDatabaseMissing('affiliate', [
            'id' => $affiliate->id,
        ]);
    }

    /** @test */
    public function can_not_delete_affiliate_without_permission()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $affiliate = $this->createAffiliate();

        $response = $this->get('/dashboard/affiliates/delete/' . base64_encode($affiliate->id));

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard');

        $this->assertDatabaseHas('affiliate', [
            'id' => $affiliate->id,
        ]);
    }

    /** @test */
    public function can_not_delete_affiliate_which_is_not_exists()
    {
        $affiliate = $this->createAffiliate();

        $response = $this->get('/dashboard/affiliates/delete/' . base64_encode($affiliate->id + 10));

        $response->assertStatus(404);

        $this->assertDatabaseHas('affiliate', [
            'id' => $affiliate->id,
        ]);
    }

    /** @test */
    public function can_attach_partner()
    {
        $affiliate = $this->createAffiliate();
        $partner = factory(\App\Partner::class)->create();

        $data = [
            'partner_id' => $partner->id,
        ];

        $response = $this->post('/dashboard/affiliates/attachaffiliate/' . base64_encode($affiliate->id), $data);

        $response->assertStatus(200)
                 ->assertViewIs('affiliates.partnerlist')
                 ->assertSee($partner->name);
    }

    /** @test */
    public function can_not_attach_partner_if_affiliate_not_found()
    {
        $affiliate = $this->createAffiliate();
        $partner = factory(\App\Partner::class)->create();

        $data = [
            'partner_id' => $partner->id,
        ];

        $response = $this->post('/dashboard/affiliates/attachaffiliate/' . base64_encode($affiliate->id + 10), $data);

        $response->assertStatus(404);
    }

    /** @test */
    public function can_not_attach_partner_if_partner_not_found()
    {
        $affiliate = $this->createAffiliate();
        $partner = factory(\App\Partner::class)->create();

        $data = [
            'partner_id' => $partner->id + 10,
        ];

        $response = $this->post('/dashboard/affiliates/attachaffiliate/' . base64_encode($affiliate->id), $data);

        $response->assertStatus(404);
    }

    /** @test */
    public function can_detach_partner()
    {
        $affiliate = $this->createAffiliate();
        $partner = factory(\App\Partner::class)->create();

        $affiliate->partners()->save($partner);

        $response = $this->get('/dashboard/affiliates/unlinkaffiliate/' . base64_encode($affiliate->id) . '/' . base64_encode($partner->id));

        $response->assertStatus(200)
                 ->assertViewIs('affiliates.partnerlist')
                 ->assertDontSee($partner->name);

        $this->assertDatabaseMissing('affiliate_partner', [
            'affiliate_id' => $affiliate->id,
            'partner_id' => $partner->id,
        ]);
    }

    /** @test */
    public function can_not_detach_partner_if_affiliate_not_found()
    {
        $affiliate = $this->createAffiliate();
        $partner = factory(\App\Partner::class)->create();

        $affiliate->partners()->save($partner);

        $response = $this->get('/dashboard/affiliates/unlinkaffiliate/' . base64_encode($affiliate->id + 10) . '/' . base64_encode($partner->id));

        $response->assertStatus(404);
    }

    /** @test */
    public function can_not_detach_partner_if_partner_not_found()
    {
        $affiliate = $this->createAffiliate();
        $partner = factory(\App\Partner::class)->create();

        $affiliate->partners()->save($partner);

        $response = $this->get('/dashboard/affiliates/unlinkaffiliate/' . base64_encode($affiliate->id) . '/' . base64_encode($partner->id + 10));

        $response->assertStatus(404);
    }

    /** @test */
    public function can_filter_affiliates_by_search_query()
    {
        $affiliate = $this->createAffiliate();

        $response = $this->get('/dashboard/affiliates?q=' . uniqid());

        $response->assertStatus(200)
                 ->assertViewIs('affiliates.list')
                 ->assertDontSee($affiliate->name);

        $response = $this->get('/dashboard/affiliates?q=' . $affiliate->name);

        $response->assertStatus(200)
                 ->assertViewIs('affiliates.list')
                 ->assertSee($affiliate->name);
    }

    /** @test */
    public function can_filter_affiliates_by_country()
    {
        $affiliate = $this->createAffiliate();

        $country1 = factory(\App\Country::class)->create();
        $country2 = factory(\App\Country::class)->create();

        $response = $this->get('/dashboard/affiliates?affiliate_country_list[]=' . $country1->id . '&affiliate_country_list[]=' . $country2->id);

        $response->assertStatus(200)
                 ->assertViewIs('affiliates.list')
                 ->assertDontSee($affiliate->name);

        $response = $this->get('/dashboard/affiliates?affiliate_country_list[]=' . $affiliate->country_id . '&affiliate_country_list[]=' . $country2->id);

        $response->assertStatus(200)
                 ->assertViewIs('affiliates.list')
                 ->assertSee($affiliate->name);
    }

    /** @test */
    public function can_filter_affiliates_by_category()
    {
        $affiliate = $this->createAffiliate();

        $category1 = factory(\App\Category::class)->create();
        $category2 = factory(\App\Category::class)->create();

        $response = $this->get('/dashboard/affiliates?affiliate_category_list[]=' . $category1->id . '&affiliate_category_list[]=' . $category2->id);

        $response->assertStatus(200)
                 ->assertViewIs('affiliates.list')
                 ->assertDontSee($affiliate->name);

        $response = $this->get('/dashboard/affiliates?affiliate_category_list[]=' . $affiliate->category_id . '&affiliate_category_list[]=' . $category2->id);

        $response->assertStatus(200)
                 ->assertViewIs('affiliates.list')
                 ->assertSee($affiliate->name);
    }

    /** @test */
    public function can_view_pagination_page()
    {
        $perPage = 15;
        $pageCount = ceil(Affiliate::where('draft', 0)->count() / $perPage);

        if ($pageCount > 1) {
            $response = $this->get('/dashboard/affiliates?page=' . $pageCount);

            $response->assertStatus(200)
                     ->assertViewIs('affiliates.list')
                     ->assertSee('affiliates?page=1')
                     ->assertDontSee('No Affiliates Found');
        }
    }

    /** @test */
    public function can_not_view_pagination_page_if_one_page()
    {
        $perPage = 15;
        $page = ceil(Affiliate::where('draft', 0)->count() / $perPage) + 1;

        $response = $this->get('/dashboard/affiliates?page=' . $page);

        $response->assertStatus(200)
                 ->assertViewIs('affiliates.list')
                 ->assertDontSee('affiliates?page=' . $page)
                 ->assertSee('No Affiliates Found');
    }
}