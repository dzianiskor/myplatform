<?php

namespace Tests\Unit;

use App\Admin;
use App\User;
use Faker\Generator;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class MemberTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    protected $user;

    public function setUp()
    {
        parent::setUp();

        $user = Admin::where('email', 'info@bluloyalty.com')->first();
        $this->user = $user;
        $this->actingAs($user)
             ->session([
                 'user_roles' => [
                     'BLU Admin',
                 ],
                 'user_permissions' => [
                     'create_members',
                     'view_members',
                     'edit_members',
                     'delete_members',
                     'view_roles',
                 ],
             ]);
    }

    /**
     * @return User
     */
    protected function createMember()
    {
        $country = factory(\App\Country::class)->create();
        $area = factory(\App\Area::class)->create([
            'country_id' => $country->id,
        ]);
        $city = factory(\App\City::class)->create([
            'area_id' => $area->id,
        ]);

        $member = factory(\App\User::class)->create([
            'country_id' => $country->id,
            'area_id' => $area->id,
            'city_id' => $city->id,
        ]);
        $member->partners()->save((\App\Partner::find(1))->first());

        return $member;
    }

    /** @test */
    public function can_create_new_member_page()
    {
        $response = $this->get('/dashboard/members/new');

        $response->assertStatus(302)
                 ->assertRedirect()
                 ->assertSeeText('/dashboard/members/view/');
    }

    /** @test */
    public function can_not_create_new_member_page_without_permissions()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $response = $this->get('/dashboard/members/new');

        $response->assertStatus(302)
                 ->assertRedirect()
                 ->assertSeeText('/dashboard');

    }

//    /** @test */
//    public function can_view_member()
//    {
//        $this->createMember();
//
//        $response = $this->get('/dashboard/members/view/' . base64_encode($member->id));
//
//        $response->assertStatus(200)
//            ->assertViewIs('member.view');
//    }

    /** @test */
    public function can_update_member()
    {
        $member = $this->createMember();

        $stub = __DIR__ . '\\..\\stubs\\test.png';
        $name = str_random(8) . '.png';
        $path = sys_get_temp_dir() . '\\' . $name;

        copy($stub, $path);

        $file = new UploadedFile($path, $name, filesize($path), 'image/png', null, true);

        $data = [
            'first_name' => uniqid(),
            'last_name' => uniqid(),
            'mobile' => '+97112345678',
        ];

        $response = $this->call('POST', '/dashboard/members/update/' . base64_encode($member->id), $data, [], ['image' => $file], ['Accept' => 'application/json']);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/members');

        $member1 = \App\User::find(['id' => $member->id])->first();

        $this->assertEquals($member1->first_name, $data['first_name']);
        $this->assertEquals($member1->last_name, $data['last_name']);
        $this->assertEquals($member1->mobile, $data['mobile']);

        $this->assertFileExists(public_path(\App\Http\Controllers\MediaController::getImageFromId($member1->profile_image)));

        @unlink(public_path(\App\Http\Controllers\MediaController::getImageFromId($member1->profile_image)));
    }

    /** @test */
    public function can_not_update_member_without_permission()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $member = $this->createMember();

        $data = [
            'first_name' => uniqid(),
            'last_name' => uniqid(),
            'mobile' => '+97112345678',
        ];

        $response = $this->post('/dashboard/members/update/' . base64_encode($member->id), $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard');

        $member1 = \App\User::find(['id' => $member->id])->first();

        $this->assertNotEquals($member1->first_name, $data['first_name']);
        $this->assertNotEquals($member1->last_name, $data['last_name']);
        $this->assertNotEquals($member1->mobile, $data['mobile']);
    }

    /** @test */
    public function can_delete_member()
    {
        $member = $this->createMember();

        $response = $this->get('/dashboard/members/delete/' . base64_encode($member->id));

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/members');

        $this->assertDatabaseMissing('users', [
            'id' => $member->id,
            'deleted_at' => null
        ]);
    }

    /** @test */
    public function can_not_delete_member_without_permission()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $member = $this->createMember();

        $response = $this->get('/dashboard/members/delete/' . base64_encode($member->id));

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard');

        $this->assertDatabaseHas('users', [
            'id' => $member->id,
            'deleted_at' => null
        ]);
    }

    /** @test */
    public function can_open_members_page()
    {
        $member = $this->createMember();

        $response = $this->get('/dashboard/members');

        $response->assertStatus(200)
                 ->assertViewIs('member.list')
                 ->assertViewHas('members');
    }

    /** @test */
    public function can_not_open_members_page_without_permission()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $member = $this->createMember();

        $response = $this->get('/dashboard/members');

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard');
    }

    /** @test */
    public function can_attach_type()
    {
        $member = $this->createMember();

        $role = factory(\App\Role::class)->create();

        $response = $this->post('/dashboard/roles/attach/' . base64_encode($member->id), ['role_id' => $role->id]);

        $response->assertStatus(200)
                 ->assertSeeText($role->name);

        $this->assertDatabaseHas('role_user', [
            'role_id' => $role->id,
            'user_id' => $member->id,
        ]);
    }

    /** @test */
    public function can_unlink_type()
    {
        $member = $this->createMember();
        $member->roles()->save(factory(\App\Role::class)->create());

        $role = $member->roles->first();

        $response = $this->get('/dashboard/roles/unlink/' . base64_encode($member->id) . '/' . base64_encode($role->id));

        $response->assertStatus(200)
                 ->assertDontSeeText($role->name);

        $this->assertDatabaseMissing('role_user', [
            'role_id' => $role->id,
            'user_id' => $member->id,
        ]);
    }

    /** @test */
    public function can_filter_members_by_search_query()
    {
        $member = $this->createMember();

        $response = $this->get('/dashboard/members?q=' . uniqid());

        $response->assertStatus(200)
                 ->assertViewIs('member.list')
                 ->assertDontSee($member->last_name);

        $response = $this->get('/dashboard/members?q=' . $member->last_name);

        $response->assertStatus(200)
                 ->assertViewIs('member.list')
                 ->assertSee($member->last_name);
    }

    /** @test */
    public function can_filter_members_by_country()
    {
        $member = $this->createMember();

        $country1 = factory(\App\Country::class)->create();
        $country2 = factory(\App\Country::class)->create();

        $response = $this->get('/dashboard/members?country_member_list[]=' . $country1->id . '&country_member_list[]=' . $country2->id);

        $response->assertStatus(200)
                 ->assertViewIs('member.list')
                 ->assertDontSee($member->last_name);

        $response = $this->get('/dashboard/members?country_member_list[]=' . $member->country_id . '&country_member_list[]=' . $country2->id);

        $response->assertStatus(200)
                 ->assertViewIs('member.list')
                 ->assertSee($member->last_name);
    }

    /** @test */
    public function can_filter_members_by_segment()
    {
        $member = $this->createMember();
        $member->segments()->save(factory(\App\Segment::class)->create());

        $segment1 = factory(\App\Segment::class)->create();
        $segment2 = factory(\App\Segment::class)->create();

        $response = $this->get('/dashboard/members?segment_list[]=' . $segment1->id . '&segment_list[]=' . $segment2->id);

        $response->assertStatus(200)
                 ->assertViewIs('member.list')
                 ->assertDontSee($member->last_name);

        $response = $this->get('/dashboard/members?segment_list[]=' . $member->segments()
                                                                             ->first()->id . '&segment_list[]=' . $segment2->id);

        $response->assertStatus(200)
                 ->assertViewIs('member.list')
                 ->assertSee($member->last_name);
    }
}
