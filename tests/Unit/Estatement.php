<?php

namespace Tests\Unit;

use App\Address;
use App\Admin;
use App\Estatement;
use App\Segment;
use Faker\Factory;
use Faker\Generator;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class EstatementTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    protected $user;

    public function setUp()
    {
        parent::setUp();

        $user = Admin::where('email', 'info@bluloyalty.com')->first();
        $this->user = $user;
        $this->actingAs($user)
             ->session([
                 'user_roles' => [
                     'BLU Admin',
                 ],
                 'user_permissions' => [
                     'create_estatement',
                     'view_estatement',
                     'edit_estatement',
                     'delete_estatement',
                 ],
             ]);
    }

    /** @test */
    public function can_create_new_estatement_page_with_permissions()
    {
        $response = $this->get('/dashboard/estatement/new');

        $response->assertStatus(302)
                 ->assertRedirect()
                 ->assertSeeText('/dashboard/estatement/view/');
    }

    /** @test */
    public function can_not_create_new_estatement_page_without_permissions()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $response = $this->get('/dashboard/estatement/new');

        $response->assertStatus(302)
                 ->assertRedirect()
                 ->assertSeeText('/dashboard');
    }

    /**
     * @return Estatement
     */
    protected function createEstatement()
    {
        $estatement = factory(\App\Estatement::class)->create([
            'partner_id' => 1,
        ]);

        return $estatement;
    }

    /** @test */
    public function can_open_edit_estatement_with_permissions()
    {
        $estatement = $this->createEstatement();

        $response = $this->get('/dashboard/estatement/view/' . base64_encode($estatement->id));

        $response->assertStatus(200)
                 ->assertViewIs('estatement.view');
    }

    /** @test */
    public function can_not_open_edit_estatement_which_is_not_exists()
    {
        $estatement = $this->createEstatement();

        $response = $this->get('/dashboard/estatement/view/' . base64_encode($estatement->id + 10));

        $response->assertStatus(404);
    }

    /** @test */
    public function can_not_open_estatement_page_which_is_not_draft_and_partner_can_not_view()
    {
        $partner = factory(\App\Partner::class)->create();
        $estatement = factory(\App\Estatement::class)->create([
            'partner_id' => $partner->id
        ]);

        $response = $this->get('/dashboard/estatement/view/' . base64_encode($estatement->id));

        $response->assertStatus(302)
                 ->assertRedirect()
                 ->assertSeeText('/dashboard');
    }

    /** @test */
    public function can_not_open_edit_estatement_without_permissions()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $estatement = $this->createEstatement();

        $response = $this->get('/dashboard/estatement/view/' . base64_encode($estatement->id));

        $response->assertStatus(302)
                 ->assertRedirect()
                 ->assertSeeText('/dashboard');
    }

    /** @test */
    public function can_update_estatement_with_permissions()
    {
        $estatement = $this->createEstatement();

        $category = factory(\App\Category::class)->create();

        $data = [
            'title' => uniqid(),
        ];

        $response = $this->post('/dashboard/estatement/update/' . base64_encode($estatement->id), $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/estatement')
                 ->assertSessionMissing('alert_error');

        $this->assertDatabaseHas('estatement', [
            'id' => $estatement->id,
            'title' => $data['title'],
        ]);
    }

    /** @test */
    public function can_not_update_estatement_without_permission()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $estatement = $this->createEstatement();
        $category = factory(\App\Category::class)->create();

        $data = [
            'title' => uniqid(),
        ];

        $response = $this->post('/dashboard/estatement/update/' . base64_encode($estatement->id), $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard');

        $this->assertDatabaseMissing('estatement', [
            'id' => $estatement->id,
            'title' => $data['title'],
        ]);
    }

    /** @test */
    public function can_not_update_estatement_which_is_not_exists()
    {
        $estatement = $this->createEstatement();
        $category = factory(\App\Category::class)->create();

        $data = [
            'title' => uniqid(),
        ];

        $response = $this->post('/dashboard/estatement/update/' . base64_encode($estatement->id + 10), $data);

        $response->assertStatus(404);

        $this->assertDatabaseMissing('estatement', [
            'id' => $estatement->id,
            'title' => $data['title'],
        ]);
    }

    /** @test */
    public function can_not_update_estatement_without_required_fields()
    {
        $estatement = $this->createEstatement();

        $data = [];
        $response = $this->post('/dashboard/estatement/update/' . base64_encode($estatement->id), $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/estatement/view/' . base64_encode($estatement->id))
                 ->assertSessionHasErrors(['title']);
    }

    /** @test */
    public function can_delete_estatement_with_permission()
    {
        $estatement = $this->createEstatement();

        $response = $this->get('/dashboard/estatement/delete/' . base64_encode($estatement->id));

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/estatement');

        $this->assertDatabaseMissing('estatement', [
            'id' => $estatement->id,
            'deleted' => false,
        ]);
    }

    /** @test */
    public function can_not_delete_estatement_without_permission()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $estatement = $this->createEstatement();

        $response = $this->get('/dashboard/estatement/delete/' . base64_encode($estatement->id));

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard');

        $this->assertDatabaseHas('estatement', [
            'id' => $estatement->id,
        ]);
    }

    /** @test */
    public function can_not_delete_estatement_which_is_not_exists()
    {
        $estatement = $this->createEstatement();

        $data = [
            'title' => uniqid(),
        ];

        $response = $this->get('/dashboard/estatement/delete/' . base64_encode($estatement->id + 10));

        $response->assertStatus(404);

        $this->assertDatabaseMissing('estatement', [
            'id' => $estatement->id,
            'title' => $data['title'],
        ]);

        $this->assertDatabaseHas('estatement', [
            'id' => $estatement->id,
        ]);
    }

    /** @test */
    public function can_filter_estatement_by_search_query()
    {
        $estatement = $this->createEstatement();

        $response = $this->get('/dashboard/estatement?q=' . uniqid());

        $response->assertStatus(200)
                 ->assertViewIs('estatement.list')
                 ->assertDontSee($estatement->title);

        $response = $this->get('/dashboard/estatement?q=' . $estatement->title);

        $response->assertStatus(200)
                 ->assertViewIs('estatement.list')
                 ->assertSee($estatement->title);
    }

    /** @test */
    public function can_load_estatement_translation_form()
    {
        $estatement = $this->createEstatement();

        $response = $this->get('/dashboard/estatementtranslation/new/' . base64_encode($estatement->id));

        $response->assertStatus(200)
                 ->assertViewIs('estatementtranslation.new');
    }

    /** @test */
    public function can_save_estatement_translation()
    {
        $faker = Factory::create();

        $estatement = $this->createEstatement();
        $estatementTranslation = factory(\App\Estatementtranslation::class)->create([
            'estatement_id' => $estatement->id,
        ]);

        $data = [
            'estatementtranslation_eid' => $estatement->id,
            'estatementtranslation_lang' => $estatementTranslation->lang_id,
            'estatementtranslation_Title' => $faker->text(20),
            'estatementtranslation_description' => $faker->text(100),
        ];

        $response = $this->post('/dashboard/estatementtranslation/create', $data);

        $response->assertStatus(200)
                 ->assertJson([
                     'failed' => false,
                     'view_url' => url('dashboard/estatementtranslation/list/' . base64_encode($estatementTranslation->estatement_id)),
                 ]);

        $this->assertDatabaseHas('estatement_translation', [
            'Title' => $data['estatementtranslation_Title'],
            'estatement_id' => $estatement->id,
        ]);
    }

    /** @test */
    public function can_not_save_estatement_translation_without_required_fields()
    {
        $estatement = $this->createEstatement();
        $estatementTranslation = factory(\App\Estatementtranslation::class)->create([
            'estatement_id' => $estatement->id,
        ]);

        $data = [];

        $response = $this->post('/dashboard/estatementtranslation/update/' . $estatementTranslation->id, $data);

        $response->assertStatus(200)
                 ->assertJsonFragment([
                     'failed' => true,
                 ]);
    }

    /** @test */
    public function can_get_list_estatement_translation()
    {
        $estatement = $this->createEstatement();
        $estatementTranslation = factory(\App\Estatementtranslation::class)->create([
            'estatement_id' => $estatement->id,
        ]);

        $response = $this->get('/dashboard/estatementtranslation/list/' . base64_encode($estatement->id));

        $response->assertStatus(200)
                 ->assertViewIs('estatementtranslation.list')
                 ->assertSee($estatementTranslation->Title);
    }

    /** @test */
    public function can_delete_estatement_translation()
    {
        $estatement = $this->createEstatement();
        $estatementTranslation = factory(\App\Estatementtranslation::class)->create([
            'estatement_id' => $estatement->id,
        ]);

        $response = $this->get('/dashboard/estatementtranslation/delete/' . base64_encode($estatementTranslation->id));

        $response->assertStatus(200)
                 ->assertJson([
                     'failed' => false,
                 ]);

        $this->assertDatabaseMissing('estatement_translation', [
            'Title' => $estatementTranslation->Title,
        ]);
    }

    /** @test */
    public function can_attach_user()
    {
        $estatement = $this->createEstatement();
        $user = factory(\App\User::class)->create();

        $data = [
            'user_id' => $user->id,
        ];

        $response = $this->post('/dashboard/estatement/linkuser/' . base64_encode($estatement->id), $data);

        $response->assertStatus(200)
                 ->assertViewIs('estatement.list.userList')
                 ->assertSee($user->name);
    }

    /** @test */
    public function can_not_attach_user_if_estatement_not_found()
    {
        $estatement = $this->createEstatement();
        $user = factory(\App\User::class)->create();

        $data = [
            'user_id' => $user->id,
        ];

        $response = $this->post('/dashboard/estatement/linkuser/' . base64_encode($estatement->id + 10), $data);

        $response->assertStatus(404);
    }

    /** @test */
    public function can_not_attach_user_if_user_not_found()
    {
        $estatement = $this->createEstatement();
        $user = factory(\App\User::class)->create();

        $data = [
            'user_id' => $user->id + 10,
        ];

        $response = $this->post('/dashboard/estatement/linkuser/' . base64_encode($estatement->id), $data);

        $response->assertStatus(404);
    }

    /** @test */
    public function can_detach_user()
    {
        $estatement = $this->createEstatement();
        $user = factory(\App\User::class)->create();

        $estatement->users()->save($user);

        $response = $this->get('/dashboard/estatement/unlinkuser/' . base64_encode($estatement->id) . '/' . base64_encode($user->id));

        $response->assertStatus(200)
                 ->assertViewIs('estatement.list.userList')
                 ->assertDontSee($user->name);

        $this->assertDatabaseMissing('estatement_user', [
            'estatement_id' => $estatement->id,
            'user_id' => $user->id,
        ]);
    }

    /** @test */
    public function can_not_detach_user_if_estatement_not_found()
    {
        $estatement = $this->createEstatement();
        $user = factory(\App\User::class)->create();

        $estatement->users()->save($user);

        $response = $this->get('/dashboard/estatement/unlinkuser/' . base64_encode($estatement->id + 10) . '/' . base64_encode($user->id));

        $response->assertStatus(404);
    }

    /** @test */
    public function can_not_detach_user_if_user_not_found()
    {
        $estatement = $this->createEstatement();
        $user = factory(\App\User::class)->create();

        $estatement->users()->save($user);

        $response = $this->get('/dashboard/estatement/unlinkuser/' . base64_encode($estatement->id) . '/' . base64_encode($user->id + 10));

        $response->assertStatus(404);
    }

    /** @test */
    public function can_attach_segment()
    {
        $estatement = $this->createEstatement();
        $segment = factory(\App\Segment::class)->create();

        $data = [
            'segment_id' => $segment->id,
        ];

        $response = $this->post('/dashboard/estatement/link_segment/' . base64_encode($estatement->id), $data);

        $response->assertStatus(200)
                 ->assertViewIs('estatement.list.segmentList')
                 ->assertSee($segment->name);
    }

    /** @test */
    public function can_not_attach_segment_if_estatement_not_found()
    {
        $estatement = $this->createEstatement();
        $segment = factory(\App\Segment::class)->create();

        $data = [
            'segment_id' => $segment->id,
        ];

        $response = $this->post('/dashboard/estatement/link_segment/' . base64_encode($estatement->id + 10), $data);

        $response->assertStatus(404);
    }

    /** @test */
    public function can_not_attach_segment_if_segment_not_found()
    {
        $estatement = $this->createEstatement();
        $segment = factory(\App\Segment::class)->create();

        $data = [
            'segment_id' => $segment->id + 10,
        ];

        $response = $this->post('/dashboard/estatement/link_segment/' . base64_encode($estatement->id), $data);

        $response->assertStatus(404);
    }

    /** @test */
    public function can_detach_segment()
    {
        $estatement = $this->createEstatement();
        $segment = factory(\App\Segment::class)->create();

        $estatement->segments()->save($segment);

        $response = $this->get('/dashboard/estatement/unlink_segment/' . base64_encode($estatement->id) . '/' . base64_encode($segment->id));

        $response->assertStatus(200)
                 ->assertViewIs('estatement.list.segmentList')
                 ->assertDontSee($segment->name);

        $this->assertDatabaseMissing('estatement_segment', [
            'estatement_id' => $estatement->id,
            'segment_id' => $segment->id,
        ]);
    }

    /** @test */
    public function can_not_detach_segment_if_estatement_not_found()
    {
        $estatement = $this->createEstatement();
        $segment = factory(\App\Segment::class)->create();

        $estatement->segments()->save($segment);

        $response = $this->get('/dashboard/estatement/unlink_segment/' . base64_encode($estatement->id + 10) . '/' . base64_encode($segment->id));

        $response->assertStatus(404);
    }

    /** @test */
    public function can_not_detach_segment_if_segment_not_found()
    {
        $estatement = $this->createEstatement();
        $segment = factory(\App\Segment::class)->create();

        $estatement->segments()->save($segment);

        $response = $this->get('/dashboard/estatement/unlink_segment/' . base64_encode($estatement->id) . '/' . base64_encode($segment->id + 10));

        $response->assertStatus(404);
    }

    /** @test */
    public function can_attach_country()
    {
        $estatement = $this->createEstatement();
        $country = factory(\App\Country::class)->create();

        $data = [
            'country_id' => $country->id,
        ];

        $response = $this->post('/dashboard/estatement/link_country/' . base64_encode($estatement->id), $data);

        $response->assertStatus(200)
                 ->assertViewIs('estatement.list.countryList')
                 ->assertSee($country->name);
    }

    /** @test */
    public function can_not_attach_country_if_estatement_not_found()
    {
        $estatement = $this->createEstatement();
        $country = factory(\App\Segment::class)->create();

        $data = [
            'country_id' => $country->id,
        ];

        $response = $this->post('/dashboard/estatement/link_country/' . base64_encode($estatement->id + 10), $data);

        $response->assertStatus(404);
    }

    /** @test */
    public function can_not_attach_country_if_country_not_found()
    {
        $estatement = $this->createEstatement();
        $country = factory(\App\Segment::class)->create();

        $data = [
            'country_id' => $country->id + 10,
        ];

        $response = $this->post('/dashboard/estatement/link_country/' . base64_encode($estatement->id), $data);

        $response->assertStatus(404);
    }

    /** @test */
    public function can_detach_country()
    {
        $estatement = $this->createEstatement();
        $country = factory(\App\Country::class)->create();

        $estatement->countries()->save($country);

        $response = $this->get('/dashboard/estatement/unlink_country/' . base64_encode($estatement->id) . '/' . base64_encode($country->id));

        $response->assertStatus(200)
                 ->assertViewIs('estatement.list.countryList')
                 ->assertDontSee($country->name);

        $this->assertDatabaseMissing('country_estatement', [
            'estatement_id' => $estatement->id,
            'country_id' => $country->id,
        ]);
    }

    /** @test */
    public function can_not_detach_country_if_estatement_not_found()
    {
        $estatement = $this->createEstatement();
        $country = factory(\App\Country::class)->create();

        $estatement->countries()->save($country);

        $response = $this->get('/dashboard/estatement/unlink_country/' . base64_encode($estatement->id + 10) . '/' . base64_encode($country->id));

        $response->assertStatus(404);
    }

    /** @test */
    public function can_not_detach_country_if_country_not_found()
    {
        $estatement = $this->createEstatement();
        $country = factory(\App\Country::class)->create();

        $estatement->countries()->save($country);

        $response = $this->get('/dashboard/estatement/unlink_country/' . base64_encode($estatement->id) . '/' . base64_encode($country->id + 10));

        $response->assertStatus(404);
    }

    /** @test */
    public function can_attach_tier()
    {
        $estatement = $this->createEstatement();
        $tier = factory(\App\Tier::class)->create();

        $data = [
            'tier_id' => $tier->id,
        ];

        $response = $this->post('/dashboard/estatement/link_tier/' . base64_encode($estatement->id), $data);

        $response->assertStatus(200)
                 ->assertViewIs('estatement.list.tierList');
    }

    /** @test */
    public function can_not_attach_tier_if_estatement_not_found()
    {
        $estatement = $this->createEstatement();
        $tier = factory(\App\Tier::class)->create();

        if (!$estatement || !$tier) {
            App::abort(404);
        }

        $data = [
            'tier_id' => $tier->id,
        ];

        $response = $this->post('/dashboard/estatement/link_tier/' . base64_encode($estatement->id + 10), $data);

        $response->assertStatus(404);
    }

    /** @test */
    public function can_not_attach_tier_if_tier_not_found()
    {
        $estatement = $this->createEstatement();
        $tier = factory(\App\Tier::class)->create();

        $data = [
            'tier_id' => $tier->id + 10,
        ];

        $response = $this->post('/dashboard/estatement/link_tier/' . base64_encode($estatement->id), $data);

        $response->assertStatus(404);
    }

    /** @test */
    public function can_detach_tier()
    {
        $estatement = $this->createEstatement();
        $tier = factory(\App\Tier::class)->create();

        $estatement->countries()->save($tier);

        $response = $this->get('/dashboard/estatement/unlink_tier/' . base64_encode($estatement->id) . '/' . base64_encode($tier->id));

        $response->assertStatus(200)
                 ->assertViewIs('estatement.list.tierList')
                 ->assertDontSee($tier->name);

        $this->assertDatabaseMissing('tier_estatement', [
            'estatement_id' => $estatement->id,
            'tiercriteria_id' => $tier->id,
        ]);
    }

    /** @test */
    public function can_not_detach_tier_if_estatement_not_found()
    {
        $estatement = $this->createEstatement();
        $tier = factory(\App\Tier::class)->create();

        $estatement->countries()->save($tier);

        $response = $this->get('/dashboard/estatement/unlink_tier/' . base64_encode($estatement->id + 10) . '/' . base64_encode($tier->id));

        $response->assertStatus(404);
    }

    /** @test */
    public function can_not_detach_tier_if_tier_not_found()
    {
        $estatement = $this->createEstatement();
        $tier = factory(\App\Tier::class)->create();

        $estatement->countries()->save($tier);

        $response = $this->get('/dashboard/estatement/unlink_tier/' . base64_encode($estatement->id) . '/' . base64_encode($tier->id + 10));

        $response->assertStatus(404);
    }

    /** @test */
    public function can_view_pagination_page()
    {
        $perPage = 15;
        $pageCount = ceil(App\Estatement::where('draft', false)->where('deleted', false)->count() / $perPage);

        $response = $this->get('/dashboard/estatement?page=' . $pageCount);

        if ($pageCount > 1) {
            $response->assertStatus(200)
                     ->assertViewIs('estatement.list')
                     ->assertSee('estatement?page=1')
                     ->assertDontSee('No E-Statements Found');
        } else {
            $response->assertStatus(200);
        }
    }

    /** @test */
    public function can_not_view_pagination_page_if_one_page()
    {
        $perPage = 15;
        $page = ceil(App\Estatement::where('draft', 0)->where('deleted', false)->count() / $perPage) + 1;

        $response = $this->get('/dashboard/estatement?page=' . $page);

        $response->assertStatus(200)
                 ->assertViewIs('estatement.list')
                 ->assertDontSee('estatement?page=' . $page)
                 ->assertSee('No E-Statements Found');
    }

    /** @test */
    public function can_filter_affiliates_by_country()
    {
        $estatement = $this->createEstatement();
        $estatement->countries()->save(factory(\App\Country::class)->create());

        $country1 = factory(\App\Country::class)->create();
        $country2 = factory(\App\Country::class)->create();

        $response = $this->get('/dashboard/estatement?&country_list=' . $country1->id . ',' . $country2->id);

        $response->assertStatus(200)
                 ->assertViewIs('estatement.list')
                 ->assertDontSee($estatement->title);

        $response = $this->get('/dashboard/estatement?&country_list=' . $estatement->countries()->first()->id . ',' . $country2->id);

        $response->assertStatus(200)
                 ->assertViewIs('estatement.list')
                 ->assertSee($estatement->title);
    }

    /** @test */
    public function can_filter_affiliates_by_partner()
    {
        $estatement = $this->createEstatement();

        $partner1 = factory(\App\Partner::class)->create();
        $partner2 = factory(\App\Partner::class)->create();

        $response = $this->get('/dashboard/estatement?&partner_estatement_list=' . $partner1->id . ',' . $partner2->id);

        $response->assertStatus(200)
                 ->assertViewIs('estatement.list')
                 ->assertDontSee($estatement->title);

        $response = $this->get('/dashboard/estatement?&partner_estatement_list=' . $estatement->partner_id . ',' . $partner2->id);

        $response->assertStatus(200)
                 ->assertViewIs('estatement.list')
                 ->assertSee($estatement->title);
    }

    /** @test */
    public function can_filter_affiliates_by_status()
    {
        $estatement = $this->createEstatement([
            'status' => 'Active'
        ]);

        $response = $this->get('/dashboard/estatement?&estatement_status=Inactive');

        $response->assertStatus(200)
                 ->assertViewIs('estatement.list')
                 ->assertDontSee($estatement->title);

        $response = $this->get('/dashboard/estatement?&estatement_status=Active');

        $response->assertStatus(200)
                 ->assertViewIs('estatement.list')
                 ->assertSee($estatement->title);
    }
}
