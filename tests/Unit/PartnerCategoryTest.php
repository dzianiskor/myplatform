<?php

namespace Tests\Unit;

use App\Admin;
use App\PartnerCategory;
use App\Language;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PartnerCategoryTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    protected $user;

    public function setUp()
    {
        parent::setUp();

        $user = Admin::where('email', 'info@bluloyalty.com')->first();
        $this->user = $user;
        $this->actingAs($user)
             ->session([
                 'user_roles' => [
                     'BLU Admin',
                 ],
                 'user_permissions' => [
                     'create_categories',
                     'view_categories',
                     'edit_categories',
                     'delete_categories',
                 ],
             ]);
    }

    /** @test */
    public function can_create_new_category_page_with_permissions()
    {
        $response = $this->get('/dashboard/partnercategory/new');

        $response->assertStatus(302)
                 ->assertRedirect()
                 ->assertSeeText('/dashboard/partnercategory/view/');
    }

    /** @test */
    public function can_not_create_new_category_page_without_permissions()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $response = $this->get('/dashboard/partnercategory/new');

        $response->assertStatus(302)
                 ->assertRedirect()
                 ->assertSeeText('/dashboard');
    }

    /** @test */
    public function can_open_edit_category_with_permissions()
    {
        $category = factory(\App\PartnerCategory::class)->create();

        $response = $this->get('/dashboard/partnercategory/view/' . base64_encode($category->id));

        $response->assertStatus(200)
                 ->assertViewIs('partnercategory.view');
    }

    /** @test */
    public function can_not_open_category_page_without_permissions()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $category = factory(\App\PartnerCategory::class)->create();

        $response = $this->get('/dashboard/partnercategory/view/' . base64_encode($category->id));

        $response->assertStatus(302)
                 ->assertRedirect()
                 ->assertSeeText('/dashboard');
    }

    /** @test */
    public function can_not_open_category_page_which_is_not_exists()
    {
        $category = factory(\App\PartnerCategory::class)->create();

        $response = $this->get('/dashboard/partnercategory/view/' . base64_encode($category->id + 10));

        $response->assertStatus(404);
    }

    /** @test */
    public function can_update_category_with_permission()
    {
        $category = factory(\App\PartnerCategory::class)->create();

        $data = [
            'name' => uniqid(),
        ];

        $response = $this->post('/dashboard/partnercategory/save/' . base64_encode($category->id), $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/partnercategory');

        $this->assertDatabaseHas('partnercategory', [
            'id' => $category->id,
            'name' => $data['name'],
        ]);
    }

    /** @test */
    public function can_not_update_category_without_permission()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $category = factory(\App\PartnerCategory::class)->create();

        $data = [
            'name' => uniqid(),
        ];

        $response = $this->post('/dashboard/partnercategory/save/' . base64_encode($category->id), $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard');

        $this->assertDatabaseMissing('partnercategory', [
            'id' => $category->id,
            'name' => $data['name'],
        ]);
    }

    /** @test */
    public function can_not_update_category_which_is_not_exists()
    {
        $category = factory(\App\PartnerCategory::class)->create();

        $data = [
            'name' => uniqid(),
        ];

        $response = $this->post('/dashboard/partnercategory/save/' . base64_encode($category->id + 10), $data);

        $response->assertStatus(404);

        $this->assertDatabaseMissing('partnercategory', [
            'id' => $category->id,
            'name' => $data['name'],
        ]);
    }

    /** @test */
    public function can_not_update_category_without_required_fields()
    {
        $category = factory(\App\PartnerCategory::class)->create();

        $data = [];
        $response = $this->post('/dashboard/partnercategory/save/' . base64_encode($category->id), $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/partnercategory/view/' . base64_encode($category->id))
                 ->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function can_delete_category_with_permission()
    {
        $category = factory(\App\PartnerCategory::class)->create();

        $response = $this->get('/dashboard/partnercategory/delete/' . base64_encode($category->id));

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/partnercategory');

        $this->assertDatabaseMissing('partnercategory', [
            'id' => $category->id,
        ]);
    }

    /** @test */
    public function can_not_delete_category_without_permission()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $category = factory(\App\PartnerCategory::class)->create();

        $response = $this->get('/dashboard/partnercategory/delete/' . base64_encode($category->id));

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard');

        $this->assertDatabaseHas('partnercategory', [
            'id' => $category->id,
        ]);
    }

    /** @test */
    public function can_not_delete_category_which_is_not_exists()
    {
        $category = factory(\App\PartnerCategory::class)->create();

        $response = $this->get('/dashboard/partnercategory/delete/' . base64_encode($category->id + 10));

        $response->assertStatus(404);

        $this->assertDatabaseHas('partnercategory', [
            'id' => $category->id,
        ]);
    }

    /** @test */
    public function can_open_categories_page()
    {
        factory(\App\PartnerCategory::class)->create();

        $response = $this->get('/dashboard/partnercategory');

        $response->assertStatus(200)
                 ->assertViewIs('partnercategory.list')
                 ->assertViewHas('categories');
    }

    /** @test */
    public function can_not_open_categories_page_without_permission()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        factory(\App\PartnerCategory::class)->create();

        $response = $this->get('/dashboard/partnercategory');

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard');
    }

    /** @test */
    public function can_filter_categories_by_search_query()
    {
        $category = factory(\App\PartnerCategory::class)->create();

        $response = $this->get('/dashboard/partnercategory?q=' . uniqid());

        $response->assertStatus(200)
                 ->assertViewIs('partnercategory.list')
                 ->assertDontSee($category->name);

        $response = $this->get('/dashboard/partnercategory?q=' . $category->name);

        $response->assertStatus(200)
                 ->assertViewIs('partnercategory.list')
                 ->assertSee($category->name);
    }

    /** @test */
    public function can_view_pagination_page()
    {
        $perPage = 15;
        $pageCount = ceil(PartnerCategory::where('draft', 0)->count() / $perPage);

        if ($pageCount > 1) {
            $response = $this->get('/dashboard/partnercategory?page=' . $pageCount);

            $response->assertStatus(200)
                     ->assertViewIs('partnercategory.list')
                     ->assertSee('partnercategory?page=1')
                     ->assertDontSee('No Partner Categories Found');
        }
    }

    /** @test */
    public function can_not_view_pagination_page_if_one_page()
    {
        $perPage = 15;
        $page = ceil(PartnerCategory::where('draft', 0)->count() / $perPage) + 1;

        $response = $this->get('/dashboard/partnercategory?page=' . $page);

        $response->assertStatus(200)
                 ->assertViewIs('partnercategory.list')
                 ->assertDontSee('partnercategory?page=' . $page)
                 ->assertSee('No Partner Categories Found');
    }
}
