<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Carbon\Carbon;
use App\Admin;
use App\User;
use App\Ticket;
use App\TransactionPoint;
use App\TransactionDate;
use App\Language;

class DashboardTest extends TestCase
{
    use DatabaseTransactions;

    protected $user;

    public function setUp()
    {
        parent::setUp();

        $user = Admin::where('email', 'info@bluloyalty.com')->first();
        $this->user = $user;
        $this->actingAs($user);
    }

    /** @test */
    public function can_view_dashboard_page()
    {
        $response = $this->get('/dashboard');

        $response->assertStatus(200);
    }

    /** @test */
    public function dashboard_can_create_tickets()
    {
        $ticket = factory(Ticket::class)->create();

        $this->assertNotEmpty($ticket);
    }

    /** @test */
    public function dashboard_can_create_transaction_point()
    {
        $result = factory(TransactionPoint::class)->create();

        $this->assertNotEmpty($result);
    }

    /** @test */
    public function dashboard_can_get_redeemers_earners_in_last_30_days_period()
    {
        $transactionPoint = TransactionPoint::take(1)->orderBy('id', 'ASC')->get();

        if ($transactionPoint->isNotEmpty()) {
            $result = TransactionPoint::where('date_from', '>=', Carbon::today()->subDays(30))->get();

            $this->assertNotEmpty($result);
        } else {
            $this->assertEmpty($transactionPoint);
        }
    }

    /** @test */
    public function dashboard_can_create_transaction_date()
    {
        $result = factory(TransactionDate::class)->create();

        $this->assertNotEmpty($result);
    }

    /** @test */
    public function dashboard_can_get_transaction_overview_in_last_30_days_period()
    {
        $transactionDate = TransactionDate::take(1)->orderBy('id', 'ASC')->get();

        if ($transactionDate->isNotEmpty()) {
            $result = TransactionDate::where('transaction_date', '>=', Carbon::today()->subDays(30))->get();

            $this->assertNotEmpty($result);
        } else {
            $this->assertEmpty($transactionDate);
        }

        $this->assertNotEmpty($result);
    }

    /** @test */
    public function dashboard_count_members_for_blu_admin_role()
    {
        $result = User::where('draft', false)->where('id', '>', 1)->count();

        $this->assertNotEmpty($result);
    }

    /** @test */
    public function dashboard_count_members_for_non_admin_role()
    {
        $result = $this->user->partnerHierarchyMembers()->count();

        $this->assertNotEmpty($result);
    }

    /** @test */
    public function dashboard_check_translate_keys_for_en_language()
    {
        $languageData = Language::where('short_code', 'en')->first();

        $this->assertNotEmpty($languageData);
    }

    /** @test */
    public function dashboard_check_translate_keys_for_ar_language()
    {
        $languageData = Language::where('short_code', 'ar')->first();

        $this->assertNotEmpty($languageData);
    }
}