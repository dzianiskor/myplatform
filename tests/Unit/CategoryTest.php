<?php

namespace Tests\Unit;

use App\Admin;
use App\Category;
use App\Language;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CategoryTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    protected $user;

    public function setUp()
    {
        parent::setUp();

        $user = Admin::where('email', 'info@bluloyalty.com')->first();
        $this->user = $user;
        $this->actingAs($user)
             ->session([
                 'user_roles' => [
                     'BLU Admin',
                 ],
                 'user_permissions' => [
                     'create_categories',
                     'view_categories',
                     'edit_categories',
                     'delete_categories',
                 ],
             ]);
    }

    /** @test */
    public function can_create_new_category_page_with_permissions()
    {
        $response = $this->get('/dashboard/categories/new');

        $response->assertStatus(302)
                 ->assertRedirect()
                 ->assertSeeText('/dashboard/categories/view/');
    }

    /** @test */
    public function can_not_create_new_category_page_without_permissions()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $response = $this->get('/dashboard/categories/new');

        $response->assertStatus(302)
                 ->assertRedirect()
                 ->assertSeeText('/dashboard');
    }

    /** @test */
    public function can_open_edit_category_with_permissions()
    {
        $category = factory(\App\Category::class)->create();

        $response = $this->get('/dashboard/categories/view/' . base64_encode($category->id));

        $response->assertStatus(200)
                 ->assertViewIs('category.view');
    }

    /** @test */
    public function can_not_open_category_page_without_permissions()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $category = factory(\App\Category::class)->create();

        $response = $this->get('/dashboard/categories/view/' . base64_encode($category->id));

        $response->assertStatus(302)
                 ->assertRedirect()
                 ->assertSeeText('/dashboard');
    }

    /** @test */
    public function can_not_open_category_page_which_is_not_exists()
    {
        $category = factory(\App\Category::class)->create();

        $response = $this->get('/dashboard/categories/view/' . base64_encode($category->id + 10));

        $response->assertStatus(404);
    }

    /** @test */
    public function can_update_category_with_permission()
    {
        $category = factory(\App\Category::class)->create();

        $data = [
            'name' => uniqid(),
        ];

        $response = $this->post('/dashboard/categories/update/' . base64_encode($category->id), $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/categories');

        $this->assertDatabaseHas('category', [
            'id' => $category->id,
            'name' => $data['name'],
        ]);
    }

    /** @test */
    public function can_not_update_category_without_permission()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $category = factory(\App\Category::class)->create();

        $data = [
            'name' => uniqid(),
        ];

        $response = $this->post('/dashboard/categories/update/' . base64_encode($category->id), $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard');

        $this->assertDatabaseMissing('category', [
            'id' => $category->id,
            'name' => $data['name'],
        ]);
    }

    /** @test */
    public function can_not_update_category_which_is_not_exists()
    {
        $category = factory(\App\Category::class)->create();

        $data = [
            'name' => uniqid(),
        ];

        $response = $this->post('/dashboard/categories/update/' . base64_encode($category->id + 10), $data);

        $response->assertStatus(404);

        $this->assertDatabaseMissing('category', [
            'id' => $category->id,
            'name' => $data['name'],
        ]);
    }

    /** @test */
    public function can_not_update_category_without_required_fields()
    {
        $category = factory(\App\Category::class)->create();

        $data = [];
        $response = $this->post('/dashboard/categories/update/' . base64_encode($category->id), $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/categories/view/' . base64_encode($category->id))
                 ->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function can_delete_category_with_permission()
    {
        $category = factory(\App\Category::class)->create();

        $response = $this->get('/dashboard/categories/delete/' . base64_encode($category->id));

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/categories');

        $this->assertDatabaseMissing('category', [
            'id' => $category->id,
        ]);
    }

    /** @test */
    public function can_not_delete_category_without_permission()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $category = factory(\App\Category::class)->create();

        $response = $this->get('/dashboard/categories/delete/' . base64_encode($category->id));

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard');

        $this->assertDatabaseHas('category', [
            'id' => $category->id,
        ]);
    }

    /** @test */
    public function can_not_delete_category_which_is_not_exists()
    {
        $category = factory(\App\Category::class)->create();

        $response = $this->get('/dashboard/categories/delete/' . base64_encode($category->id + 10));

        $response->assertStatus(404);

        $this->assertDatabaseHas('category', [
            'id' => $category->id,
        ]);
    }

    /** @test */
    public function can_open_categories_page()
    {
        factory(\App\Category::class)->create();

        $response = $this->get('/dashboard/categories');

        $response->assertStatus(200)
                 ->assertViewIs('category.list')
                 ->assertViewHas('categories');
    }

    /** @test */
    public function can_not_open_categories_page_without_permission()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        factory(\App\Category::class)->create();

        $response = $this->get('/dashboard/categories');

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard');
    }

    /** @test */
    public function can_filter_categories_by_search_query()
    {
        $category = factory(\App\Category::class)->create();

        $response = $this->get('/dashboard/categories?q=' . uniqid());

        $response->assertStatus(200)
                 ->assertViewIs('category.list')
                 ->assertDontSee($category->name);

        $response = $this->get('/dashboard/categories?q=' . $category->name);

        $response->assertStatus(200)
                 ->assertViewIs('category.list')
                 ->assertSee($category->name);
    }

    /** @test */
    public function can_add_category_translation()
    {
        $category = factory(\App\Category::class)->create();

        $response = $this->get('/dashboard/cattranslation/new/' . $category->id);

        $response->assertStatus(200)
                 ->assertViewIs('cattranslation.form');
    }

    /** @test */
    public function can_save_category_translation()
    {
        $category = factory(\App\Category::class)->create();
        $categoryTranslation = factory(\App\Cattranslation::class)->create([
            'category_id' => $category->id,
        ]);
        $lang = factory(Language::class)->create();

        $data = [
            'cattranslation_lang' => $lang->id,
            'cattranslation_name' => uniqid(),
        ];

        $response = $this->post('/dashboard/cattranslation/update/' . base64_encode($categoryTranslation->id), $data);

        $response->assertStatus(200)
                 ->assertJson([
                     'failed' => false,
                     'view_url' => url('dashboard/cattranslation/list/' . base64_encode($categoryTranslation->category_id)),
                 ]);

        $this->assertDatabaseHas('category_translation', [
            'id' => $categoryTranslation->id,
            'lang_id' => $data['cattranslation_lang'],
            'name' => $data['cattranslation_name'],
        ]);
    }

    /** @test */
    public function can_not_save_category_translation_without_required_fields()
    {
        $category = factory(\App\Category::class)->create();
        $categoryTranslation = factory(\App\Cattranslation::class)->create([
            'category_id' => $category->id,
        ]);

        $data = [];

        $response = $this->post('/dashboard/cattranslation/update/' . base64_encode($categoryTranslation->id), $data);

        $response->assertStatus(200)
                 ->assertJsonFragment([
                     'failed' => true,
                 ]);
    }

    /** @test */
    public function can_get_category_translation_list()
    {
        $category = factory(\App\Category::class)->create();
        $categoryTranslation = factory(\App\Cattranslation::class)->create([
            'category_id' => $category->id,
        ]);

        $response = $this->get('/dashboard/cattranslation/list/' . base64_encode($category->id));

        $response->assertStatus(200)
                 ->assertViewIs('cattranslation.list')
                 ->assertSee($categoryTranslation->name);
    }

    /** @test */
    public function can_delete_category_translation()
    {
        $category = factory(\App\Category::class)->create();
        $categoryTranslation = factory(\App\Cattranslation::class)->create([
            'category_id' => $category->id,
        ]);

        $response = $this->get('/dashboard/cattranslation/delete/' . base64_encode($categoryTranslation->id));

        $response->assertStatus(200)
                 ->assertJson([
                     'failed' => false,
                 ]);

        $this->assertDatabaseMissing('category_translation', [
            'id' => $categoryTranslation->id,
            'name' => $categoryTranslation->name,
        ]);
    }

    /** @test */
    public function can_view_pagination_page()
    {
        $perPage = 15;
        $pageCount = ceil(Category::where('draft', 0)->count() / $perPage);

        if ($pageCount > 1) {
            $response = $this->get('/dashboard/categories?page=' . $pageCount);

            $response->assertStatus(200)
                     ->assertViewIs('category.list')
                     ->assertSee('categories?page=1')
                     ->assertDontSee('No Categories Found');
        }
    }

    /** @test */
    public function can_not_view_pagination_page_if_one_page()
    {
        $perPage = 15;
        $page = ceil(Category::where('draft', 0)->count() / $perPage) + 1;

        $response = $this->get('/dashboard/categories?page=' . $page);

        $response->assertStatus(200)
                 ->assertViewIs('category.list')
                 ->assertDontSee('categories?page=' . $page)
                 ->assertSee('No Categories Found');
    }
}
