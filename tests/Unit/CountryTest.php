<?php

namespace Tests\Unit;

use App\Admin;
use App\Country;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CountryTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    protected $user;

    public function setUp()
    {
        parent::setUp();

        $user = Admin::where('email', 'info@bluloyalty.com')->first();
        $this->user = $user;
        $this->actingAs($user)
             ->session([
                 'user_roles' => [
                     'BLU Admin',
                 ],
                 'user_permissions' => [
                     'create_countries',
                     'view_countries',
                     'edit_countries',
                     'delete_countries',
                 ],
             ]);
    }

    /** @test */
    public function can_create_new_country_page_with_permissions()
    {
        $response = $this->get('/dashboard/countries/new');

        $response->assertStatus(200)
                 ->assertViewIs('country.new');
    }

    /** @test */
    public function can_not_create_new_country_page_without_permissions()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $response = $this->get('/dashboard/countries/new');

        $response->assertStatus(302)
                 ->assertRedirect()
                 ->assertSeeText('/dashboard');
    }

    /** @test */
    public function can_open_edit_country_with_permissions()
    {
        $country = factory(\App\Country::class)->create();

        $response = $this->get('/dashboard/countries/view/' . base64_encode($country->id));

        $response->assertStatus(200)
                 ->assertViewIs('country.view');
    }

    /** @test */
    public function can_not_open_country_page_without_permissions()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $country = factory(\App\Country::class)->create();

        $response = $this->get('/dashboard/countries/view/' . base64_encode($country->id));

        $response->assertStatus(302)
                 ->assertRedirect()
                 ->assertSeeText('/dashboard');
    }

    /** @test */
    public function can_not_open_country_page_which_is_not_exists()
    {
        $country = factory(\App\Country::class)->create();

        $response = $this->get('/dashboard/countries/view/' . base64_encode($country->id + 10));

        $response->assertStatus(404);
    }

    /** @test */
    public function can_update_country_with_permission()
    {
        $country = factory(\App\Country::class)->create();

        $data = [
            'name' => uniqid(),
            'telephone_code' => '123123',
        ];

        $response = $this->post('/dashboard/countries/update/' . base64_encode($country->id), $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/countries');

        $this->assertDatabaseHas('country', [
            'id' => $country->id,
            'name' => $data['name'],
            'telephone_code' => $data['telephone_code'],
        ]);
    }

    /** @test */
    public function can_not_update_country_without_permission()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $country = factory(\App\Country::class)->create();

        $data = [
            'name' => uniqid(),
            'telephone_code' => '123123',
        ];

        $response = $this->post('/dashboard/countries/update/' . base64_encode($country->id), $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard');

        $this->assertDatabaseMissing('country', [
            'id' => $country->id,
            'name' => $data['name'],
            'telephone_code' => $data['telephone_code'],
        ]);
    }

    /** @test */
    public function can_not_update_country_which_is_not_exists()
    {
        $country = factory(\App\Country::class)->create();

        $data = [
            'name' => uniqid(),
            'telephone_code' => '123123',
        ];

        $response = $this->post('/dashboard/countries/update/' . base64_encode($country->id + 10), $data);

        $response->assertStatus(404);

        $this->assertDatabaseMissing('country', [
            'id' => $country->id,
            'name' => $data['name'],
            'telephone_code' => $data['telephone_code'],
        ]);
    }

    /** @test */
    public function can_not_update_country_without_required_fields()
    {
        $country = factory(\App\Country::class)->create();

        $data = [];
        $response = $this->post('/dashboard/countries/update/' . base64_encode($country->id), $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/countries/view/' . base64_encode($country->id))
                 ->assertSessionHasErrors(['name']);

        $data = [
            'name' => 'q',
        ];
        $response = $this->post('/dashboard/countries/update/' . base64_encode($country->id), $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/countries/view/' . base64_encode($country->id))
                 ->assertSessionHasErrors(['name', 'telephone_code']);
    }

    /** @test */
    public function can_delete_country_with_permission()
    {
        $country = factory(\App\Country::class)->create();

        $response = $this->get('/dashboard/countries/delete/' . base64_encode($country->id));

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/countries');

        $this->assertDatabaseMissing('country', [
            'id' => $country->id,
        ]);
    }

    /** @test */
    public function can_not_delete_country_without_permission()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $country = factory(\App\Country::class)->create();

        $response = $this->get('/dashboard/countries/delete/' . base64_encode($country->id));

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard');

        $this->assertDatabaseHas('country', [
            'id' => $country->id,
        ]);
    }

    /** @test */
    public function can_not_delete_country_which_is_not_exists()
    {
        $country = factory(\App\Country::class)->create();

        $response = $this->get('/dashboard/countries/delete/' . base64_encode($country->id + 10));

        $response->assertStatus(404);

        $this->assertDatabaseHas('country', [
            'id' => $country->id,
        ]);
    }

    /** @test */
    public function can_open_countries_page()
    {
        factory(\App\Country::class)->create();

        $response = $this->get('/dashboard/countries');

        $response->assertStatus(200)
                 ->assertViewIs('country.list')
                 ->assertViewHas('countries');
    }

    /** @test */
    public function can_not_open_countries_page_without_permission()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        factory(\App\Country::class)->create();

        $response = $this->get('/dashboard/countries');

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard');
    }

    /** @test */
    public function can_filter_countries_by_search_query()
    {
        $country = factory(\App\Country::class)->create();

        $response = $this->get('/dashboard/countries?q=' . uniqid());

        $response->assertStatus(200)
                 ->assertViewIs('country.list')
                 ->assertDontSee($country->name);

        $response = $this->get('/dashboard/countries?q=' . $country->name);

        $response->assertStatus(200)
                 ->assertViewIs('country.list')
                 ->assertSee($country->name);
    }

    /** @test */
    public function can_view_pagination_page()
    {
        $perPage = 15;
        $pageCount = ceil(Country::count() / $perPage);

        if ($pageCount > 1) {
            $response = $this->get('/dashboard/countries?page=' . $pageCount);

            $response->assertStatus(200)
                     ->assertViewIs('country.list')
                     ->assertSee('countries?page=1')
                     ->assertDontSee('No Countries Found');
        }
    }

    /** @test */
    public function can_not_view_pagination_page_if_one_page()
    {
        $perPage = 15;
        $page = ceil(Country::count() / $perPage) + 1;

        $response = $this->get('/dashboard/countries?page=' . $page);

        $response->assertStatus(200)
                 ->assertViewIs('country.list')
                 ->assertDontSee('countries?page=' . $page)
                 ->assertSee('No Countries Found');
    }
}
