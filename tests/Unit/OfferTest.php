<?php

namespace Tests\Unit;

use App\Address;
use App\Admin;
use App\Offer;
use App\Segment;
use Faker\Factory;
use Faker\Generator;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class OfferTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    protected $user;

    public function setUp()
    {
        parent::setUp();

        $user = Admin::where('email', 'info@bluloyalty.com')->first();
        $this->user = $user;
        $this->actingAs($user)
             ->session([
                 'user_roles' => [
                     'BLU Admin',
                 ],
                 'user_permissions' => [
                     'create_offer',
                     'view_offer',
                     'edit_offer',
                     'delete_offer',
                 ],
             ]);
    }

    /** @test */
    public function can_create_new_offer_page_with_permissions()
    {
        $response = $this->get('/dashboard/offer/new');

        $response->assertStatus(302)
                 ->assertRedirect()
                 ->assertSeeText('/dashboard/offer/view/');
    }

    /** @test */
    public function can_not_create_new_offer_page_without_permissions()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $response = $this->get('/dashboard/offer/new');

        $response->assertStatus(302)
                 ->assertRedirect()
                 ->assertSeeText('/dashboard');
    }

    /**
     * @return Offer
     */
    protected function createOffer()
    {
        $offer = factory(\App\Offer::class)->create();
        $offer->partners()->save((\App\Partner::find(1))->first());

        return $offer;
    }

    /**
     * @return Address
     */
    protected function createAddress()
    {
        $country = factory(\App\Country::class)->create();
        $area = factory(\App\Area::class)->create([
            'country_id' => $country->id,
        ]);
        $city = factory(\App\City::class)->create([
            'area_id' => $area->id,
        ]);

        $address = factory(\App\Address::class)->create([
            'country_id' => $country->id,
            'area_id' => $area->id,
            'city_id' => $city->id,
        ]);

        return $address;
    }

    /** @test */
    public function can_open_edit_offer_with_permissions()
    {
        $offer = $this->createOffer();

        $response = $this->get('/dashboard/offer/view/' . base64_encode($offer->id));

        $response->assertStatus(200)
                 ->assertViewIs('offer.view');
    }

    /** @test */
    public function can_not_open_offer_page_which_is_not_exists()
    {
        $offer = $this->createOffer();

        $response = $this->get('/dashboard/offer/view/' . base64_encode($offer->id + 10));

        $response->assertStatus(404);
    }

    /** @test */
    public function can_not_open_offer_page_which_is_not_draft_and_partner_can_not_view()
    {
        $offer = factory(\App\Offer::class)->create();
        $partner = factory(\App\Partner::class)->create();
        $offer->partners()->save($partner);

        $response = $this->get('/dashboard/offer/view/' . base64_encode($offer->id));

        $response->assertStatus(302)
                 ->assertRedirect()
                 ->assertSeeText('/dashboard');
    }

    /** @test */
    public function can_not_open_edit_offer_without_permissions()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $offer = $this->createOffer();

        $response = $this->get('/dashboard/offer/view/' . base64_encode($offer->id));

        $response->assertStatus(302)
                 ->assertRedirect()
                 ->assertSeeText('/dashboard');
    }

    /** @test */
    public function can_update_offer_with_permissions()
    {
        $offer = $this->createOffer();

        $category = factory(\App\Category::class)->create();

        $stub = __DIR__ . '\\..\\stubs\\test.png';
        $name = str_random(8) . '.png';
        $path = sys_get_temp_dir() . '\\' . $name;

        copy($stub, $path);

        $file = new UploadedFile($path, $name, filesize($path), 'image/png', null, true);

        $data = [
            'name' => uniqid(),
            'category_id' => $category->id,
        ];

        $response = $this->call('POST', '/dashboard/offer/update/' . base64_encode($offer->id), $data, [], ['cover_image' => $file], ['Accept' => 'application/json']);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/offer')
                 ->assertSessionMissing('alert_error');

        $this->assertDatabaseHas('offer', [
            'id' => $offer->id,
            'name' => $data['name'],
        ]);

        $offer1 = \App\Offer::find(['id' => $offer->id])->first();

        $this->assertFileExists(public_path(\App\Http\Controllers\MediaController::getImageFromId($offer1->cover_image)));

        @unlink(public_path(\App\Http\Controllers\MediaController::getImageFromId($offer1->cover_image)));
    }

    /** @test */
    public function can_not_update_offer_without_permission()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $offer = $this->createOffer();
        $category = factory(\App\Category::class)->create();

        $data = [
            'name' => uniqid(),
            'category_id' => $category->id,
        ];

        $response = $this->post('/dashboard/offer/update/' . base64_encode($offer->id), $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard');

        $this->assertDatabaseMissing('offer', [
            'id' => $offer->id,
            'name' => $data['name'],
        ]);
    }

    /** @test */
    public function can_not_update_offer_which_is_not_exists()
    {
        $offer = $this->createOffer();
        $category = factory(\App\Category::class)->create();

        $data = [
            'name' => uniqid(),
            'category_id' => $category->id,
        ];

        $response = $this->post('/dashboard/offer/update/' . base64_encode($offer->id + 10), $data);

        $response->assertStatus(404);

        $this->assertDatabaseMissing('offer', [
            'id' => $offer->id,
            'name' => $data['name'],
        ]);
    }

    /** @test */
    public function can_not_update_offer_without_required_fields()
    {
        $offer = $this->createOffer();

        $data = [];
        $response = $this->post('/dashboard/offer/update/' . base64_encode($offer->id), $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/offer/view/' . base64_encode($offer->id))
                 ->assertSessionHasErrors(['name', 'category_id']);
    }

    /** @test */
    public function can_delete_offer_with_permission()
    {
        $offer = $this->createOffer();

        $response = $this->get('/dashboard/offer/delete/' . base64_encode($offer->id));

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/offer');

        $this->assertDatabaseMissing('offer', [
            'id' => $offer->id,
            'deleted' => false,
        ]);
    }

    /** @test */
    public function can_not_delete_offer_without_permission()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $offer = $this->createOffer();

        $response = $this->get('/dashboard/offer/delete/' . base64_encode($offer->id));

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard');

        $this->assertDatabaseHas('offer', [
            'id' => $offer->id,
        ]);
    }

    /** @test */
    public function can_not_delete_offer_which_is_not_exists()
    {
        $offer = $this->createOffer();

        $data = [
            'name' => uniqid(),
        ];

        $response = $this->get('/dashboard/offer/delete/' . base64_encode($offer->id + 10));

        $response->assertStatus(404);

        $this->assertDatabaseMissing('offer', [
            'id' => $offer->id,
            'name' => $data['name'],
        ]);

        $this->assertDatabaseHas('offer', [
            'id' => $offer->id,
        ]);
    }

    /** @test */
    public function can_filter_offer_by_search_query()
    {
        $offer = $this->createOffer();

        $response = $this->get('/dashboard/offer?q=' . uniqid());

        $response->assertStatus(200)
                 ->assertViewIs('offer.list')
                 ->assertDontSee($offer->name);

        $response = $this->get('/dashboard/offer?q=' . $offer->name);

        $response->assertStatus(200)
                 ->assertViewIs('offer.list')
                 ->assertSee($offer->name);
    }

    /** @test */
    public function can_load_offer_translation_form()
    {
        $offer = $this->createOffer();

        $response = $this->get('/dashboard/offertranslation/new/' . base64_encode($offer->id));

        $response->assertStatus(200)
                 ->assertViewIs('offertranslation.new');
    }

    /** @test */
    public function can_save_offer_translation()
    {
        $faker = Factory::create();

        $offer = $this->createOffer();
        $offerTranslation = factory(\App\Offertranslation::class)->create([
            'offer_id' => $offer->id,
        ]);

        $data = [
            'offertranslation_oid' => $offer->id,
            'offertranslation_lang' => $offerTranslation->lang_id,
            'offertranslation_name' => $faker->text(20),
            'offertranslation_short_description' => $faker->text(100),
            'offertranslation_description' => $faker->text(200),
            'offertranslation_terms_and_condition' => $faker->text(200),
            'offertranslation_email_text_promo_1' => $faker->text(200),
            'offertranslation_email_text_promo_2' => $faker->text(200),
        ];

        $response = $this->post('/dashboard/offertranslation/create', $data);

        $response->assertStatus(200)
                 ->assertJson([
                     'failed' => false,
                     'view_url' => url('dashboard/offertranslation/list/' . base64_encode($offerTranslation->offer_id)),
                 ]);

        $this->assertDatabaseHas('offer_translation', [
            'name' => $data['offertranslation_name'],
            'offer_id' => $offer->id,
        ]);
    }

    /** @test */
    public function can_not_save_offer_translation_without_required_fields()
    {
        $offer = $this->createOffer();
        $offerTranslation = factory(\App\Offertranslation::class)->create([
            'offer_id' => $offer->id,
        ]);

        $data = [];

        $response = $this->post('/dashboard/offertranslation/update/' . $offerTranslation->id, $data);

        $response->assertStatus(200)
                 ->assertJsonFragment([
                     'failed' => true,
                 ]);
    }

    /** @test */
    public function can_get_list_offer_translation()
    {
        $offer = $this->createOffer();
        $offerTranslation = factory(\App\Offertranslation::class)->create([
            'offer_id' => $offer->id,
        ]);

        $response = $this->get('/dashboard/offertranslation/list/' . base64_encode($offer->id));

        $response->assertStatus(200)
                 ->assertViewIs('offertranslation.list')
                 ->assertSee($offerTranslation->name);
    }

    /** @test */
    public function can_delete_offer_translation()
    {
        $offer = $this->createOffer();
        $offerTranslation = factory(\App\Offertranslation::class)->create([
            'offer_id' => $offer->id,
        ]);

        $response = $this->get('/dashboard/offertranslation/delete/' . base64_encode($offerTranslation->id));

        $response->assertStatus(200)
                 ->assertJson([
                     'failed' => false,
                 ]);

        $this->assertDatabaseMissing('offer_translation', [
            'name' => $offerTranslation->name,
        ]);
    }

    /** @test */
    public function can_attach_partner()
    {
        $offer = $this->createOffer();
        $partner = factory(\App\Partner::class)->create();

        $data = [
            'partner_id' => $partner->id,
        ];

        $response = $this->post('/dashboard/offer/link_partner/' . base64_encode($offer->id), $data);

        $response->assertStatus(200)
                 ->assertViewIs('offer.list.partnerList')
                 ->assertSee($partner->name);
    }

    /** @test */
    public function can_not_attach_partner_if_offer_not_found()
    {
        $offer = $this->createOffer();
        $partner = factory(\App\Partner::class)->create();

        $data = [
            'partner_id' => $partner->id,
        ];

        $response = $this->post('/dashboard/offer/link_partner/' . base64_encode($offer->id + 10), $data);

        $response->assertStatus(404);
    }

    /** @test */
    public function can_not_attach_partner_if_partner_not_found()
    {
        $offer = $this->createOffer();
        $partner = factory(\App\Partner::class)->create();

        $data = [
            'partner_id' => $partner->id + 10,
        ];

        $response = $this->post('/dashboard/offer/link_partner/' . base64_encode($offer->id), $data);

        $response->assertStatus(404);
    }

    /** @test */
    public function can_detach_partner()
    {
        $offer = $this->createOffer();
        $partner = factory(\App\Partner::class)->create();

        $offer->partners()->save($partner);

        $response = $this->get('/dashboard/offer/unlink_partner/' . base64_encode($offer->id) . '/' . base64_encode($partner->id));

        $response->assertStatus(200)
                 ->assertViewIs('offer.list.partnerList')
                 ->assertDontSee($partner->name);

        $this->assertDatabaseMissing('partner_offer', [
            'offer_id' => $offer->id,
            'partner_id' => $partner->id,
        ]);
    }

    /** @test */
    public function can_not_detach_partner_if_offer_not_found()
    {
        $offer = $this->createOffer();
        $partner = factory(\App\Partner::class)->create();

        $offer->partners()->save($partner);

        $response = $this->get('/dashboard/offer/unlink_partner/' . base64_encode($offer->id + 10) . '/' . base64_encode($partner->id));

        $response->assertStatus(404);
    }

    /** @test */
    public function can_not_detach_partner_if_partner_not_found()
    {
        $offer = $this->createOffer();
        $partner = factory(\App\Partner::class)->create();

        $offer->partners()->save($partner);

        $response = $this->get('/dashboard/offer/unlink_partner/' . base64_encode($offer->id) . '/' . base64_encode($partner->id + 10));

        $response->assertStatus(404);
    }

    /** @test */
    public function can_attach_segment()
    {
        $offer = $this->createOffer();
        $segment = factory(\App\Segment::class)->create();

        $data = [
            'segment_id' => $segment->id,
        ];

        $response = $this->post('/dashboard/offer/link_segment/' . base64_encode($offer->id), $data);

        $response->assertStatus(200)
                 ->assertViewIs('offer.list.segmentList')
                 ->assertSee($segment->name);
    }

    /** @test */
    public function can_not_attach_segment_if_offer_not_found()
    {
        $offer = $this->createOffer();
        $segment = factory(\App\Segment::class)->create();

        $data = [
            'segment_id' => $segment->id,
        ];

        $response = $this->post('/dashboard/offer/link_segment/' . base64_encode($offer->id + 10), $data);

        $response->assertStatus(404);
    }

    /** @test */
    public function can_not_attach_segment_if_segment_not_found()
    {
        $offer = $this->createOffer();
        $segment = factory(\App\Segment::class)->create();

        $data = [
            'segment_id' => $segment->id + 10,
        ];

        $response = $this->post('/dashboard/offer/link_segment/' . base64_encode($offer->id), $data);

        $response->assertStatus(404);
    }

    /** @test */
    public function can_detach_segment()
    {
        $offer = $this->createOffer();
        $segment = factory(\App\Segment::class)->create();

        $offer->segments()->save($segment);

        $response = $this->get('/dashboard/offer/unlink_segment/' . base64_encode($offer->id) . '/' . base64_encode($segment->id));

        $response->assertStatus(200)
                 ->assertViewIs('offer.list.segmentList')
                 ->assertDontSee($segment->name);

        $this->assertDatabaseMissing('offer_segment', [
            'offer_id' => $offer->id,
            'segment_id' => $segment->id,
        ]);
    }

    /** @test */
    public function can_not_detach_segment_if_offer_not_found()
    {
        $offer = $this->createOffer();
        $segment = factory(\App\Segment::class)->create();

        $offer->segments()->save($segment);

        $response = $this->get('/dashboard/offer/unlink_segment/' . base64_encode($offer->id + 10) . '/' . base64_encode($segment->id));

        $response->assertStatus(404);
    }

    /** @test */
    public function can_not_detach_segment_if_segment_not_found()
    {
        $offer = $this->createOffer();
        $segment = factory(\App\Segment::class)->create();

        $offer->segments()->save($segment);

        $response = $this->get('/dashboard/offer/unlink_segment/' . base64_encode($offer->id) . '/' . base64_encode($segment->id + 10));

        $response->assertStatus(404);
    }

    /** @test */
    public function can_attach_country()
    {
        $offer = $this->createOffer();
        $country = factory(\App\Country::class)->create();

        $data = [
            'country_id' => $country->id,
        ];

        $response = $this->post('/dashboard/offer/link_country/' . base64_encode($offer->id), $data);

        $response->assertStatus(200)
                 ->assertViewIs('offer.list.countryList')
                 ->assertSee($country->name);
    }

    /** @test */
    public function can_not_attach_country_if_offer_not_found()
    {
        $offer = $this->createOffer();
        $country = factory(\App\Segment::class)->create();

        $data = [
            'country_id' => $country->id,
        ];

        $response = $this->post('/dashboard/offer/link_country/' . base64_encode($offer->id + 10), $data);

        $response->assertStatus(404);
    }

    /** @test */
    public function can_not_attach_country_if_country_not_found()
    {
        $offer = $this->createOffer();
        $country = factory(\App\Segment::class)->create();

        $data = [
            'country_id' => $country->id + 10,
        ];

        $response = $this->post('/dashboard/offer/link_country/' . base64_encode($offer->id), $data);

        $response->assertStatus(404);
    }

    /** @test */
    public function can_detach_country()
    {
        $offer = $this->createOffer();
        $country = factory(\App\Country::class)->create();

        $offer->countries()->save($country);

        $response = $this->get('/dashboard/offer/unlink_country/' . base64_encode($offer->id) . '/' . base64_encode($country->id));

        $response->assertStatus(200)
                 ->assertViewIs('offer.list.countryList')
                 ->assertDontSee($country->name);

        $this->assertDatabaseMissing('country_offer', [
            'offer_id' => $offer->id,
            'country_id' => $country->id,
        ]);
    }

    /** @test */
    public function can_not_detach_country_if_offer_not_found()
    {
        $offer = $this->createOffer();
        $country = factory(\App\Country::class)->create();

        $offer->countries()->save($country);

        $response = $this->get('/dashboard/offer/unlink_country/' . base64_encode($offer->id + 10) . '/' . base64_encode($country->id));

        $response->assertStatus(404);
    }

    /** @test */
    public function can_not_detach_country_if_country_not_found()
    {
        $offer = $this->createOffer();
        $country = factory(\App\Country::class)->create();

        $offer->countries()->save($country);

        $response = $this->get('/dashboard/offer/unlink_country/' . base64_encode($offer->id) . '/' . base64_encode($country->id + 10));

        $response->assertStatus(404);
    }

    /** @test */
    public function can_attach_channel()
    {
        $offer = $this->createOffer();
        $channel = factory(\App\Partner::class)->create();

        $data = [
            'partner_id' => $channel->id,
        ];

        $response = $this->post('/dashboard/offer/link_channel/' . base64_encode($offer->id), $data);

        $response->assertStatus(200)
                 ->assertViewIs('offer.list.channelList')
                 ->assertSee($channel->name);
    }

    /** @test */
    public function can_not_attach_channel_if_offer_not_found()
    {
        $offer = $this->createOffer();
        $channel = factory(\App\Partner::class)->create();

        $data = [
            'partner_id' => $channel->id,
        ];

        $response = $this->post('/dashboard/offer/link_channel/' . base64_encode($offer->id + 10), $data);

        $response->assertStatus(404);
    }

    /** @test */
    public function can_not_attach_channel_if_channel_not_found()
    {
        $offer = $this->createOffer();
        $channel = factory(\App\Partner::class)->create();

        $data = [
            'partner_id' => $channel->id + 10,
        ];

        $response = $this->post('/dashboard/offer/link_channel/' . base64_encode($offer->id), $data);

        $response->assertStatus(404);
    }

    /** @test */
    public function can_detach_channel()
    {
        $offer = $this->createOffer();
        $channel = factory(\App\Partner::class)->create();

        DB::table('channel_offer')->insert([
            'offer_id' => $offer->id,
            'channel_id' => $channel->id,
        ]);

        $response = $this->get('/dashboard/offer/unlink_channel/' . base64_encode($offer->id) . '/' . base64_encode($channel->id));

        $response->assertStatus(200)
                 ->assertViewIs('offer.list.channelList')
                 ->assertDontSee($channel->name);

        $this->assertDatabaseMissing('channel_offer', [
            'offer_id' => $offer->id,
            'channel_id' => $channel->id,
        ]);
    }

    /** @test */
    public function can_not_detach_channel_if_offer_not_found()
    {
        $offer = $this->createOffer();
        $channel = factory(\App\Partner::class)->create();

        DB::table('channel_offer')->insert([
            'offer_id' => $offer->id,
            'channel_id' => $channel->id,
        ]);

        $response = $this->get('/dashboard/offer/unlink_channel/' . base64_encode($offer->id + 10) . '/' . base64_encode($channel->id));

        $response->assertStatus(404);
    }

    /** @test */
    public function can_not_detach_channel_if_channel_not_found()
    {
        $offer = $this->createOffer();
        $channel = factory(\App\Partner::class)->create();

        DB::table('channel_offer')->insert([
            'offer_id' => $offer->id,
            'channel_id' => $channel->id,
        ]);

        $response = $this->get('/dashboard/offer/unlink_channel/' . base64_encode($offer->id) . '/' . base64_encode($channel->id + 10));

        $response->assertStatus(404);
    }

    /** @test */
    public function can_attach_tier()
    {
        $offer = $this->createOffer();
        $tier = factory(\App\Tier::class)->create();

        $data = [
            'tier_id' => $tier->id,
        ];

        $response = $this->post('/dashboard/offer/link_tier/' . base64_encode($offer->id), $data);

        $response->assertStatus(200)
                 ->assertViewIs('offer.list.tierList')
                 ->assertSee($tier->name);
    }

    /** @test */
    public function can_not_attach_tier_if_offer_not_found()
    {
        $offer = $this->createOffer();
        $tier = factory(\App\Tier::class)->create();

        if (!$offer || !$tier) {
            App::abort(404);
        }

        $data = [
            'tier_id' => $tier->id,
        ];

        $response = $this->post('/dashboard/offer/link_tier/' . base64_encode($offer->id + 10), $data);

        $response->assertStatus(404);
    }

    /** @test */
    public function can_not_attach_tier_if_tier_not_found()
    {
        $offer = $this->createOffer();
        $tier = factory(\App\Tier::class)->create();

        $data = [
            'tier_id' => $tier->id + 10,
        ];

        $response = $this->post('/dashboard/offer/link_tier/' . base64_encode($offer->id), $data);

        $response->assertStatus(404);
    }

    /** @test */
    public function can_detach_tier()
    {
        $offer = $this->createOffer();
        $tier = factory(\App\Tier::class)->create();

        $offer->countries()->save($tier);

        $response = $this->get('/dashboard/offer/unlink_tier/' . base64_encode($offer->id) . '/' . base64_encode($tier->id));

        $response->assertStatus(200)
                 ->assertViewIs('offer.list.tierList')
                 ->assertDontSee($tier->name);

        $this->assertDatabaseMissing('tier_offer', [
            'offer_id' => $offer->id,
            'tier_id' => $tier->id,
        ]);
    }

    /** @test */
    public function can_not_detach_tier_if_offer_not_found()
    {
        $offer = $this->createOffer();
        $tier = factory(\App\Tier::class)->create();

        $offer->countries()->save($tier);

        $response = $this->get('/dashboard/offer/unlink_tier/' . base64_encode($offer->id + 10) . '/' . base64_encode($tier->id));

        $response->assertStatus(404);
    }

    /** @test */
    public function can_not_detach_tier_if_tier_not_found()
    {
        $offer = $this->createOffer();
        $tier = factory(\App\Tier::class)->create();

        $offer->countries()->save($tier);

        $response = $this->get('/dashboard/offer/unlink_tier/' . base64_encode($offer->id) . '/' . base64_encode($tier->id + 10));

        $response->assertStatus(404);
    }

    /** @test */
    public function can_view_pagination_page()
    {
        $perPage = 15;
        $pageCount = ceil(Offer::where('draft', 0)->count() / $perPage);

        if ($pageCount > 1) {
            $response = $this->get('/dashboard/offer?page=' . $pageCount);

            $response->assertStatus(200)
                     ->assertViewIs('offer.list')
                     ->assertSee('offer?page=1')
                     ->assertDontSee('No Items Found');
        }
    }

    /** @test */
    public function can_not_view_pagination_page_if_one_page()
    {
        $perPage = 15;
        $page = ceil(Offer::where('draft', 0)->count() / $perPage) + 1;

        $response = $this->get('/dashboard/offer?page=' . $page);

        $response->assertStatus(200)
                 ->assertViewIs('offer.list')
                 ->assertDontSee('offer?page=' . $page)
                 ->assertSee('No Items Found');
    }
}
