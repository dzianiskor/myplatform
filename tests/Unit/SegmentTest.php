<?php

namespace Tests\Unit;

use App\Admin;
use App\Segment;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SegmentTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    protected $user;

    public function setUp()
    {
        parent::setUp();

        $user = Admin::where('email', 'info@bluloyalty.com')->first();
        $this->user = $user;
        $this->actingAs($user)
             ->session([
                 'user_roles' => [
                     'BLU Admin',
                 ],
                 'user_permissions' => [
                     'create_segments',
                     'view_segments',
                     'edit_segments',
                     'delete_segments',
                 ],
             ]);
    }

    /** @test */
    public function can_create_new_segment_page_with_permissions()
    {
        $response = $this->get('/dashboard/segments/new');

        $response->assertStatus(302)
                 ->assertRedirect()
                 ->assertSeeText('/dashboard/segments/view/');
    }

    /** @test */
    public function can_not_create_new_segment_page_without_permissions()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $response = $this->get('/dashboard/segments/new');

        $response->assertStatus(302)
                 ->assertRedirect()
                 ->assertSeeText('/dashboard');

    }

    /** @test */
    public function can_open_edit_segment_with_permissions()
    {
        $segment = factory(\App\Segment::class)->create();

        $response = $this->get('/dashboard/segments/view/' . base64_encode($segment->id));

        $response->assertStatus(200)
                 ->assertViewIs('segment.view');
    }

    /** @test */
    public function can_not_open_segment_page_which_is_not_exists()
    {
        $segment = factory(\App\Segment::class)->create();

        $response = $this->get('/dashboard/segments/view/' . base64_encode($segment->id + 10));

        $response->assertStatus(404);
    }

    /** @test */
    public function can_not_open_segment_page_which_is_not_draft_and_partner_can_not_view()
    {
        $segment = factory(\App\Segment::class)->create([
            'partner_id' => 2,
            'draft' => 0,
        ]);

        $response = $this->get('/dashboard/segments/view/' . base64_encode($segment->id));

        $response->assertStatus(302)
                 ->assertRedirect()
                 ->assertSeeText('/dashboard');
    }

    /** @test */
    public function can_not_open_edit_segment_without_permissions()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $segment = factory(\App\Segment::class)->create();

        $response = $this->get('/dashboard/segments/view/' . base64_encode($segment->id));

        $response->assertStatus(302)
                 ->assertRedirect()
                 ->assertSeeText('/dashboard');
    }

    /** @test */
    public function can_update_segment_with_criteria()
    {
        $segment = factory(\App\Segment::class)->create();

        $data = [
            'name' => uniqid(),
            'partner_id' => 1,
            'criteria' => [
                'user.email' => [
                    'name' => ['user.email'],
                    'email' => ['@gmail.com'],
                ]
            ],
        ];

        $response = $this->post('/dashboard/segments/update/' . base64_encode($segment->id), $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/segments')
                 ->assertSessionMissing('alert_error');

        $this->assertDatabaseHas('segment', [
            'id' => $segment->id,
            'name' => $data['name'],
        ]);

        $this->assertDatabaseHas('attribute', [
            'mapping_key' => $segment->id,
            'name' => 'user.email',
            'json_data' => '{"name":"user.email","email":"@gmail.com"}',
        ]);
    }

    /** @test */
    public function can_not_update_segment_without_criteria()
    {
        $segment = factory(\App\Segment::class)->create();

        $data = [
            'name' => uniqid(),
        ];

        $response = $this->post('/dashboard/segments/update/' . base64_encode($segment->id), $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/segments')
                 ->assertSessionHas('alert_error');

        $this->assertDatabaseMissing('segment', [
            'id' => $segment->id,
            'name' => $data['name'],
        ]);
    }

    /** @test */
    public function can_not_update_segment_which_is_not_exists()
    {
        $segment = factory(\App\Segment::class)->create();

        $data = [
            'name' => uniqid(),
        ];

        $response = $this->post('/dashboard/segments/update/' . base64_encode($segment->id + 10), $data);

        $response->assertStatus(404);

        $this->assertDatabaseMissing('segment', [
            'id' => $segment->id,
            'name' => $data['name'],
        ]);
    }

    /** @test */
    public function can_not_update_segment_without_permission()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $segment = factory(\App\Segment::class)->create();

        $data = [
            'name' => uniqid(),
        ];

        $response = $this->post('/dashboard/segments/update/' . base64_encode($segment->id), $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard');

        $this->assertDatabaseMissing('segment', [
            'id' => $segment->id,
            'name' => $data['name'],
        ]);
    }

    /** @test */
    public function can_not_update_segment_without_required_fields()
    {
        $segment = factory(\App\Segment::class)->create();

        $data = [];
        $response = $this->post('/dashboard/segments/update/' . base64_encode($segment->id), $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/segments/view/' . base64_encode($segment->id))
                 ->assertSessionHasErrors(['name']);

        $data = [
            'name' => 'q',
        ];
        $response = $this->post('/dashboard/segments/update/' . base64_encode($segment->id), $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/segments/view/' . base64_encode($segment->id))
                 ->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function can_delete_segment_with_permission()
    {
        $segment = factory(\App\Segment::class)->create();
        $segment->attributes()->save(factory(\App\Attribute::class)->create());

        $response = $this->get('/dashboard/segments/delete/' . base64_encode($segment->id));

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/segments');

        $this->assertDatabaseMissing('segment', [
            'id' => $segment->id,
        ]);

        $this->assertDatabaseMissing('attribute', [
            'mapping_key' => $segment->id,
        ]);
    }

    /** @test */
    public function can_not_delete_segment_without_permission()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $segment = factory(\App\Segment::class)->create();

        $response = $this->get('/dashboard/segments/delete/' . base64_encode($segment->id));

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard');

        $this->assertDatabaseHas('segment', [
            'id' => $segment->id,
        ]);
    }

    /** @test */
    public function can_not_delete_segment_which_is_not_exists()
    {
        $segment = factory(\App\Segment::class)->create();

        $response = $this->get('/dashboard/segments/delete/' . base64_encode($segment->id + 10));

        $response->assertStatus(404);

        $this->assertDatabaseHas('segment', [
            'id' => $segment->id,
        ]);
    }

    /** @test */
    public function can_open_segments_page_with_permission()
    {
        factory(\App\Segment::class)->create();

        $response = $this->get('/dashboard/segments');

        $response->assertStatus(200)
                 ->assertViewIs('segment.list')
                 ->assertViewHas('segments');
    }

    /** @test */
    public function can_not_open_segments_page_without_permission()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        factory(\App\Segment::class)->create();

        $response = $this->get('/dashboard/segments');

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard');
    }

    /** @test */
    public function can_filter_segments_by_search_query()
    {
        $segment = factory(\App\Segment::class)->create();

        $response = $this->get('/dashboard/segments?q=' . uniqid());

        $response->assertStatus(200)
                 ->assertViewIs('segment.list')
                 ->assertDontSee($segment->name);

        $response = $this->get('/dashboard/segments?q=' . $segment->name);

        $response->assertStatus(200)
                 ->assertViewIs('segment.list')
                 ->assertSee($segment->name);
    }

    /** @test */
    public function can_filter_segments_by_partner()
    {
        $segment = factory(\App\Segment::class)->create();

        $partner1 = factory(\App\Partner::class)->create();
        $partner2 = factory(\App\Partner::class)->create();

        $response = $this->get('/dashboard/segments?partner_segment_list[]=' . $partner1->id . '&partner_segment_list[]=' . $partner2->id);

        $response->assertStatus(200)
                 ->assertViewIs('segment.list')
                 ->assertDontSee($segment->name);

        $response = $this->get('/dashboard/segments?partner_segment_list[]=' . $segment->partner_id . '&partner_segment_list[]=' . $partner2->id);

        $response->assertStatus(200)
                 ->assertViewIs('segment.list')
                 ->assertSee($segment->name);
    }

    /** @test */
    public function can_load_segment_criteria()
    {
        factory(\App\Segment::class)->create();

        $data = [
            'name' => 'user.email',
            'email' => uniqid() . '@gmail.com',
        ];

        $response = $this->get('/dashboard/segments/ejs/form_user_email.ejs?name=' . $data['name'] . '&email=' . $data['email']);

        $response->assertStatus(200)
                 ->assertViewIs('segment.ejs.form_user_email')
                 ->assertSee($data['email']);
    }

    /** @test */
    public function can_view_pagination_page()
    {
        $perPage = 15;
        $pageCount = ceil(Segment::where('draft', 0)->count() / $perPage);

        if ($pageCount > 1) {
            $response = $this->get('/dashboard/segments?page=' . $pageCount);

            $response->assertStatus(200)
                     ->assertViewIs('segment.list')
                     ->assertSee('segments?page=1')
                     ->assertDontSee('No segments found');
        }
    }

    /** @test */
    public function can_not_view_pagination_page_if_one_page()
    {
        $perPage = 15;
        $page = ceil(Segment::where('draft', 0)->count() / $perPage) + 1;

        $response = $this->get('/dashboard/segments?page=' . $page);

        $response->assertStatus(200)
                 ->assertViewIs('segment.list')
                 ->assertDontSee('segments?page=' . $page)
                 ->assertSee('No segments found');
    }
}