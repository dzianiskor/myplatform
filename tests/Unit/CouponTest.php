<?php

namespace Tests\Unit;

use App\Batch;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use App\Admin;

class CouponTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    protected $user;

    public function setUp()
    {
        parent::setUp();

        $user = Admin::where('email', 'info@bluloyalty.com')->first();
        $this->user = $user;
        $this->actingAs($user)
             ->session([
                 'user_roles' => [
                     'BLU Admin',
                 ],
                 'user_permissions' => [
                     'view_batches',
                     'create_network'
                 ],
             ]);
    }

    /** @test */
    public function can_view_coupon_list_page_with_permission()
    {
        $response = $this->get('/dashboard/coupons');
        $response->assertStatus(200);

        $this->session(['user_roles' => []]);

        $response = $this->get('/dashboard/coupons');
        $response->assertStatus(200);
    }

    /** @test */
    public function can_not_view_coupon_list_page_without_permission()
    {
        $this->session(['user_permissions' => []]);

        $response = $this->get('/dashboard/coupons');

        $response->assertStatus(302)
                 ->assertRedirect()
                 ->assertRedirect('/dashboard');
    }

    /** @test */
    public function can_filter_coupons_by_search_query()
    {
        $batch = factory(\App\Batch::class)->create();
        $response = $this->get('/dashboard/coupons?q=' . uniqid());

        $response->assertStatus(200)
                 ->assertViewIs('coupon.batch.list')
                 ->assertDontSee($batch->name);

        $response = $this->get('/dashboard/coupons?q=' . $batch->name);

        $response->assertStatus(200)
                 ->assertViewIs('coupon.batch.list')
                 ->assertSee($batch->name);
    }

    /** @test */
    public function can_view_edit_page_with_permission()
    {
        $batch = factory(\App\Batch::class)->create();
        $batch->partner_id = \App\Partner::find(1)->first()->id;
        $batch->save();

        $response = $this->get('/dashboard/coupons/edit/' . base64_encode($batch->id));
    }

    /** @test */
    public function can_not_view_edit_page_without_permission()
    {
        $batch = factory(\App\Batch::class)->create();
        $response = $this->get('/dashboard/coupons/edit/' . base64_encode($batch->id));

        $response->assertStatus(302)
                 ->assertRedirect()
                 ->assertSeeText('/dashboard');
    }

    /** @test */
    public function can_not_view_edit_page_if_batch_incorrect()
    {
        $batch = factory(\App\Batch::class)->create();
        $response = $this->get('/dashboard/coupons/edit/' . base64_encode($batch->id + 10));

        $response->assertStatus(404)
                 ->assertDontSeeText($batch->name);
    }

    /** @test */
    public function can_not_view_edit_page_if_partner_incorrect()
    {
        $batch = factory(\App\Batch::class)->create();
        $response = $this->get('/dashboard/coupons/edit/' . base64_encode($batch->id));

        $response->assertStatus(302)
                 ->assertRedirect()
                 ->assertSeeText('/dashboard');
    }

    /** @test */
    public function can_update_coupon_with_permission()
    {
        $batch = factory(\App\Batch::class)->create();
        $data = [
            'name' => uniqid(),
            'partner_id' => factory(\App\Partner::class)->create()->id,
            'country_id' => factory(\App\Country::class)->create()->id,
            'draft' => 0,
            'type' => 'Coupons',
            'price' => '0.00',
            'valid_from' => Carbon::today()->subDays(10),
            'valid_to' => Carbon::today()->subDays(10),
            'number_of_items' => random_int(1, 10),
        ];

        $response = $this->call('POST', '/dashboard/coupons/update/' . base64_encode($batch->id), $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/coupons/');

        $this->assertDatabaseHas('batch', [
            'id' => $batch->id,
            'name' => $data['name'],
            'partner_id' => $data['partner_id']
        ]);
    }

    /** @test */
    public function can_not_update_coupon_without_permission()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $batch = factory(\App\Batch::class)->create();
        $data = [
            'name' => uniqid(),
            'partner_id' => factory(\App\Partner::class)->create()->id,
            'country_id' => factory(\App\Country::class)->create()->id,
            'draft' => 0,
            'type' => 'Coupons',
            'price' => '0.00',
            'valid_from' => Carbon::today()->subDays(10),
            'valid_to' => Carbon::today()->subDays(10),
            'number_of_items' => random_int(1, 10),
        ];

        $response = $this->call('POST', '/dashboard/coupons/update/' . base64_encode($batch->id), $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard');

        $this->assertDatabaseMissing('batch', [
            'id' => $batch->id,
            'name' => $data['name']
        ]);
    }

    /** @test */
    public function can_not_update_coupon_if_batch_is_incorrect()
    {
        $batch = factory(\App\Batch::class)->create();
        $data = [
            'name' => uniqid(),
            'partner_id' => factory(\App\Partner::class)->create()->id,
            'country_id' => factory(\App\Country::class)->create()->id,
            'draft' => 0,
            'type' => 'Coupons',
            'price' => '0.00',
            'valid_from' => Carbon::today()->subDays(10),
            'valid_to' => Carbon::today()->subDays(10),
            'number_of_items' => random_int(1, 10),
        ];

        $response = $this->call('POST', '/dashboard/coupons/update/' . base64_encode($batch->id + 10), $data);

        $response->assertStatus(404);

        $this->assertDatabaseMissing('batch', [
            'id' => $batch->id,
            'name' => $data['name']
        ]);
    }

    /** @test */
    public function can_validate_errors()
    {
        $batch = factory(\App\Batch::class)->create();

        $data = [
            'type' => '',
            'number_of_items' => '',
            'partner_id' => ''
        ];
        $response = $this->post('/dashboard/coupons/update/' . base64_encode($batch->id), $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/coupons/edit/' . base64_encode($batch->id))
                 ->assertSessionHasErrors('type')
                 ->assertSessionHasErrors('number_of_items')
                 ->assertSessionHasErrors('partner_id');

        $data = [
            'type' => 'Coupons',
            'partner_id' => uniqid(),
        ];
        $response = $this->post('/dashboard/coupons/update/' . base64_encode($batch->id), $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/coupons/edit/' . base64_encode($batch->id))
                 ->assertSessionHasErrors('partner_id');

        $data = [
            'type' => 'Coupons',
            'partner_id' => factory(\App\Partner::class)->create()->id,
            'country_id' => uniqid(),
        ];
        $response = $this->post('/dashboard/coupons/update/' . base64_encode($batch->id), $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/coupons/edit/' . base64_encode($batch->id))
                 ->assertSessionHasErrors('country_id');

        $data = [
            'type' => 'Coupons',
            'partner_id' => factory(\App\Partner::class)->create()->id,
            'country_id' => factory(\App\Country::class)->create()->id,
            'number_of_items' => uniqid(),
        ];
        $response = $this->post('/dashboard/coupons/update/' . base64_encode($batch->id), $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/coupons/edit/' . base64_encode($batch->id))
                 ->assertSessionHasErrors('number_of_items');

        $data = [
            'type' => 'Coupons',
            'partner_id' => factory(\App\Partner::class)->create()->id,
            'country_id' => factory(\App\Country::class)->create()->id,
            'number_of_items' => random_int(1, 10),
            'valid_from' => random_int(1, 10),
            'valid_to' => random_int(1, 10),
        ];
        $response = $this->post('/dashboard/coupons/update/' . base64_encode($batch->id), $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/coupons/edit/' . base64_encode($batch->id))
                 ->assertSessionHasErrors('valid_from')
                 ->assertSessionHasErrors('valid_to');
    }

    /** @test */
    public function can_delete_coupon_with_permission()
    {
        $batch = factory(\App\Batch::class)->create();
        $response = $this->get('/dashboard/coupons/destroy/' . base64_encode($batch->id));

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/coupons');

        $this->assertDatabaseMissing('batch', [
            'id' => $batch->id,
        ]);
    }

    /** @test */
    public function can_not_delete_coupon_without_permission()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $batch = factory(\App\Batch::class)->create();
        $response = $this->get('/dashboard/coupons/destroy/' . base64_encode($batch->id));

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard');

        $this->assertDatabaseHas('batch', [
            'id' => $batch->id,
        ]);
    }

    /** @test */
    public function can_not_delete_coupon_with_incorrect_id()
    {
        $batch = factory(\App\Batch::class)->create();
        $response = $this->get('/dashboard/coupons/destroy/' . base64_encode($batch->id + 10));

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/coupons');

        $this->assertDatabaseHas('batch', [
            'id' => $batch->id,
        ]);
    }

    /** @test */
    public function can_get_partner_countries()
    {
        $partner = factory(\App\Partner::class)->create();
        $response = $this->get('/dashboard/coupons/partnercountries/' . base64_encode($partner->id));

        $response->assertStatus(200);
    }

    /** @test */
    public function can_verify_coupon()
    {
        $coupon = factory(\App\Coupon::class)->create();
        $response = $this->get('/dashboard/coupons/verifycoupon/' . base64_encode($coupon->coupon_code));

        $this->assertDatabaseHas('coupon', [
            'coupon_code' => $coupon->coupon_code,
        ]);
    }

    /** @test */
    public function can_not_verify_coupon_if_coupon_is_incorrect()
    {
        $coupon = factory(\App\Coupon::class)->create();
        $response = $this->get('/dashboard/coupons/verifycoupon/' . base64_encode($coupon->id));

        $response->assertStatus(404);

        $this->assertDatabaseMissing('coupon', [
            'coupon_code' => $coupon->id,
        ]);
    }

    /** @test */
    public function can_download_file_with_permission()
    {
        $batch = factory(\App\Batch::class)->create();
        $response = $this->get('/dashboard/coupons/download/' . base64_encode($batch->id) . '/csv');

        $response->assertStatus(200);
    }

    /** @test */
    public function can_not_download_file_without_permission()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $batch = factory(\App\Batch::class)->create();

        $response = $this->get('/dashboard/coupons/download/' . base64_encode($batch->id) . '/csv');

        $response->assertStatus(302)
                 ->assertRedirect()
                 ->assertRedirect('/dashboard');
    }

    /** @test */
    public function can_not_download_file_with_incorrect_id()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $batch = factory(\App\Batch::class)->create();

        $response = $this->get('/dashboard/coupons/download/' . base64_encode($batch->id) . '/csv');

        $response->assertStatus(302)
                 ->assertRedirect()
                 ->assertRedirect('/dashboard');
    }

    /** @test */
    public function can_view_pagination_page()
    {
        $perPage = 15;
        $pageCount = ceil(Batch::where('draft', 0)->count() / $perPage);

        if ($pageCount > 1) {
            $response = $this->get('/dashboard/coupons?page=' . $pageCount);

            $response->assertStatus(200)
                     ->assertViewIs('coupon.batch.list')
                     ->assertSee('coupons?page=1');
        }
    }

    /** @test */
    public function can_not_view_pagination_page_if_it_does_not_exist()
    {
        $perPage = 15;
        $page = ceil(Batch::where('draft', 0)->count() / $perPage) + 1;

        $response = $this->get('/dashboard/coupons?page=' . $page);

        $response->assertStatus(200)
                 ->assertViewIs('coupon.batch.list')
                 ->assertDontSee('admins?page=' . $page)
                 ->assertSee('No Coupons Found');
    }
}
