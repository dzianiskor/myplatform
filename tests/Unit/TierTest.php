<?php

namespace Tests\Unit;

use App\Admin;
use App\Tier;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TierTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    protected $user;

    public function setUp()
    {
        parent::setUp();

        $user = Admin::where('email', 'info@bluloyalty.com')->first();
        $this->user = $user;
        $this->actingAs($user)
             ->session([
                 'user_roles' => [
                     'BLU Admin',
                 ],
                 'user_permissions' => [
                     'create_tiers',
                     'view_tiers',
                     'edit_tiers',
                     'delete_tiers',
                 ],
             ]);
    }

    /** @test */
    public function can_create_new_tier_page_with_permissions()
    {
        $response = $this->get('/dashboard/tiers/new');

        $response->assertStatus(302)
                 ->assertRedirect()
                 ->assertSeeText('/dashboard/tiers/view/');
    }

    /** @test */
    public function can_not_create_new_tier_page_without_permissions()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $response = $this->get('/dashboard/tiers/new');

        $response->assertStatus(302)
                 ->assertRedirect()
                 ->assertSeeText('/dashboard');
    }

    /** @test */
    public function can_open_edit_tier_with_permissions()
    {
        $tier = factory(\App\Tier::class)->create();

        $response = $this->get('/dashboard/tiers/view/' . base64_encode($tier->id));

        $response->assertStatus(200)
                 ->assertViewIs('tier.view');
    }

    /** @test */
    public function can_not_open_tier_page_without_permissions()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $tier = factory(\App\Tier::class)->create();

        $response = $this->get('/dashboard/tiers/view/' . base64_encode($tier->id));

        $response->assertStatus(302)
                 ->assertRedirect()
                 ->assertSeeText('/dashboard');
    }

    /** @test */
    public function can_not_open_tier_page_which_is_not_exists()
    {
        $tier = factory(\App\Tier::class)->create();

        $response = $this->get('/dashboard/tiers/view/' . base64_encode($tier->id + 10));

        $response->assertStatus(404);
    }

    /** @test */
    public function can_not_open_tier_which_is_not_draft_and_partner_can_not_view()
    {
        $partner = factory(\App\Partner::class)->create();
        $tier = factory(\App\Tier::class)->create([
            'partner_id' => $partner->id,
        ]);

        $response = $this->get('/dashboard/tiers/view/' . base64_encode($tier->id));

        $response->assertStatus(302)
                 ->assertRedirect()
                 ->assertSeeText('/dashboard');
    }

    /** @test */
    public function can_update_tier_with_permission()
    {
        $tier = factory(\App\Tier::class)->create();

        $data = [
            'name' => uniqid(),
        ];

        $response = $this->post('/dashboard/tiers/update/' . base64_encode($tier->id), $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/tiers');

        $this->assertDatabaseHas('tier', [
            'id' => $tier->id,
            'name' => $data['name'],
        ]);
    }

    /** @test */
    public function can_not_update_tier_without_permission()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $tier = factory(\App\Tier::class)->create();

        $data = [
            'name' => uniqid(),
        ];

        $response = $this->post('/dashboard/tiers/update/' . base64_encode($tier->id), $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard');

        $this->assertDatabaseMissing('tier', [
            'id' => $tier->id,
            'name' => $data['name'],
        ]);
    }

    /** @test */
    public function can_not_update_tier_which_is_not_exists()
    {
        $tier = factory(\App\Tier::class)->create();

        $data = [
            'name' => uniqid(),
        ];

        $response = $this->post('/dashboard/tiers/update/' . base64_encode($tier->id + 10), $data);

        $response->assertStatus(404);

        $this->assertDatabaseMissing('tier', [
            'id' => $tier->id,
            'name' => $data['name'],
        ]);
    }

    /** @test */
    public function can_not_update_tier_without_required_fields()
    {
        $tier = factory(\App\Tier::class)->create();

        $data = [];
        $response = $this->post('/dashboard/tiers/update/' . base64_encode($tier->id), $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/tiers/view/' . $tier->id)
                 ->assertSessionHasErrors(['name']);

        $data = [
            'name' => 'q',
        ];
        $response = $this->post('/dashboard/tiers/update/' . base64_encode($tier->id), $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/tiers/view/' . $tier->id)
                 ->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function can_delete_tier_with_permission()
    {
        $tier = factory(\App\Tier::class)->create();

        $response = $this->get('/dashboard/tiers/delete/' . base64_encode($tier->id));

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/tiers');

        $this->assertDatabaseMissing('tier', [
            'id' => $tier->id,
        ]);
    }

    /** @test */
    public function can_not_delete_tier_without_permission()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $tier = factory(\App\Tier::class)->create();

        $response = $this->get('/dashboard/tiers/delete/' . base64_encode($tier->id));

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard');

        $this->assertDatabaseHas('tier', [
            'id' => $tier->id,
        ]);
    }

    /** @test */
    public function can_not_delete_tier_which_is_not_exists()
    {
        $tier = factory(\App\Tier::class)->create();

        $response = $this->get('/dashboard/tiers/delete/' . base64_encode($tier->id + 10));

        $response->assertStatus(404);

        $this->assertDatabaseHas('tier', [
            'id' => $tier->id,
        ]);
    }

    /** @test */
    public function can_open_tiers_page()
    {
        factory(\App\Tier::class)->create();

        $response = $this->get('/dashboard/tiers');

        $response->assertStatus(200)
                 ->assertViewIs('tier.list')
                 ->assertViewHas('tiers');
    }

    /** @test */
    public function can_not_open_tiers_page_without_permission()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        factory(\App\Tier::class)->create();

        $response = $this->get('/dashboard/tiers');

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard');
    }

    /** @test */
    public function can_filter_tiers_by_search_query()
    {
        $tier = factory(\App\Tier::class)->create();

        $response = $this->get('/dashboard/tiers?q=' . uniqid());

        $response->assertStatus(200)
                 ->assertViewIs('tier.list')
                 ->assertDontSee($tier->name);

        $response = $this->get('/dashboard/tiers?q=' . $tier->name);

        $response->assertStatus(200)
                 ->assertViewIs('tier.list')
                 ->assertSee($tier->name);
    }

    /** @test */
    public function can_add_tier_criteria()
    {
        $tier = factory(\App\Tier::class)->create();

        $response = $this->get('/dashboard/tiercriteria/new/' . $tier->id);

        $response->assertStatus(200)
                 ->assertViewIs('tiercriteria.form');
    }

    /** @test */
    public function can_save_tier_criteria()
    {
        $tier = factory(\App\Tier::class)->create();
        $tierCriteria = factory(\App\Tiercriteria::class)->create([
            'tier_id' => $tier->id,
        ]);

        $data = [
            'tiercriteria_criterion' => 'RewardedPoints',
            'tiercriteria_name' => uniqid(),
            'tiercriteria_start_value' => '10',
            'tiercriteria_end_value' => '20',
        ];

        $response = $this->post('/dashboard/tiercriteria/update/' . base64_encode($tierCriteria->id), $data);

        $response->assertStatus(200)
                 ->assertJson([
                     'failed' => false,
                     'view_url' => url('dashboard/tiercriteria/list/' . $tierCriteria->tier_id),
                 ]);

        $this->assertDatabaseHas('tier_criteria', [
            'id' => $tierCriteria->id,
            'name' => $data['tiercriteria_name'],
        ]);
    }

    /** @test */
    public function can_not_save_tier_criteria_without_required_fields()
    {
        $tier = factory(\App\Tier::class)->create();
        $tierCriteria = factory(\App\Tiercriteria::class)->create([
            'tier_id' => $tier->id,
        ]);

        $data = [];

        $response = $this->post('/dashboard/tiercriteria/update/' . base64_encode($tierCriteria->id), $data);

        $response->assertStatus(200)
                 ->assertJsonFragment([
                     'failed' => true,
                 ]);
    }

    /** @test */
    public function can_get_tier_criteria_list()
    {
        $tier = factory(\App\Tier::class)->create();
        $tierCriteria = factory(\App\Tiercriteria::class)->create([
            'tier_id' => $tier->id,
        ]);

        $response = $this->get('/dashboard/tiercriteria/list/' . $tier->id);

        $response->assertStatus(200)
                 ->assertViewIs('tiercriteria.list')
                 ->assertSee($tierCriteria->name);
    }

    /** @test */
    public function can_delete_tier_criteria()
    {
        $tier = factory(\App\Tier::class)->create();
        $tierCriteria = factory(\App\Tiercriteria::class)->create([
            'tier_id' => $tier->id,
        ]);

        $response = $this->get('/dashboard/tiercriteria/delete/' . base64_encode($tierCriteria->id));

        $response->assertStatus(200)
                 ->assertJson([
                     'failed' => false,
                 ]);

        $this->assertDatabaseMissing('tier_criteria', [
            'id' => $tierCriteria->id,
            'name' => $tierCriteria->name,
        ]);
    }

    /** @test */
    public function can_view_pagination_page()
    {
        $perPage = 15;
        $pageCount = ceil(Tier::where('draft', 0)->count() / $perPage);

        if ($pageCount > 1) {
            $response = $this->get('/dashboard/tiers?page=' . $pageCount);

            $response->assertStatus(200)
                     ->assertViewIs('tier.list')
                     ->assertSee('tiers?page=1')
                     ->assertDontSee('No Tiers Found');
        }
    }

    /** @test */
    public function can_not_view_pagination_page_if_one_page()
    {
        $perPage = 15;
        $page = ceil(Tier::where('draft', 0)->count() / $perPage) + 1;

        $response = $this->get('/dashboard/tiers?page=' . $page);

        $response->assertStatus(200)
                 ->assertViewIs('tier.list')
                 ->assertDontSee('tiers?page=' . $page)
                 ->assertSee('No Tiers Found');
    }
}
