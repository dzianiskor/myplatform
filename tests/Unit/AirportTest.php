<?php

namespace Tests\Unit;

use App\Admin;
use App\Airport;
use App\Language;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AirportTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    protected $user;

    public function setUp()
    {
        parent::setUp();

        $user = Admin::where('email', 'info@bluloyalty.com')->first();
        $this->user = $user;
        $this->actingAs($user)
             ->session([
                 'user_roles' => [
                     'BLU Admin',
                 ],
             ]);
    }

    /** @test */
    public function can_create_new_airport_page()
    {
        $response = $this->get('/dashboard/airports/new');

        $response->assertStatus(200)
                 ->assertViewIs('airport.new');
    }

    /** @test */
    public function can_open_edit_airport()
    {
        $airport = factory(\App\Airport::class)->create();

        $response = $this->get('/dashboard/airports/view/' . $airport->id);

        $response->assertStatus(200)
                 ->assertViewIs('airport.view');
    }

    /** @test */
    public function can_not_open_airport_page_which_is_not_exists()
    {
        $airport = factory(\App\Airport::class)->create();

        $response = $this->get('/dashboard/airports/view/' . ($airport->id + 10));

        $response->assertStatus(404);
    }

    /** @test */
    public function can_update_airport()
    {
        $airport = factory(\App\Airport::class)->create();

        $data = [
            'name' => uniqid(),
            'code' => 'ABC',
            'city' => uniqid(),
            'state' => uniqid(),
            'country' => uniqid()
        ];

        $response = $this->post('/dashboard/airports/update/' . $airport->id, $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/airports');

        $this->assertDatabaseHas('airports', [
            'id' => $airport->id,
            'name' => $data['name'],
            'code' => $data['code'],
            'city' => $data['city'],
            'state' => $data['state'],
            'country' => $data['country'],
        ]);
    }

    /** @test */
    public function can_not_update_airport_which_is_not_exists()
    {
        $airport = factory(\App\Airport::class)->create();

        $data = [
            'name' => uniqid(),
            'code' => 'ABC',
            'city' => uniqid(),
            'state' => uniqid(),
            'country' => uniqid()
        ];

        $response = $this->post('/dashboard/airports/update/' . ($airport->id + 10), $data);

        $response->assertStatus(404);

        $this->assertDatabaseMissing('airports', [
            'id' => $airport->id,
            'name' => $data['name'],
            'code' => $data['code'],
            'city' => $data['city'],
            'state' => $data['state'],
            'country' => $data['country'],
        ]);
    }

    /** @test */
    public function can_not_update_airport_without_required_fields()
    {
        $airport = factory(\App\Airport::class)->create();

        $data = [];
        $response = $this->post('/dashboard/airports/update/' . $airport->id, $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/airports/view/' . $airport->id)
                 ->assertSessionHasErrors(['name', 'code', 'city', 'state', 'country']);
    }

    /** @test */
    public function can_delete_airport_with()
    {
        $airport = factory(\App\Airport::class)->create();

        $response = $this->get('/dashboard/airports/delete/' . $airport->id);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/airports');

        $this->assertDatabaseMissing('airports', [
            'id' => $airport->id,
        ]);
    }

    /** @test */
    public function can_not_delete_airport_which_is_not_exists()
    {
        $airport = factory(\App\Airport::class)->create();

        $response = $this->get('/dashboard/airports/delete/' . ($airport->id + 10));

        $response->assertStatus(404);

        $this->assertDatabaseHas('airports', [
            'id' => $airport->id,
        ]);
    }

    /** @test */
    public function can_open_airports_page()
    {
        factory(\App\Airport::class)->create();

        $response = $this->get('/dashboard/airports');

        $response->assertStatus(200)
                 ->assertViewIs('airport.list')
                 ->assertViewHas('airports');
    }

    /** @test */
    public function can_filter_airports_by_search_query()
    {
        $airport = factory(\App\Airport::class)->create();

        $response = $this->get('/dashboard/airports?q=' . uniqid());

        $response->assertStatus(200)
                 ->assertViewIs('airport.list')
                 ->assertDontSee($airport->name);

        $response = $this->get('/dashboard/airports?q=' . $airport->name);

        $response->assertStatus(200)
                 ->assertViewIs('airport.list')
                 ->assertSee($airport->name);
    }

    /** @test */
    public function can_view_pagination_page()
    {
        $perPage = 15;
        $pageCount = ceil(Airport::count() / $perPage);

        if ($pageCount > 1) {
            $response = $this->get('/dashboard/airports?page=' . $pageCount);

            $response->assertStatus(200)
                     ->assertViewIs('airport.list')
                     ->assertSee('airports?page=1')
                     ->assertDontSee('No Airports Found');
        }
    }

    /** @test */
    public function can_not_view_pagination_page_if_one_page()
    {
        $perPage = 15;
        $page = ceil(Airport::count() / $perPage) + 1;

        $response = $this->get('/dashboard/airports?page=' . $page);

        $response->assertStatus(200)
                 ->assertViewIs('airport.list')
                 ->assertDontSee('airports?page=' . $page)
                 ->assertSee('No Airports Found');
    }
}
