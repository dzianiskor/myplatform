<?php

namespace Tests\Unit;

use App\Admin;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class LoyaltyTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    protected $user;

    public function setUp()
    {
        parent::setUp();

        $user = Admin::where('email', 'info@bluloyalty.com')->first();
        $this->user = $user;
        $this->actingAs($user)
             ->session([
                 'user_roles' => [
                     'BLU Admin',
                 ],
                 'user_permissions' => [
                     'create_loyalty',
                     'view_loyalty',
                     'add_loyalty',
                     'edit_loyalty',
                     'delete_loyalty',
                 ],
             ]);
    }

    /**
     * @return Affiliate
     */
    protected function createLoyalty($data = [])
    {
        $loyalty = factory(\App\Loyalty::class)->create(array_merge(['partner_id' => \App\Partner::find(1)
                                                                                                 ->first()], $data));

        return $loyalty;
    }

    /** @test */
    public function can_create_new_loyalty_page()
    {
        $response = $this->get('/dashboard/loyalty/create');

        $response->assertStatus(302)
                 ->assertRedirect()
                 ->assertSeeText('/dashboard/loyalty/edit/');

    }

    /** @test */
    public function can_not_create_new_loyalty_page_without_permissions()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $response = $this->get('/dashboard/loyalty/create');

        $response->assertStatus(302)
                 ->assertRedirect()
                 ->assertSeeText('/dashboard');

    }

    /** @test */
    public function can_open_edit_loyalty()
    {
        $loyalty = $this->createLoyalty();

        $response = $this->get('/dashboard/loyalty/edit/' . base64_encode($loyalty->id));

        $response->assertStatus(200)
                 ->assertViewIs('loyalty.view');
    }

    /** @test */
    public function can_not_open_edit_loyalty_without_permissions()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $loyalty = $this->createLoyalty();

        $response = $this->get('/dashboard/loyalty/edit/' . base64_encode($loyalty->id));

        $response->assertStatus(302)
                 ->assertRedirect()
                 ->assertSeeText('/dashboard');
    }

    /** @test */
    public function can_update_loyalty()
    {
        $loyalty = $this->createLoyalty();

        $country = factory(\App\Country::class)->create();
        $data = [
            'name' => uniqid(),
            'country_id' => $country->id,
            'original_reward' => 1,
            'reward_pts' => 3,
            'valid_to' => date('Y-m-d', time() + 100000),
        ];

        $response = $this->post('/dashboard/loyalty/update/' . base64_encode($loyalty->id), $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/loyalty')
                 ->assertSessionMissing('alert_error');

        $this->assertDatabaseHas('loyalty', [
            'id' => $loyalty->id,
            'name' => $data['name'],
        ]);
    }

    /** @test */
    public function can_not_update_loyalty_without_permission()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $loyalty = $this->createLoyalty();

        $data = [
            'name' => uniqid(),
        ];

        $response = $this->post('/dashboard/loyalty/update/' . base64_encode($loyalty->id), $data);

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard');

        $this->assertDatabaseMissing('loyalty', [
            'id' => $loyalty->id,
            'name' => $data['name'],
        ]);
    }

    /** @test */
    public function can_delete_loyalty()
    {
        $loyalty = $this->createLoyalty();

        $response = $this->get('/dashboard/loyalty/delete/' . base64_encode($loyalty->id));

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard/loyalty');

        $this->assertDatabaseMissing('loyalty', [
            'id' => $loyalty->id,
        ]);
    }

    /** @test */
    public function can_not_delete_loyalty_without_permission()
    {
        $this->session(['user_roles' => [], 'user_permissions' => []]);

        $loyalty = $this->createLoyalty();

        $response = $this->get('/dashboard/loyalty/delete/' . base64_encode($loyalty->id));

        $response->assertStatus(302)
                 ->assertRedirect('/dashboard');

        $this->assertDatabaseHas('loyalty', [
            'id' => $loyalty->id,
        ]);
    }

    /** @test */
    public function can_filter_loyalties_by_search_query()
    {
        $loyalty = $this->createLoyalty();

        $response = $this->get('/dashboard/loyalty?q=' . uniqid());

        $response->assertStatus(200)
                 ->assertViewIs('loyalty.list')
                 ->assertDontSee($loyalty->name);

        $response = $this->get('/dashboard/loyalty?q=' . $loyalty->name);

        $response->assertStatus(200)
                 ->assertViewIs('loyalty.list')
                 ->assertSee($loyalty->name);
    }

    /** @test */
    public function can_change_loyalty_ranking_order()
    {
        $loyalty1 = $this->createLoyalty(['priority' => 1]);
        $loyalty2 = $this->createLoyalty(['priority' => 2]);

        $data = [
            'sort' => [
                ['id' => $loyalty1->id, 'p' => 2],
                ['id' => $loyalty2->id, 'p' => 1],
            ],
        ];

        $response = $this->post('/dashboard/loyalty/prioritize', $data);

        $response->assertStatus(200)
                 ->assertJson(["failed" => false]);

        $this->assertDatabaseHas('loyalty', [
            'id' => $loyalty1->id,
            'priority' => 2,
        ]);

        $this->assertDatabaseHas('loyalty', [
            'id' => $loyalty2->id,
            'priority' => 1,
        ]);
    }

    /** @test */
    public function can_add_loyalty_rule()
    {
        $loyalty = $this->createLoyalty();
        $country = factory(\App\Country::class)->create();
        $currency = factory(\App\Currency::class)->create();
        factory(\App\CurrencyPricing::class)->create(['currency_id' => $currency->id]);

        $data = [
            'rule_operator' => '["and","and"]',
            'rule_type' => '["Country","Currency"]',
            'rule_comparison' => '["equal","different"]',
            'rule_value' => '["' . $country->id . '","' . $currency->id . '"]',
            'rule_value_currency' => '',
            'type' => 'Points',
            'currency_id' => $currency->id,
            'original_reward' => '5',
            'reward_pts' => '2',
        ];

        $response = $this->post('/dashboard/loyalty/rule/' . base64_encode($loyalty->id), $data);

        $response->assertStatus(200)
                 ->assertViewIs('loyalty.row')
                 ->assertSee($country->name)
                 ->assertSee($currency->name);

        $this->assertDatabaseHas('loyalty_rule', [
            'loyalty_id' => $loyalty->id,
            'type' => $data['type'],
        ]);

        $rule = \App\LoyaltyRule::where('loyalty_id', $loyalty->id)->first();

        $this->assertDatabaseHas('loyalty_exceptionrule', [
            'loyalty_rule_id' => $rule->id,
            'type' => 'Country',
            'comparison_operator' => 'equal',
            'rule_value' => $country->id,
        ]);

        $this->assertDatabaseHas('loyalty_exceptionrule', [
            'loyalty_rule_id' => $rule->id,
            'type' => 'Currency',
            'comparison_operator' => 'different',
            'rule_value' => $currency->id,
        ]);
    }

    /** @test */
    public function can_remove_loyalty_rule()
    {
        $loyalty = $this->createLoyalty();
        $rule = factory(\App\LoyaltyRule::class)->create(['loyalty_id' => $loyalty->id]);
        $exceptionRule = factory(\App\LoyaltyExceptionrule::class)->create(['loyalty_rule_id' => $rule->id]);

        $response = $this->get('/dashboard/loyalty/unlink/' . base64_encode($rule->id));

        $response->assertStatus(200)
                 ->assertJson(["failed" => false]);

        $this->assertDatabaseMissing('loyalty_rule', [
            'id' => $rule->id,
            'loyalty_id' => $loyalty->id,
            'deleted' => 0,
        ]);

        $this->assertDatabaseMissing('loyalty_exceptionrule', [
            'id' => $exceptionRule->id,
            'loyalty_rule_id' => $rule->id,
            'deleted' => 0,
        ]);
    }
}
