@extends('layouts.dashboard')

@section('title')
    Auto Generated Report | BLU
@stop
<style>
  .ui-autocomplete-loading {
    background:url(/img/loader.gif) no-repeat center right;
  }
  li.ui-menu-item{
  	font-size:12px;
  }
</style>

@section('body')
    <div class="padding">

        <div class="content-head pure-g">
            <div class="title pure-u-1-2">
                    <h1><i class="fa fa-star fa-fw"></i>Auto Generated Reports</h1>
            </div>

            <div class="controls pure-u-1-2">
                <a href="{{ url('autogeneratedreports') }}" class="pure-button">All Reports</a>
            </div>
        </div>

        {{ Form::model($program, array('url'   => 'autogeneratedreports/update/'.base64_encode($program->id),
                                       'class' => 'pure-form pure-form-stacked form-width main-form',
                                       'id'    => 'agrForm')) }}

            <fieldset>
                <legend>
                    Details

                    <span class="right">
                        <a href="#dialog-priority" class="custom-dialog" style="text-decoration:none;"><i class="fa fa-list-ol"></i> Current Rank [<span id="priority-num">{{ $program->priority }}</span>]</a>
                    </span>
                </legend>


                {{ Form::label('name', 'Program Name') }}
                {{ Form::text('name', $report->name, array('class' => 'pure-input-1 required')) }}

                {{ Form::label('url', 'URL') }}
                {{ Form::textarea('url', $report->url, array('class' => 'pure-input-1')) }}


                <select name="status" class="pure-input-1-3">
                    <option value="0" <?php echo ($report->status == 0)? "selected" : ""; ?> >Inactive</option>
                    <option value="1" <?php echo ($report->status == 1)? "selected" : ""; ?>>Active</option>
                </select>
            </fieldset>

            {{--- Form Buttons ---}}
            <div class="form-buttons">
                <div class="left">
                    <a href="{{ url('autogeneratedreports') }}" class="pure-button">Cancel</a>
                </div>
                    @if(I::can('edit_report'))
                        <div class="right">
                            <button type="submit" class="pure-button pure-button-primary">Save Report</button>
                        </div>
                    @endif
                <div class="clearfix"></div>
            </div>
            {{--- /Form Buttons ---}}

        {{ Form::close() }}
    </div>
@stop