@if(count($currencyTranslations) > 0)
    @foreach($currencyTranslations as $currencyTranslation)
        <tr>
            <td>{{ $currencyTranslation->id }}</td>
            <td>{{ $currencyTranslation->name }}</td>
            <?php
                $language = App\Language::find($currencyTranslation->lang_id);
                echo "<td>" . $language->name ."</td>";
            ?>
            <td>
                @if(!I::can('delete_currencies') && !I::can('edit_currencies'))
                    N/A
                @endif
                @if(I::can('edit_currencies'))
                    <a href="{{ url('dashboard/currencytranslation/edit/'.base64_encode($currencyTranslation->id)) }}" class="edit-currencytranslation" title="Edit">Edit</a>
                @endif
                @if(I::can('delete_currencies'))
                    <a href="{{ url('dashboard/currencytranslation/delete/'.base64_encode($currencyTranslation->id)) }}" class="delete-currencytranslation" title="Delete">Delete</a>
                @endif
            </td>
        </tr>
    @endforeach
@else
    <tr class="empty">
        <td colspan="4">No currency translation defined</td>
    </tr>
@endif
