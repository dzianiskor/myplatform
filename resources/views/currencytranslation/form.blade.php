
{{ Form::open(array('url' => url('dashboard/currencytranslation/update/'.base64_encode($currencyTranslation->id)), 'method' => 'post', 'class' => "pure-form pure-form-stacked", 'id' => 'new-currencytranslation-form')) }}
    
    @include('partials.errors')                         

    <div class="pure-g">
        <div class="pure-u-1-3">
            {{ Form::label('currencytranslation_lang', 'Language') }}
            {{ Form::select('currencytranslation_lang',$languages, array('class' => 'pure-input-1-2')) }}
            
            {{ Form::label('currencytranslation_name', 'Name') }}
        </div>
            {{ Form::text('currencytranslation_name', $currencyTranslation->name, array('class' => 'pure-input-1 required')) }}
    </div>

    {{-- Dialog Buttons --}}
    <div class="form-buttons">
        <div class="left">
            <a href="#" class="pure-button close-dialog">Cancel</a>
        </div>
        <div class="right">
            <a href="#" class="pure-button pure-button-primary save-new-currencytranslation">Save</a>
        </div>
        <div class="clearfix"></div>
    </div>
    {{-- /Dialog Buttons --}}

{{ Form::close() }}