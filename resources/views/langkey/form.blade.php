
{{ Form::open(array('url' => url('dashboard/langkey/update/'.base64_encode($langkey->id)), 'method' => 'post', 'class' => "pure-form pure-form-stacked", 'id' => 'new-langkey-form')) }}
    
    @include('partials.errors')                         

    <div class="pure-g">
        <div class="pure-u-2-3">
            {{ Form::label('keys_search', 'Keys Search') }}
                <input id="keys-autocomplete" class="keys-autocomplete" placeholder="Search" />
                <button id="keys-search">search</button>
            {{ Form::label('key', 'Key') }}
            <?php

                echo "<select name='key' id='key' style='width: 500px !important;'> <option value=''>Select Key</option>";
                               
                foreach($lockeys as $key){
                    if($langkey->key_id == $key->id){
                        echo "<option value='$key->id' selected='selected'>$key->key</option>";
                    }
                    else{
                        echo "<option value='$key->id'>$key->key</option>";
                    }
                }
                echo "</select>";
            ?>
            
            
            
            {{ Form::label('value', 'Value') }}
            {{ Form::textarea('value', $langkey->value, array('class' => 'pure-input-1 required')) }}
            
        </div>

        
    </div>

    {{-- Dialog Buttons --}}
    <div class="form-buttons">
        <div class="left">
            <a href="#" class="pure-button close-dialog">Cancel</a>
        </div>
        <div class="right">
            <a href="#" class="pure-button pure-button-primary save-new-langkey">Save</a>
        </div>
        <div class="clearfix"></div>
    </div>
    {{-- /Dialog Buttons --}}

{{ Form::close() }}

<script>
     $(document).bind('keypress', '#keys-autocomplete', function(event) {
                if(event.keyCode == 13) {
                    if($("#keys-autocomplete").val().length>=3) {
                        event.preventDefault();
                        $.get('/dashboard/langkey/keys?term='+$('#keys-autocomplete').val(), function(resp){
                            $('select#key').empty();
                            $.each(resp, function(index, value){
                                jQuery('<option/>', {
                                    value: value.id,
                                    html: value.value
                                    }).appendTo('select#key');//appends to select if parent div has id dropdown
                                });
                        });
                        return false;
                    }else{
                        event.preventDefault();
                        return false;
                    }
                 }
            // Turn on the global items search autocomplete function
                $("#keys-autocomplete").autocomplete({
                  source: "/dashboard/langkey/keys",
                  minLength: 3,
                  select: function( event, ui ) {
                      $('select#key').empty();
                    jQuery('<option/>', {
                        value: ui.item.id,
                        html: ui.item.value
                        }).appendTo('select#key').attr('selected', true); //appends to select if parent div has id dropdown
                  }
                });

        });
        
        $(document).on('click', '#keys-search', function(event) {
            if($("#keys-autocomplete").val().length>=3) {
                event.preventDefault();
                $('select#key').empty();
                $.get('/dashboard/langkey/keys?term='+$('#keys-autocomplete').val(), function(resp){
                    $.each(resp, function(index, value){
                        jQuery('<option/>', {
                            value: value.id,
                            html: value.value
                            }).appendTo('select#key');//appends to select if parent div has id dropdown
                        });
                });
                return false;
            }else{
                event.preventDefault();
                return false;
            }
     });
</script>