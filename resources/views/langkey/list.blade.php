@if(count($langkeys) > 0)
  @foreach($langkeys as $langkey)
    <tr>
        <td>{{ $langkey->id }}</td>
        <td>{{ $langkey->key->key }}</td>
        <td>{{ $langkey->value }}</td>
        
        <td>
           <a href="{{ url('dashboard/langkey/edit/'.base64_encode($langkey->id)) }}" class="edit-langkey" title="Edit">Edit</a>
           <a href="{{ url('dashboard/langkey/delete/'.base64_encode($langkey->id)) }}" class="delete-langkey" title="Delete">Delete</a>
        </td>
    </tr>
  @endforeach
@else
    <tr class="empty">
        <td colspan="4">No langkey locations defined</td>
    </tr>
@endif
