
{{ Form::open(array('url' => url('dashboard/prodmodeltranslation/update/'.$prodmodeltranslation->id), 'method' => 'post', 'class' => "pure-form pure-form-stacked", 'id' => 'new-prodmodeltranslation-form')) }}
    
    @include('partials.errors')                         

    <div class="pure-g">
        <div class="pure-u-1-3">
            {{ Form::label('prodmodeltranslation_lang', 'Language') }}
            {{ Form::select('prodmodeltranslation_lang',$languages, array('class' => 'pure-input-1-2')) }}
            
            {{ Form::label('prodmodeltranslation_name', 'Name') }}
           
            
            
        </div>
 {{ Form::text('prodmodeltranslation_name', $prodmodeltranslation->name, array('class' => 'pure-input-1 required')) }}
    </div>

    {{-- Dialog Buttons --}}
    <div class="form-buttons">
        <div class="left">
            <a href="#" class="pure-button close-dialog">Cancel</a>
        </div>
        <div class="right">
            <a href="#" class="pure-button pure-button-primary save-new-prodmodeltranslation">Save</a>
        </div>
        <div class="clearfix"></div>
    </div>
    {{-- /Dialog Buttons --}}

{{ Form::close() }}