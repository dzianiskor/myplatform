@if(count($prodmodeltranslations) > 0)
  @foreach($prodmodeltranslations as $prodmodeltranslation)
    <tr>
        <td>{{ $prodmodeltranslation->id }}</td>
        <td>{{ $prodmodeltranslation->name }}</td>
        <?php
            $language = App\Language::find($prodmodeltranslation->lang_id);
            echo "<td>" . $language->name ."</td>";
        ?>

        <td>
           <a href="{{ url('dashboard/prodmodeltranslation/edit/'.$prodmodeltranslation->id) }}" class="edit-prodmodeltranslation" title="Edit">Edit</a>
           <a href="{{ url('dashboard/prodmodeltranslation/delete/'.$prodmodeltranslation->id) }}" class="delete-prodmodeltranslation" title="Delete">Delete</a>
        </td>
    </tr>
  @endforeach
@else
    <tr class="empty">
        <td colspan="4">No prodmodeltranslation defined</td>
    </tr>
@endif
