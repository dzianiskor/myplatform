{{-- Channel Listing --}}
<h3>
    @if(I::can_edit('products', $product) && $disabled == false)
	    <a href="#dialog-add-upc" title="New UPC" class="dialog">
	        UPC <i class="fa fa-info-circle fa-fw" title="message will be added later" style="cursor: help;"></i> <i style="float: right; margin-right: 10px ! important; margin-top: 3px;" class="fa fa-plus-circle fa-fw"></i>
	    </a>
    @else
        UPC
    @endif
</h3>
<ul id="upc-listing">
    @include('productpricing/list/upcList', array('product' => $product))
</ul>
{{-- /Country Listing --}}
