{{-- Network Listing --}}
	@if(I::am('BLU Admin'))
	<h3>
	    @if(I::can_edit('products', $product))
	    <a href="#dialog-add-network" title="New Network" class="dialog">
	        Networks <i class="fa fa-info-circle fa-fw" title="message will be added later" style="cursor: help;"></i> <i style="float: right; margin-right: 10px ! important; margin-top: 3px;" class="fa fa-plus-circle fa-fw"></i>
	    </a>
	    @else
	        Networks
	    @endif
	</h3>
	<ul id="network-listing">
	    @include('productpricing/list/networkList', array('product'=>$product))
	</ul>
	<label class="error" style="display: none;">This field is required.</label>
	{{-- /Network Listing --}}
@endif