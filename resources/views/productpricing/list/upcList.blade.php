@if(!empty($error))
<div class="errors" style="padding: 0; margin-bottom: 0;">
    <ul>
        <li>{{ $error}}</li>
    </ul>
</div>
@endif
@if($product->upcs()->count() > 0)
@foreach($product->upcs as $u)
<li>
    {{ $u->upc }}
    <?php
        if (I::can('edit_products')) {
            echo '<a href="/dashboard/productpricing/edit_upc/' . base64_encode($u->id) . '" class="edit-upc" data-id="{{ $u->id }}"  style="right:70px;">
                [Edit]
            </a>';
        }
        if (I::can('delete_products')) {
            echo '<a href="/dashboard/productpricing/unlink_upc/' . base64_encode($u->product_id) . '/' . base64_encode($u->id) . '" class="delete-ajax" data-target="#upc-listing">
                [Delete]
            </a>';
        }
    ?>

</li>
@endforeach
@else
<li class="empty">No UPCs Associated</li>
@endif
