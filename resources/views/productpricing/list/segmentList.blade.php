@if(!empty($error))
<div class="errors" style="padding: 0; margin-bottom: 0;">
    <ul>
        <li>{{ $error}}</li>
    </ul>
</div>
@endif
@if(count($productRedemption->segments) > 0)
    @foreach($productRedemption->segments as $s)
        <li>
            {{ $s->name }}
            @if(I::can_edit('products', $product))
                @if (Auth::User()->managedPartners()->contains($s->partner_id) || I::belong(1))
                    <a href="/dashboard/productpricing/unlink_segment/{{ base64_encode($productRedemption->id) }}/{{ base64_encode($s->id) }}" class="delete-ajax" data-id="{{ $s->id }}" data-target="#segment-listing">
                        [Delete]
                    </a>
                @endif
            @endif
        </li>
    @endforeach
@else
    <li class="empty">No Segments Associated</li>
@endif
