@if(count($product->networks) > 0)
    @foreach($product->networks as $n)
        <li>
            {{ $n->name }}
            @if(I::can_edit('products', $product))
                <a href="/dashboard/productpricing/unlink_network/{{ base64_encode($product->id) }}/{{ base64_encode($n->id) }}" class="delete-ajax" data-id="{{ $n->id }}" data-target="#network-listing">
                    [Delete]
                </a>
            @endif
        </li>
    @endforeach
@else
    <li class="empty">No Networks Associated</li>
@endif
