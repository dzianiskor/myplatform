{{-- Partner Listing --}}
<h3>
    @if(I::can_edit('products', $product) && $disabled == false)
    <a href="#dialog-add-partner" title="New Partner" class="dialog">
        Partners <i class="fa fa-info-circle fa-fw" title="message will be added later" style="cursor: help;"></i> <i style="float: right; margin-right: 10px ! important; margin-top: 3px;" class="fa fa-plus-circle fa-fw"></i>
    </a>
    @else
        Partners
    @endif
</h3>
<ul id="partner-listing">
    @include('productpricing/list/partnerList', array('product'=>$product))
</ul>
<label class="error" style="display: none;">This field is required.</label>
{{-- /Partner Listing --}}
