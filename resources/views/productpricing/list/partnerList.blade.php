@if(!empty($error))
<div class="errors" style="padding: 0; margin-bottom: 0;">
    <ul>
        <li>{{ $error}}</li>
    </ul>
</div>
@endif
<?php $count = 0; ?>
@if(count($product->partners) > 0)
    @foreach($product->partners as $p)
        @if(Auth::User()->managedPartners()->contains($p) || I::belong(1))
            <li>
                {{ $p->name }}
                @if(I::can('delete_products') && $p->id != 1)
                    <a href="/dashboard/productpricing/unlink_partner/{{ base64_encode($product->id) }}/{{ base64_encode($p->id) }}" class="delete-ajax" data-id="{{ $p->id }}" data-target="#partner-listing">
                        [Delete]
                    </a>
                @endif
            </li>
            <?php $count++; ?>
        @endif
    @endforeach
    @if($count == 0)
        <li class="empty">You can't see this list</li>
    @endif
@else
    <li class="empty">No Partner Associated</li>
@endif
