{{-- Partner Listing --}}
<h3>
    @if(I::can_edit('products', $product))
    <a href="#dialog-add-redemption-partner" title="New Redemption Partner" class="dialog">
       Redemption Partners <i class="fa fa-info-circle fa-fw" title="message will be added later" style="cursor: help;"></i> <i style="float: right; margin-right: 10px ! important; margin-top: 3px;" class="fa fa-plus-circle fa-fw"></i>
    </a>
    @else
        Redemption Partners
    @endif
</h3>
<ul id="redemption-partners-listing">
    @include('productpricing/list/redemptionPartnersList', array('product'=>$product))
</ul>
<label class="error" style="display: none;">This field is required.</label>
{{-- /Partner Listing --}}
