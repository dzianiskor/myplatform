{{-- Country Listing --}}
<h3>
    @if(I::can_edit('products', $product))
    <a href="#dialog-add-country" title="New Country" class="dialog">
        Countries <i class="fa fa-info-circle fa-fw" title="message will be added later" style="cursor: help;"></i> <i style="float: right; margin-right: 10px ! important; margin-top: 3px;" class="fa fa-plus-circle fa-fw"></i>
    </a>
    @else
        Countries
    @endif
</h3>
<ul id="country-listing">
    @include('productpricing/list/countryList', array('productRedemption'=>$productRedemption))
</ul>
{{-- /Country Listing --}}
