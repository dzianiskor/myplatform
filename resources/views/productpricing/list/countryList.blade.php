@if(!empty($error))
<div class="errors" style="padding: 0; margin-bottom: 0;">
    <ul>
        <li>{{ $error}}</li>
    </ul>
</div>
@endif
@if(count($productRedemption->countries) > 0)
    @foreach($productRedemption->countries as $c)
        <li>
            {{ App\Country::find($c->country_id)->name }}
            @if(I::can_edit('products', $product))
            <a href="/dashboard/productpricing/edit_country_details/{{ base64_encode($c->id) }}" class="edit-country" data-id="{{ $c->id }}" style="right:70px;">
                        [Edit]
                    </a>
            <a href="/dashboard/productpricing/unlink_country/{{ base64_encode($productRedemption->id) }}/{{ base64_encode($c->id) }}" class="delete-ajax" data-id="{{ $c->id }}" data-target="#country-listing">
                [Delete]
            </a>
            @endif
        </li>
    @endforeach
@else
    <li class="empty">No Country Associated</li>
@endif
