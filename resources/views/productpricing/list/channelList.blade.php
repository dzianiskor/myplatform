@if(!empty($error))
<div class="errors" style="padding: 0; margin-bottom: 0;">
    <ul>
        <li>{{ $error}}</li>
    </ul>
</div>
@endif
@if($productRedemption->channels()->count() > 0)
    @foreach($productRedemption->channels as $c)
        <li>
            {{ App\Partner::find($c->partner_id)->name }}
        <?php
            $chPart = App\PartnerChannel::where('channel_id',$c->channel_id)->get();
            foreach($chPart as $chP){
                if(I::can_edit('products', $product)){

                        echo '<a href="/dashboard/productpricing/unlink_channel/'.base64_encode($productRedemption->id) . '/'.base64_encode($c->partner_id). '" class="delete-ajax" data-id="'.$c->partner_id. '" data-product_redemption_id="'.base64_encode($productRedemption->id) .'" data-target="#channel-listing">
                            [Delete]
                        </a>';
                        break;
                }
            }
        ?>
            
        </li>
    @endforeach
@else
    <li class="empty">No Channels Associated</li>
@endif
