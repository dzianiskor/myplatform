@if(!empty($error))
<div class="errors" style="padding: 0; margin-bottom: 0;">
    <ul>
        <li>{{ $error}}</li>
    </ul>
</div>
@endif
@if($product->ProductPartnerReward()->count() > 0)
<?php $count = 0; ?>
    @foreach($product->ProductPartnerReward as $p)
    <?php $partner = App\Partner::find($p->partner_id);  ?>
        @if(I::belong(1) || Auth::User()->managedPartners()->contains($partner))
        <li>
            {{ $partner->name }}
            @if(I::can('edit_products'))
                <a href="/dashboard/productpricing/edit_reward_partner/{{ base64_encode($p->id) }}" class="edit-reward_partner" data-id="{{ $p->id }}" style="right:70px;">
                    [Edit]
                </a>
            @endif
            @if(I::can('delete_products'))
                <a href="/dashboard/productpricing/unlink_reward_partner/{{ base64_encode($p->product_id) }}/{{ base64_encode($p->id) }}" class="delete-ajax" data-id="{{ $p->id }}" data-target="#reward-partners-listing">
                    [Delete]
                </a>
            @endif
        </li>
          <?php $count++; ?>
        @endif
    @endforeach
    @if($count == 0)
        <li class="empty">You can't see this list</li>
    @endif
@else
    <li class="empty">No Reward Partner Associated</li>
@endif
