{{-- Channel Listing --}}
<h3>
    @if(I::can_edit('products', $product))
	    <a href="#dialog-add-channel" title="New Channel" class="dialog">
	        Display Channels <i class="fa fa-info-circle fa-fw" title="message will be added later" style="cursor: help;"></i> <i style="float: right; margin-right: 10px ! important; margin-top: 3px;" class="fa fa-plus-circle fa-fw"></i>
	    </a>
    @else
        Display Channels
    @endif
</h3>
<ul id="channel-listing">
    @include('productpricing/list/channelList', array('productRedemption' => $productRedemption))
</ul>
{{-- /Country Listing --}}
