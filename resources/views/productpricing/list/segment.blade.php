{{-- Segment Listing --}}
<h3>
    @if(I::can_edit('products', $product))
    <a href="#dialog-add-segment" title="New Segment" class="dialog">
        Segments <i class="fa fa-info-circle fa-fw" title="message will be added later" style="cursor: help;"></i> <i style="float: right; margin-right: 10px ! important; margin-top: 3px;" class="fa fa-plus-circle fa-fw"></i>
    </a>
    @else
        Segments
    @endif
</h3>
<ul id="segment-listing">
    @include('productpricing/list/segmentList', array('productRedemption'=>$productRedemption))
</ul>
{{-- /Segment Listing --}}
