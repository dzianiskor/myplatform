<div class="pure-u-1-2">
    <fieldset>
        <legend>Basic Details</legend>

        {{ Form::label('status', 'Status') }}
        <select name="status" class="pure-input-1-3">
            @foreach(Meta::productStatuses() as $k => $v)
                @if($k == $product->status)
                    <option value="{{ $k }}" selected="">{{ ucfirst($v) }}</option>
                @else
                    <option value="{{ $k }}">{{ ucfirst($v) }}</option>
                @endif
            @endforeach
        </select>

        {{ Form::label('ucid', 'Unique Product ID (Auto-generated)') }}
        {{ Form::text('ucid', $product->ucid, array('class' => 'pure-input-1', 'maxlength' => 9, 'disabled' => 'disabled')) }}
        {{ Form::label('name', 'Name', array('for'=>'name')) }}
        {{ Form::text('name', $product->name, array('class'=>'pure-input-1 required', $disabled)) }}

        <div class="pure-g">
            <div class="pure-u-1-2">
                <div class="padding-right-10">
                    {{ Form::label('brand_id', 'Brand', array('for'=>'brand_id')) }}
                    {{ Form::select('brand_id', $brands, $product->brand_id, array('id' => 'brand-list', 'class' => 'pure-input-1 required', $disabled)) }}

                    @if(I::can('create_brands'))
                        <a href="#dialog-new-brand" class="btn-new-brand top-space" title="Add new brand">
                            <i class="fa fa-plus-circle fa-fw"></i>Add new brand
                        </a>
                    @endif
                </div>
            </div>

            <div class="pure-u-1-2">
                {{ Form::label('category_id', 'Category', array('for'=>'category_id')) }}
                {{ Form::select('category_id', $filter_categories, $product->category_id, array('id' => 'category-list', 'class' => 'pure-input-1 required', $disabled)) }}

                @if(I::can('create_categories'))
                    <a href="#dialog-new-category" class="btn-new-category top-space" title="Add new category">
                        <i class="fa fa-plus-circle fa-fw"></i>Add new category
                    </a>
                @endif
            </div>

            <div class="pure-u-1-2">
                <div class="padding-right-10">
                    {{ Form::label('model', 'Model', array('for'=>'model')) }}
                    {{ Form::text('model', $product->model, array('class'=>'pure-input-1', $disabled)) }}
                </div>
            </div>

            <div class="pure-u-1-2">
                {{ Form::label('sub_model', 'Sub Model', array('for'=>'sub_model')) }}
                {{ Form::text('sub_model', $product->sub_model, array('class'=>'pure-input-1', $disabled)) }}
            </div>
        </div>

        {{ Form::label('description', 'Description', array('for'=>'description')) }}
        {{ Form::textarea('description', $product->description, array('class'=>'pure-input-1', $disabled)) }}

    </fieldset>

	<fieldset>
		<legend>Cover Image</legend>

		<table style="margin-top:20px" class="file-upload">
            <tr>
                <td width="80">
					<?php if(empty($product->cover_image)): ?>
                    	<img src="https://placehold.it/74x74" alt="" />
					<?php else: ?>
						<img src="<?php echo App\Http\Controllers\MediaController::getImageFromId($product->cover_image) ?>?s=74x74" alt="" />
					<?php endif; ?>
                </td>
                <td>
                    {{ Form::label('cover_image', 'Product Cover Image') }}
                    {{ Form::file('cover_image') }}
                </td>
            </tr>
		</table>

	</fieldset>

        
	<fieldset>
		<legend>Product Images</legend>

        <table style="margin-top:20px" class="file-upload">
            @foreach($product->media as $media)
            <tr>
                <td width="80">
                    <img src="<?php echo App\Http\Controllers\MediaController::getImageFromId($media->id) ?>?s=74x74" alt="" />
                </td>
                <td>
                    <a href="#" class="delete-image" data-id="{{ $media->id }}">
						<i class="fa fa-trash-o"></i> Remove image
					</a>
                </td>
            </tr>
            @endforeach
            <tr>
                <td width="80">
                        <img src="https://placehold.it/74x74" alt="" />
                </td>
                <td>
                    {{ Form::label('image', 'Product Image') }}
                    {{ Form::file('image') }}
                </td>
            </tr>
        </table>

        <a href="#dialog-new-file" class="btn-new-file top-space" title="Add another image">
            <i class="fa fa-plus-circle fa-fw"></i> Add another image
        </a>
	</fieldset>
        <fieldset>
		<legend>Image Hosted Elsewhere </legend>

		<table style="margin-top:20px" class="file-upload">
            <tr>
                <td width="80">
					<?php if(empty($product->productimage)): ?>
                    	<img src="https://placehold.it/74x74" alt="" />
					<?php else: ?>
						<img src="{{ $product->productimage }}" width="74px" alt="" />
					<?php endif; ?>
                </td>
                <td>
                    {{ Form::label('productimage', 'Product Image URL') }}
        {{ Form::text('productimage', $product->productimage, array('class' => 'pure-input-1', $disabled)) }}
                </td>
            </tr>
		</table>

	</fieldset>

    <fieldset>
        <legend>Logistics</legend>
        
        {{ Markup::checkbox('is_voucher', 'Is a Voucher?', ($product->is_voucher == 1)? true : false, 1, $disabled) }}
        <div class="pure-g">
            <div class="pure-u-1-2">
                <div class="padding-right-10">
                    {{ Form::label('weight', 'Weight (kg)', array('for'=>'weight')) }}
                    {{ Form::text('weight', $product->weight, array('class'=>'pure-input-1', $disabled)) }}
                </div>
            </div>

            <div class="pure-u-1-2">
                {{ Form::label('volume', 'Volume (cm3)', array('for'=>'volume')) }}
                {{ Form::text('volume', $product->volume, array('class'=>'pure-input-1', $disabled)) }}
            </div>
        </div>
    </fieldset>
</div>