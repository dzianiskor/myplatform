<div class="pure-u-1-2">
    <fieldset>
        <legend>Basic Details</legend>
        
        {{ Form::label('ucid', 'Unique Product ID (Auto-generated)') }}
        {{ Form::text('ucid', $product->ucid, array('class' => 'pure-input-1', 'maxlength' => 9, 'disabled' => 'disabled')) }}
        {{ Form::label('product_id', 'Product ID') }}
        {{ Form::text('reference', $product->id, array('class' => 'pure-input-1' , 'disabled' => 'disabled')) }}
        {{ Form::label('name', 'Name', array('for'=>'name')) }}
        {{ Form::text('name', $product->name, array('class'=>'pure-input-1' , 'disabled' => 'disabled')) }}


        <div class="pure-g">
            <div class="pure-u-1-2">
                <div class="padding-right-10">
                    {{ Form::label('supplier_id', 'Supplier', array('for'=>'supplier_id')) }}
                    <?php
//                    array_unshift($suppliers, 'No Supplier Selected');
                    $suppliers[0] = 'No Supplier Selected';
                    if (empty($productRedemption->supplier_id)) {
                        $productSupplierId = 0;
                    } else {
                        $productSupplierId = $productRedemption->supplier_id;
                    }
                    ?>
                    {{ Form::select('supplier_id', $suppliers, $productSupplierId, array('id' => 'supplier-list', 'class' => 'pure-input-1 required')) }}
                </div>
            </div>
        </div>
        {{ Form::label('product_link', 'Product Link', array('for'=>'product_link')) }}
        {{ Form::text('product_link', $productRedemption->product_link, array('class'=>'pure-input-1')) }}
    </fieldset>
    <fieldset>
        <legend>Values</legend>

        {{ Markup::checkbox('display_online', 'Display Online?', ($productRedemption->display_online == 1)? true : false, 1) }}

        {{ Markup::checkbox('hot_deal', 'Display as "Hot Deal"?', ($productRedemption->hot_deal == 1)? true : false, 1) }}
        {{ Markup::checkbox('show_offline', 'Display for Logged out?', ($productRedemption->show_offline == 1)? true : false, 1) }}
        <div class="pure-g">
            <div class="pure-u-1-2">
                <div class="padding-right-10">
                    {{ Form::label('start_date', 'Start Date:') }}
                    @if ($productRedemption->start_date == null)
                    {{ Form::text('start_date', '', array('class' => 'pure-input-1 datepicker')) }}
                    @else
                    {{ Form::text('start_date', date("m/d/Y", strtotime($productRedemption->start_date)), array('class' => 'pure-input-1 datepicker')) }}
                    @endif
                </div>
            </div>

            <div class="pure-u-1-2">
                {{ Form::label('end_date', 'End Date:') }}
                @if ($productRedemption->end_date == null)
                {{ Form::text('end_date', '', array('class' => 'pure-input-1 datepicker')) }}
                @else
                {{ Form::text('end_date', date("m/d/Y", strtotime($productRedemption->end_date)), array('class' => 'pure-input-1 datepicker')) }}
                @endif
            </div>
        </div>

        <div class="pure-g">
            <div class="pure-u-1-2">
                <div class="padding-right-10">
                    {{ Form::label('qty', 'Qty in stock', array('for'=>'qty')) }}
                    {{ Form::text('qty', $productRedemption->qty, array('class'=>'pure-input-1')) }}
                </div>
            </div>

            <div class="pure-u-1-2">
                {{ Form::label('price_in_points', 'Price in points', array('for'=>'price_in_points')) }}
                {{ Form::text('price_in_points', $productRedemption->price_in_points, array('class'=>'pure-input-1','disabled' => 'disabled')) }}
            </div>
            <div class="pure-u-1-2">
                <div class="padding-right-10">
                    {{ Form::label('price_in_usd', 'Price in USD', array('for'=>'price_in_usd')) }}
                    {{ Form::text('price_in_usd', $productRedemption->price_in_usd, array('class'=>'pure-input-1', 'disabled' => 'disabled')) }}
                </div>
            </div>
            <div class="pure-u-1-2">
                {{ Form::label('retail_price_in_usd', 'Retail Price in USD', array('for'=>'retail_price_in_usd')) }}
                {{ Form::text('retail_price_in_usd', $productRedemption->retail_price_in_usd, array('class'=>'pure-input-1', 'disabled' => 'disabled')) }}
            </div>
            <div class="pure-u-1-2">
                {{ Form::label('sales_tax_in_usd', 'Tax in USD', array('for'=>'sales_tax_in_usd')) }}
                {{ Form::text('sales_tax_in_usd', $productRedemption->sales_tax_in_usd, array('class'=>'pure-input-1', 'disabled' => 'disabled')) }}
            </div>

            <div class="pure-u-1-2">
            </div>
            <div class="pure-u-1-2">
                <div class="padding-right-10">
                    {{ Form::label('original_price', 'Original Price', array('for'=>'original_price')) }}
                    {{ Form::text('original_price', $productRedemption->original_price, array('class'=>'pure-input-1')) }}
                </div>
            </div>
            <div class="pure-u-1-2">
                <div class="padding-right-10">
                    {{ Form::label('original_retail_price', 'Original Retail Price', array('for'=>'original_retail_price')) }}
                    {{ Form::text('original_retail_price', $productRedemption->original_retail_price, array('class'=>'pure-input-1')) }}
                </div>
            </div>
            <div class="pure-u-1-2">
                <div class="padding-right-10">
                    <?php
                    if (empty($productRedemption->currency_id)) {
                        $productCurrencyId = 6;
                    } else {
                        $productCurrencyId = $productRedemption->currency_id;
                    }
                    ?>
                    {{ Form::label('currency', 'Currency', array('for'=>'currency')) }}
                    {{ Form::select('currency_id', $currencies, $productCurrencyId, array('id' => 'currency_id', 'class' => 'pure-input-1')) }}
                </div>
            </div>
            
            <div class="pure-u-1-2">
                <div class="padding-right-10">
                    {{ Form::label('redemption_limit', 'Redemption Limit', array('for'=>'redemption_limit')) }}
                    {{ Form::text('redemption_limit', $productRedemption->redemption_limit, array('class'=>'pure-input-1')) }}
        </div>
            </div>
        </div>
    </fieldset>
</div>