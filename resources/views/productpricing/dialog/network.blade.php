{{-- Assign new Network --}}
<div id="dialog-add-network" style="width:300px;">
    <h4>Associate to Network</h4>

    {{ Form::open(array('class' => "pure-form pure-form-stacked")) }}
        {{ Form::select('network_id', $networks, null, array('class' => 'pure-input-1')) }}

        <a href="/dashboard/productpricing/link_network/{{ base64_encode($product->id) }}" class="pure-button pure-button-primary attribute-action" data-target="#network-listing">
            Save
        </a>
    {{ Form::close() }}
</div>
{{-- /Assign new Network --}}
