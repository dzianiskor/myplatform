{{-- Assign new Partner --}}
<div id="dialog-add-reward-partners" style="width:300px;">
    <h4>Parameters for Reward Products</h4>

    {{ Form::open(array('class' => "pure-form pure-form-stacked")) }}
        {{ Form::label('Partners_id', 'Partner') }}
        {{ Form::select('partner_id', $partners, null, array('class' => 'pure-input-1')) }}
        
        <?php //find the prefix
            $prefix = '';
            $mappingId = $product->partner_mapping_id;
            foreach ($prefixes as $value) {
                if(!empty($value)){
//                    if(strpos($product->partner_mapping_id, $value ) !== false){
                    if(substr($product->partner_mapping_id, 0, strlen($value)) === $value){
                        $prefix = $value;
                        $mappingId   = str_replace($value, '', $product->partner_mapping_id);
                        break;
                    }
                }
            }
        ?>
        <div class="pure-g">
            <div class="pure-u-1-2">
                <div class="padding-right-10">
                    {{ Form::label('partner_prefix', 'Partner Prefix') }}
                    <select name="partner_prefix" class="pure-input-1">
                        <option value="" selected="">Not Selected</option>
                        @foreach($prefixes as $k => $v)
                            <option value="{{ $k }}">{{ $v }}</option>
                        @endforeach
                    </select> 
               </div>
            </div>
            <div class="pure-u-1-2">
                {{ Form::label('partner_mapping_id', 'Partner Map ID', array('for'=>'partner_mapping_id')) }}
                {{ Form::text('partner_mapping_id', '', array('class'=>'pure-input-1')) }}
            </div>
        </div>
        
        {{ Form::label('sku', 'SKU') }}
        {{ Form::text('sku', '', array('class'=>'pure-input-1 required')) }}
        
        <a href="/dashboard/productpricing/link_reward_partner/{{ base64_encode($product->id) }}" class="pure-button pure-button-primary attribute-action" data-target="#reward-partners-listing">
            Save
        </a>
    {{ Form::close() }}
</div>
{{-- /Assign new Partner --}}