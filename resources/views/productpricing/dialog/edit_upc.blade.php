{{-- Assign new Partner --}}

<div id="dialog-add-upc" style="width:300px;">
    <h4>Parameters for Product UPC</h4>

    {{ Form::open(array('class' => "pure-form pure-form-stacked")) }}
        {{ Form::label('upc', 'UPC') }}
        {{ Form::text('upc', $productUpc->upc, array('class'=>'pure-input-1 required')) }}
        
        <a href="/dashboard/productpricing/update_upc/{{ base64_encode($productUpc->id) }}/{{ base64_encode($productUpc->product_id) }}" class="pure-button pure-button-primary attribute-action" data-target="#upc-listing">
            Save
        </a>
    {{ Form::close() }}
</div>
{{-- /Assign new Partner --}}