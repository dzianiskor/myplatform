{{-- Edit Country --}}
<div id="dialog-edit-country" style="width:300px;">
    <h4>Associate to Country</h4>

    {{ Form::open(array('class' => "pure-form pure-form-stacked")) }}
        {{ Form::label('status', 'Status') }}
        <select name="status" class="pure-input-1-3">
            @foreach(Meta::productStatuses() as $k => $v)
                @if($v == $productCountryDetails->status)
                    <option value="{{ $k }}" selected="selected">{{ ucfirst($v) }}</option>
                @else
                    <option value="{{ $k }}">{{ ucfirst($v) }}</option>
                @endif
            @endforeach
        </select>
        {{ Form::label('country_id', 'Country') }}
        {{ Form::select('country_id', $countries, $productCountryDetails->country_id, array('class' => 'pure-input-1')) }}
        {{ Form::label('custom_tarrif_taxes', 'Custom Tarrif & Other Taxes') }}
        {{ Form::text('custom_tarrif_taxes', $productCountryDetails->custom_tarrif_taxes, array('class'=>'pure-input-1')) }}
        {{ Form::label('delivery_charges', 'Delivery Charges') }}
        {{ Form::text('delivery_charges', $productCountryDetails->delivery_charges, array('class'=>'pure-input-1')) }}
        {{ Form::label('currency', 'Currency', array('for'=>'currency')) }}
        {{ Form::select('currency_id', $currencies, $productCountryDetails->currency_id, array('id' => 'currency_id', 'class' => 'pure-input-1')) }}
        
        <a href="/dashboard/productpricing/update_country/{{ base64_encode($productCountryDetails->id) }}/{{ base64_encode($productCountryDetails->product_redemption_id) }}" class="pure-button pure-button-primary attribute-action" data-target="#country-listing">
            Save
        </a>
    {{ Form::close() }}
</div>
{{-- /Assign new Country --}}