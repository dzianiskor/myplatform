{{-- Assign new Partner --}}
<div id="dialog-add-redemption-partner" style="width:300px;">
    <h4>Associate to Partner</h4>

    {{ Form::open(array('class' => "pure-form pure-form-stacked")) }}
        {{ Form::select('partner_id', $partners, null, array('class' => 'pure-input-1')) }}

        <a href="/dashboard/productpricing/link_redemption_partner/{{ base64_encode($product->id) }}" class="pure-button pure-button-primary attribute-action" data-target="#redemption-partners-listing">
            Save
        </a>
    {{ Form::close() }}
</div>
{{-- /Assign new Partner --}}