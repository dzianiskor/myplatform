{{-- Assign new Partner --}}

<div id="dialog-edit-reward-partners" style="width:300px;">
    <h4>Parameters for Reward Products</h4>

    {{ Form::open(array('class' => "pure-form pure-form-stacked")) }}
        {{ Form::label('Partners_id', 'Partner') }}
        {{ Form::select('partner_id', $partners, $productPartnerReward->partner_id, array('class' => 'pure-input-1')) }}
        
        <?php //find the prefix
            $prefix = '';
            $mappingId = $productPartnerReward->partner_mapping_id;
            foreach ($prefixes as $value) {
                if(!empty($value)){
//                    if(strpos($productPartnerReward->partner_mapping_id, $value ) !== false){
                    if(substr($productPartnerReward->partner_mapping_id, 0, strlen($value)) === $value){
                        $prefix = $value;
                        $mappingId   = str_replace($value, '', $productPartnerReward->partner_mapping_id);
                        break;
                    }
                }
            }
        ?>
        <div class="pure-g">
            <div class="pure-u-1-2">
                <div class="padding-right-10">
                    {{ Form::label('partner_prefix', 'Partner Prefix') }}
                    <select name="partner_prefix" class="pure-input-1">
                        <option value="" selected="">Not Selected</option>
                        @foreach($prefixes as $k => $v)
                            @if($k == $prefix)
                                <option value="{{ $k }}" selected="">{{ $v }}</option>
                            @else
                                <option value="{{ $k }}">{{ $v }}</option>
                            @endif
                        @endforeach
                    </select> 
               </div>
            </div>
            <div class="pure-u-1-2">
                {{ Form::label('partner_mapping_id', 'Partner Map ID', array('for'=>'partner_mapping_id')) }}
                {{ Form::text('partner_mapping_id', $mappingId, array('class'=>'pure-input-1')) }}
            </div>
        </div>
        
        {{ Form::label('sku', 'SKU') }}
        {{ Form::text('sku', $productPartnerReward->sku, array('class'=>'pure-input-1 required')) }}
        
        <a href="/dashboard/productpricing/update_reward_partner/{{ base64_encode($productPartnerReward->id) }}/{{ base64_encode($productPartnerReward->product_id) }}" class="pure-button pure-button-primary attribute-action" data-target="#reward-partners-listing">
            Save
        </a>
    {{ Form::close() }}
</div>
{{-- /Assign new Partner --}}