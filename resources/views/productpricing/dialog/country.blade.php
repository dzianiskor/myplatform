{{-- Assign new Country --}}
<div id="dialog-add-country" style="width:300px;">
    <h4>Associate to Country</h4>

    {{ Form::open(array('class' => "pure-form pure-form-stacked")) }}
        {{ Form::label('status', 'Status') }}
        <select name="status" class="pure-input-1-3">
            @foreach(Meta::productStatuses() as $k => $v)
                <option value="{{ $k }}">{{ ucfirst($v) }}</option>
            @endforeach
        </select>
        {{ Form::label('country_id', 'Country') }}
        {{ Form::select('country_id', $countries, 9, array('class' => 'pure-input-1')) }}
        {{ Form::label('custom_tarrif_taxes', 'Custom Tarrif & Other Taxes') }}
        {{ Form::text('custom_tarrif_taxes', '', array('class'=>'pure-input-1')) }}
        {{ Form::label('delivery_charges', 'Delivery Charges') }}
        {{ Form::text('delivery_charges', '', array('class'=>'pure-input-1')) }}
        {{ Form::label('currency', 'Currency', array('for'=>'currency')) }}
        {{ Form::select('currency_id', $currencies, '', array('id' => 'currency_id', 'class' => 'pure-input-1')) }}
        
        <a href="/dashboard/productpricing/link_country/{{ base64_encode($productRedemption->id) }}" class="pure-button pure-button-primary attribute-action" data-target="#country-listing">
            Save
        </a>
    {{ Form::close() }}
</div>
{{-- /Assign new Country --}}