{{-- Assign new Partner Channel--}}
<div id="dialog-add-channel" style="width:300px;">
    <h4>Add Display Channel</h4>

    {{ Form::open(array('class' => "pure-form pure-form-stacked")) }}
        {{ Form::select('partner_id', $partners, null, array('class' => 'pure-input-1')) }}

        <a href="/dashboard/productpricing/link_channel/{{ base64_encode($productRedemption->id) }}/{{ base64_encode($productRedemption->product_id) }}" class="pure-button pure-button-primary attribute-action" data-target="#channel-listing" data-product_redemption_id="{{ base64_encode($productRedemption->id) }}">
            Save
        </a>
    {{ Form::close() }}
</div>
{{-- /Assign new Partner Channel --}}