@extends('layouts.dashboard')

@section('title')
    Update Product | BLU
@stop

@section('dialogs')
@include('productpricing/dialog/channel', array('partners'  => $partners))
@include('productpricing/dialog/segment', array('segments'  => $segments))
@include('productpricing/dialog/country', array('countries' => $countries, 'currencies' => $currencies, 'delivery_options' => $delivery_options))
    
@stop

@section('body')
<div class="padding">

    <div class="content-head pure-g">
        <div class="title pure-u-1-2">
            <h1><i class="fa fa-shopping-cart fa-fw"></i> Product &raquo; {{ $productRedemption->product_id }} &raquo; Redemption Partner &raquo; {{ App\Partner::find($productRedemption->partner_id)->name }} </h1>
        </div>

        <div class="controls pure-u-1-2">
            <a href="{{ url('dashboard/productpricing/view/'.base64_encode($productRedemption->product_id)) }}" class="pure-button">Basic Details</a>
        </div>
    </div>

    {{ Form::model($product, array('action' => array('ProductPricingController@saveProductRedemption', base64_encode($productRedemption->id)),
                                   'method' => 'post',
                                   'files'  => 'true',
                                   'class'  => 'pure-form pure-form-stacked form-width',
                                   'id'     => 'productForm')) }}

        @include('partials.errors')         
                        
        <div class="pure-g">
            @include('productpricing/form_redemption_partner', array('productRedemption' => $productRedemption, 'product' => $product))
            <div class="pure-u-1-2 attributes">
                <div class="padding-left-40">
                    @include('productpricing/list/channel', array('productRedemption' => $productRedemption))
                    @include('productpricing/list/segment', array('productRedemption' => $productRedemption))
                    @include('productpricing/list/country', array('productRedemption' => $productRedemption))
                </div>
            </div>
        </div>
        <!-- Form Buttons -->
        <div class="form-buttons">
            <div class="left">
                <a href="{{ url('dashboard/catalogue') }}" class="pure-button">Cancel</a>
            </div>

            @if($product->draft)
                @if(I::can('create_products'))
                    <div class="right">
                        <button type="submit" class="pure-button pure-button-primary">Create Product</button>
                    </div>
                @endif
            @else
                @if(I::can('edit_products'))
                    <div class="right">
                        <button type="submit" class="pure-button pure-button-primary">Save Product</button>
                    </div>
                @endif
            @endif
            
            <div class="clearfix"></div>
        </div>
        <!-- /Form Buttons -->
        
    {{ Form::close() }}

</div>
@stop

@section('page_script')
    <script>
        $(function(){
            @if(!I::can('create_products') &&  !I::can('edit_products'))
                disableForm('#productForm');
            @endif

            set_menu('mnu-new-catalogue');
        });

        enqueue_script('new-product');
    </script>
@stop
