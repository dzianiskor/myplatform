<?php
namespace App;
?>
@extends('layouts.dashboard')
<?php
use I;
use User;
use Illuminate\Support\Facades\Auth;
use Form;
use ReportHelper;
use Support;
try{
?>
@section('title')
    BLU | Admin Dashboard
@stop

@section('body')
    <section class="dashboard-top">
        <div class="pure-g">
            <div class="pure-u-2-3">
				<?php if(I::can('view_pts_rewarded_redeemed') || I::can('view_member_accounts') || I::can('view_member_accounts')) { ?>
                <h3>30-Day System Overview</h3>
				<?php } if(I::can('view_pts_rewarded_redeemed')) { ?>
                <div class="stat-group">
                    <h5>{{ $sumPointsRewarded }}</h5>
                    <span>Points Rewarded</span>
                </div>

                <div class="stat-group">
                    <h5>{{ $sumPointsRedeemed }}</h5>
                    <span>Points Redeemed</span>
                </div>
				<?php }
					if(I::can('view_member_accounts')) {
				?>
                <div class="stat-group">
                    <h5>{{ number_format($accounts) }}</h5>
                    <span>Member Accounts</span>
                </div>
				<?php }
					if(I::can('view_notifications_db')) {
				?>
                <div class="stat-group">
                    <h5>{{ $sms_notifications }}</h5>
                    <span>SMS Notifications</span>
                </div>

                <div class="stat-group">
                    <h5>{{ $email_notifications }}</h5>
                    <span>Email Notifications</span>
                </div>
				<?php }
					if(I::can('view_pts_rewarded_redeemed')) {
				?>
                <div class="stat-group">
                    <h5>{{ $countRewardTransactions }}</h5>
                    <span>Reward Transactions</span>
                </div>
				<?php } ?>
                <div class="clearfix"></div>

                <div class="spacer"></div>

				<?php if(I::can('view_top_earner_redeemer')) { ?>
                <h3>30-Day Top Earners &amp; Redeemers</h3>

                @if($top_redeemers->count() > 0)

                    <h6>Redeemers</h6>
                    @foreach($top_redeemers as $t)
                        <div class="redeemer">
                            @if(empty($t->user->profile_image))
                                <img src="https://placehold.it/74x74" alt="" />
                            @else
                                <img src="/media/image/{{ $t->user->profile_image }}?s=74x74" alt="" />
                            @endif
                            <h6>
                                
                                <?php
                                    
                                    $admin_partner = Auth::User()->getTopLevelPartner();
                                    $has_middleware = false;
                                    if($admin_partner->has_middleware == '1'){
                                        $user_ids_used = array();
                                        $has_middleware = true;
                                        $user_ids_used[] = $t->user_id;
                                        $term = "";
                                        $url = $admin_partner->link_middleware_api . '?method=sensitiveInfo&api_key='.$admin_partner->middleware_api_key;
                                        $json_ids = json_encode($user_ids_used);
                                        $url .= "&user_ids=" . $json_ids ;
                                        $curl = curl_init();
                                        // Set some options - we are passing in a useragent too here
                                        curl_setopt_array($curl, array(
                                            CURLOPT_RETURNTRANSFER => 1,
                                            CURLOPT_URL => $url,
                                            CURLOPT_SSL_VERIFYPEER => False,
                                            CURLOPT_USERAGENT => 'Testing Sensitive Info'
                                        ));
                                        // Send the request & save response to $resp
                                        $resp = curl_exec($curl);
                                        $resp_curl = json_decode($resp);
                                        
                                        $Users_info = $resp_curl;
                                        if($resp){
                                            foreach($Users_info as $member) {
                                                if(in_array($member->blu_id,$user_ids_used)){
                                                    $user_name = $member->first_name ." " . $member->last_name;
                                                }
                                            }
                                        }
                                        else{
                                            $user_name = "Not Available";
                                        }
                                       
                                    }
                                ?>
                                @if($has_middleware)
                                    {{ $user_name }}
                                @elseif(isset($t->user))
                                    @if(!empty($t->user->name))
                                        @if($t->user->name != 'na na')
                                            {{ $t->user->name }}
                                        @else
                                            N/A
                                        @endif
                                    @elseif(!empty($t->user->normalizedContactNumber()))
                                        {{ $t->user->normalizedContactNumber() }}
                                    @else
                                         N/A
                                    @endif
                                @else
                                    N/A
                                @endif
                            </h6>
                            <p>
                                @if($t->points_redeemed == 1)
                                    1 Point
                                @else
                                    {{ number_format($t->points_redeemed) }} Points
                                @endif
                            </p>
                        </div>
                    @endforeach
                @endif

                @if($top_earners->count() > 0)

                    <h6>Earners</h6>

                    @foreach($top_earners as $t)
                        <div class="redeemer">
                            @if(empty($t->user->profile_image))
                                <img src="https://placehold.it/74x74" alt="" />
                            @else
                                <img src="/media/image/{{ $t->user->profile_image }}?s=74x74" alt="" />
                            @endif
                            <h6>
                                <?php
                                    
                                    $admin_partner = Auth::User()->getTopLevelPartner();
                                    $has_middleware = false;
                                    if($admin_partner->has_middleware == '1'){
                                        
                                        $user_ids_used = array();
                                        $has_middleware = true;
                                        $user_ids_used[] = $t->user_id;
                                        $term = "";
                                        $url = $admin_partner->link_middleware_api . '?method=sensitiveInfo&api_key='.$admin_partner->middleware_api_key;
                                        $json_ids = json_encode($user_ids_used);
                                        $url .= "&user_ids=" . $json_ids ;
                                        $curl = curl_init();
                                        // Set some options - we are passing in a useragent too here
                                        curl_setopt_array($curl, array(
                                            CURLOPT_RETURNTRANSFER => 1,
                                            CURLOPT_URL => $url,
                                            CURLOPT_SSL_VERIFYPEER => False,
                                            CURLOPT_USERAGENT => 'Testing Sensitive Info'
                                        ));
                                        // Send the request & save response to $resp
                                        $resp = curl_exec($curl);
                                        $resp_curl = json_decode($resp);

                                        $Users_info = $resp_curl;
                                        if($resp){
                                            foreach($Users_info as $member) {
                                                if(in_array($member->blu_id,$user_ids_used)){

                                                    $user_name = $member->first_name ." " . $member->last_name;
                                                }
                                            }
                                        }
                                        else{
                                            $user_name = "Not Available";
                                        }
                                    }
                                ?>
                                @if($has_middleware)
                                    {{ $user_name }}
                                @elseif(isset($t->user))
                                    @if(!empty($t->user->name))
                                        @if($t->user->name != 'na na')
                                            {{ $t->user->name }}
                                        @else
                                            N/A
                                        @endif
                                    @elseif(!empty($t->user->normalizedContactNumber()))
                                        {{ $t->user->normalizedContactNumber() }}
                                    @else
                                        N/A
                                    @endif
                                @else
                                    N/A
                                @endif
                            </h6>
                            <p>
                                @if($t->points_rewarded == 1)
                                    1 Point
                                @else
                                    {{ number_format($t->points_rewarded) }} Points
                                @endif
                            </p>
                        </div>
                    @endforeach
                @endif                
				<?php } ?>
            </div>
            <div class="pure-u-1-3">
                @if(I::can('view_pts_rewarded_redeemed') || I::can('view_member_accounts') || I::can('view_member_accounts'))
                    <div class="support-frame">
                        <h3>SELECT DASHBOARD SETTINGS</h3>
                        <?php
                        $toplevelPartner = Auth::User()->getTopLevelPartner();
                        ?>
                        <div class="dashbooard-settings">
                            <table>
                                <tr>
                                    <td>
                                        {{ Form::label('dashboard_network_id', 'Select Network') }}
                                        <div class="clearfix"></div>
                                        {{ Form::select('dashboard_network_id', $networks, $network_id_selected, array('class' => 'pure-input-1')) }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        {{ Form::label('dashboard_partner_id', 'Select Partners') }}
                                        <div class="clearfix"></div>
                                        {{ Form::select('dashboard_partner_id', $partners, $partner_ids_selected, array('class' => 'pure-input-1 select2', 'multiple')) }}
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="spacer"></div>
                @endif
                @if(I::can('view_tickets'))
                    <div class="support-frame">
                        <h4>Open Support Tickets</h4>
                        <div class="body">
                            <table>
                                @if($open_tickets->count() > 0)
                                    @foreach ($open_tickets as $ticket) 
                                        @if($ticket->query_source == 'Member')
                                            <tr>
                                                <td class="ticket-image">
                                                    @if($ticket->sourceInfo && $ticket->sourceInfo->profile_image)
                                                        <img src="/media/image/{{ $ticket->sourceInfo->profile_image }}?s=40x40" alt="" />
                                                    @else
                                                        <img src="https://placehold.it/40x40" alt="" />
                                                    @endif
                                                </td>
                                                <td class="ticket-name">
                                                        <a href="{{ url('dashboard/call_center/view/' . base64_encode($ticket->id)) }}">
                                                        @if($ticket->sourceInfo)
                                                        <?php
                                                        $ticket_sourceInfo = $ticket->sourceInfo;
                                                        $has_middleware = false;
                                                        if($top_level_partner->has_middleware == '1'){
                                        
                                                            $user_ids_used = array();
                                                            $has_middleware = true;
                                                            $user_ids_used[] = $ticket_sourceInfo->id;
                                                            $term = "";
                                                            $url = $admin_partner->link_middleware_api . '?method=sensitiveInfo&api_key='.$admin_partner->middleware_api_key;
                                                            $json_ids = json_encode($user_ids_used);
                                                            $url .= "&user_ids=" . $json_ids ;
                                                            $curl = curl_init();
                                                            // Set some options - we are passing in a useragent too here
                                                            curl_setopt_array($curl, array(
                                                                CURLOPT_RETURNTRANSFER => 1,
                                                                CURLOPT_URL => $url,
                                                                CURLOPT_SSL_VERIFYPEER => False,
                                                                CURLOPT_USERAGENT => 'Testing Sensitive Info'
                                                            ));
                                                            // Send the request & save response to $resp
                                                            $resp = curl_exec($curl);
                                                            $resp_curl = json_decode($resp);

                                                            $Users_info = $resp_curl;
                                                            if($resp){
                                                                foreach($Users_info as $member) {
                                                                    if(in_array($member->blu_id,$user_ids_used)){

                                                                        $user_name = $member->first_name ." " . $member->last_name;
                                                                    }
                                                                }
                                                            }
                                                            else{
                                                                $user_name = "Not Available";
                                                            }
                                                            
                                                        }
                                                        ?>
                                                        @if($has_middleware)
                                                            {{ $user_name }}
                                                        @else
                                                            @if($ticket->sourceInfo->name != 'na na')
                                                                {{ $ticket->sourceInfo->name }}
                                                            @else
                                                                N/A
                                                        @endif
                                                            @endif
                                                        @else
                                                            N/A
                                                        @endif
                                                    </a>
                                                </td>
                                                <td class="ticket-ts">
                                                    Created {{ Support::timeElapsed(strtotime($ticket->created_at)) }}
                                                </td>
                                            </tr>
                                        @else
                                            <tr>
                                                <td class="ticket-image">
                                                    <img src="/media/image/{{ Support::partnerImageFromID($ticket->source_id) }}?s=40x40" alt="" />
                                                </td>
                                                <td class="ticket-name">
                                                    <a href="{{ url('dashboard/call_center/view/'.$ticket->id) }}">
                                                        {{ Support::partnerNameFromID($ticket->source_id) }}   
                                                    </a>
                                                </td>                                                
                                                <td class="ticket-ts">
                                                    Created {{ Support::timeElapsed(strtotime($ticket->created_at)) }}
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                @else
                                    <tr>
                                        <td class="empty-text">None Found</td>
                                    </tr>
                                @endif
                            </table>
                        </div>
                        <div class="rail">
                            <a href="/dashboard/call_center" title="View More" class="pure-button pure-button-primary">View More</a>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </section>
	<?php if(I::can('view_volume_overview')) { ?>
    <section id="dashboard-chart">
        <h2>
            30-Day Reward vs Redemption Transaction Volume Overview <span class="light-text">{{ date('F j, Y', strtotime("now -30 days")) }} - {{ date('F j, Y', time()) }}</span>
        </h2>
        <div id="chart-canvas"></div>
    </section>
	<?php } ?>
@stop

@section('page_script')
    <script src="//code.highcharts.com/highcharts.js"></script>
    <script src="/highcharts/js/themes/blu.js"></script>
    <script type="text/javascript">
        $(function () {
            $('#chart-canvas').highcharts({
                title: {
                    text: null
                },
                subtitle: {
                    text: null
                },
                xAxis: {
                    categories: [
                        @foreach($transaction_overview as $t)
                            '{{ $t->date }}',
                        @endforeach
                    ]
                },
                yAxis: {
                    title: {
                        text: 'Daily Volume'
                    }
                },
                credits:{
                    enabled: false
                },                
                series: [{
                    name: 'Redemption',
                    data: [{{ ReportHelper::generateSeriesData($transaction_overview, "points_redeemed") }}]
                }, {
                    name: 'Reward',
                    data: [{{ ReportHelper::generateSeriesData($transaction_overview, "points_rewarded") }}]
                }]
            });
        });

        // Update partner on network change
        $(document).on('change', '#dashboard_network_id', function(event){
            $.cookie("dashboard_network_id", $(this).val());
            location.reload();
        });

        // Update Dashboard on partner change
        $(document).on('change', '#dashboard_partner_id', function(event){
            $.cookie("dashboard_partner_id", $(this).val());
            location.reload();
        });
    </script>
@stop
<?php
} catch (Exception $ex) {
    var_dump($ex);
    exit();
}
?>