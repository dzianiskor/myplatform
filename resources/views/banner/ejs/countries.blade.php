@if($banner->countries()->count() > 0)
	@foreach($banner->countries()->with('Country')->get() as $c)
		@if($c->country_id != 0)
	    <li>
	        {{ $c->country->name }}
			@if(I::can('delete_banners'))
				<a href="/dashboard/banners/unlink-country/{{ base64_encode($banner->id) }}/{{base64_encode($c->country->id)  }}" class="delete-ajax" data-id="{{ $c->country->id }}" data-target="#country-listing">
					[Delete]
				</a>
            @endif
	    </li>
		@endif
	@endforeach
@else
    <li class="empty">Banner Not Visible</li>
@endif