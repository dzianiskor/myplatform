@if($banner->segments()->count() > 0)
	@foreach($banner->segments()->with('Segment')->get() as $s)		
	    <li>
	        {{ $s->segment->name }}
			@if(I::can('delete_banners'))
				<a href="/dashboard/banners/unlink-segment/{{ base64_encode($banner->id) }}/{{ base64_encode($s->segment->id) }}" class="delete-ajax" data-id="{{ $s->segment->id }}" data-target="#segment-listing">
					[Delete]
				</a>
            @endif
            
	    </li>		
	@endforeach
@else
    <li class="empty">Banner Not Visible</li>
@endif
