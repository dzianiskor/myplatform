@if($banner->pages()->count() > 0)
	@foreach($banner->pages()->get() as $c)
	    <li>
	        {{ BannerHelper::pageName($c->page) }}
			@if(I::can('delete_banners'))
				<a href="/dashboard/banners/unlink-page/{{ base64_encode($banner->id)}}/{{ base64_encode($c->id)}}" class="delete-ajax" data-id="{{ $c->id }}" data-target="#page-listing">
					[Delete]
				</a>
            @endif

	    </li>
	@endforeach
@else
    <li class="empty">Banner Visible on All Pages</li>
@endif