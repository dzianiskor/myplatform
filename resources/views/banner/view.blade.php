@extends('layouts.dashboard')

@section('title')
    Update Banner | BLU
@stop

@section('dialogs')

@if($errors->any())
<h4>{{$errors->first()}}</h4>
@endif
        {{-- Assign new Segment --}}
	<div id="dialog-add-segment" style="width:300px;">
	    <h4>Associate to Segment</h4>

	    {{ Form::open(array('class' => "pure-form pure-form-stacked")) }}
			<select name="segment_id" class="pure-input-1">
				@foreach($segments as $id => $name)
					<option value="{{ $id }}">{{ $name }}</option>
				@endforeach
			</select>

	        <a href="/dashboard/banners/link-segment/{{ base64_encode($banner->id) }}" class="pure-button pure-button-primary attribute-action" data-target="#segment-listing">
	            Save
	        </a>
	    {{ Form::close() }}
	</div>
	{{-- /Assign new Segment --}}

	{{-- Assign new Country --}}
	<div id="dialog-add-country" style="width:300px;">
	    <h4>Associate to Country</h4>

	    {{ Form::open(array('class' => "pure-form pure-form-stacked")) }}
			<select name="country_id" class="pure-input-1">
<!--				<option value="0">Worldwide</option>-->
				@foreach($countries as $id => $name)
					<option value="{{ $id }}">{{ $name }}</option>
				@endforeach
			</select>

	        <a href="/dashboard/banners/link-country/{{ base64_encode($banner->id) }}" class="pure-button pure-button-primary attribute-action" data-target="#country-listing">
	            Save
	        </a>
	    {{ Form::close() }}
	</div>
	{{-- /Assign new Country --}}

	{{-- Assign new Page --}}
	<div id="dialog-add-page" style="width:300px;">
	    <h4>Associate to Page</h4>

	    {{ Form::open(array('class' => "pure-form pure-form-stacked")) }}
			<select name="page" class="pure-input-1">
				@foreach(BannerHelper::availablePages() as $page)
					<option value="{{ $page }}">{{ BannerHelper::pageName($page) }}</option>
				@endforeach
			</select>

	        <a href="/dashboard/banners/link-page/{{ base64_encode($banner->id) }}" class="pure-button pure-button-primary attribute-action" data-target="#page-listing">
	            Save
	        </a>
	    {{ Form::close() }}
	</div>
	{{-- /Assign new Page --}}
@stop

@section('body')
<div class="padding">

    <div class="content-head pure-g">
        <div class="title pure-u-1-2">
			@if($banner->draft)
            <h1><i class="fa fa-book fa-fw"></i>Banner &raquo; New Banner</h1>
			@else
            <h1><i class="fa fa-book fa-fw"></i>Banner &raquo; {{{ $banner->name }}}</h1>
			@endif
        </div>

        <div class="controls pure-u-1-2">
            <a href="{{ url('dashboard/banners') }}" class="pure-button">All Banners</a>
        </div>
    </div>

    {{ Form::model($banner, array('url'    => url('dashboard/banners/update/'.base64_encode($banner->id)),
                                  'files'  => true,
                                  'method' => 'post',
                                  'class'  => "pure-form pure-form-stacked form-width")) }}

        @include('partials.errors')

		{!! Form::hidden('redirect_to', URL::previous()) !!}

	    <div class="pure-g">
	        {{--- Left Section Content ---}}
			<div class="pure-u-1-2">
			    <fieldset>
			        <legend>Basic Details</legend>
			        {{ Form::label('name', 'Banner Name', array('for'=>'name')) }}
			        {{ Form::text('name', $banner->name, array('class'=>'pure-input-1 required')) }}

			        {{ Form::label('description', 'Description', array('for'=>'description')) }}
			        {{ Form::text('description', $banner->description, array('class'=>'pure-input-1')) }}

			        {{ Form::label('title', 'Title', array('for'=>'title')) }}
			        {{ Form::text('title', $banner->title, array('class'=>'pure-input-1')) }}



			        {{ Form::label('partner_id', 'Partner') }}
			        <select name="partner_id" class="pure-input-1-2 required" id="partner_id" data-draft="{{ $banner->draft }}" data-id="{{ $banner->id }}">
			            @if($banner->partner)
			                <option value="{{ $banner->partner->id }}">{{ $banner->partner->name }}</option>
			                @foreach($partners as $id => $name)
			                    @if($id != $banner->partner->id)
			                        <option value="{{ $id }}">{{ $name }}</option>
			                    @endif
			                @endforeach
			            @else
			                @foreach($partners as $id => $name)
			                    <option value="{{ $id }}">{{ $name }}</option>
			                @endforeach
			            @endif
			        </select>

                                {{ Form::label('lang_id', 'Language') }}

			        <select name="lang_id" class="pure-input-1-2 required" id="lang_id" data-draft="{{ $banner->draft }}" data-id="{{ $banner->id }}">
			            @if($banner->language)
			                <option value="{{ $banner->language->id }}">{{ $banner->language->name }}</option>
			                @foreach($languages as $id => $name)
			                    @if($id != $banner->lang_id)
			                        <option value="{{ $id }}">{{ $name }}</option>
			                    @endif
			                @endforeach
			            @else
			                @foreach($languages as $id => $name)
			                    <option value="{{ $id }}">{{ $name }}</option>
			                @endforeach
			            @endif
			        </select>
								<!--
					{{ Markup::checkbox("logged_in", "Logged In", $banner->logged_in, 1) }}<i class="fa fa-info-circle fa-fw" title="you have to choose a segment for logged in banners" style="cursor: help;"></i>
					{{ Markup::checkbox("logged_out", "Logged Out", $banner->logged_out, 1) }}
-->

						<input type="checkbox" name="logged_in" id="logged_in" value="1" <?php echo ($banner->logged_in) ? 'checked': ''; ?> />
						<span style="font-size: 90%;">Logged In</span><i class="fa fa-info-circle fa-fw" title="you have to choose a segment for logged in banners" style="cursor: help;"></i>
						<br/>
						<input type="checkbox" name="logged_out" id="logged_out" value="1" <?php echo ($banner->logged_out) ? 'checked': ''; ?> />
						<span style="font-size: 90%;">Logged Out</span>

			    </fieldset>

			    <fieldset>
			        <legend>Banner Image</legend>

					<span class="info">
						For best results, use images that are 1600x590 (pixels) in size.
					</span>
                                {{ Form::label('link', 'Link', array('for'=>'link')) }}
			        {{ Form::text('link', $banner->link, array('class'=>'pure-input-1')) }}

			        {{ Form::label('status', 'Status', array('for'=>'status')) }}
					<select name="status" class="pure-input-1-2">
					<?php foreach(Meta::bannerStatuses() as $s): ?>
						<?php if($s == $banner->status): ?>
							<option value="<?php echo $s; ?>" selected="selected"><?php echo $s; ?></option>
						<?php else: ?>
							<option value="<?php echo $s; ?>"><?php echo $s; ?></option>
						<?php endif; ?>
					<?php endforeach; ?>
					</select>
			        <div class="image-rail" style="width:340px;">
			            <table class="banner_view" style="margin-top:20px;margin-bottom:20px;">
			                <tr>
			                    <td width="80">
			                        @if(empty($banner->image()))
			                            <img src="http://placehold.it/74x74" alt="" />
			                        @else
			                            <img src="<?php echo App\Http\Controllers\MediaController::getImageFromId($banner->image()->id)?>?s=74x74" alt="" style="width:80px;"/>
			                        @endif
			                    </td>
			                    <td style="padding-left:10px;">
			                        {{ Form::label('image', 'Banner Image') }}
			                        {{ Form::file('image') }}
			                    </td>
			                </tr>
			            </table>
			        </div>

			    </fieldset>

                            <fieldset>
			        <legend>Mobile Banner</legend>

					<span class="info">
						For best results, use images that are 750 x 676 (pixels) in size.
					</span>
                                {{ Form::label('mob_link', 'Mobile Link', array('for'=>'mob_link')) }}
			        {{ Form::text('mob_link', $banner->mob_link, array('class'=>'pure-input-1')) }}

                                {{ Form::label('mob_status', 'Mobile Status', array('for'=>'mob_status')) }}
					<select name="mob_status" class="pure-input-1-2">
					<?php foreach(Meta::bannerStatuses() as $s): ?>
						<?php if($s == $banner->mob_status): ?>
							<option value="<?php echo $s; ?>" selected="selected"><?php echo $s; ?></option>
						<?php else: ?>
							<option value="<?php echo $s; ?>"><?php echo $s; ?></option>
						<?php endif; ?>
					<?php endforeach; ?>
					</select>
			        <div class="image-rail" style="width:340px;">
			            <table style="margin-top:20px;margin-bottom:20px;">
			                <tr>
			                    <td width="80">
			                        @if(empty($banner->mobimage()))
			                            <img src="http://placehold.it/74x74" alt="" />
			                        @else
			                            <img src="<?php echo App\Http\Controllers\MediaController::getImageFromId( $banner->mobimage()->id) ?>?s=74x74" alt="" style="width:80px;"/>
			                        @endif
			                    </td>
			                    <td style="padding-left:10px;">
			                        {{ Form::label('mob_image', 'Mobile Image') }}
			                        {{ Form::file('mob_image') }}
			                    </td>
			                </tr>
			            </table>
			        </div>

			    </fieldset>
			</div>
	        {{--- /Left Section Content ---}}

	        {{--- Right Section Content ---}}
	        <div class="pure-u-1-2 attributes">
	            <div class="padding-left-40">
					<!--{{-- Segment Listing --}}

					<h3>
					    <a href="#dialog-add-segment" title="New Segment" class="dialog">
					        Segments <i class="fa fa-plus-circle fa-fw"></i>
					    </a>
					</h3>
					<ul id="segment-listing">
						@if(count($banner->segments) > 0)
						    @foreach($banner->segments as $s)
						        <li>
						            {{ $s->name }}
									@if(I::can('delete_banners'))
										<a href="/dashboard/banners/unlink-segment/{{ $banner->id }}/{{ $s->id }}" class="delete-ajax" data-id="{{ $s->id }}" data-target="#segment-listing">
						                	[Delete]
						            	</a>
									@endif
						        </li>
						    @endforeach
						@else
						    <li class="empty">No Segments Associated</li>
						@endif
					</ul>

					{{-- /Segment Listing --}}-->

                                        {{-- Segment Listing --}}
						<h3>
                            @if(I::can_edit('banners', $banner))
						    <a href="#dialog-add-segment" title="Add Segment" class="dialog" id="segments_dialog">
						        Segments <i class="fa fa-plus-circle fa-fw"></i>
						    </a>
                            @else
                                Segments
                            @endif
						</h3>
						<ul id="segment-listing">
							@include('banner/ejs/segments', array('banner' => $banner))
						</ul>
					{{-- /Segment Listing --}}

					{{-- Country Listing --}}
						<h3>
                            @if(I::can_edit('banners', $banner))
						    <a href="#dialog-add-country" title="Add Country" class="dialog">
						        Countries <i class="fa fa-plus-circle fa-fw"></i>
						    </a>
                            @else
                                Countries
                            @endif
						</h3>
						<ul id="country-listing">
							@include('banner/ejs/countries', array('banner' => $banner))
						</ul>
					{{-- /Country Listing --}}

					{{-- Page Listing --}}
						<h3>
                            @if(I::can_edit('banners', $banner))
						    <a href="#dialog-add-page" title="Add Page" class="dialog">
						        Pages <i class="fa fa-plus-circle fa-fw"></i>
						    </a>
                            @else
                                Pages
                            @endif
						</h3>
						<ul id="page-listing">
							@include('banner/ejs/pages', array('banner' => $banner))
						</ul>
					{{-- /Page Listing --}}

	            </div>
	        </div>
	        {{--- /Right Section Content ---}}
	    </div>

        <!-- Form Buttons -->
        <div class="form-buttons">
            <div class="left">
                <a href="{{ url('dashboard/banners') }}" class="pure-button">Cancel</a>
            </div>
			@if($banner->draft)
				@if(I::can('create_banners'))
					<div class="right">
						<button type="submit" class="pure-button pure-button-primary">Create Banner</button>
					</div>
				@endif
			@else
				@if(I::can('edit_banners'))
					<div class="right">
						<button type="submit" class="pure-button pure-button-primary">Update Banner</button>
					</div>
				@endif
			@endif
            <div class="clearfix"></div>
        </div>
        <!-- /Form Buttons -->
    {{ Form::close() }}
</div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-banners');
    </script>
@stop
