@extends('layouts.dashboard')

@section('title')
    Update Category | BLU
@stop

@section('body')
<div class="padding">

    <div class="content-head pure-g">
        <div class="title pure-u-1-2">
            <h1><i class="fa fa-sort-alpha-desc fa-fw"></i>Category &raquo; {{{ $category->name }}}</h1>
        </div>

        <div class="controls pure-u-1-2">
            <a href="{{ url('/dashboard/categories') }}" class="pure-button">All Categories</a>
        </div>
    </div>

    {{ Form::model($category, array('action' => array('CategoryController@saveCategory', base64_encode($category->id)),
                                   'class'  => 'pure-form pure-form-stacked form-width')) }}

        @include('category/form', array('category' => $category))

        {!! Form::hidden('redirect_to', URL::previous()) !!}

        <!-- Form Buttons -->
        <div class="form-buttons">
            <div class="left">
                <a href="{{ url('dashboard/categories') }}" class="pure-button">Cancel</a>
            </div>
            @if($category->draft)
                @if(I::can('create_categories'))
                    <div class="right">
                        <button type="submit" class="pure-button pure-button-primary">Save Category</button>
                    </div>
                @endif
            @else
                @if(I::can('edit_categories'))
                    <div class="right">
                        <button type="submit" class="pure-button pure-button-primary">Update Category</button>
                    </div>
                @endif
            @endif
            <div class="clearfix"></div>
        </div>
        <!-- /Form Buttons -->

    {{ Form::close() }}

    {{--- Cattranslations ---}}
            <fieldset>
                <legend>Category Translation</legend>

                <table id="cattranslation-list" class="pure-table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Cattranslation Name</th>
                            <th>Language</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($category->cattranslations) > 0)
                            @foreach($category->cattranslations as $cattranslation)
                                <tr>
                                    <td>{{ $cattranslation->id }}</td>
                                    @if($cattranslation->draft)
                                        <td>{{{ $cattranslation->name }}} (Draft)</td>
                                    @else
                                        <td>{{{ $cattranslation->name }}}</td>
                                    @endif
                                    <td>
                                        <?php
                                            $lang = App\Language::where('id', $cattranslation->lang_id)->first();
                                            
                                            echo $lang->name;
                                        ?>
                                    </td>
                                    <td>
                                        @if(!I::can('delete_categories') && !I::can('edit_categories'))
                                            N/A
                                        @endif
                                        @if(I::can('edit_categories'))
                                            <a href="{{ url('dashboard/cattranslation/edit/'.base64_encode($cattranslation->id))}}" class="edit-cattranslation" title="Edit">Edit</a>
                                        @endif
                                        @if(I::can('delete_categories'))
                                            <a href="{{ url('dashboard/cattranslation/delete/'.base64_encode($cattranslation->id)) }}" class="delete-cattranslation" title="Delete">Delete</a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr class="empty">
                                <td colspan="4">No Category translation defined</td>
                            </tr>
                        @endif
                    </tbody>
                </table>

                @if($category->draft)
                    @if(I::can('create_categories'))
                        <a href="#dialog-new-cattranslation" data-category_id="{{ base64_encode($category->id) }}" class="btn-new-cattranslation top-space" title="Add new cattranslation location">
                            <i class="fa fa-plus-circle fa-fw"></i>Add new cattranslation
                        </a>
                    @endif
                @else
                    @if(I::can('edit_categories'))
                        <a href="#dialog-new-cattranslation" data-category_id="{{ base64_encode($category->id) }}" class="btn-new-cattranslation top-space" title="Add new cattranslation location">
                            <i class="fa fa-plus-circle fa-fw"></i>Add new cattranslation
                        </a>
                    @endif
                @endif
            </fieldset>
            {{--- /Cattranslations ---}}
</div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-categories');
        enqueue_script('new-category');
    </script>
@stop
