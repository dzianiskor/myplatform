<div class="pure-u-1-2">
    <fieldset>
        <legend>Basic Details</legend>

        <span class="info">
            Note: By default all categories are implicitly children of the 'All' category.
        </span>

        {{ Form::label('name', 'Category Name', array('for'=>'description')) }}
        {{ Form::text('name', Input::old('name'), array('class'=>'pure-input-1 required')) }}

        {{ Form::label('parent_category_id', 'Parent Category', array('for'=>'parent_category_id')) }}
        {{ Form::select('parent_category_id', $categories, Input::old('parent_category_id'), array('class' => 'pure-input-1-3')) }}    

    </fieldset>
</div>
		