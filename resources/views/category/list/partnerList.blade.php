@if(count($category->partners) > 0)
    @foreach($category->partners as $p)
        <li>
            {{ $p->name }}
            @if(I::can('delete_categories'))
                @if (Auth::User()->partners->contains($p) || I::am("BLU Admin"))
                    <a href="/dashboard/category/unlink_partner/{{ base64_encode($category->id) }}/{{ base64_encode($p->id) }}" class="delete-ajax" data-id="{{ $p->id }}" data-target="#partner-listing">
                        [Delete]
                    </a>
                @endif
            @endif
        </li>
    @endforeach
@else
    <li class="empty">No Partner Associated</li>
@endif
