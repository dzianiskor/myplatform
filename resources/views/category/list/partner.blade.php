{{-- Partner Listing --}}
<h3>
    @if(I::can_edit('categories', $category))
    <a href="#dialog-add-partner" title="New Partner" class="dialog">
        Partners <i class="fa fa-plus-circle fa-fw"></i>
    </a>
    @else
        Partners
    @endif
</h3>
<ul id="partner-listing">
    @include('category/list/partnerList', array('category'=>$category))
</ul>
<label class="error" style="display: none;">This field is required.</label>
{{-- /Partner Listing --}}
