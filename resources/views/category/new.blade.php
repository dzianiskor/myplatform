@extends('layouts.dashboard')

@section('title')
    New Category | BLU
@stop

@section('body')
  <div class="padding">

      <div class="content-head pure-g">
          <div class="title pure-u-1-2">
              <h1><i class="fa fa-sort-alpha-desc fa-fw"></i>New Category</h1>
          </div>

          <div class="controls pure-u-1-2">
              <a href="{{ url('dashboard/categories') }}" class="pure-button">All Categories</a>
          </div>
      </div>

      {{ Form::open(array('action' => 'CategoryController@newCategory', "class" => 'pure-form pure-form-stacked form-width')) }}

        @include('partials.errors')
        
        @include('category/form')

        <!-- Form Buttons -->
        <div class="form-buttons">
          <div class="left">
            <a href="{{ url('dashboard/categories') }}" class="pure-button">Cancel</a>
          </div>
          <div class="right">
            <button type="submit" class="pure-button pure-button-primary">Save Category</button>
          </div>
          <div class="clearfix"></div>
        </div>
        <!-- /Form Buttons -->
        
     {{ Form::close() }}
  </div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-categories');
    </script>
@stop
