    {{ Form::open(array('url'    => url('dashboard/categories/updatePopover/'.base64_encode($category->id)),
                        'method' => 'post',
                        'class'  => 'pure-form pure-form-stacked',
                        'id'     => 'new-category-form')) }}
    @include('category/form')
    <!-- Form Buttons -->
    <div class="form-buttons">
        <div class="left">
            <a href="#" class="pure-button close-dialog">Cancel</a>
        </div>
        <div class="right">
            <a href="#" class="pure-button pure-button-primary category-save">Save</a>
        </div>
        <div class="clearfix"></div>
    </div>
