@extends('layouts.empty')

@section('title')
    Admin Login | BLU
@stop

@section('body')
    <div class="single-login-form hasBackground">

        {{ Form::open(array('url' => url("auth/login"), "parsley-validate" => true, "class" => 'pure-form', "id" => "login-form")) }}

            @include('partials.errors')

            <fieldset class="pure-group single-fieldset">
                <input name="username" type="text" id="username" class="pure-input-1" placeholder="Username" />
                <input name="password" type="password" value="" id="password" class="pure-input-1" placeholder="Password" />
            </fieldset>
            {{ Form::submit('Log In', array('class' => 'pure-button pure-input-1 pure-button-primary g-recaptcha')) }}
            <!--{{ Form::submit('Log In', array('class' => 'pure-button pure-input-1 pure-button-primary g-recaptcha', 'data-sitekey' => '6LcBXCEUAAAAAB_HVVlCIPH1obgABvNuurmcTIqP', 'data-callback' => 'onSubmit')) }}-->

        {{ Form::close() }}

        <a href="/password/remind" class="single-nav-link">Forgot Password</a>
    </div>
@stop

@section('page_script')
    <script src="{{ asset('js/vendor/parsley.min.js') }}"></script>
    <script>
    function onSubmit(token) {
	document.getElementById("login-form").submit();
    }
    </script>
@stop
