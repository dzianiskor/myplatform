@extends('layouts.dashboard')

@section('title')
    {{ $report->name }} | BLU
@stop

@section('body')
    <div class="padding">

        <div class="content-head pure-g">
            <div class="title pure-u-1-2">
                <h1>
                    <i class="fa fa-bar-chart-o fa-fw"></i> {{ $report->name }}
                </h1>
            </div>
            <div class="controls pure-u-1-2">
                @include('report.partials.controls', $report)
            </div>
        </div>

        <div class="report-info">
            <i class="fa fa-info-circle"></i> {{ $report->description }}
        </div>

        <div class="pure-g">
            <div class="pure-u-1-5 report-controls">
                <div class="padding">

                    <h4>Report Settings</h4>

                    {{-- Custom Report Controls --}}
                    {{ Form::open(array('class' => "pure-form pure-form-stacked", 'method' => "GET")) }}
                    @include('report.partials.period', $defaults)

                    @include('report.partials.periodpreset', $defaults)

                    @include('report.partials.networkcurrency', $defaults)

                    <div class="js-partner-container">
                        @include('report.partials.partners', $defaults)
                    </div>

                    @include('report.partials.submit')

                    {{ Form::close() }}
                    {{-- /Custom Report Controls --}}

                </div>
            </div>
            <div class="pure-u-4-5">
                <table class="pure-table">
                    <thead>
                    <tr>

                        <th><a href="{{ Filter::searchUrl('sort=creation') }}"> Date </a> </th>
                        <th>Partner</th>
                        <th>Pts Used</th>

                    </tr>
                    </thead>
                    <tbody>
                    @if (count($resultRalph) > 0)
                        @foreach($resultRalph as $keyra1 => $t)
                            <tr>
                                <td>
                                    <?php
                                    if (isset($t->crmonth)) {
                                        $dateObj   = DateTime::createFromFormat('!m', $t->crmonth);
                                        $monthName = $dateObj->format('M');
                                        echo  $monthName . ". ";
                                    }
                                    ?>
                                    @if (strlen($t->creation) > 4)
                                        <?php
                                        $dateObj = new DateTime($t->creation);
                                        $dateformatted = $dateObj->format('F j, Y');
                                        echo $dateformatted;
                                        ?>
                                    @else
                                        {{ $t->creation }}
                                    @endif

                                </td>

                                <td>
                                    {{ $t->partner_name }}
                                </td>

                                <td>
                                    @if($t->ptsused)
                                        {{ number_format($t->ptsused) }}
                                    @else
                                        0
                                    @endif
                                </td>

                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="6">No Report Data Found</td>
                        </tr>
                    @endif
                    </tbody>
                </table>

                {{ $resultRalph instanceof Collection ? $resultRalph->appends(Input::except('page'))->links('vendor.pagination.custom-report') : '' }}
            </div>
        </div>

    </div>
@stop

@section('page_script')
    <script>
        $(document).on('change', '#network_currency', function () {
            $.ajax({
                url: '{{ route('api.reports.partials.partners') }}?network_id=' + $(this).val() + '&{!! request()->server('QUERY_STRING') !!}'
            }).done(function(data){
                $('.js-partner-container').html(data);
            });
        });
        $('#network_currency').trigger('change');

        set_menu('mnu-reporting');
    </script>
@stop
