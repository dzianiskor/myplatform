@extends('layouts.dashboard')

@section('title')
    {{ $report->name }} | BLU
@stop

@section('body')
<div class="padding">
		<input type="hidden" name="loaded" id="loaded" value="{{ count($results) }}"/>
        <div class="content-head pure-g">
            <div class="title pure-u-1-2">
                <h1>
                    <i class="fa fa-bar-chart-o fa-fw"></i> {{ $report->name }}
                </h1>
            </div>
            <div class="controls pure-u-1-2">
                @include('report.partials.controls', $report)
            </div>
        </div>
        <div class="report-info">
            <i class="fa fa-info-circle"></i> {{ $report->description }}
        </div>
		<div class="pure-g">
			  <div class="pure-u-1-5 report-controls">
				  <div class="padding">

					  <h4>Report Settings</h4>

						{{--- Custom Report Controls ---}}
						{{ Form::open(array('class' => "pure-form pure-form-stacked", 'method' => "GET")) }}
						  @include('report.partials.period', $defaults)
						{{--	@include('report.partials.travel', $defaults)--}}
						{{--	@include('report.partials.periodpreset', $defaults)--}}
						{{--	@include('report.partials.bypartner', $defaults) --}}
                                                @include('report.partials.currency', $defaults)
                                                @include('report.partials.suppliers', $defaults)
						{{--	@include('report.partials.partners', $defaults)--}}
                                                @include('report.partials.submit')
						{{ Form::close() }}
						{{--- /Custom Report Controls ---}}
					  <input type="hidden" name="report_number" id="report_number" value="31"/>
				  </div>
			  </div>

		<div class="pure-u-4-5">
			<div class="table-totals">
                    <table style="width: 100%; border-style: none; border-width: 0px;">
                        <tr>
                             <td><span>Total Cash Payments: <strong>{{  number_format($totals['totalcashpayments'],2) }}</strong></span></td>
                             <td><span>Total Points Redeemed: <strong>{{  number_format($totals['totalpointsredeemed']) }}</strong></span></td>
                        </tr>
                        <tr>
                             <td><span>Total Amount Redeemed: <strong>{{ number_format($totals['totalamounredeemed'], 2) }}</strong></span></td>
                             <td><span>Total Spent: <strong>{{ number_format($totals['totalspent'], 2) }}</strong></span></td>
                        </tr>
                    </table>
                </div>
			<table class="pure-table">
				<thead>
					<tr>
						<th>Supplier</th>
                                                <th>Total Pts Redeemed</th>
						<th>Total Cash Paid</th>
                                                <th>Total Amount Redeemed</th>
                                                <th>Total Spent</th>
					</tr>
				</thead>
				<tbody>
					 @if(count($results) > 0)

                                            @foreach($results as $key => $resMa)
                                                <?php
                                                    if(!isset($resMa["supplierId"])){
                                                        continue;
                                                    }
                                                ?>
                                                <tr>
                                                    <td>
                                                        @if($resMa["supplierId"] > 0)
                                                            {{ App\Supplier::find($resMa["supplierId"])->name }}
                                                        @else
                                                            Others
                                                        @endif
                                                    </td>
                                                    <td>
                                                            {{ number_format($resMa["total_pts_redeemed"] ); }}
                                                    </td>
                                                    <td>
                                                            {{ number_format($resMa["total_cash_paid"] * $totals['exchangeRate'],2)}}
                                                    </td>
                                                    <td>
                                                            {{ number_format ( $resMa["total_amount_redeemed"]* $totals['exchangeRate'], 2)}}
                                                    </td>
                                                    <td>
                                                            {{ number_format ( ($resMa["total_amount_redeemed"] + $resMa["total_cash_paid"] )* $totals['exchangeRate'], 2)}}
                                                    </td>
                                                </tr>
                                            @endforeach
                                            @else
                        <tr>
                            <td colspan="6">No Report Data Found</td>
                        </tr>
					 @endif
				</tbody>
			</table>
		</div>
			</div>
	  </div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-reporting');
    </script>
@stop
