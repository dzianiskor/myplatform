@extends('layouts.dashboard')

@section('title')
    {{ $report->name }} | BLU
@stop

@section('body')
    <div class="padding">
		<input type="hidden" name="loaded" id="loaded" value="{{ count($results) }}"/>
        <div class="content-head pure-g">
            <div class="title pure-u-1-2">
                <h1>
                    <i class="fa fa-bar-chart-o fa-fw"></i> {{ $report->name }}
                </h1>
            </div>
            <div class="controls pure-u-1-2">
                @include('report.partials.controls', $report)
            </div>
        </div>

        <div class="report-info">
            <i class="fa fa-info-circle"></i> {{ $report->description }}
        </div>

        <div class="pure-g">
            <div class="pure-u-1-5 report-controls">
                <div class="padding">

                    <h4>Report Settings</h4>

                    {{--- Custom Report Controls ---}}
                    {{ Form::open(array('class' => "pure-form pure-form-stacked", 'method' => "GET")) }}

                        @include('report.partials.period', $defaults)

						@include('report.partials.currency', $defaults)

						@include('report.partials.networkcurrency', $defaults)
						
                        @include('report.partials.partners', $defaults)

                        @include('report.partials.submit')

                    {{ Form::close() }}
                    {{--- /Custom Report Controls ---}}

                </div>
            </div>
            <div class="pure-u-4-5">

                <div class="table-totals">

                    <table style="width: 100%; border-style: none; border-width: 0px;">
                        <tr>
                            <td><span>Number of Reward Transactions: <strong><?php echo number_format($totals['trx_reward']); ?></strong></span></td>

                        </tr>
                        <tr>
                            <td><span>Total Points Rewarded: <strong><?php echo number_format($totals['pts_reward'],2); ?></strong></span></td>

                        </tr>
                        <tr>
                            <td><span>Total Amount Rewarded: <strong><?php echo number_format(($totals['amt_reward'] * $totals['exchangeRate']),2); ?></strong></span></td>

                        </tr>
                    </table>


                </div>

                <div class="pure-u-4-5 dataTables_wrapper">
                    <div id="datatable_processing" class="dataTables_processing" style="display: none;">Processing...</div>
                    <table class="pure-table">
                    <thead>
                        <tr>
                            <th>Period</th>
                            <th>Member Name</th>
                            <th>Total Points Rewarded</th>
                            <th>Total Amount Paid (<?php echo $totals['currencyShortCode']; ?>)</th>
                        </tr>
                    </thead>
                    <tbody>
                    @if(count($results) > 0)
                    
                        @foreach($results as $t)
                            <tr>
                                <td>
                                    {{ ReportHelper::chartDateString($defaults) }}
                                </td>
                                <?php 
                                $arr_users_ids = array();
                                    if($sensitive['has_middleware'] ==1){
                                        
                                        foreach($sensitive['user_info'] as $u_info){
                                            if($t->user_id == $u_info->blu_id){
                                                $arr_users_ids[] = $t->user_id;
                                                $t->first_name = $u_info->first_name;
                                                $t->last_name = $u_info->last_name;
                                            }
                                        }

                                    }
                                ?>
                                <td>
                                    @if($t->user && !in_array($t->user_id, $arr_users_ids) && trim($t->user->name) != 'na na')
                                        {{ $t->user->name }}
                                    @elseif($sensitive['has_middleware'])
                                        <?php
                                             $userName  = $t->first_name . ', ' . $t->last_name;
                                             if($userName != ', '){
                                                 ;
                                             }
                                             else{
                                                $userName = 'N/A'; 
                                             }
                                        ?>
                                        {{ $userName }} ({{$t->user_id}})
                                    @else
                                        <i>N/A</i>
                                    @endif
                                </td>
                                <td>
                                    {{ number_format($t->pts_rewarded) }}
                                </td>
                                <td>
                                    {{ number_format($t->tot_amount * $totals['exchangeRate'], 2) }}
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="4">No Report Data Found</td>
                        </tr>
                    @endif
                    </tbody>
                </table>

                {{ $results->appends(Input::except('page'))->links('vendor.pagination.custom-report') }}
            </div>
        </div>

    </div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-reporting');
    </script>
@stop