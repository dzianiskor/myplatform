@extends('layouts.dashboard')

@section('title')
    {{ $report->name }} | BLU
@stop

@section('body')
    <div class="padding">
        <input type="hidden" name="loaded" id="loaded" value="{{ count($results) }}"/>
        <div class="content-head pure-g">
            <div class="title pure-u-1-2">
                <h1>
                    <i class="fa fa-bar-chart-o fa-fw"></i> {{ $report->name }}
                </h1>
            </div>
            <div class="controls pure-u-1-2">
                @include('report.partials.controls', $report)
            </div>
        </div>

        <div class="report-info">
            <i class="fa fa-info-circle"></i> {{ $report->description }}
        </div>

        <div class="pure-g">
            <div class="pure-u-1-5 report-controls">
                <div class="padding">

                    <h4>Report Settings</h4>

                    {{-- Custom Report Controls --}}
                    {{ Form::open(array('class' => "pure-form pure-form-stacked", 'method' => "GET")) }}

                    @include('report.partials.period', $defaults)

                    <?php
                    $currencies = App\Currency::where('draft', false)->orderBy('name', 'ASC')->get();
                    if ($defaults['currency'] == 'not') {
                        $defaults['currency']   = 17;
                    };
                    ?>
                    <h5>Currency</h5>
                    <select name="currencies" id="currencies" style="width: 90%;">
                        @foreach($currencies as $currency)
                            <option value="<?php echo $currency->id; ?>" <?php echo $defaults['currency'] == $currency->id ? 'selected' : ''; ?>><?php echo $currency->name; ?></option>
                        @endforeach
                    </select>

                    @include('report.partials.networks', $defaults)

                    @include('report.partials.stores_part', $defaults)

                    @include('report.partials.submit')

                    {{ Form::close() }}
                    {{-- /Custom Report Controls --}}

                </div>
            </div>
            <div class="pure-u-4-5">

                <div class="table-totals">
                    <table style="width: 100%; border-style: none; border-width: 0px;" id="totals">
                        <tr>
                            <td><span>Number of Redeem Transactions: <strong id="trx_redeem">@if (isset($results)) Loading... @else 0 @endif</strong></span></td>
                        </tr>
                        <tr>
                            <td><span>Total Points Redeemed: <strong id="pts_redeem">@if (isset($results)) Loading... @else 0 @endif</strong></span></td>
                        </tr>
                        <tr>
                            <td><span>Total Amount Redeemed: <strong id="amt_redeem">@if (isset($results)) Loading... @else 0.00 @endif</strong></span></td>
                        </tr>
                    </table>
                </div>

                <table class="pure-table report-table" id="datatable">
                    <thead>
                    <th>Category</th>
                    <th>Total Points Redeemed</th>
                    <th>Total Value of Pts Redeemed</th>
                    <th>Total Transactions</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan="8" class="text-center">No Report Data Found.</td>
                        </tr>
                    </tbody>
                </table>

            </div>
        </div>

    </div>
@stop

@section('page_script')
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
    <script>
        @if (isset($results))
        $(function(){
            $('#datatable').DataTable({
                "processing": true,
                "serverSide": true,
                "lengthChange": false,
                "ordering": false,
                "searching": false,
                "pageLength": 40,
                "info": false,
                "language": {
                    "paginate": {
                        "previous": "&lt;",
                        "next": "&gt;"
                    }
                },
                "ajax": "{{ route('api.reports.report47') }}?{!! request()->server('QUERY_STRING') !!}",
                "columns": [
                    { "data": "category_name" },
                    { "data": "points_redeemed" },
                    { "data": "amt_redeem" },
                    { "data": "transactions_total" }
                ]
            });

            $.ajax({
                url: '{{ route('api.reports.report47totals') }}?exchange_rate={{ $totals['exchangeRate'] }}&{!! request()->server('QUERY_STRING') !!}',
                context: $('#totals'),
            }).done(function(data){
                $('strong', $(this)).each(function(){
                    $(this).text(data[$(this).attr('id')]);
                });
            });
        });
        @endif

        set_menu('mnu-reporting');
    </script>
@stop
