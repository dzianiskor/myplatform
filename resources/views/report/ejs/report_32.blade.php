@extends('layouts.dashboard')

@section('title')
    {{ $report->name }} | BLU
@stop

@section('body')
    <div class="padding">

        <div class="content-head pure-g">
            <div class="title pure-u-1-2">
                <h1>
                    <i class="fa fa-bar-chart-o fa-fw"></i> {{ $report->name }}
                </h1>
            </div>
            <div class="controls pure-u-1-2">
                @include('report.partials.controls', $report)
            </div>
        </div>

        <div class="report-info">
            <i class="fa fa-info-circle"></i> {{ $report->description }}
        </div>

        <div class="pure-g">
            <div class="pure-u-1-5 report-controls">
                <div class="padding">

                    <h4>Report Settings</h4>

                    {{--- Custom Report Controls ---}}
                    {{ Form::open(array('class' => "pure-form pure-form-stacked", 'method' => "GET")) }}

                        @include('report.partials.period', $defaults)

                        @include('report.partials.partners', $defaults)

                        @include('report.partials.submit')

                    {{ Form::close() }}
                    {{--- /Custom Report Controls ---}}

                </div>
            </div>
            <div class="pure-u-4-5">

                <div class="table-totals">

                    <table style="width: 100%; border-style: none; border-width: 0px;">
                        <tr>

                            <td><span>Total Points Rewarded: <strong>{{ number_format($tot['pts_reward']); }}</strong></span></td>
                        </tr>
                        <tr>

                            <td><span>Total Points Redeemed: <strong>{{ number_format($tot['pts_redeem'],0); }}</strong></span></td>
                        </tr>
                        <tr>

                            <td><span>Outstanding Balance: <strong>{{ number_format($tot['outstanding_balance'],0); }}</strong></span></td>
                        </tr>
                    </table>


                </div>

                <table class="pure-table">
                    <thead>
                        <tr>
                            <th>User ID</th>
                            <th>Member Name</th>
                            <th>References </th>
                            <th>Points Rewarded</th>
                            <th>Points Redeemed</th>
                            <th>Outstanding Balance</th>
                        </tr>
                    </thead>
                    <tbody>
                    @if(count($results) > 0)
                        @foreach($results as $t)
                            <tr>
                                <td>
                                    <!--{{ App\classes\ReportHelper::chartDateString($defaults) }}-->
									{{ $t->user_id }}
                                </td>
                                <td>
                                    @if(isset($t->user))
                                        {{$t->user->name}}
                                    @else
                                        N/A
                                    @endif
                                </td>
                                
                                <td>
                                    @foreach($t->refs as $ref)
                                        <span>{{ $ref->number }}<br></span>
                                    @endforeach
                                </td>
								<td>
                                    {{ number_format($t->pts_reward) }}
                                </td>
                                <td>
                                    {{ number_format($t->pts_used) }}
                                </td>

                                <td>
                                    {{ number_format($t->balance) }}
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="4">No Report Data Found</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
        </div>

    </div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-reporting');
    </script>
@stop