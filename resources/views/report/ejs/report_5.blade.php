@extends('layouts.dashboard')

@section('title')
    {{ $report->name }} | BLU
@stop

@section('body')
    <div class="padding">
		<input type="hidden" name="loaded" id="loaded" value="{{ count($results) }}"/>
        <div class="content-head pure-g">
            <div class="title pure-u-1-2">
                <h1>
                    <i class="fa fa-bar-chart-o fa-fw"></i> {{ $report->name }}
                </h1>
            </div>
            <div class="controls pure-u-1-2">
                @include('report.partials.controls', $report)
            </div>
        </div>

        <div class="report-info">
            <i class="fa fa-info-circle"></i> {{ $report->description }}
        </div>

        <div class="pure-g">
            <div class="pure-u-1-5 report-controls">
                <div class="padding">

                    <h4>Report Settings</h4>

                    {{--- Custom Report Controls ---}}
                    {{ Form::open(array('class' => "pure-form pure-form-stacked", 'method' => "GET")) }}

                        @include('report.partials.period', $defaults)

						@include('report.partials.currency', $defaults)

						@include('report.partials.networkcurrency', $defaults)

                        @include('report.partials.stores', $defaults)

                        @include('report.partials.submit')

                    {{ Form::close() }}
                    {{--- /Custom Report Controls ---}}

                </div>
            </div>
            <div class="pure-u-4-5">

                <div class="table-totals">

                    <table style="width: 100%; border-style: none; border-width: 0px;">
                        <tr>
                            <td><span>Number of Reward Transactions: <strong> <?php echo number_format($totals['trx_reward']); ?></strong></span></td>
                            <td><span>Number of Redeem Transactions: <strong> <?php echo number_format($totals['trx_redeem']); ?></strong></span></td>
                        </tr>
                        <tr> 
                            <td><span>Total Transaction Amount (Rewarded): <strong> <?php echo number_format($totals['pts_reward'], 2); ?></strong></span></td>
                            <td><span>Total Transaction Amount (Redeemed): <strong> <?php echo number_format($totals['pts_redeem'], 2); ?></strong></span></td>
                        </tr>
                        <tr>
                            <td><span>Total Amount Rewarded: <strong> <?php echo number_format(($totals['amt_reward'] * $totals['exchangeRate']), 2);; ?> </strong></span></td>
                            <td><span>Total Amount Redeemed: <strong> <?php echo number_format(($totals['amt_redeem'] * $totals['exchangeRate']), 2); ?></strong></span></td>
                        </tr>
                    </table>


                </div>

                <table class="pure-table">
                    <thead>
                        <tr>
                            <th width="50"><a href="{{ Filter::baseUrl('sort=id') }}">ID</a></th>
                            <th>Date &amp; Time</th>
                            <th>Store</th>
                            <th>Amount Rewarded</th>
                            <th>Amount Redeemed</th>
                            <th>Amount Paid (<?php echo $totals['currencyShortCode']; ?>)</th>
                        </tr>
                    </thead>
                    <tbody>
                    @if(count($results) > 0)
                        @foreach($results as $t)
                            <tr>
                                <td width="50">{{ $t->id }}</td>
                                <td>
                                    {{ $t->created_at }}
                                </td>
                                <td>
                                    @if($t->store)
                                        {{ $t->store->name }}
                                    @else
                                        <i>N/A</i>
                                    @endif
                                </td>
                                <td>
                                    @if($t->points_rewarded)
                                        <?php echo number_format(($t->amt_reward * $totals['exchangeRate']), 2); ?>
                                    @else
                                        0
                                    @endif
                                </td>
                                <td>
                                    @if($t->points_redeemed)
                                         <?php echo number_format(($t->amt_redeem * $totals['exchangeRate']), 2) ?>
                                    @else
                                        0
                                    @endif
                                </td>
                                <td>
                                    @if($t->total_amount || $t->original_total_amount)
										@if($totals['currency'] == $t->currency_id && $t->original_total_amount  != 0)
										
                                                                                        <?php echo number_format($t->original_total_amount  , 2); ?>
										@else
                                                                                  <?php echo  number_format(($t->total_amount * $totals['exchangeRate'])  , 2); ?>
										
										@endif
									@else
										0.00
									@endif
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="7">No Report Data Found</td>
                        </tr>
                    @endif
                    </tbody>
                </table>

                {{ $results->appends(Input::except('page'))->links('vendor.pagination.custom-report') }}
            </div>
        </div>

    </div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-reporting');
    </script>
@stop