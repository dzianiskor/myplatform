@extends('layouts.dashboard')

@section('title')
    {{ $report->name }} | BLU
@stop

@section('body')
<div class="padding">
		<input type="hidden" name="loaded" id="loaded" value="{{ count($results) }}"/>
        <div class="content-head pure-g">
            <div class="title pure-u-1-2">
                <h1>
                    <i class="fa fa-bar-chart-o fa-fw"></i> {{ $report->name }}
                </h1>
            </div>
            <div class="controls pure-u-1-2">
                @include('report.partials.controls', $report)
            </div>
        </div>

        <div class="report-info">
            <i class="fa fa-info-circle"></i> {{ $report->description }}
        </div>

		<div class="pure-g">
			  <div class="pure-u-1-5 report-controls">
				  <div class="padding">

					  <h4>Report Settings</h4>

					  {{--- Custom Report Controls ---}}
					  {{ Form::open(array('class' => "pure-form pure-form-stacked", 'method' => "GET")) }}
						  @include('report.partials.period', $defaults)
						{{--	@include('report.partials.travel', $defaults)--}}
						{{--	@include('report.partials.periodpreset', $defaults)--}}
						{{--	@include('report.partials.bypartner', $defaults) --}}
								@include('report.partials.currency', $defaults)
						  @include('report.partials.suppliers', $defaults)
						{{--	@include('report.partials.partners', $defaults)--}}
						  @include('report.partials.submit')
					  {{ Form::close() }}
					  {{--- /Custom Report Controls ---}}
					  <input type="hidden" name="report_number" id="report_number" value="31"/>
				  </div>
			  </div>

		<div class="pure-u-4-5">
			<div class="table-totals">

                    <table style="width: 100%; border-style: none; border-width: 0px;">
                        <tr>
                            <td><span>Total Quantity: <strong><?php echo number_format($totals['qty']);  ?> </strong></span></td>
                        </tr>
                        <tr>
                            <td><span>Total Retail Price: <strong><?php echo number_format($totals['retailPrice'],2); ?> </strong></span></td>
                        </tr>
                    </table>


                </div>
			<table class="pure-table">
				<thead>
					<tr>
						<th>Supplier</th>
						<th>Item</th>
                        <th>Quantity</th>
						<th>Retail Price</th>
					</tr>
				</thead>
				<tbody>
					 @if(count($results) > 0)

						@foreach($results as $key => $resMa)
                                                    <tr>
                                                            <td>
															{{ App\Supplier::find($resMa->supplierId)->name }}
                                                            </td>
                                                            <td>
															{{ $resMa->name }}
                                                            </td>
                                                            <td>
															{{ $resMa->quantity }}
                                                            </td>
                                                            <td>
															{{ $resMa->retailPrice * $resMa->quantity }}
                                                            </td>
                                                    </tr>
						@endforeach
						@else
                        <tr>
                            <td colspan="6">No Report Data Found</td>
                        </tr>
					 @endif
				</tbody>
			</table>
		</div>
			</div>
	  </div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-reporting');
    </script>
@stop