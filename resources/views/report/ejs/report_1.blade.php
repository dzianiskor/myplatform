@extends('layouts.dashboard')

@section('title')
    {{ $report->name }} | BLU
@stop

@section('body')
    <div class="padding">
        <input type="hidden" name="loaded" id="loaded" value="{{ count($results) }}"/>
        <div class="content-head pure-g">
            <div class="title pure-u-1-2">
                <h1>
                    <i class="fa fa-bar-chart-o fa-fw"></i> {{ $report->name }}
                </h1>
            </div>
            <div class="controls pure-u-1-2">
                @include('report.partials.controls', $report)
            </div>
        </div>

        <div class="report-info">
            <i class="fa fa-info-circle"></i> {{ $report->description }}
        </div>

        <div class="pure-g">
            <div class="pure-u-1-5 report-controls">
                <div class="padding">

                    <h4>Report Settings</h4>

                    {{--- Custom Report Controls ---}}
                    {{ Form::open(array('class' => "pure-form pure-form-stacked", 'method' => "GET")) }}

                    @include('report.partials.period', $defaults)

                    @include('report.partials.currency', $defaults)

                    @include('report.partials.networkcurrency', $defaults)

                    <div class="js-partner-container">
                        @include('report.partials.stores', $defaults)
                    </div>

                    @include('report.partials.submit')

                    {{ Form::close() }}
                    {{--- /Custom Report Controls ---}}

                </div>
            </div>
            <div class="pure-u-4-5">

                <div class="table-totals">
                    <table style="width: 100%; border-style: none; border-width: 0px;" id="totals">
                        <tr>
                            <td><span>Number of Reward Transactions: <strong id="trx_reward">@if (isset($results)) Loading... @else 0 @endif</strong></span></td>
                            <td><span>Number of Redeem Transactions: <strong id="trx_redeem">@if (isset($results)) Loading... @else 0 @endif</strong></span></td>
                        </tr>
                        <tr>
                            <td><span>Total Points Rewarded: <strong id="pts_reward">@if (isset($results)) Loading... @else 0.00 @endif</strong></span></td>
                            <td><span>Total Points Redeemed: <strong id="pts_redeem">@if (isset($results)) Loading... @else 0 @endif</strong></span></td>
                        </tr>
                        <tr>
                            <td><span>Total Amount Rewarded: <strong id="amt_reward">@if (isset($results)) Loading... @else 0.00 @endif</strong></span></td>
                            <td><span>Total Amount Redeemed: <strong id="amt_redeem">@if (isset($results)) Loading... @else 0.00 @endif</strong></span></td>
                        </tr>
                    </table>
                </div>

                <table class="pure-table report-table" id="datatable">
                    <thead>
                    <th width="50">ID</th>
                    <th>Date &amp; Time</th>
                    <th>Store</th>
                    <th>Points Rewarded</th>
                    <th>Points Redeemed</th>
                    <th>Value of Pts Rewarded</th>
                    <th>Value of Pts Reddemed</th>
                    <th>Trx Amount ({{ $totals['currencyShortCode'] }})</th>
                    </thead>
                    <tbody>
                    <tr>
                        <td colspan="8" class="text-center">No Report Data Found.</td>
                    </tr>
                    </tbody>
                </table>

            </div>
        </div>

    </div>
@stop

@section('page_script')
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
    <script>
        @if (isset($results))
        $(function(){
            $('#datatable').DataTable({
                "processing": true,
                "serverSide": true,
                "lengthChange": false,
                "ordering": false,
                "searching": false,
                "pageLength": 40,
                "info": false,
                "language": {
                    "paginate": {
                        "previous": "&lt;",
                        "next": "&gt;"
                    }
                },
                "ajax": "{{ route('api.reports.report1') }}?{!! request()->server('QUERY_STRING') !!}",
                "columns": [
                    { "data": "id" },
                    { "data": "created_at" },
                    {
                        "data": null,
                        "render": function ( data, type, row ) {
                            var store = '<i>N/A</i>';
                            if (row.partner_name != '') { store = row.partner_name; }
                            store += ' - ';
                            if (row.store_name != '') {
                                store += row.store_name;
                            } else {
                                store += '<i>N/A</i>';
                            }
                            return store;
                        }
                    },
                    { "data": "points_rewarded" },
                    { "data": "points_redeemed" },
                    { "data": "amt_reward" },
                    { "data": "amt_redeem" },
                    {
                        "data": null,
                        "render": function (data, type, row) {
                            return parseFloat(row.trx_amount).toFixed(2);
                        }
                    }
                ]
            });

            $.ajax({
                url: '{{ route('api.reports.report1totals') }}?exchange_rate={{ $totals['exchangeRate'] }}&{!! request()->server('QUERY_STRING') !!}',
                context: $('#totals'),
            }).done(function(data){
                $('strong', $(this)).each(function(){
                    $(this).text(data[$(this).attr('id')]);
                });
            });
        });
        @endif

        $(document).on('change', '#network_currency', function () {
            $.ajax({
                url: '{{ route('api.reports.partials.stores') }}?network_id=' + $(this).val() + '&{!! request()->server('QUERY_STRING') !!}'
            }).done(function(data){
                $('.js-partner-container').html(data);
            });
        });
        $('#network_currency').trigger('change');

        set_menu('mnu-reporting');
    </script>
@stop
