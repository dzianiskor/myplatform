@extends('layouts.dashboard')

@section('title')
    {{ $report->name }} | BLU
@stop

@section('body')
    <div class="padding">

        <div class="content-head pure-g">
            <div class="title pure-u-1-2">
                <h1>
                    <i class="fa fa-bar-chart-o fa-fw"></i> {{ $report->name }}
                </h1>
            </div>
            <div class="controls pure-u-1-2">
                @include('report.partials.controls', $report)
            </div>
        </div>

        <div class="report-info">
            <i class="fa fa-info-circle"></i> {{ $report->description }}
        </div>

        <div class="pure-g">
            <div class="pure-u-1-5 report-controls">
                <div class="padding">

                    <h4>Report Settings</h4>

                    {{--- Custom Report Controls ---}}
                    {{ Form::open(array('class' => "pure-form pure-form-stacked", 'method' => "GET")) }}
                        {{--- @include('report.partials.members', $defaults) ---}}
			{{--- @include('report.partials.networkcurrency', $defaults) ---}}
                        {{--- @include('report.partials.partners', $defaults) ---}}
                        @include('report.partials.networks', $defaults)

                        @include('report.partials.submit')

                    {{ Form::close() }}
                    {{--- /Custom Report Controls ---}}

                </div>
            </div>
            <div class="pure-u-4-5">



                <table class="pure-table">
                    <thead>
                        <tr>

                            <th>User ID</th>
                            <th>Member</th>
                            <th>Network</th>
                            <th>Balance</th>
                            <th>Points Rewarded - Points Redeemed</th>
                            <th>Discrepancy </th>

                        </tr>
                    </thead>
                    <tbody>
                    
                    @if(count($results) > 0)
                        @foreach($results as $key => $result)
                            @foreach($result as $res)
                            <?php
                                $user   = App\User::find($res['user_id']);
                                
                                if(!$user || $res['discrepancy'] == 0){
                                    continue;
                                }
                                
                                $userName   = $user->first_name . ' ' . $user->last_name;
                                
                                   ?>
                                <tr>
                                    <td>{{ $res['user_id'] }}</td>
                                    <td>
                                        @if($userName != 'na na')
                                            {{ $userName }} 
                                        @else
                                            <i>N/A</i>
                                        @endif
                                    </td>
                                    <td>{{ $res['network'] }}</td>
                                    <td>{{ $res['balance'] }}</td>
                                    <td>{{ $res['trx_balance'] }}</td>
                                    <td>{{ $res['discrepancy'] }}</td>
                                </tr>
                            @endforeach
                        @endforeach
                    @else
                        <tr>
                            <td colspan="6">No Report Data Found</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
<!--                <ul  class="pagination">
                    <li><a href="<?php echo Request::fullUrl() . '&page=' . 1; ?>"><</a></li>
                <?php 
                $path = Request::fullUrl();
                for($i=1; $i<$numberOfPages+1; $i++){ ?>
                <li><a href="<?php echo Request::fullUrl() . '&page=' . $i; ?>" ><?php echo $i; ?></a></li>
               <?php } ?>
                <li><a href="<?php echo Request::fullUrl() . '&page=' . $numberOfPages; ?>">></a></li>
                </ul>-->
            </div>
        </div>

    </div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-reporting');
    </script>
@stop