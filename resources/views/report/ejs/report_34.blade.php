@extends('layouts.dashboard')

@section('title')
    {{ $report->name }} | BLU
@stop

@section('body')
    <div class="padding">

        <div class="content-head pure-g">
            <div class="title pure-u-1-2">
                <h1>
                    <i class="fa fa-bar-chart-o fa-fw"></i> {{ $report->name }}
                </h1>
            </div>
            <div class="controls pure-u-1-2">
                @include('report.partials.controls', $report)
            </div>
        </div>

        <div class="report-info">
            <i class="fa fa-info-circle"></i> {{ $report->description }}
        </div>

        <div class="pure-g">
            <div class="pure-u-1-5 report-controls">
                <div class="padding">

                    <h4>Report Settings</h4>

                    {{-- Custom Report Controls --}}
                    {{ Form::open(array('class' => "pure-form pure-form-stacked", 'method' => "GET")) }}

                    @include('report.partials.period', $defaults)

                    @include('report.partials.currency', $defaults)
                    @include('report.partials.networks', $defaults)
                    @include('report.partials.redemptioncategories', $defaults)

                    @include('report.partials.partners', $defaults)

                    @include('report.partials.submit')

                    {{ Form::close() }}
                    {{-- /Custom Report Controls --}}

                </div>
            </div>
            <div class="pure-u-4-5">
                <div class="table-totals">
                    <table style="width: 100%; border-style: none; border-width: 0px;">
                        <tr>
                            <td>
                                <span>Total Points Redeemed : <strong>{{ number_format($tot['points']) }}</strong></span>
                            </td>
                            <td>
                                <span>Total Amount Redeemed (for overall report): <strong>{{ number_format($tot['amount'], 2) }}</strong></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span>Total Transactions : <strong><?php echo number_format($tot['total_trxs']);?></strong></span>
                            </td>
                            <td>
                                <span>Total Cash Payment : <strong><?php echo number_format($tot['total_cash_payment'], 2);?></strong></span>
                            </td>
                        </tr>
                    </table>
                </div>

                <table class="pure-table">
                    <thead>
                    <tr>
                        <th>Category</th>
                        <th>Total Trxs</th>
                        <th>Total Pts Redeemed</th>
                        <th>Total Cash Paid</th>
                        <th>Total Amount Redeemed</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(count($results) > 0)

                        <tr>
                            <td>
                                Cashback
                            </td>
                            <td>
                                <?php echo number_format($results['cashback']['total_trx']);?>

                            </td>
                            <td>
                                <?php echo number_format($results['cashback']['points']);?>

                            </td>
                            <td>
                                <?php echo number_format($results['cashback']['cash_payment']);?>

                            </td>
                            <td>
                                <?php echo number_format($results['cashback']['amount'], 2);?>

                            </td>
                        </tr>
                        <tr>
                            <td>
                                Flights
                            </td>
                            <td>
                                <?php echo number_format($results['flight']['total_trx']);?>

                            </td>
                            <td>
                                <?php echo number_format($results['flight']['points']);?>

                            </td>
                            <td>
                                <?php echo number_format($results['flight']['cash_payment']);?>

                            </td>
                            <td>
                                <?php echo number_format($results['flight']['amount'], 2);?>

                            </td>
                        </tr>
                        <tr>
                            <td>
                                Hotels
                            </td>
                            <td>
                                <?php echo number_format($results['hotel']['total_trx']);?>

                            </td>
                            <td>
                                <?php echo number_format($results['hotel']['points']);?>

                            </td>
                            <td>
                                <?php echo number_format($results['hotel']['cash_payment'], 2);?>

                            </td>
                            <td>
                                <?php echo number_format($results['hotel']['amount'], 2);?>

                            </td>
                        </tr>
                        <tr>
                            <td>
                                Cars
                            </td>
                            <td>
                                <?php echo number_format($results['car']['total_trx']);?>

                            </td>
                            <td>
                                <?php echo number_format($results['car']['points']);?>

                            </td>
                            <td>
                                <?php echo number_format($results['car']['cash_payment'], 2);?>

                            </td>
                            <td>
                                <?php echo number_format($results['car']['amount'], 2);?>

                            </td>
                        </tr>
                        <tr>
                            <td>
                                Items at Suppliers
                            </td>
                            <td>
                                <?php echo number_format($results['oth_supp']['total_trx']);?>

                            </td>
                            <td>
                                <?php echo number_format($results['oth_supp']['points']);?>

                            </td>
                            <td>
                                <?php echo number_format($results['oth_supp']['cash_payment'], 2);?>

                            </td>
                            <?php echo number_format($results['oth_supp']['amount'], 2);?>
                            <td>

                        </tr>
                        <tr>
                            <td>
                                Items at selected partners
                            </td>
                            <td>
                                <?php echo number_format($results['part_supp']['total_trx']);?>

                            </td>
                            <td>
                                <?php echo number_format($results['part_supp']['points']);?>

                            </td>
                            <td>
                                <?php echo number_format($results['part_supp']['cash_payment']);?>

                            </td>
                            <td>
                                <?php echo number_format($results['part_supp']['amount'], 2);?>

                            </td>
                        </tr>
                        <tr>
                            <td>
                                Transfers
                            </td>
                            <td>
                                <?php echo number_format($results['transfer']['total_trx']);?>

                            </td>
                            <td>
                                <?php echo number_format($results['transfer']['points']);?>

                            </td>
                            <td>
                                <?php echo number_format($results['transfer']['cash_payment']);?>

                            </td>
                            <td>
                                <?php echo number_format($results['transfer']['amount'], 2);?>

                            </td>
                        </tr>
                    @else
                        <tr>
                            <td colspan="4">No Report Data Found</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
        </div>

    </div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-reporting');
    </script>
@stop
