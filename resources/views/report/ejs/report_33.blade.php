@extends('layouts.dashboard')

@section('title')
    {{ $report->name }} | BLU
@stop

@section('body')
    <div class="padding">

        <div class="content-head pure-g">
            <div class="title pure-u-1-2">
                <h1>
                    <i class="fa fa-bar-chart-o fa-fw"></i> {{ $report->name }}
                </h1>
            </div>
            <div class="controls pure-u-1-2">
                @include('report.partials.controls', $report)
            </div>
        </div>

        <div class="report-info">
            <i class="fa fa-info-circle"></i> {{ $report->description }}
        </div>

        <div class="pure-g">
            <div class="pure-u-1-5 report-controls">
                <div class="padding">

                    <h4>Report Settings</h4>

                    {{--- Custom Report Controls ---}}
                    {{ Form::open(array('class' => "pure-form pure-form-stacked", 'method' => "GET")) }}

                        @include('report.partials.period', $defaults)

                        @include('report.partials.partners', $defaults)

                        @include('report.partials.submit')

                    {{ Form::close() }}
                    {{--- /Custom Report Controls ---}}

                </div>
            </div>
            <div class="pure-u-4-5">

                <div class="table-totals">

                    <table style="width: 100%; border-style: none; border-width: 0px;">
                        <tr>

                            <td><span>Total Points Rewarded: <strong>{{ number_format($tot['pts_reward']); }}</strong></span></td>
                        </tr>
                        <tr>

                            <td><span>Total Points Redeemed: <strong>{{ number_format($tot['amt_reward'],2); }}</strong></span></td>
                        </tr>
                    </table>


                </div>

                <table class="pure-table">
                    <thead>
                        <tr>
                            <th>User ID</th>
                            <th>Member Name</th>
							<th>Points Rewarded</th>
                            <th>Points Redeemed</th>
							<th>Outstanding Balance</th>
                        </tr>
                    </thead>
                    <tbody>
                    @if(count($results) > 0)
                        @foreach($results as $t)
                            <tr>
                                <td>
                                    <!--{{ App\classes\ReportHelper::chartDateString($defaults) }}-->
                                    {{ $t->user_id }}
                                </td>
                                <td>
                                    <?php
                                    $admin_partner = Auth::User()->getTopLevelPartner();
                                    $has_middleware = false;
                                    $user_info = array();
                                    if($admin_partner->has_middleware == '1'){

                                        $user_ids_used = array();
                                        $has_middleware = true;
                                        $user_ids_used[] = $t->user_id;
                                        $term = "";
                                        $url = $admin_partner->link_middleware_api . '?method=searchSensitive&api_key='.$admin_partner->middleware_api_key;
                                        $json_ids = json_encode($user_ids_used);
                                        $url .= "&user_ids=" . $json_ids . "&term='" . $term . "'";
                                        $curl = curl_init();
                                        // Set some options - we are passing in a useragent too here
                                        curl_setopt_array($curl, array(
                                            CURLOPT_RETURNTRANSFER => 1,
                                            CURLOPT_URL => $url,
                                            CURLOPT_USERAGENT => 'Testing Sensitive Info'
                                        ));
                                        // Send the request & save response to $resp
                                        $resp = curl_exec($curl);
                                        
                                        $resp_curl = json_decode($resp);

                                        $Users_info = $resp_curl;
                                        if(!empty($Users_info)){
                                        foreach($Users_info as $member) {
                                            if(in_array($member->blu_id,$user_ids_used)){
                                                $user_info['first_name'] = $member->first_name;
                                                $user_info['last_name'] = $member->last_name;

                                            }
                                        }
                                        }else{
                                           $user_info['first_name'] = 'N/A';
                                           $user_info['last_name'] = 'N/A'; 
                                        }
                                    }
                                    else{
                                        $user = App\User::find($t->user_id);
                                        $userName   = $user->first_name . " " . $user->last_name;
                                    }
                                    ?>
                                    
                                    @if($has_middleware)
                                        {{$user_info['first_name']}} {{$user_info['last_name']}} 
                                    @elseif(isset($userName) && $userName != "na na")
                                        {{$userName}}
                                    @else
                                        N/A
                                    @endif
                                </td>
								<td>
                                    {{ number_format($t->points_rewarded) }}
                                </td>
                                <td>
                                    {{ number_format($t->points_used) }}
                                </td>
								<td>
                                    {{ number_format($t->points_rewarded - $t->points_used) }}
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="4">No Report Data Found</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
        </div>

    </div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-reporting');
    </script>
@stop