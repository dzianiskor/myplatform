@extends('layouts.dashboard')

@section('title')
    {{ $report->name }} | BLU
@stop

@section('body')

    <div class="padding">
        <div class="content-head pure-g">
            <div class="title pure-u-1-2">
                <h1>
                    <i class="fa fa-bar-chart-o fa-fw"></i> {{ $report->name }}
                </h1>
            </div>
            <div class="controls pure-u-1-2">
                @include('report.partials.controls', $report)
            </div>
        </div>

        <div class="report-info">
            <i class="fa fa-info-circle"></i> {{ $report->description }}
        </div>

        <div class="pure-g">
            <div class="pure-u-1-5 report-controls">
                <div class="padding">

                    <h4>Report Settings</h4>

                    {{-- Custom Report Controls --}}
                    {{ Form::open(array('class' => "pure-form pure-form-stacked", 'method' => "GET")) }}

                    @include('report.partials.period', $defaults)

                    @include('report.partials.currency', $defaults)

                    @include('report.partials.membersautocomplete-separated', $defaults)

                    @include('report.partials.networks2', $defaults)

                    <div class="js-partner-container">
                        @include('report.partials.partners', $defaults)
                    </div>

                    @include('report.partials.submit')
                    <input type="hidden" name="report_number" id="report_number" value="39"/>
                    {{ Form::close() }}
                    {{-- /Custom Report Controls --}}

                </div>
            </div>

            <div class="pure-u-4-5">
                <div class="table-totals">
                    <table style="width: 100%; border-style: none; border-width: 0px;" id="totals">
                        <tr>
                            <td><span>Number of Reward Transactions: <strong id="nb_of_rewards">@if (isset($results)) Loading... @else 0 @endif</strong></span></td>
                        </tr>
                        <tr>
                            <td><span>Total Points Rewarded: <strong id="pts_reward">@if (isset($results)) Loading... @else 0.00 @endif</strong></span></td>
                            <td><span>Total Amount Rewarded: <strong>{{ $totals['currencyShortCode'] }}</strong> <strong id="amount_reward">@if (isset($results)) Loading... @else 0.00 @endif</strong></span></td>
                        </tr>
                    </table>

                </div>
                <table class="pure-table" id="datatable">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Reference</th>
                        <th>Member</th>
                        <th>Date</th>
                        <th>Store</th>
                        <th>Loyalty</th>
                        <th>Event Name</th>
                        <th>Pts Rewarded</th>
                        <th>Amount Spent (<?php echo $totals['currencyShortCode']; ?>)</th>
                        <th>Original Amount</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td colspan="11" class="text-center">No Report Data Found.</td>
                    </tr>
                    </tbody>
                </table>

            </div>
        </div>
    </div>
@stop

@section('page_script')
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
    <script>

        @if (isset($results))
        $(function(){
            $('#datatable').DataTable({
                "processing": true,
                "serverSide": true,
                "lengthChange": false,
                "bAutoWidth": false,
                "ordering": false,
                "searching": false,
                "pageLength": 40,
                "info": false,
                "language": {
                    "paginate": {
                        "previous": "&lt;",
                        "next": "&gt;"
                    }
                },
                "ajax": "{{ route('api.reports.report39') }}?partner_id={{ $totals['partner_id'] }}&{!! request()->server('QUERY_STRING') !!}",
                "columns": [
                    { "data": "id" },
                    {
                        "data": null,
                        "render": function ( data, type, row ) {
                            if (row.reference_number) {
                                return row.reference_number;
                            } else {
                                return row.reference_parent_number;
                            }

                            return 'N/A';
                        }
                    },
                    {
                        "data": null,
                        "render": function ( data, type, row ) {
                            var name = row.user_first_name + ' ' + row.user_last_name;
                            if (name == 'na na' || name == ' ')
                                return 'N/A';
                            return name;
                        }
                    },
                    { "data": "created_at_date" },
                    {
                        "data": null,
                        "render": function ( data, type, row ) {
                            if (row.store_name != '') {
                                return row.store_name;
                            }
                            return '<i>N/A</i>';
                        }
                    },
                    {
                        "data": null,
                        "render": function ( data, type, row ) {
                            if (row.loyalty_name != '') {
                                return row.loyalty_name;
                            }
                            return '<i>N/A</i>';
                        }
                    },
                    {
                        "data": null,
                        "render": function ( data, type, row ) {
                            if (row.product_name != '') {
                                return row.product_name;
                            }
                            return '<i>N/A</i>';
                        }
                    },
                    { "data": "points_rewarded" },
                    {
                        "data": null,
                        "render": function ( data, type, row ) {
                            var trx_amount = row.total_amount * {{ $totals['exchangeRate'] }};
                            return parseFloat(trx_amount).toFixed(2);
                        }
                    },
                    {
                        "data": null,
                        "render": function (data, type, row) {
                            if (row.total_amount || row.original_total_amount) {
                                if (row.original_total_amount != 0) {
                                    var trx_amount = row.original_total_amount;
                                }
                                else {
                                    var trx_amount = row.total_amount * {{ $totals['exchangeRate'] }};
                                }
                                return row.tcurrency_short_code + ' ' + parseFloat(trx_amount).toFixed(2);
                            }
                            return '0.00';
                        }
                    }
                ]
            });

            $.ajax({
                url: '{{ route('api.reports.report39totals') }}?exchange_rate={{ $totals['exchangeRate'] }}&{!! request()->server('QUERY_STRING') !!}',
                context: $('#totals'),
            }).done(function(data){
                $('strong', $(this)).each(function(){
                    $(this).text(data[$(this).attr('id')]);
                });
            });
        });
        @endif

        $(document).on('change', '#network_ids', function () {
            $.ajax({
                url: '{{ route('api.reports.partials.partners') }}?network_id=' + $(this).val() + '&{!! request()->server('QUERY_STRING') !!}'
            }).done(function(data){
                $('.js-partner-container').html(data);
            });
        });
        $('#network_ids').trigger('change');

        set_menu('mnu-reporting');
    </script>
@stop
