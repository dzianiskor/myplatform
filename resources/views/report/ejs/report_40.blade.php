@extends('layouts.dashboard')

@section('title')
    {{ $report->name }} | BLU
@stop

@section('body')

    <div class="padding">
        <div class="content-head pure-g">
            <div class="title pure-u-1-2">
                <h1>
                    <i class="fa fa-bar-chart-o fa-fw"></i> {{ $report->name }}
                </h1>
            </div>
            <div class="controls pure-u-1-2">
                @include('report.partials.controls', $report)
            </div>
        </div>

        <div class="report-info">
            <i class="fa fa-info-circle"></i> {{ $report->description }}
        </div>

        <div class="pure-g">
            <div class="pure-u-1-5 report-controls">
                <div class="padding">

                    <h4>Report Settings</h4>

                    {{-- Custom Report Controls --}}
                    {{ Form::open(array('class' => "pure-form pure-form-stacked", 'method' => "GET")) }}

                    @include('report.partials.period', $defaults)

                    @include('report.partials.networkcurrency', $defaults)

                    @include('report.partials.membersautocomplete-separated', $defaults)

                    <div class="js-partner-container">
                        @include('report.partials.partners', $defaults)
                    </div>

                    @include('report.partials.submit')
                    <input type="hidden" name="report_number" id="report_number" value="40"/>
                    {{ Form::close() }}
                    {{-- /Custom Report Controls --}}

                </div>
            </div>
            <div class="pure-u-4-5">

                <div class="table-totals">
                    <table style="width: 100%; border-style: none; border-width: 0px;" id="totals">
                        <tr>
                            <td><span>Total Points Redeemed: <strong id="total_points_redeemed">@if (isset($results)) Loading... @else 0 @endif</strong></span></td>
                            <td><span>Total Delivery Charges (in Pts): <strong id="total_delivery_cost">@if (isset($results)) Loading... @else 0 @endif</strong></span></td>
                        </tr>
                        <tr>
                            <td><span>Total Cash Paid: <strong id="total_cash_payment">@if (isset($results)) Loading... @else 0.00 @endif</strong></span></td>
                        </tr>
                    </table>
                </div>

                <table class="pure-table" id="datatable">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Reference</th>
                        <th>Member</th>
                        <th>Mobile</th>
                        <th>Date</th>
                        <th>Category</th>
                        <th>Item</th>
                        <th>Quantity</th>
                        <th>Pts Redeemed</th>
                        <th>Delivery Charges</th>
                        <th>Cash Paid</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td colspan="11" class="text-center">No Report Data Found.</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>

    </div>
@stop

@section('page_script')
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
    <script>
        @if (isset($results))
        $(function(){
            $('#datatable').DataTable({
                "processing": true,
                "serverSide": true,
                "lengthChange": false,
                "ordering": false,
                "searching": false,
                "pageLength": 40,
                "info": false,
                "language": {
                    "paginate": {
                        "previous": "&lt;",
                        "next": "&gt;"
                    }
                },
                "ajax": "{{ route('api.reports.report40') }}?{!! request()->server('QUERY_STRING') !!}",
                "columns": [
                    { "data": "trx_id" },
                    { "data": "refID" },
                    { "data": "name" },
                    { "data": "normalized_mobile" },
                    { "data": "date" },
                    { "data": "category" },
                    { "data": "itemName" },
                    { "data": "quantity" },
                    { "data": "ptsRedeemed" },
                    { "data": "delivery_cost" },
                    { "data": "cash_payment" }
                ]
            });

            $.ajax({
                url: '{{ route('api.reports.report40totals') }}?{!! request()->server('QUERY_STRING') !!}',
                context: $('#totals'),
            }).done(function(data){
                $('strong', $(this)).each(function(){
                    $(this).text(data[$(this).attr('id')]);
                });
            });
        });
        @endif

        $(document).on('change', '#network_currency', function () {
            $.ajax({
                url: '{{ route('api.reports.partials.partners') }}?network_id=' + $(this).val() + '&{!! request()->server('QUERY_STRING') !!}'
            }).done(function(data){
                $('.js-partner-container').html(data);
            });
        });
        $('#network_currency').trigger('change');

        set_menu('mnu-reporting');
    </script>
@stop
