@extends('layouts.dashboard')

@section('title')
    {{ $report->name }} | BLU
@stop

@section('body')

    <div class="padding">
		<input type="hidden" name="loaded" id="loaded" value="{{ $results->count() }}"/>
        <div class="content-head pure-g">
            <div class="title pure-u-1-2">
                <h1>
                    <i class="fa fa-bar-chart-o fa-fw"></i> {{ $report->name }}
                </h1>
            </div>
            <div class="controls pure-u-1-2">
                @include('report.partials.controls', $report)
            </div>
        </div>

        <div class="report-info">
            <i class="fa fa-info-circle"></i> {{ $report->description }}
        </div>

        <div class="pure-g">
            <div class="pure-u-1-5 report-controls">
                <div class="padding">

                    <h4>Report Settings</h4>

                    {{--- Custom Report Controls ---}}
                    {{ Form::open(array('class' => "pure-form pure-form-stacked", 'method' => "GET")) }}

                        @include('report.partials.period', $defaults)

                        @include('report.partials.membersautocomplete-separated', $defaults)
                        
                        @include('report.partials.stores_part', $defaults)

                        @include('report.partials.submit')
                        <input type="hidden" name="report_number" id="report_number" value="42"/>
                    {{ Form::close() }}
                    {{--- /Custom Report Controls ---}}

                </div>
            </div>
            <div class="pure-u-4-5">
                <table class="pure-table">
                    <thead>
                        <tr>
                            <th>Registration Date</th>
                            <th>Registration Source</th>
                            <th>Reference</th>
                            <th>Member</th>
                            <th>Mobile</th>
                            <th>First Time Login</th>
                            <th>First Time Login Date</th>
                            <th>Channel</th>
                            <th>First Transaction Posted</th>
                        </tr>
                    </thead>
                    <tbody>
                    
                    @if($results->count() > 0)
                        @foreach($results as $user)
                            <tr>
                                <td>{{ $user->registration_date }}</td>
                                <td>
                                    {{ $user->pos_name }}
                                </td>
                                <td>
                                    {{ $user->ref_number ?? 'N/A' }}
                                </td>
                                <td>
                                    {{ $user->member_name }}
                                </td>
                                <td>
                                    {{ $user->normalized_mobile }}
                                </td>
                                <td>
                                    {{ $user->first_login }}
                                </td>
                                <td>
                                    {{ $user->first_login_date }}
                                </td>
                                <td>
                                    {{ $user->first_login_channel }}
                                </td>
                                <td>
                                    {{ ($transactionDate = $user->first_transaction_date ?? null) ? \Carbon\Carbon::parse($transactionDate)->format('Y-m-d H:i:s') : 'N/A' }}
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="9">No Report Data Found</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
                {{ $results->appends(Input::except('page'))->links('vendor.pagination.custom-report') }}
            </div>
        </div>
    </div>
@stop
@section('page_script')
    <script>
        set_menu('mnu-reporting');
    </script>
@stop