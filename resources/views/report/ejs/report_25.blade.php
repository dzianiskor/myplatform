@extends('layouts.dashboard')

@section('title')
    {{ $report->name }} | BLU
@stop

@section('body')
    <div class="padding">

        <div class="content-head pure-g">
            <div class="title pure-u-1-2">
                <h1>
                    <i class="fa fa-bar-chart-o fa-fw"></i> {{ $report->name }}
                </h1>
            </div>
            <div class="controls pure-u-1-2">
                @include('report.partials.controls', $report)
            </div>
        </div>

        <div class="report-info">
            <i class="fa fa-info-circle"></i> {{ $report->description }}
        </div>

        <div class="pure-g">
            <div class="pure-u-1-5 report-controls">
                <div class="padding">

                    <h4>Report Settings</h4>

                    {{-- Custom Report Controls --}}
                    {{ Form::open(array('class' => "pure-form pure-form-stacked", 'method' => "GET")) }}
                        @include('report.partials.period', $defaults)
                        @include('report.partials.periodpreset', $defaults)
                        @include('report.partials.bypartner', $defaults)
                        @include('report.partials.networks', $defaults)
                        {{ Form::hidden('user_id', Auth::id()) }}
                        @include('report.partials.submit')
                    {{ Form::close() }}
                    {{-- /Custom Report Controls --}}

                </div>
            </div>
            <div class="pure-u-4-5">

                <div class="table-totals">
                    <table style="width: 100%; border-style: none; border-width: 0px;" id="totals">
                        <tr>
                            <td><span>Number of Reward Transactions:
                                    <strong id="num_reward_trx">@if (isset($results)) Loading... @else N/A @endif</strong></span></td>
                            <td><span>Number of Redeem Transactions:
                                    <strong id="num_redeem_trx">@if (isset($results)) Loading... @else N/A @endif</strong></span></td>
                        </tr>
                    </table>
                </div>

                <table class="pure-table" id="datatable">
                    <thead>
                        <th>Date</th>
                        <th>Network</th>
                        <th>Pts rewarded</th>
                        <th>reward trxs</th>
                        <th>pts redeemed</th>
                        <th>redemption trxs</th>
                        <th>Net Pts Activity</th>
                    </thead>
                    <tbody>
                    <tr>
                        <td colspan="7" class="text-center">No Report Data Found.</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop

@section('page_script')
    <script>
        @if (isset($results))
            $(function(){
                $('#datatable').DataTable({
                    "processing": true,
                    "serverSide": true,
                    "lengthChange": false,
                    "ordering": false,
                    "searching": false,
                    "pageLength": 50,
                    "paging": true,
                    "info": false,
                    "language": {
                        "paginate": {
                            "previous": "&lt;",
                            "next": "&gt;"
                        }
                    },
                    "ajax": "{{ route('api.reports.report25') }}?{!! request()->server('QUERY_STRING') !!}",
                    "columns": [
                        { "data": "date" },
                        {
                            "data": null,
                            "render": function ( data, type, row ) {
                                if (!row.partner_name) {
                                    row.partner_name = '<i>N/A</i>'
                                }
                                if (!row.network_name) {
                                    row.network_name = '<i>N/A</i>'
                                }
                                return row.partner_name + ', ' + row.network_name;
                            }
                        },
                        { "data": "preward" },
                        { "data": "trx_rew" },
                        { "data": "predeem" },
                        { "data": "trx_red" },
                        { "data": "activity" }
                    ]
                });

                $.ajax({
                    url: '{{ route('api.reports.report25totals') }}?{!! request()->server('QUERY_STRING') !!}',
                    context: $('#totals'),
                }).done(function(data){
                    $('strong', $(this)).each(function(){
                        $(this).text(data[$(this).attr('id')]);
                    });
                });
            });
        @endif


        set_menu('mnu-reporting');
    </script>
@stop
