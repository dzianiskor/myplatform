@extends('layouts.dashboard')

@section('title')
    {{ $report->name }} | BLU
@stop

@section('body')
    <div class="padding">

        <div class="content-head pure-g">
            <div class="title pure-u-1-2">
                <h1>
                    <i class="fa fa-bar-chart-o fa-fw"></i> {{ $report->name }}
                </h1>
            </div>
            <div class="controls pure-u-1-2">
                @include('report.partials.controls', $report)
            </div>
        </div>

        <div class="report-info">
            <i class="fa fa-info-circle"></i> {{ $report->description }}
        </div>

        <div class="pure-g">
            <div class="pure-u-1-5 report-controls">
                <div class="padding">

                    <h4>Report Settings</h4>

                    {{-- Custom Report Controls --}}
                    {{ Form::open(array('class' => "pure-form pure-form-stacked", 'method' => "GET")) }}
                        @include('report.partials.period', $defaults)
                        @include('report.partials.periodpreset', $defaults)

                        @include('report.partials.networks', $defaults)
                        @include('report.partials.partners', $defaults)

                        @include('report.partials.submit')

                    {{ Form::close() }}
                    {{-- /Custom Report Controls --}}

                </div>
            </div>
            <div class="pure-u-4-5">

                <div class="table-totals">
                    <table style="width: 100%; border-style: none; border-width: 0px;" id="totals">
                        <tr>
                            <td><span>Number of Reward Transactions: <strong id="num_reward_trx">@if (isset($results)) Loading... @else 0 @endif</strong></span></td>
                            <td><span>Number of Redeem Transactions: <strong id="num_redeem_trx">@if (isset($results)) Loading... @else 0 @endif</strong></span></td>
                        </tr>
                    </table>



                </div>

                <table class="pure-table" id="datatable">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Pts rewarded</th>
                            <th>Reward trxs</th>
                            <th>Pts redeemed</th>
                            <th>Redemption trxs</th>
                            <th>Net Pts Activity</th>
                        </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td colspan="7" class="text-center">No Report Data Found.</td>
                    </tr>
                    </tbody>
                </table>

            </div>
        </div>

    </div>
@stop

@section('page_script')
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
    <script>
        @if (isset($results))
        $(function(){
            $('#datatable').DataTable({
                "processing": true,
                "serverSide": true,
                "lengthChange": false,
                "ordering": false,
                "searching": false,
                "pageLength": 50,
                "paging": true,
                "info": false,
                "language": {
                    "paginate": {
                        "previous": "&lt;",
                        "next": "&gt;"
                    }
                },
                "ajax": "{{ route('api.reports.report22') }}?{!! request()->server('QUERY_STRING') !!}",
                "columns": [
                    { "data": "date" },
                    { "data": "preward" },
                    { "data": "trx_rew" },
                    { "data": "predeem" },
                    { "data": "trx_red" },
                    { "data": "activity" }
                ]
            });

            $.ajax({
                url: '{{ route('api.reports.report22totals') }}?{!! request()->server('QUERY_STRING') !!}',
                context: $('#totals'),
            }).done(function(data){
                $('strong', $(this)).each(function(){
                    $(this).text(data[$(this).attr('id')]);
                });
            });
        });
        @endif

        set_menu('mnu-reporting');
    </script>
@stop
