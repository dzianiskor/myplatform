@extends('layouts.dashboard')

@section('title')
    {{ $report->name }} | BLU
@stop

@section('body')
    <div class="padding">

        <div class="content-head pure-g">
            <div class="title pure-u-1-2">
                <h1>
                    <i class="fa fa-bar-chart-o fa-fw"></i> {{ $report->name }} 
                </h1>
            </div>
            <div class="controls pure-u-1-2">
                 @include('report.partials.controls', $report)
            </div>                        
        </div>

        <div class="report-info">
            <i class="fa fa-info-circle"></i> {{ $report->description }}
        </div>

        <div class="pure-g">
            <div class="pure-u-1-5 report-controls">
                <div class="padding">

                    <h4>Report Settings</h4>

                    {{--- Custom Report Controls ---}}
                    {{ Form::open(array('class' => "pure-form pure-form-stacked", 'method' => "GET")) }}
                    
                        @include('report.partials.networks',$defaults)
                        @include('report.partials.submit')

                    {{ Form::close() }}
                    {{--- /Custom Report Controls ---}}

                </div>
            </div>
            <div class="pure-u-4-5">

                <table class="pure-table report-table">
                    <thead>
                        <tr>
                            
                            <th>Current Balances</th>
                            <th>Points Rewarded</th>
                            <th>Reward TRXs</th>
                            <th>Points Redeemed</th>
                            <th>Redemption TRXs</th>
                            <th>Net Pts Activity</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                    @if(count($results) > 0)
                        
                            <tr>
                                <td>
                                    <?php echo number_format($results['tot_balances']);?>
                            
                                </td>
                                <td>
                                      <?php echo number_format($results['tot_reward']);?>
                               
                                </td>
                                <td>
                                      <?php echo number_format($results['tot_num_trx_reward']);?>
                                   
                                </td>
                                <td>
                                      <?php echo number_format($results['tot_redeem']);?>
                                                       
                                </td>
                                <td>
                                      <?php echo number_format($results['tot_num_trx_redeem']); ?>
                                                
                                </td>
                                
                                <td>
                                      <?php echo number_format($results['tot_trx']); ?>
                                                     
                                </td>
                            </tr>                 
                        
                    @else
                        <tr>
                            <td colspan="7">No Report Data Found</td>
                        </tr>
                    @endif
                    </tbody>
                </table>     

                {{--- $results->appends(Input::except('page'))->links('vendor.pagination.custom-report') ---}}
            </div>
        </div>

    </div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-reporting');
    </script>
@stop
