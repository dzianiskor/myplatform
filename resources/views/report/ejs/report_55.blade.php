@extends('layouts.dashboard')

@section('title')
    {{ $report->name }} | BLU
@stop

@section('body')
    <div class="padding">
		<input type="hidden" name="loaded" id="loaded" value="{{ count($results) }}"/>
        <div class="content-head pure-g">
            <div class="title pure-u-1-2">
                <h1>
                    <i class="fa fa-bar-chart-o fa-fw"></i> {{ $report->name }}
                </h1>
            </div>
            <div class="controls pure-u-1-2">
                @include('report.partials.controls', $report)
            </div>
        </div>

        <div class="report-info">
            <i class="fa fa-info-circle"></i> {{ $report->description }}
        </div>

        <div class="pure-g">
            <div class="pure-u-1-5 report-controls">
                <div class="padding">

                    <h4>Report Settings</h4>

                    {{--- Custom Report Controls ---}}
                    {{ Form::open(array('class' => "pure-form pure-form-stacked", 'method' => "GET")) }}

                        @include('report.partials.period', $defaults)

                        @include('report.partials.membersautocomplete-separated', $defaults)

                        @include('report.partials.networks', $defaults)

                        @include('report.partials.partners', $defaults)

                        @include('report.partials.submit')

                    {{ Form::close() }}
                    {{--- /Custom Report Controls ---}}

                </div>
            </div>
            <div class="pure-u-4-5">

                <div class="table-totals">
                    <table style="width: 100%; border-style: none; border-width: 0px;" id="totals">
                        <tr>
                            <td><span>Number of Reward Transactions: <strong id="total_reward_transactions">@if (isset($results)) Loading... @else 0 @endif</strong></span></td>
                        </tr>
                        <tr>
                            <td><span>Total Points Rewarded: <strong id="total_points_rewarded">@if (isset($results)) Loading... @else 0 @endif</strong></span></td>
                        </tr>
                    </table>
                </div>

                <table class="pure-table report-table" id="datatable">
                    <thead>
                        <th width="50">ID</th>
                        <th>Reference</th>
                        <th>Member</th>
                        <th>Date &amp; Time</th>
                        <th>Description</th>
                        <th>Pts Rewarded</th>
                        <th>Amount of Pts Rewarded</th>
                        <th>Transaction Amount & Currency</th>
                        <th>Billing Amount & Currency</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan="9" class="text-center">No Report Data Found.</td>
                        </tr>
                    </tbody>
                </table>

            </div>
        </div>

    </div>
@stop

@section('page_script')
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
    <script>
        @if (isset($results))
        $(function(){
            $('#datatable').DataTable({
                "processing": true,
                "serverSide": true,
                "lengthChange": false,
                "ordering": false,
                "searching": false,
                "pageLength": 40,
                "info": false,
                "language": {
                    "paginate": {
                        "previous": "&lt;",
                        "next": "&gt;"
                    }
                },
                "ajax": "{{ route('api.reports.report55') }}?{!! request()->server('QUERY_STRING') !!}",
                "columns": [
                    { "data": "id" },
                    { "data": "reference_number" },
                    {
                        "data": null,
                        "render": function ( data, type, row ) {
                            return '<i>' + row.first_name + ' ' + row.last_name + '</i>';
                        }
                    },
                    { "data": "created_at" },
                    { "data": "ref_number" },
                    { "data": "points_rewarded" },
                    { "data": "amt_reward" },
                    {
                        "data": null,
                        "render": function ( data, type, row ) {
                            return '<i>' + row.original_total_amount + ' ' + row.currency_short_code + '</i>';
                        }
                    },
                    {
                        "data": null,
                        "render": function ( data, type, row ) {
                            return '<i>' + row.partner_amount + ' ' + row.partner_currency_short_code + '</i>';
                        }
                    }
                ]
            });

            $.ajax({
                url: '{{ route('api.reports.report55totals') }}?{!! request()->server('QUERY_STRING') !!}',
                context: $('#totals'),
            }).done(function(data){
                $('strong', $(this)).each(function(){
                    $(this).text(data[$(this).attr('id')]);
                });
            });
        });
        @endif

        set_menu('mnu-reporting');
    </script>
@stop
