@extends('layouts.dashboard')

@section('title')
    {{ $report->name }} | BLU
@stop

@section('body')
    <div class="padding">

        <div class="content-head pure-g">
            <div class="title pure-u-1-2">
                <h1>
                    <i class="fa fa-bar-chart-o fa-fw"></i> {{ $report->name }}
                </h1>
            </div>
            <div class="controls pure-u-1-2">
                @include('report.partials.controls', $report)
            </div>
        </div>

        <div class="report-info">
            <i class="fa fa-info-circle"></i> {{ $report->description }}
        </div>

        <div class="pure-g">
            <div class="pure-u-1-5 report-controls">
                <div class="padding">

                    <h4>Report Settings</h4>

                    {{--- Custom Report Controls ---}}
                    {{ Form::open(array('class' => "pure-form pure-form-stacked", 'method' => "GET")) }}
                        @include('report.partials.networks', $defaults)

                        @include('report.partials.submit')

                    {{ Form::close() }}
                    {{--- /Custom Report Controls ---}}

                </div>
            </div>
            <div class="pure-u-4-5">



                <table class="pure-table">
                    <thead>
                        <tr>

                            <th>User ID</th>
                            <th>Member</th>
                            <th>Network</th>
                            <th>Points Redeemed </th>
                            <th>Points Used </th>
                            <th>Discrepancy </th>

                        </tr>
                    </thead>
                    <tbody>
                    
                    @if(count($results) > 0)
                        @foreach($results as $key => $result)
                            <?php
                                $userName   = $result['user_name'];
                                $ptsRedeemed = $result['pts_redeemed'];
                                $ptsUsed = $result['pts_used'];
                                $discrepancy = $result['discrepancy'];
                                
                                if(empty($ptsRedeemed)){
                                    $ptsRedeemed = 0;
                                }
                                if(empty($ptsUsed)){
                                    $ptsUsed = 0;
                                }
                                
                                if(empty($discrepancy)){
                                    $discrepancy = $ptsRedeemed - $ptsUsed;
                                }
                            ?>
                                <tr>
                                    <td>{{ $result['user_id'] }}</td>
                                    <td>
                                        @if($userName != 'na na' && !empty($userName))
                                            {{ $userName }} 
                                        @else
                                            <i>N/A</i>
                                        @endif
                                    </td>
                                    <td>{{ $result['network'] }}</td>
                                    <td>{{ number_format($ptsRedeemed,2) }}</td>
                                    <td>{{ number_format($ptsUsed,2) }}</td>
                                    <td>{{ number_format($discrepancy,2) }}</td>
                                </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="6">No Report Data Found</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
        </div>

    </div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-reporting');
    </script>
@stop