@extends('layouts.dashboard')

@section('title')
    {{ $report->name }} | BLU
@stop

@section('body')
    <div class="padding">
		<input type="hidden" name="loaded" id="loaded" value="{{ count($results) }}"/>
        <div class="content-head pure-g">
            <div class="title pure-u-1-2">
                <h1>
                    <i class="fa fa-bar-chart-o fa-fw"></i> {{ $report->name }}
                </h1>
            </div>
            <div class="controls pure-u-1-2">
                @include('report.partials.controls', $report)
            </div>
        </div>

        <div class="report-info">
            <i class="fa fa-info-circle"></i> {{ $report->description }}
        </div>

        <div class="pure-g">
            <div class="pure-u-1-5 report-controls">
                <div class="padding">

                    <h4>Report Settings</h4>

                    {{--- Custom Report Controls ---}}
                    {{ Form::open(array('class' => "pure-form pure-form-stacked", 'method' => "GET")) }}

                        @include('report.partials.period', $defaults)

						@include('report.partials.currency', $defaults)

						@include('report.partials.networkcurrency', $defaults)

                        @include('report.partials.countries', $defaults)

                        @include('report.partials.submit')

                    {{ Form::close() }}
                    {{--- /Custom Report Controls ---}}

                </div>
            </div>
            <div class="pure-u-4-5">

                <div id="table">
                    <table class="pure-table">
                        <thead>
                            <tr>
                                <th>Period</th>
                                <th>Country</th>
                                <th>Trx Rewarded</th>
                                <th>Trx Redeemed</th>
                                <th>Points Rewarded</th>
                                <th>Points Redeemed</th>
                                <th>Amount Reward</th>
                                <th>Amount Redeem</th>
                            </tr>
                        </thead>
                        <tbody>

                        @if(count($results) > 0)
                            @foreach($results as $t)
                                <tr>
                                    <td>
                                        {{ ReportHelper::chartDateString($defaults) }}
                                    </td>
                                    <td>
                                        @if($t->country)
                                            {{ $t->country->name }}
                                        @else
                                            <i>N/A</i>
                                        @endif
                                    </td>
                                    <td>
                                        {{ number_format($t->trx_reward) }}
                                    </td>
                                    <td>
                                        {{ number_format($t->trx_redeem) }}
                                    </td>
                                    <td>
                                        {{ number_format($t->points_rewarded) }}
                                    </td>
                                    <td>
                                        {{ number_format($t->points_redeemed) }}
                                    </td>
                                    <td>
                                        {{ number_format($t->amt_reward * $totals['exchangeRate'], 2) }}
                                    </td>
                                    <td>
                                        {{ number_format($t->amt_redeem * $totals['exchangeRate'], 2) }}
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="9">No Report Data Found</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>

                <div id="chart">
                    <h5>Transaction Overview</h5>
                    <div class="chart-canvas" id="chart_1"></div>

                    <h5>Points Overview</h5>
                    <div class="chart-canvas" id="chart_2"></div>

                    <h5>Amount Overview</h5>
                    <div class="chart-canvas" id="chart_3"></div>
                </div>

                {{--- $results->appends(Input::except('page'))->links('vendor.pagination.custom-report') ---}}
            </div>
        </div>

    </div>
@stop

@section('page_script')
    <script src="http://code.highcharts.com/highcharts.js"></script>
    <script src="/highcharts/js/themes/blu.js"></script>
    <script>
        set_menu('mnu-reporting');

        var chart_defaults = {
            chart: {
                type: 'column'
            },
            subtitle: {
                text: '{{ ReportHelper::chartDateString($defaults) }}'
            },
            xAxis: {
                categories: [
                    @foreach($results as $p)
                        @if($p->country)
                            '{{ addslashes($p->country->name) }}',
                        @else
                            'N/A',
                        @endif
                    @endforeach
                ]
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            credits:{
                enabled: false
            }
        };

        $(function () {

            // Load Chart 1
            $('#chart_1').highcharts($.extend({}, chart_defaults, {
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Total'
                    }
                },
                series: [{
                    name: 'Redemption',
                    data: [{{ ReportHelper::generateSeriesData($results, "trx_redeem") }}]

                }, {
                    name: 'Reward',
                    data: [{{ ReportHelper::generateSeriesData($results, "trx_reward") }}]

                }]
            }));

            // Load Chart 2
            $('#chart_2').highcharts($.extend({}, chart_defaults, {
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Total'
                    }
                },
                series: [{
                    name: 'Redemption',
                    data: [{{ ReportHelper::generateSeriesData($results, "points_redeemed") }}]

                }, {
                    name: 'Reward',
                    data: [{{ ReportHelper::generateSeriesData($results, "points_rewarded") }}]

                }]
            }));

            // Load Chart 3
            $('#chart_3').highcharts($.extend({}, chart_defaults, {
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Total (USD)'
                    }
                },
                series: [{
                    name: 'Redemption',
                    data: [{{ ReportHelper::generateSeriesData($results, "amt_redeem") }}]

                }, {
                    name: 'Reward',
                    data: [{{ ReportHelper::generateSeriesData($results, "amt_reward") }}]

                }]
            }));

            // Hide the charts
            $('div#chart').hide();

        });
    </script>
@stop