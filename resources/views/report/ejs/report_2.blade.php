@extends('layouts.dashboard')

@section('title')
    {{ $report->name }} | BLU
@stop

@section('body')
    <div class="padding">

        <div class="content-head pure-g">
            <div class="title pure-u-1-2">
                <h1>
                    <i class="fa fa-bar-chart-o fa-fw"></i> {{ $report->name }} 
                </h1>
            </div>
            <div class="controls pure-u-1-2">
                @include('report.partials.controls', $report)
            </div>                        
        </div>

        <div class="report-info">
            <i class="fa fa-info-circle"></i> {{ $report->description }}
        </div>

        <div class="pure-g">
            <div class="pure-u-1-5 report-controls">
                <div class="padding">

                    <h4>Report Settings</h4>

                    {{--- Custom Report Controls ---}}
                    {{ Form::open(array('class' => "pure-form pure-form-stacked", 'method' => "GET")) }}

                        @include('report.partials.segments', $defaults)

                        @include('report.partials.submit')

                    {{ Form::close() }}
                    {{--- /Custom Report Controls ---}}

                </div>
            </div>
            <div class="pure-u-4-5">
                <table class="pure-table">
                    <thead>
                        <tr>
                            <th width="50"><a href="{{ Filter::baseUrl('sort=id') }}">ID</a></th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Mobile</th>
                            <th>Email</th>
                            <th>Gender</th>
                            <th>DOB</th>
                        </tr>
                    </thead>
                    <tbody>

                    @if(count($results) > 0)
                        @foreach($results as $t)
                            <tr>
                                <td width="50">{{ $t->id }}</td>
                                <?php 
                                
                                    if($sensitive['has_middleware'] ==1){
                                        foreach($sensitive['user_info'] as $u_info){
                                            if(is_object($u_info)){
                                                if($t->id == $u_info->blu_id){
                                                    $t->first_name = $u_info->first_name;
                                                    $t->last_name = $u_info->last_name;
                                                    $t->mobile = $u_info->mobile;
                                                    $t->email = $u_info->email;
                                                }
                                            }
                                        }
                                    }
                                ?>
                                <td>
                                    @if($t->first_name && $t->first_name != 'na')
                                    {{ $t->first_name }}
                                    @else
                                        <i>N/A</i>
                                    @endif
                                </td>
                                <td>
                                    @if($t->last_name && $t->last_name != 'na')
                                    {{ $t->last_name }}
                                    @else
                                        <i>N/A</i>
                                    @endif
                                </td>
                                <td>
                                   @if($t->mobile && !$sensitive['has_middleware'])
                                        {{ $t->normalizedContactNumber() }}
                                    @elseif($sensitive['has_middleware'])
                                        {{ $t->mobile }}
                                    @else
                                        <i>N/A</i>
                                    @endif
                                </td>
                                <td>
                                    @if($t->email)
                                        {{ $t->email }}
                                    @else
                                        <i>N/A</i>
                                    @endif
                                </td>
                                <td>
                                    {{ $t->gender }}                        
                                </td>
                                <td>
                                    {{ $t->dob }}                   
                                </td>
                            </tr>                    
                        @endforeach
                    @else
                        <tr>
                            <td colspan="7">No Report Data Found</td>
                        </tr>
                    @endif
                    </tbody>    
                </table>  

                {{ $results->appends(Input::except('page'))->links('vendor.pagination.custom-report') }}
            </div>
        </div>

    </div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-reporting');
    </script>
@stop