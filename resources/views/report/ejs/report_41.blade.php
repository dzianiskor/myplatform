@extends('layouts.dashboard')

@section('title')
    {{ $report->name }} | BLU
@stop

@section('body')

    <div class="padding">
		<input type="hidden" name="loaded" id="loaded" value="{{ $results->count() }}"/>
        <div class="content-head pure-g">
            <div class="title pure-u-1-2">
                <h1>
                    <i class="fa fa-bar-chart-o fa-fw"></i> {{ $report->name }}
                </h1>
            </div>
            <div class="controls pure-u-1-2">
                @include('report.partials.controls', $report)
            </div>
        </div>

        <div class="report-info">
            <i class="fa fa-info-circle"></i> {{ $report->description }}
        </div>

        <div class="pure-g">
            <div class="pure-u-1-5 report-controls">
                <div class="padding">

                    <h4>Report Settings</h4>

                    {{--- Custom Report Controls ---}}
                    {{ Form::open(array('class' => "pure-form pure-form-stacked", 'method' => "GET")) }}

                        @include('report.partials.period', $defaults)

                        @include('report.partials.membersautocomplete', $defaults)
                        
                        @include('report.partials.partners', $defaults)

                        @include('report.partials.submit')
                        <input type="hidden" name="report_number" id="report_number" value="41"/>
                    {{ Form::close() }}
                    {{--- /Custom Report Controls ---}}

                </div>
            </div>
            <div class="pure-u-4-5">
                <table class="pure-table">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Reference</th>
                            <th>Member</th>
                            <th>Date &amp; Time</th>
                            <th>Item</th>
                            <th>Shipping Company</th>
                            <th>Order Status</th>
                        </tr>
                    </thead>
                    <tbody>
                    
                    @if($results->count() > 0)
                        @foreach($results as $t)
                        <?php
                            $admin_partner = Auth::User()->getTopLevelPartner();
                            $has_middleware = false;
                            $user_info = array();
                            if($admin_partner->has_middleware == '1'){

                                $user_ids_used = array();
                                $has_middleware = true;
                                $user_ids_used[] = $t->user_id;
                                $term = "";
                                $url = $admin_partner->link_middleware_api . '?method=searchSensitive&api_key='.$admin_partner->middleware_api_key;
                                $json_ids = json_encode($user_ids_used);
                                $url .= "&user_ids=" . $json_ids . "&term='" . $term . "'";
                                $curl = curl_init();
                                // Set some options - we are passing in a useragent too here
                                curl_setopt_array($curl, array(
                                    CURLOPT_RETURNTRANSFER => 1,
                                    CURLOPT_URL => $url,
                                    CURLOPT_USERAGENT => 'Testing Sensitive Info'
                                ));
                                // Send the request & save response to $resp
                                $resp = curl_exec($curl);
                                $resp_curl = json_decode($resp);

                                $Users_info = $resp_curl;

                                foreach($Users_info as $member) {
                                    if(in_array($member->blu_id,$user_ids_used)){
                                        $user_info['first_name'] = $member->first_name;
                                        $user_info['last_name'] = $member->last_name;
                                    }
                                }
                            }else{
                                $user = App\User::find($t->user_id);
                                $name = $user->name;
                                if($name == 'na na' || $name == ''){
                                $name = 'N/A';
                            }
                            }
                        ?>
                            <tr>
                                <td>{{ $t->trx_id }}</td>
                                <td>
                                    <?php
                                        $refID = 'N/A';
                                        $ref = DB::table('reference')->where('user_id', $t->user_id)->where('partner_id', Auth::User()->getTopLevelPartner()->id)->first();
                                        
                                        if(!empty($ref)){
                                            $refID = $ref->number;
                                        }
                                        
                                        echo $refID;
                                    ?>
                                </td>
                                <td>
                                    @if($has_middleware)
                                    <?php
                                        if(empty($user_info['first_name']) && empty($user_info['last_name'])){
                                            $user = App\User::find($t->user_id);
                                            $user_info['first_name'] = $user->first_name;
                                            $user_info['last_name'] = $user->last_name;
                                        }

                                        if(empty($user_info['first_name'])){
                                            $user_info['first_name'] = '';
                                        }
                                        if(empty($user_info['last_name'])){
                                            $user_info['last_name'] = '';
                                        }
                                        
                                        $memberName = $user_info['first_name'] . ' ' . $user_info['last_name'];
                                        if($memberName == 'na na' || $memberName == ' '){
                                            $memberName = 'N/A';
                                        }
                                    ?>
                                        {{ $memberName }} 
                                    @elseif(isset($name))
                                        {{$name}}
                                    @else
                                        N/A
                                    @endif
                                </td>
                                <td>
                                    <?php
                                        if(!empty($t->created_at)){
                                            echo $t->created_at;
                                        }
                                        else{
                                            echo 'N/A';
                                        }
                                    ?>
                                </td>
                                <td>
                                    <?php
                                        if(!empty($t->item_name)){
                                            echo $t->item_name;
                                        }
                                        else{
                                            echo 'N/A';
                                        }
                                    ?>
                                </td>
                                <td>
                                    <?php
                                        if(!empty($t->shipping_company)){
                                            echo $t->shipping_company;
                                        }
                                        else{
                                            echo 'N/A';
                                        }
                                    ?>
                                </td>
                                <td>
                                    <?php
                                        if(!empty($t->order_status)){
                                            echo $t->order_status;
                                        }
                                        else{
                                            echo 'N/A';
                                        }
                                    ?>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="7">No Report Data Found</td>
                        </tr>
                    @endif
                    </tbody>
                </table>

                {{ $results->appends(Input::except('page'))->links('vendor.pagination.custom-report') }}
            </div>
        </div>

    </div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-reporting');
    </script>
@stop