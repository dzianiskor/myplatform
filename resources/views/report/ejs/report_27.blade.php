@extends('layouts.dashboard')

@section('title')
    {{ $report->name }} | BLU
@stop

@section('body')
    <div class="padding">

        <div class="content-head pure-g">
            <div class="title pure-u-1-2">
                <h1>
                    <i class="fa fa-bar-chart-o fa-fw"></i> {{ $report->name }} 
                </h1>
            </div>
            <div class="controls pure-u-1-2">
                @include('report.partials.controls', $report)
            </div>                        
        </div>

        <div class="report-info">
            <i class="fa fa-info-circle"></i> {{ $report->description }}
        </div>

        <div class="pure-g">
            <div class="pure-u-1-5 report-controls">
                <div class="padding">

                    <h4>Report Settings</h4>

                    {{--- Custom Report Controls ---}}
                    {{ Form::open(array('class' => "pure-form pure-form-stacked", 'method' => "GET")) }}
                        @include('report.partials.period', $defaults)
                        {{--- @include('report.partials.periodpreset', $defaults) ---}}
                        {{--- @include('report.partials.bypartner', $defaults) ---}}
						@include('report.partials.networkcurrency', $defaults)
                        @include('report.partials.networks', $defaults)
                        @include('report.partials.members', $defaults)

                        @include('report.partials.submit')

                    {{ Form::close() }}
                    {{--- /Custom Report Controls ---}}

                </div>
            </div>
            <div class="pure-u-4-5">
                
                

                <table class="pure-table">
                    <thead>
                        <tr>
                            
                            <th><a href="{{ Filter::searchUrl('sort=creation') }}"> Date </a> </th>
                            <th>Issuing Partner</th>
                            <th>Transaction ID</th>
                            <th>Pts Used</th>
                            <th>Pts Used At</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                    <?php
//                    echo "<pre>";
//                    foreach($resultRalph as $key => $resra ){
//                        foreach($resra as $keyra1 => $resra1){
//                            echo $key;
//                            var_dump($resra1->creation);
//                            var_dump($resra1->ptsused);
//                        }
//                        
//                    }
//                    exit();
                    ?>
                    @if(count($resultRalph) > 0)
                        @foreach($resultRalph as $key => $resra)
                            @foreach($resra as $keyra1 => $t)
                            <tr>
                                
                                <td>
                                    <?php 
                                        if(isset($t->crmonth)){
                                            $dateObj   = DateTime::createFromFormat('!m', $t->crmonth);
                                            $monthName = $dateObj->format('M');
                                            echo  $monthName . ". ";
                                        }
                                        ?>
                                        @if(strlen($t->creation) > 4)
                                            <?php 
                                                $dateObj = new DateTime( $t->creation);
                                                $dateformatted = $dateObj->format('F j, Y');
                                                echo $dateformatted;
                                            ?>
                                        @else
                                            {{ $t->creation }}
                                        @endif
                                         
                                </td>
                                <td>
                                    <?php 
                                    $partner = App\Partner::find($key);
                                    echo $partner->name;
                                    ?>
                                    
                                </td>
                                <td>
                                    @if($t->trx_ref_id)
                                        {{ $t->trx_ref_id }}
                                    @else
                                        N/A
                                    @endif
                                </td>
                                <td>
                                    @if($t->ptsused)
                                        {{ number_format($t->ptsused) }}
                                    @else
                                        0
                                    @endif  
                                </td>
                                <td>
                                    @if($t->trx_ref_id)
                                        <?php
                                            $trans = App\Transaction::find($t->trx_ref_id);
                                            if($trans){
                                                $store = App\Store::find($trans->store_id);
                                                $trx_partner = App\Partner::find($trans->partner_id);
                                                $storeName = "N/A";
                                                if(is_object($store)){
                                                    $storeName = $store->name;
                                                }
                                                $partnerName = "N/A";
                                                $auth_partner_ids = Auth::User()->managedPartnerIDs();
                                                if(is_object($trx_partner)){
                                                    if($trx_partner->category_id == 13 && !I::am('BLU Admin') && !in_array($trx_partner->id,$auth_partner_ids) ){
                                                        echo "Bank Partner";
                                                    }
                                                    else{
                                                        $partnerName = $trx_partner->name;
                                                        echo $partnerName ." - " . $storeName;
                                                    }
                                
                                                }
                                                
                                            }
                                            else{
                                                echo "N/A";
                                            }
                                        ?>
                                    @else
                                        N/A
                                    @endif
                                </td>
                            </tr>    
                            @endforeach
                        @endforeach
                    @else
                        <tr>
                            <td colspan="6">No Report Data Found</td>
                        </tr>
                    @endif
                    </tbody>           
                </table>  

                
            </div>
        </div>

    </div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-reporting');
    </script>
@stop