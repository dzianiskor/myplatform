@extends('layouts.dashboard')

@section('title')
    {{ $report->name }} | BLU
@stop

@section('body')
    <div class="padding">
		<input type="hidden" name="loaded" id="loaded" value="{{ count($results) }}"/>
        <div class="content-head pure-g">
            <div class="title pure-u-1-2">
                <h1>
                    <i class="fa fa-bar-chart-o fa-fw"></i> {{ $report->name }}
                </h1>
            </div>
            <div class="controls pure-u-1-2">
                @include('report.partials.controls', $report)
            </div>
        </div>

        <div class="report-info">
            <i class="fa fa-info-circle"></i> {{ $report->description }}
        </div>

        <div class="pure-g">
            <div class="pure-u-1-5 report-controls">
                <div class="padding">

                    <h4>Report Settings</h4>

                    {{--- Custom Report Controls ---}}
                    {{ Form::open(array('class' => "pure-form pure-form-stacked", 'method' => "GET")) }}

                        @include('report.partials.period', $defaults)
                        
                        @include('report.partials.myperiodpreset', $defaults)
                        
                        @include('report.partials.networkcurrency', $defaults)

                        @include('report.partials.currency', $defaults)

                        @include('report.partials.partners', $defaults)
                        
                        @include('report.partials.trxtype', $defaults)

                        @include('report.partials.submit')

                    {{ Form::close() }}
                    {{--- /Custom Report Controls ---}}

                </div>
            </div>
            <div class="pure-u-4-5">

                <div id="table">
                    <table class="pure-table">
                        <thead>
                            <tr>
                                <th>Period</th>
                                <th>Points</th>
                                <th>Transaction number</th>
                                <th>Amount (<?php echo $totals['currencyShortCode']; ?>)</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php 
                            $chartsData = array();
                        ?>
                        @if(count($results) > 0)
                            @foreach($results as $t)
                            <?php
                                $monthsArray = array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
                                if($defaults['trxtype'] == 'reward'){
                                    $totAmountArray = CurrencyHelper::convertFromUSD($t->total_amount, $totals['currencyShortCode']);
                                    $totAmount = $totAmountArray['amount'];
                                }
                                else{
                                   $totAmount =  PointsHelper::pointsToAmount($t->p_sum, $totals['currencyShortCode'], $defaults['network_currency']);
                                }
                                
                                $monthName  = '';
                                if($defaults['monyeardaterange'] == 'monthly'){
                                    $monthName  = date('M', strtotime($t->creation));
                                    $year  = date('Y', strtotime($t->creation));
                                    foreach ($monthsArray as $mon) {
                                        if($monthName == $mon){
                                            $chartsData[$year][$mon]['trx']  = $t->trx_number;
                                            $chartsData[$year][$mon]['points']  = $t->p_sum;
                                            $chartsData[$year][$mon]['amount']  = $totAmount;
                                        }elseif(empty($chartsData[$year][$mon])){
                                            $chartsData[$year][$mon]['trx']  = 0;
                                            $chartsData[$year][$mon]['points']  = 0;
                                            $chartsData[$year][$mon]['amount']  = 0;
                                        }
                                    }
                                    
                                }
                                else{
                                   $chartsData[$t->creation]['trx']  = $t->trx_number; 
                                   $chartsData[$t->creation]['points']  = $t->p_sum; 
                                   $chartsData[$t->creation]['amount']  = $totAmount; 
                                }
                                
                            ?>
                                <tr>
                                    <td>
                                        <?php
                                            if($defaults['monyeardaterange'] == 'monthly'){
                                                echo date('M-Y', strtotime($t->creation));
                                            }else{
                                                echo $t->creation;
                                            }
                                        ?>
                                    </td>
                                    <td>
                                        {{ number_format($t->p_sum, 0) }}
                                    </td>
                                    <td>
                                        {{ number_format($t->trx_number, 0) }}
                                    </td>
                                    <td>
                                        {{ number_format($totAmount, 2) }}
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="4">No Report Data Found</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>

                <div id="chart">
                    <h5>Points Overview</h5>
                    <div class="chart-canvas" id="chart_1"></div>

                    <h5>Amount Overview</h5>
                    <div class="chart-canvas" id="chart_2"></div>

                    <h5>Transaction Overview</h5>
                    <div class="chart-canvas" id="chart_3"></div>
                </div>

                {{--- $results->appends(Input::except('page'))->links('vendor.pagination.custom-report') ---}}
            </div>
        </div>

    </div>
@stop
<?php
ksort($chartsData);
?>
@section('page_script')
    <script src="//code.highcharts.com/highcharts.js"></script>
    <script src="/highcharts/js/themes/blu.js"></script>
    <script>
        set_menu('mnu-reporting');

        var chart_defaults = {
            chart: {
                type: 'column'
            },
            subtitle: {
                text: '{{ ReportHelper::chartDateString($defaults) }}'
            },
            xAxis: {
                @if($defaults['monyeardaterange'] == 'monthly')
                    categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                @else
                    categories: ['Year']
                @endif
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            credits:{
                enabled: false
            }
        };

        $(function () {

            // Load Chart 1
            $('#chart_1').highcharts($.extend({}, chart_defaults, {
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Total'
                    }
                },
                plotOptions: {
                    series: {
                        pointPadding: 0,
                        groupPadding: 0,
                        borderWidth: 0.2
                    }
                },
                series: [
                    @if($defaults['monyeardaterange'] == 'monthly')
                        @foreach($chartsData as $year => $p)
                            {
                                name: {{ $year }},

                                data: [
                                        @foreach($p as $month => $p)
                                        <?php echo $p['points']; ?>,
                                        @endforeach
                                      ]

                            }, 
                        @endforeach
                    @else
                            @foreach($chartsData as $year => $p)
                            {
                                name: {{ $year }},

                                data: [
                                        <?php echo $p['points']; ?>,
                                      ]

                            }, 
                        @endforeach
                    @endif
            ]
            }));

            // Load Chart 2
            $('#chart_2').highcharts($.extend({}, chart_defaults, {
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Total'
                    }
                },
                plotOptions: {
                    series: {
                        pointPadding: 0,
                        groupPadding: 0,
                        borderWidth: 0.2
                    }
                },
                series: [
                    @if($defaults['monyeardaterange'] == 'monthly')
                        @foreach($chartsData as $year => $p)
                            {
                                name: {{ $year }},

                                data: [
                                        @foreach($p as $month => $p)
                                        <?php echo $p['amount']; ?>,
                                        @endforeach
                                      ]

                            }, 
                        @endforeach
                    @else
                            @foreach($chartsData as $year => $p)
                            {
                                name: {{ $year }},

                                data: [
                                        <?php echo $p['amount']; ?>
                                      ]

                            }, 
                        @endforeach
                    @endif
            ]
            }));

            // Load Chart 3
            $('#chart_3').highcharts($.extend({}, chart_defaults, {
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Total'
                    }
                },
                plotOptions: {
                    series: {
                        pointPadding: 0,
                        groupPadding: 0,
                        borderWidth: 0.2
                    }
                },
                series:[
                    @if($defaults['monyeardaterange'] == 'monthly')
                        @foreach($chartsData as $year => $p)
                            {
                                name: {{ $year }},

                                data: [
                                        @foreach($p as $month => $p)
                                        <?php echo $p['trx']; ?>,
                                        @endforeach
                                      ]

                            }, 
                        @endforeach
                    @else
                            @foreach($chartsData as $year => $p)
                            {
                                name: {{ $year }},

                                data: [
                                        <?php echo $p['trx']; ?>
                                      ]

                            }, 
                        @endforeach
                    @endif
            ]
            }));

            // Hide the charts
            $('div#chart').hide();

        });
    </script>
@stop