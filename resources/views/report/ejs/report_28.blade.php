@extends('layouts.dashboard')

@section('title')
    {{ $report->name }} | BLU
@stop

@section('body')
    <div class="padding">

        <div class="content-head pure-g">
            <div class="title pure-u-1-2">
                <h1>
                    <i class="fa fa-bar-chart-o fa-fw"></i> {{ $report->name }} 
                </h1>
            </div>
            <div class="controls pure-u-1-2">
                @include('report.partials.controls', $report)
            </div>                        
        </div>

        <div class="report-info">
            <i class="fa fa-info-circle"></i> {{ $report->description }}
        </div>

        <div class="pure-g">
            <div class="pure-u-1-5 report-controls">
                <div class="padding">

                    <h4>Report Settings</h4>

                    {{--- Custom Report Controls ---}}
                    {{ Form::open(array('class' => "pure-form pure-form-stacked", 'method' => "GET")) }}
                        @include('report.partials.period', $defaults)
                        @include('report.partials.periodpreset', $defaults)
                        {{--- @include('report.partials.bypartner', $defaults) ---}}
						@include('report.partials.networkcurrency', $defaults)
                        @include('report.partials.partners', $defaults)

                        @include('report.partials.submit')

                    {{ Form::close() }}
                    {{--- /Custom Report Controls ---}}

                </div>
            </div>
            <div class="pure-u-4-5">
                
                

                <table class="pure-table">
                    <thead>
                        <tr>
                            
                            <th><a href="{{ Filter::searchUrl('sort=creation') }}"> Date </a> </th>
                            <th>Redemption Transaction</th>
                            <th>Partner</th>
                            <th>Pts Used</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                    <?php
//                    echo "<pre>";
//                    foreach($resultRalph as $key => $resra ){
//                        foreach($resra as $keyra1 => $resra1){
//                            echo "keyra1: " . $keyra1;
//                            echo "<br> key: ".$key . "<br>";
//                            var_dump($resra1);
//                            exit();
//                        }
//                        
//                    }
//                    
                    ?>
                    @if(count($resultRalph) > 0)
                        @foreach($resultRalph as $key => $resra)
                            @foreach($resra as $keyra1 => $temp_obj)
                            <?php 
                            //echo "<pre>";
                            $t = $temp_obj[0];
                            //var_dump($temp_obj);
                            //exit();
                            ?>
                            
                            
                            <tr>
                                
                                <td>
                                    <?php 
                                        if(isset($t->crmonth)){
                                            $dateObj   = DateTime::createFromFormat('!m', $t->crmonth);
                                            $monthName = $dateObj->format('M');
                                            echo  $monthName . ". ";
                                        }
                                        ?>
                                        @if(strlen($t->creation) > 4)
                                            <?php 
                                                $dateObj = new DateTime( $t->creation);
                                                $dateformatted = $dateObj->format('F j, Y');
                                                echo $dateformatted;
                                            ?>
                                        @else
                                            {{ $t->creation }}
                                        @endif
                                         
                                </td>
                                <td>
                                    <?php 
                                    //$transaction = App\Transaction::find($key);
                                    echo $key;
                                    ?>
                                    
                                </td>
                                <td>
                                    <?php 
                                    $partner = App\Partner::find($keyra1);
                                    echo $partner->name;
                                    ?>
                                    
                                </td>
                                
                                <td>
                                    @if($t->points_used)
                                        {{ number_format($t->points_used) }}
                                    @else
                                        0
                                    @endif  
                                </td>
                                
                            </tr>    
                            @endforeach
                        @endforeach
                    @else
                        <tr>
                            <td colspan="6">No Report Data Found</td>
                        </tr>
                    @endif
                    </tbody>           
                </table>  

                
            </div>
        </div>

    </div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-reporting');
    </script>
@stop