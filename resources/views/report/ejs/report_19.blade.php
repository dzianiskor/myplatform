@extends('layouts.dashboard')

@section('title')
    {{ $report->name }} | BLU
@stop

@section('body')
    <div class="padding">

        <div class="content-head pure-g">
            <div class="title pure-u-1-2">
                <h1>
                    <i class="fa fa-bar-chart-o fa-fw"></i> {{ $report->name }} 
                </h1>
            </div>
            <div class="controls pure-u-1-2">
                @include('report.partials.controls', $report)
            </div>                        
        </div>

        <div class="report-info">
            <i class="fa fa-info-circle"></i> {{ $report->description }}
        </div>

        <div class="pure-g">
            <div class="pure-u-1-5 report-controls">
                <div class="padding">

                    <h4>Report Settings</h4>

                    {{--- Custom Report Controls ---}}
                    {{ Form::open(array('class' => "pure-form pure-form-stacked", 'method' => "GET")) }}

                        @include('report.partials.partners', $defaults)

                        @include('report.partials.submit')

                    {{ Form::close() }}
                    {{--- /Custom Report Controls ---}}

                </div>
            </div>
            <div class="pure-u-4-5">

                <table class="pure-table report-table">
                    <thead>
                        <tr>
                            <th width="50"><a href="{{ Filter::baseUrl('sort=id') }}">ID</a></th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Mobile</th>
                            <th>Email</th>
                            <th>Gender</th>
                            <th>DOB</th>
                        </tr>
                    </thead>
                    <tbody>
                    @if(count($results) > 0)
                        @foreach($results as $t)
                        
                        <?php
                                    $admin_partner = Auth::User()->getTopLevelPartner();
                                    $has_middleware = false;
                                    if($admin_partner->has_middleware == '1'){

                                        $user_ids_used = array();
                                        $has_middleware = true;
                                        $user_ids_used[] = $t->id;
                                        $term = "";
                                        $url = $admin_partner->link_middleware_api . '?method=sensitiveInfo&api_key='.$admin_partner->middleware_api_key;
                                        $json_ids = json_encode($user_ids_used);
                                        $url .= "&user_ids=" . $json_ids;
                                        $curl = curl_init();
                                        // Set some options - we are passing in a useragent too here
                                        curl_setopt_array($curl, array(
                                            CURLOPT_RETURNTRANSFER => 1,
                                            CURLOPT_URL => $url,
                                            CURLOPT_SSL_VERIFYPEER => False,
                                            CURLOPT_USERAGENT => 'Testing Sensitive Info'
                                        ));
                                        // Send the request & save response to $resp
                                        $resp = curl_exec($curl);
                                        $resp_curl = json_decode($resp);

                                        $Users_info = $resp_curl;

                                        foreach($Users_info as $member) {
                                            if(in_array($member->blu_id,$user_ids_used)){
                                                $first_name = $member->first_name;
                                                $last_name = $member->last_name;
                                                $mobile = $member->mobile;
                                                $email = $member->email;
                                            }
                                        }
                                    }
                                    ?>
                                        
                            <tr>
                                <td width="50">{{ $t->id }}</td>
                                <td>
                                    @if($has_middleware)
                                        {{ $first_name }}
                                    @elseif($t->first_name != 'na')
                                    {{ $t->first_name }}
                                    @else
                                        <i>N/A</i>
                                    @endif
                                    
                                </td>
                                <td>
                                    @if($has_middleware)
                                        {{ $last_name }}
                                    @elseif($t->last_name != 'na')
                                    {{ $t->last_name }}
                                    @else
                                        <i>N/A</i>
                                    @endif
                                    
                                </td>
                                <td>
                                    @if($has_middleware)
                                        {{ $mobile }}
                                    @elseif($t->mobile)
                                        {{ $t->normalizedContactNumber() }}
                                    @else
                                        <i>N/A</i>
                                    @endif
                                    
                                </td>
                                <td>
                                    @if($has_middleware)
                                        {{ $email }}
                                    @elseif($t->email)
                                        {{ $t->email }}
                                    @else
                                        <i>N/A</i>
                                    @endif
                                    
                                </td>
                                <td>
                                    {{ $t->gender }}                        
                                </td>
                                <td>
                                    {{ $t->dob }}                   
                                </td>
                            </tr>                 
                        @endforeach
                    @else
                        <tr>
                            <td colspan="7">No Report Data Found</td>
                        </tr>
                    @endif
                    </tbody>
                </table>     

                {{ $results->appends(Input::except('page'))->links('vendor.pagination.custom-report') }}
            </div>
        </div>

    </div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-reporting');
    </script>
@stop
