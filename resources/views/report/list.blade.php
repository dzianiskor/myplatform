@extends('layouts.dashboard')

@section('title')
    Reporting | BLU
@stop

@section('body')
    <div class="padding">

        <div class="content-head pure-g">
            <div class="title pure-u-1-2">
                <h1><i class="fa fa-bar-chart-o fa-fw"></i> Reporting &raquo; {{ ucfirst($title) }}</h1>
            </div>
            @if($type == 'tiers' || Input::has('q'))
                <div class="controls pure-u-1-2">
                    <form action="{{ Filter::searchUrl() }}" method="get">
                        <input type="text" placeholder="Search" name="q" value="{{ Input::get('q') }}" />
                        <button type="submit" title="" class="list-search"></button>
                    </form>
                    <a href="/dashboard/reports" class="pure-button">Report Tiers</a>
                </div>
            @else
                <div class="controls pure-u-1-2">
                    <a href="/dashboard/reports" class="pure-button">Report Tiers</a>
                </div>
            @endif
        </div>

        <table class="pure-table">
            <thead>
                <tr>
                    <th width="50">ID</th>
                    <th style="text-align:left;"><a href="{{ Filter::baseUrl('sort=name') }}">Name</a></th>
                    <th width="150">Actions</th>
                </tr>
            </thead>

            <tbody>
                @if(count($items) > 0)
                    @foreach($items as $r)
                        
                                @if($type == 'tiers')
                                    @if($r->name == 'Basic Reports' && I::can('view_basic_reports'))
                        <tr>
                            <td>{{ $r->id }}</td>
                            <td style="text-align:left">{{ $r->name }}</td>
                            <td>
                                        <a href="/dashboard/reports/{{ strtolower($type) }}/{{ base64_encode($r->id) }}" class="view" title="View">View</a>
                                            </td>
                                        </tr>
                                    @endif
                                    @if($r->name == 'Premium Reports' && I::can('view_premium_reports'))
                                        <tr>
                                            <td>{{ $r->id }}</td>
                                            <td style="text-align:left">{{ $r->name }}</td>
                                            <td>
                                        <a href="/dashboard/reports/{{ strtolower($type) }}/{{ base64_encode($r->id) }}" class="view" title="View">View</a>
                                            </td>
                                        </tr>
                                    @endif                                    
                                    @if($r->name == 'Full Partner Reports' && I::can('view_full_reports'))
                                        <tr>
                                            <td>{{ $r->id }}</td>
                                            <td style="text-align:left">{{ $r->name }}</td>
                                            <td>
                                        <a href="/dashboard/reports/{{ strtolower($type) }}/{{ base64_encode($r->id) }}" class="view" title="View">View</a>
                                            </td>
                                        </tr>
                                    @endif        
                                    @if($r->name == 'Risk Management Reports' && I::can('view_risk_reports'))
                                        <tr>
                                            <td>{{ $r->id }}</td>
                                            <td style="text-align:left">{{ $r->name }}</td>
                                            <td>
                                        <a href="/dashboard/reports/{{ strtolower($type) }}/{{ base64_encode($r->id) }}" class="view" title="View">View</a>
                                            </td>
                                        </tr>
                                    @endif
                                    @if($r->name == 'Activity Summary Reports' && I::can('view_activity_summary_reports'))
                                        <tr>
                                            <td>{{ $r->id }}</td>
                                            <td style="text-align:left">{{ $r->name }}</td>
                                            <td>
                                        <a href="/dashboard/reports/{{ strtolower($type) }}/{{ base64_encode($r->id) }}" class="view" title="View">View</a>
                                            </td>
                                        </tr>
                                    @endif
                                    @if($r->name == 'Redemption Reports' && I::can('view_redemption_partner_rewarded_points_reports'))
                                        <tr>
                                            <td>{{ $r->id }}</td>
                                            <td style="text-align:left">{{ $r->name }}</td>
                                            <td>
                                        <a href="/dashboard/reports/{{ strtolower($type) }}/{{ base64_encode($r->id) }}" class="view" title="View">View</a>
                                            </td>
                                        </tr>
                                    @endif
                                    @if($r->name == 'BLU Internal Reports' && I::can('view_blu_internal_reports'))
                                        <tr>
                                            <td>{{ $r->id }}</td>
                                            <td style="text-align:left">{{ $r->name }}</td>
                                            <td>
                                                <a href="/dashboard/reports/{{ strtolower($type) }}/{{ base64_encode($r->id) }}" class="view" title="View">View</a>
                                            </td>
                                        </tr>
                                    @endif
                                @else
                                    <tr>
                                        <td>{{ $r->id }}</td>
                                        <td style="text-align:left">{{ $r->name }}</td>
                                        <td>
                                    <a href="/dashboard/reports/{{ strtolower($type) }}/{{ base64_encode($r->id) }}" class="view" title="View">View</a>
                            </td>
                        </tr>                                    
                                @endif
                                                                
                    @endforeach
                @else
                    <tr>
                        <td colspan="3">No Reports Found</td>
                    </tr>
                @endif
            </tbody>
        </table>

        {{ $items->setPath(Request::url())->appends(Input::except('page'))->links()}}

    </div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-reporting');
    </script>
@stop
