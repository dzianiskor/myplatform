<h5>Select Member Type</h5>

@if(Role::where('id', '>', 4)->count() > 0)
	@foreach(Role::where('id', '>', 4)->get() as $role)
		<label class="partner-name"><input type="checkbox" class="filter1" value="{{ $role->id }}" {{ ReportHelper::isSelected("checked", $role->id, $defaults['filter1']) }} /> {{ $role->name }}</label>
	@endforeach
@else
	<span class="not-found">No Roles Found</span>
@endif

