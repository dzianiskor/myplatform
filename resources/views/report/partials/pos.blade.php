<h5>Select POS <span style="font-size: 12px; font-weight: normal; float:right;">Select All <input type="checkbox" class="selectAll"/></span></h5>

@foreach(Auth::User()->managedPartners() as $partner)
    @if($partner->stores->count() > 0)
        <div class="checkbox-wrap">
            <label class="partner-name"><a href="#" class="plus"><i class="fa fa-caret-right fa-lg"></i></a> {{ $partner->name }}</label>
            @foreach($partner->stores as $store)
                @if($store->pointOfSales->count() > 0)
                    <label class="entity-name">
                        @if(in_array($store->id, $defaults['parent_ids']))
                            <input name="parent_ids[]" value="{{ $store->id }}" type="checkbox" class="parent_ids parent" checked="checked" data-store_id="{{ $store->id }}"/> {{ $store->name }}
                        @else
                            <input name="parent_ids[]" value="{{ $store->id }}" type="checkbox" class="parent_ids parent" data-store_id="{{ $store->id }}"/> {{ $store->name }}
                        @endif
                    </label>
                    @foreach($store->pointOfSales as $pos)
                        <label class="entity-name level2">
                            <input type="checkbox" name="entity_id" value="{{ $pos->id }}" class="entity_id child" data-store_id="{{ $store->id }}" {{ ReportHelper::isSelected("checked", $pos->id, $defaults['entities']) }} /> {{ $pos->name }}
                        </label>
                    @endforeach
                @endif
            @endforeach
        </div>
    @endif
@endforeach