<h5>Select Countries <span style="font-size: 12px; font-weight: normal; float:right;">Select All <input type="checkbox" class="selectAll"/></span></h5>

@foreach(App\Country::orderedList() as $country)    
    <div class="checkbox-wrap">
        @if(in_array($country->id, $defaults['entities']))
            <label class="partner-name">
                <input type="checkbox" class="entity_id parent" checked="checked" value="{{ $country->id }}" /> {{ $country->name }}
            </label>
        @else
            <label class="partner-name">
                <input type="checkbox" class="entity_id parent" value="{{ $country->id }}" /> {{ $country->name }}
            </label>
        @endif
    </div>
@endforeach