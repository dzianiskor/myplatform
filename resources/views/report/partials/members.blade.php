<h5>Select Member</h5>

<style>
  .ui-autocomplete-loading {
    background:url(/img/loader.gif) no-repeat center right;
  }
  li.ui-menu-item{
  	font-size:12px;
  }
</style>

<fieldset>
	<input type="hidden" name="entity_id" class="entity_id" value="{{ Input::get('entity') }}" />
	<input id="members-autocomplete" class="report-input members-autocomplete" placeholder="Search" value="{{ (!empty(Input::get('entity')))? Support::memberNameFromID(Input::get('entity')) : null }}" />
</fieldset>

