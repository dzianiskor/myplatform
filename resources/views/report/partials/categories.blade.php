<h5>Select Categories <span style="font-size: 12px; font-weight: normal; float:right;">Select All <input type="checkbox" class="selectAll"/></span></h5>

@foreach(CategoryHelper::hierarcy(1) as $c)    
    <div class="checkbox-wrap">
        <label class="partner-name">
            <a href="#" class="plus"><i class="fa fa-caret-right fa-lg"></i></a> 
            <input type="checkbox" class="entity_id parent" value="{{ $c->id }}" {{ ReportHelper::isSelected("checked", $c->id, $defaults['entities']) }} /> {{{ $c->name }}}
        </label>   
        @if($c->children)     
            @foreach($c->children as $child)
                <label class="entity-name">
                    <input type="checkbox" name="entity_id" value="{{ $child->id }}" class="entity_id child" {{ ReportHelper::isSelected("checked", $child->id, $defaults['entities']) }} /> {{{ $child->name }}}
                </label>
            @endforeach
        @endif
    </div>
@endforeach