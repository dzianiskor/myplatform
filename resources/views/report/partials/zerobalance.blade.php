<h5>Include Zero Balances</h5>
<div class="checkbox-wrap">
    <label class="partner-name">
        <input type="checkbox" class="parent zerobalance" name="zerobalance" value="zerobalance" 
               @if($defaults['zerobalance'] != 'not')
                    checked="checked"
               @endif
               /> Zero Balances
    </label>
</div>