<h5>
    Select Networks
    <span style="font-size: 12px; font-weight: normal; float:right;">Select All <input type="checkbox" data-select-all="network"/></span>
</h5>

@foreach(Auth::User()->managedNetworks() as $network)   
    
    <div class="checkbox-wrap">
        <label class="partner-name">

                @if(in_array($network->id, $defaults['network_ids']))
                    <input type="checkbox" class="network_ids" name="network_ids[]" value="{{ $network->id }}" checked="checked" data-select-item="network" /> {{ $network->name }}
                @else
                    <input type="checkbox" class="network_ids" name="network_ids[]" value="{{ $network->id }}" data-select-item="network" /> {{ $network->name }}
                @endif
        </label>
        
    </div>
@endforeach