<h5>Select Partners <span style="font-size: 12px; font-weight: normal; float:right;">Select All <input type="checkbox" class="selectAll"/></span></h5>
<div class="store-wrap">
    @foreach(Auth::User()->managedPartners() as $partner)
        @if (!isset($networkId) || $partner->networks->contains('id', $networkId))
            <div class="checkbox-wrap">
                <label class="partner-name">
                    <a href="#" class="plus"><i class="fa fa-caret-right fa-lg"></i></a>

                    @if(in_array($partner->id, $defaults['entities']))
                        <input type="checkbox" class="entity_id parent" value="{{ $partner->id }}" checked="checked"/> {{ $partner->name }}
                    @else
                        <input type="checkbox" class="entity_id parent" value="{{ $partner->id }}" /> {{ $partner->name }}
                    @endif
                </label>
                @foreach($partner->children as $partner)
                    <label class="entity-name">
                        @if(in_array($partner->id, $defaults['entities']))
                            <input type="checkbox" name="entity_id[]" value="{{ $partner->id }}" class="entity_id child"  checked="checked" /> {{ $partner->name }}
                        @else
                            <input type="checkbox" name="entity_id[]" value="{{ $partner->id }}" class="entity_id child" /> {{ $partner->name }}
                        @endif
                    </label>
                @endforeach
            </div>
        @endif
    @endforeach
</div>