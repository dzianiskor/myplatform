<div class="report-period">
    <p>
        <i class="fa fa-clock-o"></i> <span>Start:</span>
        {{ Form::text('start_date', date('m/d/Y', strtotime($defaults['start_date'])), array('class' => 'report-input datepicker-report')) }}                    
    </p>
    <p>
        <i class="fa fa-clock-o"></i> <span>End:</span>
        {{ Form::text('end_date', date('m/d/Y', strtotime($defaults['end_date'])), array('class' => 'report-input datepicker-report')) }}                    
    </p>
</div>