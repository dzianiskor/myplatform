<h5>Select Options <span style="font-size: 12px; font-weight: normal; float:right;">Select All <input type="checkbox" class="selectAll"/></span></h5>

<span class="info">
You can select either a partner or a segment, not both.
</span>

@foreach(Auth::User()->managedPartners() as $partner)
    @if($partner->segments->count() > 0)
        <div class="checkbox-wrap">
            <label class="partner-name">
                <a href="#" class="plus"><i class="fa fa-caret-right fa-lg"></i></a>

                @if(in_array($partner->id, $defaults['filter1']))
                    <input type="checkbox" class="parent filter1" name="filter_1[]" value="{{ $partner->id }}"  checked="checked" /> {{ $partner->name }}
                @else
                    <input type="checkbox" class="parent filter1" name="filter_1[]" value="{{ $partner->id }}" /> {{ $partner->name }}
                @endif
            </label>
            @foreach($partner->segments as $segment)
                <label class="entity-name">
                    @if(in_array($segment->id, $defaults['entities']))
                        <input type="checkbox" name="entity_id[]" value="{{ $segment->id }}" class="entity_id child"  checked="checked" /> {{ $segment->name }}
                    @else
                        <input type="checkbox" name="entity_id[]" value="{{ $segment->id }}" class="entity_id child" /> {{ $segment->name }}
                    @endif
                </label>
            @endforeach    
        </div>
    @endif
@endforeach