<h5>Select Products <span style="font-size: 12px; font-weight: normal; float:right;">Select All <input type="checkbox" class="selectAll"/></span></h5>

@foreach(Auth::User()->managedItems() as $c)    
    <label class="partner-name">
        <input type="checkbox" class="entity_id parent" value="{{ $c->id }}" {{ ReportHelper::isSelected("checked", $c->id, $defaults['entities']) }} /> {{{ $c->name }}}
    </label>      
@endforeach