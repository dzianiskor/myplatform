<?php
	$networks = Auth::User()->managedNetworks();
	if($defaults['network_currency'] == 'not'){
		$defaults['network_currency']	= 1;
	}
?>
<h5>Select Network</h5>
<select name="network_currency" id="network_currency" style="width: 90%;">
		<option value="0">Select a currency</option>
	@foreach($networks as $network)
		<?php
			$networkId		= $network->id;
			$networkName	= $network->name;
		?>
		<option value="<?php echo $networkId; ?>" <?php echo $defaults['network_currency'] == $networkId ? 'selected' : ''; ?>><?php echo $networkName; ?></option>
	@endforeach
</select>