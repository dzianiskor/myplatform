<h5>Range</h5>

<div class="checkbox-wrap">
    <label>
    	@if($defaults['daterange'] == 'daily')
        	<input type="radio" name="range" value="daily"  checked="checked"> Daily     
        @else
        	<input type="radio" name="range" value="daily" > Daily     
        @endif
    </label>
    <label>
    	@if($defaults['daterange'] == 'monthly')
        	<input type="radio" name="range" value="monthly"  checked="checked"> Monthly
        @else
        	<input type="radio" name="range" value="monthly" > Monthly
        @endif
    </label>   
    <label>
    	@if($defaults['daterange'] == 'yearly')
        	<input type="radio" name="range" value="yearly"  checked="checked"> Yearly
        @else
        	<input type="radio" name="range" value="yearly" > Yearly
        @endif
    </label>
</div>