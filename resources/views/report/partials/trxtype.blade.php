<h5>Data</h5>

<div class="checkbox-wrap">
    <label>
    	@if($defaults['trxtype'] == 'reward')
        	<input type="radio" name="trxtype" value="reward"  checked="checked"> Reward
        @else
        	<input type="radio" name="trxtype" value="reward" > Reward
        @endif
    </label>   
    <label>
    	@if($defaults['trxtype'] == 'redeem')
        	<input type="radio" name="trxtype" value="redeem"  checked="checked"> Redeem
        @else
        	<input type="radio" name="trxtype" value="redeem" > Redeem
        @endif
    </label>
</div>