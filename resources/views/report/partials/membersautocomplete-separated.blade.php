<h5>Select Member</h5>

<style>
	.ui-autocomplete-loading {
		background: url(/img/loader.gif) no-repeat center right;
	}

	li.ui-menu-item {
		font-size: 12px;
	}

	#members-autocomplete-reference {
		margin-top: 10px;
	}
</style>

<fieldset>
	<input type="hidden" name="member_id" class="member_id" value="{{ (!empty($defaults['member_id'][0]) && $defaults['member_id'][0] != 0)? $defaults['member_id'][0] : 0 }}"/>
	<input id="members-autocomplete-name" class="report-input members-autocomplete-new" placeholder="Search by name" value="{{ (!empty($defaults['member_id'][0]) && $defaults['member_id'][0] !=0)? Support::memberNameFromID($defaults['member_id'][0]) : null }}"/>
	<input id="members-autocomplete-reference" class="report-input members-autocomplete-new" name="reference_number" placeholder="Search by reference" value="{{ !empty($defaults['reference_number']) ? $defaults['reference_number'] : null }}"/>
	<div class="member-balance @if(empty($defaults['member_id'][0]) || $defaults['member_id'][0] ==0) hidden @endif">
		<span class="member-balance-title">Current Balance:</span> <span class="member-balance-value">{{ number_format(Support::getBalanceByUserID($defaults['member_id'][0]), 0, '.', ',') }}</span>
	</div>
</fieldset>