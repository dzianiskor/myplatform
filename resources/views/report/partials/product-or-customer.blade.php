<h5>Type of Report</h5>

<div class="checkbox-wrap">
    <label>
    	@if(Input::get('filter1') == 'member')
        	<input type="radio" name="report_type" value="member" class="filter1" checked="checked"> Members     
        @else
        	<input type="radio" name="report_type" value="member" class="filter1"> Members     
        @endif
    </label>
    <label>
    	@if(Input::get('filter1') == 'product')
        	<input type="radio" name="report_type" value="product" class="filter1" checked="checked"> Products
        @else
        	<input type="radio" name="report_type" value="product" class="filter1"> Products
        @endif
    </label>    
</div>