<h5>Select Suppliers <span style="font-size: 12px; font-weight: normal; float:right;">Select All <input type="checkbox" class="selectAllSup"/></span></h5>

@foreach(App\Supplier::where('draft',false)->orderBy('name')->pluck('name','id') as $supplierId => $supplierName)
    <div class="checkbox-wrap">
        <label class="supplier-name">
            <a href="#" class="plus"><i class="fa fa-caret-right fa-lg"></i></a>

            @if(in_array($supplierId, $defaults['suppliers']))
                <input type="checkbox" name="supplier[]" class="supplier_id" value="{{ $supplierId }}" checked="checked"/> {{ $supplierName}}
            @else
                <input type="checkbox" name="supplier[]" class="supplier_id" value="{{ $supplierId}}" /> {{ $supplierName }}
            @endif
        </label>
    </div>
@endforeach