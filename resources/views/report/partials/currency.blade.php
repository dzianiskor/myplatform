<?php
$currencies = App\Currency::where('draft', false)->orderBy('name', 'ASC')->get();
if($defaults['currency'] == 'not'){
	$defaults['currency']	= 6;
};
?>
<h5>Select Currency </h5>
<select name="currencies" id="currencies" style="width: 90%;">
		<option value="0">Select a currency</option>
	@foreach($currencies as $currency)
		<option value="<?php echo $currency->id; ?>" <?php echo $defaults['currency'] == $currency->id ? 'selected' : ''; ?>><?php echo $currency->name; ?></option>
	@endforeach
</select>