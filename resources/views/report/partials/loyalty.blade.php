<h5>Select Loyalty Programs <span style="font-size: 12px; font-weight: normal; float:right;">Select All <input type="checkbox" class="selectAll"/></span></h5>

@foreach(Auth::User()->managedPartners() as $partner)
    @if($partner->loyaltyPrograms->count() > 0)
        <div class="checkbox-wrap">
            <label class="partner-name">
                <a href="#" class="plus"><i class="fa fa-caret-right fa-lg"></i></a>
                @if(in_array($partner->id, $defaults['parent_ids']))
                    <input name="parent_ids[]" value="{{ $partner->id }}" type="checkbox" class="parent_ids parent" checked='checked'/> {{ $partner->name }}
                @else
                    <input name="parent_ids[]" value="{{ $partner->id }}" type="checkbox" class="parent_ids parent"/> {{ $partner->name }}
                @endif
            </label>
            @foreach($partner->loyaltyPrograms as $program)
                <label class="entity-name">
                    @if(in_array($program->id, $defaults['entities']))
                        <input type="checkbox" name="entity_id[]" value="{{ $program->id }}" class="entity_id child"  checked="checked" /> {{ $program->name }}
                    @else
                        <input type="checkbox" name="entity_id[]" value="{{ $program->id }}" class="entity_id child" /> {{ $program->name }}
                    @endif
                </label>
            @endforeach
        </div>
    @endif
@endforeach