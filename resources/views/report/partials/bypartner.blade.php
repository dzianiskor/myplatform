<h5>Detail Level</h5>
<div class="checkbox-wrap">
    <label class="partner-name">
        <input type="checkbox" class="parent bypartner" name="bypartner" value="bypartner" 
               @if($defaults['bypartner'] != 'not')
                    checked="checked"
               @endif
               /> By Partner
    </label>
</div>