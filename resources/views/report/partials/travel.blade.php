<h5>Select Travel  <span style="font-size: 12px; font-weight: normal; float:right;">Select All <input type="checkbox" class="selectAllSup"/></span></h5>

    <div class="checkbox-wrap">
        <label class="travel-name">
            <a href="#" class="plus"><i class="fa fa-caret-right fa-lg"></i></a>
            @if(in_array('Flight', $defaults['travels']))
                <input type="checkbox" name="travel[]" class="travel_id" value="Flight" checked="checked"/> Flight
			@else
			<input type="checkbox" name="travel[]" class="travel_id" value="Flight"/> Flight
            @endif
        </label>
    </div>

    <div class="checkbox-wrap">
        <label class="travel-name">
            <a href="#" class="plus"><i class="fa fa-caret-right fa-lg"></i></a>
            @if(in_array('Hotel', $defaults['travels']))
                <input type="checkbox" name="travel[]" class="travel_id" value="Hotel" checked="checked"/> Hotel
			@else
                <input type="checkbox" name="travel[]" class="travel_id" value="Hotel"/> Hotel
            @endif
        </label>
    </div>

    <div class="checkbox-wrap">
        <label class="travel-name">
            <a href="#" class="plus"><i class="fa fa-caret-right fa-lg"></i></a>
            @if(in_array('Car', $defaults['travels']))
                <input type="checkbox" name="travel[]" class="travel_id" value="Car" checked="checked"/> Car
			@else
                <input type="checkbox" name="travel[]" class="travel_id" value="Car"/> Car
            @endif
        </label>
    </div>