<h5>Select Stores</h5>

@foreach(Auth::User()->managedPartners() as $partner)
    @if($partner->stores->count() > 0)
        <div class="checkbox-wrap">
            <label class="partner-name">
                <a href="#" class="plus"><i class="fa fa-caret-right fa-lg"></i></a>

                @if(in_array($partner->id, $defaults['parent_ids']))
                    <input name="parent_ids[]" value="{{ $partner->id }}" type="checkbox" class="parent_ids parent" checked='checked'/> {{ $partner->name }}
                @else
                    <input name="parent_ids[]" value="{{ $partner->id }}" type="checkbox" class="parent_ids parent"/> {{ $partner->name }}
                @endif
            </label>
            @foreach($partner->stores as $store)
                <label class="entity-name" style="display: none;">
                    @if(in_array($store->id, $defaults['entities']))
                        <input type="checkbox" name="entity_id[]" value="{{ $store->id }}" class="entity_id child"  checked="checked" /> {{ $store->name }}
                    @else
                        <input type="checkbox" name="entity_id[]" value="{{ $store->id }}" class="entity_id child" /> {{ $store->name }}
                    @endif
                </label>
            @endforeach
        </div>
    @endif
@endforeach