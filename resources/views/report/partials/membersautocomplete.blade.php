<h5>Select Member</h5>

<style>
  .ui-autocomplete-loading {
    background:url(/img/loader.gif) no-repeat center right;
  }
  li.ui-menu-item{
  	font-size:12px;
  }
</style>

<fieldset>
	<input type="hidden" name="member_id" class="member_id" value="{{ (!empty($defaults['member_id'][0]) && $defaults['member_id'][0] != 0)? $defaults['member_id'][0] : 0 }}" />
	<input id="members-autocomplete-new" class="report-input members-autocomplete-new" placeholder="Search" value="{{ (!empty($defaults['member_id'][0]) && $defaults['member_id'][0] !=0)? Support::memberNameFromID($defaults['member_id'][0]) : null }}" />
</fieldset>

