@if($report->has_chart)
	<a href="#table" class="report-type inline-control active"><i class="fa fa-th"></i> View Table</a>            
	<a href="#chart" class="report-type inline-control"><i class="fa fa-bar-chart-o"></i> View Charts</a>            
@endif
<a href="{{ ExportHelper::generateDownloadPath() }}" target="_blank" class="pure-button pure-button-primary">Export Excel</a>
<a href="/dashboard/reports/tiers/{{ base64_encode($report->report_tier_id) }}"  class="pure-button">All Reports</a>