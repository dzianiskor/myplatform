<?php
$networks = Auth::User()->managedNetworks();
if($defaults['network_ids'][0] == 'not') {
    $defaults['network_ids'][0] = 1;
}
?>
<h5>Select Network</h5>
<select name="network_ids" id="network_ids" style="width: 90%;">
    @foreach($networks as $network)
        <?php
        $networkId	 = $network->id;
        $networkName = $network->name;
        ?>
        <option value="<?php echo $networkId; ?>" <?php echo $defaults['network_ids'][0] == $networkId ? 'selected' : ''; ?>>
            <?php echo $networkName; ?>
        </option>
    @endforeach
</select>