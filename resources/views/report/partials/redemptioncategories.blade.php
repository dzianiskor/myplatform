<h5>Select Categories <span style="font-size: 12px; font-weight: normal; float:right;">Select All <input type="checkbox" class="selectAllRedCat"/></span></h5>
<div class="checkbox-wrap">
	<label class="category-name">
		<a href="#" class="plus"><i class="fa fa-caret-right fa-lg"></i></a>
		<input type="checkbox" name="redemptioncategories[]" class="redemptioncategories" value="cashback" checked="checked"/> Cashback
	</label>
</div>
<div class="checkbox-wrap">
	<label class="category-name">
		<a href="#" class="plus"><i class="fa fa-caret-right fa-lg"></i></a>
		<input type="checkbox" name="redemptioncategories[]" class="redemptioncategories" value="Flights" checked="checked"/> Flights
	</label>
</div>
<div class="checkbox-wrap">
	<label class="category-name">
		<a href="#" class="plus"><i class="fa fa-caret-right fa-lg"></i></a>
		<input type="checkbox" name="redemptioncategories[]" class="redemptioncategories" value="Hotels" checked="checked"/> Hotels
	</label>
</div>
<div class="checkbox-wrap">
	<label class="category-name">
		<a href="#" class="plus"><i class="fa fa-caret-right fa-lg"></i></a>
		<input type="checkbox" name="redemptioncategories[]" class="redemptioncategories" value="Cars" checked="checked"/> Cars
	</label>
</div>
<div class="checkbox-wrap">
	<label class="category-name">
		<a href="#" class="plus"><i class="fa fa-caret-right fa-lg"></i></a>
		<input type="checkbox" name="redemptioncategories[]" class="redemptioncategories" value="itemsatsuppliers" checked="checked"/> Items at suppliers
	</label>
</div>
<div class="checkbox-wrap">
	<label class="category-name">
		<a href="#" class="plus"><i class="fa fa-caret-right fa-lg"></i></a>
		<input type="checkbox" name="redemptioncategories[]" class="redemptioncategories" value="itemsatselectedpartners" checked="checked"/> Items at selected partners
	</label>
</div>
<div class="checkbox-wrap">
	<label class="category-name">
		<a href="#" class="plus"><i class="fa fa-caret-right fa-lg"></i></a>
		<input type="checkbox" name="redemptioncategories[]" class="redemptioncategories" value="transfer" checked="checked"/> Points transferred
	</label>
</div>