<h5>Select Stores <span style="font-size: 12px; font-weight: normal; float:right;">Select All <input type="checkbox" class="selectAll"/></span></h5>

@foreach(Auth::User()->managedPartners() as $partner)
    @if($partner->stores->count() > 0 && (!isset($networkId) || $partner->networks->contains('id', $networkId)))
        <div class="checkbox-wrap">
            <label class="partner-name">
                <a href="#" class="plus"><i class="fa fa-caret-right fa-lg"></i></a>
                @if(isset($defaults['parent_ids']) && in_array($partner->id, $defaults['parent_ids']))
                    <input name="parent_ids[]" value="{{ $partner->id }}" type="checkbox" class="parent_ids parent" checked='checked'/> {{ $partner->name }}
                @else
                    <input name="parent_ids[]" value="{{ $partner->id }}" type="checkbox" class="parent_ids parent"/> {{ $partner->name }}
                @endif
            </label>
            @foreach($partner->stores as $store)
                <label class="entity-name">
                    @if(isset($defaults['entities']) && in_array($store->id, $defaults['entities']))
                        <input type="checkbox" name="entity_id[]" value="{{ $store->id }}" class="entity_id child"  checked="checked" /> {{ $store->name }}
                    @else
                        <input type="checkbox" name="entity_id[]" value="{{ $store->id }}" class="entity_id child" /> {{ $store->name }}
                    @endif
                </label>
            @endforeach
        </div>
    @endif
@endforeach