@if(count($offertranslations) > 0)
  @foreach($offertranslations as $offertranslation)
    <tr>
        <td>{{ $offertranslation->id }}</td>
        <td>{{ $offertranslation->name }}</td>
        <td>
            <?php
                $lang = App\Language::where('id', $offertranslation->lang_id)->first();

                echo $lang->name;
            ?>
        </td>
        <td>
            @if(!I::can('delete_offer') && !I::can('edit_offer'))
                N/A
            @endif
            @if(I::can('edit_offer'))
                <a href="{{ url('dashboard/offertranslation/edit/'.base64_encode($offertranslation->id)) }}" class="edit-offertranslation" title="Edit">Edit</a>
            @endif
            @if(I::can('delete_offer'))
                <a href="{{ url('dashboard/offertranslation/delete/'.base64_encode($offertranslation->id)) }}" class="delete-offertranslation" title="Delete">Delete</a>
            @endif
        </td>
    </tr>
  @endforeach
@else
    <tr class="empty">
        <td colspan="4">No offertranslation defined</td>
    </tr>
@endif
