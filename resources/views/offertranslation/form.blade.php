<div style="width: 800px !important;">
{{ Form::open(array('url' => url('dashboard/offertranslation/update/'.base64_encode($offertranslation->id)), 'method' => 'post', 'class' => "pure-form pure-form-stacked", 'id' => 'new-offertranslation-form')) }}
    
    @include('partials.errors')                         
    <div class="pure-g">
        <div class="pure-u-1">
            {{ Form::label('offertranslation_lang', 'Language') }}
            {{ Form::select('offertranslation_lang',$languages, null, array('class' => 'pure-input-1')) }}
            
            {{ Form::label('offertranslation_name', 'Name') }}
            {{ Form::text('offertranslation_name', $offertranslation->name, array('class' => 'pure-input-1 required')) }}
            
            {{ Form::label('offertranslation_short_description', 'Short Description') }}
            {{ Form::textarea('offertranslation_short_description', $offertranslation->short_description, array('class' => 'pure-input-1')) }}
            
            {{ Form::label('offertranslation_description', 'Description') }}
            {{ Form::textarea('offertranslation_description', $offertranslation->description, array('class' => 'pure-input-1')) }}
            
            {{ Form::label('offertranslation_terms_and_condition', 'Terms & Conditions') }}
            {{ Form::textarea('offertranslation_terms_and_condition', $offertranslation->terms_and_condition, array('class' => 'pure-input-1')) }}
            
            {{ Form::label('offertranslation_email_text_promo_1', 'Email Text Promo 1') }}
            {{ Form::textarea('offertranslation_email_text_promo_1', $offertranslation->email_text_promo_1, array('class' => 'pure-input-1')) }}
            
            {{ Form::label('offertranslation_email_text_promo_2', 'Email Text Promo 2') }}
            {{ Form::textarea('offertranslation_email_text_promo_2', $offertranslation->email_text_promo_2, array('class' => 'pure-input-1')) }}

            
        </div>

    </div>

    {{-- Dialog Buttons --}}
    <div class="form-buttons">
        <div class="left">
            <a href="#" class="pure-button close-dialog">Cancel</a>
        </div>
        <div class="right">
            <a href="#" class="pure-button pure-button-primary save-new-offertranslation">Save</a>
        </div>
        <div class="clearfix"></div>
    </div>
    {{-- /Dialog Buttons --}}

{{ Form::close() }}
</div>