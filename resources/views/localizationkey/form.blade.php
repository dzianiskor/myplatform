<fieldset>
	<legend>Basic Details</legend>
        
    {{ Form::label('key', 'Key', array('for'=>'key')) }}
    {{ Form::text('key', Input::get('key'), array('class'=>'pure-input-1')) }}
    
    {{ Form::label('description', 'Description', array('for'=>'description')) }}
    {{ Form::text('description', Input::get('description'), array('class'=>'pure-input-1 required')) }}
    <?php
        if(isset($localizationkey)){
            $is_platform_key = $localizationkey->is_platform_key;
        }
        else{
            $is_platform_key = 0;
        }
    ?>
    {{ Markup::checkbox('is_platform_key', 'Platform Key', $is_platform_key) }}

</fieldset>
