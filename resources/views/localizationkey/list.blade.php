@extends('layouts.dashboard')

@section('title')
    Localization Keys | BLU
@stop

@section('body')
<div class="padding">

    <div class="content-head pure-g">
        <div class="title pure-u-1-2">
            <h1><i class="fa fa-globe fa-fw"></i>Localization Keys ({{ $localizationkeys->total() }})</h1>
        </div>

        <div class="controls pure-u-1-2">
            <form action="{{ Filter::searchUrl() }}" method="get">
                <input type="text" placeholder="Search" name="q" value="{{ Input::get('q') }}" />
                <button type="submit" title="" class="list-search"></button>
            </form>
            
                <a href="{{ url('dashboard/localizationkeys/new') }}" class="pure-button pure-button-primary">Add Localizationkeys</a>

        </div>
    </div>

    <table class="pure-table">
        <thead>
            <tr>
                <th width="50"><a href="{{ Filter::baseUrl('sort=id') }}">ID</a></th>
                <th style="text-align:left;padding-left:20px;"><a href="{{ Filter::baseUrl('sort=key') }}">Key</a></th>
                <th>Description</th>
                <th width="150">Actions</th>
            </tr>
        </thead>

        <tbody>
        @if(count($localizationkeys) > 0)
            @foreach ($localizationkeys as $localizationkey)
                <tr>
                    <td>
                        {{{ $localizationkey->id }}}
                    </td>
                    <td style="text-align:left;padding-left:20px;">
                        <a href="{{ url('dashboard/localizationkeys/view/'.base64_encode($localizationkey->id)) }}" class="edit" title="Edit">{{{ $localizationkey->key }}}</a>
                    </td>
                    <td>
                        {{{ $localizationkey->description}}}
                    </td>
                    
                    <td>
                        <a href="{{ url('dashboard/localizationkeys/view/'.base64_encode($localizationkey->id)) }}" class="edit" title="View">View</a>

                        @if(I::can('delete_localizationkeys'))
                            <a href="{{ url('dashboard/localizationkeys/delete/'.base64_encode($localizationkey->id)) }}" class="delete" title="Delete">Delete</a>
                        @endif
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="5">No Localizationkeys Found</td>
            </tr>
        @endif
        </tbody>
    </table>

    {{ $localizationkeys->setPath('localizationkeys')->appends(Input::except('page'))->links() }}

</div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-localizationkeys');
    </script>
@stop
