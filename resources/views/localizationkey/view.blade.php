@extends('layouts.dashboard')

@section('title')
    Update Localization Key | BLU
@stop

@section('body')
    <div class="padding">

        <div class="content-head pure-g">
            <div class="title pure-u-1-2">
                <h1><i class="fa fa-globe fa-fw"></i>Localization Key &raquo; {{{ $localizationkey->key }}}</h1>
            </div>

            <div class="controls pure-u-1-2">
                <a href="{{ url('/dashboard/localizationkeys') }}" class="pure-button">All Localization Keys</a>
            </div>
        </div>

        {{ Form::model($localizationkey, array('action' => array('LocalizationkeyController@saveLocalizationkey', base64_encode($localizationkey->id)),
                                       'class'  => 'pure-form pure-form-stacked form-width')) }}

            @include('partials.errors')                                       
            @include('localizationkey/form', array('localizationkey' => $localizationkey))

            {!! Form::hidden('redirect_to', URL::previous()) !!}

            <!-- Form Buttons -->
            <div class="form-buttons">
                <div class="left">
                    <a href="{{ url('dashboard/localizationkeys') }}" class="pure-button">Cancel</a>
                </div>
                <div class="right">
                    <button type="submit" class="pure-button pure-button-primary">Save Localizationkey</button>
                </div>
                <div class="clearfix"></div>
            </div>
            <!-- /Form Buttons -->
        {{ Form::close() }}        
    </div>
@stop

@section('page_script')
@if(!I::can_edit('localizationkeys', $localizationkey))
 <!--<script>
     enqueue_script('disable-form');
 </script>-->
@endif
    <script>
        set_menu('mnu-localizationkeys');
    </script>
@stop
