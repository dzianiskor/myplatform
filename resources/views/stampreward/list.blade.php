@extends('layouts.dashboard')

@section('title')
    Stamp Programs | BLU
@stop

@section('body')
    <div class="padding">

        <div class="content-head pure-g">
            <div class="title pure-u-1-2">
                <h1><i class="fa fa-star fa-fw"></i> Stamp Programs ({{ count($stamprewards) }})</h1>
            </div>

            <div class="controls pure-u-1-2">
                <form action="{{ Filter::searchUrl() }}" method="get">
                    <input type="text" placeholder="Search" name="q" value="{{ Input::get('q') }}" />
                    <button type="submit" title="" class="list-search"></button>
                </form>
                @if(I::can('create_stampreward'))
                    <a href="{{ url('dashboard/stampreward/create') }}" class="pure-button pure-button-primary">New Reward</a>
                @endif
            </div>
        </div>

        <table class="pure-table">
            <thead>
                <tr>
                    <th width="50">ID</th>
                    <th style="text-align:left;padding-left:20px;"><a href="{{ Filter::baseUrl('sort=name') }}">Name</a></th>
                    <th><a href="{{ Filter::baseUrl('sort=partner_id') }}">Partner</a></th>
                    <th>Number of Stamps</th>
                    <th>Status</th>
                    <th width="150">Actions</th>
                </tr>
            </thead>
            <tbody>
            @if(count($stamprewards) > 0)
                @foreach ($stamprewards as $p)
                    <tr>
                        <td>
                            {{ $p->id }}
                        </td>
                        <td style="text-align:left;padding-left:20px;">
                            {{{ $p->name }}}
                        </td>                      
                        <td>
                            {{{ $p->partner->name }}}
                        </td>
                        <td>
                            {{{ $p->stamp_number }}}
                        </td>
                                        
                        <td>
                            @if($p->display_online == TRUE)
                                <span class="ico-yes"></span>
                            @else
                                <span class="ico-no"></span>
                            @endif
                        </td>
                        
                        <td>
                            @if(I::can('view_stampreward'))
                                <a href="/dashboard/stampreward/edit/{{ base64_encode($p->id) }}" class="edit" title="View">View</a>
                            @endif
                            
                            @if(I::can('delete_stampreward'))
                                <a href="/dashboard/stampreward/delete/{{ base64_encode($p->id) }}" class="delete" title="Delete">Delete</a>
                            @endif
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="9">No Stamp Programs Found</td>
                </tr>
            @endif
            </tbody>
        </table>

        
    </div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-stampreward');
    </script>
@stop
