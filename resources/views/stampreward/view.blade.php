@extends("layouts.dashboard")
<?php
//    echo "<pre>";
//    var_dump($partners);
//    
//    echo "<BR> Stamp Reward";
//    var_dump($stampreward);
//    exit();
?>
@section('title')
    Stamp | BLU
@stop

@section('js_globals')
    var PROGRAM_ID = {{ $stampreward->id }};
@stop


@section('body')

    <div class="padding">

        <div class="content-head pure-g">
            <div class="title pure-u-1-2">
                @if($stampreward->draft)
                    <h1><i class="fa fa-star fa-fw"></i>New Stamp Program</h1>
                @else
                    <h1><i class="fa fa-star fa-fw"></i>Stamp Program &raquo; {{{ $stampreward->name }}}</h1>
                @endif
            </div>

            <div class="controls pure-u-1-2">
                <a href="{{ url('dashboard/stampreward') }}" class="pure-button">All Programs</a>
            </div>
        </div>

        {{ Form::model($stampreward, array('url'   => '/dashboard/stampreward/update/'.base64_encode($stampreward->id),
                                       'class' => 'pure-form pure-form-stacked form-width main-form', 
                                       'files'  => true,
                                       'id'    => 'stamprewardForm')) }}

            @include('partials.errors')

            <fieldset>
                <legend>
                    Basic Details
                </legend>

                {{ Form::label('partner_id', 'Partner') }}
                <select name="partner_id" class="pure-input-1-3">
                    <option disabled="disabled">Select Partner</option>
                    @foreach($partners as $key => $p)
                        @if($key == $stampreward->partner_id)
                            <option value="{{ $key }}" selected>{{ $p }}</option>
                        @else
                            <option value="{{ $key }}">{{ $p }}</option>
                        @endif
                    @endforeach
                </select>

                {{ Form::label('name', 'Program Name') }}
                {{ Form::text('name', $stampreward->name, array('class' => 'pure-input-1 required')) }}

                {{ Form::label('stamp_number', 'Number of Stamp') }}
                {{ Form::text('stamp_number', $stampreward->stamp_number, array('class' => 'pure-input-1 required')) }}

                
                <div class="image-rail" style="width:340px;">
                    <table style="margin-top:20px;margin-bottom:20px;">
                        <tr>
                            <td width="80">
                                @if(empty($stampreward->image))
                                    <img src="https://placehold.it/74x74" alt="" />
                                @else
                                    <img src="<?php echo App\Http\Controllers\MediaController::getImageFromId( $stampreward->image ) ?>?s=74x74" alt="" />
                                @endif
                            </td>
                            <td style="padding-left:10px;">
                                {{ Form::label('image', 'Stamp Reward Image') }}
                                {{ Form::file('image') }}
                            </td>
                        </tr>
                    </table>
                </div>
                
                {{ Form::label('display_online', 'Display Online') }}

                <select name="display_online" class="pure-input-1-3">
                    
                        @if($stampreward->display_online == 0)
                            <option value="0" selected>No</option>
                            <option value="1">Yes</option>
                        @else
                            <option value="0">No</option>
                            <option value="1"  selected>Yes</option>
                        @endif
                </select>
            </fieldset>

            {{--- Form Buttons ---}}
            <div class="form-buttons">
                <div class="left">
                    <a href="{{ url('dashboard/stampreward') }}" class="pure-button">Cancel</a>
                </div>

                
                    @if(I::can('edit_stampreward'))
                        <div class="right">
                            <button type="submit" class="pure-button pure-button-primary">Save Program</button>
                        </div>
                    @endif

                <div class="clearfix"></div>
            </div>
            {{--- /Form Buttons ---}}          
        
        {{ Form::close() }}
    </div>
@stop

@section('page_script')
    <script>

        @if(!I::can('edit_stampreward') && !I::can('create_stampreward'))
            disableForm('#stamprewardForm');
        @endif

        set_menu('mnu-stampreward');   

               
    </script>
@stop