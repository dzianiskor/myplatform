@if(count($mobilePush->segments) > 0)
    @foreach($mobilePush->segments as $s)
        <li>
            {{ $s->name }} <a href="/dashboard/mobilepush/unlink/{{ base64_encode($mobilePush->id) }}/{{ base64_encode($s->id) }}" class="delete-ajax" data-target="#segment-listing">
                [Delete]
            </a>
        </li>
    @endforeach
@else
    <li class="empty">No Segments Associated</li>
@endif