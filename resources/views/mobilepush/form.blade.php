<fieldset>
	<legend>Basic Details</legend>
    {{ Form::label('push_time', 'Push Time', array('for'=>'push_time')) }}
    {{ Form::text('push_time', date("Y/m/d H:i", strtotime($mobilepush->push_time)), array('class' => 'pure-input-1 datetime-now required noInput')) }}
	
	<div class="spacer"></div>
    {{ Form::label('message', 'Message', array('for'=>'message')) }}
    {{ Form::text('message', $mobilepush->message, array('class'=>'pure-input-1 required')) }}
	
	<div class="spacer"></div>
    
    {{ Form::label('activity', 'Activity', array('for'=>'activity')) }}
    {{ Form::text('activity', $mobilepush->activity, array('class'=>'pure-input-1 required')) }}
	
	{{ Form::label('partner_id', 'Partner') }}
	<select name="partner_id" class="pure-input-1-3">
		<option value="">Select Partner</option>
		@foreach($partners as $p)
			@if($p->id == $mobilepush->partner_id)
			<option value="{{ $p->id }}" selected="selected">{{ $p->name }}</option>
			@else
				<option value="{{ $p->id }}">{{ $p->name }}</option>
			@endif
		@endforeach
	</select>
	<div class="spacer"></div>
        <fieldset>
            <span class="info">
                    For best results, use images that are 500x500 (pixels) in size.
            </span>

            <div class="image-rail" style="width:340px;">
                <table class="mobile_push" style="margin-top:20px;margin-bottom:20px;">
                    <tr>
                        <td width="80">
                            @if(empty($mobilepush->image()))
                                <img src="https://placehold.it/74x74" alt="" />
                            @else
                                <img src="<?php echo App\Http\Controllers\MediaController::getImageFromId($mobilepush->image()->id) ?>?s=74x74" alt="" />
                            @endif
                        </td>
                        <td style="padding-left:10px;">
                            {{ Form::label('image', 'Mobile Push Image') }}
                            {{ Form::file('image') }}
                        </td>
                    </tr>
                </table>
            </div>

        </fieldset>
        <div class="spacer"></div>
    
</fieldset>