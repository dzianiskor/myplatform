@extends('layouts.dashboard')

@section('title')
    Mobile Pushes | BLU
@stop

@section('body')
<div class="padding">

    <div class="content-head pure-g">
        <div class="title pure-u-1-2">
            <h1><i class="fa fa-money fa-fw"></i> Mobile Push Notifications ({{ count($mobilepushs) }})</h1>
        </div>

        <div class="controls pure-u-1-2">
            <form action="{{ Filter::searchUrl() }}" method="get">
                <input type="text" placeholder="Search" name="q" value="{{ Input::get('q') }}" />
                <button type="submit" title="" class="list-search"></button>
            </form>
            @if(I::can('create_push_notifications'))
                <a href="{{ url('dashboard/mobilepush/create') }}" class="pure-button pure-button-primary">Add Notification</a>
            @endif
        </div>
    </div>

    <table class="pure-table">
        <thead>
            <tr>
                <th width="10%"><a href="{{ Filter::baseUrl('sort=id') }}">ID</a></th>
                <th style="text-align:center;padding-left:20px;" width="70%"><a href="{{ Filter::baseUrl('sort=message') }}">Message</a></th>
                <th width="20%">Actions</th>
            </tr>
        </thead>
        <tbody>
        @if(count($mobilepushs) > 0)
            @foreach ($mobilepushs as $mobilepush)
                <tr>
                    <td>
                        {{ $mobilepush->id }}
                    </td>
                    <td style="text-align:left;">
                        @if(I::can('view_push_notifications'))
                            <a href="/dashboard/mobilepush/edit/{{{base64_encode($mobilepush->id)}}}" class="edit" title="View">{{{ $mobilepush->message }}}</a>
                        @elseif(!I::can('view_push_notifications'))
                           {{ $mobilepush->message }}
                        @endif
                    </td>
                    <td>
                        @if(I::can('view_push_notifications'))
                            <a href="/dashboard/mobilepush/edit/{{ base64_encode($mobilepush->id) }}" class="edit" title="View">View</a>
                        @endif
                        @if(I::can('delete_push_notifications'))
                            <a href="/dashboard/mobilepush/delete/{{ base64_encode($mobilepush->id) }}" class="delete" title="Delete">Delete</a>
                        @endif
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="5">No Games Found</td>
            </tr>
        @endif
        </tbody>
    </table>

</div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-mobilepush');
    </script>
@stop
