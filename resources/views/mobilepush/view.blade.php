@extends('layouts.dashboard')

@section('title')
    Update Mobile Pushes | BLU
@stop

@section('body')
    <div class="padding">

        <div class="content-head pure-g">
            <div class="title pure-u-1-2">
                <h1><i class="fa fa-money fa-fw"></i> Mobile Push &raquo; {{{ $mobilepush->message }}}</h1>
            </div>

            <div class="controls pure-u-1-2">
                <a href="{{ url('/dashboard/mobilepush') }}" class="pure-button">All Mobile Pushes</a>
            </div>
        </div>

        {{ Form::model($mobilepush, array('url'   => '/dashboard/mobilepush/update/'.base64_encode($mobilepush->id),
                                        'files'  => true,
                                        'method' => 'post',
                                        'class' => 'pure-form pure-form-stacked form-width')) }}

            @include('partials.errors')                                       
            @include('mobilepush/form', array('mobilepush' => $mobilepush))

            {!! Form::hidden('redirect_to', URL::previous()) !!}

            <!-- Form Buttons -->
            <div class="form-buttons">
                @if($mobilepush->draft)
                    @if(I::can('create_push_notifications'))
                        <div class="left">
                            <a href="{{ url('dashboard/mobilepush') }}" class="pure-button">Cancel</a>
                        </div>
                        <div class="right">
                            <button type="submit" class="pure-button pure-button-primary">Save Mobile Push</button>
                        </div>
                    @endif
                @else
                    @if(I::can('edit_push_notifications'))
                        <div class="left">
                            <a href="{{ url('dashboard/mobilepush') }}" class="pure-button">Cancel</a>
                        </div>
                        <div class="right">
                            <button type="submit" class="pure-button pure-button-primary">Update Mobile Push</button>
                        </div>
                    @endif
                @endif

                <div class="clearfix"></div>
            </div>
            <!-- /Form Buttons -->
        {{ Form::close() }}        
    </div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-mobilepush');
    </script>
@stop
