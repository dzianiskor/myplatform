@extends('layouts.dashboard')

@section('title')
    Update Partner Category | BLU
@stop

@section('body')
<div class="padding">

    <div class="content-head pure-g">
        <div class="title pure-u-1-2">
            <h1><i class="fa fa-sort-alpha-desc fa-fw"></i>Partner Category &raquo; {{{ $category->name }}}</h1>
        </div>

        <div class="controls pure-u-1-2">
            <a href="{{ url('/dashboard/partnercategory') }}" class="pure-button">All Categories</a>
        </div>
    </div>

    {{ Form::model($category, array('url'    => url('/dashboard/partnercategory/save/'.base64_encode($category->id)),
                                    'class'  => 'pure-form pure-form-stacked form-width')) }}

        @include('partnercategory/form', array('category' => $category))

        {!! Form::hidden('redirect_to', URL::previous()) !!}

        <!-- Form Buttons -->
        <div class="form-buttons">
            <div class="left">
                <a href="{{ url('dashboard/categories') }}" class="pure-button">Cancel</a>
            </div>
            <div class="right">
                <button type="submit" class="pure-button pure-button-primary">Save Category</button>
            </div>
            <div class="clearfix"></div>
        </div>
        <!-- /Form Buttons -->

    {{ Form::close() }}
</div>
@stop

@section('page_script')
@if(!I::can_edit('categories', $category))
 <script>
     enqueue_script('disable-form');
 </script>
@endif
    <script>
        set_menu('mnu-partnercategories');
    </script>
@stop
