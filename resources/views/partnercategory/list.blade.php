@extends('layouts.dashboard')

@section('title')
    Partner Categories | BLU
@stop

@section('body')
<div class="padding">

    <div class="content-head pure-g">
        <div class="title pure-u-1-2">
            <h1><i class="fa fa-sort-alpha-desc  fa-fw"></i>Partner Categories ({{ $categories->total() }})</h1>
        </div>

        <div class="controls pure-u-1-2">
            <form action="{{ Filter::searchUrl() }}" method="get">
                <input type="text" placeholder="Search" name="q" value="{{ Input::get('q') }}" />
                <button type="submit" title="" class="list-search"></button>
            </form>
            @if(I::can('create_categories'))
                <a href="{{ url('dashboard/partnercategory/new') }}" class="pure-button pure-button-primary">Add Categories</a>
            @endif
        </div>
    </div>

    <table class="pure-table">
        <thead>
            <tr>
                <th width="50"><a href="{{ Filter::baseUrl('sort=id') }}">ID</a></th>
                <th style="text-align:left;padding-left:20px;"><a href="{{ Filter::baseUrl('sort=name') }}">Name</a></th>
                <th width="150">Actions</th>
            </tr>
        </thead>

        <tbody>
        @if(count($categories) > 0)
            @foreach ($categories as $category)
                    <tr>
                        <td>{{ $category->id }}</td>
                        <td style="text-align:left;padding-left:20px;">
                            @if(!empty($category->name))
                                {{{ $category->name }}}
                            @else 
                                <i>None Specified</i>
                            @endif
                        </td>
                        <td>
                            @if($category->id == 1)
                                <a href="{{ url('dashboard/partnercategory/view/'.base64_encode($category->id))}}" class="edit" title="View">View</a>
                            @else
                                <a href="{{ url('dashboard/partnercategory/view/'.base64_encode($category->id)) }}" class="edit" title="View">View</a>

                                @if(I::can('delete_categories'))
                                    <a href="{{ url('dashboard/partnercategory/delete/'.base64_encode($category->id)) }}" class="delete" title="Delete">Delete</a>
                                @endif
                            @endif
                        </td>
                    </tr>
            @endforeach
        @else
            <tr>
                <td colspan="3">No Partner Categories Found</td>
            </tr>
        @endif
        </tbody>
    </table>

    {{ $categories->setPath('partnercategory')->appends(Input::except('page'))->links() }}

</div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-partnercategories');
    </script>
@stop
