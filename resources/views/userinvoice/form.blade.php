
{{ Form::open(array('url' => url('dashboard/userinvoice/update/'.base64_encode($userinvoice->id)), 'method' => 'post', 'class' => "pure-form pure-form-stacked", 'id' => 'new-userinvoice-form')) }}
    
    @include('partials.errors')                         

    <div class="pure-g">
        <div class="pure-u-1">

            <label>User: {{ $userinvoice->user()->first_name }} - Phone {{ $userinvoice->user()->normalized_mobile }}</label>
            
            {{ Markup::checkbox("inserted", "Processed", $userinvoice->inserted, 1) }}

            
            
            <td>@if(empty($userinvoice->image()->media_id))
                    <img src="https://placehold.it/380x380" alt="" />
                @else
                    <img src="<?php echo App\Http\Controllers\MediaController::getImageFromId( $userinvoice->image()->media_id ) ?>?s=380x380" alt="" />
                @endif</td>
        </div>

        
    </div>

    {{-- Dialog Buttons --}}
    <div class="form-buttons">
        <div class="left">
            <a href="#" class="pure-button close-dialog">Cancel</a>
        </div>
        <div class="right">
            <a href="#" class="pure-button pure-button-primary save-new-userinvoice">Save</a>
        </div>
        <div class="clearfix"></div>
    </div>
    {{-- /Dialog Buttons --}}

{{ Form::close() }}