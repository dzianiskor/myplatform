@extends('layouts.dashboard')
@section('title')

        Invoices &raquo; New  | BLU
@stop

@section('body')
{{--- Invoices ---}}
            <fieldset>
                <legend>Invoice</legend>

                <table id="userinvoice-list" class="pure-table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Picture</th>
                            <th>User</th>
                            <th>Processed</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                       @if(count($userinvoices) > 0)
                          @foreach($userinvoices as $userinvoice)
                                <?php

                                    $uinvoice = App\UserInvoice::find($userinvoice->id);

//                                    ?>
                                <tr>
                                    <td>{{ $userinvoice->id }}</td>

                                    <td>@if(empty($uinvoice->image()->media_id))
                                            <a href="{{ url('dashboard/userinvoice/edit/'.base64_encode($userinvoice->id)) }}" class="edit-userinvoice" title="Edit"><img src="https://placehold.it/74x74" alt="" /></a>
                                        @else
                                        <a href="{{ url('dashboard/userinvoice/edit/'.base64_encode($userinvoice->id)) }}" class="edit-userinvoice" title="Edit"><img src="<?php echo App\Http\Controllers\MediaController::getImageFromId( $uinvoice->image()->media_id ) ?>?s=74x74" alt="" /></a>
                                        @endif</td>
                                    <td>{{ $uinvoice->user()->first_name }} {{ $uinvoice->user()->last_name }}</td>
                                    <td>
                                        @if($uinvoice->inserted == 1)
                                            <i class="fa fa-check fa-2x" style="color:#00aa00;"></i>
                                        @else
                                            <i class="fa fa-times fa-2x" style="color:#aa0000;"></i>
                                        @endif
                                    </td>
                                    <td>

                                            <a href="{{ url('dashboard/userinvoice/edit/'.base64_encode($userinvoice->id)) }}" class="edit-userinvoice" title="Edit">Edit</a>


                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr class="empty">
                                <td colspan="4">No invoice  defined</td>
                            </tr>
                        @endif
                    </tbody>
                </table>




            </fieldset>
            {{--- /Invoices ---}}
            @stop
@section('page_script')
<script>
        set_menu('mnu-userinvoice');

        enqueue_script('new-invoice');

    </script>
    @stop