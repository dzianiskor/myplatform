<div style="width: 800px !important;">
{{ Form::open(array('url' => url('dashboard/estatementtranslation/create/'), 'method' => 'post', 'class' => "pure-form pure-form-stacked", 'id' => 'new-estatementtranslation-form')) }}
    
    @include('partials.errors')                         

    <div class="pure-g">
        <div class="pure-u-1">
            {{ Form::hidden('estatementtranslation_eid',$estatement_id, null, array('class' => 'pure-input-1')) }}
            {{ Form::label('estatementtranslation_lang', 'Language') }}
            {{ Form::select('estatementtranslation_lang',$languages, null, array('class' => 'pure-input-1')) }}
            
            {{ Form::label('estatementtranslation_Title', 'Title') }}
            {{ Form::text('estatementtranslation_Title', '', array('class' => 'pure-input-1 required')) }}
            
            {{ Form::label('estatementtranslation_description', 'Description') }}
            {{ Form::textarea('estatementtranslation_description','', array('class' => 'pure-input-1')) }}
        </div>

    </div>

    {{-- Dialog Buttons --}}
    <div class="form-buttons">
        <div class="left">
            <a href="#" class="pure-button close-dialog">Cancel</a>
        </div>
        <div class="right">
            <a href="#" class="pure-button pure-button-primary save-new-estatementtranslation">Save</a>
        </div>
        <div class="clearfix"></div>
    </div>
    {{-- /Dialog Buttons --}}

{{ Form::close() }}
</div>