@if(count($estatementtranslations) > 0)
  @foreach($estatementtranslations as $estatementtranslation)
    <tr>
        <td>{{ $estatementtranslation->id }}</td>
        <td>{{ $estatementtranslation->Title }}</td>
        <td>{{ $estatementtranslation->description }}</td>
        <td>
            <?php
                $lang = App\Language::where('id', $estatementtranslation->lang_id)->first();
                echo $lang->name;
            ?>
        </td>
        <td>
            @if(!I::can('delete_estatement') && !I::can('edit_estatement'))
                N/A
            @endif
            @if(I::can('edit_estatement'))
                <a href="{{ url('dashboard/estatementtranslation/edit/'.base64_encode($estatementtranslation->id)) }}" class="edit-estatementtranslation" title="Edit">Edit</a>
            @endif
            @if(I::can('delete_estatement'))
                <a href="{{ url('dashboard/estatementtranslation/delete/'.base64_encode($estatementtranslation->id)) }}" class="delete-estatementtranslation" title="Delete">Delete</a>
            @endif
        </td>
    </tr>
  @endforeach
@else
    <tr class="empty">
        <td colspan="4">No estatementtranslation defined</td>
    </tr>
@endif
