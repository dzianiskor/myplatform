<div style="width: 800px !important;">
{{ Form::open(array('url' => url('dashboard/prodtranslation/update/'.$prodtranslation->id), 'method' => 'post', 'class' => "pure-form pure-form-stacked", 'id' => 'new-prodtranslation-form')) }}
    
    @include('partials.errors')                         

    <div class="pure-g">
        <div class="pure-u-1">
            {{ Form::label('prodtranslation_lang', 'Language') }}
            {{ Form::select('prodtranslation_lang',$languages, null, array('class' => 'pure-input-1')) }}
            
            {{ Form::label('prodtranslation_name', 'Name') }}
            {{ Form::text('prodtranslation_name', $prodtranslation->name, array('class' => 'pure-input-1 required')) }}
            
            {{ Form::label('prodtranslation_model', 'Model') }}
            {{ Form::text('prodtranslation_model', $prodtranslation->model, array('class' => 'pure-input-1')) }}

            {{ Form::label('prodtranslation_sub_model', 'Sub Model') }}
            {{ Form::text('prodtranslation_sub_model', $prodtranslation->sub_model, array('class' => 'pure-input-1')) }}

            {{ Form::label('prodtranslation_description', 'Description') }}
            {{ Form::textarea('prodtranslation_description', $prodtranslation->description, array('class' => 'pure-input-1')) }}
        </div>

    </div>

    {{-- Dialog Buttons --}}
    <div class="form-buttons">
        <div class="left">
            <a href="#" class="pure-button close-dialog">Cancel</a>
        </div>
        <div class="right">
            <a href="#" class="pure-button pure-button-primary save-new-prodtranslation">Save</a>
        </div>
        <div class="clearfix"></div>
    </div>
    {{-- /Dialog Buttons --}}

{{ Form::close() }}
</div>