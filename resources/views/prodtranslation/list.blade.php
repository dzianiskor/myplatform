@if(count($prodtranslations) > 0)
  @foreach($prodtranslations as $prodtranslation)
    <tr>
        <td>{{ $prodtranslation->id }}</td>
        <td>{{ $prodtranslation->name }}</td>
        <td>
            <?php
                $lang = App\Language::where('id', $prodtranslation->lang_id)->first();

                echo $lang->name;
            ?>
        </td>
        <td>
            @if(!I::can('delete_products') && !I::can('edit_products'))
                N/A
            @endif
            @if(I::can('edit_products'))
                <a href="{{ url('dashboard/prodtranslation/edit/'.$prodtranslation->id) }}" class="edit-prodtranslation" title="Edit">Edit</a>
            @endif
            @if(I::can('delete_products'))
                <a href="{{ url('dashboard/prodtranslation/delete/'.$prodtranslation->id) }}" class="delete-prodtranslation" title="Delete">Delete</a>
            @endif        </td>
    </tr>
  @endforeach
@else
    <tr class="empty">
        <td colspan="4">No prodtranslation defined</td>
    </tr>
@endif
