@if(count($items) > 0)
    @foreach($items as $c)
        <li>{{ $c->number }} <a href="/dashboard/reference/unlink/{{ $c->user_id }}/{{ $c->id }}" class="delete-ajax" data-target="#reference-listing">[Delete]</a></li>
    @endforeach
@else
    <li class="empty">No References Associated</li>
@endif