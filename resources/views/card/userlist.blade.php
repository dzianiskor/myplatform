@if(count($cards) > 0)
    @foreach($cards as $c)
        <li>{{ $c->number }} <a href="/dashboard/cards/unlink/{{ $c->user_id }}/{{ $c->id }}" class="delete-ajax" data-target="#card-listing">[Delete]</a></li>
    @endforeach
@else
    <li class="empty">No Cards Associated</li>
@endif