{{-- Assign new User --}}
<div id="dialog-add-user" style="width:300px;">
    <h4>Associate to User</h4>

    {{ Form::open(array('class' => "pure-form pure-form-stacked", 'onkeypress' => "return event.keyCode != 13;")) }}
    <fieldset>
        <input type="hidden" name="user_id" id="user_id" class="user_id" value="" />
        <input type="text" id="members-autocomplete" class="report-input members-autocomplete pure-input-1-1" placeholder="Search Users" value="{{ (!empty(Input::get('entity')))? Support::memberNameFromID(Input::get('entity')) : null }}" />
    </fieldset>
    <a href="/dashboard/estatement/linkuser/{{ base64_encode($estatement->id) }}" class="pure-button pure-button-primary attribute-action" data-target="#user-listing">
        Save
    </a>
    {{ Form::close() }}
</div>
{{-- /Assign new User --}}