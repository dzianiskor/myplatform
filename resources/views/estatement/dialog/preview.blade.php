<div id="dialog-preview" style="width: 700px;">
    <h4>Preview</h4>
    <?php
    if ($lang == "ar") {
        $align = "right";
        $otheralign = "left";
        $standard = array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9");
        $arabic = array("٠", "١", "٢", "٣", "٤", "٥", "٦", "٧", "٨", "٩");
        $months = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
        $amonths = array("يناير", "فبراير", "مارس", "أبريل", "مايو", "يونيو", "يوليو", "أغسطس", "سبتمبر", "أكتوبر", "نوفمبر", "ديسمبر");
    } else {
        $align = "left";
        $otheralign = "right";
    }
    ?>


    <style type="text/css">
<?php if ($lang == "ar"): ?>
            #dialog-preview #body{direction:rtl; font-family: 'adobe-arabic' !important}
<?php endif; ?>
        #dialog-preview p{
            margin:10px 0;
            padding:0;
        }
        #dialog-preview table{
            border-collapse:collapse;
        }
        #dialog-preview h1,h2,h3,h4,h5,h6{
            display:block;
            margin:0;
            padding:0;
        }
        #dialog-preview img,a img{
            border:0;
            height:auto;
            outline:none;
            text-decoration:none;
        }
        #dialog-preview body,#bodyTable,#bodyCell{
            height:100%;
            margin:0;
            padding:0;
            width:100%;
        }
        #dialog-preview #outlook a{
            padding:0;
        }
        #dialog-preview img{
            -ms-interpolation-mode:bicubic;
        }
        #dialog-preview table{
            mso-table-lspace:0pt;
            mso-table-rspace:0pt;
        }
        #dialog-preview .ReadMsgBody{
            width:100%;
        }
        #dialog-preview .ExternalClass{
            width:100%;
        }
        #dialog-preview p,a,li,td,blockquote{
            mso-line-height-rule:exactly;
        }
        #dialog-preview a[href^=tel],a[href^=sms]{
            color:inherit;
            cursor:default;
            text-decoration:none;
        }
        #dialog-preview p,a,li,td,body,table,blockquote{
            -ms-text-size-adjust:100%;
            -webkit-text-size-adjust:100%;
        }
        #dialog-preview .ExternalClass,.ExternalClass p,.ExternalClass td,.ExternalClass div,.ExternalClass span,.ExternalClass font{
            line-height:100%;
        }
        #dialog-preview a[x-apple-data-detectors]{
            color:inherit !important;
            text-decoration:none !important;
            font-size:inherit !important;
            font-family:inherit !important;
            font-weight:inherit !important;
            line-height:inherit !important;
        }
        #dialog-preview #bodyCell{
            padding:10px;
        }
        #dialog-preview .templateContainer{
            width:600px !important;
        }
        #dialog-preview a.mcnButton{
            display:block;
        }
        #dialog-preview .mcnImage{
            vertical-align:bottom;
        }
        #dialog-preview .mcnTextContent{
            word-break:break-word;
        }
        #dialog-preview .mcnTextContent img{
            height:auto !important;
        }
        #dialog-preview .mcnDividerBlock{
            table-layout:fixed !important;
        }
        #dialog-preview body,#bodyTable{
            background-color:#e7e7e8;
        }
        #dialog-preview #bodyCell{
            border-top:0;
        }
        #dialog-preview .templateContainer{
            border:0;
        }
        #dialog-preview h1{
            color:#202020;
            font-family:Helvetica;
            font-size:26px;
            font-style:normal;
            font-weight:bold;
            line-height:125%;
            letter-spacing:normal;
            text-align:<?php echo $align; ?>;
        }
        #dialog-preview h2{
            color:#202020;
            font-family:Helvetica;
            font-size:22px;
            font-style:normal;
            font-weight:bold;
            line-height:125%;
            letter-spacing:normal;
            text-align:<?php echo $align; ?>;
        }
        #dialog-preview h3{
            color:#202020;
            font-family:Helvetica;
            font-size:20px;
            font-style:normal;
            font-weight:bold;
            line-height:125%;
            letter-spacing:normal;
            text-align:<?php echo $align; ?>;
        }
        #dialog-preview h4{
            color:#202020;
            font-family:Helvetica;
            /*font-size:5px;*/
            font-style:normal;
            font-weight:bold;
            line-height:125%;
            letter-spacing:normal;
            text-align:<?php echo $align; ?>;
        }
        #dialog-preview #templatePreheader{
            background-color:#fafafa;
            background-image:none;
            background-repeat:no-repeat;
            background-position:center;
            background-size:cover;
            border-top:0;
            border-bottom:0;
            padding-top:9px;
            padding-bottom:9px;
        }
        #dialog-preview #templatePreheader .mcnTextContent,#templatePreheader .mcnTextContent p{
            color:#656565;
            font-family:Helvetica;
            font-size:12px;
            line-height:150%;
            text-align:<?php echo $align; ?>;
        }
        #dialog-preview #templatePreheader .mcnTextContent a,#templatePreheader .mcnTextContent p a{
            color:#656565;
            font-weight:normal;
            text-decoration:underline;
        }
        #dialog-preview #templateHeader{
            background-color:#FFFFFF;
            background-image:none;
            background-repeat:no-repeat;
            background-position:center;
            background-size:cover;
            border-top:0;
            border-bottom:0;
            padding-top:9px;
            padding-bottom:0;
        }
        #dialog-preview #templateHeader .mcnTextContent,#templateHeader .mcnTextContent p{
            color:#202020;
            font-family:Helvetica;
            font-size:16px;
            line-height:150%;
            text-align:<?php echo $align; ?>;
        }
        #dialog-preview #templateHeader .mcnTextContent a,#templateHeader .mcnTextContent p a{
            color:#2BAADF;
            font-weight:normal;
            text-decoration:underline;
        }
        #dialog-preview #templateSidebar{
            background-color:#FFFFFF;
            border-top:0;
            border-bottom:0;
            padding-top:0;
            padding-bottom:0;
        }
        #dialog-preview #templateSidebar .mcnTextContent,#templateSidebar .mcnTextContent p{
            color:#656565;
            font-family:Helvetica;
            font-size:12px;
            line-height:150%;
            text-align:<?php echo $align; ?>;
        }
        #dialog-preview #templateSidebar .mcnTextContent a,#templateSidebar .mcnTextContent p a{
            color:#2BAADF;
            font-weight:normal;
            text-decoration:underline;
        }
        #dialog-preview #templateBody,#templateColumns{
            background-color:#FFFFFF;
            border-top:0;
            border-bottom:0;
            padding-top:0;
            padding-bottom:0;
        }
        #dialog-preview #templateBody .mcnTextContent,#templateBody .mcnTextContent p{
            color:#202020;
            font-family:Helvetica;
            font-size:16px;
            line-height:150%;
            text-align:<?php echo $align; ?>;
        }
        #dialog-preview #templateBody .mcnTextContent a,#templateBody .mcnTextContent p a{
            color:#2BAADF;
            font-weight:normal;
            text-decoration:underline;
        }
        #dialog-preview #templateFooter{
            background-color:#e7e7e8;
            background-image:none;
            background-repeat:no-repeat;
            background-position:center;
            background-size:cover;
            border-bottom:0;
            padding-top:0;
            padding-bottom:10px;
        }
        #dialog-preview #templateFooter .mcnTextContent,#templateFooter .mcnTextContent p{
            color:#656565;
            font-family:Helvetica;
            font-size:12px;
            line-height:150%;
            text-align:center;
        }
        #dialog-preview #templateFooter .mcnTextContent a,#templateFooter .mcnTextContent p a{
            color:#656565;
            font-weight:normal;
            text-decoration:underline;
        }
        /*@media only screen and (min-width:768px){
                .templateContainer{
                        width:600px !important;
                }

}	@media only screen and (max-width: 480px){
                body,table,td,p,a,li,blockquote{
                        -webkit-text-size-adjust:none !important;
                }

}	@media only screen and (max-width: 480px){
                body{
                        width:100% !important;
                        min-width:100% !important;
                }

}	@media only screen and (max-width: 480px){
                #bodyCell{
                        padding-top:10px !important;
                }

}	@media only screen and (max-width: 480px){
                #templateSidebar,#templateBody{
                        max-width:100% !important;
                        width:100% !important;
                }

}	@media only screen and (max-width: 480px){
                .mcnImage{
                        width:100% !important;
                }

}	@media only screen and (max-width: 480px){
                .mcnCartContainer,.mcnCaptionTopContent,.mcnRecContentContainer,.mcnCaptionBottomContent,.mcnTextContentContainer,.mcnBoxedTextContentContainer,.mcnImageGroupContentContainer,.mcnCaptionLeftTextContentContainer,.mcnCaptionRightTextContentContainer,.mcnCaptionLeftImageContentContainer,.mcnCaptionRightImageContentContainer,.mcnImageCardLeftTextContentContainer,.mcnImageCardRightTextContentContainer{
                        max-width:100% !important;
                        width:100% !important;
                }

}	@media only screen and (max-width: 480px){
                .mcnBoxedTextContentContainer{
                        min-width:100% !important;
                }

}	@media only screen and (max-width: 480px){
                .mcnImageGroupContent{
                        padding:9px !important;
                }

}	@media only screen and (max-width: 480px){
                .mcnCaptionLeftContentOuter .mcnTextContent,.mcnCaptionRightContentOuter .mcnTextContent{
                        padding-top:9px !important;
                }

}	@media only screen and (max-width: 480px){
                .mcnImageCardTopImageContent,.mcnCaptionBlockInner .mcnCaptionTopContent:last-child .mcnTextContent{
                        padding-top:5px !important;
                }

}	@media only screen and (max-width: 480px){
                .mcnImageCardBottomImageContent{
                        padding-bottom:9px !important;
                }

}	@media only screen and (max-width: 480px){
                .mcnImageGroupBlockInner{
                        padding-top:0 !important;
                        padding-bottom:0 !important;
                }

}	@media only screen and (max-width: 480px){
                .mcnImageGroupBlockOuter{
                        padding-top:9px !important;
                        padding-bottom:9px !important;
                }

}	@media only screen and (max-width: 480px){
                .mcnTextContent,.mcnBoxedTextContentColumn{
                        padding-<?php echo $otheralign; ?>:5px !important;
                        padding-<?php echo $align; ?>:5px !important;
                }

}	@media only screen and (max-width: 480px){
                .mcnImageCardLeftImageContent,.mcnImageCardRightImageContent{
                        padding-<?php echo $otheralign; ?>:5px !important;
                        padding-bottom:0 !important;
                        padding-<?php echo $align; ?>:5px !important;
                }

}	@media only screen and (max-width: 480px){
                .mcpreview-image-uploader{
                        display:none !important;
                        width:100% !important;
                }

}	@media only screen and (max-width: 480px){
                h1{
                        font-size:22px !important;
                        line-height:125% !important;
                }

}	@media only screen and (max-width: 480px){
                h2{
                        font-size:20px !important;
                        line-height:125% !important;
                }

}	@media only screen and (max-width: 480px){
                h3{
                        font-size:5px !important;
                        line-height:125% !important;
                }

}	@media only screen and (max-width: 480px){
                h4{
                        font-size:16px !important;
                        line-height:150% !important;
                }

}	@media only screen and (max-width: 480px){
                .mcnBoxedTextContentContainer .mcnTextContent,.mcnBoxedTextContentContainer .mcnTextContent p{
                        font-size:14px !important;
                        line-height:150% !important;
                }

}	@media only screen and (max-width: 480px){
                #templatePreheader{
                        display:block !important;
                }

}	@media only screen and (max-width: 480px){
                #templatePreheader .mcnTextContent,#templatePreheader .mcnTextContent p{
                        font-size:14px !important;
                        line-height:150% !important;
                }

}	@media only screen and (max-width: 480px){
                #templateHeader .mcnTextContent,#templateHeader .mcnTextContent p{
                        font-size:16px !important;
                        line-height:150% !important;
                }

}	@media only screen and (max-width: 480px){
                #templateSidebar .mcnTextContent,#templateSidebar .mcnTextContent p{
                        font-size:16px !important;
                        line-height:150% !important;
                }

}	@media only screen and (max-width: 480px){
                #templateBody .mcnTextContent,#templateBody .mcnTextContent p{
                        font-size:16px !important;
                        line-height:150% !important;
                }

}	@media only screen and (max-width: 480px){
                #templateFooter .mcnTextContent,#templateFooter .mcnTextContent p{
                        font-size:14px !important;
                        line-height:150% !important;
                }
}*/
        
        <?php

        $estatement = \App\Estatement::find($eStatement->id);
        $partner_id = $estatement->partner_id;
        
        
         $partner = \App\Partner::find($partner_id);
            $emailWhitelabelingData = $partner->emailwhitelabelings;
            $blu_partner = \App\Partner::find(1);
            $bluEmailWhitelabelingData = $blu_partner->emailwhitelabelings;
            foreach ($emailWhitelabelingData as $emailWhitelabeling) {
                $emailWhiteLabel[$emailWhitelabeling->field][$emailWhitelabeling->lang] = $emailWhitelabeling->value;
            }

            foreach ($bluEmailWhitelabelingData as $emailWhitelabeling) {
                
                if (isset($emailWhiteLabel) && array_key_exists($emailWhitelabeling->field, $emailWhiteLabel) && array_key_exists($emailWhitelabeling->lang, $emailWhiteLabel[$emailWhitelabeling->field]) && $emailWhiteLabel[$emailWhitelabeling->field][$emailWhitelabeling->lang] !== "") {
                    continue;
                } else {
                    $emailWhiteLabel[$emailWhitelabeling->field][$emailWhitelabeling->lang] = $emailWhitelabeling->value;
                }
            }
            
           if (!empty($partner->color)) {
               $primaryColor = $partner->color;
           } else {
               $primaryColor = $blu_partner->color;
           }
           if (!empty($partner->secondary_color)) {
               $secondaryColor = $partner->secondary_color;
           } else {
               $secondaryColor = $blu_partner->secondary_color;
           }
        ?>
    </style></head>
<div id="body" style="<?php if ($lang == "ar") echo "direction: rtl;"; ?>height: 100%;margin: 0;padding: 0;width: 100%;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #e7e7e8;">
    <center>
        <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;height: 100%;margin: 0;padding: 0;width: 100%;background-color: #e7e7e8;">
            <tbody><tr>
                    <td align="center" valign="middle" id="bodyCell" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;height: 100%;margin: 0;padding: 10px;width: 100%;border-top: 0;">
                        <!-- BEGIN TEMPLATE // -->
                        <!--[if gte mso 9]>
                        <table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
                        <tr>
                        <td align="center" valign="middle" width="600" style="width:600px;">
                        <![endif]-->
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;width: 600px !important;">
                            <tbody>
                                <tr>
                                    <td valign="middle" id="templateHeader" style="background:#FFFFFF none no-repeat center/cover;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #FFFFFF;background-image: none;background-repeat: no-repeat;background-position: center;background-size: cover;border-top: 1px solid <?php echo $secondaryColor ?>;border-left: 1px solid <?php echo $secondaryColor ?>; border-right: 1px solid <?php echo $secondaryColor ?>;border-bottom: 0;padding-top: 10px;padding-bottom: 10px;">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                            <tbody class="mcnTextBlockOuter">
                                                <tr>
                                                    <td valign="middle" class="mcnTextBlockInner" style="padding-top: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                        <!--[if mso]>
                                                                        <table align="<?php echo $align; ?>"border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                                                                        <tr>
                                                                        <![endif]-->

                                                        <!--[if mso]>
                                                        <td valign="middle" width="210" style="width:210px;">
                                                        <![endif]-->
                                                        <table align="<?php echo $align; ?>"border="0" cellpadding="0" cellspacing="0" style="height:90px;width: 192px;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" width="100%" class="mcnTextContentContainer">
                                                            <tbody><tr>

                                                                    <td valign="middle" class="mcnTextContent" style="padding-top: 0;padding-<?php echo $align; ?>: 5px;padding-bottom: 0;padding-<?php echo $otheralign; ?>: 5px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #202020;font-family: Helvetica;font-size: 16px;line-height: 150%;text-align:center;">
                                                                        @if(isset($emailWhiteLabel['image']['en']) && !empty($emailWhiteLabel['image']['en']))
                                                                            <img align="none" alt="JANA Rewards" width="110" src="https://bluai.com/media/image/<?php echo $emailWhiteLabel['image']['en']; ?>" style="width: 120px;margin: 0px;border: 0;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;">
                                                                        @endif
                                                                    </td>
                                                                </tr>
                                                            </tbody></table>
                                                        <!--[if mso]>
                                                        </td>
                                                        <![endif]-->

                                                        <!--[if mso]>
                                                        <td valign="middle" width="390" style="width:390px;">
                                                        <![endif]-->
                                                        <table align="<?php echo $align; ?>"border="0" cellpadding="0" cellspacing="0" style="height:90px; width: 385px;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%; margin: 0 10px" width="100%" class="mcnTextContentContainer">
                                                            <tbody><tr>

                                                                    <td valign="middle" class="mcnTextContent" style="height:90px;padding:0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #202020;font-family: Helvetica;font-size: 16px;line-height: 150%;text-align:<?php echo $align; ?>;background-color:<?php echo $primaryColor ?>;-webkit-border-radius: 10px;-moz-border-radius: 10px;border-radius: 10px;">

                                                                        <h1 class="null" style="display: block;margin: 0;padding: 0;color: #202020;font-family: 'MS Sans Serif', sans-serif, Helvetia;text-transform: uppercase;font-size: 25px;font-style: normal;font-weight: normal;line-height: 125%;letter-spacing: normal;text-align:center;"><span style="color:#FFFFFF"><?php echo App\Http\Controllers\DashboardController::platformTranslateKeysForLanguage('lbl-estat-pointsstatement', $lang); ?></span></h1>

                                                                    </td>
                                                                </tr>
                                                            </tbody></table>
                                                        <!--[if mso]>
                                                        </td>
                                                        <![endif]-->

                                                        <!--[if mso]>
                                                        </tr>
                                                        </table>
                                                        <![endif]-->
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table></td>
                                </tr>
                                <tr>
                                    <td valign="middle" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;border-left:1px solid <?php echo $secondaryColor ?>;border-right:1px solid <?php echo $secondaryColor ?>; border-bottom:1px solid <?php echo $secondaryColor ?>;">
                                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" id="templateColumns" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #FFFFFF;border-top: 0;border-bottom: 0;padding-top: 0;padding-bottom: 0;">
                                            <tbody><tr>
                                                    <td valign="middle" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                        <!--[if gte mso 9]>
                                                        <table align="center" border="0" cellspacing="0" cellpadding="0" dir="rtl" width="600" style="width:600px;">
                                                        <tr>
                                                        <td align="center" dir="ltr" valign="middle" width="400" style="width:400px;">
                                                        <![endif]-->
                                                        <table align="<?php echo $otheralign; ?>"border="0" cellpadding="0" cellspacing="0" width="400" id="templateBody" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #FFFFFF;border-top: 0;border-bottom: 0;padding-top: 0;padding-bottom: 0;">
                                                            <tbody class="mcnBoxedTextBlockOuter">
                                                                <tr>
                                                                    <td class="mcnBoxedTextBlockInner" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" valign="middle"><!--[if gte mso 9]>
                                                                    <td align="center" valign="middle" ">
                                                                    <![endif]-->
                                                                        <table align="<?php echo $align ?>" border="0" cellpadding="0" cellspacing="0" class="mcnBoxedTextContentContainer" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" width="100%">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td style="padding-top: 0;padding-<?php echo $align ?>: 5px;padding-bottom: 10px;padding-<?php echo $otheralign ?>: 10px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">

                                                                                        <!--[if gte mso 9]>
                                                                                        <td align="center" valign="middle" ">
                                                                                        <![endif]-->
                                                                                        <?php
                                                                                        if(count($items)){
                                                                                        $item = $items{0};
                                                                                        $itemTitle = '';
                                                                                        $itemBody = '';
                                                                                        $itemImage = '';
                                                                                        if ($item->type == 'product') {
                                                                                            $product = App\Product::find($item->item_id);
                                                                                            if ($lang != 'en') {
                                                                                                $productTranslation = \App\Prodtranslation::where('product_id', $product->id)->where('lang_id', 2)->first();
                                                                                                if ($productTranslation) {
                                                                                                    $product->name = $productTranslation->name;
                                                                                                    $product->model = $productTranslation->model;
                                                                                                    $product->sub_model = $productTranslation->sub_model;
                                                                                                    $product->description = $productTranslation->description;
                                                                                                }
                                                                                            }
                                                                                            $itemTitle = $product->name;
                                                                                            $itemBody = $product->description;
                                                                                            $itemImage = $product->cover_image;
                                                                                        } else {
                                                                                            $offer = App\Offer::find($item->item_id);
                                                                                            if ($lang != 'en') {
                                                                                                $offerTranslation = \App\Offertranslation::where('offer_id', $offer->id)->where('lang_id', 2)->first();
                                                                                                if ($offerTranslation) {
                                                                                                    $offer->name = $offerTranslation->name;
                                                                                                    $offer->short_description = $offerTranslation->short_description;
                                                                                                    $offer->description = $offerTranslation->description;
                                                                                                    $offer->terms_and_condition = $offerTranslation->terms_and_condition;
                                                                                                    $offer->email_text_promo_1 = $offerTranslation->email_text_promo_1;
                                                                                                    $offer->email_text_promo_2 = $offerTranslation->email_text_promo_2;
                                                                                                }
                                                                                            }
                                                                                            $itemTitle = $offer->name;
                                                                                            $itemBody = $offer->email_text_promo_1;
                                                                                            $itemImage = $offer->cover_image;
                                                                                        }
                                                                                        ?>
                                              
                                                                                        
                                                                                        <table border="0" cellpadding="18" cellspacing="0" class="mcnTextContentContainer" style="min-width: 100% !important;background-color: <?php print $primaryColor; ?>; -webkit-border-radius: 10px;-moz-border-radius: 10px;border-radius: 10px;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" width="100%">
                                                                                                <tbody>
                                                                                                    <tr>
                                                                                                        <td class="mcnTextContent" style="height:170px; padding:30px;color: #ffffff;font-family: Helvetica;font-size: 12px;font-weight: normal;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;" valign="top">
                                                                                                            <div style="text-align:<?php echo $align ?>;">
                                                                                                                <div style="display:inline-block; float:right"><span class="sg-image" ><img align="none" alt="<?php print $itemTitle; ?>" height="125" src="<?php echo "https://bluai.com/media/image/" . $itemImage ?>" style="width: 175px; height: 125px; margin: 0px 0px 0px 10px; border: 0px; outline: none; text-decoration: none;" width="175" /></span></div>

                                                                                                                <p style="font-weight:bold;color:#ffffff; font-size: 15px; margin:0 0 5px 0"><span style="color: rgb(255, 255, 255); font-family: Helvetica; font-size: 15px; font-weight: bold; text-align: center; background-color:<?php print $primaryColor; ?>;"><?php print $itemTitle; ?></span></p>

                                                                                                                <p style="margin:0; color:#ffffff;"><span style="color: rgb(255, 255, 255); font-family: Helvetica; font-size: 14px; text-align: center; background-color: <?php print $primaryColor; ?>;"><?php print $itemBody; ?></span></p>
                                                                                                            </div>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                        
                                                                                        
                                                                                        <?php } ?>
                                                                                        <!--[if gte mso 9]>
                                                                                        </td>
                                                                                        <![endif]-->

                                                                                        <!--[if gte mso 9]>
                                                                        </tr>
                                                                        </table>
                                                                                        <![endif]-->
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>

                                                                        
                                                                        
                                                                        <table border="0" cellpadding="0" cellspacing="0" class="mcnBoxedTextBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" width="100%"><!--[if gte mso 9]>
<table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
<![endif]-->
                                                                            <tbody class="mcnBoxedTextBlockOuter">
                                                                                <tr>
                                                                                    <td class="mcnBoxedTextBlockInner" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" valign="middle"><!--[if gte mso 9]>
<td align="center" valign="middle" ">
<![endif]-->
                                                                                        <table align="<?php echo $align ?>" border="0" cellpadding="0" cellspacing="0" class="mcnBoxedTextContentContainer" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" width="100%">
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td style="padding-top: 0;padding-<?php echo $align ?>: 5px;padding-bottom: 10px;padding-<?php echo $otheralign ?>: 10px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                                                        <table border="0" cellpadding="18" cellspacing="0" class="mcnTextContentContainer" style="min-width: 100% !important;background-color: #E7E8E9; -webkit-border-radius: 10px;-moz-border-radius: 10px;border-radius: 10px;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" width="100%">
                                                                                                            <tbody>
                                                                                                                <tr>
                                                                                                                    <td class="mcnTextContent" style="padding:30px; height: 213px; color: #F2F2F2;font-family: Helvetica;font-size: 14px;font-weight: normal;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;line-height: 150%;" valign="top">
                                                                                                                        <h2 class="null" style="display: block;margin: 0;padding: 0;color: #000000;font-family: Helvetica;font-size: 22px;font-style: normal;font-weight: bold;line-height: 125%;letter-spacing: normal;text-align:<?php echo $align ?>;"><?php echo $eStatement->title ?></h2>

                                                                                                                        <div style="text-align:<?php echo $align ?>; margin-top:15px"><span style="color:#000000"><?php echo $eStatement->description ?></span></div>

                                                                                                                        <div>&nbsp;</div>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </tbody>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                        <!--[if gte mso 9]>
</td>
<![endif]--><!--[if gte mso 9]>
</tr>
</table>
<![endif]--></td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                        
                                                                   
                                                                        
                                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnButtonBlock" style="margin-top: 0; min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                            <tbody class="mcnButtonBlockOuter">
                                                                                <tr>
                                                                                    <td style="padding-top: 0;padding-<?php echo $otheralign; ?>: 10px;padding-bottom: 5px;padding-<?php echo $align; ?>: 5px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" valign="middle" align="center" class="mcnButtonBlockInner">
                                                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnButtonContentContainer" style="border-collapse: separate !important;border-radius: 3px;background-color: <?php echo $secondaryColor ?>; mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td align="center" valign="middle" class="mcnButtonContent" style="font-family: Arial;font-size: 16px;padding: 15px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                                                        <a class="mcnButton " title="><?php echo App\Http\Controllers\DashboardController::platformTranslateKeysForLanguage('lbl-estat-viewaccount', $lang); ?>" href="<?php if(isset($emailWhiteLabel['website_url']['en'])){ echo $emailWhiteLabel['website_url']['en'] . "/account/statement";} ?>" target="_blank" style="font-weight: bold;letter-spacing: normal;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;display: block;"><?php echo App\Http\Controllers\DashboardController::platformTranslateKeysForLanguage('lbl-estat-viewaccount', $lang); ?></a>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table></td>
                                                                </tr>
                                                            </tbody></table>
                                                        <!--[if gte mso 9]>
                                                        </td>
                                                        <td align="center" dir="ltr" valign="middle" width="200" style="width:200px;">
                                                        <![endif]-->
                                                        <table align="<?php echo $align; ?>"border="0" cellpadding="0" cellspacing="0" width="198" id="templateSidebar" style="width: 197px;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #FFFFFF;border-top: 0;border-bottom: 0;padding-top: 0;padding-bottom: 0;">
                                                            <tbody><tr>
                                                                    <td valign="middle" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnBoxedTextBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                            <!--[if gte mso 9]>
                                                                                <table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
                                                                                <![endif]-->
                                                                            <tbody class="mcnBoxedTextBlockOuter">
                                                                                <tr>
                                                                                    <td valign="middle" class="mcnBoxedTextBlockInner" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">

                                                                                        <!--[if gte mso 9]>
                                                                                        <td align="center" valign="middle" ">
                                                                                        <![endif]-->
                                                                                        <table align="<?php echo $align; ?>"border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnBoxedTextContentContainer">
                                                                                            <tbody><tr>

                                                                                                    <td style="padding-top: 0;padding-<?php echo $align; ?>: 10px;padding-bottom: 10px;padding-<?php echo $otheralign; ?>: 5px; mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">

                                                                                                        <table border="0" cellpadding="18" cellspacing="0" class="mcnTextContentContainer" width="100%" style="min-width: 100% !important;background-color: <?php echo $primaryColor ?>; -webkit-border-radius: 10px;-moz-border-radius: 10px;border-radius: 10px; border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                                                            <tbody><tr>
                                                                                                                    <td valign="middle" class="mcnTextContent" style="height: 228px; padding:0 10px; color: #F2F2F2;font-family: Helvetica;font-size: 14px;font-weight: normal;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;line-height: 150%;">
                                                                                                                        <div style="text-align:<?php echo $align; ?>;line-height:1.3">
                                                                                                                            <span style="font-size:11px"><?php echo App\Http\Controllers\DashboardController::platformTranslateKeysForLanguage('lbl-estat-membername', $lang); ?></span><br>
                                                                                                                            <?php print 'Test User'; ?><br>
                                                                                                                            <hr style="border: 0 none;  border-top: 1px dashed #fff;  background: none;  height:0;padding:0;margin:5px 0" />
                                                                                                                            <span style="font-size:11px"><?php echo App\Http\Controllers\DashboardController::platformTranslateKeysForLanguage('lbl-estat-pointsbalance', $lang); ?></span><br>
                                                                                                                            <?php
                                                                                                                            if ($lang == "ar") {
                                                                                                                                print str_replace($standard, $arabic, number_format(10000, 0));
                                                                                                                            } else {
                                                                                                                                print number_format(10000, 0);
                                                                                                                            }
                                                                                                                            ?><br>
                                                                                                                            
                                                                                                                            
                                                                                                                            <?php  if ($partner_id == '4317') : ?>
                                                                                                                            <hr style="border: 0 none;  border-top: 1px dashed #fff;  background: none;  height:0;padding:0;margin:5px 0" />
                                                                                                                            <span style="font-size:11px"><?php echo App\Http\Controllers\DashboardController::platformTranslateKeysForLanguage('lbl-estat-eligiblepoints', $lang); ?></span><br>
                                                                                                                            <?php
                                                                                                                            if ($lang == "ar") {
                                                                                                                                print str_replace($standard, $arabic, number_format(3000, 0));
                                                                                                                            } else {
                                                                                                                                print number_format(3000, 0);
                                                                                                                            }
                                                                                                                            ?><br>
                                                                                                                            <?php endif; ?>
                                                                                                                            
                                                                                                                            <hr style="border: 0 none;  border-top: 1px dashed #fff;  background: none;  height:0;padding:0;margin:5px 0" />
                                                                                                                            <span style="font-size:11px"><?php echo App\Http\Controllers\DashboardController::platformTranslateKeysForLanguage('lbl-estat-status', $lang); ?></span><br>
<?php print 'Active'; ?></div>

                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </tbody></table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody></table>
                                                                                        <!--[if gte mso 9]>
                                                                                        </td>
                                                                                        <![endif]-->

                                                                                        <!--[if gte mso 9]>
                                                                        </tr>
                                                                        </table>
                                                                                        <![endif]-->
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnBoxedTextBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                            <!--[if gte mso 9]>
                                                                                <table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
                                                                                <![endif]-->
                                                                            <tbody class="mcnBoxedTextBlockOuter">
                                                                                <tr>
                                                                                    <td valign="middle" class="mcnBoxedTextBlockInner" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">

                                                                                        <!--[if gte mso 9]>
                                                                                        <td align="center" valign="middle" ">
                                                                                        <![endif]-->
                                                                                        <table align="<?php echo $align; ?>"border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnBoxedTextContentContainer">
                                                                                            <tbody><tr>

                                                                                                    <td style="padding-top: 0;padding-<?php echo $align; ?>: 10px;padding-bottom: 10px;padding-<?php echo $otheralign; ?>: 5px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">

                                                                                                        <table border="0" cellpadding="18" cellspacing="0" class="mcnTextContentContainer" width="100%" style="min-width: 100% !important;background-color: <?php echo $primaryColor ?>; -webkit-border-radius: 10px;-moz-border-radius: 10px;border-radius: 10px; border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                                                            <tbody><tr>
                                                                                                                    <td valign="top" class="mcnTextContent" style="padding:10px; height:310px; color: #F2F2F2;font-family: Helvetica;font-size: 14px;font-weight: normal;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;line-height: 150%;">
                                                                                                                        <!--<h2 class="null" style="text-align:<?php //echo $align;?>;display: block;margin: 0;padding: 0;color: #202020;font-family: Helvetica;font-size: 20px;font-style: normal;font-weight: bold;line-height: 125%;letter-spacing: normal;"><span style="color:#FFF0F5"><?php //if($lang == "ar") {echo DashboardController::platformTranslateKeysForLanguage('lbl-estat-monthactivity', $lang) . ' ' . str_replace($months, $amonths, date("F", strtotime("first day of previous month"))); } else {echo date("F", strtotime("first day of previous month")). ' ' . DashboardController::platformTranslateKeysForLanguage('lbl-estat-monthactivity', $lang);}?></span></h2>-->
                                                                                                                        <div style="text-align:<?php echo $align; ?>;line-height:1.3"><span style="font-size:11px"><?php echo App\Http\Controllers\DashboardController::platformTranslateKeysForLanguage('lbl-estat-period', $lang); ?></span><br>
                                                                                                                            <?php
                                                                                                                            if ($lang == "ar") {
                                                                                                                                print str_replace($standard, $arabic, date("m/Y", strtotime("first day of previous month")));
                                                                                                                            } else {
                                                                                                                                print date("m/Y", strtotime("first day of previous month"));
                                                                                                                            }
                                                                                                                            ?><br>
                                                                                                                            <hr style="border: 0 none;  border-top: 1px dashed #fff;  background: none;  height:0;padding:0;margin:5px 0" />
                                                                                                                            <div style="text-align:<?php echo $align; ?>;"><span style="font-size:11px"><?php echo App\Http\Controllers\DashboardController::platformTranslateKeysForLanguage('lbl-estat-openingbalance', $lang); ?></span><br>
                                                                                                                                <?php
                                                                                                                                if ($lang == "ar") {
                                                                                                                                    print str_replace($standard, $arabic, number_format(2000, 0));
                                                                                                                                } else {
                                                                                                                                    print number_format(2000, 0);
                                                                                                                                }
                                                                                                                                ?><br>
                                                                                                                                <hr style="border: 0 none;  border-top: 1px dashed #fff;  background: none;  height:0;padding:0;margin:5px 0" />
                                                                                                                                <span style="font-size:11px"><?php echo App\Http\Controllers\DashboardController::platformTranslateKeysForLanguage('lbl-estat-pointsearned', $lang); ?></span><br>
<?php
if ($lang == "ar") {
    print str_replace($standard, $arabic, number_format(6000, 0));
} else {
    print number_format(6000, 0);
}
?><br>
                                                                                                                                <hr style="border: 0 none;  border-top: 1px dashed #fff;  background: none;  height:0;padding:0;margin:5px 0" />
                                                                                                                                <span style="font-size:11px"><?php echo App\Http\Controllers\DashboardController::platformTranslateKeysForLanguage('lbl-estat-pointsredeemed', $lang); ?></span><br>
                                                                                                                                <?php
                                                                                                                                if ($lang == "ar") {
                                                                                                                                    print str_replace($standard, $arabic, number_format(5000, 0));
                                                                                                                                } else {
                                                                                                                                    print number_format(5000, 0);
                                                                                                                                }
                                                                                                                                ?><br>
                                                                                                                                <hr style="border: 0 none;  border-top: 1px dashed #fff;  background: none;  height:0;padding:0;margin:5px 0" />
                                                                                                                                <span style="font-size:11px"><?php echo App\Http\Controllers\DashboardController::platformTranslateKeysForLanguage('lbl-estat-pointsexpired', $lang) . ' ';
                                                                                                                                if ($lang == "ar") {
                                                                                                                                    echo str_replace($months, $amonths, date("F", strtotime("first day of previous month")));
                                                                                                                                } else {
                                                                                                                                    echo date("F", strtotime("first day of previous month"));
                                                                                                                                } ?></span><br>
                                                                                                                                <?php
                                                                                                                                if ($lang == "ar") {
                                                                                                                                    print str_replace($standard, $arabic, number_format(1000, 0));
                                                                                                                                } else {
                                                                                                                                    print number_format(1000, 0);
                                                                                                                                }
                                                                                                                                ?><br>
                                                                                                                                <hr style="border: 0 none;  border-top: 1px dashed #fff;  background: none;  height:0;padding:0;margin:5px 0" />
                                                                                                                                <span style="font-size:11px"><?php echo App\Http\Controllers\DashboardController::platformTranslateKeysForLanguage('lbl-estat-nextexpiry', $lang); ?></span><br>
                                                                                                                                <?php
                                                                                                                                if ($lang == "ar") {
                                                                                                                                    echo '١ يناير' . ' ' . date('Y', strtotime('+1 year'));
//    echo '';
                                                                                                                                } else {
                                                                                                                                    echo 'January 1st' . ' ' . date('Y', strtotime('+1 year'));
//    echo ''; 
                                                                                                                                }
                                                                                                                                ?><br>
<?php
if ($lang == "ar") {
    print str_replace($standard, $arabic, number_format(2000, 0));
} else {
    print number_format(2000, 0);
}
?><br>
                                                                                                                            </div>

                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </tbody></table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody></table>
                                                                                        <!--[if gte mso 9]>
                                                                                        </td>
                                                                                        <![endif]-->

                                                                                        <!--[if gte mso 9]>
                                                                        </tr>
                                                                        </table>
                                                                                        <![endif]-->
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table></td>
                                                                </tr>
                                                            </tbody></table>
                                                        <!--[if gte mso 9]>
                                                        </td>
                                                        </tr>
                                                        </table>
                                                        <![endif]-->
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="middle" id="templateFooter" style="background:#FAFAFA none no-repeat center/cover;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #e7e7e8;background-image: none;background-repeat: no-repeat;background-position: center;background-size: cover;border-bottom: 0;padding-top: 0;padding-bottom: 10px;">
                                        <?php
                                        $itemTitle;
                                        $itemBody;
                                        $itemImage;
                                        if (count($items) > 0) {
                                            foreach ($items as $item) {
                                                if ($item->rank != 1) {
                                                    if ($item->type == 'product') {
                                                        $product = App\Product::find($item->item_id);
                                                        if ($lang != 'en') {
                                                            $productTranslation = \App\Prodtranslation::where('product_id', $product->id)->where('lang_id', 2)->first();
                                                            if ($productTranslation) {
                                                                $product->name = $productTranslation->name;
                                                                $product->model = $productTranslation->model;
                                                                $product->sub_model = $productTranslation->sub_model;
                                                                $product->description = $productTranslation->description;
                                                            }
                                                        }
                                                        $itemTitle = $product->name;
                                                        $itemBody = $product->description;
                                                        $itemImage = $product->cover_image;
                                                    } else {
                                                        $offer = App\Offer::find($item->item_id);
                                                        if ($lang != 'en') {
                                                            $offerTranslation = \App\Offertranslation::where('offer_id', $offer->id)->where('lang_id', 2)->first();
                                                            if ($offerTranslation) {
                                                                $offer->name = $offerTranslation->name;
                                                                $offer->short_description = $offerTranslation->short_description;
                                                                $offer->description = $offerTranslation->description;
                                                                $offer->terms_and_condition = $offerTranslation->terms_and_condition;
                                                                $offer->email_text_promo_1 = $offerTranslation->email_text_promo_1;
                                                                $offer->email_text_promo_2 = $offerTranslation->email_text_promo_2;
                                                            }
                                                        }
                                                        $itemTitle = $offer->name;
                                                        $itemBody = $offer->email_text_promo_2;
                                                        $itemImage = $offer->cover_image;
                                                    }
                                                    ?>
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width: 100%;border-collapse: separate; mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;margin-top:10px; background-color:#ffffff; border: 1px solid <?php echo $secondaryColor ?>; -webkit-border-radius: 10px;-moz-border-radius: 10px;border-radius: 10px;">
                                                        <tbody class="mcnTextBlockOuter" style="padding:5px;">
                                                            <tr>
                                                               	<td class="mcnTextBlockInner" style="padding-top: 10px; padding-bottom: 10px; mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" valign="middle"><!--[if mso]>
                                    <table align="<?php echo $align ?>"border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                                    <tr>
                                    <![endif]--><!--[if mso]>
                                    <td valign="middle" width="210" style="width:210px;">
                                    <![endif]-->
									<table align="<?php echo $align ?>" border="0" cellpadding="0" cellspacing="0" class="mcnTextContentContainer" style="width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" width="100%">
										<tbody>
											<tr>
												<td class="mcnTextContent" style="width:194px; padding-top: 0;padding-<?php echo $align ?>: 5px; padding-<?php echo $otheralign ?>: 5px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #656565;font-family: Helvetica;font-size: 12px;line-height: 150%;text-align: center;border-<?php echo $otheralign ?>: 1px solid <?php echo $secondaryColor ?>" valign="middle"><span class="sg-image"  style="float: none; display: block; text-align: center;"><img align="none" alt="<?php echo $itemTitle ?>" height="125" src="<?php echo "https://bluai.com/media/image/" .$itemImage ?>" style="width: 165px;height: 125px;margin: 0px; border: 0;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" width="165" /></span></td>
												<td class="mcnTextContent" style="padding: 0 30px; mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #656565;font-family: Helvetica;font-size: 12px;line-height: 150%;text-align:<?php echo $align ?>;" valign="middle">
												<p style="text-align:<?php echo $align ?>;font-weight:bold; font-size: 13px; margin:0 0 5px 0;"><span style="color: rgb(101, 101, 101); font-family: Helvetica; font-size: 13px; font-weight: bold; background-color: rgb(255, 255, 255);"><?php echo $itemTitle ?></span></p>

												<p style="margin:0; text-align:<?php echo $align ?>;"><span style="color: rgb(101, 101, 101); font-family: Helvetica; font-size: 12px; background-color: rgb(255, 255, 255);"><?php echo $itemBody ?></span></p>
												</td>
											</tr>
										</tbody>
									</table>
									<!--[if mso]>
                                    </td>
                                    <![endif]--><!--[if mso]>
                                    </tr>
                                    </table>
                                    <![endif]--></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
        <?php
        } else {
            continue;
        }
    }
}
?>
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                            <tbody class="mcnTextBlockOuter">
                                                <tr>
                                                    <td valign="middle" class="mcnTextBlockInner" style="padding-top: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                        <!--[if mso]>
                                                                        <table align="<?php echo $align; ?>"border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                                                                        <tr>
                                                                        <![endif]-->

                                                        <!--[if mso]>
                                                        <td valign="middle" width="600" style="width:600px;">
                                                        <![endif]-->
                                                        	<table align="<?php echo $align ?>" border="0" cellpadding="0" cellspacing="0" class="mcnTextContentContainer" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" width="100%">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td class="mcnTextContent" style="padding-top: 0;padding-<?php echo $otheralign ?>: 5px;padding-bottom: 9px;padding-<?php echo $align ?>: 5px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #656565;font-family: Helvetica;font-size: 12px;line-height: 150%;text-align: center;" valign="middle"><em>Copyright &copy; 2017&nbsp;</em><?php echo ($lang == "ar") ? $emailWhiteLabel['copyright']['ar'] : $emailWhiteLabel['copyright']['en'] ?><em>&nbsp;All rights reserved.</em></td>
                                                                        </tr>
                                                                    </tbody>
								</table>
                                                        <!--[if mso]>
                                                        </td>
                                                        <![endif]-->

                                                        <!--[if mso]>
                                                        </tr>
                                                        </table>
                                                        <![endif]-->
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table></td>
                                </tr>
                            </tbody></table>
                        <!--[if gte mso 9]>
                        </td>
                        </tr>
                        </table>
                        <![endif]-->
                        <!-- // END TEMPLATE -->
                    </td>
                </tr>
            </tbody></table>
    </center>
    <style type="text/css">
        /*@media only screen and (max-width: 480px){
            table#canspamBar td{font-size:14px !important;}
            table#canspamBar td a{display:block !important; margin-top:10px !important;}
        }*/
    </style>
</div>
</div>