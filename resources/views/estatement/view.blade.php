@extends('layouts.dashboard')

@section('title')
Update E-Statement | BLU
@stop

@section('dialogs')
@include('estatement/dialog/country', array('countries' => $countries))
@include('estatement/dialog/segment', array('segments'  => $segments))
@include('estatement/dialog/tier',    array('tiers'  => $tiers))
@include('estatement/dialog/user',    array('estatement'  => $estatement))
@include('estatement/dialog/preview',    array('eStatement'  => $estatement, 'items'=>App\Estatementitem::where('estatement_id', $estatement->id)->orderBy('rank', 'asc')->get(), 'lang' => 'en'))
@stop

@section('body')
<div class="padding">

    <div class="content-head pure-g">
        <div class="title pure-u-1-2">
            <h1><i class="fa fa-list fa-fw"></i> E-Statement </h1>
        </div>

        <div class="controls pure-u-1-2">
            <a href="{{ url('dashboard/estatement') }}" class="pure-button">E-Statements</a>
        </div>
    </div>

    {{ Form::model($estatement, array('action' => array('EstatementController@saveEstatement', base64_encode($estatement->id)),
                                   'method' => 'post',
                                   'files'  => 'true',
                                   'class'  => 'pure-form pure-form-stacked form-width',
                                   'id'     => 'estatementForm')) }}

    @include('partials.errors')

    {!! Form::hidden('redirect_to', URL::previous()) !!}

    <div class="pure-g">
        @include('estatement/form', array('estatement' => $estatement))
        <div class="pure-u-1-2 attributes">
            <div class="padding-left-40">
                @include('estatement/list/segment', array('estatement' => $estatement))
                @include('estatement/list/country', array('estatement' => $estatement))
                @include('estatement/list/tier',    array('estatement' => $estatement))
                @include('estatement/list/user',    array('estatement' => $estatement))
            </div>
        </div>
    </div>
    {{--- EstatementItems ---}}
        <fieldset>
        <legend>E-Statement Offers</legend>
            <table id="estatementitem-list" class="pure-table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Type</th>
                        <th>Name</th>
                        <th>Rank</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if(count($estatementitems) > 0)
                    @foreach($estatementitems as $estatementitem)
                    <tr>
                        <td>{{ $estatementitem->id }}</td>
                        <td>{{ $itemTypes[$estatementitem->type] }}</td>
                         @if($estatementitem->type == 'product')
                            <td>{{ App\Product::find($estatementitem->item_id)->name }}</td>
                         @else
                            <td>{{ App\Offer::find($estatementitem->item_id)->name }}</td>
                         @endif
                        <td>
                            {{ $estatementitem->rank }}
                        </td>
                        <td>
                            @if(I::can('delete_estatement'))
                                <a href="{{ url('dashboard/estatementitem/delete/'.base64_encode($estatementitem->id)) }}" class="delete-estatementitem" title="Delete">Delete</a>
                            @else
                                N/A
                            @endif
                        </td>
                    </tr>
                    @endforeach
                    @else
                    <tr class="empty">
                        <td colspan="5">No Items Linked</td>
                    </tr>
                    @endif
                </tbody>
            </table>
            @if($estatement->draft)
                @if(I::can('create_estatement'))
                    <a href="#dialog-new-estatementitem" data-estatement_id="{{ base64_encode($estatement->id) }}" class="btn-new-estatementitem top-space" title="Add new estatement item">
                        <i class="fa fa-plus-circle fa-fw"></i>Add new E-Statement Offer
                    </a>
                @endif
            @else
                @if(I::can('edit_estatement'))
                    <a href="#dialog-new-estatementitem" data-estatement_id="{{ base64_encode($estatement->id) }}" class="btn-new-estatementitem top-space" title="Add new estatement item">
                        <i class="fa fa-plus-circle fa-fw"></i>Add new E-Statement Offer
                    </a>
                @endif
            @endif
        </fieldset>
    {{--- END Estatementtranslations ---}}

    {{--- Estatementtranslations ---}}
    <fieldset>
        <legend>E-Statement Translation</legend>

        <table id="estatementtranslation-list" class="pure-table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Language</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @if(count($estatement->estatementtranslations) > 0)
                @foreach($estatement->estatementtranslations as $estatementtranslation)
                <tr>
                    <td>{{ $estatementtranslation->id }}</td>
                    <td>{{ $estatementtranslation->Title }}</td>
                    <td>{{ $estatementtranslation->description }}</td>
                    <td>
                        <?php
                        echo $estatementtranslation->lang->name;
                        ?>
                    </td>
                    <td>
                        @if(!I::can('delete_estatement') && !I::can('edit_estatement'))
                            N/A
                        @endif
                        @if(I::can('edit_estatement'))
                            <a href="{{ url('dashboard/estatementtranslation/edit/'.base64_encode($estatementtranslation->id)) }}" class="edit-estatementtranslation" title="Edit">Edit</a>
                        @endif
                        @if(I::can('delete_estatement'))
                            <a href="{{ url('dashboard/estatementtranslation/delete/'.base64_encode($estatementtranslation->id)) }}" class="delete-estatementtranslation" title="Delete">Delete</a>
                        @endif
                    </td>
                </tr>
                @endforeach
                @else
                <tr class="empty">
                    <td colspan="5">No estatementtranslation locations defined</td>
                </tr>
                @endif
            </tbody>
        </table>

        @if(I::can('create_estatement') || I::can('edit_estatement'))
            <a href="#dialog-new-estatementtranslation" data-estatement_id="{{ base64_encode($estatement->id) }}" class="btn-new-estatementtranslation top-space" title="Add new estatementtranslation location">
                <i class="fa fa-plus-circle fa-fw"></i>Add new E-Statement Translation
            </a>
        @endif
    </fieldset>
    {{--- /Estatementtranslations ---}}
    <!-- Form Buttons -->

    <div class="form-buttons">
        <div class="left" style="width: 40%;">
            <a href="{{ url('dashboard/estatement') }}" class="pure-button">Cancel</a>
        </div>

        @if($estatement->draft)
            @if(I::can('create_estatement'))
                <div class="right" style="width: 60%;">
                    <button type="submit" class="pure-button pure-button-primary">Create Estatement</button>
                </div>
            @endif
        @else
            @if(I::can('edit_estatement'))
                <div class="right" style="width: 60%;">
                    <a type="button" href="#dialog-preview" id="preview" class="dialog pure-button pure-button-primary">Preview</a>
                    <button type="button" id="send_now" data-estatement_id="{{ base64_encode($estatement->id) }}" class="pure-button pure-button-primary">Send Now</button>
                    <button type="submit" class="pure-button pure-button-primary">Save Estatement</button>
                </div>
            @endif
        @endif

        <div class="clearfix"></div>
    </div>
    <!-- /Form Buttons -->

    {{ Form::close() }}

</div>
@stop

@section('page_script')
<script>
    $(function(){
        set_menu('mnu-estatement');
    });
    enqueue_script('new-estatement');
</script>
@stop
