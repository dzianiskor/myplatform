@extends('layouts.dashboard')

@section('title')
Update E-Statement | BLU
@stop

@section('dialogs')

@stop

@section('body')
<div class="padding">

    <div class="content-head pure-g">
        <div class="title pure-u-1-2">
            <h1><i class="fa fa-list fa-fw"></i> E-Statement History </h1>
        </div>

        <div class="controls pure-u-1-2">
            <a href="{{ url('dashboard/estatement') }}" class="pure-button">E-Statement History</a>
        </div>
    </div>
    @include('partials.errors')         

    <div class="pure-g">
        @include('estatement/form', array('estatement' => $estatementHistory))
        <div class="pure-u-1-2 attributes">
            <div class="padding-left-40">
                @include('estatement/history_list/segment', array('estatementHistory' => $estatementHistory))
                @include('estatement/history_list/country', array('estatementHistory' => $estatementHistory))
                @include('estatement/history_list/tier',    array('estatementHistory' => $estatementHistory))
            </div>
        </div>
    </div>
    {{--- EstatementItems ---}}
        <fieldset>
        <legend>E-Statement Offers</legend>
            <table id="estatementitem-list" class="pure-table">
                <thead>
                    <tr>
                        <th>Type</th>
                        <th>Name</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $estatementitems = json_decode($estatementHistory->items, true);
                    ?>
                    @if(count($estatementitems) > 0)
                    @foreach($estatementitems as $estatementitem)
                    <?php
                        
                    ?>
                    <tr>
                        <td>{{ $itemTypes[$estatementitem['type']] }}</td>
                         @if($estatementitem['type'] == 'product')
                         <td>{{ App\Product::find($estatementitem['id'])->name }}</td>
                         @else
                         <td>{{ App\Offer::find($estatementitem['id'])->name }}</td>
                         @endif
                    </tr>
                    @endforeach
                    @else
                    <tr class="empty">
                        <td colspan="5">No Items Linked</td>
                    </tr>
                    @endif
                </tbody>
            </table>
        </fieldset>
    {{--- END Estatementtranslations ---}}
    
    <!-- Form Buttons -->
    <div class="form-buttons">
        <div class="left">
            <a href="{{ url('dashboard/estatement') }}" class="pure-button">Cancel</a>
        </div>
        <div class="clearfix"></div>
    </div>
    <!-- /Form Buttons -->

    {{ Form::close() }}

</div>
@stop