@extends('layouts.dashboard')

@section('title')
E-Statements | BLU
@stop
@section('page_style')
<style>
    .ui-tabs{
       border: 0px;
       background: none;
    }
    .ui-tabs-panel{
       border: 0px;
       background: transparent;
    }
</style>
@stop

@section('body')
<div id="tabs"> 
    <ul>
        <li>
            <a href="#tabs-1">E-Statements</a>
        </li>
        <li>
            <a href="#tabs-2">E-statements History</a>
        </li>
    </ul>

    <div id="tabs-1">
        <div class="padding">
            <div class="content-head pure-g">
                <div class="title pure-u-1-2">
                    <h1><i class="fa fa-list fa-fw"></i> E-Statements ({{ $estatements->total() }})</h1>
                </div>

                <div class="controls pure-u-1-2">
                    <form action="{{ Filter::searchUrl() }}" method="get">
                        <?php
                        $querystring = isset($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING'] : '';
                        if ($querystring != '') {
                            $queryArr = explode('&', $querystring);
                            foreach ($queryArr as $val) {
                                if (empty($val)) {
                                    continue;
                                }
                                $valArr = explode('=', $val);
                                if ($valArr[0] != "q") {
                                    echo '<input type="hidden" name="' . urldecode($valArr[0]) . '" value="' . $valArr[1] . '"> ';
                                }
                            }
                        }
                        ?>
                        <input type="text" placeholder="Search" name="q" value="{{ Input::get('q') }}" />
                        <button type="submit" title="" class="list-search"></button>
                    </form>
                    @if(I::can('create_estatement'))
                    <a href="{{ url('dashboard/estatement/new') }}" class="pure-button pure-button-primary">Add E-Statement</a>
                    @endif
                </div>

                <div id="catalog-filter" class="pure-u-1-2">
                    <input id="filter_route" type="hidden" value="estatement">
                    <form id="filter-catalogue" action="{{ Filter::searchUrl() }}" method="get">

                        <div class="filter-block filter-block-pricing-width">
                            <span class="filter-label">Partner:</span>
                            <select style="width:100%;" name="partner_estatement_list[]" data-filter-catalogue="partner_estatement_list" data-filter-catalogue-title="Select Partner" multiple="multiple">
                                @foreach ($lists['partners'] as $key => $value)
                                    <option value="{{ $key }}">{{ $value }}</option>
                                @endforeach
                            </select>
                            <input data-filter-catalogue="partner_estatement_list" type="hidden" value="{{ Input::get('partner_estatement_list') }}">
                        </div>

                        <div class="filter-block filter-block-pricing-width">
                            <span class="filter-label">Country:</span>
                            <select style="width:100%;" name="country_list[]" data-filter-catalogue="country_list" data-filter-catalogue-title="Select Country" multiple="multiple">
                                @foreach ($lists['countries'] as $key => $value)
                                    <option value="{{ $key }}">{{ $value }}</option>
                                @endforeach
                            </select>
                            <input data-filter-catalogue="country_list" type="hidden" value="{{ Input::get('country_list') }}">
                        </div>

                        <div class="filter-block filter-block-pricing-width">
                            <span class="filter-label">Status:</span>
                            <select style="width:100%;" name="estatement_status[]" data-filter-catalogue="estatement_status" data-filter-catalogue-title="Select Status" multiple="multiple">
                                @foreach ($lists['statuses'] as $key => $value)
                                    <option value="{{ $key }}">{{ $value }}</option>
                                @endforeach
                            </select>
                            <input data-filter-catalogue="estatement_status" type="hidden" value="{{ Input::get('estatement_status') }}">
                        </div>

                        <div class="submit-filter-block disable-width">
                            <div style="float:right;">
                                <a href="<?php echo e(url('dashboard/estatement')); ?>" title="Clear Filters" class="pure-button pure-button-primary">
                                    Clear Filters <i class="fa fa-ban fa-fw"></i>
                                </a>
                            </div>
                            <button class="pure-button pure-button-primary" type="submit" title="" style="float:right;margin-right:15px;">
                                Submit Filters
                            </button>
                        </div>
                    </form>
                </div>
            </div>

            <table class="pure-table">
                <thead>
                    <tr>
                        <th><a href="{{ Filter::baseUrl('sort=id') }}">ID</a></th>
                        <th><a href="{{ Filter::baseUrl('sort=name') }}">Name</a></th>
                        <th>Partner</th>
                        <th>Delivery Date</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>

                <tbody>
                    @if(count($estatements) > 0)
                    @foreach ($estatements as $estatement)
                    <tr>
                        <td>
                            {{{ $estatement->id }}}
                        </td>
                        <td style="text-align:left;padding-left:20px;">
                            <a href="{{ url('dashboard/estatement/view/'.base64_encode($estatement->id)) }}" class="edit" title="Edit">{{ $estatement->title }}</a>
                        </td>
                        <td>
                            {{ $lists['partners'][$estatement->partner_id] }}
                        </td>
                        <td>
                            {{ isset($lists['monthDays'][$estatement->delivery_date]) ? $lists['monthDays'][$estatement->delivery_date] . ' at ' .  $estatement->time : '' }}
                        </td>
                        <td>
                            {{ $estatement->status }}
                        </td>
                        <td>
                            @if(I::can('view_estatement'))
                            <a href="{{ url('dashboard/estatement/view/'.base64_encode($estatement->id)) }}" class="edit" title="View">View</a>
                            @endif

                            @if(I::can('delete_estatement'))
                            <a href="{{ url('dashboard/estatement/delete/'.base64_encode($estatement->id)) }}" class="delete" title="Delete">Delete</a>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                    @else
                    <tr>
                        <td colspan="6">No E-Statements Found</td>
                    </tr>
                    @endif
                </tbody>
            </table>

            {{ $estatements->setPath('estatement')->appends(Input::except('page'))->links() }}

        </div>
    </div>

    <div id="tabs-2">
        <div class="padding">
            <div class="content-head pure-g">
                <div class="title pure-u-1-2">
                    <h1><i class="fa fa-list fa-fw"></i> E-Statements History ({{ $estatementHistory->count() }})</h1>
                </div>

                <div class="controls pure-u-1-2">
                    <form action="{{ Filter::searchUrl() }}" method="get">
                        <?php
                        $querystring = isset($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING'] : '';
                        if ($querystring != '') {
                            $queryArr = explode('&', $querystring);
                            foreach ($queryArr as $val) {
                                if (empty($val)) {
                                    continue;
                                }
                                $valArr = explode('=', $val);
                                if ($valArr[0] != "q") {
                                    echo '<input type="hidden" name="' . urldecode($valArr[0]) . '" value="' . $valArr[1] . '"> ';
                                }
                            }
                        }
                        ?>
                        <input type="text" placeholder="Search" name="q_history" value="{{ Input::get('q_history') }}" />
                        <button type="submit" title="" class="list-search"></button>
                    </form>
                </div>
            </div>

            <table class="pure-table">
                <thead>
                    <tr>
                        <th><a href="{{ Filter::baseUrl('sort=id') }}">ID</a></th>
                        <th><a href="{{ Filter::baseUrl('sort=name') }}">Name</a></th>
                        <th>Partner</th>
                        <th>Delivery Date</th>
                        <th>Sent At</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>

                <tbody>
                    @if(count($estatementHistory) > 0)
                    @foreach ($estatementHistory as $estatement_history)
                    <tr>
                        <td>
                            {{{ $estatement->id }}}
                        </td>
                        <td style="text-align:left;padding-left:20px;">
                            <a href="{{ url('dashboard/estatement/view/'.base64_encode($estatement_history->id)) }}" class="edit" title="Edit">{{ $estatement_history->title }}</a>
                        </td>
                        <td>
                            {{ $lists['partners'][$estatement_history->partner_id] }}
                        </td>
                        <td>
                            {{ $lists['monthDays'][$estatement_history->delivery_date] . ' at ' .  $estatement_history->time }}
                        </td>
                        <td>
                            {{ $estatement_history->created_at }}
                        </td>
                        <td>
                            {{ $estatement_history->status }}
                        </td>
                        <td>
                            @if(I::can('view_estatement'))
                            <a href="{{ url('dashboard/estatement/view_history/'.base64_encode($estatement_history->id)) }}" class="edit" title="View">View</a>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                    @else
                    <tr>
                        <td colspan="7">No E-Statement History Found</td>
                    </tr>
                    @endif
                </tbody>
            </table>

            {{ $estatements->setPath('estatement')->appends(Input::except('page'))->links() }}

        </div>
    </div>
</div>

@stop

@section('page_script')
<!--for tabs-->
<link rel="stylesheet" href="{{ asset('css/jquery-ui.css') }}">
@stop