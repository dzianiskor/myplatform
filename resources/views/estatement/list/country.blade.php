{{-- Country Listing --}}
<h3>
    @if(I::can_edit('estatement', $estatement))
    <a href="#dialog-add-country" title="New Country" class="dialog">
        Countries <i class="fa fa-plus-circle fa-fw"></i>
    </a>
    @else
        Countries
    @endif
</h3>
<ul id="country-listing">
    @include('estatement/list/countryList', array('estatement'=>$estatement))
</ul>
{{-- /Country Listing --}}
