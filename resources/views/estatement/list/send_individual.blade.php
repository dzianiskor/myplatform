{{-- Tiers Listing --}}
<h3>
        Send Test Email

</h3>
<fieldset>
         {{ Form::label('members-autocomplete', 'User', array('for'=>'members-autocomplete')) }}
	<input type="hidden" name="entity_id" id="entity_id" class="entity_id" value="" />
	<input id="members-autocomplete" class="report-input members-autocomplete pure-input-1-2" placeholder="Search" value="{{ (!empty(Input::get('entity')))? Support::memberNameFromID(Input::get('entity')) : null }}" />
</fieldset>
    <button type="button" id="send_test_email" data-estatement_id="{{ base64_encode($estatement->id) }}" class="pure-button pure-button-primary">Send Test Email</button>
{{-- /Tiers Listing --}}
