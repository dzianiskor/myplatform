@if(count($estatement->segments) > 0)
    @foreach($estatement->segments as $s)
        <li>
            {{ $s->name }}
            @if(I::can('delete_estatement') || I::am("BLU Admin"))
                <a href="/dashboard/estatement/unlink_segment/{{ base64_encode($estatement->id) }}/{{ base64_encode($s->id) }}" class="delete-ajax" data-id="{{ $s->id }}" data-target="#segment-listing">
                    [Delete]
                </a>
            @endif
        </li>
    @endforeach
@else
    <li class="empty">No Segments Associated</li>
@endif
