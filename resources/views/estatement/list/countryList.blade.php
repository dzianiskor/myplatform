@if(count($estatement->countries) > 0)
    @foreach($estatement->countries as $c)
        <li>
            {{ $c->name }}
            @if(I::can('delete_estatement'))
                <a href="/dashboard/estatement/unlink_country/{{ base64_encode($estatement->id) }}/{{ base64_encode($c->id) }}" class="delete-ajax" data-id="{{ $c->id }}" data-target="#country-listing">
                    [Delete]
                </a>
            @endif
        </li>
    @endforeach
@else
    <li class="empty">No Country Associated</li>
@endif
