{{-- Users Listing --}}
<h3>
    @if(I::can_edit('estatement', $estatement))
    <a href="#dialog-add-user" title="New User" class="dialog">
        Users <i class="fa fa-plus-circle fa-fw"></i>
    </a>
    @else
        Users
    @endif
</h3>
<ul id="user-listing">
    @include('estatement/list/userList', array('estatement'=>$estatement))
</ul>
{{-- /Users Listing --}}
