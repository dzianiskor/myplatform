{{-- Segment Listing --}}
<h3>
    @if(I::can_edit('estatement', $estatement))
    <a href="#dialog-add-segment" title="New Segment" class="dialog">
        Segments <i class="fa fa-plus-circle fa-fw"></i>
    </a>
    @else
        Segments
    @endif
</h3>
<ul id="segment-listing">
    @include('estatement/list/segmentList', array('estatement'=>$estatement))
</ul>
{{-- /Segment Listing --}}
