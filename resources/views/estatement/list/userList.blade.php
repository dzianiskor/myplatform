@if(count($estatement->users) > 0)
    @foreach($estatement->users as $u)
        <li>
            {{ $u->name }}
            @if(I::can('delete_estatement'))
                <a href="/dashboard/estatement/unlinkuser/{{ base64_encode($estatement->id) }}/{{ base64_encode($u->id) }}" class="delete-ajax" data-target="#user-listing">
                    [Delete]
                </a>
            @endif
        </li>
    @endforeach
@else
    <li class="empty">No Users Associated</li>
@endif