{{-- Tiers Listing --}}
<h3>
    @if(I::can_edit('estatement', $estatement))
    <a href="#dialog-add-tier" title="New Tier" class="dialog">
        Tiers <i class="fa fa-plus-circle fa-fw"></i>
    </a>
    @else
        Tiers
    @endif
</h3>
<ul id="tier-listing">
    @include('estatement/list/tierList', array('estatement'=>$estatement))
</ul>
{{-- /Tiers Listing --}}
