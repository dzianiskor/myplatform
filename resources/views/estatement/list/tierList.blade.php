@if(count($estatement->tiers) > 0)
    @foreach($estatement->tiers as $t)
        <li>
            {{ $t->name }}
            @if(I::can('delete_estatement'))
                @if (Auth::User()->partners->contains($t->partner_id) || I::am("BLU Admin"))
                    <a href="/dashboard/estatement/unlink_tier/{{ base64_encode($estatement->id) }}/{{ base64_encode($t->id) }}" class="delete-ajax" data-id="{{ $t->id }}" data-target="#tier-listing">
                        [Delete]
                    </a>
                @endif
            @endif
        </li>
    @endforeach
@else
    <li class="empty">No Tiers Associated</li>
@endif
