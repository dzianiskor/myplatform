
<div class="pure-u-1-2">
    <fieldset>
        <legend>Basic Details</legend>
        {{ Form::label('status', 'Status') }}
        <select name="status" class="pure-input-1-3">
            @foreach(Meta::offerStatuses() as $k => $v)
                @if($v == $estatement->status)
                    <option value="{{ $k }}" selected="">{{ ucfirst($v) }}</option>
                @else
                    <option value="{{ $k }}">{{ ucfirst($v) }}</option>
                @endif
            @endforeach
        </select>
        {{ Form::label('title', 'Title', array('for'=>'Title')) }}
        {{ Form::text('title', $estatement->title, array('class'=>'pure-input-1 required')) }}

        {{ Form::label('description', 'Description', array('for'=>'description')) }}
        {{ Form::textarea('description', $estatement->description, array('class'=>'pure-input-1')) }}

        <div class="pure-g">
            <div class="pure-u-1-2">
                {{ Form::label('partner_id', 'Partner', array('for'=>'partner_id')) }}
                {{ Form::select('partner_id', $partners, $estatement->partner_id, array('id' => 'partner-list', 'class' => 'pure-input-1 required')) }}
            </div>
        </div>
        
        <div class="pure-g">
            <div class="pure-u-1-2">
                {{ Form::label('delivery_date', 'Delivery Date', array('for'=>'partner_id')) }}
                {{ Form::select('delivery_date', $monthDays, $estatement->delivery_date, array('class' => 'pure-input-1')) }}
            </div>
        </div>
        
        <div class="pure-g">
            <div class="pure-u-1-2">
                  <div class="padding-right-10">
                    {{ Form::label('time', 'Time', array('for'=>'time')) }}
                    @if ($estatement->time == null)
                    {{ Form::text('time', '', array('class' => 'pure-input-1 timepicker')) }}
                    @else
                    {{ Form::text('time', date("H:i:s", strtotime($estatement->time)), array('class' => 'pure-input-1 timepicker')) }}
                    @endif
                </div>
            </div>
        </div>

    </fieldset>

    <fieldset>
        

    </fieldset>
</div>