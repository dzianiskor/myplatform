{{-- Segment Listing --}}
<h3>
    Segments
</h3>
<ul id="segment-listing">
    @include('estatement/history_list/segmentList', array('eStatementHistory'=>$estatementHistory))
</ul>
{{-- /Segment Listing --}}
