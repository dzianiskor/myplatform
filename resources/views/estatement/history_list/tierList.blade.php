<?php $tiers = json_decode($eStatementHistory->tiers, true); ?>
@if(count($tiers) > 0)
    @foreach($tiers as $tId)
    <?php $t = App\Country::find($tId); ?>
        <li>
            {{ $t->name }}
        </li>
    @endforeach
@else
    <li class="empty">No Tiers Associated</li>
@endif
