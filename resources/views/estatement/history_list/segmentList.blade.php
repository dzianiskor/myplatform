<?php $segments = json_decode($eStatementHistory->segments, true); ?>
@if(count($segments) > 0)
    @foreach($segments as $sId)
    <?php $s = App\Segment::find($sId); ?>
        <li>
            {{ $s->name }}
        </li>
    @endforeach
@else
    <li class="empty">No Segments Associated</li>
@endif
