<?php $countries = json_decode($eStatementHistory->coutries, true); ?>
@if(count($countries) > 0)
    @foreach($countries as $cId)
    <?php $c = App\Country::find($cId); ?>
        <li>
            {{ $c->name }}
        </li>
    @endforeach
@else
    <li class="empty">No Country Associated</li>
@endif
