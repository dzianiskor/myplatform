@extends('layouts.dashboard')

@section('title')
    Reward Types | BLU
@stop

@section('body')
    <div class="padding">

        <div class="content-head pure-g">
            <div class="title pure-u-1-2">
                <h1><i class="fa fa-star fa-fw"></i> Reward Types (4)</h1>
            </div>

            <div class="controls pure-u-1-2">
                <form action="{{ Filter::searchUrl() }}" method="get">
                    <input type="text" placeholder="Search" name="q" value="{{ Input::get('q') }}" />
                    <button type="submit" title="" class="list-search"></button>
                </form>
                    <a href="{{ url('dashboard/rewardtypes/create') }}" class="pure-button pure-button-primary">New Reward</a>
            </div>
        </div>

        <table class="pure-table">
            <thead>
                <tr>
                    <th width="50">ID</th>
                    <th style="text-align:left;padding-left:20px;"><a href="{{ Filter::baseUrl('sort=name') }}">Name</a></th>
                    <th><a href="{{ Filter::baseUrl('sort=partner_id') }}">Partner</a></th>
                    <th>Status</th>
                    <th width="150">Actions</th>
                </tr>
            </thead>
            <tbody>
                    <tr>
                        <td>
                            1
                        </td>
                        <td style="text-align:left;padding-left:20px;">
                            Welcome Rewards
                        </td>                      
                        <td>
                            Jumeirah
                        </td>          
                        <td>
                            <span class="ico-yes"></span>
                        </td>
                        <td>
                            @if(I::can('view_stampreward'))
                                <a href="/dashboard/rewardtypes/edit/1" class="edit" title="View">View</a>
                            @endif
                            
                            @if(I::can('delete_stampreward'))
                                <a href="/dashboard/rewardtypes/delete/1" class="delete" title="Delete">Delete</a>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td>
                            2
                        </td>
                        <td style="text-align:left;padding-left:20px;">
                            Spend Rewards
                        </td>                      
                        <td>
                            Jumeirah
                        </td>          
                        <td>
                            <span class="ico-yes"></span>
                        </td>
                        <td>
                            @if(I::can('view_stampreward'))
                                <a href="/dashboard/rewardtypes/edit/2" class="edit" title="View">View</a>
                            @endif
                            
                            @if(I::can('delete_stampreward'))
                                <a href="/dashboard/rewardtypes/delete/2" class="delete" title="Delete">Delete</a>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td>
                            3
                        </td>
                        <td style="text-align:left;padding-left:20px;">
                            Surprise Rewards
                        </td>                      
                        <td>
                            Jumeirah
                        </td>          
                        <td>
                            <span class="ico-yes"></span>
                        </td>
                        <td>
                            @if(I::can('view_stampreward'))
                                <a href="/dashboard/rewardtypes/edit/3" class="edit" title="View">View</a>
                            @endif
                            
                            @if(I::can('delete_stampreward'))
                                <a href="/dashboard/rewardtypes/delete/3" class="delete" title="Delete">Delete</a>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td>
                            4
                        </td>
                        <td style="text-align:left;padding-left:20px;">
                            On-going Offers
                        </td>                      
                        <td>
                            Jumeirah
                        </td>          
                        <td>
                            <span class="ico-yes"></span>
                        </td>
                        <td>
                            @if(I::can('view_stampreward'))
                                <a href="/dashboard/rewardtypes/edit/4" class="edit" title="View">View</a>
                            @endif
                            
                            @if(I::can('delete_stampreward'))
                                <a href="/dashboard/rewardtypes/delete/4" class="delete" title="Delete">Delete</a>
                            @endif
                        </td>
                    </tr>
            </tbody>
        </table>

        
    </div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-rewardtypes');
    </script>
@stop
