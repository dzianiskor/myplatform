@extends("layouts.dashboard")
<?php
//    echo "<pre>";
//    var_dump($partners);
//    
//    echo "<BR> Stamp Reward";
//    var_dump($stampreward);
//    exit();
?>
@section('title')
    Reward Type | BLU
@stop

@section('body')

    <div class="padding">

        <div class="content-head pure-g">
            <div class="title pure-u-1-2">
                    <h1><i class="fa fa-star fa-fw"></i>New Reward Type</h1>
            </div>

            <div class="controls pure-u-1-2">
                <a href="{{ url('dashboard/rewardtypes') }}" class="pure-button">All Reward Type</a>
            </div>
        </div>

        {{ Form::open(array('url'   => '/dashboard/rewardtypes/update/1',
                                       'class' => 'pure-form pure-form-stacked form-width main-form', 
                                       'files'  => true,
                                       'id'    => 'rewardtypeform')) }}

            <fieldset>
                <legend>
                    Basic Details
                </legend>

                {{ Form::label('partner_id', 'Partner') }}
                <select name="partner_id" class="pure-input-1-3">
                    <option disabled="disabled">Select Partner</option>
                        <option value="1">Jumeirah</option>
                </select>

                {{ Form::label('name', 'Reward Type Name') }}
                {{ Form::text('name', '', array('class' => 'pure-input-1 required')) }}

                {{ Form::label('display_online', 'Display Online') }}

                <select name="display_online" class="pure-input-1-3">
                            <option value="0">No</option>
                            <option value="1"  selected>Yes</option>
                </select>
            </fieldset>

            {{--- Form Buttons ---}}
            <div class="form-buttons">
                <div class="left">
                    <a href="{{ url('dashboard/rewardtypes') }}" class="pure-button">Cancel</a>
                </div>

                        <div class="right">
                            <button type="submit" class="pure-button pure-button-primary">Save Reward Type</button>
                        </div>

                <div class="clearfix"></div>
            </div>
            {{--- /Form Buttons ---}}          
        
        {{ Form::close() }}
    </div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-rewardtypes');        
    </script>
@stop