@extends('layouts.dashboard')

@section('title')
    Cardprints | BLU
@stop

@section('body')
<div class="padding">

    <div class="content-head pure-g">
        <div class="title pure-u-1-2">
            <h1><i class="fa fa-picture-o fa-fw"></i>Cardprints ({{ $cardprints->total() }})</h1>
        </div>

        <div class="controls pure-u-1-2">
            <form action="{{ Filter::searchUrl() }}" method="get">
                <input type="text" placeholder="Search" name="q" value="{{ Input::get('q') }}" />
                <button type="submit" title="" class="list-search"></button>
            </form>
            @if(I::can('create_card_print'))
                <a href="{{ url('dashboard/cardprints/create') }}" class="pure-button pure-button-primary">Add Cardprint</a>
            @endif
        </div>

        <div id="catalog-filter" class="pure-u-1-2">
            <input id="filter_route" type="hidden" value="cardprints">
            <form id="filter-catalogue" action="{{ Filter::searchUrl() }}" method="get">

                <div class="filter-block filter-block-pricing-width">
                    <span class="filter-label">Partner:</span>
                    <select style="width:100%;" name="partner_cardprint_list[]" data-filter-catalogue="partner_cardprint_list" data-filter-catalogue-title="Select Partner" multiple="multiple">
                        @foreach ($lists['partners'] as $key => $value)
                            <option value="{{ $key }}">{{ $value }}</option>
                        @endforeach
                    </select>
                    <input data-filter-catalogue="partner_cardprint_list" type="hidden" value="{{ Input::get('partner_cardprint_list') }}">
                </div>

                <div class="submit-filter-block disable-width">
                    <div style="float:right;">
                        <a href="<?php echo e(url('dashboard/cardprints')); ?>" title="Clear Filters" class="pure-button pure-button-primary">
                            Clear Filters <i class="fa fa-ban fa-fw"></i>
                        </a>
                    </div>
                    <button class="pure-button pure-button-primary" type="submit" title="" style="float:right;margin-right:15px;">
                        Submit Filters
                    </button>
                </div>
            </form>
        </div>
    </div>

    <table class="pure-table">
        <thead>
            <tr>
                <th width="50"><a href="{{ Filter::baseUrl('sort=id') }}">ID</a></th>
                <th width="90">Preview</th>
                <th style="text-align:left;padding-left:20px"><a href="{{ Filter::baseUrl('sort=name') }}">Name</a></th>
                <th style="text-align:center;">Partner</th>
				{{--<th>Website Status</th>--}}
				{{--<th width="90">Mobile Preview</th>--}}
				{{--<th>Mobile Status</th>--}}
                <th width="150">Actions</th>
            </tr>
        </thead>

        <tbody>
        @if(count($cardprints) > 0)
            @foreach ($cardprints as $b)
                <tr>
                    <td>
                        {{ $b->id }}
                    </td>
                    <td>
                        @if(empty($b->image()))
                            <img src="http://placehold.it/74x74" alt="" width="80px" />
                        @else
                            <img src="<?php echo App\Http\Controllers\MediaController::getImageFromId( $b->image()->id);?>?s=74x74" alt="" width="80px" />
                        @endif

                    </td>
                    <td style="text-align:left;padding-left:20px">
                        <a href="{{ url('dashboard/cardprints/edit/'.base64_encode($b->id)) }}" class="edit" title="View">{{{ $b->name }}}</a>
                    </td>
                    <td style="text-align:center;">
                        <?php
                        if(isset($b->partner->name)){
                            echo $b->partner->name;
                        }
                        else{
                            echo 'N/A';
                        }
                        ?>

                    </td>
					
                    <td>
                        <a href="{{ url('dashboard/cardprints/edit/'.base64_encode($b->id)) }}" class="edit" title="View">View</a>

                        @if(I::can('delete_cardprints'))
                            <a href="{{ url('dashboard/cardprints/delete/'.base64_encode($b->id)) }}" class="delete" title="Delete">Delete</a>
                        @endif
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="8">No Cardprints Found</td>
            </tr>
        @endif
        </tbody>
    </table>

    {{ $cardprints->setPath('cardprints')->appends(Input::except('page'))->links() }}

</div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-cardprints');
    </script>
@stop
