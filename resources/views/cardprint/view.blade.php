@extends('layouts.dashboard')

@section('title')
    Update Cardprint | BLU
@stop

@section('dialogs')

@if($errors->any())
<h4>{{$errors->first()}}</h4>
@endif
        
@stop

@section('body')
<div class="padding">

    <div class="content-head pure-g">
        <div class="title pure-u-1-2">
			@if($cardprint->draft)
            <h1><i class="fa fa-book fa-fw"></i>Cardprint &raquo; New Cardprint</h1>
			@else
            <h1><i class="fa fa-book fa-fw"></i>Cardprint &raquo; {{{ $cardprint->name }}}</h1>
			@endif
        </div>

        <div class="controls pure-u-1-2">
            <a href="{{ url('dashboard/cardprints') }}" class="pure-button">All Cardprints</a>
        </div>
    </div>

    {{ Form::model($cardprint, array('url'    => url('dashboard/cardprints/update/'.base64_encode($cardprint->id)),
                                  'files'  => true,
                                  'method' => 'post',
                                  'class'  => "pure-form pure-form-stacked form-width")) }}

        @include('partials.errors')

        {!! Form::hidden('redirect_to', URL::previous()) !!}

	    <div class="pure-g">
	        {{--- Left Section Content ---}}
			<div class="pure-u-1">
			    <fieldset>
			        <legend>Basic Details</legend>
			        {{ Form::label('name', 'Cardprint Name', array('for'=>'name')) }}
			        {{ Form::text('name', $cardprint->name, array('class'=>'pure-input-1 required')) }}

			        {{ Form::label('description', 'Description', array('for'=>'description')) }}
			        {{ Form::text('description', $cardprint->description, array('class'=>'pure-input-1')) }}

			        {{ Form::label('partner_id', 'Partner') }}
			        <select name="partner_id" class="pure-input-1-2 required" id="partner_id" data-draft="{{ $cardprint->draft }}" data-id="{{ $cardprint->id }}">
			            @if($cardprint->partner)
			                <option value="{{ $cardprint->partner->id }}">{{ $cardprint->partner->name }}</option>
			                @foreach($partners as $id => $name)
			                    @if($id != $cardprint->partner->id)
			                        <option value="{{ $id }}">{{ $name }}</option>
			                    @endif
			                @endforeach
			            @else
			                @foreach($partners as $id => $name)
			                    <option value="{{ $id }}">{{ $name }}</option>
			                @endforeach
			            @endif
			        </select>
                                
                                {{ Form::label('top', 'Barcode Position from Top', array('for'=>'top')) }}
			        {{ Form::text('top', $cardprint->top, array('class'=>'pure-input-1')) }}
                                
                                {{ Form::label('left', 'Barcode Position from Left', array('for'=>'left')) }}
			        {{ Form::text('left', $cardprint->left, array('class'=>'pure-input-1')) }}
				
			    </fieldset>

			    <fieldset>
			        <legend>Cardprint Image</legend>

					<span class="info">
						For best results, use images that are 324 x 204 (pixels) in size.
					</span>
                                
			        <div class="image-rail" style="width:340px;">
			            <table style="margin-top:20px;margin-bottom:20px;">
			                <tr>
			                    <td width="80">
			                        @if(empty($cardprint->image()))
			                            <img src="http://placehold.it/74x74" alt="" width="80px" />
			                        @else
			                            <img src="<?php echo App\Http\Controllers\MediaController::getImageFromId($cardprint->image()->id) ?>?s=74x74" alt="" width="80px" />
			                        @endif
			                    </td>
			                    <td style="padding-left:10px;">
			                        {{ Form::label('image', 'Cardprint Image') }}
			                        {{ Form::file('image') }}
			                    </td>
			                </tr>
			            </table>
			        </div>
                                @if(empty($cardprint->image()))
                                    <div id="box" style="width:324px; height:204px; background: #aaaaaa; border: 1px solid #000;"></div>
                                @else
                                    <div id="box" style="background-image: url('<?php echo App\Http\Controllers\MediaController::getImageFromId( $cardprint->image()->id);?>'); width:324px; height:204px;  border: 1px solid #000;"></div>
                                @endif
                                
			    </fieldset>

                           
			</div>
	        {{--- /Left Section Content ---}}

	        
	    </div>

        <!-- Form Buttons -->
        <div class="form-buttons">
            <div class="left">
                <a href="{{ url('dashboard/cardprints') }}" class="pure-button">Cancel</a>
            </div>
            @if($cardprint->draft)
                @if(I::can('create_card_print'))
                    <div class="right">
                        <button type="submit" class="pure-button pure-button-primary">Save Cardprint</button>
                    </div>
                @endif
            @else
                @if(I::can('edit_cardprints'))
                    <div class="right">
                        <button type="submit" class="pure-button pure-button-primary">Update Cardprint</button>
                    </div>
                @endif
            @endif
            <div class="clearfix"></div>
        </div>
        <!-- /Form Buttons -->
    {{ Form::close() }}
</div>
@stop

@section('page_script')

@if(!I::can('edit_cardprints') && !I::can('create_card_print'))
    <script>
        enqueue_script('disable-form');
    </script>
@endif

<script type="text/javascript">
    $(document).ready(function() {
        $("#box").mousedown(function(event){            
            var relX = event.pageX - $(this).offset().left;
            var relY = event.pageY - $(this).offset().top;
            
            $("#top").val(parseInt(relY));
            $("#left").val(parseInt(relX));
        });
    });
</script>
    <script>
        set_menu('mnu-cardprints');
    </script>
@stop
