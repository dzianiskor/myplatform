@if(count($user->partners) > 0)
    @foreach($user->partners as $p)
        <li>
            {{ $p->name }} <a href="/dashboard/partners/unlink/{{ base64_encode($user->id) }}/{{ base64_encode($p->id) }}" class="delete-ajax" data-id="{{ $p->id }}" data-target="#partner-listing">
                [Delete]
            </a>
        </li>
    @endforeach
@else
    <li class="empty">No Partners Associated</li>
@endif