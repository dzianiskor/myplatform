@extends('layouts.dashboard')

@section('title')
    Partners | BLU
@stop

@section('body')
    <div class="padding">
        <div class="content-head pure-g">
            <div class="title pure-u-1-2">
                <h1><i class="fa fa-map-marker fa-fw"></i>Partners ({{ $partners->total() }})</h1>
            </div>

            <div class="controls pure-u-1-2">
                <form action="{{ Filter::searchUrl() }}" method="get">
                    <input type="text" placeholder="Search" name="q" value="{{ Input::get('q') }}" />
                    <button type="submit" title="" class="list-search"></button>
                </form>
                @if(I::can('create_partners'))
                    <a href="{{ url('dashboard/partners/new') }}" class="pure-button pure-button-primary">Add Partner</a>
                @endif
            </div>

            <div id="catalog-filter" class="pure-u-1-2">
                <input id="filter_route" type="hidden" value="partners">

                <form id="filter-catalogue" action="{{ Filter::searchUrl() }}" method="get">

                    <div class="filter-block filter-block-pricing-width">
                        <span class="filter-label">Partner:</span>
                        <select style="width:100%;" name="parent_list[]" id="parent_list_select" multiple="multiple">
                            @foreach ($lists['partnerData'] as $key => $value)
                                <option value="{{ $key }}">{{ $value }}</option>
                            @endforeach
                        </select>
                        <input id="filterPartners" type="hidden" value="{{ Input::get('parent_list') }}">
                    </div>

                    <div class="filter-block filter-block-pricing-width">
                        <span class="filter-label">Network:</span>
                        <select style="width:100%;" name="network_list[]" id="network_list_select" multiple="multiple">
                            @foreach ($lists['networks'] as $key => $value)
                                <option value="{{ $key }}">{{ $value }}</option>
                            @endforeach
                        </select>
                        <input id="filterNetworks" type="hidden" value="{{ Input::get('network_list') }}">
                    </div>

                    <div class="filter-block filter-block-pricing-width">
                        <span class="filter-label">Category:</span>
                        <select style="width:100%;" name="category_list[]" id="category_list_select" multiple="multiple">
                            @foreach ($lists['categories'] as $key => $value)
                                <option value="{{ $key }}">{{ $value }}</option>
                            @endforeach
                        </select>
                        <input id="filterCategories" type="hidden" value="{{ Input::get('category_list') }}">
                    </div>

                    <div class="submit-filter-block disable-width">
                        <div style="float:right;">
                            <a href="{{ url('dashboard/partners') }}" title="Clear Filters" class="pure-button pure-button-primary">
                                Clear Filters <i class="fa fa-ban fa-fw"></i>
                            </a>
                        </div>

                        <button class="pure-button pure-button-primary" type="submit" title="" style="float:right;margin-right:15px;">
                            Submit Filters
                        </button>
                    </div>

                </form>
            </div>


        </div>

        <table class="pure-table partner_list">
            <thead>
                <tr>
                    <th width="50">
                        <a href="{{ Filter::baseUrl('sort=id') }}">ID</a>
                    </th>
                    <th width="90">Image</th>
                    <th style="text-align:left;padding-left:20px;">
                        <a href="{{ Filter::baseUrl('sort=name') }}">Name</a>
                    </th>
                    <th>
                        <a href="{{ Filter::baseUrl('sort=status') }}">Enabled</a>
                    </th>
                    <th>Network</th>
                    <th>Category</th>
                    <th>Parent Partner</th>
                    <th>Country</th>
                    <th width="150">Actions</th>
                </tr>
            </thead>

            <tbody>
                @if(count($partners) > 0)
                    @foreach($partners as $p)
                        <tr>
                            <td>
                                {{ $p->id }}
                            </td>
                            <td>
                                @if(empty($p->image))
                                    <img src="https://placehold.it/74x74" alt="" />
                                @else
                                    <img src="<?php echo App\Http\Controllers\MediaController::getImageFromId($p->image) ?>?s=74x74" alt="" />
                                @endif
                            </td>
                            </td>
                            <td style="text-align:left;padding-left:20px;">
                                <a href="/dashboard/partners/view/{{base64_encode($p->id) }}" title="View Partner">
                                    {{ ucwords($p->name) }}
                                </a>
                            </td>
                            <td>
                                @if($p->status == 'Enabled')
                                    <span class="ico-yes"></span>
                                @else
                                    <span class="ico-no"></span>
                                @endif
                            </td>
                            <td>
                                @if($p->networks->first())
                                    {{ $p->networks->first()->name }}
                                @else
                                    <i>N/A</i>
                                @endif
                            </td>
                            <td>
                                @if ($p->category)
                                    {{ $p->category->name }}
                                @else
                                    <i>N/A</i>
                                @endif
                            </td>
                            <td>
                                @if ($p->parent)
                                    {{ $p->parent->name }}
                                @else
                                    <i>N/A</i>
                                @endif
                            </td>
                            <td>
                                @if ($p->country)
                                    {{ $p->country->name }}
                                @else
                                    <i>N/A</i>
                                @endif
                            </td>
                            <td>
                                @if(I::can('view_partners'))
                                    <a href="/dashboard/partners/view/{{ base64_encode($p->id) }}" class="edit" title="View">View</a>
                                @endif

                                @if(I::can('delete_partners') && $p->id != 1)
                                    <a href="/dashboard/partners/delete/{{ base64_encode($p->id) }}" class="delete" title="Delete">Delete</a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="9">No Partners Found</td>
                    </tr>
                @endif
            </tbody>
        </table>

        {{ $partners->setPath('partners')->appends(Input::except('page'))->links() }}

    </div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-partners');
    </script>
@stop
