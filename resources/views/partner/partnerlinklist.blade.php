@if($partner->partnerlinked->count() == 0)
    <li class="empty">No Partners Associated</li>
@else
    
    @foreach($partner->partnerlinked as $c)
        <?php
//            echo "<pre>";
//            var_dump($c);
//            var_dump(App\Partner::find($c->partner_linked_id));
//            exit();
            $l_partner = App\Partner::find($c->partner_linked_id);
        ?>
        @if($c->partner_linked_id)
            <li>
                <div style="width: 80%;">{{{ $l_partner->name }}}</div>
				@if(I::can('delete_partners'))
				    <div>
                        <a href="/dashboard/partners/detach/partnerlink/{{ base64_encode($partner->id) }}/{{ base64_encode($c->partner_linked_id) }}" class="delete-ajax" data-target="#partner-link-listing">
                            [Delete]
                        </a>
                    </div>
                @endif
            </li>
        @else
            <li>
                <span style="color:#a95656;font-style:italic;">Partner no longer exists</span>
				@if(I::can('delete_partners'))
					<a href="/dashboard/partners/detach/partnerlink/{{ base64_encode($partner->id) }}/{{ base64_encode($c->partner_linked_id) }}" class="delete-ajax" data-target="#partner-link-listing">
                        [Delete]
                    </a>
                @endif
            </li>
        @endif
    @endforeach
@endif
