@if(count($supplier->partners) > 0)
    @foreach($supplier->partners as $p)
        <li>
            {{ $p->name }} <a href="/dashboard/partners/unlinksupplier/{{ base64_encode($supplier->id) }}/{{ base64_encode($p->id) }}" class="delete-ajax" data-id="{{ $p->id }}" data-target="#partner-listing">
                [Delete]
            </a>
        </li>
    @endforeach
@else
    <li class="empty">No Partners Associated</li>
@endif