@extends('layouts.dashboard')

@section('title')
Partner &raquo; {{{ $partner->name }}}  | BLU
@stop
<style>		
    .ui-autocomplete-loading {		
        background:url(/img/loader.gif) no-repeat center right;		
    }		
    li.ui-menu-item{		
        font-size:12px;		
    }		
</style>
@section('dialogs')

{{-- Assign new Channel --}}
<div id="dialog-add-channel" style="width:300px;">
    <h4>Add Display Channel</h4>

    {{ Form::open(array('class' => "pure-form pure-form-stacked")) }}
    {{ Form::select('channel_id', $partners, null, array('class' => 'pure-input-1')) }}

    <a href="/dashboard/partners/attach/channel/{{ base64_encode($partner->id)  }}" class="pure-button pure-button-primary attribute-action" data-target="#channel-listing">
        Save
    </a>
    {{ Form::close() }}
</div>

{{-- Assign new Linked Partner --}}
<div id="dialog-add-partner-linked" style="width:300px;">
    <h4>Add Linked Partner</h4>

    {{ Form::open(array('class' => "pure-form pure-form-stacked")) }}
    {{ Form::select('partner_link_id', $partners, null, array('class' => 'pure-input-1')) }}

    <a href="/dashboard/partners/attach/partnerlink/{{ base64_encode($partner->id)}}" class="pure-button pure-button-primary attribute-action" data-target="#partner-link-listing">
        Save
    </a>
    {{ Form::close() }}
</div>

{{-- Assign new Country --}}
<div id="dialog-add-country" style="width:300px;">
    <h4>Add Display Country</h4>

    {{ Form::open(array('class' => "pure-form pure-form-stacked")) }}
    {{ Form::select('country_id', $countries, null, array('class' => 'pure-input-1')) }}

    <a href="/dashboard/partners/link_country/{{ base64_encode($partner->id)  }}" class="pure-button pure-button-primary attribute-action" data-target="#country-listing">
        Save
    </a>
    {{ Form::close() }}
</div>

{-- Assign new Auto Generated Report --}}
<div id="dialog-add-agr" style="width:300px;">
    <h4>Add Report</h4>

    {{ Form::open(array('class' => "pure-form pure-form-stacked")) }}
    {{ Form::label('frequency', 'Frequency') }}
    {{ Form::select('frequency', array('not selected'=>'Not Selected', 'daily'=>'Daily', 'weekly'=>'Weekly', 'monthly'=>'Monthly', 'yearly'=>'Yearly'), null, array('class' => 'pure-input-1')) }}
    {{ Form::label('agr_id', 'Reports') }}
    {{ Form::select('agr_id', $agr_reports, null, array('class' => 'pure-input-1')) }}

    <a href="/dashboard/partners/attach/agreport/{{  base64_encode($partner->id)}}" class="pure-button pure-button-primary attribute-action" data-target="#agr-listing">
        Save
    </a>
    {{ Form::close() }}
</div>
@stop

@section('body')
<div class="padding">

    {{-- Page Header --}}
    <div class="content-head pure-g">
        <div class="title pure-u-1-2">
            <h1><i class="fa fa-map-marker fa-fw"></i>Partner &raquo; {{{ $partner->name }}}</h1>
        </div>

        <div class="controls pure-u-1-2">
            <a href="{{ url('dashboard/partners') }}" class="pure-button">All Partners</a>
        </div>
    </div>
    {{-- /Page Header --}}

    {{ Form::open(array('url' => url('dashboard/partners/update/'.base64_encode($partner->id)), 'files' => true, 'method' => 'post', 'class' => "pure-form pure-form-stacked form-width", 'id' => 'partnerForm')) }}

    @include('partials.errors')

    {!! Form::hidden('redirect_to', URL::previous('dashboard/partners')) !!}

    <div class="pure-g">
        {{--- Left Section Content ---}}
        <div class="pure-u-3-5">
            <fieldset>
                <legend>Basic Details</legend>

                {{ Form::label('status', 'Status') }}
                {{ Form::select('status', $statuses, $partner->status, array('class' => 'pure-input-1-3')) }}

                {{ Form::label('ucid', 'Unique Customer ID (Auto-generated)') }}
                {{ Form::text('ucid', $partner->ucid, array('class' => 'pure-input-1', 'maxlength' => 9, 'disabled' => 'disabled')) }}

                {{ Form::label('name', 'Partner Name') }}
                {{ Form::text('name', $partner->name, array('class' => 'pure-input-1 required')) }}

                {{ Form::hidden('partner_id', $partner->id, array('class' => 'pure-input-1')) }}
                {{ Form::hidden('draft', $partner->draft, array('class' => 'pure-input-1')) }}

                {{ Form::label('prefix', 'Prefix') }}
                {{ Form::text('prefix', $partner->prefix, array('class' => 'pure-input-1 required')) }}

                <div class="color-wrapper">
                    {{ Form::label('color', 'Primary Color') }}
                    {{ Form::text('color', $partner->color, array('class' => 'pure-input-1')) }}
                </div>

                <div class="color-wrapper">
                    {{ Form::label('secondary_color', 'Secondary Color') }}
                    {{ Form::text('secondary_color', $partner->secondary_color, array('class' => 'pure-input-1')) }}
                </div>

                {{ Form::label('parent_partner_mapping_id', 'Parent Partner Mapping ID') }}
                {{ Form::text('parent_partner_mapping_id', $partner->parent_partner_mapping_id, array('class' => 'pure-input-1-2')) }}


                {{ Form::label('description', 'Partner Description') }}
                {{ Form::textarea('description', $partner->description, array('class' => 'pure-input-1', 'rows' => 10)) }}

                <div class="pure-g">
                    <div class="pure-u-1-2">
                        <div class="padding-right-10">
                            {{ Form::label('contact_name', 'Contact Name') }}
                            {{ Form::text('contact_name', $partner->contact_name, array('class' => 'pure-input-1')) }}
                        </div>
                    </div>

                    <div class="pure-u-1-2">
                        {{ Form::label('contact_email', 'Contact Email') }}
                        {{ Form::text('contact_email', $partner->contact_email, array('class' => 'pure-input-1')) }}
                    </div>

                    <div class="pure-u-1-2">
                        <div class="padding-right-10">
                            {{ Form::label('contact_number', 'Contact Number') }}
                            {{ Form::text('contact_number', $partner->contact_number, array('class' => 'pure-input-1 enforceNumeric')) }}
                        </div>
                    </div>

                    <div class="pure-u-1-2">
                        {{ Form::label('contact_website', 'Contact Website') }}
                        {{ Form::text('contact_website', $partner->contact_website, array('class' => 'pure-input-1')) }}
                    </div>

                    <div class="pure-u-1-2">
                        <div class="padding-right-10">
                            {{ Form::label('general_manager', 'General Manager') }}
                            {{ Form::text('general_manager', $partner->general_manager, array('class' => 'pure-input-1')) }}
                        </div>
                    </div>

                    <div class="pure-u-1-2">
                        {{ Form::label('marketing_manager', 'Marketing Manager') }}
                        {{ Form::text('marketing_manager', $partner->marketing_manager, array('class' => 'pure-input-1')) }}
                    </div>

                    <div class="pure-u-1-2">
                        {{ Markup::checkbox('prevent_multiple_login', 'Prevent Multiple Login', $partner->prevent_multiple_login) }}
                    </div>
                </div>

                @if($partner->id != 1)
                {{ Form::label('parent_id', 'Parent Partner') }}
                <select name="parent_id" class="pure-input-1-3 required" id="parent_id" data-draft="{{ $partner->draft }}" data-id="{{ $partner->id }}">
                    @if($partner->parent)
                    <option value="{{ $partner->parent->id }}">{{ $partner->parent->name }}</option>
                    @foreach($partners as $id => $name)
                    @if($id != $partner->parent->id)
                    <option value="{{ $id }}">{{ $name }}</option>
                    @endif
                    @endforeach
                    @else
                    @foreach($partners as $id => $name)
                    <option value="{{ $id }}">{{ $name }}</option>
                    @endforeach
                    @endif
                </select>

                @endif

                @if(I::am('BLU Admin') || I::am('BLU Admin Dev'))
                {{ Markup::checkbox('is_top_level_partner', 'Is Top Level Partner', $partner->is_top_level_partner) }}
                @endif

                {{ Form::label('category_id', 'Partner Category') }}
                {{ Form::select('category_id', $categories, $partner->category_id, array('class' => 'pure-input-1-3 required')) }}

                {{ Form::label('network_id', 'Partner Network') }}

                @if(count($partner->networks))
                    {{ Form::select('network_id', $networks, $partner->networks->first()->id, array('class' => 'pure-input-1-3 required')) }}
                @else
                    {{ Form::select('network_id', $networks, null, array('class' => 'pure-input-1-3 required')) }}
                @endif

                <table class="partner_view" style="margin-top:20px">
                    <tr>
                        <td width="80">
                            @if(empty($partner->image))
                            <img src="https://placehold.it/74x74" alt="" />
                            @else
                            <img src="<?php echo App\Http\Controllers\MediaController::getImageFromId($partner->image) ?>?s=74x74" alt="" />
                            @endif
                        </td>
                        <td>
                            {{ Form::label('image', 'Cover Image') }}
                            {{ Form::file('image') }}
                        </td>
                    </tr>
                </table>

            </fieldset>

            <fieldset>
                <legend>Address Details</legend>

                {{ Form::label('country_id', 'Country') }}
                {{ Markup::country_select('country_id', $partner->country_id, array('class' => 'pure-input-1-3')) }}

                {{ Form::label('area_id', 'Area') }}
                {{ Markup::area_select('area_id', $partner->country_id, $partner->area_id, array('class' => 'pure-input-1-3')) }}

                {{ Form::label('city_id', 'City') }}
                {{ Markup::city_select('city_id', $partner->area_id, $partner->city_id, array('class' => 'pure-input-1-3')) }}

                {{ Form::label('address_1', 'Address') }}
                {{ Form::text('address_1', $partner->address_1, array('class' => 'pure-input-1')) }}

                {{ Form::label('address_2', 'Address (Continued)') }}
                {{ Form::text('address_2', $partner->address_2, array('class' => 'pure-input-1')) }}
            </fieldset>

            <fieldset>
                <legend>SMS Setup</legend>

                {{ Form::label('smsgate', 'SMS Gateway') }}
                {{ Form::select('smsgate', $gateways, $partner->smsgate, array('class' => 'pure-input-1-2')) }}

                {{ Form::label('smsgate_username', 'Gateway Username') }}
                {{ Form::text('smsgate_username', $partner->smsgate_username, array('class' => 'pure-input-1-2')) }}

                {{ Form::label('smsgate_password', 'Gateway Password / Pin') }}
                {{ Form::password('smsgate_password', array('class' => 'pure-input-1-2')) }}

                {{ Form::label('smsgate_number', 'Gateway Number / Sender ID') }}
                {{ Form::text('smsgate_number', $partner->smsgate_number, array('class' => 'pure-input-1-2')) }}

                {{ Form::label('welcome_sms', 'Welcome SMS') }}
                {{ Form::textarea('welcome_sms', $partner->welcome_sms, array('class' => 'pure-input-1', 'rows' => 10, 'placeholder'=>'Do not use carriage return.')) }}
            </fieldset>

            @if(I::belong(1))
            <?php
            $emailWhitelabelingData = $partner->emailwhitelabelings;
            foreach ($emailWhitelabelingData as $emailWhitelabeling) {
                $emailWhiteLabel[$emailWhitelabeling->field][$emailWhitelabeling->lang] = $emailWhitelabeling->value;
            }
            ?>
            <fieldset>
                <legend>Email White Label Setup</legend>
                <div class="pure-g" id="program_name_section">
                    @if(!empty($emailWhiteLabel['program_name']))
                    @foreach($emailWhiteLabel['program_name'] as $lang => $programName)
                    <div id="program_name_div" style="width: 100%">
                        <div class="pure-u-1-2">
                            <div class="padding-right-10">
                                {{ Form::label('program_name', 'Program Name') }}
                                {{ Form::text('program_name[]', $programName, array('class' => 'pure-input-1')) }}
                            </div>
                        </div>

                        <div class="pure-u-1-2">
                            {{ Form::label('lang_name', 'Language') }}
                            {{ Form::select('lang_name[]', $languages, $lang, array('class' => 'pure-input-1')) }}
                        </div>
                        @if($programName === reset($emailWhiteLabel['program_name']))
                        <a href="#" class="remove-cloned" title="remove" style="float: right; position: absolute; margin-top: 40px; margin-left: -20px; display: none;">
                            <i class="fa fa-trash-o fa-fw"></i>
                        </a>
                        @else
                        <a href="#" class="remove-cloned" title="remove" style="float: right; position: absolute; margin-top: 40px; margin-left: -20px;">
                            <i class="fa fa-trash-o fa-fw"></i>
                        </a>
                        @endif
                    </div>
                    @endforeach
                    @else
                    <div id="program_name_div" style="width: 100%">
                        <div class="pure-u-1-2">
                            <div class="padding-right-10">
                                {{ Form::label('program_name', 'Program Name') }}
                                {{ Form::text('program_name[]', '', array('class' => 'pure-input-1')) }}
                            </div>
                        </div>

                        <div class="pure-u-1-2">
                            {{ Form::label('lang_name', 'Language') }}
                            {{ Form::select('lang_name[]', $languages, null, array('class' => 'pure-input-1')) }}
                        </div>
                        <a href="#" class="remove-cloned" title="remove" style="float: right; position: absolute; margin-top: 40px; margin-left: -20px; display: none;">
                            <i class="fa fa-trash-o fa-fw"></i>
                        </a>
                    </div>
                    @endif
                </div>
                <a href="#dialog-new-lang_name" class="btn-new-lang-name bottom-top-space" title="Add Language" style="float: right;">
                    <i class="fa fa-plus-circle fa-fw"></i> Add Language
                </a>
                <div class="clearfix"></div>
                <div class="pure-g" id="program_subject_section">
                    @if(!empty($emailWhiteLabel['partner_name_subject']))
                    @foreach($emailWhiteLabel['partner_name_subject'] as $lang => $partnerNameSubject)
                    <div id="program_subject_div" style="width: 100%">
                        <div class="pure-u-1-2">
                            <div class="padding-right-10">
                                {{ Form::label('partner_name_subject', 'Program Name in subject') }}
                                {{ Form::text('partner_name_subject[]', $partnerNameSubject, array('class' => 'pure-input-1')) }}
                            </div>
                        </div>

                        <div class="pure-u-1-2">
                            {{ Form::label('lang_name_in_subject', 'Language') }}
                            {{ Form::select('lang_name_in_subject[]', $languages, $lang, array('class' => 'form_z pure-form pure-form-stacked pure-input-1')) }}
                        </div>

                        @if($partnerNameSubject === reset($emailWhiteLabel['partner_name_subject']))
                        <a href="#" class="remove-cloned" title="remove" style="float: right; position: absolute; margin-top: 40px; margin-left: -20px; display: none;">
                            <i class="fa fa-trash-o fa-fw"></i>
                        </a>
                        @else
                        <a href="#" class="remove-cloned" title="remove" style="float: right; position: absolute; margin-top: 40px; margin-left: -20px;">
                            <i class="fa fa-trash-o fa-fw"></i>
                        </a>
                        @endif
                    </div>
                    @endforeach
                    @else
                    <div id="program_subject_div" style="width: 100%">
                        <div class="pure-u-1-2">
                            <div class="padding-right-10">
                                {{ Form::label('partner_name_subject', 'Program Name in subject') }}
                                {{ Form::text('partner_name_subject[]', '', array('class' => 'pure-input-1')) }}
                            </div>
                        </div>

                        <div class="pure-u-1-2">
                            {{ Form::label('lang_name_in_subject', 'Language') }}
                            {{ Form::select('lang_name_in_subject[]', $languages, null, array('class' => 'form_z pure-form pure-form-stacked pure-input-1')) }}
                        </div>
                        <a href="#" class="remove-cloned" title="remove" style="float: right; position: absolute; margin-top: 40px; margin-left: -20px; display: none;">
                            <i class="fa fa-trash-o fa-fw"></i>
                        </a>
                    </div>
                    @endif
                </div>
                <a href="#dialog-new-lang-name-in-subject" class="btn-new-lang-name-in-subject bottom-top-space" title="Add Language" style="float: right;">
                    <i class="fa fa-plus-circle fa-fw"></i> Add Language
                </a>
                <div class="clearfix"></div>
                <div class="pure-g" id="program_content_section">
                    @if(!empty($emailWhiteLabel['partner_name_content']))
                    @foreach($emailWhiteLabel['partner_name_content'] as $lang => $partnerNameContent)
                    <div id="program_content_div" style="width: 100%">
                        <div class="pure-u-1-2">
                            <div class="padding-right-10">
                                {{ Form::label('partner_name_content', 'Program Name in Content') }}
                                {{ Form::text('partner_name_content[]', $partnerNameContent, array('class' => 'pure-input-1')) }}
                            </div>
                        </div>

                        <div class="pure-u-1-2">
                            {{ Form::label('lang_name_in_content', 'Language') }}
                            {{ Form::select('lang_name_in_content[]', $languages, $lang, array('class' => 'pure-input-1')) }}
                        </div>
                        @if($partnerNameContent === reset($emailWhiteLabel['partner_name_content']))
                        <a href="#" class="remove-cloned" title="remove" style="float: right; position: absolute; margin-top: 40px; margin-left: -20px; display: none;">
                            <i class="fa fa-trash-o fa-fw"></i>
                        </a>
                        @else
                        <a href="#" class="remove-cloned" title="remove" style="float: right; position: absolute; margin-top: 40px; margin-left: -20px;">
                            <i class="fa fa-trash-o fa-fw"></i>
                        </a>
                        @endif
                    </div>
                    @endforeach
                    @else
                    <div id="program_content_div" style="width: 100%">
                        <div class="pure-u-1-2">
                            <div class="padding-right-10">
                                {{ Form::label('partner_name_content', 'Program Name in Content') }}
                                {{ Form::text('partner_name_content[]', '', array('class' => 'pure-input-1')) }}
                            </div>
                        </div>

                        <div class="pure-u-1-2">
                            {{ Form::label('lang_name_in_content', 'Language') }}
                            {{ Form::select('lang_name_in_content[]', $languages, null, array('class' => 'pure-input-1')) }}
                        </div>
                        <a href="#" class="remove-cloned" title="remove" style="float: right; position: absolute; margin-top: 40px; margin-left: -20px; display: none;">
                            <i class="fa fa-trash-o fa-fw"></i>
                        </a>
                    </div>
                    @endif
                </div>
                <a href="#dialog-new-lang-name-in-content" class="btn-new-lang-name-in-content bottom-top-space" title="Add Language" style="float: right;">
                    <i class="fa fa-plus-circle fa-fw"></i> Add Language
                </a>
                <div class="clearfix"></div>
                {{ Form::label('copyright', 'Copyright') }}
                @if(!empty($emailWhiteLabel["copyright"]["en"]))
                {{ Form::text('copyright', $emailWhiteLabel["copyright"]["en"], array('class' => 'pure-input-1')) }}
                @else
                {{ Form::text('copyright', '', array('class' => 'pure-input-1')) }}
                @endif
                <div class="clearfix"></div>
                {{ Form::label('support_email', 'Support Email') }}
                @if(!empty($emailWhiteLabel["support_email"]["en"]))
                {{ Form::text('support_email', $emailWhiteLabel["support_email"]["en"], array('class' => 'pure-input-1')) }}
                @else
                {{ Form::text('support_email', '', array('class' => 'pure-input-1')) }}
                @endif
                <div class="clearfix"></div>
                {{ Form::label('website_url', 'Website URL') }}
                @if(!empty($emailWhiteLabel["website_url"]["en"]))
                {{ Form::text('website_url', $emailWhiteLabel["website_url"]["en"], array('class' => 'pure-input-1')) }}
                @else
                {{ Form::text('website_url', '', array('class' => 'pure-input-1')) }}
                @endif
                <div class="clearfix"></div>
                <div class="image-rail" style="width:340px;">
                    <table style="margin-top:20px">
                        <tr>
                            <td width="80">
                                @if(empty($emailWhiteLabel['image']['en']))
                                <img src="https://placehold.it/74x74" alt="" />
                                @else
                                <img src="<?php echo App\Http\Controllers\MediaController::getImageFromId($emailWhiteLabel['image']['en']) ?>?s=74x74" alt="" />
                                @endif
                            </td>
                            <td>
                                {{ Form::label('email_image', 'Image') }}
                                {{ Form::file('email_image') }}
                                @if(!empty($emailWhiteLabel["image"]["en"]))
                                {{ Form::hidden('email_image_url', $emailWhiteLabel["image"]["en"]) }}
                                @endif
                            </td>
                        </tr>
                    </table>
                </div>
            </fieldset>
            @endif

            @if(I::am('BLU Admin') || I::am('BLU Admin Dev'))
            <fieldset>
                <legend>Merchant App Setup</legend>

                <div class="pure-u-1-2">
                    {{ Form::label('merchant_app', 'Merchant App') }}
                    {{ Form::select('merchant_app', Meta::merchantApps(), $partner->merchant_app, array('class' => 'pure-input-1')) }}
                </div>

                <div class="pure-u-1-2">
                    {{ Markup::checkbox('can_reward_inactive', 'Can Reward Inactive', $partner->can_reward_inactive) }}
                </div>
            </fieldset>
            @endif

            @if(I::am('BLU Admin'))
            <fieldset>
                <legend>Middle-ware</legend>

                <div class="pure-u-1-2">
                    {{ Markup::checkbox('has_middleware', 'Has Middleware', $partner->has_middleware) }}
                </div>

                {{ Form::label('link_middleware_api', 'Link To Middle-ware API') }}
                {{ Form::text('link_middleware_api', $partner->link_middleware_api, array('class' => 'pure-input-1')) }}


            </fieldset>
            <fieldset>
                <legend>Entity Configuration</legend>

                <div class="pure-g">
                    <div class="pure-u-1-2">
                        <div class="padding-right-10">
                            {{ Form::label('device_settings_id', 'Device Settings ID') }}
                            {{ Form::text('device_settings_id', $partner->device_settings_id, array('class' => 'pure-input-1-5')) }}
                        </div>
                    </div>

                    <div class="pure-u-1-2">
                        {{ Form::label('reward_max', 'Reward Limit') }}
                        {{ Form::text('reward_max', $partner->reward_max, array('class' => 'pure-input-1-5')) }}
                    </div>

                    <div class="pure-u-1-2">
                        <div class="padding-right-10">
                            {{ Form::label('redeem_unit', 'Redemption Units') }}
                            {{ Form::text('redeem_unit', $partner->redeem_unit, array('class' => 'pure-input-1-5')) }}
                        </div>
                    </div>

                    <div class="pure-u-1-2">
                        {{ Form::label('redeem_max', 'Redemption Limit') }}
                        {{ Form::text('redeem_max', $partner->redeem_max, array('class' => 'pure-input-1-5')) }}
                    </div>

                    <div class="pure-u-1-2">
                        {{ Form::label('threshold_12_months', '12 Months Threshold') }}
                        {{ Form::text('threshold_12_months', $partner->threshold_12_months, array('class' => 'pure-input-1-5')) }}
                    </div>

                    <div class="pure-u-1-2">
                        {{ Form::label('threshold_24_months', '24 Months Threshold') }}
                        {{ Form::text('threshold_24_months', $partner->threshold_24_months, array('class' => 'pure-input-1-5')) }}
                    </div>

                    <div class="spacer"></div>

                    <div class="pure-u-1-2">
                        {{ Markup::checkbox('reward', 'Reward', $partner->reward) }}
                    </div>

                    <div class="pure-u-1-2">
                        {{ Markup::checkbox('mobile', 'Mobile', $partner->mobile) }}
                    </div>

                    <div class="pure-u-1-2">
                        {{ Markup::checkbox('card', 'Card', $partner->card) }}
                    </div>

                    <div class="pure-u-1-2">
                        {{ Markup::checkbox('can_create_users', 'Can Create Users', $partner->can_create_users) }}
                    </div>

                    <div class="pure-u-1-2">
                        {{ Markup::checkbox('request_pin', 'Request Pin', $partner->request_pin) }}
                    </div>

                    <div class="pure-u-1-2">
                        {{ Markup::checkbox('redeem', 'Redeem', $partner->redeem) }}
                    </div>

                    <div class="pure-u-1-2">
                        {{ Markup::checkbox('void_operations', 'Void Operations', $partner->void_operations) }}
                    </div>

                    <div class="pure-u-1-2">
                        {{ Markup::checkbox('barcode', 'Barcode', $partner->barcode) }}
                    </div>

                    <div class="pure-u-1-2">
                        {{ Markup::checkbox('coupon', 'Coupon', $partner->coupon) }}
                    </div>

                    <div class="pure-u-1-2">
                        {{ Markup::checkbox('display_online', 'Display Online', $partner->display_online) }}
                    </div>
                    <div class="pure-u-1-2">
                        {{ Markup::checkbox('enforce_static_ip', 'Enforce Static IP', $partner->enforce_static_ip) }}
                    </div>
                </div>
            </fieldset>
            @endif

            @if(I::am('BLU Admin') || I::am('BLU Admin Dev'))
            <fieldset>
                <legend>Points Setup</legend>
                <div class="pure-g">
                    <div class="pure-u-1-2">
                        <div class="padding-right-10">
                            {{ Form::label('cashback_value_in_currency', 'Cashback Value in Currency') }}
                            {{ Form::text('cashback_value_in_currency',  $partner->cashback_value_in_currency, array('class'=>'pure-input-1', 'placeholder' => '0.01')) }}
                        </div>
                    </div>

                    <div class="pure-u-1-2">
                        <div class="padding-right-10">
                            {{ Form::label('miles_value_in_currency', 'Miles Value in Currency') }}
                            {{ Form::text('miles_value_in_currency',  $partner->miles_value_in_currency, array('class'=>'pure-input-1', 'placeholder' => '0.01')) }}
                        </div>
                    </div>
                </div>
            </fieldset>
            @endif

            @if(I::am('BLU Admin') || I::can('view_bonus_on_registration_settings'))
            <?php
            $bonusPoints = 0;
            $bonusValidFrom = '';
            $bonusValidTo = '';
            $bonusIssuedBy = null;
            $bonusSegment = null;
            $bonusActive = 0;

            if (!empty($bonus)) {
                $bonusPoints = $bonus->points;
                $bonusValidFrom = $bonus->valid_from;
                $bonusValidTo = $bonus->valid_to;
                $bonusIssuedBy = $bonus->issued_by;
                $bonusSegment = $bonus->segment;
                $bonusActive = $bonus->active;

                $bonusProduct = App\Product::find($bonus->product_id);
                if (!empty($bonusProduct)) {
                    $productId = $bonusProduct->id;
                    $productName = $bonusProduct->name;
                }
            }
            if ($partner->draft == true) {
                $myPartners[$partner->id] = $partner->name;
            }
            ?>
            <fieldset>
                <legend>Bonus on Registration Setup</legend>

                <div class="pure-g">
                    <div class="pure-u-1-2">
                        {{ Markup::checkbox('bonus_on_registration', 'Allow Bonus on Reward', $partner->bonus_on_registration) }}
                    </div>
                    <div class="pure-u-1-2">
                        {{ Markup::checkbox('active', 'Active', $bonusActive) }}
                    </div>
                </div>
                <div class="pure-g">
                    <div class="pure-u-1-2">
                        {{ Form::label('reward_points', 'Points to be Rewarded') }}
                        {{ Form::text('reward_points', $bonusPoints, array('class' => 'pure-input-1-5')) }}
                    </div> 
                    <div class="pure-u-1-2">
                        {{ Form::label('partner', 'Issued by') }}
                        {{ Form::select('issued_by', $myPartners, $bonusIssuedBy, array('class' => 'pure-input-1')) }}
                    </div>
                </div>		
                <div class="pure-g">		
                    <div class="pure-u-1-2">		
                        {{ Form::label('product', 'Product Search') }}		
                        <input type="text" id="product-autocomplete" class="product-autocomplete  pure-input-1-2 inline-block-display" placeholder="Search" />		
                        <button id="product-search"class="pure-input-1-1 rounded-search">search</button>		
                        <select name="product" id="product" class="pure-input-1">		
                            <option value="0">Select Product</option>		
                            <?php if (!empty($bonusProduct)) { ?>		
                                <option value="<?php echo $productId; ?>" selected="selected"><?php echo $productName; ?></option>		
                            <?php } ?>		
                        </select>		
                    </div> 
                    </div> 
                    <!--                            <div class="pure-g">
                                                    <div class="pure-u-1-2">
                                                        {{ Form::label('valid_from', 'Valid From') }}
                                                        {{ Form::text('valid_from', $bonusValidFrom, array('class' => 'pure-input-1-5')) }}
                                                    </div>
                                                    <div class="pure-u-1-2">
                                                        {{ Form::label('valid_to', 'Valid To') }}
                                                        {{ Form::text('valid_to', $bonusValidTo, array('class' => 'pure-input-1-5')) }}
                                                    </div>
                                                </div>
                                                <div class="pure-g">
                                                    <div class="pure-u-1-2">
                                                        {{ Form::label('segment', 'Segment') }}
                                                        {{ Form::select('segment', $segments, $bonusSegment, array('class' => 'pure-input-1')) }}
                                                    </div>
                                                </div>-->
            </fieldset>
            @endif
        </div>
        {{--- /Left Section Content ---}}

        {{--- Right Section Content ---}}
        <div class="pure-u-2-5 attributes">

            <div class="padding-left-30">
                {{-- Channel Listing --}}
                <h3>
                    @if(I::can_edit('partners', $partner))
                    <a href="#dialog-add-channel" title="New Display Channel" class="dialog">
                        White label channel partners display <i class="fa fa-plus-circle fa-fw"></i>
                    </a>
                    @else
                    White label channel partners display
                    @endif
                </h3>
                <ul id="channel-listing">
                    @include('partner.channellist')
                </ul>
                {{-- /Channel Listing --}}
            </div>

            <div class="padding-left-30">
                {{-- Linked Partners Listing --}}
                <h3>
                    @if(I::can_edit('partners', $partner))
                    <a href="#dialog-add-partner-linked" title="New Linked Parnter" class="dialog">
                        Linked Partners <i class="fa fa-plus-circle fa-fw"></i>
                    </a>
                    @else
                    Linked Parnters
                    @endif
                </h3>
                <ul id="partner-link-listing">
                    @include('partner.partnerlinklist')
                </ul>
                {{-- /Partner Link Listing --}}
            </div>

            <div class="padding-left-30">
                {{-- Country Listing --}}
                <h3>
                    @if(I::can_edit('partners', $partner))
                    <a href="#dialog-add-country" title="New Display Country" class="dialog">
                        Countries <i class="fa fa-plus-circle fa-fw"></i>
                    </a>
                    @else
                    Display Countries
                    @endif
                </h3>
                <ul id="country-listing">

                    @include('partner.countrylist', array('countries', $countries))
                </ul>
                {{-- /Channel Listing --}}
            </div>
            @if(I::am('BLU Admin'))
            <div class="padding-left-30">
                {{-- auto generated reports Listing --}}
                <h3>
                    @if(I::can_edit('partners', $partner))
                        <a href="#dialog-add-agr" title="New Display Auto Generated Reports" class="dialog">
                            Auto-Generated Reports <i class="fa fa-plus-circle fa-fw"></i>
                        </a>
                    @else
                        Display Auto-Generated Reports
                    @endif
                </h3>
                <ul id="agr-listing">
                    @include('partner.agrlist')
                </ul>
                {{-- /auto generated reports  Listing --}}
            </div>
            @endif
        </div>
        {{--- /Right Section Content ---}}
    </div>

    {{--- Stores ---}}
    <fieldset>
        <legend>Store Locations</legend>

        <table id="store-list" class="pure-table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Store Name</th>
                    <th>Coordinates</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @if(count($partner->stores) > 0)
                @foreach($partner->stores as $store)
                <tr>
                    <td>{{ $store->id }}</td>
                    @if($store->draft)
                    <td>{{{ $store->name }}} (Draft)</td>
                    @else
                    <td>{{{ $store->name }}}</td>
                    @endif
                    <td>
                        <a href="http://maps.google.com/maps?q={{ round($store->lat,6) }},{{ round($store->lng,6) }}&zoom=14" target="_blank">View</a>
                    </td>
                    <td>
                        @if(I::can_edit('partners', $partner))
                        <a href="{{ url('dashboard/store/edit/'.base64_encode($store->id)) }}" class="edit-store" title="Edit">Edit</a>
                        <a href="{{ url('dashboard/store/delete/'.base64_encode($store->id)) }}" class="delete-store" title="Delete">Delete</a>
                        @else
                        N/A
                        @endif
                    </td>
                </tr>
                @endforeach
                @else
                <tr class="empty">
                    <td colspan="4">No store locations defined</td>
                </tr>
                @endif
            </tbody>
        </table>

        @if(I::can_edit('partners', $partner))
        <a href="#dialog-new-store" data-partner_id="{{ $partner->id }}" class="btn-new-store top-space" title="Add new store location">
            <i class="fa fa-plus-circle fa-fw"></i>Add new store location
        </a>
        @endif
    </fieldset>
    {{--- /Stores ---}}

    {{--- Form Buttons ---}}
    <div class="form-buttons">
        <div class="left">
            <a href="{{ url('dashboard/partners') }}" class="pure-button">Cancel</a>
        </div>

        @if($partner->draft == true)
        @if(I::can('create_partners'))
        <div class="right">
            <button type="submit" class="pure-button pure-button-primary">Create Partner</button>
        </div>
        @endif
        @else
        @if(I::can('edit_partners'))
        <div class="right">
            <button type="submit" class="pure-button pure-button-primary">Save Partner</button>
        </div>
        @endif
        @endif

        <div class="clearfix"></div>
    </div>
    {{--- /Form Buttons ---}}

    {{ Form::close() }}
</div>
@stop

@section('page_script')
@if(!I::can_edit('partners', $partner))
<script>
    //enqueue_script('disable-form');
</script>
@endif

<script src="https://maps.googleapis.com/maps/api/js?key={{ Config::get('google.maps_api_key') }}&sensor=true"></script>

<script>
    set_menu('mnu-partners');

    enqueue_script('new-partner');

    $(document).on('click', '#product-search', function (event) {
        if ($("#product-autocomplete").val().length >= 3) {
            event.preventDefault();
            $('select#product').empty();
            $.get('/dashboard/loyalty/products?term=' + $('#product-autocomplete').val(), function (resp) {
                $.each(resp, function (index, value) {
                    jQuery('<option/>', {
                        value: value.id,
                        html: value.value
                    }).appendTo('select#product');//appends to select if parent div has id dropdown		
                });
            });
            return false;
        } else {
            event.preventDefault();
            return false;
        }
    });
    $(document).bind('keypress', '#product-autocomplete', function (event) {
        if (event.keyCode == 13) {
            if ($("#product-autocomplete").val().length >= 3) {
                event.preventDefault();
                $('select#product').empty();
                $.get('/dashboard/loyalty/products?term=' + $('#items-autocomplete').val(), function (resp) {
                    $.each(resp, function (index, value) {
                        jQuery('<option/>', {
                            value: value.id,
                            html: value.value
                        }).appendTo('select#product');//appends to select if parent div has id dropdown		
                    });
                });
                return false;
            } else {
                event.preventDefault();
                return false;
            }
        }
// Turn on the global items search autocomplete function		
        $("#product-autocomplete").autocomplete({
            source: "/dashboard/loyalty/products",
            minLength: 3,
            select: function (event, ui) {
                console.log(ui);
                jQuery('<option/>', {
                    value: ui.item.id,
                    html: ui.item.value
                }).appendTo('select#product').attr('selected', true); //appends to select if parent div has id dropdown		
            }
        });
    });
</script>
@stop