@if($partner->countries->count() == 0)
    <li class="empty">No Countries Associated</li>
@else
    @foreach($partner->countries as $c)
        <li>
            {{{ $countries[$c->country_id] }}}

            @if(I::can('delete_partners'))
                <a href="/dashboard/partners/unlink_country/{{ base64_encode($partner->id) }}/{{ base64_encode($c->country_id) }}" class="delete-ajax" data-target="#country-listing">
                    [Delete]
                </a>
            @endif
        </li>
    @endforeach
@endif
