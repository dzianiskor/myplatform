@if(count($partner->networks()) == 0)
    <li class="empty">No Network Associated</li>
@else
    @foreach($partner->networks as $n)
        <li>
            {{{ $n->name }}}
            
            @if(I::can_edit('partners', $partner))
                <a href="/dashboard/partners/detach/network/{{ base64_encode($partner->id) }}/{{ base64_encode($n->id) }}" class="delete-ajax" data-target="#network-listing">
                    [Delete]
                </a>
            @endif
        </li>
    @endforeach
@endif
