@if($partner->channels->count() == 0)
    <li class="empty">No Channels Associated</li>
@else
    @foreach($partner->channels as $c)
        @if($c->channel)
            <li>
                <div style="width: 80%;">{{{ $c->channel->name }}}</div>
                @if(I::can('delete_partners'))
				    <div>
                        <a href="/dashboard/partners/detach/channel/{{ base64_encode($partner->id) }}/{{ base64_encode($c->channel_id) }}" class="delete-ajax" data-target="#channel-listing">
                            [Delete]
                        </a>
                    </div>
                @endif
            </li>
        @else
            <li>
                <span style="color:#a95656;font-style:italic;">Channel no longer exists</span>
                @if(I::can('delete_partners'))
                    <a href="/dashboard/partners/detach/channel/{{ base64_encode($partner->id) }}/{{ base64_encode($c->channel_id) }}" class="delete-ajax" data-target="#channel-listing">
                        [Delete]
                    </a>
                @endif
            </li>
        @endif
    @endforeach
@endif
