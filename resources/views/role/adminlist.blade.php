@if(count($user->roles) > 0)
    @foreach($user->roles as $r)
        <li>
            {{ $r->name }} <a href="/dashboard/roles/unlinkadmin/{{ base64_encode($user->id) }}/{{ base64_encode($r->id)}}" class="delete-ajax" data-id="{{ $r->id }}" data-target="#role-listing">[Delete]</a>
        </li>
    @endforeach
@else
    <li class="empty">No Admin Types Associated</li>
@endif