@extends('layouts.dashboard')

@section('title')
    Member Types | BLU
@stop

@section('body')
    <div class="padding">

        <div class="content-head pure-g">
            <div class="title pure-u-1-2">
                <h1><i class="fa fa-unlock-alt fa-fw"></i>Member Types ({{ $roles->total() }})</h1>
            </div>

            <div class="controls pure-u-1-2">
                <form action="{{ Filter::searchUrl() }}" method="get">
                    <?php
                    $querystring = $_SERVER['QUERY_STRING'];
                    $queryAll = array();
                    if ($querystring != '') {
                        $queryArr = explode('&', $querystring);
                        foreach ($queryArr as $val) {
                            $valArr = explode('=', $val);
                            if (!empty($queryAll[urldecode($valArr[0])])) {
                                $queryAll[urldecode($valArr[0])] = $queryAll[urldecode($valArr[0])] . ',' . urldecode($valArr[1]);
                            } else {
                                $queryAll[urldecode($valArr[0])] = urldecode($valArr[1]);
                            }
                        }
                    }?>
                    <input type="text" placeholder="Search" name="q" value="{{ Input::get('q') }}"/>
                    <button type="submit" title="" class="list-search"></button>
                </form>

                @if(I::can('create_roles'))
                    <a href="{{ url('dashboard/roles/new') }}" class="pure-button pure-button-primary">Add Type</a>
                @endif
            </div>
        </div>

        <table class="pure-table">
            <thead>
                <tr>
                    <th width="50">ID</th>
                    <th style="text-align:left;padding-left:20px"><a href="{{ Filter::baseUrl('sort=name') }}">Name</a></th>
                    <th width="150">Actions</th>
                </tr>
            </thead>

            <tbody>
                @if(count($roles) > 0)
                    @foreach($roles as $r)
                        <tr>
                            <td>{{ $r->id }}</td>
                            @if($r->name == 'New Role')
                                <td style="text-align:left;padding-left:20px"><i>{{{ $r->name }}}</i></td>
                            @else
                                <td style="text-align:left;padding-left:20px">{{{ $r->name }}}</td>
                            @endif
                            <td>
                                @if($r->id != 1)
                                    <a href="/dashboard/roles/view/{{ base64_encode($r->id) }}" class="edit" title="View">View</a>

                                    @if(!I::am('BLU Admin') && !I::am('Partner Admin'))
                                        @if($r->id > 2 && I::can('delete_roles') && $r->id > Auth::User()->highestRoleID())
                                            <a href="/dashboard/roles/delete/{{ base64_encode($r->id) }}" class="delete" title="Delete">Delete</a>
                                        @endif
                                    @else
                                        @if($r->id > 2 && I::can('delete_roles'))
                                            <a href="/dashboard/roles/delete/{{ base64_encode($r->id)}}" class="delete" title="Delete">Delete</a>
                                        @endif
                                    @endif
                                @else
                                    <i>N/A</i>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="3">No Roles Found</td>
                    </tr>
                @endif
            </tbody>
        </table>

        {{ $roles->setPath('roles')->appends(Input::except('page'))->links() }}

    </div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-access_rights');
    </script>
@stop
