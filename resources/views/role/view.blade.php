@extends('layouts.dashboard')

@section('title')
    Member Type &raquo; {{{ $role->name }}}  | BLU
@stop

@section('body')
    <div class="padding">

        {{-- Page Header --}}
        <div class="content-head pure-g">
            <div class="title pure-u-1-2">
                <h1><i class="fa fa-unlock-alt fa-fw"></i>Member Type &raquo; {{{ $role->name }}}</h1>
            </div>

            <div class="controls pure-u-1-2">
                <a href="{{ url('dashboard/roles') }}" class="pure-button">All Member Types</a>
            </div>
        </div>
        {{-- /Page Header --}}

        {{ Form::open(array('url' => url('dashboard/roles/update/'.base64_encode($role->id)),  'method' => 'post', 'class' => "pure-form pure-form-stacked form-width", "id" => "roleForm")) }}

            @include('partials.errors')

            {!! Form::hidden('redirect_to', URL::previous()) !!}

            <fieldset>
                <legend>Basic Details</legend>
                {{ Form::label('name', 'New Member Type') }}
                {{ Form::text('name', $role->name, array('class' => 'pure-input-1 required')) }}
            </fieldset>

            <fieldset>
                <legend>
                    Permission Matrix 

                    @if(I::can('create_roles'))
                        <a href="#" id="select-all-permissions" class="pure-button-main pure-button">Select/Deselect All</a>
                    @endif                    
                </legend>
                
                <table class="pure-table condensed-rows">
                    <thead>
                        <tr>
                            <th width="200">Enabled</th>
                            <th class="align-left">Permission Description</th>
                        </tr>
                    </thead>
                    @foreach($permissions as $group => $list)
                        <tbody>
                            <tr class="group-title">
                                <td>
                                    {{ $group }}
                                </td>
                                <td style="padding-left:5px">
                                    <a href="#" class="select-group-permissions">Select / Deselect All</a>
                                </td>
                            </tr>
                            @foreach($list as $p)
                                <tr>
                                    @if($p['checked'])
                                        <td>
                                            <input id="p_{{ $p['id'] }}" type="checkbox" name="permission[]" value="{{ $p['id'] }}" checked="checked" />
                                        </td>
                                    @else
                                        <td>
                                            <input id="p_{{ $p['id'] }}" type="checkbox" name="permission[]" value="{{ $p['id'] }}" />
                                        </td>
                                    @endif
                                    <td style="padding-left:5px" class="align-left">
                                        <label for="p_{{ $p['id'] }}">{{ $p['name'] }}</label>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    @endforeach
                </table>
            </fieldset>

            <!-- Form Buttons -->
            <div class="form-buttons">
                <div class="left">
                    <a href="{{ url('dashboard/roles') }}" class="pure-button">Cancel</a>
                </div>

                @if(I::am('BLU Admin'))
                    <div class="right">
                        <button type="submit" class="pure-button pure-button-primary">Save Member Type</button>
                    </div>
                @else
                    @if(I::can('create_roles') && $role->id > Auth::User()->highestRoleID())
                        <div class="right">
                            <button type="submit" class="pure-button pure-button-primary">Save Member Type</button>
                        </div>
                    @endif
                @endif           

                <div class="clearfix"></div>
            </div>
            <!-- /Form Buttons -->

        {{ Form::close() }}
    </div>
@stop

@section('page_script')
    <script>
        enqueue_script('roles');

        $(function(){
            set_menu('mnu-access_rights');

            @if(!I::can('create_roles'))
                disableForm('#roleForm');
            @endif

            @if(!I::am('BLU Admin') && $role->id < Auth::User()->highestRoleID())
                disableForm('#roleForm');
            @endif
        });
    </script>
@stop