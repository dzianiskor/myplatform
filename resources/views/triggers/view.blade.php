@extends('layouts.dashboard')

@section('title')
Update Trigger | BLU
@stop

@section('dialogs')
@if($errors->any())
<h4>{{$errors->first()}}</h4>
@endif

@include('triggers/dialog/partner', array('partners'  => $partners))
@include('triggers/dialog/segment', array('segments'  => $segments))
@include('triggers/dialog/tier',    array('tiers'  => $tiers))

{{-- Exception Rules --}}
    <div id="dialog-add-rule" style="width:400px;">
        <h4>Add Rule</h4>
        
        <a href="#dialog-new-type" class="btn-new-type top-bottom-space" title="Add another type">
            <i class="fa fa-plus-circle fa-fw"></i> Add another type
        </a>
        <div class="exception-type-wrapper">
            <div class="type-forms">                    
                {{ Form::open(array('url' => "#", 'class' => "pure-form")) }}
                    <select name="rule-item" id="rule-item" class="pure-input-1 rule-item">
                        <option selected="selected" disabled>Select Criteria</option>
                        <option value="1">Amount</option>
                        <option value="2">Log-ins</option>
                        <option value="3">Check-ins</option>
                    </select>
                {{ Form::close() }}

                <div class="property-forms">                    
                    {{--- Amount Match ---}}
                    {{ Form::open(array('url' => "#", 'class' => "form_9 pure-form pure-form-stacked", "id" => 'form_9')) }}
                        {{ Form::label('rule_value', 'Amount Matches') }}
                        <input type="text" name="rule_value_amount" id="rule_value_amount" class=" inline-block-display" placeholder="Amount" />
                        {{ Form::select('rule_value_currency', ['AED', 'USD', 'EUR'], '', array('id' => 'rule_value_currency', 'class' => 'pure-input-2-3')) }}
                    {{ Form::close() }}
                    {{--- /Amount Type Match ---}}
        
                </div>
            </div>
        </div>

        {{ Form::open(array('url' => "/dashboard/loyalty/rule/1", 'class' => "pure-form pure-form-stacked form_hidden_attributes")) }}

            {{ Form::hidden('rule_operator', '["and"]') }}
            {{ Form::hidden('rule_type', null) }}
            {{ Form::hidden('rule_comparison', null) }}
            {{ Form::hidden('rule_value', null) }}
            {{ Form::hidden('rule_value_currency') }}

            <div class="pure-g block padding-top-10 padding-bottom-10">
                <div class="pure-u-1">
                    <?php
                            $ruleCurrencyId	= 6;
                    ?>
                    {{ Form::label('currency', 'Currency', array('for'=>'currency')) }}
                    {{ Form::select('currency_id', ['AED', 'USD', 'EUR'], $ruleCurrencyId, array('id' => 'currency_id', 'class' => 'pure-input-2-3 loayltyexrulecur')) }}

                    {{ Form::label('original_reward', 'Amount to be rewarded') }}
                    {{ Form::text('original_reward', '', array('class' => 'pure-input-1-2 required', 'placeholder' => '1.00')) }}

                    {{ Form::label('reward_usd', 'AED') }}
                    {{ Form::text('reward_usd', '', array('class' => 'pure-input-1-2', 'placeholder' => '0 AED', 'disabled' => 'disabled')) }}
                    
                    {{ Form::label('type', 'Reward Types', array('for'=>'type')) }}
                    {{ Form::select('type', ['Offer'], $ruleCurrencyId, array('id' => 'type', 'class' => 'pure-input-2-3 loayltyexrulecur')) }}
                    
                    {{ Form::label('offers', 'Offers', array('for'=>'offers')) }}
                    {{ Form::select('offers', ['Complimentary three-course dinner for two, with a bottle of house grape or two mocktails', 'Complimentary room night on your next hotel stay', 'Complimentary bottle of house grape or two complimentary desserts', 'Receive a complimentary beach pass or two complimentary Wild Wadi Waterpark™ passes'], $ruleCurrencyId, array('id' => 'offers', 'class' => 'pure-input-2-3 loayltyexrulecur')) }}
		</div>
            </div>
        {{ Form::close() }}

        <a href="#" class="pure-button pure-button-primary basic-action" data-target="#rule-table">
            Save
        </a>
    </div>
@stop

@section('body')
<div class="padding">

    <div class="content-head pure-g">
        <div class="title pure-u-1-2">
            <h1><i class="fa fa-shopping-cart fa-fw"></i> Trigger </h1>
        </div>

        <div class="controls pure-u-1-2">
            <a href="{{ url('dashboard/offer') }}" class="pure-button">Triggers</a>
        </div>
    </div>

    {{ Form::open(array('action' => array('TriggersController@saveTrigger', 1),
                                   'method' => 'post',
                                   'files'  => 'true',
                                   'class'  => 'pure-form pure-form-stacked form-width',
                                   'id'     => 'offerForm')) }}

    @include('partials.errors')         

    <div class="pure-g">
        @include('triggers/form')
        <div class="pure-u-1-2 attributes">
            <div class="padding-left-40">
                @include('triggers/list/partner')
                @include('triggers/list/segment')
                @include('triggers/list/tier')
            </div>
        </div>
        <div class="pure-u-1">
            <fieldset>
                <legend>
                    Events

                    <span class="right">
                        <a href="#dialog-add-rule" class="dialog">Add Event</a>
                    </span>
                </legend>

                <table id="rule-table" class="pure-table">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Title</th>
                            <th>Type</th>
                            <th>Reward</th>
                            <th>For</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>Complimentary Massage</td>
                            <td>Amount</td>
                            <td>Spend Reward</td>
                            <td>15,000 AED</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Beach Pass</td>
                            <td>Amount</td>
                            <td>Spend Reward</td>
                            <td>20,000 AED</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>Hotel Stay</td>
                            <td>Log-ins</td>
                            <td>On-going Offers</td>
                            <td>50</td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>Free Dessert</td>
                            <td>Check-ins</td>
                            <td>On-going Offers</td>
                            <td>10</td>
                        </tr>
                    </tbody>
                </table>

            </fieldset>
        </div>
    </div>
    <!-- Form Buttons -->
    <div class="form-buttons">
        <div class="left">
            <a href="{{ url('dashboard/triggers') }}" class="pure-button">Cancel</a>
        </div>

        <div class="right">
            <button type="submit" class="pure-button pure-button-primary">Save Trigger</button>
        </div>

        <div class="clearfix"></div>
    </div>
    <!-- /Form Buttons -->

    {{ Form::close() }}

</div>
@stop

@section('page_script')
<script>
    $(function(){
            set_menu('mnu-triggers');
    });
    enqueue_script('new-trigger');
</script>
@stop
