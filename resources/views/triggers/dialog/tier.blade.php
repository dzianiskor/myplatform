{{-- Assign new Tier --}}
<div id="dialog-add-tier" style="width:300px;">
    <h4>Associate to Tier</h4>

    {{ Form::open(array('class' => "pure-form pure-form-stacked")) }}
        {{ Form::select('tier_id', $tiers, null, array('class' => 'pure-input-1')) }}

        <a href="/dashboard/offer/link_tier/1" class="pure-button pure-button-primary attribute-action" data-target="#tier-listing">
            Save
        </a>
    {{ Form::close() }}
</div>
{{-- /Assign new Tier --}}