{{-- Assign new Partner Channel--}}
<div id="dialog-add-channel" style="width:300px;">
    <h4>Add Display Channel</h4>

    {{ Form::open(array('class' => "pure-form pure-form-stacked")) }}
        {{ Form::select('partner_id', $partners, null, array('class' => 'pure-input-1')) }}

        <a href="/dashboard/offer/link_channel/1" class="pure-button pure-button-primary attribute-action" data-target="#channel-listing">
            Save
        </a>
    {{ Form::close() }}
</div>
{{-- /Assign new Partner Channel --}}