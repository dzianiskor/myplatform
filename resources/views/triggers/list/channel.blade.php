{{-- Channel Listing --}}
<h3>
	    <a href="#dialog-add-channel" title="New Channel" class="dialog">
	        Display Channels <i class="fa fa-plus-circle fa-fw"></i>
	    </a>

</h3>
<ul id="channel-listing">
    @include('triggers/list/channelList')
</ul>
{{-- /Channel Listing --}}
