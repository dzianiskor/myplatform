{{-- Segment Listing --}}
<h3>
    <a href="#dialog-add-segment" title="New Segment" class="dialog" id="segments_dialog">
        Segments <i class="fa fa-plus-circle fa-fw"></i>
    </a>
</h3>
<ul id="segment-listing">
    @include('triggers/list/segmentList')
</ul>
{{-- /Segment Listing --}}
