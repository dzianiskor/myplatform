{{-- Partner Listing --}}
<h3>
    <a href="#dialog-add-partner" title="New Partner" class="dialog">
        Partners <i class="fa fa-plus-circle fa-fw"></i>
    </a>
</h3>
<ul id="partner-listing">
    @include('triggers/list/partnerList')
</ul>
<label class="error" style="display: none;">This field is required.</label>
{{-- /Partner Listing --}}
