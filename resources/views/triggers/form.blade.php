
<div class="pure-u-1-2">
    <fieldset>
        <legend>Basic Details</legend>
        {{ Form::label('status', 'Status') }}
        <select name="status" class="pure-input-1-3">
                            <option value="0">Inactive</option>
                            <option value="1"  selected>Active</option>
                </select>
        {{ Form::label('name', 'Name', array('for'=>'name')) }}
        {{ Form::text('name', '', array('class'=>'pure-input-1 required')) }}

        {{ Form::label('description', 'Main Description', array('for'=>'description')) }}
        {{ Form::textarea('description', '', array('class'=>'pure-input-1')) }}
        
        <fieldset>
                <legend>Validity Period</legend>

                <div class="pure-g">
                    <div class="pure-u-1-2">
                        {{ Form::label('valid_from', 'Valid From') }}
                        {{ Form::text('valid_from', '', array('class' => 'pure-input-1 required datepicker-year')) }}
                    </div>

                    <div class="pure-u-1-2">
                        {{ Form::label('valid_from_time', 'Valid From Time') }}
                        {{ Form::text('valid_from_time', '', array('class' => 'pure-input-1 required timepicker')) }}
                    </div>
                </div>

                <div class="pure-g">
                    <div class="pure-u-1-2">
                        {{ Form::label('valid_to', 'Valid To') }}
                        {{ Form::text('valid_to', '', array('class' => 'pure-input-1 required datepicker-year')) }}
                    </div>

                    <div class="pure-u-1-2">
                        {{ Form::label('valid_to_time', 'Valid To Time') }}
                        {{ Form::text('valid_to_time', '', array('class' => 'pure-input-1 required timepicker')) }}
                    </div>
                </div>
            </fieldset>
    </fieldset>
</div>