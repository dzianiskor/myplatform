@extends('layouts.dashboard')

@section('title')
Triggers | BLU
@stop

@section('dialogs')
{{-- Filter Partner --}}
<div id="dialog-add-partner" style="width:300px;">
    <h4>Filter by Partner</h4>

    <form action="{{ Filter::searchUrl() }}" method="get">
        <?php
        $querystring = $_SERVER['QUERY_STRING'];

        $queryAll = array();
        $part_list = array();
        if ($querystring != '') {
            $queryArr = explode('&', $querystring);

            foreach ($queryArr as $val) {
                $valArr = explode('=', $val);
                if (!empty($queryAll[urldecode($valArr[0])])) {
                    $queryAll[urldecode($valArr[0])] = $queryAll[urldecode($valArr[0])] . ',' . urldecode($valArr[1]);
                } else {
                    $queryAll[urldecode($valArr[0])] = urldecode($valArr[1]);
                }
                $tempField = str_replace('[]', '', urldecode($valArr[0]));

                if ($tempField != "partner_list" && $tempField != 'page') {
                    echo '<input type="hidden" name="' . urldecode($valArr[0]) . '" value="' . $valArr[1] . '"> ';
                } else {
                    $part_list[] = $valArr[1];
                }
            }
        }
//				echo "<pre>";
//				print_r($queryAll);
//				exit;
        foreach ($lists['partners'] as $key => $value) {
            if (in_array($key, $part_list)) {
                echo '<input type="checkbox" name="partner_list[]" value="' . $key . '" checked="checked"> ' . $value . "<br>";
            } else {
                echo '<input type="checkbox" name="partner_list[]" value="' . $key . '"> ' . $value . "<br>";
            }
        }
        ?>

        <button class="pure-button pure-button-primary" type="submit" title="" >Filter</button>
    </form>
</div>
{{-- End Filter Partner --}}

{{-- Filter Status --}}
<div id="dialog-add-status" style="width:300px;">
    <h4>Filter by Status</h4>

    <form action="{{ Filter::searchUrl() }}" method="get">
        <?php
        $querystring = $_SERVER['QUERY_STRING'];
        $status_list = array();
        if ($querystring != '') {
            $queryArr = explode('&', $querystring);
            foreach ($queryArr as $val) {
                $valArr = explode('=', $val);

                $tempField = str_replace('[]', '', urldecode($valArr[0]));

                if ($tempField != "offer_status" && $tempField != 'page') {
                    echo '<input type="hidden" name="' . urldecode($valArr[0]) . '" value="' . $valArr[1] . '"> ';
                } else {
                    $status_list[] = $valArr[1];
                }
            }
        }
        foreach ($lists['statuses'] as $key => $value) {
            if (in_array($key, $status_list)) {
                echo '<input type="checkbox" name="offer_status[]" value="' . $key . '" checked="checked"> ' . $value . "<br>";
            } else {
                echo '<input type="checkbox" name="offer_status[]" value="' . $key . '"> ' . $value . "<br>";
            }
        }
        ?>

        <button class="pure-button pure-button-primary" type="submit" title="" >Filter</button>
    </form>
</div>
{{-- End Filter Status --}}

@stop

@section('body')
<div class="padding">

    <div class="content-head pure-g">
        <div class="title pure-u-1-2">
            <h1><i class="fa fa-eye fa-fw"></i> Triggers</h1>
        </div>

        <div class="controls pure-u-1-2">
            <form action="{{ Filter::searchUrl() }}" method="get">
                <?php
                $querystring = $_SERVER['QUERY_STRING'];
                if ($querystring != '') {
                    $queryArr = explode('&', $querystring);
                    foreach ($queryArr as $val) {
                        $valArr = explode('=', $val);
                        if ($valArr[0] != "q") {
                            echo '<input type="hidden" name="' . urldecode($valArr[0]) . '" value="' . $valArr[1] . '"> ';
                        }
                    }
                }
                ?>
                <input type="text" placeholder="Search" name="q" value="{{ Input::get('q') }}" />
                <button type="submit" title="" class="list-search"></button>
            </form>
            @if(I::can('create_offer'))
            <a href="{{ url('dashboard/triggers/new') }}" class="pure-button pure-button-primary">Add Trigger</a>
            @endif
        </div>
        <div class="pure-u-1-2" style="width:100%">



            <table style="margin-top: 20px; float: right; width:100%; margin-right:-5px;">
                <tr>
                    <td style="text-align:right;padding-left:20px;">
                        <a href="{{ url('dashboard/triggers') }}" title="Clear Filters" class="pure-button-primary1 pure-button">
                            Clear Filters <i class="fa fa-ban fa-fw"></i>
                        </a>
                        <a href="#dialog-add-partner" title="Partner" class="dialog pure-button-primary1 pure-button">
                            Partner <i class="fa fa-filter fa-fw"></i>
                        </a>
                        
                        <a href="#dialog-add-status" title="Status" class="dialog pure-button-primary1 pure-button">
                            Status <i class="fa fa-filter fa-fw"></i>
                        </a>

                    </td>
                </tr>

            </table>


        </div>
    </div>

    <table class="pure-table">
        <thead>
            <tr>
                <th><a href="{{ Filter::baseUrl('sort=id') }}">ID</a></th>
                <th><a href="{{ Filter::baseUrl('sort=name') }}">Name</a></th>
                <th>Status</th>
                <th>Actions</th>
            </tr>
        </thead>

        <tbody>
            <tr>
                <td>
                    1
                </td>
                <td style="text-align:left;padding-left:20px;">
                    <a href="{{ url('dashboard/triggers/view/1')}}" class="edit" title="Edit">Trigger 1</a>
                </td>
                <td>
                    <span class="ico-yes"></span>
                </td>
                <td>
                    <a href="{{ url('dashboard/triggers/view/1')}}" class="edit" title="View">View</a>
                    <a href="{{ url('dashboard/triggers/delete/1')}}" class="delete" title="Delete">Delete</a>
                </td>
            </tr>
            
            <tr>
                <td>
                    2
                </td>
                <td style="text-align:left;padding-left:20px;">
                    <a href="{{ url('dashboard/triggers/view/2')}}" class="edit" title="Edit">Trigger 2</a>
                </td>
                <td>
                    <span class="ico-yes"></span>
                </td>
                <td>
                    <a href="{{ url('dashboard/triggers/view/2')}}" class="edit" title="View">View</a>
                    <a href="{{ url('dashboard/triggers/delete/2')}}" class="delete" title="Delete">Delete</a>
                </td>
            </tr>
            
            <tr>
                <td>
                    3
                </td>
                <td style="text-align:left;padding-left:20px;">
                    <a href="{{ url('dashboard/triggers/view/3')}}" class="edit" title="Edit">Trigger 3</a>
                </td>
                <td>
                    <span class="ico-yes"></span>
                </td>
                <td>
                    <a href="{{ url('dashboard/triggers/view/3')}}" class="edit" title="View">View</a>
                    <a href="{{ url('dashboard/triggers/delete/3')}}" class="delete" title="Delete">Delete</a>
                </td>
            </tr>
        </tbody>
    </table>
    <?php unset($queryAll['page']); ?>

</div>
@stop

@section('page_script')
<script>
    set_menu('mnu-triggers');
</script>
@stop