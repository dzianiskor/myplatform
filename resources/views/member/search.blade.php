@extends('layouts.dashboard')

@section('title')
Members | BLU
@stop
@section('dialogs')

{{-- Filter Partner --}}
<div id="dialog-add-partner" style="width:300px;">
    <h4>Filter by Partner</h4>

    <form action="{{ Filter::searchUrl() }}" method="get">
        <?php
        $querystring = $_SERVER['QUERY_STRING'];
        $queryAll = array();
        $part_list = array();
        if ($querystring != '') {
            $queryArr = explode('&', $querystring);
            foreach ($queryArr as $val) {
                $valArr = explode('=', $val);
                if (!empty($queryAll[urldecode($valArr[0])])) {
                    $queryAll[urldecode($valArr[0])] = $queryAll[urldecode($valArr[0])] . ',' . urldecode($valArr[1]);
                } else {
                    $queryAll[urldecode($valArr[0])] = urldecode($valArr[1]);
                }
                $tempField = str_replace('[]', '', urldecode($valArr[0]));

                if ($tempField != "partner_member_list" && $tempField != 'page') {
                    echo '<input type="hidden" name="' . urldecode($valArr[0]) . '" value="' . $valArr[1] . '"> ';
                } else {
                    $part_list[] = $valArr[1];
                }
            }
        }
        foreach ($lists['partners'] as $key => $value) {
            if (in_array($key, $part_list)) {
                echo '<input type="checkbox" name="partner_member_list[]" value="' . $key . '" checked="checked"> ' . $value . "<br>";
            } else {
                echo '<input type="checkbox" name="partner_member_list[]" value="' . $key . '"> ' . $value . "<br>";
            }
        }
        ?>

        <button class="pure-button pure-button-primary" type="submit" title="" >Filter</button>
    </form>
</div>
{{-- End Filter Partner --}}

{{-- Filter Country --}}
<div id="dialog-add-country" style="width:300px;">
    <h4>Filter by Country</h4>

    <form action="{{ Filter::searchUrl() }}" method="get">
            <?php
            $querystring = $_SERVER['QUERY_STRING'];
            $count_list = array();
            if ($querystring != '') {
                $queryArr = explode('&', $querystring);
                foreach ($queryArr as $val) {
                    $valArr = explode('=', $val);
                    $tempField = str_replace('[]', '', urldecode($valArr[0]));

                    if ($tempField != "country_member_list" && $tempField != 'page') {
                        echo '<input type="hidden" name="' . urldecode($valArr[0]) . '" value="' . $valArr[1] . '"> ';
                    } else {
                        $count_list[] = $valArr[1];
                    }
                }
            }
            foreach ($lists['countries'] as $key => $value) {
                if (in_array($key, $count_list)) {
                    echo '<input type="checkbox" name="country_member_list[]" value="' . $key . '" checked="checked"> ' . $value . "<br>";
                } else {
                    echo '<input type="checkbox" name="country_member_list[]" value="' . $key . '"> ' . $value . "<br>";
                }
            }
            ?>

        <button class="pure-button pure-button-primary" type="submit" title="" >Filter</button>
    </form>
</div>
{{-- End Filter Country --}}

{{-- Filter Segment --}}
<div id="dialog-add-segment" style="width:300px;">
    <h4>Filter by Segment</h4>

    <form action="{{ Filter::searchUrl() }}" method="get">
        <?php
        $querystring = $_SERVER['QUERY_STRING'];
        $seg_list = array();
        if ($querystring != '') {
            $queryArr = explode('&', $querystring);
            foreach ($queryArr as $val) {
                $valArr = explode('=', $val);
                $tempField = str_replace('[]', '', urldecode($valArr[0]));

                if ($tempField != "segment_list" && $tempField != 'page') {
                    echo '<input type="hidden" name="' . urldecode($valArr[0]) . '" value="' . $valArr[1] . '"> ';
                } else {
                    $seg_list[] = $valArr[1];
                }
            }
        }

        foreach ($lists['segments'] as $key => $value) {
            if (in_array($key, $seg_list)) {
                echo '<input type="checkbox" name="segment_list[]" value="' . $key . '" checked="checked"> ' . $value . "<br>";
            } else {
                echo '<input type="checkbox" name="segment_list[]" value="' . $key . '"> ' . $value . "<br>";
            }
        }
        ?>

        <button class="pure-button pure-button-primary" type="submit" title="" >Filter</button>
    </form>
</div>
{{-- End Filter Segment --}}
@stop
@section('body')
<div class="padding">

    <div class="content-head pure-g">
        <div class="title pure-u-1-2">
            <?php if (!empty($countMembers)) { ?>
                            <h1><i class="fa fa-user fa-fw"></i>Members ({{ number_format($countMembers) }})</h1>
            <?php } elseif (is_object($members)) {
                ?>
                            <h1><i class="fa fa-user fa-fw"></i>Members ({{ number_format($members->count()) }})</h1>
            <?php } elseif (is_array($members)) {
                ?>
                            <h1><i class="fa fa-user fa-fw"></i>Members ({{ number_format(count($members)) }})</h1>
            <?php }
            ?>
        </div>

        <div class="controls pure-u-1-2">
            <form action="{{ Filter::searchUrl() }}" method="get">
            <?php
            $querystring = $_SERVER['QUERY_STRING'];
            if ($querystring != '') {
                $queryArr = explode('&', $querystring);
                foreach ($queryArr as $val) {
                    $valArr = explode('=', $val);
                    if ($valArr[0] != "q") {
                        echo '<input type="hidden" name="' . urldecode($valArr[0]) . '" value="' . $valArr[1] . '"> ';
                    }
                }
            }
            ?>
                <input type="text" placeholder="Search" name="q" value="{{ Input::get('q') }}" />
                <button type="submit" title="" class="list-search"></button>
            </form>
            @if(I::can('create_members'))
            <a href="{{ url('dashboard/members/new') }}" class="pure-button pure-button-primary">Add Member</a>
            @endif
        </div>
        <div class="pure-u-1-2" style="width:100%">

            <table style="margin-top: 20px; float: right; width:100%; margin-right:-5px;">
                <tr>
                    <td style="text-align:right;padding-left:20px;">
                        <a href="{{ url('dashboard/members') }}" title="Clear Filters" class="pure-button-primary1 pure-button">
                            Clear Filters <i class="fa fa-ban fa-fw"></i>
                        </a>
                        <a href="#dialog-add-partner" title="Partner" class="dialog pure-button-primary1 pure-button">
                            Partner <i class="fa fa-filter fa-fw"></i>
                        </a>

                        <a href="#dialog-add-country" title="Country" class="dialog pure-button-primary1 pure-button">
                            Country <i class="fa fa-filter fa-fw"></i>
                        </a>

                        <a href="#dialog-add-segment" title="Segment" class="dialog pure-button-primary1 pure-button">
                            Segment <i class="fa fa-filter fa-fw"></i>
                        </a>

                    </td>
                </tr>
            </table>
        </div>
    </div>

    <table class="pure-table">
        <thead>
            <tr>
                <th width="50"><a href="{{ Filter::baseUrl('sort=id') }}">ID</a></th>
                <!--<th width="90">Image</th>-->
                <th style="text-align:left;padding-left:20px;"><a href="{{ Filter::baseUrl('sort=last_name') }}">Name</a></th>
                <th>Country</th>
                <th><a href="{{ Filter::baseUrl('sort=email') }}">Email</a></th>
                <th><a href="{{ Filter::baseUrl('sort=mobile') }}">Mobile</a></th>
                <th><a href="{{ Filter::baseUrl('sort=balance') }}">Balance</a></th>
                <th><a href="{{ Filter::baseUrl('sort=status') }}">Active</a></th>
                <th>Network</th>
                <th width="150">Actions</th>
            </tr>
        </thead>

        <tbody>
            @if(count($members) > 0)
            <?php $network = Auth::User()->getTopLevelPartner()->firstNetwork()->name;?>
            @foreach($members as $m)
            <tr>
                <td>
                    {{{ $m->user_id }}}
                </td>
<!--                            <td>
                    @if(empty($m->profile_image))
                        <img src="http://placehold.it/74x74" alt="" />
                    @else
                        <img src="/media/image/{{ $m->profile_image }}?s=74x74" alt="" />
                    @endif
                </td>-->
                <td style="text-align:left;padding-left:20px;">
                    <a href="/dashboard/members/view/{{ base64_encode($m->user_id)  }}" title="View Member">{{{ $m->first_name }}} {{{ $m->last_name }}}</a>
                </td>
                <td>
                    <?php
                    $country = App\Country::find($m->country_id);
                    ?>
                    @if ($country)
                    {{{ $country->name }}}
                    @else
                    <i>N/A</i>
                    @endif
                </td>
                <td>
                    @if($m->email)
                    <a href="mailto:{{ $m->email }}" title="Email Member">{{{ $m->email }}}</a>
                    @else
                    <i>N/A</i>
                    @endif
                </td>
                <td>
                    @if($m->normalized_mobile)
                    {{{ $m->normalized_mobile }}}
                    @else
                    <i>N/A</i>
                    @endif
                </td>
                <td>
                    <a href="#" title="View Balance Information" class="balance-tooltip" data-id="{{ $m->user_id }}">Details</a>
                </td>
                <td>
                    @if($m->status == 'active')
                    <span class="ico-yes"></span>
                    @else
                    <span class="ico-no"></span>
                    @endif
                </td>
                <td>
                    <?php
                    //$user_member = \App\User::find($m->user_id);
                    //@if ($user_member->networks()->first())
                    //{{ $user_member->networks()->first()->name }}
                    //@else
                    //{{ \App\Network::first()->name }}
                    //@endif
                    echo $network;
                    ?>
                </td>
                <td>
                    @if(I::am('BLU Admin'))
                    <a href="/dashboard/members/view/{{ base64_encode($m->user_id) }}" class="edit" title="View">View</a>

                    @if($m->user_id > 1)
                    <a href="/dashboard/members/delete/{{ base64_encode($m->user_id) }}" class="delete" title="Edit">Delete</a>
                    @endif
                    @else
                    @if($m->user_id > 1)
                    @if(I::can('view_members'))
                    <a href="/dashboard/members/view/{{ base64_encode($m->user_id)}}" class="edit" title="View">View</a>
                    @endif

                    @if(I::can('delete_members'))
                    <a href="/dashboard/members/delete/{{ base64_encode($m->user_id) }}" class="delete" title="Edit">Delete</a>
                    @endif
                    @else
                    N/A
                    @endif
                    @endif
                </td>
            </tr>
            @endforeach
            @else
            <tr>
                <td colspan="10">No Members Found</td>
            </tr>
            @endif
        </tbody>
    </table>
<?php unset($queryAll['page']); ?>
    {{ $members->appends($queryAll)->links() }}

</div>
@stop

@section('page_script')
<script>
    set_menu('mnu-members');
</script>
@stop