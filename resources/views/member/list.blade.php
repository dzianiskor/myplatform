@extends('layouts.dashboard')

@section('title')
    Members | BLU
@stop

@section('body')
    <div class="padding">

        <div class="content-head pure-g">
            <div class="title pure-u-1-2">
                <h1><i class="fa fa-user fa-fw"></i>Members ({{ number_format($members->total()) }})</h1>
            </div>

            <div class="controls pure-u-1-2">
                <form action="{{ Filter::searchUrl() }}" method="get">
                    <input type="text" placeholder="Search" name="q" value="{{ Input::get('q') }}" />
                    <button type="submit" title="" class="list-search"></button>
                </form>
                @if(I::can('create_members'))
                    <a href="{{ url('dashboard/members/new') }}" class="pure-button pure-button-primary">Add Member</a>
                @endif
            </div>

            <div id="catalog-filter" class="pure-u-1-2">
                <input id="filter_route" type="hidden" value="members">
                <form id="filter-catalogue" action="{{ Filter::searchUrl() }}" method="get">

                    <div class="filter-block filter-block-pricing-width">
                        <span class="filter-label">Partner:</span>
                        <select style="width:100%;" name="partner[]" data-filter-catalogue="partner" data-filter-catalogue-title="Select Partner" multiple="multiple">
                            @foreach ($lists['partners'] as $key => $value)
                                <option value="{{ $key }}">{{ $value }}</option>
                            @endforeach
                        </select>
                        <input data-filter-catalogue="partner" type="hidden" value="{{ Input::get('partner') }}">
                    </div>

                    <div class="filter-block filter-block-pricing-width">
                        <span class="filter-label">Country:</span>
                        <select style="width:100%;" name="country[]" data-filter-catalogue="country" data-filter-catalogue-title="Select Country" multiple="multiple">
                            @foreach ($lists['countries'] as $key => $value)
                                <option value="{{ $key }}">{{ $value }}</option>
                            @endforeach
                        </select>
                        <input data-filter-catalogue="country" type="hidden" value="{{ Input::get('country') }}">
                    </div>

                    <div class="filter-block filter-block-pricing-width">
                        <span class="filter-label">Segment:</span>
                        <select style="width:100%;" name="segment[]" data-filter-catalogue="segment" data-filter-catalogue-title="Select Segment" multiple="multiple">
                            @foreach ($lists['segments'] as $key => $value)
                                <option value="{{ $key }}">{{ $value }}</option>
                            @endforeach
                        </select>
                        <input data-filter-catalogue="segment" type="hidden" value="{{ Input::get('segment') }}">
                    </div>

                    <div class="submit-filter-block disable-width">
                        <div style="float:right;">
                            <a href="<?php echo e(url('dashboard/members')); ?>" title="Clear Filters" class="pure-button pure-button-primary">
                                Clear Filters <i class="fa fa-ban fa-fw"></i>
                            </a>
                        </div>
                        <button class="pure-button pure-button-primary" type="submit" title="" style="float:right;margin-right:15px;">
                            Submit Filters
                        </button>
                    </div>
                </form>
            </div>

        </div>

        <table class="pure-table">
            <thead>
                <tr>
                    <th width="50"><a href="{{ Filter::baseUrl('sort=id') }}">ID</a></th>
                    <!--<th width="90">Image</th>-->
                    <th style="text-align:left;padding-left:20px;"><a href="{{ Filter::baseUrl('sort=last_name') }}">Name</a></th>
                    <th>Country</th>
                    <th><a href="{{ Filter::baseUrl('sort=email') }}">Email</a></th>
                    <th><a href="{{ Filter::baseUrl('sort=mobile') }}">Mobile</a></th>
                    <th><a href="{{ Filter::baseUrl('sort=balance') }}">Balance</a></th>
                    <th><a href="{{ Filter::baseUrl('sort=status') }}">Active</a></th>
                    <th>Network</th>
                    <th width="150">Actions</th>
                </tr>
            </thead>

            <tbody>
                @if(count($members) > 0)
                    @foreach($members as $m)
                        <tr>
                            <td>
                                {{{ $m->id }}}
                            </td>
<!--                            <td>
                                @if(empty($m->profile_image))
                                    <img src="http://placehold.it/74x74" alt="" />
                                @else
                                    <img src="/media/image/{{ $m->profile_image }}?s=74x74" alt="" />
                                @endif
                            </td>-->
                            <?php
                            $memberName = $m->first_name . " " . $m->last_name;
                            if(trim($memberName) == "na"){
                                $memberName = "N/A";
                            }
                            elseif ($memberName == "na na") {
                                $memberName = "N/A";
                            }
                            ?>
                            <td style="text-align:left;padding-left:20px;">
                                <a href="/dashboard/members/view/{{ base64_encode($m->id) }}" title="View Member">{{{ $memberName }}}</a>
                            </td>
                            <td>
                                @if ($m->country)
                                    {{{ $m->country->name }}}
                                @else
                                    <i>N/A</i>
                                @endif
                            </td>
                            <td>
                                @if($m->email)
                                    <a href="mailto:{{ $m->email }}" title="Email Member">{{{ $m->email }}}</a>
                                @else
                                    <i>N/A</i>
                                @endif
                            </td>
                            <td>
                                @if($m->normalizedContactNumber())
                                    {{{ $m->normalizedContactNumber() }}}
                                @else
                                    <i>N/A</i>
                                @endif
                            </td>
                            <td>
                                <a href="#" title="View Balance Information" class="balance-tooltip" data-id="{{ base64_encode($m->id)}}">Details</a>
                            </td>
                            <td>
                                @if($m->status == 'active')
                                    <span class="ico-yes"></span>
                                @else
                                    <span class="ico-no"></span>
                                @endif
                            </td>
                            <td>
                                @if ($m->networks->first())
                                    {{ $m->networks->first()->name }}
                                @else
                                    {{ \App\Network::first()->name }}
                                @endif
                            </td>
                            <td>
                                @if(I::am('BLU Admin'))
                                    <a href="/dashboard/members/view/{{ base64_encode($m->id)}}" class="edit" title="View">View</a>
                                
                                    @if($m->id > 1)
                                        <a href="/dashboard/members/delete/{{ base64_encode($m->id) }}" class="delete" title="Edit">Delete</a>
                                    @endif
                                @else
                                    @if($m->id > 1)
                                        @if(I::can('view_members'))
                                            <a href="/dashboard/members/view/{{ base64_encode($m->id) }}" class="edit" title="View">View</a>
                                        @endif

                                        @if(I::can('delete_members'))
                                            <a href="/dashboard/members/delete/{{ base64_encode($m->id) }}" class="delete" title="Edit">Delete</a>
                                        @endif   
                                    @else
                                        N/A                                     
                                    @endif
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="10">No Members Found</td>
                    </tr>
                @endif
            </tbody>
        </table>

        {{ $members->setPath('members')->appends(Input::except('page'))->links() }}

    </div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-members');
    </script>
@stop