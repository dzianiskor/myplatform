<table>
	@if(I::am('BLU Admin'))
		@foreach($user->balances() as $b)
			<tr>
				<td style="text-align:left;padding-right:5px;">{{ $b->network->name }}:</td>
				<td>{{ number_format($b->balance) }}</td>
			</tr>
		@endforeach
	@else
		{{---@foreach($user->balances() as $b)
			 Only show BLU Network Balance 
			@if($b->network->id == 1)---}}
                            @if(!empty($balance))
				<tr>
					<td style="text-align:left;padding-right:5px;">{{ $balance->network->name }}:</td>
					<td>{{ number_format($balance->balance) }}</td>
				</tr>
                            @else
                                <tr>
                                    <td style="text-align:left;padding-right:5px;" colspan="2">N/A</td>
				</tr>
                            @endif
			{{--- @endif 
		@endforeach		---}}
	@endif
</table>