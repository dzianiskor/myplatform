@extends('layouts.dashboard')

@section('js_globals')
    var MEMBER_ID = {{ $member->id }};
@stop

@section('title')
    @if($member->draft)
        Member &raquo; New  | BLU
    @else
        Member &raquo; {{{ $member->last_name }}}, {{{ $member->first_name }}}  | BLU
    @endif
@stop

@section('dialogs')
    {{-- Assign new Partner --}}
    <div id="dialog-add-partner" style="width:300px;">
        <h4>Associate to Partner</h4>

        {{ Form::open(array('class' => "pure-form pure-form-stacked")) }}
            {{ Form::select('partner_id', $partners, null, array('class' => 'pure-input-1')) }}

            <a href="/dashboard/partners/attach/{{ base64_encode($member->id) }}" class="pure-button pure-button-primary attribute-action" data-target="#partner-listing">
                Save
            </a>
        {{ Form::close() }}
    </div>

    {{-- Add New Card --}}
    <div id="dialog-add-card" style="width:300px;">
        <h4>Add Card</h4>

        {{ Form::open(array('class' => "pure-form pure-form-stacked")) }}
            {{ Form::text('card_number', '', array('class' => 'pure-input-1')) }}

            <a href="/dashboard/cards/save/{{ base64_encode($member->id) }}" class="pure-button pure-button-primary attribute-action" data-target="#card-listing">
                Save
            </a>
        {{ Form::close() }}
    </div>

    {{-- Add New Reference ID --}}
    <div id="dialog-add-reference" style="width:300px;">
        <h4>Add Reference ID</h4>

        {{ Form::open(array('class' => "pure-form pure-form-stacked")) }}
            {{ Form::text('reference_number', '', array('class' => 'pure-input-1')) }}

            <a href="/dashboard/reference/link/{{ base64_encode($member->id)}}" class="pure-button pure-button-primary attribute-action" data-target="#reference-listing">
                Save
            </a>
        {{ Form::close() }}
    </div>

    {{-- Add New Affiliate program Membership --}}
    <div id="dialog-add-affiliate-program" style="width:400px;">
        <h4>Affiliate Program Memberships</h4>

        {{ Form::open(array('class' => "pure-form pure-form-stacked", 'onkeypress' => "return event.keyCode != 13;")) }}
        <div class="content-head pure-g">
            <div class="title pure-u-1-2">
                <div class="padding-right-10">
                    {{ Form::label('program_id', 'Affiliate Programs') }}
                    {{ Form::select('program_id', $affiliatePrograms, '', array('class' => 'pure-input-1')) }}
                </div>
            </div>
            <div class="title pure-u-1-2">
                {{ Form::label('membership_number', 'Membership Number') }}
                {{ Form::text('membership_number', '', array('class' => 'pure-input-1')) }}
            </div>
        </div>
        <a href="/dashboard/members/createmembership/{{ base64_encode($member->id) }}" class="pure-button pure-button-primary attribute-action" data-target="#affiliate-program-memberships-listing">
            Save
        </a>
        {{ Form::close() }}
    </div>
@stop

@section('body')
    <div class="padding">

        {{-- Page Header --}}
        <div class="content-head pure-g">
            <div class="title pure-u-1-2">
                @if($member->draft)
                    <h1><i class="fa fa-user fa-fw"></i>Member &raquo; New</h1>
                @else
                <?php
                    $memberName = $member->first_name . " " . $member->last_name;
                    if($memberName == "na"){
                        $memberName = "N/A";
                    }
                    elseif ($memberName == "na na") {
                        $memberName = "N/A";
                    }
                ?>
                    <h1><i class="fa fa-user fa-fw"></i>Member &raquo; {{ $memberName }} </h1>
                @endif
            </div>

            <div class="controls pure-u-1-2">
                <a href="{{ url('dashboard/members') }}" class="pure-button">All Members</a>
            </div>
        </div>
        {{-- /Page Header --}}

        {{ Form::open(array('url' 		   => url('dashboard/members/update/'.base64_encode($member->id)),
                            'files' 	   => true,
                            'method' 	   => 'post',
                            'class' 	   => "pure-form pure-form-stacked form-width",
							'autocomplete' => 'off',
                            'id'           => 'memberForm')) }}

            @include('partials.errors')

            {!! Form::hidden('redirect_to', URL::previous()) !!}

            <div class="pure-g">
                <div class="pure-u-1-2">
                    <fieldset>
                        <legend>Basic Details</legend>
                        <?php
                            $memberFirstName = $member->first_name;
                            $memberMiddleName = $member->middle_name;
                            $memberLastName = $member->last_name;
                            if($memberFirstName == "na"){
                                $memberFirstName = "N/A";
                            }
                            if($memberMiddleName == "na"){
                                $memberMiddleName = "N/A";
                            }
                            if($memberLastName == "na"){
                                $memberLastName = "N/A";
                            }
                        ?>
                        {{ Form::label('status', 'Status') }}
                        {{ Form::select('status', $statuses, $member->status, array('class' => 'pure-input-1-3')) }}

                        {{ Form::label('ucid', 'Unique Customer ID') }}
                        {{ Form::text('ucid', $member->ucid, array('class' => 'pure-input-1', 'maxlength' => 9, 'disabled' => 'disabled')) }}

                        <a href="#" title="View Balance Information" class="balance-tooltip padded-href" data-id="{{ $member->id }}">
                            <i class="fa fa-dot-circle-o"></i> See Network Balances
                        </a>

                        {{ Form::label('title', 'Title') }}
                        {{ Form::select('title', Meta::titleList(), $member->title, array('class' => 'pure-input-1-3')) }}

                        {{ Form::label('first_name', 'First Name') }}
                        {{ Form::text('first_name', $memberFirstName, array('class' => 'pure-input-1 required')) }}

                        {{ Form::label('middle_name', 'Middle Name') }}
                        {{ Form::text('middle_name', $memberMiddleName, array('class' => 'pure-input-1')) }}

                        {{ Form::label('last_name', 'Last Name') }}
                        {{ Form::text('last_name', $memberLastName, array('class' => 'pure-input-1 required')) }}

                        @if($member->draft)
                            {{ Form::label('pin', 'PIN Code') }}
                            {{ Form::text('pin', '', array('class' => 'pure-input-1-2 required','maxlength' => 4, 'minlength' => 4,'required' => 'required')) }}
                        @endif

                        <div class="image-rail" style="width:340px;">
                            <table style="margin-top:20px;margin-bottom:20px;">
                                <tr>
                                    <td width="80">
                                        @if(empty($member->profile_image))
                                            <img src="https://placehold.it/74x74" alt="" />
                                        @else
                                            <img src="<?php echo App\Http\Controllers\MediaController::getImageFromId($member->profile_image) ?>?s=74x74" alt="" style="width:80px;"/>
                                        @endif
                                    </td>
                                    <td style="padding-left:10px;">
                                        {{ Form::label('image', 'Profile Image') }}
                                        {{ Form::file('image') }}
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </fieldset>

                    <fieldset>
                        <legend>Contact Details</legend>

                        {{ Form::label('telephone_code', 'Country Telephone Code') }}
						<select name="telephone_code" class="pure-input-1">
							<?php foreach(Meta::countries() as $c): ?>
								<?php if($c->telephone_code == $member->telephone_code): ?>
									<option value="<?php echo $c->telephone_code; ?>" selected="selected"><?php echo $c->name; ?> (<?php echo $c->telephone_code; ?>)</option>
								<?php else: ?>
									<option value="<?php echo $c->telephone_code; ?>"><?php echo $c->name; ?> (<?php echo $c->telephone_code; ?>)</option>
								<?php endif; ?>
							<?php endforeach; ?>
						</select>

                        {{ Form::label('mobile', 'Mobile Number') }}
                        {{ Form::text('mobile', $member->mobile, array('class' => 'pure-input-1 enforceNumeric required')) }}

                        <div id="mobile-error" class="inline-error">
                            The number provided is already in use by another account.
                        </div>

                        {{ Form::label('email', 'Email Address') }}
                        {{ Form::text('email', $member->email, array('class' => 'pure-input-1', 'id'=>'email')) }}

                        <div id="email-error" class="inline-error">
                            The email provided is already in use by another account.
                        </div>
					</fieldset>

                    <fieldset>
                        <legend>Additional Details</legend>

                        {{ Form::label('marital_status', 'Marital Status') }}
                        {{ Form::select('marital_status', Meta::maritalStatuses(), $member->marital_status, array('class' => 'pure-input-1-3')) }}

                        {{ Form::label('gender', 'Gender') }}
                        {{ Form::select('gender', Meta::genderList(), $member->gender, array('class' => 'pure-input-1-3')) }}

                        {{ Form::label('year', 'Date of Birth') }}

                        <div class="pure-g">
                            <div class="pure-u-1-3">
                                {{ Form::select('day', Meta::dayArray(), Meta::dateComponent($member->dob, 'day'), array('class' => 'pure-input-1')) }}
                            </div>

                            <div class="pure-u-1-3">
                                <div class="padding-lr-10">
                                    {{ Form::select('month', Meta::monthArray(), Meta::dateComponent($member->dob, 'month'), array('class' => 'pure-input-1')) }}
                                </div>
                            </div>

                            <div class="pure-u-1-3">
                                {{ Form::select('year', Meta::yearArray(), Meta::dateComponent($member->dob, 'year'), array('class' => 'pure-input-1')) }}
                            </div>
                        </div>

                        <div class="clearfix"></div>

                        {{ Form::label('occupation_id', 'Occupation') }}
                        {{ Markup::select('occupation_id', $occupations, $member->occupation_id, array('class' => 'pure-input-1')) }}

                        {{ Form::label('income_bracket', 'Income Bracket') }}
                        {{ Form::select('income_bracket', Meta::incomeList(), $member->income_bracket, array('class' => 'pure-input-1')) }}

                        {{ Form::label('num_children', 'Number of Children') }}
                        {{ Form::text('num_children', $member->num_children, array('class' => 'pure-input-1 enforceNumeric')) }}

                        {{ Form::label('tag', 'Special Attributes') }}
                        {{ Form::text('tag', $member->tag, array('class' => 'pure-input-1')) }}
                    </fieldset>

                    <fieldset>
                        <legend>Address Details</legend>

                        {{ Form::label('country_id', 'Country') }}
                        {{ Markup::country_select('country_id', $member->country_id, array('class' => 'pure-input-1-3')) }}

                        {{ Form::label('area_id', 'Area') }}
                        {{ Markup::area_select('area_id', $member->country_id, $member->area_id, array('class' => 'pure-input-1-3')) }}

                        {{ Form::label('city_id', 'City') }}
                        {{ Markup::city_select('city_id', $member->area_id, $member->city_id, array('class' => 'pure-input-1-3')) }}

                        {{ Form::label('address_1', 'Address') }}
                        {{ Form::text('address_1', $member->address_1, array('class' => 'pure-input-1')) }}

                        {{ Form::label('address_2', 'Address (Continued)') }}
                        {{ Form::text('address_2', $member->address_2, array('class' => 'pure-input-1')) }}
                    </fieldset>

                    <fieldset style="display:none;">
                        <legend>Admin Authentication Details</legend>

                        <span class="info">
                            Only complete this section if you want to allow this user to log into the backend.
                        </span>

                        {{ Form::label('username', 'Username') }}
                        {{ Form::text('username', $member->username, array('class' => 'pure-input-1')) }}

                        {{ Form::label('password', 'Password (Current password not shown)') }}
                        {{ Form::password('password', null, array('class' => 'pure-input-1', 'id'=>'password')) }}

                        {{ Form::label('confirm_password', 'Confirm Password') }}
                        {{ Form::password('confirm_password', null, array('class' => 'pure-input-1', 'id'=>'confirm_password')) }}

                        {{ Form::label('api_key', 'API Key') }}
                        {{ Form::text('username', $member->api_key, array('class' => 'pure-input-1', 'disabled' => 'disabled')) }}
                    </fieldset>

                </div>
                <div class="pure-u-1-2 attributes">
                    <div class="padding-left-40">

                        {{-- Networks Listing --}}
                        {{---
                        <h3>Networks</h3>
                        <ul id="segment-listing">
                            @if(count($member->networks) > 0)
                                @foreach($member->networks as $n)
                                    <li>{{ $n->name }}</li>
                                @endforeach
                            @else
                                <li class="empty">No Networks Associated</li>
                            @endif
                        </ul>
                        ---}}
                        {{-- /Networks Listing --}}

                        {{-- Partner Listing --}}
                        <h3>
                            @if($member->id != Auth::User()->id && I::can_edit('members', $member))
                                <a href="#dialog-add-partner" title="New Partner" class="dialog">
                                    Partners <i class="fa fa-plus-circle fa-fw"></i>
                                </a>
                            @else
                                Partners
                            @endif
                        </h3>
                        <ul id="partner-listing">
                            @if($member->partners->count() > 0)
                                {{-- */$first = true;/* --}}
                                @foreach($member->partners as $p)
                                    @if(Auth::User()->canSeePartner($p->id))
                                        @if(I::am('BLU Admin') && $p->id != 1)
                                            <li>
                                                {{ $p->name }}
                                                @if(I::can('delete_members'))
                                                    <a href="/dashboard/partners/unlink/{{ base64_encode($member->id) }}/{{ base64_encode($p->id) }}" class="delete-ajax" data-id="{{ $p->id }}" data-target="#partner-listing">[Delete]</a>
                                                @endif
                                            </li>
                                        @else
                                            <li>
                                                {{ $p->name }}
                                                @if(I::can('delete_members') && $p->id != 1)
                                                    <a href="/dashboard/partners/unlink/{{ $member->id }}/{{ $p->id }}" class="delete-ajax" data-id="{{ $p->id }}" data-target="#partner-listing">[Delete]</a>
                                                @endif
                                            </li>
                                        @endif
                                    @endif
                                {{-- */$first = false;/* --}}
                                @endforeach
                            @else
                                <li class="empty">No Partner Associated</li>
                            @endif
                        </ul>
                        {{-- /Partner Listing --}}

                        {{-- Segments Listing --}}
                        <h3>Segments</h3>
                        <ul id="segment-listing">
                            @if(count($member->segments) > 0)
                                @foreach($member->segments as $s)
                                    @if(in_array($s->partner_id, Auth::User()->managedPartnerIDs()))
                                        <li>{{ $s->name }}</li>
                                    @endif
                                @endforeach
                            @else
                                <li class="empty">No Segments Associated</li>
                            @endif
                        </ul>
                        {{-- /Segments Listing --}}

                        {{-- Card Listing --}}
                        <h3>
                            @if(I::can('assign_cards'))
                                <a href="#dialog-add-card" title="New Card" class="dialog">
                                    Cards <i class="fa fa-plus-circle fa-fw"></i>
                                </a>
                            @else
                                Cards
                            @endif
                        </h3>
                        <ul id="card-listing">
                            @if(count($member->cards) > 0)
                                @foreach($member->cards as $c)
                                @if(I::am('BLU Admin'))
                                    <li>
                                        {{ $c->number }}

                                        @if(I::can('delete_members'))
                                            <a href="/dashboard/cards/unlink/{{ $member->id }}/{{ $c->id }}" class="delete-ajax" data-target="#card-listing">
                                                [Delete]
                                            </a>
                                        @endif
                                    </li>
                                @else
                               <?php
                                   $prtid1 = Auth::User()->getTopLevelPartner()->id;
                                   $crdprtid    = $c->partner_id;

                                   if($prtid1 == $crdprtid){
                               ?>
                                <li>
                                    {{ $c->number }}

                                    @if(I::can('delete_members'))
                                        <a href="/dashboard/cards/unlink/{{ $member->id }}/{{ $c->id }}" class="delete-ajax" data-target="#card-listing">
                                            [Delete]
                                        </a>
                                    @endif
                                </li>
                                <?php
                                        }
                                    ?>
                                    @endif
                                @endforeach
                            @else
                                <li class="empty">No Cards Associated</li>
                            @endif
                        </ul>
                        {{-- /Card Listing --}}

                        {{-- Reference Listing --}}
                        <h3>
                            @if(I::can('assign_ref_ids'))
                                <a href="#dialog-add-reference" title="New Reference" class="dialog">
                                    References <i class="fa fa-plus-circle fa-fw"></i>
                                </a>
                            @else
                                References
                            @endif
                        </h3>
                        <ul id="reference-listing">
                            @if(count($member->references) > 0)
                                @foreach($member->references as $c)
                                    @if(I::am('BLU Admin'))
                                    <li>
                                        {{ $c->number }}
                                        @if(I::can('delete_members'))
                                            <a href="/dashboard/reference/unlink/{{ $member->id }}/{{ $c->id }}" class="delete-ajax" data-target="#reference-listing">
                                                [Delete]
                                            </a>
                                        @endif
                                    </li>
                                    @else
                                    <?php
                                        $prtid = Auth::User()->getTopLevelPartner()->id;
                                        $refprtid    = $c->partner_id;

                                        if($prtid == $refprtid){
                                    ?>
                                        <li>
                                            {{ $c->number }}
                                            @if(I::can('delete_members'))
                                                <a href="/dashboard/reference/unlink/{{ $member->id }}/{{ $c->id }}" class="delete-ajax" data-target="#reference-listing">
                                                    [Delete]
                                                </a>
                                            @endif
                                        </li>
                                    <?php
                                        }
                                    ?>
                                    @endif
                                @endforeach
                            @else
                                <li class="empty">No References Associated</li>
                            @endif
                        </ul>
                        {{-- /Card Listing --}}

                        {{-- Affiliate Program Listing --}}
                        <h3>
                            @if(I::can('assign_affiliate_program'))
                                <a href="#dialog-add-affiliate-program" title="New Affiliate Program Membership" class="dialog">
                                    Affiliate Program Membership <i class="fa fa-plus-circle fa-fw"></i>
                                </a>
                            @else
                                Affiliate Program Membership
                            @endif
                        </h3>
                        <ul id="affiliate-program-memberships-listing">
                            <?php
                            $affliateProgramsMemberships = $member->affliateProgramsMemberships;
                            ?>
                            @if(count($affliateProgramsMemberships) > 0)
                                @foreach($affliateProgramsMemberships as $apm)

                                    @if(isset($allAffiliatePrograms[$apm->program_id]))
                                        <li>
                                            {{ $allAffiliatePrograms[$apm->program_id] }}
                                            @if(I::can('edit_members'))
                                                <a href="/dashboard/members/viewmembership/{{ base64_encode($apm->id) }}" class="edit_membership" style="right: 70px;">
                                                    [Edit]
                                                </a>
                                            @endif
                                            @if(I::can('delete_members'))
                                                <a href="/dashboard/members/deletemembership/{{ base64_encode($member->id) }}/{{ base64_encode($apm->id) }}" class="delete-ajax" data-target="#affiliate-program-memberships-listing">
                                                    [Delete]
                                                </a>
                                            @endif
                                        </li>
                                    @endif

                                @endforeach
                            @else
                                <li class="empty">No Affiliate Program Memberships Associated</li>
                            @endif
                        </ul>
                        {{-- /Affiliate Program Memberships Listing --}}

                    </div>
                </div>
            </div>

            <!-- Form Buttons -->
            <div class="form-buttons">
                <div class="left">
                    <a href="{{ url('dashboard/members') }}" class="pure-button">Cancel</a>
                </div>
                @if($member->draft)
                    @if(I::can('create_members'))
                        <div class="right">
                            <button type="submit" class="pure-button pure-button-primary">Create Member</button>
                        </div>
                    @endif
                @else
                    @if(I::can('edit_members'))
                        <div class="right">
                            <button type="submit" class="pure-button pure-button-primary">Save Member</button>
                        </div>
                    @endif
                @endif
                <div class="clearfix"></div>
            </div>
            <!-- /Form Buttons -->
        {{ Form::close() }}
    </div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-members');

        // Verify email/mobile uniqueness
        $(document).on('blur','#mobile, #email', function(){
            _this = $(this);

            if (!_this.val()) {
                $('#'+_this.attr('name')+'-error').hide();
                $('.form-buttons .pure-button-primary').prop('disabled', false);
                return;
            }

            $.get('/dashboard/members/verify/'+_this.attr('name')+'/'+$(this).val()+'/'+MEMBER_ID, function(json){
                if(!json.unique){
                    $('#'+_this.attr('name')+'-error').show();
                    $('.form-buttons .pure-button-primary').prop('disabled', true);
                } else {
                    $('#'+_this.attr('name')+'-error').hide();
                    $('.form-buttons .pure-button-primary').prop('disabled', false);
                }
            });
        });

        $(document).on('click', ".form-buttons .pure-button-primary", function(e){
            if ($('ul#partner-listing li.empty').html() == 'No Partner Associated') {
                e.preventDefault();

                alert('A valid partner entity must be associated with a member account before saving.');

                return;
            }
        });

        // Edit country details
        $(document).on('click', 'a.edit_membership', function(e) {
            e.preventDefault();
            _this = $(this);

            $.get(_this.attr('href'), function(html) {
                showDialogWithContents(html, function(){

                });
            });
        });
    </script>
@stop