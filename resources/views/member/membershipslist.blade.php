@if(count($member->affliateProgramsMemberships) > 0)
    @foreach($member->affliateProgramsMemberships as $apm)
        <li>
            {{ App\AffiliateProgram::find($apm->program_id)->name }}
            @if(I::can('edit_members'))
                <a href="/dashboard/members/viewmembership/{{ base64_encode($apm->id) }}" class="edit_membership" style="right: 70px;">
                    [Edit]
                </a>
            @endif
            @if(I::can('delete_members'))
                <a href="/dashboard/members/deletemembership/{{ base64_encode($member->id) }}/{{ base64_encode($apm->id) }}" class="delete-ajax" data-target="#affiliate-program-memberships-listing">
                    [Delete]
                </a>
            @endif
        </li>
    @endforeach
@else
    <li class="empty">No Affiliate Program Memberships Associated</li>
@endif