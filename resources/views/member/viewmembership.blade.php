<h4>Affiliate Program Memberships</h4>

{{ Form::open(array('class' => "pure-form pure-form-stacked")) }}
<div class="content-head pure-g">
    <div class="title pure-u-1-2">
        <div class="padding-right-10">
            {{ Form::label('program_id', 'Affiliate Programs') }}
            {{ Form::select('program_id', $affiliatePrograms, $affiliateProgramMembership->program_id, array('class' => 'pure-input-1')) }}
        </div>
    </div>
    <div class="title pure-u-1-2">
        {{ Form::label('membership_number', 'Membership Number') }}
        {{ Form::text('membership_number', $affiliateProgramMembership->membership_number, array('class' => 'pure-input-1')) }}
    </div>
</div>
<a href="/dashboard/members/updatemembership/{{ base64_encode($affiliateProgramMembership->id) }}/{{ base64_encode($affiliateProgramMembership->user_id) }}" class="pure-button pure-button-primary attribute-action" data-target="#affiliate-program-memberships-listing">
    Save
</a>
{{ Form::close() }}