@extends('layouts.mobile')

@section('body')
    {{ Form::model($member, array('url' => url("mobile/member/$member->id"), 'method' => 'post', 'class' => 'pure-form pure-form-stacked')) }}

        <fieldset>
            <legend>Member Details</legend>

            {{ Form::label('mobile', 'Mobile Number') }}
            {{ Form::text('mobile', $member->normalized_mobile, array('disabled' => 'disabled', 'class' => 'pure-input-1')) }}

            {{ Form::label('first_name', 'First Name') }}
            {{ Form::text('first_name', $member->first_name, array('class' => 'pure-input-1')) }}

            {{ Form::label('last_name', 'Last Name') }}
            {{ Form::text('last_name', $member->last_name, array('class' => 'pure-input-1')) }}

            {{ Form::label('email', 'Email') }}
            {{ Form::text('email', $member->email, array('class' => 'pure-input-1')) }}            

            {{ Form::label('card', 'Card') }}
            {{ Form::text('card', '', array('class' => 'pure-input-1')) }}            

            {{ Form::label('gender', 'Gender') }}
            {{ Form::select('gender', Meta::genderList(), $member->gender, array('class' => 'pure-input-1')) }}
        </fieldset>

        @include('mobile.partials.buttons', array('title' => 'Proceed'))

        @if(isset($coupon_code))
            {{ Form::hidden('coupon_code', $coupon_code) }}
        @endif

        @if(isset($amount))
            {{ Form::hidden('amount',$amount) }}
        @endif

        @if(isset($currency))
            {{ Form::hidden('currency', $currency) }}
        @endif

    {{  Form::Close() }}
@stop

@section('page_script')
    <script>
        enqueue_script('mobile');
    </script>
@stop
