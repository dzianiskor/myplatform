@extends('layouts.mobile')

@section('body')
    {{ Form::open(array('url' => url('mobile/scan/'), 'method' => 'post', 'class' => 'page-padding pure-form pure-form-stacked')) }}

        <fieldset>
            <legend>Scan or swipe member card</legend>

            {{ Form::text('scan', '', array('id' => 'scan', 'class' => 'pure-input-1', 'placeholder' => 'Barcode Number')) }}

            <div class="center">
                <button id="doscan" class="pure-button pure-button-primary">Scan barcode</button>
            </div>
        </fieldset>

        @include('mobile.partials.buttons', array('title' => 'Search'))

    {{ Form::Close() }}
@stop

@section('page_script')
    <script>
        enqueue_script('mobile');

        $(function(){
            $('input#scan').focus();

            if(isPhoneGap()) {
                $('#doscan').show();
            }
        });

    </script>
@stop


