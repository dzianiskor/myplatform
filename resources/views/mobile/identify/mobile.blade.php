@extends('layouts.mobile')

@section('body')
@if($errors->any())
<h6 class="emp">{{$errors->first()}}</h6>
@endif
    {{ Form::model($mobile,array('url' => url('mobile/mobile/'), 
                'method' => 'post', 
                'class' => 'page-padding pure-form pure-form-stacked')) }}
        <div class="pure-u-1-1 right">
            <a   href="/mobile/menu" class="pure-button pure-button-primary" style="float: right; color: #FFF">Menu</a>
        </div>
        <fieldset>
            <legend>Enter member mobile number, Reference or Member Card Number</legend>

            {{ Form::label('telephone_code', 'Country Code') }}                    
            <select name="telephone_code" id="telephone_code" class="pure-input-1">
                <?php foreach(Meta::countries() as $c): ?>
                  
                    @if($mobile && $c->telephone_code == $mobile->country_id)
                        <option value="<?php echo $c->telephone_code; ?>" selected="selected">
                            <?php echo $c->name; ?> (<?php echo $c->telephone_code; ?>)
                        </option>
                    @else
                        <option value="<?php echo $c->telephone_code; ?>">
                            <?php echo $c->name; ?> (<?php echo $c->telephone_code; ?>)
                        </option>
                    @endif
                <?php endforeach; ?>
            </select>  

            {{ Form::label('mobile', 'Mobile Number') }}                    
            <input type="tel" name="mobile" id="mobile" class="enforceNumeric" />          

            {{ Form::label('reference', 'Reference') }}                    
            <input type="text" name="reference" id="reference" class="" />  
            
            {{ Form::label('barcode', 'Scan or swipe member card') }}                    
            <input type="text" name="barcode" id="barcode" class="" />          

        </fieldset>

        @include('mobile.partials.buttons', array('title' => 'Search'))
    
    {{ Form::Close() }}
@stop

@section('page_script')
    <script>
        enqueue_script('mobile');

        $(function(){
            $('input#mobile').focus();
        });        
    </script>
@stop
