@extends('layouts.mobile')

@section('body')
	<div class="mobile-error">
		<h1>
			Access Error
		</h1>
		
		<p>
			Your user account does not have access to this application.
		</p>

		<a href="/mobile" class="pure-button">Back</a>
	</div>
@stop

@section('page_script')
	<script>
	    _cookie = $.fn.cookie('mobile_uid', null);
	    
	    localStorage.removeItem('mobile_uid');
	</script>
@stop
