<!-- Form Buttons -->
<div class="form-buttons">
        <div style="text-align:right">
		<a href="/mobile/print-transaction/{{ $trx_id }}" class="pure-button pure-button-primary dark-button">Print</a>
	
		<a href="/mobile" class="pure-button pure-button-primary dark-button">{{ $title or 'Save' }}</a>
	</div>
</div>
<!-- /Form Buttons -->