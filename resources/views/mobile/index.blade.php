@extends('layouts.mobile')

@section('body')
    <div id="main-menu">
        <div class="pure-u-1-1 left">
            <a   href="/mobile/mobile" class="pure-button pure-button-primary" style="color: #FFF">Return</a>
        </div>
        <span class="version">v{{ Config::get('blu.mobile_version') }}</span>

        <table>
            <tr>
                <td>
                    <a class="pure-button pure-button-main" href="/mobile/settings">Settings</a>
                </td>
            </tr>
<!--            <tr>
                <td>
                    <a class="pure-button pure-button-main" href="/mobile/scan">Card</a>
                </td>
            </tr>
            <tr>
                <td>
                    <a class="pure-button pure-button-main" href="/mobile/mobile">Mobile</a>
                </td>
            </tr>-->
            <tr>
                <td>
                    <a class="pure-button pure-button-main" href="/mobile/transactions">Todays Transactions</a>
                </td>
            </tr>
            <tr>
                <td>
                    <a class="pure-button pure-button-main lightRedBtn" href="/mobile/logout">Logout</a>
                </td>
            </tr>            
        </table>
    </div>
@stop

@section('page_script')
    <script>
        enqueue_script('mobile');

        $(function(){
            $('a.lightRedBtn').on('click', function(e){
                e.stopPropagation();
                e.preventDefault();

                _cookie = $.fn.cookie('mobile_uid', null);
                
                localStorage.removeItem('mobile_uid');

                window.location.href = '/mobile/logout';
            });
        });
    </script>
@stop
