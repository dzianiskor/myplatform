@extends('layouts.mobile')

@section('body')
	<div class="mobile-error">
		<h1>
			Something went wrong!
		</h1>
		<p>
			{{ $error }}
		</p>
	</div>

	@include('mobile.partials.back')
@stop

@section('page_script')
@stop
