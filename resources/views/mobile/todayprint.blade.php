<!DOCTYPE html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta http-equiv="cache-control" content="max-age=0" />
        <meta http-equiv="cache-control" content="no-cache" />
        <meta http-equiv="expires" content="0" />
        <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
        <meta http-equiv="pragma" content="no-cache" />
        <title>{{ $page_title or 'BLU Mobile' }}</title>
        <meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="{{ asset('css/pure-min_0.4.2.css') }}">
        
        <link rel="icon" href="/favicon.ico" type="image/x-icon" />
        <meta http-equiv="cleartype" content="on" />
        <meta name="rating" content="general" />

        {{--- Add all ext service URL's here ---}}
        <meta http-equiv="x-dns-prefetch-control" content="on">
        <link rel="dns-prefetch" href="//google-analytics.com">
        <link rel="dns-prefetch" href="//www.google-analytics.com">
        <link rel="dns-prefetch" href="//ajax.googleapis.com">
        <link rel="dns-prefetch" href="//fonts.google.com">
        <link rel="dns-prefetch" href="//fonts.googleapis.com">
        <link rel="dns-prefetch" href="//yui.yahooapis.com">
        <link rel="dns-prefetch" href="//netdna.bootstrapcdn.com">
        <link rel="dns-prefetch" href="//code.jquery.com">
        <link rel="dns-prefetch" href="//jquery.com">
        <link rel="dns-prefetch" href="//ajax.aspnetcdn.com">
        <link rel="dns-prefetch" href="//aspnetcdn.com">

        {{--- Layout for Javascript templates ---}}
        @yield('templates')

        {{--- Set any Javascript globals here ---}}
        <script type="text/javascript">
            @yield('js_globals')
        </script>
    </head>
 
    <body onload="window.print(); window.location.href = '/mobile';">

    <div class="">
        <h2>Today's Store Transactions</h2>

        <table class="">
            <thead>
                <th style="width: 50px" class="">Tx id</th>
                <th style="text-align: left; padding-left: 15px; max-width:175px;">User</th>
                <th style="width: 150px; padding-left:15px;" class="">Amount</th>
            </thead>
            @if($transactions->count() > 0)
                @foreach($transactions as $transaction)
                    @if($transaction->voided)
                        <tr class="voided">
                    @else
                        <tr>
                    @endif
                        <td>
                            {{ $transaction->id }}
                        </td>
                        <td style="text-align: left; padding-left: 15px;  max-width:175px;">
                            {{ $transaction->user->getTitleAndFullName() }} <br>( {{ $transaction->user->normalizedContactNumber() }} )
                        </td>
                        <td style="width: 150px; padding-left:15px; text-align: center;">
                            @if($transaction->trx_reward == 1)
                                    {{ number_format($transaction->total_amount * $transaction->exchange_rate, 2) }} {{ $transaction->currency->short_code }}
                            @else
                                    -{{ $transaction->points_redeemed }} Pts
                            @endif
                        </td>
                    </tr>
                @endforeach
            @else 
                <tr>
                    <td colspan="3" class="empty" style="background-color:#fff">No Transactions Found</td>
                </tr>
            @endif
        </table>
    </div>

    {{-- $transactions->links() --}}    

    

    </body>

@section('page_script')
<script>
    enqueue_script('mobile');
</script>
@stop
