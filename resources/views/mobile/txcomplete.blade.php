@extends('layouts.mobile')

@section('body')

    {{ Form::open(array('url'    => '/mobile',
                        'method' => 'get',
                        'class'  => 'pure-form pure-form-stacked page-padding')) }}

        <fieldset>
            <legend>Transaction Details</legend>

            <table>
                @if($response->status != 200)
                    <tr>
                        <td>Status:</td>
                        <td>
                            <img src="/img/no.png" alt="Failure" />
                        </td>
                    </tr>
                    <tr>
                        <td>Messsage:</td>
                        <td>
                            {{ $response->message }}
                        </td>
                    </tr>
                @else

                    <tr>
                        <td>Status:</td>
                        <td>
                            <img src="/img/yes.png" alt="Success" />
                        </td>
                    </tr>
                    <tr>
                        <td>Store:</td>
                        <td>
                            <strong>{{ $store->name }}</strong>
                        </td>
                    </tr>

                    <tr>
                        <td>Transaction Date &amp; Time:</td>
                        <td>
                            <?php
                                $strtotime1 = '';
                                if(isset($response->transaction['trx'])){
                                    $strtotime1 = strtotime($response->transaction['trx']['created_at']);
                                }
                                else{
                                    $strtotime1 = strtotime($response->transaction['created_at']);
                                }
                            ?>
                            <strong>{{ date("F j, Y, g:i a", $strtotime1) }}</strong>
                        </td>
                    </tr>
                    <tr>
                        <td>Member:</td>
                        <td>
                            <strong>{{$member->name}}</strong>
                        </td>
                    </tr>
                    <tr>
                        <td>Transaction Type:</td>
                        <td>
                            <strong>{{ ucfirst($type) }}</strong>
                        </td>
                    </tr>
                    @if($type == 'reward')
                        <tr>
                            <td>Points Rewarded:</td>
                            <td>
                                <strong>+ {{ number_format($response->transaction['points_rewarded']) }} Points</strong>
                            </td>
                        </tr>
                    @elseif($type == 'bonus')
                        <tr>
                            <td>Bonus Points:</td>
                            <td>
                                <strong>+ {{ number_format($response->transaction['points_rewarded']) }} Points</strong>
                            </td>
                        </tr>
                    @else
                        <tr>
                            <td>Points Redeemed:</td>
                            <td>
                                <strong>- {{ number_format($response->transaction['trx']['points_redeemed']) }} Points</strong>
                            </td>
                        </tr>
                    @endif

                @endif

            </table>

        </fieldset>

    {{ Form::close() }}
    @if(isset($response->transaction['trx']))
        @include('mobile.partials.done', array('title' => 'Done', 'trx_id' => $response->transaction["trx"]["id"]))
    @else
        @include('mobile.partials.done', array('title' => 'Done', 'trx_id' => $response->transaction['id']))
    @endif

@stop

@section('page_script')
    <script>
        enqueue_script('mobile');
    </script>
@stop
