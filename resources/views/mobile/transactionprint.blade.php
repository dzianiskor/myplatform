<!DOCTYPE html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta http-equiv="cache-control" content="max-age=0" />
        <meta http-equiv="cache-control" content="no-cache" />
        <meta http-equiv="expires" content="0" />
        <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
        <meta http-equiv="pragma" content="no-cache" />
        <title>{{ $page_title or 'BLU Mobile' }}</title>
        <meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="{{ asset('css/pure-min_0.4.2.css') }}">
        
        <link rel="icon" href="/favicon.ico" type="image/x-icon" />
        <meta http-equiv="cleartype" content="on" />
        <meta name="rating" content="general" />

        {{--- Add all ext service URL's here ---}}
        <meta http-equiv="x-dns-prefetch-control" content="on">
        <link rel="dns-prefetch" href="//google-analytics.com">
        <link rel="dns-prefetch" href="//www.google-analytics.com">
        <link rel="dns-prefetch" href="//ajax.googleapis.com">
        <link rel="dns-prefetch" href="//fonts.google.com">
        <link rel="dns-prefetch" href="//fonts.googleapis.com">
        <link rel="dns-prefetch" href="//yui.yahooapis.com">
        <link rel="dns-prefetch" href="//netdna.bootstrapcdn.com">
        <link rel="dns-prefetch" href="//code.jquery.com">
        <link rel="dns-prefetch" href="//jquery.com">
        <link rel="dns-prefetch" href="//ajax.aspnetcdn.com">
        <link rel="dns-prefetch" href="//aspnetcdn.com">

        

        
    </head>
    


    <body  style="height: 10cm; width: 8cm;"  onload="window.print(); window.location.href = '/mobile';">



        <fieldset>
            <legend>Transaction Details</legend>

            <table>
                <?php if($transaction->voided){ ?>
                <tr>
                    <td>
                        Status:
                    </td>
                    <td class="emp">
                        <strong>VOIDED</strong>
                    </td>
                </tr>
                <?php } ?>
                <tr>
                    <td>
                        Member Name:
                    </td>
                    <td>
                        <strong><?php echo $transaction->user->getTitleAndFullName(); ?></strong>
                    </td>
                </tr>
               <tr>
                    <td>
                        Mobile:
                    </td>
                    <td>
                        <strong><?php echo $transaction->user->normalizedContactNumber(); ?></strong>
                    </td>
                </tr>    
               <tr>
                    <td>
                        Email:
                    </td>
                    <td>
                        <strong><?php echo $transaction->user->email; ?></strong>
                    </td>
                </tr>     
               <tr>
                    <td>
                        Transaction Type:
                    </td>
                    <td>
                        <?php if($transaction->trx_reward == 1){ ?>
                            <strong>Reward (+<?php echo $transaction->points_rewarded; ?> Pts)</strong>
                        <?php } else { ?>
                            <strong>Redeem (-<?php echo $transaction->points_redeemed; ?> Pts)</strong>
                        <?php } ?>
                    </td>
                </tr>    
               <tr>
                    <td>
                        Date &amp; Time:
                    </td>
                    <td>
                        <strong><?php echo date("F j, Y, g:i a", strtotime($transaction->created_at)); ?></strong>
                    </td>
                </tr> 
                <tr>
                    <td>
                        Remaining Balance:
                    </td>
                    <td>
                        <strong><?php 
                        //$partner = App\Partner::find($transaction->partner_id);
                        echo $transaction->user->balance($transaction->network_id); ?></strong>
                    </td>
                </tr>
            </table>
        </fieldset>

           
    </body>


