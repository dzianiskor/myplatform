@extends('layouts.mobile')

@section('body')
    {{ Form::open(array('url'    => url('mobile/txinfo/'),
                        'method' => 'post',
                        'class'  => 'pure-form pure-form-stacked')) }}

        <fieldset>
            <legend>Member Details</legend>

            <table>
                <tr>
                    <td>Name</td>
                    <td><strong>{{ $member->name }}</strong></td>
                </tr>                
                <tr>
                    <td>Mobile Number</td>
                    <td><strong>{{ $member->normalizedContactNumber() }}</strong></td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td><strong>{{ $member->email }}</strong></td>
                </tr>                
            </table>
        </fieldset>

        <fieldset>
            <legend>Transaction Details</legend>

            {{ Form::label('currency_id', 'Currency') }}
            <select name="currency_id" id="currency_id" class="pure-input-1">
                @foreach($currencies as $c)
                    @if($c->short_code == $mobile->currency_id)
                        <option value="{{ $c->short_code }}" selected="selected">{{ $c->name }}</option>
                    @else
                        <option value="{{ $c->short_code }}">{{ $c->name }}</option>
                    @endif
                @endforeach
            </select>

            {{ Form::label('amount', 'Amount') }}
            <input type="text" id="amount" name="amount" value="" placeholder="0" class="required pure-input-1" autocorrect="off" />
            
            {{ Form::label('points', 'Points') }}
            <input type="text" id="points" name="points" value="" onchange="calcAmount()" placeholder="0" class="required pure-input-1" autocorrect="off" />

            {{ Form::label('coupon_code', 'Coupon Code') }}
            {{ Form::text('coupon_code', null, array('class' => 'pure-input-1')) }}
        </fieldset>            

        {{ Form::hidden('member_id', $member->id) }}

        @include('mobile.partials.buttons', array('title' => 'Continue'))

    {{ Form::close() }}
@stop

@section('page_script')
    
@stop
<script>
    function calcAmount(){
        $('#amount').val(parseInt($('#points').val())/100);
        return;
    }
</script>
