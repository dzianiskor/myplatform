@if(count($supplier->networks()) == 0)
    <li class="empty">No Network Associated</li>
@else
    @foreach($supplier->networks as $n)
        <li>
            {{{ $n->name }}}

            @if(I::can('delete_tiers'))
                <a href="/dashboard/suppliers/detach/network/{{ $supplier->id }}/{{ $n->id }}" class="delete-ajax" data-target="#network-listing">
                    [Delete]
                </a>
            @endif
        </li>
    @endforeach
@endif
