@extends('layouts.dashboard')

@section('title')
    Tiers | BLU
@stop

@section('body')
    <div class="padding">

        <div class="content-head pure-g">
            <div class="title pure-u-1-2">
                <h1><i class="fa fa-map-marker fa-fw"></i>Tiers ({{ $tiers->total() }})</h1>
            </div>

            <div class="controls pure-u-1-2">
                <form action="{{ Filter::searchUrl() }}" method="get">
                    <input type="text" placeholder="Search" name="q" value="{{ Input::get('q') }}" />
                    <button type="submit" title="" class="list-search"></button>
                </form>
                @if(I::can('create_tiers'))
                    <a href="{{ url('dashboard/tiers/new') }}" class="pure-button pure-button-primary">Add Tier</a>
                @endif
            </div>
        </div>

        <table class="pure-table">
            <thead>
                <tr>
                    <th width="50"><a href="{{ Filter::baseUrl('sort=id') }}">ID</a></th>
                    <th style="text-align:left;padding-left:20px;"><a href="{{ Filter::baseUrl('sort=name') }}">Name</a></th>
                    <th>Partner</th>
                    <th width="150">Actions</th>
                </tr>
            </thead>

            <tbody>
                @if(count($tiers) > 0)
                    @foreach($tiers as $p)
                        <tr>
                            <td>
                                {{{ $p->id}}}
                            </td>

                            <td style="text-align:left;padding-left:20px;">
                                <a href="/dashboard/tiers/view/{{ base64_encode($p->id) }}" title="View Tier">
                                    {{ ucwords($p->name) }}
                                </a>
                            </td>
                            
                            <td>
                                @if ($p->partner_id)
                                    <?php
                                        $partner = App\Partner::find($p->partner_id);
                                        if($partner){
                                            echo $partner->name;
                                        }else{
                                            echo 'N/A';
                                        }
                                    ?>
                                @else
                                    <i>N/A</i>
                                @endif
                            </td>                            
                            <td>
                                @if(I::can('view_tiers'))
                                    <a href="/dashboard/tiers/view/{{ base64_encode($p->id) }}" class="edit" title="View">View</a>
                                @endif

                                @if(I::can('delete_tiers') && $p->id != 1)
                                    <a href="/dashboard/tiers/delete/{{ base64_encode($p->id) }}" class="delete" title="Delete">Delete</a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="9">No Tiers Found</td>
                    </tr>
                @endif
            </tbody>
        </table>

        {{ $tiers->setPath('tiers')->appends(Input::except('page'))->links() }}

    </div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-tiers');
    </script>
@stop
