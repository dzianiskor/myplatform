@extends('layouts.dashboard')

@section('title')
    Tier &raquo; {{{ $tier->name }}}  | BLU
@stop



@section('body')
    <div class="padding">

        {{-- Page Header --}}
        <div class="content-head pure-g">
            <div class="title pure-u-1-2">
                <h1><i class="fa fa-map-marker fa-fw"></i>Tier &raquo; {{{ $tier->name }}}</h1>
            </div>

            <div class="controls pure-u-1-2">
                <a href="{{ url('dashboard/tiers') }}" class="pure-button">All Tiers</a>
            </div>
        </div>
        {{-- /Page Header --}}

       
        {{ Form::open(array('url' => url('dashboard/tiers/update/' . base64_encode($tier->id)), 'files' => true, 'method' => 'post', 'class' => "pure-form pure-form-stacked form-width", 'id' => 'tierForm')) }}

            @include('partials.errors')

            {!! Form::hidden('redirect_to', URL::previous()) !!}

            <div class="pure-g">
                {{--- Left Section Content ---}}
                <div class="pure-u-1">
                    <fieldset>
                        <legend>Basic Details</legend>

                        {{ Form::label('name', 'Tier Name') }}
                        {{ Form::text('name', $tier->name, array('class' => 'pure-input-1 required')) }}
                        
                        
                            {{ Form::label('partner_id', 'Parent Partner') }}
                            <select name="partner_id" class="pure-input-1-3 required" id="partner_id">
                                <option value="">Select Partner</option>
                                @if($tier->partner_id)
                                    
                                    @foreach($partners as $id => $name)
                                        @if($id != $tier->partner_id)
                                            <option value="{{ $id }}">{{ $name }}</option>
                                        @else
                                            <option value="{{ $id }}" selected="selected">{{ $name }}</option>
                                        @endif
                                    @endforeach
                                @else
                                    @foreach($partners as $id => $name)
                                        <option value="{{ $id }}">{{ $name }}</option>
                                    @endforeach
                                @endif
                            </select>
                        
                        
                        {{ Form::label('description', 'Tier Description') }}
                        {{ Form::textarea('description', $tier->description, array('class' => 'pure-input-1', 'rows' => 10)) }}
                        <input type="hidden" name="start_month" value="1"/>
                        {{ Form::label('start_month', 'Start Month') }}
                        <select name="start_month1" class="pure-input-1-3 required" id="start_month" disabled="disabled">
                            @if($tier->start_month == 1)
                                <option value="1" selected="selected">January</option>
                            @else
                                <option value="1" selected="selected">January</option>
                            @endif
                            @if($tier->start_month == 2)
                                <option value="2" selected="selected">February</option>
                            @else
                                <option value="2">February</option>
                            @endif
                            @if($tier->start_month == 3)
                                <option value="3" selected="selected">March</option>
                            @else
                                <option value="3">March</option>
                            @endif
                            @if($tier->start_month == 4)
                                <option value="4" selected="selected">April</option>
                            @else
                                <option value="4">April</option>
                            @endif
                            @if($tier->start_month == 5)
                                <option value="5" selected="selected">May</option>
                            @else
                                <option value="5">May</option>
                            @endif
                            @if($tier->start_month == 6)
                                <option value="6" selected="selected">June</option>
                            @else
                                <option value="6">June</option>
                            @endif
                            @if($tier->start_month == 7)
                                <option value="7" selected="selected">July</option>
                            @else
                                <option value="7">July</option>
                            @endif
                            @if($tier->start_month == 8)
                                <option value="8" selected="selected">August</option>
                            @else
                                <option value="8">August</option>
                            @endif
                            @if($tier->start_month == 9)
                                <option value="9" selected="selected">September</option>
                            @else
                                <option value="9">September</option>
                            @endif
                            @if($tier->start_month == 10)
                                <option value="10" selected="selected">October</option>
                            @else
                                <option value="10">October</option>
                            @endif
                            @if($tier->start_month == 11)
                                <option value="11" selected="selected">November</option>
                            @else
                                <option value="11">November</option>
                            @endif
                            @if($tier->start_month == 12)
                                <option value="12" selected="selected">December</option>
                            @else
                                <option value="12">December</option>
                            @endif
                            
                        </select>
                        <input type="hidden" name="promotion_in_months" value="12"/>
                        {{ Form::label('promotion_in_months', 'Promotion (in Months)') }}
                        <select name="promotion_in_months1" class="pure-input-1-3 required" id="promotion_in_months" disabled="disabled">
                            @if($tier->promotion_in_months == 1)
                                <option value="1" selected="selected">1 Month</option>
                            @else
                                <option value="1">1 Month</option>
                            @endif
                            @if($tier->promotion_in_months == 2)
                                <option value="2" selected="selected">2 Months</option>
                            @else
                                <option value="2">2 Months</option>
                            @endif
                            @if($tier->promotion_in_months == 3)
                                <option value="3" selected="selected">3 Months</option>
                            @else
                                <option value="3">3 Months</option>
                            @endif
                            @if($tier->promotion_in_months == 4)
                                <option value="4" selected="selected">4 Months</option>
                            @else
                                <option value="4">4 Months</option>
                            @endif
                            @if($tier->promotion_in_months == 6)
                                <option value="6" selected="selected">6 Months</option>
                            @else
                                <option value="6">6 Months</option>
                            @endif
                            @if($tier->promotion_in_months == 12)
                                <option value="12" selected="selected">12 Months</option>
                            @else
                                <option value="12" selected="selected">12 Months</option>
                            @endif
                        </select>
                        
                        <input type="hidden" name="demotion_in_months" value="12"/>
                        {{ Form::label('demotion_in_months', 'Demotion (in Months)') }}
                        <select name="demotion_in_months1" class="pure-input-1-3 required" id="demotion_in_months" disabled="disabled">
                            @if($tier->demotion_in_months == 1)
                                <option value="1" selected="selected">1 Month</option>
                            @else
                                <option value="1">1 Month</option>
                            @endif
                            @if($tier->demotion_in_months == 2)
                                <option value="2" selected="selected">2 Months</option>
                            @else
                                <option value="2">2 Months</option>
                            @endif
                            @if($tier->demotion_in_months == 3)
                                <option value="3" selected="selected">3 Months</option>
                            @else
                                <option value="3">3 Months</option>
                            @endif
                            @if($tier->demotion_in_months == 4)
                                <option value="4" selected="selected">4 Months</option>
                            @else
                                <option value="4">4 Months</option>
                            @endif
                            @if($tier->demotion_in_months == 6)
                                <option value="6" selected="selected">6 Months</option>
                            @else
                                <option value="6">6 Months</option>
                            @endif
                            @if($tier->demotion_in_months == 12)
                                <option value="12" selected="selected">12 Months</option>
                            @else
                                <option value="12" selected="selected">12 Months</option>
                            @endif
                        </select>
                        <?php $months_expiry =0;?>
                        @if(count($criterias) > 0)
                            @foreach($criterias as $tiercriteria)
                                <?php $months_expiry = $tiercriteria->expiry_in_months; ?>
                            @endforeach
                        @else
                            <?php $months_expiry = 12; ?>
                        @endif
                        {{ Form::label('tiercriteria_expiry_in_months', 'Expiry for all Tiers') }}
                        <select name="tiercriteria_expiry_in_months" class="pure-input-1-3">
            
                            
                                <option value="12" <?php echo ($months_expiry == 12?'selected="selected"':'');?>>1 Year</option>

                                <option value="24" <?php echo ($months_expiry == 24?'selected="selected"':'');?>>2 Year</option>
                            
                                <option value="36" <?php echo ($months_expiry == 36?'selected="selected"':'');?>>3 Years</option>
                            
                                <option value="48" <?php echo ($months_expiry == 48?'selected="selected"':'');?>>4 Years</option>
                            
                                <option value="60" <?php echo ($months_expiry == 60?'selected="selected"':'');?>>5 Years</option>
                            
                                <option value="72" <?php echo ($months_expiry == 72?'selected="selected"':'');?>>6 Years</option>
                           
                                <option value="84" <?php echo ($months_expiry == 84?'selected="selected"':'');?>>7 Years</option>
                            
                                <option value="96" <?php echo ($months_expiry == 96?'selected="selected"':'');?>>8 Years</option>
                            
                                <option value="108" <?php echo ($months_expiry == 108?'selected="selected"':'');?>>9 Years</option>
                           
                                <option value="120" <?php echo ($months_expiry == 120?'selected="selected"':'');?>>10 Years</option>
                        </select>
            
                    </fieldset>

                    

                    
                 </div>
                {{--- /Left Section Content ---}}

                
            </div>

            {{--- Criterias ---}}                  
            <fieldset>
                <legend>Criteria</legend>

                <table id="tier-criteria-list" class="pure-table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Criterion</th>
                            <th>Start Value</th>
                            <th>End Value</th>
                            @if(I::can('edit_tiers') || I::can('view_tiers') || I::can('delete_tiers'))
                                <th>Actions</th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($criterias) > 0)
                            @foreach($criterias as $tiercriteria)
                                <tr>
                                    <td>{{ $tiercriteria->id }}</td>
                                    @if($tiercriteria->draft)
                                        <td>{{ $tiercriteria->name }} (Draft)</td>
                                    @else
                                        <td>{{ $tiercriteria->name }}</td>
                                    @endif
                                    <td>
                                      {{ $tiercriteria->start_value }}
                                    </td>
                                    <td>
                                      {{ $tiercriteria->end_value }}
                                    </td>
                                    <td>
                                        @if(I::can('edit_tiers') || I::can('view_tiers'))
                                            <a href="{{ url('dashboard/tiercriteria/edit/'.base64_encode($tiercriteria->id))  }}" class="edit-tier-criteria" title="Edit">Edit</a>
                                        @endif
                                        @if(I::can('delete_tiers'))
                                            <a href="{{ url('dashboard/tiercriteria/delete/'.base64_encode($tiercriteria->id))  }}" class="delete-tier-criteria" title="Delete">Delete</a>
                                        @else
                                            N/A
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr class="empty">
                                <td colspan="5">No tier criteria defined</td>
                            </tr>
                        @endif
                    </tbody>
                </table>

                @if(I::can_edit('tiers', $tier))
                    <a href="#dialog-new-tier-criteria" data-tier_id="{{ $tier->id }}" class="btn-new-tier-criteria top-space" title="Add new criteria">
                        <i class="fa fa-plus-circle fa-fw"></i>Add new criterion
                    </a>
                @endif
            </fieldset>
            {{--- /Criterias ---}}                  

            {{--- Form Buttons ---}}                  
            <div class="form-buttons">
                <div class="left">
                    <a href="{{ url('dashboard/tiers') }}" class="pure-button">Cancel</a>
                </div>

                @if($tier->draft == true)
                    @if(I::can('create_tiers'))
                        <div class="right">
                            <button type="submit" class="pure-button pure-button-primary">Create Tier</button>
                        </div>
                    @endif
                @else
                    @if(I::can('edit_tiers'))
                        <div class="right">
                            <button type="submit" class="pure-button pure-button-primary">Save Tier</button>
                        </div>
                    @endif
                @endif

                <div class="clearfix"></div>
            </div>
            {{--- /Form Buttons ---}}                  

        {{ Form::close() }}
    </div>
@stop

@section('page_script')
    @if(!I::can_edit('tiers', $tier))
        <script>
            //enqueue_script('disable-form');
        </script>
    @endif

    

    <script>
        set_menu('mnu-tiers');
        
        enqueue_script('new-tier');

        @if(!I::can('create_tiers') && !I::can('edit_tiers'))
            disableForm('#tierForm');
        @endif
    </script>
@stop