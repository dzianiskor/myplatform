@if(count($segmentusers) > 0)
    @foreach($segmentusers as $segmentuser)
        <tr>
        <td>{{ $segmentuser->user_id }}</td>
        <td>{{ $segmentuser->user->first_name }} {{ $segmentuser->user->last_name }}</td>
        <td>
            @if (I::can('delete_segments'))
                <a href="{{ url('dashboard/segmentuser/delete/' . base64_encode($segmentuser->user_id)) . '/' . base64_encode($segmentuser->segment_id) }}" class="delete-segmentuser" title="Delete">Delete</a>
            @else
                N/A
            @endif
        </td>
    </tr>
    @endforeach
@else
<tr class="empty">
    <td colspan="5">No Users Linked</td>
</tr>
@endif