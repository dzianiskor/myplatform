<div style="width: 800px !important;">
{{ Form::open(array('url' => url('dashboard/segmentuser/create/'), 'method' => 'post', 'class' => "pure-form pure-form-stacked", 'id' => 'new-segmentuser-form')) }}
    
    @include('partials.errors')                         

    <div class="pure-g">
        <div class="pure-u-1">
            {{ Form::hidden('segment_id', $data['segment_id'], null, array('class' => 'pure-input-1')) }}

            {{ Form::open(array('url' => "#", 'class' => "pure-form", "id" => 'formcashback')) }}
            <span id="Product">
                {{ Form::label('user_id', 'User Selection') }}
                <input type="text" id="users-autocomplete" class="inline-block-display" placeholder="Search" />
                <button id="users-search" class="rounded-search">Search</button>
                <select name="user_id" id="user_id" class="pure-input-1"></select>
            </span>

            {{ Form::close() }}
        </div>
    </div>

    {{-- Dialog Buttons --}}
    <div class="form-buttons">
        <div class="left">
            <a href="#" class="pure-button close-dialog">Cancel</a>
        </div>
        <div class="right">
            <a href="#" class="pure-button pure-button-primary save-new-segmentuser">Save</a>
        </div>
        <div class="clearfix"></div>
    </div>
    {{-- /Dialog Buttons --}}

{{ Form::close() }}
</div>