<h4>Rules history</h4>
<table class="pure-table" style="width:100%">
    <thead>
        <tr>
            <th>ID</th>
            <th>Type</th>
            <th>Points</th>
            <th>Reward</th>
            <th>Currency</th>
            <th>For</th>
            <th>Matching</th>
            <th>Created date</th>
            <th>Deletion date</th>
        </tr>
    </thead>
    <tbody>
    @if ($rules->count() > 0)
        @foreach($rules as $rule)
            <?php
            $ruleType = array();
            $rulevalue = array();
            $exception_rules_obj = $rule->exception_rules()->withTrashed()->get();
            if (!empty($exception_rules_obj)) {
                foreach ($exception_rules_obj as $exception_rule_obj) {
                    $ruleType[] = $exception_rule_obj->type;
                    //                                        $rulevalue[] = $exception_rule_obj->rule_value;
                    if ($exception_rule_obj->type == 'Segment')
                        $rulevalue[] = Meta::segment_name_from_id($exception_rule_obj->rule_value);

                    if ($exception_rule_obj->type == 'Country')
                        $rulevalue[] = Meta::country_name_from_id($exception_rule_obj->rule_value);

                    if ($exception_rule_obj->type == 'Store')
                        $rulevalue[] = Meta::store_name_from_id($exception_rule_obj->rule_value);

                    if ($exception_rule_obj->type == 'Item')
                        $rulevalue[] = Meta::item_name_from_id($exception_rule_obj->rule_value);

                    if ($exception_rule_obj->type == 'Category')
                        $rulevalue[] = Meta::category_name_from_id($exception_rule_obj->rule_value);

                    if ($exception_rule_obj->type == 'Brand')
                        $rulevalue[] = Meta::brand_name_from_id($exception_rule_obj->rule_value);

                    if ($exception_rule_obj->type == 'Currency')
                        $rulevalue[] = Meta::currency_name_from_id($exception_rule_obj->rule_value);

                    if ($exception_rule_obj->type == 'Merchant Category Code')
                        $rulevalue[] = Meta::mcc_name_from_id($exception_rule_obj->rule_value);

                    if ($exception_rule_obj->type == 'Transaction Types')
                        $rulevalue[] = Meta::trxtype_name_from_id($exception_rule_obj->rule_value);

                    if ($exception_rule_obj->type == 'Amount')
                        $rulevalue[] = $exception_rule_obj->rule_value . ' ' . Meta::currency_name_from_id($exception_rule_obj->rule_value_currency);
                }
                $ruleType = implode(', ', $ruleType);
                $ruleValue = implode(', ', $rulevalue);
            } else {

                $ruleType = $rule->rule_type;
                if ($rule->type == 'Segment')
                    $ruleValue = Meta::segment_name_from_id($ruleType->rule_value);

                if ($rule->type == 'Country')
                    $ruleValue = Meta::country_name_from_id($rule->rule_value);

                if ($rule->type == 'Store')
                    $ruleValue = Meta::store_name_from_id($rule->rule_value);

                if ($rule->type == 'Item')
                    $ruleValue = Meta::item_name_from_id($rule->rule_value);

                if ($rule->type == 'Category')
                    $ruleValue = Meta::category_name_from_id($rule->rule_value);

                if ($rule->type == 'Brand')
                    $ruleValue = Meta::brand_name_from_id($rule->rule_value);

                if ($rule->type == 'Currency')
                    $ruleValue = Meta::currency_name_from_id($rule->rule_value);

                if ($rule->type == 'Merchant Category Code')
                    $ruleValue = Meta::mcc_name_from_id($rule->rule_value);

                if ($rule->type == 'Transaction Types')
                    $ruleValue = Meta::trxtype_name_from_id($rule->rule_value);

                if ($rule->type == 'Amount')
                    $ruleValue = $rule->rule_value . ' ' . Meta::currency_name_from_id($ruleValue->rule_value_currency);
            }
            if (empty($ruleType)) {
                $ruleType = $rule->rule_type;
            }
            if (empty($ruleValue)) {
                $ruleValue = $rule->rule_value;
            }
            ?>
            <tr>
                <td>{{ $rule->id }}</td>
                <td>{{ $rule->type }}</td>
                <td>{{ $rule->reward_pts }}</td>
                <td>{{ number_format($rule->original_reward, 2) }}</td>
                <td>
                    <?php
                    if ($rule->type !== 'Event') {
                        echo App\Currency::find($rule->currency_id)->short_code;
                    }
                    ?>
                </td>
                <td>{{ $ruleType }}</td>
                <td>
                    {{ $ruleValue }}
                </td>
                <td>{{ $rule->created_at }}</td>
                <td>{{ $rule->deleted_at }}</td>
            </tr>
        @endforeach
    @else
        <tr>
            <td colspan="9">No results</td>
        </tr>
    @endif
    </tbody>
</table>