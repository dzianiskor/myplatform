<?php
$ruleType = array();
$rulevalue = array();
$exception_rules_obj = App\LoyaltyExceptionrule::where('loyalty_rule_id', $rule->id)->get();
if (!empty($exception_rules_obj)) {
    foreach ($exception_rules_obj as $exception_rule_obj) {
        $ruleType[] = $exception_rule_obj->type;
//                                        $rulevalue[] = $exception_rule_obj->rule_value;
        if ($exception_rule_obj->type == 'Segment')
            $rulevalue[] = Meta::segment_name_from_id($exception_rule_obj->rule_value);

        if ($exception_rule_obj->type == 'Country')
            $rulevalue[] = Meta::country_name_from_id($exception_rule_obj->rule_value);

        if ($exception_rule_obj->type == 'Store')
            $rulevalue[] = Meta::store_name_from_id($exception_rule_obj->rule_value);

        if ($exception_rule_obj->type == 'Item')
            $rulevalue[] = Meta::item_name_from_id($exception_rule_obj->rule_value);

        if ($exception_rule_obj->type == 'Category')
            $rulevalue[] = Meta::category_name_from_id($exception_rule_obj->rule_value);

        if ($exception_rule_obj->type == 'Brand')
            $rulevalue[] = Meta::brand_name_from_id($exception_rule_obj->rule_value);

        if ($exception_rule_obj->type == 'Currency')
            $rulevalue[] = Meta::currency_name_from_id($exception_rule_obj->rule_value);

        if ($exception_rule_obj->type == 'Merchant Category Code')
            $rulevalue[] = Meta::mcc_name_from_id($exception_rule_obj->rule_value);

        if ($exception_rule_obj->type == 'Transaction Types')
            $rulevalue[] = Meta::trxtype_name_from_id($exception_rule_obj->rule_value);
    }
    $ruleType = implode(', ', $ruleType);
    $ruleValue = implode(', ', $rulevalue);
}
else {

    $ruleType = $rule->rule_type;
    if ($rule->type == 'Segment')
        $ruleValue = Meta::segment_name_from_id($ruleType->rule_value);

    if ($rule->type == 'Country')
        $ruleValue = Meta::country_name_from_id($rule->rule_value);

    if ($rule->type == 'Store')
        $ruleValue = Meta::store_name_from_id($rule->rule_value);

    if ($rule->type == 'Item')
        $ruleValue = Meta::item_name_from_id($rule->rule_value);

    if ($rule->type == 'Category')
        $ruleValue = Meta::category_name_from_id($rule->rule_value);

    if ($rule->type == 'Brand')
        $ruleValue = Meta::brand_name_from_id($rule->rule_value);

    if ($rule->type == 'Currency')
        $ruleValue = Meta::currency_name_from_id($rule->rule_value);

    if ($rule->type == 'Merchant Category Code')
        $ruleValue = Meta::mcc_name_from_id($rule->rule_value);

    if ($rule->type == 'Transaction Types')
        $ruleValue = Meta::trxtype_name_from_id($rule->rule_value);
}
if (empty($ruleType)) {
    $ruleType = $rule->rule_type;
}
if (empty($ruleValue)) {
    $ruleValue = $rule->rule_value;
}
?>
<tr>
    <td>{{ $rule->id }}</td>
    <td>{{ $rule->type }}</td>
    <td>{{ $rule->reward_pts }}</td>
    <td>{{ number_format($rule->original_reward, 2) }}</td>
    <td>
        <?php
        if ($rule->type !== 'Event') {
            echo App\Currency::find($rule->currency_id)->short_code;
        }
        ?>
    </td>
    <td>{{ $ruleType }}</td>
    <td>
        {{ $ruleValue }}
    </td>
    <td>
        @if($rule->status == 'Enable')
            <span id="rule_id_{{ $rule->id }}" class="ico-yes"></span>
        @else
            <span id="rule_id_{{ $rule->id }}" class="ico-no"></span>
        @endif
    </td>
    <td>
        @if (I::can('edit_loyalty') || I::can('delete_loyalty'))
            @if (I::can('edit_loyalty'))
                <a href="{{ url('dashboard/loyalty/editrule/' . base64_encode($rule->id)) }}" class="edit-loyalty-rule">Edit</a>
            @endif
            @if (I::can('delete_loyalty'))
                <a href="/dashboard/loyalty/unlink/{{ base64_encode($rule->id) }}" class="delete-rule">Remove</a>
            @endif
        @endif
    </td>
</tr>


