<div id="dialog-edit-rule" style="width:400px;">
    <h4>Edit Exception Rule</h4>

    {{ Form::open(array('url' => "/dashboard/loyalty/updaterule/".base64_encode($rule->id), 'class' => "pure-form pure-form-stacked form_hidden_attributes")) }}
        {{ Form::hidden('rule_status', null) }}
    {{ Form::close() }}

    <div class="form_z pure-form pure-form-stacked">
        <select id="rule_status" class="pure-input-1 rule-status">
            <option value="Enable" @if ($rule->status == 'Enable') selected="selected" @endif>Enable</option>
            <option value="Disable" @if ($rule->status == 'Disable') selected="selected" @endif>Disable</option>
        </select>
    </div>

    <div class="exception-type-wrapper">
        <div class="type-forms">
            <div class="pure-form">
                <select name="rule-item" id="rule-item" disabled class="pure-input-1 rule-item">
                    <option selected="selected" >
                        @if (isset($exception_rules_obj->type))
                            {{ $exception_rules_obj->type }}
                        @else
                            {{ $rule->rule_type }}
                        @endif
                    </option>
                </select>
            </div>

            <div class="form_x pure-form pure-form-stacked">
                <select name="rule_comparison" id="rule_comparison" disabled class="pure-input-1 rule-comparison">
                    @if (isset($exception_rules_obj->comparison_operator))
                        <option selected="selected">
                            @if ($exception_rules_obj->comparison_operator == 'equal')
                                {{ '=' }}
                            @elseif ($exception_rules_obj->comparison_operator == 'different')
                                {{ '!=' }}
                            @elseif ($exception_rules_obj->comparison_operator == 'less_than')
                                {{ '<' }}
                            @elseif ($exception_rules_obj->comparison_operator == 'greater_than')
                                {{ '>' }}
                            @elseif ($exception_rules_obj->comparison_operator == 'less_or_equal')
                                {{ '&lt;=' }}
                            @elseif ($exception_rules_obj->comparison_operator == 'greater_or_equal')
                                {{ '&gt;=' }}
                            @endif
                        </option>
                    @else
                        <option selected="selected">=</option>
                    @endif
                </select>
            </div>

            <div class="form_0 pure-form pure-form-stacked">
                <select name="rule_value" id="rule_value" class="pure-input-1" disabled>
                    <?php

                        if (isset($exception_rules_obj)) {
                            $ruleType = $exception_rules_obj->type;
                            $ruleId = $exception_rules_obj->rule_value;
                        } else {
                            $ruleType = $rule->rule_type;
                            $ruleId = '';
                        }

                        $ruleValue = '';
                        if($ruleId) {
                            if ($ruleType == 'Segment') {
                                $ruleValue = Meta::segment_name_from_id($ruleId);
                            }
                            if ($ruleType == 'Country') {
                                $ruleValue = Meta::country_name_from_id($ruleId);
                            }
                            if ($ruleType == 'Store') {
                                $ruleValue = Meta::store_name_from_id($ruleId);
                            }
                            if ($ruleType == 'Item') {
                                $ruleValue = Meta::item_name_from_id($ruleId);
                            }
                            if ($ruleType == 'Category') {
                                $ruleValue = Meta::category_name_from_id($ruleId);
                            }
                            if ($ruleType == 'Brand') {
                                $ruleValue = Meta::brand_name_from_id($ruleId);
                            }
                            if ($ruleType == 'Currency') {
                                $ruleValue = Meta::currency_name_from_id($ruleId);
                            }
                            if ($ruleType == 'Merchant Category Code') {
                                $ruleValue = Meta::mcc_name_from_id($ruleId);
                            }
                            if ($ruleType == 'Transaction Types') {
                                $ruleValue = Meta::trxtype_name_from_id($ruleId);
                            }
                            if($ruleType == 'Amount') {
                                $ruleValue = $ruleId . ' ' . Meta::currency_name_from_id($rule->currency_id);
                            }
                        }
                    ?>

                    <label>{{ $ruleType }}</label>
                    <option selected="selected">
                        @if (isset($ruleValue[0]) && !empty($ruleValue[0]))
                            {{ $ruleValue[0] }}
                        @else
                            {{ '' }}
                        @endif
                    </option>
                </select>
            </div>

            <div class="pure-form pure-form-stacked">
                <div class="pure-g block padding-top-10 padding-bottom-10">
                    <div class="pure-u-1-2">
                        <label class="radio-label block padding-bottom-10 loyaltytyexrulecur">
                            {{ Form::radio('type', 'Points') }} Amount
                        </label>

                        {{ Form::label('currency', 'Currency', array('for'=>'currency')) }}
                        {{ Form::select('currency_id', $currencies, $rule->currency_id, array('id' => 'currency_id', 'disabled' => 'disabled', 'class' => 'pure-input-2-3 loayltyexrulecur')) }}

                        {{ Form::label('original_reward', 'Amount to be rewarded') }}
                        {{ Form::text('original_reward', ($rule->type == 'Points' && $rule->original_reward) ? $rule->original_reward : '1.00', array('disabled' => 'disabled', 'class' => 'pure-input-1-2 required')) }}

                        {{ Form::label('reward_usd', 'USD') }}
                        {{ Form::text('reward_usd', ($rule->type == 'Points' && $rule->reward_usd) ? $rule->reward_usd : '0 USD', array('class' => 'pure-input-1-2', 'disabled' => 'disabled')) }}

                        {{ Form::label('reward_pts', 'Points to be Rewarded') }}
                        {{ Form::text('reward_pts', ($rule->type == 'Points' && $rule->reward_pts) ? $rule->reward_pts : '0 pts' , array('class' => 'pure-input-1-2', 'disabled' => 'disabled')) }}
                    </div>
                    <div class="pure-u-1-2">
                        <label class="radio-label block padding-bottom-10 loyaltytyexrulecur">
                            {{ Form::radio('type', 'Price') }} Preset Number of Points
                        </label>
                        {{ Form::label('reward_pts', 'Points') }}
                        {{ Form::text('reward_pts', ($rule->type == 'Price' && $rule->reward_pts) ? $rule->reward_pts : '0 pts', array('class' => 'pure-input-1-2', 'disabled' => 'disabled')) }}
                    </div>
                    <div class="pure-u-1-2 loayltyexruleevent">
                        <label class="radio-label block padding-bottom-10 loyaltytyexrulecur">
                            {{ Form::radio('type', 'Event') }} Event
                        </label>
                        {{ Form::label('original_reward', 'Events to be rewarded') }}
                        {{ Form::text('original_reward', ($rule->type == 'Event' && $rule->original_reward) ? $rule->original_reward : '1.00', array('class' => 'pure-input-1-2 required','disabled' => 'disabled')) }}

                        {{ Form::label('reward_pts', 'Event') }}
                        {{ Form::text('reward_pts', ($rule->type == 'Price' && $rule->reward_pts) ? $rule->reward_pts : '0 pts', array('class' => 'pure-input-1-2', 'disabled' => 'disabled')) }}
                    </div>
                </div>
            </div>

            <a href="#" style="width: 87%" class="pure-button pure-button-primary update-loyalty-rule" data-target="#rule-table">
                Update
            </a>

        </div>
    </div>
</div>