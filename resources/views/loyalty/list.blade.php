@extends('layouts.dashboard')

@section('title')
    Loyalty Programs | BLU
@stop

@section('body')
    <div class="padding">
        <div class="content-head pure-g">
            <div class="title pure-u-1-2">
                <h1><i class="fa fa-star fa-fw"></i> Loyalty Programs ({{ $programs->total() }})</h1>
            </div>
            <div class="controls pure-u-1-2">
                <form action="{{ Filter::searchUrl() }}" method="get">
                    <input type="text" placeholder="Search" name="q" value="{{ Input::get('q') }}" />
                    <button type="submit" title="" class="list-search"></button>
                </form>
                @if(I::can('create_loyalty'))
                    <a href="{{ url('dashboard/loyalty/create') }}" class="pure-button pure-button-primary">New Program</a>
                @endif
            </div>


            <div id="catalog-filter" class="pure-u-1-2">
                <input id="filter_route" type="hidden" value="loyalty">
                <form id="filter-catalogue" action="{{ Filter::searchUrl() }}" method="get">

                    <div class="filter-block filter-block-pricing-width">
                        <span class="filter-label">Partner:</span>
                        <select style="width:100%;" name="partner_loyalty_list[]" data-filter-catalogue="partner_loyalty_list" data-filter-catalogue-title="Select Partner" multiple="multiple">
                            @foreach ($lists['partnersList'] as $key => $value)
                                <option value="{{ $key }}">{{ $value }}</option>
                            @endforeach
                        </select>
                        <input data-filter-catalogue="partner_loyalty_list" type="hidden" value="{{ Input::get('partner_loyalty_list') }}">
                    </div>

                    <div class="filter-block filter-block-pricing-width">
                        <span class="filter-label">Network:</span>
                        <select style="width:100%;" name="network_loyalty_list[]" data-filter-catalogue="network_loyalty_list" data-filter-catalogue-title="Select Network" multiple="multiple">
                            @foreach ($lists['networks'] as $key => $value)
                                <option value="{{ $key }}">{{ $value }}</option>
                            @endforeach
                        </select>
                        <input data-filter-catalogue="network_loyalty_list" type="hidden" value="{{ Input::get('network_loyalty_list') }}">
                    </div>

                    <div class="submit-filter-block disable-width">
                        <div style="float:right;">
                            <a href="<?php echo e(url('dashboard/loyalty')); ?>" title="Clear Filters" class="pure-button pure-button-primary">
                                Clear Filters <i class="fa fa-ban fa-fw"></i>
                            </a>
                        </div>
                        <button class="pure-button pure-button-primary" type="submit" title="" style="float:right;margin-right:15px;">
                            Submit Filters
                        </button>
                    </div>
                </form>
            </div>
        </div>
		@if(!empty(Input::get('msg')))
				<h6><i class="fa fa-exclamation-triangle fa-fw"></i> {{ Input::get('msg') }}</h6>
		@endif
        <table class="pure-table">
            <thead>
                <tr>
                    <th width="50"><a href="{{ Filter::baseUrl('sort=id') }}">ID</a></th>
                    <th style="text-align:left;padding-left:20px;"><a href="{{ Filter::baseUrl('sort=name') }}">Name</a></th>
                    <th>Network</th>
                    <th><a href="{{ Filter::baseUrl('sort=partner_id') }}">Partner</a></th>
                    <th><a href="{{ Filter::baseUrl('sort=priority') }}">Rank</a></th>
                    <th><a href="{{ Filter::baseUrl('sort=created_at') }}">Created</a></th>
                    <th>Status</th>
                    <th>Country</th>
                    <th width="150">Actions</th>
                </tr>
            </thead>
            <tbody>
            @if(count($programs) > 0)
                @foreach ($programs as $p)
                    <tr>
                        <td>
                            {{ $p->id }}
                        </td>
                        <td style="text-align:left;padding-left:20px;">
                            {{{ $p->name }}}
                        </td>
                        <td>
                            @if($p->partner->firstNetwork())
                                {{{ $p->partner->firstNetwork()->name }}}
                            @else
                                N/A
                            @endif
                        </td>                        
                        <td>
                            {{{ $p->partner->name }}}
                        </td>
                        <td>
                            {{{ $p->priority }}}
                        </td>
                        <td>
                            {{{ date("M j, Y, g:i a", strtotime($p->created_at)) }}}
                        </td>                 
                        <td>
                            @if($p->status == 'Enabled')
                                <span class="ico-yes"></span>
                            @else
                                <span class="ico-no"></span>
                            @endif
                        </td>
                        <td>
                            @if($p->country)
                                {{{ $p->country->name }}}
                            @else
                                <i>N/A</i>
                            @endif
                        </td>
                        <td>
                            @if(I::can('view_loyalty'))
                                <a href="/dashboard/loyalty/edit/{{ base64_encode($p->id) }}" class="edit" title="View">View</a>
                            @endif
                            
                            @if(I::can('delete_loyalty'))
                                <a href="/dashboard/loyalty/delete/{{ base64_encode($p->id) }}" class="delete" title="Delete">Delete</a>
                            @endif
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="9">No Loyalty Programs Found</td>
                </tr>
            @endif
            </tbody>
        </table>

        {{ $programs->setPath('loyalty')->appends(Input::except('page'))->links() }}

    </div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-loyalty');
    </script>
@stop
