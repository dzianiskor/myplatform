@extends('layouts.dashboard')

@section('title')
    Loyalty | BLU
@stop
<style>
    .ui-autocomplete-loading {
        background:url(/img/loader.gif) no-repeat center right;
    }
    li.ui-menu-item{
        font-size:12px;
    }
</style>
@section('js_globals')
    var PROGRAM_ID = {{ $program->id }};
@stop
<?php
foreach ($countries as $c) {
    $partnerCountries[$c->id]	= $c->name;
}

//	foreach ($partners as $p){
//		if($p->id == $program->partner_id){
//			if(count($p->countries) > 0 ){
//			   foreach($p->countries as $c){
//				   $partnerCountries[$c->country_id] = $country[$c->country_id];
//			   }
//			}else{
//				   $partnerCountries = $country;
//			   }
//			}
//		}
?>
@section('dialogs')
    {{-- Priority --}}
    <div id="dialog-priority" style="width:300px;">
        <h4>Loyalty Program Ranking Order</h4>

        @if(count($programs) > 0)
            <form>
                <span class="info">Drag on program to re-order rank.</span>
            </form>

            <div class="spacer"></div>

            <ul class="sortable-list" data-action="/dashboard/loyalty/priorities">
                @foreach($programs as $p)
                    <li data-id="{{ $p->id }}"><span class="p">{{ $p->priority }}</span>{{{ $p->name }}}</li>
                @endforeach
            </ul>
            <a href="#" class="pure-button pure-button-primary save-sortable">
                Save
            </a>
        @else
            <p>No other loyalty programs available.</p>
        @endif
    </div>

    {{-- SMS Variables --}}
    <div id="dialog-sms-tags" style="width:500px;">
        <h4>Available SMS Variables</h4>
        <table class="pure-table" style="width:100%">
            <thead>
            <tr>
                <th style="text-align:left">Description</th>
                <th>Tag</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td class="padding-left-10 align-left">Member Title</td>
                <td><i>{title}</i></td>
            </tr>
            <tr>
                <td class="padding-left-10 align-left">Member First Name</td>
                <td><i>{first_name}</i></td>
            </tr>
            <tr>
                <td class="padding-left-10 align-left">Member Last Name</td>
                <td><i>{last_name}</i></td>
            </tr>
            <tr>
                <td class="padding-left-10 align-left">Points Redeemed/Rewarded</td>
                <td><i>{points}</i></td>
            </tr>
            <tr>
                <td class="padding-left-10 align-left">Points Balance</td>
                <td><i>{balance}</i></td>
            </tr>
            <tr>
                <td class="padding-left-10 align-left">Store/Outlet</td>
                <td><i>{store}</i></td>
            </tr>
            <tr>
                <td class="padding-left-10 align-left">Amouunt</td>
                <td><i>{amount}</i></td>
            </tr>
            <tr>
                <td class="padding-left-10 align-left">Date</td>
                <td><i>{date}</i></td>
            </tr>
            <tr>
                <td class="padding-left-10 align-left">Time</td>
                <td><i>{time}</i></td>
            </tr>
            </tbody>
        </table>
    </div>

    {{-- Rules History --}}
    <div id="dialog-rules-history" style="width:800px;">
        <h4>Available SMS Variables</h4>
        <table class="pure-table" style="width:100%">
            <thead>
            <tr>
                <th style="text-align:left">Description</th>
                <th>Tag</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td class="padding-left-10 align-left">Member Title</td>
                <td><i>{title}</i></td>
            </tr>
            <tr>
                <td class="padding-left-10 align-left">Member First Name</td>
                <td><i>{first_name}</i></td>
            </tr>
            <tr>
                <td class="padding-left-10 align-left">Member Last Name</td>
                <td><i>{last_name}</i></td>
            </tr>
            <tr>
                <td class="padding-left-10 align-left">Points Redeemed/Rewarded</td>
                <td><i>{points}</i></td>
            </tr>
            <tr>
                <td class="padding-left-10 align-left">Points Balance</td>
                <td><i>{balance}</i></td>
            </tr>
            <tr>
                <td class="padding-left-10 align-left">Store/Outlet</td>
                <td><i>{store}</i></td>
            </tr>
            </tbody>
        </table>
    </div>

    {{-- Exception Rules --}}
    <div id="dialog-add-rule" style="width:400px;">
        <h4>Add Exception Rule</h4>

        <div id="rule-error" style="display: none;"></div>

        {{ Form::open(array('url' => "#", 'class' => "form_z pure-form pure-form-stacked", "id" => 'form_status')) }}
        <select id="rule_status" class="pure-input-1 rule-status">
            <option value="Enable" selected="selected">Enable</option>
            <option value="Disable">Disable</option>
        </select>
        {{ Form::close() }}

        <a href="#dialog-new-type" class="btn-new-type top-bottom-space" title="Add another type">
            <i class="fa fa-plus-circle fa-fw"></i> Add another type
        </a>
        <div class="exception-type-wrapper">
            <div class="type-forms">
                {{ Form::open(array('url' => "#", 'class' => "form_z pure-form pure-form-stacked", "id" => 'form_z')) }}
                <select name="rule_operator" id="rule_operator" class="pure-input-1 rule-operator">
                    <option data-index="0" value="and">AND</option>
                </select>
                {{ Form::close() }}

                {{ Form::open(array('url' => "#", 'class' => "pure-form")) }}
                <select name="rule-item" id="rule-item" class="pure-input-1 rule-item">
                    <option selected="selected" disabled>Select Criteria</option>
                    @foreach(Meta::loyaltyExceptionRuleTypes() as $k => $v)
                        @if ($v == 'Brand' || $v == 'Amount')
                            <option style="display: none;" data-index=".form_{{ $k }}" value="{{ $v }}">{{ $v }}</option>
                        @else
                            <option data-index=".form_{{ $k }}" value="{{ $v }}">{{ $v }}</option>
                        @endif
                    @endforeach
                </select>
                {{ Form::close() }}

                <div class="property-forms">
                    {{ Form::open(array('url' => "#", 'class' => "form_x pure-form pure-form-stacked", "id" => 'form_x')) }}
                    <select name="rule_comparison" id="rule_comparison" class="pure-input-1 rule-comparison">
                        <option value="" selected="selected" disabled>Select Comparison</option>
                        <option data-index="0" value="equal">=</option>
                        <option data-index="1" value="different">!=</option>
                        {{--<option data-index="2" value="less_than">&lt;</option>--}}
                        {{--<option data-index="3" value="greater_than">&gt;</option>--}}
                        {{--<option data-index="4" value="less_or_equal">&lt;=</option>--}}
                        {{--<option data-index="5" value="greater_or_equal">&gt;=</option>--}}
                    </select>
                    {{ Form::close() }}

                    {{--- Segment Match ---}}
                    {{ Form::open(array('url' => "#", 'class' => "form_0 pure-form pure-form-stacked", "id" => 'form_0')) }}
                    {{ Form::label('segment', 'Segment Matches') }}
                    <select name="rule_value" id="rule_value" class="pure-input-1">
                        <option selected="selected" disabled>Select Segment</option>
                        @foreach($segments as $s)
                            <option data-index="{{ $s->id }}" value="{{ $s->id }}">{{ $s->name }}</option>
                        @endforeach
                    </select>
                    {{ Form::close() }}
                    {{--- /Segment Match ---}}

                    {{--- Country Match ---}}
                    {{ Form::open(array('url' => "#", 'class' => "form_1 pure-form pure-form-stacked", "id" => 'form_1')) }}

                    {{ Form::label('rule_value', 'Country Matches') }}
                    <select name="rule_value" id="rule_value" class="pure-input-1">
                        <option selected="selected" disabled>Select Country</option>
                        @foreach($partnerCountries as $countryId => $countryName)
                            <option data-index="{{ $countryId }}" value="{{ $countryId }}">{{ $countryName }}</option>
                        @endforeach
                    </select>

                    {{ Form::close() }}
                    {{--- /Country Match ---}}

                    {{--- Store Match ---}}
                    {{ Form::open(array('url' => "#", 'class' => "form_2 pure-form pure-form-stacked", "id" => 'form_2')) }}
                    {{ Form::label('rule_value', 'Store Matches') }}
                    <select name="rule_value" id="rule_value" class="pure-input-1">
                        <option selected="selected" disabled>Select Store</option>
                        @foreach($stores as $s)
                            <option data-index="{{ $s->id }}" value="{{ $s->id }}">{{ $s->name }}</option>
                        @endforeach
                    </select>

                    {{ Form::close() }}
                    {{--- /Store Match ---}}

                    {{--- Item Match ---}}
                    {{ Form::open(array('url' => "#", 'class' => "form_3 pure-form pure-form-stacked", "id" => 'form_3')) }}

                    {{ Form::label('rule_value', 'Item Search') }}
                    <input type="text" id="items-autocomplete" class="items-autocomplete inline-block-display" placeholder="Search" />
                    <button id="items-search" class="items-search rounded-search">search</button>
                    <select name="rule_value" id="rule_value_items" class="pure-input-1 rule_value_items">
                        <option selected="selected" disabled>Select Item</option>
                        @foreach($items as $s)
                            <option data-index="{{ $s->id }}" value="{{ $s->id }}">{{ $s->name }}</option>
                        @endforeach
                    </select>

                    {{ Form::close() }}
                    {{--- /Item Match ---}}

                    {{--- Category Match ---}}
                    {{ Form::open(array('url' => "#", 'class' => "form_4 pure-form pure-form-stacked", "id" => 'form_4')) }}

                    {{ Form::label('rule_value', 'Category Matches') }}
                    <select name="rule_value" id="rule_value" class="pure-input-1">
                        <option selected="selected" disabled>Select Category</option>
                        @foreach($categories as $s)
                            <option data-index="{{ $s->id }}" value="{{ $s->id }}">{{ $s->name }}</option>
                        @endforeach
                    </select>

                    {{ Form::close() }}
                    {{--- /Category Match ---}}

                    {{--- Brand Match ---}}
                    {{ Form::open(array('url' => "#", 'class' => "form_5 pure-form pure-form-stacked", "id" => 'form_5')) }}

                    {{ Form::label('rule_value', 'Brand Matches') }}
                    <select name="rule_value" id="rule_value" class="pure-input-1">
                        <option selected="selected" disabled>Select Brand</option>
                        @foreach($brands as $s)
                            <option data-index="{{ $s->id }}" value="{{ $s->id }}">{{ $s->name }}</option>
                        @endforeach
                    </select>

                    {{ Form::close() }}
                    {{--- /Brand Match ---}}

                    {{--- Currency Match ---}}
                    {{ Form::open(array('url' => "#", 'class' => "form_6 pure-form pure-form-stacked", "id" => 'form_6')) }}
                    {{ Form::label('rule_value', 'Currency Matches') }}
                    <select name="rule_value" id="rule_value" class="pure-input-1">
                        <option selected="selected" disabled>Select Currency</option>
                        @foreach($currencies as $k => $c)
                            <option data-index="{{ $k }}" value="{{ $k }}">{{ $c }}</option>
                        @endforeach
                    </select>

                    {{ Form::close() }}
                    {{--- /Currency Match ---}}

                    {{--- MCC Match ---}}
                    {{ Form::open(array('url' => "#", 'class' => "form_7 pure-form pure-form-stacked", "id" => 'form_7')) }}
                    {{ Form::label('rule_value', 'MCC Matches') }}
                    <input type="text" id="mcc-autocomplete" class="mcc-autocomplete inline-block-display" placeholder="Search MCC" />
                    <button id="mcc-search" class="mcc-search rounded-search">search</button>
                    <select name="rule_value" id="rule_value_items" class="pure-input-1 rule_value_items">

                    </select>

                    {{ Form::close() }}
                    {{--- /MCC Match ---}}

                    {{--- Transaction Type Match ---}}
                    {{ Form::open(array('url' => "#", 'class' => "form_8 pure-form pure-form-stacked", "id" => 'form_8')) }}
                    {{ Form::label('rule_value', 'Transaction Type Matches') }}
                    <input type="text" id="trxType-autocomplete" class="trxType-autocomplete inline-block-display" placeholder="Search Transaction Types" />
                    <button id="trxType-search" class="trxType-search rounded-search">search</button>
                    <select name="rule_value" id="rule_value_trxType" class="pure-input-1 rule_value_trxType">

                    </select>

                    {{ Form::close() }}
                    {{--- /Transaction Type Match ---}}

                    {{--- Amount Match ---}}
                    {{ Form::open(array('url' => "#", 'class' => "form_9 pure-form pure-form-stacked", "id" => 'form_9')) }}
                    {{ Form::label('rule_value', 'Amount Matches') }}
                    <input type="text" name="rule_value_amount" id="rule_value_amount" class=" inline-block-display" placeholder="Amount" />
                    {{ Form::select('rule_value_currency', $currencies, '', array('id' => 'rule_value_currency', 'class' => 'pure-input-2-3')) }}
                    {{ Form::close() }}
                    {{--- /Amount Type Match ---}}

                </div>
            </div>
        </div>

        {{ Form::open(array('url' => "/dashboard/loyalty/rule/".base64_encode($program->id), 'class' => "pure-form pure-form-stacked form_hidden_attributes")) }}

        {{ Form::hidden('rule_operator', 'and') }}
        {{ Form::hidden('rule_type', null) }}
        {{ Form::hidden('rule_status', null) }}
        {{ Form::hidden('rule_comparison', null) }}
        {{ Form::hidden('rule_value', null) }}
        {{ Form::hidden('rule_value_currency') }}

        <div class="pure-g block padding-top-10 padding-bottom-10">
            <div class="pure-u-1-2">
                <label class="radio-label block padding-bottom-10 loyaltytyexrulecur">
                    {{ Form::radio('type', 'Points', true) }} Amount
                </label>
                <?php
                $ruleCurrencyId	= 6;
                ?>
                {{ Form::label('currency', 'Currency', array('for'=>'currency')) }}
                {{ Form::select('currency_id', $currencies, $ruleCurrencyId, array('id' => 'currency_id', 'class' => 'pure-input-2-3 loayltyexrulecur')) }}

                {{ Form::label('original_reward', 'Amount to be rewarded') }}
                {{ Form::text('original_reward', '', array('class' => 'pure-input-1-2 required', 'placeholder' => '1.00')) }}



                {{ Form::label('reward_usd', 'USD') }}
                {{ Form::text('reward_usd', '', array('class' => 'pure-input-1-2', 'placeholder' => '0 USD', 'disabled' => 'disabled')) }}
                {{ Form::label('reward_pts', 'Points to be Rewarded') }}
                {{ Form::text('reward_pts', '', array('class' => 'pure-input-1-2', 'placeholder' => '0 pts')) }}
            </div>
            <div class="pure-u-1-2">
                <label class="radio-label block padding-bottom-10 loyaltytyexrulecur">
                    {{ Form::radio('type', 'Price') }} Preset Number of Points
                </label>
                {{ Form::label('reward_pts', 'Points') }}
                {{ Form::text('reward_pts', null, array('class' => 'pure-input-1-2', 'disabled' => 'disabled', 'placeholder' => '0 pts')) }}
            </div>
            <div class="pure-u-1-2 loayltyexruleevent">
                <label class="radio-label block padding-bottom-10 loyaltytyexrulecur">
                    {{ Form::radio('type', 'Event') }} Event
                </label>
                {{ Form::label('original_reward', 'Events to be rewarded') }}
                {{ Form::text('original_reward', '', array('class' => 'pure-input-1-2 required','disabled' => 'disabled', 'placeholder' => '1.00')) }}
                {{ Form::label('reward_pts', 'Points') }}
                {{ Form::text('reward_pts', null, array('class' => 'pure-input-1-2', 'disabled' => 'disabled', 'placeholder' => '0 pts')) }}
            </div>
        </div>
        {{ Form::close() }}

        <a href="#" class="pure-button pure-button-primary basic-action" data-target="#rule-table">
            Save
        </a>
    </div>

    {{-- Cashback Eligible Product --}}
    <div id="dialog-add-cashback" style="width:400px;">
        <h4>Add Cashback Eligible Product</h4>
        {{--- Item Match ---}}
        {{ Form::open(array('url' => "#", 'class' => "pure-form", "id" => 'formcashback')) }}

        {{ Form::label('product_id', 'Item Search') }}
            <input type="text" id="items-autocomplete" class="items-autocomplete inline-block-display" placeholder="Search" />

            <button id="items-search-cashback" class="items-search-cashback rounded-search">search</button>

            <select name="rule_value_items_cashback" id="rule_value_items_cashback" class="pure-input-1 rule_value_items"> </select>

        {{ Form::close() }}
        {{--- /Item Match ---}}

        {{ Form::open(array('url' => "/dashboard/loyalty/cashbackeligible/".base64_encode($program->id), 'class' => "pure-form pure-form-stacked form_hidden_attributes_cashback")) }}

        {{ Form::hidden('rule_value_items', null) }}

        {{ Form::close() }}
        <a href="#" class="pure-button pure-button-primary basic-action-cashback" data-target="#cashback-table">
            Save
        </a>
    </div>
@stop

@section('body')
    <div class="padding">

        <div class="content-head pure-g">
            <div class="title pure-u-1-2">
                @if($program->draft)
                    <h1><i class="fa fa-star fa-fw"></i>New Loyalty Program</h1>
                @else
                    <h1><i class="fa fa-star fa-fw"></i>Loyalty Program &raquo; {{{ $program->name }}}</h1>
                @endif
            </div>

            <div class="controls pure-u-1-2">
                <a href="{{ url('dashboard/loyalty') }}" class="pure-button">All Programs</a>
            </div>
        </div>

        {{ Form::model($program, array('url'   => '/dashboard/loyalty/update/'.base64_encode($program->id),
                                       'class' => 'pure-form pure-form-stacked form-width main-form',
                                       'id'    => 'loyaltyForm')) }}

        @include('partials.errors')

        {!! Form::hidden('redirect_to', URL::previous()) !!}

        <fieldset>
            <legend>
                Basic Details

                <span class="right">
                        <a href="#dialog-priority" class="custom-dialog" style="text-decoration:none;"><i class="fa fa-list-ol"></i> Current Rank [<span id="priority-num">{{ $program->priority }}</span>]</a>
                    </span>
            </legend>

            {{ Form::label('partner_id', 'Partner') }}
            <select name="partner_id" class="pure-input-1-3">
                <option disabled="disabled">Select Partner</option>
                @foreach($partners as $p)
                    @if($p->id == $program->partner_id)
                        <option value="{{ $p->id }}" selected>{{ $p->name }}</option>
                    @else
                        <option value="{{ $p->id }}">{{ $p->name }}</option>
                    @endif
                @endforeach
            </select>

            {{ Form::label('name', 'Program Name') }}
            {{ Form::text('name', $program->name, array('class' => 'pure-input-1 required')) }}

            {{ Form::label('description', 'Description') }}
            {{ Form::textarea('description', $program->description, array('class' => 'pure-input-1')) }}

            {{ Form::label('status', 'Status') }}

            <select name="status" class="pure-input-1-3">
                @foreach(Meta::loyaltyStatuses() as $status)
                    @if($program->status == $status)
                        <option value="{{ $status }}" selected>{{ $status }}</option>
                    @else
                        <option value="{{ $status }}">{{ $status }}</option>
                    @endif
                @endforeach
            </select>
        </fieldset>

        <fieldset>
            <legend>Validity Period</legend>

            <div class="pure-g">
                <div class="pure-u-1-2">
                    {{ Form::label('valid_from', 'Valid From') }}
                    {{ Form::text('valid_from', $program->valid_from, array('class' => 'pure-input-1-3 required datepicker-year')) }}
                </div>

                <div class="pure-u-1-2">
                    {{ Form::label('valid_from_time', 'Valid From Time') }}
                    {{ Form::text('valid_from_time', $program->valid_from_time, array('class' => 'pure-input-1-3 required timepicker')) }}
                </div>
            </div>

            <div class="pure-g">
                <div class="pure-u-1-2">
                    {{ Form::label('valid_to', 'Valid To') }}
                    {{ Form::text('valid_to', $program->valid_to, array('class' => 'pure-input-1-3 required datepicker-year')) }}
                </div>

                <div class="pure-u-1-2">
                    {{ Form::label('valid_to_time', 'Valid To Time') }}
                    {{ Form::text('valid_to_time', $program->valid_to_time, array('class' => 'pure-input-1-3 required timepicker')) }}
                </div>
            </div>
            <div class="pure-g">
                <div class="pure-u-1">
                    <?php $cashbackValidity = (empty($program->cashback_validity) ? '12' : $program->cashback_validity);?>
                    {{ Form::label('cashback_validity', 'Cashback Validity (in Months)') }}
                    {{ Form::text('cashback_validity', $cashbackValidity, array('class' => 'pure-input-1-3 required')) }}
                </div>
            </div>
        </fieldset>

        <fieldset>
            <legend>SMS Details</legend>

            <span class="info">
                    If you do not wish to send SMS's notifications, please leave these fields blank.
                </span>

            {{ Form::label('reward_sms', 'Reward SMS') }}
            {{ Form::text('reward_sms', $program->reward_sms, array('class' => 'pure-input-1 dynamic-off-state')) }}

            {{ Form::label('redemption_sms', 'Redemption SMS') }}
            {{ Form::text('redemption_sms', $program->redemption_sms, array('class' => 'pure-input-1 dynamic-off-state')) }}

            <span class="inline-help">
                    <a href="#dialog-sms-tags" class="dialog">View the available variable tags</a> that can be used in your SMS text.
                </span>
        </fieldset>

        <fieldset>
            <legend>Default Rule (Points System)</legend>

            <span class="info">
                    For every $<span class="x">X</span> that is spent, <span class="y">Y points are</span> awarded.
                </span>
            <div class="pure-g">
                <div class="pure-u-1-2">
                    <div class="padding-right-10">
                        {{ Form::label('original_reward', 'Amount to be rewarded') }}
                        {{ Form::text('original_reward', $program->original_reward, array('class' => 'pure-input-1-2 required')) }}
                    </div>
                </div>

                <div class="pure-u-1-2">
                    <?php if(empty($program->currency_id)){
                        $programCurrencyId	= 6;
                    }
                    else{
                        $programCurrencyId	= $program->currency_id;
                    }
                    ?>
                    {{ Form::label('currency', 'Currency', array('for'=>'currency')) }}
                    {{ Form::select('currency_id', $currencies, $programCurrencyId, array('id' => 'currency_id', 'class' => 'pure-input-1-2')) }}
                </div>

                <div class="pure-u-1-2">
                    <div class="padding-right-10">
                        {{ Form::label('reward_usd', 'USD') }}
                        {{ Form::text('reward_usd', $program->reward_usd, array('class' => 'pure-input-1-2', 'disabled' => 'disabled')) }}
                    </div>
                </div>
                <div class="pure-u-1-2">
                    {{ Form::label('reward_pts', 'Points to be Rewarded') }}
                    {{ Form::text('reward_pts', number_format($program->reward_pts, 0), array('class' => 'pure-input-1-2 required')) }}
                </div>
            </div>
        </fieldset>

        <fieldset>
            <legend>
                Exception Rules

                @if($program->draft)
                    @if(I::can('create_loyalty'))
                        <span class="right">
                                <a href="#dialog-add-rule" class="dialog">Add Rule</a>
                            </span>
                    @endif
                @else
                    @if(I::can('edit_loyalty'))
                        <span class="right">
                                <a href="#dialog-add-rule" class="dialog">Add Rule</a>
                            </span>
                    @endif
                @endif
            </legend>

            <table id="rule-table" class="pure-table">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Type</th>
                    <th>Points</th>
                    <th>Reward</th>
                    <th>Currency</th>
                    <th>For</th>
                    <th>Matching</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @if($program->rules->count() == 0)
                    <tr class="empty">
                        <td colspan="9">No Exception Rules Found</td>
                    </tr>
                @else
                    @foreach($program->rules as $rule)
                    <?php
                        if ($rule->deleted == 1) {
                            continue;
                        }

                        $ruleType = array();
                        $rulevalue = array();
                        $exception_rules_obj = App\LoyaltyExceptionrule::where('loyalty_rule_id', $rule->id)->get();
                        if(!empty($exception_rules_obj)){
                            foreach ($exception_rules_obj as $exception_rule_obj){
                                $ruleType[] = $exception_rule_obj->type;
//                                        $rulevalue[] = $exception_rule_obj->rule_value;

                                if($exception_rule_obj->type == 'Segment')
                                    $rulevalue[] = Meta::segment_name_from_id($exception_rule_obj->rule_value);

                                if($exception_rule_obj->type == 'Country')
                                    $rulevalue[] = Meta::country_name_from_id($exception_rule_obj->rule_value);

                                if($exception_rule_obj->type == 'Store')
                                    $rulevalue[] = Meta::store_name_from_id($exception_rule_obj->rule_value);

                                if($exception_rule_obj->type == 'Item')
                                    $rulevalue[] = Meta::item_name_from_id($exception_rule_obj->rule_value);

                                if($exception_rule_obj->type == 'Category')
                                    $rulevalue[] = Meta::category_name_from_id($exception_rule_obj->rule_value);

                                if($exception_rule_obj->type == 'Brand')
                                    $rulevalue[] = Meta::brand_name_from_id($exception_rule_obj->rule_value);

                                if($exception_rule_obj->type == 'Currency')
                                    $rulevalue[] = Meta::currency_name_from_id($exception_rule_obj->rule_value);

                                if($exception_rule_obj->type == 'Merchant Category Code')
                                    $rulevalue[] = Meta::mcc_name_from_id($exception_rule_obj->rule_value);

                                if($exception_rule_obj->type == 'Transaction Types')
                                    $rulevalue[] = Meta::trxtype_name_from_id($exception_rule_obj->rule_value);

                                if($exception_rule_obj->type == 'Amount')
                                    $rulevalue[] = $exception_rule_obj->rule_value . ' ' . Meta::currency_name_from_id($exception_rule_obj->rule_value_currency);
                            }
                            $ruleType = implode(', ', $ruleType);
                            $ruleValue = implode(', ', $rulevalue);
                        } else {
                            $ruleType = $rule->rule_type;
                            if($rule->type == 'Segment')
                                $ruleValue = Meta::segment_name_from_id($ruleType->rule_value);

                            if($rule->type == 'Country')
                                $ruleValue = Meta::country_name_from_id($rule->rule_value);

                            if($rule->type == 'Store')
                                $ruleValue = Meta::store_name_from_id($rule->rule_value);

                            if($rule->type == 'Item')
                                $ruleValue = Meta::item_name_from_id($rule->rule_value);

                            if($rule->type == 'Category')
                                $ruleValue = Meta::category_name_from_id($rule->rule_value);

                            if($rule->type == 'Brand')
                                $ruleValue = Meta::brand_name_from_id($rule->rule_value);

                            if($rule->type == 'Currency')
                                $ruleValue = Meta::currency_name_from_id($rule->rule_value);

                            if($rule->type == 'Merchant Category Code')
                                $ruleValue = Meta::mcc_name_from_id($rule->rule_value);

                            if($rule->type == 'Transaction Types')
                                $ruleValue = Meta::trxtype_name_from_id($rule->rule_value);

                            if($rule->type == 'Amount')
                                $rulevalue = $rule->rule_value . ' ' . Meta::currency_name_from_id($rule->rule_value_currency);

                        }
                        if(empty($ruleType)){
                            $ruleType = $rule->rule_type;
                        }
                        if(empty($ruleValue)){
                            $ruleValue = $rule->rule_value;
                        }
                        ?>
                        <tr>
                            <td>
                                <?php echo $rule->id;?>
                            </td>
                            <td>
                                @if($rule->type == 'Points')
                                    Point System
                                @elseif($rule->type == 'Event')
                                    Event
                                @else
                                    Preset
                                @endif
                            </td>
                            <td>{{ $rule->reward_pts }}</td>
                            <?php
                            $ruleCurrecyId		= !empty($rule->currency_id) ? $rule->currency_id : 6;
                            $ruleCurrency		= App\Currency::find($ruleCurrecyId);
                            $ruleCurrencyCode	= $ruleCurrency->short_code;
                            $rewardValue		= !empty($rule->original_reward)?$rule->original_reward:$rule->reward_usd;
                            ?>
                            <td>{{ $rewardValue }}</td>
                            <td>
                                @if($rule->type !== 'Event')
                                    {{ $ruleCurrencyCode }}
                                @endif
                            </td>
                            <td>
                                {{ $ruleType }}
                            </td>
                            <td>
                                {{ $ruleValue }}
                            </td>
                            <td>
                                @if($rule->status == 'Enable')
                                    <span id="rule_id_{{ $rule->id }}" class="ico-yes"></span>
                                @else
                                    <span id="rule_id_{{ $rule->id }}" class="ico-no"></span>
                                @endif
                            </td>
                            <td>
                                @if (I::can('edit_loyalty'))
                                    <a href="{{ url('dashboard/loyalty/editrule/' . base64_encode($rule->id)) }}" class="edit-loyalty-rule">Edit</a>
                                @endif
                                @if (I::can('delete_loyalty'))
                                    <a href="/dashboard/loyalty/unlink/{{ base64_encode($rule->id) }}" class="delete-rule">Remove</a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
            <div class="form-buttons">
                <div class="right">
                    <a href="#dialog-rules-history" class="pure-button show-rules-history">Rules History</a>
                </div>
            </div>
        </fieldset>

        <fieldset>
            <legend>
                Cashback Eligible Products
                @if($program->draft)
                    @if(I::can('create_loyalty'))
                        <span class="right">
                            <a href="#dialog-add-cashback" class="dialog">Add Cashback</a>
                        </span>
                    @endif
                @else
                    @if(I::can('edit_loyalty'))
                        <span class="right">
                            <a href="#dialog-add-cashback" class="dialog">Add Cashback</a>
                        </span>
                    @endif
                @endif
            </legend>

            <table id="cashback-table" class="pure-table">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Product</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @if($program->cashbacks->count() == 0)
                    <tr class="empty">
                        <td colspan="7">No Cashback Eligible Products Found</td>
                    </tr>
                @else
                    @foreach($program->cashbacks as $cashback)
                        <?php
                            $product = App\Product::find($cashback->product_id);
                            if(!$product){
                                continue;
                            }
                        ?>
                        <tr>
                            <td>
                                {{ $cashback->id }}
                            </td>
                            <td>{{ $product->name }}</td>
                            <td>
                                @if(I::can('delete_loyalty'))
                                    <a href="/dashboard/loyalty/unlinkcashback/{{ base64_encode($cashback->id) }}" class="delete-cashback">Remove</a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </fieldset>

        {{--- Form Buttons ---}}
        <div class="form-buttons">
            <div class="left">
                <a href="{{ url('dashboard/loyalty') }}" class="pure-button">Cancel</a>
            </div>

            @if($program->draft)
                @if(I::can('create_loyalty'))
                    <div class="right">
                        <button type="submit" class="pure-button pure-button-primary">Create Program</button>
                    </div>
                @endif
            @else
                @if(I::can('edit_loyalty'))
                    <div class="right">
                        <button type="submit" class="pure-button pure-button-primary">Save Program</button>
                    </div>
                @endif
            @endif

            <div class="clearfix"></div>
        </div>
        {{--- /Form Buttons ---}}

        {{ Form::close() }}
    </div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-loyalty');

        $(document).on('click', '.show-rules-history', function () {
            $.get('/dashboard/loyalty/rules-history/{{ base64_encode($program->id) }}', function(resp){
                $('#dialog-rules-history').html(resp);

                showDialog($('#dialog-rules-history'));
            });

            return false;
        });

        // Save the exceptional rule
        $(document).on('click', '#dialog-container a.basic-action', function(e){
            e.preventDefault();

            if($('#dialog-container input[name=rule_type]').val() == '') {
                alert("Please select a rule type before saving.");
                return false;
            }

            _ruleType = $('#dialog-container #rule-item option:selected').text();
            _ruleStatus = $('#rule_status option:selected').first().text();

            //            if($('#dialog-container input[name=rule_value]').val() == '') {
            if($('#rule_value').val() == '') {
                alert("Please select a " + _ruleType + " before saving.");
                return false;
            }

            var _form = $('#dialog-container form.form_hidden_attributes');

            $('input[name=rule_status]').val(_ruleStatus);

            $.post(_form.attr('action'), _form.serialize(), function(resp){
                console.log(resp);
                $('#rule-table tbody').append(resp);

                $('#rule-table tbody tr.empty').hide();

                closeDialog();
            });
        });

        // Edit Store
        $(document).on('click', 'a.edit-loyalty-rule', function(e){
            e.preventDefault();

            _this = $(this);

            $.get(_this.attr('href'), function(html) {
                showDialogWithContents(html, function(){

                });
            });
        });

        // Update exceptional rule status
        $(document).on('click', '#dialog-container a.update-loyalty-rule', function(e){
            e.preventDefault();

            var _form = $('#dialog-container form.form_hidden_attributes');
            _ruleStatus = $('#rule_status option:selected').text();

            $('input[name=rule_status]').val(_ruleStatus);

            $.post(_form.attr('action'), _form.serialize(), function(resp){
                if (!resp.failed) {
                    if (resp.status == 'Enable') {
                        $('#rule_id_' + resp.id).attr('class', 'ico-yes');
                    } else {
                        $('#rule_id_' + resp.id).attr('class', 'ico-no');
                    }
                }
                closeDialog();
            });
        });

        $(document).on('click', '#dialog-container a.basic-action-cashback', function(e){
            e.preventDefault();

            var productId = $('#rule_value_items_cashback').val();
            $('input[name=rule_value_items]').val(productId);

            var _form = $('#dialog-container form.form_hidden_attributes_cashback');
            $.post(_form.attr('action'), _form.serialize(), function(resp) {
                $('#cashback-table tbody').append(resp);

                $('#cashback-table tbody tr.empty').hide();

                closeDialog();
            });
        });

        $(document).on('click', '.items-search-cashback', function(event) {
            if($(this).parent().find(".items-autocomplete").val().length>=3) {
                event.preventDefault();

                $('#rule_value_items_cashback').empty();

                $.get('/dashboard/loyalty/products?term='+$(this).parent().find('.items-autocomplete').val(), function(resp){
                    $.each(resp, function(index, value){
                        $('<option>').val(value.id).text(value.value).appendTo('#rule_value_items_cashback');
                    });
                });

                return false;
            } else {
                event.preventDefault();
                return false;
            }
        });

        // Set the rule value in the post form
        $(document).on('change', 'select#rule_value', function() {
            if(valueArray.length <= 1){
                valueArray[0] = $(this).val();
                $('input[name=rule_value]').val(JSON.stringify(valueArray));
            }
        });

        $(document).on('change', 'select#rule_value_items', function() {
            if(valueArray.length <= 1){
                valueArray[0] = $(this).val();
                $('input[name=rule_value]').val(JSON.stringify(valueArray));
            }
        });

        // Set the rule comparison in the post form
        $(document).on('change', 'select#rule_comparison', function() {
            if(comparisonArray.length <= 1){
                comparisonArray[0] = $(this).val();
                $('input[name=rule_comparison]').val(JSON.stringify(comparisonArray));
            }
        });

        // Set the rule value in the post form
        $(document).on('change', 'select#currency_id', function() {
            $('input[name=currency_id]').val($(this).val());
        });

        // Enable/disable section
        $(document).on('change', 'form.form_hidden_attributes input[type=radio]', function() {
            $(this).closest('div.pure-g').find('input[type=text]').attr('disabled', true);
            $(this).closest('div.pure-g').find('select').attr('disabled', true);
            $(this).closest('div.pure-u-1-2').find('input[type=text]').attr('disabled', false);
            $(this).closest('div.pure-u-1-2').find('select').attr('disabled', false);
            $(this).closest('div.pure-g').find('#reward_usd').attr('disabled', true);
        });

        // Delete the exception rule
        $(document).on('click', 'a.delete-rule', function(e) {
            e.preventDefault();

            var _this = $(this);

            if(confirm("Are you sure you want to delete this item?")) {
                $.get($(this).attr('href'), function(){
                    _this.closest('tr').remove();
                });
            }
        });

        // Delete the cashback eligible item
        $(document).on('click', 'a.delete-cashback', function(e) {
            e.preventDefault();

            var _this = $(this);

            if(confirm("Are you sure you want to delete this item?")) {
                $.get($(this).attr('href'), function(){
                    _this.closest('tr').remove();
                });
            }
        });

        // Change the inline info dialog contents
        $(document).on('keyup', 'form.main-form input#reward_usd', function() {
            var val = $(this).val();

            if(!val || val == 0) {
                val = 'X';
            }

            $('span.x').html(val);
        });

        $(document).on('keyup', 'form.main-form input#reward_pts', function() {
            var val = $(this).val();

            if(!val || val == 0) {
                val = 'Y points are';
            }

            if(val == 1) {
                val = '1 point is';
            }

            if(val > 1) {
                val = val + ' points are';
            }

            $('span.y').html(val);
        });

        // Handle the sortable priorities
        $(document).on('click', 'a.custom-dialog', function(e){
            e.preventDefault();

            showDialog($(this).attr('href'));

            $('.sortable-list').sortable({
                update: function(event, ui) {
                    $('.sortable-list li').each(function(){
                        $(this).find('span.p').html($(this).index() + 1);
                    });
                }
            }).disableSelection();
        });

        // Update the prioritization order
        $(document).on('click', 'a.save-sortable', function(e){
            e.preventDefault();

            var buffer = [];

            $('#dialog-container ul.sortable-list li').each(function() {
                // Update display of current loyalty program priority
                if($(this).data('id') == PROGRAM_ID) {
                    $('span#priority-num').html($(this).find('span.p').html());
                }

                buffer.push({
                    id: $(this).data('id'),
                    p: $(this).find('span.p').html()
                });
            });

            $.post('/dashboard/loyalty/prioritize', {sort: buffer}, function(resp){
                closeDialog();
            });
        });

    </script>
@stop
