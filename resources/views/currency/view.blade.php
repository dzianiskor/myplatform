@extends('layouts.dashboard')

@section('title')
    Update Currency | BLU
@stop

@section('body')
    <div class="padding">

        <div class="content-head pure-g">
            <div class="title pure-u-1-2">
                <h1><i class="fa fa-money fa-fw"></i> Currency &raquo; {{{ $currency->name }}}</h1>
            </div>

            <div class="controls pure-u-1-2">
                <a href="{{ url('/dashboard/currencies') }}" class="pure-button">All Currencies</a>
            </div>
        </div>

        {{ Form::model($currency, array('url'   => '/dashboard/currencies/update/'.base64_encode($currency->id),
                                        'class' => 'pure-form pure-form-stacked form-width')) }}

            @include('partials.errors')                                       
            @include('currency/form', array('currency' => $currency))

            {!! Form::hidden('redirect_to', URL::previous()) !!}

            <!-- Form Buttons -->
            <div class="form-buttons">
                <div class="left">
                    <a href="{{ url('dashboard/currencies') }}" class="pure-button">Cancel</a>
                </div>
                @if($currency->draft)
                    @if(I::can('create_currencies'))
                        <div class="right">
                            <button type="submit" class="pure-button pure-button-primary">Create Currency</button>
                        </div>
                    @endif
                @else
                    @if(I::can('edit_currencies'))
                        <div class="right">
                            <button type="submit" class="pure-button pure-button-primary">Update Currency</button>
                        </div>
                    @endif
                @endif
                <div class="clearfix"></div>
            </div>
            <!-- /Form Buttons -->
        {{ Form::close() }}       
        
        
        {{--- Currency translations ---}}
        <fieldset>
            <legend>Currency Translation</legend>

            <table id="currencytranslation-list" class="pure-table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Currency Translation Name</th>
                    <th>Language</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @if(count($currency->currencyTranslations) > 0)
                    @foreach($currency->currencyTranslations as $currencyTranslation)
                        <tr>
                            <td>{{ $currencyTranslation->id }}</td>
                            @if($currencyTranslation->draft)
                                <td>{{ $currencyTranslation->name }} (Draft)</td>
                            @else
                                <td>{{ $currencyTranslation->name }}</td>
                            @endif
                            <td>
                                <?php
                                $lang = App\Language::where('id', $currencyTranslation->lang_id)->first();

                                echo $lang->name;
                                ?>
                            </td>
                            <td>
                                @if(!I::can('delete_currencies') && !I::can('edit_currencies'))
                                    N/A
                                @endif
                                @if(I::can('edit_currencies'))
                                    <a href="{{ url('dashboard/currencytranslation/edit/'.base64_encode($currencyTranslation->id)) }}" class="edit-currencytranslation" title="Edit">Edit</a>
                                @endif
                                @if(I::can('delete_currencies'))
                                    <a href="{{ url('dashboard/currencytranslation/delete/'.base64_encode($currencyTranslation->id)) }}" class="delete-currencytranslation" title="Delete">Delete</a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr class="empty">
                        <td colspan="4">No Currency translation defined</td>
                    </tr>
                @endif
                </tbody>
            </table>
            @if($currency->draft)
                @if(I::can('create_currencies'))
                    <a href="#dialog-new-currencytranslation" data-currency="{{ base64_encode($currency->id) }}" class="btn-new-currencytranslation top-space" title="Add new currency translation location">
                        <i class="fa fa-plus-circle fa-fw"></i>Add new currency translation
                    </a>
                @endif
            @else
                @if(I::can('edit_currencies'))
                    <a href="#dialog-new-currencytranslation" data-currency="{{ base64_encode($currency->id) }}" class="btn-new-currencytranslation top-space" title="Add new currency translation location">
                        <i class="fa fa-plus-circle fa-fw"></i>Add new currency translation
                    </a>
                @endif
            @endif
        </fieldset>
            {{--- /Currency translations ---}}
    </div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-currencies');
        enqueue_script('new-currency');
    </script>
@stop
