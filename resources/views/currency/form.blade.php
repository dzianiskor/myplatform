<fieldset>
	<legend>Basic Details</legend>

    <span class="info">Note: Currencies are updated automatically every 12 hours with the latest exchange rates.</span>

    {{ Form::label('name', 'Currency Name', array('for'=>'name')) }}
    {{ Form::text('name', $currency->name, array('class'=>'pure-input-1 required')) }}
	
	<div class="spacer"></div>
	
    <span class="info">Note: valid ISO 4217 currency short-codes can be  <a href="http://www.casi.org.uk/info/1051list/annexd.html">found here</a>.</span>

    {{ Form::label('short_code', 'Short Code', array('for'=>'short_code')) }}

    @if($currency->draft)
        {{ Form::text('short_code', '', array('class'=>'pure-input-1-3 required')) }}
    @else
        {{ Form::text('short_code', $currency->short_code, array('class'=>'pure-input-1-3 required')) }}
    @endif
    
    {{ Form::label('latest_rate', 'Latest Rate', array('for'=>'latest_rate')) }}
    @if ($currency->latestPrice())
        {{ Form::text('latest_rate', $currency->latestPrice()->rate, array('class'=>'pure-input-1-3')) }}
    @else
        {{ Form::text('latest_rate', "0.00", array('class'=>'pure-input-1-3')) }}
    @endif
</fieldset>