@extends('layouts.dashboard')

@section('title')
    Currencies | BLU
@stop

@section('body')
<div class="padding">

    <div class="content-head pure-g">
        <div class="title pure-u-1-2">
            <h1><i class="fa fa-money fa-fw"></i> Currencies ({{ $currencies->total() }})</h1>
        </div>

        <div class="controls pure-u-1-2">
            <form action="{{ Filter::searchUrl() }}" method="get">
                <input type="text" placeholder="Search" name="q" value="{{ Input::get('q') }}" />
                <button type="submit" title="" class="list-search"></button>
            </form>
            @if(I::can('create_currencies'))
                <a href="{{ url('dashboard/currencies/create') }}" class="pure-button pure-button-primary">Add Currency</a>
            @endif
        </div>
    </div>

    <table class="pure-table">
        <thead>
            <tr>
                <th width="50"><a href="{{ Filter::baseUrl('sort=id') }}">ID</a></th>
                <th style="width:100px;"><a href="{{ Filter::baseUrl('sort=short_code') }}">Short Code</a></th>
                <th style="text-align:left;padding-left:20px;"><a href="{{ Filter::baseUrl('sort=name') }}">Name</a></th>
                <th>Latest Rate (to USD)</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        @if(count($currencies) > 0)
            @foreach ($currencies as $currency)
                <tr>
                    <td>
                        {{ $currency->id }}
                    </td>
                    <td>
                        {{{ $currency->short_code }}}
                    </td>
                    <td style="text-align:left;padding-left:20px;">
                        <a href="/dashboard/currencies/edit/{{{base64_encode($currency->id)}}}" class="edit" title="View">{{{ $currency->name }}}</a>
                    </td>
                    <td>
                        @if ($currency->latestPrice())
                            {{{ $currency->latestPrice()->rate }}}
						@else
							Pending...
                        @endif
                    </td>
                    <td>
                        <a href="/dashboard/currencies/edit/{{ base64_encode($currency->id) }}" class="edit" title="View">View</a>

                        @if(I::can('delete_currencies'))
                            <a href="/dashboard/currencies/delete/{{ base64_encode($currency->id) }}" class="delete" title="Delete">Delete</a>
                        @endif
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="5">No Currencies Found</td>
            </tr>
        @endif
        </tbody>
    </table>

    {{ $currencies->setPath('currencies')->appends(Input::except('page'))->links() }}

</div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-currencies');
    </script>
@stop
