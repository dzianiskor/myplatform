<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<style>
    a:link {
      color:#046380;
      text-decoration:underline;
    }
    a:visited {
      color:#A7A37E;
      text-decoration:none;
    }
    a:hover {
      color:#002F2F;
      text-decoration:underline;
    }
    a:active {
      color:#046380;
      text-decoration:none;
    }
</style>
</head>
<body>
  <table align="center" cellpadding="0" cellspacing="0" style="width:600px">
    <tbody>
      <tr>
        <td>
        <table align="center" border="0" cellpadding="0" cellspacing="0" style="width:510px">
          <tbody>
            <tr>
              <td>
              <h1>MAIN TITLE</h1>

              <h3>Subtitle 01</h3>

              <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Integer egestas, libero eget viverra nonummy, turpis diam scelerisque dui, eu tempor quam leo quis justo. Sed velit magna, vestibulum nec, aliquam a, placerat eget, odio. Sed mbattis, ipsum in sodales vestibulum, sem mi egestas felis, et adipiscing ante enim id metus. Etiam elit dui, congue quis, interdum in, venenatis ac, orci. Vivamus vulputate. Nullam ante. Aliquam ornare, nisl at consequat laoreet, orci sem cursus purus, a pulvinar diam sem ut metus. Donec eu erat non lorem lacinia commodo. Nunc bibendum. Aliquam euismod odio a nulla euismod rutrum. Proin lobortis leo. Maecenas non ante. Nam nibh. Morbi interdum elit non augue</p>

              <p>&nbsp;</p>

              <h3>Subtitle 02</h3>

              <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Integer egestas, libero eget viverra nonummy, turpis diam scelerisque dui, eu tempor quam leo quis justo. Sed velit magna, vestibulum nec, aliquam a, placerat eget, odio. Sed mbattis, ipsum in sodales vestibulum, sem mi egestas felis, et adipiscing ante enim id metus. Etiam elit dui, congue quis, interdum in, venenatis ac, orci. Vivamus vulputate. Nullam ante. Aliquam ornare, nisl at consequat laoreet, orci sem cursus purus, a pulvinar diam sem ut metus. Donec eu erat non lorem lacinia commodo. Nunc bibendum. Aliquam euismod odio a nulla euismod rutrum. Proin lobortis leo. Maecenas non ante. Nam nibh. Morbi interdum elit non augue.</p>

              <p>&nbsp;</p>

              <h3>Subtitle 03</h3>

              <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Integer egestas, libero eget viverra nonummy, turpis diam scelerisque dui, eu tempor quam leo quis justo. Sed velit magna, vestibulum nec, aliquam a, placerat eget, odio. Sed mbattis, ipsum in sodales vestibulum, sem mi egestas felis, et adipiscing ante enim id metus. Etiam elit dui, congue quis, interdum in, venenatis ac, orci. Vivamus vulputate. Nullam ante. Aliquam ornare, nisl at consequat laoreet, orci sem cursus purus, a pulvinar diam sem ut metus. Donec eu erat non lorem lacinia commodo. Nunc bibendum. Aliquam euismod odio a nulla euismod rutrum. Proin lobortis leo. Maecenas non ante. Nam nibh. Morbi interdum elit non augue.</p>

              <p>&nbsp;</p>
              CAN-SPAN<br />
              Your Company<br />
              Address, PostCode, Telephone<br />
              <a href="#">Your Site</a><br />
              &nbsp;</td>
            </tr>
          </tbody>
        </table>
        </td>
      </tr>
    </tbody>
  </table>

</body>
</html>
