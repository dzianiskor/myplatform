@if(count($notification->segments) > 0)
    @foreach($notification->segments as $s)
        <li>
            {{ $s->name }}
            @if(I::can('delete_notifications'))
                <a href="/dashboard/notifications/unlink/{{ base64_encode($notification->id) }}/{{ base64_encode($s->id) }}" class="delete-ajax" data-target="#segment-listing">
                    [Delete]
                </a>
            @endif
        </li>
    @endforeach
@else
    <li class="empty">No Segments Associated</li>
@endif