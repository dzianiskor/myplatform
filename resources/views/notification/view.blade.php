@extends('layouts.dashboard')

@section('title')
    Notification &raquo; {{{ $notification->type }}} &raquo; {{{ $notification->name }}}  | BLU
@stop

@section('dialogs')
    <div id="dialog-template-layout" style="width:300px;">
        <h4>Choose Email Template</h4>
        <div class="image-choice">
            <a href="{{ url('/dashboard/notifications/new?type=email') }}" title="Email Notification">Email</a>
            <a href="{{ url('/dashboard/notifications/new?type=sms') }}" title="SMS Notification">SMS</a>
        </div>
    </div>

    <div id="dialog-add-segment" style="width:300px;">
        <h4>Associate to Segment</h4>

        {{ Form::open(array('class' => "pure-form pure-form-stacked")) }}
            <select name="segment_id" class="pure-input-1">
                <option>Select segment</option>
                @foreach($segments as $s)
                    <option value="{{ base64_encode($s->id) }}">{{{ $s->name }}}</option>
                @endforeach
            </select>
            <a href="/dashboard/notifications/link/{{ base64_encode($notification->id) }}" class="pure-button pure-button-primary attribute-action" data-target="#segment-listing">
                Save
            </a>
        {{ Form::close() }}
    </div>    
    
    <div id="dialog-add-user" style="width:300px;">
        <h4>Associate to User</h4>

        {{ Form::open(array('class' => "pure-form pure-form-stacked")) }}
            <fieldset>
                {{ Form::label('members-autocomplete', 'User', array('for'=>'members-autocomplete')) }}
               <input type="hidden" name="user_id" id="user_id" class="user_id" value="" />
               <input id="members-autocomplete" class="report-input members-autocomplete pure-input-1-1" placeholder="Search" value="{{ (!empty(Input::get('entity')))? Support::memberNameFromID(Input::get('entity')) : null }}" />
            </fieldset>
            <a href="/dashboard/notifications/linkuser/{{ base64_encode($notification->id) }}" class="pure-button pure-button-primary attribute-action" data-target="#user-listing">
                Save
            </a>
        {{ Form::close() }}
    </div>
@stop

@section('body')
    <div class="padding">

        {{-- Page Header --}}
        <div class="content-head pure-g">
            <div class="title pure-u-1-2">
                <h1><i class="fa fa-envelope fa-fw"></i>Notification &raquo; {{{ $notification->type }}} &raquo; {{{ $notification->name }}}</h1>
            </div>

            <div class="controls pure-u-1-2">
                <a href="{{ url('dashboard/notifications') }}" class="pure-button">All Notifications</a>
            </div>
        </div>
        {{-- /Page Header --}}

        {{ Form::open(array('url' => url('dashboard/notifications/update/'.base64_encode($notification->id)), 'method' => 'post', 'class' => "pure-form pure-form-stacked form-width notification", "id" => "notificationForm")) }}

            @include('partials.errors')

            {!! Form::hidden('redirect_to', URL::previous()) !!}

            <div class="pure-g">
                <div class="pure-u-1-2">
                    @if(!$notification->draft)
                        <fieldset>
                            <legend>Creator Details</legend>                
                            <table class="agent-details">
                                <tr>
                                    <td>Created by:</td>
                                    <td>{{ Support::memberRevNameFromID($notification->created_by) }}</td>
                                </tr>
                                <tr>
                                    <td>Created at:</td>
                                    <td>{{ date("M j, Y, g:i a", strtotime($notification->created_at)) }}</td>
                                </tr>                
                            </table>                
                        </fieldset>            
                    @endif            

                    <fieldset>
                        <legend>Basic Details</legend>

                        {{ Form::label('partner_id', 'Partner') }}
                        <select name="partner_id" class="pure-input-1-2 required">
                            <option>Select partner</option>
                            @foreach($partners as $p)
                                @if($p->id == $notification->partner_id)
                                    <option value="{{ $p->id }}" selected="selected">{{ $p->name }}</option>
                                @else
                                    <option value="{{ $p->id }}">{{ $p->name }}</option>
                                @endif
                            @endforeach
                        </select>

                        {{ Form::label('name', 'Name') }}
                        {{ Form::text('name', $notification->name, array('class' => 'pure-input-1 required')) }}
                    </fieldset>

                    <fieldset>
                        <legend>Publishing Details</legend>
                        {{ Form::label('start_date', 'Start Date') }}
                        {{ Form::text('start_date', date("Y/m/d H:i", strtotime($notification->start_date)), array('class' => 'pure-input-1 datetime-now required')) }}
                    </fieldset>
                </div>
                <div class="pure-u-1-2 attributes">
                    <div class="padding-left-40">
                        {{-- Segments Listing --}}
                        <h3>
                            @if(I::can_edit('notifications', $notification))
                            <a href="#dialog-add-segment" title="Add Segment" class="dialog">
                                Segments <i class="fa fa-plus-circle fa-fw"></i>
                            </a>
                            @else
                                Segments
                            @endif
                        </h3>
                        <ul id="segment-listing">
                            @if(count($notification->segments) > 0)
                                @foreach($notification->segments as $s)
                                    <li>
                                        {{ $s->name }}
                                        @if(I::can('delete_notifications'))
                                            <a href="/dashboard/notifications/unlink/{{ base64_encode($notification->id) }}/{{ base64_encode($s->id) }}" class="delete-ajax" data-target="#segment-listing">
                                                [Delete]
                                            </a>
                                        @endif
                                    </li>
                                @endforeach
                            @else
                                <li class="empty">No Segments Associated</li>
                            @endif
                        </ul>
                        {{-- /Segments Listing --}}
                    </div>
                <div class="padding-left-40">
                        {{-- Users Listing --}}
                        <h3>
                            @if(I::can_edit('notifications', $notification))
                            <a href="#dialog-add-user" title="Add User" class="dialog">
                                Users <i class="fa fa-plus-circle fa-fw"></i>
                            </a>
                            @else
                                Users
                            @endif
                        </h3>
                        <ul id="user-listing">
                            @if(count($notification->users) > 0)
                                @foreach($notification->users as $u)
                                    <li>
                                        {{ $u->name }}
                                        @if(I::can('delete_notifications'))
                                            <a href="/dashboard/notifications/unlinkuser/{{ base64_encode($notification->id) }}/{{ base64_encode($u->id) }}" class="delete-ajax" data-target="#user-listing">
                                                [Delete]
                                            </a>
                                        @endif
                                    </li>
                                @endforeach
                            @else
                                <li class="empty">No Users Associated</li>
                            @endif
                        </ul>
                        {{-- /Users Listing --}}
                    </div>               
                </div>
            </div>
            
            @if($notification->type == 'SMS')
                <fieldset>
                    <legend>SMS Details</legend>
                    
                    <span class="info">
                        Please note: SMS messages have length restrictions (160 English characters, 70 Arabic characters)
                    </span>

                    {{ Form::label('body', 'Contents') }}
                    {{ Form::textarea('body', $notification->body, array('class' => 'pure-input-1')) }}

                    <span class="inline-help">
                        <a href="/dashboard/notifications/test/{{ base64_encode($notification->id) }}" title="Send Test Notification" class="test-notification">Click here</a> to send a test notification to yourself.
                    </span>                    
                </fieldset>
            @else
                <fieldset>
                    <legend>Email Details</legend>

                    <span class="info">
                        Note: Emails without subject lines cannot be sent successfully.
                    </span>

                    {{ Form::label('subject', 'Subject') }}
                    {{ Form::text('subject', $notification->subject, array('class' => 'pure-input-1', 'placeholder' => '')) }}                    

                    {{ Form::label('subject', 'Email Body') }}
                    {{ Form::textarea('body', $notification->body, array('class' => 'pure-input-1', 'style' => 'height:400px', 'id' => 'editor')) }}

                    <span class="inline-help">
                        <a href="/dashboard/notifications/test/{{ base64_encode($notification->id) }}" title="Send Test Notification" class="test-notification">Click here</a> to send a test notification to yourself.
                    </span>
                </fieldset>
            @endif        

            <!-- Form Buttons -->
            <div class="form-buttons">
                <div class="left">
                    <a href="{{ url('dashboard/notifications') }}" class="pure-button">Cancel</a>
                </div>
                <div class="right">
                    <a href="/dashboard/notifications/send/{{ base64_encode($notification->id) }}" title="Send Notification" class="test-notification"><button type="button" class="pure-button pure-button-primary">Send Now</button></a>
                    @if($notification->sent && $notification->invoked)
                        <button type="submit" class="pure-button pure-button-primary" disabled="disabled">Sent</button>                    
                    @endif

                    @if($notification->invoked && !$notification->sent)
                        @if(I::can('edit_notifications'))
                            <button type="submit" class="pure-button pure-button-primary">Update</button>
                        @endif
                    @endif

                    @if($notification->draft)
                        @if(I::can('create_notifications'))
                            <button type="submit" class="pure-button pure-button-primary">Schedule</button>
                        @endif
                    @endif
                </div>
                <div class="clearfix"></div>
            </div>
            <!-- /Form Buttons -->

        {{ Form::close() }}
    </div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-notifications');

        $(function() {
            $(document).on('click', 'a.test-notification', function(e){
                e.preventDefault();
                
                NProgress.start();

                $.post($(this).attr('href'), $('form.notification').serialize(), function(resp){
                    console.log(resp);
                    NProgress.done();

                    if(resp.failed) {
                        alert(resp.message);
                    } else {
//                        alert(resp.toSource());
                        alert('The preview notification has been successfully sent to you.');
                    }
                });
            });
            
              // Turn on the global member search autocomplete function
            $(document).bind('keypress', '#members-autocomplete', function(event) {
            if($('#members-autocomplete').length > 0) {

                $("#members-autocomplete").autocomplete({
                  source: "/dashboard/reports/members",
                  minLength: 2,
                  select: function( event, ui ) {
                    $('input[name="user_id"]').val(ui.item.id);
                  }
                });
            }
        });
        });
    </script>

    @if($notification->type == 'Email')
        <script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
        <script type="text/javascript" src="/ckeditor/adapters/jquery.js"></script>
        <script type="text/javascript">
            $(function() {
                $('textarea#editor').ckeditor($.noop, {
                    height: '600px'
                });                
            });
        </script>
    @endif
@stop