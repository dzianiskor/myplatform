@extends('layouts.dashboard')

@section('title')
    Notifications | BLU
@stop

@section('dialogs')
    <div id="dialog-notification-type" style="width:300px;">
        <h4>Choose Notification Type</h4>
        <div class="button-choice">
            <a href="{{ url('/dashboard/notifications/new?type=email') }}" title="Email Notification">Email</a>
            <a href="{{ url('/dashboard/notifications/new?type=sms') }}" title="SMS Notification">SMS</a>
        </div>
    </div>
@stop

@section('body')
    <div class="padding">

        <div class="content-head pure-g">
            <div class="title pure-u-1-2">
                <h1><i class="fa fa-envelope fa-fw"></i> Notifications ({{ $notifications->total() }})</h1>
            </div>

            <div class="controls pure-u-1-2">
                <form action="{{ Filter::searchUrl() }}" method="get">
                    <input type="text" placeholder="Search" name="q" value="{{ Input::get('q') }}" />
                    <button type="submit" title="" class="list-search"></button>
                </form>
                @if(I::can('create_notifications'))
                    <a href="#dialog-notification-type" class="pure-button pure-button-primary dialog">New Notification</a>
                @endif
            </div>

            <div id="catalog-filter" class="pure-u-1-2">
                <input id="filter_route" type="hidden" value="notifications">
                <form id="filter-catalogue" action="{{ Filter::searchUrl() }}" method="get">
                    <div class="filter-block filter-block-pricing-width">
                        <span class="filter-label">Partner:</span>
                        <select style="width:100%;" name="partner_list[]" id="partner_list_select" multiple="multiple">
                            @foreach ($lists['partnerData'] as $key => $value)
                                <option value="{{ $key }}">{{ $value }}</option>
                            @endforeach
                        </select>
                        <input id="filterPartners" type="hidden" value="{{ Input::get('partner_list') }}">
                    </div>
                    <div class="filter-block filter-block-pricing-width">
                        <span class="filter-label">Type:</span>
                        <select style="width:100%;" name="type_list[]" data-filter-catalogue="type_list" data-filter-catalogue-title="Select Type" multiple="multiple">
                            @foreach ($lists['typeData'] as $key => $value)
                                <option value="{{ $key }}">{{ $value }}</option>
                            @endforeach
                        </select>
                        <input data-filter-catalogue="type_list" type="hidden" value="{{ Input::get('type_list') }}">
                    </div>
                    <div class="submit-filter-block disable-width">
                        <div style="float:right;">
                            <a href="{{ url('dashboard/notifications') }}" title="Clear Filters" class="pure-button pure-button-primary">
                                Clear Filters <i class="fa fa-ban fa-fw"></i>
                            </a>
                        </div>
                        <button class="pure-button pure-button-primary" type="submit" title="" style="float:right;margin-right:15px;">
                            Submit Filters
                        </button>
                    </div>
                </form>
            </div>
        </div>

        <table class="pure-table">
            <thead>
                <tr>
                    <th width="50"><a href="{{ Filter::baseUrl('sort=id') }}">ID</a></th>
                    <th style="text-align:left;padding-left:20px;"><a href="{{ Filter::baseUrl('sort=name') }}">Name</a></th>
                    <th><a href="{{ Filter::baseUrl('sort=partner_id') }}">Partner</a></th>
                    <th><a href="{{ Filter::baseUrl('sort=type') }}">Type</a></th>
                    <th><a href="{{ Filter::baseUrl('sort=invoked') }}">Scheduled</a></th>
                    <th><a href="{{ Filter::baseUrl('sort=sent') }}">Sent</a></th>
                    <th><a href="{{ Filter::baseUrl('sort=created_at') }}">Delivery Date</a></th>
                    <th width="150">Actions</th>
                </tr>
            </thead>
            <tbody>
            @if(count($notifications) > 0)
                @foreach ($notifications as $p)
                    <tr>
                        <td>
                            {{ $p->id }}
                        </td>
                        <td style="text-align:left;padding-left:20px;">
                            <a href="/dashboard/notifications/view/{{ base64_encode($p->id) }}" class="edit" title="View">{{{ $p->name }}}</a>
                        </td>
                        <td>
                            {{{ $p->partner->name }}}
                        </td>
                        <td>
                            {{{ $p->type }}}
                        </td>
                        <td>
                            @if($p->invoked)
                                <span class="ico-yes"></span>
                            @else
                                <span class="ico-no"></span>
                            @endif
                        </td>              
                        <td>
                            @if($p->sent)
                                <span class="ico-yes"></span>
                            @else
                                <span class="ico-no"></span>
                            @endif
                        </td>              
                        <td>
                            {{{ date("M j, Y, g:i a", strtotime($p->start_date)) }}}
                        </td>                 
                        <td>
                            @if(I::can('view_notifications'))
                            <a href="/dashboard/notifications/view/{{ base64_encode($p->id) }}" class="edit" title="View">View</a>
                            @endif

                            {{--- Cant delete once sent ---}}
                            @if(!$p->sent)
                                @if(I::can('delete_notifications'))
                                    <a href="/dashboard/notifications/delete/{{ base64_encode($p->id) }}" class="delete" title="Delete">Delete</a>
                                @endif
                            @endif
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="8">No Notifications Found</td>
                </tr>
            @endif
            </tbody>
        </table>

        {{ $notifications->setPath('notifications')->appends(Input::except('page'))->links() }}

    </div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-notifications');
    </script>
@stop
