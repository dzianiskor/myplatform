@if(count($notification->users) > 0)
    @foreach($notification->users as $u)
        <li>
            {{ $u->name }}
            @if(I::can('delete_notifications'))
                <a href="/dashboard/notifications/unlinkuser/{{ base64_encode($notification->id) }}/{{ base64_encode($u->id) }}" class="delete-ajax" data-target="#user-listing">
                    [Delete]
                </a>
            @endif
        </li>
    @endforeach
@else
    <li class="empty">No Users Associated</li>
@endif