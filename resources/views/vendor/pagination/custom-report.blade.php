<div class="dataTables_paginate paging_simple_numbers" id="datatable_report_paginate">
    @if ($paginator->hasPages())


        @if ($paginator->onFirstPage())
            <a class="paginate_button previous disabled" id="datatable_previous">&lt;</a>
        @else
            <a href="{{ $paginator->previousPageUrl() }}" class="paginate_button previous disabled" id="datatable_previous">&lt;</a>
        @endif

        <span>
            @foreach ($elements as $element)
                @if (is_string($element))
                    <a class="disabled" class="paginate_button">{{ $element }}</a>
                @endif

                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <a class="paginate_button current">{{ $page }}</a>
                        @else
                            <a class="paginate_button" href="{{ $url }}">{{ $page }}</a>
                        @endif
                    @endforeach
                @endif
            @endforeach
        </span>

        @if ($paginator->hasMorePages())
            <a class="paginate_button next disabled" href="{{ $paginator->nextPageUrl() }}" rel="next">&gt;</a></li>
        @else
            <a class="paginate_button next disabled">&lt;</a>
        @endif
    @endif
</div>
