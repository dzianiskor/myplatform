@extends('layouts.dashboard')

@section('title')
    Support Ticket | BLU
@stop

@section('body')
@include('partials.errors')

    <div class="pure-g">
        <div class="pure-u-1-3">
			<input type="hidden" id="API_KEY" name="api_key" value="c4ca4238a0b923820dcc509a6f75849b" />
            <form method="GET" action="/dashboard/travel/flights"  onsubmit="return validate()" id="flight-form" class="pure-form pure-form-stacked" autocomplete="off">
	            <label>From</label>
	            <input type="text" name="originplace" placeholder="Enter country, city or airport" class="pure-input-1 autoLocation noEnter" required />
                    <p id="locresults" ></p>
	            <label>To</label>
	            <input type="text" name="destinationplace" placeholder="Enter country, city or airport" class="pure-input-1 autoLocation noEnter" required />

				<label>
					<input type="checkbox" name="return" value="1" checked="checked" /> Return Flight?
				</label>

	            <table>
		            <tr>
		            	<td>
					        <label>Departure Date</label>
				            <input type="text" name="outbounddate" class="startDateSelector pure-input-1 noEnter" value="<?php echo date("d-m-Y", strtotime("+1 day")); ?>" />
		            	</td>
		            	<td>
				            <label>Return Date</label>
				            <input type="text" name="inbounddate" class="endDateSelector pure-input-1 noEnter" value="<?php echo date("d-m-Y", strtotime("+2 days")); ?>" />
		            	</td>
		            </tr>
	            </table>

			    <div class="pure-g">
			        <div class="pure-u-1-3">
			 			<label>12+ years</label>
			 			<input type="text" value="1" class="pure-input-1 enforceNumeric" name="adults" />
			        </div>

			        <div class="pure-u-1-3">
			        	<div class="wrap-middle-input">
			        		<label>Under 12</label>
            				<input type="text" value="0" class="pure-input-1 enforceNumeric" name="children" />
            			</div>
			        </div>

			        <div class="pure-u-1-3">
            			<label>Under 2</label>
            			<input type="text" value="0" class="pure-input-1 enforceNumeric" name="infants" />
			        </div>
			    </div>

			    <label>Cabin Class</label>
	            <select name="cabinclass" class="pure-input-1">
	            	<option value="Economy">Economy</option>
					<option value="PremiumEconomy">Premium Economy</option>
					<option value="Business">Business</option>
					<option value="First">First Class</option>
	            </select>


	            <div class="button-rail">
	            	<button class="greenBtn">Search</button>
	            </div>
            </form>
        </div>

        <script src="/js/vendor/jquery-1.10.1.min.js"></script>
        <script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
    </div>
<script>

		function validate(){
                   var start =  $('input.startDateSelector').datepicker('getDate');
                   var end =  $('input.endDateSelector').datepicker('getDate');;
                   if ( end < start){
                       alert("Check Dates");
                       return false;
                   }else
                       return true;
                };

    	$(function(){

		//set_menu('#mnuTravel');

		$("input.autoLocation").autocomplete({
			source: "search_flight_locations",
			minLength: 2,
			select: function( event, ui ) {
                            console.log(ui.item);
				$(this).val(ui.item.value);
			}
		});

/*		$("input.autoCity").autocomplete({
			source: "/travel/search_locations",
			minLength: 2,
			response: function(ecent, ui){
				alert('ok');
			},
			select: function( event, ui ) {
				$(this).val(ui.item.value);
			}
		});

		$("input.startDateSelector").datepicker({
			dateFormat: 'dd-mm-yy',
			minDate: 0,
	        onSelect: function (date ) {
                        var element = $("input.startDateSelector").attr('name');

                        // var endDate = $('input.endDateSelector').datepicker('getDate');

                        var formatLowerCase="dd-mm-yyyy".toLowerCase();
                        var formatItems=formatLowerCase.split("-");
                        var dateItems=date.split("-");
                        var monthIndex=formatItems.indexOf("mm");
                        var dayIndex=formatItems.indexOf("dd");
                        var yearIndex=formatItems.indexOf("yyyy");
                        var month=parseInt(dateItems[monthIndex]);
                            month-=1;
                        var endDate = new Date(dateItems[yearIndex],month,dateItems[dayIndex]);
	            $('input.endDateSelector').datepicker('setDate', endDate);

	            $('input.endDateSelector').datepicker('option', 'minDate', endDate);
	        }
		});
                */
    /* Flight datepickers */
    $('input[name="outbounddate"]').datepicker({
      dateFormat: 'dd-mm-yy',
      minDate: 0,
      onSelect: function (date) {
        var formatLowerCase="dd-mm-yyyy".toLowerCase();
        var formatItems=formatLowerCase.split("-");
        var dateItems=date.split("-");
        var monthIndex=formatItems.indexOf("mm");
        var dayIndex=formatItems.indexOf("dd");
        var yearIndex=formatItems.indexOf("yyyy");
        var month=parseInt(dateItems[monthIndex]);
        month-=1;
        var endDate = new Date(dateItems[yearIndex],month,dateItems[dayIndex]);
        endDate.setTime(endDate.getTime() + (1 * 24 * 60 * 60 * 1000));
        $('input[name="inbounddate"]').datepicker('setDate', endDate);

        $('input[name="inbounddate"]').datepicker('option', 'minDate', endDate);
      }
    });

    /* Hotel datepickers */
    $('input[name="arrivalDate"]').datepicker({
		  dateFormat: 'dd-mm-yy',
		  minDate: 0,
      onSelect: function (date) {
        var formatLowerCase="dd-mm-yyyy".toLowerCase();
        var formatItems=formatLowerCase.split("-");
        var dateItems=date.split("-");
        var monthIndex=formatItems.indexOf("mm");
        var dayIndex=formatItems.indexOf("dd");
        var yearIndex=formatItems.indexOf("yyyy");

        var month=parseInt(dateItems[monthIndex]);
        month-=1;
        var endDate = new Date(dateItems[yearIndex],month,dateItems[dayIndex]);
        endDate.setTime(endDate.getTime() + (1 * 24 * 60 * 60 * 1000));
        $('input[name="departureDate"]').datepicker('setDate', endDate);

        $('input[name="departureDate"]').datepicker('option', 'minDate', endDate);
      }
	  });

    /* Cars datepickers */
    $('.endTimeSelector').datetimepicker({
      value: '<?php echo date("Y-m-d", strtotime("+2 days")); ?>' + ' ' + '12:00',
      format: 'Y-m-d H:i'
    });
     $('.endTimeSelector').datetimepicker({
      format: 'Y-m-d H:i'
    });
    $('.startTimeSelector').datetimepicker({
      onSelectDate:function(current_time,$input){
        var formatLowerCase="yyyy-mm-dd".toLowerCase();
        var formated_time = current_time.dateFormat('Y-m-d');
        var formatItems=formatLowerCase.split("-");
        var dateItems=formated_time.split("-");
        var monthIndex=formatItems.indexOf("mm");
        var dayIndex=formatItems.indexOf("dd");
        var yearIndex=formatItems.indexOf("yyyy");
        var month=parseInt(dateItems[monthIndex]);
        month-=1;
        var endDate = new Date(dateItems[yearIndex],month,dateItems[dayIndex] );
        endDate.setTime(endDate.getTime() + (1 * 24 * 60 * 60 * 1000));
        endDate = endDate.dateFormat('Y-m-d');

        $('.endTimeSelector').datetimepicker({
          value: endDate,
          minDate: endDate,
          startDate: endDate,
          formatDate: 'Y-m-d H:i'
        });
        // $('.endTimeSelector').val(endDate);
      }
    });
    $('.endTimeSelector').datetimepicker({
        onSelectDate:function(current_time , $input ) {
          var formatLowerCase="yyyy-mm-dd".toLowerCase();
        var formated_time = current_time.dateFormat('Y-m-d');
        var formatItems=formatLowerCase.split("-");
        var dateItems=formated_time.split("-");
        var monthIndex=formatItems.indexOf("mm");
        var dayIndex=formatItems.indexOf("dd");
        var yearIndex=formatItems.indexOf("yyyy");
        var month=parseInt(dateItems[monthIndex]);
        month-=1;
        var endDate = new Date(dateItems[yearIndex],month,dateItems[dayIndex]);
        endDate.setTime(endDate.getTime() + (1 * 24 * 60 * 60 * 1000));
        endDate = endDate.dateFormat('Y-m-d');
        $('.endTimeSelector').datetimepicker({
          value: endDate + ' ' + '12:00',
          minDate: endDate,
          startDate: endDate,
          formatDate: 'Y-m-d H:i'
        });

        }
    });

		$("input.endDateSelector").datepicker({
			dateFormat: 'dd-mm-yy',
			minDate: 0
		});

                $('input[name="inbounddate"]').prop('disabled', false);
                
		$('input[name="return"]').change(function(){
			if($(this).is(':checked')) {
				$('input[name="inbounddate"]').prop('disabled', false);
			} else {
				$('input[name="inbounddate"]').prop('disabled', true);
			}
		});

		$(document).on('click', '#tab-nav a', function(e){
			e.preventDefault();

			$('#tab-nav li').removeClass('active');

			$(this).closest('li').addClass('active');

			$('div.tab').hide();

			$($(this).attr('href')).show();
		});

		$(document).on('change', 'select.doUpdateCity', function() {
			NProgress.start();

			$('select.targetCity option').first().html('Loading...');

			$.get('/travel/ajax_cities', {country: $(this).val()}, function(resp){
				$('.targetCity').html('<option value="">Select</option>' + resp);
				NProgress.done();
			})
		});

		$(document).on('change', 'select.doUpdateLocation', function() {
			NProgress.start();

			$('select.targetLocation option').first().html('Loading...');

			$.get('/travel/ajax_locations', {country: $('select.doUpdateCity').val(), city: $(this).val()}, function(resp){
				$('.targetLocation').html(resp);
				NProgress.done();
			})
		});

		$(document).on('change', 'select.doUpdateCity2', function() {
			NProgress.start();

			$('select.targetCity2 option').first().html('Loading...');

			$.get('/travel/ajax_cities', {country: $(this).val()}, function(resp){
				$('.targetCity2').html('<option value="">Select</option>' + resp);
				NProgress.done();
			})
		});

		$(document).on('change', 'select.doUpdateLocation2', function() {
			NProgress.start();

			$('select.targetLocation2 option').first().html('Loading...');

			$.get('/travel/ajax_locations', {country: $('select.doUpdateCity2').val(), city: $(this).val()}, function(resp){
				$('.targetLocation2').html(resp);
				NProgress.done();
			})
		});

		$(document).on('change', 'input[name="sameCarLocation"]', function(){
			if($(this).is(':checked')) {
				$('div.carDropOff').fadeOut().addClass('hidden');
				$('div.carDropOff select').prop('disabled', true);
			} else {
				$('div.carDropOff').removeClass('hidden').fadeIn();
				$('div.carDropOff select').prop('disabled', false);
			}
		});

		$(document).on('change', 'input[name="carAge"]', function(){
			if($(this).is(':checked')) {
				$('div.carAge').fadeOut().addClass('hidden');
			} else {
				$('div.carAge').removeClass('hidden').fadeIn();
			}
		});
	});

</script>
@stop