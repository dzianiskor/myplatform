@extends('layouts.dashboard')

@section('title')
    Support Ticket | BLU
@stop

@section('body')
<div id="hotel-page" class="travel-search-page container">
<div class="summary-bar">
		<div class="container">
			Hotel Search > Hotels list
		</div>
	</div>
    <div class="pure-g">
        <div class="pure-u-1-3">

        	<div class="padding-right-20 block-labels">

				<form method="POST" action="/dashboard/travel/poll_hotels" id="frmPoll" class="pure-form">
					<input type="hidden" name="destinationString" value="<?php echo Input::get('city'); ?>" />
	            	<input type="hidden" name="arrivalDate" value="<?php echo date("m/d/Y", strtotime(Input::get('arrivalDate'))) ?>" />
	            	<input type="hidden" name="departureDate" value="<?php echo date("m/d/Y", strtotime(Input::get('departureDate'))); ?>" />
					<input type="hidden" name="adults" value="<?php echo Input::get('adults'); ?>" />
					<input type="hidden" name="children" value="<?php echo Input::get('children'); ?>" />
					<input type="hidden" name="infants" value="<?php echo Input::get('infants'); ?>" />
					<input type="hidden" name="numberOfBedRooms" value="<?php echo Input::get('rooms'); ?>" />
					<input type="hidden" name="sorttype" value="<?php echo Input::get('sorttype'); ?>" />

	            	<h3>Search Hotel by Name</h3>
	            	<div class="section">
	            		<input type="text" name="hotelName" class="hotelName" value="" placeholder="Hotel Name" />
	            	</div>

	        		<h3>Star Rating</h3>

	            	<div class="section">
                            	<label>
	            			<input type="checkbox" id="selecctall" checked="checked" /> Select All
	            		</label>
	            		<label>
	            			<input type="checkbox" name="ratingComp[]" value="5" checked="checked" class="checkboxRate"/> 5 Stars <div class="star5"></div>
	            		</label>
	            		<label>
	            			<input type="checkbox" name="ratingComp[]" value="4" checked="checked" class="checkboxRate"/> 4 Stars <div class="star4"></div>
	            		</label>
	            		<label>
	            			<input type="checkbox" name="ratingComp[]" value="3" checked="checked" class="checkboxRate"/> 3 Stars <div class="star3"></div>
	            		</label>
	            		<label>
	            			<input type="checkbox" name="ratingComp[]" value="2" checked="checked" class="checkboxRate"/> 2 Stars <div class="star2"></div>
	            		</label>
	            		<label>
	            			<input type="checkbox" name="ratingComp[]" value="1" checked="checked" class="checkboxRate"/> 1 Star <div class="star1"></div>
	            		</label>
	            	</div>

					<h3>Price</h3>

	            	<div class="section">
	            		<label>
                                    	<label>
	            				<input type="radio" name="rateComp" value="0;100000000.00" checked="checked"> All Prices
	            			</label>
	            			<label>
	            				<input type="radio" name="rateComp" value="0;50.00"> Less than 5,000 Pts
	            			</label>
	            			<label>
	            				<input type="radio" name="rateComp" value="50.00;100.00"> 5,000 Pts to 10,000 Pts
	            			</label>
	            			<label>
	            				<input type="radio" name="rateComp" value="100.00;500.00"> 10,000 Pts to 50,000 Pts
	            			</label>
	            			<label>
	            				<input type="radio" name="rateComp" value="500.00;1000.00"> 50,000 Pts to 100,000 Pts
	            			</label>
	            			<label>
	            				<input type="radio" name="rateComp" value="1000.00;100000.00"> Greater than 100,000 Pts
	            			</label>
	            		</label>
	            	</div>

	        		<h3>Accommodation Type</h3>

	            	<div class="section">
                                <label>
	            			<input type="checkbox" id="allPropCategories" checked="checked" /> Select All
	            		</label>
	            		<label>
	            			<input type="checkbox" name="propertyCategory[]" value="1" checked="checked" class="checkboxCategory" /> Hotel
	            		</label>
	            		<label>
	            			<input type="checkbox" name="propertyCategory[]" value="2" checked="checked" class="checkboxCategory"/> Suite
	            		</label>
	            		<label>
	            			<input type="checkbox" name="propertyCategory[]" value="3" checked="checked" class="checkboxCategory"/> Resort
	            		</label>
	            		<label>
	            			<input type="checkbox" name="propertyCategory[]" value="4" checked="checked" class="checkboxCategory"/> Vacation Rental / Condo
	            		</label>
	            		<label>
	            			<input type="checkbox" name="propertyCategory[]" value="5" checked="checked" class="checkboxCategory"/> Bed &amp; Breakfast
	            		</label>
	            		<label>
	            			<input type="checkbox" name="propertyCategory[]" value="6" checked="checked" class="checkboxCategory"/> All Inclusive
	            		</label>
	            	</div>

				</form>

            </div>

        </div>

        <div class="pure-u-2-3">

            <div class="title-rail">
	        	<h1>
	        		<?php echo Input::get('city'); ?><br />

	        	</h1>

	        	<p class="trip-date">
	        		<?php echo date("F j, Y", strtotime(Input::get('arrivalDate'))); ?> - <?php echo date("F j, Y", strtotime(Input::get('departureDate'))); ?>
	        	</p>

	        	<a href="/travel" title="Change Search" class="redBtn">Change Search</a>
        	</div>

        	<div class="sort-results">
        		<strong>Sort by:</strong>
        		<a href="#" data-value="PRICE" title="Price">Price</a> /
        		<a href="#" data-value="ALPHA" title="Hotel Name">Hotel Name</a> /
        		<a href="#" data-value="QUALITY" title="Star Rating">Star Rating</a> /
        		<a href="#" data-value="OVERALL_VALUE" title="Popularity">Popularity</a>
        	</div>

        	<div id="search-results">
        		<!--
	        	<ul class="result-top">
	        		<li>Price</li>
	        		<li>Airline</li>
	        		<li>Outbound</li>
	        		<li>Return</li>
	        	</ul>
	        	-->

	        	<div class="clearfix"></div>

        		<div id="listing">
        			<span class="loading">Fetching hotels, one moment...</span>
        		</div>
        	</div>

        </div>
    </div>

</div>
@stop

@section('page_script')
<script>

// Poll flight search and update interface
var extJson;
function poll()
{
	var frm = $('#frmPoll');

	NProgress.start();

	var arrivalDate = $('input[name="arrivalDate"]').val();
	var departureDate = $('input[name="departureDate"]').val();
	var rooms = $('input[name="numberOfBedRooms"]').val();
        var numberOfAdults = $('input[name="adults"]').val();
        var numberOfChildren = $('input[name="children"]').val();
        var numberOfInfants = $('input[name="infants"]').val();
        // add the search option functionality by hotel name <fboukarroum>
        var hotelName = $('input[name="hotelName"]').val().trim();
        // add the check of price selection <fboukarroum>
        var rateCompPrice = $("input[name='rateComp']:checked").val();
        var result = rateCompPrice.split(';');
        var minPrice = parseInt(result[0]);
        var maxPrice = parseInt(result[1]);
        // add the check of rating stars selection <fboukarroum>
        var rateStars = [];
        var accommodationTypes=[];
         //console.log(frm.serialize());
         $('input[name="ratingComp[]"]:checked').each(function() {
            rateStars.push(this.value);
         });
          // add the check of accommodation types selection <fboukarroum>
          $('input[name="propertyCategory[]"]:checked').each(function() {
            accommodationTypes.push(this.value);
         });
	$.post(frm.attr('action'), frm.serialize(), function(json){
                extJson = $.parseJSON(json);
//                console.log(json);
//                console.log(json.hotels.HotelListResponse.HotelList.HotelSummary);
//				exit;
//		var results = Handlebars.compile($("#results-template").html());
                //console.log(hotel_list);
		// Update Results

		$('#search-results #listing').html('');
		var hotel_list = extJson.HotelListResponse.HotelList.HotelSummary;
//		console.log(extJson.HotelListResponse);
//		exit;
//		console.log(hotel_list.length);
		//$.each(json, function(i, v){
		var contextList = [];
		for(var i=0;i<hotel_list.length;i++)
		{
				var v = hotel_list[i];
				var hname = v.name;
				 // add the search option functionality by hotel name <fboukarroum>
				if (hname.toString().toLowerCase().indexOf(hotelName.toLowerCase()) < 0)
				{
					continue;
				}
				// add the check of price selection <fboukarroum>
				if ( ( minPrice >= parseInt(v.lowRate) ) || ( parseInt(v.lowRate)>= maxPrice ) )
				{
					continue;
				}
				// add the check of rating stars selection <fboukarroum>
				var upHRating = Math.round(v.hotelRating);
				if ($.inArray(upHRating.toString(), rateStars) == -1 )
				{
					continue;
				}
				// add the check of accommodation types selection <fboukarroum>
				if ($.inArray(v.propertyCategory.toString(), accommodationTypes) == -1 )
				{
					continue;
				}
				var context = {
						thumbnail: v.thumbNailUrl,
						id: v.hotelId,
						name: v.name,
						rating: v.hotelRating,
						reviewCount: v.tripAdvisorReviewCount,
						address: v.address1,
//                                    avg: accounting.formatNumber(Math.round((v.highRate + v.lowRate) / 0.02)),
						avg: accounting.formatNumber( v.lowRate / 0.01 ),
						available: 1,// v.RoomRateDetailsList.RoomRateDetails.RateInfos.RateInfo.currentAllotment,
						arrivalDate: arrivalDate,
						departureDate: departureDate,
						rooms: rooms,
						numberOfAdults: numberOfAdults,
						numberOfChildren: numberOfChildren,
						numberOfInfants: numberOfInfants
				};

				var html		= '	<div class="result"> <div class="f-image">';
					html		+= '<img src="http://images.travelnow.com'+ context.thumbnail +'" alt="" />';
					html		+= '</div><div class="f-detail">';
					html		+= '<a href="/dashboard/travel/hotel_info/'+ context.id +'?hotelimg='+ context.thumbnail +'&arrivalDate='+ context.arrivalDate +'&departureDate='+ context.departureDate +'&numberOfAdults='+ context.numberOfAdults +'&numberOfChildren='+ context.numberOfChildren +'&numberOfInfants='+ context.numberOfInfants +'&rooms='+ context.rooms +'" title="'+ context.name +'">'+ context.name +' <img src="../../public/img/'+ context.rating +'-star.png" alt="'+ context.rating +' Star Rating" /></a>';
					html		+= '<p>'+ context.address +'</p>';
					html		+= '<p><strong>'+ context.rating +'</strong> out of <strong>5</strong> ('+ context.reviewCount +' Reviews)</p>';
					html		+= '</div>';
					html		+= '<div class="f-price"><p class="avg-price">'+ context.avg +' pts</p><p>Avg/Night</p></div>';
					html		+= '<div class="clearfix"></div></div>';

				contextList.push(context);
//				$('#search-results #listing').append(results(context));
				$('#search-results #listing').append(html);
				NProgress.done();
		}

			//for(var i =0; i<contextList.length; i++){
//			var sort = $('input[name="sorttype"]').val()
//			 console.log("sort type ="+sort);
//			if($('input[name="sorttype"]').val()=="PRICE"){
//				contextList.sort(function(a, b){
//				var keyAtext =a.avg;
//				var keyBtext = b.avg;
//				var keyA = keyAtext.replace(",", "");
//				var keyB = keyBtext.replace(",", "");
//				// Compare the 2
//				if(keyA - keyB < 0 ) return -1;
//				if(keyA - keyB > 0 ) return 1;
//				return 0;
//			});
//			}
//			else if($('input[name="sorttype"]').val()=="QUALITY"){
//				contextList.sort(function(a, b){
//				var keyA = a.rating;
//				var keyB = b.rating;
//				// Compare the 2
//				if(keyA - keyB < 0) return -1;
//				if(keyA - keyB > 0) return 1;
//				return 0;
//			});
//			}
//			else if($('input[name="sorttype"]').val()=="ALPHA"){
//				contextList.sort(function(a, b){
//				var keyA = a.name;
//				var keyB = b.name;
//				// Compare the 2
//				if(keyA < keyB) return -1;
//				if(keyA > keyB) return 1;
//				return 0;
//			});
//			}
//			else if($('input[name="sorttype"]').val()=="OVERALL_VALUE"){
//				contextList.sort(function(a, b){
//				var keyA = a.reviewCount;
//				var keyB = b.reviewCount;
//				// Compare the 2
//				if(keyA - keyB < 0) return -1;
//				if(keyA - keyB > 0) return 1;
//				return 0;
//			});
//			}
//		$('#search-results #listing').html('');
//		for(var i =0; i<contextList.length; i++)
//		{
//			$('#search-results #listing').append(html);
//		}
		//$('#search-results #listing').append(results(context));

		NProgress.done();
	});
}

$(function(){

	$(document).on('change', '#frmPoll input[type="checkbox"], #frmPoll input[type="radio"]', function(){
		poll();
	});

	$(document).on('click', '.sort-results a', function(e){
		e.preventDefault();
		$('input[name="sorttype"]').val($(this).data('value'));
		$('.sort-results a').removeClass('active');
		$(this).addClass('active');
		poll();
	});

	$(document).on('keypress', '.hotelName', function(e) {
	    if(e.which == 13) {
	    	e.preventDefault();
	        poll();
	    }
	});

	// Off we go...
	poll();
});
// add select / deselect all for rates <fboukarroum>
$(document).ready(function() {
    $('#selecctall').click(function() {  //on click
        if(this.checked) { // check select status
            $('.checkboxRate').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkboxRate"
            });
        }else{
            $('.checkboxRate').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkboxRate"
            });
        }
    });

});

$(document).ready(function() {
    $('#allPropCategories').click(function() {  //on click
        if(this.checked) { // check select status
            $('.checkboxCategory').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkboxRate"
            });
        }else{
            $('.checkboxCategory').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkboxRate"
            });
        }
    });

});

</script>
@stop