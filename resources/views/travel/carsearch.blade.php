@extends('layouts.dashboard')

@section('title')
    Support Ticket- Hotel Search | BLU
@stop

@section('body')
@include('partials.errors')
<div class="hotel-search-form container">
	<div class="summary-bar">
		<div class="container">
			Hotel Search
		</div>
	</div>
	<div class="pure-g">
		<div class="pure-u-1-3">
			<form method="GET" action="/dashboard/travel/cars" onsubmit="return validate()" id="car-form" class="pure-form pure-form-stacked" autocomplete="off">

        		<div class="carPickup">
				    <label>Country</label>

                                    <?php $countrylist = TravelController::rc_country_list();?>

		            <select name="country" class="pure-input-1 doUpdateCity">
		            	<option value="">Select</option>
		            	<?php foreach( $countrylist as $name ){
								if(!is_array($name)){ ?>
									<option value="<?php echo $name; ?>"><?php echo $name; ?></option>
						<?php } }; ?>
		            </select>

				    <label>City</label>
		            <select name="city" class="pure-input-1 targetCity doUpdateLocation">
		            	<option value="">Select</option>
		            </select>

				    <label>Location</label>
		            <select name="location" class="pure-input-1 targetLocation">
		            	<option value="">Select</option>
		            </select>
	            </div>

        		<div class="carDropOff hidden">
				    <label>Drop off Country</label>
		            <select name="country_drop" class="pure-input-1 doUpdateCity2">
		            	<option value="">Select</option>
		            	<?php foreach(TravelController::rc_country_list() as $name): ?>
		            		<option value="<?php echo $name; ?>"><?php echo $name; ?></option>
		            	<?php endforeach; ?>
		            </select>

				    <label>Drop off City</label>
		            <select name="city_drop" class="pure-input-1 targetCity2 doUpdateLocation2">
		            	<option value="">Select</option>
		            </select>

				    <label>Drop off Location</label>
		            <select name="location_drop" class="pure-input-1 targetLocation2">
		            	<option value="">Select</option>
		            </select>
	            </div>

				<label>
					<input type="checkbox" name="sameCarLocation" value="1" checked="checked" /> Return to same location?
				</label>

	            <table>
		            <tr>
		            	<td>
					        <label>Pick Up</label>
				            <input type="text" name="pickup" class="startTimeSelector pure-input-1 noEnter" value="<?php echo date("Y-m-d", strtotime("+1 day")); ?>" />
		            	</td>
		            	<td>
				            <label>Drop Off</label>
                    <input type="text" name="dropoff" class="endTimeSelector pure-input-1 noEnter" />
				            <!-- <input type="text" name="dropoff" class="endTimeSelector pure-input-1 noEnter" value="<?php echo date("Y-m-d", strtotime("+2 days")); ?>" /> -->
		            	</td>
		            </tr>
	            </table>

				<label>
					<input type="checkbox" name="carAge" value="1" checked="checked" /> Aged between 25 and 70?
				</label>

				<div class="carAge hidden">
    				<label>Driver Age</label>
    				<input type="text" class="pure-input-1 enforceNumeric" name="age" value="30" />
    			</div>

	            <div class="button-rail">
	            	<button class="greenBtn">Search</button>
	            </div>
            </form>
		</div>
	</div>
</div>
@stop

@section('page_script')
<script>
    function validate(){
		var start =  $('input.startDateSelector').datepicker('getDate');
		var end =  $('input.endDateSelector').datepicker('getDate');
		if ( end < start){
			alert("Check Dates");
			return false;
		}else
			return true;
	};

	$(function(){
		$(document).on('change', 'select.doUpdateCity', function() {
				NProgress.start();

				$('select.targetCity option').first().html('Loading...');

				$.get('/dashboard/travel/ajax_cities', {country: $(this).val()}, function(resp){
					$('.targetCity').html('<option value="">Select</option>' + resp);
					NProgress.done();
				})
		});

		$(document).on('change', 'select.doUpdateLocation', function() {
			NProgress.start();

			$('select.targetLocation option').first().html('Loading...');

			$.get('/dashboard/travel/ajax_locations', {country: $('select.doUpdateCity').val(), city: $(this).val()}, function(resp){
				$('.targetLocation').html(resp);
				NProgress.done();
			})
		});

		$(document).on('change', 'select.doUpdateCity2', function() {
			NProgress.start();

			$('select.targetCity2 option').first().html('Loading...');

			$.get('/dashboard/travel/ajax_cities', {country: $(this).val()}, function(resp){
				$('.targetCity2').html('<option value="">Select</option>' + resp);
				NProgress.done();
			})
		});

		$(document).on('change', 'select.doUpdateLocation2', function() {
			NProgress.start();

			$('select.targetLocation2 option').first().html('Loading...');

			$.get('/dashboard/travel/ajax_locations', {country: $('select.doUpdateCity2').val(), city: $(this).val()}, function(resp){
				$('.targetLocation2').html(resp);
				NProgress.done();
			})
		});

		$(document).on('change', 'input[name="sameCarLocation"]', function(){
			if($(this).is(':checked')) {
				$('div.carDropOff').fadeOut().addClass('hidden');
				$('div.carDropOff select').prop('disabled', true);
			} else {
				$('div.carDropOff').removeClass('hidden').fadeIn();
				$('div.carDropOff select').prop('disabled', false);
			}
		});

		$(document).on('change', 'input[name="carAge"]', function(){
			if($(this).is(':checked')) {
				$('div.carAge').fadeOut().addClass('hidden');
			} else {
				$('div.carAge').removeClass('hidden').fadeIn();
			}
		});

		 /* Cars datepickers */
		$('.endTimeSelector').datetimepicker({
		  value: '<?php echo date("Y-m-d", strtotime("+2 days")); ?>' + ' ' + '12:00',
		  format: 'Y-m-d H:i'
		});
		 $('.endTimeSelector').datetimepicker({
		  format: 'Y-m-d H:i'
		});
		 $('.startTimeSelector').datetimepicker({
		  format: 'Y-m-d H:i'
		});
		$('.startTimeSelector').datetimepicker({
		  onSelectDate:function(current_time,$input){
			var formatLowerCase="yyyy-mm-dd".toLowerCase();
			var formated_time = current_time.dateFormat('Y-m-d');
			var formatItems=formatLowerCase.split("-");
			var dateItems=formated_time.split("-");
			var monthIndex=formatItems.indexOf("mm");
			var dayIndex=formatItems.indexOf("dd");
			var yearIndex=formatItems.indexOf("yyyy");
			var month=parseInt(dateItems[monthIndex]);
			month-=1;
			var endDate = new Date(dateItems[yearIndex],month,dateItems[dayIndex] );
			endDate.setTime(endDate.getTime() + (1 * 24 * 60 * 60 * 1000));
			//endDate.setTime(endDate.getTime());
			endDate = endDate.dateFormat('Y-m-d H:i');

			$('.endTimeSelector').datetimepicker({
			  value: endDate,
			  minDate: endDate,
			  startDate: endDate,
			  formatDate: 'Y-m-d H:i'
			});
			// $('.endTimeSelector').val(endDate);
			$('input[name="dropoff"]').datetimepicker('setDate', endDate);

			$('input[name="dropoff"]').datetimepicker('option', 'minDate', endDate);
		  }
		});
		$('.endTimeSelector').datetimepicker({
			onSelectDate:function(current_time , $input ) {
			  var formatLowerCase="yyyy-mm-dd".toLowerCase();
			var formated_time = current_time.dateFormat('Y-m-d');
			var formatItems=formatLowerCase.split("-");
			var dateItems=formated_time.split("-");
			var monthIndex=formatItems.indexOf("mm");
			var dayIndex=formatItems.indexOf("dd");
			var yearIndex=formatItems.indexOf("yyyy");
			var month=parseInt(dateItems[monthIndex]);
			month-=1;
			var endDate = new Date(dateItems[yearIndex],month,dateItems[dayIndex]);
	//        endDate.setTime(endDate.getTime() + (1 * 24 * 60 * 60 * 1000));
			endDate.setTime(endDate.getTime());
			endDate = endDate.dateFormat('Y-m-d');
			$('.endTimeSelector').datetimepicker({
			  value: endDate + ' ' + '12:00',
			  minDate: endDate,
			  startDate: endDate,
			  formatDate: 'Y-m-d H:i'
			});

			}
		});
	});
</script>
@stop