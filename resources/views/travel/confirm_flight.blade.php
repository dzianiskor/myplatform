@extends('layouts.dashboard')

@section('title')
    Confirm Flight | BLU
@stop

@section('body')
<div id="confirm-travel">
	<div class="summary-bar">
		<div class="container">
			Flight Search > Personal Details > Summary
		</div>
	</div>

	<div class="container">
		<h3>Flight Booking Details Summary</h3>

		<p>
			Below is the summary of your flight booking request. Please review all the information for accuracy.
			In case of any changes, click on the MODIFY button to make those amendments.
			Otherwise, please read the BLU Travel terms and check the box to be able to submit booking request. The request will be processed and a confirmation will be sent shortly to your email address.
		</p>
<?php

	$user_travel_id = session::get('user_travel_id');
	//Get User Data
	$user = App\User::where('id', $user_travel_id)->first();
//	echo "<pre>";
//	print_r(session::all());
//	print_r($user);
//	exit;
	$data	= json_decode( base64_decode($meta), true );

	$out_depart_time			= $data['out_depart_time'];
	$price						= $data['price'];
	$out_depart_station_code	= $data['out_depart_station_code'];

	$out_depart_station_name	= $data['out_depart_station_name'];
	$out_arrive_time			= $data['out_arrive_time'];
	$out_arrive_station_code	= $data['out_arrive_station_code'];

	$out_arrive_station_name	= $data['out_arrive_station_name'];
	$out_carrier_name			= $data['out_carrier_name'];
	$out_stops					= $data['out_stops'];
	$out_duration				= $data['out_duration'];
        if(!empty($data['in_depart_station_code'])){
	$in_depart_station_code	= $data['in_depart_station_code'];
        }
          if(!empty($data['in_depart_station_name'])){
	$in_depart_station_name	= $data['in_depart_station_name'];
          }
             if(!empty($data['in_depart_time'])){
	$in_arrive_time			= $data['in_arrive_time'];
             }
          if(!empty($data['in_depart_time'])){
	$in_depart_time			= $data['in_depart_time'];
          }
          if(!empty($data['in_arrive_station_code'])){
	$in_arrive_station_code	= $data['in_arrive_station_code'];
             }
        if(!empty($data['in_arrive_station_code'])){

	$in_arrive_station_name	= $data['in_arrive_station_code'];
        }
          if(!empty($data['in_carrier_name'])){
	$in_carrier_name			= $data['in_carrier_name'];
          }
            if(isset($data['in_stops'])){
                $in_stops					= $data['in_stops'];
            }
              if(!empty($data['in_duration'])){
                $in_duration				= $data['in_duration'];
              }

?>
		<table class="pure-table pure-table-bordered">
		    <thead>
		        <tr>
		            <th>&nbsp;</th>
		            <th>Airline / Flight</th>
		            <th>Depart / Arrive</th>
		            <th>City / Airport</th>
		            <th>Duration / Stops</th>
		        </tr>
		    </thead>

		    <tbody>
		    	<?php if(isset($out_depart_time)): ?>
		        <tr>
		            <td>
		            	<img src="/img/departBig.png" />
		            </td>
		            <td>
		            	<?php echo $out_carrier_name; ?>
		            </td>
		            <td>
		            	<span><?php echo date("F j, Y, g:i a", strtotime($out_depart_time)); ?></span>
		            	<span><?php echo date("F j, Y, g:i a", strtotime($out_arrive_time)); ?></span>
		            </td>
		            <td>
		            	<span><?php echo $out_depart_station_name; ?> (<?php echo $out_depart_station_code; ?>)</span>
		            	<span><?php echo $out_arrive_station_name; ?> (<?php echo $out_arrive_station_code; ?>)</span>
		            </td>
		            <td>
		            	<span><?php echo floor($out_duration / 60)."H ".($out_duration % 60)."m"; ?></span>
		            	<span><?php echo $out_stops; ?></span>
		            </td>
		        </tr>
		    	<?php endif; ?>

		    	<?php if(isset($in_depart_time)): ?>
		        <tr>
		            <td>
		            	<img src="/img/arrivalBig.png" />
		            </td>
		            <td>
		            	<?php echo $in_carrier_name; ?>
		            </td>
		            <td>
		            	<span><?php echo date("F j, Y, g:i a", strtotime($in_depart_time)); ?></span>
		            	<span><?php echo date("F j, Y, g:i a", strtotime($in_arrive_time)); ?></span>
		            </td>
		            <td>
		            	<span><?php echo $in_depart_station_name; ?> (<?php echo $in_depart_station_code; ?>)</span>
		            	<span><?php echo $in_arrive_station_name; ?> (<?php echo $in_arrive_station_code; ?>)</span>
		            </td>
		            <td>
		            	<span><?php echo floor($in_duration / 60)."H ".($in_duration % 60)."m"; ?></span>
		            	<span><?php echo $in_stops; ?></span>
		            </td>
		        </tr>
		    	<?php endif; ?>

		    </tbody>
		</table>

		<div class="total-points">
			<span>Total Points</span>
			<span class="points"><?php echo number_format($price); ?></span>
		</div>
	</div>

	<div class="container">

		<h5>Personal Details</h5>

		<form action="/dashboard/travel/do_confirm" id="submitForm" class="pure-form pure-form-stacked">

			<div id="form-errors"></div>
			<input type="hidden" name="user_travel_id" value="<?php echo $user_travel_id; ?>"/>
			<input type="hidden" name="points" value="<?php echo $price; ?>" />
			<input type="hidden" name="type" value="flight" />
			<input type="hidden" name="meta" value="<?php echo $meta; ?>" />

			<div class="personal-details">
				<label for="title">Title</label>
				<select name="title" id="title" class="pure-input-1-4">
					<?php foreach(Meta::titleList() as $t): ?>
						<?php if($user->title == $t): ?>
							<option value="<?php echo $t; ?>" selected="selected"><?php echo $t; ?></option>
						<?php else: ?>
							<option value="<?php echo $t; ?>"><?php echo $t; ?></option>
						<?php endif; ?>
					<?php endforeach; ?>
				</select>

				<label>First Name</label>
				<input type="text" name="first_name" value="<?php echo $user->first_name; ?>"  class="pure-input-1" required />

				<label>Last Name</label>
				<input type="text" name="last_name" value="<?php echo $user->last_name; ?>" class="pure-input-1" required />

				<label>Email</label>
				<input type="text" name="email" value="<?php echo $user->email; ?>" class="pure-input-1" required />

				<label>Gender</label>
				<select name="gender" id="gender" class="pure-input-1-4">
					<?php foreach(Meta::genderList() as $k => $v): ?>
						<?php if($user->gender == $k): ?>
							<option value="<?php echo $k; ?>" selected="selected"><?php echo $v; ?></option>
						<?php else: ?>
							<option value="<?php echo $k; ?>"><?php echo $v; ?></option>
						<?php endif; ?>
					<?php endforeach; ?>
				</select>

				<label>Date of Birth</label>
				<input type="text" name="dob" value="<?php echo $user->dob; ?>" class="pure-input-1" required />

				<label>Passport Country</label>
				<input type="text" name="passport_country" value="<?php // echo country_name(user_attr('country_id')); ?>" class="pure-input-1" required />

				<label>Mobile Number</label>
				<input type="text" name="mobile" value="<?php echo $user->telephone_code; ?><?php echo $user->mobile; ?>" class="pure-input-1" required />
			</div>

<!--			<div class="pin">
				<label>PIN Code</label>
				<input type="password" name="pin" value="" placeholder="4-Digit Pin" class="pure-input-1-4"  required />
			</div>-->

			<div class="terms">
				<label>
					<input type="checkbox" name="chkTerms" value="1" /> I have read the <a href="/pages/travel_terms" title="Travel Terms">BLU travel terms</a>.
				</label>
			</div>

			<div class="note">
				<h4>Important Information:</h4>
				<p>
					Please note that certain restrictions might apply, especially if you are redeeming for the lowest fares. <br />
					Please check with the airline operator for further details on restrictions.
				</p>
			</div>

			<div class="button-rail">
				<button class="greenBtn">Submit Booking Request</button>
			</div>

		</form>

	</div>
</div>
@stop

@section('page_script')
<script>
$(function(){
	$(document).on('click', '#submitForm button', function(e){
		e.preventDefault();

		if(!$('input[name="chkTerms"]').is(':checked')) {
			alert("Please read & confirm the terms and conditions before proceeding.");

			return false;
		}

		NProgress.start();

		var frm   = $('#submitForm');
		var _this = $(this);

		_this.prop('disabled', true).addClass('btnDisabled');
		$.post(frm.attr('action'), frm.serialize(), function(json){

			NProgress.done();

			if(!json.failed) {
//				alert(json.message);
				window.location.href = '/dashboard/call_center/view/' + json.ticket_id + '?load';
			} else {
				$('#form-errors').html(json.messages).fadeIn();

				$('#form-errors').fadeIn();

				_this.prop('disabled', false).removeClass('btnDisabled');
			}
		});
	});
});
</script>
@stop