@extends('layouts.dashboard')

@section('title')
    Confirm Car | BLU
@stop

@section('body')
<div id="confirm-travel" class="container">
	<div class="summary-bar">
		<div class="container">
			Car Search > Personal Details > Summary
		</div>
	</div>

	<div class="container">
		<h3>Car Booking Details Summary</h3>

		<p>
			Below is the summary of your car booking request. Please review all the information for accuracy.
			In case of any changes, click on the MODIFY button to make those amendments.
			Otherwise, please read the BLU Travel terms and check the box to be able to submit booking request. The request will be processed and a confirmation will be sent shortly to your email address.
		</p>

		<table class="pure-table pure-table-bordered">
		    <thead>
		        <tr>
		            <th>&nbsp;</th>
		            <th>Vehicle</th>
		            <th>Pickup</th>
		            <th>Dropoff</th>
		        </tr>
		    </thead>
                <?php if(isset($this->session->userdata['data']) ): ?>
		    <tbody>
			    <tr>
					<td>
						<img src="<?php echo $this->session->userdata['data']->img; ?>" alt="" />
					</td>
			    	<td>
			    		<?php echo $this->session->userdata['data']->vehicle; ?>
			    	</td>
			    	<td>
			    		<?php echo $this->session->userdata['data']->pickup_location_name; ?>
			    		<br />
                                        <?php echo $this->session->userdata['data']->pickup_city; ?>
			    		<br />
                                         <?php echo $this->session->userdata['data']->pickup_country; ?>
			    		<br />
                                        <?php echo $this->session->userdata['data']->pickup; ?>
			    	</td>
			    	<td>
                                    <?php echo $this->session->userdata['data']->dropoff_location_name; ?>
                                    <br />
                                    <?php echo $this->session->userdata['data']->dropoff_city; ?>
                                    <br />
                                    <?php echo $this->session->userdata['data']->dropoff_country; ?>
                                    <br />
                                    <?php echo $this->session->userdata['data']->dropoff; ?>
			    	</td>
			    </tr>
		    </tbody>
                <?php else: ?>
		    <tbody>
			    <tr>
					<td>
						<img src="<?php echo Input::get('img'); ?>" alt="" />
					</td>
			    	<td>
			    		<?php echo Input::get('vehicle'); ?>
			    	</td>
			    	<td>
			    		<?php echo Input::get('pickup_location_name'); ?>
			    		<br />
			    		<?php echo Input::get('pickup_city'); ?>
			    		<br />
			    		<?php echo Input::get('pickup_country'); ?>
			    		<br />
			    		<?php echo Input::get('pickup'); ?>
			    	</td>
			    	<td>
			    		<?php echo Input::get('dropoff_location_name'); ?>
			    		<br />
			    		<?php echo Input::get('dropoff_city'); ?>
			    		<br />
			    		<?php echo Input::get('dropoff_country'); ?>
			    		<br />
			    		<?php echo Input::get('dropoff'); ?>
			    	</td>
			    </tr>
		    </tbody>
                <?php endif; ?>
		</table>
		<?php
			$user_travel_id = session::get('user_travel_id');
			//Get User Data
			$user = App\User::where('id', $user_travel_id)->first();
			$price				= trim(Input::get('points'));
		?>
		<div class="total-points">
			<span>Total Points</span>
			<span class="points">
				<?php echo number_format($price); ?>
			</span>
		</div>
	</div>

	<div class="container">

		<h5>Personal Details</h5>

			<form action="/dashboard/travel/do_confirm" id="submitForm" class="pure-form pure-form-stacked">
			<div id="form-errors"></div>
			<?php if(isset($this->session->userdata['data']->errorMessage)): ?>
				<div id="messageError" class="TravelchargesNotice"><?php echo $this->session->userdata['data']->errorMessage; ?></div>
			<?php endif; ?>
			<input type="hidden" name="user_travel_id" value="<?php echo $user_travel_id; ?>"/>
			<input type="hidden" name="points" value="<?php echo Input::get('points'); ?>" />
			<input type="hidden" name="type" value="car" />
			<input type="hidden" name="meta" value="<?php echo $meta; ?>" />

			<div class="personal-details">
				<label for="title">Title</label>
				<select name="title" id="title" class="pure-input-1-4">
					<?php foreach(Meta::titleList() as $t): ?>
						<?php if($user->title == $t): ?>
							<option value="<?php echo $t; ?>" selected="selected"><?php echo $t; ?></option>
						<?php else: ?>
							<option value="<?php echo $t; ?>"><?php echo $t; ?></option>
						<?php endif; ?>
					<?php endforeach; ?>
				</select>

				<label>First Name</label>
				<input type="text" name="first_name" value="<?php echo $user->first_name; ?>"  class="pure-input-1" required />

				<label>Last Name</label>
				<input type="text" name="last_name" value="<?php echo $user->last_name; ?>" class="pure-input-1" required />

				<label>Email</label>
				<input type="text" name="email" value="<?php echo $user->email; ?>" class="pure-input-1" required />

				<label>Gender</label>
				<select name="gender" id="gender" class="pure-input-1-4">
					<?php foreach(Meta::genderList() as $k => $v): ?>
						<?php if($user->gender == $k): ?>
							<option value="<?php echo $k; ?>" selected="selected"><?php echo $v; ?></option>
						<?php else: ?>
							<option value="<?php echo $k; ?>"><?php echo $v; ?></option>
						<?php endif; ?>
					<?php endforeach; ?>
				</select>

				<label>Date of Birth</label>
				<input type="text" name="dob" value="<?php echo $user->dob; ?>" class="pure-input-1" required />

				<label>Passport Country</label>
				<input type="text" name="passport_country" value="<?php echo Meta::country_name_from_id($user->country_id); ?>" class="pure-input-1" required />

				<label>Mobile Number</label>
				<input type="text" name="mobile" value="<?php echo $user->telephone_code; ?><?php echo $user->mobile; ?>" class="pure-input-1" required />
			</div>

<!--			<div class="pin">
				<label>PIN Code</label>
				<input type="password" name="pin" value="" placeholder="4-Digit Pin" class="pure-input-1-4"  required />
			</div>

			<div class="terms">
				<label>
					<input type="checkbox" name="chkTerms" value="1" /> I have read the <a href="/pages/travel_terms" title="Travel Terms">BLU travel terms</a>.
				</label>
			</div>-->

			<div class="note">
				<h4>Important Information:</h4>
				<p>
					Please note that certain restrictions might apply, especially if you are redeeming for the lowest price. <br />
					Please check with the car rental operator for further details on restrictions.
				</p>
			</div>

			<div class="button-rail">
				<button class="greenBtn">Submit Booking Request</button>
			</div>
		</form>

	</div>
</div>
@stop

@section('page_script')
<script>
$(function(){
	$(document).on('click', '#submitForm button', function(e){
		e.preventDefault();

//		if(!$('input[name="chkTerms"]').is(':checked')) {
//			alert("Please read & confirm the terms and conditions before proceeding.");
//
//			return false;
//		}

		NProgress.start();

		var frm   = $('#submitForm');
		var _this = $(this);

		_this.prop('disabled', true).addClass('btnDisabled');

		$.post(frm.attr('action'), frm.serialize(), function(json){

			NProgress.done();

			if(!json.failed) {
//				alert("Your booking has been placed & the information emailed to you.");
				window.location.href = '/dashboard/call_center/view/' + json.ticket_id + '?load';
			} else {
				$('#form-errors').html(json.messages).fadeIn();

				$('#form-errors').fadeIn();

				_this.prop('disabled', false).removeClass('btnDisabled');
			}
		});
	});
});
</script>
@stop