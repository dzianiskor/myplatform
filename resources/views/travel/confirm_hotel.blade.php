@extends('layouts.dashboard')

@section('title')
    Confirm Hotel | BLU
@stop

@section('body')
<div id="confirm-travel" class="container">
	<div class="summary-bar">
		<div class="container">
			Hotel Search > Personal Details > Summary
		</div>
	</div>

	<div class="container">
		<h3>Hotel Booking Details Summary</h3>

		<p>
			Below is the summary of your hotel booking request. Please review all the information for accuracy.
			In case of any changes, click on the MODIFY button to make those amendments.
			Otherwise, please read the BLU Travel terms and check the box to be able to submit booking request. The request will be processed and a confirmation will be sent shortly to your email address.
		</p>

		<table class="pure-table pure-table-bordered">
		    <thead>
		        <tr>
		            <th>&nbsp;</th>
		            <th>Hotel</th>
		            <th>Check-in</th>
		            <th>Check-out</th>
		            <th>Duration / Rooms</th>
		        </tr>
		    </thead>

		    <tbody>
			    <tr>
					<td>
                                                <?php $img_small = str_replace("_t.jpg","_s.jpg",$form['img']);?>
						<img src="<?php echo "http://images.trvl-media.com" . $img_small; ?>" alt="" />
					</td>
			    	<td>
			    		<?php echo $form['hotelName']; ?>
			    		<br />
			    		<?php echo $form['hotelAddress1']; ?>
			    		<br />
			    		<?php echo $form['hotelAddress2']; ?>
			    		<br />
			    		<?php echo $form['city']; ?>
			    	</td>
			    	<td>
			    		<?php echo date ( 'd/m/Y' , strtotime($form['arrivalDate'])); ?>
			    	</td>
			    	<td>
			    		<?php echo date ( 'd/m/Y' , strtotime($form['departureDate'])); ?>
			    	</td>
			    	<td>
			    		<?php if(Meta::days_diff($form['departureDate'], $form['arrivalDate']) == 1): ?>
			    			<?php echo Meta::days_diff($form['departureDate'], $form['arrivalDate']) ?> Day / <?php echo ($form['rooms'] == 1)? $form['rooms'].' Room': 'Rooms'; ?>
			    		<?php else: ?>
			    			<?php echo Meta::days_diff($form['departureDate'], $form['arrivalDate']) ?> Days / <?php echo ($form['rooms'] == 1)? $form['rooms'].' Room': 'Rooms'; ?>
			    		<?php endif; ?>
			    	</td>
			    </tr>
		    </tbody>
		</table>
		<?php
			$user_travel_id = session::get('user_travel_id');
			//Get User Data
			$user = App\User::where('id', $user_travel_id)->first();

			$daysDiff					= Meta::days_diff($form['departureDate'], $form['arrivalDate']);
			$form['points']				= $form['points'] * $daysDiff;
			$surchargeTotal				= $form['surchargeTotal'];
			$hotelFees					= $form['hotelFees'];
			$totalPriceInPoints			= $form['points'] + $surchargeTotal + $hotelFees;
		?>
		<div class="total-points">
			<span>Sub-Total Points</span>
            <span id="total_pts" class="points">
				<?php echo number_format($form['points']);  ?>
			</span>
		</div>
		<div class="total-points">
			<span>Local Taxes</span>
            <span id="total_pts" class="points">
				<?php echo number_format($surchargeTotal);  ?>
			</span>
		</div>
		<?php if( $hotelFees > 0 ){ ?>
		<div class="total-points">
			<span>Hotel Fees</span>
            <span id="total_pts" class="points">
				<?php echo number_format($hotelFees);  ?>
			</span>
		</div>
		<?php } ?>
		<hr>
		<div class="total-points">
			<span>Total Points</span>
            <span id="total_pts" class="points">
				<?php echo number_format($totalPriceInPoints);  ?>
			</span>
		</div>
	</div>

	<div class="container">

		<h5>Guest Details</h5>

		<form action="/dashboard/travel/do_confirm" id="submitForm" class="pure-form pure-form-stacked">
			<div id="form-errors"></div>
			<?php if(isset($errorMessage)): ?>
				<div id="messageError" class="TravelchargesNotice"><?php echo($errorMessage); ?></div>
			<?php endif; ?>
			<input type="hidden" name="user_travel_id" value="<?php echo $user_travel_id; ?>"/>
			<input type="hidden" name="points" value="<?php echo $totalPriceInPoints; ?>" />
			<input type="hidden" name="type" value="hotel" />
			<input type="hidden" name="meta" value="<?php echo $meta; ?>" />

			<div class="personal-details">
				<label for="title">Title</label>
				<select name="title" id="title" class="pure-input-1-4">
					<?php foreach(Meta::titleList() as $t): ?>
						<?php if($user->title == $t): ?>
							<option value="<?php echo $t; ?>" selected="selected"><?php echo $t; ?></option>
						<?php else: ?>
							<option value="<?php echo $t; ?>"><?php echo $t; ?></option>
						<?php endif; ?>
					<?php endforeach; ?>
				</select>

				<label>First Name *</label>
				<input type="text" name="book_first_name" value="<?php echo $user->first_name; ?>"  class="pure-input-1" required />

				<label>Last Name *</label>
				<input type="text" name="book_last_name" value="<?php echo $user->last_name; ?>" class="pure-input-1" required />

				<label>Email *</label>
				<input type="text" name="book_email" value="<?php echo $user->email; ?>" class="pure-input-1" required />

				<label>Confirm Email *</label>
				<input type="text" name="confirm_email" id="confirm_email" value="<?php echo $user->email; ?>" class="pure-input-1" required />

				<label>Street Name/Bldg/Unit Number *</label>
				<input type="text" name="book_address_1" value="NA" class="pure-input-1" required />

				<label>City *</label>
				<input type="text" name="book_city" value="<?php echo Meta::city_name_from_id($user->city_id); ?>" class="pure-input-1" required />

				<label>Country *</label>
				<input type="text" name="book_country_code" value="<?php echo Meta::country_name_from_id($user->country_id); ?>" class="pure-input-1" required />

				<label>Postal Code *</label>
				<input type="text" name="book_postal_code" value="NA" class="pure-input-1" required />

				<label>Mobile Number *</label>
				<input type="text" name="book_mobile" value="<?php echo $user->telephone_code; ?><?php echo $user->mobile; ?>" class="pure-input-1" required />

				<div class="pin">
					<label>PIN Code</label>
					<input type="password" name="pin" value="" placeholder="4-Digit Pin" class="pure-input-1-4"  required />
				</div>

			<div class="terms">
				<label>
					<input type="checkbox" name="chkTerms" value="1" /> I have read the <a href="/pages/travel_terms" title="Travel Terms">BLU travel terms</a>.
				</label>
			</div>
			</div>

			<div class="note">
				<h4>Important Information:</h4>
				<p>
					Please note that certain restrictions might apply, especially if you are redeeming for the lowest fares. <br />
					Please check with the airline operator for further details on restrictions.
				</p>
			</div>

			<div class="button-rail">
				<button class="greenBtn">Submit Booking Request</button>
			</div>

		</form>


	</div>
</div>
@stop

@section('page_script')

<script>
$(function(){
	$(document).on('click', '#submitForm button', function(e){
		e.preventDefault();

		if(!$('input[name="chkTerms"]').is(':checked')) {
			alert("Please read & confirm the terms and conditions before proceeding.");

			return false;
		}

		NProgress.start();

		var frm   = $('#submitForm');
		var _this = $(this);

		_this.prop('disabled', true).addClass('btnDisabled');

		$.post(frm.attr('action'), frm.serialize(), function(json){

			NProgress.done();

			if(!json.failed) {
//				alert("Your booking has been placed & the information emailed to you.");
				window.location.href = '/dashboard/call_center/view/' + json.ticket_id + '?load';
			} else {
				$('#form-errors').html(json.messages).fadeIn();

				$('#form-errors').fadeIn();

				_this.prop('disabled', false).removeClass('btnDisabled');
			}
		});
	});
});

</script>
@stop