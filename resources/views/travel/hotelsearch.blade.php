@extends('layouts.dashboard')

@section('title')
    Support Ticket- Hotel Search | BLU
@stop

@section('body')
@include('partials.errors')

<div class="hotel-search-form container">
	<div class="summary-bar">
		<div class="container">
			Hotel Search
		</div>
	</div>
	<div class="pure-g">
		<div class="pure-u-1-3">
			<form method="GET" action="/dashboard/travel/hotels" onsubmit="return validate()" id="hotel-form" class="pure-form pure-form-stacked" autocomplete="off">
				<input type="hidden" id="API_KEY" name="api_key" value="<?php echo $api_key; ?>" />
				<input type="hidden" id="USER_ID" name="user_id" value="<?php echo $user_id; ?>" />
				<label>Location</label>
				<input type="text" name="city" placeholder="Enter country, city or airport" class="pure-input-1 autoCity noEnter" required />

				<table>
					<tr>
						<td>
							<label>Check-in</label>
							<input type="text" name="arrivalDate" class="startDateSelector pure-input-1 noEnter" value="<?php echo date("d-m-Y", strtotime("+1 day")); ?>" />
						</td>
						<td>
							<label>Check-out</label>
							<input type="text" name="departureDate" class="endDateSelector pure-input-1 noEnter" value="<?php echo date("d-m-Y", strtotime("+2 days")); ?>" />
						</td>
					</tr>
				</table>

				<div class="pure-g">
					<div class="pure-u-1-3">
						<label>12+ years</label>
						<input type="text" value="1" class="pure-input-1 enforceNumeric" name="adults" />
					</div>

					<div class="pure-u-1-3">
						<div class="wrap-middle-input">
							<label>Under 12</label>
							<input type="text" value="0" class="pure-input-1 enforceNumeric" name="children" />
						</div>
					</div>

					<div class="pure-u-1-3">
						<label>Under 2</label>
						<input type="text" value="0" class="pure-input-1 enforceNumeric" name="infants" />
					</div>
				</div>

				<label>Rooms</label>
				<select name="rooms" class="pure-input-1">
					<option value="1">1 Room</option>
					<option value="2">2 Rooms</option>
					<option value="3">3 Rooms</option>
					<option value="4">4 Rooms</option>
				</select>

				<div class="button-rail">
					<button class="greenBtn">Search</button>
				</div>
			</form>
		</div>
	</div>
</div>
@stop

@section('page_script')
	<script>
				function validate(){
                   var start =  $('input.startDateSelector').datepicker('getDate');
                   var end =  $('input.endDateSelector').datepicker('getDate');;
                   if ( end < start){
                       alert("Check Dates");
                       return false;
                   }else
                       return true;
                };

		$(function(){
			$("input.autoCity").autocomplete({
					source: "search_locations",
				minLength: 2,
				select: function( event, ui ) {
					$(this).val(ui.item.value);
				}
			});

			/* Hotel datepickers */
			$('input[name="arrivalDate"]').datepicker({
				  dateFormat: 'dd-mm-yy',
				  minDate: 0,
			  onSelect: function (date) {
				var formatLowerCase="dd-mm-yyyy".toLowerCase();
				var formatItems=formatLowerCase.split("-");
				var dateItems=date.split("-");
				var monthIndex=formatItems.indexOf("mm");
				var dayIndex=formatItems.indexOf("dd");
				var yearIndex=formatItems.indexOf("yyyy");

				var month=parseInt(dateItems[monthIndex]);
				month-=1;
				var endDate = new Date(dateItems[yearIndex],month,dateItems[dayIndex]);
				endDate.setTime(endDate.getTime() + (1 * 24 * 60 * 60 * 1000));
				//endDate.setTime(endDate.getTime());
				$('input[name="departureDate"]').datepicker('setDate', endDate);

				$('input[name="departureDate"]').datepicker('option', 'minDate', endDate);
			  }
			});

			$("input.endDateSelector").datepicker({
				dateFormat: 'dd-mm-yy',
				minDate: 0
			});

		});
	</script>
@stop
