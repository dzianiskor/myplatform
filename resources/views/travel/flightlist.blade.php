@extends('layouts.dashboard')

@section('title')
    Support Ticket | BLU
@stop

@section('body')
<div id="flight-page" class="travel-search-page container">
    <div class="pure-g">
        <div class="pure-u-1-3">
        	<div class="padding-right-20">
		        <div class="best-price">
		        	<span class="cabinClass"><?php echo Input::get('cabinclass'); ?></span>
		        	<span class="bestPrice">Best Price: <span>0</span> Pts</span>
		        </div>

				<form method="POST" action="/dashboard/travel/poll_flights" id="frmPollFlights">
				<?php
					$outbounddate = Input::get('outbounddate');
					$inbounddate = Input::get('inbounddate');
					$arr_outbound_date = explode('-',$outbounddate);					
					$outbounddate = $arr_outbound_date[1] . '/' . $arr_outbound_date[0] . '/' . $arr_outbound_date[2];
                                        if ( isset($inbounddate) && $inbounddate != "")
                                        {
                                            $arr_inbound_date = explode('-',$inbounddate);
                                            $inbounddate = $arr_inbound_date[1] . '/' . $arr_inbound_date[0] . '/' . $arr_inbound_date[2];
                                        }
				?>
					<input type="hidden" name="session_url" value="<?php //echo $session_url; ?>" />
					<input type="hidden" name="currency" value="<?php echo Input::get('currency');?>"/>
					<input type="hidden" name="originplace" value="<?php echo Input::get('originplace');?>"/>
					<input type="hidden" name="destinationplace" value="<?php echo Input::get('destinationplace');?>"/>
					<input type="hidden" name="outbounddate" value="<?php echo $outbounddate;?>"/>
					<input type="hidden" name="inbounddate" value="<?php echo $inbounddate;?>"/>
					<input type="hidden" name="cabinclass" value="<?php echo Input::get('cabinclass');?>"/>
					<input type="hidden" name="adults" value="<?php echo Input::get('adults');?>"/>
					<input type="hidden" name="children" value="<?php echo Input::get('children');?>"/>
					<input type="hidden" name="infants" value="<?php echo Input::get('infants');?>"/>
					<input type="hidden" name="sorttype" value="price" />
					<input type="hidden" name="pageindex" value="0" />
					<input type="hidden" name="stopsFilter" value=""/>
                                         <input type="hidden" name="return" value="<?php echo Input::get('return'); ?>" />
					<h3>Stops</h3>

					<div class="section">
						<select name="stops">
							<option value="" selected="selected">Any</option>
							<option value="0">Direct (No Stops)</option>
							<option value="1">1 Stop</option>
							<option value="2">2 Stops</option>
							<option value="3">3 Stops</option>
						</select>
					</div>
<!--
					<h3>Departure Times</h3>

					<div class="section">

						<h5>Outbound</h5>

						<div id="outboundslider-display" class="time-display">
							<span class="start">00:00</span> - <span class="end">23:59</span>
						</div>

						<div id="outboundslider" class="range-slider"></div>

						<input type="hidden" name="outbounddepartstarttime" value="00:00" />
						<input type="hidden" name="outbounddepartendtime" value="23:59" />

						<h5>Return Times</h5>

						<div id="inboundslider-display" class="time-display">
							<span class="start">00:00</span> - <span class="end">23:59</span>
						</div>

						<div id="inboundslider" class="range-slider"></div>

						<input type="hidden" name="inbounddepartstarttime" value="00:00" />
						<input type="hidden" name="inbounddepartendtime" value="23:59" />

						<h5>Journey Duration</h5>

						<div id="durationslider-display" class="time-display">
							Maximum <span class="hours">30</span> Hours
						</div>

						<div id="durationslider" class="slider"></div>

						<input type="hidden" name="duration" value="-1" />
					</div>-->

					<h3>Filter Airlines</h3>
								Powered by <img src="/img/skyscanner2.png" style="width:100px"/>

					<div class="section doSelectAirlines">
                                             <label><input type="checkbox" id="allCarriers" checked="checked" /> Select All</label>
						<ul id="list-carriers"></ul>
					</div>
				</form>
			</div>
		</div>


 <div class="pure-u-2-3">
	<div class="title-rail">
		<h1>
			From: <?php echo Input::get('originplace'); ?><br />
			To: <?php echo Input::get('destinationplace'); ?>
		</h1>

		<p class="trip-date">
			<?php if(Input::get('return')): ?>
				Depart <?php echo date("F j, Y", strtotime(Input::get('outbounddate'))); ?> - Return <?php echo date("F j, Y", strtotime(Input::get('inbounddate'))); ?>
			<?php else:?>
				Depart <?php echo date("F j, Y", strtotime(Input::get('outbounddate'))); ?>
			<?php endif; ?>
		</p>

		<a href="/dashboard/travel/flightsearch" title="Change Search" class="redBtn">Change Search</a>
	</div>

	<div class="sort-results">
		<strong>Sort by:</strong>
		<a href="#" data-value="price" title="Price" class="active">Price</a> /
		<a href="#" data-value="duration" title="Duration">Duration</a> /
		<a href="#" data-value="outbounddeparttime" title="Depart">Depart</a> /
		<a href="#" data-value="inboundarrivetime" title="Arrival">Arrival</a>
	</div>

	<div id="search-results">
		<ul class="result-top">
			<li>Price</li>
			<li>Airline</li>
			<li>Outbound</li>
                        <?php if(Input::get('return')): ?>
			<li>Return</li>
                        <?php endif; ?>
		</ul>

		<div class="clearfix"></div>

		<div id="listing">
			<span class="loading">Fetching flights, one moment...</span>
		</div>
	</div>

	<div id="travel-pagination">
		<ul>
			<li><a href="#" data-page="0" class="active">1</a></li>
			<li><a href="#" data-page="1">2</a></li>
			<li><a href="#" data-page="2">3</a></li>
			<li><a href="#" data-page="3">4</a></li>
			<li><a href="#" data-page="4">5</a></li>
			<li><a href="#" data-page="5">6</a></li>
			<li><a href="#" data-page="6">7</a></li>
			<li><a href="#" data-page="7">8</a></li>
		</ul>

		<div class="clearfix"></div>
	</div>

</div>
</div>
</div>
@stop

@section('page_script')
<script>
var poll;
var poll_count = 0;

// Create readable time values from intervals
function timeValue(i)
{
	switch(i) {
		case 0:
			return "00:00";
		case 1:
			return "01:00";
		case 2:
			return "02:00";
		case 3:
			return "03:00";
		case 4:
			return "04:00";
		case 5:
			return "05:00";
		case 6:
			return "06:00";
		case 7:
			return "07:00";
		case 8:
			return "08:00";
		case 9:
			return "09:00";
		case 10:
			return "10:00";
		case 11:
			return "11:00";
		case 12:
			return "12:00";
		case 13:
			return "13:00";
		case 14:
			return "14:00";
		case 15:
			return "15:00";
		case 16:
			return "16:00";
		case 17:
			return "17:00";
		case 18:
			return "18:00";
		case 19:
			return "19:00";
		case 20:
			return "20:00";
		case 21:
			return "21:00";
		case 22:
			return "22:00";
		case 23:
			return "23:00";
		case 24:
			return "23:59";
	}
}

/*
 */

// Create a basic time range
function timeRange()
{
	return [ 0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24];
}

// Poll flight search and update interface

function poll_flights()
{
	clearTimeout(poll);

	var frm = $('#frmPollFlights');



	NProgress.start();

	$.post(frm.attr('action'), frm.serialize(), function(json){

var json = <?php echo $results; ?>;

		if(json == null ) {
			poll = setTimeout(function() {
				poll_flights();
			}, 5000);

			switch(poll_count) {
				case 5:
					$('span.loading').html('This is taking longer than expected, hang tight...');
					break;
				case 15:
					$('span.loading').html('Still searching...');
					break;
				case 25:
					$('span.loading').html('We\'re not getting any results back from airport services.');
					break;
				case 32:
					$('span.loading').html('Something has gone wrong, we do appologise.');
					break;
			}

			poll_count++;

			return;
		}
                //Console.log(json);
		// Update Airlines

		var carrier_html	= '';
		$.each(json.Carriers, function(index, obj){
			carrier_html	+= '<li>';
			carrier_html	+= '<label><input type="checkbox" name="skipCarrierLookup[]" value="' + obj.Id + '" checked="checked" /> ' + obj.Name + '</label>'
			carrier_html	+= '</li>';
		});
		$('#list-carriers').html(carrier_html);

		// Update Search Results
		updateSearchResult(json);

	});

}

function updateSearchResult(json){
	var lowest_fare = 100000000;
	var contextList = [];
	$('#search-results #listing').html('');
	for(var i = 0; i < json.Itineraries.length; i++) {
		var iteniery = json.Itineraries[i];

		var context = {
				lowest_fare: 0,
				lowest_fare_agent_id: 0,

				// Outbound
				out_id: iteniery.OutboundLegId,
				out_depart_time: 0,
				out_depart_station_id: 0,
				out_depart_station_info: {},
				out_arrival_time: 0,
				out_arrival_station_id: 0,
				out_arrival_station_info: {},
				out_carrier_id: 0,
				out_carrier_info: {},
				out_duration: 0,
				out_stops:0,

				// Inbound
				in_id: iteniery.InboundLegId,
				in_depart_time: 0,
				in_depart_station_id: 0,
				in_depart_station_info: {},
				in_arrival_time: 0,
				in_arrival_station_id: 0,
				in_arrival_station_info: {},
				in_carrier_id: 0,
				in_carrier_info: {},
				in_duration: 0,
				in_stops: 0,

				// Booking Details
				booking_details_link: iteniery.BookingDetailsLink.Uri,
				booking_details_body: iteniery.BookingDetailsLink.Body
			};

			// Get Lowest price for iteniery
			$.each(iteniery.PricingOptions, function(index, value){
				if(context.lowest_fare == 0) {
//					context.lowest_fare = Math.ceil(value.Price * 100);
					context.lowest_fare = Math.round(value.Price * 100);
				} else {
					if(Math.ceil(value.Price * 100) < context.lowest_fare) {
//						context.lowest_fare =  Math.ceil(value.Price * 100);
						context.lowest_fare =  Math.round(value.Price * 100);
						context.lowest_fare_agent_id = value.Agents[0];
					}
				}
//				if(Math.ceil(value.Price * 100) < lowest_fare) {
				if(Math.round(value.Price * 100) < lowest_fare) {
//					lowest_fare = Math.ceil(value.Price * 100);
					lowest_fare = Math.round(value.Price * 100);
					$('.bestPrice span').html(accounting.formatNumber(lowest_fare));
				}
			});


			// Fetch flight details
			$.each(json.Legs, function(index, leg){
				if(leg.Id == context.out_id) {
					context.out_depart_time         = leg.Departure;
					context.out_depart_station_id   = leg.OriginStation;
					context.out_arrival_time        = leg.Arrival;
					context.out_arrival_station_id  = leg.DestinationStation;
					context.out_carrier_id          = leg.Carriers[0];
					context.out_duration            = leg.Duration;
					context.out_stops				= leg.Stops.length;
				}
				if(leg.Id == context.in_id) {
					context.in_depart_time          = leg.Departure;
					context.in_depart_station_id    = leg.OriginStation;
					context.in_arrival_time         = leg.Arrival;
					context.in_arrival_station_id   = leg.DestinationStation;
					context.in_carrier_id           = leg.Carriers[0];
					context.in_duration             = leg.Duration;
					context.in_stops				= leg.Stops.length;
				}
			});

			// Fetch Carrier Info
			$.each(json.Carriers, function(index, carrier){
				if(carrier.Id == context.out_carrier_id) {
					context.out_carrier_info = carrier;
				}
				if(carrier.Id == context.in_carrier_id) {
					context.in_carrier_info = carrier;
				}
			});

			// Fetch Station Info
			$.each(json.Places, function(index, place){
				if(place.Id == context.out_depart_station_id) {
					context.out_depart_station_info = place;
				}
				if(place.Id == context.out_arrival_station_id) {
					context.out_arrival_station_info = place;
				}
				if(place.Id == context.in_depart_station_id) {
					context.in_depart_station_info = place;
				}
				if(place.Id == context.in_arrival_station_id) {
					context.in_arrival_station_info = place;
				}
			});

			var stopsFilter = $('input[name="stopsFilter"]').val();
				console.log(stopsFilter + "Hello");
				if(stopsFilter=="" || stopsFilter==null){
					var html ='';
					html	 = '<div class="result carrier-' + context.out_carrier_id + ' carrier-' + context.in_carrier_id +'" price="' + context.lowest_fare + '" agent_id="' + context.lowest_fare_agent_id + '">';
					html	+= '<ul><li class="p"><h5>' + accounting.formatNumber(context.lowest_fare) + ' PTS</h5><form method="GET" class="doConfirm" action="/dashboard/travel/confirm_flight"><input type="hidden" name="price" value="' + context.lowest_fare + '">';
					html	+= '<input type="hidden" name="out_depart_time" value="' + context.out_depart_time + '" />';

					if( !$.isEmptyObject(context.out_carrier_info)){
						html	+= '<input type="hidden" name="out_depart_station_code" value="' + context.out_depart_station_info.Code + '" />';
						html	+= '<input type="hidden" name="out_depart_station_id" value="' + context.out_depart_station_info.Id + '" />';
						html	+= '<input type="hidden" name="out_depart_station_name" value="' + context.out_depart_station_info.Name + '" />';
						html	+= '<input type="hidden" name="out_arrive_time" value="' + context.out_arrival_time + '" />';
						html	+= '<input type="hidden" name="out_arrive_station_code" value="' + context.out_arrival_station_info.Code + '" />';
						html	+= '<input type="hidden" name="out_arrive_station_id" value="' + context.out_arrival_station_info.Id + '" />';
						html	+= '<input type="hidden" name="out_arrive_station_name" value="' + context.out_arrival_station_info.Name + '" />';
						html	+= '<input type="hidden" name="out_carrier_name" value="' + context.out_carrier_info.Name + '" />';
						html	+= '<input type="hidden" name="out_stops" value="' + context.out_stops + '" />';
						html	+= '<input type="hidden" name="out_duration" value="' + context.out_duration + '" />';
					}
//					if(!$.isEmptyObject(context.in_carrier_info)){
						html	+= '<input type="hidden" name="in_depart_time" value="' + context.in_depart_time +'" />';
						html	+= '<input type="hidden" name="in_depart_station_code" value="' + context.in_depart_station_info.Code + '" />';
						html	+= '<input type="hidden" name="in_depart_station_id" value="' + context.in_depart_station_info.Id + '" />';
						html	+= '<input type="hidden" name="in_depart_station_name" value="' + context.in_depart_station_info.Name + '" />';

						html	+= '<input type="hidden" name="in_arrive_time" value="' + context.in_arrival_time + '" />';
						html	+= '<input type="hidden" name="in_arrive_station_code" value="' + context.in_arrival_station_info.Code + '" />';
						html	+= '<input type="hidden" name="in_arrive_station_id" value="' + context.in_arrival_station_info.Id + '" />';
						html	+= '<input type="hidden" name="in_arrive_station_name" value="' + context.in_arrival_station_info.Name + '" />';

						html	+= '<input type="hidden" name="in_carrier_name" value="' + context.in_carrier_info.Name +'" />';
						html	+= '<input type="hidden" name="in_stops" value="' + context.in_stops +'" />';
						html	+= '<input type="hidden" name="in_duration" value="' + context.in_duration +'" />';
//					}
                                        html	+= '<input type="hidden" name="return" value="<?php echo Input::get('return'); ?>" />';
                                        html	+= '<input type="hidden" name="cabinclass" value="<?php echo Input::get('cabinclass'); ?>" />';
					html	+= '<button class="greenBtn enforceAuth loadDetails">Purchase</button></form></li>';
					html	+= '<li class="a">';
					html	+= '<img src="' + context.out_carrier_info.ImageUrl + '" alt="" />';
//					if(!$.isEmptyObject(context.in_carrier_info)){
						html	+= '<img src="' + context.in_carrier_info.ImageUrl + '" alt="" />';
//					}

					{// time display
						out_time_depart		=	context.out_depart_time.split("T");
                                                if(!$.isEmptyObject(context.in_depart_time)){
						in_time_depart		=	context.in_depart_time.split("T");
                                                }
                                                if(!$.isEmptyObject(context.in_arrival_time)){
                                                in_time_arrival		=	context.in_arrival_time.split("T");
                                                }
						out_time_arrival	=	context.out_arrival_time .split("T");
						

						//duration

						var hours = Math.floor( context.out_duration / 60);
						var minutes = context.out_duration % 60;
						duration_out	= hours +'H ' + minutes +'m '

						var hours = Math.floor( context.in_duration / 60);
						var minutes = context.in_duration % 60;
						duration_in	= hours +'H ' + minutes +'m '
					}

					html	+= '</li>';

					html	+= '<li class="o">';
					html	+= '<table><tr><td class="emp">Departure:</td>';
					html	+= '<td class="emp">' + context.out_depart_station_info.Code + ' ' + out_time_depart[1].split(":")[0] +":"+out_time_depart[1].split(":")[1]  + '</td>';
					html	+= '</tr><tr><td class="emp">Arrival:</td>';
					html	+= '<td class="emp">' + context.out_arrival_station_info.Code + ' ' + out_time_arrival[1].split(":")[0] +":"+out_time_arrival[1].split(":")[1]  + '</td>';
					html	+= '</tr><tr><td class="sub">Duration:</td>';
					html	+= '<td class="sub">' +  duration_out + '</td>';
					html	+= '</tr><tr><td class="sub">Stops:</td>';
					html	+= '<td class="sub" id="outStops">' + context.out_stops + '</td>';
					html	+= '</tr></table>';
					html	+= '</li>';

					if(!$.isEmptyObject(context.in_id)){
						html	+= '<li class="r">';
						html	+= '<table><tr><td class="emp">Departure:</td>';
						html	+= '<td class="emp">' + context.in_depart_station_info.Code + ' ' + in_time_depart[1].split(":")[0] +":"+in_time_depart[1].split(":")[1] + '</td>';
						html	+= '</tr><tr><td class="emp">Arrival:</td>';
						html	+= '<td class="emp">' + context.in_arrival_station_info.Code + ' ' + in_time_arrival[1].split(":")[0] +":"+in_time_arrival[1].split(":")[1]	 + '</td>';
						html	+= '</tr><tr><td class="sub">Duration:</td>';
						html	+= '<td class="sub">' +  duration_in + '</td>';
						html	+= '</tr><tr><td class="sub">Stops:</td>';
						html	+= '<td class="sub" id="outStops">' + context.in_stops + '</td>';
						html	+= '</tr></table>';
						html	+= '</li>';
					}
					html	+= '</ul>';
					html	+= '<div class="clearfix"></div>';

					html	+= '</div>';

	$('div#search-results #listing').append(html);
				}
				else{
					if(context.out_stops==stopsFilter){
							var html ='';
					html	 = '<div class="result carrier-' + context.out_carrier_id + ' carrier-' + context.in_carrier_id +'" price="' + context.lowest_fare + '" agent_id="' + context.lowest_fare_agent_id + '">';
					html	+= '<ul><li class="p"><h5>' + accounting.formatNumber(context.lowest_fare) + ' PTS</h5><form method="GET" class="doConfirm" action="/dashboard/travel/confirm_flight"><input type="hidden" name="price" value="' + context.lowest_fare + '">';
					html	+= '<input type="hidden" name="out_depart_time" value="' + context.out_depart_time + '" />';

					if( !$.isEmptyObject(context.out_carrier_info)){
						html	+= '<input type="hidden" name="out_depart_station_code" value="' + context.out_depart_station_info.Code + '" />';
						html	+= '<input type="hidden" name="out_depart_station_id" value="' + context.out_depart_station_info.Id + '" />';
						html	+= '<input type="hidden" name="out_depart_station_name" value="' + context.out_depart_station_info.Name + '" />';
						html	+= '<input type="hidden" name="out_arrive_time" value="' + context.out_arrival_time + '" />';
						html	+= '<input type="hidden" name="out_arrive_station_code" value="' + context.out_arrival_station_info.Code + '" />';
						html	+= '<input type="hidden" name="out_arrive_station_id" value="' + context.out_arrival_station_info.Id + '" />';
						html	+= '<input type="hidden" name="out_arrive_station_name" value="' + context.out_arrival_station_info.Name + '" />';
						html	+= '<input type="hidden" name="out_carrier_name" value="' + context.out_carrier_info.Name + '" />';
						html	+= '<input type="hidden" name="out_stops" value="' + context.out_stops + '" />';
						html	+= '<input type="hidden" name="out_duration" value="' + context.out_duration + '" />';
					}
//					if(!$.isEmptyObject(context.in_carrier_info)){
						html	+= '<input type="hidden" name="in_depart_time" value="' + context.in_depart_time +'" />';
						html	+= '<input type="hidden" name="in_depart_station_code" value="' + context.in_depart_station_info.Code + '" />';
						html	+= '<input type="hidden" name="in_depart_station_id" value="' + context.in_depart_station_info.Id + '" />';
						html	+= '<input type="hidden" name="in_depart_station_name" value="' + context.in_depart_station_info.Name + '" />';

						html	+= '<input type="hidden" name="in_arrive_time" value="' + context.in_arrival_time + '" />';
						html	+= '<input type="hidden" name="in_arrive_station_code" value="' + context.in_arrival_station_info.Code + '" />';
						html	+= '<input type="hidden" name="in_arrive_station_id" value="' + context.in_arrival_station_info.Id + '" />';
						html	+= '<input type="hidden" name="in_arrive_station_name" value="' + context.in_arrival_station_info.Name + '" />';

						html	+= '<input type="hidden" name="in_carrier_name" value="' + context.in_carrier_info.Name +'" />';
						html	+= '<input type="hidden" name="in_stops" value="' + context.in_stops +'" />';
						html	+= '<input type="hidden" name="in_duration" value="' + context.in_duration +'" />';
//					}

                                        html	+= '<input type="hidden" name="return" value="<?php echo Input::get('return'); ?>" />';
                                        html	+= '<input type="hidden" name="cabinclass" value="<?php echo Input::get('cabinclass'); ?>" />';
					html	+= '<button class="greenBtn enforceAuth loadDetails">Purchase</button></form></li>';
					html	+= '<li class="a">';
					html	+= '<img src="' + context.out_carrier_info.ImageUrl + '" alt="" />';
//					if(!$.isEmptyObject(context.in_carrier_info)){
						html	+= '<img src="' + context.in_carrier_info.ImageUrl + '" alt="" />';
//					}

					{// time display
						out_time_depart		=	context.out_depart_time.split("T");
                                                 if(!$.isEmptyObject(context.in_depart_time)){
						in_time_depart		=	context.in_depart_time.split("T");
                                                }
						out_time_arrival	=	context.out_arrival_time .split("T");
                                                if(!$.isEmptyObject(context.in_arrival_time)){
						in_time_arrival		=	context.in_arrival_time.split("T");
                                                }
						//duration

						var hours = Math.floor( context.out_duration / 60);
						var minutes = context.out_duration % 60;
						duration_out	= hours +'H ' + minutes +'m '

						var hours = Math.floor( context.in_duration / 60);
						var minutes = context.in_duration % 60;
						duration_in	= hours +'H ' + minutes +'m '
					}

					html	+= '</li>';

					html	+= '<li class="o">';
					html	+= '<table><tr><td class="emp">Departure:</td>';
					html	+= '<td class="emp">' + context.out_depart_station_info.Code + ' ' + out_time_depart[1].split(":")[0] +":"+out_time_depart[1].split(":")[1]  + '</td>';
					html	+= '</tr><tr><td class="emp">Arrival:</td>';
					html	+= '<td class="emp">' + context.out_arrival_station_info.Code + ' ' + out_time_arrival[1].split(":")[0] +":"+out_time_arrival[1].split(":")[1]  + '</td>';
					html	+= '</tr><tr><td class="sub">Duration:</td>';
					html	+= '<td class="sub">' +  duration_out + '</td>';
					html	+= '</tr><tr><td class="sub">Stops:</td>';
					html	+= '<td class="sub" id="outStops">' + context.out_stops + '</td>';
					html	+= '</tr></table>';
					html	+= '</li>';

					if(!$.isEmptyObject(context.in_id)){
						html	+= '<li class="r">';
						html	+= '<table><tr><td class="emp">Departure:</td>';
						html	+= '<td class="emp">' + context.in_depart_station_info.Code + ' ' + in_time_depart[1].split(":")[0] +":"+in_time_depart[1].split(":")[1] + '</td>';
						html	+= '</tr><tr><td class="emp">Arrival:</td>';
						html	+= '<td class="emp">' + context.in_arrival_station_info.Code + ' ' + in_time_arrival[1].split(":")[0] +":"+in_time_arrival[1].split(":")[1]	 + '</td>';
						html	+= '</tr><tr><td class="sub">Duration:</td>';
						html	+= '<td class="sub">' +  duration_in + '</td>';
						html	+= '</tr><tr><td class="sub">Stops:</td>';
						html	+= '<td class="sub" id="outStops">' + context.in_stops + '</td>';
						html	+= '</tr></table>';
						html	+= '</li>';
					}
					html	+= '</ul>';
					html	+= '<div class="clearfix"></div>';

					html	+= '</div>';

	$('div#search-results #listing').append(html);
					}
				}
	}
}

$(function(){

	$(document).on('change', '#frmPollFlights select, #frmPollFlights input[type="text"]', function(){
		poll_flights();
	});
	// stops filter
        $(document).on('change', 'select[name="stops"]', function(e){
            $('input[name="stopsFilter"]').val($(this).val());
            console.log($('input[name="stopsFilter"]').val() + "-----11");

            poll_flights();
        });

		$(document).on('click', '.sort-results a', function(e){
		e.preventDefault();
		$('input[name="sorttype"]').val($(this).data('value'));
		$('.sort-results a').removeClass('active');
		$(this).addClass('active');
		poll_flights();
	});

	$(document).on('click', '.result .showDetails', function(e){
		e.preventDefault();

		_frm = $(this).closest('form');

		$.post('/travel/flight_detail', _frm.serialize(), function(resp){
			console.log(resp);
		});
	});

	$(document).on('click', '#travel-pagination a', function(e){
		e.preventDefault();

		$('input[name="pageindex"]').val($(this).data('page'));

		$('#travel-pagination a').removeClass('active');

		$(this).addClass('active');

		poll_flights();
	});

	$(document).on('change', '.doSelectAirlines input', function(e){
		e.preventDefault();

		if($(this).is(':checked')) {
			$('#listing div.carrier-'+$(this).val()).fadeIn();
		} else {
			$('#listing div.carrier-'+$(this).val()).fadeOut();
		}
	});

	poll_flights();
});
</script>
@stop