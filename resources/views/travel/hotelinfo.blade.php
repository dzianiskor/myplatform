@extends('layouts.dashboard')

@section('title')
    Support Ticket | BLU
@stop
<?php
	$userId		= $user_id;
	$hotelData	= json_encode($results);
	$hotelDet	= json_decode($hotelData, true);
	$hotel		= $hotelDet['HotelInformationResponse'];
	$rooms		= $hotelDet['HotelRoomAvailabilityResponse'];

//	echo "<pre>";
//	print_r($hotelDet);
//	exit;
?>
@section('body')
	<div id="hotel-page" class="travel-detail-page container">
	<div class="summary-bar">
		<div class="container">
			Hotel Search > Hotel Info
		</div>
	</div>
	<div class="blue-box">

		<div class="title-rail">
			<h1>
				<?php echo $hotel['HotelSummary']['name']; ?> <div class="star<?php echo round($hotel['HotelSummary']['hotelRating']); ?>"></div>
			</h1>
			<p>
				<?php echo $hotel['HotelSummary']['address1']; ?>

				<?php if(isset($hotel['HotelSummary']['address2'])): ?>
					, <?php echo $hotel['HotelSummary']['address2']; ?>
				<?php endif; ?>

                                , <?php echo $hotel['HotelSummary']['city']; ?>, <?php if (array_key_exists('postalCode', $hotel)) {echo $hotel['HotelSummary']['postalCode'];} ?>
			</p>
		</div>

		<div class="floated">
			<div class="gallery">

				<div class="preview">
					<img src="" alt="" class="preview" />
				</div>

				<div class="thumbnails">
					<?php foreach($hotel['HotelImages']['HotelImage'] as $image): ?>
						<img src="<?php echo $image['thumbnailUrl']; ?>" alt="" data-full="<?php echo $image['url']; ?>" />
					<?php endforeach; ?>
				</div>

				<div class="clearfix"></div>

			</div>

			<div class="tripadvisor">
				<p>
					<span class="emp"><?php echo $hotel['HotelSummary']['hotelRating']; ?></span> out of 5 Expedia Guest Rating
				</p>
                                <?php if(isset($hotel['HotelSummary']['tripAdvisorRatingUrl'])): ?>
				<p>
					<img src="<?php echo $hotel['HotelSummary']['tripAdvisorRatingUrl']; ?>" />
				</p>
                                <?php if(isset($hotel['HotelSummary']['tripAdvisorReviewCount'])): ?>
				<p>
					TripAdvisor Traveler Rating Based on <?php echo $hotel['HotelSummary']['tripAdvisorReviewCount']; ?> reviews.
				</p>
                                <?php endif; ?>
                                <?php endif; ?>
			</div>
			<div class="clearfix"></div>
		</div>

	</div>

	<div class="availability">
			<h2>Available Rooms</h2>

			<?php

				if($rooms['@size'] == 1) {
					$rooms['HotelRoomResponse'] = array($rooms['HotelRoomResponse']);
				}
			?>

			<?php foreach($rooms['HotelRoomResponse'] as $a): ?>
					<?php
						$promo		= $a['RateInfos']['RateInfo']['@promo'];
						$roomTypeCode = $a['roomTypeCode'];
						$roomTypes = $hotel['RoomTypes']['RoomType'];
						// Find roomType from $hotel
						foreach($roomTypes as $b):
							if(isset($b['@roomCode']) && $b['@roomCode']==$roomTypeCode){
								$roomType = $b;
								break;
							}
						endforeach;
					?>
				<div class="<?php echo ($promo == 'true') ? 'room-promo' : 'room'; ?>">
					<h3>
						<?php echo $a['rateDescription']; ?>
					</h3>

					<p>
						Max Occupancy: <?php echo $a['rateOccupancyPerRoom'];
									// add the label of Refundable or non - refundable based on the returned flag <fboukarroum>
									if ( $a['RateInfos']['RateInfo']['nonRefundable'] )
										echo "<br> Non-Refundable";
									else
										echo "<br> Free Cancellation";
									 // add the label of value add description that includes the info of breakfast <fboukarroum>
									if ( isset($a['ValueAdds']['@size']) )
									{
										$siz=$a['ValueAdds']['@size'];
									}
									if ( isset($a['ValueAdds']['ValueAdd']) )
									{

										if ($siz > 1)
										{
										 foreach ($a['ValueAdds']['ValueAdd'] as $key ):
											  echo "<br> ".$key['description'];
										 endforeach;
										}
										if ($siz == 1)
										{
										 echo "<br> ".$a['ValueAdds']['ValueAdd']['description'];
										}
									}
										?>
					</p>

					<a href="#" title="More Info &raquo;" class="info">Show More Information &raquo;</a>

					<div class="hidden">
						<div class="grey">
							<p>
								<?php echo $roomType['descriptionLong']; ?>
							</p>

							<ul>
								<?php foreach($roomType['roomAmenities']['RoomAmenity'] as $r): ?>
									<li><?php echo $r['amenity']; ?></li>
								<?php endforeach; ?>
							</ul>
						</div>
					</div>

					<div class="pricing">
						<?php
	//                                            echo "<pre>";
	//                                                var_dump($a);exit();


							if($promo == 'true'){
								$points			= ($a['RateInfos']['RateInfo']['ChargeableRateInfo']['@averageRate'] / 0.01);
							}else{
								$points			= ($a['RateInfos']['RateInfo']['ChargeableRateInfo']['@averageBaseRate'] / 0.01);
							}

							$surchargeTotal	= ($a['RateInfos']['RateInfo']['ChargeableRateInfo']['@surchargeTotal'] / 0.01);
							$hotelFees		= 0;

							if( isset( $a['RateInfos']['RateInfo']['HotelFees']['HotelFee'][0]['@amount'] ) ){
								$size			= $a['RateInfos']['RateInfo']['HotelFees']['@size'];
								if( $size > 1 ){
									$tax1	= $a['RateInfos']['RateInfo']['HotelFees']['HotelFee'][1]['@amount'] / $size;
									$hotelFees = ($a['RateInfos']['RateInfo']['HotelFees']['HotelFee'][0]['@amount'] + $tax1) / 0.01;
								}else{
									$hotelFees		= ($a['RateInfos']['RateInfo']['HotelFees']['HotelFee'][0]['@amount'] / 0.01);
								}
							}
							elseif(isset( $a['RateInfos']['RateInfo']['HotelFees']['HotelFee']['@amount'] ) ){
								$hotelFees		= ($a['RateInfos']['RateInfo']['HotelFees']['HotelFee']['@amount'] / 0.01);
							}

							if (array_key_exists('postalCode', $hotel))
							{
								$url = array(
										'roomCode'			=> $a['roomTypeCode'],
										'points'			=> $points,
										'surchargeTotal'	=> $surchargeTotal,
										'hotelFees'			=> $hotelFees,
										'arrivalDate'		=> Input::get('arrivalDate'),
										'departureDate'		=> Input::get('departureDate'),
										'hotelId'			=> $hotel['@hotelId'],
										'hotelName'			=> $hotel['HotelSummary']['name'],
										'hotelAddress1'		=> $hotel['HotelSummary']['address1'],
										'city'				=> $hotel['HotelSummary']['city'],
										'postalCode'		=> $hotel['HotelSummary']['postalCode'],
										'countryCode'		=> $hotel['HotelSummary']['countryCode'],
										'rooms'				=> Input::get('rooms'),
										'img'				=> $hotel_image,
										'rateKey'			=> $a['RateInfos']['RateInfo']['RoomGroup']['Room']['rateKey'],
										'rateCode'			=> $a['rateCode'],
										'chargeableRate'	=> $a['RateInfos']['RateInfo']['ChargeableRateInfo']['@total'],
										'numberOfAdults'	=> Input::get('numberOfAdults'),
										'numberOfChildren'	=> Input::get('numberOfChildren'),
										'numberOfInfants'	=> Input::get('numberOfInfants')
								);
							}else{
								$url = array(
										'roomCode'			=> $a['roomTypeCode'],
										'points'			=> $points,
										'surchargeTotal'	=> $surchargeTotal,
										'hotelFees'			=> $hotelFees,
										'arrivalDate'		=> Input::get('arrivalDate'),
										'departureDate'		=> Input::get('departureDate'),
										'hotelId'			=> $hotel['@hotelId'],
										'hotelName'			=> $hotel['HotelSummary']['name'],
										'hotelAddress1'		=> $hotel['HotelSummary']['address1'],
										'city'				=> $hotel['HotelSummary']['city'],
										'countryCode'		=> $hotel['HotelSummary']['countryCode'],
										'rooms'				=> Input::get('rooms'),
										'img'				=> $hotel_image,
										'rateKey'			=> $a['RateInfos']['RateInfo']['RoomGroup']['Room']['rateKey'],
										'rateCode'			=> $a['rateCode'],
										'chargeableRate'	=> $a['RateInfos']['RateInfo']['ChargeableRateInfo']['@total'],
										'numberOfAdults'	=> Input::get('numberOfAdults'),
										'numberOfChildren'	=> Input::get('numberOfChildren'),
										'numberOfInfants'	=> Input::get('numberOfInfants')
								);
							}

							if(isset($hotel['HotelSummary']['address2'])) {
								$url['hotelAddress2'] = $hotel['HotelSummary']['address2'];
							} else {
								$url['hotelAddress2'] = '';
							}

							$url['user_id'] = $userId;
						?>
						<span>
							<?php echo number_format($points); ?> Pts<!--
							<?php if($promo == 'true'){ ?><br/><label class="promo-label">Promo</label> <?php }?>
												   -->
						</span>
						<a href="/dashboard/travel/confirm_hotel?<?php echo http_build_query($url); ?>" class="greenBtn enforceAuth" title="Book">Book</a>

					</div>

				</div>
			<?php endforeach; ?>

		</div>

		<div class="map-directions">
			<h2>Map &amp; Directions</h2>

			<div id="hotel-map"></div>

			<div class="area-information">
				<h3>Area Information</h3>

				<?php echo html_entity_decode($hotel['HotelDetails']['areaInformation']); ?>
			</div>

		</div>

		<div class="hotel-details">
			<h2>Hotel Details</h2>

			<div class="wrap">
				<div class="detail-title">
					Hotel Ameneties
				</div>

				<div class="hotel-detail-item">
					<ul>
						<?php foreach($hotel['PropertyAmenities']['PropertyAmenity'] as $i): ?>
							<li><?php echo $i['amenity']; ?></li>
						<?php endforeach; ?>
					</ul>
					<div class="clearfix"></div>
				</div>

				<div class="clearline"></div>


				<div class="detail-title">
					Know Before You Go
				</div>

				<div class="hotel-detail-item">
					<p>
						<?php echo trim(strip_tags($hotel['HotelDetails']['knowBeforeYouGoDescription'])); ?>
					</p>
				</div>

				<div class="clearline"></div>


				<div class="detail-title">
					Rooms
				</div>

				<div class="hotel-detail-item">
					<p>
						<?php echo trim($hotel['HotelDetails']['roomDetailDescription']); ?>
					</p>
				</div>

				<div class="clearline"></div>

				<div class="detail-title">
					Business, Other Amenities
				</div>

				<div class="hotel-detail-item">
					<p>
						<?php echo trim($hotel['HotelDetails']['businessAmenitiesDescription']); ?>
					</p>
				</div>

				<div class="clearfix"></div>

			</div>

		</div>

	</div>
@stop

@section('page_script')
<script>
	$(function(){

		$('img.preview').attr('src', $('.thumbnails img').first().data('full'));

		$(document).on('click', '.thumbnails img', function(){
			$('img.preview').attr('src', $(this).data('full'));
		});

		$(document).on('click', '.info', function(e){
			e.preventDefault();

			$(this).closest('div.room').find('div.hidden').removeClass('hidden');
		});

		// Show the Maps
		var map = new GMaps({
		  div: '#hotel-map',
		  lat: <?php echo $hotel['HotelSummary']['latitude']; ?>,
		  lng: <?php echo $hotel['HotelSummary']['longitude']; ?>
		});

		map.addMarker({
		  lat: <?php echo $hotel['HotelSummary']['latitude']; ?>,
		  lng: <?php echo $hotel['HotelSummary']['longitude']; ?>,
		  title: "<?php echo $hotel['HotelSummary']['name']; ?>"
		});
	});
</script>
@stop