@extends('layouts.dashboard')

@section('title')
    Support Ticket | BLU
@stop

@section('body')
<div id="car-page" class="travel-search-page container">
	<div class="summary-bar">
		<div class="container">
			Cars
		</div>
	</div>
    <div class="pure-g">
        <div class="pure-u-1-3">
        	<div class="padding-right-20 block-labels">
				<form id="frmPoll" class="pure-form" accept-charset="utf-8" method="post" action="#">
				<h3>Vehicle Type <span>Select All <input type="checkbox" id="selecctallType" checked="checked" /> </span></h3>
	            	<div class="section">
	            		<label>
	            			<input value="gMini" name="vehicletype[]" type="checkbox" checked="checked" class="checkboxType"> Mini cars
	            		</label>
	            		<label>
	            			<input value="gEconomy" name="vehicletype[]" type="checkbox" checked="checked" class="checkboxType"> Economy cars
	            		</label>
	            		<label>
	            			<input value="gCompact" name="vehicletype[]" type="checkbox" checked="checked" class="checkboxType"> Compact cars
	            		</label>
	            		<label>
	            			<input value="gIntermediate" name="vehicletype[]" type="checkbox" checked="checked" class="checkboxType"> Intermediate cars
	            		</label>
	            		<label>
	            			<input value="gStandard" name="vehicletype[]" type="checkbox" checked="checked" class="checkboxType"> Standard cars
	            		</label>
	            		<label>
	            			<input value="gFull" name="vehicletype[]" type="checkbox" checked="checked" class="checkboxType"> Full-size cars
	            		</label>
	            		<label>
	            			<input value="gPremium" name="vehicletype[]" type="checkbox" checked="checked" class="checkboxType"> SUVs
	            		</label>
	            		<label>
	            			<input value="gSpecial" name="vehicletype[]" type="checkbox" checked="checked" class="checkboxType"> 7 seat people carriers
	            		</label>
	            		<label>
	            			<input value="gLuxury" name="vehicletype[]" type="checkbox" checked="checked" class="checkboxType"> Luxury
	            		</label>
	            	</div>

					<h3>Supplier Location <span>Select All <input type="checkbox" id="selecctallLocation" checked="checked" /> </span></h3>

	            	<div class="section">
	            		<label>
		            		<label>
		            			<input type="checkbox" name="location[]" value="airportYes" checked="checked" class="checkboxLocation"> On Airport
		            		</label>
		            		<label>
		            			<input type="checkbox" name="location[]" value="airportNo" checked="checked" class="checkboxLocation"> Away from Airport
		            		</label>
	            		</label>
	            	</div>

	        		<h3>Fuel Options <span>Select All <input type="checkbox" id="selecctallFuel" checked="checked" /> </span></h3>

	            	<div class="section">
	            		<label>
	            			<input type="checkbox" name="fuel[]" value="fp1" checked="checked" class="checkboxFuel" /> Full to Full
	            		</label>
	            		<label>
	            			<input type="checkbox" name="fuel[]" value="fp2" checked="checked" class="checkboxFuel" /> Prepay, w/ refunds
	            		</label>
	            		<label>
	            			<input type="checkbox" name="fuel[]" value="fp3" checked="checked" class="checkboxFuel" /> Prepay, no refunds
	            		</label>
	            		<label>
	            			<input type="checkbox" name="fuel[]" value="fp4" checked="checked" class="checkboxFuel" /> Free Tank
	            		</label>
	            	</div>

	        		<h3>Car Specifications <span>Select All <input type="checkbox" id="selecctallSpec" checked="checked" /> </span></h3>

	            	<div class="section">
	            		<label>
	            			<input type="checkbox" name="specs[]" value="airconYes" checked="checked" class="checkboxSpec" /> With Air-conditioning
	            		</label>
	            		<label>
	            			<input type="checkbox" name="transmission[]" value="tAuto" checked="checked" class="checkboxSpec" /> Automatic Transmission
	            		</label>
	            	</div>
				</form>
	            <?php // echo form_close(); ?>

            </div>

        </div>

        <div class="pure-u-2-3">

            <div class="title-rail">
	        	<h1>
					Car Rental &raquo; <?php echo Input::get('city'); ?><br />
	        	</h1>

	        	<p class="trip-date">
	        		<?php echo date("F j, Y - g:i:s", strtotime(Input::get('pickup'))); ?> - <?php echo date("F j, Y - g:i:s", strtotime(Input::get('dropoff'))); ?>
	        	</p>

	        	<a href="/travel" title="Change Search" class="redBtn">Change Search</a>
        	</div>

        	<div id="search-results">
                        <?php if(isset($cars['MatchList'])){$cars = $cars['MatchList'];} ?>
        		<?php if(isset($cars['Match']) and (count($cars['Match']) > 0)): ?>
		        	<div id="listing">
		        	<?php foreach($cars['Match'] as $car): ?>

		        		<?php

		        			// Car Class

    						$group = '';
    						$groupClass = '';

    						if(preg_match('/((X...)|(.V..)).*/', $car['Vehicle']['@attributes']['group'])) {
    							$group = "Special";
    							$groupClass = "gSpecial";
    						}

    						if(preg_match('/[EM][^V].*/', $car['Vehicle']['@attributes']['group'])) {
    							$group = "Mini";
    							$groupClass = "gMini";
    						}

    						if(preg_match('/[E][^V].*/', $car['Vehicle']['@attributes']['group'])) {
    							$group = "Economy";
    							$groupClass = "gEconomy";
    						}

    						if(preg_match('/[C][^V].*/', $car['Vehicle']['@attributes']['group'])) {
    							$group = "Compact";
    							$groupClass = "gCompact";
    						}

    						if(preg_match('/[I][^V].*/', $car['Vehicle']['@attributes']['group'])) {
    							$group = "Midsize";
    							$groupClass = "gMidsize";
    						}

    						if(preg_match('/[I][^V].*/', $car['Vehicle']['@attributes']['group'])) {
    							$group = "Intermediate";
    							$groupClass = "gIntermediate";
    						}

    						if(preg_match('/[S][^V].*/', $car['Vehicle']['@attributes']['group'])) {
    							$group = "Standard";
    							$groupClass = "gStandard";
    						}

    						if(preg_match('/[F][^V].*/', $car['Vehicle']['@attributes']['group'])) {
    							$group = "Full";
    							$groupClass = "gFull";
    						}

    						if(preg_match('/[P][^V].*/', $car['Vehicle']['@attributes']['group'])) {
    							$group = "Premium";
    							$groupClass = "gPremium";
    						}

    						if(preg_match('/[L][^V].*/', $car['Vehicle']['@attributes']['group'])) {
    							$group = "Luxury";
    							$groupClass = "gLuxury";
    						}

    						// Location

    						$onAirport = '';
    						$onAirportClass = '';

    						if($car['Route']['PickUp']['Location']['@attributes']['onAirport'] == 'yes') {
    							$onAirport = "On Airport";
    							$onAirportClass = 'airportYes';
    						} else {
    							$onAirport = "Not On Airport";
    							$onAirportClass = 'airportNo';
    						}

    						// Fuel policy

    						$fuelPolicy = '';
    						$fuelPolicyClass = '';

    						if($car['Vehicle']['@attributes']['fuelPolicy'] == 0) {
    							$fuelPolicy = 'Unknown';
    							$fuelPolicyClass = 'fp0';
    						}

    						if($car['Vehicle']['@attributes']['fuelPolicy'] == 1) {
    							$fuelPolicy = 'Full to Full';
    							$fuelPolicyClass = 'fp1';
    						}

    						if($car['Vehicle']['@attributes']['fuelPolicy'] == 2) {
    							$fuelPolicy = 'Prepay, no refunds';
    							$fuelPolicyClass = 'fp2';
    						}

    						if($car['Vehicle']['@attributes']['fuelPolicy'] == 3) {
    							$fuelPolicy = 'Prepay, with refunds';
    							$fuelPolicyClass = 'fp3';
    						}

    						if($car['Vehicle']['@attributes']['fuelPolicy'] == 4) {
    							$fuelPolicy = 'Free Tank';
    							$fuelPolicyClass = 'fp4';
    						}

    						// Transmission

    						$transmission = '';
    						$transmissionClass= '';

    						if($car['Vehicle']['@attributes']['automatic'] == 'Automatic') {
    							$transmission = 'Automatic';
    							$transmissionClass = 'tAuto';
    						} else {
    							$transmission = 'Manual';
    							$transmissionClass = 'tManual';
    						}

    						$aircon = '';
    						$airconClass = '';

    						if($car['Vehicle']['@attributes']['aircon'] == 'no') {
    							$aircon = 'No';
    							$airconClass = 'airconNo';
    						} else {
    							$aircon = 'Yes';
    							$airconClass = 'airconYes';
    						}

		        		?>

		        		<div class="result <?php echo $groupClass; ?> <?php echo $fuelPolicyClass; ?> <?php echo $onAirportClass; ?> <?php echo $transmissionClass; ?> <?php echo $airconClass; ?>">

		        			<div class="car-image">
								<img src="<?php echo $car['Vehicle']['ImageURL']; ?>" alt="" style="width: 120px;" />
		        			</div>

		        			<div class="car-detail">
		        				<h1><?php echo $car['Vehicle']['Name']; ?> or Similar</h1>
		        				<p>
		        					<?php echo $car['Route']['PickUp']['Location']['@attributes']['locName']; ?>

		        					<a href="http://maps.google.com/maps?q=<?php echo $car['Supplier']['@attributes']['lat']; ?>,<?php echo $car['Supplier']['@attributes']['long']; ?>" target="_blank" title="View on Google Maps">(View on Google Maps)</a>
		        				</p>

		        				<table>
			        				<tr>
			        					<td style="width:120px;">Class</td>
			        					<td><?php echo $group; ?></td>
			        				</tr>
			        				<tr>
			        					<td>Location</td>
			        					<td><?php echo $onAirport; ?></td>
			        				</tr>
			        				<tr>
			        					<td>Fuel Option</td>
							        	<td><?php echo $fuelPolicy; ?></td>
			        				</tr>
			        				<tr>
			        					<td>Capacity</td>
			        					<td><?php echo $car['Vehicle']['@attributes']['seats']; ?></td>
			        				</tr>
			        				<tr>
			        					<td>Doors</td>
			        					<td><?php echo $car['Vehicle']['@attributes']['doors']; ?></td>
			        				</tr>
			        				<tr>
			        					<td>Aircon</td>
			        					<td><?php echo $aircon; ?></td>
			        				</tr>
			        				<tr>
			        					<td>Transmission</td>
			        					<td><?php echo $transmission; ?></td>
			        				</tr>
			        				<tr>
										<td colspan="2">Terms & Conditions Apply</td>
			        				</tr>
		        				</table>
		        			</div>

		        			<div class="car-booking">
		        				<span><?php echo number_format(intval(trim($car['Price']) * 102)); ?> Points</span>

								<form id="bookingForm" accept-charset="utf-8" method="post" action="/dashboard/travel/confirm_car">
		        					<input type="hidden" name="points" value="<?php echo (int)(trim($car['Price']) * 102); ?>" />
		        					<input type="hidden" name="vehicleId" value="<?php echo $car['Vehicle']['@attributes']['id']; ?>" />
		        					<input type="hidden" name="img" value="<?php echo $car['Vehicle']['ImageURL']; ?>" />
		        					<input type="hidden" name="pickup" value="<?php echo Input::get('pickup'); ?>" />
		        					<input type="hidden" name="dropoff" value="<?php echo Input::get('dropoff'); ?>" />
	        						<input type="hidden" name="pickup_country" value="<?php echo Input::get('country'); ?>" />
	        						<input type="hidden" name="pickup_city" value="<?php echo Input::get('city'); ?>" />
	        						<input type="hidden" name="pickup_location" value="<?php echo Input::get('location'); ?>" />
									<input type="hidden" name="pickup_location_name" value="<?php echo $car['Route']['PickUp']['Location']['@attributes']['locName']; ?>" />
									<input type="hidden" name="dropoff_location_name" value="<?php echo $car['Route']['DropOff']['Location']['@attributes']['locName']; ?>" />
	        						<input type="hidden" name="vehicle" value="<?php echo $car['Vehicle']['Name']; ?>" />

		        					<?php if(Input::get('sameCarLocation')): ?>
		        						<input type="hidden" name="dropoff_country" value="<?php echo Input::get('country'); ?>" />
		        						<input type="hidden" name="dropoff_city" value="<?php echo Input::get('city'); ?>" />
		        						<input type="hidden" name="dropoff_location" value="<?php echo Input::get('location'); ?>" />
		        					<?php else: ?>
		        						<input type="hidden" name="dropoff_country" value="<?php echo Input::get('country_drop'); ?>" />
		        						<input type="hidden" name="dropoff_city" value="<?php echo Input::get('city_drop'); ?>" />
		        						<input type="hidden" name="dropoff_location" value="<?php echo Input::get('location_drop'); ?>" />
		        					<?php endif; ?>

		        					<a href="#" title="Book Now" class="doBookNow greenBtn enforceAuth">Book Now</a>

		        					<!--<a href="/pages/car_terms" target="_blank" title="Terms &amp; Conditions" class="terms">Terms &amp; Conditions</a>-->
								</form>
		        			</div>

		        			<div class="clearfix"></div>

		        		</div>

		        	<?php endforeach; ?>
		        	</div>
	        	<?php else: ?>
		    		<div id="listing">
		    			<span class="loading">No cars matching your criteria could be located.</span>
		    		</div>
	        	<?php endif; ?>

        	</div>

        </div>
    </div>

</div>
@stop

@section('page_script')
<script>
	$(function(){

		// add select / deselect all for vahicle type
		$('#selecctallType').click(function() {  //on click
			if(this.checked) { // check select status
				$('.checkboxType').each(function() { //loop through each checkbox
					this.checked = true;  //select all checkboxes with class "checkboxRate"
				});
			$('.result').fadeIn();
			}else{
				$('.checkboxType').each(function() { //loop through each checkbox
					this.checked = false; //deselect all checkboxes with class "checkboxRate"
				});
				$('.result').fadeOut();
			}



		});

		// add select / deselect all for supplier location
		$('#selecctallLocation').click(function() {  //on click
			if(this.checked) { // check select status
				$('.checkboxLocation').each(function() { //loop through each checkbox
					this.checked = true;  //select all checkboxes with class "checkboxRate"
				});
				$('.result').fadeIn();
			}else{
				$('.checkboxLocation').each(function() { //loop through each checkbox
					this.checked = false; //deselect all checkboxes with class "checkboxRate"
				});
				$('.result').fadeOut();
			}
		});

		// add select / deselect all for supplier location
		$('#selecctallFuel').click(function() {  //on click
			if(this.checked) { // check select status
				$('.checkboxFuel').each(function() { //loop through each checkbox
					this.checked = true;  //select all checkboxes with class "checkboxRate"
				});
				$('.result').fadeIn();
			}else{
				$('.checkboxFuel').each(function() { //loop through each checkbox
					this.checked = false; //deselect all checkboxes with class "checkboxRate"
				});
				$('.result').fadeOut();
			}
		});

		// add select / deselect all for supplier location
		$('#selecctallSpec').click(function() {  //on click
			if(this.checked) { // check select status
				$('.checkboxSpec').each(function() { //loop through each checkbox
					this.checked = true;  //select all checkboxes with class "checkboxRate"
				});
				$('.result').fadeIn();
			}else{
				$('.checkboxSpec').each(function() { //loop through each checkbox
					this.checked = false; //deselect all checkboxes with class "checkboxRate"
				});
				$('.result').fadeOut();
			}
		});

		$(document).on('change', 'input[type="checkbox"]', function(){
			if($(this).is(':checked')) {
				$('.'+$(this).val()).fadeIn();
			} else {
				$('.'+$(this).val()).fadeOut();
			}
		});

		$(document).on('click', 'a.doBookNow', function(e){
			e.preventDefault();
//			$('#bookingForm').submit();
			$(this).closest('form').submit();
		});
	});
</script>
@stop