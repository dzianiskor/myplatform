<div id="simple-product-view" style="width:100%" class="simple-product-view-rtl">
	@if($products)
		<div class="spacer"></div>

		<table class="pure-table" style="width:100%">
			<thead>
				<tr>
					<th><?php echo $languageKeysCtrl["call-center-view-product-name"]; ?></th>
					<th><?php echo $languageKeysCtrl["call-center-view-product-brand"]; ?></th>
					<th><?php echo $languageKeysCtrl["call-center-view-product-model"]; ?></th>
                    <th><?php echo $languageKeysCtrl["call-center-view-product-points-and-delivery"]; ?></th>
                    <th><?php echo $languageKeysCtrl["call-center-view-product-add-item"]; ?></th>
				</tr>
			</thead>
			<tbody>
            @foreach($products as $product)
				<tr>
					<td>
						{{ $product->name }}
					</td>
					<td>
						@if($product->brand)
							{{{ $product->brand->name }}}
						@else
							<i>N/A</i>
						@endif
					</td>
					<td>
						{{ $product->model }}
					</td>
					<td>
						<?php
							$originalPriceInPoints1	= PointsHelper::productPriceInPoints($product->price_in_points, $product->original_price, $product->currency_id, $network_id);
							$originalPriceInPoints	= round($originalPriceInPoints1 + ($product->original_sales_tax * $originalPriceInPoints1 / 100));
							$deliveryPriceInPoints	= PointsHelper::deliveryPriceInPoints($product->delivery_charges, $product->delivery_currency_id, $network_id);
						?>
						{{ number_format(intval($originalPriceInPoints) + intval($deliveryPriceInPoints)) }}
					</td>
                    <td>
                        @if($product->qty > 0)
                            <button class="pure-button pure-button-primary redeem-item-action" data-item="{{ $product->id }}">
                                <?php echo $languageKeysCtrl["call-center-view-product-confirm"]; ?>
                            </button>
                        @else
                            <button class="pure-button pure-button-primary redeem-item-action" data-item="{{ $product->id }}" disabled="disabled">
                                <?php echo $languageKeysCtrl["call-center-view-product-out-of-stock"]; ?>
                            </button>
                        @endif
                    </td>
				</tr>
            @endforeach
			</tbody>
		</table>

		<div class="spacer"></div>

	@else
		<span class="not-found"><?php echo $languageKeysCtrl["call-center-view-product-no-product-with-that-reference-id-could-be-found"]; ?></span>
	@endif
</div>