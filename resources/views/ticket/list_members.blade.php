<?php
$count_users = 0;
if(is_object($users)){
    $count_users = $users->count();
}
elseif(is_array($users)){
    $count_users = count($users);
}

?>

@if($count_users == 0)
	<li>No Tickets Found</li>
@else
	@foreach($users as $u)
        <?php
            $userName = $u->last_name . ", " . $u->first_name;
            if($userName == "na, na" || $userName == ", "){
                $userName = "N/A";
            }
        ?>
		<li><a href="#" data-id="{{ base64_encode($u->user_id) }}" title="">{{ $userName }}</a></li>
	@endforeach
@endif
