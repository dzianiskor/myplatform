<div class="entity-profile-page">

	{{ Form::open(array('url' => url('dashboard/call_center/update_partner/'.base64_encode($partner->id)), 'method' => 'post', 'class' => "pure-form pure-form-stacked form-dynamic")) }}

		<input type="hidden" name="ticket_id" value="{{ $ticket_id }}" />

		<fieldset>
			<legend><?php echo $languageKeysCtrl["call-center-view-patner-partner-details"]; ?></legend>

            {{ Form::label('status', $languageKeysCtrl["call-center-view-patner-unique-status"] }}
            {{ Form::select('status', Meta::partnerStatuses(), $partner->status, array('class' => 'pure-input-1-3')) }}

	    	{{ Form::label('ucid', $languageKeysCtrl["call-center-view-patner-unique-customer-id"].':' }}
	    	{{ Form::text('ucid', $partner->ucid, array('class' => "pure-input-1", 'disabled' => 'disabled')) }}

	        {{ Form::label('name', $languageKeysCtrl["call-center-view-patner-name"].':' }}
	        {{ Form::text('name', $partner->name, array('class' => "pure-input-1")) }}

	        {{ Form::label('description', $languageKeysCtrl["call-center-view-patner-description"].':' }}
	        {{ Form::textarea('description', $partner->description, array('class' => "pure-input-1")) }}

	        <div class="pure-g">
	            <div class="pure-u-1-3 pure-u-med-1-2">
		        	{{ Form::label('contact_name', $languageKeysCtrl["call-center-view-patner-contact-name"].':' }}
		        	{{ Form::text('contact_name', $partner->contact_name, array('class' => "pure-input-1")) }}
	            </div>
	            <div class="pure-u-1-3 pure-u-med-1-2">
	            	<div class="padding-left-10">
			        	{{ Form::label('contact_email', $languageKeysCtrl["call-center-view-patner-contact-email"].':' }}
			        	{{ Form::text('contact_email', $partner->contact_email, array('class' => "pure-input-1")) }}
			        </div>
	            </div>
	            <div class="pure-u-1-3 pure-u-med-1-2">
	            	<div class="padding-left-10">
			        	{{ Form::label('contact_number', $languageKeysCtrl["call-center-view-patner-contact-number"].':' }}
			        	{{ Form::text('contact_number', $partner->contact_email, array('class' => "pure-input-1")) }}
			        </div>
	            </div>
	        </div>
        </fieldset>

        - Form Buttons -
        <div class="form-buttons">
            <div style="text-align:right;">
            	@if(I::can('edit_profiles'))
                	<a href="#" id="update-profile" class="pure-button pure-button-primary">
						<?php echo $languageKeysCtrl["call-center-view-patner-update-profile"]; ?>
					</a>
                @endif
            </div>
            <div class="clearfix"></div>
        </div>
        - /Form Buttons -

		<div class="spacer"></div>

        <legend><?php echo $languageKeysCtrl["call-center-view-patner-recent-transactions"]; ?></legend>

        <div class="spacer"></div>

        <table class="pure-table">
            <thead>
                <tr>
                    <th><?php echo $languageKeysCtrl["call-center-view-patner-id"]; ?></th>
                    <th><?php echo $languageKeysCtrl["call-center-view-patner-date"]; ?></th>
                    <th><?php echo $languageKeysCtrl["call-center-view-patner-store"]; ?></th>
                    <th><?php echo $languageKeysCtrl["call-center-view-patner-pts-rewarded"]; ?></th>
                    <th><?php echo $languageKeysCtrl["call-center-view-patner-pts-redeemed"]; ?></th>
                    <th><?php echo $languageKeysCtrl["call-center-view-patner-total-paid"]; ?></th>
                </tr>
            </thead>
            <tbody>
	            @if($partner->transactions->count() == 0)
	            	<tr>
	            		<td colspan="7"><?php echo $languageKeysCtrl["call-center-view-patner-no-transactions-found"]; ?></td>
	            	</tr>
	            @else
	            	@foreach($partner->transactions as $t)
	            	<tr>
	            		<td>{{ $t->id }}</td>
	            		<td>{{ date("M j, Y, g:i a", strtotime($t->created_at)) }}</td>
	            		<td>
	            			@if($t->store)
	            				{{ $t->store->name }}
	            			@else
	            				N/A
	            			@endif
	            		</td>
	            		<td>{{ $t->points_rewarded }}</td>
	            		<td>{{ $t->points_redeemed }}</td>
	            		<td>{{ $t->total_amount }}</td>
	            	</tr>
	            	@endforeach
	            @endif
            </tbody>
        </table>

	{{ Form::close() }}
</div>