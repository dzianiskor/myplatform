@extends('layouts.mobile')

@section('body')

    {{ Form::open(array('url'    => '/mobile',
                        'method' => 'get',
                        'class'  => 'pure-form pure-form-stacked page-padding')) }}

        <fieldset>
            <legend>Reversal Transaction Details</legend>

            <table>
                @if($status == 200)
                    <tr>
                        <td>Status:</td>
                        <td>
                            <img src="/img/yes.png" alt="Success" />
                        </td>
                    </tr>
                @else
                    <tr>
                        <td>Status:</td>
                        <td>
                            <img src="/img/no.png" alt="Failure"/>
                        </td>
                    </tr>
                @endif
                @if ($message)
                    <tr>
                        <td>Messsage:</td>
                        <td>
                            {{ $message }}
                        </td>
                    </tr>
                @endif
                <tr>
                    <td>Name:</td>
                    <td>
                        <strong>{{ $user->first_name }} {{ $user->last_name }}</strong>
                    </td>
                </tr>

                <tr>
                    <td>Reference ID:</td>
                    <td>
                        <strong>{{ $transaction->ref_number }}</strong>
                    </td>
                </tr>
                <tr>
                    <td>Transaction ID:</td>
                    <td>
                        <strong>{{ $transaction->id }}</strong>
                    </td>
                </tr>
                <tr>
                    <td>Transaction Type:</td>
                    <td>
                        <strong>{{ ucfirst($type) }}</strong>
                    </td>
                </tr>
                <tr>
                    <td>Points to be reversed:</td>
                    <td>
                        <strong>{{ number_format($points) }}</strong>
                    </td>
                </tr>
            </table>

        </fieldset>

    {{ Form::close() }}

    <div class="form-buttons">
        <div style="text-align:right">
            <a href="{{ route('tickets') }}" class="pure-button pure-button-primary dark-button">Done</a>
        </div>
    </div>

@stop

@section('page_script')
    <script>
        enqueue_script('mobile');
    </script>
@stop
