<?php //if($canReverseTrx){ ?>
<a href="#" id="reverse-trx" data-trx="{{ base64_encode($trxId) }}" class="pure-button" style="margin-right:10px;<?php if(!$canReverseTrx){echo 'display:none';} ?>"<?php if(!$canReverseTrx){echo ' disabled="disabled"';} ?>>Reverse Transaction</a>
<?php //} ?>
<br/>
 <legend>Items</legend>
 <br/>
<table class="pure-table">
    <thead>
        <tr>
            <th>Item Id</th>
            <th>Item Name</th>
            <th>Cost</th>
            <th>Quantity</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
       
        <?php foreach ($trxItems as $trxItem){ 
            $reversedItems  = App\TransactionItem::where('ref_number', 'like', '%'.$trxItem->id.'%')->get();
            $reversedQty = 0;
            foreach($reversedItems as $reversedItem){
                $reversedQty    -= $reversedItem->quantity;
            }
            
            ?>
        {{ Form::open(array('url' => url('dashboard/call_center/reverse_trx_item/'.base64_encode($trxItem->id)), 'method' => 'post', 'class' => "pure-form pure-form-stacked form-dynamic")) }}
        <tr>
            <td>{{ $trxItem->product_id }}</td>
            <td><?php echo App\Product::find($trxItem->product_id)->name; ?></td>
            <td>
                <?php $remainingQty = $trxItem->quantity-$reversedQty; ?>
                {{ ($trxItem->price_in_points/$trxItem->quantity) * $remainingQty }}</td>
            <td>
                <select id="quantity<?php echo $trxItem->id; ?>" name="quantity">
                    <option value="0">Select Quantity</option>
                    <?php for($i=1; $i<=$remainingQty; $i++){ ?>
                    <option value="{{ $i }}">{{ $i }}</option>
                    <?php } ?>
                </select>
            </td>
            <td>
                <?php if($remainingQty == 0){ 
                    echo 'Reversed';
                }else{
                ?>
                <a href="#" id="reverse-trx-item" data-trx="{{ base64_encode($trxItem->id) }}" class="pure-button" style="margin-right:10px;">Reverse</a>
                <?php } ?>
            </td>
        </tr>
        {{ Form::close() }}
        <?php } ?>
    </tbody>
</table>
 
 
<div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 10px; width: 100%; margin: 0; padding: 0;"></div>
<div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 5px; width: 100%; background-color: #35aeea; margin: 10px 0; padding: 0;"></div>
<div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 10px; width: 100%; margin: 0; padding: 0;"></div>
<legend>Reversed Items</legend>
<br/>
<table class="pure-table">
    <thead>
        <tr>
            <th>Item Id</th>
            <th>Item Name</th>
            <th>Cost</th>
            <th>Quantity</th>
            <th>Reversed At</th>
        </tr>
    </thead>
    <tbody>
       <?php 
        if($reversedItemsFlag == 0 ){ ?>
            <tr>
                <td colspan="5">
                    No Items Reversed.
                </td>
            </tr>
       <?php 
        }else{
            foreach ($trxItems as $trxItem){ 
               $reversedItems  = App\TransactionItem::where('ref_number', 'like', '%'.$trxItem->id.'%')->get();

               foreach($reversedItems as $reversedItem){
                   ?>
                   <tr>
                       <td>{{ $reversedItem->product_id }}</td>
                       <td><?php echo App\Product::find($reversedItem->product_id)->name; ?></td>
                       <td>
                           {{ $reversedItem->price_in_points *(-1) }}
                       </td>
                       <td>
                           {{ $reversedItem->quantity *(-1) }}
                       </td>
                       <td>
                           {{ $reversedItem->created_at }}
                       </td>
                   </tr>
                   <?php 
               }
            }
        } 
        ?>
    </tbody>
</table>
