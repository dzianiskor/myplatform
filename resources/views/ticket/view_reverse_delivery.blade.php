 <legend>Deliveries</legend>
 <br/>
<table class="pure-table">
    <thead>
        <tr>
            <th>Item Id</th>
            <th>Item Name</th>
            <th>Cost</th>
            <th>Quantity</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($trxItemsDelivery as $trxItemDelivery){ 
            $trxItem    = App\TransactionItem::find($trxItemDelivery->trx_item_id);
            $deliveryCurrency   = App\Currency::where('id', $trxItemDelivery->currency_id)->first();
            $deliveryCost   = PointsHelper::amountToPoints($trxItemDelivery->amt_delivery, $deliveryCurrency->short_code, $trxItemDelivery->network_id);
            
            $reversedDeliveries  = TransactionItemDelivery::where('ref_number', 'like', '%'.$trxItemDelivery->id.'%')->get();
            $reversedQty = 0;
            foreach($reversedDeliveries as $reversedDelivery){
                $reversedQty    -= $reversedDelivery->quantity;
            }
        ?>
        <tr>
            <td>{{ $trxItem->product_id }}</td>
            <td><?php echo App\Product::find($trxItem->product_id)->name; ?></td>
            <td>
                <?php $remainingQty = $trxItemDelivery->quantity-$reversedQty; ?>
                {{ ($deliveryCost/$trxItemDelivery->quantity) * $remainingQty }}</td>
            <td>
                <select id="quantity<?php echo $trxItemDelivery->id; ?>" name="quantity">
                    <option value="0">Select Quantity</option>
                    <?php for($i=1; $i<=$remainingQty; $i++){ ?>
                    <option value="{{ $i }}">{{ $i }}</option>
                    <?php } ?>
                </select>
            </td>
            <td>
                <?php if($remainingQty == 0){ 
                    echo 'Reversed';
                }else{
                ?>
                <a href="#" id="reverse-trx-delivery" data-trx="{{ base64_encode($trxItemDelivery->id) }}" class="pure-button" style="margin-right:10px;">Reverse</a>
                <?php } ?>
            </td>
        </tr>
        <?php } ?>
    </tbody>
</table>
 
<div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 10px; width: 100%; margin: 0; padding: 0;"></div>
<div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 5px; width: 100%; background-color: #35aeea; margin: 10px 0; padding: 0;"></div>
<div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 10px; width: 100%; margin: 0; padding: 0;"></div>
 <legend>Reversed Deliveries</legend>
 <br/>
 <table class="pure-table">
    <thead>
        <tr>
            <th>Item Id</th>
            <th>Item Name</th>
            <th>Cost</th>
            <th>Quantity</th>
            <th>Reversed At</th>
        </tr>
    </thead>
    <tbody>
        <?php 
        if($reversedDeliveryFlag == 0 ){ ?>
            <tr>
                <td colspan="5">
                    No Deliveries Reversed.
                </td>
            </tr>
       <?php 
        }else{
            foreach ($trxItemsDelivery as $trxItemDelivery){ 
                $trxItem    = App\TransactionItem::find($trxItemDelivery->trx_item_id);
                $deliveryCurrency   = App\Currency::where('id', $trxItemDelivery->currency_id)->first();
                $deliveryCost   = PointsHelper::amountToPoints($trxItemDelivery->amt_delivery, $deliveryCurrency->short_code, $trxItemDelivery->network_id);

                $reversedDeliveries  = TransactionItemDelivery::where('ref_number', 'like', '%'.$trxItemDelivery->id.'%')->get();
                $reversedQty = 0;
                foreach($reversedDeliveries as $reversedDelivery){
                    $reversedQty    -= $reversedDelivery->quantity;
                }
                foreach($reversedDeliveries as $reversedDelivery){
                ?>
                    <tr>
                        <td>{{ $trxItem->product_id }}</td>
                        <td><?php echo App\Product::find($trxItem->product_id)->name; ?></td>
                        <td>
                            {{ $reversedDelivery->delivery_cost * (-1) }}</td>
                        <td>
                            {{ $reversedDelivery->quantity * (-1) }}
                        </td>
                        <td>
                          {{ $reversedDelivery->created_at }}
                        </td>
                    </tr>
                <?php
                    }   
            } 
        }
        ?>
    </tbody>
 </table>