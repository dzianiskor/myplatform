{{ Form::model($ticket, array('action'  => array('TicketController@saveTicket', base64_encode($ticket->id)),
                               'method' => 'post',
                               'files'  => 'true',
                               'class'  => 'pure-form pure-form-stacked form-dynamic ticket_member_form')) }}

    @include('partials.errors')

    {!! Form::hidden('redirect_to', URL::previous()) !!}

    @if(!$ticket->draft)
        <fieldset>
            <legend>Agent Details</legend>

            <table class="agent-details">
                <tr>
                    <td>Created by :</td>
                    @if(count($ticket->admin))
                        <td>{{ $ticket->admin->first_name . ' ' . $ticket->admin->last_name }}</td>
                    @else
                        <td>N/A</td>
                    @endif
                </tr>
                <tr>
                    <td>Created at :</td>
                    <td>{{ date("M j, Y, g:i a", strtotime($ticket->created_at)) }}</td>
                </tr>
            </table>
        </fieldset>
    @endif

    <fieldset>
        <legend>Basic Details</legend>

        {{ Form::label('status', 'Ticket Status :') }}
        {{ Form::select('status', Meta::ticketStatuses(), $ticket->status, array('class' => "pure-input-1")) }}

        {{ Form::label('ticket_type_id', 'Ticket Type :') }}
        <select name="ticket_type_id" id="ticket_type_id">
            <?php
            foreach (Meta::ticketTypes() as $tId => $tName){
                if ($tId == $ticket->ticket_type_id) {
                    echo '<option value="' . $tId . '" selected=selected>'. $tName .'</option>';
                } else {
                    echo '<option value="' . $tId . '">'. $tName .'</option>';
                }
            }
            ?>
        </select>

        {{ Form::hidden('source_id', ($ticket->source_id > 0 )?  base64_encode($ticket->source_id) : 1) }}
        {{ Form::hidden('source_type', $ticket->query_source) }}

        @if($ticket->query_source == 'Member')
            {{ Form::label('source_id', 'Member:') }}
            @if($ticket->draft)
                {{ Form::text('member_name', Support::memberRevNameFromID($ticket->source_id), array('class' => 'pure-input-1 required search', 'placeholder' => 'Type name & pres enter')) }}
            @else
                {{ Form::text('member_name', Support::memberRevNameFromID($ticket->source_id), array('class' => 'pure-input-1', 'disabled' => "disabled")) }}
            @endif
        @else
            {{ Form::label('source_id', 'Partner:' ) }}
            @if($ticket->draft)
                <select id="partner_id" name="partner_id" class="pure-input-1 required">
                    @foreach(Meta::listPartners() as $p)
                        @if($p->id == $ticket->source_id)
                            <option value="{{ $p->id }}" selected="selected">{{ $p->name }}</option>
                        @else
                            <option value="{{ $p->id }}">{{ $p->name }}</option>
                        @endif
                    @endforeach
                </select>
            @else
                <select id="partner_id" name="partner_id" class="pure-input-1 required" disabled="disabled">
                    @foreach(Meta::listPartners() as $p)
                        @if($p->id == $ticket->source_id)
                            <option value="{{ $p->id }}" selected="selected">{{ $p->name }}</option>
                        @else
                            <option value="{{ $p->id }}">{{ $p->name }}</option>
                        @endif
                    @endforeach
                </select>
            @endif
        @endif

        {{ Form::label('ticket', 'Detail:') }}
        {{ Form::textarea('ticket', $ticket->ticket, array('class'=>'pure-input-1 required')) }}

        @if(I::can('view_history'))
            <a href="#dialog-ticket-history" title="View Ticket History" class="info-link dialog">
                <i class="fa fa-list"></i>Ticket History
            </a>
        @endif
    </fieldset>

    {{--- Form Buttons ---}}
    <div class="form-buttons">
        <div class="right">
            <input type="hidden" name="from_travel" id="from_travel" value="0"/>
            <input type="hidden" name="ticket_id" id="ticket_id" value="{{ $ticket->id }}"/>
            @if($ticket->draft)
                @if(I::can('create_tickets'))
                    <button type="submit" class="pure-button pure-button-primary ticket_member_form">Create Ticket</button>
                @endif
            @else
                @if(I::can('edit_tickets'))
                    <button type="submit" class="pure-button pure-button-primary ticket_member_form">Update Ticket</button>
                @endif
            @endif
        </div>
        <div class="clearfix"></div>
    </div>
    {{--- /Form Buttons ---}}

{{ Form::close() }}
