<div class="entity-profile-page">

	{{ Form::open(array('url' => url('dashboard/call_center/update_member/'.base64_encode($user->id)), 'method' => 'post', 'class' => "pure-form pure-form-stacked form-dynamic")) }}

		<input type="hidden" name="ticket_id" value="{{ $ticket_id }}" />

		<fieldset>
			<legend><?php if (isset($languageKeysCtrl["call-center-view-member-user-details"]) && !empty($languageKeysCtrl["call-center-view-member-user-details"])) { echo $languageKeysCtrl["call-center-view-member-user-details"]; }?></legend>
			<div class="pure-g">
	            <div class="pure-u-1-2">
					{{ Form::label('status', $languageKeysCtrl["call-center-view-member-status"]) }}
					{{ Form::select('status', Meta::memberStatuses(), $user->status, array('class' => 'pure-input-1-2')) }}
				</div>
	            <div class="pure-u-1-3">
				<?php $firstocc = FALSE; ?>
					@if(I::am('BLU Admin'))
					@if(isset($user->references->first()->number) )
						{{ Form::label('Reference', $languageKeysCtrl["call-center-view-member-reference"]) }}
						{{ Form::label('Reference',$user->references->first()->number) }}
						@endif
							  @else
								  @foreach($user->references as $c)
									<?php
										$prtid = Auth::User()->getTopLevelPartner()->id;
										$refprtid    = $c->partner_id;

										if($prtid == $refprtid && !$firstocc){
											$firstocc = TRUE;
										?>
									{{ Form::label('Reference', $languageKeysCtrl["call-center-view-member-reference"]) }}
									{{ Form::label('Reference', $c->number) }}
						<?php
								}
							?>
						  @endforeach
					   @endif
				</div>
			</div>

	    	{{ Form::label('ucid', $languageKeysCtrl["call-center-view-member-unique-customer-id"].':') }}
	    	{{ Form::text('ucid', $user->ucid, array('class' => "pure-input-1", 'disabled' => 'disabled')) }}
                <?php
                    $memberFirstName = $user->first_name;
                    $memberMiddleName = $user->middle_name;
                    $memberLastName = $user->last_name;
                    if($memberFirstName == "na"){
                        $memberFirstName = "N/A";
                    }
                    if($memberMiddleName == "na"){
                        $memberMiddleName = "N/A";
                    }
                    if($memberLastName == "na"){
                        $memberLastName = "N/A";
                    }
                ?>
	        <div class="pure-g">
	            <div class="pure-u-1-3 pure-u-med-1-2">
			        {{ Form::label('first_name', $languageKeysCtrl["call-center-view-member-first-name"].':') }}
			        {{ Form::text('first_name', $memberFirstName, array('class' => "pure-input-1")) }}
	            </div>
	            <div class="pure-u-1-3 pure-u-med-1-2">
	            	<div class="padding-left-10">
			        	{{ Form::label('middle_name', $languageKeysCtrl["call-center-view-member-middle-name"].':') }}
			        	{{ Form::text('middle_name', $memberMiddleName, array('class' => "pure-input-1")) }}
			        </div>
	            </div>
	            <div class="pure-u-1-3 pure-u-med-1-2">
	            	<div class="padding-left-10">
			        	{{ Form::label('last_name', $languageKeysCtrl["call-center-view-member-last-name"].':') }}
			        	{{ Form::text('last_name', $memberLastName, array('class' => "pure-input-1")) }}
			        </div>
	            </div>
	        </div>

	        <div class="pure-g">
	            <div class="pure-u-1-3 pure-u-med-1-2">
		        	{{ Form::label('dob', $languageKeysCtrl["call-center-view-member-date-of-birth"].':') }}
		        	{{ Form::text('dob', date("m/d/Y", strtotime($user->dob)), array('class' => "pure-input-1 birthdatepicker")) }}
	            </div>
	            <div class="pure-u-1-3 pure-u-med-1-2">
	            	<div class="padding-left-10">
		            	{{ Form::label('balance', $languageKeysCtrl["call-center-view-member-network-balance"].':') }}

                                <select name="network_id" class="pure-input-1">
                                    @if(!I::am('BLU Admin') && !I::am('BLU Admin Dev'))
                                        @if(!empty($balances))
                                            <option value="{{$balances->network_id}}">{{ $balances->network->name }} ({{ number_format($balances->balance) }} Pts)</option>
                                        @else
                                            <option value="0"></option>
                                        @endif
                                    @else
		            		@foreach($balances as $b)
		            			<option value="{{$b->network_id}}">{{ $b->network->name }} ({{ number_format($b->balance) }} Pts) </option>
		            		@endforeach
                                    @endif
		            	</select>
	            	</div>
	            </div>
	            <div class="pure-u-1-3 pure-u-med-1-2">
	            	<div class="padding-left-10">
		                {{ Form::label('gender', $languageKeysCtrl["call-center-view-member-gender"].':') }}
		                {{ Form::select('gender', Meta::genderList(), $user->gender, array('class' => 'pure-input-1')) }}
			        </div>
	            </div>
	        </div>
        </fieldset>

        <fieldset>
            <legend><?php echo $languageKeysCtrl["call-center-view-member-contact-details"]; ?></legend>

            {{ Form::label('telephone_code', $languageKeysCtrl["call-center-view-member-country-telephone-code"]) }}
			<select name="telephone_code" class="pure-input-1">
				<?php foreach(Meta::countries() as $c): ?>
					<?php if($c->telephone_code == $user->telephone_code): ?>
						<option value="<?php echo $c->telephone_code; ?>" selected="selected"><?php echo $c->name; ?> (<?php echo $c->telephone_code; ?>)</option>
					<?php else: ?>
						<option value="<?php echo $c->telephone_code; ?>"><?php echo $c->name; ?> (<?php echo $c->telephone_code; ?>)</option>
					<?php endif; ?>
				<?php endforeach; ?>
			</select>

	        {{ Form::label('mobile', $languageKeysCtrl["call-center-view-member-mobile"].':') }}
	        {{ Form::text('mobile', $user->mobile, array('class' => "pure-input-1")) }}

			<div id="mobile-error" class="inline-error">
				The number provided is already in use by another account.
			</div>

	    	{{ Form::label('email', $languageKeysCtrl["call-center-view-member-email"].':') }}
        	{{ Form::text('email', $user->email, array('class' => "pure-input-1")) }}

			<div id="email-error" class="inline-error">
				<!--The email provided is already in use by another account.-->
				<?php echo $languageKeysCtrl["call-center-view-member-user-the-email-provided-is-already-in-use-by-another-account"]; ?>
			</div>
		</fieldset>

        <fieldset>
            <legend><?php echo $languageKeysCtrl["call-center-view-member-user-address-details"]; ?></legend>

            {{ Form::label('country_id', $languageKeysCtrl["call-center-view-member-country"].':') }}
            {{ Markup::country_select('country_id', $user->country_id, array('class' => 'pure-input-1-3')) }}

            {{ Form::label('area_id', $languageKeysCtrl["call-center-view-member-area"].':') }}
            {{ Markup::area_select('area_id', $user->country_id, $user->area_id, array('class' => 'pure-input-1-3')) }}

            {{ Form::label('city_id', $languageKeysCtrl["call-center-view-member-city"].':') }}
            {{ Markup::city_select('city_id', $user->area_id, $user->city_id, array('class' => 'pure-input-1-3')) }}

            {{ Form::label('address_1', $languageKeysCtrl["call-center-view-member-address"].':') }}
            {{ Form::text('address_1', $user->address_1, array('class' => 'pure-input-1')) }}

            {{ Form::label('address_2', $languageKeysCtrl["call-center-view-member-address-continued"].':') }}
            {{ Form::text('address_2', $user->address_2, array('class' => 'pure-input-1')) }}
        </fieldset>

        {{--- Form Buttons ---}}
        <div class="form-buttons">
            <div style="text-align:right;">
                @if(I::can('resend_pin'))
                    <a href="#" id="send-pin" data-user="{{ base64_encode($user->id) }}" class="pure-button" style="margin-right:10px;">
						<?php echo $languageKeysCtrl["call-center-view-member-user-send-pin"]; ?>
					</a>
                @endif
            	@if(I::can('redeem_items'))
            		<a href="#dialog-redeem-item" data-user="{{ $user->id }}" class="dialog pure-button" style="margin-right:10px;">
						<?php echo $languageKeysCtrl["call-center-view-member-user-redeem-item"]; ?>
					</a>
            	@endif
            	@if(I::can('edit_profiles'))
                	<a href="#" id="update-profile" class="pure-button pure-button-primary">
						<?php echo $languageKeysCtrl["call-center-view-member-user-update-profile"]; ?>
					</a>
                @endif
            </div>
            <div class="clearfix"></div>
        </div>
        {{--- /Form Buttons ---}}

		<div class="spacer"></div>

        <legend><?php echo $languageKeysCtrl["call-center-view-member-user-recent-transactions"]; ?></legend>

        <div class="spacer"></div>

        <table class="pure-table">
            <thead>
                <tr>
                    <th><?php echo $languageKeysCtrl["call-center-view-member-user-id"]; ?></th>
                    <th><?php echo $languageKeysCtrl["call-center-view-member-user-date"]; ?></th>
                    <th><?php echo $languageKeysCtrl["call-center-view-member-user-store"]; ?></th>
                    <th><?php echo $languageKeysCtrl["call-center-view-member-user-pts-rewarded"]; ?></th>
                    <th><?php echo $languageKeysCtrl["call-center-view-member-user-pts-redeemed"]; ?></th>
                    <th><?php echo $languageKeysCtrl["call-center-view-member-user-total-paid"]; ?> </th>
                    <th><?php echo $languageKeysCtrl["call-center-view-member-user-reverse-trx"]; ?></th>
                </tr>
            </thead>
            <tbody>
	            @if(count($transactions) == 0)
	            	<tr>
	            		<td colspan="7"><?php echo $languageKeysCtrl["call-center-view-member-user-no-transactions-found"]; ?></td>
	            	</tr>
	            @else
	            	@foreach($transactions as $t)
                        <?php
							$short_code = '';
                        	if (!empty($t->currency_id)) {
								$short_code = $t->currency->short_code;
                        	} else {
                        	    if ($short_code == '') {
                                    $currencyId = 1;
                                    $currency = App\Currency::where('id', $currencyId)->first();
                                    $short_code = $currency->short_code;
								}
                        	}
                        ?>
	            	<tr>
	            		<td>{{ $t->id }}</td>
	            		<td>{{ date("M j, Y, g:i a", strtotime($t->created_at)) }}</td>
	            		<td>
	            			@if($t->store)
	            				{{ $t->store->name }}
	            			@else
	            				N/A
	            			@endif
	            		</td>
	            		<td>{{ number_format($t->points_rewarded, 0, '.', ',') }}</td>
	            		<td>{{ number_format($t->points_redeemed) }}</td>
	            		<td>{{ number_format($t->original_total_amount, 2) }} {{ $short_code }}</td>
                        <td>
                        @if(I::can('reverse_transaction'))
                            @if($t->trans_class != "Reversal" && $t->trans_class != "Reversal Item" && $t->trans_class != "Reversal Delivery")
                                @if($t->reversed != 1)
                                    @if($t->trans_class == "Items" || $t->trans_class == "Hotel" || $t->trans_class == "Flight" || $t->trans_class == "Car" )
                                        <a href="#dialog-reverse" id="new-reverse" data-trx="{{ base64_encode($t->id) }}" class="dialog pure-button" style="margin-right:10px;">
                                            <?php echo $languageKeysCtrl["call-center-view-member-user-reverse"]; ?>
                                        </a>
                                    @else
                                        <a href="#" id="reverse-trx" data-trx="{{ base64_encode($t->id) }}" class="pure-button" style="margin-right:10px;">
                                            <?php echo $languageKeysCtrl["call-center-view-member-user-reverse"]; ?>
                                        </a>
                                    @endif
                                        @else
                                            <a href="#" class="pure-button" style="margin-right:10px;" disabled="disabled">
                                                <?php echo $languageKeysCtrl["call-center-view-member-user-reversed"]; ?>
                                            </a>
                                        @endif
                                @endif
                            @endif
                        </td>
	            	</tr>
	            	@endforeach
	            @endif
            </tbody>
        </table>

	{{ Form::close() }}
</div>