@extends('layouts.dashboard')

@section('title')
    Call Center | BLU
@stop

@section('dialogs')
    <div id="dialog-ticket-type" style="width:300px;">
        <h4>New Ticket</h4>
        <div class="button-choice">
            <a href="{{ url('dashboard/call_center/new?type=member') }}" title="Member">Member</a>
            <a href="{{ url('dashboard/call_center/new?type=partner') }}" title="Partner">Partner</a>
        </div>
    </div>
@stop

@section('body')

<div class="padding content-call-center">

        <div class="content-head pure-g">
            <div class="title pure-u-1-2 title-rtl">
                <h1><i class="fa fa-phone-square  fa-fw"></i> Tickets ({{ $tickets->total() }})</h1>
            </div>

            <div class="controls pure-u-1-2 controls-rtl">
                <form action="{{ Filter::searchUrl() }}" method="get">
                    <input type="text" placeholder="Search" name="q" value="{{ Input::get('q') }}" />
                    <button type="submit" title="" class="list-search"></button>
                </form>
                @if(I::can('create_tickets'))
                    <a href="#dialog-ticket-type" class="pure-button pure-button-primary dialog">New Ticket</a>
                @endif
            </div>

            <div id="catalog-filter" class="pure-u-1-2">
                <input id="filter_route" type="hidden" value="call_center">
                <form id="filter-catalogue" action="{{ Filter::searchUrl() }}" method="get">
                    <div class="filter-block filter-block-pricing-width">
                        <span class="filter-label">Customer Type:</span>
                        <select style="width:100%;" name="customer_type_list[]" data-filter-catalogue="customer_type_list" data-filter-catalogue-title="Customer Type" multiple="multiple">
                            @foreach ($lists['customerTypes'] as $key => $value)
                                <option value="{{ $key }}">{{ $value }}</option>
                            @endforeach
                        </select>
                        <input data-filter-catalogue="customer_type_list" type="hidden" value="{{ Input::get('customer_type_list') }}">
                    </div>
                    <div class="filter-block filter-block-pricing-width">
                        <span class="filter-label">Status:</span>
                        <select style="width:100%;" name="status_list[]" data-filter-catalogue="status_list" data-filter-catalogue-title="Status" multiple="multiple">
                            @foreach ($lists['statuses'] as $key => $value)
                                <option value="{{ $key }}">{{ $value }}</option>
                            @endforeach
                        </select>
                        <input data-filter-catalogue="status_list" type="hidden" value="{{ Input::get('status_list') }}">
                    </div>
                    <div class="submit-filter-block disable-width">
                        <div style="float:right;">
                            <a href="{{ url('dashboard/call_center') }}" title="Clear Filters" class="pure-button pure-button-primary">
                                Clear Filters <i class="fa fa-ban fa-fw"></i>
                            </a>
                        </div>
                        <button class="pure-button pure-button-primary" type="submit" title="" style="float:right;margin-right:15px;">
                            Submit Filters
                        </button>
                    </div>
                </form>
            </div>
        </div>

        <table class="pure-table">
            <thead>
                <tr>
                    <th width="50">ID</th>
                    <th>STATUS</th>
                    <th>CUSTOMER TYPE</th>
                    <th>IDENTITY</th>
                    <th>CREATED AT</th>
                    <th>AGENT</th>
                    <th width="150">ACTIONS</th>
                </tr>
            </thead>

            <tbody>
            @if($tickets->count() > 0)
                @foreach ($tickets as $ticket)
                    <tr>
                        <td>
                            {{ $ticket->id }}
                        </td>
                        <td>
                            <a href="{{ url('dashboard/call_center/view/'.base64_encode($ticket->id)) }}" class="edit" title="Edit">
                                {{ $ticket->status }}
                            </a>
                        </td>
                        <td>
                            {{ $ticket->query_source }}
                        </td>
                        <td>
                            {{ $ticket->identity }}
                        </td>
                        <td>
                            {{ date("M j, Y, g:i a", strtotime($ticket->created_at)) }}
                        </td>
                        <td>
                            {{ $ticket->admin_name }}
                        </td>
                        <td>
                            @if(I::can('view_tickets'))
                                <a href="{{ url('dashboard/call_center/view/'.base64_encode($ticket->id).'?load') }}" class="view" title="View">View</a>
                            @endif

                            @if(I::can('delete_tickets'))
                                <a href="{{ url('dashboard/call_center/delete/'.base64_encode($ticket->id).'?load') }}" class="delete" title="Delete">Delete</a>
                            @endif
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="7">No Tickets Found</td>
                </tr>
            @endif
            </tbody>
        </table>

        {{ $tickets->setPath('call_center')->appends(Input::except('page'))->links() }}

    </div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-call_center');
    </script>
@stop
