@extends('layouts.dashboard')

@section('title')
    Support Ticket | BLU
@stop

@section('js_globals')
    var TICKET_ID = {{ $ticket->id }};
@stop

@section('dialogs')
    {{--- Select Member ---}}
    <div id="dialog-select-member" style="width:400px;" class="dialog-select-member-rtl">
        <h4>Select Member</h4>
        <div class="scroll support-popup-list">
            <ul id="support-select-member">
                <li>Member not found</li>
            </ul>
        </div>
    </div>

    {{--- Ticket History ---}}
    <div id="dialog-ticket-history" style="width:500px;" class="dialog-ticket-history-rtl">
        <h4>Ticket History</h4>
        <div class="scroll support-popup-list">
            <table class="pure-table" style="width:100%">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>DESCRIPTION</th>
                    <th>RESPONSIBLE</th>
                </tr>
                </thead>
                <tbody>
                @foreach($ticket->activity as $a)
                    <tr>
                        <td>{{ $a->id }}</td>
                        <td>{{ $a->description }}</td>
                        <td>{{ Support::memberNameFromID($a->user_id) }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    {{--- Redeem Product ---}}
    <div id="dialog-redeem-item" style="width:500px;" class="dialog-redeem-item-rtl">
        <h4><?php echo $languageKeysCtrl["call-center-dialog-redeem-item"]; ?></h4>
        <form class="pure-form">
            <span class="info">
                <?php echo $languageKeysCtrl["call-center-dialog-redeem-item-search-using-product"]; ?>
            </span>
            {{ Form::label('network_id', $languageKeysCtrl["call-center-dialog-redeem-item-network-label"] ) }}
            {{ Form::select('network_id', $networks, null, array('class' => 'pure-input-1')) }}
            {{ Form::label('country_id', $languageKeysCtrl["call-center-dialog-redeem-item-country-label"] ) }}
            {{ Form::select('country_id', $countries, null, array('class' => 'pure-input-1')) }}
            {{ Form::hidden('channel_id', Auth::User()->getTopLevelPartner()->id) }}
            {{ Form::label('item_search', $languageKeysCtrl["call-center-dialog-redeem-item-search-label"]) }}
            {{ Form::text('item_search', '', array('class' => "pure-input-1", 'placeholder' => '')) }}
        </form>
        <div id="redeemable-item"></div>
    </div>
    {{--- Redeem Travel ---}}
    <div id="dialog-redeem-travel" style="width:500px;" class="dialog-redeem-travel-rtl">
        <h4><?php echo $languageKeysCtrl["call-center-dialog-redeem-travel"]; ?></h4>
        <a href="/dashboard/travel/flightsearch" class="pure-button"><?php echo $languageKeysCtrl["call-center-dialog-redeem-travel-flights"]; ?></a>
        <a href="/dashboard/travel/hotelsearch" class="pure-button"><?php echo $languageKeysCtrl["call-center-dialog-redeem-travel-hotels"]; ?></a>
        <a href="/dashboard/travel/carsearch" class="pure-button"><?php echo $languageKeysCtrl["call-center-dialog-redeem-travel-cars"]; ?></a>
    </div>

    {{--- Reverse Transaction ---}}
    <div id="dialog-reverse" style="width:631px;" class="dialog-reverse-rtl">
        <h4>Reverse  </h4>
        <div id="tabs">
            <ul>
                <li>
                    <a href="#tabs-1">Reverse Items</a>
                </li>
                <li>
                    <a href="#tabs-2">Reverse Delivery</a>
                </li>
            </ul>

            <div id="tabs-1">
                <h3>Loading..</h3>
            </div>

            <div id="tabs-2">
                <h3>Loading..</h3>
            </div>
        </div>
    </div>

@stop

@section('body')
    <div class="padding call-center-view-rtl" id="support-center-page">

        <div class="content-head pure-g">
            <div class="title pure-u-1-2 title-rtl">
                @if($ticket->draft)
                    <h1><i class="fa fa-phone fa-fw"></i><?php echo $languageKeysCtrl["call-center-title-support-ticket-new"]; ?></h1>
                @else
                    <h1><i class="fa fa-phone fa-fw"></i><?php echo $languageKeysCtrl["call-center-title-support-ticket-modify"]; ?> #{{ $ticket->id }}</h1>
                @endif
            </div>

            <div class="controls pure-u-1-2 controls-rtl">
                <a href="{{ url('dashboard/call_center') }}" class="pure-button"><?php echo $languageKeysCtrl["call-center-all-tickets"]; ?></a>
            </div>
        </div>

        <div class="pure-g">
            <div class="pure-u-1-3">
                <div class="l-box ticket-view-rtl" id="ticket-view">
                    @include('ticket/form', array('ticket' => $ticket))
                </div>
            </div>
            <div class="pure-u-2-3">
                <div class="r-box empty profile-view-rtl" id="profile-view"></div>
            </div>
        </div>

    </div>
@stop

@section('page_script')
    <!--for tabs-->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="/js/vendor/chosen/chosen.jquery.min.js"></script>
    <script>
        set_menu('mnu-call_center');

        // Load up the partner or member profile
        function loadSupportProfile() {
            NProgress.start();

            var data = {
                source_id: $('input[name=source_id]').val(),
                source_type: $('input[name=source_type]').val(),
                ticket_id: TICKET_ID
            };

            $.post('/dashboard/call_center/load_profile', data, function(html){



                NProgress.done();
                $('#profile-view').html(html).removeClass('empty');
            });
        }

        $(function() {
            // Handle the member search
            $('input[name=member_name]').keypress(function(e){
                if(e.which == 13) {
                    NProgress.start();

                    var data = {
                        name: $(this).val(),
                        ticket_id: TICKET_ID
                    };
                    console.log(data);
                    $.post('/dashboard/call_center/search', data, function(html){
                        $('#dialog-select-member ul').html(html);

                        showDialog('#dialog-select-member');

                        NProgress.done();
                    });

                    return false;
                }
            });

            // Handle the partner selection
            $(document).on('change', 'select[name=partner_id]', function(e) {
                $('input[name=source_id]').val(btoa($(this).val()));

                loadSupportProfile();
            });

            // Handle member name selection
            $(document).on('click', 'ul#support-select-member a', function(e) {
                e.preventDefault();

                $('input[name=source_id]').val($(this).data('id'));

                $('input[name=member_name]').val($(this).html());

                closeDialog();

                loadSupportProfile();
            });

            // Handle the profile updates
            $(document).on('click', '#update-profile', function(e) {
                e.preventDefault();

                NProgress.start();

                _this = $(this);
                _this.attr('disabled', true).html('Updating...');

                $.post($(this).closest('form').attr('action'), $(this).closest('form').serialize(), function(resp) {
                    NProgress.done();

                    _this.attr('disabled', false).html('Update Profile');

                    if(resp.failed) {
                        alert(resp.message);
                    }
                });
            });

            // Handle the item redemption dialog
            $(document).on('keypress', 'input[name=item_search]', function(e){

                if(e.which == 13) {
                    e.preventDefault();

                    NProgress.start();

                    $.get('/dashboard/call_center/load_products-new/'+$(this).val()+'/'+$('#network_id').val()+'/'+$('#country_id').val()+'/'+$('input[name=channel_id]').val(), function(html){
                        $('div#redeemable-item').html(html);
                        NProgress.done();
                    });
                }
            });

            // Handle the item redemption dialog
            $(document).on('change', '#network_id', function(e){

                if($('#item_search').val() == ''){
                    return false;
                }

                e.preventDefault();

                NProgress.start();

                $.get('/dashboard/call_center/load_products-new/'+$('#item_search').val()+'/'+$(this).val()+'/'+$('#country_id').val()+'/'+$('input[name=channel_id]').val(), function(html){
                    $('div#redeemable-item').html(html);
                    NProgress.done();
                });
            });

            // Handle the item redemption action
            $(document).on('click', '.redeem-item-action', function(e){
                e.preventDefault();

                NProgress.start();

                var data = {
                    user_id    : $('input[name=source_id]').val(),
                    product_id : $(this).data('item'),
                    ticket_id  : TICKET_ID,
                    network_id    : $('#network_id').val(),
                    country_id    : $('#country_id').val(),
                    channel_id    : $('input[name=channel_id]').val()
                };

                $.post('/dashboard/call_center/redeem_item', data, function(resp){
                    NProgress.done();

                    alert(resp.message);

                    if(!resp.failed) {
                        closeDialog();

                        $.get('/dashboard/call_center/balance/'+data.user_id, function(json) {
                            $('input[name=balance]').val(json.balance);
                        });
                    }
                });
            });

            // Handle the item redemption action
            $(document).on('click', '#send-pin', function(e){
                e.preventDefault();

                user_id = $(this).data('user');

                $.get('/dashboard/members/sendpin/' + user_id, function(resp){
                    NProgress.done();

                    alert(resp.message);

                });
            });

            $(document).on('click', '#ticket_redeem_travel', function(e){
                e.preventDefault();

                $('#from_travel').val('1');
                $('#ticket').val('Redeem Travel');
                var ticket_id	= $('#ticket_id').val();
                $.ajax({

                    type:"POST",
                    url:'/dashboard/call_center/update/'+ticket_id,
                    data:$('.ticket_member_form').serialize(),
                    dataType: 'json',
                    success: function(data){
                        alert(data.msg);
                    },
                    error: function(data){
                        exit;
                    }
                });
//				$('.ticket_member_form').click();
            });

            // Handle the item redemption action
            $(document).on('click', '#reverse-trx', function(e){
                e.preventDefault();
                trx_id = $(this).data('trx');
                if(!confirm('Are you sure you want to proceed with the reversal?')){
                    return false;
                }
                $.get('/dashboard/call_center/reverse_trx/' + trx_id, function(resp){
                    NProgress.done();
                    if(resp.result.status == 600){
                        alert("Could not reverse the transaction, the user has insufficient network balance.");
                    }
                    else{
                        if($('#dialog-container').is(':visible')){
                            $.get('/dashboard/call_center/reverse_module/' + trx_id, function(resp){
                                NProgress.done();
                                var response = $.parseJSON( resp );
                                $("#tabs-1").html(response.trxItems);
                                $("#tabs-2").html(response.trxItemsDelivery);
                            });
                        }
                        if(resp.cash > 0){
                            alert("Transaction Number: " + resp.reversed + " has been Reversed. Please note " + resp.currency + resp.cash + " should be credited back to the member\'s account");
                        }else{
                            alert("Transaction Number: " + resp.reversed + " has been Reversed.");
                        }
                    }

                });
                $(this).attr('disabled', true);
                $(this).removeAttr('id');
            });
            // Verify email/mobile uniqueness
            $(document).on('blur','#mobile, #email', function(){
                _this = $(this);
                var user_id = $('input[name=source_id]').val();
                if (!_this.val()) {
                    $('#'+_this.attr('name')+'-error').hide();
                    $('.form-buttons .pure-button-primary').prop('disabled', false);
                    return;
                }
                $.get('/dashboard/members/verify/'+_this.attr('name')+'/'+$(this).val()+'/'+user_id, function(json){
                    if(!json.unique){
                        $('#'+_this.attr('name')+'-error').show();
                        $('.form-buttons .pure-button-primary').prop('disabled', true);
                    } else {
                        $('#'+_this.attr('name')+'-error').hide();
                        $('.form-buttons .pure-button-primary').prop('disabled', false);
                    }
                });
            });

            // Handle the new reverse functionality
            $(document).on('click', '#new-reverse', function(e){
                e.preventDefault();
                trx_id = $(this).data('trx');

                $.get('/dashboard/call_center/reverse_module/' + trx_id, function(resp){
                    NProgress.done();
                    var response = $.parseJSON( resp );
                    $("#tabs-1").html(response.trxItems);
                    $("#tabs-2").html(response.trxItemsDelivery);
                });
            });

            // Force a ticket load
            @if(!$ticket->draft)
            loadSupportProfile();
            @endif

            $(document).on('click', '#reverse-trx-item', function(e) {
                e.preventDefault();
                var trx_id = $('#dialog-wrapper #reverse-trx').data('trx');
                var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9+/=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/rn/g,"n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}

                var qty = $('#quantity'+Base64.decode($(this).data('trx'))).val();
                var trx_item_id  = $(this).data('trx');
                if(qty == 0){
                    alert('please choose a quantity');
                    return false;
                }

                if(!confirm('Are you sure you want to proceed with the reversal of this Item?')){
                    return false;
                }

                $.post('/dashboard/call_center/reverse_trx_item/' + trx_item_id + '/'+ qty, function(resp){
                    console.log(resp);
                    NProgress.done();
                    if(resp.cash > 0){
                        alert('Item Transaction Reversed. Please note ' + resp.currency + resp.cash +' should be credited back to the member\'s account');
                    }
                    else{
                        alert('Item Transaction Reversed.');
                    }
                    if($('#dialog-container').is(':visible')){
                        $.get('/dashboard/call_center/reverse_module/' + trx_id, function(resp){
                            NProgress.done();
                            var response = $.parseJSON( resp );
                            $("#tabs-1").html(response.trxItems);
                            $("#tabs-2").html(response.trxItemsDelivery);
                        });
                    }
                });

            });

            $(document).on('click', '#reverse-trx-delivery', function(e) {
                e.preventDefault();
                // Create Base64 Object
                var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9+/=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/rn/g,"n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}
                var trx_id = $('#dialog-wrapper #reverse-trx').data('trx');
                console.log('trx ' + trx_id);
                var qty = $('#quantity'+Base64.decode($(this).data('trx'))).val();
                var trx_item_id  = $(this).data('trx');
                if(qty == 0){
                    alert('please choose a quantity');
                    return false;
                }

                if(!confirm("Are you sure you want to proceed with the reversal of this Item delivery?")){
                    return false;
                }

                $.post('/dashboard/call_center/reverse_trx_delivery/' + trx_item_id + '/'+ qty, function(resp){
                    NProgress.done();
                    alert('Item Delivery Transaction Reversed.');
                    if($('#dialog-container').is(':visible')){
                        console.log('trx ' + trx_id);
                        $.get('/dashboard/call_center/reverse_module/' + trx_id, function(resp){
                            NProgress.done();
                            var response = $.parseJSON( resp );
                            $("#tabs-1").html(response.trxItems);
                            $("#tabs-2").html(response.trxItemsDelivery);
                        });
                    }
                });

            });
        });
    </script>
@stop