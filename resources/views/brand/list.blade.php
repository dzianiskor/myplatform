@extends('layouts.dashboard')

@section('title')
    Brands | BLU
@stop

@section('body')
<div class="padding">

    <div class="content-head pure-g">
        <div class="title pure-u-1-2">
            <h1><i class="fa fa-book fa-fw"></i>Brands ({{ $brands->total() }})</h1>
        </div>

        <div class="controls pure-u-1-2">
            <form action="{{ Filter::searchUrl() }}" method="get">
                <input type="text" placeholder="Search" name="q" value="{{ Input::get('q') }}" />
                <button type="submit" title="" class="list-search"></button>
            </form>
            @if(I::can('create_brands'))
                <a href="{{ url('dashboard/brands/new') }}" class="pure-button pure-button-primary">Add Brand</a>
            @endif
        </div>
    </div>

    <table class="pure-table">
        <thead>
            <tr>
                <th width="50"><a href="{{ Filter::baseUrl('sort=id') }}">ID</a></th>
                <th style="text-align:left;padding-left:20px"><a href="{{ Filter::baseUrl('sort=name') }}">Name</a></th>
                <th width="150">Actions</th>
            </tr>
        </thead>

        <tbody>
        @if(count($brands) > 0)
            @foreach ($brands as $brand)
                <tr>
                    <td>
                        {{ $brand->id }}
                    </td>
                    <td style="text-align:left;padding-left:20px">
                        {{{ $brand->name }}}
                    </td>
                    <td>
                        <a href="{{ url('dashboard/brands/view/'.base64_encode($brand->id)) }}" class="edit" title="View">View</a>

                        @if(I::can('delete_brands'))
                            <a href="{{ url('dashboard/brands/delete/'.base64_encode($brand->id)) }}" class="delete" title="Delete">Delete</a>
                        @endif
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="3">No Brands Found</td>
            </tr>
        @endif
        </tbody>
    </table>

    {{ $brands->setPath('brands')->appends(Input::except('page'))->links() }}

</div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-brands');
    </script>
@stop
