{{ Form::open(array('url'    => url('/dashboard/brands/updatePopover/'.base64_encode($brand->id)),
                    'method' => 'post',
                    'class'  => 'pure-form pure-form-stacked',
                    'id'     => 'new-brand-form',
                    'style'  => 'width:300px')) }}
    
    @include('brand/form')

    <!-- Form Buttons -->
    <div class="form-buttons">
        <div class="right">
            <a href="#" class="pure-button pure-button-primary brand-save">Save</a>
        </div>
        <div class="clearfix"></div>
    </div>
    <!-- /Form Buttons -->

{{ Form::close() }}
