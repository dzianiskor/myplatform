@extends('layouts.dashboard')

@section('title')
    Update Brand | BLU
@stop

@section('body')
<div class="padding">

    <div class="content-head pure-g">
        <div class="title pure-u-1-2">
            <h1><i class="fa fa-book fa-fw"></i>Brand &raquo; {{{ $brand->name }}}</h1>
        </div>

        <div class="controls pure-u-1-2">
            <a href="{{ url('dashboard/brands') }}" class="pure-button">All Brands</a>
        </div>
    </div>

    {{ Form::model($brand, array('action' => array('BrandController@saveBrand', base64_encode($brand->id)),
                                 'class'  => 'pure-form pure-form-stacked form-width')) }}

        @include('partials.errors')

        @include('brand/form', array('brand' => $brand))

        {!! Form::hidden('redirect_to', URL::previous()) !!}

        <!-- Form Buttons -->
        <div class="form-buttons">
            <div class="left">
                <a href="{{ url('dashboard/brands') }}" class="pure-button">Cancel</a>
            </div>
                <div class="right">
                    <button type="submit" class="pure-button pure-button-primary">Save Brand</button>
                </div>
            <div class="clearfix"></div>
        </div>
        <!-- /Form Buttons -->
    {{ Form::close() }}
    
    {{--- Brandtranslations ---}}
            <fieldset>
                <legend>Brand Translation</legend>

                <table id="brandtranslation-list" class="pure-table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Brandtranslation Name</th>
                            <th>Language</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($brand->brandtranslations) > 0)
                            @foreach($brand->brandtranslations as $brandtranslation)
                                <tr>
                                    <td>{{ $brandtranslation->id }}</td>
                                    @if($brandtranslation->draft)
                                        <td>{{{ $brandtranslation->name }}} (Draft)</td>
                                    @else
                                        <td>{{{ $brandtranslation->name }}}</td>
                                    @endif
                                    <td>
                                        <?php
                                            $lang = App\Language::where('id', $brandtranslation->lang_id)->first();
                                            
                                            echo $lang->name;
                                        ?>
                                    </td>
                                    <td>
                                        @if(!I::can('delete_brands') && !I::can('edit_brands'))
                                            N/A
                                        @endif
                                        @if(I::can('edit_brands'))
                                            <a href="{{ url('dashboard/brandtranslation/edit/'.base64_encode($brandtranslation->id)) }}" class="edit-brandtranslation" title="Edit">Edit</a>
                                        @endif
                                        @if(I::can('delete_brands'))
                                            <a href="{{ url('dashboard/brandtranslation/delete/'.base64_encode($brandtranslation->id)) }}" class="delete-brandtranslation" title="Delete">Delete</a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr class="empty">
                                <td colspan="4">No Brand translation defined</td>
                            </tr>
                        @endif
                    </tbody>
                </table>

                @if($brand->draft)
                    @if(I::can('create_brands'))
                        <a href="#dialog-new-brandtranslation" data-brand_id="{{ base64_encode($brand->id)  }}" class="btn-new-brandtranslation top-space" title="Add new brandtranslation location">
                            <i class="fa fa-plus-circle fa-fw"></i>Add new brandtranslation
                        </a>
                    @endif
                @else
                    @if(I::can('edit_brands'))
                        <a href="#dialog-new-brandtranslation" data-brand_id="{{ base64_encode($brand->id)  }}" class="btn-new-brandtranslation top-space" title="Add new brandtranslation location">
                            <i class="fa fa-plus-circle fa-fw"></i>Add new brandtranslation
                        </a>
                    @endif
                @endif
            </fieldset>
            {{--- /Brandtranslations ---}}
    
</div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-brands');
        enqueue_script('new-brand');
    </script>
@stop
