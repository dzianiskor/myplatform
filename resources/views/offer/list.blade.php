@extends('layouts.dashboard')

@section('title')
Offers | BLU
@stop

@section('body')
<div class="padding">

    <div class="content-head pure-g">
        <div class="title pure-u-1-2">
            <h1><i class="fa fa-eye fa-fw"></i> Offers ({{ $offers->total() }})</h1>
        </div>

        <div class="controls pure-u-1-2">
            <form action="{{ Filter::searchUrl() }}" method="get">
                <?php
                $querystring = isset($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING'] : '';
                if ($querystring != '') {
                    $queryArr = explode('&', $querystring);
                    foreach ($queryArr as $val) {
                        if (empty($val)) {
                            continue;
                        }
                        $valArr = explode('=', $val);
                        if ($valArr[0] != "q") {
                            echo '<input type="hidden" name="' . urldecode($valArr[0]) . '" value="' . $valArr[1] . '"> ';
                        }
                    }
                }
                ?>
                <input type="text" placeholder="Search" name="q" value="{{ Input::get('q') }}" />
                <button type="submit" title="" class="list-search"></button>
            </form>
            @if(I::can('create_offer'))
                <a href="{{ url('dashboard/offer/new') }}" class="pure-button pure-button-primary">Add Offer</a>
            @endif
        </div>

        <div id="catalog-filter" class="pure-u-1-2">
            <input id="filter_route" type="hidden" value="offer">
            <form id="filter-catalogue" action="{{ Filter::searchUrl() }}" method="get">

                <div class="filter-block filter-block-pricing-width">
                    <span class="filter-label">Partner:</span>
                    <select style="width:100%;" name="partner_list[]" data-filter-catalogue="partner_list" data-filter-catalogue-title="Select Partner" multiple="multiple">
                        @foreach ($lists['partners'] as $key => $value)
                            <option value="{{ $key }}">{{ $value }}</option>
                        @endforeach
                    </select>
                    <input data-filter-catalogue="partner_list" type="hidden" value="{{ Input::get('partner_list') }}">
                </div>

                <div class="filter-block filter-block-pricing-width">
                    <span class="filter-label">Country:</span>
                    <select style="width:100%;" name="country_list[]" data-filter-catalogue="country_list" data-filter-catalogue-title="Select Country" multiple="multiple">
                        @foreach ($lists['countries'] as $key => $value)
                            <option value="{{ $key }}">{{ $value }}</option>
                        @endforeach
                    </select>
                    <input data-filter-catalogue="country_list" type="hidden" value="{{ Input::get('country_list') }}">
                </div>

                <div class="filter-block filter-block-pricing-width">
                    <span class="filter-label">Channel:</span>
                    <select style="width:100%;" name="offer_displaychannel_list[]" data-filter-catalogue="offer_displaychannel_list" data-filter-catalogue-title="Select Channel" multiple="multiple">
                        @foreach ($lists['partners'] as $key => $value)
                            <option value="{{ $key }}">{{ $value }}</option>
                        @endforeach
                    </select>
                    <input data-filter-catalogue="offer_displaychannel_list" type="hidden" value="{{ Input::get('offer_displaychannel_list') }}">
                </div>

                <div class="filter-block filter-block-pricing-width">
                    <span class="filter-label">Status:</span>
                    <select style="width:100%;" name="offer_status[]" data-filter-catalogue="offer_status" data-filter-catalogue-title="Select Status" multiple="multiple">
                        @foreach ($lists['statuses'] as $key => $value)
                            <option value="{{ $key }}">{{ $value }}</option>
                        @endforeach
                    </select>
                    <input data-filter-catalogue="offer_status" type="hidden" value="{{ Input::get('offer_status') }}">
                </div>

                <div class="submit-filter-block disable-width">
                    <div style="float:right;">
                        <a href="<?php echo e(url('dashboard/offer')); ?>" title="Clear Filters" class="pure-button pure-button-primary">
                            Clear Filters <i class="fa fa-ban fa-fw"></i>
                        </a>
                    </div>
                    <button class="pure-button pure-button-primary" type="submit" title="" style="float:right;margin-right:15px;">
                        Submit Filters
                    </button>
                </div>
            </form>
        </div>
    </div>

    <table class="pure-table offer_list">
        <thead>
            <tr>
                <th><a href="{{ Filter::baseUrl('sort=id') }}">ID</a></th>
                <th>Preview</th>
                <th><a href="{{ Filter::baseUrl('sort=name') }}">Name</a></th>
                <th>Status</th>
                <th>Actions</th>
            </tr>
        </thead>

        <tbody>
            @if(count($offers) > 0)
            @foreach ($offers as $offer)
            <tr>
                <td>
                    {{{ $offer->id }}}

                </td>
                <td>
                    @if(empty($offer->cover_image))
                    <img src="https://placehold.it/74x74" alt="" />
                    @else
                    <img src="<?php echo App\Http\Controllers\MediaController::getImageFromId($offer->cover_image) ?>?s=74x74" alt="" />
                    @endif
                </td>
                <td style="text-align:left;padding-left:20px;">
                    <a href="{{ url('dashboard/offer/view/'.base64_encode($offer->id)) }}" class="edit" title="Edit">{{ $offer->name }}</a>
                </td>
                <td>
                    {{ $offer->status }}
                </td>
                <td>
                    @if(I::can('view_offer'))
                    <a href="{{ url('dashboard/offer/view/'.base64_encode($offer->id)) }}" class="edit" title="View">View</a>
                    @endif

                    @if(I::can('delete_offer'))
                    <a href="{{ url('dashboard/offer/delete/'.base64_encode($offer->id)) }}" class="delete" title="Delete">Delete</a>
                    @endif
                </td>
            </tr>
            @endforeach
            @else
            <tr>
                <td colspan="5">No Items Found</td>
            </tr>
            @endif
        </tbody>
    </table>

    {{ $offers->setPath('offer')->appends(Input::except('page'))->links() }}

</div>
@stop

@section('page_script')
<script>
    set_menu('mnu-offers');
</script>
@stop