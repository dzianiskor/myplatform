{{-- Segment Listing --}}
<h3>
    @if(I::can_edit('offer', $offer))
    <a href="#dialog-add-segment" title="New Segment" class="dialog" id="segments_dialog">
        Segments <i class="fa fa-plus-circle fa-fw"></i>
    </a>
    @else
        Segments
    @endif
</h3>
<ul id="segment-listing">
    @include('offer/list/segmentList', array('offer'=>$offer))
</ul>
{{-- /Segment Listing --}}
