@if(count($offer->tiers) > 0)
    @foreach($offer->tiers as $t)
        <li>
            {{ $t->name }} (<?php $tier = App\Tier::find($t->tier_id); echo $t->name;?>)
            @if(I::can('delete_offer'))
                @if (Auth::User()->partners->contains($t->partner_id) || I::am("BLU Admin"))
                    <a href="/dashboard/offer/unlink_tier/{{ base64_encode($offer->id) }}/{{ base64_encode($t->id) }}" class="delete-ajax" data-id="{{ $t->id }}" data-target="#tier-listing">
                        [Delete]
                    </a>
                @endif
            @endif
        </li>
    @endforeach
@else
    <li class="empty">No Tiers Associated</li>
@endif
