@if(count($offer->segments) > 0)
    @foreach($offer->segments as $s)
        @if (!$segments->has($s->id))
            @continue
        @endif
        <li>
            {{ $s->name }}
            @if(I::can('delete_offer') || I::am("BLU Admin"))
                <a href="/dashboard/offer/unlink_segment/{{ base64_encode($offer->id) }}/{{ base64_encode($s->id) }}" class="delete-ajax" data-id="{{ $s->id }}" data-target="#segment-listing">
                    [Delete]
                </a>
            @endif
        </li>
    @endforeach
@else
    <li class="empty">No Segments Associated</li>
@endif
