{{-- Partner Listing --}}
<h3>
    @if(I::can_edit('offer', $offer))
    <a href="#dialog-add-partner" title="New Partner" class="dialog">
        Partners <i class="fa fa-plus-circle fa-fw"></i>
    </a>
    @else
        Partners
    @endif
</h3>
<ul id="partner-listing">
    @include('offer/list/partnerList', array('offer'=>$offer))
</ul>
<label class="error" style="display: none;">This field is required.</label>
{{-- /Partner Listing --}}
