{{-- Tiers Listing --}}
<h3>
    @if(I::can_edit('offer', $offer))
    <a href="#dialog-add-tier" title="New Tier" class="dialog">
        Tiers <i class="fa fa-plus-circle fa-fw"></i>
    </a>
    @else
        Tiers
    @endif
</h3>
<ul id="tier-listing">
    @include('offer/list/tierList', array('offer'=>$offer))
</ul>
{{-- /Tiers Listing --}}
