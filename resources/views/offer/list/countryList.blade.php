@if(count($offer->countries) > 0)
    @foreach($offer->countries as $c)
        <li>
            {{ $c->name }}
            @if(I::can('delete_offer'))
            <a href="/dashboard/offer/unlink_country/{{ base64_encode($offer->id) }}/{{ base64_encode($c->id) }}" class="delete-ajax" data-id="{{ $c->id }}" data-target="#country-listing">
                [Delete]
            </a>
            @endif
        </li>
    @endforeach
@else
    <li class="empty">No Country Associated</li>
@endif
