@if($offer->channels()->count() > 0)
    @foreach($offer->channels as $c)
        @if (!$partners->has($c->channel_id))
            @continue
        @endif
        <li>
            @if( is_object($c->channel) )
                {{ $c->channel->name }}
            @else
                {{ Deleted }}
            @endif
            <?php
            if (I::am('BLU Admin') || I::can('delete_offer')) {
                echo '<a href="/dashboard/offer/unlink_channel/' . base64_encode($offer->id) . '/' . base64_encode($c->channel_id) . '" class="delete-ajax" data-id="' . $c->channel_id . '" data-target="#channel-listing">
                    [Delete]
                </a>';
            }
            ?>

        </li>
    @endforeach
@else
    <li class="empty">No Channels Associated</li>
@endif
