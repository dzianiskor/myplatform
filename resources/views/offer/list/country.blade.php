{{-- Country Listing --}}
<h3>
    @if(I::can_edit('offer', $offer))
    <a href="#dialog-add-country" title="New Country" class="dialog">
        Countries <i class="fa fa-plus-circle fa-fw"></i>
    </a>
    @else
        Countries
    @endif
</h3>
<ul id="country-listing">
    @include('offer/list/countryList', array('offer'=>$offer))
</ul>
{{-- /Country Listing --}}
