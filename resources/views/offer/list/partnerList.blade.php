@if(count($offer->partners) > 0)
    @foreach($offer->partners as $p)
        @if (!$partners->has($p->id))
            @continue
        @endif
        <li>
            {{ $p->name }}
            @if(I::am('BLU Admin') || I::can('delete_offer'))
                @if ($p->id != 1)
                    <a href="/dashboard/offer/unlink_partner/{{ base64_encode($offer->id) }}/{{ base64_encode($p->id) }}" class="delete-ajax" data-id="{{ $p->id }}" data-target="#partner-listing">
                        [Delete]
                    </a>
                @endif
            @endif
        </li>
    @endforeach
@else
    <li class="empty">No Partner Associated</li>
@endif
