{{-- Channel Listing --}}
<h3>
    @if(I::can_edit('offer', $offer))
	    <a href="#dialog-add-channel" title="New Channel" class="dialog">
	        Display Channels <i class="fa fa-plus-circle fa-fw"></i>
	    </a>
    @else
        Display Channels
    @endif
</h3>
<ul id="channel-listing">
    @include('offer/list/channelList', array('offer' => $offer))
</ul>
{{-- /Channel Listing --}}
