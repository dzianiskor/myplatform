@extends('layouts.dashboard')

@section('title')
Update Offer | BLU
@stop

@section('dialogs')
@if(isset($errors) && $errors->any())
<h4>{{$errors->first()}}</h4>
@endif

@include('offer/dialog/partner', array('partners'  => $partners))
@include('offer/dialog/country', array('countries' => $countries))
@include('offer/dialog/segment', array('segments'  => $segments))
@include('offer/dialog/channel', array('partners'  => $partners))
@include('offer/dialog/tier',    array('tiers'  => $tiers, 'tiers2' => $tiers2))
@stop

@section('body')
<div class="padding">

    <div class="content-head pure-g">
        <div class="title pure-u-1-2">
            <h1><i class="fa fa-shopping-cart fa-fw"></i> Offer </h1>
        </div>

        <div class="controls pure-u-1-2">
            <a href="{{ url('dashboard/offer') }}" class="pure-button">Offers</a>
        </div>
    </div>

    {{ Form::model($offer, array('action' => array('OfferController@saveOffer', base64_encode($offer->id)),
                                   'method' => 'post',
                                   'files'  => 'true',
                                   'class'  => 'pure-form pure-form-stacked form-width',
                                   'id'     => 'offerForm')) }}

    @include('partials.errors')

    {!! Form::hidden('redirect_to', URL::previous()) !!}

    <div class="pure-g">
        @include('offer/form', array('offer' => $offer))
        <div class="pure-u-1-2 attributes">
            <div class="padding-left-40">
                @include('offer/list/partner', array('offer' => $offer))
                @include('offer/list/segment', array('offer' => $offer))
                @include('offer/list/country', array('offer' => $offer))
                @include('offer/list/channel', array('offer' => $offer))
                @include('offer/list/tier',    array('offer' => $offer))
            </div>
        </div>
    </div>
    {{--- Offertranslations ---}}
    <fieldset>
        <legend>Offer Translation</legend>

        <table id="offertranslation-list" class="pure-table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Offertranslation Name</th>
                    <th>Language</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @if(count($offer->offertranslations) > 0)
                @foreach($offer->offertranslations as $offertranslation)
                <?php $lang = App\Language::where('id', $offertranslation->lang_id)->first(); ?>
                <tr>
                    <td>{{ $offertranslation->id }}</td>
                    @if($offertranslation->draft)
                    <td <?php if ($lang->name == 'Arabic') {
                    echo "style='direction:rtl !important;'";
                } ?>>{{{ $offertranslation->name }}} (Draft)</td>
                    @else
                    <td <?php if ($lang->name == 'Arabic') {
                    echo "style='direction:rtl !important;'";
                } ?> >{{{ $offertranslation->name }}}</td>
                    @endif
                    <td>
<?php
echo $lang->name;
?>
                    </td>
                    <td>
                        @if(I::can('edit_offer'))
                            <a href="{{ url('dashboard/offertranslation/edit/'.base64_encode($offertranslation->id)) }}" class="edit-offertranslation" title="Edit">Edit</a>
                        @endif
                        @if(I::can('delete_offer'))
                            <a href="{{ url('dashboard/offertranslation/delete/'.base64_encode($offertranslation->id)) }}" class="delete-offertranslation" title="Delete">Delete</a>
                        @endif
                        @if(!I::can('delete_offer') && !I::can('edit_offer'))
                            N/A
                        @endif
                    </td>
                </tr>
                @endforeach
                @else
                <tr class="empty">
                    <td colspan="4">No offertranslation locations defined</td>
                </tr>
                @endif
            </tbody>
        </table>

        @if(I::can('create_offer') || I::can('edit_offer'))
            <a href="#dialog-new-offertranslation" data-offer_id="{{ base64_encode($offer->id) }}" class="btn-new-offertranslation top-space" title="Add new offertranslation location">
                <i class="fa fa-plus-circle fa-fw"></i>Add new offertranslation
            </a>
        @endif
    </fieldset>
    {{--- /Offertranslations ---}}
    <!-- Form Buttons -->
    <div class="form-buttons">
        <div class="left">
            <a href="{{ url('dashboard/offer') }}" class="pure-button">Cancel</a>
        </div>

        @if($offer->draft)
            @if(I::can('create_offer'))
                <div class="right">
                    <button type="submit" class="pure-button pure-button-primary">Create Offer</button>
                </div>
            @endif
        @else
            @if(I::can('edit_offer'))
                <div class="right">
                    <button type="submit" class="pure-button pure-button-primary">Save Offer</button>
                </div>
            @endif
        @endif

        <div class="clearfix"></div>
    </div>
    <!-- /Form Buttons -->

    {{ Form::close() }}

</div>
@stop

@section('page_script')
<script>
    $(function(){
        set_menu('mnu-offers');
    });
    enqueue_script('new-offer');
</script>
@stop
