
<div class="pure-u-1-2">
    <fieldset>
        <legend>Basic Details</legend>
        {{ Form::label('status', 'Status') }}
        <select name="status" class="pure-input-1-3">
            @foreach(Meta::offerStatuses() as $k => $v)
                @if($v == $offer->status)
                    <option value="{{ $k }}" selected="">{{ ucfirst($v) }}</option>
                @else
                    <option value="{{ $k }}">{{ ucfirst($v) }}</option>
                @endif
            @endforeach
        </select>
        {{ Form::label('name', 'Name', array('for'=>'name')) }}
        {{ Form::text('name', $offer->name, array('class'=>'pure-input-1 required')) }}

        {{ Form::label('short_description', 'Short Description', array('for'=>'short_description')) }}
        {{ Form::textarea('short_description', $offer->short_description, array('class'=>'pure-input-1')) }}

        <div class="pure-g">
            <div class="pure-u-1-2">
                <div class="padding-right-10">
                    {{ Form::label('validity_start_date', 'Valid From:') }}
                    @if ($offer->validity_start_date == null)
                    {{ Form::text('validity_start_date', '', array('class' => 'pure-input-1 datepicker')) }}
                    @else
                    {{ Form::text('validity_start_date', date("m/d/Y", strtotime($offer->vallidity_start_date)), array('class' => 'pure-input-1 datepicker')) }}
                    @endif
                </div>
            </div>

            <div class="pure-u-1-2">
                {{ Form::label('validity_end_date', 'Valid To:') }}
                @if ($offer->validity_end_date == null)
                {{ Form::text('validity_end_date', '', array('class' => 'pure-input-1 datepicker')) }}
                @else
                {{ Form::text('validity_end_date', date("m/d/Y", strtotime($offer->end_date)), array('class' => 'pure-input-1 datepicker')) }}
                @endif
            </div>
        </div>

        {{ Form::label('description', 'Main Description', array('for'=>'description')) }}
        {{ Form::textarea('description', $offer->description, array('class'=>'pure-input-1')) }}

        {{ Form::label('terms_and_conditions', 'Terms & Conditions', array('for'=>'terms_and_conditions')) }}
        {{ Form::textarea('terms_and_conditions', $offer->terms_and_conditions, array('class'=>'pure-input-1')) }}
        
        {{ Form::label('external_link', 'External Link', array('for'=>'external_link')) }}
        {{ Form::text('external_link', $offer->external_link, array('class'=>'pure-input-1')) }}
        
        <div class="pure-g">
            <div class="pure-u-1-2">
                <div class="padding-right-10">
                    {{ Form::label('start_date', 'Start Date:') }}
                    @if ($offer->start_date == null)
                    {{ Form::text('start_date', '', array('class' => 'pure-input-1 datepicker')) }}
                    @else
                    {{ Form::text('start_date', date("m/d/Y", strtotime($offer->start_date)), array('class' => 'pure-input-1 datepicker')) }}
                    @endif
                </div>
            </div>

            <div class="pure-u-1-2">
                {{ Form::label('end_date', 'End Date:') }}
                @if ($offer->start_date == null)
                {{ Form::text('end_date', '', array('class' => 'pure-input-1 datepicker')) }}
                @else
                {{ Form::text('end_date', date("m/d/Y", strtotime($offer->end_date)), array('class' => 'pure-input-1 datepicker')) }}
                @endif
            </div>
        </div>

        <div class="pure-g">
            <div class="pure-u-1-2">
                {{ Form::label('category_id', 'Category', array('for'=>'category_id')) }}
                {{ Form::select('category_id', $categories, $offer->category_id, array('id' => 'category-list', 'class' => 'pure-input-1')) }}

                @if(I::can('create_categories'))
                <a href="#dialog-new-category" class="btn-new-category top-space" title="Add new category">
                    <i class="fa fa-plus-circle fa-fw"></i>Add new category
                </a>
                @endif
            </div>
            <div class="pure-u-1-2">
                {{ Form::label('reward_type', 'Offer / Reward Type', array('for'=>'reward_type')) }}
                {{ Form::select('reward_type', ['Welcome Rewards', 'Spend Rewards', 'Surprise Rewards', 'On-going Offers'], array('id' => 'reward-type', 'class' => 'pure-input-1')) }}
            </div>
        </div>
        <div class="pure-g">
            <div class="pure-u-1-2">
                {{ Form::label('expiry_period', 'Expiry Period', array('for'=>'expiry_period')) }}
                {{ Form::select('expiry_period', ['1 month', '2 months', '3 months', '4 months', '5 months', '6 months', '7 months', '8 months', '9 months', '10 months', '11 months', '12 months'], array('id' => 'expiry-period', 'class' => 'pure-input-1')) }}
            </div>
        </div>

    </fieldset>

    <fieldset>
        <legend>Offer Image</legend>

        <table style="margin-top:20px" class="file-upload">
            <tr>
                <td width="80">
                    <?php if (empty($offer->cover_image)): ?>
                        <img src="https://placehold.it/74x74" alt="" />
                    <?php else: ?>
                        <img src="<?php echo App\Http\Controllers\MediaController::getImageFromId($offer->cover_image) ?>?s=74x74" alt="" />
                    <?php endif; ?>
                </td>
                <td>
                    {{ Form::label('cover_image', 'Offer Cover Image') }}
                    {{ Form::file('cover_image') }}
                </td>
            </tr>
        </table>
        <p class="description" style="font-size:12px">For best results, use images that are 230x162 (pixels) in size.</p>

    </fieldset>

    <fieldset>
        <legend>Values</legend>
        {{ Markup::checkbox('display_online', 'Display Online?', ($offer->display_online == 1)? true : false, 1) }}
        {{ Markup::checkbox('logged_in', 'Display for Logged in? <i class="fa fa-info-circle fa-fw" title="you have to choose a segment for logged in Offers" style="cursor: help;"></i>', ($offer->logged_in == 1)? true : false, 1) }}
        {{ Markup::checkbox('logged_out', 'Display for Logged out?', ($offer->logged_out == 1)? true : false, 1) }}

    </fieldset>
    
    <fieldset>
        <legend>Email Text Promo</legend>
        {{ Form::label('email_text_promo_1', 'Email Text Promo 1', array('for'=>'email_text_promo_1')) }}
        {{ Form::textarea('email_text_promo_1', $offer->email_text_promo_1, array('class'=>'pure-input-1')) }}
        
        {{ Form::label('email_text_promo_2', 'Email Text Promo 2', array('for'=>'email_text_promo_2')) }}
        {{ Form::textarea('email_text_promo_2', $offer->email_text_promo_2, array('class'=>'pure-input-1')) }}

    </fieldset>
</div>