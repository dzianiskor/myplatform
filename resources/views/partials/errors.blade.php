@if(isset($errors) && count($errors->all()) > 0)
    <div class="errors">
        <ul>
            @foreach($errors->all() as $message)
                <li>{{ $message }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if(Session::has('flash_error'))
    <div class="errors">
        {{ Session::get('flash_error') }}
    </div>
@endif

@if(Session::has('flash_status'))
    <div class="status">
        {{ Session::get('flash_status') }}
    </div>
@endif
