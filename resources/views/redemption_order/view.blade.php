@extends('layouts.dashboard')

@section('title')
    View Redemption Order | BLU
@stop

@section('dialogs')
 {{--- Dialogs ---}}
    <?php foreach ($redemptionOrders as $redemptionOrder) { ?>    
     <div id="dialog-edit-item_{{ $redemptionOrder->id }}" style="width:631px;" class="dialog-edit-item_{{ $redemptionOrder->id }}">
        <h4>Update Redemption Order</h4>
        {{ Form::open(array('url' => url('dashboard/redemption_order/update_order/'.base64_encode($redemptionOrder->id)), 'method' => 'post', 'id' => 'update-order-form', 'class' => "pure-form pure-form-stacked form-dynamic")) }}
        <fieldset>
            <?php if($redemptionOrder->type == 'Item'){ ?>
            <div class="pure-g">
                <div class="pure-u-1-2">
                   {{ Form::label('item_cost', 'Item Cost') }} 
                   {{ Form::text('item_cost', $redemptionOrder->cost, array('class' => "pure-input-1-1 required")) }}
                </div>
                <div class="pure-u-1-2">
                   {{ Form::label('currency', 'Currency') }} 
                   {{ Form::select('currency', $lists['currencies'], $redemptionOrder->cost_currency, array('class' => 'pure-input-1-1 required')) }}
                </div>
                <div class="pure-u-1-2">
                   {{ Form::label('delivery_cost', 'Delivery Cost') }} 
                   {{ Form::text('delivery_cost', $redemptionOrder->delivery_cost, array('class' => "pure-input-1-1 required")) }}
                </div>
                <div class="pure-u-1-2">
                   {{ Form::label('delivery_currency', 'Currency') }} 
                   {{ Form::select('delivery_currency', $lists['currencies'], $redemptionOrder->delivery_cost_currency, array('class' => 'pure-input-1-1 required')) }}
                </div>
                <div class="pure-u-1-2">
                   {{ Form::label('payment_gateway_cost', 'Payment Gateway Cost') }} 
                   {{ Form::text('payment_gateway_cost', $redemptionOrder->payment_gateway_cost, array('class' => "pure-input-1-1 required")) }}
                </div>
                <div class="pure-u-1-2">
                   {{ Form::label('payment_gateway_cost_currency', 'Currency') }} 
                   {{ Form::select('payment_gateway_cost_currency', $lists['currencies'], $redemptionOrder->payment_gateway_cost_currency, array('class' => 'pure-input-1-1 required')) }}
                </div>
                <div class="pure-u-1-2">
                   {{ Form::label('order_status', 'Order Status') }} 
                   {{ Form::select('order_status', $lists['orderStatuses'], $redemptionOrder->order_status, array('class' => 'pure-input-1-1 required')) }}
                </div>
                <div class="pure-u-1-2">
                   {{ Form::label('order_date', 'Order Date') }}
                        {{ Form::text('order_date', $redemptionOrder->order_date, array('class' => 'pure-input-1-1 datepicker-year required')) }}
                </div>
                <div class="pure-u-1-2">
                   {{ Form::label('trackning_number', 'Tracking Number') }}
                        {{ Form::text('tracking_number', $redemptionOrder->tracking_number, array('class' => 'pure-input-1-1 required')) }}
                </div>
                <div class="pure-u-1-2">
                   {{ Form::label('shipping_company', 'Shipping Company') }}
                        {{ Form::text('shipping_company', $redemptionOrder->shipping_company, array('class' => 'pure-input-1-1 required')) }}
                </div>
                <div class="pure-u-1-2">
                   {{ Form::label('suppliers', 'Suppliers') }} 
                   {{ Form::select('suppliers', $lists['suppliers'], $redemptionOrder->supplier_id, array('class' => 'pure-input-1-1 required')) }}
                </div>
            </div>
            <?php } else{ ?>
                    <div class="pure-g">
                <div class="pure-u-1-2">
                   {{ Form::label('cost', 'Cost') }} 
                   {{ Form::text('cost', $redemptionOrder->cost, array('class' => "pure-input-1-1 required")) }}
                </div>
                <div class="pure-u-1-2">
                   {{ Form::label('currency', 'Currency') }} 
                   {{ Form::select('currency', $lists['currencies'], $redemptionOrder->cost_currency, array('class' => 'pure-input-1-1 required')) }}
                </div>
                <div class="pure-u-1-2">
                   {{ Form::label('airline_fees', 'Airline Fees') }} 
                   {{ Form::text('airline_fees', $redemptionOrder->airline_fees, array('class' => "pure-input-1-1 required")) }}
                </div>
                <div class="pure-u-1-2">
                   {{ Form::label('airline_fees_currency', 'Currency') }} 
                   {{ Form::select('airline_fees_currency', $lists['currencies'], $redemptionOrder->airline_fees_currency, array('class' => 'pure-input-1-1 required')) }}
                </div>
                <div class="pure-u-1-2">
                   {{ Form::label('payment_gateway_cost', 'Payment Gateway Cost') }} 
                   {{ Form::text('payment_gateway_cost', $redemptionOrder->payment_gateway_cost, array('class' => "pure-input-1-1 required")) }}
                </div>
                <div class="pure-u-1-2">
                   {{ Form::label('payment_gateway_cost_currency', 'Currency') }} 
                   {{ Form::select('payment_gateway_cost_currency', $lists['currencies'], $redemptionOrder->payment_gateway_cost_currency, array('class' => 'pure-input-1-1 required')) }}
                </div>
                <div class="pure-u-1-2">
                   {{ Form::label('order_status', 'Order Status') }} 
                   {{ Form::select('order_status', $lists['orderStatuses'], $redemptionOrder->order_status, array('class' => 'pure-input-1-1 required')) }}
                </div>
                    <?php		
                    if(empty($redemptionOrder->order_date) || $redemptionOrder->order_date == '1970-01-01'){		
                    $orderDate  = '';		
                    }else{		
                    $orderDate  = $redemptionOrder->order_date;		
                    }		
                    ?>
                <div class="pure-u-1-2">
                   {{ Form::label('order_date', 'Order Date') }}
                   {{ Form::text('order_date', $orderDate, array('class' => 'pure-input-1-1 datepicker-year')) }}
                </div>
                <div class="pure-u-1-2">
                   {{ Form::label('suppliers', 'Suppliers') }} 
                   {{ Form::select('suppliers', $lists['suppliers'], $redemptionOrder->supplier_id, array('class' => 'pure-input-1-1 required')) }}
                </div>
            </div>
            <?php } ?>
             <a href="#" id="update-redemption-order" data-redemption_order_id="{{ base64_encode($redemptionOrder->id) }}" class="pure-button" style="margin-right:10px; float: right;">Save</a>
        </fieldset>    
        {{ Form::close() }}
    </div>
    <?php } ?>
@stop

@section('body')
<?php 

$redemptionsType =  $redOrder->type; 
$user       =  App\User::find($redOrder->user_id);
$trx        = App\Transaction::find($redOrder->trx_id);  
$network    = App\Network::find($trx->network_id);
$admin      = App\Admin::find($trx->auth_staff_id);
$notes      = json_decode($redOrder->notes, true);
if (!$admin) {
    if ($trx->reversed == 1) {
        $reversedTrx = App\Transaction::where('ref_number', $redOrder->trx_id)->first();
        $revAdmin = App\Admin::find($reversedTrx->auth_staff_id);
        $ReversedBy = $revAdmin->first_name . ' ' . $revAdmin->last_name;
    } else {
        $ReversedBy = 'N/A';
    }
} else {
    $ReversedBy = $admin->first_name . ' ' . $admin->last_name;
}
if (!empty($notes['address_1'])) {
    $address = $notes['address_1'];
} else {
    $address = $user->address_1;
}
if (!empty($notes['address_2'])) {
    $address2 = $notes['address_2'];
} else {
    $address2 = $user->address_2;
}
?>
<div class="padding">
    <div class="content-head pure-g">
        <div class="title pure-u-1-2">
            <h1><i class="fa fa-globe fa-fw"></i>Redemption Order </h1>
        </div>
        <div class="controls pure-u-1-2">
        <a href="{{ url('dashboard/redemption_order') }}" class="pure-button">All Redemption Orders</a>
    </div>
    </div>  
    	<?php		
            $partner    = App\Partner::find($trx->partner_id);		
            $reference = App\Reference::where('user_id', $user->id)->where('partner_id', $trx->partner_id)->first();		
            if(!$reference){		
            $refNumber  = 'N/A';  		
            }else{		
            $refNumber  = $reference->number; 		
            }		
        ?>
    
    <div class="redemption-order-details">
        <div class="redemption-order-number">Redemption Order #:{{ $redemptionOrder->trx_id }}</div>
            <div class="pure-u-1-2">		
            <div><strong>Member Details:</strong></div>		
            <div><span>Member ID:</span> {{ $user->id }}</div>		
            <div><span>Reference:</span> {{ $refNumber }}</div>		
            <div><span>Customer Name:</span> {{ $user->first_name . ' ' . $user->last_name }}</div>		
            <div><span>Address:</span> {{ $address }}</div>		
            <div><span>Address 2:</span> {{ $address2 }}</div>		
            </div>		
            <div class="pure-u-1-2">		
            <div><strong>Transaction Details:</strong></div>		
            <div><span>Partner Name:</span> {{ $partner->name }}</div>		
            <div><span>Country Of Order:</span> {{ App\Country::find($trx->country_id)->name }}</div>		
            <div><span>Reversed By:</span> {{ $ReversedBy }}</div>		
            </div>
    </div>
    @if($errors->any())
        <h6 style="color: red;">{{$errors->first()}}</h6>
    @endif
    <?php if($redemptionsType == 'Item'){ ?>
        <table id="redemption-order-table" class="pure-table display responsive nowrap" cellspacing="0">
            <thead>
                <tr>
                    <th>ID</th>
                    <!--<th>User ID</th>-->
<!--                    <th>Product ID</th>-->
                    <th>Product</th>
<!--                    <th>Model</th>
                    <th>Sub-Model</th>-->
                    <th>quantity</th>
                    <th>Paid in Points</th>
                    <th>Delivery in Points</th>
                    <th>Network Currency</th>
                    <th>Paid in Cash</th>
                    <th>Currency</th>
                    <th>Full Price Paid in USD</th>
                    <th>Cost</th>
                    <th>Cost Currency</th>
                    <th>Supplier</th>
                    <th>Delivery Cost</th>
                    <th>Delivery Currency</th>
                    <th>Payment Gateway Cost</th>
                    <th>Payment Gateway Cost Currency</th>
                    <th>Order Status</th>
                    <th>Date of Order</th>
                    <th>Tracking Number</th>
                    <th>Shipping Company</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($redemptionOrders as $redemptionOrder) {
                if($trx->trans_class == 'Reversal Delivery'){
                $trxItem = TransactionItemDelivery::where('trx_item_id', $redemptionOrder->trx_item_id)->where('trx_id', $redemptionOrder->trx_id)->first();
                }
                else{
                $trxItem = App\TransactionItem::find($redemptionOrder->trx_item_id);
                }
                $Product = App\Product::find($redemptionOrder->product_id);
                $supplierName   = 'N/A';		
                $supplier       = App\Supplier::find($redemptionOrder->supplier_id);		
                if($supplier){		
                $supplierName   = $supplier->name;		
                }

                if(!$Product){
                        $productName    = 'N/A';
                    }else{
                        $productName    = $Product->name;
                    }
                if(empty($redemptionOrder->order_date) || $redemptionOrder->order_date == '1970-01-01'){		
                $orderDate  = 'N/A';		
                }else{		
                $orderDate  = $redemptionOrder->order_date;		
                }		
                $costCurrencyShortCode = 'USD';		
                $costCurrency = App\Currency::find($redemptionOrder->cost_currency);		
                if($costCurrency){		
                $costCurrencyShortCode = $costCurrency->short_code;		
                }
                ?>
                    <tr>
                        <td>{{ $redemptionOrder->id }}</td>
                        <td>
                             <a href="{{ url('dashboard/productpricing/view/'.base64_encode($redemptionOrder->product_id)) }}" class="tooltip">
                               {{ $productName }} 
                               <span class="tooltiptext">Model : {{ $redemptionOrder->product_model }} <br/> Sub-Model : {{ $redemptionOrder->product_sub_model }}</span>
                            </a>
                            
                        </td>
                        <td>{{ $redemptionOrder->quantity }}</td>
                        <?php
                           if(isset($trxItem->paid_in_points)){		
                           echo "<td>" . number_format($trxItem->paid_in_points,0) . "</td>";		
                           }		
                           else{		
                           echo "<td>0</td>";		
                           }		
                       ?>
                        <td>{{ $trxItem->delivery_cost }}</td>
                        <td>{{ $network->name }}</td>
                        <td>{{ $redemptionOrder->cash_payment }}</td>
                        <td>{{ App\Currency::find($redemptionOrder->cash_payment_currency)->short_code }}</td>
                        <td>{{ $redemptionOrder->full_price_in_usd }}</td>
                        <td>{{ $redemptionOrder->cost }}</td>
                        <td>{{ $costCurrencyShortCode }}</td>
                        <td>{{ $supplierName }}</td>
                        <td>{{ $redemptionOrder->delivery_cost }}</td>
                        <td>{{ App\Currency::find($redemptionOrder->delivery_cost_currency)->short_code }}</td>
                        <td>{{ $redemptionOrder->payment_gateway_cost }}</td>
                        <td>{{ App\Currency::find($redemptionOrder->payment_gateway_cost_currency)->short_code }}</td>
                        <td>{{ $redemptionOrder->order_status }}</td>
                        <td>{{ $orderDate }}</td>
                        <td>{{ $redemptionOrder->tracking_number }}</td>
                        <td>{{ $redemptionOrder->shipping_company }}</td>
                        <td>
                            <?php if(I::can('edit_redemption_order')){ ?>
                                <a href="#dialog-edit-item_{{$redemptionOrder->id}}" id="dialog-edit-item" data-redemption_order="{{ $redemptionOrder->id }}" data-type="{{ $redemptionOrder->type }}" class="dialog pure-button" style="margin-right:10px;" title="Edit">Edit</a>
                            <?php } ?>
                        </td>
                    </tr>  
                   <?php } ?>
                </tbody>
            </table>
        <?php
        } else {
            $supplierName = 'N/A';
            $supplier = App\Supplier::find($redemptionOrder->supplier_id);
            if ($supplier) {
                $supplierName = $supplier->name;
            }
            ?>		
            <table id="redemption-order-table" class="pure-table display responsive nowrap" cellspacing="0">
                <thead>
                <tr>
                    <th><a href="{{ Filter::baseUrl('sort=id') }}">ID</a></th>
                    <th>Notes</th>
                    <th>Passenger Name</th>
                    <th>Company</th>
                    <th>Start Booking Date</th>
                    <th>End Booking Date</th>
                    <th>Class</th>
                    <th>Paid in Points</th>
                    <th>Network Currency</th>
                    <th>Paid in Cash</th>
                    <th>Currency</th>
                    <th>Full Price Paid in USD</th>
                    <th>Cost</th>
                    <th>Cost Currency</th>
                    <th>Supplier</th>
                    <th>Airline Fees</th>
                    <th>Airline Fees Currency</th>
                    <th>Order Status</th>
                    <th>Date of Order</th>
                    <th>Action</th>
                </tr>
            </thead>

            <tbody>
                <?php 
                  foreach ($redemptionOrders as $redemptionOrder) {  
                    switch ($trx->trans_class) {
                        case 'Hotel':
                            $passengerName      = $notes['book_first_name'] . ' ' . $notes['book_last_name'];
                            $startBookingDate   = $notes['arrivalDate'];
                            $endBookingDate     = $notes['departureDate'];
                            $class              = 'N/A';
                            $showNotes          = 'N/A';

                            break;
                        case 'Flight':
                            $passengerName      = $user->first_name . ' ' . $user->last_name;
                            $startBookingDate   = $notes['out_depart_time'];
                            $endBookingDate     = $notes['in_arrive_time'];
                            $class              = $notes['cabinclass'];
                            $showNotes          = $notes['out_carrier_name'];

                            break;
                        case 'Car':
                            $passengerName      = $user->first_name . ' ' . $user->last_name;
                            $startBookingDate   = $notes['pickup'];
                            $endBookingDate     = $notes['dropoff'];
                            $class              = 'N/A';
                            $showNotes          = $notes['vehicle'];
                            break;

                        default:
                            $passengerName      = 'N/A';
                            $startBookingDate   = 'N/A';
                            $endBookingDate     = 'N/A';
                            $class              = 'N/A';
                            $showNotes          = 'N/A';
                            break;
                    }
                    
                    if(empty($redemptionOrder->order_date) || $redemptionOrder->order_date == '1970-01-01'){		
                    $orderDate  = 'N/A';		
                    }else{		
                    $orderDate  = $redemptionOrder->order_date;		
                    }		
                    $costCurrencyShortCode = 'USD';		
                    $costCurrency = App\Currency::find($redemptionOrder->cost_currency);		
                    if($costCurrency){		
                    $costCurrencyShortCode = $costCurrency->short_code;		
                    }		
                    $airlineCurrencyShortCode = 'USD';		
                    $airlinetCurrency = App\Currency::find($redemptionOrder->airline_fees_currency);		
                    if($costCurrency){		
                    $airlineCurrencyShortCode = $airlinetCurrency->short_code;		
                    }
                    ?>
                    <tr>
                        <td>{{ $redemptionOrder->id }}</td>
                        <td>{{ $showNotes }}</td>
                        <td>{{ $passengerName }}</td>
                        <td>{{ $redemptionOrder->company }}</td>
                        <td>{{ $startBookingDate }}</td>
                        <td>{{ $endBookingDate }}</td>
                        <td>{{ $class }}</td>
                        <td>{{ number_format($trx->points_redeemed,0) }}</td>
                        <td>{{ $network->name }}</td>
                        <td>{{ $trx->cash_payment }}</td>
                        <td>{{ App\Currency::find(6)->short_code }}</td>
                        <td>{{ $redemptionOrder->full_price_in_usd }}</td>
                        <td>{{ $redemptionOrder->cost }}</td>
                        <td>{{ $costCurrencyShortCode }}</td>
                        <td>{{ $supplierName }}</td>
                        <td>{{ $redemptionOrder->airline_fees }}</td>
                        <td>{{ $airlineCurrencyShortCode }}</td>
                        <td>{{ $redemptionOrder->order_status }}</td>
                     	<td>{{ $orderDate }}</td>
                        <td>
                            <?php if(  I::can('edit_redemption_order')){ ?>
                                <a href="#dialog-edit-item_{{$redemptionOrder->id}}" id="dialog-edit-item" data-redemption_order="{{ $redemptionOrder->id }}" data-type="{{ $redemptionOrder->type }}" class="dialog pure-button" style="margin-right:10px;" title="Edit">Edit</a>
                            <?php } ?>
                        </td>
                    </tr> 
                <?php } ?>
            </tbody>
        </table>
    <?php } ?>
	<?php // unset($queryAll['page']); ?>
    {{-- $banners->appends($queryAll)->links() --}}
</div>
@stop

@section('page_script')
<script>
    set_menu('mnu-redemption_order');
    $(function() {
        // Handle the new reverse functionality
        $(document).on('click', '#dialog-edit-item', function(e){
            e.preventDefault();
            trx_id = $(this).data('redemption_order');
            redemption_type = $(this).data('type');
        });
        
        $(document).on('click', '#update-redemption-order', function(e) {
             e.preventDefault();
            $('#update-order-form').submit();
        });
    });
</script>
@stop