@extends('layouts.dashboard')

@section('title')
    Redemption Orders | BLU
@stop
@section('dialogs')

@stop
@section('body')
<div class="padding">
    <div class="content-head pure-g">
        <div class="title pure-u-1-2">
            <h1><i class="fa fa-picture-o fa-fw"></i>Redemption Orders ({{ count($redemptionOrders) }})</h1>
        </div>

        <div class="controls pure-u-1-2">
            <form action="{{ Filter::searchUrl() }}" method="get">
                <?php
                $querystring = $_SERVER['QUERY_STRING'];
                $queryAll = array();
                $part_list = array();
                if ($querystring != '') {
                    $queryArr = explode('&', $querystring);
                    foreach ($queryArr as $val) {
                        $valArr = explode('=', $val);
                        if (!empty($queryAll[urldecode($valArr[0])])) {
                            $queryAll[urldecode($valArr[0])] = $queryAll[urldecode($valArr[0])] . ',' . urldecode($valArr[1]);
                        } else {
                            $queryAll[urldecode($valArr[0])] = urldecode($valArr[1]);
                        }
                    }
                }
                ?>
                <input type="text" placeholder="Search" name="q" value="{{ Input::get('q') }}" />
                <button type="submit" title="" class="list-search"></button>
            </form>
        </div>
<!--        <div class="pure-u-1-2" style="width:100%">
            <table style="margin-top: 20px; float: right; width:100%; margin-right:-5px;">
                <tr>
                    <td style="text-align:right;padding-left:20px;">
                        <a href="{{ url('dashboard/redemption_order') }}" title="Clear Filters" class="pure-button-primary1 pure-button">
                            Clear Filters <i class="fa fa-ban fa-fw"></i>
                        </a>
                    </td>
                </tr>
            </table>
        </div>-->
    </div>
        <table class="pure-table">
            <thead>
                <tr>
                    <th>Transaction Id</th>
                    <th>Date</th>
                    <th>Reference</th>
                    <th>Member</th>
                    <th>Country Of Order</th>
                    <th>Type</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($redemptionOrders as $redemptionOrder) { 
                $trx = App\Transaction::find($redemptionOrder->trx_id);
                if (!$trx) {
                    continue;
                }
                $user = App\User::find($trx->user_id); 
                $reference = App\Reference::where('user_id', $user->id)->where('partner_id', $trx->partner_id)->first();
                if(!$reference){
                  $refNumber  = 'N/A';  
                }else{
                   $refNumber  = $reference->number; 
                }
                $countryName = 'N/A';
                $country = App\Country::find($trx->country_id);
                
                 if(!empty($country)){
                    $countryName = $country->name;
                }
                
                ?>
                    <tr>
                        <td>{{ $trx->id }}</td>
                        <td>{{ $trx->created_at }}</td>
                        <td>{{ $refNumber }}</td>
                        <td>{{ $user->first_name . ' ' . $user->last_name }}</td>
                        <td>{{ $countryName }}</td>
                        <td>{{ $trx->trans_class }}</td>
                        <td>
                            <a href="{{ url('dashboard/redemption_order/view/'.base64_encode($redemptionOrder->trx_id)) }}" class="edit" title="Edit">Edit</a>
                        </td>
                    </tr>  
                <?php } ?>
            </tbody>
        </table>

        {{ $redemptionOrders->appends($queryAll)->appends(Input::except('page'))->links() }}

</div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-redemption_order');
    </script>
@stop
