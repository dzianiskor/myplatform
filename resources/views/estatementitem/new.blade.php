<div style="width: 800px !important;">
{{ Form::open(array('url' => url('dashboard/estatementitem/create/'), 'method' => 'post', 'class' => "pure-form pure-form-stacked", 'id' => 'new-estatementitem-form')) }}
    
    @include('partials.errors')                         

    <div class="pure-g">
        <div class="pure-u-1">
            {{ Form::hidden('estatementitem_eid',$data['estatement_id'], null, array('class' => 'pure-input-1')) }}
            {{ Form::label('estatementitem_type', 'Type') }}
            {{ Form::select('estatementitem_type',$lists['itemTypes'], null, array('id'=>'estatementitem_type', 'class' => 'pure-input-1')) }}
            
            {{--- Item Match ---}}
            {{ Form::open(array('url' => "#", 'class' => "pure-form", "id" => 'formcashback')) }}
            <span id="Product">
                {{ Form::label('product_id', 'Item Selection') }}
                <input type="text" id="products-autocomplete" class="items-products inline-block-display" placeholder="Search" />
                <button id="products-search" class="rounded-search">search</button>
                <select name="product_id" id="product_id" class="pure-input-1">
                </select>
            </span>
            
            <span id="Offer" style="display: none">
                {{ Form::label('offer_id', 'Offer Selection') }}
                {{ Form::select('offer_id',$lists['offers'], null, array('class' => 'pure-input-1')) }}
            </span>
            

            {{ Form::close() }}
            {{--- end Item Match ---}}
            
            {{ Form::label('estatementitem_rank', 'Rank') }}
            {{ Form::text('estatementitem_rank', '', array('class' => 'pure-input-1 required')) }}

        </div>

    </div>

    {{-- Dialog Buttons --}}
    <div class="form-buttons">
        <div class="left">
            <a href="#" class="pure-button close-dialog">Cancel</a>
        </div>
        <div class="right">
            <a href="#" class="pure-button pure-button-primary save-new-estatementitem">Save</a>
        </div>
        <div class="clearfix"></div>
    </div>
    {{-- /Dialog Buttons --}}

{{ Form::close() }}
</div>