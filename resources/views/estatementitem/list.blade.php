@if(count($estatementitems) > 0)
@foreach($estatementitems as $estatementitem)
<tr>
    <td>{{ $estatementitem->id }}</td>
    <td>{{ $itemTypes[$estatementitem->type] }}</td>
    @if($estatementitem->type == 'product')
    <td>{{ App\Product::find($estatementitem->item_id)->name }}</td>
    @else
    <td>{{ App\Offer::find($estatementitem->item_id)->name }}</td>
    @endif
    <td>
        {{ $estatementitem->rank }}
    </td>
    <td>
        @if(I::can('delete_estatement'))
            <a href="{{ url('dashboard/estatementitem/delete/'.base64_encode($estatementitem->id)) }}" class="delete-estatementitem" title="Delete">Delete</a>
        @else
            N/A
        @endif
    </td>
</tr>
@endforeach
@else
<tr class="empty">
    <td colspan="5">No Items Linked</td>
</tr>
@endif