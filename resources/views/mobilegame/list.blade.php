@extends('layouts.dashboard')

@section('title')
    Mobile Games | BLU
@stop

@section('body')
<div class="padding">

    <div class="content-head pure-g">
        <div class="title pure-u-1-2">
            <h1><i class="fa fa-money fa-fw"></i> Mobile Games ({{ count($mobilegames) }})</h1>
        </div>

        <div class="controls pure-u-1-2">
            <form action="{{ Filter::searchUrl() }}" method="get">
                <input type="text" placeholder="Search" name="q" value="{{ Input::get('q') }}" />
                <button type="submit" title="" class="list-search"></button>
            </form>
            @if(I::can('create_mobile_game'))
                <a href="{{ url('dashboard/mobilegame/create') }}" class="pure-button pure-button-primary">Add Game</a>
            @endif
        </div>
    </div>

    <table class="pure-table">
        <thead>
            <tr>
                <th width="50"><a href="{{ Filter::baseUrl('sort=id') }}">ID</a></th>
                <th style="padding-left:20px;"><a href="{{ Filter::baseUrl('sort=name') }}">Name</a></th>
                <th>Partner</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        @if(count($mobilegames) > 0)
            @foreach ($mobilegames as $mobilegame)
                <tr>
                    <td>
                        {{ $mobilegame->id }}
                    </td>
                    <td>
                        <a href="/dashboard/mobilegame/edit/{{{base64_encode($mobilegame->id)}}}" class="edit" title="View">{{{ $mobilegame->name }}}</a>
                    </td>
                    <td>
                        {{ $mobilegame->partners->pluck('name')->implode(', ') }}
                    </td>
                    <td>
                        <a href="/dashboard/mobilegame/edit/{{ base64_encode($mobilegame->id) }}" class="edit" title="View">View</a>
                        @if(I::can('delete_mobile_game'))
                            <a href="/dashboard/mobilegame/delete/{{ base64_encode($mobilegame->id) }}" class="delete" title="Delete">Delete</a>
                        @endif
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="5">No Games Found</td>
            </tr>
        @endif
        </tbody>
    </table>

</div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-mobilegame');
    </script>
@stop
