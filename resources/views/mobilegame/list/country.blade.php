{{-- Country Listing --}}
<h3>
    @if(I::can('edit_mobile_game', $mobilegame))
        <a href="#dialog-add-country" title="New Country" class="dialog">
            Countries <i class="fa fa-plus-circle fa-fw"></i>
        </a>
    @else
        Countries
    @endif
</h3>
<ul id="country-listing">
    @include('mobilegame/list/countryList', array('mobilegame'=>$mobilegame))
</ul>
{{-- /Country Listing --}}
