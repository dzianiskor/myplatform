@if(count($mobilegame->countries) > 0)
    @foreach($mobilegame->countries as $c)
        <li>
            {{ $c->name }}
            @if(I::can('delete_mobile_game'))
                <a href="/dashboard/mobilegame/unlink_country/{{ base64_encode($mobilegame->id) }}/{{ base64_encode($c->id) }}" class="delete-ajax" data-id="{{ $c->id }}" data-target="#country-listing">
                    [Delete]
                </a>
            @endif
        </li>
    @endforeach
@else
    <li class="empty">No Country Associated</li>
@endif
