@if(count($mobilegame->partners) > 0)
    @foreach($mobilegame->partners as $p)
        @if (!$partners->has($p->id))
            @continue
        @endif
        <li>
            {{ $p->name }}
            @if(I::can('delete_mobile_game'))
                @if (!Auth::User()->partners->contains($p))
                    <a href="/dashboard/mobilegame/unlink_partner/{{ base64_encode($mobilegame->id) }}/{{ base64_encode($p->id) }}" class="delete-ajax" data-id="{{ $p->id }}" data-target="#partner-listing">
                        [Delete]
                    </a>
                @endif
            @endif
        </li>
    @endforeach
@else
    <li class="empty">No Partner Associated</li>
@endif
