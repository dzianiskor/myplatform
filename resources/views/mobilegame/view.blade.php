@extends('layouts.dashboard')

@section('title')
    Update Mobile Game | BLU
@stop
@section('dialogs')
    {{-- Assign new Country --}}
    @if(I::can('view_mobile_game', $mobilegame))
        @include('mobilegame/dialog/country', array('countries' => $countries))
        @include('mobilegame/dialog/partner', array('partners' => $partners))
    @endif
    
@stop
@section('body')
    <div class="padding">

        <div class="content-head pure-g">
            <div class="title pure-u-1-2">
                <h1><i class="fa fa-money fa-fw"></i> Mobile Game &raquo; {{{ $mobilegame->name }}}</h1>
            </div>

            <div class="controls pure-u-1-2">
                <a href="{{ url('/dashboard/mobilegame') }}" class="pure-button">All Mobile Games</a>
            </div>
        </div>
        
        {{ Form::model($mobilegame, array('url'   => '/dashboard/mobilegame/update/'.base64_encode($mobilegame->id),
                                        'files'  => true,
                                        'method' => 'post',
                                        'class' => 'pure-form pure-form-stacked form-width')) }}

            @include('partials.errors')

            {!! Form::hidden('redirect_to', URL::previous()) !!}

          <div class="pure-g">  
                <div class="pure-u-3-5">
                    @include('mobilegame/form', array('mobilegame' => $mobilegame))
                </div>
            
        
                <div class="pure-u-2-5 attributes">
                        <div class="padding-left-40">
                            @include('mobilegame/list/country', array('mobilegame' => $mobilegame))
                            @include('mobilegame/list/partner', array('mobilegame' => $mobilegame))
                        </div>
                </div>
          </div>  
        <!-- Form Buttons -->
        <div class="form-buttons">
            <div class="left">
                <a href="{{ url('dashboard/mobilegame') }}" class="pure-button">Cancel</a>
            </div>
            @if($mobilegame->draft)
                @if(I::can('create_mobile_game'))
                    <div class="right">
                        <button type="submit" class="pure-button pure-button-primary">Save Mobile Game</button>
                    </div>
                @endif
            @else
                @if(I::can('edit_mobile_game'))
                    <div class="right">
                        <button type="submit" class="pure-button pure-button-primary">Update Mobile Game</button>
                    </div>
                @endif
            @endif
            <div class="clearfix"></div>
        </div>
        <!-- /Form Buttons -->
    {{ Form::close() }}  
    </div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-mobilegame');
    </script>
@stop
