<fieldset>
	<legend>Basic Details</legend>

    {{ Form::label('name', 'Game Name', array('for'=>'name')) }}
    {{ Form::text('name', $mobilegame->name, array('class'=>'pure-input-1 required')) }}
    
    {{ Form::label('status', 'Status', array('for'=>'status')) }}
    <select id="status" name="status" class="pure-input-1">
        <?php if($mobilegame->enabled == 1){
            echo '<option value="1" selected="selected">Enabled</option>
        <option value="0">Disabled</option>';
        }
        else{
            echo '<option value="1">Enabled</option>
            <option value="0" selected="selected">Disabled</option>';
        }
?>
        
    </select>
	
	<div class="spacer"></div>
    {{ Form::label('start_date', 'Start Date', array('for'=>'start_date')) }}
    {{ Form::text('start_date', date('m/d/Y', strtotime($mobilegame->start_date)), array('class' => 'report-input datepicker-report')) }}
	
	<div class="spacer"></div>
    {{ Form::label('end_date', 'End Date', array('for'=>'end_date')) }}
    {{ Form::text('end_date', date('m/d/Y', strtotime($mobilegame->end_date)), array('class' => 'report-input datepicker-report')) }}
	
	<div class="spacer"></div>
    {{ Form::label('timer', 'Timer', array('for'=>'timer')) }}
    {{ Form::text('timer', $mobilegame->timer, array('class'=>'pure-input-1 required')) }}
	
	<div class="spacer"></div>
        <fieldset>
            <span class="info">
                    For best results, use images that are 500x500 (pixels) in size.
            </span>

            <div class="image-rail" style="width:340px;">
                <table class="mobileGame_view" style="margin-top:20px;margin-bottom:20px;">
                    <tr>
                        <td width="80">
                            @if(empty($mobilegame->image()))
                                <img src="https://placehold.it/74x74" alt="" />
                            @else
                                <img src="<?php echo App\Http\Controllers\MediaController::getImageFromId($mobilegame->image()->id) ?>?s=74x74" alt="" />
                            @endif
                        </td>
                        <td style="padding-left:10px;">
                            {{ Form::label('image', 'Mobile Game Image') }}
                            {{ Form::file('image') }}
                        </td>
                    </tr>
                </table>
            </div>

        </fieldset>
        <div class="spacer"></div>
    {{ Form::label('percentage', 'Winning Percentage', array('for'=>'percentage')) }}
    {{ Form::text('percentage', $mobilegame->percentage, array('class'=>'pure-input-1 required')) }}
	
	<div class="spacer"></div>
    {{ Form::label('max_points_to_grant', 'Max Points Granted (during time period)', array('for'=>'max_points_to_grant')) }}
    {{ Form::text('max_points_to_grant', $mobilegame->max_points_to_grant, array('class'=>'pure-input-1 required')) }}
	
	<div class="spacer"></div>
    {{ Form::label('max_points_per_user', 'Maximum Points Granted (per user)', array('for'=>'max_points_per_user')) }}
    {{ Form::text('max_points_per_user', $mobilegame->max_points_per_user, array('class'=>'pure-input-1 required')) }}
	
	<div class="spacer"></div>
        {{ Form::label('difficulty', 'Status', array('for'=>'difficulty')) }}
    <select id="difficulty" name="difficulty" class="pure-input-1">
        <?php 
            if($mobilegame->difficulty == 'easy'){
                echo '<option value="easy" selected="selected">Easy</option>
                    <option value="medium">Medium</option>
                    <option value="hard">Hard</option>
                    ';
            }
            elseif($mobilegame->difficulty == 'medium'){
                echo '<option value="easy">Easy</option>
                    <option value="medium" selected="selected">Medium</option>
                    <option value="hard">Hard</option>
                    ';
            }
            else{
                echo '<option value="easy">Easy</option>
                    <option value="medium">Medium</option>
                    <option value="hard" selected="selected">Hard</option>
                    ';
            }
        ?>
        
    </select>
   <div class="spacer"></div>
   
    
    
</fieldset>