@extends('layouts.dashboard')

@section('title')
    Segments | BLU
@stop

@section('body')
    <div class="padding">

        <div class="content-head pure-g">
            <div class="title pure-u-1-2">
                <h1><i class="fa fa-unlock-alt fa-fw"></i>Segments ({{ $segments->total() }})</h1>
            </div>

            <div class="controls pure-u-1-2">
                <form action="{{ Filter::searchUrl() }}" method="get">
                    <?php
                    $querystring = $_SERVER['QUERY_STRING'];
                    $queryAll = array();
                    if ($querystring != '') {
                        $queryArr = explode('&', $querystring);
                        foreach ($queryArr as $val) {
                            if (empty($val)) {
                                continue;
                            }
                            $valArr = explode('=', $val);
                            if (!empty($queryAll[urldecode($valArr[0])])) {
                                $queryAll[urldecode($valArr[0])] = $queryAll[urldecode($valArr[0])] . ',' . urldecode($valArr[1]);
                            } else {
                                $queryAll[urldecode($valArr[0])] = urldecode($valArr[1]);
                            }
                        }
                    }
                    ?>
                    <input type="text" placeholder="Search" name="q" value="{{ Input::get('q') }}"/>
                    <button type="submit" title="" class="list-search"></button>
                </form>
                @if(I::can('create_segments'))
                    <a href="{{ url('dashboard/segments/new_manual') }}" class="pure-button pure-button-primary">Add
                        Manual Segment</a>
                    <a href="{{ url('dashboard/segments/new') }}" class="pure-button pure-button-primary">Add
                        Segment</a>
                @endif
            </div>


            <div id="catalog-filter" class="pure-u-1-2">
                <input id="filter_route" type="hidden" value="segments">
                <form id="filter-catalogue" action="{{ Filter::searchUrl() }}" method="get">

                    <div class="filter-block filter-block-pricing-width">
                        <span class="filter-label">Partner:</span>
                        <select style="width:100%;" name="partner[]" data-filter-catalogue="partner" data-filter-catalogue-title="Select Partner" multiple="multiple">
                            @foreach ($lists['partners'] as $key => $value)
                                <option value="{{ $key }}">{{ $value }}</option>
                            @endforeach
                        </select>
                        <input data-filter-catalogue="partner" type="hidden" value="{{ Input::get('partner') }}">
                    </div>

                    <div class="submit-filter-block disable-width">
                        <div style="float:right;">
                            <a href="<?php echo e(url('dashboard/segments')); ?>" title="Clear Filters" class="pure-button pure-button-primary">
                                Clear Filters <i class="fa fa-ban fa-fw"></i>
                            </a>
                        </div>
                        <button class="pure-button pure-button-primary" type="submit" title="" style="float:right;margin-right:15px;">
                            Submit Filters
                        </button>
                    </div>
                </form>
            </div>
        </div>

        <table class="pure-table">
            <thead>
                <tr>
                    <th width="70"><a href="{{ Filter::baseUrl('sort=id') }}">ID</a></th>
                    <th style="text-align:left;padding-left:20px;"><a href="{{ Filter::baseUrl('sort=name') }}">Name</a></th>
                    <th>Partner</th>
                    <th>Network</th>
                    <th>Member Count</th>
                    <th>Is manual</th>
                    <th width="150">Actions</th>
                </tr>
            </thead>
           <tbody>
               @if(count($segments) > 0)
                   @foreach($segments as $s)
                      <tr>
                          <td>
                            {{ $s->id }}
                          </td>
                          @if($s->draft)
                            <td style="text-align:left;padding-left:20px;">
                                <i>{{ ucfirst($s->name) }}</i>
                            </td>
                          @else
                            <td style="text-align:left;padding-left:20px;">
                              @if($s->manual)
                                  <a href="/dashboard/segments/view_manual/{{ base64_encode($s->id) }}" class="edit" title="View">{{ ucfirst($s->name) }}</a>
                              @else
                                  <a href="/dashboard/segments/view/{{ base64_encode($s->id) }}" class="edit" title="View">{{ ucfirst($s->name) }}</a>
                              @endif
                            </td>
                          @endif
                          <td>
                            @if($s->partner)
                                {{ $s->partner->name }}
                            @else
                              N/A
                            @endif
                          </td>
                          <td>
                            @if($s->partner)
                                {{ $s->partner->networks->pluck('name')->implode(', ') }}
                            @else
                              N/A
                            @endif
                          </td>
                          <td>{{ number_format($s->users()->count()) }}</td>
                          <td>
                              @if($s->manual)
                                  Yes
                              @else
                                  No
                              @endif
                          </td>
                          <td>
                              @if($s->manual)
                                  <a href="/dashboard/segments/view_manual/{{ base64_encode($s->id) }}" class="edit" title="View">View</a>
                              @else
                                  <a href="/dashboard/segments/view/{{ base64_encode($s->id) }}" class="edit" title="View">View</a>
                              @endif
                            
                              @if(I::can('delete_segments'))
                                  <a href="/dashboard/segments/delete/{{ base64_encode($s->id)}}" class="delete" title="Edit">Delete</a>
                              @endif
                          </td>
                      </tr>
                   @endforeach
               @else
                   <tr>
                        <td colspan="7">No segments found</td>
                   </tr>
               @endif
           </tbody>
        </table>

        {{ $segments->setPath('segments')->appends(Input::except('page'))->links() }}

    </div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-segments');
    </script>
@stop
