@extends('layouts.dashboard')

@section('title')
    Segments &raquo; {{ $segment->name }}  | BLU
@stop

@section('dialogs')
    {{-- Assign new Property --}}
    <div id="dialog-add-property" style="width:300px;">
        <h4>Add Segment Field</h4>

        {{ Form::open(array('url' => "#", 'class' => "pure-form")) }}
            <select name="property-item" id="property-item" class="pure-input-1">
                <option selected="selected" disabled>Select Property</option>
                @foreach($properties as $group => $criteria)
                    <optgroup label="{{ $group }}">
                        @foreach($criteria as $p) 
                            <option value="{{ $p }}">{{ $p }}</option>
                        @endforeach
                    </optgroup>
                @endforeach
            </select>
        {{ Form::close() }}

        <div class="property-forms">

            {{--- Email Match ---}}
            {{ Form::open(array('url' => "#", 'class' => "pure-form", "id" => 'form_user_email')) }}
                {{ Form::hidden('name', 'user.email') }}
                {{ Form::label('email', 'Email Matches') }}
                {{ Form::text('email', '@gmail.com', array('class' => 'pure-input-1')) }}
            {{ Form::close() }}

            {{--- Role Match ---}}
            {{ Form::open(array('url' => "#", 'class' => "pure-form", "id" => 'form_user_role')) }}
                {{ Form::hidden('name', 'user.role') }}
                {{ Form::label('role', 'Role Matches') }}
                {{ Markup::simple_select('role', $roles) }}                
            {{ Form::close() }}                              

            {{--- Verification Match ---}}
            {{ Form::open(array('url' => "#", 'class' => "pure-form", "id" => 'form_user_verified')) }}
                {{ Form::hidden('name', 'user.verified') }}
                {{ Form::label('verification', 'Account Verified?') }}
                <div class="radio">{{ Form::radio('verification', 'true', true) }} Yes</div>
                <div class="radio">{{ Form::radio('verification', 'false') }} No</div>
            {{ Form::close() }}            

            {{--- Title Match ---}}
            {{ Form::open(array('url' => "#", 'class' => "pure-form", "id" => 'form_user_title')) }}
                {{ Form::hidden('name', 'user.title') }}
                {{ Form::label('title', 'Saluation Title: ') }}
                {{ Form::select('title', Meta::titleList()) }}                            
            {{ Form::close() }}      

            {{--- Status Match ---}}
            {{ Form::open(array('url' => "#", 'class' => "pure-form", "id" => 'form_user_status')) }}
                {{ Form::hidden('name', 'user.status') }}
                {{ Form::label('status', 'Status: ') }}
                {{ Form::select('status', Meta::memberStatuses()) }}                            
            {{ Form::close() }}      

            {{--- First Name Match ---}}
            {{ Form::open(array('url' => "#", 'class' => "pure-form", "id" => 'form_user_first_name')) }}
                {{ Form::hidden('name', 'user.first_name') }}            
                {{ Form::label('first_name', 'First Name Matches') }}
                {{ Form::text('first_name', '', array('class' => 'pure-input-1')) }}
            {{ Form::close() }}        

            {{--- Last Name Match ---}}
            {{ Form::open(array('url' => "#", 'class' => "pure-form", "id" => 'form_user_last_name')) }}
                {{ Form::hidden('name', 'user.last_name') }}            
                {{ Form::label('last_name', 'Last Name Matches') }}
                {{ Form::text('last_name', '', array('class' => 'pure-input-1')) }}            
            {{ Form::close() }}        

            {{--- Gender Match ---}}
            {{ Form::open(array('url' => "#", 'class' => "pure-form", "id" => 'form_user_gender')) }}
                {{ Form::hidden('name', 'user.gender') }}
                {{ Form::label('gender', 'Gender: ') }}
                {{ Form::select('gender', Meta::genderList()) }}                             
            {{ Form::close() }}         

            {{--- Marital Status Match ---}}
            {{ Form::open(array('url' => "#", 'class' => "pure-form", "id" => 'form_user_marital_status')) }}
                {{ Form::hidden('name', 'user.marital_status') }}            
                {{ Form::label('marital_status', 'Marital Status: ') }}
                {{ Form::select('marital_status', Meta::maritalStatuses()) }}                                         
            {{ Form::close() }} 

            {{--- Age Match ---}}
            {{ Form::open(array('url' => "#", 'class' => "pure-form", "id" => 'form_user_age')) }}
                {{ Form::hidden('name', 'user.age') }}
                {{ Form::label('min_age', 'Minimum Age') }}
                {{ Form::text('min_age', '', array('class' => 'pure-input-1', 'placeholder' => '18')) }}                

                {{ Form::label('max_age', 'Maximum Age') }}
                {{ Form::text('max_age', '', array('class' => 'pure-input-1', 'placeholder' => '25')) }}                                
            {{ Form::close() }}             

            {{--- Country Match ---}}
            {{ Form::open(array('url' => "#", 'class' => "pure-form", "id" => 'form_user_country')) }}
                {{ Form::hidden('name', 'user.country') }}            
                {{ Form::label('country', 'Country Matches: ') }}
                {{ Markup::simple_select('country', Meta::countries(), array('style' => 'width:150px')) }}                
            {{ Form::close() }}

            {{--- City Match ---}}
            {{ Form::open(array('url' => "#", 'class' => "pure-form", "id" => 'form_user_city')) }}
                {{ Form::hidden('name', 'user.city') }}
                {{ Form::label('city', 'City Matches: ') }}
                {{ Markup::simple_select('city', Meta::cities(), array('style' => 'width:150px')) }}
            {{ Form::close() }}

            {{--- Area Match ---}}
            {{ Form::open(array('url' => "#", 'class' => "pure-form", "id" => 'form_user_area')) }}
                {{ Form::hidden('name', 'user.area') }}            
                {{ Form::label('area', 'Area Matches: ') }}
                {{ Markup::simple_select('area', Meta::areas(true), array('style' => 'width:150px')) }}                     
            {{ Form::close() }}                      

            {{--- Income Bracket Match ---}}
            {{ Form::open(array('url' => "#", 'class' => "pure-form", "id" => 'form_user_income_bracket')) }}
                {{ Form::hidden('name', 'user.income_bracket') }}            
                {{ Form::label('income_bracket', 'Income Bracket: ') }}
                {{ Form::select('income_bracket', Meta::incomeList()) }}                             
            {{ Form::close() }} 

            {{--- Occupation Match ---}}
            {{ Form::open(array('url' => "#", 'class' => "pure-form", "id" => 'form_user_occupation')) }}
                {{ Form::hidden('name', 'user.occupation') }}            
                {{ Form::label('occupation', 'Occupation: ') }}
                {{ Form::select('occupation', Meta::occupationList()) }}                                             
            {{ Form::close() }} 

            {{--- Tags Match ---}}
            {{ Form::open(array('url' => "#", 'class' => "pure-form", "id" => 'form_user_tags')) }}
                {{ Form::hidden('name', 'user.tags') }}                        
                {{ Form::label('tags', 'Special Attributes:') }}
                {{ Form::text('tags', 'tag1, tag2', array('class' => 'pure-input-1')) }}
            {{ Form::close() }} 

            {{--- Num Children Match ---}}
            {{ Form::open(array('url' => "#", 'class' => "pure-form", "id" => 'form_user_num_children')) }}
                {{ Form::hidden('name', 'user.num_children') }}                        
                {{ Form::label('num_children', 'Number of Children:') }}
                {{ Form::text('num_children', '0', array('class' => 'pure-input-1 enforceNumeric')) }}
            {{ Form::close() }}             

            {{--- Points Earned Match ---}}
            {{ Form::open(array('url' => "#", 'class' => "pure-form", "id" => 'form_transactions_points_earned')) }}
                {{ Form::hidden('name', 'transactions.points_earned') }}            
                {{ Form::label('min_points_earn', 'Minimum Points Earned:') }}
                {{ Form::text('min_points_earn', '0', array('class' => 'pure-input-1')) }}

                {{ Form::label('max_points_earn', 'Maximum Points Earned:') }}
                {{ Form::text('max_points_earn', '1000', array('class' => 'pure-input-1')) }}                

                {{ Form::label('earn_start_date', 'Start Date:') }}
                {{ Form::text('earn_start_date', null, array('class' => 'pure-input-1 earndatepicker')) }}                

                {{ Form::label('earn_end_date', 'End Date:') }}
                {{ Form::text('earn_end_date', null, array('class' => 'pure-input-1 earndatepicker')) }}                                
            {{ Form::close() }} 

            {{--- Points Redeemed Match ---}}
            {{ Form::open(array('url' => "#", 'class' => "pure-form", "id" => 'form_transactions_points_redeemed')) }}
                {{ Form::hidden('name', 'transactions.points_redeemed') }}            
                {{ Form::label('min_points_redeem', 'Minimum Points Redeemed:') }}
                {{ Form::text('min_points_redeem', '0', array('class' => 'pure-input-1')) }}

                {{ Form::label('max_points_redeem', 'Maximum Points Redeemed:') }}
                {{ Form::text('max_points_redeem', '1000', array('class' => 'pure-input-1')) }}                

                {{ Form::label('redeem_start_date', 'Start Date:') }}
                {{ Form::text('redeem_start_date', null, array('class' => 'pure-input-1 earndatepicker')) }}                

                {{ Form::label('redeem_end_date', 'End Date:') }}
                {{ Form::text('redeem_end_date', null, array('class' => 'pure-input-1 earndatepicker')) }}                                                
            {{ Form::close() }} 

            {{--- Spend Match ---}}
            {{ Form::open(array('url' => "#", 'class' => "pure-form", "id" => 'form_transactions_spend')) }}
                {{ Form::hidden('name', 'transactions.spend') }}            
                {{ Form::label('min_spend', 'Minimum Spend (USD):') }}
                {{ Form::text('min_spend', '0', array('class' => 'pure-input-1')) }}

                {{ Form::label('max_spend', 'Maximum Spend (USD):') }}
                {{ Form::text('max_spend', '1000', array('class' => 'pure-input-1')) }}                 

                {{ Form::label('spend_start_date', 'Start Date:') }}
                {{ Form::text('spend_start_date', null, array('class' => 'pure-input-1 earndatepicker')) }}                

                {{ Form::label('spend_end_date', 'End Date:') }}
                {{ Form::text('spend_end_date', null, array('class' => 'pure-input-1 earndatepicker')) }}                                                
            {{ Form::close() }}                         

            {{--- Balance Match ---}}
            {{ Form::open(array('url' => "#", 'class' => "pure-form", "id" => 'form_user_balance')) }}
                {{ Form::hidden('name', 'user.balance') }}            
                {{ Form::label('min_balance', 'Minimum Balance (Points):') }}
                {{ Form::text('min_balance', '0', array('class' => 'pure-input-1')) }}

                {{ Form::label('max_balance', 'Maximum Balance (Points):') }}
                {{ Form::text('max_balance', '1000', array('class' => 'pure-input-1')) }}                                                
            {{ Form::close() }}                                     

            {{--- Partner Match ---}}
            {{ Form::open(array('url' => "#", 'class' => "pure-form", "id" => 'form_partner_name')) }}
                {{ Form::hidden('name', 'partner.name') }}            
                {{ Form::label('partner', 'Partner Matches: ') }}
                {{ Markup::simple_select('partner', $partners) }}    
            {{ Form::close() }}                                                 

            {{--- Transaction Product Name Match ---}}
            {{ Form::open(array('url' => "#", 'class' => "pure-form", "id" => 'form_transactions_product_name')) }}
                {{ Form::hidden('name', 'transactions.product.name') }}            
                {{ Form::label('product_name', 'Product Name Matches:') }}
                {{ Form::text('product_name', '', array('class' => 'pure-input-1')) }}                                                                
            {{ Form::close() }}                                                             

            {{--- Transaction Product Brand Match ---}}
            {{ Form::open(array('url' => "#", 'class' => "pure-form", "id" => 'form_transactions_product_brand')) }}
                {{ Form::hidden('name', 'transactions.product.brand') }}            
                {{ Form::label('product_brand', 'Brand Matches: ') }}
                {{ Markup::simple_select('product_brand', $brands) }}                    
            {{ Form::close() }}                                                                         

            {{--- Transaction Product Model Match ---}}
            {{ Form::open(array('url' => "#", 'class' => "pure-form", "id" => 'form_transactions_product_model')) }}
                {{ Form::hidden('name', 'transactions.product.model') }}            
                {{ Form::label('product_model', 'Product Model Matches:') }}
                {{ Form::text('product_model', '', array('class' => 'pure-input-1')) }}                                                                                
            {{ Form::close() }}                                                                              

            {{--- Transaction Location City Match ---}}
            {{ Form::open(array('url' => "#", 'class' => "pure-form", "id" => 'form_transactions_location_city')) }}
                {{ Form::hidden('name', 'transactions.location.city') }}               
                {{ Form::label('transaction_city', 'City Matches: ') }}
                {{ Markup::simple_select('transaction_city', Meta::cities(), array('style' => 'width:150px')) }}                                
            {{ Form::close() }}                                                                              

            {{--- Transaction Location Area Match ---}}
            {{ Form::open(array('url' => "#", 'class' => "pure-form", "id" => 'form_transactions_location_area')) }}
                {{ Form::hidden('name', 'transactions.location.area') }}               
                {{ Form::label('transaction_area', 'Area Matches: ') }}
                {{ Markup::simple_select('transaction_area', Meta::areas(true), array('style' => 'width:150px')) }}                
            {{ Form::close() }}                                                                              

            {{--- Transaction Location Country Match ---}}
            {{ Form::open(array('url' => "#", 'class' => "pure-form", "id" => 'form_transactions_location_country')) }}
                {{ Form::hidden('name', 'transactions.location.country') }}               
                {{ Form::label('transaction_country', 'Country Matches: ') }}
                {{ Markup::simple_select('transaction_country', Meta::countries(), array('style' => 'width:150px')) }}                
            {{ Form::close() }}                                                                              
        
        </div>

        <a href="#" class="pure-button pure-button-primary attribute-action-save" data-target="#card-listing">
            Save
        </a>                
    </div>
@stop

@section('body')
    <div class="padding">

        {{-- Page Header --}}
        <div class="content-head pure-g">
            <div class="title pure-u-1-2">
                <h1><i class="fa fa-dot-circle-o fa-fw"></i>Segment &raquo; {{ $segment->name }}</h1>
            </div>

            <div class="controls pure-u-1-2">
                <a href="{{ url('dashboard/segments') }}" class="pure-button">All Segments</a>
            </div>
        </div>
        {{-- /Page Header --}}

        {{ Form::open(array('url' => url('dashboard/segments/update/'.base64_encode($segment->id)), 'files' => true, 'method' => 'post', 'class' => "pure-form pure-form-stacked form-width segment-form")) }}

            @include('partials.errors')

            {!! Form::hidden('redirect_to', URL::previous()) !!}

            <fieldset>
                <legend>Basic Details</legend>

                {{ Form::label('partner_id', 'Partner') }}
                <select name="partner_id" class="pure-input-1-3">
                    <option value="">Select Partner</option>
                    @foreach($partners as $p)
                        @if($p->id == $segment->partner_id)
                            <option value="{{ $p->id }}" selected="selected">{{ $p->name }}</option>
                        @else
                            <option value="{{ $p->id }}">{{ $p->name }}</option>
                        @endif
                    @endforeach
                </select>

                {{ Form::label('name', 'Segment Name') }}
                {{ Form::text('name', $segment->name, array('class' => 'pure-input-1')) }}
            </fieldset>

            <fieldset>
                <legend>
                    Segment Criteria
                    @if($segment->draft)
                        @if(I::can('create_segments'))
                            <span class="right">
                                <a href="#dialog-add-property" class="dialog">Add Field</a>
                            </span>
                        @endif
                    @else
                        @if(I::can('edit_segments'))
                            <span class="right">
                                <a href="#dialog-add-property" class="dialog">Add Field</a>
                            </span>
                        @endif
                    @endif
                </legend>

                <table id="criteria-table" class="pure-table">
                    <thead>
                        <tr>
                            <th>Field</th>
                            <th>Value</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($segment->attributes->count() > 0)
                            @foreach($segment->attributes as $s)
                               <?php echo(View::make("segment.ejs.form_".str_replace('.', '_', $s->name),
                                   array_merge(['criteria_id' => $s->id, 'segment_id' => $s->mapping_key], (array)json_decode($s->json_data)))->render()); ?>
                           @endforeach
                        @else
                            <tr class="empty">
                                <td colspan="3">No criteria found</td>
                            </tr>
                        @endif
                    </tbody>
                </table>

            </fieldset>

            <!-- Form Buttons -->
            <div class="form-buttons">
                <div class="left">
                    <a href="{{ url('dashboard/segments') }}" class="pure-button">Cancel</a>
                </div>
                @if($segment->draft)
                    @if(I::can('create_segments'))
                        <div class="right">
                            <button type="submit" class="pure-button pure-button-primary">Save Segment</button>
                        </div>
                    @endif
                @else
                    @if(I::can('edit_segments'))
                        <div class="right">
                            <button type="submit" class="pure-button pure-button-primary">Update Segment</button>
                        </div>
                    @endif
                @endif
                <div class="clearfix"></div>
            </div>
            <!-- /Form Buttons -->

        {{ Form::close() }}
    </div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-segments');

        $(function() {

            // Check if we have segments
            $(document).on('click', '.form-buttons button', function(e){
                e.preventDefault();

                if($('#criteria-table tr.empty').is(':visible') || $('#criteria-table tr').length == 0) {
                    alert("Please make sure that you've added criteria to your segment before saving.");
                    return false;
                } 

                if($('select[name=partner_id]').val() == '') {
                    alert("Please select a Partner before saving.");
                    return false;
                }                 

                $('form.segment-form').submit();
            });

            // Show the property form
            $(document).on('change', '#property-item', function(){
                $('div.property-forms form').hide();
                if($(this).val() != '') {
                    $("#form_" + $(this).val().replace(/\./g,'_')).fadeIn();
                }
            });

            // Add the criteria
            $(document).on('click', 'a.attribute-action-save', function(e){
                e.preventDefault();

                _this = $(this);
                _form = $('.property-forms form:visible');

                if(typeof _form != 'undefined') {
                    $.get("/dashboard/segments/ejs/"+_form.attr('id')+".ejs", _form.serialize(), function(resp) {
                        $('#criteria-table tbody').append(resp);
                        $('#criteria-table tbody tr.empty').hide();
                        closeDialog();
                    });
                }
            });

            // Remove the criteria
            $(document).on('click', '#criteria-table tbody a.delete-criteria', function(e) {
                if(confirm('Are you sure you want to delete this item?')) {
                    e.preventDefault();
                    $.get("/dashboard/segments/delete_criteria/"+$(this).data('segment')+"/"+$(this).data('criteria'));

                    $(this).closest('tr').remove();
                    if($('#criteria-table tbody tr').length == 0) {
                        $('#criteria-table tbody tr.empty').show();
                    }
                }
            });

        });
    </script>
@stop