@extends('layouts.dashboard')

@section('title')
    Segments &raquo; {{ $segment->name }}  | BLU
@stop

@section('dialogs')
@stop

@section('body')
    <div class="padding">

        {{-- Page Header --}}
        <div class="content-head pure-g">
            <div class="title pure-u-1-2">
                <h1><i class="fa fa-dot-circle-o fa-fw"></i>Segment &raquo; {{ $segment->name }}</h1>
            </div>

            <div class="controls pure-u-1-2">
                <a href="{{ url('dashboard/segments') }}" class="pure-button">All Segments</a>
            </div>
        </div>
        {{-- /Page Header --}}

        {{ Form::open(array('url' => url('dashboard/segments/manual_update/'.base64_encode($segment->id)),
                            'files' => true,
                            'method' => 'post',
                            'class' => "pure-form pure-form-stacked form-width segment-form")) }}

            @include('partials.errors')

            <fieldset>
                <legend>Basic Details</legend>

                <select name="partner_id" class="pure-input-1-3" style="display: none;">
                    @foreach($partners as $p)
                        @if($p->id == $segment->partner_id)
                        <option value="{{ $p->id }}" selected="selected">{{ $p->name }}</option>
                        @else
                            <option value="{{ $p->id }}">{{ $p->name }}</option>
                        @endif
                    @endforeach
                </select>

                {{ Form::label('name', 'Segment Name') }}
                {{ Form::text('name', $segment->name, array('class' => 'pure-input-1')) }}
            </fieldset>

            <fieldset>
                <div class="image-rail" style="width:340px;">
                    <table class="banner_view" style="margin-top:20px;margin-bottom:20px;">
                        <tr>
                            <td style="padding-left:10px;">
                                {{ Form::label('import_file', 'Import File') }}
                                {{ Form::file('import_file') }}
                            </td>
                        </tr>
                    </table>
                </div>
            </fieldset>

            <fieldset>
                <table id="segmentuser-list" class="pure-table">
                    <thead>
                        <tr>
                            <th>User Id</th>
                            <th>Username</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="empty">
                            @if($segment->users->count() > 0)
                                @foreach($segment->users as $u)
                                    <tr>
                                        <td>
                                            {{ $u->id }}
                                        </td>
                                        <td>
                                            @if($u->first_name && $u->last_name)
                                                {{ $u->first_name }}  {{ $u->last_name }}
                                            @elseif($u->first_name != 'na')
                                                {{ $u->first_name }}
                                            @else
                                                N/A
                                            @endif
                                        </td>
                                        <td>
                                            @if (I::can('delete_segments'))
                                                <a href="{{ url('dashboard/segmentuser/delete/' . base64_encode($u->id)) . '/' . base64_encode($segment->id) }}" class="delete-segmentuser" title="Delete">Delete</a>
                                            @else
                                                N/A
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tr>
                    </tbody>
                </table>

                @if($segment->draft)
                    @if(I::can('create_segments'))
                        <a href="#dialog-new-segmentuser" data-segment_id="{{ base64_encode($segment->id) }}" class="btn-new-segmentuser top-space" title="Add new estatement item">
                            <i class="fa fa-plus-circle fa-fw"></i>Add new User
                        </a>
                    @endif
                @else
                    @if(I::can('edit_segments'))
                        <a href="#dialog-new-segmentuser" data-segment_id="{{ base64_encode($segment->id) }}" class="btn-new-segmentuser top-space" title="Add new estatement item">
                            <i class="fa fa-plus-circle fa-fw"></i>Add new User
                        </a>
                    @endif
                @endif
            </fieldset>

            <!-- Form Buttons -->
            <div class="form-buttons">
                <div class="left">
                    <a href="{{ url('dashboard/segments') }}" class="pure-button">Cancel</a>
                </div>
                @if($segment->draft)
                    @if(I::can('create_segments'))
                        <div class="right">
                            <button type="submit" class="pure-button pure-button-primary">Save Segment</button>
                        </div>
                    @endif
                @else
                    @if(I::can('edit_segments'))
                        <div class="right">
                            <button type="submit" class="pure-button pure-button-primary">Update Segment</button>
                        </div>
                    @endif
                @endif
                <div class="clearfix"></div>
            </div>
            <!-- /Form Buttons -->

        {{ Form::close() }}
    </div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-segments');

        enqueue_script('new-segment');
    </script>
@stop