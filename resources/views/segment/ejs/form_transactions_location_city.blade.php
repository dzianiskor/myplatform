<tr>
	<td>{{ $name }}</td>
	<td>{{ Meta::city_name_from_id($transaction_city) }}</td>
	<td>
		@if(I::can('delete_segments'))
			<a href="#" class="delete-criteria" title="Delete" data-segment="{{ $segment_id or '' }}" data-criteria="{{ $criteria_id or '' }}">
				Delete
			</a>
		@else
			N/A
		@endif
	    <input type="hidden" name="criteria[{{$name}}][name][]" value="{{ $name }}" />		
	    <input type="hidden" name="criteria[{{$name}}][transaction_city][]" value="{{ $transaction_city }}" />		
	</td>
</tr>                                                           