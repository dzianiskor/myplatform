<tr>
	<td>{{ $name }}</td>
	<td>
		{{ Meta::partner_name_from_id($partner) }} 
	</td>
	<td>
		@if(I::can('delete_segments'))
			<a href="#" class="delete-criteria" title="Delete" data-segment="{{ $segment_id or '' }}" data-criteria="{{ $criteria_id or '' }}">
				Delete
			</a>
		@else
			N/A
		@endif
	    <input type="hidden" name="criteria[{{$name}}][name][]" value="{{ $name }}" />		
	    <input type="hidden" name="criteria[{{$name}}][partner][]" value="{{ $partner }}" />		
	</td>
</tr>