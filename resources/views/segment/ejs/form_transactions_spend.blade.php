<tr>
	<td>{{ $name }}</td>
	<td>
		{{ $min_spend }} - {{ $max_spend }} 
		<span class="date">
			({{ $spend_start_date }} - {{ $spend_end_date }})
		</span>
	</td>
	<td>
		@if(I::can('delete_segments'))
			<a href="#" class="delete-criteria" title="Delete" data-segment="{{ $segment_id or '' }}" data-criteria="{{ $criteria_id or '' }}">
				Delete
			</a>
		@else
			N/A
		@endif
	    <input type="hidden" name="criteria[{{$name}}][name][]" value="{{ $name }}" />		
	    <input type="hidden" name="criteria[{{$name}}][min_spend][]" value="{{ $min_spend }}" />		
	    <input type="hidden" name="criteria[{{$name}}][max_spend][]" value="{{ $max_spend }}" />		
	    <input type="hidden" name="criteria[{{$name}}][spend_start_date][]" value="{{ $spend_start_date }}" />		
	    <input type="hidden" name="criteria[{{$name}}][spend_end_date][]" value="{{ $spend_end_date }}" />		
	</td>
</tr>     