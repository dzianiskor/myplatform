<tr>
	<td>{{ $name }}</td>
	<td>
		{{ $min_balance }} - {{ $max_balance }} 
	</td>
	<td>
		@if(I::can('delete_segments'))
			<a href="#" class="delete-criteria" title="Delete" data-segment="{{ $segment_id or '' }}" data-criteria="{{ $criteria_id or '' }}">
				Delete
			</a>
		@else
			N/A
		@endif
	    <input type="hidden" name="criteria[{{$name}}][name][]" value="{{ $name }}" />		
	    <input type="hidden" name="criteria[{{$name}}][min_balance][]" value="{{ $min_balance }}" />		
	    <input type="hidden" name="criteria[{{$name}}][max_balance][]" value="{{ $max_balance }}" />				
	</td>
</tr>     