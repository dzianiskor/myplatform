<tr>
	<td>{{ $name }}</td>
	<td>{{ $min_age }} - {{ $max_age }}</td>
	<td>
		@if(I::can('delete_segments'))
			<a href="#" class="delete-criteria" title="Delete" data-segment="{{ $segment_id or '' }}" data-criteria="{{ $criteria_id or '' }}">
				Delete
			</a>
		@else
			N/A
		@endif
	    <input type="hidden" name="criteria[{{$name}}][name][]" value="{{ $name }}" />		
	    <input type="hidden" name="criteria[{{$name}}][min_age][]" value="{{ $min_age }}" />		
	    <input type="hidden" name="criteria[{{$name}}][max_age][]" value="{{ $max_age }}" />		
	</td>
</tr>