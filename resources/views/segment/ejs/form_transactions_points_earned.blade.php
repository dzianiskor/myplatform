<tr>
	<td>{{ $name }}</td>
	<td>
		{{ $min_points_earn }} - {{ $max_points_earn }} <br />
		<span class="date">
			{{ $earn_start_date }} - {{ $earn_end_date }}
		</span>
	</td>
	<td>
		@if(I::can('delete_segments'))
			<a href="#" class="delete-criteria" title="Delete" data-segment="{{ $segment_id or '' }}" data-criteria="{{ $criteria_id or '' }}">
				Delete
			</a>
		@else
			N/A
		@endif
	    <input type="hidden" name="criteria[{{$name}}][name][]" value="{{ $name }}" />		
	    <input type="hidden" name="criteria[{{$name}}][min_points_earn][]" value="{{ $min_points_earn }}" />		
	    <input type="hidden" name="criteria[{{$name}}][max_points_earn][]" value="{{ $max_points_earn }}" />		
	    <input type="hidden" name="criteria[{{$name}}][earn_start_date][]" value="{{ $earn_start_date }}" />		
	    <input type="hidden" name="criteria[{{$name}}][earn_end_date][]" value="{{ $earn_end_date }}" />		
	</td>
</tr>