<tr>
	<td>{{ $name }}</td>
	<td>{{ $last_name }}</td>
	<td>
		@if(I::can('delete_segments'))
			<a href="#" class="delete-criteria" title="Delete" data-segment="{{ $segment_id or '' }}" data-criteria="{{ $criteria_id or '' }}">
				Delete
			</a>
		@else
			N/A
		@endif
	    <input type="hidden" name="criteria[{{$name}}][name][]" value="{{ $name }}" />		
	    <input type="hidden" name="criteria[{{$name}}][last_name][]" value="{{ $last_name }}" />		
	</td>
</tr>