<tr>
	<td>{{ $name }}</td>
	<td>
		{{ $min_points_redeem }} - {{ $max_points_redeem }} 
		<span class="date">
			({{ $redeem_start_date }} - {{ $redeem_end_date }})
		</span>
	</td>
	<td>
		@if(I::can('delete_segments'))
			<a href="#" class="delete-criteria" title="Delete" data-segment="{{ $segment_id or '' }}" data-criteria="{{ $criteria_id or '' }}">
				Delete
			</a>
		@else
			N/A
		@endif
	    <input type="hidden" name="criteria[{{$name}}][name][]" value="{{ $name }}" />		
	    <input type="hidden" name="criteria[{{$name}}][min_points_redeem][]" value="{{ $min_points_redeem }}" />		
	    <input type="hidden" name="criteria[{{$name}}][max_points_redeem][]" value="{{ $max_points_redeem }}" />		
	    <input type="hidden" name="criteria[{{$name}}][redeem_start_date][]" value="{{ $redeem_start_date }}" />		
	    <input type="hidden" name="criteria[{{$name}}][redeem_end_date][]" value="{{ $redeem_end_date }}" />		
	</td>
</tr>     