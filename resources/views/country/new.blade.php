@extends('layouts.dashboard')

@section('title')
    New Country | BLU
@stop

@section('body')
    <div class="padding">

        <div class="content-head pure-g">
            <div class="title pure-u-1-2">
                <h1><i class="fa fa-globe fa-fw"></i>Add Country</h1>
            </div>

            <div class="controls pure-u-1-2">
                <a href="{{ url('dashboard/countries') }}" class="pure-button">All Countries</a>
            </div>
        </div>

        {{ Form::open(array('action' => 'CountryController@newCountry', "class" => 'pure-form pure-form-stacked form-width')) }}
            @include('partials.errors')
            @include('country/form')

            <!-- Form Buttons -->
            <div class="form-buttons">
                <div class="left">
                    <a href="{{ url('dashboard/countries') }}" class="pure-button">Cancel</a>
                </div>
                <div class="right">
                    <button type="submit" class="pure-button pure-button-primary">Save Country</button>
                </div>
                <div class="clearfix"></div>
            </div>
            <!-- /Form Buttons -->
        {{ Form::close() }}            

    </div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-countries');
    </script>
@stop
