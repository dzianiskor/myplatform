@extends('layouts.dashboard')

@section('title')
    Countries | BLU
@stop

@section('body')
<div class="padding">

    <div class="content-head pure-g">
        <div class="title pure-u-1-2">
            <h1><i class="fa fa-globe fa-fw"></i>Countries ({{ $countries->total() }})</h1>
        </div>

        <div class="controls pure-u-1-2">
            <form action="{{ Filter::searchUrl() }}" method="get">
                <input type="text" placeholder="Search" name="q" value="{{ Input::get('q') }}" />
                <button type="submit" title="" class="list-search"></button>
            </form>
            @if(I::can('create_countries'))
                <a href="{{ url('dashboard/countries/new') }}" class="pure-button pure-button-primary">Add Countries</a>
            @endif
        </div>
    </div>

    <table class="pure-table">
        <thead>
            <tr>
                <th width="50"><a href="{{ Filter::baseUrl('sort=id') }}">ID</a></th>
                <th style="text-align:left;padding-left:20px;"><a href="{{ Filter::baseUrl('sort=name') }}">Name</a></th>
                <th><a href="{{ Filter::baseUrl('sort=short_code') }}">Short Code</a></th>
                <th>Telephone Code</th>
                <th width="150">Actions</th>
            </tr>
        </thead>

        <tbody>
        @if(count($countries) > 0)
            @foreach ($countries as $country)
                <tr>
                    <td>
                        {{{ $country->id }}}
                    </td>
                    <td style="text-align:left;padding-left:20px;">
                        <a href="{{ url('dashboard/countries/view/'.base64_encode($country->id)) }}" class="edit" title="Edit">{{{ $country->name }}}</a>
                    </td>
                    <td>
                        {{{ $country->short_code}}}
                    </td>
                    <td>
                        {{{ $country->telephone_code }}}
                    </td>
                    <td>
                        <a href="{{ url('dashboard/countries/view/'.base64_encode($country->id)) }}" class="edit" title="View">View</a>

                        @if(I::can('delete_countries'))
                            <a href="{{ url('dashboard/countries/delete/'.base64_encode($country->id)) }}" class="delete" title="Delete">Delete</a>
                        @endif
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="5">No Countries Found</td>
            </tr>
        @endif
        </tbody>
    </table>

    {{ $countries->setPath('countries')->appends(Input::except('page'))->links() }}

</div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-countries');
    </script>
@stop
