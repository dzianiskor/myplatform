<fieldset>
	<legend>Basic Details</legend>
    {{ Form::label('name', 'Country Name', array('for'=>'name')) }}
    {{ Form::text('name', Input::old('name'), array('class'=>'pure-input-1 required')) }}

    {{ Form::label('short_code', 'Short Code', array('for'=>'short_code')) }}
    {{ Form::text('short_code', Input::old('short_code'), array('class'=>'pure-input-1')) }}

    {{ Form::label('telephone_code', 'Telephone Code', array('for'=>'telephone_code')) }}
    {{ Form::text('telephone_code', Input::old('telephone_code'), array('class'=>'pure-input-1 required')) }}
    
    {{ Form::label('iso_code', 'ISO Code', array('for'=>'iso_code')) }}
    {{ Form::text('iso_code', Input::old('iso_code'), array('class'=>'pure-input-1')) }}
</fieldset>
