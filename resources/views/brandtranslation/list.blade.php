@if(count($brandtranslations) > 0)
  @foreach($brandtranslations as $brandtranslation)
    <tr>
        <td>{{ $brandtranslation->id }}</td>
        <td>{{ $brandtranslation->name }}</td>
        <?php
            $language = App\Language::find($brandtranslation->lang_id);
            echo "<td>" . $language->name ."</td>";
        ?>

        <td>
           <a href="{{ url('dashboard/brandtranslation/edit/'.base64_encode($brandtranslation->id)) }}" class="edit-brandtranslation" title="Edit">Edit</a>
        </td>
    </tr>
  @endforeach
@else
    <tr class="empty">
        <td colspan="4">No brandtranslation defined</td>
    </tr>
@endif
