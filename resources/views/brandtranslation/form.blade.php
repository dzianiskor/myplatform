
{{ Form::open(array('url' => url('dashboard/brandtranslation/update/'.base64_encode($brandtranslation->id)), 'method' => 'post', 'class' => "pure-form pure-form-stacked", 'id' => 'new-brandtranslation-form')) }}
    
    @include('partials.errors')                         

    <div class="pure-g">
        <div class="pure-u-1-3">
            {{ Form::label('brandtranslation_lang', 'Language') }}
            {{ Form::select('brandtranslation_lang',$languages, array('class' => 'pure-input-1-2')) }}
            
            {{ Form::label('brandtranslation_name', 'Name') }}
        </div>
            {{ Form::text('brandtranslation_name', $brandtranslation->name, array('class' => 'pure-input-1 required')) }}
            
        </div>

    {{-- Dialog Buttons --}}
    <div class="form-buttons">
        <div class="left">
            <a href="#" class="pure-button close-dialog">Cancel</a>
        </div>
        <div class="right">
            <a href="#" class="pure-button pure-button-primary save-new-brandtranslation">Save</a>
        </div>
        <div class="clearfix"></div>
    </div>
    {{-- /Dialog Buttons --}}

{{ Form::close() }}