<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>API Test</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <link rel="stylesheet" href="{{ asset('css/main.css') }}">
        <link rel="stylesheet" href="{{ asset('css/pure-min-0.3.0.css') }}">
        <script src="{{ asset('js/vendor/modernizr-2.6.2.min.js') }}"></script>
        <style type="text/css">
            html, body{
                background-color:#fafafa;
            }
            .float-left{
            	padding:20px;
            }
            .float-right{
            	width:50%;
            	height:100%;
            	background:#323232;
            	padding:20px;
            	color:#d9d9d9;
            }
            label{
            	display:block;
            	margin-top:10px;
            	font-style:italic;
            }
            h3{
            	margin:0;
            	padding:0;
            }

            form.redemption, form.reward-prod{
            	display:none;
            }

        </style>
        {{-- Scripting --}}
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.10.1.min.js"><\/script>')</script>
	<script src="/js/vendor/nprogress.js"></script>

        <script>
	        $(function(){
	        	$('input[type=radio]').change(function(){

	        		$('form').hide();

	        		$('form.'+$(this).val()).show();
                                alert($(this).val());
	        	});

	        	$(document).on('click', 'form button', function(e){
	        		e.preventDefault();
	        		 
	        		 NProgress.start();

	        		_frm = $(this).closest('form')

	        		$.post(_frm.attr('action'), _frm.serialize(), function(resp){
	        			$('.float-right').html(resp);
	        			NProgress.done();
	        		});
	        	});
	        });
        </script>
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <div class="body-section">
			<div class="float-left">
				<h3>BLU Transaction API Test</h3>

				<label>Transaction Type:</label>
				<label style="margin:0;cursor:pointer"><input type="radio" name="type" value="reward" checked> Reward (Amount)</label>
				<label style="margin:0;cursor:pointer"><input type="radio" name="type" value="reward-prod"> Reward (Product)</label>
				<label style="margin:0;cursor:pointer"><input type="radio" name="type" value="redemption"> Redemption</label>

				<form class="reward" method="post" action="/test/reward">
					<label>Choose Member:</label>
					<select name="user_id">
						@foreach($members as $m)
							<option value="{{$m->id}}">{{ $m->first_name }} {{ $m->last_name }}</option>
						@endforeach
					</select>

					<label>Amount to Spend:</label>
					<input type="text" name="amount" value="100" placeholder="" />										

					<label>Choose Currency:</label>
					<select name="currency_code">
						@foreach($currencies as $c)
							<option value="{{$c->short_code}}">{{ $c->name }}</option>
						@endforeach
					</select>						

					<label>Choose Partner:</label>
					<select name="partner_id">
						@foreach($partners as $p)
							@if(!$p->firstNetwork())
								<option value="{{$p->id}}">N/A -> {{ $p->name }}</option>
							@else
								<option value="{{$p->id}}">{{ $p->firstNetwork()->name }} -> {{ $p->name }}</option>
							@endif
						@endforeach
					</select>		

					<label>Choose Store:</label>
                                        <?php //var_dump($stores); ?>
					<select name="store_id">
						@foreach($stores as $s)
                                                    @if($s->partner)
                                                            @if($s->partner->firstNetwork())
                                                                    <option value="{{$s->id}}">{{ $s->partner->firstNetwork()->name }} -> {{ $s->partner->name }} -> {{ $s->name }}</option>
                                                            @endif
                                                    @endif
                                                @endforeach
					</select>						

					<label>POS ID:</label>
					<input type="text" name="pos_id" placeholder="Optional" />							
					
					<label>Coupon Code:</label>
					<input type="text" name="coupon_code" placeholder="Optional" />																			

					<label>Reference:</label>
					<input type="text" name="reference" placeholder="Optional" value="Transaction Test Tool" />																						

					<label>Invoice Number:</label>
					<input type="text" name="invoice_num" placeholder="Optional" />

					<label>Notes:</label>
					<input type="text" name="notes" placeholder="Optional" />

					<label>Choose Staff Member:</label>
					<select name="staff_id">
						<option value="">NONE</option>
						@foreach($members as $m)
							<option value="{{$m->id}}">{{ $m->first_name }} {{ $m->last_name }}</option>
						@endforeach
					</select>					

					<br />
					<br />

					<button>Submit</button>
				</form>

				<form class="reward-prod" method="post" action="/test/reward-product">
					<label>Choose Member:</label>
					<select name="user_id">
						@foreach($members as $m)
							<option value="{{$m->id}}">{{ $m->first_name }} {{ $m->last_name }}</option>
						@endforeach
					</select>

					<label>Choose Product:</label>
					<select name="product_id">
						@foreach($products as $p)
							<option value="{{$p->id}}">{{ $p->name }} (${{ $p->price_in_usd }} / {{ number_format($p->price_in_points) }} Pts)</option>
						@endforeach
					</select>							

					<!--
					<label>Choose Currency:</label>
					<select name="currency_code">
						@foreach($currencies as $c)
							<option value="{{$c->short_code}}">{{ $c->name }}</option>
						@endforeach
					</select>						
					-->

					<label>Choose Partner:</label>
					<select name="partner_id">
						@foreach($partners as $p)
							@if(!$p->firstNetwork())
								<option value="{{$p->id}}">N/A -> {{ $p->name }}</option>
							@else
								<option value="{{$p->id}}">
                                                                    {{ $p->name }}</option>
							@endif
						@endforeach
					</select>		

					<label>Choose Store:</label>
                                        <?php //var_dump($stores); ?>
					<select name="store_id">
						@foreach($stores as $s)
                                                    @if($s->partner)
                                                            @if($s->partner->firstNetwork())
                                                                    <option value="{{$s->id}}">{{ $s->partner->firstNetwork()->name }} -> {{ $s->partner->name }} -> {{ $s->name }}</option>
                                                            @endif
                                                    @endif
                                                @endforeach
					</select>									

					<label>POS ID:</label>
					<input type="text" name="pos_id" placeholder="Optional" />																					

					<label>Coupon Code:</label>
					<input type="text" name="coupon_code" placeholder="Optional" />										

					<label>Reference:</label>
					<input type="text" name="reference" placeholder="Optional" value="Transaction Test Tool" />																						

					<label>Invoice Number:</label>
					<input type="text" name="invoice_num" placeholder="Optional" />

					<label>Notes:</label>
					<input type="text" name="notes" placeholder="Optional" />

					<label>Choose Staff Member:</label>
					<select name="staff_id">
						<option value="">NONE</option>
						@foreach($members as $m)
							<option value="{{$m->id}}">{{ $m->first_name }} {{ $m->last_name }}</option>
						@endforeach
					</select>										

					<br />
					<br />

					<button>Submit</button>
				</form>				

				<form class="redemption" method="post" action="/test/redemption">
					<label>Choose Member:</label>
					<select name="user_id">
						@foreach($members as $m)
							<option value="{{$m->id}}">{{ $m->first_name }} {{ $m->last_name }}</option>
						@endforeach
					</select>

					

                                        <label>Choose Store:</label>
                                        <?php //var_dump($stores); ?>
					<select name="store_id" class="doUpdateProducts">
						@foreach($stores as $s)
                                                    @if($s->partner)
                                                            @if($s->partner->firstNetwork())
                                                                    <option value="{{$s->id}}">{{ $s->partner->firstNetwork()->name }} -> {{ $s->partner->name }} -> {{ $s->name }}</option>
                                                            @endif
                                                    @endif
                                                @endforeach
					</select>	
					<label>Choose Products:</label>
					<select name="product_id">
						@foreach($products as $p)
							<option value="{{$p->id}}">{{ $p->name }} ({{ $p->price_in_points }} Points)</option>
						@endforeach
					</select>		

					<label>Quantity:</label>
					<input type="text" name="quantity" value="1" placeholder="Optional" />					

					<label>POS ID:</label>
					<input type="text" name="pos_id" placeholder="Optional" />																					

					<label>Reference:</label>
					<input type="text" name="reference" placeholder="Optional" value="Transaction Test Tool" />																						

					<label>Notes:</label>
					<input type="text" name="notes" placeholder="Optional" />		

					<label>Choose Staff Member:</label>
					<select name="staff_id">
						<option value="">NONE</option>
						@foreach($members as $m)
							<option value="{{$m->id}}">{{ $m->first_name }} {{ $m->last_name }} </option>
						@endforeach
					</select>													

					<br />
					<br />		

					<button>Submit</button>
				</form>					

			</div>

			<div class="float-right">
				Results
			</div>

			<div class="clearfix"></div> 
        </div>

        
    </body>
</html>
