@if(count($affiliateProgram->partners) > 0)
    @foreach($affiliateProgram->partners as $p)
        <li>
            {{ $p->name }}
            @if(I::can('delete_affiliate_program') || I::am("BLU Admin"))
                <a href="/dashboard/affiliate_program/unlinkpartner/{{ base64_encode($affiliateProgram->id) }}/{{ base64_encode($p->id) }}" class="delete-ajax" data-id="{{ $p->id }}" data-target="#partner-listing">
                    [Delete]
                </a>
            @endif
        </li>
    @endforeach
@else
    <li class="empty">No Partner Associated</li>
@endif
