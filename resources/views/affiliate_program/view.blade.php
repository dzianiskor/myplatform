@extends('layouts.dashboard')

@section('title')
    @if($affiliateProgram->draft)
        Affiliate Program &raquo; New  | BLU
    @else
        Affiliate Program &raquo; {{{ $affiliateProgram->name }}}  | BLU
    @endif
@stop

@section('dialogs')
    {{-- Assign new Partner --}}
    <div id="dialog-add-partner" style="width:300px;">
        <h4>Associate to Display Channel</h4>

        {{ Form::open(array('class' => "pure-form pure-form-stacked")) }}
            {{ Form::select('partner_id', $partners, null, array('class' => 'pure-input-1')) }}

            <a href="/dashboard/affiliate_program/attachpartner/{{ base64_encode($affiliateProgram->id) }}" class="pure-button pure-button-primary attribute-action" data-target="#partner-listing">
                Save
            </a>
        {{ Form::close() }}
    </div>
@stop

@section('body')
    <div class="padding">

        {{-- Page Header --}}
        <div class="content-head pure-g">
            <div class="title pure-u-1-2">
                @if($affiliateProgram->draft)
                    <h1><i class="fa fa-user fa-fw"></i>Affiliate Program &raquo; New</h1>
                @else
                    <h1><i class="fa fa-user fa-fw"></i>Affiliate Program &raquo; {{ $affiliateProgram->name }}</h1>
                @endif
            </div>

            <div class="controls pure-u-1-2">
                <a href="{{ url('dashboard/affiliate_program') }}" class="pure-button">All Affiliate Programs</a>
            </div>
        </div>
        {{-- /Page Header --}}

        {{ Form::open(array('url' 		   => url('/dashboard/affiliate_program/update/'.base64_encode($affiliateProgram->id)),
                            'files' 	   => true,
                            'method' 	   => 'post',
                            'class' 	   => "pure-form pure-form-stacked form-width",
							'autocomplete' => 'off',
                            'id'           => 'memberForm')) }}

            @include('partials.errors')

            {!! Form::hidden('redirect_to', URL::previous()) !!}

            <div class="pure-g">
                <div class="pure-u-1-2">
                    <fieldset>
                        <legend>Basic Details</legend>

                        {{ Form::label('status', 'Status') }}
                        {{ Form::select('status', $statuses, $affiliateProgram->status, array('class' => 'pure-input-1-3')) }}

                        {{ Form::label('ucid', 'Unique Affiliate Program ID (Auto-generated)') }}
                        {{ Form::text('ucid', $affiliateProgram->ucid, array('class' => 'pure-input-1', 'maxlength' => 9, 'disabled' => 'disabled')) }}

                        {{ Form::label('name', 'Program Name') }}
                        {{ Form::text('name', $affiliateProgram->name, array('class' => 'pure-input-1 required')) }}

                        {{ Form::label('affiliate_id', 'Affiliates', array('for'=>'category_id')) }}
                        {{ Form::select('affiliate_id', $affiliates, $affiliateProgram->affiliate_id, array('id' => 'category-list', 'class' => 'pure-input-1 required')) }}

                        {{ Form::label('description', 'Description', array('for'=>'description')) }}
                        {{ Form::textarea('description', $affiliateProgram->description, array('class'=>'pure-input-1')) }}
        
                        <div class="image-rail" style="width:340px;">
                            <table style="margin-top:20px;margin-bottom:20px;">
                                <tr>
                                    <td width="80">
                                        @if(empty($affiliateProgram->image))
                                            <img src="https://placehold.it/74x74" alt="" />
                                        @else
                                            <img src="<?php echo App\Http\Controllers\MediaController::getImageFromId($affiliateProgram->image)?>?s=74x74" alt="" style="width:80px;"/>
                                        @endif
                                    </td>
                                    <td style="padding-left:10px;">
                                        {{ Form::label('image', 'Image') }}
                                        {{ Form::file('image') }}
                                    </td>
                                </tr>
                            </table>
                        </div>                            
                    </fieldset>
                </div>
                <div class="pure-u-1-2 attributes">
                    <div class="padding-left-40">
                        {{-- Partner Listing --}}
                        <h3>
                            @if(I::can('edit_affiliate_program') || I::can('add_affiliate_program'))
                                <a href="#dialog-add-partner" title="New Partner" class="dialog">
                                    Display Channels <i class="fa fa-plus-circle fa-fw"></i>
                                </a>
                            @else
                                Display Channels
                            @endif
                        </h3>
                        <ul id="partner-listing">
                            @if($affiliateProgram->partners->count() > 0)
                                {{-- */$first = true;/* --}}
                                @foreach($affiliateProgram->partners as $p)
                                    @if(Auth::User()->canSeePartner($p->id))
                                        <li>
                                            {{ $p->name }}
                                            @if(I::am('BLU Admin') || I::can('delete_affiliate_program'))
                                                <a href="/dashboard/affiliate_program/unlinkpartner/{{ base64_encode($affiliateProgram->id) }}/{{ base64_encode($p->id) }}" class="delete-ajax" data-id="{{ $p->id }}" data-target="#partner-listing">[Delete]</a>
                                            @endif
                                        </li>
                                    @endif
                                {{-- */$first = false;/* --}}
                                @endforeach
                            @else
                                <li class="empty">No Partner Associated</li>
                            @endif
                        </ul>
                        {{-- /Partner Listing --}}
                    </div>
                </div>
            </div>

            <!-- Form Buttons -->
            <div class="form-buttons">
                <div class="left">
                    <a href="{{ url('dashboard/affiliate_program') }}" class="pure-button">Cancel</a>
                </div>
                @if($affiliateProgram->draft)
                    @if(I::can('add_affiliate_program'))
                        <div class="right">
                            <button type="submit" class="pure-button pure-button-primary">Create Program</button>
                        </div>
                    @endif
                @else
                    @if(I::can('edit_affiliate_program'))
                        <div class="right">
                            <button type="submit" class="pure-button pure-button-primary">Save Program</button>
                        </div>
                    @endif
                @endif
                <div class="clearfix"></div>
            </div>
            <!-- /Form Buttons -->
        {{ Form::close() }}
    </div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-affiliate_program');
        $(document).on('click', ".form-buttons .pure-button-primary", function(e){
            if ($('ul#partner-listing li.empty').html() == 'No Partner Associated') {
                e.preventDefault();

                alert('A valid partner entity must be associated with an affiliate program before saving.');

                return;
            }
        });
    </script>
@stop
