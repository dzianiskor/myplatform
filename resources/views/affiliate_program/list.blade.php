@extends('layouts.dashboard')

@section('title')
    Affiliate Program | BLU
@stop

@section('body')
    <div class="padding">

        <div class="content-head pure-g">
            <div class="title pure-u-1-2">
                <h1><i class="fa fa-user fa-fw"></i>Affiliate Program ({{ number_format($affiliatePrograms->total()) }})</h1>
            </div>

            <div class="controls pure-u-1-2">
                <form action="{{ Filter::searchUrl() }}" method="get">
                    <input type="text" placeholder="Search" name="q" value="{{ Input::get('q') }}" />
                    <button type="submit" title="" class="list-search"></button>
                </form>
                @if(I::can('add_affiliate_program'))
                    <a href="{{ url('dashboard/affiliate_program/new') }}" class="pure-button pure-button-primary">Add Affiliate Program</a>
                @endif
            </div>


            <div id="catalog-filter" class="pure-u-1-2">
                <input id="filter_route" type="hidden" value="affiliate_program">
                <form id="filter-catalogue" action="{{ Filter::searchUrl() }}" method="get">

                    <div class="filter-block filter-block-pricing-width">
                        <span class="filter-label">Affiliate:</span>
                        <select style="width:100%;" name="affiliate_list[]" data-filter-catalogue="affiliate_list" data-filter-catalogue-title="Select Affiliate" multiple="multiple">
                            @foreach ($lists['affiliates'] as $key => $value)
                                <option value="{{ $key }}">{{ $value }}</option>
                            @endforeach
                        </select>
                        <input data-filter-catalogue="affiliate_list" type="hidden" value="{{ Input::get('affiliate_list') }}">
                    </div>

                    <div class="filter-block filter-block-pricing-width">
                        <span class="filter-label">Channel:</span>
                        <select style="width:100%;" name="affiliate_program_channel_list[]" data-filter-catalogue="affiliate_program_channel_list" data-filter-catalogue-title="Select Channel" multiple="multiple">
                            @foreach ($lists['partners'] as $key => $value)
                                <option value="{{ $key }}">{{ $value }}</option>
                            @endforeach
                        </select>
                        <input data-filter-catalogue="affiliate_program_channel_list" type="hidden" value="{{ Input::get('affiliate_program_channel_list') }}">
                    </div>

                    <div class="submit-filter-block disable-width">
                        <div style="float:right;">
                            <a href="<?php echo e(url('dashboard/affiliate_program')); ?>" title="Clear Filters" class="pure-button pure-button-primary">
                                Clear Filters <i class="fa fa-ban fa-fw"></i>
                            </a>
                        </div>
                        <button class="pure-button pure-button-primary" type="submit" title="" style="float:right;margin-right:15px;">
                            Submit Filters
                        </button>
                    </div>
                </form>
            </div>
        </div>

        <table class="pure-table">
            <thead>
                <tr>
                    <th width="50"><a href="{{ Filter::baseUrl('sort=id') }}">ID</a></th>
                    <th width="90">Image</th>
                    <th>Name</th>
                    <th>Enabled</th>
                    <th>Affiliate</th>
                    <th width="150">Actions</th>
                </tr>
            </thead>

            <tbody>
                @if(count($affiliatePrograms) > 0)
                    @foreach($affiliatePrograms as $affiliateProgram)
                        <tr>
                            <td>
                                {{{ $affiliateProgram->id }}}
                            </td>
                            <td>
                                @if(empty($affiliateProgram->image))
                                    <img src="https://placehold.it/74x74" alt="" />
                                @else
                                    <img src="<?php echo App\Http\Controllers\MediaController::getImageFromId( $affiliateProgram->image)?>?s=74x74" alt="" style="width:80px;"/>
                                @endif
                            </td>
                            <td style="text-align:center;padding-left:20px;">
                                <a href="/dashboard/affiliate_program/view/{{ base64_encode($affiliateProgram->id) }}" title="View Affiliate">{{{ $affiliateProgram->name }}}</a>
                            </td>
                            <td>
                                @if($affiliateProgram->status == 'enabled')
                                    <span class="ico-yes"></span>
                                @else
                                    <span class="ico-no"></span>
                                @endif
                            </td>
                            <td>
                                @if($affiliateProgram->affiliate_id > 0)
                                    <?php
                                        $affiliate = App\Affiliate::find($affiliateProgram->affiliate_id);
                                    ?>
                                    @if($affiliate)
                                        {{{ $affiliate->name }}}
                                    @else
                                        {{ "N/A" }}
                                    @endif
                                @else
                                    {{ "N/A" }}
                                @endif
                            </td>
                            <td>
                                @if(I::am('BLU Admin'))
                                    <a href="/dashboard/affiliate_program/view/{{ base64_encode($affiliateProgram->id) }}" class="edit" title="View">View</a>

                                        <a href="/dashboard/affiliate_program/delete/{{ base64_encode($affiliateProgram->id) }}" class="delete" title="Delete">Delete</a>
                                @else
                                        @if(I::can('view_affiliate_program'))
                                            <a href="/dashboard/affiliate_program/view/{{ base64_encode($affiliateProgram->id) }}" class="edit" title="View">View</a>
                                        @endif

                                        @if(I::can('delete_affiliate_program'))
                                            <a href="/dashboard/affiliate_program/delete/{{ base64_encode($affiliateProgram->id) }}" class="delete" title="Delete">Delete</a>
                                        @endif
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="10">No Affiliate Programs Found</td>
                    </tr>
                @endif
            </tbody>
        </table>

        {{ $affiliatePrograms->setPath('affiliate_program')->appends(Input::except('page'))->links() }}

    </div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-affiliate_program');
    </script>
@stop