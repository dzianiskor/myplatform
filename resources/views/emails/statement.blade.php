<h3>Customer Statement for Period {{ $start }} to {{ $end }}</h3>

<h6>Customer Details:</h6>

<table>
	<tr>
		<td>Customer:</td>
		<td>{{ $user->titleAndFullName }}</td>
	</tr>	
	<tr>
		<td>Email:</td>
		<td>{{ $user->email }}</td>
	</tr>	
	<tr>
		<td>Mobile:</td>
		<td>{{ $user->normalizedContactNumber() }}</td>
	</tr>		
	@if($user->country)
		<tr>
			<td>Country:</td>
			<td>{{ $user->country->name }}</td>
		</tr>
	@endif
	@if($user->city)
		<tr>
			<td>City:</td>
			<td>{{ $user->city->name }}</td>
		</tr>
	@endif	
	@if($user->area)
		<tr>
			<td>Area:</td>
			<td>{{ $user->area->name }}</td>
		</tr>
	@endif		
	<tr>
		<td>Address 1:</td>
		<td>{{ $user->address_1 }}</td>
	</tr>		
	<tr>
		<td>Address 2:</td>
		<td>{{ $user->address_2 }}</td>
	</tr>		
</table>

<h6>Month Transactions:</h6>

<table width="100%">
    <thead>
        <tr>
            <td>Date</td>
            <td>Transaction</td>
            <td>Amount</td>
            <td>Balance</td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                {{{ $start }}}
            </td>
            <td>
                Opening Balance
            </td>
            <td></td>
            <td>
                {{ number_format($openingBalance) }}
            </td>
        </tr>
    @foreach ($transactions as $transaction)
        <tr>
            <td>
                {{ $transaction->created_at }}
            </td>
            <td>
                {{ $transaction->ref_number }}
            </td>
            <td>
                {{ number_format((0 - $transaction->points_redeemed) + $transaction->points_rewarded) }}
            </td>
            <td>
                {{ number_format($transaction->points_balance) }}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>