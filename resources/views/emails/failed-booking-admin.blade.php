<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/xhtml" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">
  <head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>FAILED BOOKING AT HICHHIKER</title>
  </head>
  <body bgcolor="#dedede" style="background-color: #dedede; font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%; margin: 0; padding: 0;">


<!-- body -->
<table style="background-color: #dedede;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; width: 100%; margin: 0; padding: 20px;"><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"></td>
		<td bgcolor="#FFFFFF" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto; padding: 20px; border: 1px solid #f0f0f0;">

			<!-- content -->
			<div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; max-width: 600px; display: block; margin: 0 auto; padding: 0;">
			<table style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; width: 600px; margin: 0; padding: 0;"><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">
<p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal;  margin: 0; padding: 30px 0 5px;">
	<img src="{{ asset('img/email-logo.png') }}" alt="BLU" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; max-width: 100%; margin: 0; padding: 0;" /></p>
	<br>

                                           
                                                <h2 style="font-family: 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; font-size: 28px; line-height: 1.2; color: #000; font-weight: 200; margin: 0; padding: 0 0 40px;">Failed Flight Booking at HitchHiker</h2>
                                           
						<p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
							Dear <strong style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">Admin</strong>,
						</p>

							<p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
								   This is an automated email to inform you that the below booking has failed at HitchHiker:<br/><br/>
							</p>						

						<div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 10px; width: 100%; margin: 0; padding: 0;"></div>

						<div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 5px; width: 100%; background-color: #35aeea; margin: 10px 0; padding: 0;"></div>

						<div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 10px; width: 100%; margin: 0; padding: 0;"></div>

						<table style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; width: 100%; margin: 0; padding: 0;">
                                                    <tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td width="30%" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">Transaction ID</td> <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">{{  $transaction->id }}</td></tr>
                                                    <tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td width="30%" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">Transaction Date/Time</td><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"> {{  $transaction->created_at }}</td></tr>    
                                                    <tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td width="30%" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;" valign="top">Error Description</td><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"> {{  $returnederror }}</td></tr>
                                                    <tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td width="30%" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">Full Name</td><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"> {{ $user->name }}</td></tr>
                                                    <tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td width="30%" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">Mobile</td> <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">{{ $user->normalizedContactNumber() }}</td></tr>
                                                    <tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td width="30%" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">User ID</td> <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">{{ $user->id }}</td></tr>
                                                    <tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td width="30%" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">Reference ID</td> <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">  <?php  if(isset($reference->number)){ ?>{{ $reference->number }}<?php } ?></td></tr>
                                                    <tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td width="30%" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">Points Redeemed</td> <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">{{number_format(abs($transaction->points_redeemed))}}</td></tr>
                                                    <tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td width="30%" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">Amount Paid (USD)</td> <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">{{number_format(abs($transaction->cash_payment))}} </td></tr>
                                                </table>
                                                <div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 10px; width: 100%; margin: 0; padding: 0;"></div>

							<?php
									$notes = $transaction->notes;
									$json_notes = json_decode($notes);
									if(isset($json_notes)){
										$o_carrier_name = $json_notes->out_carrier_name;
										$o_depart_time = $json_notes->out_depart_time;
										$o_arrive_time = $json_notes->out_arrive_time;
										$o_depart_station_name = $json_notes->out_depart_station_name;
										$o_arrive_station_name = $json_notes->out_arrive_station_name;
										$o_depart_station_code = $json_notes->out_depart_station_code;
										$o_arrive_station_code = $json_notes->out_arrive_station_code;
										$o_duration = $json_notes->out_duration;
                                                                                if(strpos($o_duration,"H") !== FALSE ){
                                                                                    $o_duration_string = $o_duration;
                                                                                }else{
                                                                                    $o_duration_string = floor($o_duration / 60)."H ".($o_duration % 60)."m";
                                                                                }
										$o_stops = $json_notes->out_stops;
										$return = -1;
										if(isset($json_notes->return)){
											$return = $json_notes->return;
                                                                                        $i_carrier_name = $json_notes->in_carrier_name;
                                                                                        $i_depart_time = $json_notes->in_depart_time;
                                                                                        $i_arrive_time = $json_notes->in_arrive_time;
                                                                                        $i_depart_station_name = $json_notes->in_depart_station_name;
                                                                                        $i_arrive_station_name = $json_notes->in_arrive_station_name;
                                                                                        $i_depart_station_code = $json_notes->in_depart_station_code;
                                                                                        $i_arrive_station_code = $json_notes->in_arrive_station_code;
                                                                                        $i_duration = $json_notes->in_duration;
                                                                                        if(strpos($i_duration,"H") !== FALSE ){
                                                                                            $i_duration_string = $i_duration;
                                                                                        }else{
                                                                                            $i_duration_string = floor($i_duration / 60)."H ".($i_duration % 60)."m";
                                                                                        }
                                                                                        $i_stops = $json_notes->in_stops;
										}
										if(isset($json_notes->cabinclass)){
											$cabin_class = $json_notes->cabinclass;
										}
									}
								
							 ?>
                                                <p><span style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 140%; line-height: 1.6; margin: 0; padding: 0;"><?php  if( isset($cabin_class) ){ echo " Cabin Class: ".$cabin_class; } ?></span></p>
						<div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 10px; width: 100%; margin: 0; padding: 0;"></div>

							<table border="0" cellspacing="0" cellpadding="0" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; width: 100%; margin: 0; padding: 0;"><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px; font-weight: bold; color: #000000; text-align: center; background-color: #e0e0e0; margin: 0; padding: 5px;" align="center" bgcolor="#58595B"></td>
									<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; font-weight: bold; color: #000000; text-align: center; background-color: #e0e0e0; margin: 0; padding: 5px;" align="center" bgcolor="#58595B">Airline / Flight</td>
									<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; font-weight: bold; color: #000000; text-align: center; background-color: #e0e0e0; margin: 0; padding: 5px;" align="center" bgcolor="#58595B">Depart / Arrive</td>
                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; font-weight: bold; color: #000000; text-align: center; background-color: #e0e0e0; margin: 0; padding: 5px;" align="center" bgcolor="#58595B">City / Airport</td>
                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; font-weight: bold; color: #000000; text-align: center; background-color: #e0e0e0; margin: 0; padding: 5px;" align="center" bgcolor="#58595B">Duration / Stops </td>
                                                            </tr><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td rowspan="2" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px; text-align: center; margin: 0; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; " align="center"><img src="{{ asset('img/departBig.png') }}" width="100" /></td>
									<td rowspan="2" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; text-align: center; margin: 0; padding: 10px 5px;" align="center"><?php echo $o_carrier_name ?></td>
									<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; /*border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;*/border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px;/*border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px;*/ text-align: center; margin: 0; padding: 10px 5px 0px 5px;" align="center"><?php echo date("F j, Y, g:i a", strtotime($o_depart_time)) ?></td>

                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; /*border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px;*//*border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px;*/ text-align: center; margin: 0; padding: 10px 5px 0px 5px;" align="center"><?php echo $o_depart_station_name . " (" . $o_depart_station_code . ")" ?></td>

                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px;/*border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px;*/ text-align: center; margin: 0; padding: 10px 5px 0px 5px;" align="center"><?php echo $o_duration_string ?></td>

								</tr><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">
									<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; /*border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;*/border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin: 0; padding: 0px 5px 10px 5px;" align="center"><?php echo date("F j, Y, g:i a", strtotime($o_arrive_time)) ?></td>
                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; /*border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px;*/ border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin: 0; padding: 0px 5px 10px 5px;" align="center"><?php echo $o_arrive_station_name . " (" . $o_arrive_station_code . ")"?></td>
                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin: 0; padding: 0px 5px 10px 5px;" align="center"><?php echo $o_stops ?></td>

								</tr>
                                                            <?php  if($return == "1"){ ?>
                                                            <tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td rowspan="2" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px; text-align: center; margin: 0; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; " align="center"><img src="{{ asset('img/arrivalBig.png') }}" width="100" /></td>
									<td rowspan="2" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin: 0; padding: 10px 5px;" align="center"><?php echo $i_carrier_name ?></td>
									<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; /*border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;*/border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; /*border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px;*/ text-align: center; margin: 0; padding: 10px 5px 0px 5px;" align="center"><?php echo date("F j, Y, g:i a", strtotime($i_depart_time)) ?></td>

                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; /*border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; *//*border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px;*/ text-align: center; margin: 0; padding: 10px 5px 0px 5px;" align="center"><?php echo $i_depart_station_name . " (" . $i_depart_station_code . ")" ?></td>

                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; /*border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px;*/ text-align: center; margin: 0; padding: 10px 5px 0px 5px;" align="center"><?php echo $i_duration_string ?></td>

								</tr>
                                                            <tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">
									<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; /*border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;*/border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin: 0; padding: 0px 5px 10px 5px;" align="center"><?php echo date("F j, Y, g:i a", strtotime($i_arrive_time)) ?></td>
                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; /*border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px;*/ border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin: 0; padding: 0px 5px 10px 5px;" align="center"><?php echo $i_arrive_station_name . " (" . $i_arrive_station_code . ")"?></td>
                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin: 0; padding: 0px 5px 10px 5px;" align="center"><?php echo $i_stops ?></td>

								</tr>
                                                            <?php } ?>
                                                        </table>
                                </td>
				</tr></table></div>
			<!-- /content -->

		</td>
		<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"></td>
	</tr></table><!-- /body --><!-- footer --><table style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; width: 100%; clear: both !important; margin: 0; padding: 0;"><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"></td>
		<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto; padding: 0;">

			<!-- content -->
			<!-- /content -->

		</td>
		<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"></td>
	</tr></table><!-- /footer --></body>
</html>
