<?php
	$trxCurrency    = App\Currency::where('id', $transaction->currency_id)->first();
	$shortCode      = $trxCurrency->short_code;

	$programNameEn	= $emailPartnerData["program_name"]["en"];
	$partnerNameEn	= $emailPartnerData["partner_name_content"]["en"];
	$supportEmailEn	= $emailPartnerData["support_email"]["en"];

	$primaryColor	= $emailPartnerData["partner"]["primary"];


// echo "<pre>";
// print_r($email_template);
// echo "</pre>";
// exit;

	$imageWidth = 200;
	if($transaction->partner_id != 4694):
		list($width, $height) = getimagesize(url("/") . "/media/image/" . $emailPartnerData["image"]["en"]);
		if ($height > $width / 2) {
			$imageWidth = 100;
		}
	endif;

	$cashPayment		= $transaction->cash_payment;
	$transClass			= $transaction->trans_class;
	$refNb				= $transaction->ref_number;
	$notes				= $transaction->notes;
	$transId			= $transaction->id;

	$transClassLower	= strtolower($transClass);
	$refNbLower			= strtolower($refNb);
	$notesLower			= strtolower($notes);

	$json_notes			= json_decode($notes);

//	$queriesEn	= "For any questions or queries, please contact us on <a href='mailto:" . $supportEmailEn . "' style='color:" . $primaryColor . "; margin:0; padding:0;'>" . $supportEmailEn . "</a>";
//	$queriesAr	= "لإرسال أي استفسارات أو أسئلة إلينا يرجى التواصل معنا عبر <a href='mailto:" . $supportEmailEn . "' style='color:" . $primaryColor . "; margin:0; padding:0;'>" . $supportEmailEn . "</a>";
//
	$automatedMsg		= "<p style='font-size:14px; line-height:1.6; font-weight:normal; margin: 0 0 10px; padding:0;'>This is an automated customer order confirmation for the following transaction:<br/><br/></p>";
	$afterAutomatedMsg	= "<div style='line-height:1.6; height:10px; width:100%; margin:0; padding:0;'></div>"
		. "<div style='line-height:1.6; height:5px; width:100%; margin:10px 0; padding:0; background-color:<?php echo \"$primaryColor\" ?>;'></div>"
		. "<div style='line-height:1.6; height:10px; width:100%; margin:0; padding:0;'></div>";

	if ($transClassLower == "reversal") {
		$transaction2	= App\Transaction::find($refNb);
		$transId		= $transaction2->id;

		$emailTitleEn	= "Reversal Transaction Order Summary (#" . $transId . ")";

		$contentEn	 = "<p style='font-size:14px; line-height:1.6; font-weight:normal; margin: 0 0 10px; padding:0;'>This is an automated reversal transaction order confirmation for transaction #" . $transId . ":<br/><br/></p>";
		$contentEn	.= $afterAutomatedMsg;
	}
	else if ($transClassLower == "car") {
	
		if($cashPayment > 0):
			$emailTitleEn	= "Car Rental Booking Redemption &amp; Receipt : " . " (#" . $transId . ")";
		else:
			$emailTitleEn	= "Car Rental Booking Redemption " . " ( #" . $transId . ")";
		endif;
		$contentEn	 = $automatedMsg;
		$contentEn	.= $afterAutomatedMsg;
	}
	else if ($transClassLower == "flight") {
		if($cashPayment > 0):
			$emailTitleEn	= "Flight Booking Redemption &amp; Receipt : " . " (#" . $transId . ")";
		else:
			$emailTitleEn	= "Flight Booking Redemption " . " ( #" . $transId . ")";
		endif;
		$contentEn	 = $automatedMsg;
		$contentEn	.= $afterAutomatedMsg;
	}
	else if ($transClassLower == "hotel") {
		if($cashPayment > 0):
			$emailTitleEn	= "Hotel Booking Redemption &amp; Receipt : " . " (#" . $transId . ")";
		else:
			$emailTitleEn	= "Hotel Booking Redemption " . " ( #" . $transId . ")";
		endif;

		$contentEn	 = $automatedMsg;
		$contentEn	.= $afterAutomatedMsg;
	}
	else if ($transClassLower == "points") {
		if ($refNbLower == "cashback" || $notesLower == "cashback") {
			$emailTitleEn = "Cashback Order Summary" . " (#" . $transId . ")";

			$contentEn	 = $automatedMsg;
			$contentEn	.= $afterAutomatedMsg;
		}
		else if (strpos($refNbLower, "miles points") > 0) {
			$emailTitleEn = "Miles Conversion Order Summary" . " (#" . $transId . ")";

			$contentEn	 = $automatedMsg;
			$contentEn	.= $afterAutomatedMsg;
		}
		else {
			$emailTitleEn = "Transfer Order Summary" . " (#" . $transId . ")";

			$contentEn	 = $automatedMsg;
			$contentEn	.= $afterAutomatedMsg;
		}
	}
	else if ($transClassLower == "amount") {
		$emailTitleEn	= "In Store Transaction Summary" . " (#" . $transId . ")";

		$contentEn	 = $automatedMsg;
		$contentEn	.= $afterAutomatedMsg;
	}
	else if ($transClassLower == "items") {
		if($cashPayment > 0):
			$emailTitleEn	= "Redemption Order Summary &amp; Receipt : " . " (#" . $transId . ")";
		else:
			$emailTitleEn	= "Redemption Order Summary" . " (#" . $transId . ")";
		endif;

		$contentEn	 = $automatedMsg;
		$contentEn	.= $afterAutomatedMsg;
	}
	else {

		if($cashPayment > 0):
			$emailTitleEn	= "Redemption Booking Redemption &amp; Receipt : " . " (#" . $transId . ")";
		else:
			$emailTitleEn	= "Redemption Booking Redemption " . " ( #" . $transId . ")";
		endif;

		$contentEn	 = $automatedMsg;
		$contentEn	.= $afterAutomatedMsg;
	}


	$ref = App\Reference::where("user_id", $user->id)->where("partner_id", $transaction->partner_id)->first();
	$loyalty = 'NA';
	if($ref) {
		$loyalty = $ref->number;
	}

	$customerName		= $user->name;
	$customerMobile		= $user->normalized_mobile;
	$customerEmail		= !empty($json_notes->email)		? $json_notes->email		: $user->email;
	$customerAddress1	= !empty($json_notes->address_1)	? $json_notes->address_1	: $user->address_1;
	$customerAddress2	= !empty($json_notes->address_2)	? $json_notes->address_2	: $user->address_2;
	$customerCity		= !empty($json_notes->city)			? $json_notes->city			: $user->city->name ?? '';
	$customerArea		= !empty($json_notes->area)			? $json_notes->area			: $user->area->name ?? '';
	$customerCountry	= !empty($json_notes->country)		? $json_notes->country		: $user->country->name;
	$customerPostalCode	= !empty($json_notes->postal_code)	? $json_notes->postal_code	: $user->postal_code;

	if ($customerAddress1 == "")	{ $customerAddress1 = "N/A"; }
	if ($customerAddress2 == "")	{ $customerAddress2 = "N/A"; }
	if ($customerCity == "")		{ $customerCity = "N/A"; }
	if ($customerArea == "")		{ $customerArea = "N/A"; }
	if ($customerCountry == "")		{ $customerCountry = "N/A"; }
	if ($customerPostalCode == "")	{ $customerPostalCode = "N/A"; }

	if (!empty($json_notes->city)) {
		$city			= App\City::where('id', $customerCity)->first();
		if ($city) {
			$customerCity	= $city->name;
		}
	}
	if (!empty($json_notes->area)) {
		$area			= App\Area::where('id', $customerArea)->first();
		if ($area) {
			$customerArea	= $area->name;
		}
	}
	if (!empty($json_notes->country)) {
		$country			= App\Country::where('id', $customerCountry)->first();
		if ($country) {
			$customerCountry	= $country->name;
		}
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta name="viewport" content="width=device-width" />
		<meta http-equiv="Content-Type" content="text/html;  charset=UTF-8" />
		<title><?php echo $programNameEn ?> Order Confirmation</title>
	</head>
	<body bgcolor="#dedede" style="font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:100%; line-height:1.6; margin:0; padding:0; -webkit-font-smoothing:antialiased; -webkit-text-size-adjust:none; width: 100%!important; height:100%;">
		<table style="background-color:#dedede; line-height:1.6; width:100%; margin:0; padding:20px;">
			<tr>
				<td></td>
				<td bgcolor="#FFFFFF" style="line-height:1.6; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto; padding: 20px; border: 1px solid #f0f0f0;">
					<div style="line-height:1.6; max-width: 600px; display: block; margin: 0 auto; padding:0;">
						<table style="line-height:1.6; width: 600px; margin:0; padding:0;">
							<tr>
								<td>
									<p style="font-size:14px; line-height:1.6; font-weight:normal; margin:0; padding: 15px 0 5px;">
									<?php if($transaction->partner_id === 4148): ?>
										<img src="<?php echo url('/');?>/media/image/<?php echo $emailPartnerData['image']['en']?>" alt="<?php echo $partnerNameEn ?>" style=" line-height:1.6; margin:0; padding:0; float: left !important;" />
									<?php else: ?>
										<img src="<?php echo url('/');?>/media/image/<?php echo $emailPartnerData['image']['en']?>" alt="<?php echo $partnerNameEn ?>" style=" line-height:1.6; width:<?php echo $imageWidth; ?>px; margin:0; padding:0; float: left !important;" />
									<?php endif; ?>
									</p>
									<br /><br />
									<br /><br />

									<p style="text-align:center;" align="center">
										<h2 style="font-size:28px; line-height:1.2; color:#000; font-weight:200; margin:0; padding:0 0 40px;"><?php echo $emailTitleEn ?></h2>
									</p>
									<p style="font-size:14px; line-height:1.6; font-weight:normal; margin:0 0 10px; padding:0;">
										Dear <strong>Admin</strong>,
									</p>
									<?php echo $contentEn ?>

									<table style="line-height:1.6; width:100%; margin:0; padding:0;">
										<tr>
											<td style="vertical-align:top;">
												<p style="font-size:14px; line-height:1.6; font-weight:normal; margin: 0 0 10px; padding:0;">
													<strong style="line-height:1.6; margin:0; padding:0;">Customer Details:</strong>
												</p>
												<table style="line-height:1.6; width:100%; margin:0; padding:0;">
													<tr>
														<td>Loyalty ID: <?php echo $loyalty ?></td>
													</tr>
													<tr>
														<td>Full Name: <?php echo $customerName ?></td>
													</tr>
													<tr>
														<td>Mobile: <?php echo $customerMobile ?></td>
													</tr>
													<tr>
														<td>Email: <?php echo $customerEmail ?></td>
													</tr>
													<tr>
														<td>Address: <?php echo $customerAddress1 ?></td>
													</tr>
													<tr>
														<td>Building, Residence, Floor: <?php echo $customerAddress2 ?></td>
													</tr>
													<tr>
														<td>City: <?php echo $customerCity ?></td>
													</tr>
													<tr>
														<td>State: <?php echo $customerArea ?></td>
													</tr>
													<tr>
														<td>Country: <?php echo $customerCountry ?></td>
													</tr>
													<?php if ($transClassLower == "reversal") { ?>
														<tr>
															<td>Reversed By: <?php echo Auth::user()->name ?></td>
														</tr>
													<?php } ?>
												</table>
											</td>
											<td style="vertical-align: top;">
												@if(strtolower($transaction->trans_class) == "flight" || (isset($transaction2) && strtolower($transaction2->trans_class) == "flight"))
													<p style="font-size:14px; line-height:1.6; font-weight:normal; margin: 0 0 10px; padding:0;">
														<strong style="line-height:1.6; margin:0; padding:0;">Passenger Details:</strong>
													</p>
													<table style="line-height:1.6; width:100%; margin:0; padding:0;">
														<?php
														$count = count($json_notes->first_name);
														for($i = 0; $i < $count; $i++){ ?>
															<tr><td>Title: <?php echo $json_notes->title[$i]; ?></td></tr>
															<tr><td>Name: <?php echo $json_notes->first_name[$i] . ' ' . $json_notes->last_name[$i]; ?></td></tr>
															<tr><td>Mobile: <?php echo $json_notes->mobile[$i]; ?></td></tr>
															<tr><td>Email: <div style="text-align: left; margin:0; padding:0;"><?php echo $json_notes->email[$i]; ?></div></td></tr>
															<tr><td>Date of Birth: <?php echo $json_notes->dob_day[$i] . '-' . $json_notes->dob_month[$i] . '-' .$json_notes->dob_year[$i]; ?></td></tr>
															<tr><td>Nationality: <?php echo str_replace("_", " ", $json_notes->nationality[$i]); ?></td></tr>
															<tr><td>Passport Number: <?php echo $json_notes->passport[$i]; ?></td></tr>
															<tr><td>Passport Expiry Date: <?php echo $json_notes->passportexpiry_day[$i] . '-' . $json_notes->passportexpiry_month[$i] .'-' .$json_notes->passportexpiry_year[$i]; ?></td></tr>
														<?php } ?>
													</table>
												@endif
											</td>
										</tr>
									</table>
									<div style="line-height:1.6; height: 10px; width:100%; margin:0; padding:0;"></div>
									<?php
									if ($transClassLower == "reversal") {
//										$reversedTransaction	= $transaction;
										$transId				= $transaction->ref_number;
										$transaction			= App\Transaction::find($transId);
									}

									if(strtolower($transaction->trans_class) == "flight"){
										$notes		= $transaction->notes;
										$json_notes	= json_decode($notes);

										if(isset($json_notes)){
											$o_carrier_name = $json_notes->out_carrier_name;
											$o_depart_time = $json_notes->out_depart_time;
											$o_arrive_time = $json_notes->out_arrive_time;
											$o_depart_station_name = $json_notes->out_depart_station_name;
											$o_arrive_station_name = $json_notes->out_arrive_station_name;
											$o_depart_station_code = $json_notes->out_depart_station_code;
											$o_arrive_station_code = $json_notes->out_arrive_station_code;
											$o_duration = $json_notes->out_duration;
											$o_stops = $json_notes->out_stops;
											$i_carrier_name = $json_notes->in_carrier_name;
											$i_depart_time = $json_notes->in_depart_time;
											$i_arrive_time = $json_notes->in_arrive_time;
											$i_depart_station_name = $json_notes->in_depart_station_name;
											$i_arrive_station_name = $json_notes->in_arrive_station_name;
											$i_depart_station_code = $json_notes->in_depart_station_code;
											$i_arrive_station_code = $json_notes->in_arrive_station_code;
											$i_duration = $json_notes->in_duration;
											$i_stops = $json_notes->in_stops;
											$return = -1;

											if(isset($json_notes->return)){
												$return = $json_notes->return;
											}
											if(isset($json_notes->cabinclass)){
												$cabin_class = $json_notes->cabinclass;
											}
										}
									} ?>
									<div style="line-height:1.6; height: 5px; width:100%; background-color: <?php echo $primaryColor ?>; margin: 10px 0; padding:0;"></div>
									<p align="center"><strong style="font-size: 140%; line-height:1.6; margin:0; padding:0;"><?php  if( isset($cabin_class) ){ echo " Cabin Class: ".$cabin_class; } ?></strong></p>
									<div style="line-height:1.6; height: 10px; width:100%; margin:0; padding:0;"></div>

									@if($transaction->trans_class == "Items")
										<p style="font-size:14px; line-height:1.6; font-weight:normal; margin: 0 0 10px; padding:0;">
											<strong style="line-height:1.6; margin:0; padding:0;">Order Details:</strong>
										</p>

										<table border="0" cellspacing="0" cellpadding="0" style="line-height:1.6; width:100%; margin:0; padding:0;">
											<tr>
												<td style="line-height:1.6; font-weight: bold; color: #fff; text-align: center; background-color: #58595B; margin:0; padding: 5px;" align="center" bgcolor="#58595B">Item</td>
												<td style="line-height:1.6; font-weight: bold; color: #fff; text-align: center; background-color: #58595B; margin:0; padding: 5px;" align="center" bgcolor="#58595B">Description</td>
												<td style="line-height:1.6; font-weight: bold; color: #fff; text-align: center; background-color: #58595B; margin:0; padding: 5px;" align="center" bgcolor="#58595B">Qty</td>
												<td style="line-height:1.6; font-weight: bold; color: #fff; text-align: center; background-color: #58595B; margin:0; padding: 5px;" align="center" bgcolor="#58595B">Total Price</td>
											</tr>

											@foreach($transaction->items as $i)
												<?php $img_src = url('/') . "/media/image/" . $i->product->cover_image;?>
												<tr>
													<td style="line-height:1.6; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin:0; padding: 10px 5px;" align="center"><img src="<?php echo $img_src; ?>" width="100" /></td>
													<td style="line-height:1.6; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin:0; padding: 10px 5px;" align="center">{{ $i->product->name }}</td>
													<td style="line-height:1.6; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin:0; padding: 10px 5px;" align="center">{{ $i->quantity }}</td>
													<?php
													$priceInPoints1	= PointsHelper::productPriceInPoints($i->product->price_in_points, $i->product->original_price, $i->product->currency_id, $transaction->network_id);
													$priceInPoints	= $priceInPoints1 + ($i->product->original_sales_tax * $priceInPoints1 / 100);
													$total_points	= ($priceInPoints * $i->quantity);
													?>
													<td style="line-height:1.6; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin:0; padding: 10px 5px;" align="center"><?php echo number_format(abs($i->price_in_points));?></td>
												</tr>
											@endforeach

											<tr>
												<td colspan="4" style="line-height:1.6; text-align: right; font-weight: bold; margin:0; padding: 0 4% 0 0;" align="right">Items Subtotal: {{ number_format($transaction->points_redeemed - $transaction->delivery_cost) }} Points</td>
											</tr>

											<tr>
												<td colspan="4" style="line-height:1.6; text-align: right; font-weight: bold; margin:0; padding: 0 4% 0 0;" align="right">Shipping Charges: {{ number_format($transaction->delivery_cost) }} Points</td>
											</tr>

											<tr>
												<td colspan="4" style="line-height:1.6; text-align: right; font-weight: bold; margin:0; padding: 0 4% 0 0;" align="right">Total: {{ number_format($transaction->points_redeemed) }} Points</td>
											</tr>

											@if( $transaction->cash_payment > 0)
												<tr>
													<td colspan="4" style="line-height:1.6; text-align: right; font-weight: bold; margin:0; padding: 0 4% 0 0;" align="right">Total Amount Paid (USD): {{ $transaction->cash_payment }}</td>
												</tr>
											@endif
										</table>
									@endif

									@if($transaction->trans_class == "Amount")
										<p style="font-size:14px; line-height:1.6; font-weight:normal; margin: 0 0 10px; padding:0;">
											<strong style="line-height:1.6; margin:0; padding:0;">Transaction Details:</strong>
										</p>
										<?php
										$value =  explode(" | ", $transaction->ref_number);
										if(!empty($value[0])){
											$ref =  $value[0];
										}
										else{
											$ref =  $transaction->ref_number;
										}
										?>

										<table border="0" cellspacing="0" cellpadding="0" style="line-height:1.6; width:100%; margin:0; padding:0;">
											<tr>
												<td style="line-height:1.6; font-weight: bold; color: #fff; text-align: center; background-color: #58595B; margin:0; padding: 5px;" align="center" bgcolor="#58595B">Date</td>
												@if($transaction->trx_reward != '1')
													<td style="line-height:1.6; font-weight: bold; color: #fff; text-align: center; background-color: #58595B; margin:0; padding: 5px;" align="center" bgcolor="#58595B">Amount</td>
												@endif
												<td style="line-height:1.6; font-weight: bold; color: #fff; text-align: center; background-color: #58595B; margin:0; padding: 5px;" align="center" bgcolor="#58595B">Points</td>
												<td style="line-height:1.6; font-weight: bold; color: #fff; text-align: center; background-color: #58595B; margin:0; padding: 5px;" align="center" bgcolor="#58595B">Partner Store</td>
											</tr>
											<tr>
												<td style="line-height:1.6; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin:0; padding: 10px 5px;" align="center">{{ date("F j, Y, g:i a", strtotime($transaction->created_at)) }}</td>
												@if($transaction->trx_reward != '1')
													<td style="line-height:1.6; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin:0; padding: 10px 5px;" align="center">{{ $shortCode }}  {{ number_format($transaction->original_total_amount,2) }}</td>
												@endif
												@if($transaction->trx_reward == '1')
													<td style="line-height:1.6; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin:0; padding: 10px 5px;" align="center">{{ number_format($transaction->points_rewarded) }}</td>
												@elseif ($transaction->trx_redeem == '1')
													<td style="line-height:1.6; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin:0; padding: 10px 5px;" align="center">{{ number_format($transaction->points_redeemed) }}</td>
												@endif
												<td style="line-height:1.6; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin:0; padding: 10px 5px;" align="center">{{ $ref }}</td>
											</tr>
										</table>
									@endif

									@if($transaction->trans_class == "Points")
										@if(strtolower($transaction->ref_number)=="cashback" || strtolower($transaction->notes)=="cashback")
											<p style="font-size:14px; line-height:1.6; font-weight:normal; margin: 0 0 10px; padding:0;">
												<strong style="line-height:1.6; margin:0; padding:0;">Cashback Details:</strong>
											</p>
											<table border="0" cellspacing="0" cellpadding="0" style="line-height:1.6; width:100%; margin:0; padding:0;">
												<tr>
													<td style="line-height:1.6; font-weight: bold; color: #fff; text-align: center; background-color: #58595B; margin:0; padding: 5px;" align="center" bgcolor="#58595B">Date</td>
													<td style="line-height:1.6; font-weight: bold; color: #fff; text-align: center; background-color: #58595B; margin:0; padding: 5px;" align="center" bgcolor="#58595B">Points</td>
													<td style="line-height:1.6; font-weight: bold; color: #fff; text-align: center; background-color: #58595B; margin:0; padding: 5px;" align="center" bgcolor="#58595B">Amount</td>
													<!--<td style="line-height:1.6; font-weight: bold; color: #fff; text-align: center; background-color: #58595B; margin:0; padding: 5px;" align="center" bgcolor="#58595B">Card</td>-->
												</tr>
												<tr>
													<td style="line-height:1.6; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin:0; padding: 10px 5px;" align="center">{{ date("F j, Y, g:i a", strtotime($transaction->created_at)) }}</td>
													<td style="line-height:1.6; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin:0; padding: 10px 5px;" align="center">{{ number_format($transaction->points_redeemed) }}</td>
													<td style="line-height:1.6; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin:0; padding: 10px 5px;" align="center">{{ App\Currency::find($transaction->currency_id)->short_code }} {{ number_format($transaction->original_total_amount,2) }}</td>
													<?php
													//$card_type = json_decode($transaction->notes);
													//$xtea = new XTEA($transaction->partner_id);
													//$first_digits = $xtea->Decrypt($card_type->first_digits);
													//$last_digits = $xtea->Decrypt($card_type->last_digits);
													?>
													<!--<td style="line-height:1.6; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin:0; padding: 10px 5px;" align="center"><?php //echo $card_type->card_type;?><br><?php //echo $first_digits . "******" . $last_digits;?></td>-->
												</tr>
											</table>
										@elseif(strpos(strtolower($transaction->ref_number),"miles points") > 0)
											<p style="font-size:14px; line-height:1.6; font-weight:normal; margin: 0 0 10px; padding:0;">
												<strong style="line-height:1.6; margin:0; padding:0;">Miles Conversion Details:</strong>
											</p>
											<table border="0" cellspacing="0" cellpadding="0" style="line-height:1.6; width:100%; margin:0; padding:0;">
												<tr>
													<td style="line-height:1.6; font-weight: bold; color: #fff; text-align: center; background-color: #58595B; margin:0; padding: 5px;" align="center" bgcolor="#58595B">Date</td>
													<td style="line-height:1.6; font-weight: bold; color: #fff; text-align: center; background-color: #58595B; margin:0; padding: 5px;" align="center" bgcolor="#58595B">Points</td>
													<td style="line-height:1.6; font-weight: bold; color: #fff; text-align: center; background-color: #58595B; margin:0; padding: 5px;" align="center" bgcolor="#58595B">Miles</td>
													<td style="line-height:1.6; font-weight: bold; color: #fff; text-align: center; background-color: #58595B; margin:0; padding: 5px;" align="center" bgcolor="#58595B">Frequent Flyer Details</td>
												</tr>
												<tr>
													<td style="line-height:1.6; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin:0; padding: 10px 5px;" align="center">{{ date("F j, Y, g:i a", strtotime($transaction->created_at)) }}</td>
													<?php
													$miles_details = json_decode($transaction->ref_number, TRUE);
													$miles = $miles_details['miles points'];
													?>
													<td style="line-height:1.6; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin:0; padding: 10px 5px;" align="center">{{ number_format($miles) }}</td>
													<td style="line-height:1.6; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin:0; padding: 10px 5px;" align="center">{{ number_format($transaction->points_redeemed) }}</td>
													<?php
													$frequentFlyerDetails = json_decode($transaction->notes, TRUE);
													$affiliateProgram = App\AffiliateProgram::find($frequentFlyerDetails['airline'])->name;
													$frequentFlyerNumber = $frequentFlyerDetails['frequent_flyer_number'];
													?>
													<td style="line-height:1.6; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin:0; padding: 10px 5px;" align="center"><?php echo $affiliateProgram;?><br><?php echo $frequentFlyerNumber;?></td>
												</tr>
											</table>
										@else
											<p style="font-size:14px; line-height:1.6; font-weight:normal; margin: 0 0 10px; padding:0;">
												<strong style="line-height:1.6; margin:0; padding:0;">Transaction Details:</strong>
											</p>
											<table border="0" cellspacing="0" cellpadding="0" style="line-height:1.6; width:100%; margin:0; padding:0;">
												<tr>
													<td style="line-height:1.6; font-weight: bold; color: #fff; text-align: center; background-color: #58595B; margin:0; padding: 5px;" align="center" bgcolor="#58595B">Date</td>
													<td style="line-height:1.6; font-weight: bold; color: #fff; text-align: center; background-color: #58595B; margin:0; padding: 5px;" align="center" bgcolor="#58595B">Points</td>
												</tr>
												<tr>
													<td style="line-height:1.6; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin:0; padding: 10px 5px;" align="center">{{ date("F j, Y, g:i a", strtotime($transaction->created_at)) }}</td>
													<td style="line-height:1.6; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin:0; padding: 10px 5px;" align="center">{{ number_format($transaction->points_redeemed) }}</td>
												</tr>
											</table>
										@endif
									@endif

									@if(strtolower($transaction->trans_class) == "flight")
										<table border="0" cellspacing="0" cellpadding="0" style="line-height:1.6; width:100%; margin:0; padding:0;">
											<tr>
												<td style="line-height:1.6; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px; font-weight: bold; color: #000000; text-align: center; background-color: #e0e0e0; margin:0; padding: 5px;" align="center" bgcolor="#58595B"></td>
												<td style="line-height:1.6; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; font-weight: bold; color: #000000; text-align: center; background-color: #e0e0e0; margin:0; padding: 5px;" align="center" bgcolor="#58595B">Airline / Flight</td>
												<td style="line-height:1.6; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; font-weight: bold; color: #000000; text-align: center; background-color: #e0e0e0; margin:0; padding: 5px;" align="center" bgcolor="#58595B">Depart / Arrive</td>
												<td style="line-height:1.6; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; font-weight: bold; color: #000000; text-align: center; background-color: #e0e0e0; margin:0; padding: 5px;" align="center" bgcolor="#58595B">City / Airport</td>
												<td style="line-height:1.6; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; font-weight: bold; color: #000000; text-align: center; background-color: #e0e0e0; margin:0; padding: 5px;" align="center" bgcolor="#58595B">Duration / Stops </td>
											</tr>
											<tr>
												<td rowspan="2" style="line-height:1.6; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px; text-align: center; margin:0; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; " align="center"><img src="{{ asset('img/departBig.png') }}" width="100" /></td>
												<td rowspan="2" style="line-height:1.6; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; text-align: center; margin:0; padding: 10px 5px;" align="center"><?php echo $o_carrier_name ?></td>
												<td style="line-height:1.6; /*border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;*/border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px;/*border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px;*/ text-align: center; margin:0; padding: 10px 5px 0px 5px;" align="center"><?php echo date("F j, Y, g:i a", strtotime($o_depart_time)) ?></td>
												<td style="line-height:1.6; /*border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px;*//*border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px;*/ text-align: center; margin:0; padding: 10px 5px 0px 5px;" align="center"><?php echo $o_depart_station_name . " (" . $o_depart_station_code . ")" ?></td>
												<td style="line-height:1.6; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px;/*border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px;*/ text-align: center; margin:0; padding: 10px 5px 0px 5px;" align="center"><?php echo floor($o_duration / 60)."H ".($o_duration % 60)."m" ?></td>
											</tr>
											<tr>
												<td style="line-height:1.6; /*border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;*/border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin:0; padding: 0px 5px 10px 5px;" align="center"><?php echo date("F j, Y, g:i a", strtotime($o_arrive_time)) ?></td>
												<td style="line-height:1.6; /*border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px;*/ border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin:0; padding: 0px 5px 10px 5px;" align="center"><?php echo $o_arrive_station_name . " (" . $o_arrive_station_code . ")"?></td>
												<td style="line-height:1.6; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin:0; padding: 0px 5px 10px 5px;" align="center"><?php echo $o_stops ?></td>
											</tr>
											<?php  if($return == "1"): ?>
												<tr>
													<td rowspan="2" style="line-height:1.6; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px; text-align: center; margin:0; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; " align="center"><img src="{{ asset('img/arrivalBig.png') }}" width="100" /></td>
													<td rowspan="2" style="line-height:1.6; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin:0; padding: 10px 5px;" align="center"><?php echo $i_carrier_name ?></td>
													<td style="line-height:1.6; /*border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;*/border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; /*border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px;*/ text-align: center; margin:0; padding: 10px 5px 0px 5px;" align="center"><?php echo date("F j, Y, g:i a", strtotime($i_depart_time)) ?></td>
													<td style="line-height:1.6; /*border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; *//*border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px;*/ text-align: center; margin:0; padding: 10px 5px 0px 5px;" align="center"><?php echo $i_depart_station_name . " (" . $i_depart_station_code . ")" ?></td>
													<td style="line-height:1.6; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; /*border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px;*/ text-align: center; margin:0; padding: 10px 5px 0px 5px;" align="center"><?php echo floor($i_duration / 60)."H ".($i_duration % 60)."m" ?></td>
												</tr>
												<tr>
													<td style="line-height:1.6; /*border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;*/border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin:0; padding: 0px 5px 10px 5px;" align="center"><?php echo date("F j, Y, g:i a", strtotime($i_arrive_time)) ?></td>
													<td style="line-height:1.6; /*border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px;*/ border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin:0; padding: 0px 5px 10px 5px;" align="center"><?php echo $i_arrive_station_name . " (" . $i_arrive_station_code . ")"?></td>
													<td style="line-height:1.6; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin:0; padding: 0px 5px 10px 5px;" align="center"><?php echo $i_stops ?></td>
												</tr>
											<?php endif; ?>
										</table>
									@endif

									@if(strtolower($transaction->trans_class) == "hotel")
										<?php
										$h_name = 'N/A';
										$room_details = 'N/A';
										$room_name = 'N/A';
										$h_address = 'N/A';
										$image_source = 'N/A';

										if(isset($json_notes)):
											if(!empty($json_notes->hotelName)){
												$h_name = $json_notes->hotelName;
											}
											if(!empty($json_notes->hotelAddress1)){
												$h_address = $json_notes->hotelAddress1;
											}
											if(!empty($json_notes->hotel_name)){
											   $h_name = $json_notes->hotel_name;
											}
											if (!empty($json_notes->roomName)) {
												$room_details = $json_notes->roomName;
											}
											if(!empty($json_notes->roomDetails)){
											   $room_details = $json_notes->roomDetails;
											}
											if(!empty($json_notes->reservation->item->name)){
											   $room_name = $json_notes->reservation->item->name;
											}
											if(!empty($json_notes->hotel_address)){
											   $h_address = $json_notes->hotel_address;
											}
											$h_arrivalDate = $json_notes->arrivalDate;
											$h_departureDate = $json_notes->departureDate;
											$image_source = "";
											if(!empty($json_notes->img)){
												$image_source = $transaction->getHotelImageUrl();
											}
											if(!empty($json_notes->hotel_image)){
												$image_source = $json_notes->hotel_image;
											}
//											$h_thumbnail		= $json_notes->thumbnail;
//											$h_name				= $json_notes->hotelName;
//											$h_address			= $json_notes->hotelAddress1;
//											$h_arrivalDate		= $json_notes->arrivalDate;
//											$h_departureDate	= $json_notes->departureDate;
//											$img_url_prefix		= "http://images.trvl-media.com";
//											$h_thumbnail		= urldecode($json_notes->img);
//											$h_img_small		= str_replace("_t.jpg","_s.jpg",$h_thumbnail);
//											$h_img				= $img_url_prefix . $h_img_small;
											?>
											<table border="0" cellspacing="0" cellpadding="0" style="line-height: 0.8; width:100%; margin:0; padding:0;">
												<tr style="line-height: 0.2; margin:0; padding:0;">
													<td rowspan="6" style="line-height: 0.2; text-align: center; margin:0; " align="center"><img src="<?php echo $image_source; ?>" width="180" /></td>
													<td style="line-height: 0.2; text-align: left; margin:0; padding: 10px 5px;" align="left"><strong>Customer Name: </strong>{{ $json_notes->book_first_name . ' ' . $json_notes->book_last_name }}</td>
												</tr>
												<tr style="line-height: 0.2; margin:0; padding:0;">
													<td style="line-height: 0.37; text-align: left; margin:0; padding: 10px 5px;" align="left"><strong>Customer Mobile: </strong>{{ $json_notes->book_mobile }}</td>
												</tr>
												<tr style="line-height: 0.37; margin:0; padding:0;">
													<td style="line-height: 0.2; text-align: left; margin:0; padding: 10px 5px;" align="left"><strong>Customer Email: </strong>{{ $json_notes->book_email }}</td>
												</tr>
												<tr style="line-height: 0.2; margin:0; padding:0;">
													<td style="line-height: 0.2; text-align: left; margin:0; padding: 10px 5px;" align="left"><strong>Hotel Name: </strong><?php echo $h_name; ?></td>
												</tr>
												<tr style="line-height: 0.2; margin:0; padding:0;">
													<td style="line-height: 1.2; text-align: left; margin:0; padding: 10px 5px;" align="left"><strong>Room Details: </strong><?php echo $room_details; ?></td>
												</tr>
												<tr style="line-height: 0.2; margin:0; padding:0;">
													<td style="line-height: 1.2; text-align: left; margin:0; padding: 10px 5px;" align="left"><strong>Hotel Address: </strong><?php echo $h_address; ?></td>
												</tr>
												<tr style="line-height: 0.2; margin:0; padding:0;">
													<td style="line-height: 0.2; text-align: left; margin:0; padding: 10px 5px;" align="left"><strong>Check in: </strong><?php echo $h_arrivalDate; ?></td>
												</tr>
												<tr style="line-height: 0.2; margin:0; padding:0;">
													<td style="line-height: 0.2; text-align: left; margin:0; padding: 10px 5px;" align="left"><strong>Check out: </strong><?php echo $h_departureDate; ?></td>
												</tr>
											</table>
										<?php endif; ?>
									@endif

									@if(strtolower($transaction->trans_class) == "car")
										<?php
										if(isset($json_notes)):
											$image_source = $json_notes->img;
											$p_time = $json_notes->pickup;
											$p_locName = $json_notes->pickup_location_name;
											$d_time = $json_notes->dropoff;
											$d_locName = $json_notes->dropoff_location_name;
											?>
											<table border="0" cellspacing="0" cellpadding="0" style="line-height: 0.8; width:100%; margin:0; padding:0;">
												<tr style="line-height: 0.2; margin:0; padding:0;">
													<td rowspan="3" style="line-height: 0.2; text-align: center; margin:0; " align="center"><img src="<?php echo $image_source; ?>" width="90" /></td>
													<td style="line-height: 0.2; text-align: left; margin:0; padding: 10px 5px;" align="left"><strong>Vehicle Type: </strong><?php echo $json_notes->vehicle; ?></td>
												</tr>
												<tr style="line-height: 0.2; margin:0; padding:0;">
													<td style="line-height: 0.2; text-align: left; margin:0; padding: 10px 5px;" align="left"><strong>Pick up: </strong><?php echo $p_locName . " at " . $p_time; ?></td>
												</tr>
												<tr style="line-height: 0.2; margin:0; padding:0;">
													<td style="line-height: 0.2; text-align: left; margin:0; padding: 10px 5px;" align="left"><strong>Drop Off: </strong><?php echo $d_locName . " at " . $d_time; ?></td>
												</tr>
											</table>
										<?php endif; ?>
									@endif
									<div style="line-height:1.6; height: 10px; width:100%; margin:0; padding:0;"></div>

									@if((strtolower($transaction->trans_class) == "flight" or strtolower($transaction->trans_class) == "hotel" or strtolower($transaction->trans_class) == "car"))
										<div style="line-height:1.6; height: 5px; width:100%; background-color: <?php echo $emailPartnerData['partner']['primary']?>; margin: 10px 0; padding:0;"></div>
										<p style="font-size:14px; line-height:1.6; font-weight:normal; text-align: right; margin: 0 0 10px; padding:0;">
											<strong style="line-height:1.6; margin:0; padding:0;">Total Points:</strong>
											{{number_format(abs(PointsHelper::amountToPoints($transaction->total_amount, 'USD', $transaction->network_id)))}}
										</p>
										<p style="font-size:14px; line-height:1.6; font-weight:normal; text-align: right; margin: 0 0 10px; padding:0;">
											<strong style="line-height:1.6; margin:0; padding:0;">Total Points Redeemed:</strong>
											{{number_format(abs($transaction->points_redeemed))}}
										</p>
										<p style="font-size:14px; line-height:1.6; font-weight:normal; text-align: right; margin: 0 0 10px; padding:0;">
											<strong style="line-height:1.6; margin:0; padding:0;">Total Amount Paid (USD):</strong>
											{{number_format(abs($transaction->cash_payment), 2)}}
										</p>
									@endif
									<div style="line-height:1.6; height: 10px; width:100%; margin:0; padding:0;"></div>
									<div style="line-height:1.6; height: 10px; width:100%; margin:0; padding:0;"></div>

									<table style="line-height:1.6; width:100%; margin:0; padding:0;">
										<tr>
											<td style="font-size: 11px; line-height:1.6; text-align: center; margin:0; padding:0;" align="center">
												This email was sent from a notification-only email address that cannot accept incoming notifications. Please do not reply to this message.
												<br style="line-height:1.6; margin:0; padding:0;" />
												Copyright <?php echo date('Y'); ?> <?php echo $emailPartnerData['copyright']['en']?>. All Rights Reserved
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</div>
				</td>
				<td></td>
			</tr>
		</table>

		<table style="line-height:1.6; width:100%; clear: both !important; margin:0; padding:0;">
			<tr>
				<td></td>
				<td style="line-height:1.6; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto; padding:0;"></td>
				<td></td>
			</tr>
		</table>

		@if(strtolower($transaction->trans_class) == "flight")
			<?php
            if(isset($pnr)){
                $response_json1 = $pnr;
				$response_json = json_encode($response_json1);
				$response_decode_pnr = json_decode($response_json,true);
                if(isset($response_decode_pnr['RetrievePassengerNameRecordResponse']['RetrievePassengerNameRecordResult']['TicketingDate'])){
					$ticketingDateOr = $response_decode_pnr['RetrievePassengerNameRecordResponse']['RetrievePassengerNameRecordResult']['TicketingDate'];
					$pos = strpos($ticketingDateOr, "T");
					if ($pos !== false) {
						$ticketingDate = substr($ticketingDateOr, 0, strpos($ticketingDateOr, "T"));
					}
					else{
						$ticketingDate = $transaction->ref_number;
					}
				}
				else{
					$ticketingDate = 'NOT SET';
				}

                $original_passenger_numbers = array();
                $original_passenger_names = array();
                $original_passenger_lastnames = array();

                if(isset($response_decode_pnr['RetrievePassengerNameRecordResponse']['RetrievePassengerNameRecordResult']['Passengers']['PNRResponsePassenger']) && !empty($response_decode_pnr['RetrievePassengerNameRecordResponse']['RetrievePassengerNameRecordResult']['Passengers']['PNRResponsePassenger']) ) {
					if(!empty($response_decode_pnr['RetrievePassengerNameRecordResponse']['RetrievePassengerNameRecordResult']['Passengers']['PNRResponsePassenger'][0]) ){
						for($z=0; $z<count($response_decode_pnr['RetrievePassengerNameRecordResponse']['RetrievePassengerNameRecordResult']['Passengers']['PNRResponsePassenger']);$z++) {
							$original_passenger_numbers[$z] = $response_decode_pnr['RetrievePassengerNameRecordResponse']['RetrievePassengerNameRecordResult']['Passengers']['PNRResponsePassenger'][$z]['OriginalPassengerNumber'];
                            $original_passenger_names[$z] = $response_decode_pnr['RetrievePassengerNameRecordResponse']['RetrievePassengerNameRecordResult']['Passengers']['PNRResponsePassenger'][$z]['FirstName'];
                            $original_passenger_lastnames[$z] = $response_decode_pnr['RetrievePassengerNameRecordResponse']['RetrievePassengerNameRecordResult']['Passengers']['PNRResponsePassenger'][$z]['Name'];
                        }
					}
					else{
                        $original_passenger_numbers[0] = $response_decode_pnr['RetrievePassengerNameRecordResponse']['RetrievePassengerNameRecordResult']['Passengers']['PNRResponsePassenger']['OriginalPassengerNumber'];
                        $original_passenger_names[0] = $response_decode_pnr['RetrievePassengerNameRecordResponse']['RetrievePassengerNameRecordResult']['Passengers']['PNRResponsePassenger']['FirstName'];
                        $original_passenger_lastnames[0] = $response_decode_pnr['RetrievePassengerNameRecordResponse']['RetrievePassengerNameRecordResult']['Passengers']['PNRResponsePassenger']['Name'];
					}
				}
				else{
                    $original_passenger_numbers[0] = 'NOT SET';
                    $original_passenger_names[0] = 'NOT SET';
                    $original_passenger_lastnames[0] = 'NOT SET';
                }

                $segments_airlines_pnr = array();
                $segments_ref_pnr = array();
                if(isset($response_decode_pnr['RetrievePassengerNameRecordResponse']['RetrievePassengerNameRecordResult']['Segments']['PNRResponseSegment']) && !empty($response_decode_pnr['RetrievePassengerNameRecordResponse']['RetrievePassengerNameRecordResult']['Segments']['PNRResponseSegment'])) {
                    if (!empty($response_decode_pnr['RetrievePassengerNameRecordResponse']['RetrievePassengerNameRecordResult']['Segments']['PNRResponseSegment'][0]) ){
                        for($i=0; $i<count($response_decode_pnr['RetrievePassengerNameRecordResponse']['RetrievePassengerNameRecordResult']['Segments']['PNRResponseSegment']);$i++) {
                            $segments_airlines_pnr[$i] = $response_decode_pnr['RetrievePassengerNameRecordResponse']['RetrievePassengerNameRecordResult']['Segments']['PNRResponseSegment'][$i]['AirlineCode'];
                            $segments_ref_pnr[$i] = $response_decode_pnr['RetrievePassengerNameRecordResponse']['RetrievePassengerNameRecordResult']['Segments']['PNRResponseSegment'][$i]['AirlineRecordLocator'];
                            $original_segment_numbers[$i] = $response_decode_pnr['RetrievePassengerNameRecordResponse']['RetrievePassengerNameRecordResult']['Segments']['PNRResponseSegment'][$i]['OriginalSegmentNumber'];
                        }
                    }
                    else{
                        $segments_airlines_pnr[0] = $response_decode_pnr['RetrievePassengerNameRecordResponse']['RetrievePassengerNameRecordResult']['Segments']['PNRResponseSegment'][0]['AirlineCode'];
                        $segments_ref_pnr[0] = $response_decode_pnr['RetrievePassengerNameRecordResponse']['RetrievePassengerNameRecordResult']['Segments']['PNRResponseSegment'][0]['AirlineRecordLocator'];
                        $original_segment_numbers[0] = $response_decode_pnr['RetrievePassengerNameRecordResponse']['RetrievePassengerNameRecordResult']['Segments']['PNRResponseSegment']['OriginalSegmentNumber'];
                    }
                }
                $outsegments_count = count($out_segments);
                $insegments_count = count($in_segments);

				$original_passenger_numbers_count = count($original_passenger_numbers);
				for ( $p= 0; $p < $original_passenger_numbers_count; $p++){ ?>
					<table style="background-color: #dedede;line-height:1.6; width:100%; margin:0; padding: 20px;">
						<tr>
							<td></td>
						</tr>
						<tr>
							<td bgcolor="#FFFFFF" style="line-height:1.6; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto; padding: 20px; border: 1px solid #f0f0f0;">
								<!-- content -->
								<div style="line-height:1.6; max-width: 600px; display: block; margin: 0 auto; padding:0;">
									<table style="line-height:1.6; width: 600px; margin:0; padding:0;">
										<tr>
											<td>
												<span style="font-size:14px; line-height:1.6; font-weight:normal;  margin:0; padding: 30px 0 5px;">
													<img src="<?php echo url('/');?>/media/image/<?php echo $emailPartnerData['image']['en']?>" alt="<?php echo $emailPartnerData['partner_name_content']['en']?>" style="line-height:1.6; max-width:100%; margin:0; padding:0;" />
												</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												<span style="font-size: 28px; line-height: 1.2; color: #35aeea; font-weight: 200; margin:0; padding: 0 0 40px;">BOOKING CONFIRMATION</span>
												<div style="line-height:1.6; height: 10px; width:100%; margin:0; padding:0;"></div>
												<div style="line-height:1.6; height: 40px; width: 35%; background-color: #35aeea; margin: 10px 0; padding:0;float:right;color:#FFF;text-align: center;">HH REFERENCE<br>&nbsp;&nbsp;&nbsp;{{ $record_locator }}</div>
												<div style="line-height:1.6; height: 10px; width:100%; margin:0; padding:0;"></div>
												<div style="line-height:1.6; height: 10px; width:100%; margin:0; padding:0;"></div>
												<div style="line-height:1.6; height: 23px; width:100%; background-color: #35aeea; margin: 10px 0; padding:0;float:right;color:#FFF;text-align: left;">Passenger Information <span style="float: right"><?php  if( isset($ticketingDate) ){ echo " Issue Date: ".$ticketingDate; } ?> </span></div>
												<div style="line-height:1.6; height: 10px; width:100%; margin:0; padding:0;"></div>
												<div style="line-height:1.6; height: 10px; width:100%; margin:0; padding:0;"></div>
												<div style="line-height:1.6; height: 10px; width:100%; margin:0; padding:0;"></div>
												<div style="line-height:1.6; height: 10px; width:100%; margin:0; padding:0;"></div>
												<div style="line-height:1.6; height: 60px; width:100%; margin:0; padding:0;"><span style="font-weight: bold;">Name: </span><?php echo $original_passenger_names[$p]; ?></div>
												<div style="line-height:1.6; height: 20px; width:100%; margin:0; padding:0;"><span style="font-weight: bold;">Last Name:</span> <?php echo $original_passenger_lastnames[$p]; ?></div>
												<div style="line-height:1.6; height: 10px; width:100%; margin:0; padding:0;"><span style="font-weight: bold;">Mobile: </span><?php echo $adult_mobile; ?></div>
												<div style="line-height:1.6; height: 10px; width:100%; margin:0; padding:0;"></div>
												<div style="line-height:1.6; height: 20px; width:100%; background-color: #35aeea; margin: 10px 0; padding:0;float:right;color:#FFF;text-align: left;">Itinerary</div>
												<div style="line-height:1.6; height: 28px; width:100%; margin:0; padding:0;"></div>
												<div style="line-height:1.6; height: 10px; width:100%; margin:0; padding:0;"></div>

												<?php for($i=0;$i<count($out_segments);$i++) { ?>
													<div style="line-height:1.6; height: 20px; width:100%; background-color: #35aeea; margin: 10px 0; padding:0;float:right;color:#FFF;text-align: left;">Outbound: <?php echo $out_segments[$i][0]; ?> - <?php echo $out_segments[$i][2]; ?><span style="float: right;font-weight: bold;">Reservation Ref:  <span style="font-weight: normal"><?php echo $segments_ref_pnr[$i]; ?></span></span></div>
													<div style="line-height:1.6; height: 10px; width:100%; margin:0; padding:0;"></div>
													<div style="line-height:1.6; height: 54px; width:100%; margin:0; padding:0;"><span style="font-weight: bold;">Departure Date: </span><?php echo $out_segments[$i][1]; ?><span style="float: right;font-weight: bold;">Airline:  <span style="font-weight: normal"><?php echo $segments_airlines_pnr[$i]; ?></span></span></div>
													<div style="line-height:1.6; height: 23px; width:100%; margin:0; padding:0;"><span style="font-weight: bold;">Departure: </span><?php echo $out_segments[$i][0]; ?><span style="float: right;font-weight: bold;">Aircraft:  <span style="font-weight: normal"><?php echo $out_segments[$i][8]; ?></span></span></div>
													<div style="line-height:1.6; height: 23px; width:100%; margin:0; padding:0;"><span style="font-weight: bold;">Arrival Date: </span><?php echo $out_segments[$i][3]; ?><span style="float: right;font-weight: bold;">Cabin:  <span style="font-weight: normal"><?php echo $out_segments[$i][6]; ?></span></span></div>
													<div style="line-height:1.6; height: 23px; width:100%; margin:0; padding:0;"><span style="font-weight: bold;">Arrival: </span><?php echo $out_segments[$i][2]; ?><span style="float: right;font-weight: bold;">Baggage:  <span style="font-weight: normal"><?php echo $out_segments[$i][7]; ?></span></span></div>
													<div style="line-height:1.6; height: 10px; width:100%; margin:0; padding:0;"><span style="font-weight: bold;">Flight No: </span><?php echo $out_segments[$i][5]; ?></div>
													<?php
													$k = $i;
												}
												for($j=0;$j<count($in_segments);$j++){
													$k++;?>

													<div style="line-height:1.6; height: 20px; width:100%; background-color: #35aeea; margin: 10px 0; padding:0;float:right;color:#FFF;text-align: left;">Inbound: <?php echo $in_segments[$j][0]; ?> - <?php echo $in_segments[$j][2]; ?><span style="float: right;font-weight: bold;">Reservation Ref:  <span style="font-weight: normal"><?php echo $segments_ref_pnr[$k]; ?></span></span></div>
													<div style="line-height:1.6; height: 62px; width:100%; margin:0; padding:0;"><span style="font-weight: bold;">Departure Date:  </span><?php echo $in_segments[$j][1]; ?><span style="float: right;font-weight: bold;">Airline:  <span style="font-weight: normal"><?php echo $segments_airlines_pnr[$k]; ?></span></span></div>
													<div style="line-height:1.6; height: 23px; width:100%; margin:0; padding:0;"><span style="font-weight: bold;">Departure:  </span><?php echo $in_segments[$j][0]; ?><span style="float: right;font-weight: bold;">Aircraft:  <span style="font-weight: normal"><?php echo $in_segments[$j][8]; ?></span></span></div>
													<div style="line-height:1.6; height: 23px; width:100%; margin:0; padding:0;"><span style="font-weight: bold;">Arrival Date:  </span><?php echo $in_segments[$j][3]; ?><span style="float: right;font-weight: bold;">Cabin:  <span style="font-weight: normal"><?php echo $in_segments[$j][6]; ?></span></span></div>
													<div style="line-height:1.6; height: 23px; width:100%; margin:0; padding:0;"><span style="font-weight: bold;">Arrival:  </span><?php echo $in_segments[$j][2]; ?><span style="float: right;font-weight: bold;">Baggage: <span style="font-weight: normal"><?php echo $in_segments[$j][7]; ?></span></span></div>
													<div style="line-height:1.6; height: 10px; width:100%; margin:0; padding:0;"><span style="font-weight: bold;">Flight No:  </span><?php echo $in_segments[$j][5]; ?></div>
												<?php }  ?>
											</td>
										</tr>
									</table>
								</div>
							</td>
							<td></td>
						</tr>
						<tr>
							<td>
								<table style="line-height:1.6; width:100%; margin:0; padding:0;">
									<tr>
										<td style="font-size: 11px; line-height:1.6; text-align: center; margin:0; padding:0;color: #35aeea;" align="center">
											Terms and Conditions apply to this booking.
											<br style="line-height:1.6; margin:0; padding:0;" />
											Copyright <?php echo date('Y'); ?> . All Rights Reserved <?php echo $emailPartnerData['copyright']['en']?>, <?php echo $supportEmailEn ?> <?php echo $emailPartnerData['website_url']['en']?>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>

					<table style="line-height:1.6; width:100%; clear: both !important; margin:0; padding:0;">
						<tr>
							<td></td>
						</tr>
						<tr>
							<td style="line-height:1.6; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto; padding:0;"></td>
							<td></td>
						</tr>
					</table>
					<?php
				}
			} ?>
		@endif
	</body>
</html>
