<?php
if (strtolower($lang) == "ar") {
    $align = "right";
    $otheralign = "left";
    $standard = array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9");
    $arabic = array("٠", "١", "٢", "٣", "٤", "٥", "٦", "٧", "٨", "٩");
    $months = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
    $amonths = array("يناير", "فبراير", "مارس", "أبريل", "مايو", "يونيو", "يوليو", "أغسطس", "سبتمبر", "أكتوبر", "نوفمبر", "ديسمبر");
} else {
    $align = "left";
    $otheralign = "right";
}
$image_url = "https://bluai.com";
?>
<?php
$itemTitle;
$itemBody;
$itemImage;
foreach ($items as $item) {
    if ($item->rank != 1) {
        if ($item->type == 'product') {
            $product = App\Product::find($item->item_id);
            if (strtolower($lang) != 'en') {
                $productTranslation = Prodtranslation::where('product_id', $product->id)->where('lang_id', 2)->first();
                if ($productTranslation) {
                    $product->name = $productTranslation->name;
                    $product->model = $productTranslation->model;
                    $product->sub_model = $productTranslation->sub_model;
                    $product->description = $productTranslation->description;
                }
            }
            $itemTitle = $product->name;
            $itemBody = $product->description;
            $itemImage = $product->cover_image;
        } else {
            $offer = App\Offer::find($item->item_id);
            if (strtolower($lang) != 'en') {
                $offerTranslation = Offertranslation::where('offer_id', $offer->id)->where('lang_id', 2)->first();
                if ($offerTranslation) {
                    $offer->name = $offerTranslation->name;
                    $offer->short_description = $offerTranslation->short_description;
                    $offer->description = $offerTranslation->description;
                    $offer->terms_and_condition = $offerTranslation->terms_and_condition;
                    $offer->email_text_promo_1 = $offerTranslation->email_text_promo_1;
                    $offer->email_text_promo_2 = $offerTranslation->email_text_promo_2;
                }
            }
            $itemTitle = $offer->name;
            $itemBody = $offer->email_text_promo_2;
            $itemImage = $offer->cover_image;
        }
        ?>
        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width: 100%;border-collapse: separate; mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;margin-top:10px; background-color:#ffffff; border: 1px solid #75cdd9; -webkit-border-radius: 10px;-moz-border-radius: 10px;border-radius: 10px;">
            <tbody class="mcnTextBlockOuter" style="padding:5px;">
                <tr>
                    <td valign="middle" class="mcnTextBlockInner" style="padding-top: 10px; padding-bottom: 10px; mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                        <!--[if mso]>
                                        <table align="<?php echo $align; ?>"border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                                        <tr>
                                        <![endif]-->

                        <!--[if mso]>
                        <td valign="middle" width="210" style="width:210px;">
                        <![endif]-->
                        <table align="<?php echo $align; ?>"border="0" cellpadding="0" cellspacing="0" style="width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" width="100%" class="mcnTextContentContainer">
                            <tbody><tr>

                                    <td valign="middle" class="mcnTextContent" style="width:194px; padding-top: 0;padding-<?php echo $align; ?>: 5px; padding-<?php echo $otheralign; ?>: 5px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #656565;font-family: Helvetica;font-size: 12px;line-height: 150%;text-align: center;<?php if (strtolower($lang) == "ar") {
            echo "border-left: 1px solid #75cdd9";
        } else {
            echo "border-right:1px solid #75cdd9";
        } ?>">
                                        <img align="none" height="125" src="<?php print $image_url; ?>/media/image/<?php print $itemImage; ?>" alt="<?php print $itemTitle; ?>" style="width: 165px;height: 125px;margin: 0px; border: 0;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" width="165">
                                    </td>

                                    <td valign="middle" class="mcnTextContent" style="padding: 0 30px; mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #656565;font-family: Helvetica;font-size: 12px;line-height: 150%;text-align: <?php echo $align; ?>;">
                                        <p style="text-align:<?php echo $align; ?>;font-weight:bold; font-size: 13px; margin:0 0 5px 0;"><?php print $itemTitle; ?></p><p style="margin:0; text-align:<?php echo $align; ?>;"><?php print strip_tags($itemBody); ?></p>
                                    </td>
                                </tr>
                            </tbody></table>

                    </td>
                </tr>
            </tbody>
        </table>
    <?php
    } else {
        continue;
    }
}?>