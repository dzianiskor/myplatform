<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/xhtml" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">
  <head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>BOOKING CONFIRMATION</title>
  </head>
  <body bgcolor="#FFFFFF" style="background-color: #FFFFFF; font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%; margin: 0; padding: 0;">
<table style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; width: 100%; margin: 0; padding: 20px;"><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"></td>
		<td bgcolor="#FFFFFF" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto; padding: 20px; border: 1px solid #f0f0f0;">


			<div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; max-width: 600px; display: block; margin: 0 auto; padding: 0;">
			<table style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; width: 100%; margin: 0; padding: 0;"><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">
<p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0; padding: 30px 0 5px;">
		<?php list($width, $height) = getimagesize("https://bluai.com/media/image/" . $emailPartnerData['image']['en']);
            $imageWidth = 200;
            if($height > $width / 2){
                $imageWidth = 100;
            }
        ?>
		<img src="src=https://bluai.com/media/image/<?php echo $emailPartnerData['image']['en']?>" alt="<?php echo $emailPartnerData['partner_name_content']['en']?> style=" font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; <?php if($spartner_id!=4188){ echo "width:". $imageWidth ."px"; } ?>; max-width: 100%; margin: 0; padding: 0;" /></p>
                                    <br>

                                            <p style="text-align: center;" align="center"><h2 style="font-family: 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; font-size: 28px; line-height: 1.2; color: #000; font-weight: 200; margin: 0; padding: 0 0 40px;">Flight Booking Redemption Summary @if( $transaction->cash_payment > 0) & Receipt @endif </h2></p>
                                      
						<p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
							Dear <strong style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">{{ $user->name }}</strong>,
						</p>
                                               
                                                
                                                    <p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
                                                            Thank you for redeeming on the <?php echo $emailPartnerData['partner_name_content']['en']?> points travel catalog.
                                                                    <br>
                                                    </p>                                               
							<p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
								Below you will find the summary of your flight booking along with your booking confirmation.
							</p>					
                                                    <p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
                                                        Your reservation is booked and confirmed. There is no need to call us to reconfirm this reservation. <br>    
                                                        For any questions or queries, please contact us on <a href="mailto:<?php echo $emailPartnerData['copyright']['en']?>" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; color: #348eda; margin: 0; padding: 0;"><?php echo $emailPartnerData['copyright']['en']?></a>
                                                    </p> 
                                                    <?php
                                                        $notes = $transaction->notes;
                                                        $json_notes = json_decode($notes);
                                                        if(isset($json_notes)){
                                                            $o_carrier_name = $json_notes->out_carrier_name;
                                                            $o_depart_time = $json_notes->out_depart_time;
                                                            $o_arrive_time = $json_notes->out_arrive_time;
                                                            $o_depart_station_name = $json_notes->out_depart_station_name;
                                                            $o_arrive_station_name = $json_notes->out_arrive_station_name;
                                                            $o_depart_station_code = $json_notes->out_depart_station_code;
                                                            $o_arrive_station_code = $json_notes->out_arrive_station_code;
                                                            $o_duration = $json_notes->out_duration;
                                                            if(strpos($o_duration,"H") !== FALSE ){
                                                                $o_duration_string = $o_duration;
                                                            }else{
                                                                $o_duration_string = floor($o_duration / 60)."H ".($o_duration % 60)."m";
                                                            }
                                                            $o_stops = $json_notes->out_stops;

                                                            $return = -1;
                                                            if(isset($json_notes->return)){
                                                                $return = $json_notes->return;
                                                                $i_carrier_name = $json_notes->in_carrier_name;
                                                                $i_depart_time = $json_notes->in_depart_time;
                                                                $i_arrive_time = $json_notes->in_arrive_time;
                                                                $i_depart_station_name = $json_notes->in_depart_station_name;
                                                                $i_arrive_station_name = $json_notes->in_arrive_station_name;
                                                                $i_depart_station_code = $json_notes->in_depart_station_code;
                                                                $i_arrive_station_code = $json_notes->in_arrive_station_code;
                                                                $i_duration = $json_notes->in_duration;
                                                                if(strpos($i_duration,"H") !== FALSE ){
                                                                    $i_duration_string = $i_duration;
                                                                }else{
                                                                    $i_duration_string = floor($i_duration / 60)."H ".($i_duration % 60)."m";
                                                                }
                                                                $i_stops = $json_notes->in_stops;
                                                            }
                                                            if(isset($json_notes->cabinclass)){
                                                                $cabin_class = $json_notes->cabinclass;
                                                            }
                                                        }
                                                        ?>
						<div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 5px; width: 100%; background-color: #35aeea; margin: 10px 0; padding: 0;"></div>
                                                <p align="center"><strong style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 140%; line-height: 1.6; margin: 0; padding: 0;"><?php  if( isset($cabin_class) ){ echo " Cabin Class: ".$cabin_class; } ?></strong></p>
						<div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 10px; width: 100%; margin: 0; padding: 0;"></div>

							<table border="0" cellspacing="0" cellpadding="0" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; width: 100%; margin: 0; padding: 0;"><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px; font-weight: bold; color: #000000; text-align: center; background-color: #e0e0e0; margin: 0; padding: 5px;" align="center" bgcolor="#58595B"></td>
									<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; font-weight: bold; color: #000000; text-align: center; background-color: #e0e0e0; margin: 0; padding: 5px;" align="center" bgcolor="#58595B">Airline / Flight</td>
									<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; font-weight: bold; color: #000000; text-align: center; background-color: #e0e0e0; margin: 0; padding: 5px;" align="center" bgcolor="#58595B">Depart / Arrive</td>
                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; font-weight: bold; color: #000000; text-align: center; background-color: #e0e0e0; margin: 0; padding: 5px;" align="center" bgcolor="#58595B">City / Airport</td>
                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; font-weight: bold; color: #000000; text-align: center; background-color: #e0e0e0; margin: 0; padding: 5px;" align="center" bgcolor="#58595B">Duration / Stops </td>
                                                            </tr><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td rowspan="2" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px; text-align: center; margin: 0; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; " align="center"><img src="{{ asset('img/departBig.png') }}" width="100" /></td>
									<td rowspan="2" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; text-align: center; margin: 0; padding: 10px 5px;" align="center"><?php echo $o_carrier_name ?></td>
									<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; /*border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;*/border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px;/*border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px;*/ text-align: center; margin: 0; padding: 10px 5px 0px 5px;" align="center"><?php echo date("F j, Y, g:i a", strtotime($o_depart_time)) ?></td>

                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; /*border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px;*//*border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px;*/ text-align: center; margin: 0; padding: 10px 5px 0px 5px;" align="center"><?php echo $o_depart_station_name . " (" . $o_depart_station_code . ")" ?></td>

                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px;/*border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px;*/ text-align: center; margin: 0; padding: 10px 5px 0px 5px;" align="center"><?php echo $o_duration_string ?></td>

								</tr><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">
									<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; /*border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;*/border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin: 0; padding: 0px 5px 10px 5px;" align="center"><?php echo date("F j, Y, g:i a", strtotime($o_arrive_time)) ?></td>
                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; /*border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px;*/ border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin: 0; padding: 0px 5px 10px 5px;" align="center"><?php echo $o_arrive_station_name . " (" . $o_arrive_station_code . ")"?></td>
                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin: 0; padding: 0px 5px 10px 5px;" align="center"><?php echo $o_stops ?></td>

								</tr>

                                                             <?php  if($return == "1"): ?>
                                                                <tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td rowspan="2" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px; text-align: center; margin: 0; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; " align="center"><img src="{{ asset('img/arrivalBig.png') }}" width="100" /></td>
									<td rowspan="2" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin: 0; padding: 10px 5px;" align="center"><?php echo $i_carrier_name ?></td>
									<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; /*border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;*/border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; /*border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px;*/ text-align: center; margin: 0; padding: 10px 5px 0px 5px;" align="center"><?php echo date("F j, Y, g:i a", strtotime($i_depart_time)) ?></td>

                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; /*border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; *//*border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px;*/ text-align: center; margin: 0; padding: 10px 5px 0px 5px;" align="center"><?php echo $i_depart_station_name . " (" . $i_depart_station_code . ")" ?></td>

                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; /*border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px;*/ text-align: center; margin: 0; padding: 10px 5px 0px 5px;" align="center"><?php echo $i_duration_string ?></td>

								</tr>
                                                                <tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">
									<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; /*border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;*/border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin: 0; padding: 0px 5px 10px 5px;" align="center"><?php echo date("F j, Y, g:i a", strtotime($i_arrive_time)) ?></td>
                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; /*border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px;*/ border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin: 0; padding: 0px 5px 10px 5px;" align="center"><?php echo $i_arrive_station_name . " (" . $i_arrive_station_code . ")"?></td>
                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin: 0; padding: 0px 5px 10px 5px;" align="center"><?php echo $i_stops ?></td>

								</tr>
                                                                <?php endif; ?>
                                                        </table>
						<div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 10px; width: 100%; margin: 0; padding: 0;"></div>
						
							<div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 5px; width: 100%; background-color: #35aeea; margin: 10px 0; padding: 0;"></div>

							<p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; text-align: right; margin: 0 0 10px; padding: 0;">
								<strong style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">Total Points:</strong>
								<?php // echo $total_points; ?>
								{{number_format(abs($transaction->total_amount*100))}}
							</p>
							<p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; text-align: right; margin: 0 0 10px; padding: 0;">
								<strong style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">Total Points Redeemed:</strong>
								{{number_format(abs($transaction->points_redeemed))}}
							</p>
							<p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; text-align: right; margin: 0 0 10px; padding: 0;">
								<strong style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">Total Amount Paid (USD):</strong>
								{{number_format(abs($transaction->cash_payment))}}
							</p>
						
						<div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 10px; width: 100%; margin: 0; padding: 0;"></div>

						<div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 10px; width: 100%; margin: 0; padding: 0;"></div>

						<div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 10px; width: 100%; margin: 0; padding: 0;"></div>

						<table style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; width: 100%; margin: 0; padding: 0;"><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; text-align: center; margin: 0; padding: 0;" align="center">
									<p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
										<a href="<?php echo $emailPartnerData['website_url']['en']?>" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 2; color: #FFF; text-decoration: none; font-weight: bold; text-align: center; cursor: pointer; display: inline-block; border-radius: 25px; background-color: #348eda; margin: 0 10px 0 0; padding: 0; border-color: #348eda; border-style: solid; border-width: 10px 20px;">Visit <?php echo $emailPartnerData['partner_name']['en']?> for amazing rewards!</a>
									</p>
								</td>
							</tr></table><div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 10px; width: 100%; margin: 0; padding: 0;"></div>

						<div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 10px; width: 100%; margin: 0; padding: 0;"></div>

						<div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 10px; width: 100%; margin: 0; padding: 0;"></div>

						<table style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; width: 100%; margin: 0; padding: 0;"><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 11px; line-height: 1.6; text-align: center; margin: 0; padding: 0;" align="center">
									This email was sent from a notification-only email address that cannot accept incoming notifications. Please do not reply to this message.
									<br style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;" />
									Copyright <?php echo  date('Y'); ?> <?php echo $emailPartnerData['copyright']['en']?>. All Rights Reserved
								</td>
							</tr></table></td>
				</tr></table></div>


		</td>
		<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"></td>
	</tr></table><table style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; width: 100%; clear: both !important; margin: 0; padding: 0;"><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"></td>
		<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto; padding: 0;">




		</td>
		<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"></td>
	</tr></table>
<?php
        $response_json1 = $pnr;
        $response_json = json_encode($response_json1);
        $response_decode_pnr = json_decode($response_json,true);
        if(isset($response_decode_pnr['RetrievePassengerNameRecordResponse']['RetrievePassengerNameRecordResult']['TicketingDate'])){
             $ticketingDateOr = $response_decode_pnr['RetrievePassengerNameRecordResponse']['RetrievePassengerNameRecordResult']['TicketingDate'];
              $pos = strpos($ticketingDateOr, "T");
                if ($pos !== false) {
                    $ticketingDate = substr($ticketingDateOr, 0, strpos($ticketingDateOr, "T"));                                                                                                     
                }else{ 
                    $ticketingDate = $transaction->ref_number; 
                }
         }else{
             $ticketingDate = 'NOT SET';
         }
        $original_passenger_numbers = array();
        $original_passenger_names = array();
        $original_passenger_lastnames = array();
        
        if(isset($response_decode_pnr['RetrievePassengerNameRecordResponse']['RetrievePassengerNameRecordResult']['Passengers']['PNRResponsePassenger']) && !empty($response_decode_pnr['RetrievePassengerNameRecordResponse']['RetrievePassengerNameRecordResult']['Passengers']['PNRResponsePassenger']) )
         {
             if(!empty($response_decode_pnr['RetrievePassengerNameRecordResponse']['RetrievePassengerNameRecordResult']['Passengers']['PNRResponsePassenger'][0]) ){
                 for($z=0; $z<count($response_decode_pnr['RetrievePassengerNameRecordResponse']['RetrievePassengerNameRecordResult']['Passengers']['PNRResponsePassenger']);$z++)
                {
                    $original_passenger_numbers[$z] = $response_decode_pnr['RetrievePassengerNameRecordResponse']['RetrievePassengerNameRecordResult']['Passengers']['PNRResponsePassenger'][$z]['OriginalPassengerNumber'];
                    $original_passenger_names[$z] = $response_decode_pnr['RetrievePassengerNameRecordResponse']['RetrievePassengerNameRecordResult']['Passengers']['PNRResponsePassenger'][$z]['FirstName'];
                    $original_passenger_lastnames[$z] = $response_decode_pnr['RetrievePassengerNameRecordResponse']['RetrievePassengerNameRecordResult']['Passengers']['PNRResponsePassenger'][$z]['Name'];
                }
             }else{
                $original_passenger_numbers[0] = $response_decode_pnr['RetrievePassengerNameRecordResponse']['RetrievePassengerNameRecordResult']['Passengers']['PNRResponsePassenger']['OriginalPassengerNumber'];
                $original_passenger_names[0] = $response_decode_pnr['RetrievePassengerNameRecordResponse']['RetrievePassengerNameRecordResult']['Passengers']['PNRResponsePassenger']['FirstName'];
                $original_passenger_lastnames[0] = $response_decode_pnr['RetrievePassengerNameRecordResponse']['RetrievePassengerNameRecordResult']['Passengers']['PNRResponsePassenger']['Name'];
             }

         }else{
            $original_passenger_numbers[0] = 'NOT SET';
            $original_passenger_names[0] = 'NOT SET';
            $original_passenger_lastnames[0] = 'NOT SET';
        }
        
        if(isset($response_decode_pnr['RetrievePassengerNameRecordResponse']['RetrievePassengerNameRecordResult']['Segments']['PNRResponseSegment']) && !empty($response_decode_pnr['RetrievePassengerNameRecordResponse']['RetrievePassengerNameRecordResult']['Segments']['PNRResponseSegment']))
        {
            if (!empty($response_decode_pnr['RetrievePassengerNameRecordResponse']['RetrievePassengerNameRecordResult']['Segments']['PNRResponseSegment'][0]) ){
                for($i=0; $i<count($response_decode_pnr['RetrievePassengerNameRecordResponse']['RetrievePassengerNameRecordResult']['Segments']['PNRResponseSegment']);$i++)
                {
                    $original_segment_numbers[$i] = $response_decode_pnr['RetrievePassengerNameRecordResponse']['RetrievePassengerNameRecordResult']['Segments']['PNRResponseSegment'][$i]['OriginalSegmentNumber'];
                }
            }
            else{
                $original_segment_numbers[0] = $response_decode_pnr['RetrievePassengerNameRecordResponse']['RetrievePassengerNameRecordResult']['Segments']['PNRResponseSegment']['OriginalSegmentNumber'];
            }
        }
        $outsegments_count = count($out_segments);
        $insegments_count = count($in_segments);
    ?>
<!-- body -->
 <?php 
  $original_passenger_numbers_count = count($original_passenger_numbers);
 for ( $p= 0; $p < $original_passenger_numbers_count; $p++){?>
<table style="background-color: #FFFFFF;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; width: 100%; margin: 0; padding: 20px;"><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"></td>
    <tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">
        <td bgcolor="#FFFFFF" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto; padding: 20px; border: 1px solid #f0f0f0;">

        <!-- content -->
        <div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; max-width: 600px; display: block; margin: 0 auto; padding: 0;">
        <table style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; width: 600px; margin: 0; padding: 0;">
            <tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">
                <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">
                    <span style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal;  margin: 0; padding: 30px 0 5px;">
                        <img src="https://bluai.com/media/image/<?php echo $emailPartnerData['image']['en']?>" alt="<?php echo $emailPartnerData['partner_name_content']['en']?>" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; max-width: 100%; margin: 0; padding: 0;" /></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                
                    <span style="font-family: 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; font-size: 28px; line-height: 1.2; color: #35aeea; font-weight: 200; margin: 0; padding: 0 0 40px;">BOOKING CONFIRMATION</span>
                    <div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 10px; width: 100%; margin: 0; padding: 0;"></div>
                    <div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 40px; width: 35%; background-color: #35aeea; margin: 10px 0; padding: 0;float:right;color:#FFF;text-align: center;">BOOKING REFERENCE<br>&nbsp;&nbsp;&nbsp;{{ $record_locator }}</div>
                    <div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 10px; width: 100%; margin: 0; padding: 0;"></div>       					
                    <div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 10px; width: 100%; margin: 0; padding: 0;"></div>
                    <div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 23px; width: 100%; background-color: #35aeea; margin: 10px 0; padding: 0;float:right;color:#FFF;text-align: left;">Passenger Information <span style="float: right"><?php  if( isset($ticketingDate) ){ echo " Issue Date: ".$ticketingDate; } ?> </span></div>                  
                    <div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 10px; width: 100%; margin: 0; padding: 0;"></div>
                    <div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 10px; width: 100%; margin: 0; padding: 0;"></div>
                    <div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 10px; width: 100%; margin: 0; padding: 0;"></div>
                    <div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 10px; width: 100%; margin: 0; padding: 0;"></div>
                    <div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 60px; width: 100%; margin: 0; padding: 0;"><span style="font-weight: bold;">Name: </span><?php echo $original_passenger_names[$p]; ?></div>       					
                    <div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 20px; width: 100%; margin: 0; padding: 0;"><span style="font-weight: bold;">Last Name:</span> <?php echo $original_passenger_lastnames[$p]; ?></div>
                    <div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 10px; width: 100%; margin: 0; padding: 0;"><span style="font-weight: bold;">Mobile: </span><?php echo $adult_mobile; ?></div>
                    <div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 10px; width: 100%; margin: 0; padding: 0;"></div>
                <div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 20px; width: 100%; background-color: #35aeea; margin: 10px 0; padding: 0;float:right;color:#FFF;text-align: left;">Itinerary</div>                  
                    <div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 28px; width: 100%; margin: 0; padding: 0;"></div>
                    <div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 10px; width: 100%; margin: 0; padding: 0;"></div>
<?php for($i=0;$i<count($out_segments);$i++){?>                    
                <div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 20px; width: 100%; background-color: #35aeea; margin: 10px 0; padding: 0;float:right;color:#FFF;text-align: left;">Outbound: <?php echo $out_segments[$i][0]; ?> - <?php echo $out_segments[$i][2]; ?></div>                  
                    <div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 10px; width: 100%; margin: 0; padding: 0;"></div>
                    <div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 54px; width: 100%; margin: 0; padding: 0;"><span style="font-weight: bold;">Departure Date: </span><?php echo $out_segments[$i][1]; ?><span style="float: right;font-weight: bold;">  Aircraft:  <span style="font-weight: normal"><?php echo $out_segments[$i][8]; ?></span></span></div>       					
                    <div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 23px; width: 100%; margin: 0; padding: 0;"><span style="font-weight: bold;">Departure: </span><?php echo $out_segments[$i][0]; ?><span style="float: right;font-weight: bold;">  Cabin:  <span style="font-weight: normal"><?php echo $out_segments[$i][6]; ?></span></span></div>
                    <div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 23px; width: 100%; margin: 0; padding: 0;"><span style="font-weight: bold;">Arrival Date: </span><?php echo $out_segments[$i][3]; ?><span style="float: right;font-weight: bold;">  Baggage:  <span style="font-weight: normal"><?php echo $out_segments[$i][7]; ?></span></span></div>
                    <div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 23px; width: 100%; margin: 0; padding: 0;"><span style="font-weight: bold;">Arrival: </span><?php echo $out_segments[$i][2]; ?></div>
                    <div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 10px; width: 100%; margin: 0; padding: 0;"><span style="font-weight: bold;">Flight No: </span><?php echo $out_segments[$i][5]; ?></div>               
<?php }  
for($j=0;$j<count($in_segments);$j++){?>               
                    <div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 20px; width: 100%; background-color: #35aeea; margin: 10px 0; padding: 0;float:right;color:#FFF;text-align: left;">Inbound: <?php echo $in_segments[$j][0]; ?> - <?php echo $in_segments[$j][2]; ?> </span></div>                  
                    <div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 62px; width: 100%; margin: 0; padding: 0;"><span style="font-weight: bold;">Departure Date:  </span><?php echo $in_segments[$j][1]; ?><span style="float: right;font-weight: bold;">  Aircraft:  <span style="font-weight: normal"><?php echo $in_segments[$j][8]; ?></span></span></div>       					
                    <div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 23px; width: 100%; margin: 0; padding: 0;"><span style="font-weight: bold;">Departure:  </span><?php echo $in_segments[$j][0]; ?><span style="float: right;font-weight: bold;">  Cabin:  <span style="font-weight: normal"><?php echo $in_segments[$j][6]; ?></span></span></div>
                    <div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 23px; width: 100%; margin: 0; padding: 0;"><span style="font-weight: bold;">Arrival Date:  </span><?php echo $in_segments[$j][3]; ?><span style="float: right;font-weight: bold;">  Baggage: <span style="font-weight: normal"><?php echo $in_segments[$j][7]; ?></span></span></div>
                    <div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 23px; width: 100%; margin: 0; padding: 0;"><span style="font-weight: bold;">Arrival:  </span><?php echo $in_segments[$j][2]; ?></div>
               <div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 10px; width: 100%; margin: 0; padding: 0;"><span style="font-weight: bold;">Flight No:  </span><?php echo $in_segments[$j][5]; ?></div>
   <?php }  ?>
                </td>
            </tr>
        </table>            
        </div>
        <!-- /content -->
        </td>
        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"></td>
    </tr>
        <tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">
       <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"> 
           <table style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; width: 100%; margin: 0; padding: 0;">
    <tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">
        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 11px; line-height: 1.6; text-align: center; margin: 0; padding: 0;color: #35aeea;" align="center">
Terms and Conditions apply to this booking.
<br style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;" />
Copyright <?php echo  date('Y'); ?> . All Rights Reserved <?php echo $emailPartnerData['copyright']['en']?>, <?php echo $emailPartnerData['support_email']['en']?> <?php echo $emailPartnerData['website_url']['en']?>
</td>
</tr>
</table></td>
            </tr>
</table><!-- /body --><!-- footer -->
        
<table style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; width: 100%; clear: both !important; margin: 0; padding: 0;"><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"></td>
    <tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">	
        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto; padding: 0;">

                        <!-- content -->
                        <!-- /content -->

        </td>
        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"></td>
    </tr>
</table><!-- /footer -->
  </body>
 <?php }?>
</html>
