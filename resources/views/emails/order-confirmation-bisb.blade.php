<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/xhtml" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">
  <head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>BisB Order Confirmation</title>
  </head>
  <body bgcolor="#dedede" style="background-color: #dedede; font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%; margin: 0; padding: 0;">


<!-- body -->
<table style="background-color: #dedede;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; width: 100%; margin: 0; padding: 20px;"><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"></td>
		<td bgcolor="#FFFFFF" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto; padding: 20px; border: 1px solid #f0f0f0;">

			<!-- content -->
			<div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; max-width: 600px; display: block; margin: 0 auto; padding: 0;">
			<table style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; width: 600px; margin: 0; padding: 0;"><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">
<p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal;  margin: 0; padding: 15px 0 5px;">
	<img src="{{ asset('img/email-logo-bisb.png') }}" alt="BisB" style=" font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; max-width: 35%; margin: 0; padding: 0; float: right !important;" /></p>
	<br/>
	<br/>
	<br/>
	<br/>
                                            @if(!(strtolower($transaction->trans_class) == "flight" or strtolower($transaction->trans_class) == "hotel" or strtolower($transaction->trans_class) == "car" or $transaction->trans_class == "Points" or $transaction->trans_class == "Amount" or strtolower($transaction->trans_class) == "reversal"))
						<h2 style="font-family: 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; font-size: 28px; line-height: 1.2; color: #000; font-weight: 200; margin: 0; padding: 0 0 40px;">
                                                         <?php if($transaction->cash_payment > 0): ?>
                                                          Redemption Order Summary & Receipt (#{{ $transaction->id }})
                                                        <?php else: ?>
                                                           Redemption Order Summary (#{{ $transaction->id }})
                                                        <?php endif; ?>
                                                </h2>
                                            @endif
                                            @if(strtolower($transaction->trans_class) == "reversal")
                                                <?php
                                                        $transId = $transaction->ref_number;
                                                        $transaction2	= App\Transaction::find($transId);
                                                ?>
                                                <h2 style="font-family: 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; font-size: 28px; line-height: 1.2; color: #000; font-weight: 200; margin: 0; padding: 0 0 40px;">Reversal Transaction Order Summary (#{{ $transaction->id }})</h2>
                                            @endif
                                            @if(strtolower($transaction->trans_class) == "car")
                                                <h2 style="font-family: 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; font-size: 28px; line-height: 1.2; color: #000; font-weight: 200; margin: 0; padding: 0 0 40px;">
                                                        <?php if($transaction->cash_payment > 0): ?>
                                                             Car Rental Booking Redemption  & Receipt (#{{ $transaction->id }})
                                                        <?php else: ?>
                                                            Car Rental Booking Redemption (#{{ $transaction->id }})
                                                        <?php endif; ?> 
                                                </h2>
                                            @endif
                                            @if(strtolower($transaction->trans_class) == "flight")
                                                <h2 style="font-family: 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; font-size: 28px; line-height: 1.2; color: #000; font-weight: 200; margin: 0; padding: 0 0 40px;">
                                                        <?php if($transaction->cash_payment > 0): ?>
                                                                  Flight Booking Redemption  & Receipt (#{{ $transaction->id }})
                                                        <?php else: ?>
                                                                Flight Booking Redemption (#{{ $transaction->id }})
                                                        <?php endif; ?>
                                                </h2>
                                            @endif
                                            @if(strtolower($transaction->trans_class) == "hotel")
                                                <h2 style="font-family: 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; font-size: 28px; line-height: 1.2; color: #000; font-weight: 200; margin: 0; padding: 0 0 40px;">
                                            
                                                        <?php if($transaction->cash_payment > 0): ?>
                                                                  Hotel Booking Redemption  & Receipt (#{{ $transaction->id }})
                                                        <?php else: ?>
                                                                 Hotel Booking Redemption (#{{ $transaction->id }})
                                                        <?php endif; ?> 
                                                </h2>
                                            @endif
                                            @if($transaction->trans_class == "Points")
                                                @if(strtolower($transaction->ref_number)=="cashback")
                                                    <h2 style="font-family: 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; font-size: 28px; line-height: 1.2; color: #000; font-weight: 200; margin: 0; padding: 0 0 40px;">Cashback Order Summary (#{{ $transaction->id }})</h2>
                                                @elseif(strpos(strtolower($transaction->ref_number),"miles points") > 0)
                                                    <h2 style="font-family: 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; font-size: 28px; line-height: 1.2; color: #000; font-weight: 200; margin: 0; padding: 0 0 40px;">Miles Conversion Order Summary (#{{ $transaction->id }})</h2>
                                                @else
                                                <h2 style="font-family: 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; font-size: 28px; line-height: 1.2; color: #000; font-weight: 200; margin: 0; padding: 0 0 40px;">Transfer Order Summary (#{{ $transaction->id }})</h2>
                                                @endif
                                            @endif
                                            @if($transaction->trans_class == "Amount")
                                                <h2 style="font-family: 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; font-size: 28px; line-height: 1.2; color: #000; font-weight: 200; margin: 0; padding: 0 0 40px;">In Store Transaction Summary (#{{ $transaction->id }})</h2>
                                            @endif
						<p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
							Dear <strong style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">Admin</strong>,
						</p>

						@if(strtolower($transaction->trans_class) == "reversal")
                                                    <p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
								   This is an automated reversal transaction order confirmation for transaction #{{ $transaction2->id }}:<br/><br/>
							</p>
						@else
							<p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
                                                           This is an automated customer order confirmation for the following transaction:<br/><br/>
                                                    </p>
						@endif

						<div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 10px; width: 100%; margin: 0; padding: 0;"></div>

						<div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 5px; width: 100%; background-color: #552988; margin: 10px 0; padding: 0;"></div>

						<div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 10px; width: 100%; margin: 0; padding: 0;"></div>

                                                <table style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; width: 100%; margin: 0; padding: 0;">
                                                <?php

							$ref = \App\Reference::where('user_id', $user->id)->where('partner_id', $transaction->partner_id)->first();
							$loyalty = 'NA';
							if($ref){
								$loyalty = $ref->number;
							}
						?>
                                                    <tr>
                                                        <td style="vertical-align: top;">
                                                           <p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
                                                                <strong style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">Customer Details:</strong>
                                                        </p> 
                                                          
                                                         <table style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; width: 100%; margin: 0; padding: 0;">
                                                            <tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">Loyalty ID:  <?php echo $loyalty; ?></td>
                                                            </tr><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">Full Name: {{ $user->name }}</td>
                                                            </tr><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">Mobile: {{ $user->normalized_mobile }}</td>
                                                                <?php 
                                                                if(strtolower($transaction->trans_class) == "reversal"){
									$transId	= $transaction->ref_number;
									$revTransaction	= App\Transaction::find($transId);
								}
                                                                
                                                                $notes = $transaction->notes;
                                                                $json_notes = json_decode($notes);
                                                                if(!empty($json_notes->email)){
                                                                $email = $json_notes->email;
                                                                }else{
                                                                    $email = $user->email;
                                                                }
                                                                ?>
                                                            </tr><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">Email: <div style="text-align: left; margin: 0; padding: 0;">{{ $user->email }}</div></td>
                                                            </tr><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">Address: {{ $user->address_1 }}</td>
                                                            </tr><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">Building, Residence, Floor: {{ $user->address_2 }}</td>
                                                            </tr><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">
                                                                    @if($user->city)
                                                                            <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">City: {{ $user->city->name }}</td>
                                                                    @else
                                                                            <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">City: N/A</td>
                                                                    @endif
                                                            </tr><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">
                                                                    @if($user->area)
                                                                            <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">State: {{ $user->area->name }}</td>
                                                                    @else
                                                                            <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">State: N/A</td>
                                                                    @endif
                                                            </tr><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">
                                                                    @if($user->country)
                                                                            <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">Country: {{ $user->country->name }}</td>
                                                                    @else
                                                                            <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">Country: N/A</td>
                                                                    @endif
                                                            </tr>
                                                            <?php
                                                            if(strtolower($transaction->trans_class) == "reversal"){
                                                                    ?>
                                                            <tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">
                                                                            Reversed By: {{ Auth::user()->name }}
                                                                    </td>
                                                            </tr>
                                                            <?php } ?>
                                                        </table>   
                                                        </td>
                                                        <td style="vertical-align: top;">
                                                            @if(strtolower($transaction->trans_class) == "flight" || (isset($revTransaction) && strtolower($revTransaction->trans_class) == "flight"))
                                                                <p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
                                                                        <strong style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">Passenger Details:</strong>
                                                                </p>
                                                            
                                                                <?php 
                                                                    $notes = $transaction->notes;
                                                                    $json_notes = json_decode($notes);
                                                                ?>
                                                                <table style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; width: 100%; margin: 0; padding: 0;">
                                                                    <tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">Name: {{ $json_notes->full_name }}</td></tr>
                                                                    <tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">Mobile: {{ $json_notes->mobile }}</td></tr>
                                                                    <tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">Email: <div style="text-align: left; margin: 0; padding: 0;">{{ $json_notes->email }}</div></td></tr>
                                                                    <tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">Date of Birth: {{ $json_notes->dob }}</td></tr>
                                                                    <tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">Passport Number: {{ $json_notes->passport }}</td></tr>
                                                                </table>
                                                            @endif
                                                            
                                                        </td>
                                                    </tr>
                                                </table>

                                                <div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 10px; width: 100%; margin: 0; padding: 0;"></div>

							<?php
								if(strtolower($transaction->trans_class) == "reversal"){
//									$reversedTransaction	= $transaction;
									$transId				= $transaction->ref_number;
									$transaction	= App\Transaction::find($transId);
								}

								if(strtolower($transaction->trans_class) == "flight"){
									$notes = $transaction->notes;
									$json_notes = json_decode($notes);
									if(isset($json_notes)){
										$o_carrier_name = $json_notes->out_carrier_name;
										$o_depart_time = $json_notes->out_depart_time;
										$o_arrive_time = $json_notes->out_arrive_time;
										$o_depart_station_name = $json_notes->out_depart_station_name;
										$o_arrive_station_name = $json_notes->out_arrive_station_name;
										$o_depart_station_code = $json_notes->out_depart_station_code;
										$o_arrive_station_code = $json_notes->out_arrive_station_code;
										$o_duration = $json_notes->out_duration;
										$o_stops = $json_notes->out_stops;
										$i_carrier_name = $json_notes->in_carrier_name;
										$i_depart_time = $json_notes->in_depart_time;
										$i_arrive_time = $json_notes->in_arrive_time;
										$i_depart_station_name = $json_notes->in_depart_station_name;
										$i_arrive_station_name = $json_notes->in_arrive_station_name;
										$i_depart_station_code = $json_notes->in_depart_station_code;
										$i_arrive_station_code = $json_notes->in_arrive_station_code;
										$i_duration = $json_notes->in_duration;
										$i_stops = $json_notes->in_stops;
										$return = -1;
										if(isset($json_notes->return)){
											$return = $json_notes->return;
										}
										if(isset($json_notes->cabinclass)){
											$cabin_class = $json_notes->cabinclass;
										}
									}
								}
							 ?>

						<div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 5px; width: 100%; background-color: #552988; margin: 10px 0; padding: 0;"></div>
                                                <p align="center"><strong style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 140%; line-height: 1.6; margin: 0; padding: 0;"><?php  if( isset($cabin_class) ){ echo " Cabin Class: ".$cabin_class; } ?></strong></p>   
						<div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 10px; width: 100%; margin: 0; padding: 0;"></div>

						@if($transaction->trans_class == "Items")
							<p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
								<strong style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">Order Details:</strong>
							</p>

							<table border="0" cellspacing="0" cellpadding="0" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; width: 100%; margin: 0; padding: 0;"><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; font-weight: bold; color: #fff; text-align: center; background-color: #58595B; margin: 0; padding: 5px;" align="center" bgcolor="#58595B">Item</td>
                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; font-weight: bold; color: #fff; text-align: center; background-color: #58595B; margin: 0; padding: 5px;" align="center" bgcolor="#58595B">Description</td>
									<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; font-weight: bold; color: #fff; text-align: center; background-color: #58595B; margin: 0; padding: 5px;" align="center" bgcolor="#58595B">Qty</td>
									<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; font-weight: bold; color: #fff; text-align: center; background-color: #58595B; margin: 0; padding: 5px;" align="center" bgcolor="#58595B">Total Price</td>

								</tr>

								@foreach($transaction->items as $i)
                                                                <?php $img_src = "https://bluai.com//media/image/" . $i->product->cover_image;?>
								<tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin: 0; padding: 10px 5px;" align="center"><img src="<?php echo $img_src; ?>" width="100" /></td>
                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin: 0; padding: 10px 5px;" align="center">{{ $i->product->name }}</td>
									<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin: 0; padding: 10px 5px;" align="center">{{ $i->quantity }}</td>
                                                                        <?php 
                                                                            $priceInPoints1	= PointsHelper::productPriceInPoints($i->product->price_in_points, $i->product->original_price, $i->product->currency_id, $transaction->network_id);
                                                                            $priceInPoints	= $priceInPoints1 + ($i->product->original_sales_tax * $priceInPoints1 / 100);
                                                                            $total_points	= ($priceInPoints * $i->quantity);
                                                                        ?>
									<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin: 0; padding: 10px 5px;" align="center"><?php echo number_format(abs($i->price_in_points));?></td>

								</tr>
								@endforeach

								<tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">
                                                                    <td colspan="4" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; text-align: right; font-weight: bold; margin: 0; padding: 0 4% 0 0;" align="right">Items Subtotal: {{ number_format($transaction->points_redeemed - $transaction->delivery_cost) }} Points</td>
								</tr>
                                                                <tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">
                                                                    <td colspan="4" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; text-align: right; font-weight: bold; margin: 0; padding: 0 4% 0 0;" align="right">Shipping Charges: {{ number_format($transaction->delivery_cost) }} Points</td>
								</tr>
                                                                <tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">
                                                                    <td colspan="4" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; text-align: right; font-weight: bold; margin: 0; padding: 0 4% 0 0;" align="right">Total: {{ number_format($transaction->points_redeemed) }} Points</td>
								</tr>
                                                                @if( $transaction->cash_payment > 0)
								<tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">
                                                                    <td colspan="4" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; text-align: right; font-weight: bold; margin: 0; padding: 0 4% 0 0;" align="right">Total Amount Paid (USD): {{ $transaction->cash_payment }}</td>
								</tr>
								@endif
                                                        </table>
						@endif

						@if($transaction->trans_class == "Amount")
							<p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
								<strong style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">Transaction Details:</strong>
							</p>
                                                        <?php 
                                                        $value =  explode(" | ", $transaction->ref_number);
                                                        if(!empty($value[0])){
                                                            $ref =  $value[0];
                                                        }
                                                        else{
                                                            $ref =  $transaction->ref_number;
                                                        }
                                                        ?>
                                                
							<table border="0" cellspacing="0" cellpadding="0" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; width: 100%; margin: 0; padding: 0;"><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; font-weight: bold; color: #fff; text-align: center; background-color: #58595B; margin: 0; padding: 5px;" align="center" bgcolor="#58595B">Date</td>
									@if($transaction->trx_reward != '1')
                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; font-weight: bold; color: #fff; text-align: center; background-color: #58595B; margin: 0; padding: 5px;" align="center" bgcolor="#58595B">Amount</td>
									@endif
                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; font-weight: bold; color: #fff; text-align: center; background-color: #58595B; margin: 0; padding: 5px;" align="center" bgcolor="#58595B">Points</td>
                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; font-weight: bold; color: #fff; text-align: center; background-color: #58595B; margin: 0; padding: 5px;" align="center" bgcolor="#58595B">Partner Store</td>
								</tr><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin: 0; padding: 10px 5px;" align="center">{{ date("F j, Y, g:i a", strtotime($transaction->created_at)) }}</td>
									@if($transaction->trx_reward != '1')
                                                                        <?php 
                                                                            $trxCurrency    = App\Currency::where('id', $transaction->currency_id)->first();
                                                                            $shortCode      = $trxCurrency->short_code;
                                                                        ?>
                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin: 0; padding: 10px 5px;" align="center">{{ $shortCode }}  {{ number_format($transaction->original_total_amount,2) }}</td>
									@endif
                                                                        @if($transaction->trx_reward == '1')
                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin: 0; padding: 10px 5px;" align="center">{{ number_format($transaction->points_rewarded) }}</td>
                                                                        @elseif($transaction->trx_redeem == '1')
                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin: 0; padding: 10px 5px;" align="center">{{ number_format($transaction->points_redeemed) }}</td>
                                                                        @endif
                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin: 0; padding: 10px 5px;" align="center">{{ $ref }}</td>
								</tr></table>
						@endif

						@if($transaction->trans_class == "Points")
							@if(strtolower($transaction->ref_number)=="cashback")
							<p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
									<strong style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">Cashback Details:</strong>
								</p>

							<table border="0" cellspacing="0" cellpadding="0" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; width: 100%; margin: 0; padding: 0;"><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; font-weight: bold; color: #fff; text-align: center; background-color: #58595B; margin: 0; padding: 5px;" align="center" bgcolor="#58595B">Date</td>
									<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; font-weight: bold; color: #fff; text-align: center; background-color: #58595B; margin: 0; padding: 5px;" align="center" bgcolor="#58595B">Points</td>
									<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; font-weight: bold; color: #fff; text-align: center; background-color: #58595B; margin: 0; padding: 5px;" align="center" bgcolor="#58595B">Amount</td>
                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; font-weight: bold; color: #fff; text-align: center; background-color: #58595B; margin: 0; padding: 5px;" align="center" bgcolor="#58595B">Card</td>
								</tr><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin: 0; padding: 10px 5px;" align="center">{{ date("F j, Y, g:i a", strtotime($transaction->created_at)) }}</td>
									<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin: 0; padding: 10px 5px;" align="center">{{ number_format($transaction->points_redeemed) }}</td>
									<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin: 0; padding: 10px 5px;" align="center">{{ App\Currency::find($transaction->currency_id)->short_code }} {{ number_format($transaction->original_total_amount,2) }}</td>
                                                                        <?php
                                                                        $card_type = json_decode($transaction->notes);
                                                                        $xtea = new XTEA($transaction->partner_id);
                                                                        $first_digits = $xtea->Decrypt($card_type->first_digits);
                                                                        $last_digits = $xtea->Decrypt($card_type->last_digits);
                                                                        ?>
                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin: 0; padding: 10px 5px;" align="center"><?php echo $first_digits. "*" . $last_digits;?></td>
								</tr>
							</table>
                                                        
                                                        @elseif(strpos(strtolower($transaction->ref_number),"miles points") > 0)
                                                        <p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
									<strong style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">Miles Conversion Details:</strong>
								</p>

							<table border="0" cellspacing="0" cellpadding="0" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; width: 100%; margin: 0; padding: 0;"><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; font-weight: bold; color: #fff; text-align: center; background-color: #58595B; margin: 0; padding: 5px;" align="center" bgcolor="#58595B">Date</td>
									<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; font-weight: bold; color: #fff; text-align: center; background-color: #58595B; margin: 0; padding: 5px;" align="center" bgcolor="#58595B">Points</td>
									<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; font-weight: bold; color: #fff; text-align: center; background-color: #58595B; margin: 0; padding: 5px;" align="center" bgcolor="#58595B">Miles</td>
                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; font-weight: bold; color: #fff; text-align: center; background-color: #58595B; margin: 0; padding: 5px;" align="center" bgcolor="#58595B">Frequent Flyer Details</td>
                                                                        </tr><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin: 0; padding: 10px 5px;" align="center">{{ date("F j, Y, g:i a", strtotime($transaction->created_at)) }}</td>
									<?php
                                                                        $miles_details = json_decode($transaction->ref_number, TRUE);
                                                                        $miles = $miles_details['miles points'];
                                                                        ?>
                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin: 0; padding: 10px 5px;" align="center">{{ number_format($miles) }}</td>
									<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin: 0; padding: 10px 5px;" align="center">{{ number_format($transaction->points_redeemed) }}</td>
                                                                        <?php
                                                                        $frequentFlyerDetails = json_decode($transaction->notes, TRUE);
                                                                        $affiliateProgram = App\AffiliateProgram::find($frequentFlyerDetails['airline'])->name;
                                                                        $frequentFlyerNumber = $frequentFlyerDetails['frequent_flyer_number'];
                                                                        ?>
                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin: 0; padding: 10px 5px;" align="center"><?php echo $affiliateProgram;?><br><?php echo $frequentFlyerNumber;?></td>
								</tr>
							</table>
							@else

							<p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
								<strong style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">Transaction Details:</strong>
							</p>

							<table border="0" cellspacing="0" cellpadding="0" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; width: 100%; margin: 0; padding: 0;"><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; font-weight: bold; color: #fff; text-align: center; background-color: #58595B; margin: 0; padding: 5px;" align="center" bgcolor="#58595B">Date</td>
									<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; font-weight: bold; color: #fff; text-align: center; background-color: #58595B; margin: 0; padding: 5px;" align="center" bgcolor="#58595B">Points</td>
								</tr><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin: 0; padding: 10px 5px;" align="center">{{ date("F j, Y, g:i a", strtotime($transaction->created_at)) }}</td>
									<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin: 0; padding: 10px 5px;" align="center">{{ number_format($transaction->points_redeemed) }}</td>
								</tr></table>
						
						@endif
						@endif

						@if(strtolower($transaction->trans_class) == "flight")
							<table border="0" cellspacing="0" cellpadding="0" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; width: 100%; margin: 0; padding: 0;"><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px; font-weight: bold; color: #000000; text-align: center; background-color: #e0e0e0; margin: 0; padding: 5px;" align="center" bgcolor="#58595B"></td>
									<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; font-weight: bold; color: #000000; text-align: center; background-color: #e0e0e0; margin: 0; padding: 5px;" align="center" bgcolor="#58595B">Airline / Flight</td>
									<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; font-weight: bold; color: #000000; text-align: center; background-color: #e0e0e0; margin: 0; padding: 5px;" align="center" bgcolor="#58595B">Depart / Arrive</td>
                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; font-weight: bold; color: #000000; text-align: center; background-color: #e0e0e0; margin: 0; padding: 5px;" align="center" bgcolor="#58595B">City / Airport</td>
                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; font-weight: bold; color: #000000; text-align: center; background-color: #e0e0e0; margin: 0; padding: 5px;" align="center" bgcolor="#58595B">Duration / Stops </td>
                                                            </tr><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td rowspan="2" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px; text-align: center; margin: 0; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; " align="center"><img src="{{ asset('img/departBig.png') }}" width="100" /></td>
									<td rowspan="2" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; text-align: center; margin: 0; padding: 10px 5px;" align="center"><?php echo $o_carrier_name ?></td>
									<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; /*border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;*/border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px;/*border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px;*/ text-align: center; margin: 0; padding: 10px 5px 0px 5px;" align="center"><?php echo date("F j, Y, g:i a", strtotime($o_depart_time)) ?></td>

                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; /*border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px;*//*border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px;*/ text-align: center; margin: 0; padding: 10px 5px 0px 5px;" align="center"><?php echo $o_depart_station_name . " (" . $o_depart_station_code . ")" ?></td>

                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px;/*border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px;*/ text-align: center; margin: 0; padding: 10px 5px 0px 5px;" align="center"><?php echo floor($o_duration / 60)."H ".($o_duration % 60)."m" ?></td>

								</tr><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">
									<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; /*border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;*/border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin: 0; padding: 0px 5px 10px 5px;" align="center"><?php echo date("F j, Y, g:i a", strtotime($o_arrive_time)) ?></td>
                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; /*border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px;*/ border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin: 0; padding: 0px 5px 10px 5px;" align="center"><?php echo $o_arrive_station_name . " (" . $o_arrive_station_code . ")"?></td>
                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin: 0; padding: 0px 5px 10px 5px;" align="center"><?php echo $o_stops ?></td>

								</tr>
                                                            <?php  if($return == "1"): ?>
                                                            <tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td rowspan="2" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px; text-align: center; margin: 0; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; " align="center"><img src="{{ asset('img/arrivalBig.png') }}" width="100" /></td>
									<td rowspan="2" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin: 0; padding: 10px 5px;" align="center"><?php echo $i_carrier_name ?></td>
									<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; /*border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;*/border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; /*border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px;*/ text-align: center; margin: 0; padding: 10px 5px 0px 5px;" align="center"><?php echo date("F j, Y, g:i a", strtotime($i_depart_time)) ?></td>

                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; /*border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; *//*border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px;*/ text-align: center; margin: 0; padding: 10px 5px 0px 5px;" align="center"><?php echo $i_depart_station_name . " (" . $i_depart_station_code . ")" ?></td>

                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; /*border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px;*/ text-align: center; margin: 0; padding: 10px 5px 0px 5px;" align="center"><?php echo floor($i_duration / 60)."H ".($i_duration % 60)."m" ?></td>

								</tr>
                                                            <tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">
									<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; /*border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;*/border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin: 0; padding: 0px 5px 10px 5px;" align="center"><?php echo date("F j, Y, g:i a", strtotime($i_arrive_time)) ?></td>
                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; /*border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px;*/ border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin: 0; padding: 0px 5px 10px 5px;" align="center"><?php echo $i_arrive_station_name . " (" . $i_arrive_station_code . ")"?></td>
                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin: 0; padding: 0px 5px 10px 5px;" align="center"><?php echo $i_stops ?></td>

								</tr>
                                                            <?php endif; ?>
                                                        </table>
						@endif



						@if(strtolower($transaction->trans_class) == "hotel")
							<?php

                                                            $notes = $transaction->notes;
                                                            $json_notes = json_decode($notes);
                                                            $h_name = 'N/A';
                                                            $room_details = 'N/A';
                                                            $room_name = 'N/A';
                                                            $h_address = 'N/A';
                                                            $image_source = 'N/A';
                                                            
                                                            if(isset($json_notes)):
                                                                
                                                                if(!empty($json_notes->hotelName)){
                                                                    $h_name = $json_notes->hotelName;
                                                                }
                                                                 if(!empty($json_notes->hotelAddress1)){
                                                                    $h_address = $json_notes->hotelAddress1;                                                                     
                                                                 }
                                                                 if(!empty($json_notes->hotel_name)){
                                                                    $h_name = $json_notes->hotel_name;                                                                 
                                                                 }
                                                                 if(!empty($json_notes->roomDetails)){
                                                                    $room_details = $json_notes->roomDetails;                                                                 
                                                                 }
                                                                 if(!empty($json_notes->reservation->item->name)){
                                                                    $room_name = $json_notes->reservation->item->name;
                                                                 }
                                                                 if(!empty($json_notes->hotel_address)){
                                                                    $h_address = $json_notes->hotel_address;
                                                                 }
                                                                $h_arrivalDate = $json_notes->arrivalDate;
                                                                $h_departureDate = $json_notes->departureDate;
                                                                 $image_source = "";
                                                                if(!empty($json_notes->img)){
                                                                    $image_source = $transaction->getHotelImageUrl();
                                                                }
                                                                if(!empty($json_notes->hotel_image)){
                                                                    $image_source = $json_notes->hotel_image;
                                                                }
                                                                //$h_thumbnail = $json_notes->thumbnail;
//                                                                $h_name = $json_notes->hotelName;
//                                                                $h_address = $json_notes->hotelAddress1;
//                                                                $h_arrivalDate = $json_notes->arrivalDate;
//                                                                $h_departureDate = $json_notes->departureDate;
//                                                                $img_url_prefix = "http://images.trvl-media.com";
//                                                                $h_thumbnail = urldecode($json_notes->img);
//                                                                $h_img_small = str_replace("_t.jpg","_s.jpg",$h_thumbnail);
//                                                                $h_img = $img_url_prefix . $h_img_small;
                                                        ?>
                                                            <table border="0" cellspacing="0" cellpadding="0" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 0.8; width: 100%; margin: 0; padding: 0;">
                                                                <tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 0.2; margin: 0; padding: 0;">
                                                                    <td rowspan="6" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 0.2; text-align: center; margin: 0; " align="center"><img src="<?php echo $image_source; ?>" width="180" /></td>
									<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 0.2; text-align: left; margin: 0; padding: 10px 5px;" align="left"><strong>Customer Name: </strong>{{ $json_notes->book_first_name . ' ' . $json_notes->book_last_name }}</td>
								</tr><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 0.2; margin: 0; padding: 0;">
                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 0.37; text-align: left; margin: 0; padding: 10px 5px;" align="left"><strong>Customer Mobile: </strong>{{ $json_notes->book_mobile }}</td>
								</tr><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 0.37; margin: 0; padding: 0;">
                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 0.2; text-align: left; margin: 0; padding: 10px 5px;" align="left"><strong>Customer Email: </strong>{{ $json_notes->book_email }}</td>
								</tr><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 0.2; margin: 0; padding: 0;">
                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 0.2; text-align: left; margin: 0; padding: 10px 5px;" align="left"><strong>Hotel Name: </strong><?php echo $h_name; ?></td>
								</tr><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 0.2; margin: 0; padding: 0;">
                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.2; text-align: left; margin: 0; padding: 10px 5px;" align="left"><strong>Room Details: </strong><?php echo $room_details; ?></td>
								</tr><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 0.2; margin: 0; padding: 0;">
                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 0.2; text-align: left; margin: 0; padding: 10px 5px;" align="left"><strong>Hotel Address: </strong><?php echo $h_address; ?></td>
								</tr><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 0.2; margin: 0; padding: 0;">
                                                                    <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 0.2; text-align: left; margin: 0; padding: 10px 5px;" align="left"><strong>Check in: </strong><?php echo $h_arrivalDate; ?></td>
								</tr><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 0.2; margin: 0; padding: 0;">
                                                                    <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 0.2; text-align: left; margin: 0; padding: 10px 5px;" align="left"><strong>Check out: </strong><?php echo $h_departureDate; ?></td>
								</tr></table>
                                                        <?php endif; ?>
						@endif

						@if(strtolower($transaction->trans_class) == "car")
							<?php

                                                            $notes = $transaction->notes;
                                                            $json_notes = json_decode($notes);
                                                            if(isset($json_notes)):
                                                            $image_source = $json_notes->img;
                                                            $p_time = $json_notes->pickup;
                                                            $p_locName = $json_notes->pickup_location_name;
                                                            $d_time = $json_notes->dropoff;
                                                            $d_locName = $json_notes->dropoff_location_name;
                                                        ?>
                                                            <table border="0" cellspacing="0" cellpadding="0" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 0.8; width: 100%; margin: 0; padding: 0;">
                                                                <tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 0.2; margin: 0; padding: 0;">
                                                                    <td rowspan="3" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 0.2; text-align: center; margin: 0; " align="center"><img src="<?php echo $image_source; ?>" width="90" /></td>
									<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 0.2; text-align: left; margin: 0; padding: 10px 5px;" align="left"><strong>Vehicle Type: </strong><?php echo $json_notes->vehicle; ?></td>
								</tr><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 0.2; margin: 0; padding: 0;">
                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 0.2; text-align: left; margin: 0; padding: 10px 5px;" align="left"><strong>Pick up: </strong><?php echo $p_locName . " at " . $p_time; ?></td>
								</tr><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 0.2; margin: 0; padding: 0;">
                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 0.2; text-align: left; margin: 0; padding: 10px 5px;" align="left"><strong>Drop Off: </strong><?php echo $d_locName . " at " . $d_time; ?></td>
								</tr></table>
                                                        <?php endif; ?>
						@endif

						<div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 10px; width: 100%; margin: 0; padding: 0;"></div>

                                                @if((strtolower($transaction->trans_class) == "flight" or strtolower($transaction->trans_class) == "hotel" or strtolower($transaction->trans_class) == "car"))
                                                    <div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 5px; width: 100%; background-color: #552988; margin: 10px 0; padding: 0;"></div>

							<p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; text-align: right; margin: 0 0 10px; padding: 0;">
										<strong style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">Total Points:</strong>
										{{number_format(abs(PointsHelper::amountToPoints($transaction->total_amount, 'USD', $transaction->network_id)))}}
							</p>
							<p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; text-align: right; margin: 0 0 10px; padding: 0;">
										<strong style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">Total Points Redeemed:</strong>
										{{number_format(abs($transaction->points_redeemed))}}
							</p>
							<p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; text-align: right; margin: 0 0 10px; padding: 0;">
										<strong style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">Total Amount Paid (USD):</strong>
										{{number_format(abs($transaction->cash_payment), 2)}}
							</p>
						@endif

						<div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 10px; width: 100%; margin: 0; padding: 0;"></div>

						<div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 10px; width: 100%; margin: 0; padding: 0;"></div>

						<table style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; width: 100%; margin: 0; padding: 0;"><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 11px; line-height: 1.6; text-align: center; margin: 0; padding: 0;" align="center">
									This email was sent from a notification-only email address that cannot accept incoming notifications. Please do not reply to this message.
									<br style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;" />
									Copyright <?php echo  date('Y'); ?> Bahrain Islamic Bank. All Rights Reserved
								</td>
							</tr></table></td>
				</tr></table></div>
			<!-- /content -->

		</td>
		<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"></td>
	</tr></table><!-- /body --><!-- footer --><table style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; width: 100%; clear: both !important; margin: 0; padding: 0;"><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"></td>
		<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto; padding: 0;">

			<!-- content -->
			<!-- /content -->

		</td>
		<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"></td>
	</tr></table><!-- /footer --></body>
</html>
