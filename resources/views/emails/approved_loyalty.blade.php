<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/xhtml" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">
    <head>
        <meta name="viewport" content="width=device-width" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Loyalty Request Approved</title>
    </head>
    <body bgcolor="#dedede" style="background-color: #dedede; font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%; margin: 0; padding: 0;">
        <table style="background-color: #dedede;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; width: 100%; margin: 0; padding: 20px;">
            <tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">
                <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"></td>
                <td bgcolor="#FFFFFF" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto; padding: 20px; border: 1px solid #f0f0f0;">
                <div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; max-width: 600px; display: block; margin: 0 auto; padding: 0;">
                    <table style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; width: 600px; margin: 0; padding: 0;">
                        <tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">
                            <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">
                                <p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal;  margin: 0; padding: 30px 0 5px;" >
                                    <img src="{{ asset('img/email-logo.png') }}" alt="BLU" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; max-width: 100%; margin: 0; padding: 0;" />
                                </p>
                                <br>
                                <h2 style="font-family: 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; font-size: 28px; line-height: 1.2; color: #000; font-weight: 200; margin: 0; padding: 0 0 40px;">Loyalty Request Approved</h2>

                                <p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
                                    Dear <strong style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">{{ $admin->name }}</strong>,
                                </p>
                                <p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
                                    Your Loyalty Program  "{{ $dataToBeApproved['name'] }}" request has been approved by {{ $parent->name }} 
                                </p>
                                
                                <div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 10px; width: 100%; margin: 0; padding: 0;"></div>
                                <div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 5px; width: 100%; background-color: #35aeea; margin: 10px 0; padding: 0;"></div>
                                <div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 10px; width: 100%; margin: 0; padding: 0;"></div>
                                                
                                <table style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; width: 100%; clear: both !important; margin: 0; padding: 0;">
                                    <tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"></td>
                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto; padding: 0;">
                                            <table>
                                                <tr>
                                                    <td>
                                                        Partner
                                                    </td>
                                                    <td> 
                                                        <?php echo App\Partner::find($dataToBeApproved['partner_id'])->name; ?>  
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Name
                                                    </td>
                                                    <td><?php echo $dataToBeApproved['name']; ?>  
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Description
                                                    </td>
                                                    <td>
                                                        <?php echo $dataToBeApproved['description']; ?>  
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Status
                                                    </td>
                                                    <td>
                                                        <?php echo $dataToBeApproved['status']; ?>  
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Valid From
                                                    </td>
                                                    <td>
                                                        <?php echo $dataToBeApproved['valid_from']; ?>  
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Valid From Time
                                                    </td>
                                                    <td>
                                                        <?php echo $dataToBeApproved['valid_from_time']; ?>  
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Valid to
                                                    </td>
                                                    <td>
                                                        <?php echo $dataToBeApproved['valid_to']; ?>  
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Valid To Time
                                                    </td>
                                                    <td>
                                                        <?php echo $dataToBeApproved['valid_to_time']; ?>  
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Cashback Validity
                                                    </td>
                                                    <td>
                                                        <?php echo $dataToBeApproved['cashback_validity']; ?>  
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Reward Sms
                                                    </td>
                                                    <td>
                                                        <?php echo $dataToBeApproved['reward_sms']; ?>  
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Redemption Sms
                                                    </td>
                                                    <td>
                                                        <?php echo $dataToBeApproved['redemption_sms']; ?>  
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Price in original currency
                                                    </td>
                                                    <td>
                                                        <?php echo $dataToBeApproved['original_reward']; ?>  
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Currency
                                                    </td>
                                                    <td>
                                                        <?php echo App\Currency::find($dataToBeApproved['currency_id'])->name; ?>  
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Points to be Rewarded
                                                    </td>
                                                    <td>
                                                        <?php echo $dataToBeApproved['reward_pts']; ?>  
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Priority
                                                    </td>
                                                    <td>
                                                        <?php echo $dataToBeApproved['priority']; ?>  
                                                    </td>
                                                </tr>

                                            </table>
                                        </td>
                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"></td>
                                    </tr>
                                </table>
                                <table style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; width: 100%; margin: 0; padding: 0;"><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 11px; line-height: 1.6; text-align: center; margin: 0; padding: 0;" align="center">
                                            This email was sent from a notification-only email address that cannot accept incoming notifications. Please do not reply to this message.
                                            <br style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;" />
                                            Copyright <?php echo  date('Y'); ?> BLU Solutions Limited. All Rights Reserved
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
                </td>
            <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"></td>
        </tr>
        </table>
    </body>
</html>
