<!doctype html>
<?php if(strtolower($lang) == "ar"){
    $align = "right";
    $otheralign = "left";
    $standard = array("0","1","2","3","4","5","6","7","8","9");
    $arabic = array("٠","١","٢","٣","٤","٥","٦","٧","٨","٩");
    $months = array("January", "February", "March", "April", "May", "June", "July", "August", "September" ,"October", "November", "December");
    $amonths = array("يناير", "فبراير", "مارس", "أبريل", "مايو", "يونيو", "يوليو", "أغسطس", "سبتمبر", "أكتوبر", "نوفمبر", "ديسمبر");
} else {
    $align = "left";
    $otheralign = "right";
}
$image_url = "https://bluai.com";
?>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office"<?php if(strtolower($lang) == "ar") echo " style='direction: rtl;";?>><head>
		<!-- NAME: LEFT SIDEBAR -->
		<!--[if gte mso 15]>
		<xml>
			<o:OfficeDocumentSettings>
			<o:AllowPNG/>
			<o:PixelsPerInch>96</o:PixelsPerInch>
			</o:OfficeDocumentSettings>
		</xml>
		<![endif]-->
		<meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<title>JANA Rewards Statement</title>
        
    <style type="text/css">
                <?php if(strtolower($lang) == "ar"):?>
                html, body{direction:rtl; font-family: 'adobe-arabic' !important}
                <?php endif;?>
		p{
			margin:10px 0;
			padding:0;
		}
		table{
			border-collapse:collapse;
		}
		h1,h2,h3,h4,h5,h6{
			display:block;
			margin:0;
			padding:0;
		}
		img,a img{
			border:0;
			height:auto;
			outline:none;
			text-decoration:none;
		}
		body,#bodyTable,#bodyCell{
			height:100%;
			margin:0;
			padding:0;
			width:100%;
		}
		#outlook a{
			padding:0;
		}
		img{
			-ms-interpolation-mode:bicubic;
		}
		table{
			mso-table-lspace:0pt;
			mso-table-rspace:0pt;
		}
		.ReadMsgBody{
			width:100%;
		}
		.ExternalClass{
			width:100%;
		}
		p,a,li,td,blockquote{
			mso-line-height-rule:exactly;
		}
		a[href^=tel],a[href^=sms]{
			color:inherit;
			cursor:default;
			text-decoration:none;
		}
		p,a,li,td,body,table,blockquote{
			-ms-text-size-adjust:100%;
			-webkit-text-size-adjust:100%;
		}
		.ExternalClass,.ExternalClass p,.ExternalClass td,.ExternalClass div,.ExternalClass span,.ExternalClass font{
			line-height:100%;
		}
		a[x-apple-data-detectors]{
			color:inherit !important;
			text-decoration:none !important;
			font-size:inherit !important;
			font-family:inherit !important;
			font-weight:inherit !important;
			line-height:inherit !important;
		}
		#bodyCell{
			padding:10px;
		}
		.templateContainer{
			width:600px !important;
		}
		a.mcnButton{
			display:block;
		}
		.mcnImage{
			vertical-align:bottom;
		}
		.mcnTextContent{
			word-break:break-word;
		}
		.mcnTextContent img{
			height:auto !important;
		}
		.mcnDividerBlock{
			table-layout:fixed !important;
		}
		body,#bodyTable{
			background-color:#e7e7e8;
		}
		#bodyCell{
			border-top:0;
		}
		.templateContainer{
			border:0;
		}
		h1{
			color:#202020;
			font-family:Helvetica;
			font-size:26px;
			font-style:normal;
			font-weight:bold;
			line-height:125%;
			letter-spacing:normal;
			text-align:<?php echo $align;?>;
		}
		h2{
			color:#202020;
			font-family:Helvetica;
			font-size:22px;
			font-style:normal;
			font-weight:bold;
			line-height:125%;
			letter-spacing:normal;
			text-align:<?php echo $align;?>;
		}
		h3{
			color:#202020;
			font-family:Helvetica;
			font-size:20px;
			font-style:normal;
			font-weight:bold;
			line-height:125%;
			letter-spacing:normal;
			text-align:<?php echo $align;?>;
		}
		h4{
			color:#202020;
			font-family:Helvetica;
			font-size:5px;
			font-style:normal;
			font-weight:bold;
			line-height:125%;
			letter-spacing:normal;
			text-align:<?php echo $align;?>;
		}
		#templatePreheader{
			background-color:#fafafa;
			background-image:none;
			background-repeat:no-repeat;
			background-position:center;
			background-size:cover;
			border-top:0;
			border-bottom:0;
			padding-top:9px;
			padding-bottom:9px;
		}
		#templatePreheader .mcnTextContent,#templatePreheader .mcnTextContent p{
			color:#656565;
			font-family:Helvetica;
			font-size:12px;
			line-height:150%;
			text-align:<?php echo $align;?>;
		}
		#templatePreheader .mcnTextContent a,#templatePreheader .mcnTextContent p a{
			color:#656565;
			font-weight:normal;
			text-decoration:underline;
		}
		#templateHeader{
			background-color:#FFFFFF;
			background-image:none;
			background-repeat:no-repeat;
			background-position:center;
			background-size:cover;
			border-top:0;
			border-bottom:0;
			padding-top:9px;
			padding-bottom:0;
		}
		#templateHeader .mcnTextContent,#templateHeader .mcnTextContent p{
			color:#202020;
			font-family:Helvetica;
			font-size:14px;
			line-height:150%;
			text-align:<?php echo $align;?>;
		}
		#templateHeader .mcnTextContent a,#templateHeader .mcnTextContent p a{
			color:#2BAADF;
			font-weight:normal;
			text-decoration:underline;
		}
		#templateSidebar{
			background-color:#FFFFFF;
			border-top:0;
			border-bottom:0;
			padding-top:0;
			padding-bottom:0;
		}
		#templateSidebar .mcnTextContent,#templateSidebar .mcnTextContent p{
			color:#656565;
			font-family:Helvetica;
			font-size:12px;
			line-height:150%;
			text-align:<?php echo $align;?>;
		}
		#templateSidebar .mcnTextContent a,#templateSidebar .mcnTextContent p a{
			color:#2BAADF;
			font-weight:normal;
			text-decoration:underline;
		}
		#templateBody,#templateColumns{
			background-color:#FFFFFF;
			border-top:0;
			border-bottom:0;
			padding-top:0;
			padding-bottom:0;
		}
		#templateBody .mcnTextContent,#templateBody .mcnTextContent p{
			color:#202020;
			font-family:Helvetica;
			font-size:14px;
			line-height:150%;
			text-align:<?php echo $align;?>;
		}
		#templateBody .mcnTextContent a,#templateBody .mcnTextContent p a{
			color:#2BAADF;
			font-weight:normal;
			text-decoration:underline;
		}
		#templateFooter{
			background-color:#e7e7e8;
			background-image:none;
			background-repeat:no-repeat;
			background-position:center;
			background-size:cover;
			border-bottom:0;
			padding-top:0;
			padding-bottom:10px;
		}
		#templateFooter .mcnTextContent,#templateFooter .mcnTextContent p{
			color:#656565;
			font-family:Helvetica;
			font-size:12px;
			line-height:150%;
			text-align:center;
		}
		#templateFooter .mcnTextContent a,#templateFooter .mcnTextContent p a{
			color:#656565;
			font-weight:normal;
			text-decoration:underline;
		}
	/*@media only screen and (min-width:768px){
		.templateContainer{
			width:600px !important;
		}

}	@media only screen and (max-width: 480px){
		body,table,td,p,a,li,blockquote{
			-webkit-text-size-adjust:none !important;
		}

}	@media only screen and (max-width: 480px){
		body{
			width:100% !important;
			min-width:100% !important;
		}

}	@media only screen and (max-width: 480px){
		#bodyCell{
			padding-top:10px !important;
		}

}	@media only screen and (max-width: 480px){
		#templateSidebar,#templateBody{
			max-width:100% !important;
			width:100% !important;
		}

}	@media only screen and (max-width: 480px){
		.mcnImage{
			width:100% !important;
		}

}	@media only screen and (max-width: 480px){
		.mcnCartContainer,.mcnCaptionTopContent,.mcnRecContentContainer,.mcnCaptionBottomContent,.mcnTextContentContainer,.mcnBoxedTextContentContainer,.mcnImageGroupContentContainer,.mcnCaptionLeftTextContentContainer,.mcnCaptionRightTextContentContainer,.mcnCaptionLeftImageContentContainer,.mcnCaptionRightImageContentContainer,.mcnImageCardLeftTextContentContainer,.mcnImageCardRightTextContentContainer{
			max-width:100% !important;
			width:100% !important;
		}

}	@media only screen and (max-width: 480px){
		.mcnBoxedTextContentContainer{
			min-width:100% !important;
		}

}	@media only screen and (max-width: 480px){
		.mcnImageGroupContent{
			padding:9px !important;
		}

}	@media only screen and (max-width: 480px){
		.mcnCaptionLeftContentOuter .mcnTextContent,.mcnCaptionRightContentOuter .mcnTextContent{
			padding-top:9px !important;
		}

}	@media only screen and (max-width: 480px){
		.mcnImageCardTopImageContent,.mcnCaptionBlockInner .mcnCaptionTopContent:last-child .mcnTextContent{
			padding-top:5px !important;
		}

}	@media only screen and (max-width: 480px){
		.mcnImageCardBottomImageContent{
			padding-bottom:9px !important;
		}

}	@media only screen and (max-width: 480px){
		.mcnImageGroupBlockInner{
			padding-top:0 !important;
			padding-bottom:0 !important;
		}

}	@media only screen and (max-width: 480px){
		.mcnImageGroupBlockOuter{
			padding-top:9px !important;
			padding-bottom:9px !important;
		}

}	@media only screen and (max-width: 480px){
		.mcnTextContent,.mcnBoxedTextContentColumn{
			padding-<?php echo $otheralign;?>:5px !important;
			padding-<?php echo $align;?>:5px !important;
		}

}	@media only screen and (max-width: 480px){
		.mcnImageCardLeftImageContent,.mcnImageCardRightImageContent{
			padding-<?php echo $otheralign;?>:5px !important;
			padding-bottom:0 !important;
			padding-<?php echo $align;?>:5px !important;
		}

}	@media only screen and (max-width: 480px){
		.mcpreview-image-uploader{
			display:none !important;
			width:100% !important;
		}

}	@media only screen and (max-width: 480px){
		h1{
			font-size:22px !important;
			line-height:125% !important;
		}

}	@media only screen and (max-width: 480px){
		h2{
			font-size:20px !important;
			line-height:125% !important;
		}

}	@media only screen and (max-width: 480px){
		h3{
			font-size:5px !important;
			line-height:125% !important;
		}

}	@media only screen and (max-width: 480px){
		h4{
			font-size:16px !important;
			line-height:150% !important;
		}

}	@media only screen and (max-width: 480px){
		.mcnBoxedTextContentContainer .mcnTextContent,.mcnBoxedTextContentContainer .mcnTextContent p{
			font-size:14px !important;
			line-height:150% !important;
		}

}	@media only screen and (max-width: 480px){
		#templatePreheader{
			display:block !important;
		}

}	@media only screen and (max-width: 480px){
		#templatePreheader .mcnTextContent,#templatePreheader .mcnTextContent p{
			font-size:14px !important;
			line-height:150% !important;
		}

}	@media only screen and (max-width: 480px){
		#templateHeader .mcnTextContent,#templateHeader .mcnTextContent p{
			font-size:16px !important;
			line-height:150% !important;
		}

}	@media only screen and (max-width: 480px){
		#templateSidebar .mcnTextContent,#templateSidebar .mcnTextContent p{
			font-size:16px !important;
			line-height:150% !important;
		}

}	@media only screen and (max-width: 480px){
		#templateBody .mcnTextContent,#templateBody .mcnTextContent p{
			font-size:16px !important;
			line-height:150% !important;
		}

}	@media only screen and (max-width: 480px){
		#templateFooter .mcnTextContent,#templateFooter .mcnTextContent p{
			font-size:14px !important;
			line-height:150% !important;
		}
}*/
    </style></head>
    <body style="<?php if(strtolower($lang) == "ar") echo "direction: rtl;";?>height: 100%;margin: 0;padding: 0;width: 100%;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #e7e7e8;">
        <center>
            <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;height: 100%;margin: 0;padding: 0;width: 100%;background-color: #e7e7e8;">
                <tbody><tr>
                    <td align="center" valign="middle" id="bodyCell" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;height: 100%;margin: 0;padding: 10px;width: 100%;border-top: 0;">
						<!-- BEGIN TEMPLATE // -->
						<!--[if gte mso 9]>
						<table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
						<tr>
						<td align="center" valign="middle" width="600" style="width:600px;">
						<![endif]-->
						<table border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;width: 600px !important;">
							<tbody>
							<tr>
								<td valign="middle" id="templateHeader" style="background:#FFFFFF none no-repeat center/cover;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #FFFFFF;background-image: none;background-repeat: no-repeat;background-position: center;background-size: cover;border-top: 1px solid #75cdd9;border-left: 1px solid #75cdd9; border-right: 1px solid #75cdd9;border-bottom: 0;padding-top: 10px;padding-bottom: 10px;">
                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="middle" class="mcnTextBlockInner" style="padding-top: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
              	<!--[if mso]>
				<table align="<?php echo $align;?>"border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]-->
			    
				<!--[if mso]>
				<td valign="middle" width="210" style="width:210px;">
				<![endif]-->
                <table align="<?php echo $align;?>"border="0" cellpadding="0" cellspacing="0" style="height:90px;width: 192px;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" width="100%" class="mcnTextContentContainer">
                    <tbody><tr>
                        
                        <td valign="middle" class="mcnTextContent" style="padding-top: 0;padding-<?php echo $align;?>: 5px;padding-bottom: 0;padding-<?php echo $otheralign;?>: 5px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #202020;font-family: Helvetica;font-size: 16px;line-height: 150%;text-align:center;">
                        
                            <img align="none" alt="JANA Rewards" width="110" src="https://www.fransijana.com.sa/public/img/logos/Jana-Logo.png" style="width: 120px;margin: 0px;border: 0;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;">
                        </td>
                    </tr>
                </tbody></table>
				<!--[if mso]>
				</td>
				<![endif]-->
                
				<!--[if mso]>
				<td valign="middle" width="390" style="width:390px;">
				<![endif]-->
                <table align="<?php echo $align;?>"border="0" cellpadding="0" cellspacing="0" style="height:90px; width: 240px;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%; margin: 0 10px" width="100%" class="mcnTextContentContainer">
                    <tbody><tr>
                        
                        <td valign="middle" class="mcnTextContent" style="height:90px;padding:0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #202020;font-family: Helvetica;font-size: 16px;line-height: 150%;text-align:<?php echo $align;?>;background-color:#015A6C;-webkit-border-radius: 10px;-moz-border-radius: 10px;border-radius: 10px;">
                        
                            <h1 class="null" style="display: block;margin: 0;padding: 0;color: #202020;font-family: 'MS Sans Serif', sans-serif, Helvetia;text-transform: uppercase;font-size: 20px;font-style: normal;font-weight: normal;line-height: 125%;letter-spacing: normal;text-align:center;"><span style="color:#FFFFFF"><?php echo App\Http\Controllers\DashboardController::platformTranslateKeysForLanguage('lbl-estat-pointsstatement' , strtolower($lang));?></span></h1>

                        </td>
                    </tr>
                </tbody></table>
				<!--[if mso]>
				</td>
				<![endif]-->
                <!--[if mso]>
				<td valign="middle" width="210" style="width:210px;">
				<![endif]-->
                <table align="<?php echo $align;?>"border="0" cellpadding="0" cellspacing="0" style="height:90px;width: 145px;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" width="100%" class="mcnTextContentContainer">
                    <tbody><tr>
                        
                        <td valign="middle" class="mcnTextContent" style="padding-top: 0;padding-<?php echo $align;?>: 5px;padding-bottom: 0;padding-<?php echo $otheralign;?>: 5px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #202020;font-family: Helvetica;font-size: 16px;line-height: 150%;text-align:center;">
                        
                            <img align="none" alt="JANA Rewards" width="132" src="https://www.fransijana.com.sa/public/img/logos/BSF-logo-website.png" style="width: 120px;margin: 0px;border: 0;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;">
                        </td>
                    </tr>
                </tbody></table>
				<!--[if mso]>
				</td>
				<![endif]-->
                
				<!--[if mso]>
				</tr>
				</table>
				<![endif]-->
            </td>
        </tr>
    </tbody>
</table></td>
							</tr>
							<tr>
								<td valign="middle" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;border-left:1px solid #75cdd9;border-right:1px solid #75cdd9; border-bottom:1px solid #75cdd9;">
									<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" id="templateColumns" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #FFFFFF;border-top: 0;border-bottom: 0;padding-top: 0;padding-bottom: 0;">
										<tbody><tr>
											<td valign="middle" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
												<!--[if gte mso 9]>
												<table align="center" border="0" cellspacing="0" cellpadding="0" dir="rtl" width="600" style="width:600px;">
												<tr>
												<td align="center" dir="ltr" valign="middle" width="400" style="width:400px;">
												<![endif]-->
												<table align="<?php echo $otheralign;?>"border="0" cellpadding="0" cellspacing="0" width="400" id="templateBody" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #FFFFFF;border-top: 0;border-bottom: 0;padding-top: 0;padding-bottom: 0;">
													<tbody><tr>
														<td style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnBoxedTextBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
    <!--[if gte mso 9]>
	<table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
	<![endif]-->
	<tbody class="mcnBoxedTextBlockOuter">
        <tr>
            <td valign="middle" class="mcnBoxedTextBlockInner" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                
				<!--[if gte mso 9]>
				<td align="center" valign="middle" ">
				<![endif]-->
                                <?php
        $item = $items{0};
        $itemTitle = '';
        $itemBody = '';
        $itemImage = '';
    if($item->type == 'product'){
        $product = App\Product::find($item->item_id);
        if(strtolower($lang) != 'en'){
            $productTranslation = Prodtranslation::where('product_id', $product->id)->where('lang_id', 2)->first();
            if($productTranslation){
                $product->name = $productTranslation->name;
                $product->model = $productTranslation->model;
                $product->sub_model = $productTranslation->sub_model;
                $product->description = $productTranslation->description;
            }
        }
        $itemTitle = $product->name;
        $itemBody = $product->description;
        $itemImage = $product->cover_image;
    }else{
        $offer = App\Offer::find($item->item_id);
        if(strtolower($lang) != 'en'){
            $offerTranslation = Offertranslation::where('offer_id', $offer->id)->where('lang_id', 2)->first();
            if($offerTranslation){
                $offer->name = $offerTranslation->name;
                $offer->short_description = $offerTranslation->short_description;
                $offer->description = $offerTranslation->description;
                $offer->terms_and_condition = $offerTranslation->terms_and_condition;
                $offer->email_text_promo_1 = $offerTranslation->email_text_promo_1;
                $offer->email_text_promo_2 = $offerTranslation->email_text_promo_2;
            }
        }
        $itemTitle = $offer->name;
        $itemBody = $offer->email_text_promo_1;
        $itemImage = $offer->cover_image;
    }?>
                <table align="<?php echo $align;?>"border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnBoxedTextContentContainer">
                    <tbody><tr>
                        
                        <td style="padding-top: 0;padding-<?php echo $align;?>: 5px;padding-bottom: 10px;padding-<?php echo $otheralign;?>: 10px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                        
                            <table border="0" cellpadding="18" cellspacing="0" class="mcnTextContentContainer" width="100%" style="min-width: 100% !important;background-color: #015A6C; -webkit-border-radius: 10px;-moz-border-radius: 10px;border-radius: 10px;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                <tbody><tr>
                                    <td valign="top" class="mcnTextContent" style="height:180px; padding:30px;color: #ffffff;font-family: Helvetica;font-size: 12px;font-weight: normal;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;">
                                        <div style="text-align:<?php echo $align;?>;"><div style="display:inline-block; float:right"><img align="none" height="125" src="<?php print  $image_url ;?>/media/image/<?php print $itemImage;?>" alt="<?php print $itemTitle;?>" style="width: 175px;height: 125px;margin:0 0 0 10px;float: right;border: 0;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" width="175"></div><p style="font-weight:bold;color:#ffffff; font-size: 15px; margin:0 0 5px 0"><?php print $itemTitle;?></p><p style="margin:0; color:#ffffff;"><?php print substr(strip_tags($itemBody), 0, 150);?></p></div>

                                    </td>
                                </tr>
                            </tbody></table>
                        </td>
                    </tr>
                </tbody></table>
				<!--[if gte mso 9]>
				</td>
				<![endif]-->
                
				<!--[if gte mso 9]>
                </tr>
                </table>
				<![endif]-->
            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnBoxedTextBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
    <!--[if gte mso 9]>
	<table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
	<![endif]-->
	<tbody class="mcnBoxedTextBlockOuter">
        <tr>
            <td valign="middle" class="mcnBoxedTextBlockInner" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                
				<!--[if gte mso 9]>
				<td align="center" valign="middle" ">
				<![endif]-->
                <table align="<?php echo $align;?>"border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnBoxedTextContentContainer">
                    <tbody><tr>
                        
                        <td style="padding-top: 0;padding-<?php echo $align;?>: 5px;padding-bottom: 10px;padding-<?php echo $otheralign;?>: 10px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                        
                            <table border="0" cellpadding="18" cellspacing="0" class="mcnTextContentContainer" width="100%" style="min-width: 100% !important;background-color: #E7E8E9; -webkit-border-radius: 10px;-moz-border-radius: 10px;border-radius: 10px;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                <tbody><tr>
                                    <td valign="top" class="mcnTextContent" style="padding:30px; height: 213px; color: #F2F2F2;font-family: Helvetica;font-size: 14px;font-weight: normal;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;line-height: 150%;">
                                        <h2 class="null" style="display: block;margin: 0;padding: 0;color: #000000;font-family: Helvetica;font-size: 22px;font-style: normal;font-weight: bold;line-height: 125%;letter-spacing: normal;text-align:<?php echo $align;?>;"><?php print $eStatement->title; ?></h2>

<div style="text-align:<?php echo $align;?>; margin-top:15px"><span style="color:#000000"><?php print $eStatement->description; ?></span></div>

                                    </td>
                                </tr>
                            </tbody></table>
                        </td>
                    </tr>
                </tbody></table>
				<!--[if gte mso 9]>
				</td>
				<![endif]-->
                
				<!--[if gte mso 9]>
                </tr>
                </table>
				<![endif]-->
            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnButtonBlock" style="margin-top: 0; min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
    <tbody class="mcnButtonBlockOuter">
        <tr>
            <td style="padding-top: 0;padding-<?php echo $otheralign;?>: 10px;padding-bottom: 5px;padding-<?php echo $align;?>: 5px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" valign="middle" align="center" class="mcnButtonBlockInner">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnButtonContentContainer" style="border-collapse: separate !important;border-radius: 3px;background-color: #75CDD9; mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                    <tbody>
                        <tr>
                            <td align="center" valign="middle" class="mcnButtonContent" style="font-family: Arial;font-size: 16px;padding: 15px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                <a class="mcnButton " title="><?php echo App\Http\Controllers\DashboardController::platformTranslateKeysForLanguage('lbl-estat-viewaccount', strtolower($lang));?>" href="http://fransijana.com.sa/account/statement" target="_blank" style="font-weight: bold;letter-spacing: normal;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;display: block;"><?php echo App\Http\Controllers\DashboardController::platformTranslateKeysForLanguage('lbl-estat-viewaccount', strtolower($lang));?></a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table></td>
													</tr>
												</tbody></table>
												<!--[if gte mso 9]>
												</td>
												<td align="center" dir="ltr" valign="middle" width="200" style="width:200px;">
												<![endif]-->
												<table align="<?php echo $align;?>"border="0" cellpadding="0" cellspacing="0" width="198" id="templateSidebar" style="width: 197px;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #FFFFFF;border-top: 0;border-bottom: 0;padding-top: 0;padding-bottom: 0;">
													<tbody><tr>
														<td valign="middle" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnBoxedTextBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
    <!--[if gte mso 9]>
	<table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
	<![endif]-->
	<tbody class="mcnBoxedTextBlockOuter">
        <tr>
            <td valign="middle" class="mcnBoxedTextBlockInner" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                
				<!--[if gte mso 9]>
				<td align="center" valign="middle" ">
				<![endif]-->
                <table align="<?php echo $align;?>"border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnBoxedTextContentContainer">
                    <tbody><tr>
                        <td style="padding-top: 0;padding-<?php echo $align;?>: 10px;padding-bottom: 10px;padding-<?php echo $otheralign;?>: 5px; mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                            <table border="0" cellpadding="18" cellspacing="0" class="mcnTextContentContainer" width="100%" style="min-width: 100% !important;background-color: #015A6C; -webkit-border-radius: 10px;-moz-border-radius: 10px;border-radius: 10px; border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                <tbody><tr>
                                    <td valign="middle" class="mcnTextContent" style="height: 240px; padding:0 10px; color: #F2F2F2;font-family: Helvetica;font-size: 14px;font-weight: normal;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;line-height: 150%;">
                                        <div style="text-align:<?php echo $align;?>;line-height:1.3">
<span style="font-size:11px"><?php echo App\Http\Controllers\DashboardController::platformTranslateKeysForLanguage('lbl-estat-membername', strtolower($lang));?></span><br>
<?php print $user['name'];?><br>
<hr style="border: 0 none;  border-top: 1px dashed #fff;  background: none;  height:0;padding:0;margin:5px 0" />
<span style="font-size:11px"><?php echo App\Http\Controllers\DashboardController::platformTranslateKeysForLanguage('lbl-estat-pointsbalance', strtolower($lang));?></span><br>
<?php if (strtolower($lang) == "ar") {
    print str_replace($standard, $arabic, number_format($user['balance'],0));
} else {
    print number_format($user['balance'],0);
}
?><br>
<hr style="border: 0 none;  border-top: 1px dashed #fff;  background: none;  height:0;padding:0;margin:5px 0" />
<span style="font-size:11px"><?php echo App\Http\Controllers\DashboardController::platformTranslateKeysForLanguage('lbl-estat-eligiblepoints', strtolower($lang));?></span><br>
<?php if (strtolower($lang) == "ar") {
    print str_replace($standard, $arabic, number_format($user['cashback_balance'],0));
} else {
    print number_format($user['cashback_balance'],0);
}
?><br>
<hr style="border: 0 none;  border-top: 1px dashed #fff;  background: none;  height:0;padding:0;margin:5px 0" />
<span style="font-size:11px"><?php echo App\Http\Controllers\DashboardController::platformTranslateKeysForLanguage('lbl-estat-status', strtolower($lang));?></span><br>
<?php print $user['status'];?></div>

                                    </td>
                                </tr>
                            </tbody></table>
                        </td>
                    </tr>
                </tbody></table>
				<!--[if gte mso 9]>
				</td>
				<![endif]-->
                
				<!--[if gte mso 9]>
                </tr>
                </table>
				<![endif]-->
            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnBoxedTextBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
    <!--[if gte mso 9]>
	<table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
	<![endif]-->
	<tbody class="mcnBoxedTextBlockOuter">
        <tr>
            <td valign="middle" class="mcnBoxedTextBlockInner" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                
				<!--[if gte mso 9]>
				<td align="center" valign="middle" ">
				<![endif]-->
                <table align="<?php echo $align;?>"border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnBoxedTextContentContainer">
                    <tbody><tr>
                        
                        <td style="padding-top: 0;padding-<?php echo $align;?>: 10px;padding-bottom: 10px;padding-<?php echo $otheralign;?>: 5px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                        
                            <table border="0" cellpadding="18" cellspacing="0" class="mcnTextContentContainer" width="100%" style="min-width: 100% !important;background-color: #015A6C; -webkit-border-radius: 10px;-moz-border-radius: 10px;border-radius: 10px; border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                <tbody><tr>
                                    <td valign="top" class="mcnTextContent" style="padding:10px; height:310px; color: #F2F2F2;font-family: Helvetica;font-size: 14px;font-weight: normal;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;line-height: 150%;">
                                        <!--<h2 class="null" style="text-align:<?php //echo $align;?>;display: block;margin: 0;padding: 0;color: #202020;font-family: Helvetica;font-size: 20px;font-style: normal;font-weight: bold;line-height: 125%;letter-spacing: normal;"><span style="color:#FFF0F5"><?php //if($lang == "ar") {echo App\Http\Controllers\DashboardController::platformTranslateKeysForLanguage('lbl-estat-monthactivity', $lang) . ' ' . str_replace($months, $amonths, date("F", strtotime("first day of previous month"))); } else {echo date("F", strtotime("first day of previous month")). ' ' . App\Http\Controllers\DashboardController::platformTranslateKeysForLanguage('lbl-estat-monthactivity', $lang);}?></span></h2>-->
<div style="text-align:<?php echo $align;?>;line-height:1.3"><span style="font-size:11px"><?php echo App\Http\Controllers\DashboardController::platformTranslateKeysForLanguage('lbl-estat-period', strtolower($lang));?></span><br>
<?php if (strtolower($lang) == "ar") {
    print str_replace($standard, $arabic, date("m/Y", strtotime("first day of previous month")));
} else {
    print date("m/Y", strtotime("first day of previous month"));
}
?><br>
<hr style="border: 0 none;  border-top: 1px dashed #fff;  background: none;  height:0;padding:0;margin:5px 0" />
<div style="text-align:<?php echo $align;?>;"><span style="font-size:11px"><?php echo App\Http\Controllers\DashboardController::platformTranslateKeysForLanguage('lbl-estat-openingbalance', strtolower($lang));?></span><br>
<?php if (strtolower($lang) == "ar") {
    print str_replace($standard, $arabic, number_format($user['opening_balance'],0));
} else {
    print number_format($user['opening_balance'],0);
}
?><br>
<hr style="border: 0 none;  border-top: 1px dashed #fff;  background: none;  height:0;padding:0;margin:5px 0" />
<span style="font-size:11px"><?php echo App\Http\Controllers\DashboardController::platformTranslateKeysForLanguage('lbl-estat-pointsearned', strtolower($lang));?></span><br>
<?php if (strtolower($lang) == "ar") {
    print str_replace($standard, $arabic, number_format($user['sum_rewarded_points'],0));
} else {
    print number_format($user['sum_rewarded_points'],0);
}
?><br>
<hr style="border: 0 none;  border-top: 1px dashed #fff;  background: none;  height:0;padding:0;margin:5px 0" />
<span style="font-size:11px"><?php echo App\Http\Controllers\DashboardController::platformTranslateKeysForLanguage('lbl-estat-pointsredeemed', strtolower($lang));?></span><br>
<?php if (strtolower($lang) == "ar") {
    print str_replace($standard, $arabic, number_format($user['sum_redeemed_points'],0));
} else {
    print number_format($user['sum_redeemed_points'],0);
}
?><br>
<hr style="border: 0 none;  border-top: 1px dashed #fff;  background: none;  height:0;padding:0;margin:5px 0" />
<span style="font-size:11px"><?php echo App\Http\Controllers\DashboardController::platformTranslateKeysForLanguage('lbl-estat-pointsexpired', strtolower($lang)) . ' '; if(strtolower($lang) == "ar"){ echo str_replace($months, $amonths, date("F", strtotime("first day of previous month")));} else { echo date("F", strtotime("first day of previous month"));}?></span><br>
<?php if (strtolower($lang) == "ar") {
    print str_replace($standard, $arabic, number_format($user['sum_expired_points'],0));
} else {
    print number_format($user['sum_expired_points'],0);
}
?><br>
<hr style="border: 0 none;  border-top: 1px dashed #fff;  background: none;  height:0;padding:0;margin:5px 0" />
<span style="font-size:11px"><?php echo App\Http\Controllers\DashboardController::platformTranslateKeysForLanguage('lbl-estat-nextexpiry', strtolower($lang));?></span><br>
<?php 
if(strtolower($lang) == "ar"){
    echo str_replace('January 1st', '١ يناير', $user['next_expiry']) . ' ' . str_replace($standard, $arabic, date('Y', strtotime('+1 year')));
}else{
    echo $user['next_expiry'] . ' ' . date('Y', strtotime('+1 year')); 
}
?><br>
<?php if (strtolower($lang) == "ar") {
    print str_replace($standard, $arabic, number_format($user['sum_expiring_points'],0));
} else {
    print number_format($user['sum_expiring_points'],0);
}
?><br>
</div>

                                    </td>
                                </tr>
                            </tbody></table>
                        </td>
                    </tr>
                </tbody></table>
				<!--[if gte mso 9]>
				</td>
				<![endif]-->
                
				<!--[if gte mso 9]>
                </tr>
                </table>
				<![endif]-->
            </td>
        </tr>
    </tbody>
</table></td>
													</tr>
												</tbody></table>
												<!--[if gte mso 9]>
												</td>
												</tr>
												</table>
												<![endif]-->
											</td>
										</tr>
									</tbody></table>
								</td>
							</tr>
							<tr>
								<td valign="middle" id="templateFooter" style="background:#FAFAFA none no-repeat center/cover;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #e7e7e8;background-image: none;background-repeat: no-repeat;background-position: center;background-size: cover;border-bottom: 0;padding-top: 0;padding-bottom: 10px;">
        <?php
    $itemTitle;
    $itemBody;
    $itemImage;
    foreach ($items as $item) {
        if($item->rank != 1){
            if($item->type == 'product'){
                $product = App\Product::find($item->item_id);
                if(strtolower($lang) != 'en'){
                    $productTranslation = Prodtranslation::where('product_id', $product->id)->where('lang_id', 2)->first();
                    if($productTranslation){
                        $product->name = $productTranslation->name;
                        $product->model = $productTranslation->model;
                        $product->sub_model = $productTranslation->sub_model;
                        $product->description = $productTranslation->description;
                    }
                }
                $itemTitle = $product->name;
                $itemBody = $product->description;
                $itemImage = $product->cover_image;
            }else{
                $offer = App\Offer::find($item->item_id);
                if(strtolower($lang) != 'en'){
                    $offerTranslation = Offertranslation::where('offer_id', $offer->id)->where('lang_id', 2)->first();
                    if($offerTranslation){
                        $offer->name = $offerTranslation->name;
                        $offer->short_description = $offerTranslation->short_description;
                        $offer->description = $offerTranslation->description;
                        $offer->terms_and_condition = $offerTranslation->terms_and_condition;
                        $offer->email_text_promo_1 = $offerTranslation->email_text_promo_1;
                        $offer->email_text_promo_2 = $offerTranslation->email_text_promo_2;
                    }
                }
                $itemTitle = $offer->name;
                $itemBody = $offer->email_text_promo_2;
                $itemImage = $offer->cover_image;
            }?>
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width: 100%;border-collapse: separate; mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;margin-top:10px; background-color:#ffffff; border: 1px solid #75cdd9; -webkit-border-radius: 10px;-moz-border-radius: 10px;border-radius: 10px;">
            <tbody class="mcnTextBlockOuter" style="padding:5px;">
                <tr>
                <td valign="middle" class="mcnTextBlockInner" style="padding-top: 10px; padding-bottom: 10px; mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                    <!--[if mso]>
                                    <table align="<?php echo $align;?>"border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                                    <tr>
                                    <![endif]-->

                                    <!--[if mso]>
                                    <td valign="middle" width="210" style="width:210px;">
                                    <![endif]-->
                    <table align="<?php echo $align;?>"border="0" cellpadding="0" cellspacing="0" style="width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" width="100%" class="mcnTextContentContainer">
                        <tbody><tr>

                            <td valign="middle" class="mcnTextContent" style="width:194px; padding-top: 0;padding-<?php echo $align;?>: 5px; padding-<?php echo $otheralign;?>: 5px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #656565;font-family: Helvetica;font-size: 12px;line-height: 150%;text-align: center;<?php if(strtolower($lang) == "ar") {echo "border-left: 1px solid #75cdd9";} else {echo "border-right:1px solid #75cdd9";}?>">
                                <img align="none" height="125" src="<?php print $image_url ;?>/media/image/<?php print $itemImage;?>" alt="<?php print $itemTitle;?>" style="width: 165px;height: 125px;margin: 0px; border: 0;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" width="165">
                            </td>
                                
                            <td valign="middle" class="mcnTextContent" style="padding: 0 30px; mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #656565;font-family: Helvetica;font-size: 12px;line-height: 150%;text-align: <?php echo $align;?>;">
                                <p style="text-align:<?php echo $align;?>;font-weight:bold; font-size: 13px; margin:0 0 5px 0;"><?php print $itemTitle;?></p><p style="margin:0; text-align:<?php echo $align;?>;"><?php print strip_tags($itemBody);?></p>
                            </td>
                        </tr>
                    </tbody></table>
                                    <!--[if mso]>
                                    </td>
                                    <![endif]-->

                                    <!--[if mso]>
                                    </tr>
                                    </table>
                                    <![endif]-->
                </td>
            </tr>
            </tbody>
            </table>
            <?php } else {
                continue;
            }
        }?>
    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="middle" class="mcnTextBlockInner" style="padding-top: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
              	<!--[if mso]>
				<table align="<?php echo $align;?>"border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]-->
			    
				<!--[if mso]>
				<td valign="middle" width="600" style="width:600px;">
				<![endif]-->
                <table align="<?php echo $align;?>"border="0" cellpadding="0" cellspacing="0" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" width="100%" class="mcnTextContentContainer">
                    <tbody><tr>
                        
                        <td valign="middle" class="mcnTextContent" style="padding-top: 0;padding-<?php echo $otheralign;?>: 5px;padding-bottom: 9px;padding-<?php echo $align;?>: 5px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #656565;font-family: Helvetica;font-size: 12px;line-height: 150%;text-align: center;">
                        
                            <em>Copyright 2017&nbsp;</em>Banque Saudi Fransi.<em>&nbsp;All rights reserved.</em>
                        </td>
                    </tr>
                </tbody></table>
				<!--[if mso]>
				</td>
				<![endif]-->
                
				<!--[if mso]>
				</tr>
				</table>
				<![endif]-->
            </td>
        </tr>
    </tbody>
</table></td>
							</tr>
						</tbody></table>
						<!--[if gte mso 9]>
						</td>
						</tr>
						</table>
						<![endif]-->
						<!-- // END TEMPLATE -->
                    </td>
                </tr>
            </tbody></table>
        </center>
                <style type="text/css">
                    /*@media only screen and (max-width: 480px){
                        table#canspamBar td{font-size:14px !important;}
                        table#canspamBar td a{display:block !important; margin-top:10px !important;}
                    }*/
                </style>
</body></html>
