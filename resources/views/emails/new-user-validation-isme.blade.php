<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/xhtml" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">
  <head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html;  charset=UTF-8" />
    <title>isme by Jumeirah Validation Code</title>
  </head>
  <body bgcolor="#dedede" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%; margin: 0; padding: 0;">

<table style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; width: 100%; margin: 0; padding: 20px;"><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"></td>
		<td bgcolor="#FFFFFF" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto; padding: 20px; border: 1px solid #f0f0f0;">


			<div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; max-width: 600px; display: block; margin: 0 auto; padding: 0;">
			<table style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; width: 100%; margin: 0; padding: 0;"><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">

<p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0; padding: 15px 0 5px;">
        <?php list($width, $height) = getimagesize("https://bluai.com/media/image/" . $emailPartnerData['image']['en']);
            $imageWidth = 200;
            if($height > $width / 2){
                $imageWidth = 100;
            }
        ?>
	<img src="https://bluai.com/media/image/<?php echo $emailPartnerData['image']['en']?>" alt="<?php echo $emailPartnerData['partner_name_content']['en']?>" style=" font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; <?php echo "width:". $imageWidth ." px"?>; margin: 0; padding: 0; float: left !important;" /></p>
	<br/>
	<br/>
	<br/>
	<br/>
        <p style="text-align: center;" align="center"><h2 style="font-family: 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; font-size: 28px; line-height: 1.2; color: #000; font-weight: 200; margin: 0; padding: 0 0 40px;">Enrolment Validation Code</h2></p>

        <table cellpadding="0" cellspacing="0" width="100%" style="table-layout:fixed">
        <tbody>
        <tr>
        <td style="padding:10px 15px;border:0px transparent">
        <p>
        Hello,</p>
        <p>
        This is your <em>isme by Jumeirah</em> validation code: <?php echo ($user->otp) ? $user->otp : $user->passcode;?>. Please enter this code in the App to continue and complete your enrolment as a new member of
        <em>isme by Jumeirah</em>.</p>
        <p>
        We hope that you will find your membership a fun and enriching experience. Go ahead and explore the wide range of exclusive benefits and offers that are now available to you!</p>
        <p>
        We look forward to seeing you soon!</p>
        <p>
        Warmest regards,</p>
        <br>
        <img alt="" src="https://bluai.com/img/isme/vickysig.png"><br>
        <p style="color:#000000;font-family:Arial,Helvetica,sans-serif;font-size:14px;font-weight:bold">
        Vicky Elliot<br>
        <em>Group Director of Brand Loyalty</em></p>
        <br>
        <p>
        For more information, please call us on 800 - isme (4763) or visit <a href="https://isme.jumeirah.com/" style="color:#7f7f7f" id="m_-3961445062579499844LPlnk610488" target="_blank">
        isme.jumeirah.com</a></p>
        <div id="m_-3961445062579499844LPBorder_GT_15036411195150.5280366067640767" style="margin-bottom:20px;overflow:auto;width:100%;text-indent:0px">
        <table id="m_-3961445062579499844LPContainer_15036411194940.5984201454531055" role="presentation" cellspacing="0" style="width:90%;background-color:rgb(255,255,255);overflow:auto;padding-top:20px;padding-bottom:20px;margin-top:20px;border-top:1px dotted rgb(200,200,200);border-bottom:1px dotted rgb(200,200,200)">
        <tbody>
        <tr valign="top" style="border-spacing:0px">
        <td id="m_-3961445062579499844TextCell_15036411195070.6516121188115738" colspan="2" style="vertical-align:top;padding:0px;display:table-cell">
        <div id="m_-3961445062579499844LPRemovePreviewContainer_15036411195080.21157767016360007"></div>
        <div id="m_-3961445062579499844LPTitle_15036411195080.2559878447466615" style="color:rgb(130,186,0);font-weight:normal;font-size:21px;font-family:wf_segoe-ui_light,'Segoe UI Light','Segoe WP Light','Segoe UI','Segoe WP',Tahoma,Arial,sans-serif;line-height:21px">
        <a id="m_-3961445062579499844LPUrlAnchor_15036411195100.4533183084870933" href="https://isme.jumeirah.com/" style="text-decoration:none" target="_blank">ISME</a></div>
        <div id="m_-3961445062579499844LPMetadata_15036411195110.3109263579618984" style="margin:10px 0px 16px;color:rgb(102,102,102);font-weight:normal;font-family:wf_segoe-ui_normal,'Segoe UI','Segoe WP',Tahoma,Arial,sans-serif;font-size:14px;line-height:14px">
        <a href="http://isme.jumeirah.com" target="_blank">isme.jumeirah.com</a></div>
        <div id="m_-3961445062579499844LPDescription_15036411195130.9510039232026735" style="display:block;color:rgb(102,102,102);font-weight:normal;font-family:wf_segoe-ui_normal,'Segoe UI','Segoe WP',Tahoma,Arial,sans-serif;font-size:14px;line-height:20px;max-height:100px;overflow:hidden">
        Complimentary to download, your isme by Jumeirah™ membership will put your dream lifestyle in your hands with exciting benefits, savings and rewards at all JUMEIRAH ...</div>
        </td>
        </tr>
        </tbody>
        </table>
        </div>
        <br>
        </td>
        </tr>
        <tr>
        <td align="center">
        <table width="95%" align="center" style="border:1px solid #d8d8d8">
        <tbody>
        <tr>
        <td align="right" valign="middle" width="50%">
        <p style="font-size:10px;color:#7f7f7f;font-family:Verdana,Geneva,sans-serif;line-height:18px;text-align:right">
        Follow Us &nbsp; &nbsp;</p>
        </td>
        <td align="left" valign="middle" width="50%"><a href="https://www.facebook.com/ismebyJumeirah" target="_blank""><img alt="" src="https://bluai.com/img/isme/facebook-icon%5B2%5D.png"></a>
        <a href="https://twitter.com/ismebyjumeirah" target="_blank"><img alt="" src="https://bluai.com/img/isme/twitter-icon%5B2%5D.png"></a>
        <a href="https://www.instagram.com/ismebyjumeirah/" target="_blank"><img alt="" src="https://bluai.com/img/isme/instagram-icon%5B2%5D.png"></a></td>
        </tr>
        </tbody>
        </table>
        </td>
        </tr>
        <tr>
        <td style="padding:10px 15px;border:0px transparent">
        <p style="text-align:center;font-size:9px;color:#7f7f7f;font-family:Verdana,Geneva,sans-serif;line-height:18px">
        Jumeirah Hotels &amp; Resorts reserves all rights to change discounts and terms and conditions. Restaurant bookings and vouchers are subject to availability. In case any of your contact details change, kindly inform us or update them on your
        <em>isme by Jumeirah</em> App.</p>
        </td>
        </tr>
        <tr>
        <td bgcolor="#000000" style="padding:10px 19px">
        <p style="font-size:10px;color:#ffffff;font-family:Verdana,Geneva,sans-serif;line-height:18px">
        The information in this email is private and confidential. It is intended only for the use of the person(s) named. Your email address has been recorded as you are a registered member of the
        <em>isme by Jumeirah</em> programme. If you are not the intended recipient, you are notified that any dissemination or copying of this communication is prohibited and kindly request to notify the sender and then to delete this message. Jumeirah International
         LLC gives no representation or guarantee with respect to the integrity of any emails or attached files. Please do not reply to this email as it is an automated email and cannot be responded to. You can contact us at:
        <a href="mailto:info@isme.jumeirah.com" style="color:#ffffff" target="_blank">info@isme.jumeirah.com</a> or by calling 800 - isme (4763) within the United Arab Emirates or 00-971-800–4763 for international calls. Jumeirah Group, Academic City Road, P.O. Box 214159, Dubai,
         United Arab Emirates <a href="https://isme.jumeirah.com/" style="color:#ffffff" target="_blank">
        isme.jumeirah.com</a></p>
        </td>
        </tr>
        </tbody>
        </table>
						</td>
				</tr></table></div>

		</td>
		<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"></td>
	</tr></table><table style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; width: 100%; clear: both !important; margin: 0; padding: 0;"><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"></td>
		<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto; padding: 0;">
                    <a href="http://inside.jumeirah.com/" target="_blank"><img src="https://bluai.com/img/isme/jum.jpg"></a><br>
                        <h3>Disclaimer</h3><br>
                            <p>Jumeirah is a trade name of Jumeirah International LLC, a limited liability company incorporated in Dubai. Commercial Registration Number 57869. Share Capital Dhs. 300,000 fully paid up.
<br>
<br>
The information in this email is private &amp; confidential. It is intended only for the use of the person(s) named. If you are not the intended recipient, you are notified that any dissemination or copying of this communication is prohibited and kindly requested
 to notify the sender and to then delete this message. Jumeirah International LLC gives no representation or guarantee with respect to the integrity of any emails or attached files and the recipient should check the integrity of and scan this email and any
 attached files for viruses prior to opening. </p>
                    
		</td>
		<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"></td>
	</tr></table></body>
</html>
