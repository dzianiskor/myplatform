<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/xhtml" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">
  <head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html;  charset=UTF-8" />
    <title>Welcome to isme by Jumeirah</title>
  </head>
  <body bgcolor="#dedede" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%; margin: 0; padding: 0;">

<table style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; width: 100%; margin: 0; padding: 20px;"><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"></td>
		<td bgcolor="#FFFFFF" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto; padding: 20px; border: 1px solid #f0f0f0;">


			<div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; max-width: 600px; display: block; margin: 0 auto; padding: 0;">
			<table style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; width: 100%; margin: 0; padding: 0;"><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">

<p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0; padding: 15px 0 5px;">
        <?php list($width, $height) = getimagesize("https://bluai.com/media/image/" . $emailPartnerData['image']['en']);
            $imageWidth = 200;
            if($height > $width / 2){
                $imageWidth = 100;
            }
        ?>
	<img src="https://bluai.com/media/image/<?php echo $emailPartnerData['image']['en']?>" alt="<?php echo $emailPartnerData['partner_name_content']['en']?>" style=" font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; <?php echo "width:". $imageWidth ." px"?>; margin: 0; padding: 0; float: left !important;" /></p>
	<br/>
	<br/>
	<br/>
	<br/>
        <p style="text-align: center;" align="center"><h2 style="font-family: 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; font-size: 28px; line-height: 1.2; color: #000; font-weight: 200; margin: 0; padding: 0 0 40px;">Welcome to isme by Jumeirah</h2></p>

        <table cellpadding="0" cellspacing="0" width="100%" style="table-layout:fixed">
        <tbody>
        <tr>
        <td style="padding:10px 15px;border:0px transparent">
        <p>
        Dear <?php echo $user->first_name . " " . $user->last_name;?>,</p>
        <p>
        Welcome to fabulous lifestyle at your fingertips! If you haven<span style="color:rgb(127,127,127);font-family:Verdana,Geneva,sans-serif;font-size:12px;text-align:justify;background-color:rgb(255,255,255)">`</span>t already, please
        <a href="https://goo.gl/Bw56mt" target="_blank">download the app here.</a></p>
        <p>
        These are your login details:</p>
        <p>
        Member Number: <?php echo $user->normalized_mobile;?></p>
        <p>
        PIN: <?php echo $user->passcode;?></p>
        <p>
        Once you log into the <em>isme by Jumeirah</em> App, you will be able to instantly redeem sought-after rewards in over 90 participating venues at Jumeirah`s UAE resorts; Burj Al Arab Jumeirah, Jumeirah Beach Hotel, Madinat Jumeirah, Jumeirah Creekside Hotel,
         Jumeirah Zabeel Saray, Jumeirah Emirates Towers and Jumeirah at Etihad Towers!</p>
        <p>
        Already a Jumeirah Sirius member? Simply enter your Jumeirah Sirius membership number into your
        <em>isme by Jumeirah</em> profile in the App to continue collecting Jumeirah Sirius Points on all your activity through the App.</p>
        <p>
        We look forward to seeing you soon!</p>
        <p>
        Warmest regards,</p>
        <br>
        <img alt="" src="https://bluai.com/img/isme/vickysig.png"><br>
        <p style="color:#000000;font-family:Arial,Helvetica,sans-serif;font-size:14px;font-weight:bold">
        Vicky Elliot<br>
        <em>Group Director of Brand Loyalty</em><br>
        </p>
        <p>
        For more information, please call us on 800 - isme (4763) or visit <a href="https://isme.jumeirah.com/" style="color:#7f7f7f" target="_blank">
        isme.jumeirah.com</a></p>
        </td>
        </tr>
        <tr>
        <td align="center">
        <table width="95%" align="center" style="border:1px solid #d8d8d8">
        <tbody>
        <tr>
        <td align="right" valign="middle" width="50%">
        <p style="font-size:10px;color:#7f7f7f;font-family:Verdana,Geneva,sans-serif;line-height:18px;text-align:right">
        Follow Us &nbsp; &nbsp;</p>
        </td>
        <td align="left" valign="middle" width="50%"><a href="https://www.facebook.com/ismebyJumeirah" target="_blank""><img alt="" src="https://bluai.com/img/isme/facebook-icon%5B2%5D.png"></a>
        <a href="https://twitter.com/ismebyjumeirah" target="_blank"><img alt="" src="https://bluai.com/img/isme/twitter-icon%5B2%5D.png"></a>
        <a href="https://www.instagram.com/ismebyjumeirah/" target="_blank"><img alt="" src="https://bluai.com/img/isme/instagram-icon%5B2%5D.png"></a></td>
        </tr>
        </tbody>
        </table>
        </td>
        </tr>
        <tr>
        <td style="padding:10px 15px;border:0px transparent">
        <p style="text-align:center;font-size:9px;color:#7f7f7f;font-family:Verdana,Geneva,sans-serif;line-height:18px">
        Jumeirah Hotels &amp; Resorts reserves all rights to change discounts and terms and conditions. Restaurant bookings and vouchers are subject to availability. In case any of your contact details change, kindly inform us or update them on your
        <em>isme by Jumeirah</em> App.</p>
        </td>
        </tr>
        <tr>
        <td>
        <p style="text-align:center"><a href="https://itunes.apple.com/ae/app/isme-by-jumeirah/id1095761971?mt=8" target="_blank"><img alt="" src="https://bluai.com/img/isme/Appstore.png"></a> &nbsp; &nbsp;
        <a href="https://play.google.com/store/apps/details?id=com.jumeirah.isme" target="_blank"><img alt="" src="https://bluai.com/img/isme/GoogleStore.png"></a></p>
        </td>
        </tr>
        <tr>
        <td bgcolor="#000000" style="padding:10px 19px">
        <p style="font-size:10px;color:#ffffff;font-family:Verdana,Geneva,sans-serif;line-height:18px">
        The information in this email is private and confidential. It is intended only for the use of the person(s) named. Your email address has been recorded as you are a registered member of the
        <em>isme by Jumeirah</em> programme. If you are not the intended recipient, you are notified that any dissemination or copying of this communication is prohibited and kindly request to notify the sender and then to delete this message. Jumeirah International
         LLC gives no representation or guarantee with respect to the integrity of any emails or attached files. Please do not reply to this email as it is an automated email and cannot be responded to. You can contact us at:
        <a href="mailto:info@isme.jumeirah.com" style="color:#ffffff" target="_blank">info@isme.jumeirah.com</a> or by calling 800 - isme (4763) within the United Arab Emirates or 00-971-800–4763 for international calls. Jumeirah Group, Academic City Road, P.O. Box 214159, Dubai,
         United Arab Emirates <a href="https://isme.jumeirah.com/" style="color:#ffffff" target="_blank">
        isme.jumeirah.com</a></p>
        </td>
        </tr>
        </tbody>
        </table>
						</td>
				</tr></table></div>

		</td>
		<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"></td>
	</tr></table><table style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; width: 100%; clear: both !important; margin: 0; padding: 0;"><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"></td>
		<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto; padding: 0;">
                    <a href="http://inside.jumeirah.com/" target="_blank"><img src="https://bluai.com/img/isme/jum.jpg"></a><br>
                        <h3>Disclaimer</h3><br>
                            <p>Jumeirah is a trade name of Jumeirah International LLC, a limited liability company incorporated in Dubai. Commercial Registration Number 57869. Share Capital Dhs. 300,000 fully paid up.
<br>
<br>
The information in this email is private &amp; confidential. It is intended only for the use of the person(s) named. If you are not the intended recipient, you are notified that any dissemination or copying of this communication is prohibited and kindly requested
 to notify the sender and to then delete this message. Jumeirah International LLC gives no representation or guarantee with respect to the integrity of any emails or attached files and the recipient should check the integrity of and scan this email and any
 attached files for viruses prior to opening. </p>
                    
		</td>
		<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"></td>
	</tr></table></body>
</html>
