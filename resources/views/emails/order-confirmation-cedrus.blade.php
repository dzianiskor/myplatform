<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/xhtml" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">
  <head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>CEDRUS Order Confirmation</title>
  </head>
  <body bgcolor="#dedede" style="background-color: #dedede; font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%; margin: 0; padding: 0;">


<!-- body -->
<table style="background-color: #dedede;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; width: 100%; margin: 0; padding: 20px;"><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"></td>
		<td bgcolor="#FFFFFF" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto; padding: 20px; border: 1px solid #f0f0f0;">

			<!-- content -->
			<div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; max-width: 600px; display: block; margin: 0 auto; padding: 0;">
			<table style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; width: 600px; margin: 0; padding: 0;"><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">
<p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal;  margin: 0; padding: 30px 0 5px;">
	<img src="{{ asset('img/email-logo-cedrus-bank.png') }}" alt="Cedrus Bank" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; max-width: 100%; margin: 0; padding: 0;" /></p>
	<br>

	<?php
		if (strtolower($transaction->trans_class) == "reversal") {
			$transId		= $transaction->ref_number;
			$transaction2	= Transaction::find($transId);

			$emailTitle = "Reversal Transaction Order Summary (#" . $transaction->id . ")";
		}
		else if (strtolower($transaction->trans_class) == "car") {
			$emailTitle = "Car Rental Booking Redemption Summary " . ($transaction->cash_payment > 0 ? " & Receipt" : "") . " (#" . $transaction->id . ")";
		}
		else if (strtolower($transaction->trans_class) == "flight") {
			$emailTitle = "Flight Booking Redemption Summary " . ($transaction->cash_payment > 0 ? " & Receipt" : "") . " (#" . $transaction->id . ")";
		}
		else if (strtolower($transaction->trans_class) == "hotel") {
			$emailTitle = "Hotel Booking Redemption Summary " . ($transaction->cash_payment > 0 ? " & Receipt" : "") . " (#" . $transaction->id . ")";
		}
		else if ($transaction->trans_class == "Points") {
			$emailTitle = "Transfer Order Summary " . " (#" . $transaction->id . ")";
		}
		else if ($transaction->trans_class == "Amount") {
			$emailTitle = "In Store Transaction Summary " . " (#" . $transaction->id . ")";
		}
		else {
			$emailTitle = "Redemption Order Summary " . ($transaction->cash_payment > 0 ? " & Receipt" : "") . " (#" . $transaction->id . ")";
		}
		?>

		<h2 style="font-family: 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; font-size: 28px; line-height: 1.2; color: #000; font-weight: 200; margin: 0; padding: 0 0 40px;"><?php echo $emailTitle ?></h2>
                                  
						<p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
						
							Dear <strong style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">Admin</strong>,
						</p>

						@if(strtolower($transaction->trans_class) == "reversal")
                                                    <p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
								   This is an automated reversal transaction order confirmation for transaction #{{ $transaction2->id }}:<br/><br/>
							</p>
						@else
							<p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
                                                           This is an automated customer order confirmation for the following transaction:<br/><br/>
                                                    </p>
						@endif

						<div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 10px; width: 100%; margin: 0; padding: 0;"></div>

						<div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 5px; width: 100%; background-color: #35aeea; margin: 10px 0; padding: 0;"></div>

						<div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 10px; width: 100%; margin: 0; padding: 0;"></div>

						<p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
							<strong style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">Customer Details:</strong>
						</p>

						<table style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; width: 100%; margin: 0; padding: 0;"><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">Full Name: {{ $user->name }}</td>
							</tr><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">Mobile: {{ $user->normalizedContactNumber() }}</td>
							</tr><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">Address Line 1: {{ $user->address_1 }}</td>
							</tr><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">Address Line 2: {{ $user->address_2 }}</td>
							</tr><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">
								@if($user->city)
									<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">City: {{ $user->city->name }}</td>
								@else
									<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">City: N/A</td>
								@endif
                                                        </tr><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">
                                                                @if($user->area)
									<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">State: {{ $user->area->name }}</td>
								@else
									<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">State: N/A</td>
								@endif
							</tr><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">
								@if($user->country)
									<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">Country: {{ $user->country->name }}</td>
								@else
									<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">Country: N/A</td>
								@endif
							</tr>
                                                    <tr>
                                                            <td colspan="2" style="vertical-align: top;">
                                                            @if(strtolower($transaction->trans_class) == "flight" || (isset($revTransaction) && strtolower($revTransaction->trans_class) == "flight"))
                                                                <?php 
                                                                    $notes = $transaction->notes;
                                                                    $json_notes = json_decode($notes);
                                                                ?>
                                                                <?php if(isset($json_notes->first_name) && count($json_notes->first_name) > 0):?>
                                                                <p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
                                                                        <strong style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">Passenger Details:</strong>
                                                                </p>
                                                                <table style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; width: 100%; margin: 0; padding: 0;">
                                                                    <?php
                                                                    $count = count($json_notes->first_name);
                                                                    for($i = 0; $i < $count; $i++){
                                                                    ?>
                                                                    <tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">Title: <?php echo $json_notes->title[$i]; ?></td></tr>
                                                                    <tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">Name: <?php echo $json_notes->first_name[$i] . ' ' . $json_notes->last_name[$i]; ?></td></tr>
                                                                    <tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">Mobile: <?php echo $json_notes->mobile[$i]; ?></td></tr>
                                                                    <tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">Email: <div style="text-align: left; margin: 0; padding: 0;"><?php echo $json_notes->email[$i]; ?></div></td></tr>
                                                                    <tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">Date of Birth: <?php echo $json_notes->dob_day[$i] . '-' . $json_notes->dob_month[$i] . '-' .$json_notes->dob_year[$i]; ?></td></tr>
                                                                    <tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">Nationality: <?php echo str_replace("_", " ", $json_notes->nationality[$i]); ?></td></tr>
                                                                    <tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">Passport Number: <?php echo $json_notes->passport[$i]; ?></td></tr>
                                                                    <tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">Passport Expiry Date: <?php echo $json_notes->passportexpiry_day[$i] . '-' . $json_notes->passportexpiry_month[$i] .'-' .$json_notes->passportexpiry_year[$i]; ?></td></tr>
                                                                    <?php } ?>
                                                                </table>
                                                                <?php endif;?>
                                                            @endif
                                                            
                                                        </td>
                                                    </tr>
							<?php
							if(strtolower($transaction->trans_class) == "reversal"){
								?>
							<tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">
									Reversed By: {{ Auth::user()->name }}
								</td>
							</tr>
							<?php } ?>
						</table><div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 10px; width: 100%; margin: 0; padding: 0;"></div>

                                                       <?php
								if(strtolower($transaction->trans_class) == "reversal"){
//									$reversedTransaction	= $transaction;
									$transId				= $transaction->ref_number;
									$transaction	= Transaction::find($transId);
								}

                                                        if(strtolower($transaction->trans_class) == "flight"){
                                                            $notes = $transaction->notes;
                                                            $json_notes = json_decode($notes);
                                                            if(isset($json_notes)){
                                                                $o_carrier_name = $json_notes->out_carrier_name;
                                                                $o_depart_time = $json_notes->out_depart_time;
                                                                $o_arrive_time = $json_notes->out_arrive_time;
                                                                $o_depart_station_name = $json_notes->out_depart_station_name;
                                                                $o_arrive_station_name = $json_notes->out_arrive_station_name;
                                                                $o_depart_station_code = $json_notes->out_depart_station_code;
                                                                $o_arrive_station_code = $json_notes->out_arrive_station_code;
                                                                $o_duration = $json_notes->out_duration;
                                                                $o_stops = $json_notes->out_stops;
                                                                $i_carrier_name = $json_notes->in_carrier_name;
                                                                $i_depart_time = $json_notes->in_depart_time;
                                                                $i_arrive_time = $json_notes->in_arrive_time;
                                                                $i_depart_station_name = $json_notes->in_depart_station_name;
                                                                $i_arrive_station_name = $json_notes->in_arrive_station_name;
                                                                $i_depart_station_code = $json_notes->in_depart_station_code;
                                                                $i_arrive_station_code = $json_notes->in_arrive_station_code;
                                                                $i_duration = $json_notes->in_duration;
                                                                $i_stops = $json_notes->in_stops;
                                                                $return = -1;
                                                                if(isset($json_notes->return)){
                                                                    $return = $json_notes->return;
                                                                }
                                                                if(isset($json_notes->cabinclass)){
                                                                    $cabin_class = $json_notes->cabinclass;
                                                                }
                                                            }
                                                        }
                                                        ?>

						<div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 5px; width: 100%; background-color: #35aeea; margin: 10px 0; padding: 0;"></div>
                                                <p align="center"><strong style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 140%; line-height: 1.6; margin: 0; padding: 0;"><?php  if( isset($cabin_class) ){ echo " Cabin Class: ".$cabin_class; } ?></strong></p>
						<div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 10px; width: 100%; margin: 0; padding: 0;"></div>

						@if($transaction->trans_class == "Items")
							<p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
								<strong style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">Order Details:</strong>
							</p>

							<table border="0" cellspacing="0" cellpadding="0" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; width: 100%; margin: 0; padding: 0;"><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; font-weight: bold; color: #fff; text-align: center; background-color: #58595B; margin: 0; padding: 5px;" align="center" bgcolor="#58595B">Item</td>
                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; font-weight: bold; color: #fff; text-align: center; background-color: #58595B; margin: 0; padding: 5px;" align="center" bgcolor="#58595B">Description</td>
									<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; font-weight: bold; color: #fff; text-align: center; background-color: #58595B; margin: 0; padding: 5px;" align="center" bgcolor="#58595B">Qty</td>
									<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; font-weight: bold; color: #fff; text-align: center; background-color: #58595B; margin: 0; padding: 5px;" align="center" bgcolor="#58595B">Total Price</td>

								</tr>

								@foreach($transaction->items as $i)
                                                                <?php $img_src = "http://www.bluai.com/media/image/" . $i->product->cover_image;?>
								<tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin: 0; padding: 10px 5px;" align="center"><img src="<?php echo $img_src; ?>" width="100" /></td>
                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin: 0; padding: 10px 5px;" align="center">{{ $i->product->name }}</td>
									<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin: 0; padding: 10px 5px;" align="center">{{ $i->quantity }}</td>
									<?php
                                                                            $priceInPoints1	= PointsHelper::productPriceInPoints($i->product->price_in_points, $i->product->original_price, $i->product->currency_id, $transaction->network_id);
                                                                            $priceInPoints	= $priceInPoints1 + ($i->product->original_sales_tax * $priceInPoints1 / 100);
                                                                            $total_points	= ($priceInPoints * $i->quantity);
									?>
									<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin: 0; padding: 10px 5px;" align="center"><?php echo number_format(abs($i->price_in_points));?></td>

								</tr>
								@endforeach

								<tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td colspan="3" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; text-align: right; font-weight: bold; margin: 0; padding: 0 10px 0 0;" align="right"> Subtotal (Points): </td>
									<td colspan="1" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">
										{{ number_format($transaction->points_redeemed - $transaction->delivery_cost) }} Points
									</td>
								</tr><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td colspan="3" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; text-align: right; font-weight: bold; margin: 0; padding: 0 10px 0 0;" align="right">Shipping Charges: </td>
									<td colspan="1" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">
										{{ number_format($transaction->delivery_cost) }} Points
									</td>
								</tr><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td colspan="3" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; text-align: right; font-weight: bold; margin: 0; padding: 0 10px 0 0;" align="right">Total (Points):</td>
									<td colspan="1" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; font-weight: bold; margin: 0; padding: 0;">
										{{ number_format($transaction->points_redeemed) }} Points
									</td>
								</tr>
								@if( $transaction->cash_payment > 0)
								<tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td colspan="3" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; text-align: right; font-weight: bold; margin: 0; padding: 0 10px 0 0;" align="right">Total Amount Paid (USD):</td>
									<td colspan="1" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; font-weight: bold; margin: 0; padding: 0;">
										{{ $transaction->cash_payment }}
									</td>
								</tr>
								@endif
							</table>
						@endif

						@if($transaction->trans_class == "Amount")
							<p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
								<strong style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">Transaction Details:</strong>
							</p>

							<table border="0" cellspacing="0" cellpadding="0" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; width: 100%; margin: 0; padding: 0;"><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; font-weight: bold; color: #fff; text-align: center; background-color: #58595B; margin: 0; padding: 5px;" align="center" bgcolor="#58595B">Date</td>
									<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; font-weight: bold; color: #fff; text-align: center; background-color: #58595B; margin: 0; padding: 5px;" align="center" bgcolor="#58595B">Amount(USD Value)</td>
									<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; font-weight: bold; color: #fff; text-align: center; background-color: #58595B; margin: 0; padding: 5px;" align="center" bgcolor="#58595B">Points</td>
                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; font-weight: bold; color: #fff; text-align: center; background-color: #58595B; margin: 0; padding: 5px;" align="center" bgcolor="#58595B">Transaction</td>
								</tr><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin: 0; padding: 10px 5px;" align="center">{{ date("F j, Y, g:i a", strtotime($transaction->created_at)) }}</td>
									<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin: 0; padding: 10px 5px;" align="center">${{ number_format($transaction->total_amount, 2) }}</td>
									<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin: 0; padding: 10px 5px;" align="center">{{ number_format($transaction->points_redeemed) }}</td>
                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin: 0; padding: 10px 5px;" align="center">{{ $transaction->ref_number }}</td>
								</tr></table>
						@endif

						@if($transaction->trans_class == "Points")
							<p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
								<strong style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">Transaction Details:</strong>
							</p>

							<table border="0" cellspacing="0" cellpadding="0" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; width: 100%; margin: 0; padding: 0;"><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; font-weight: bold; color: #fff; text-align: center; background-color: #58595B; margin: 0; padding: 5px;" align="center" bgcolor="#58595B">Date</td>
									<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; font-weight: bold; color: #fff; text-align: center; background-color: #58595B; margin: 0; padding: 5px;" align="center" bgcolor="#58595B">Points</td>
									<!--<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; font-weight: bold; color: #fff; text-align: center; background-color: #58595B; margin: 0; padding: 5px;" align="center" bgcolor="#58595B">Value in USD</td>-->
								</tr><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin: 0; padding: 10px 5px;" align="center">{{ date("F j, Y, g:i a", strtotime($transaction->created_at)) }}</td>
									<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin: 0; padding: 10px 5px;" align="center">{{ number_format($transaction->points_redeemed) }}</td>
									<!--<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin: 0; padding: 10px 5px;" align="center">{{ number_format($transaction->total_amount) }}</td>-->
							</tr></table>
						@endif

						@if(strtolower($transaction->trans_class) == "flight")
							<table border="0" cellspacing="0" cellpadding="0" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; width: 100%; margin: 0; padding: 0;"><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px; font-weight: bold; color: #000000; text-align: center; background-color: #e0e0e0; margin: 0; padding: 5px;" align="center" bgcolor="#58595B"></td>
									<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; font-weight: bold; color: #000000; text-align: center; background-color: #e0e0e0; margin: 0; padding: 5px;" align="center" bgcolor="#58595B">Airline / Flight</td>
									<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; font-weight: bold; color: #000000; text-align: center; background-color: #e0e0e0; margin: 0; padding: 5px;" align="center" bgcolor="#58595B">Depart / Arrive</td>
                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; font-weight: bold; color: #000000; text-align: center; background-color: #e0e0e0; margin: 0; padding: 5px;" align="center" bgcolor="#58595B">City / Airport</td>
                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; font-weight: bold; color: #000000; text-align: center; background-color: #e0e0e0; margin: 0; padding: 5px;" align="center" bgcolor="#58595B">Duration / Stops </td>
                                                            </tr><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td rowspan="2" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px; text-align: center; margin: 0; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; " align="center"><img src="{{ asset('img/departBig.png') }}" width="100" /></td>
									<td rowspan="2" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; text-align: center; margin: 0; padding: 10px 5px;" align="center"><?php echo $o_carrier_name ?></td>
									<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; /*border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;*/border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px;/*border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px;*/ text-align: center; margin: 0; padding: 10px 5px 0px 5px;" align="center"><?php echo date("F j, Y, g:i a", strtotime($o_depart_time)) ?></td>

                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; /*border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px;*//*border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px;*/ text-align: center; margin: 0; padding: 10px 5px 0px 5px;" align="center"><?php echo $o_depart_station_name . " (" . $o_depart_station_code . ")" ?></td>

                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px;/*border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px;*/ text-align: center; margin: 0; padding: 10px 5px 0px 5px;" align="center"><?php echo floor($o_duration / 60)."H ".($o_duration % 60)."m" ?></td>

								</tr><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">
									<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; /*border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;*/border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin: 0; padding: 0px 5px 10px 5px;" align="center"><?php echo date("F j, Y, g:i a", strtotime($o_arrive_time)) ?></td>
                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; /*border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px;*/ border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin: 0; padding: 0px 5px 10px 5px;" align="center"><?php echo $o_arrive_station_name . " (" . $o_arrive_station_code . ")"?></td>
                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin: 0; padding: 0px 5px 10px 5px;" align="center"><?php echo $o_stops ?></td>

								</tr>
                                                            <?php  if($return == "1"){ ?>
                                                            <tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td rowspan="2" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px; text-align: center; margin: 0; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; " align="center"><img src="{{ asset('img/arrivalBig.png') }}" width="100" /></td>
									<td rowspan="2" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin: 0; padding: 10px 5px;" align="center"><?php echo $i_carrier_name ?></td>
									<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; /*border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;*/border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; /*border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px;*/ text-align: center; margin: 0; padding: 10px 5px 0px 5px;" align="center"><?php echo date("F j, Y, g:i a", strtotime($i_depart_time)) ?></td>

                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; /*border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; *//*border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px;*/ text-align: center; margin: 0; padding: 10px 5px 0px 5px;" align="center"><?php echo $i_depart_station_name . " (" . $i_depart_station_code . ")" ?></td>

                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; /*border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px;*/ text-align: center; margin: 0; padding: 10px 5px 0px 5px;" align="center"><?php echo floor($i_duration / 60)."H ".($i_duration % 60)."m" ?></td>

								</tr>
                                                            <tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">
									<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; /*border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;*/border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin: 0; padding: 0px 5px 10px 5px;" align="center"><?php echo date("F j, Y, g:i a", strtotime($i_arrive_time)) ?></td>
                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; /*border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px;*/ border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin: 0; padding: 0px 5px 10px 5px;" align="center"><?php echo $i_arrive_station_name . " (" . $i_arrive_station_code . ")"?></td>
                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin: 0; padding: 0px 5px 10px 5px;" align="center"><?php echo $i_stops ?></td>

								</tr>
                                                            <?php } ?>
                                                        </table>
                                                        @endif



						@if(strtolower($transaction->trans_class) == "hotel")
							<?php

                                                            $notes = $transaction->notes;
                                                            $json_notes = json_decode($notes);

                                                            if(isset($json_notes)):
                                                                $h_name = $json_notes->hotelName;
                                                                $h_address = $json_notes->hotelAddress1;
                                                                $h_arrivalDate = $json_notes->arrivalDate;
                                                                $h_departureDate = $json_notes->departureDate;
																$image_source = $transaction->getHotelImageUrl();
//                                                                $img_url_prefix = "http://images.trvl-media.com";
//                                                                $h_thumbnail = urldecode($json_notes->img);
//                                                                $h_img_small = str_replace("_t.jpg","_s.jpg",$h_thumbnail);
//                                                                $h_img = $img_url_prefix . $h_img_small;
                                                        ?>
                                                            <table border="0" cellspacing="0" cellpadding="0" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 0.8; width: 100%; margin: 0; padding: 0;">
                                                                <tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 0.2; margin: 0; padding: 0;">
                                                                    <td rowspan="6" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 0.2; text-align: center; margin: 0; " align="center"><img src="<?php echo $image_source; ?>" width="180" /></td>
									<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 0.2; text-align: left; margin: 0; padding: 10px 5px;" align="left"><strong>Customer Name: </strong>{{ $user->name }}</td>
								</tr><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 0.2; margin: 0; padding: 0;">
                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 0.2; text-align: left; margin: 0; padding: 10px 5px;" align="left"><strong>Customer Mobile: </strong>{{ $user->normalizedContactNumber() }}</td>
								</tr><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 0.2; margin: 0; padding: 0;">
                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 0.2; text-align: left; margin: 0; padding: 10px 5px;" align="left"><strong>Hotel Name: </strong><?php echo $h_name; ?></td>
								</tr><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 0.2; margin: 0; padding: 0;">
                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 0.2; text-align: left; margin: 0; padding: 10px 5px;" align="left"><strong>Hotel Address: </strong><?php echo $h_address; ?></td>
								</tr><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 0.2; margin: 0; padding: 0;">
                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 0.2; text-align: left; margin: 0; padding: 10px 5px;" align="left"><strong>Check in: </strong><?php echo $h_arrivalDate; ?></td>
								</tr><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 0.2; margin: 0; padding: 0;">
                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 0.2; text-align: left; margin: 0; padding: 10px 5px;" align="left"><strong>Check out: </strong><?php echo $h_departureDate; ?></td>
								</tr></table>
                                                        <?php endif; ?>
						@endif

						@if(strtolower($transaction->trans_class) == "car")
							<?php

                                                            $notes = $transaction->notes;
                                                            $json_notes = json_decode($notes);
                                                            if(isset($json_notes)):
                                                            $image_source = $json_notes->img;
                                                            $p_time = $json_notes->pickup;
                                                            $p_locName = $json_notes->pickup_location_name;
                                                            $d_time = $json_notes->dropoff;
                                                            $d_locName = $json_notes->dropoff_location_name;
                                                        ?>
                                                            <table border="0" cellspacing="0" cellpadding="0" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 0.8; width: 100%; margin: 0; padding: 0;">
                                                                <tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 0.2; margin: 0; padding: 0;">
                                                                    <td rowspan="3" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 0.2; text-align: center; margin: 0; " align="center"><img src="<?php echo $image_source; ?>" width="90" /></td>
									<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 0.2; text-align: left; margin: 0; padding: 10px 5px;" align="left"><strong>Vehicle Type: </strong><?php echo $json_notes->vehicle; ?></td>
								</tr><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 0.2; margin: 0; padding: 0;">
                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 0.2; text-align: left; margin: 0; padding: 10px 5px;" align="left"><strong>Pick up: </strong><?php echo $p_locName . " at " . $p_time; ?></td>
								</tr><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 0.2; margin: 0; padding: 0;">
                                                                        <td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 0.2; text-align: left; margin: 0; padding: 10px 5px;" align="left"><strong>Drop Off: </strong><?php echo $d_locName . " at " . $d_time; ?></td>
								</tr></table>
                                                        <?php endif; ?>
						@endif

						<div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 10px; width: 100%; margin: 0; padding: 0;"></div>

                                                @if((strtolower($transaction->trans_class) == "flight" or strtolower($transaction->trans_class) == "hotel" or strtolower($transaction->trans_class) == "car"))
                                                    <div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 5px; width: 100%; background-color: #35aeea; margin: 10px 0; padding: 0;"></div>

                                                    <p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; text-align: right; margin: 0 0 10px; padding: 0;">
                                                                <strong style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">Total Points:</strong>
                                                                {{number_format(abs($transaction->total_amount*100))}}
                                                    </p>
                                                    <p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; text-align: right; margin: 0 0 10px; padding: 0;">
                                                                <strong style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">Total Points Redeemed:</strong>
                                                                {{number_format(abs($transaction->points_redeemed))}}
                                                    </p>
                                                    <p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; text-align: right; margin: 0 0 10px; padding: 0;">
                                                                <strong style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">Total Amount Paid (USD):</strong>
                                                                {{number_format(abs($transaction->cash_payment),2)}}
                                                    </p>
						@endif

						<div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 10px; width: 100%; margin: 0; padding: 0;"></div>

						<div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; height: 10px; width: 100%; margin: 0; padding: 0;"></div>

						<table style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; width: 100%; margin: 0; padding: 0;"><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 11px; line-height: 1.6; text-align: center; margin: 0; padding: 0;" align="center">
									This email was sent from a notification-only email address that cannot accept incoming notifications. Please do not reply to this message.
									<br style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;" />
									Copyright <?php echo date('Y'); ?> Cedrus Bank s.a.l. Part of the BLU Points network.
								</td>
							</tr></table></td>
				</tr></table></div>
			<!-- /content -->

		</td>
		<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"></td>
	</tr></table><!-- /body --><!-- footer --><table style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; width: 100%; clear: both !important; margin: 0; padding: 0;"><tr style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"><td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"></td>
		<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto; padding: 0;">

			<!-- content -->
			<!-- /content -->

		</td>
		<td style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;"></td>
	</tr></table><!-- /footer --></body>
</html>
