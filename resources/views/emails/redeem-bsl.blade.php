<?php
	$style0		= "font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:100%; line-height:1.6;";
	$style1		= "font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:100%; line-height:1.6; margin:0; padding:0;";
	$style2		= "font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:100%; line-height:1.6; margin:0; padding:0; width:100%;";
	$style3		= "font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:14px; line-height:1.6; font-weight:normal; margin:0 0 10px; padding:0;";
	$style4		= "font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:100%; line-height:0.2; margin:0; padding:0;";
	$style5		= "font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:100%; line-height:0.2; margin:0; padding:10px 5px; text-align:left;";
	$style6		= "font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:100%; line-height:1.6; margin:0; padding:5px; font-weight:bold; color:#fff; text-align:center; background-color:#58595B;";
	$style7		= "font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:100%; line-height:1.6; margin:0; padding:10px 5px; border-bottom-style:solid; border-bottom-color:#d7d7d7; border-bottom-width:1px; text-align:center;";
	$style8		= "font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:100%; line-height:1.6; margin:0; padding:0 3% 0 0; text-align:right; font-weight:bold; ";
	$style9		= "font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:100%; line-height:1.6; margin:0; padding:5px; border-left-style:solid; border-left-color:#d7d7d7; border-left-width:1px; border-right-style:solid; border-right-color:#d7d7d7; border-right-width:1px; font-weight:bold; color:#000000; text-align:center; background-color:#e0e0e0;";
	$style10	= "font-family:'Helvetica Neue','Helvetica','Arial','Lucida Grande','sans-serif'; font-size:28px; line-height:1.2; color:#000; font-weight:200; margin:0; padding:0 0 40px;";

	$trxCurrency	= App\Currency::where("id", $transaction->currency_id)->first();
	$shortCode		= $trxCurrency->short_code;

	if ($shortCode == "BHD") {
		$arabic_currency		= "المبلغ الاجمالي المدفوع بالدينار البحريني";
		$arabic_currency_value	= "القيمة بالدينار البحريني";
	}
	elseif ($shortCode == "SAR") {
		$arabic_currency		= "المبلغ الاجمالي المدفوع بالريال سعودي";
		$arabic_currency_value	= "القيمة بالريال سعودي";
	}
	elseif ($shortCode == "JOD") {
		$arabic_currency		= "المبلغ الإجمالي المدفوع بالدينار الاردني";
		$arabic_currency_value	= "القيمة بالدينار الاردني";
	}
	else {
		$arabic_currency		= "المبلغ الاجمالي المدفوع بالدولار الأميركي";
		$arabic_currency_value	= "القيمة بالدولار الأميركي";
	}

	$programNameEn	= $emailPartnerData["program_name"]["en"];
	$partnerNameEn	= $emailPartnerData["partner_name_content"]["en"];
	$supportEmailEn	= $emailPartnerData["support_email"]["en"];

//	$programNameAr	= $emailPartnerData["program_name"]["ar"];
	$partnerNameAr	= $emailPartnerData["partner_name_content"]["ar"];
//	$supportEmailAr	= $emailPartnerData["support_email"]["ar"];

	$primaryColor	= $emailPartnerData["partner"]["primary"];

	$imageWidth = 200;
	if($transaction->partner_id != 4694):
		list($width, $height) = getimagesize(url("/") . "/media/image/" . $emailPartnerData["image"]["en"]);
		if ($height > $width / 2) {
			$imageWidth = 100;
		}
	endif;

	$userFullName		= $user->name;
	$userFirstName		= $user->first_name;

	$cashPayment		= $transaction->cash_payment;
	$transClass			= $transaction->trans_class;
	$refNb				= $transaction->ref_number;
	$notes				= $transaction->notes;

	$transClassLower	= strtolower($transClass);
	$refNbLower			= strtolower($refNb);
	$notesLower			= strtolower($notes);

	$json_notes			= json_decode($notes);

	$queriesEn	= "<p style='$style3'>For any questions or queries, please contact us on <a href='mailto:" . $supportEmailEn . "' style='$style0 color:" . $primaryColor . "; margin:0; padding:0;'>" . $supportEmailEn . "</a></p>";
	$queriesAr	= "<p style='$style3'>لإرسال أي استفسارات أو أسئلة إلينا يرجى التواصل معنا عبر <a href='mailto:" . $supportEmailEn . "' style='$style0 color:" . $primaryColor . "; margin:0; padding:0;'>" . $supportEmailEn . "</a></p>";

	$dearEn		= $userFullName;
	$dearAr		= $userFullName;

	if ($transClassLower == "car") {
		$emailTitleEn	= "Car Rental Redemption Summary" . ($cashPayment > 0 ? " &amp; Receipt" : "");
		$emailTitleAr	= "ملخص حجز سيارة" . ($cashPayment > 0 ? " ووصل" : "");

		$contentEn	 = "<p style='$style3'>Thank you for redeeming " . $partnerNameEn . " moments on the travel catalog.</p>";
		$contentEn	.= "<p style='$style3'>Below you will find the summary of your car rental booking. The final confirmation will be sent via email within one business day. Should the car and/or the dates requested be unavailable, one of our customer service representatives will get in touch with you to suggest an alternative car, date or to credit the moments back to your account.</p>";
		$contentEn	.= $queriesEn;

		$contentAr	 = "<p style='$style3'> نشكرك على استخدامك نقاط" . $partnerNameAr . "  للحجز من كاتالوج السفر.</p>";
		$contentAr	.= "<p style='$style3'>يوجد أدناه ملخص حجزك لسيارة مستأجرة و سيتم إرسال بريد الكتروني إليك خلال يوم عمل واحد. في حال عدم توفر السيارة المطلوبة أو عدم توفرها في التاريخ المطلوب, سيتصل بك أحد موظفي خدمة العملاء لدينا لاقتراح سيارة بديلة أو تاريخ بديل أو للمساعدة في إعادة النقاط المصروفة الى حسابك.</p>";
		$contentAr	.= $queriesAr;
	}
	else if ($transClassLower == "flight") {
		$emailTitleEn	= "Flight Booking Redemption Summary" . ($cashPayment > 0 ? " &amp; Receipt" : "");
		$emailTitleAr	= "ملخص حجز رحلة جوية" . ($cashPayment > 0 ? " ووصل" : "");

		$contentEn	 = "<p style='$style3'>Thank you for redeeming " . $partnerNameEn . " moments on the travel catalog.</p>";
		$contentEn	.= "<p style='$style3'>Below you will find the summary of your flight booking. The final confirmation along with the changes and cancellation details will be sent via email within one business day. Should the flights and/or the dates requested be unavailable, one of our customer service representatives will get in touch with you to suggest an alternative flight, date or to credit the moments back to your account.</p>";
		$contentEn	.= $queriesEn;

		$contentAr	 = "<p style='$style3'> نشكرك على استخدامك نقاط" . $partnerNameAr . "  للحجز من كاتالوج السفر.</p>";
		$contentAr	.= "<p style='$style3'>يوجد أدناه ملخص لحجزك و سيتم إرسال بريد الكتروني إليك خلال يوم عمل واحد, يحتوي على التأكيد النهائي وتفاصيل شروط تغيير أو الغاء الحجز. في حال عدم توفر الرحلة الجوية المطلوبة أو عدم توفر الرحلة في التواريخ المختارة, سيتصل بك أحد موظفي خدمة العملاء لدينا لاقتراح رحلة جوية بديلة أو تاريخ بديل أو للمساعدة في إعادة النقاط المصروفة الى حسابك.</p>";
		$contentAr	.= $queriesAr;
	}
	else if ($transClassLower == "hotel") {
		$emailTitleEn	= "Hotel Booking Redemption Summary" . ($cashPayment > 0 ? " &amp; Receipt" : "");
		$emailTitleAr	= "ملخص الحجز الفندقي" . ($cashPayment > 0 ? " ووصل" : "");

		$contentEn	 = "<p style='$style3'>Thank you for redeeming " . $partnerNameEn . " moments on the travel catalog.</p>";
		$contentEn	.= "<p style='$style3'>Below you will find the summary of your hotel booking. The final confirmation along with the changes and cancellation details will be sent via email within one business day. Should the room and/or the dates requested be unavailable, one of our customer service representatives will get in touch with you to suggest an alternative hotel, date or to credit the moments back to your account.</p>";
		$contentEn	.= $queriesEn;

		$contentAr	 = "<p style='$style3'> نشكرك على استخدامك نقاط" . $partnerNameAr . "  للحجز من كاتالوج السفر.</p>";
		$contentAr	.= "<p style='$style3'>يوجد أدناه ملخص لحجزك و سيتم إرسال بريد الكتروني إليك خلال يوم عمل واحد, يحتوي على التأكيد النهائي وتفاصيل شروط تغيير أو الغاء الحجز. في حال عدم توفر الغرفة المطلوبة أو عدم توفرها في التواريخ المختارة, سيتصل بك أحد موظفي خدمة العملاء لدينا لاقتراح فندق بديل أو تاريخ بديل أو للمساعدة في إعادة النقاط المصروفة الى حسابك.</p>";
		$contentAr	.= $queriesAr;
	}
	else if ($transClassLower == "points") {
		if ($refNbLower == "cashback" || $notesLower == "cashback") {
			$emailTitleEn = "Cashback Order Summary";
			$emailTitleAr	= "ملخص طلب سحب النقاط نقدا";

			$contentEn	 = "<p style='$style3'>Thank you for using Cashback Moments Conversion.<br />Your moments have been successfully converted.<br />The cashback amount will be credited to your Credit Card account within a period of 7 business days.</p>";

			$contentAr	 = "<p style='$style3'>نشكرك على استخدام خدمة سحب النقاط نقداً.<br /> لقد تم تحويل نقاطك بنجاح.<br />سيتم إضافة المبلغ النقدي الى بطاقتك الإئتمانية خلال سبعة أيام عمل.</p>";
		}
		else if (strpos($refNbLower, "miles points") > 0) {
			$emailTitleEn = "Miles Conversion Order Summary";
			$emailTitleAr	= "ملخص طلب تحويل النقاط الى أميال";

			$contentEn	 = "<p style='$style3'>Thank you for using Miles Conversion.<br />Your moments have been successfully converted.<br />The miles will be credited to your Frequent Flyer Account.</p>";

			$contentAr	 = "<p style='$style3'>نشكرك على استخدام خدمة تحويل الاميال.<br /> لقد تم تحويل نقاطك بنجاح.<br />سيتم إضافة الاميال الى رقمك المسافر الدائم.</p>";
		}
		else if (strpos($refNbLower, "donate points") > 0) {
			$emailTitleEn = "Donation Summary";
			$emailTitleAr	= "ملخص طلب التبرع";

			$contentEn	 = "<p style='$style3'>Thank you for donating on " . $partnerNameEn . " Website.</p>";

			$contentAr	 = "<p style='$style3'> شكرا على التبرع على موقع " . $partnerNameAr . ".</p>";
		}
		else {
			$emailTitleEn = "Transfer Order Summary";
			$emailTitleAr	= "ملخص طلب تحويل";

			$contentEn	 = "<p style='$style3'>Thank you for redeeming on the " . $partnerNameEn . " online catalogue.<br />Your moments have been successfully transferred.</p>";

			$contentAr	 = "<p style='$style3'> شكراً لاستخدام نقاطك على كاتالوغ" . $partnerNameAr . " الالكتروني.<br />لقد تم تحويل نقاطك بنجاح.</p>";
		}
	}
	else if ($transClassLower == "amount") {
		$emailTitleEn	= "In Store Transaction Summary";
		$emailTitleAr	= "ملخص الدفع في متجر شريك";

		$dearEn			= $userFirstName;
		$dearAr			= $userFirstName;

		$contentEn	 = "<p style='$style3'>Thank you for redeeming at one of the partner stores.</p>";
		$contentEn	.= $queriesEn;

		$contentAr	 = "<p style='$style3'>نشكرك على صرف نقاطك في أحد المتاجر الشريكة لنا.</p>";
		$contentAr	.= $queriesAr;
	}
	else {
		$emailTitleEn	= "Redemption Order Summary" . ($cashPayment > 0 ? " &amp; Receipt" : "");
		$emailTitleAr	= "ملخص استخدام النقاط" . ($cashPayment > 0 ? " ووصل" : "");

		$contentEn	 = "<p style='$style3'>Thank you for redeeming " . $partnerNameEn . " moments on the online catalog.</p>";
		if ($transClassLower == "items") {
			$contentEn	.= "<p style='$style3'>Your order is now in process. Your order will be delivered within 5 business days. If any item from your order is unavailable, one of our customer service representatives will contact you to suggest an alternative one or to credit the moments back to your account.<br /></p>";
			$contentEn	.= "<p style='$style3'>In case more than one item is redeemed, please note that each item might be delivered separately on a different day.<br /></p>";
			$contentEn	.= "<p style='$style3'>If you have redeemed your moments for a charity donation, we would like to thank you for your generosity on behalf of the organization receiving the donation. " . $queriesEn . "</p>";
		}
		else {
			$contentEn	.= $queriesEn;
		}

		$contentAr	 = "<p> شكراً لاستخدام نقاط" . $partnerNameAr . " من خلال شراء السلع أو الخدمات من الكاتالوج الإلكتروني.</p>";
		if ($transClassLower == "items") {
			$contentAr	.= "<p style='$style3'>سنقوم الان بتنفيذ طلبك. سيستغرق التوصيل حتى 5 أيام عمل, اذا كانت اي من السلع في طلبك غير متوفرة, سيتصل بك احد موظفي خدمة العملاء لدينا لاقتراح سلعة بديلة أو للمساعدة في اعادة النقاط المستخدمة على السلعة غير المتوفرة الى حسابك.<br /></p>";
			$contentAr	.= "<p style='$style3'>في حال شرائك ﻷكثر من سلعة, قد يتم توصيل كل سلعة في يوم مختلف عن الاخر.<br /></p>";
			$contentAr	.= "<p style='$style3'>أما في حال صرفك للنقاط للتبرع الى جمعية خيرية, فنحن نود أن نشكرك على كرمك بالنيابة عن الجمعية التي تبرعت اليها. " . $queriesAr . "</p>";
		}
		else {
			$contentAr	.= $queriesAr;
		}
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/xhtml" style="<?php echo $style1 ?>">
	<head>
		<meta name="viewport" content="width=device-width" />
		<meta http-equiv="Content-Type" content="text/html;  charset=UTF-8" />
		<title><?php echo $programNameEn ?> Redemption Receipt</title>
	</head>
	<body bgcolor="#dedede" style="<?php echo $style1 ?> width:100%!important; height:100%; -webkit-font-smoothing:antialiased; -webkit-text-size-adjust:none;">
		<table style="<?php echo $style0 ?> width:100%; margin:0; padding:20px;">
			<tr style="<?php echo $style1 ?>">
				<td style="<?php echo $style1 ?>"></td>
				<td bgcolor="#FFFFFF" style="<?php echo $style0 ?> display:block!important; max-width:600px!important; clear:both!important; margin:0 auto; padding:20px; border:1px solid #f0f0f0;">
					<div style="<?php echo $style0 ?> max-width:600px; display:block; margin:0 auto; padding:0;">
						<table style="<?php echo $style2 ?>">
							<tr style="<?php echo $style1 ?>">
								<td style="<?php echo $style1 ?>">
									<p style="font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:14px; line-height:1.6; font-weight:normal; margin:0; padding:15px 0 5px;">
										<?php //if($transaction->trans_class == "Amount") { ?>
										<?php if($transaction->partner_id === 4148): ?>
											<img src="<?php echo url('/'); ?>/media/image/<?php echo $emailPartnerData['image']['en'] ?>" alt="<?php echo $emailPartnerData['partner_name_content']['en'] ?>" style="<?php echo $style1 ?>; margin:0; padding:0; float:left!important;" />
										<?php else: ?>
										    <img src="<?php echo url('/'); ?>/media/image/<?php echo $emailPartnerData['image']['en'] ?>" alt="<?php echo $emailPartnerData['partner_name_content']['en'] ?>" style="<?php echo $style1 .  "width:" . $imageWidth . "px"; ?>; margin:0; padding:0; float:left!important;" />
										<?php endif; ?>
										<?php //} else {  ?>
											<!-- <img src="<?php //echo url('/'); ?>/media/image/<?php //echo $emailPartnerData['image']['en'] ?>" alt="<?php// echo $emailPartnerData['partner_name_content']['en'] ?>" style="<?php //$style1 . ($transaction->partner_id !== 4148 ? "width:" . $imageWidth . "px" : "") ?>; margin:0; padding:0; float:left!important;" /> -->
										<?php //}  ?>
									</p>
									<br /><br />
									<br /><br />

									<p style="text-align:center;" align="center">
										<h2 style="<?php echo $style10 ?>"><?php echo $emailTitleEn ?></h2>
									</p>
									<p style="<?php echo $style3 ?>">
										Dear <strong style="<?php echo $style1 ?>"><?php echo $dearEn ?></strong>,
									</p>
									<?php echo $contentEn ?>

									<!-- arabic -->
									<div style="direction:rtl!important; <?php echo $style0 ?> max-width:600px; display:block; margin:0 auto; padding:0;">
										<table style="<?php echo $style2 ?>">
											<tr style="<?php echo $style1 ?>">
												<td style="<?php echo $style1 ?>">
													<br /><br />

													<p style="text-align:center;" align="center">
														<h2 style="<?php echo $style10 ?>"><?php echo $emailTitleAr ?></h2>
													</p>
													<p style="font-size:14px; line-height:1.6; font-weight:normal; margin:0 0 10px; padding:0;">
														عزيز(ت)نا <strong style="<?php echo $style3 ?>"><?php echo $dearAr ?></strong>،
													</p>
													<?php echo $contentAr ?>
												</td>
											</tr>
										</table>
									</div>
									<!-- END arabic -->


									<?php if ($transClassLower == "car") { ?>
										<?php
										if (isset($json_notes)):
											$image_source	= $json_notes->img;
											$p_time			= $json_notes->pickup;
											$p_locName		= $json_notes->pickup_location_name;
											$d_time			= $json_notes->dropoff;
											$d_locName		= $json_notes->dropoff_location_name;
											?>
											<table border="0" cellspacing="0" cellpadding="0" style="font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:100%; line-height: 0.8; width: 100%; margin: 0; padding: 0;">
												<tr style="<?php echo $style4 ?>">
													<td rowspan="3" style="font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:100%; line-height: 0.2; text-align: center; margin: 0; " align="center"><img src="<?php echo $image_source; ?>" width="90" /></td>
													<td style="<?php echo $style5 ?>" align="left"><strong>Vehicle Type \ <span style="direction:rtl!important;">نوع السيارة</span>: </strong><?php echo $json_notes->vehicle; ?></td>
												</tr>
												<tr style="<?php echo $style4 ?>">
													<td style="<?php echo $style5 ?>" align="left"><strong>Pick up \ <span style="direction:rtl!important;">مكان الاستقبال</span>: </strong><?php echo $p_locName . " at " . $p_time; ?></td>
												</tr>
												<tr style="<?php echo $style4 ?>">
													<td style="<?php echo $style5 ?>" align="left"><strong>Drop Off \ <span style="direction:rtl!important;">مكان التوصيل</span>: </strong><?php echo $d_locName . " at " . $d_time; ?></td>
												</tr>
											</table>
										<?php endif; ?>
									<?php } else if ($transClassLower == "flight") { ?>
										<div style="font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:100%; line-height:1.6;  height:5px; width:100%; background-color:<?php echo $primaryColor ?>; margin:10px 0; padding:0;"></div>
										<p style="<?php echo $style3 ?>">
											<strong style="<?php echo $style1 ?>">Passenger Details \ <span style="direction:rtl!important;">تفاصيل الراكب</span>:</strong>
										</p>
										<table border="0" cellspacing="0" cellpadding="0" style="font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:100%; line-height:0.8; width:100%; margin:0; padding:0;">
											<?php
											$count = count($json_notes->first_name);
											for ($i = 0; $i < $count; $i++) {
												?>
												<tr style="<?php echo $style4 ?>"><td style="<?php echo $style5 ?>" align="left"><strong>Title \ <span style="direction:rtl!important;">اللقب</span>: </strong><?php echo $json_notes->title[$i]; ?></td></tr>
												<tr style="<?php echo $style4 ?>"><td style="<?php echo $style5 ?>" align="left"><strong>Name \ <span style="direction:rtl!important;">الاسم الكامل</span>: </strong><?php echo $json_notes->first_name[$i] . ' ' . $json_notes->last_name[$i]; ?></td></tr>
												<tr style="<?php echo $style4 ?>"><td style="<?php echo $style5 ?>" align="left"><strong>Mobile Number \ <span style="direction:rtl!important;">الهاتف</span>: </strong><div style="text-align: center; padding: 0 180px 0 0;"><?php echo $json_notes->mobile[$i]; ?></div></td></tr>
												<tr style="<?php echo $style4 ?>"><td style="<?php echo $style5 ?>" align="left"><strong>Email \ <span style="direction:rtl!important;">البريد الالكتروني</span>: </strong><?php echo $json_notes->email[$i]; ?></td></tr>
												<tr style="<?php echo $style4 ?>"><td style="<?php echo $style5 ?>" align="left"><strong>Date of Birth \ <span style="direction:rtl!important;">تاريخ الميلاد</span>: </strong><div style="text-align: center; padding: 0 175px 0 0;"><?php echo $json_notes->dob_day[$i] . '-' . $json_notes->dob_month[$i] . '-' . $json_notes->dob_year[$i]; ?></div></td></tr>
												<tr style="<?php echo $style4 ?>"><td style="<?php echo $style5 ?>" align="left"><strong>Nationality \ <span style="direction:rtl!important;">الجنسية</span>: </strong><?php echo str_replace("_", " ", $json_notes->nationality[$i]); ?></td></tr>
												<tr style="<?php echo $style4 ?>"><td style="<?php echo $style5 ?>" align="left"><strong>Passport Number \ <span style="direction:rtl!important;">رقم جواز السفر</span>: </strong><div style="text-align: center; padding: 0 120px 0 0;"><?php echo $json_notes->passport[$i]; ?></div></td></tr>
												<tr style="<?php echo $style4 ?>"><td style="<?php echo $style5 ?>" align="left"><strong>Passport Expiry Date \ <span style="direction:rtl!important;">تاريخ إنتهاء صلاحية جواز السفر</span>: </strong><div style="text-align: right; padding: 0 175px 0 0;"><?php echo $json_notes->passportexpiry_day[$i] . '-' . $json_notes->passportexpiry_month[$i] . '-' . $json_notes->passportexpiry_year[$i]; ?></div></td></tr>
											<?php } ?>
										</table>
										<?php
										if (isset($json_notes)) {
											$o_carrier_name = $json_notes->out_carrier_name;
											$o_depart_time = $json_notes->out_depart_time;
											$o_arrive_time = $json_notes->out_arrive_time;
											$o_depart_station_name = $json_notes->out_depart_station_name;
											$o_arrive_station_name = $json_notes->out_arrive_station_name;
											$o_depart_station_code = $json_notes->out_depart_station_code;
											$o_arrive_station_code = $json_notes->out_arrive_station_code;
											$o_duration = $json_notes->out_duration;
											$o_stops = $json_notes->out_stops;
											$return = -1;
											if (isset($json_notes->return)) {
												$return = $json_notes->return;
											}
											if ($return != -1) {
												$i_carrier_name = $json_notes->in_carrier_name;
												$i_depart_time = $json_notes->in_depart_time;
												$i_arrive_time = $json_notes->in_arrive_time;
												$i_depart_station_name = $json_notes->in_depart_station_name;
												$i_arrive_station_name = $json_notes->in_arrive_station_name;
												$i_depart_station_code = $json_notes->in_depart_station_code;
												$i_arrive_station_code = $json_notes->in_arrive_station_code;
												$i_duration = $json_notes->in_duration;
												$i_stops = $json_notes->in_stops;
											}
											if (isset($json_notes->cabinclass)) {
												$cabin_class = $json_notes->cabinclass;
											}
										}
										?>

										<div style="font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:100%; line-height:1.6; height: 5px; width: 100%; background-color: <?php echo $primaryColor ?>; margin: 10px 0; padding: 0;"></div>
										<p align="center">
											<strong style="font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:140%; line-height:1.6; margin:0; padding:0;">
												<?php
												if (isset($cabin_class)) {
													echo " Cabin Class \ درجة المقعد: " . $cabin_class;
												}
												?>
											</strong>
										</p>
										<div style="font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:100%; line-height:1.6; height: 10px; width: 100%; margin: 0; padding: 0;"></div>

										<table border="0" cellspacing="0" cellpadding="0" style="<?php echo $style2 ?>">
											<tr style="<?php echo $style1 ?>">
												<td style="font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:100%; line-height:1.6; border-left-style:solid; border-left-color:#d7d7d7; border-left-width:1px; font-weight:bold; color:#000000; text-align:center; background-color:#e0e0e0; margin:0; padding:5px;" align="center" bgcolor="#58595B"></td>
												<td style="<?php echo $style9 ?>" align="center" bgcolor="#58595B">Airline / Flight <br /><span style="direction:rtl!important;">شركة الطيران \ الرحلة</span></td>
												<td style="<?php echo $style9 ?>" align="center" bgcolor="#58595B">Depart / Arrive <br /><span style="direction:rtl!important;">تاريخ المغادرة \ الوصول</span></td>
												<td style="<?php echo $style9 ?>" align="center" bgcolor="#58595B">City / Airport <br /><span style="direction:rtl!important;">المدينة \ المطار</span></td>
												<td style="<?php echo $style9 ?>" align="center" bgcolor="#58595B">Duration / Stops <br /><span style="direction:rtl!important;">المدة \ وقفات</span> </td>
											</tr>
											<tr style="<?php echo $style1 ?>">
												<td rowspan="2" style="font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:100%; line-height:1.6; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px; text-align: center; margin: 0; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; " align="center"><img src="{{ asset('img/departBig.png') }}" width="100" /></td>
												<td rowspan="2" style="font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:100%; line-height:1.6; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; text-align: center; margin: 0; padding: 10px 5px;" align="center"><?php echo $o_carrier_name ?></td>
												<td style="font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:100%; line-height:1.6; /*border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;*/border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px;/*border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px;*/ text-align: center; margin: 0; padding: 10px 5px 0px 5px;" align="center"><?php echo date("F j, Y, g:i a", strtotime($o_depart_time)) ?></td>
												<td style="font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:100%; line-height:1.6; /*border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px;*//*border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px;*/ text-align: center; margin: 0; padding: 10px 5px 0px 5px;" align="center"><?php echo $o_depart_station_name . " (" . $o_depart_station_code . ")" ?></td>
												<td style="font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:100%; line-height:1.6; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px;/*border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px;*/ text-align: center; margin: 0; padding: 10px 5px 0px 5px;" align="center"><?php echo floor($o_duration / 60) . "H " . ($o_duration % 60) . "m" ?></td>
											</tr>
											<tr style="<?php echo $style1 ?>">
												<td style="font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:100%; line-height:1.6; /*border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;*/border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin: 0; padding: 0px 5px 10px 5px;" align="center"><?php echo date("F j, Y, g:i a", strtotime($o_arrive_time)) ?></td>
												<td style="font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:100%; line-height:1.6; /*border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px;*/ border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin: 0; padding: 0px 5px 10px 5px;" align="center"><?php echo $o_arrive_station_name . " (" . $o_arrive_station_code . ")" ?></td>
												<td style="font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:100%; line-height:1.6; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin: 0; padding: 0px 5px 10px 5px;" align="center"><?php echo $o_stops ?></td>
											</tr>
											<?php if ($return == "1"): ?>
												<tr style="<?php echo $style1 ?>">
													<td rowspan="2" style="font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:100%; line-height:1.6; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px; text-align: center; margin: 0; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; " align="center"><img src="{{ asset('img/arrivalBig.png') }}" width="100" /></td>
													<td rowspan="2" style="font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:100%; line-height:1.6; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin: 0; padding: 10px 5px;" align="center"><?php echo $i_carrier_name ?></td>
													<td style="font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:100%; line-height:1.6; /*border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;*/border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; /*border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px;*/ text-align: center; margin: 0; padding: 10px 5px 0px 5px;" align="center"><?php echo date("F j, Y, g:i a", strtotime($i_depart_time)) ?></td>
													<td style="font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:100%; line-height:1.6; /*border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; *//*border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px;*/ text-align: center; margin: 0; padding: 10px 5px 0px 5px;" align="center"><?php echo $i_depart_station_name . " (" . $i_depart_station_code . ")" ?></td>
													<td style="font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:100%; line-height:1.6; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; /*border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px;*/ text-align: center; margin: 0; padding: 10px 5px 0px 5px;" align="center"><?php echo floor($i_duration / 60) . "H " . ($i_duration % 60) . "m" ?></td>
												</tr>
												<tr style="<?php echo $style1 ?>">
													<td style="font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:100%; line-height:1.6; /*border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;*/border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin: 0; padding: 0px 5px 10px 5px;" align="center"><?php echo date("F j, Y, g:i a", strtotime($i_arrive_time)) ?></td>
													<td style="font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:100%; line-height:1.6; /*border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px;*/ border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin: 0; padding: 0px 5px 10px 5px;" align="center"><?php echo $i_arrive_station_name . " (" . $i_arrive_station_code . ")" ?></td>
													<td style="font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:100%; line-height:1.6; border-left-style: solid; border-left-color: #d7d7d7; border-left-width: 1px;border-right-style: solid; border-right-color: #d7d7d7; border-right-width: 1px; border-bottom-style: solid; border-bottom-color: #d7d7d7; border-bottom-width: 1px; text-align: center; margin: 0; padding: 0px 5px 10px 5px;" align="center"><?php echo $i_stops ?></td>
												</tr>
											<?php endif; ?>
										</table>
									<?php } else if ($transClassLower == "hotel") { ?>
										<?php
										$booking_results = isset($json_notes->booking_results) ? json_decode($json_notes->booking_results) : "";
										$h_name			= 'N/A';
										$room_details	= 'N/A';
										$room_name		= 'N/A';
										$h_address		= 'N/A';
										$image_source	= 'N/A';

										if (isset($json_notes)) {
											//$h_thumbnail = $json_notes->thumbnail;
											if (!empty($json_notes->hotelName)) {
												$h_name = $json_notes->hotelName;
											}
											if (!empty($json_notes->roomName)) {
												$room_details = $json_notes->roomName;
											}
											if (!empty($json_notes->hotel_name)) {
												$h_name = $json_notes->hotel_name;
											}
											if (!empty($json_notes->roomDetails)) {
												$room_details = $json_notes->roomDetails;
											}
											if (!empty($json_notes->reservation->item->name)) {
												$room_name = $json_notes->reservation->item->name;
											}
											if (!empty($json_notes->hotelAddress1)) {
												$h_address = $json_notes->hotelAddress1;
											}
											if (!empty($json_notes->hotel_address)) {
												$h_address = $json_notes->hotel_address;
											}
											$h_arrivalDate = $json_notes->arrivalDate;
											$h_departureDate = $json_notes->departureDate;

											if (!empty($json_notes->img)) {
												$image_source = $transaction->getHotelImageUrl();
											}
											if (!empty($json_notes->hotel_image)) {
												$image_source = $json_notes->hotel_image;
											}

											$cancelation_html		= "";
											$booking_code			= isset($booking_results->code)				? $booking_results->code			: "";
											$additional_info		= isset($booking_results->additional_info)	? $booking_results->additional_info	: "";
											$cancelation_policies	= isset($booking_results->policies)			? $booking_results->policies		: "";
											if ($cancelation_policies != "") {
												$days_remaining		= isset($cancelation_policies->days_remaining)	? $cancelation_policies->days_remaining : 0;
												$ratio				= isset($cancelation_policies->ratio)			? $cancelation_policies->ratio			: 1;
												$checkin_date		= isset($booking_results->checkin)				? $booking_results->checkin				: "";

												if ($checkin_date != "") {
													$safe_date_unix		= strtotime($checkin_date) - ($days_remaining * 60 * 60 * 24);
													$safe_date			= date("d M, Y", $safe_date_unix);
													$charge_percentage	= $ratio * 100;
													$cancelation_html = "No cancellation charges up tp " . $safe_date . ". Cancelations made after " . $safe_date . " will be assessed " . $charge_percentage . "%";
												}
											}

											$colspan	= !empty($json_notes->reservation->item->name) ? 9 : 8;
											?>
											<table border="0" cellspacing="0" cellpadding="0" style="font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:100%; line-height: 0.8; width: 100%; margin: 0; padding: 0;">
												<tr style="<?php echo $style4 ?>">
													<td rowspan="<?php echo $colspan ?>" style="font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:100%; line-height: 0.2; text-align: center; margin: 0; " align="center"><img src="<?php echo $image_source; ?>" width="180" /></td>
													<td style="<?php echo $style5 ?>" align="left"><strong>Booking Code: {{ $booking_code }}</strong></td>
												</tr>
												<tr style="<?php echo $style4 ?>">
													<td style="<?php echo $style5 ?>" align="left"><strong>Customer Name \ <span style="direction:rtl!important;">اسم الزبون</span>: </strong>{{ $json_notes->book_first_name . ' ' . $json_notes->book_last_name }}</td>
												</tr>
												<tr style="<?php echo $style4 ?>">
													<td style="font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:100%; line-height: 0.37; text-align: left; margin: 0; padding: 10px 5px;" align="left"><strong>Customer Mobile \ <span style="direction:rtl!important;">رقم هاتف الزبون</span>: </strong><div style="text-align: left; margin-top: 10px;">{{ $json_notes->book_mobile }}</div></td>
												</tr>
												<tr style="font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:100%; line-height: 0.37; margin: 0; padding: 0;">
													<td style="<?php echo $style5 ?>" align="left"><strong>Customer Email \ <span style="direction:rtl!important;">البريد الالكتروني</span>: </strong><div style="text-align: left; margin-top: 10px;">{{ $json_notes->book_email }}</div></td>
												</tr>
												<tr style="<?php echo $style4 ?>">
													<td style="<?php echo $style5 ?>" align="left"><strong>Hotel Name \ <span style="direction:rtl!important;">اسم الفندق</span>: </strong><?php echo $h_name; ?></td>
												</tr>
												<tr style="<?php echo $style4 ?>">
													<td style="font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:100%; line-height: 1.2; text-align: left; margin: 0; padding: 10px 5px;" align="left"><strong>Room Details: \ <span style="direction:rtl!important;">تفاصيل الغرفة</span> </strong><?php echo $room_details; ?></td>
												</tr>
												<?php /* ?>
												<tr style="<?php echo $style4 ?>">
													<td style="<?php echo $style5 ?>" align="left"><strong>Hotel Address \ <span style="direction:rtl!important;">عنوان الفندق</span>: </strong><?php echo $h_address; ?></td>
												</tr>
												<?php */ ?>
												<?php if (!empty($json_notes->reservation->item->name)) { ?>
													<tr style="<?php echo $style4 ?>">
														<td style="<?php echo $style5 ?>" align="left"><strong>Room Name: </strong><?php echo $room_name; ?></td>
													</tr>
												<?php } ?>
												<tr style="<?php echo $style4 ?>">
													<td style="<?php echo $style5 ?>" align="left"><strong>Check in \ <span style="direction:rtl!important;">الوصول</span>: </strong><?php echo $h_arrivalDate; ?></td>
												</tr>
												<tr style="<?php echo $style4 ?>">
													<td style="<?php echo $style5 ?>" align="left"><strong>Check out \ <span style="direction:rtl!important;">المغادرة</span>: </strong><?php echo $h_departureDate; ?></td>
												</tr>
												<?php if ($additional_info != "") { ?>
													<tr>
														<td colspan="2">
															<p><strong>Additional Information</strong></p>
															<div><?php echo $additional_info ?></div>
														</td>
													</tr>
												<?php } ?>
												<?php if ($cancelation_html != "") { ?>
													<tr>
														<td colspan="2">
															<p><strong>Cancellation Policy</strong></p>
															<div style="line-height:1.5;"><?php echo $cancelation_html ?></div>
														</td>
													</tr>
												<?php } ?>
											</table>
											<?php
										} ?>
									<?php } else if ($transClassLower == "points") { ?>
										<?php if ($refNbLower == "cashback" || $notesLower == "cashback") { ?>
											<p style="<?php echo $style3 ?>">
												<strong style="<?php echo $style1 ?>">Cashback Details \ تفاصيل السحب:</strong>
											</p>

											<table border="0" cellspacing="0" cellpadding="0" style="<?php echo $style2 ?>">
												<tr style="<?php echo $style1 ?>">
													<td style="<?php echo $style6 ?>" align="center" bgcolor="#58595B">Date \ التاريخ</td>
													<td style="<?php echo $style6 ?>" align="center" bgcolor="#58595B">Moments \ النقاط</td>
													<td style="<?php echo $style6 ?>" align="center" bgcolor="#58595B">Value \ القيمة</td>
												</tr>
												<tr style="<?php echo $style1 ?>">
													<td style="<?php echo $style7 ?>" align="center">{{ date("F j, Y, g:i a", strtotime($transaction->created_at)+7200 ) }}</td>
													<td style="<?php echo $style7 ?>" align="center">{{ number_format($transaction->points_redeemed) }}</td>
													<td style="<?php echo $style7 ?>" align="center">{{ App\Currency::find($transaction->currency_id)->short_code }} {{ number_format($transaction->original_total_amount,2) }}</td>
												</tr>
											</table>
										<?php } else if (strpos($refNbLower, "miles points") > 0) { ?>
											<?php
											$miles_details = json_decode($transaction->ref_number, TRUE);
											$miles = $miles_details['miles points'];

											$frequentFlyerDetails = json_decode($transaction->notes, TRUE);
											$affiliateProgram = App\AffiliateProgram::find($frequentFlyerDetails['airline'])->name;
											$frequentFlyerNumber = $frequentFlyerDetails['frequent_flyer_number'];
											?>
											<p style="<?php echo $style3 ?>">
												<strong style="<?php echo $style1 ?>">Miles Conversion Details \ تفاصيل تحويل أميال:</strong>
											</p>

											<table border="0" cellspacing="0" cellpadding="0" style="<?php echo $style2 ?>">
												<tr style="<?php echo $style1 ?>">
													<td style="<?php echo $style6 ?>" align="center" bgcolor="#58595B">Date \ التاريخ</td>
													<td style="<?php echo $style6 ?>" align="center" bgcolor="#58595B">Moments \ النقاط</td>
													<td style="<?php echo $style6 ?>" align="center" bgcolor="#58595B">Miles \أميال</td>
													<td style="<?php echo $style6 ?>" align="center" bgcolor="#58595B">Frequent Flyer \ المسافر الدائم</td>
												</tr>
												<tr style="<?php echo $style1 ?>">
													<td style="<?php echo $style7 ?>" align="center">{{ date("F j, Y, g:i a", strtotime($transaction->created_at)+7200 ) }}</td>
													<td style="<?php echo $style7 ?>" align="center">{{ number_format($miles) }}</td>
													<td style="<?php echo $style7 ?>" align="center">{{ number_format($transaction->points_redeemed) }}</td>
													<td style="<?php echo $style7 ?>" align="center"><?php echo $affiliateProgram; ?><br /><?php echo $frequentFlyerNumber; ?></td>
												</tr>
											</table>
										<?php //} else if (strpos($refNbLower, "donate points") > 0) { ?>

										<?php } else { ?>
											<p style="<?php echo $style3 ?>">
												<strong style="<?php echo $style1 ?>">Transaction Details \ تفاصيل التحويل:</strong>
											</p>

											<table border="0" cellspacing="0" cellpadding="0" style="<?php echo $style2 ?>">
												<tr style="<?php echo $style1 ?>">
													<td style="<?php echo $style6 ?>" align="center" bgcolor="#58595B">Date \ التاريخ</td>
													<td style="<?php echo $style6 ?>" align="center" bgcolor="#58595B">Moments \ النقاط</td>
												</tr>
												<tr style="<?php echo $style1 ?>">
													<td style="<?php echo $style7 ?>" align="center">{{ date("F j, Y, g:i a", strtotime($transaction->created_at)+7200 ) }}</td>
													<td style="<?php echo $style7 ?>" align="center">{{ number_format($transaction->points_redeemed) }}</td>
												</tr>
											</table>
										<?php } ?>
									<?php } else if ($transClassLower == "amount") { ?>
										<p style="<?php echo $style3 ?>">
											<strong style="<?php echo $style1 ?>">Transaction Details \ تفاصيل المعاملة:</strong>
										</p>

										<table border="0" cellspacing="0" cellpadding="0" style="<?php echo $style2 ?>">
											<tr style="<?php echo $style1 ?>">
												<td style="<?php echo $style6 ?>" align="center" bgcolor="#58595B">Date \ التاريخ</td>
												<td style="<?php echo $style6 ?>" align="center" bgcolor="#58595B">Amount \ المبلغ</td>
												@if($user->coupon_price > 0 )
													<td style="<?php echo $style6 ?>" align="center" bgcolor="#58595B">Coupon</td>
												@endif
												<td style="<?php echo $style6 ?>" align="center" bgcolor="#58595B">Moments \ نقاط</td>
												<td style="<?php echo $style6 ?>" align="center" bgcolor="#58595B">Partner Store \ متجر شريك </td>
											</tr>
											<tr style="<?php echo $style1 ?>">
												<td style="<?php echo $style7 ?>" align="center">{{ date("F j, Y, g:i a", strtotime($transaction->created_at)+7200 ) }}</td>
												<td style="<?php echo $style7 ?>" align="center"> {{ $shortCode }} {{ number_format($transaction->original_total_amount,2) }}</td>
												@if($user->coupon_price > 0 )
													<td style="<?php echo $style7 ?>" align="center">${{ number_format($user->coupon_price, 2) }}</td>
												@endif
												@if($transaction->trx_reward == '1')
													<td style="<?php echo $style7 ?>" align="center">{{ number_format($transaction->points_rewarded) }}</td>
												@else
													<td style="<?php echo $style7 ?>" align="center">{{ number_format($transaction->points_redeemed) }}</td>
												@endif
													<td style="<?php echo $style7 ?>" align="center">{{ $transaction->ref_number }}</td>
											</tr>
										</table>
									<?php } else if ($transClassLower == "items") { ?>
										<p style="<?php echo $style3 ?>">
											<strong style="<?php echo $style1 ?>">Order Details \ تفاصيل الطلب:</strong>
										</p>
										<table border="0" cellspacing="0" cellpadding="0" style="<?php echo $style2 ?>">
											<tr style="<?php echo $style1 ?>">
												<td style="<?php echo $style6 ?>" align="center" bgcolor="#58595B">Item \ السلعة</td>
												<td style="<?php echo $style6 ?>" align="center" bgcolor="#58595B">Description \ الوصف</td>
												<td style="<?php echo $style6 ?>" align="center" bgcolor="#58595B">Qty \ العدد</td>
												<td style="<?php echo $style6 ?>" align="center" bgcolor="#58595B">Total Moments \ اجمالي النقاط</td>
											</tr>

											@foreach($transaction->items as $i)
												<?php $img_src = url('/') . "/media/image/" . $i->product->cover_image; ?>
												<tr style="<?php echo $style1 ?>">
													<td style="<?php echo $style7 ?>" align="center"><img src="<?php echo $img_src; ?>" width="100" /></td>
													<td style="<?php echo $style7 ?>" align="center">{{ $i->product->name }}</td>
													<td style="<?php echo $style7 ?>" align="center">{{ $i->quantity }}</td>
													<?php
													$priceInPoints1 = PointsHelper::productPriceInPoints($i->product->price_in_points, $i->product->original_price, $i->product->currency_id, $transaction->network_id);
													$priceInPoints = $priceInPoints1 + ($i->product->original_sales_tax * $priceInPoints1 / 100);
													$total_points = $priceInPoints; // * $i->quantity;
													?>
													<td style="<?php echo $style7 ?>" align="center"><?php echo number_format(abs($i->price_in_points)); ?></td>
												</tr>
											@endforeach

											<tr style="height:30px; <?php echo $style1 ?>">
												<td colspan="4" style="<?php echo $style8 ?>" align="right"><div style="display:inline-block">Subtotal (Moments) \ <span style="direction:rtl!important;">المجموع بالنقاط</span>:</div><div style="display:inline-block"> {{ number_format($transaction->points_redeemed - $transaction->delivery_cost) }} Moments \ نقطة</div></td>
											</tr>
											<tr style="height:30px; <?php echo $style1 ?>">
												<td colspan="4" style="<?php echo $style8 ?>" align="right"><div style="display:inline-block">Shipping Charges \ <span style="direction:rtl!important;">تكاليف التوصيل</span>:</div><div style="display:inline-block"> {{ number_format($transaction->delivery_cost) }} Moments \ نقطة</div></td>
											</tr>
											<tr style="height:30px; <?php echo $style1 ?>">
												<td colspan="4" style="<?php echo $style8 ?>" align="right"><div style="display:inline-block">Total Moments Redeemed \ <span style="direction:rtl!important;">اجمالي النقاط المصروفة</span>:</div><div style="display:inline-block"> {{ number_format($transaction->points_redeemed) }} Moments \ نقطة </div></td>
											</tr>
											@if( $transaction->cash_payment > 0)
											<tr style="<?php echo $style1 ?>">
												<td colspan="4" style="<?php echo $style8 ?>" align="right"><div style="display:inline-block">Total Amount Paid (<?php echo $shortCode; ?>) \ <span style="direction:rtl!important;"><?php echo $arabic_currency; ?></span>:</div><div style="display:inline-block"> {{ $transaction->cash_payment * $transaction->exchange_rate }}</div></td>
											</tr>
											@endif
										</table>
									<?php } else { ?>
										<?php
										$email		= !empty($json_notes->email)	? $json_notes->email	: $user->email;
										$area		= $user->area					? $user->area->name		: "N/A";
										$city		= $user->city					? $user->city->name		: "N/A";
										$country	= $user->country				? $user->country->name	: "N/A";
										?>
										<div style="font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:100%; line-height:1.6; height: 10px; width: 100%; margin: 0; padding: 0;"></div>
										<div style="font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:100%; line-height:1.6; height: 5px; width: 100%; background-color: <?php echo $primaryColor ?>; margin: 10px 0; padding: 0;"></div>
										<div style="font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:100%; line-height:1.6; height: 10px; width: 100%; margin: 0; padding: 0;"></div>

										<p style="<?php echo $style3 ?>">
											<?php if ($transaction->trans_class == "Points"): ?>
												<strong style="<?php echo $style1 ?>">Member Details:</strong>
											<?php else: ?>
												<strong style="<?php echo $style1 ?>">Shipping Address:</strong>
											<?php endif; ?>
										</p>

										<table style="line-height:1.6; width: 100%; margin: 0; padding: 0;">
											<tr style="<?php echo $style1 ?>"><td style="<?php echo $style1 ?>">Full Name \ الاسم الكامل: {{ $user->name }}</td></tr>
											<tr style="<?php echo $style1 ?>"><td style="<?php echo $style1 ?>">Mobile \ الهاتف: <div style="text-align: left; margin: 0; padding: 0 180px 0 0;">{{ $user->normalized_mobile }}</div></td></tr>
											<tr style="<?php echo $style1 ?>"><td style="<?php echo $style1 ?>">Email \ البريد الإلكتروني: <div style="text-align: left; margin: 0; padding: 0;">{{ $email }}</div></td></tr>
											<tr style="<?php echo $style1 ?>"><td style="<?php echo $style1 ?>">Address \ العنوان : {{ $user->address_1 }}</td></tr>
											<tr style="<?php echo $style1 ?>"><td style="<?php echo $style1 ?>">Building, Residence, Floor \ البناء، السكن، الطابق: {{ $user->address_2 }}</td></tr>
											<tr style="<?php echo $style1 ?>"><td style="<?php echo $style1 ?>">State \ الولاية أو المحافظة أو المقاطعة: {{ $area }}</td></tr>
											<tr style="<?php echo $style1 ?>"><td style="<?php echo $style1 ?>">City \ المدينة: {{ $city }}</td></tr>
											<tr style="<?php echo $style1 ?>"><td style="<?php echo $style1 ?>">Country \ البلد: {{ $country }}</td></tr>
										</table>
										<div style="font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:100%; line-height:1.6; height: 10px; width: 100%; margin: 0; padding: 0;"></div>
									<?php } ?>

									<div style="font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:100%; line-height:1.6; height: 10px; width: 100%; margin: 0; padding: 0;"></div>
									<?php if ($transClassLower == "flight" || $transClassLower == "hotel" || $transClassLower == "car") { ?>
										<div style="font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:100%; line-height:1.6; height: 5px; width: 100%; background-color: <?php echo $primaryColor ?>; margin: 10px 0; padding: 0;"></div>
										<p style="font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size: 14px; line-height:1.6; font-weight: normal; text-align: right; margin: 0 0 0px; padding: 0;">
											<strong style="<?php echo $style1 ?>">Total Moments \ اجمالي النقاط:</strong>
											<div style="text-align: right; margin: 0; padding: 0;">{{number_format(abs(PointsHelper::amountToPoints($transaction->total_amount, 'USD', $transaction->network_id)))}}</div>
										</p>
										<p style="font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size: 14px; line-height:1.6; font-weight: normal; text-align: right; margin: 0 0 0px; padding: 0;">
											<strong style="<?php echo $style1 ?>">Total Moments Redeemed \ <span style="direction:rtl!important; text-align: left;">اجمالي النقاط المصروفة</span>:</strong>
											<div style="text-align: right; margin: 0; padding: 0;">{{number_format(abs($transaction->points_redeemed))}}</div>
										</p>
										<p style="font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size: 14px; line-height:1.6; font-weight: normal; text-align: right; margin: 0 0 0px; padding: 0;">
											<strong style="<?php echo $style1 ?>">Total Amount Paid (<?php echo $shortCode; ?>) \ <span style="direction:rtl!important; text-align: left;"><?php echo $arabic_currency; ?></span>:</strong>
											<div style="text-align: right; margin: 0; padding: 0;">{{number_format(abs($transaction->cash_payment * $transaction->exchange_rate), 2)}}</div>
										</p>
									<?php } ?>
									<div style="font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:100%; line-height:1.6; height: 10px; width: 100%; margin: 0; padding: 0;"></div>
									<div style="font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:100%; line-height:1.6; height: 10px; width: 100%; margin: 0; padding: 0;"></div>
									<div style="font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:100%; line-height:1.6; height: 10px; width: 100%; margin: 0; padding: 0;"></div>

									<table style="<?php echo $style2 ?>">
										<tr style="<?php echo $style1 ?>">
											<td style="font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:100%; line-height:1.6; text-align: center; margin: 0; padding: 0;" align="center">
												<p style="<?php echo $style3 ?>">
													@if($transaction->trans_class == "Amount")
														<a href="<?php echo $emailPartnerData['website_url']['en'] ?>" style="font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:100%; line-height: 2; color: #FFF; text-decoration: none; font-weight: bold; text-align: center; cursor: pointer; display: inline-block; border-radius: 25px; background-color: <?php echo $primaryColor ?>; margin: 0 10px 0 0; padding: 0; border-color: <?php echo $primaryColor ?>; border-style: solid; border-width: 10px 20px;">Visit <?php echo $programNameEn ?> for amazing rewards!</a>
													@else
														<a href="<?php echo $emailPartnerData['website_url']['en'] ?>" style="font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:100%; line-height: 2; color: #FFF; text-decoration: none; font-weight: bold; text-align: center; cursor: pointer; display: inline-block; border-radius: 25px; background-color: <?php echo $primaryColor ?>; margin: 0 10px 0 0; padding: 0; border-color: <?php echo $primaryColor ?>; border-style: solid; border-width: 10px 20px;">Visit <?php echo $programNameEn ?> for amazing rewards!</a>
													@endif
												</p>
											</td>
										</tr>
									</table>

									<div style="font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:100%; line-height:1.6; height: 10px; width: 100%; margin: 0; padding: 0;"></div>
									<div style="font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:100%; line-height:1.6; height: 10px; width: 100%; margin: 0; padding: 0;"></div>
									<div style="font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:100%; line-height:1.6; height: 10px; width: 100%; margin: 0; padding: 0;"></div>

									<table style="<?php echo $style2 ?>">
										<tr style="<?php echo $style1 ?>">
											<td style="font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:100%; font-size: 11px; line-height:1.6; text-align: center; margin: 0; padding: 0;" align="center">
												This email was sent from a notification-only email address that cannot accept incoming notifications. Please do not reply to this message.
												<br style="<?php echo $style1 ?>" />
												Copyright © {{ date('Y') }} <?php echo $emailPartnerData['copyright']['en'] ?>. All Rights Reserved
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</div>
				</td>
				<td style="<?php echo $style1 ?>"></td>
			</tr>
		</table>
		<table style="font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:100%; line-height:1.6; width: 100%; clear: both !important; margin: 0; padding: 0;">
			<tr style="<?php echo $style1 ?>">
				<td style="<?php echo $style1 ?>"></td>
				<td style="font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:100%; line-height:1.6; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto; padding: 0;"></td>
				<td style="<?php echo $style1 ?>"></td>
			</tr>
		</table>
	</body>
</html>