<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/xhtml" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; margin: 0; padding: 0;">
  <head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>BLU Reward Receipt</title>
  </head>
    <body bgcolor="#f6f6f6" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%; margin: 0; padding: 0;">
      <table style="width: 800px; margin: 0 auto; border-style: none; border-width: 0px; background-color: #ffffff;">
          <tr><td>
      <p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; text-align: left; margin: 0; padding: 30px 0 5px;">
	<img src="{{ asset('img/email-logo.png') }}"  alt="BLU Points" style="width: 200px; font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 100%; line-height: 1.6; max-width: 100%; margin: 0; padding: 0;" /></p>
              </td></tr>
                  <tr><td><p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; text-align: center; margin: 0;  padding: 30px 0 5px;">
          <h2 style="font-family: 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; font-size: 28px; line-height: 1.2; color: #000; font-weight: 200; margin: 0; padding: 0 0 40px;">BLU Points Reward Summary</h2></p>
                      </td></tr>



            <tr>
                <td style="text-align: left;">
                    Dear <strong>{{ $name }}</strong>,<br><br>

                            You have collected <strong>{{ $points_rewarded }}</strong> reward points from <strong>{{ $start }}</strong> to <strong>{{ $end }}</strong> by using BLU Points Network . Please login to your account for more details by visiting the following link: <a href=http://www.blupoints.com/">http://www.blupoints.com</a>
                    <br><br>
                    The more you use BLU Points Network, the more valuable points you will earn.
                    <br><br>
                    For any further clarification, please send us an e-mail to: info@blupoints.com 
                </td>
            </tr>
        </table>
        </p>
</body>
 </html>
