@extends('layouts.dashboard')

@section('title')
    Reward Summary E-mail | BLU
@stop

@section('body')
<div class="padding">
    
    
    <div class="content-head pure-g">
        <div class="title pure-u-1-2">
            <h1><i class="fa fa-chain-broken fa-fw"></i>Reward Summary Email</h1>
            
            
        </div>

    </div>
           
    {{ Form::open(array('url' => url('/monthlyemail/monthlymail'), 'files' => false, 'method' => 'post', 'class' => "pure-form pure-form-stacked form-width")) }}
        <div class="pure-u-1-3 report-controls">
                <div class="padding">
                    @include('partials.errors')                  
                    <?php if(isset($defaults)){ ?>
                    @include('report.partials.period', $defaults)
                    @include('report.partials.segments', $defaults)
                    <?php } 
                    ?>
                </div>
        </div>
     <div class="pure-u-1-3">
         <div class="padding">
        <input type="checkbox" class="parent" name="zerobalance" value="zerobalance" />Include 0 Balances<br>
        <input type="checkbox" class="parent" name="inactive" value="inactive" />Include inactive users<br>
        <input type="checkbox" class="parent" name="allusers" value="allusers" />Include all partner users<br>

        
        <label class="radio-label block padding-bottom-10 emailclick">
                        {{ Form::radio('type', 'Email') }} Email
                    </label>
        <label class="radio-label block padding-bottom-10 smsclick">
                        {{ Form::radio('type', 'Sms') }} SMS
                    </label>
        <!-- Form Buttons -->
        <div class="form-buttons">
            <div class="left">
                <button type="submit" class="pure-button pure-button-primary">Process</button>
                <br><p><?php 
                if(isset($result)){
                    echo $result;
                }
                    ?></p>
            </div>
            <div class="clearfix"></div>
        </div>
     </div>
     </div>
        <!-- /Form Buttons -->
    {{ Form::close() }}       
    {{ Session::get('success') }}
    <?php if(isset($params)) var_dump($params); ?>

</div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-monthlyreward');
        $(".smsclick").click(function(){
            $(".report-period").hide();
        });
        $(".emailclick").click(function(){
            $(".report-period").show();
        });
        
    </script>
@stop
