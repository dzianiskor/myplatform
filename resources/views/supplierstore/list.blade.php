@if(count($supplierstores) > 0)
    @foreach($supplierstores as $supplierstore)
        <tr>
            <td>{{ $supplierstore->id }}</td>
            <td>{{ $supplierstore->name }}</td>
            <td>
                <a href="http://maps.google.com/maps?q={{ round($supplierstore->lat,6) }},{{ round($supplierstore->lng,6) }}" target="_blank">View</a>
            </td>
            <td>
                @if(!I::can('delete_suppliers') && !I::can('edit_suppliers'))
                    N/A
                @endif
                @if(I::can('edit_suppliers'))
                    <a href="{{ url('dashboard/supplierstore/edit/'.$supplierstore->id) }}" class="edit-supplier-store" title="Edit">Edit</a>
                @endif
                @if(I::can('delete_suppliers'))
                    <a href="{{ url('dashboard/supplierstore/delete/' . base64_encode($supplier_id) . '/' . base64_encode($supplierstore->id)) }}" class="delete-supplier-store" title="Delete">Delete</a>
                @endif
            </td>
        </tr>
    @endforeach
@else
    <tr class="empty">
        <td colspan="4">No store locations defined</td>
    </tr>
@endif
