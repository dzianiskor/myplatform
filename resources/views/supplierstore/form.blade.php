
{{ Form::open(array('url' => url('dashboard/supplierstore/update/'.$supplierstore->id), 'method' => 'post', 'class' => "pure-form pure-form-stacked", 'id' => 'new-supplier-store-form')) }}

@include('partials.errors')

<div class="pure-g">
    <div class="pure-u-1-3">
        {{ Form::label('supplierstore_name', 'Store Name') }}
        {{ Form::text('supplierstore_name', $supplierstore->name, array('class' => 'pure-input-1 required')) }}

        {{ Form::label('supplierstore_country', 'Country') }}
        {{ Markup::country_select('supplierstore_country', $supplierstore->address ? $supplierstore->address->country_id : '', array('class' => 'pure-input-1-2')) }}

        {{ Form::label('supplierstore_area', 'Area') }}
        {{ Markup::area_select('supplierstore_area', $supplierstore->address ? $supplierstore->address->country_id : '', $supplierstore->address ? $supplierstore->address->area_id : '', array('class' => 'pure-input-1-2')) }}

        {{ Form::label('supplierstore_city', 'City') }}
        {{ Markup::city_select('supplierstore_city', $supplierstore->address ? $supplierstore->address->area_id : '', $supplierstore->address ? $supplierstore->address->city_id : '', array('class' => 'pure-input-1-2')) }}

        {{ Form::label('supplierstore_street', 'Street') }}
        {{ Form::text('supplierstore_street', $supplierstore->address ? $supplierstore->address->street : '', array('class' => 'pure-input-1')) }}

        {{ Form::label('supplierstore_floor', 'Floor') }}
        {{ Form::text('supplierstore_floor', $supplierstore->address ? $supplierstore->address->floor : '', array('class' => 'pure-input-1')) }}

        {{ Form::label('supplierstore_building', 'Building') }}
        {{ Form::text('supplierstore_building', $supplierstore->address ? $supplierstore->address->building : '', array('class' => 'pure-input-1')) }}
    </div>
    <div class="pure-u-2-3">
        <div class="padding-left-60">
            <div class="map-canvas" id="map"></div>

            <div class="pure-g">
                <div class="pure-u-1-2">
                    <div class="padding-right-10">
                        {{ Form::label('supplierstore_latitude', 'Latitude') }}
                        {{ Form::text('supplierstore_latitude', $supplierstore->lat, array('class' => 'pure-input-1')) }}
                    </div>
                </div>
                <div class="pure-u-1-2">
                    {{ Form::label('supplierstore_longitude', 'Longitude') }}
                    {{ Form::text('supplierstore_longitude', $supplierstore->lng, array('class' => 'pure-input-1')) }}
                </div>
            </div>


        </div>
    </div>

</div>

{{-- Dialog Buttons --}}
<div class="form-buttons">
    <div class="left">
        <a href="#" class="pure-button close-dialog">Cancel</a>
    </div>
    <div class="right">
        <a href="#" class="pure-button pure-button-primary save-new-supplier-store">Save</a>
    </div>
    <div class="clearfix"></div>
</div>
{{-- /Dialog Buttons --}}

{{ Form::close() }}