@extends('layouts.dashboard')

@section('title')
    Card &amp; Reference ID Generator | BLU
@stop

@section('body')
    <div class="padding">

        <div class="content-head pure-g">
            <div class="title pure-u-1-2">
                <h1><i class="fa fa-paperclip fa-fw"></i>Card &amp; Ref ID Batches ({{ $batches->total() }})</h1>
            </div>

            <div class="controls pure-u-1-2">
                <form action="{{ Filter::searchUrl() }}" method="get">
                    <input type="text" placeholder="Search" name="q" value="{{ Input::get('q') }}" />
                    <button type="submit" title="" class="list-search"></button>
                </form>
                @if(I::can('create_cards'))
                    <a href="{{ url('dashboard/generator/create') }}" class="pure-button pure-button-primary">New Batch</a>
                @endif
            </div>
        </div>

        <table class="pure-table">
            <thead>
                <tr>
                    <th width="50"><a href="{{ Filter::baseUrl('sort=id') }}">ID</a></th>
                    <th><a href="{{ Filter::baseUrl('sort=type') }}">Type</a></th>
                    <th>Partner</th>
                    <th><a href="{{ Filter::baseUrl('sort=created_at') }}">Date Generated</a></th>
                    <th><a href="{{ Filter::baseUrl('sort=number_of_items') }}">Item Count</a></th>
                    <th width="150">Download</th>
                </tr>
            </thead>

            <tbody>
                @if(count($batches) > 0)
                    @foreach($batches as $b)
                        <tr>
                            <td>
                                {{ $b->id }}
                            </td>
                            <td>
                                @if($b->type == 'Cards')
                                    <a href="/dashboard/generator/listbatch/{{ base64_encode($b->id) }}" class="list" title="List">{{ $b->type }}</a>
                                @else
                                {{ $b->type }}
                                @endif
                                
                                
                            </td>
                            <td>
                                @if ($b->partner)
                                    {{ $b->partner->name }}
                                @endif
                            </td>                            
                            <td>
                                {{ date("F j, Y, g:i a", strtotime($b->created_at)) }}
                            </td>
                            <td>
                                {{ number_format($b->number_of_items) }}
                            </td>
                            <td>
                                @if($b->type == 'Cards' && I::can('download_cards'))
                                    <a href="/dashboard/generator/download/{{ base64_encode($b->id) }}/csv" class="download csv" title="Download CSV">CSV</a>
                                    <a href="/dashboard/generator/download/{{ base64_encode($b->id) }}/xls" class="download xls" title="Download Excel">Excel</a>
                                @endif
                                @if($b->type == 'References' && I::can('download_ref_ids'))
                                    <a href="/dashboard/generator/download/{{ base64_encode($b->id) }}/csv" class="download csv" title="Download CSV">CSV</a>
                                    <a href="/dashboard/generator/download/{{ base64_encode($b->id) }}/xls" class="download xls" title="Download Excel">Excel</a>
                                @endif                                
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="6">No Batches Found</td>
                    </tr>
                @endif
            </tbody>
        </table>

        {{ $batches->setPath('generator')->links() }}
       
    </div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-generator');
    </script>
@stop
