<fieldset>
    <legend>Batch Settings</legend>

    <span class="info">
        Batch generation could take some time depending on the number of items being created.
    </span>

    {{ Form::label('generate-type', 'Type of batch to generate:') }}
    @if(I::can('create_cards'))
        <label class="radio-label">
            {{ Form::radio('generate-type', "Cards", true) }} Card Accounts
        </label>
    @endif

    @if(I::can('create_ref_ids'))
        <label class="radio-label">
            {{ Form::radio('generate-type', "References") }} Reference ID's
        </label>
    @endif
</fieldset>

<fieldset class="batch-type Cards">
    <legend>Card Account Settings</legend>

    {{ Form::label('partner_id_card', 'Assigned to Partner') }}
    {{ Form::select('partner_id_card', $partners, null, array('class' => 'pure-input-1-4 required')) }}

    {{ Form::label('card_count', 'Number of Cards to Generate') }}
    {{ Form::text('card_count', '100', array('class'=>'pure-input-1-4 required', 'placeholder' => '100')) }}
</fieldset>

<fieldset class="batch-type References" style="display:none">
    <legend>Reference ID Settings</legend>

    {{ Form::label('partner_id', 'Assigned to Partner') }}
    {{ Form::select('partner_id', $partners, null, array('class' => 'pure-input-1-4 required')) }}
    
    {{ Form::label('prefix', 'Prefix') }}
    {{ Form::text('prefix', '', array('class'=>'pure-input-1-4 required', 'placeholder' => 'XYZ')) }}

    {{ Form::label('reference_count', 'Number of Reference ID\'s to Generate') }}
    {{ Form::text('reference_count', '100', array('class'=>'pure-input-1-4 required', 'placeholder' => '100')) }}
</fieldset>

