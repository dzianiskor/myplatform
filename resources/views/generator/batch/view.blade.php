@extends('layouts.dashboard')

@section('title')
    Card & Reference ID Generator | BLU
@stop

@section('body')
    <div class="padding">

        <div class="content-head pure-g">
            <div class="title pure-u-1-2">
                <h1><i class="fa fa-paperclip fa-fw"></i>Generate New Batch</h1>
            </div>

            <div class="controls pure-u-1-2">
                <a href="{{ url('dashboard/generator') }}" class="pure-button">All Batches</a>
            </div>
        </div>

        {{ Form::model($batch, array('url'   => '/dashboard/generator/update/'.base64_encode($batch->id),
                                     'class' => 'pure-form pure-form-stacked form-width')) }}

            @include('partials.errors')

            @include('generator/batch/form', array('batch' => $batch))

            {{--- Form Buttons ---}}
            <div class="form-buttons">
                <div class="left">
                    <a href="{{ url('dashboard/generator') }}" class="pure-button">Cancel</a>
                </div>
                <div class="right">
                    <button type="submit" class="pure-button pure-button-primary">Create Batch</button>
                </div>
                <div class="clearfix"></div>
            </div>
            {{--- /Form Buttons ---}}          
        
        {{ Form::close() }}
    </div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-generator');
    </script>
    <script>
        $(function(){
            $('input[name=generate-type]').change(function(){
                $('fieldset.batch-type ').hide();
                $('fieldset.'+$(this).val()).fadeIn();
            });
        });
    </script>
@stop