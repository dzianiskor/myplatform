@extends('layouts.dashboard')

@section('title')
    Card &amp; Reference ID Generator | BLU
@stop

@section('body')
    <div class="padding">

        <div class="content-head pure-g">
            <div class="title pure-u-1-2">
                <h1><i class="fa fa-paperclip fa-fw"></i>Card &amp; Ref ID Batches ({{ count($batch) }})</h1>
            </div>

            
        </div>

        <table class="pure-table">
            <thead>
                <tr>
                    <th width="50"><a href="{{ Filter::baseUrl('sort=id') }}">ID</a></th>
                    <th><a href="{{ Filter::baseUrl('sort=type') }}">Card</a></th>
                    <th>Date Generated</th>
                    <th width="150">Action</th>
                </tr>
            </thead>

            <tbody>
                @if(count($batch) > 0)
                    
                    @foreach($batch as $b)
                        <tr>
                            <td>
                                {{ $b['id'] }}
                            </td>
                            <td>
                                {{ $b['number'] }}
                            </td>
                            <td>
                                {{ date("F j, Y, g:i a", strtotime($b['created_at'])) }}
                            </td>
                            <td>
                                <a href="/dashboard/generator/printcard/{{ base64_encode($b['id']) }}" class="print" title="Print">Print</a>                      
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="6">No Cards Found</td>
                    </tr>
                @endif
            </tbody>
        </table>

        
    </div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-generator');
    </script>
@stop
