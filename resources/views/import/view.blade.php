@extends('layouts.dashboard')

@section('title')
    Import Data | BLU
@stop

@section('body')
<div class="padding">

    <div class="content-head pure-g">
        <div class="title pure-u-1-2">
            <h1><i class="fa fa-chain-broken fa-fw"></i>Import Data</h1>
        </div>

    </div>

    {{ Form::open(array('url' => url('import/import'), 'files' => true, 'method' => 'post', 'class' => "pure-form pure-form-stacked form-width")) }}

        @include('partials.errors')                  

        {{ Form::label('file', 'Import File') }}
        {{ Form::file('file') }}

        <!-- Form Buttons -->
        <div class="form-buttons">
            <div class="left">
                <button type="submit" class="pure-button pure-button-primary">Import file</button>
            </div>
            <div class="clearfix"></div>
        </div>
        <!-- /Form Buttons -->
    {{ Form::close() }}       
    {{ Session::get('success') }}
    <?php if(null !== Session::get('revert')) {?>
        {{ Form::open(array('url' => url('import/reverseimport'), 'files' => true, 'method' => 'get')) }}
        <input type="hidden" value="{{ Session::get('revert') }}" name="filename"/>
        <!-- Form Buttons -->
        <div>
            <div class="left">
                <button type="submit" class="pure-button pure-button-primary">Delete existing file</button>
            </div>
            <div class="clearfix"></div>
        </div>
        <!-- /Form Buttons -->
    {{ Form::close() }} 
    <?php }?>
    <?php if(isset($params)) var_dump($params); ?>

</div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-import');
    </script>
@stop
