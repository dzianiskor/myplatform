
{{ Form::open(array('url' => url('dashboard/tiercriteria/update/'.base64_encode($tiercriteria->id)), 'method' => 'post', 'class' => "pure-form pure-form-stacked", 'id' => 'new-tier-criteria-form')) }}
    
    @include('partials.errors')                         

    <div class="pure-g">
        <div class="pure-u-1">
            {{ Form::label('tiercriteria_criterion', 'Criterion', array('for'=>'tiercriteria_criterion')) }}
            <select name="tiercriteria_criterion" class="pure-input-1">
            <?php foreach(Meta::tierCriteriaOptions() as $k => $s): ?>
                    <?php if($k == $tiercriteria->criterion): ?>
                            <option value="<?php echo $k; ?>" selected="selected"><?php echo $s; ?></option>
                    <?php else: ?>
                            <option value="<?php echo $k; ?>"><?php echo $s; ?></option>
                    <?php endif; ?>
            <?php endforeach; ?>	
            </select>
            
            

            {{ Form::label('tiercriteria_name', 'Name') }}
            {{ Form::text('tiercriteria_name', $tiercriteria->name, array('class' => 'pure-input-1')) }}

            {{ Form::label('tiercriteria_start_value', 'Start Value') }}
            {{ Form::text('tiercriteria_start_value', $tiercriteria->start_value, array('class' => 'pure-input-1')) }}

            {{ Form::label('tiercriteria_end_value', 'End Value') }}
            {{ Form::text('tiercriteria_end_value', $tiercriteria->end_value, array('class' => 'pure-input-1')) }}
            <input type="hidden" name="tiercriteria_expiry_in_months" value="<?php echo $tiercriteria->expiry_in_months; ?>"/>
            {{ Form::label('tiercriteria_expiry_in_months', 'Expiry (in Months)') }}
            <select name="tiercriteria_expiry_in_months1" disabled="disabled" class="pure-input-1">
            
                <?php if(12 == $tiercriteria->expiry_in_months): ?>
                    <option value="12" selected="selected">1 Year</option>
                <?php else: ?>
                    <option value="12">1 Year</option>
                <?php endif; ?>
                <?php if(24 == $tiercriteria->expiry_in_months): ?>
                    <option value="24" selected="selected">2 Year</option>
                <?php else: ?>
                    <option value="24">2 Year</option>
                <?php endif; ?>
                <?php if(36 == $tiercriteria->expiry_in_months): ?>
                    <option value="36" selected="selected">3 Years</option>
                <?php else: ?>
                    <option value="36">3 Years</option>
                <?php endif; ?>
                <?php if(48 == $tiercriteria->expiry_in_months): ?>
                    <option value="48" selected="selected">4 Years</option>
                <?php else: ?>
                    <option value="48">4 Years</option>
                <?php endif; ?>
                <?php if(60 == $tiercriteria->expiry_in_months): ?>
                    <option value="60" selected="selected">5 Years</option>
                <?php else: ?>
                    <option value="60">5 Years</option>
                <?php endif; ?>
                <?php if(72 == $tiercriteria->expiry_in_months): ?>
                    <option value="72" selected="selected">6 Years</option>
                <?php else: ?>
                    <option value="72">6 Years</option>
                <?php endif; ?>
                <?php if(84 == $tiercriteria->expiry_in_months): ?>
                    <option value="84" selected="selected">7 Years</option>
                <?php else: ?>
                    <option value="84">7 Years</option>
                <?php endif; ?>
                <?php if(96 == $tiercriteria->expiry_in_months): ?>
                    <option value="96" selected="selected">8 Years</option>
                <?php else: ?>
                    <option value="96">8 Years</option>
                <?php endif; ?>
                <?php if(108 == $tiercriteria->expiry_in_months): ?>
                    <option value="108" selected="selected">9 Years</option>
                <?php else: ?>
                    <option value="108">9 Years</option>
                <?php endif; ?>
                <?php if(120 == $tiercriteria->expiry_in_months): ?>
                    <option value="120" selected="selected">10 Years</option>
                <?php else: ?>
                    <option value="120">10 Years</option>
                <?php endif; ?>
            </select>
            
        </div>
        

    </div>

    {{-- Dialog Buttons --}}
    <div class="form-buttons">
        <div class="left">
            <a href="#" class="pure-button close-dialog">Cancel</a>
        </div>
        <div class="right">
            <a href="#" class="pure-button pure-button-primary save-new-tier-criteria">Save</a>
        </div>
        <div class="clearfix"></div>
    </div>
    {{-- /Dialog Buttons --}}

{{ Form::close() }}