@if(count($tiercriterias) > 0)
  @foreach($tiercriterias as $tiercriteria)
    <tr>
        <td>{{ $tiercriteria->id }}</td>
        <td>{{ $tiercriteria->name }}</td>
        <td>{{ $tiercriteria->start_value }}</td>
        <td>{{ $tiercriteria->end_value }}</td>
        <td>
            @if(I::can('edit_tiers'))
                <a href="{{ url('dashboard/tiercriteria/edit/'.base64_encode($tiercriteria->id))  }}" class="edit-tier-criteria" title="Edit">Edit</a>
            @endif
            @if(I::can('delete_tiers'))
                <a href="{{ url('dashboard/tiercriteria/delete/'.base64_encode($tiercriteria->id))  }}" class="delete-tier-criteria" title="Delete">Delete</a>
            @else
                N/A
            @endif
        </td>
    </tr>
  @endforeach
@else
    <tr class="empty">
        <td colspan="4">No criteria defined</td>
    </tr>
@endif
