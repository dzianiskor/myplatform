<fieldset>
	<legend>Basic Details</legend>
    {{ Form::label('name', 'Language Name', array('for'=>'name')) }}
    {{ Form::text('name', Input::get('name'), array('class'=>'pure-input-1 required')) }}

    {{ Form::label('short_code', 'Short Code', array('for'=>'short_code')) }}
    {{ Form::text('short_code', Input::get('short_code'), array('class'=>'pure-input-1')) }}

</fieldset>
