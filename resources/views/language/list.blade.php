@extends('layouts.dashboard')

@section('title')
    Languages | BLU
@stop

@section('body')
<div class="padding">

    <div class="content-head pure-g">
        <div class="title pure-u-1-2">
            <h1><i class="fa fa-globe fa-fw"></i>Languages ({{ $languages->total() }})</h1>
        </div>

        <div class="controls pure-u-1-2">
            <form action="{{ Filter::searchUrl() }}" method="get">
                <input type="text" placeholder="Search" name="q" value="{{ Input::get('q') }}" />
                <button type="submit" title="" class="list-search"></button>
            </form>
            
                <a href="{{ url('dashboard/languages/new') }}" class="pure-button pure-button-primary">Add Languages</a>

        </div>
    </div>

    <table class="pure-table">
        <thead>
            <tr>
                <th width="50"><a href="{{ Filter::baseUrl('sort=id') }}">ID</a></th>
                <th style="text-align:left;padding-left:20px;"><a href="{{ Filter::baseUrl('sort=name') }}">Name</a></th>
                <th><a href="{{ Filter::baseUrl('sort=short_code') }}">Short Code</a></th>
                <th width="150">Actions</th>
            </tr>
        </thead>

        <tbody>
        @if(count($languages) > 0)
            @foreach ($languages as $language)
                <tr>
                    <td>
                        {{{ $language->id }}}
                    </td>
                    <td style="text-align:left;padding-left:20px;">
                        <a href="{{ url('dashboard/languages/view/'.base64_encode($language->id))}}" class="edit" title="Edit">{{{ $language->name }}}</a>
                    </td>
                    <td>
                        {{{ $language->short_code}}}
                    </td>
                    
                    <td>
                        <a href="{{ url('dashboard/languages/view/'.base64_encode($language->id)) }}" class="edit" title="View">View</a>

                        @if(I::can('delete_languages'))
                            <a href="{{ url('dashboard/languages/delete/'.base64_encode($language->id)) }}" class="delete" title="Delete">Delete</a>
                        @endif
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="5">No Languages Found</td>
            </tr>
        @endif
        </tbody>
    </table>
</div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-languages');
    </script>
@stop
