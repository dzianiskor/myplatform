@extends('layouts.dashboard')

@section('title')
    Update Language | BLU
@stop

@section('body')
    <div class="padding">

        <div class="content-head pure-g">
            <div class="title pure-u-1-2">
                <h1><i class="fa fa-globe fa-fw"></i>Language &raquo; {{{ $language->name }}}</h1>
            </div>

            <div class="controls pure-u-1-2">
                <a href="{{ url('/dashboard/languages') }}" class="pure-button">All Languages</a>
            </div>
        </div>

    {{ Form::model($language, array('action' => array('LanguageController@saveLanguage', base64_encode($language->id)),
                                   'class'  => 'pure-form pure-form-stacked form-width')) }}

    @include('partials.errors')
    @include('language/form', array('language' => $language))

    <!-- Form Buttons -->
        <div class="form-buttons">
            <div class="left">
                <a href="{{ url('dashboard/languages') }}" class="pure-button">Cancel</a>
            </div>
            <div class="right">
                <button type="submit" class="pure-button pure-button-primary">Save Language</button>
            </div>
            <div class="clearfix"></div>
        </div>
        <!-- /Form Buttons -->
        {{ Form::close() }}
        {{--- Keys ---}}
        <fieldset>
            <legend class="top-space">Key Locations</legend>

            <a href="#dialog-new-key" data-lang_id="{{ base64_encode($language->id) }}" class="btn-new-key top-space bottom-space" title="Add new key">
                <i class="fa fa-plus-circle fa-fw"></i>Add new key
            </a>

            <table id="langkey-list" class="pure-table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Key</th>
                    <th>Value</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @if(count($language->keys) > 0)
                    @foreach($language->keys()->orderBy('id', 'DESC')->get() as $key)
                        <tr>
                            <td>{{ $key->id }}</td>
                            <td>{{{ $key->key->key }}}</td>
                            <td>{{{ $key->value }}}</td>
                            <td>
                                <a href="{{ url('dashboard/langkey/edit/'.base64_encode($key->id)) }}" class="edit-langkey" title="Edit">Edit</a>
                                <a href="{{ url('dashboard/langkey/delete/'.base64_encode($key->id)) }}" class="delete-langkey" title="Delete">Delete</a>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr class="empty">
                        <td colspan="4">No key defined</td>
                    </tr>
                @endif
                </tbody>
            </table>

            <a href="#dialog-new-key" data-lang_id="{{ base64_encode($language->id) }}" class="btn-new-key top-space" title="Add new key">
                <i class="fa fa-plus-circle fa-fw"></i>Add new key
            </a>

        </fieldset>
        {{--- /Keys ---}}

    </div>
@stop

@section('page_script')
    @if(!I::can_edit('languages', $language))
        <!--<script>
     enqueue_script('disable-form');
 </script>-->
    @endif
    <script>
        set_menu('mnu-languages');
        enqueue_script('new-langkey');
    </script>
@stop
