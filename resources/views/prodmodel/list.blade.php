@extends('layouts.dashboard')

@section('title')
    Product Prodmodel | BLU
@stop

@section('body')
<div class="padding">

    <div class="content-head pure-g">
        <div class="title pure-u-1-2">
            <h1><i class="fa fa-sort-alpha-desc  fa-fw"></i>Product Prodmodel ({{ $prodmodels->count() }})</h1>
        </div>

        <div class="controls pure-u-1-2">
            <form action="{{ Filter::searchUrl() }}" method="get">
                <input type="text" placeholder="Search" name="q" value="{{ Input::get('q') }}" />
                <button type="submit" title="" class="list-search"></button>
            </form>
            @if(I::can('create_prodmodels'))
                <a href="{{ url('dashboard/prodmodels/new') }}" class="pure-button pure-button-primary">Add Prodmodel</a>
            @endif
        </div>
    </div>

    <table class="pure-table">
        <thead>
            <tr>
                <th width="50"><a href="{{ Filter::baseUrl('sort=id') }}">ID</a></th>
                <th style="text-align:left;padding-left:20px;"><a href="{{ Filter::baseUrl('sort=name') }}">Name</a></th>
                <th width="150">Actions</th>
            </tr>
        </thead>

        <tbody>
        @if(count($prodmodels) > 0)
            @foreach ($prodmodels as $prodmodel)
                    <tr>
                        <td>{{ $prodmodel->id }}</td>
                        <td style="text-align:left;padding-left:20px;">
                            @if(!empty($prodmodel->name))
                                {{{ $prodmodel->name }}}
                            @else 
                                <i>N/A</i>
                            @endif
                        </td>
                        
                        <td>
                            @if($prodmodel->id == 1)
                                <a href="{{ url('dashboard/prodmodels/view/'.$prodmodel->id) }}" class="edit" title="View">View</a>
                            @else
                                <a href="{{ url('dashboard/prodmodels/view/'.$prodmodel->id) }}" class="edit" title="View">View</a>

                                @if(I::can('delete_prodmodels'))
                                    <a href="{{ url('dashboard/prodmodels/delete/'.$prodmodel->id) }}" class="delete" title="Delete">Delete</a>
                                @endif
                            @endif
                        </td>
                    </tr>
            @endforeach
        @else
            <tr>
                <td colspan="4">No Prodmodel Found</td>
            </tr>
        @endif
        </tbody>
    </table>

    {{ $prodmodels->links() }}
</div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-prodmodels');
    </script>
@stop
