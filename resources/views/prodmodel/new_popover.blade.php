    {{ Form::open(array('url'    => url('dashboard/prodmodels/updatePopover/'.$prodmodel->id),
                        'method' => 'post',
                        'class'  => 'pure-form pure-form-stacked',
                        'id'     => 'new-prodmodel-form')) }}
    @include('prodmodel/form')
    <!-- Form Buttons -->
    <div class="form-buttons">
        <div class="left">
            <a href="#" class="pure-button close-dialog">Cancel</a>
        </div>
        <div class="right">
            <a href="#" class="pure-button pure-button-primary prodmodel-save">Save</a>
        </div>
        <div class="clearfix"></div>
    </div>
