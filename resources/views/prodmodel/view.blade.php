@extends('layouts.dashboard')

@section('title')
    Update Prodmodel | BLU
@stop

@section('body')
<div class="padding">

    <div class="content-head pure-g">
        <div class="title pure-u-1-2">
            <h1><i class="fa fa-sort-alpha-desc fa-fw"></i>Prodmodel &raquo; {{{ $prodmodel->name }}}</h1>
        </div>

        <div class="controls pure-u-1-2">
            <a href="{{ url('/dashboard/prodmodels') }}" class="pure-button">All Prodmodels</a>
        </div>
    </div>

    {{ Form::model($prodmodel, array('action' => array('ProdmodelController@saveProdmodel', $prodmodel->id),
                                   'class'  => 'pure-form pure-form-stacked form-width')) }}

        @include('prodmodel/form', array('prodmodel' => $prodmodel))

        <!-- Form Buttons -->
        <div class="form-buttons">
            <div class="left">
                <a href="{{ url('dashboard/prodmodels') }}" class="pure-button">Cancel</a>
            </div>
                <div class="right">
                    <button type="submit" class="pure-button pure-button-primary">Save Prodmodel</button>
                </div>
            <div class="clearfix"></div>
        </div>
        <!-- /Form Buttons -->

    {{ Form::close() }}
    {{--- Cattranslations ---}}
            <fieldset>
                <legend>Prodmodel Translation</legend>

                <table id="prodmodeltranslation-list" class="pure-table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Cattranslation Name</th>
                            <th>Language</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($prodmodel->prodmodeltranslations) > 0)
                            @foreach($prodmodel->prodmodeltranslations as $prodmodeltranslation)
                                <tr>
                                    <td>{{ $prodmodeltranslation->id }}</td>
                                    @if($prodmodeltranslation->draft)
                                        <td>{{{ $prodmodeltranslation->name }}} (Draft)</td>
                                    @else
                                        <td>{{{ $prodmodeltranslation->name }}}</td>
                                    @endif
                                    <td>
                                        <?php
                                            $lang = App\Language::where('id', $prodmodeltranslation->lang_id)->first();
                                            
                                            echo $lang->name;
                                        ?>
                                    </td>
                                    <td>
                                        @if(I::can_edit('partners', $prodmodel))
                                            <a href="{{ url('dashboard/prodmodeltranslation/edit/'.$prodmodeltranslation->id) }}" class="edit-prodmodeltranslation" title="Edit">Edit</a>
                                            <a href="{{ url('dashboard/prodmodeltranslation/delete/'.$prodmodeltranslation->id) }}" class="delete-prodmodeltranslation" title="Delete">Delete</a>
                                        @else
                                            N/A
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr class="empty">
                                <td colspan="4">No Prodmodel translation defined</td>
                            </tr>
                        @endif
                    </tbody>
                </table>

                
                    <a href="#dialog-new-prodmodeltranslation" data-prodmodel_id="{{ $prodmodel->id }}" class="btn-new-prodmodeltranslation top-space" title="Add new prodmodeltranslation location">
                        <i class="fa fa-plus-circle fa-fw"></i>Add new prodmodeltranslation
                    </a>
            </fieldset>
            {{--- /Cattranslations ---}}
</div>
@stop

@section('page_script')
@if(!I::can_edit('prodmodels', $prodmodel))
 <script>
     enqueue_script('disable-form');
 </script>
@endif
    <script>
        set_menu('mnu-prodmodels');
        enqueue_script('new-prodmodel');
    </script>
@stop
