@if(count($supplier->partners) > 0)
    @foreach($supplier->partners as $p)
        <li>
            {{ $p->name }}
            @if(I::can('delete_suppliers') || I::am("BLU Admin"))
                @if($p->id != 1)
                    <a href="/dashboard/partners/unlinksupplier/{{base64_encode($supplier->id ) }}/{{ base64_encode($p->id ) }}" class="delete-ajax" data-id="{{ $p->id }}" data-target="#partner-listing">
                        [Delete]
                    </a>
                @endif
            @endif
        </li>
    @endforeach
@else
    <li class="empty">No Partner Associated</li>
@endif
