{{-- Partner Listing --}}
<h3>
    @if(I::can_edit('suppliers', $supplier))
    <a href="#dialog-add-partner" title="New Partner" class="dialog">
        Partners <i class="fa fa-plus-circle fa-fw"></i>
    </a>
    @else
        Partners
    @endif
</h3>
<ul id="partner-listing">
    @include('supplier/list/partnerList', array('supplier'=>$supplier))
</ul>
<label class="error" style="display: none;">This field is required.</label>
{{-- /Partner Listing --}}
