@extends('layouts.dashboard')

@section('title')
    Suppliers | BLU
@stop

@section('body')
    <div class="padding">

        <div class="content-head pure-g">
            <div class="title pure-u-1-2">
                <h1><i class="fa fa-map-marker fa-fw"></i>Suppliers ({{ $suppliers->total() }})</h1>
            </div>

            <div class="controls pure-u-1-2">
                <form action="{{ Filter::searchUrl() }}" method="get">
                    <input type="text" placeholder="Search" name="q" value="{{ Input::get('q') }}" />
                    <button type="submit" title="" class="list-search"></button>
                </form>
                @if(I::can('create_suppliers'))
                    <a href="{{ url('dashboard/suppliers/new') }}" class="pure-button pure-button-primary">Add Supplier</a>
                @endif
            </div>


            <div id="catalog-filter" class="pure-u-1-2">
                <input id="filter_route" type="hidden" value="suppliers">
                <form id="filter-catalogue" action="{{ Filter::searchUrl() }}" method="get">

                    <div class="filter-block filter-block-pricing-width">
                        <span class="filter-label">Network:</span>
                        <select style="width:100%;" name="network_list[]" data-filter-catalogue="network_list" data-filter-catalogue-title="Select Network" multiple="multiple">
                            @foreach ($lists['networks'] as $key => $value)
                                <option value="{{ $key }}">{{ $value }}</option>
                            @endforeach
                        </select>
                        <input data-filter-catalogue="network_list" type="hidden" value="{{ Input::get('network_list') }}">
                    </div>

                    <div class="filter-block filter-block-pricing-width">
                        <span class="filter-label">Partner:</span>
                        <select style="width:100%;" name="partner_list[]" data-filter-catalogue="partner_list" data-filter-catalogue-title="Select Partner" multiple="multiple">
                            @foreach ($lists['partners'] as $key => $value)
                                <option value="{{ $key }}">{{ $value }}</option>
                            @endforeach
                        </select>
                        <input data-filter-catalogue="partner_list" type="hidden" value="{{ Input::get('partner_list') }}">
                    </div>

                    <div class="filter-block filter-block-pricing-width">
                        <span class="filter-label">Category:</span>
                        <select style="width:100%;" name="category_list[]" data-filter-catalogue="category_list" data-filter-catalogue-title="Select Category" multiple="multiple">
                            @foreach ($lists['categories'] as $key => $value)
                                <option value="{{ $key }}">{{ $value }}</option>
                            @endforeach
                        </select>
                        <input data-filter-catalogue="category_list" type="hidden" value="{{ Input::get('category_list') }}">
                    </div>

                    <div class="filter-block filter-block-pricing-width">
                        <span class="filter-label">Country:</span>
                        <select style="width:100%;" name="country_member_list[]" data-filter-catalogue="country_member_list" data-filter-catalogue-title="Select Country" multiple="multiple">
                            @foreach ($lists['countries'] as $key => $value)
                                <option value="{{ $key }}">{{ $value }}</option>
                            @endforeach
                        </select>
                        <input data-filter-catalogue="country_member_list" type="hidden" value="{{ Input::get('country_member_list') }}">
                    </div>

                    <div class="submit-filter-block disable-width">
                        <div style="float:right;">
                            <a href="<?php echo e(url('dashboard/suppliers')); ?>" title="Clear Filters" class="pure-button pure-button-primary">
                                Clear Filters <i class="fa fa-ban fa-fw"></i>
                            </a>
                        </div>
                        <button class="pure-button pure-button-primary" type="submit" title="" style="float:right;margin-right:15px;">
                            Submit Filters
                        </button>
                    </div>
                </form>
            </div>
        </div>

        <table class="supplier_list pure-table">
            <thead>
                <tr>
                    <th width="50"><a href="{{ Filter::baseUrl('sort=id') }}">ID</a></th>
                    <th width="90">Image</th>
                    <th style="text-align:left;padding-left:20px;"><a href="{{ Filter::baseUrl('sort=name') }}">Name</a></th>
                    <th><a href="{{ Filter::baseUrl('sort=status') }}">Enabled</a></th>
                    <th>Network</th>
                    <th>Category</th>
                    <th>Parent Supplier</th>
                    <th>Country</th>
                    <th width="150">Actions</th>
                </tr>
            </thead>

            <tbody>
                @if(count($suppliers) > 0)
                    @foreach($suppliers as $p)
                        <tr>
                            <td>
                                {{{ $p->id}}}
                            </td>
                            <td>
                                @if(empty($p->image))
                                    <img src="https://placehold.it/74x74" alt="" />
                                @else
                                    <img src="<?php echo App\Http\Controllers\MediaController::getImageFromId( $p->image ) ?>?s=74x74" alt="" />
                                @endif
                            </td>                            
                            </td>
                            <td style="text-align:left;padding-left:20px;">
                                <a href="/dashboard/suppliers/view/{{ base64_encode($p->id) }}" title="View Supplier">
                                    {{ ucwords($p->name) }}
                                </a>
                            </td>
                            <td>
                                @if($p->status == 'Enabled')
                                    <span class="ico-yes"></span>
                                @else
                                    <span class="ico-no"></span>
                                @endif
                            </td>
                            <td>
                                @if($p->networks->first())
                                    {{ $p->networks->first()->name }}
                                @else
                                    <i>N/A</i>
                                @endif
                            </td>
                            <td>
                                @if ($p->category)
                                    {{ $p->category->name }}
                                @else
                                    <i>N/A</i>
                                @endif
                            </td>
                            <td>
                                @if ($p->parent)
                                    {{ $p->parent->name }}
                                @else
                                    <i>N/A</i>
                                @endif
                            </td>
                            <td>
                                @if ($p->country)
                                    {{ $p->country->name }}
                                @else
                                    <i>N/A</i>
                                @endif
                            </td>                            
                            <td>
                                @if(I::can('view_suppliers'))
                                    <a href="/dashboard/suppliers/view/{{ base64_encode($p->id) }}" class="edit" title="View">View</a>
                                @endif

                                @if(I::can('delete_suppliers') && $p->id != 1)
                                    <a href="/dashboard/suppliers/delete/{{ base64_encode($p->id) }}" class="delete" title="Delete">Delete</a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="9">No Suppliers Found</td>
                    </tr>
                @endif
            </tbody>
        </table>

        {{ $suppliers->setPath('suppliers')->appends(Input::except('page'))->links() }}

    </div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-suppliers');
    </script>
@stop
