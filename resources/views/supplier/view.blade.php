@extends('layouts.dashboard')

@section('title')
    Supplier &raquo; {{ $supplier->name }}  | BLU
@stop

@section('dialogs')
    {{-- Assign new Partner --}}
    <div id="dialog-add-partner" style="width:300px;">
        <h4>Associate to Partner</h4>

        {{ Form::open(array('class' => "pure-form pure-form-stacked")) }}
        {{ Form::select('partner_id', $partners, null, array('class' => 'pure-input-1')) }}

        <a href="/dashboard/partners/attachsupplier/{{ base64_encode($supplier->id) }}" class="pure-button pure-button-primary attribute-action" data-target="#partner-listing">
            Save
        </a>
        {{ Form::close() }}
    </div>
    {{-- Assign new Network --}}
    @if(I::am('BLU Admin'))
        <div id="dialog-add-network" style="width:300px;">
            <h4>Associate to Network</h4>

            {{ Form::open(array('class' => "pure-form pure-form-stacked")) }}
            {{ Form::select('network_id', $networks, null, array('class' => 'pure-input-1')) }}

            <a href="/dashboard/suppliers/attach/network/{{ base64_encode($supplier->id)}}" class="pure-button pure-button-primary attribute-action" data-target="#network-listing">
                Save
            </a>
            {{ Form::close() }}
        </div>
    @endif
@stop

@section('body')
    <div class="padding">

        {{-- Page Header --}}
        <div class="content-head pure-g">
            <div class="title pure-u-1-2">
                <h1><i class="fa fa-map-marker fa-fw"></i>Supplier &raquo; {{{ $supplier->name }}}</h1>
            </div>

            <div class="controls pure-u-1-2">
                <a href="{{ url('dashboard/suppliers') }}" class="pure-button">All Suppliers</a>
            </div>
        </div>
        {{-- /Page Header --}}

        {{ Form::open(array('url' => url('dashboard/suppliers/update/'.base64_encode($supplier->id)), 'files' => true, 'method' => 'post', 'class' => "pure-form pure-form-stacked form-width", 'id' => 'supplierForm')) }}

        @include('partials.errors')

        {!! Form::hidden('redirect_to', URL::previous()) !!}

        <div class="pure-g">
            {{--- Left Section Content ---}}
            <div class="pure-u-3-5">
                <fieldset>
                    <legend>Basic Details</legend>

                    {{ Form::label('status', 'Status') }}
                    {{ Form::select('status', $statuses, $supplier->status, array('class' => 'pure-input-1-3')) }}

                    {{ Form::label('ucid', 'Unique Customer ID (Auto-generated)') }}
                    {{ Form::text('ucid', $supplier->ucid, array('class' => 'pure-input-1', 'maxlength' => 9, 'disabled' => 'disabled')) }}

                    {{ Form::label('name', 'Supplier Name') }}
                    {{ Form::text('name', $supplier->name, array('class' => 'pure-input-1 required')) }}

                    {{ Form::label('partner_id', 'Same as') }}
                    <?php $partners[0] = 'No partner selected'; ?>
                    {{ Form::select('partner_id', $partners, $supplier->partner_id, array('class' => 'pure-input-1 required')) }}

                    {{ Form::label('description', 'Supplier Description') }}
                    {{ Form::textarea('description', $supplier->description, array('class' => 'pure-input-1', 'rows' => 10)) }}

                    <div class="pure-g">
                        <div class="pure-u-1-2">
                            <div class="padding-right-10">
                                {{ Form::label('contact_name', 'Contact Name') }}
                                {{ Form::text('contact_name', $supplier->contact_name, array('class' => 'pure-input-1')) }}
                            </div>
                        </div>

                        <div class="pure-u-1-2">
                            {{ Form::label('contact_email', 'Contact Email') }}
                            {{ Form::text('contact_email', $supplier->contact_email, array('class' => 'pure-input-1')) }}
                        </div>

                        <div class="pure-u-1-2">
                            <div class="padding-right-10">
                                {{ Form::label('contact_number', 'Contact Number') }}
                                {{ Form::text('contact_number', $supplier->contact_number, array('class' => 'pure-input-1 enforceNumeric')) }}
                            </div>
                        </div>

                        <div class="pure-u-1-2">
                            {{ Form::label('contact_website', 'Contact Website') }}
                            {{ Form::text('contact_website', $supplier->contact_website, array('class' => 'pure-input-1')) }}
                        </div>

                        <div class="pure-u-1-2">
                            <div class="padding-right-10">
                                {{ Form::label('general_manager', 'General Manager') }}
                                {{ Form::text('general_manager', $supplier->general_manager, array('class' => 'pure-input-1')) }}
                            </div>
                        </div>

                        <div class="pure-u-1-2">
                            {{ Form::label('marketing_manager', 'Marketing Manager') }}
                            {{ Form::text('marketing_manager', $supplier->marketing_manager, array('class' => 'pure-input-1')) }}
                        </div>
                    </div>

                    @if($supplier->id != 1)
                        {{ Form::label('parent_id', 'Parent Supplier') }}
                        <select name="parent_id" class="pure-input-1-3 required" id="parent_id" data-draft="{{ $supplier->draft }}" data-id="{{ $supplier->id }}">
                            @if($supplier->parent)
                                @foreach($suppliers as $id => $name)
                                    @if($id != $supplier->id)
                                        @if($id == $supplier->parent->id)
                                            <option value="{{ $id }}" selected="selected">{{ $name }}</option>
                                        @else
                                            <option value="{{ $id }}">{{ $name }}</option>
                                        @endif

                                    @endif
                                @endforeach
                            @else
                                @foreach($suppliers as $id => $name)
                                    @if($id != $supplier->id)
                                        <option value="{{ $id }}">{{ $name }}</option>
                                    @endif
                                @endforeach
                            @endif
                        </select>
                    @endif

                    {{ Form::label('category_id', 'Supplier Category') }}
                    {{ Form::select('category_id', $categories, $supplier->category_id, array('class' => 'pure-input-1-3 required')) }}

                    <table class="supplier_view" style="margin-top:20px">
                        <tr>
                            <td width="80">
                                @if(empty($supplier->image))
                                    <img src="https://placehold.it/74x74" alt="" />
                                @else
                                    <img src="<?php echo App\Http\Controllers\MediaController::getImageFromId( $supplier->image ) ?>?s=74x74" alt="" />
                                @endif
                            </td>
                            <td>
                                {{ Form::label('image', 'Cover Image') }}
                                {{ Form::file('image') }}
                            </td>
                        </tr>
                    </table>
                </fieldset>

                <fieldset>
                    <legend>Address Details</legend>
                    {{ Form::label('country_id', 'Country') }}
                    {{ Markup::country_select('country_id', $supplier->country_id, array('class' => 'pure-input-1-3')) }}

                    {{ Form::label('area_id', 'Area') }}
                    {{ Markup::area_select('area_id', $supplier->country_id, $supplier->area_id, array('class' => 'pure-input-1-3')) }}

                    {{ Form::label('city_id', 'City') }}
                    {{ Markup::city_select('city_id', $supplier->area_id, $supplier->city_id, array('class' => 'pure-input-1-3')) }}

                    {{ Form::label('address_1', 'Address') }}
                    {{ Form::text('address_1', $supplier->address_1, array('class' => 'pure-input-1')) }}

                    {{ Form::label('address_2', 'Address (Continued)') }}
                    {{ Form::text('address_2', $supplier->address_2, array('class' => 'pure-input-1')) }}
                </fieldset>
            </div>
            {{--- /Left Section Content ---}}

            {{--- Right Section Content ---}}
            <div class="pure-u-2-5 attributes">
                <div class="padding-left-30">
                    {{-- Partner Listing --}}
                    @include('supplier/list/partner', array('supplier' => $supplier))
                    {{-- /Partner Listing --}}
                </div>
                <div class="padding-left-30">
                    {{-- Network Listing --}}
                    <h3>
                        @if(I::can_edit('suppliers', $supplier))
                            <a href="#dialog-add-network" title="New Network" class="dialog">
                                Networks <i class="fa fa-plus-circle fa-fw"></i>
                            </a>
                        @else
                            Networks
                        @endif
                    </h3>
                    <ul id="network-listing">
                        @include('supplier.networklist')
                    </ul>
                    {{-- /Network Listing --}}
                </div>
            </div>
            {{--- /Right Section Content ---}}
        </div>

        {{--- Stores ---}}
        <fieldset>
            <legend>Store Locations</legend>

            <table id="supplier-store-list" class="pure-table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Store Name</th>
                    <th>Coordinates</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @if(count($supplier->stores) > 0)
                    @foreach($supplier->stores as $supplierstore)
                        <tr>
                            <td>{{ $supplierstore->id }}</td>
                            @if($supplierstore->draft)
                                <td>{{ $supplierstore->name }} (Draft)</td>
                            @else
                                <td>{{ $supplierstore->name }}</td>
                            @endif
                            <td>
                                <a href="http://maps.google.com/maps?q={{ round($supplierstore->lat,6) }},{{ round($supplierstore->lng,6) }}&zoom=14" target="_blank">View</a>
                            </td>
                            <td>
                                @if(!I::can('delete_suppliers') && !I::can('edit_suppliers'))
                                    N/A
                                @endif
                                @if(I::can('edit_suppliers'))
                                    <a href="{{ url('dashboard/supplierstore/edit/'.$supplierstore->id) }}" class="edit-supplier-store" title="Edit">Edit</a>
                                @endif
                                @if(I::can('delete_suppliers'))
                                    <a href="{{ url('dashboard/supplierstore/delete/' . base64_encode($supplier->id) . '/' .base64_encode($supplierstore->id)) }}" class="delete-supplier-store" title="Delete">Delete</a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr class="empty">
                        <td colspan="4">No supplierstore locations defined</td>
                    </tr>
                @endif
                </tbody>
            </table>

            @if($supplier->draft)
                @if(I::can('create_suppliers'))
                    <a href="#dialog-new-supplier-store" data-supplier_id="{{ $supplier->id }}" class="btn-new-supplier-store top-space" title="Add new store location">
                        <i class="fa fa-plus-circle fa-fw"></i>Add new store location
                    </a>
                @endif
            @else
                @if(I::can('edit_suppliers'))
                    <a href="#dialog-new-supplier-store" data-supplier_id="{{ $supplier->id }}" class="btn-new-supplier-store top-space" title="Add new store location">
                        <i class="fa fa-plus-circle fa-fw"></i>Add new store location
                    </a>
                @endif
            @endif
        </fieldset>
        {{--- /Stores ---}}

        {{--- Form Buttons ---}}
        <div class="form-buttons">
            <div class="left">
                <a href="{{ url('dashboard/suppliers') }}" class="pure-button">Cancel</a>
            </div>

            @if($supplier->draft == true)
                @if(I::can('create_suppliers'))
                    <div class="right">
                        <button type="submit" class="pure-button pure-button-primary">Create Supplier</button>
                    </div>
                @endif
            @else
                @if(I::can('edit_suppliers'))
                    <div class="right">
                        <button type="submit" class="pure-button pure-button-primary">Save Supplier</button>
                    </div>
                @endif
            @endif

            <div class="clearfix"></div>
        </div>
        {{--- /Form Buttons ---}}

        {{ Form::close() }}
    </div>
@stop

@section('page_script')
    <script>
        $(document).on('click','.delete-ajax', function(e){
            e.preventDefault();

            _this = $(this);

            if(confirm("Are you sure you want to delete this item?")) {
                $.get(_this.attr('href'), function(data, status, xhr) {
                    if(_this.data('target')) {
                        $(_this.data('target')).html(data);
                    }
                });
            }
        });
    </script>

    <script src="https://maps.googleapis.com/maps/api/js?key={{ Config::get('google.maps_api_key') }}&sensor=true"></script>

    <script>
        set_menu('mnu-suppliers');

        enqueue_script('new-supplier');
    </script>
@stop