@extends('layouts.mobile_barcode')

@section('body')

    {{ Form::open(array('url'    => url('mobile_barcode/txtype/'),
                        'method' => 'post',
                        'class'  => 'pure-form pure-form-stacked page-padding')) }}

        {{ Form::hidden('member_id',   $member->id) }}
        {{ Form::hidden('amount',      $amount) }}
        {{ Form::hidden('currency',    $currency) }}
        {{ Form::hidden('coupon_code', $coupon_code) }}
        {{ Form::hidden('type',        'reward') }}

        <fieldset>
            <legend><strong>Member Details</strong></legend>

            <table>
                <tr>
                    <td>Name</td>
                    <td><strong>{{ $member->name }} | {{ $member->fullGender() }} / {{ $member->age() }}</strong></td>
                </tr>
                <tr>
                    <td>Mobile Number</td>
                    <td><strong>{{ $member->normalizedContactNumber() }}</strong></td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td><strong>{{ $member->email }}</strong></td>
                </tr>
                <tr>
                    <td>Balance</td>
                    <td><strong>{{ number_format($member->balance($partner->firstNetwork()->id)) }} Points</strong></td>
                </tr>
            </table>
        </fieldset>

        <fieldset>
            <legend><strong>Transaction Details</strong></legend>

            <table>
                <tr>
                    <td>Loyalty Program</td>
                    <td><strong>{{ $program_name }}</strong></td>
                </tr>
                <tr>
                    <td>Invoice Total Amount</td>
                    <td class="emp">
                        <strong>{{ number_format($amount, 2) }} {{ $currency }}</strong>
                    </td>
                </tr>
                <tr>
                    <td>Coupon value</td>
                    <td class="emp">
						@if($partner->id == 3)
							@if( $couponType == 'value')
								<strong>{{ number_format($couponPrice, 2) }} {{ $currency }}</strong>
							@else
								<strong>{{ number_format($couponPrice, 2) }}  % </strong>
							@endif
						@else
							<strong>{{ number_format($couponPrice, 2) }} {{ $currency }}</strong>
						@endif
                    </td>
                </tr>
            </table>

        </fieldset>

        <div class="checkout-buttons">

            <div class="pure-g">
                <div class="pure-u-1-4">
                    <a href="#" class="cancel-button-large" data-type="cancel">
                        Cancel<br />
                        Transaction
                    </a>
                </div>
                <div class="pure-u-1-4">
                    <div class="padding-lr">
                    @if($redeem_points <= $member->balance($partner->firstNetwork()->id))
                        <a href="#" class="redeem-button-large red" data-type="redeem" >
                            Redeem<br />(- {{ number_format($redeem_points) }} Pts)
                        </a>
                    @else
                        <a href="#" class="redeem-button-large disabled" data-type="redeem">
                            Redeem<br />(- {{ number_format($redeem_points) }} Pts)
                        </a>
                    @endif
                    </div>
                </div>
                <?php
                $partners_ids = ManagedObjectsHelper::managedPartners()->lists('id');
                if(!in_array('3', $partners_ids) || I::am("BLU Admin") || I::am('BLU Admin Dev')){?>
                    <div class="pure-u-1-4">
						<div class="padding-lr">
							<a href="#" class="reward-button-large" data-type="reward">
								Reward<br />(+ {{ number_format(round($reward_points)) }} Pts)
							</a>
						</div>
                </div>
                <?php
                 }
                ?>

                <?php
                $partners_ids = ManagedObjectsHelper::managedPartners()->lists('id');
                if(!in_array('3', $partners_ids) || I::am("BLU Admin") || I::am('BLU Admin Dev')){?>
                    <div class="pure-u-1-4">
						<div class="padding-lr">
							<a href="#" class="bonus-button-large" data-type="bonus">
								Bonus<br />(+ {{ number_format(round($bonusPoints)) }} Pts)
							</a>
							<input type="hidden" name="points" value="{{ round($bonusPoints) }}" />
						</div>
                </div>
                <?php
                 }
                ?>

            </div>

        </div>

    {{ Form::close() }}
@stop

@section('page_script')
    <script>
//        enqueue_script('mobile_barcode');

        $(function(){
            $('.checkout-buttons a').on('click', function(e){
                e.stopPropagation();
                e.preventDefault();

                if(!$(this).hasClass('disabled')) {
                    if($(this).data('type') == 'cancel') {
                        window.location.href = '/mobile_barcode';
                    } else {
                        $('input[name="type"]').val($(this).data('type'));
                        $(this).closest('form').submit();
                    }
                } else {
                    alert('This member balance is insufficient for redemptions');
                }
            });
        });
    </script>
@stop