@extends('layouts.mobile_barcode')

@section('body')
    {{ Form::open(array('url'    => url('mobile_barcode/txinfo/'),
                        'method' => 'post',
                        'class'  => 'pure-form pure-form-stacked')) }}

        <fieldset>
            <legend>Member Details</legend>
            
            <table>
                <tr>
                    <td>Name</td>
                    <td><strong>{{ $member->name }}</strong></td>
                    <td>Mobile Number</td>
                    <td><strong>{{ $member->normalizedContactNumber() }}</strong></td>
                </tr>                
                <tr>
                    <td>Email</td>
                    <td><strong>{{ $member->email }}</strong></td>
                    <td>Balance</td>
                    <td><strong>{{ number_format($member->balance()) }} Points</strong></td>
                </tr>                             
            </table>
        </fieldset>
                        
        <!-- REWARD -->
        <fieldset>
            <legend>To <strong>REWARD</strong> Points, please scan the first barcode in the field below: 
            </legend>
            
            {{ Form::label('barcode', 'Barcode', array('style'=>'display: inline-block !important;')) }}
            <label style="color: red; display: inline-block !important; font-size: 0.9em; vertical-align: middle;">
                    If this is a return, click here.
                    <input type="checkbox" name="return_item" id="return_item" value="1" style="vertical-align: middle;">
                </label>
            <input type="text" id="scan" name="reward_barcode" value="" class="pure-input-1 focus" />
        </fieldset>   
       <!-- END REWARD -->                 
                        
        <fieldset>
            <legend>To <strong>REDEEM</strong> Points, please scan the second barcode in the field below:</legend>
            {{ Form::label('barcode', 'Barcode') }}
            <input type="text" id="scan" name="redeem_barcode" class="pure-input-1" />
<!--            {{ Form::label('currency_id', 'Currency') }}
            <select name="currency_id" id="currency_id" class="pure-input-1">
                @foreach($currencies as $c)
                    @if($c->short_code == $mobile->currency_id)
                        <option value="{{ $c->short_code }}" selected="selected">{{ $c->name }}</option>
                    @else
                        <option value="{{ $c->short_code }}">{{ $c->name }}</option>
                    @endif
                @endforeach
            </select>

            {{ Form::label('amount', 'Amount') }}
            <input type="text" id="amount" name="amount" value="" placeholder="0" class="required pure-input-1" autocorrect="off" />
            
            {{ Form::label('points', 'Points') }}
            <input type="text" id="points" name="points" value="" onchange="calcAmount()" placeholder="0" class="required pure-input-1" autocorrect="off" />

            {{ Form::label('coupon_code', 'Coupon Code') }}
            {{ Form::text('coupon_code', null, array('class' => 'pure-input-1')) }}-->
        </fieldset>            

        {{ Form::hidden('member_id', $member->id) }}

        @include('mobile_barcode.partials.buttons', array('title' => 'Continue'))

    {{ Form::close() }}
@stop

@section('page_script')
<script>
//    enqueue_script('mobile_barcode');
    
    function calcAmount(){
        $('#amount').val(parseInt($('#points').val())/100);
        return;
    }
    
    $(function(){
            $('input.focus').focus();

            if(isPhoneGap()) {
                $('.doProceed').show();
            }
        });
</script>
@stop

