@extends('layouts.mobile_barcode')

@section('body')

    <div class="page-padding">
        <h2>Today's Store Transactions</h2>

        <table class="center pure-table">
            <thead>
                <th style="width: 50px" class="center">Tx id</th>
                <th style="text-align: left; padding-left: 15px">User</th>
                <th style="width: 150px" class="center">Amount</th>
            </thead>
            @if($transactions->count() > 0)
                @foreach($transactions as $transaction)
                    @if($transaction->voided)
                        <tr class="voided">
                    @else
                        <tr>
                    @endif
                        <td>
                            <a href="/mobile_barcode/transaction/{{$transaction->id}}">{{ $transaction->id }}</a>
                        </td>
                        <td style="text-align: left; padding-left: 15px">
                            <a href="/mobile_barcode/transaction/{{$transaction->id}}">{{ $transaction->user->getTitleAndFullName() }}</a>
                        </td>
                        <td>
                            @if($transaction->trx_reward == 1)
                                <a href="/mobile_barcode/transaction/{{$transaction->id}}">
                                    {{ number_format($transaction->original_total_amount, 2) }} {{ $transaction->currency->short_code }}
                                </a>
                            @else
                                <a href="/mobile_barcode/transaction/{{$transaction->id}}">
                                    -{{ $transaction->points_redeemed }} Pts
                                </a>
                            @endif
                        </td>
                    </tr>
                @endforeach
            @else 
                <tr>
                    <td colspan="3" class="empty" style="background-color:#fff">No Transactions Found</td>
                </tr>
            @endif
        </table>
    </div>

    {{ $transactions->links() }}    

    <!-- Form Buttons -->
    <div class="form-buttons">
        <div style="text-align:right">
            <a href="/mobile_barcode/print-transactions" class="pure-button pure-button-primary dark-button">{{ $title or 'Print' }}</a>
            &nbsp;
            <a href="/mobile_barcode" class="pure-button pure-button-primary dark-button">{{ $title or 'Go Back' }}</a>
        </div>
    </div>
    <!-- /Form Buttons -->

@stop

@section('page_script')
<script>
    enqueue_script('mobile_barcode');
</script>
@stop
