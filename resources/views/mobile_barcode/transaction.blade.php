@extends('layouts.mobile_barcode')

@section('body')

    {{ Form::open(array('url'     => url('/'),
                                  'method'  => 'post',
                                  'class'   => 'pure-form pure-form-stacked')) }}

        <fieldset>
            <legend>Transaction Details</legend>

            <table>
                @if($transaction->voided)
                <tr>
                    <td>
                        Status:
                    </td>
                    <td class="emp">
                        <strong>VOIDED</strong>
                    </td>
                </tr>
                @endif
                <tr>
                    <td>
                        Member Name:
                    </td>
                    <td>
                        <strong>{{ $transaction->user->getTitleAndFullName() }}</strong>
                    </td>
                </tr>
               <tr>
                    <td>
                        Mobile:
                    </td>
                    <td>
                        <strong>{{ $transaction->user->normalizedContactNumber() }}</strong>
                    </td>
                </tr>    
               <tr>
                    <td>
                        Email:
                    </td>
                    <td>
                        <strong>{{ $transaction->user->email }}</strong>
                    </td>
                </tr>     
               <tr>
                    <td>
                        Transaction Type:
                    </td>
                    <td>
                        @if($transaction->trx_reward == 1)
                            <strong>Reward (+{{ $transaction->points_rewarded }} Pts)</strong>
                        @else
                            <strong>Redeem (-{{ $transaction->points_redeemed }} Pts)</strong>
                        @endif
                    </td>
                </tr>    
               <tr>
                    <td>
                        Date &amp; Time:
                    </td>
                    <td>
                        <strong>{{ date("F j, Y, g:i a", strtotime($transaction->created_at)) }}</strong>
                    </td>
                </tr>                                                             
            </table>
        </fieldset>

        <!-- Form Buttons -->
        <div class="form-buttons">
            <div class="pure-g">
                <div class="pure-u-1-2 left">
                    <a href="{{ URL::previous() }}" class="pure-button pure-button-primary dark-button">Go back</a>
                </div>

                <!--<div class="pure-u-1-2 right">
                    @if(!$transaction->voided)
                        <a href="/mobile_barcode/voidtx/{{$transaction->id}}" class="pure-button pure-button-primary void redBtn">Void Transaction</a>
                    @endif
                </div>-->
            </div>
        </div>
        <!-- /Form Buttons -->

    {{  Form::Close() }}    
@stop

@section('page_script')
    <script>
        enqueue_script('mobile_barcode');

        $(function(){
            $('a.void').on('click', function(e){
                if(!confirm("Are you sure you want to void this transaction?")) {
                    e.stopPropagation();
                    e.preventDefault();
                }
            });
        }); 
    </script>
@stop
