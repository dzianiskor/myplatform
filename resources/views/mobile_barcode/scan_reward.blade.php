@extends('layouts.mobile_barcode')

@section('body')
    {{ Form::open(array('url' => url('mobile_barcode/rewardbyscan/'), 
                'method' => 'post', 
                'class' => 'pure-form pure-form-stacked')) }}

        <fieldset>
            <legend>Please scan the second barcode in the field below:</legend>

            {{ Form::text('scan', '', array('id' => 'scan', 'class' => 'pure-input-1')) }}
            {{ Form::hidden('pos', $pos , array('id' => 'pos', 'class' => 'pure-input-1')) }}
            {{ Form::hidden('partner', $partner , array('id' => 'partner', 'class' => 'pure-input-1')) }}
            {{ Form::hidden('invoice', $invoice , array('id' => 'invoice', 'class' => 'pure-input-1')) }}
            {{ Form::hidden('member_id', $member , array('id' => 'member_id', 'class' => 'pure-input-1')) }}
            {{ Form::hidden('invoice_barcode', $invoice_barcode , array('id' => 'invoice_barcode', 'class' => 'pure-input-1')) }}
            {{ Form::hidden('return', $return_item , array('id' => 'return', 'class' => 'pure-input-1')) }}

            <div class="center">
                <button id="doscan" class="pure-button pure-button-primary">Continue</button>
            </div>
        </fieldset>
        
        @include('mobile_barcode.partials.buttons_scan', array('title' => 'Continue'))

    {{ Form::Close() }}
@stop

@section('page_script')
    <script>
//        enqueue_script('mobile_barcode');

        $(function(){
            
            $('.doProceed').on('click',function(e){
                if($('#scan').val().length === 0){
                    alert('Please scan barcode to proceed.');
                    return false;
                }
            });
    
            $('input#scan').focus();

            if(isPhoneGap()) {
                $('#doscan').show();
            }
        });

    </script>
@stop


