@extends('layouts.mobile_barcode')

@section('body')
    {{ Form::open(array('url'    => url('mobile_barcode/txinfoom/'),
                        'method' => 'post',
                        'class'  => 'pure-form pure-form-stacked')) }}

        <fieldset>
            <legend>Member Details</legend>

            <table>
                <tr>
                    <td>Name</td>
                    <td><strong>{{ $member->name }}</strong></td>
                </tr>
                <tr>
                    <td>Mobile Number</td>
                    <td><strong>{{ $member->normalizedContactNumber() }}</strong></td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td><strong>{{ $member->email }}</strong></td>
                </tr>
				<tr>
                    <td>Balance</td>
                    <td><strong>{{ number_format($member->balance()) }} Points</strong></td>
                    <input type="hidden" id="currency_id" name="currency_id" value="USD" />
                </tr>
            </table>
        </fieldset>

        <fieldset>
            <legend>Transaction Details</legend>
            {{ Form::label('coupon_code', 'Coupon Code') }}
            {{ Form::text('coupon_code', null, array('class' => 'pure-input-1')) }}
<!--            <table>
                <tr>
                    <td>
                        {{ Form::label('Price', 'Price') }}
                    </td>
                    <td>
                        <label class="om-price-lable">
                            {{ Form::radio('amount', "10", null, array('class' => 'om-price-radio')) }} 1000 BLU Points (10$)
                        </label>
                        <label class="om-price-lable">
                            {{ Form::radio('amount', "20", null, array('class' => 'om-price-radio')) }} 2000 BLU Points (20$)
                        </label>
                        <label class="om-price-lable">
                            {{ Form::radio('amount', "30", null, array('class' => 'om-price-radio')) }} 3000 BLU Points (30$)
                        </label>
                        <label class="om-price-lable">
                            {{ Form::radio('amount', "50", null, array('class' => 'om-price-radio')) }} 5000 BLU Points (50$)
                        </label>
                        <label class="om-price-lable">
                            <a href="#" class="om-price-radio doProceed">Coupon</a>
                        </label>
                    </td>
                </tr>
            </table>-->
            <table>
                <tr>
                    <td>
                        {{ Form::label('Price', 'Price') }}
                    </td>
                    <td>
                        <label class="om-price-lable">
                            <!--{{ Form::radio('amount', "10", null, array('class' => 'om-price-radio')) }} 1000 BLU Points (10$)-->
                            @if($member->balance() >= 1000)
                                <a href="#" class="redeem-button-large om-redeem-button-large" data-value="10" >
                                    1000 BLU Points (10$)
                                </a>
                            @else
                                <a href="#" class="redeem-button-large disabled om-redeem-button-large" data-value="10" >
                                    1000 BLU Points (10$)
                                </a>
                            @endif
                        </label>
                        <label class="om-price-lable">
                            <!--{{ Form::radio('amount', "20", null, array('class' => 'om-price-radio')) }} 2000 BLU Points (20$)-->
                            @if($member->balance() >= 2000)
                                <a href="#" class="redeem-button-large om-redeem-button-large" data-value="20" >
                                    2000 BLU Points (20$)
                                </a>
                            @else
                                <a href="#" class="redeem-button-large disabled om-redeem-button-large" data-value="20" >
                                    2000 BLU Points (20$)
                                </a>
                            @endif
                        </label>
                        <label class="om-price-lable">
                            <!--{{ Form::radio('amount', "30", null, array('class' => 'om-price-radio')) }} 3000 BLU Points (30$)-->
                            @if($member->balance() >= 3000)
                                <a href="#" class="redeem-button-large om-redeem-button-large" data-value="30" >
                                    3000 BLU Points (30$)
                                </a>
                            @else
                                <a href="#" class="redeem-button-large disabled om-redeem-button-large" data-value="30" >
                                    3000 BLU Points (30$)
                                </a>
                            @endif
                        </label>
                        <label class="om-price-lable">
                            <!--{{ Form::radio('amount', "50", null, array('class' => 'om-price-radio')) }} 5000 BLU Points (50$)-->
                            @if($member->balance() >= 5000)
                                <a href="#" class="redeem-button-large om-redeem-button-large" data-value="50" >
                                    5000 BLU Points (50$)
                                </a>
                            @else
                                <a href="#" class="redeem-button-large disabled om-redeem-button-large" data-value="50" >
                                    5000 BLU Points (50$)
                                </a>
                            @endif
                        </label>
                        <label class="om-price-lable">
                            <a href="#" class="redeem-button-large om-redeem-button-large" data-value="0" >
                                Coupon
                            </a>
                        </label>
                    </td>
                </tr>
            </table>
            <input type="hidden" id="amount" name="amount" value="" />
<!--            {{ Form::label('amount', 'Amount') }}
            <input type="text" id="amount" name="amount" value="" placeholder="0" class="required pure-input-1" autocorrect="off" />

            {{ Form::label('points', 'Points') }}
            <input type="text" id="points" name="points" value="" onchange="calcAmount()" placeholder="0" class="required pure-input-1" autocorrect="off" />-->

        </fieldset>
        
        <!-- Form Buttons -->
        <div class="form-buttons">
            <div class="pure-g">
                <div class="pure-u-1-2 left">
                    <a href="/mobile_barcode" class="pure-button">Cancel</a>
                </div>
            </div>
        </div>
        {{ Form::hidden('member_id', $member->id) }}



    {{ Form::close() }}
@stop

@section('page_script')
<script>
	$(function(){
		$('.om-price-lable').on('click', function(e){
                    if($(this).find( 'a' ).data('value') == 0 && $('#coupon_code').val() == ''){
                        alert('No Coupon is provided.');
                        return;
                    }
                    if(!$(this).find( 'a' ).hasClass('disabled')) {
                    $('#amount').val($(this).find( 'a' ).data('value'));
                    $(this).find( 'a' ).closest('form').submit();  
                    } else {
                        alert('This member balance is insufficient for redemptions');
                    }
		});
	});
	$(function(){
		$('.om-price-radio').on('change', function(e){
                    $(this).closest('form').submit();
//			$('.om-price-lable').css('background', '#c93c49 !important');
//			$(this).css('background', '#3c99c9 !important');
		});
	});

    function calcAmount(){
        $('#amount').val(parseInt($('#points').val())/100);
        return;
    }

</script>

@stop
