@extends('layouts.mobile_barcode')

@section('body')
    {{ Form::open(array('url'    => url('mobile_barcode/txtype/'),
                        'method' => 'post',
                        'class'  => 'pure-form pure-form-stacked page-padding')) }}

          <fieldset>
            @if($couponPrice > 0 )
                <legend><strong>Coupon Details</strong></legend>
            @endif
            <table>
                @if(!empty($message) )
                <tr>
                    <td>Status:</td>
                    <td>
                        <img src="/img/no.png" alt="Failure" />
                    </td>
                </tr>
                @endif
                @if($couponPrice > 0 )
                <tr>
                    <td>Coupon value</td>
                    <td class="emp">
                        @if($partner_id == 3)
                                @if( $couponType == 'value')
                                        {{ number_format($couponPrice, 2) }} {{ $couponCurrency }}
                                @else
                                        {{ number_format($couponPrice, 2) }}  % 
                                @endif
                                @else
                                        {{ number_format($couponPrice, 2) }} {{ $couponCurrency }}
                                @endif
                    </td>
                </tr>
                @endif
                @if(!empty($message) )
                <tr>
                    <td>Message</td>
                    <td class="emp">
                        {{ $message }}
                    </td>
                </tr>
                @endif
            </table>
        </fieldset>
  
        <fieldset>
            <legend>Please provide your PIN code to proceed with redemption</legend>

            {{ Form::label('pin', 'PIN Code') }}    
            {{ Form::password('pin', array('class' => 'pure-input-1 enforceNumeric')) }}
        </fieldset>

        {{ Form::hidden('type',         $type) }}
        {{ Form::hidden('member_id',    $member->id) }}
        {{ Form::hidden('amount',       $amount) }}
        {{ Form::hidden('currency',     $currency) }}
        {{ Form::hidden('coupon_code',  $coupon_code) }}
        
        <!-- Form Buttons -->
        <div class="form-buttons">
            <div class="pure-g">
                <div class="pure-u-1-2 left">
                    <a href="/mobile_barcode" class="pure-button">Cancel</a>
                </div>

                <div class="pure-u-1-2 right">
                        <a href="#" id="send-pin" data-user="{{ $member->id }}" data-partner="{{ $partner_id }}" class="pure-button" style="margin-right:10px;">Send PIN</a>
                        <a href="#" class="pure-button pure-button-primary <?php echo ($error == 1) ? '' : 'doProceed'; ?>" <?php echo ($error == 1) ? 'disabled="disabled"' : ''; ?>>Proceed</a>
                </div>
            </div>
        </div>
        <!-- /Form Buttons -->
    {{ Form::close() }}
@stop

@section('page_script')
<script>
//    enqueue_script('mobile_barcode');
</script>
<script>
    // Handle the item redemption action
            $(document).on('click', '#send-pin', function(e){
                e.preventDefault();

                user_id = $(this).data('user');
                partner_id = $(this).data('partner');
                $.get('/dashboard/members/sendsms/' + user_id +"/"+ partner_id + "/pin_resend", function(resp){
                    NProgress.done();
                    var obj = JSON.parse( resp );
                    alert(obj.message);

                });
            });
</script>
@stop
