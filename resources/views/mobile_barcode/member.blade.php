@extends('layouts.mobile_barcode')

@section('body')
    {{ Form::model($member, array('url' => url("mobile_barcode/member/$member->id"), 'method' => 'post', 'class' => 'pure-form pure-form-stacked')) }}

        <fieldset>
            <legend>Member Details</legend>

            <div class="form-fields-wrapper">
            {{ Form::label('mobile', 'Mobile Number') }}
            {{ Form::text('mobile', $member->normalized_mobile, array('disabled' => 'disabled', 'class' => 'pure-input-1')) }}
            </div>
            <div class="form-fields-wrapper">
            {{ Form::label('card', 'Card') }}
            {{ Form::text('card', '', array('class' => 'pure-input-1')) }} 
            </div>
            <div class="form-fields-wrapper">
            {{ Form::label('first_name', 'First Name') }}
            {{ Form::text('first_name', $member->first_name, array('class' => 'pure-input-1')) }}
            </div>
            <div class="form-fields-wrapper">
            {{ Form::label('last_name', 'Last Name') }}
            {{ Form::text('last_name', $member->last_name, array('class' => 'pure-input-1')) }}
            </div>
            <div class="form-fields-wrapper">
            {{ Form::label('email', 'Email') }}
            {{ Form::text('email', $member->email, array('class' => 'pure-input-1')) }}
            </div>
            <div class="form-fields-wrapper">
            {{ Form::label('gender', 'Gender') }}
            {{ Form::select('gender', Meta::genderList(), $member->gender, array('class' => 'pure-input-1')) }}
            </div>
        </fieldset>

        @include('mobile_barcode.partials.buttons', array('title' => 'Proceed'))

        @if(isset($coupon_code))
            {{ Form::hidden('coupon_code', $coupon_code) }}
        @endif

        @if(isset($amount))
            {{ Form::hidden('amount',$amount) }}
        @endif

        @if(isset($currency))
            {{ Form::hidden('currency', $currency) }}
        @endif

    {{  Form::Close() }}
@stop

@section('page_script')
    <script>
        enqueue_script('mobile_barcode');
    </script>
@stop
