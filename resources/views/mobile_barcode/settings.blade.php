@extends('layouts.mobile_barcode')

@section('body')

    {{ Form::model($mobile, array('url'     => url('mobile_barcode/settings/'.$mobile->id),
                                  'method'  => 'post',
                                  'class'   => 'pure-form pure-form-stacked')) }}
        <fieldset>
            <legend>App Settings</legend>

            {{ Form::label('partner_id', 'Partner') }}
            {{ Form::select('partner_id', $partners, $mobile->partner_id, array('id'=>'partner', 'class' => 'pure-input-1')) }}

            {{ Form::label('store_id', 'Store') }}

			<span id="store-wrapper">
				<select name="store_id" id="store_id" class="pure-input-1">
					@foreach($stores as $s)
						@if($s->partner)
							@if($s->id == $mobile->store_id)
								<option value="{{ $s->id }}" selected="selected">{{ $s->partner->name }} > {{ $s->name }}</option>
							@else
								<option value="{{ $s->id }}">{{ $s->partner->name }} > {{ $s->name }}</option>
							@endif
						@endif
					@endforeach
				</select>
			</span>

            {{ Form::label('pos_id', 'Point of Sale') }}

			<span id="pos-wrapper">
				<select name="pos_id" id="pos_id" class="pure-input-1">
					@foreach($pos as $s)
						@if($s->id == $mobile->pos_id)
							<option value="{{ $s->id }}" selected="selected">{{ $s->name }}</option>
						@else
							<option value="{{ $s->id }}">{{ $s->name }}</option>
						@endif
					@endforeach
				</select>
			</span>
			
            {{ Form::label('country_id', 'Country Code') }}
            <select name="country_id" id="country_id" class="pure-input-1">
                <?php foreach(Meta::countries() as $c): ?>
                    @if($c->telephone_code == $mobile->country_id)
                        <option value="<?php echo $c->telephone_code; ?>" selected="selected">
                            <?php echo $c->name; ?> (<?php echo $c->telephone_code; ?>)
                        </option>
                    @else
                        <option value="<?php echo $c->telephone_code; ?>">
                            <?php echo $c->name; ?> (<?php echo $c->telephone_code; ?>)
                        </option>
                    @endif
                <?php endforeach; ?>
            </select>
            
            <!--{{ Form::label('currency_id', 'Currency') }}-->
            <select name="currency_id" id="currency_id" class="pure-input-1" style="display: none">
                @foreach($currencies as $c)
                    @if($c->short_code == $mobile->currency_id)
                        <option value="{{ $c->short_code }}" selected="selected">{{ $c->name }}</option>
                    @else
                        <option value="{{ $c->short_code }}">{{ $c->name }}</option>
                    @endif

                @endforeach
            </select>

        </fieldset>

        @include('mobile_barcode.partials.buttons', array('title' => 'Save Settings'))

    {{  Form::Close() }}
@stop

@section('page_script')
    <script>
        enqueue_script('mobile_barcode');
    </script>
@stop
