<!-- Form Buttons -->
<div class="form-buttons">
	<div class="right">
		<a href="{{ URL::previous() }}" class="pure-button pure-button-primary dark-button">{{ $title or 'Go Back' }}</a>
	</div>  
</div>
<!-- /Form Buttons -->


    