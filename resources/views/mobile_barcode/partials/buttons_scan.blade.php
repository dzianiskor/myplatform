<!-- Form Buttons -->
<div class="form-buttons">
    <div class="pure-g">
        <div class="pure-u-1-2 left">
            <a href="/mobile_barcode" class="pure-button">Cancel</a>
        </div>

        <div class="pure-u-1-2 right">
                <button id="doscan" class="pure-button pure-button-primary doProceed" style="display: block !important; float: right;">Continue</button>
        </div>
    </div>
</div>
<!-- /Form Buttons -->


