<!-- Form Buttons -->
<div class="form-buttons">
    <div class="pure-g">
        <div class="pure-u-1-2 left">
            <a href="/mobile_barcode" class="pure-button">Cancel</a>
        </div>

        <div class="pure-u-1-2 right">
        	<a href="#" class="pure-button pure-button-primary doProceed">{{ $title or 'Save' }}</a>
        </div>
    </div>
</div>
<!-- /Form Buttons -->


