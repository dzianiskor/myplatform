<fieldset>
    <legend>Basic Details</legend>
        
    {{ Form::label('name', 'Airport Name', array('for'=>'name')) }}
    {{ Form::text('name', Input::old('name'), array('class'=>'pure-input-1 required')) }}

    {{ Form::label('code', 'Code', array('for'=>'code')) }}
    {{ Form::text('code', Input::old('code'), array('class'=>'pure-input-1 required')) }}

    {{ Form::label('city', 'City', array('for'=>'city')) }}
    {{ Form::text('city', Input::old('city'), array('class'=>'pure-input-1 required')) }}

    {{ Form::label('state', 'State', array('for'=>'state')) }}
    {{ Form::text('state', Input::old('state'), array('class'=>'pure-input-1 required')) }}
    
    {{ Form::label('country', 'Country', array('for'=>'country')) }}
    {{ Form::text('country', Input::old('country'), array('class'=>'pure-input-1 required')) }}
    
    {{ Form::label('lat', 'lat', array('for'=>'lat')) }}
    {{ Form::text('lat', Input::old('lat'), array('class'=>'pure-input-1')) }}
    
    {{ Form::label('lon', 'lon', array('for'=>'lon')) }}
    {{ Form::text('lon', Input::old('lon'), array('class'=>'pure-input-1')) }}
    
    {{ Form::label('woeid', 'Woeid', array('for'=>'woeid')) }}
    {{ Form::text('woeid', Input::old('woeid'), array('class'=>'pure-input-1')) }}
    
    {{ Form::label('tz', 'TZ', array('for'=>'tz')) }}
    {{ Form::text('tz', Input::old('tz'), array('class'=>'pure-input-1')) }}
    
    {{ Form::label('phone', 'Phone', array('for'=>'phone')) }}
    {{ Form::text('phone', Input::old('phone'), array('class'=>'pure-input-1')) }}
    
    {{ Form::label('type', 'Type', array('for'=>'type')) }}
    {{ Form::text('type', Input::old('type'), array('class'=>'pure-input-1')) }}
    
    {{ Form::label('email', 'Email', array('for'=>'email')) }}
    {{ Form::text('email', Input::old('email'), array('class'=>'pure-input-1')) }}
    
    {{ Form::label('url', 'URL', array('for'=>'url')) }}
    {{ Form::text('url', Input::old('url'), array('class'=>'pure-input-1')) }}
    
    {{ Form::label('runway_length', 'Runway Length', array('for'=>'runway_length')) }}
    {{ Form::text('runway_length', Input::old('runway_length'), array('class'=>'pure-input-1')) }}
    
    {{ Form::label('elev', 'Elev', array('for'=>'elev')) }}
    {{ Form::text('elev', Input::old('elev'), array('class'=>'pure-input-1')) }}
    
    {{ Form::label('icao', 'ICAO', array('for'=>'icao')) }}
    {{ Form::text('icao', Input::old('icao'), array('class'=>'pure-input-1')) }}
    
    {{ Form::label('direct_flights', 'Direct Flights', array('for'=>'direct_flights')) }}
    {{ Form::text('direct_flights', Input::old('direct_flights'), array('class'=>'pure-input-1')) }}
    
    {{ Form::label('carriers', 'Carriers', array('for'=>'carriers')) }}
    {{ Form::text('carriers', Input::old('carriers'), array('class'=>'pure-input-1')) }}
    
</fieldset>
