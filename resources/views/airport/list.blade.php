@extends('layouts.dashboard')

@section('title')
    Airports | BLU
@stop

@section('body')
<div class="padding">

    <div class="content-head pure-g">
        <div class="title pure-u-1-2">
            <h1><i class="fa fa-globe fa-fw"></i>Airports ({{ $airports->total() }})</h1>
        </div>

        <div class="controls pure-u-1-2">
            <form action="{{ Filter::searchUrl() }}" method="get">
                <input type="text" placeholder="Search" name="q" value="{{ Input::get('q') }}" />
                <button type="submit" title="" class="list-search"></button>
            </form>
            @if(I::am('BLU Admin'))
                <a href="{{ url('dashboard/airports/new') }}" class="pure-button pure-button-primary">Add Airport</a>
            @endif
        </div>
    </div>

    <table class="pure-table">
        <thead>
            <tr>
                <th width="50"><a href="{{ Filter::baseUrl('sort=id') }}">ID</a></th>
                <th style="text-align:left;padding-left:20px;"><a href="{{ Filter::baseUrl('sort=name') }}">Name</a></th>
                <th><a href="{{ Filter::baseUrl('sort=code') }}">Code</a></th>
                <th>City</th>
                <th>State</th>
                <th>Country</th>
                <th width="150">Actions</th>
            </tr>
        </thead>

        <tbody>
        @if(count($airports) > 0)
            @foreach ($airports as $airport)
                <tr>
                    <td>
                        {{{ $airport->id }}}
                    </td>
                    <td style="text-align:left;padding-left:20px;">
                        <a href="{{ url('dashboard/airports/view/'.$airport->id) }}" class="edit" title="Edit">{{{ $airport->name }}}</a>
                    </td>
<!--                    <td>
                        {{{ $airport->name}}}
                    </td>-->
                    <td>
                        {{{ $airport->code }}}
                    </td>
                    <td>
                        {{{ $airport->city }}}
                    </td>
                    <td>
                        {{{ $airport->state }}}
                    </td>
                    <td>
                        {{{ $airport->country }}}
                    </td>
                    <td>
                        <a href="{{ url('dashboard/airports/view/'.$airport->id) }}" class="edit" title="View">View</a>

                         @if(I::am('BLU Admin'))
                            <a href="{{ url('dashboard/airports/delete/'.$airport->id) }}" class="delete" title="Delete">Delete</a>
                        @endif
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="7">No Airports Found</td>
            </tr>
        @endif
        </tbody>
    </table>

    {{ $airports->setPath('airports')->appends(Input::except('page'))->links() }}

</div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-airports');
    </script>
@stop
