@extends('layouts.dashboard')

@section('title')
    Update Airport | BLU
@stop

@section('body')
    <div class="padding">

        <div class="content-head pure-g">
            <div class="title pure-u-1-2">
                <h1><i class="fa fa-globe fa-fw"></i>Airport &raquo; {{{ $airport->name }}}</h1>
            </div>

            <div class="controls pure-u-1-2">
                <a href="{{ url('/dashboard/airports') }}" class="pure-button">All Airports</a>
            </div>
        </div>

        {{ Form::model($airport, array('action' => array('AirportController@saveAirport', $airport->id),
                                       'class'  => 'pure-form pure-form-stacked form-width')) }}

            @include('partials.errors')                                       
            @include('airport/form', array('airport' => $airport))

            {!! Form::hidden('redirect_to', URL::previous()) !!}

            <!-- Form Buttons -->
            <div class="form-buttons">
                <div class="left">
                    <a href="{{ url('dashboard/airports') }}" class="pure-button">Cancel</a>
                </div>
                <div class="right">
                    <button type="submit" class="pure-button pure-button-primary">Save Airport</button>
                </div>
                <div class="clearfix"></div>
            </div>
            <!-- /Form Buttons -->
        {{ Form::close() }}        
    </div>
@stop

@section('page_script')
@if(!(I::am('BLU Admin')))
 <script>
     enqueue_script('disable-form');
 </script>
@endif
    <script>
        set_menu('mnu-airports');
    </script>
@stop
