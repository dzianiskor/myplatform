
{{ Form::open(array('url' => url('dashboard/store/update/'.base64_encode($store->id)), 'method' => 'post', 'class' => "pure-form pure-form-stacked", 'id' => 'new-store-form')) }}

    @include('partials.errors')

    <div class="pure-g">
        <input type="hidden" name="store_id" id="store_id" value="<?php echo $store->id; ?>" ?>
        <div class="pure-u-1-3">
            {{ Form::label('store_name', 'Store Name') }}
            {{ Form::text('store_name', $store->name, array('class' => 'pure-input-1 required')) }}
            <div class="pure-u-1-1">
                {{ Markup::checkbox('display_online', 'Display Online', $store->display_online) }}
            </div>

            {{ Form::label('mapping_id', 'Mapping ID') }}
            {{ Form::text('mapping_id', $store->mapping_id, array('class' => 'pure-input-1')) }}

            {{ Form::label('store_country', 'Country') }}
            {{ Markup::country_select('store_country', $store->address->country_id, array('class' => 'pure-input-1-2')) }}

            {{ Form::label('store_area', 'Area') }}
            {{ Markup::area_select('store_area', $store->address->country_id, $store->address->area_id, array('class' => 'pure-input-1-2')) }}

            {{ Form::label('store_city', 'City') }}
            {{ Markup::city_select('store_city', $store->address->area_id, $store->address->city_id, array('class' => 'pure-input-1-2')) }}

            {{ Form::label('store_street', 'Street') }}
            {{ Form::text('store_street', $store->address->street, array('class' => 'pure-input-1')) }}

            {{ Form::label('store_floor', 'Floor') }}
            {{ Form::text('store_floor', $store->address->floor, array('class' => 'pure-input-1')) }}

            {{ Form::label('store_building', 'Building') }}
            {{ Form::text('store_building', $store->address->building, array('class' => 'pure-input-1')) }}
        </div>

        <div class="pure-u-2-3">
            <div class="padding-left-60">
                <div class="map-canvas" id="map"></div>

                <div class="pure-g">
                    <div class="pure-u-1-2">
                        <div class="padding-right-10">
                            {{ Form::label('store_latitude', 'Latitude') }}
                            {{ Form::text('store_latitude', $store->lat, array('class' => 'pure-input-1')) }}
                        </div>
                    </div>
                    <div class="pure-u-1-2">
                        {{ Form::label('store_longitude', 'Longitude') }}
                        {{ Form::text('store_longitude', $store->lng, array('class' => 'pure-input-1')) }}
                    </div>
                </div>

                <h5 class="section-title">Point of Sale</h5>

                <table id="pos-table">
                    @if(count($store->pointOfSales) > 0)
                        @foreach ($store->pointOfSales as $pos)
                            <tr>
                                <td style="padding-right:10px;">
                                    {{ Form::text('pos_name[]', $pos->name, array('class' => 'pure-input-1', 'placeholder' => 'Name', 'style' => 'width:170px;', 'required' => 'required')) }}
                                </td>
                                <td style="padding-right:10px; display: none;">
                                    {{ Form::text('pos_id[]', $pos->id, array('class' => 'pure-input-1', 'placeholder' => 'pos_id', 'style' => 'width:170px;', 'required' => 'required')) }}
                                </td>
                                <td>
                                    {{ Form::text('pos_mapping[]', $pos->mapping_id, array('class' => 'pure-input-1', 'placeholder' => 'Mapping ID', 'style' => 'width:170px;')) }}
                                </td>
                                <td>
                                    <a href="#" class="delete" title="Delete">
                                        <i class="fa fa-trash-o fa-fw"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td style="padding-right:10px;">
                                {{ Form::text('pos_name[]', null, array('class' => 'pure-input-1', 'placeholder' => 'Name (Required)', 'style' => 'width:170px;', 'required' => 'required')) }}
                            </td>
                            <td>
                                {{ Form::text('pos_mapping[]', null, array('class' => 'pure-input-1', 'placeholder' => 'Mapping ID', 'style' => 'width:170px;')) }}
                            </td>
                            <td>
                                <a href="#" class="delete" title="Delete">
                                    <i class="fa fa-trash-o fa-fw"></i>
                                </a>
                            </td>
                        </tr>
                   @endif
                </table>

                <a href="#" title="Add P.O.S" class="add-pos top-space">
                    <i class="fa fa-plus-circle fa-fw"></i>Add new Point of Sale
                </a>
            </div>
        </div>
    </div>

    {{-- Dialog Buttons --}}
    <div class="form-buttons">
        <div class="left">
            <a href="#" class="pure-button close-dialog">Cancel</a>
        </div>
        <div class="right">
            <a href="#" class="pure-button pure-button-primary save-new-store">Save</a>
        </div>
        <div class="clearfix"></div>
    </div>
    {{-- /Dialog Buttons --}}

{{ Form::close() }}