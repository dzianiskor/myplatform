@if(count($stores) > 0)
  @foreach($stores as $store)
    <tr>
        <td>{{ $store->id }}</td>
        <td>{{ $store->name }}</td>
        <td>
          <a href="http://maps.google.com/maps?q={{ round($store->lat,6) }},{{ round($store->lng,6) }}" target="_blank">View</a>
        </td>
        <td>
           <a href="{{ url('dashboard/store/edit/'.base64_encode($store->id)) }}" class="edit-store" title="Edit">Edit</a>
           <a href="{{ url('dashboard/store/delete/'.base64_encode($store->id)) }}" class="delete-store" title="Delete">Delete</a>
        </td>
    </tr>
  @endforeach
@else
    <tr class="empty">
        <td colspan="4">No store locations defined</td>
    </tr>
@endif
