@extends('layouts.dashboard')

@section('title')
    Carriers | BLU
@stop

@section('body')
<div class="padding">

    <div class="content-head pure-g">
        <div class="title pure-u-1-2">
            <h1><i class="fa fa-globe fa-fw"></i>Carriers ({{ $carriers->total() }})</h1>
        </div>

        <div class="controls pure-u-1-2">
            <form action="{{ Filter::searchUrl() }}" method="get">
                <input type="text" placeholder="Search" name="q" value="{{ Input::get('q') }}" />
                <button type="submit" title="" class="list-search"></button>
            </form>
            @if(I::am('BLU Admin'))
                <a href="{{ url('dashboard/carriers/new') }}" class="pure-button pure-button-primary">Add Carrier</a>
            @endif
        </div>
    </div>

    <table class="pure-table">
        <thead>
            <tr>
                <th width="50"><a href="{{ Filter::baseUrl('sort=id') }}">ID</a></th>
                <th style="text-align:left;padding-left:20px;"><a href="{{ Filter::baseUrl('sort=name') }}">Name</a></th>
                <th>IATA Code</th>
                <th>ICAO Code</th>
                 <th>Callsign</th>
                <th>Country</th>
                <th>Active</th>
                <th width="150">Actions</th>
            </tr>
        </thead>

        <tbody>
        @if(count($carriers) > 0)
            @foreach ($carriers as $carrier)
                <tr>
                    <td>
                        {{{ $carrier->id }}}
                    </td>
                    <td style="text-align:left;padding-left:20px;">
                        <a href="{{ url('dashboard/carriers/view/'.$carrier->id) }}" class="edit" title="Edit">{{{ $carrier->name }}}</a>
                    </td>
                    <td>
                        {{{ $carrier->iata_code }}}
                    </td>
                    <td>
                        {{{ $carrier->icao_code }}}
                    </td>
                    <td>
                        {{{ $carrier->callsign }}}
                    </td>
                    <td>
                        {{{ $carrier->country }}}
                    </td>
                    <td>
                        {{{ $carrier->is_active }}}
                    </td>                    
                    
                    <td>
                        <a href="{{ url('dashboard/carriers/view/'.$carrier->id) }}" class="edit" title="View">View</a>
                         @if(I::am('BLU Admin'))
                        <a href="{{ url('dashboard/carriers/delete/'.$carrier->id) }}" class="delete" title="Delete">Delete</a>
                        @endif
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="8">No Carriers Found</td>
            </tr>
        @endif
        </tbody>
    </table>

    {{ $carriers->setPath('carriers')->appends(Input::except('page'))->links() }}

</div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-carriers');
    </script>
@stop
