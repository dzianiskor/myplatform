@extends('layouts.dashboard')

@section('title')
    Update Carrier | BLU
@stop

@section('body')
    <div class="padding">

        <div class="content-head pure-g">
            <div class="title pure-u-1-2">
                <h1><i class="fa fa-globe fa-fw"></i>Carrier &raquo; {{{ $carrier->name }}}</h1>
            </div>

            <div class="controls pure-u-1-2">
                <a href="{{ url('/dashboard/carriers') }}" class="pure-button">All Carriers</a>
            </div>
        </div>

        {{ Form::model($carrier, array('action' => array('CarrierController@saveCarrier', $carrier->id),
                                       'class'  => 'pure-form pure-form-stacked form-width')) }}

            @include('partials.errors')                                       
            @include('carrier/form', array('carrier' => $carrier))

            {!! Form::hidden('redirect_to', URL::previous()) !!}

            <!-- Form Buttons -->
            <div class="form-buttons">
                <div class="left">
                    <a href="{{ url('dashboard/carriers') }}" class="pure-button">Cancel</a>
                </div>
                <div class="right">
                    <button type="submit" class="pure-button pure-button-primary">Save Carrier</button>
                </div>
                <div class="clearfix"></div>
            </div>
            <!-- /Form Buttons -->
        {{ Form::close() }}        
    </div>
@stop

@section('page_script')
@if(!(I::am('BLU Admin')))
 <script>
     enqueue_script('disable-form');
 </script>
@endif
    <script>
        set_menu('mnu-carriers');
    </script>
@stop
