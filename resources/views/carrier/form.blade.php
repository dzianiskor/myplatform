<fieldset>
    <legend>Basic Details</legend>
        
    {{ Form::label('name', 'Carrier Name', array('for'=>'name')) }}
    {{ Form::text('name', Input::old('name'), array('class'=>'pure-input-1 required')) }}

    {{ Form::label('iata_code', 'IATA Code', array('for'=>'iata_code')) }}
    {{ Form::text('iata_code', Input::old('iata_code'), array('class'=>'pure-input-1')) }}

    {{ Form::label('icao_code', 'City', array('for'=>'icao_code')) }}
    {{ Form::text('icao_code', Input::old('icao_code'), array('class'=>'pure-input-1')) }}

    {{ Form::label('alternative_name', 'Alternative Name', array('for'=>'alternative_name')) }}
    {{ Form::text('alternative_name', Input::old('alternative_name'), array('class'=>'pure-input-1')) }}
   
    {{ Form::label('callsign', 'Callsign', array('for'=>'callsign')) }}
    {{ Form::text('callsign', Input::old('callsign'), array('class'=>'pure-input-1')) }}
    
    {{ Form::label('country', 'Country', array('for'=>'country')) }}
    {{ Form::text('country', Input::old('country'), array('class'=>'pure-input-1 required')) }}
    
    {{ Form::label('is_active', 'Is Active Y/N', array('for'=>'is_active')) }}
    {{ Form::text('is_active', Input::old('is_active'), array('class'=>'pure-input-1')) }}
</fieldset>
