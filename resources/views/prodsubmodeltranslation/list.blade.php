@if(count($prodsubmodeltranslations) > 0)
  @foreach($prodsubmodeltranslations as $prodsubmodeltranslation)
    <tr>
        <td>{{ $prodsubmodeltranslation->id }}</td>
        <td>{{ $prodsubmodeltranslation->name }}</td>
        <?php
            $language = App\Language::find($prodsubmodeltranslation->lang_id);
            echo "<td>" . $language->name ."</td>";
        ?>

        <td>
           <a href="{{ url('dashboard/prodsubmodeltranslation/edit/'.$prodsubmodeltranslation->id) }}" class="edit-prodsubmodeltranslation" title="Edit">Edit</a>
           <a href="{{ url('dashboard/prodsubmodeltranslation/delete/'.$prodsubmodeltranslation->id) }}" class="delete-prodsubmodeltranslation" title="Delete">Delete</a>
        </td>
    </tr>
  @endforeach
@else
    <tr class="empty">
        <td colspan="4">No prodsubmodeltranslation defined</td>
    </tr>
@endif
