
{{ Form::open(array('url' => url('dashboard/prodsubmodeltranslation/update/'.$prodsubmodeltranslation->id), 'method' => 'post', 'class' => "pure-form pure-form-stacked", 'id' => 'new-prodsubmodeltranslation-form')) }}
    
    @include('partials.errors')                         

    <div class="pure-g">
        <div class="pure-u-1-3">
            {{ Form::label('prodsubmodeltranslation_lang', 'Language') }}
            {{ Form::select('prodsubmodeltranslation_lang',$languages, array('class' => 'pure-input-1-2')) }}
            
            {{ Form::label('prodsubmodeltranslation_name', 'Name') }}
           
            
            
        </div>
 {{ Form::text('prodsubmodeltranslation_name', $prodsubmodeltranslation->name, array('class' => 'pure-input-1 required')) }}
    </div>

    {{-- Dialog Buttons --}}
    <div class="form-buttons">
        <div class="left">
            <a href="#" class="pure-button close-dialog">Cancel</a>
        </div>
        <div class="right">
            <a href="#" class="pure-button pure-button-primary save-new-prodsubmodeltranslation">Save</a>
        </div>
        <div class="clearfix"></div>
    </div>
    {{-- /Dialog Buttons --}}

{{ Form::close() }}