@extends('layouts.empty')

@section('title')
    Admin Password Reset | BLU
@stop

@section('body')
    <div class="single-login-form hasBackground">

        {{ Form::open(array('url' => route('password.remind'), "parsley-validate" => true, "class" => 'pure-form', 'autocomplete' => 'off')) }}

            @include('partials.errors')

            <fieldset class="single-fieldset">
                {{ Form::text('email', Input::old('email'), array('class'=>'pure-input-1 required', 'placeholder' => 'Your email address')) }}
            </fieldset>

            {{ Form::submit('Recover Account', array('class' => 'pure-button pure-input-1 pure-button-primary')) }}

        {{ Form::close() }}

        <a href="/auth/login" class="single-nav-link">I remembered my details</a>
    </div>
@stop

@section('page_script')
    <script src="{{ asset('js/vendor/parsley.min.js') }}"></script>
@stop