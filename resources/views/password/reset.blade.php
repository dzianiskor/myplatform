@extends('layouts.empty')

@section('title')
    Admin Password Reset | BLU
@stop

@section('body')
    <div class="single-login-form hasBackground">

        {{ Form::open(array('action' => 'RemindersController@postReset', "parsley-validate" => true, "class" => 'pure-form', 'autocomplete' => 'off')) }}

            @include('partials.errors')

            <input type="hidden" name="token" value="{{ $token }}">

            <fieldset class="pure-group single-fieldset">
                <input type="text" name="email" class="pure-input-1 required" value="{{ Input::old('email') }}" placeholder="Your email address" />
                <input type="password" name="password" class="pure-input-1 required" value="{{ Input::old('password') }}" placeholder="New password" />
                <input type="password" name="password_confirmation" class="pure-input-1 required" value="{{ Input::old('password_confirmation') }}" placeholder="Confirm new password" />
            </fieldset>

            {{ Form::submit('Reset Password', array('class' => 'pure-button pure-input-1 pure-button-primary')) }}

        {{ Form::close() }}

        <a href="/auth/login" class="single-nav-link">Take me to the log in page</a>
    </div>
@stop

@section('page_script')
    <script src="{{ asset('js/vendor/parsley.min.js') }}"></script>
@stop