@extends('layouts.dashboard')

@section('title')
    Mobile Game Ads | BLU
@stop

@section('body')
<div class="padding">

    <div class="content-head pure-g">
        <div class="title pure-u-1-2">
            <h1><i class="fa fa-money fa-fw"></i> Mobile Game Ads ({{ count($mobilegameads) }})</h1>
        </div>

        <div class="controls pure-u-1-2">
            <form action="{{ Filter::searchUrl() }}" method="get">
                <input type="text" placeholder="Search" name="q" value="{{ Input::get('q') }}" />
                <button type="submit" title="" class="list-search"></button>
            </form>
            @if(I::can('create_mobile_game_ads'))
                <a href="{{ url('dashboard/mobilegameads/create') }}" class="pure-button pure-button-primary">Add Game Ad</a>
            @endif
        </div>
    </div>

    <table class="pure-table">
        <thead>
            <tr>
                <th width="50"><a href="{{ Filter::baseUrl('sort=id') }}">ID</a></th>
                <th style="padding-left:20px;"><a href="{{ Filter::baseUrl('sort=title') }}">Title</a></th>
                <th>Partner</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        @if(count($mobilegameads) > 0)
            @foreach ($mobilegameads as $mobilegamead)
                <tr>
                    <td>
                        {{ $mobilegamead->id }}
                    </td>
                    <td>
                        @if(I::can('view_mobile_game_ads'))
                            <a href="/dashboard/mobilegameads/edit/{{{base64_encode($mobilegamead->id)}}}" class="edit" title="View">{{ $mobilegamead->title }}</a>
                        @else
                            {{ $mobilegamead->title }}
                        @endif
                    </td>
                    <td>
                        {{ $mobilegamead->partner->name }}
                    </td>
                    <td>
                        @if(I::can('view_mobile_game_ads'))
                            <a href="/dashboard/mobilegameads/edit/{{ base64_encode($mobilegamead->id) }}" class="edit" title="View">View</a>
                        @endif
                        @if(I::can('delete_mobile_game_ads'))
                            <a href="/dashboard/mobilegameads/delete/{{ base64_encode($mobilegamead->id) }}" class="delete" title="Delete">Delete</a>
                        @endif
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="5">No Game Ads Found</td>
            </tr>
        @endif
        </tbody>
    </table>

</div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-mobilegameads');
    </script>
@stop
