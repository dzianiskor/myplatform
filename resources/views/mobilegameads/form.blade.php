<fieldset>
	<legend>Basic Details</legend>

    {{ Form::label('title', 'Title', array('for'=>'title')) }}
    {{ Form::text('title', $mobilegamead->title, array('class'=>'pure-input-1 required')) }}
	
	<div class="spacer"></div>
    {{ Form::label('description', 'Description', array('for'=>'description')) }}
    {{ Form::text('description', $mobilegamead->description, array('class'=>'pure-input-1 required')) }}
    <div class="spacer"></div>
	{{ Form::label('partner_id', 'Partner') }}
        <select name="partner_id" class="pure-input-1-2 required" id="partner_id" data-draft="{{ $mobilegamead->draft }}" data-id="{{ $mobilegamead->id }}">
            @if($mobilegamead->partner)
                <option value="{{ $mobilegamead->partner->id }}">{{ $mobilegamead->partner->name }}</option>
                @foreach($partners as $id => $name)
                    @if($id != $mobilegamead->partner->id)
                        <option value="{{ $id }}">{{ $name }}</option>
                    @endif
                @endforeach
            @else
                @foreach($partners as $id => $name)
                    <option value="{{ $id }}">{{ $name }}</option>
                @endforeach
            @endif
        </select>
	<div class="spacer"></div>
    {{ Form::label('url', 'URL', array('for'=>'url')) }}
    {{ Form::text('url', $mobilegamead->url, array('class'=>'pure-input-1 required')) }}
	
	<div class="spacer"></div>
        <fieldset>
            <span class="info">
                    For best results, use images that are 500x500 (pixels) in size.
            </span>

            <div class="image-rail" style="width:340px;">
                <table style="margin-top:20px;margin-bottom:20px;">
                    <tr>
                        <td width="80">
                            @if(empty($mobilegamead->image()))
                                <img src="https://placehold.it/74x74" alt="" width="80px" />
                            @else
                                <img src="<?php echo App\Http\Controllers\MediaController::getImageFromId($mobilegamead->image()->id) ?>?s=74x74" alt="" width="80px" />
                            @endif
                        </td>
                        <td style="padding-left:10px;">
                            {{ Form::label('image', 'Mobile Game Ad Image') }}
                            {{ Form::file('image') }}
                        </td>
                    </tr>
                </table>
            </div>

        </fieldset>
        <div class="spacer"></div>

   
    
    
</fieldset>