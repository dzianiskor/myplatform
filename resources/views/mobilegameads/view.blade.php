@extends('layouts.dashboard')

@section('title')
    Update Mobile Game Ads | BLU
@stop

@section('body')
    <div class="padding">

        <div class="content-head pure-g">
            <div class="title pure-u-1-2">
                <h1><i class="fa fa-money fa-fw"></i> Mobile Game Ad &raquo; {{{ $mobilegamead->name }}}</h1>
            </div>

            <div class="controls pure-u-1-2">
                @if(I::can('create_mobile_game_ads'))
                    <a href="{{ url('/dashboard/mobilegameads') }}" class="pure-button">All Mobile Game Ads</a>
                @endif
            </div>
        </div>

        {{ Form::model($mobilegamead, array('url'   => '/dashboard/mobilegameads/update/'.base64_encode($mobilegamead->id),
                                        'files'  => true,
                                        'method' => 'post',
                                        'class' => 'pure-form pure-form-stacked form-width')) }}

            @include('partials.errors')                                       
            @include('mobilegameads/form', array('mobilegamead' => $mobilegamead))

            {!! Form::hidden('redirect_to', URL::previous()) !!}

            <!-- Form Buttons -->
            <div class="form-buttons">
                <div class="left">
                    <a href="{{ url('dashboard/mobilegameads') }}" class="pure-button">Cancel</a>
                </div>

                @if($mobilegamead->draft)
                    @if(I::can('create_mobile_game_ads'))
                        <div class="right">
                            <button type="submit" class="pure-button pure-button-primary">Save Mobile Game Ad</button>
                        </div>
                    @endif
                @else
                    @if(I::can('edit_mobile_game_ads'))
                        <div class="right">
                            <button type="submit" class="pure-button pure-button-primary">Update Mobile Game Ad</button>
                        </div>
                    @endif
                @endif

                <div class="clearfix"></div>
            </div>
            <!-- /Form Buttons -->
        {{ Form::close() }}        
    </div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-mobilegameads');
    </script>
@stop
