@extends('layouts.dashboard')

@section('title')
    Update Private Network | BLU
@stop

@section('body')
<div class="padding">

    <div class="content-head pure-g">
        <div class="title pure-u-1-2">
            <h1><i class="fa fa-chain-broken fa-fw"></i>Private Network &raquo; {{ $network->name }}</h1>
        </div>

        <div class="controls pure-u-1-2">
            <a href="{{ url('dashboard/network') }}" class="pure-button">All Networks</a>
        </div>
    </div>

    {{ Form::open(array('url' => url('/dashboard/network/update/'.base64_encode($network->id)), 'files' => true, 'method' => 'post', 'class' => "pure-form pure-form-stacked form-width", 'id' => 'networkForm')) }}

        @include('partials.errors')

        {!! Form::hidden('redirect_to', URL::previous()) !!}

        <fieldset>
            <legend>Basic Details</legend>

            {{ Form::label('status', 'Network Status', array('for'=>'status')) }}
            <select name="status" id="status" class="pure-input-1-3">
                @foreach($statuses as $status)
                    @if($status == $network->status)
                        <option value="{{ $status }}" selected>{{ $status }}</option>
                    @else
                        <option value="{{ $status }}">{{ $status }}</option>
                    @endif
                @endforeach
            </select>

            {{ Form::label('name', 'Network Name', array('for'=>'name')) }}
            {{ Form::text('name', $network->name, array('class'=>'pure-input-1 required')) }}

            {{ Form::label('description', 'Description', array('for'=>'description')) }}
            {{ Form::textarea('description', $network->description, array('class'=>'pure-input-1')) }}

            <table class="network_view" style="margin-top:20px">
                <tr>
                    <td width="80">
                        @if(empty($network->image))
                            <img src="https://placehold.it/74x74" alt="" />
                        @else
                            <img src="<?php echo App\Http\Controllers\MediaController::getImageFromId($network->image) ?>?s=74x74" alt="" />
                        @endif
                    </td>
                    <td>
                        {{ Form::label('image', 'Cover Image') }}
                        {{ Form::file('image') }}
                    </td>
                </tr>
            </table>
        </fieldset>

        <fieldset>
            <legend>Points Setup</legend>

            <span class="info">
                Note: One network point (x) = (y) BLU points. Example - 2 network points = 1 BLU Point.
            </span>

            {{ Form::label('currency', 'Currency Name', array('for'=>'currency')) }}
            {{ Form::text('currency', $network->currency, array('class'=>'pure-input-1')) }}

            <div class="pure-g">
                <div class="pure-u-1-2">
                    <div class="padding-right-10">
                        {{ Form::label('network_point', 'Network Point') }}
                        {{ Form::text('network_point',  $network->network_point, array('class'=>'pure-input-1', 'placeholder' => '1.00', 'disabled' => 'disabled')) }}
                    </div>
                </div>

                <div class="pure-u-1-2">
                    {{ Form::label('blu_point', 'BLU Point') }}
                    {{ Form::text('blu_point',  $network->blu_point, array('class'=>'pure-input-1', 'placeholder' => '1.00', 'disabled' => 'disabled')) }}
                </div>
            </div>

            {{ Markup::checkbox("do_points_conversion", "Enabled Points Conversion?", $network->do_points_conversion, 1) }}

			<div class="pure-g">
                <div class="pure-u-1-2">
                    <div class="padding-right-10">
                        {{ Form::label('value_in_currency', 'Value in Currency') }}
                        {{ Form::text('value_in_currency',  $network->value_in_currency, array('class'=>'pure-input-1', 'placeholder' => '0.01')) }}
					</div>
                </div>

                <div class="pure-u-1-2">
					<?php $currencies = App\Currency::where('draft', false)->orderBy('name', 'ASC')->get();
							if(empty($network->currency_id)){
							$networkCurrencyId	= 6;
							}
							else{
								$networkCurrencyId	= $network->currency_id;
							}
					?>
					{{ Form::label('currency', 'Currency') }}
					<select name="currencies" id="currencies" style="width: 90%;">
						<option value="0">Select a currency</option>
						@foreach($currencies as $currency)
						<option value="{{ $currency->id }}" <?php echo  $networkCurrencyId == $currency->id ? 'selected' : ''; ?> > {{ $currency->name }} </option>
						@endforeach
					</select>
                </div>
            </div>
        </fieldset>

        <fieldset>
            <legend>Affiliate Organization</legend>

            <div class="pure-g">
                <div class="pure-u-1-2">
                    <div class="padding-right-10">
                        {{ Form::label('affiliate_name', 'Organization Name', array('for'=>'affiliate_name')) }}
                        {{ Form::text('affiliate_name', $network->affiliate_name, array('class'=>'pure-input-1 required')) }}
                    </div>
                </div>

                <div class="pure-u-1-2">
                    {{ Form::label('affiliate_telephone', 'Telephone', array('for'=>'affiliate_name')) }}
                    {{ Form::text('affiliate_telephone', $network->affiliate_telephone, array('class'=>'pure-input-1 enforceNumeric')) }}
                </div>

                <div class="pure-u-1-2">
                    <div class="padding-right-10">
                        {{ Form::label('affiliate_address_1', 'Address 1', array('for'=>'affiliate_address_1')) }}
                        {{ Form::text('affiliate_address_1', $network->affiliate_address_1, array('class'=>'pure-input-1')) }}
                    </div>
                </div>

                <div class="pure-u-1-2">
                    {{ Form::label('affiliate_address_2', 'Address 2', array('for'=>'affiliate_address_2')) }}
                    {{ Form::text('affiliate_address_2', $network->affiliate_address_2, array('class'=>'pure-input-1')) }}
                </div>

                <div class="pure-u-1-2">
                    <div class="padding-right-10">
                        {{ Form::label('affiliate_main_telephone', 'Main Telephone Number', array('for'=>'affiliate_main_telephone')) }}
                        {{ Form::text('affiliate_main_telephone', $network->affiliate_main_telephone, array('class'=>'pure-input-1 enforceNumeric')) }}
                    </div>
                </div>

                <div class="pure-u-1-2">
                    {{ Form::label('affiliate_email', 'Main Email Address', array('for'=>'affiliate_email')) }}
                    {{ Form::text('affiliate_email', $network->affiliate_email, array('class'=>'pure-input-1')) }}
                </div>
            </div>

            {{ Form::label('affiliate_country', 'Country') }}
            {{ Markup::country_select('affiliate_country', $network->affiliate_country, array('class' => 'pure-input-1-2 required')) }}

            {{ Form::label('affiliate_area', 'Area') }}
            {{ Markup::area_select('affiliate_area', $network->affiliate_country, $network->affiliate_area, array('class' => 'pure-input-1-2 required')) }}

            {{ Form::label('affiliate_city', 'City') }}
            {{ Markup::city_select('affiliate_city', $network->affiliate_area, $network->affiliate_city, array('class' => 'pure-input-1-2 required')) }}

        </fieldset>

        <fieldset>
            <legend>Affiliate Contact Information</legend>
            {{ Form::label('affiliate_contact_name', 'Contact Name', array('for'=>'affiliate_contact_name')) }}
            {{ Form::text('affiliate_contact_name', $network->affiliate_contact_name, array('class'=>'pure-input-1')) }}


            <div class="pure-g">
                <div class="pure-u-1-2">
                    <div class="padding-right-10">
                        {{ Form::label('affiliate_contact_telephone', 'Contact Telephone', array('for'=>'affiliate_contact_telephone')) }}
                        {{ Form::text('affiliate_contact_telephone', $network->affiliate_contact_telephone, array('class'=>'pure-input-1 enforceNumeric')) }}
                    </div>
                </div>

                <div class="pure-u-1-2">
                    {{ Form::label('affiliate_contact_ext', 'Contact Ext', array('for'=>'affiliate_contact_ext')) }}
                    {{ Form::text('affiliate_contact_ext', $network->affiliate_contact_ext, array('class'=>'pure-input-1')) }}
                </div>

                <div class="pure-u-1-2">
                    <div class="padding-right-10">
                        {{ Form::label('affiliate_contact_mobile', 'Contact Mobile', array('for'=>'affiliate_contact_mobile')) }}
                        {{ Form::text('affiliate_contact_mobile', $network->affiliate_contact_mobile, array('class'=>'pure-input-1 enforceNumeric')) }}
                    </div>
                </div>

                <div class="pure-u-1-2">
                    {{ Form::label('affiliate_contact_email', 'Contact Email', array('for'=>'affiliate_contact_email')) }}
                    {{ Form::text('affiliate_contact_email', $network->affiliate_contact_email, array('class'=>'pure-input-1')) }}
                </div>
            </div>

        </fieldset>

        <!-- Form Buttons -->
        <div class="form-buttons">
            @if($network->draft)
                @if(I::can('create_network'))
                    <div class="left">
                        <a href="{{ url('dashboard/network') }}" class="pure-button">Cancel</a>
                    </div>

                    <div class="right">
                        <button type="submit" class="pure-button pure-button-primary">Save Network</button>
                    </div>
                @endif
            @else
                @if(I::can('edit_network'))
                    <div class="left">
                        <a href="{{ url('dashboard/network') }}" class="pure-button">Cancel</a>
                    </div>

                    <div class="right">
                        <button type="submit" class="pure-button pure-button-primary">Update Network</button>
                    </div>
                @endif
            @endif

            <div class="clearfix"></div>
        </div>
        <!-- /Form Buttons -->
    {{ Form::close() }}

</div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-network');

        @if(!I::can('create_network'))
            disableForm('#networkForm');
        @endif
    </script>
@stop
