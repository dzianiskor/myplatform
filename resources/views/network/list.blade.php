@extends('layouts.dashboard')

@section('title')
    Private Network | BLU
@stop

@section('body')
<div class="padding">

    <div class="content-head pure-g">
        <div class="title pure-u-1-2">
            <h1><i class="fa fa-chain-broken fa-fw"></i>Private Network ({{ $networks->total() }})</h1>
        </div>

        <div class="controls pure-u-1-2">
            <form action="{{ Filter::searchUrl() }}" method="get">
                <input type="text" placeholder="Search" name="q" value="{{ Input::get('q') }}" />
                <button type="submit" title="" class="list-search"></button>
            </form>
            @if(I::can('create_network'))
                <a href="{{ url('dashboard/network/new') }}" class="pure-button pure-button-primary">Add Network</a>
            @endif
        </div>
    </div>

    <table class="pure-table network_list">
        <thead>
            <tr>
                <th width="50"><a href="{{ Filter::baseUrl('sort=id') }}">ID</a></th>
                <th width="90">Image</th>
                <th style="text-align:left;padding-left:20px;"><a href="{{ Filter::baseUrl('sort=name') }}">Name</a></th>
                <th><a href="{{ Filter::baseUrl('sort=currency') }}">Currency</a></th>
                <th>Convert Points?</th>
                <th><a href="{{ Filter::baseUrl('sort=affiliate_name') }}">Affiliate</a></th>
                <th>Status</th>
                <th>Country</th>
                <th width="150">Actions</th>
            </tr>
        </thead>

        <tbody>
        @if(count($networks) > 0)
            @foreach ($networks as $network)
                <tr>
                    <td>
                        {{{ $network->id }}}
                    </td>
                    <td>
                        @if(empty($network->image))
                            <img src="https://placehold.it/74x74" alt="" />
                        @else
                            <img src="<?php echo App\Http\Controllers\MediaController::getImageFromId($network->image) ?>?s=74x74" alt="" />
                        @endif
                    </td>                    
                    <td style="text-align:left;padding-left:20px;">
                        @if (I::am('BLU Admin'))
                            <a href="{{ url('dashboard/network/view/'.base64_encode($network->id)) }}" class="edit" title="Edit">{{ ucwords($network->name) }}</a>
                        @else 
                            {{ ucwords($network->name) }}
                        @endif
                    </td>
                    <td>
                        {{ $network->currency }}
                    </td>
                    <td>
                        @if($network->do_points_conversion)
                            <span class="ico-yes"></span>
                        @else
                            <span class="ico-no"></span>
                        @endif
                    </td>
                    <td>
                        @if(empty($network->affiliate_name))
                            <i>N/A</i>
                        @else
                            {{{ ucwords($network->affiliate_name) }}}
                        @endif
                    </td>
                    <td>
                        @if($network->status == 'Active')
                            <span class="ico-yes"></span>
                        @else
                            <span class="ico-no"></span>
                        @endif                    
                    </td>
                    <td>
                        @if($network->country)
                            {{ $network->country->name }}
                        @else
                            <i>N/A</i>
                        @endif
                    </td>
                    <td>
                        @if(I::can('view_network'))
                            <a href="{{ url('dashboard/network/view/'.base64_encode($network->id)) }}" class="edit" title="View">View</a>
                        @endif
                        @if(I::am('BLU Admin') && $network->id > 0)
                            <a href="{{ url('dashboard/network/delete/'.base64_encode($network->id)) }}" class="delete" title="Delete">Delete</a>                        
                        @endif
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="9">No Networks Found</td>
            </tr>
        @endif
        </tbody>
    </table>

    {{ $networks->setPath('network')->appends(Input::except('page'))->links() }}

</div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-network');
    </script>
@stop