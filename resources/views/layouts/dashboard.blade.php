<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>@yield('title')</title>
        <meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('css/pure-min_0.3.0.css') }}">
        <link rel="stylesheet" href="{{ asset('css/theme/smoothness/jquery-ui.css') }}">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css">		
        <link rel="stylesheet" href="https://cdn.datatables.net/responsive/1.0.7/css/responsive.dataTables.min.css">
        <link rel="stylesheet" href="{{ asset('css/tooltipster/tooltipster.css') }}">
        <link rel="stylesheet" href="{{ asset('css/datetimepicker/jquery.datetimepicker.css') }}">
        <link rel="stylesheet" href="{{ asset('css/spectrum/spectrum.css') }}">
        <link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">
        <?php if(empty(Auth::user()->pref_lang) || Auth::user()->pref_lang == 'en'){ ?>
        <link rel="stylesheet" href="{{ asset('css/main.css') }}">
        <?php } else{ ?>
        <link rel="stylesheet" href="{{ asset('css/main-rtl.css') }}">
        <?php } ?>
        <link rel="stylesheet" href="/css/jquery.timepicker.css">
        <script src="{{ asset('js/vendor/modernizr-2.6.2.min.js') }}"></script>
        <link rel="icon" href="/favicon.ico" type="image/x-icon" />
        <meta http-equiv="cleartype" content="on" />
        <meta name="rating" content="general" />

        {{--- Add all ext service URL's here ---}}
        <link rel="dns-prefetch" href="//google-analytics.com">
        <link rel="dns-prefetch" href="//www.google-analytics.com">
        <link rel="dns-prefetch" href="//ajax.googleapis.com">
        <link rel="dns-prefetch" href="//fonts.google.com">
        <link rel="dns-prefetch" href="//fonts.googleapis.com">
        <link rel="dns-prefetch" href="//yui.yahooapis.com">
        <link rel="dns-prefetch" href="//netdna.bootstrapcdn.com">
        <link rel="dns-prefetch" href="//code.jquery.com">
        <link rel="dns-prefetch" href="//jquery.com">
        <link rel="dns-prefetch" href="//ajax.aspnetcdn.com">
        <link rel="dns-prefetch" href="//aspnetcdn.com">

        {{--- Layout for Javascript templates ---}}
        @yield('templates')

        {{--- Set any Javascript globals here ---}}
        <script type="text/javascript">
            @yield('js_globals')
        </script>

        <!--[if lt IE 9]>
            <script src="{{ asset('js/vendor/html5shiv.js') }}"></script>
        <![endif]-->
    </head>
    <body class="loading">
        <!--[if lt IE 8]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        {{--- Do not edit ---}}
        <div id="dialog-container" class="dialog-container-rtl">
            <div id="dialog-wrapper">
                <a href="Close" class="close">Close</a>
                <div class="body">
                    <!-- Dynamically Generated -->
                </div>
            </div>
        </div>
        {{--- /Do not edit ---}}

        <div id="inline-dialogs">
            @yield('dialogs')
        </div>

        <div id="main-container">
            {{-- Left Navigation --}}
            <section id="left-navigation">
                <form class="pure-form">
                    <input type="text" class="pure-input-rounded" ame="nav-search" id="nav-search" placeholder="Navigation Search" />
                </form>

                <div id="scroll-rail">
					<?php if(I::can('view_pts_rewarded_redeemed') || I::can('view_member_accounts') || I::can('view_member_accounts') || I::can('view_network') || I::can('view_partners') || I::can('view_admins') || I::can('view_segments') || I::can('view_suppliers')) { ?>
                    <h6>General</h6>
                    <ul>
							<?php if(I::can('view_pts_rewarded_redeemed') || I::can('view_member_accounts') || I::can('view_member_accounts')) { ?>
                        <li id="mnu-overview" class="active">
                            <a href="{{ url('dashboard') }}" title="Overview"><i class="fa fa-th-list  fa-fw"></i>Dashboard</a>
                        </li>
							<?php } ?>
                        @if(I::can('view_network'))
                            <li id="mnu-network" >
                                <a href="{{ url('dashboard/network') }}" title="Network"><i class="fa fa-chain-broken fa-fw"></i>Networks</a>
                            </li>
                        @endif
                        @if(I::can('view_partners'))
                            <li id="mnu-partners" >
                                <a href="{{ url('dashboard/partners') }}" title="Partners"><i class="fa fa-map-marker fa-fw"></i>Partners</a>
                            </li>
                        @endif
                        @if(I::can('view_admins'))
                            <li id="mnu-admins" >
                                <a href="{{ url('dashboard/admins') }}" title="Admins"><i class="fa fa-user fa-fw"></i>Admins</a>
                            </li>
                        @endif
                        @if(I::can('view_members'))
                            <li id="mnu-members" >
                                <a href="{{ url('dashboard/members') }}" title="Members"><i class="fa fa-users fa-fw"></i>Members</a>
                            </li>
                        @endif
                        @if(I::can('view_segments'))
                            <li id="mnu-segments" >
                                <a href="{{ url('dashboard/segments') }}" title="Segments"><i class="fa fa-dot-circle-o fa-fw"></i>Segments</a>
                            </li>
                        @endif
                        @if(I::can('view_suppliers'))
                            <li id="mnu-suppliers" >
                                <a href="{{ url('dashboard/suppliers') }}" title="Segments"><i class="fa fa-truck fa-fw"></i>Suppliers</a>
                            </li>
                        @endif
                                                        @if(I::am("BLU Admin") || I::can('view_tiers'))
								<li id="mnu-tiers" >
									<a href="{{ url('dashboard/tiers') }}" title="Tiers"><i class="fa fa-sort-amount-asc fa-fw"></i>Tiers</a>
								</li>
							@endif
                                                        @if(I::can('view_affiliates'))
								<li id="mnu-affiliates" >
									<a href="{{ url('dashboard/affiliates') }}" title="Affiliates"><i class="fa fa-users fa-fw"></i>Affiliates</a>
								</li>
							@endif
                                                        @if(I::can('view_affiliate_program'))
								<li id="mnu-affiliate_program" >
									<a href="{{ url('dashboard/affiliate_program') }}" title="Affiliate Programs"><i class="fa fa-users fa-fw"></i>Affiliate Program</a>
								</li>
							@endif
                    </ul>
					<?php } ?>

					<?php if(I::can('view_loyalty') || I::can('view_coupons') || I::can('view_products') || I::can('view_notifications') || I::can('view_userinvoice') || I::can('view_banners') || I::can('view_offer') || I::can('view_estatement')) { ?>
                    <h6>Interaction</h6>
                    <ul>
                        @if(I::can('view_loyalty'))
                            <li id="mnu-loyalty" >
                                <a href="{{ url('dashboard/loyalty') }}" title="Loyalty"><i class="fa fa-star fa-fw"></i>Loyalty</a>
                            </li>
                        @endif
                        @if(I::can('view_coupons'))
                            <li id="mnu-coupons" >
                                <a href="{{ url('dashboard/coupons') }}" title="Coupons"><i class="fa fa-tags fa-fw"></i>Coupons</a>
                            </li>
                        @endif
                        @if(I::can('view_products'))
                            <li id="mnu-catalogue" >
                                <a href="{{ url('dashboard/catalogue') }}" title="Catalog"><i class="fa fa-shopping-cart fa-fw"></i>Catalogue</a>
                            </li>
                            <li id="mnu-new-catalogue" >
                                <a href="{{ url('dashboard/productpricing') }}" title="Catalog"><i class="fa fa-shopping-cart fa-fw"></i>New Catalogue</a>
                            </li>
                        @endif
                        @if(I::can('view_notifications'))
                            <li id="mnu-notifications" >
                                <a href="{{ url('dashboard/notifications') }}" title="Notifications"><i class="fa fa-envelope fa-fw"></i>Notifications</a>
                            </li>
                        @endif
						@if(I::can('view_banners'))
                        <li id="mnu-banners" >
                            <a href="{{ url('dashboard/banners') }}" title="Categories"><i class="fa fa-picture-o fa-fw"></i>Banners</a>
                        </li>
                        @endif
			@if(I::can('view_offer'))
                        <li id="mnu-offers" >
                            <a href="{{ url('dashboard/offer') }}" title="Offers"><i class="fa fa-eye fa-fw"></i>Offers</a>
                        </li>
                        @endif
			@if(I::can('view_estatement'))
                        <li id="mnu-estatement" >
                            <a href="{{ url('dashboard/estatement') }}" title="E-Statements"><i class="fa fa-list fa-fw"></i>E-Statements</a>
                        </li>
                        @endif
                    </ul>
					<?php } ?>

					<?php if(I::can('view_countries') || I::can('view_currencies') || I::can('view_roles') || I::can('view_brands') || I::can('view_categories') || I::can('view_batches') || I::can('view_stampreward') || I::am('BLU Admin') || I::am('BLU Admin Dev') || I::am('Translator Role') ) { ?>
                    <h6>Settings</h6>
                    <ul>
                        @if(I::can('view_countries'))
                            <li id="mnu-countries" >
                                <a href="{{ url('dashboard/countries') }}" title="Loyalty"><i class="fa fa-globe fa-fw"></i>Countries</a>
                            </li>
                        @endif
                        @if(I::can('view_currencies'))
                            <li id="mnu-currencies" >
                                <a href="{{ url('dashboard/currencies') }}" title="Coupons"><i class="fa fa-money fa-fw"></i>Currencies</a>
                            </li>
                        @endif
                        @if(I::can('view_roles'))
                            <li id="mnu-access_rights" >
                                <a href="{{ url('dashboard/roles') }}" title="Access Rights"><i class="fa fa-unlock-alt  fa-fw"></i>Access Rights</a>
                            </li>
                        @endif
                        @if(I::can('view_brands'))
                            <li id="mnu-brands" >
                                <a href="{{ url('dashboard/brands') }}" title="Brands"><i class="fa fa-book fa-fw"></i>Brands</a>
                            </li>
                        @endif
                        @if(I::can('view_categories'))
                            <li id="mnu-categories" >
                                <a href="{{ url('dashboard/categories') }}" title="Product Categories"><i class="fa fa-sort-alpha-desc fa-fw"></i>Product Categories</a>
                            </li>
                            <li id="mnu-partnercategories" >
                                <a href="{{ url('dashboard/partnercategory') }}" title="Partner Categories"><i class="fa fa-sort-alpha-desc fa-fw"></i>Partner Categories</a>
                            </li>
                        @endif
                        @if(I::can('view_batches'))
                            <li id="mnu-generator" >
                                <a href="{{ url('dashboard/generator') }}" title="Generator"><i class="fa fa-paperclip fa-fw"></i>Cards &amp; ID's</a>
                            </li>
                            <li id="mnu-cardprints" >
                                <a href="{{ url('dashboard/cardprints') }}" title="Card Print"><i class="fa fa-print fa-fw"></i>Cards Printing</a>
                            </li>
                        @endif
                        @if(I::am('BLU Admin') || I::am('BLU Admin Dev') || I::am('Translator Role'))
                            <li id="mnu-languages" ><a href="{{ url('dashboard/languages') }}" title="Languages"><i class="fa fa-globe fa-fw"></i>Languages</a></li>
                            <li id="mnu-localizationkeys" ><a href="{{ url('dashboard/localizationkeys') }}" title="Language Keys"><i class="fa fa-key fa-fw"></i>Language Keys</a></li>
                        @endif
                        @if(I::am('BLU Admin'))
                            <li id="mnu-airports" >
                                <a href="{{ url('dashboard/airports') }}" title="Loyalty"><i class="fa fa-plane fa-fw"></i>Airports</a>
                            </li>
                        @endif
                        @if(I::am('BLU Admin'))
                            <li id="mnu-carriers" >
                                <a href="{{ url('dashboard/carriers') }}" title="Loyalty"><i class="fa fa-plane fa-fw"></i>Carriers</a>
                            </li>
                        @endif
                    </ul>

					<?php } ?>
                    <h6>Insight</h6>
                    @if(I::can('view_report_tiers'))
                        <ul>
                            <li id="mnu-reporting" ><a href="{{ url('dashboard/reports') }}" title="Reporting"><i class="fa fa-bar-chart-o fa-fw"></i>Reporting</a></li>
                        </ul>
                    @endif
                    @if(I::am('BLU Admin') || I::can('view_autogenerated_reports'))
                        <ul>
                            <li id="mnu-agr-reporting" ><a href="{{ url('autogeneratedreports') }}" title="Auto Generated Reports"><i class="fa fa-bar-chart-o fa-fw"></i>Auto Generated Reports</a></li>
                        </ul>
                    @endif

                    <h6>Support</h6>
                        <ul>
                    @if(I::can('view_tickets'))
                        <ul>
                            <li id="mnu-call_center" ><a href="{{ url('dashboard/call_center') }}" title="Call Center"><i class="fa fa-phone-square fa-fw"></i>Call Center</a></li>
                           @endif
                           @if(I::can('view_redemption_order'))
                                <li id="mnu-redemption_order" >
                                    <a href="{{ url('dashboard/redemption_order') }}" title="Redmption Order"><i class="fa fa-globe fa-fw"></i>Redemption Orders</a>
                                </li>
                            
                        </ul>
                    @endif


                    @if(I::can('view_import'))
                        <h6>Import</h6>
                        <ul>
                            <li id="mnu-import" ><a href="{{ url('import') }}" title="Import"><i class="fa fa-phone-square fa-fw"></i>Import</a></li>
                        </ul>
                    @endif

                    @if(I::can('view_summary_email'))
                        <h6>Reward Email</h6>
                        <ul>
                            <li id="mnu-monthlyreward" ><a href="{{ url('monthlyemail') }}" title="Reward Email"><i class="fa fa-envelope fa-fw"></i>Reward Email</a></li>
                        </ul>
                    @endif

                    @if(I::can('view_mobile_game') || I::can('view_mobile_game_ads') || I::can('view_push_notifications'))
                        <h6>Mobile</h6>
                        <ul>
                            @if(I::can('view_mobile_game'))
                                <li id="mnu-mobilegame" >
                                    <a href="{{ url('dashboard/mobilegame') }}" title="Mobile Game">
                                        <i class="fa fa-gamepad fa-fw"></i>
                                        Game Settings
                                    </a>
                                </li>
                            @endif
                            @if(I::can('view_mobile_game_ads'))
                                <li id="mnu-mobilegameads" >
                                    <a href="{{ url('dashboard/mobilegameads') }}" title="Mobile Game Ads">
                                        <i class="fa fa-picture-o fa-fw"></i>
                                        Game Banners
                                    </a>
                                </li>
                            @endif
                            @if(I::can('view_push_notifications'))
                                <li id="mnu-mobilepush" >
                                    <a href="{{ url('dashboard/mobilepush') }}" title="Mobile Pushes">
                                        <i class="fa fa-envelope-o fa-fw"></i>
                                        Push Notifications
                                    </a>
                                </li>
                            @endif
                        </ul>
                    @endif
                    <br><br>
                </div>
            </section>
            {{-- /Left Navigation --}}

            {{-- Content Area --}}
            <section id="content-area">
                <header>
                    <span>
                        @if(!empty(Auth::user()->profile_image))
                            <img src="<?php echo App\Http\Controllers\MediaController::getImageFromId(Auth::user()->profile_image) ?>?s=26x26" alt="" /> Welcome, {{{ ucwords(Auth::user()->username) }}} ({{{ Auth::user()->roleString() }}})
                        @else
                            <img src="https://placehold.it/26x26" alt="" /> Welcome, {{{ ucwords(Auth::user()->username) }}} ({{{ Auth::user()->roleString() }}})
                        @endif
                    </span>
                    <span>
                    {{ Form::open(array('url' 		   => url('dashboard/admins/updatelang/'.Auth::user()->id),
                            'method' 	   => 'get',
                                    'style'=> 'display: inline !important;',
                            'id'           => 'languageForm')) }}
                    <select name="language" id="set-language" class="custom-select" onchange="this.form.submit();">
                        <option value="en" <?php echo Auth::user()->pref_lang == 'en'? 'selected' : ''; ?>>English</option>
                        <option value="ar"<?php echo Auth::user()->pref_lang == 'ar'? 'selected' : ''; ?>>العربية</option>
                    </select>
                    {{ Form::close() }}
                    </span>
                    <a href="{{ url('auth/logout') }}" title="Sign out">
                        Sign out
                    </a>
                </header>
                <div id="content">
                    @yield('body', 'Module Content Unavailable')
                </div>
            </section>
            {{-- /Content Area --}}
        </div>

        {{-- Scripting --}}
        <script src="/js/vendor/jquery-1.10.1.min.js"></script>
        <script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
        <script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/responsive/1.0.7/js/dataTables.responsive.min.js"></script>
        <script src="/js/vendor/nprogress.js"></script>
        <script src="/js/vendor/jquery.timepicker.min.js"></script>
        <script src="/js/vendor/jquery.tooltipster.min.js"></script>
        <script src="/js/vendor/jquery.datetimepicker.js"></script>
        <script src="/js/vendor/spectrum.js"></script>
        <script src="/js/vendor/select2.full.js"></script>
        <script src="/js/vendor/jquery.cookie.js"></script>
        <script src="{{ asset('js/plugins.js') }}"></script>
        <script src="{{ asset('js/main.js') }}"></script>

        @yield('page_script')

        @include('partials.tracking')
    </body>
</html>
