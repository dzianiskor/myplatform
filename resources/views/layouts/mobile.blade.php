<!DOCTYPE html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta http-equiv="cache-control" content="max-age=0" />
        <meta http-equiv="cache-control" content="no-cache" />
        <meta http-equiv="expires" content="0" />
        <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
        <meta http-equiv="pragma" content="no-cache" />
        <title>{{ $page_title or 'BLU Mobile' }}</title>
        <meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="{{ asset('css/pure-min-0.4.2.css') }}">
        <link rel="stylesheet" href="{{ asset('css/mobile.css') }}">
        <link rel="icon" href="/favicon.ico" type="image/x-icon" />
        <meta http-equiv="cleartype" content="on" />
        <meta name="rating" content="general" />

        {{--- Add all ext service URL's here ---}}
        <meta http-equiv="x-dns-prefetch-control" content="on">
        <link rel="dns-prefetch" href="//google-analytics.com">
        <link rel="dns-prefetch" href="//www.google-analytics.com">
        <link rel="dns-prefetch" href="//ajax.googleapis.com">
        <link rel="dns-prefetch" href="//fonts.google.com">
        <link rel="dns-prefetch" href="//fonts.googleapis.com">
        <link rel="dns-prefetch" href="//yui.yahooapis.com">
        <link rel="dns-prefetch" href="//netdna.bootstrapcdn.com">
        <link rel="dns-prefetch" href="//code.jquery.com">
        <link rel="dns-prefetch" href="//jquery.com">
        <link rel="dns-prefetch" href="//ajax.aspnetcdn.com">
        <link rel="dns-prefetch" href="//aspnetcdn.com">

        {{--- Layout for Javascript templates ---}}
        @yield('templates')

        {{--- Set any Javascript globals here ---}}
        <script type="text/javascript">
            @yield('js_globals')
        </script>
    </head>
    <body class="{{ $body_class or 'regular' }}">
        <div class="status-bar"></div>

        {{-- Content Area --}}
        <div id="content">
            @yield('body', 'Module Content Unavailable')
        </div>
        {{-- /Content Area --}}

        {{-- Scripting --}}
        <script src="/js/vendor/zepto.min.js"></script>
        <script src="/js/vendor/zepto.cookie.min.js"></script>
        <script src="/js/vendor/nprogress.js"></script>
        <script src="/js/main.js?q={{ rand(0,100 )}}"></script>
        <script src="/js/pages/mobile.js?q={{ rand(0,100 )}}"></script>

        @yield('page_script')

        @include('partials.tracking')
    </body>
</html>
    
