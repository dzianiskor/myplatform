<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>@yield('title')</title>
        <meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="{{ asset('css/main.css?q=1') }}">
        <link rel="stylesheet" href="{{ asset('css/pure-min-0.3.0.css') }}">
        <meta http-equiv="cleartype" content="on" />
        <meta name="rating" content="general" />        
        <script src="{{ asset('js/vendor/modernizr-2.6.2.min.js') }}"></script>
        <style type="text/css">
            html, body{
                background-color:#fafafa;
            }
        </style>
        <!--[if lt IE 9]>
            <script src="{{ asset('js/vendor/html5shiv.js') }}"></script>
        <![endif]-->
        <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    </head>
    <body class="{{ $body_class or 'regular' }}">
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        @yield('body', 'Page Content Missing')

        {{-- Scripting --}}
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.10.1.min.js"><\/script>')</script>

        <script src="{{ asset('js/plugins.js') }}"></script>
        <script src="{{ asset('js/main.js') }}"></script>

        @yield('page_script')

        @include('partials.tracking')
    </body>
</html>
