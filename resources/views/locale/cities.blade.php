@if(count($cities) > 0)
	@foreach($cities as $c)
		<option value="{{ $c->id }}">{{ $c->name }}</option>
	@endforeach
@else
	<option>All Cities</option>
@endif