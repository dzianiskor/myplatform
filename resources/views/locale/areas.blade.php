@if(count($areas) > 0)
	@foreach($areas as $c)
		<option value="{{ $c->id }}">{{ $c->name }}</option>
	@endforeach
@else
	<option>All Areas</option>
@endif