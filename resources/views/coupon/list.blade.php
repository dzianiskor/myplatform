    <table class="pure-table">
        <thead>
            <tr>
                <th>ID</th>
                <th>Redeemed transaction</th>
            </tr>
        </thead>

        <tbody>
        @if(count($coupons) > 0)
            @foreach ($coupons as $coupon)
                <tr class="pure-table-odd">
                    <td>
                        {{ $coupon->id}}
                    </td>
                    <td>
                        @if($coupon->transaction)
                            {{ $coupon->transaction->id}}
                        @endif
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="8">No Redeemed Coupons Found</td>
            </tr>
        @endif
        </tbody>
    </table>
