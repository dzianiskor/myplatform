@extends('layouts.dashboard')

@section('title')
    Coupons | BLU
@stop

@section('body')
<div class="padding">

    <div class="content-head pure-g">
        <div class="title pure-u-1-2">
            <h1><i class="fa fa-tags  fa-fw"></i>Coupons ({{ $batches->total() }})</h1>
        </div>

        <div class="controls pure-u-1-2">
            <form action="{{ Filter::searchUrl() }}" method="get">
                <input type="text" placeholder="Search" name="q" value="{{ Input::get('q') }}" />
                <button type="submit" title="" class="list-search"></button>
            </form>
            @if(I::can('create_coupons'))
                <a href="{{ url('/dashboard/coupons/create') }}" class="pure-button pure-button-primary">Add Batch</a>
            @endif
        </div>

        <div id="catalog-filter" class="pure-u-1-2">
            <input id="filter_route" type="hidden" value="coupons">
            <form id="filter-catalogue" action="{{ Filter::searchUrl() }}" method="get">
                <div class="filter-block filter-block-pricing-width">
                    <span class="filter-label">Partner:</span>
                    <select style="width:100%;" name="partner_list[]" id="partner_list_select" multiple="multiple">
                        @foreach ($lists['partnerData'] as $key => $value)
                            <option value="{{ $key }}">{{ $value }}</option>
                        @endforeach
                    </select>
                    <input id="filterPartners" type="hidden" value="{{ Input::get('partner_list') }}">
                </div>
                <div class="submit-filter-block disable-width">
                    <div style="float:right;">
                        <a href="{{ url('dashboard/coupons') }}" title="Clear Filters" class="pure-button pure-button-primary">
                            Clear Filters <i class="fa fa-ban fa-fw"></i>
                        </a>
                    </div>
                    <button class="pure-button pure-button-primary" type="submit" title="" style="float:right;margin-right:15px;">
                        Submit Filters
                    </button>
                </div>
            </form>
        </div>
    </div>
    @include('partials.errors')
    <table class="pure-table">
        <thead>
            <tr>
                <th width="50"><a href="{{ Filter::baseUrl('sort=id') }}">ID</a></th>
                <th style="text-align:left;padding-left:20px;"><a href="{{ Filter::baseUrl('sort=name') }}">Name</a></th>
                <th>Valid From</th>
                <th>Valid To</th>
                <th>Price</th>
                <th>Partner</th>
                <th>Count</th>
                <th>Download</th>
                <th width="150">Actions</th>
            </tr>
        </thead>

        <tbody>
        @if(count($batches) > 0)
            @foreach ($batches as $batch)
                <tr>
                    <td>
                        {{ $batch->id }}
                    </td>
                    <td style="text-align:left;padding-left:20px;">
                        <a href="{{ url('/dashboard/coupons/edit/'.base64_encode($batch->id)) }}" class="edit" title="Edit">{{ $batch->name }}</a>
                    </td>
                    <td>
                        {{ $batch->valid_from }}
                    </td>
                    <td>
                        {{ $batch->valid_to }}
                    </td>
                    <td>
                        {{ $batch->price}}
                    </td>
                    <td>
                        @if($batch->partner)
                        
                            {{ $batch->partner->name}}
                        @endif
                    </td>
                    <td>
                        {{ number_format($batch->number_of_items) }}
                    </td>
                    <td>
                        <a href="/dashboard/coupons/download/{{ base64_encode($batch->id) }}/csv" target="_blank" class="download csv" title="Download CSV">CSV</a>
                        <a href="/dashboard/coupons/download/{{ base64_encode($batch->id) }}/xls" target="_blank" class="download xls" title="Download Excel">Excel</a>
                    </td>                    
                    <td>
                        @if(I::can('view_coupons'))
                            <a href="{{ url('dashboard/coupons/edit/'.base64_encode($batch->id)) }}" class="edit" title="View">View</a>
                        @endif

                        @if(I::can('delete_coupons'))
                            <a href="{{ url('dashboard/coupons/destroy/'.base64_encode($batch->id)) }}" class="delete" title="Delete">Delete</a>
                        @endif
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="9">No Coupons Found</td>
            </tr>
        @endif
        </tbody>
    </table>

    {{ $batches->setPath('coupons')->appends(Input::except('page'))->links() }}

</div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-coupons');
    </script>
@stop
