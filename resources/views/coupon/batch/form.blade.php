{{ Form::model($batch, array('url'   => '/dashboard/coupons/update/'.base64_encode($batch->id),
                             'class' => 'pure-form pure-form-stacked form-width')) }}

    @include('partials.errors')

    {!! Form::hidden('redirect_to', URL::previous()) !!}

    <fieldset>
        <legend>Basic Details</legend>

        {{ Form::label('partner_id', 'Partner', array('for'=>'partner_id')) }}
        <select id="partner_id" name="partner_id" class="pure-input-1-4 required">
            <option value="">None</option>
            @foreach($partners as $id => $name)
                @if($id == $batch->partner_id)
                    <option value="{{ $id }}" selected="selected">{{ $name }}</option>
                @else
                    <option value="{{ $id }}">{{ $name }}</option>
                @endif
            @endforeach
        </select>

        {{ Form::label('name', 'Name') }}
        {{ Form::text('name', Input::old('name'), array('class' => 'pure-input-1')) }}
    </fieldset>

    <fieldset>
        <legend>Date Range</legend>
        <div class="pure-g">
            <div class="pure-u-1-2">
                <div class="padding-right-10">
                    {{ Form::label('valid_from', 'Start Date:') }}
                    {{ Form::text('valid_from', date("m/d/Y", strtotime($batch->valid_from)), array('class' => 'pure-input-1 datepicker required')) }}
                </div>
            </div>

            <div class="pure-u-1-2">
                {{ Form::label('valid_to', 'End Date:') }}
                {{ Form::text('valid_to', date("m/d/Y", strtotime($batch->valid_to)), array('class' => 'pure-input-1 datepicker required')) }}
            </div>
        </div>


    </fieldset>

    <fieldset>
        <legend>Target Info</legend>
        <div class="pure-g">
            <div class="pure-u-1-2">
                <div class="padding-right-10">
                    {{ Form::label('country_id', 'Country', array('for'=>'country_id')) }}
                    <select id="country_id" name="country_id" class="pure-input-1 required">
                        <option value="">Select country</option>
                        @foreach($countries as $id => $name)
                            @if($id == $batch->country_id)
                                <option value="{{ $id }}" selected="selected">{{ $name }}</option>
                            @else
                                <option value="{{ $id }}">{{ $name }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="pure-u-1-2">
                {{ Form::label('segment_id', 'Segments', array('for'=>'segment_id')) }}
                <select id="segment_id" name="segment_id" class="pure-input-1">
                    <option value="">Select segment</option>
                    @foreach($segments as $id => $name)
                        @if($id == $batch->segment_id)
                            <option value="{{ $id }}" selected="selected">{{ $name }}</option>
                        @else
                            <option value="{{ $id }}">{{ $name }}</option>
                        @endif
                    @endforeach
                </select>
            </div>
        </div>
    </fieldset>

    <fieldset>
        <legend>Batch Info</legend>
		<div class="pure-g">
			<div class="pure-u-1-2">
				{{ Form::label('type', 'Type', array('for'=>'type')) }}
				<select id="type" name="type" class="pure-input-1">
					<option value="">Select type</option>
					<option value="value">Coupon Value</option>
					<option value="percentage">Percentage Discount</option>
				</select>
			</div>

			<div class="pure-u-1-2">
					<?php $currencies = App\Currency::where('draft', false)->orderBy('name', 'ASC')->get();
							if(empty($batch->currency_id)){
							$batchCurrencyId	= 6;
							}
							else{
								$batchCurrencyId	= $batch->currency_id;
							}
					?>
					{{ Form::label('currency', 'Currency') }}
					<select name="currencies" id="currencies" style="width: 90%;">
						<option value="0">Select a currency</option>
						@foreach($currencies as $currency)
						<option value="{{ $currency->id }}" <?php echo  $batchCurrencyId == $currency->id ? 'selected' : ''; ?> > {{ $currency->name }} </option>
						@endforeach
					</select>
                </div>
		</div>
        <div class="pure-g">
            <div class="pure-u-1-2">
				<span id="price">
					<div class="padding-right-10">
						{{ Form::label('price', 'Coupon Value', array('for'=>'price')) }}
					</div>
				</span>
				<span id="discount" style="display: none;" >
					<div class="padding-right-10">
						{{ Form::label('percentage', 'Percentage Discount', array('for'=>'price')) }}
					</div>
				</span>
				{{ Form::text('price', Input::old('price'), array('class'=>'pure-input-1')) }}
            </div>

            <div class="pure-u-1-2">
                {{ Form::label('number_of_items', 'Number of Coupons', array('for'=>'number_of_items')) }}

                @if($batch->draft)
                    {{ Form::text('number_of_items', Input::old('number_of_items'), array('class'=>'pure-input-1 required')) }}
                @else
                    {{ Form::text('number_of_items', Input::old('number_of_items'), array('class'=>'pure-input-1', 'disabled' => 'disabled')) }}
                    {{ Form::hidden('number_of_items', Input::old('number_of_items')) }}
                @endif

            </div>
        </div>

        {{ Form::label('terms_and_conditions', 'Terms and Conditions', array('for'=>'terms_and_conditions')) }}
        {{ Form::textarea('terms_and_conditions', Input::old('terms_and_conditions'), array('class'=>'pure-input-1')) }}

    </fieldset>

    <fieldset>
        <legend>SMS Notifications</legend>

        <span class="info">
            Please note that SMS messages have a character limit - 160 in English, 70 in Arabic.
        </span>

        {{ Form::label('opt_in_sms', 'Opt-In SMS', array('for'=>'opt_in_sms')) }}
        {{ Form::text('opt_in_sms', Input::old('opt_in_sms'), array('class'=>'pure-input-1 dynamic-off-state', 'maxlength' => 160)) }}

        {{ Form::label('redeem_sms', 'Redeem SMS', array('for'=>'redeem_sms')) }}
        {{ Form::text('redeem_sms', Input::old('redeem_sms'), array('class'=>'pure-input-1 dynamic-off-state', 'maxlength' => 160)) }}

    </fieldset>

    {{--- Form Buttons ---}}
    <div class="form-buttons">
        <div class="left">
            <a href="{{ url('dashboard/coupons') }}" class="pure-button">Cancel</a>
        </div>
        <div class="right">
            @if($batch->draft)
                @if(I::can('create_coupons'))
                    <button type="submit" class="pure-button pure-button-primary">Save Batch</button>
                @endif
            @else
                @if(I::can('edit_coupons'))
                    <button type="submit" class="pure-button pure-button-primary">Update Batch</button>
                @endif
            @endif
        </div>
        <div class="clearfix"></div>
    </div>
    {{--- /Form Buttons ---}}
{{ Form::close() }}

@section('page_script')
    <script>
		// Load partnercountries in coupon
		$(document).on('change', '#type', function(){
			if($(this).val() == 'value'){
				$("#price").css("display", "block");
				$("#discount").css("display", "none");
			}
			else if($(this).val() == 'percentage'){
				$("#price").css("display", "none");
				$("#discount").css("display", "block");
			}
		});
    </script>
@stop