@extends('layouts.dashboard')

@section('title')
    Coupon Batch | BLU
@stop

@section('body')
<div class="padding">

    <div class="content-head pure-g">
        <div class="title pure-u-1-2">
            @if($batch->draft)
                <h1><i class="fa fa-tags fa-fw"></i>New Coupon Batch</h1>
            @else
                <h1><i class="fa fa-tags fa-fw"></i>Coupon Batch &raquo; {{ $batch->name }}</h1>
            @endif
        </div>

        <div class="controls pure-u-1-2">
            <a href="{{ url('dashboard/coupons') }}" class="pure-button">All Coupons</a>
        </div>
    </div>

    @include('coupon/batch/form', array('batch'=> $batch))

    <div class="padding"></div>

    @if (!$batch->draft)
        {{--- @include('coupon/list', array('coupons'=>$batch->redeemedCoupons)) ---}}
    @endif
</div>
@stop

@section('page_script')
@if(!I::can_edit('coupons', $batch))
 <script>
     enqueue_script('disable-form');
 </script>
@endif
    <script>
        set_menu('mnu-coupons');
    </script>
@stop
