@extends('layouts.dashboard')

@section('title')
    Verify Coupon | BLU
@stop

@section('body')
<div class="padding">

    <div class="content-head pure-g">
        <div class="title pure-u-1-2">
            <h1><i class="fa fa-tags fa-fw"></i>Verify Coupon</h1>
        </div>

        <div class="controls pure-u-1-2">
            <a href="{{ url('/dashboard/coupons') }}" class="pure-button">All Coupons</a>
        </div>
    </div>

    {{ Form::open(array('url'   => '/dashboard/coupons/verifycoupon/',
                        'class' => 'pure-form pure-form-stacked form-width',
                        'id'    => 'coupon-form')) }}

    <fieldset>
        {{ Form::label('coupon_code', 'Name') }}
        {{ Form::text('coupon_code', '', array('class' => 'pure-input-1')) }}
    </fieldset>

    <!-- Form Buttons -->
    <div class="form-buttons">
        <div class="left">
            <a href="{{ url('dashboard/coupons') }}" class="pure-button">Cancel</a>
        </div>
        <div class="right">
            <a href="#" class="pure-button pure-button-primary btn-verify">Verify Coupon</a>
        </div>
        <div class="clearfix"></div>
    </div>
    <!-- /Form Buttons -->
    {{ Form::close() }}

</div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-coupons');
        enqueue_script('coupons');
    </script>
@stop
