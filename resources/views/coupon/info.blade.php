@if (!$coupon)
    Invalid coupon code
@elseif (!$coupon->transaction)
    Coupon not redeemed
@else
    {{ $coupon->transaction->user->name }}
@endif
