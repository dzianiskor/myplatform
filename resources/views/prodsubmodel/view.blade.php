@extends('layouts.dashboard')

@section('title')
    Update Prodsubmodel | BLU
@stop

@section('body')
<div class="padding">

    <div class="content-head pure-g">
        <div class="title pure-u-1-2">
            <h1><i class="fa fa-sort-alpha-desc fa-fw"></i>Prodsubmodel &raquo; {{{ $prodsubmodel->name }}}</h1>
        </div>

        <div class="controls pure-u-1-2">
            <a href="{{ url('/dashboard/prodsubmodels') }}" class="pure-button">All Prodsubmodels</a>
        </div>
    </div>

    {{ Form::model($prodsubmodel, array('action' => array('ProdsubmodelController@saveProdsubmodel', $prodsubmodel->id),
                                   'class'  => 'pure-form pure-form-stacked form-width')) }}

        @include('prodsubmodel/form', array('prodsubmodel' => $prodsubmodel))

        <!-- Form Buttons -->
        <div class="form-buttons">
            <div class="left">
                <a href="{{ url('dashboard/prodsubmodels') }}" class="pure-button">Cancel</a>
            </div>
                <div class="right">
                    <button type="submit" class="pure-button pure-button-primary">Save Prodsubmodel</button>
                </div>
            <div class="clearfix"></div>
        </div>
        <!-- /Form Buttons -->

    {{ Form::close() }}
    {{--- Cattranslations ---}}
            <fieldset>
                <legend>Prodsubmodel Translation</legend>

                <table id="prodsubmodeltranslation-list" class="pure-table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Cattranslation Name</th>
                            <th>Language</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($prodsubmodel->prodsubmodeltranslations) > 0)
                            @foreach($prodsubmodel->prodsubmodeltranslations as $prodsubmodeltranslation)
                                <tr>
                                    <td>{{ $prodsubmodeltranslation->id }}</td>
                                    @if($prodsubmodeltranslation->draft)
                                        <td>{{{ $prodsubmodeltranslation->name }}} (Draft)</td>
                                    @else
                                        <td>{{{ $prodsubmodeltranslation->name }}}</td>
                                    @endif
                                    <td>
                                        <?php
                                            $lang = App\Language::where('id', $prodsubmodeltranslation->lang_id)->first();
                                            
                                            echo $lang->name;
                                        ?>
                                    </td>
                                    <td>
                                        @if(I::can_edit('partners', $prodsubmodel))
                                            <a href="{{ url('dashboard/prodsubmodeltranslation/edit/'.$prodsubmodeltranslation->id) }}" class="edit-prodsubmodeltranslation" title="Edit">Edit</a>
                                            <a href="{{ url('dashboard/prodsubmodeltranslation/delete/'.$prodsubmodeltranslation->id) }}" class="delete-prodsubmodeltranslation" title="Delete">Delete</a>
                                        @else
                                            N/A
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr class="empty">
                                <td colspan="4">No Prodsubmodel translation defined</td>
                            </tr>
                        @endif
                    </tbody>
                </table>

                
                    <a href="#dialog-new-prodsubmodeltranslation" data-prodsubmodel_id="{{ $prodsubmodel->id }}" class="btn-new-prodsubmodeltranslation top-space" title="Add new prodsubmodeltranslation location">
                        <i class="fa fa-plus-circle fa-fw"></i>Add new prodsubmodeltranslation
                    </a>
            </fieldset>
            {{--- /Cattranslations ---}}
</div>
@stop

@section('page_script')
@if(!I::can_edit('prodsubmodels', $prodsubmodel))
 <script>
     enqueue_script('disable-form');
 </script>
@endif
    <script>
        set_menu('mnu-prodsubmodels');
        enqueue_script('new-prodsubmodel');
    </script>
@stop
