@extends('layouts.dashboard')

@section('title')
    New Prodsubmodel | BLU
@stop

@section('body')
  <div class="padding">

      <div class="content-head pure-g">
          <div class="title pure-u-1-2">
              <h1><i class="fa fa-sort-alpha-desc fa-fw"></i>New Prodsubmodel</h1>
          </div>

          <div class="controls pure-u-1-2">
              <a href="{{ url('dashboard/prodsubmodels') }}" class="pure-button">All Categories</a>
          </div>
      </div>

      {{ Form::open(array('action' => 'ProdsubmodelController@newProdsubmodel', "class" => 'pure-form pure-form-stacked form-width')) }}

        @include('partials.errors')
        
        @include('prodsubmodel/form')

        <!-- Form Buttons -->
        <div class="form-buttons">
          <div class="left">
            <a href="{{ url('dashboard/prodsubmodels') }}" class="pure-button">Cancel</a>
          </div>
          <div class="right">
            <button type="submit" class="pure-button pure-button-primary">Save Prodsubmodel</button>
          </div>
          <div class="clearfix"></div>
        </div>
        <!-- /Form Buttons -->
        
     {{ Form::close() }}
  </div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-prodsubmodels');
    </script>
@stop
