@extends('layouts.dashboard')

@section('title')
    Product Prodsubmodel | BLU
@stop

@section('body')
<div class="padding">

    <div class="content-head pure-g">
        <div class="title pure-u-1-2">
            <h1><i class="fa fa-sort-alpha-desc  fa-fw"></i>Product Prodsubmodel ({{ $prodsubmodels->count() }})</h1>
        </div>

        <div class="controls pure-u-1-2">
            <form action="{{ Filter::searchUrl() }}" method="get">
                <input type="text" placeholder="Search" name="q" value="{{ Input::get('q') }}" />
                <button type="submit" title="" class="list-search"></button>
            </form>
            @if(I::can('create_prodsubmodels'))
                <a href="{{ url('dashboard/prodsubmodels/new') }}" class="pure-button pure-button-primary">Add Prodsubmodel</a>
            @endif
        </div>
    </div>

    <table class="pure-table">
        <thead>
            <tr>
                <th width="50"><a href="{{ Filter::baseUrl('sort=id') }}">ID</a></th>
                <th style="text-align:left;padding-left:20px;"><a href="{{ Filter::baseUrl('sort=name') }}">Name</a></th>
                <th width="150">Actions</th>
            </tr>
        </thead>

        <tbody>
        @if(count($prodsubmodels) > 0)
            @foreach ($prodsubmodels as $prodsubmodel)
                    <tr>
                        <td>{{ $prodsubmodel->id }}</td>
                        <td style="text-align:left;padding-left:20px;">
                            @if(!empty($prodsubmodel->name))
                                {{{ $prodsubmodel->name }}}
                            @else 
                                <i>N/A</i>
                            @endif
                        </td>
                        
                        <td>
                            @if($prodsubmodel->id == 1)
                                <a href="{{ url('dashboard/prodsubmodels/view/'.$prodsubmodel->id) }}" class="edit" title="View">View</a>
                            @else
                                <a href="{{ url('dashboard/prodsubmodels/view/'.$prodsubmodel->id) }}" class="edit" title="View">View</a>

                                @if(I::can('delete_prodsubmodels'))
                                    <a href="{{ url('dashboard/prodsubmodels/delete/'.$prodsubmodel->id) }}" class="delete" title="Delete">Delete</a>
                                @endif
                            @endif
                        </td>
                    </tr>
            @endforeach
        @else
            <tr>
                <td colspan="4">No Prodsubmodel Found</td>
            </tr>
        @endif
        </tbody>
    </table>

    {{ $prodsubmodels->links() }}
</div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-prodsubmodels');
    </script>
@stop
