    {{ Form::open(array('url'    => url('dashboard/prodsubmodels/updatePopover/'.$prodsubmodel->id),
                        'method' => 'post',
                        'class'  => 'pure-form pure-form-stacked',
                        'id'     => 'new-prodsubmodel-form')) }}
    @include('prodsubmodel/form')
    <!-- Form Buttons -->
    <div class="form-buttons">
        <div class="left">
            <a href="#" class="pure-button close-dialog">Cancel</a>
        </div>
        <div class="right">
            <a href="#" class="pure-button pure-button-primary prodsubmodel-save">Save</a>
        </div>
        <div class="clearfix"></div>
    </div>
