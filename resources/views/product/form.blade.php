<div class="pure-u-1-2">
    <fieldset>
        <legend>Basic Details</legend>
        {{ Form::label('status', 'Status') }}
        <select name="status" class="pure-input-1-3">
            @foreach(Meta::productStatuses() as $k => $v)
                @if($k == $product->status)
                    <option value="{{ $k }}" selected="">{{ ucfirst($v) }}</option>
                @else
                    <option value="{{ $k }}">{{ ucfirst($v) }}</option>
                @endif
            @endforeach
        </select>

        {{ Form::label('ucid', 'Unique Product ID (Auto-generated)') }}
        {{ Form::text('ucid', $product->ucid, array('class' => 'pure-input-1', 'maxlength' => 9, 'disabled' => 'disabled')) }}

        {{ Form::label('name', 'Name', array('for'=>'name')) }}
        {{ Form::text('name', $product->name, array('class'=>'pure-input-1 required')) }}

        <?php //find the prefix
            $prefix = '';
            $mappingId = $product->partner_mapping_id;
            foreach ($prefixes as $value) {
                if(!empty($value)){
                    if(strpos($product->partner_mapping_id, $value ) !== false){
                        $prefix = $value;
                        $mappingId   = str_replace($value, '', $product->partner_mapping_id);
                        break;
                    }
                }
            }
        ?>
        <div class="pure-g">
            <div class="pure-u-1-2">
                <div class="padding-right-10">
                    {{ Form::label('partner_prefix', 'Partner Prefix') }}
                    <select name="partner_prefix" class="pure-input-1">
                        <option value="" selected="">Not Selected</option>
                        @foreach($prefixes as $k => $v)
                            @if($k == $prefix)
                                <option value="{{ $k }}" selected="">{{ $v }}</option>
                            @else
                                <option value="{{ $k }}">{{ $v }}</option>
                            @endif
                        @endforeach
                    </select> 
               </div>
            </div>
            <div class="pure-u-1-2">
                {{ Form::label('partner_mapping_id', 'Partner Map ID', array('for'=>'partner_mapping_id')) }}
                {{ Form::text('partner_mapping_id', $mappingId, array('class'=>'pure-input-1')) }}
            </div>
        </div>
        <div class="pure-g">
            <div class="pure-u-1-2">
                <div class="padding-right-10">
                    {{ Form::label('brand_id', 'Brand', array('for'=>'brand_id')) }}
                    {{ Form::select('brand_id', $brands, $product->brand_id, array('id' => 'brand-list', 'class' => 'pure-input-1 required')) }}

                    @if(I::can('create_brands'))
                        <a href="#dialog-new-brand" class="btn-new-brand top-space" title="Add new brand">
                            <i class="fa fa-plus-circle fa-fw"></i>Add new brand
                        </a>
                    @endif
                </div>
            </div>

            <div class="pure-u-1-2">
                {{ Form::label('category_id', 'Category', array('for'=>'category_id')) }}
                {{ Form::select('category_id', $categories, $product->category_id, array('id' => 'category-list', 'class' => 'pure-input-1 required')) }}

                @if(I::can('create_categories'))
                    <a href="#dialog-new-category" class="btn-new-category top-space" title="Add new category">
                        <i class="fa fa-plus-circle fa-fw"></i>Add new category
                    </a>
                @endif
            </div>

            <div class="pure-u-1-2">
                <div class="padding-right-10">
                    {{ Form::label('model', 'Model', array('for'=>'model')) }}
                    {{ Form::text('model', $product->model, array('class'=>'pure-input-1')) }}
                </div>
            </div>

            <div class="pure-u-1-2">
                {{ Form::label('sub_model', 'Sub Model', array('for'=>'sub_model')) }}
                {{ Form::text('sub_model', $product->sub_model, array('class'=>'pure-input-1')) }}
            </div>
            <div class="pure-u-1-2">
                <div class="padding-right-10">
                    {{ Form::label('supplier_id', 'Supplier', array('for'=>'supplier_id')) }}
                    <?php 
                    //                    array_unshift($suppliers, 'No Supplier Selected');
                        $suppliers[0] = 'No Supplier Selected';
                        if(empty($product->supplier_id)){
                            $productSupplierId  = 0;
                        }else{
                            $productSupplierId  = $product->supplier_id;
                        }
                    ?>
                    {{ Form::select('supplier_id', $suppliers, $productSupplierId, array('id' => 'supplier-list', 'class' => 'pure-input-1 required')) }}
                </div>
            </div>

        </div>

        {{ Form::label('description', 'Description', array('for'=>'description')) }}
        {{ Form::textarea('description', $product->description, array('class'=>'pure-input-1')) }}

    </fieldset>

	<fieldset>
		<legend>Cover Image</legend>

		<table style="margin-top:20px" class="file-upload">
            <tr>
                <td width="80">
					<?php if(empty($product->cover_image)): ?>
                    	<img src="https://placehold.it/74x74" alt="" />
					<?php else: ?>
						<img src="<?php echo App\Http\Controllers\MediaController::getImageFromId($product->cover_image) ?>?s=74x74" alt="" />
					<?php endif; ?>
                </td>
                <td>
                    {{ Form::label('cover_image', 'Product Cover Image') }}
                    {{ Form::file('cover_image') }}
                </td>
            </tr>
		</table>

	</fieldset>

	<fieldset>
		<legend>Product Images</legend>

        <table style="margin-top:20px" class="file-upload">
            @foreach($product->media as $media)
            <tr>
                <td width="80">
                    <img src="<?php echo App\Http\Controllers\MediaController::getImageFromId($media->id) ?>?s=74x74" alt="" />
                </td>
                <td>
                    <a href="#" class="delete-image" data-id="{{ $media->id }}">
						<i class="fa fa-trash-o"></i> Remove image
					</a>
                </td>
            </tr>
            @endforeach
            <tr>
                <td width="80">
                        <img src="https://placehold.it/74x74" alt="" />
                </td>
                <td>
                    {{ Form::label('image', 'Product Image') }}
                    {{ Form::file('image') }}
                </td>
            </tr>
        </table>

        <a href="#dialog-new-file" class="btn-new-file top-space" title="Add another image">
            <i class="fa fa-plus-circle fa-fw"></i> Add another image
        </a>
	</fieldset>

    <fieldset>
        <legend>Values</legend>

        {{ Markup::checkbox('display_online', 'Display Online?', ($product->display_online == 1)? true : false, 1) }}

        {{ Markup::checkbox('hot_deal', 'Display as "Hot Deal"?', ($product->hot_deal == 1)? true : false, 1) }}
        {{ Markup::checkbox('is_voucher', 'Is a Voucher?', ($product->is_voucher == 1)? true : false, 1) }}
	{{ Markup::checkbox('show_offline', 'Display for Logged out?', ($product->show_offline == 1)? true : false, 1) }}
        <div class="pure-g">
            <div class="pure-u-1-2">
                <div class="padding-right-10">
                    {{ Form::label('start_date', 'Start Date:') }}
                    @if ($product->start_date == null)
                        {{ Form::text('start_date', '', array('class' => 'pure-input-1 datepicker')) }}
                    @else
                        {{ Form::text('start_date', date("m/d/Y", strtotime($product->start_date)), array('class' => 'pure-input-1 datepicker')) }}
                    @endif
                </div>
            </div>

            <div class="pure-u-1-2">
                {{ Form::label('end_date', 'End Date:') }}
                @if ($product->start_date == null)
                    {{ Form::text('end_date', '', array('class' => 'pure-input-1 datepicker')) }}
                @else
                    {{ Form::text('end_date', date("m/d/Y", strtotime($product->end_date)), array('class' => 'pure-input-1 datepicker')) }}
                @endif
            </div>
        </div>

        <div class="pure-g">
            <div class="pure-u-1-2">
                <div class="padding-right-10">
                    {{ Form::label('qty', 'Qty in stock', array('for'=>'qty')) }}
                    {{ Form::text('qty', $product->qty, array('class'=>'pure-input-1')) }}
                </div>
            </div>

            <div class="pure-u-1-2">
                {{ Form::label('price_in_points', 'Price in points', array('for'=>'price_in_points')) }}
                {{ Form::text('price_in_points', $product->price_in_points, array('class'=>'pure-input-1','disabled' => 'disabled')) }}
            </div>
            <div class="pure-u-1-2">
                <div class="padding-right-10">
                    {{ Form::label('price_in_usd', 'Price in USD', array('for'=>'price_in_usd')) }}
                    {{ Form::text('price_in_usd', $product->price_in_usd, array('class'=>'pure-input-1', 'disabled' => 'disabled')) }}
                </div>
            </div>
            <div class="pure-u-1-2">
                {{ Form::label('retail_price_in_usd', 'Retail Price in USD', array('for'=>'retail_price_in_usd')) }}
                {{ Form::text('retail_price_in_usd', $product->retail_price_in_usd, array('class'=>'pure-input-1', 'disabled' => 'disabled')) }}
            </div>
            <div class="pure-u-1-2">
                {{ Form::label('sales_tax_in_usd', 'Tax in USD', array('for'=>'sales_tax_in_usd')) }}
                {{ Form::text('sales_tax_in_usd', $product->sales_tax_in_usd, array('class'=>'pure-input-1', 'disabled' => 'disabled')) }}
            </div>

            <div class="pure-u-1-2">
        </div>
            <div class="pure-u-1-2">
				<div class="padding-right-10">
					{{ Form::label('original_price', 'Original Price', array('for'=>'original_price')) }}
					{{ Form::text('original_price', $product->original_price, array('class'=>'pure-input-1')) }}
				</div>
            </div>
            <div class="pure-u-1-2">
				<div class="padding-right-10">
					{{ Form::label('original_retail_price', 'Original Retail Price', array('for'=>'original_retail_price')) }}
					{{ Form::text('original_retail_price', $product->original_retail_price, array('class'=>'pure-input-1')) }}
				</div>
            </div>
            <div class="pure-u-1-2">
				<div class="padding-right-10">
					{{ Form::label('original_sales_tax', 'Original Sales Tax', array('for'=>'original_sales_tax')) }}
					{{ Form::text('original_sales_tax', $product->original_sales_tax, array('class'=>'pure-input-1')) }}
				</div>
            </div>
            <div class="pure-u-1-2">
				<div class="padding-right-10">
					<?php if(empty($product->currency_id)){
							$productCurrencyId	= 6;
						}
						else{
							$productCurrencyId	= $product->currency_id;
						}
					?>
					{{ Form::label('currency', 'Currency', array('for'=>'currency')) }}
					{{ Form::select('currency_id', $currencies, $productCurrencyId, array('id' => 'currency_id', 'class' => 'pure-input-1')) }}
				</div>
            </div>
        </div>
    </fieldset>


    <fieldset>
        <legend>Logistics</legend>
        <div class="pure-g">
            <div class="pure-u-1-2">
                <div class="padding-right-10">
                    {{ Form::label('weight', 'Weight (kg)', array('for'=>'weight')) }}
                    {{ Form::text('weight', $product->weight, array('class'=>'pure-input-1')) }}
                </div>
            </div>

            <div class="pure-u-1-2">
                {{ Form::label('volume', 'Volume (cm3)', array('for'=>'volume')) }}
                {{ Form::text('volume', $product->volume, array('class'=>'pure-input-1')) }}
            </div>
        </div>

        {{ Form::label('pickup_address', 'Pickup Address', array('for'=>'delivery_options')) }}
        {{ Form::textarea('pickup_address', $product->pickup_address, array('class'=>'pure-input-1')) }}

        {{ Form::label('delivery_charges', 'Delivery Charges', array('for'=>'delivery_charges')) }}
        {{ Form::text('delivery_charges', $product->delivery_charges, array('class'=>'pure-input-1')) }}

		<?php if(empty($product->delivery_currency_id)){
				$productDeliveryCurrencyId	= 6;
			}
			else{
				$productDeliveryCurrencyId	= $product->delivery_currency_id;
			}
		?>
		{{ Form::label('currency', 'Currency', array('for'=>'currency')) }}
		{{ Form::select('delivery_currency_id', $currencies, $productDeliveryCurrencyId, array('id' => 'delivery_currency_id', 'class' => 'pure-input-1')) }}

        {{ Form::label('delivery_options', 'Delivery Options', array('for'=>'delivery_options')) }}
        {{ Form::select('delivery_options', $delivery_options, $product->delivery_options) }}
    </fieldset>
</div>