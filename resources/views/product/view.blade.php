@extends('layouts.dashboard')

@section('title')
    Update Product | BLU
@stop

@section('dialogs')
    @include('product/dialog/partner', array('partners'  => $partners))
    <!--@include('product/dialog/network', array('networks'  => $networks))-->
    @include('product/dialog/country', array('countries' => $countries))
    @include('product/dialog/segment', array('segments'  => $segments))
    @include('product/dialog/channel', array('partners'  => $partners))
@stop

@section('body')
<div class="padding">

    <div class="content-head pure-g">
        <div class="title pure-u-1-2">
            <h1><i class="fa fa-shopping-cart fa-fw"></i> Product &raquo; {{ $product->ucid }}</h1>
        </div>

        <div class="controls pure-u-1-2">
            <a href="{{ url('dashboard/catalogue') }}" class="pure-button">Catalogue</a>
        </div>
    </div>

    {{ Form::model($product, array('action' => array('ProductController@saveProduct',base64_encode($product->id)),
                                   'method' => 'post',
                                   'files'  => 'true',
                                   'class'  => 'pure-form pure-form-stacked form-width',
                                   'id'     => 'productForm')) }}

        @include('partials.errors')

        {!! Form::hidden('redirect_to', URL::previous()) !!}

        <div class="pure-g">
            @include('product/form', array('product' => $product))
            <div class="pure-u-1-2 attributes">
                <div class="padding-left-40">
                    @include('product/list/partner', array('product' => $product))
<!--                    @include('product/list/network', array('product' => $product))-->
                    @include('product/list/segment', array('product' => $product))
                    @include('product/list/country', array('product' => $product))
                    @include('product/list/channel', array('product' => $product))
                </div>
            </div>
        </div>
{{--- Prodtranslations ---}}
            <fieldset>
                <legend>Product Translation</legend>

                <table id="prodtranslation-list" class="pure-table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Prodtranslation Name</th>
                            <th>Language</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($product->prodtranslations) > 0)
                            @foreach($product->prodtranslations as $prodtranslation)
                            <?php $lang = App\Language::where('id', $prodtranslation->lang_id)->first(); ?>
                                <tr>
                                    <td>{{ $prodtranslation->id }}</td>
                                    @if($prodtranslation->draft)
                                        <td <?php if($lang->name == 'Arabic'){echo "style='direction:rtl !important;'";} ?>>{{{ $prodtranslation->name }}} (Draft)</td>
                                    @else
                                        <td <?php if($lang->name == 'Arabic'){echo "style='direction:rtl !important;'";} ?> >{{{ $prodtranslation->name }}}</td>
                                    @endif
                                    <td>
                                        <?php
                                            echo $lang->name;
                                        ?>
                                    </td>
                                    <td>
                                        @if(!I::can('delete_products') && !I::can('edit_products'))
                                            N/A
                                        @endif
                                        @if(I::can('edit_products'))
                                            <a href="{{ url('dashboard/prodtranslation/edit/'.$prodtranslation->id) }}" class="edit-prodtranslation" title="Edit">Edit</a>
                                        @endif
                                        @if(I::can('delete_products'))
                                            <a href="{{ url('dashboard/prodtranslation/delete/'.$prodtranslation->id) }}" class="delete-prodtranslation" title="Delete">Delete</a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr class="empty">
                                <td colspan="4">No prodtranslation locations defined</td>
                            </tr>
                        @endif
                    </tbody>
                </table>

                @if(I::can('create_products') || I::can('edit_products'))
                    <a href="#dialog-new-prodtranslation" data-product_id="{{ $product->id }}" class="btn-new-prodtranslation top-space" title="Add new prodtranslation location">
                        <i class="fa fa-plus-circle fa-fw"></i>Add new prodtranslation
                    </a>
                @endif
            </fieldset>
            {{--- /Prodtranslations ---}}
        <!-- Form Buttons -->
        <div class="form-buttons">
            <div class="left">
                <a href="{{ url('dashboard/catalogue') }}" class="pure-button">Cancel</a>
            </div>

            @if($product->draft)
                @if(I::can('create_products'))
                    <div class="right">
                        <button type="submit" class="pure-button pure-button-primary">Create Product</button>
                    </div>
                @endif
            @else
                @if(I::can('edit_products'))
                    <div class="right">
                        <button type="submit" class="pure-button pure-button-primary">Save Product</button>
                    </div>
                @endif
            @endif
            
            <div class="clearfix"></div>
        </div>
        <!-- /Form Buttons -->
        
    {{ Form::close() }}

</div>
@stop

@section('page_script')
    <script>
        $(function(){
            set_menu('mnu-catalogue');
        });

        enqueue_script('new-product');
    </script>
@stop
