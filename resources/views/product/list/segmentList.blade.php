@if(count($product->segments) > 0)
    @foreach($product->segments as $s)
        @if(in_array($s->name, $segments->toArray()))
        <li>
            {{ $s->name }}
            @if(I::can('delete_products'))
                <a href="/dashboard/catalogue/unlink_segment/{{ base64_encode($product->id)  }}/{{ base64_encode($s->id) }}" class="delete-ajax" data-id="{{ $s->id }}" data-target="#segment-listing">
                    [Delete]
                </a>
            @endif
        </li>
        @endif
    @endforeach
@else
    <li class="empty">No Segments Associated</li>
@endif
