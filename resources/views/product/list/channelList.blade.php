@if($product->channels()->count() > 0)
    @foreach($product->channels as $c)
        @if(! isset($hideNotAllowedPartners) || Auth::User()->canSeePartner($c->channel_id))
            <li>
                @if( is_object($c->channel) )
                    {{ $c->channel->name }}
                @else
                    Deleted Channel
                @endif

                @if(I::can('delete_products'))
                    <a href="/dashboard/catalogue/unlink_channel/{{ base64_encode($product->id) }}/{{ base64_encode($c->channel_id)}}" class="delete-ajax" data-id="{{ $c->channel_id }}" data-target="#channel-listing">
                        [Delete]
                    </a>
                @endif
            </li>
        @endif
    @endforeach
@else
    <li class="empty">No Channels Associated</li>
@endif
