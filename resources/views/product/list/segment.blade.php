{{-- Segment Listing --}}
<h3>
    @if(I::can_edit('products', $product))
    <a href="#dialog-add-segment" title="New Segment" class="dialog">
        Segments <i class="fa fa-plus-circle fa-fw"></i>
    </a>
    @else
        Segments
    @endif
</h3>
<ul id="segment-listing">
    @include('product/list/segmentList', array('product'=>$product))
</ul>
{{-- /Segment Listing --}}
