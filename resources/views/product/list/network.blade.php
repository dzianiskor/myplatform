{{-- Network Listing --}}
	@if(I::am('BLU Admin') || I::am('BLU Admin 2'))
	<h3>
	    @if(I::can_edit('products', $product))
	    <a href="#dialog-add-network" title="New Network" class="dialog">
	        Networks <i class="fa fa-plus-circle fa-fw"></i>
	    </a>
	    @else
	        Networks
	    @endif
	</h3>
	<ul id="network-listing">
	    @include('product/list/networkList', array('product'=>$product))
	</ul>
	<label class="error" style="display: none;">This field is required.</label>
	{{-- /Network Listing --}}
@endif