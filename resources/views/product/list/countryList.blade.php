@if(count($product->countries) > 0)
    @foreach($product->countries as $c)
        <li>
            {{ $c->name }}
            @if(I::can('delete_products'))
                <a href="/dashboard/catalogue/unlink_country/{{ base64_encode($product->id) }}/{{base64_encode($c->id) }}" class="delete-ajax" data-id="{{ $c->id }}" data-target="#country-listing">
                    [Delete]
                </a>
            @endif
        </li>
    @endforeach
@else
    <li class="empty">No Country Associated</li>
@endif
