@if(count($product->partners) > 0)
    @foreach($product->partners as $p)
        @if(! isset($hideNotAllowedPartners) || Auth::User()->canSeePartner($p->id))
            <li>
                {{ $p->name }}
                @if(I::can('delete_products'))
                    @if (! Auth::User()->partners->contains($p))
                        <a href="/dashboard/catalogue/unlink_partner/{{ base64_encode($product->id) }}/{{ base64_encode($p->id) }}" class="delete-ajax" data-id="{{ $p->id }}" data-target="#partner-listing">
                            [Delete]
                        </a>
                    @endif
                @endif
            </li>
        @endif
    @endforeach
@else
    <li class="empty">No Partner Associated</li>
@endif
