@extends('layouts.dashboard')

@section('title')
    Catalogue | BLU
@stop

@section('body')
<div class="padding">

    <div class="content-head pure-g">
        <div class="title pure-u-1-2">
            <h1><i class="fa fa-shopping-cart fa-fw"></i> Catalogue ({{ $products->total() }})</h1>
        </div>

        <div class="controls pure-u-1-2">
            <form action="{{ Filter::searchUrl() }}" method="get">
                <?php
                    $querystring = $query_string;
                    if($querystring != ''){
                        $queryArr = explode('&', $querystring);
                        foreach($queryArr as $val){
                            if (empty($val)) {
                                continue;
                            }
                            $valArr = explode('=', $val);
                            if($valArr[0] != "q"){
                                echo '<input type="hidden" name="'. urldecode($valArr[0]) .'" value="'.$valArr[1] . '"> ';
                            }

                        }
                    }?>
                <input type="text" placeholder="Search" name="q" value="{{ Input::get('q') }}" />
                <button type="submit" title="" class="list-search"></button>
            </form>
            @if(I::can('create_products'))
                <a href="{{ url('dashboard/catalogue/new') }}" class="pure-button pure-button-primary">Add Product</a>
            @endif
        </div>

        <div id="catalog-filter" class="pure-u-1-2">
            <input id="filter_route" type="hidden" value="catalogue">
            <form id="filter-catalogue" action="{{ Filter::searchUrl() }}" method="get">

                <div class="filter-block filter-block-pricing-width">
                    <span class="filter-label">Partner:</span>
                    <select style="width:100%;" name="partner_list[]" data-filter-catalogue="partner_list" data-filter-catalogue-title="Select Partner" multiple="multiple">
                        @foreach ($lists['partners'] as $key => $value)
                            <option value="{{ $key }}">{{ $value }}</option>
                        @endforeach
                    </select>
                    <input data-filter-catalogue="partner_list" type="hidden" value="{{ Input::get('partner_list') }}">
                </div>

                <div class="filter-block filter-block-pricing-width">
                    <span class="filter-label">Supplier:</span>
                    <select style="width:100%;" name="supplier_list[]" data-filter-catalogue="supplier_list" data-filter-catalogue-title="Select Supplier" multiple="multiple">
                        @foreach ($lists['suppliers'] as $key => $value)
                            <option value="{{ $key }}">{{ $value }}</option>
                        @endforeach
                    </select>
                    <input data-filter-catalogue="supplier_list" type="hidden" value="{{ Input::get('supplier_list') }}">
                </div>

                <div class="filter-block filter-block-pricing-width">
                    <span class="filter-label">Country:</span>
                    <select style="width:100%;" name="country_list[]" data-filter-catalogue="country_list" data-filter-catalogue-title="Select Country" multiple="multiple">
                        @foreach ($lists['countries'] as $key => $value)
                            <option value="{{ $key }}">{{ $value }}</option>
                        @endforeach
                    </select>
                    <input data-filter-catalogue="country_list" type="hidden" value="{{ Input::get('country_list') }}">
                </div>

                <div class="filter-block filter-block-pricing-width">
                    <span class="filter-label">Channel:</span>
                    <select style="width:100%;" name="displaychannel_list[]" data-filter-catalogue="displaychannel_list" data-filter-catalogue-title="Select Channel" multiple="multiple">
                        @foreach ($lists['partners'] as $key => $value)
                            <option value="{{ $key }}">{{ $value }}</option>
                        @endforeach
                    </select>
                    <input data-filter-catalogue="displaychannel_list" type="hidden" value="{{ Input::get('displaychannel_list') }}">
                </div>

                <div class="filter-block filter-block-pricing-width">
                    <span class="filter-label">Brand:</span>
                    <select style="width:100%;" name="brand_list[]" data-filter-catalogue="brand_list" data-filter-catalogue-title="Select Brand" multiple="multiple">
                        @foreach ($lists['brands'] as $key => $value)
                            <option value="{{ $key }}">{{ $value }}</option>
                        @endforeach
                    </select>
                    <input data-filter-catalogue="brand_list" type="hidden" value="{{ Input::get('brand_list') }}">
                </div>

                <div class="submit-filter-block disable-width">
                    <div style="float:right;">
                        <a href="<?php echo e(url('dashboard/catalogue')); ?>" title="Clear Filters" class="pure-button pure-button-primary">
                            Clear Filters <i class="fa fa-ban fa-fw"></i>
                        </a>
                    </div>
                    <button class="pure-button pure-button-primary" type="submit" title="" style="float:right;margin-right:15px;">
                        Submit Filters
                    </button>
                </div>
            </form>
        </div>
    </div>

    <table class="pure-table product_list">
        <thead>
            <tr>
                <th width="50"><a href="{{ Filter::baseUrl('sort=id') }}">ID</a></th>
				<th width="90">Image</th>
                <th style="text-align:left;padding-left:20px;"><a href="{{ Filter::baseUrl('sort=name') }}">Name</a></th>
                <th>Brand</th>
                <th><a href="{{ Filter::baseUrl('sort=model') }}">Model</a></th>
                <th>Category</th>
                <th>Partner</th>
                <th>Display Online</th>
                <th width="150">Actions</th>
            </tr>
        </thead>

        <tbody>
        @if(count($products) > 0)
            @foreach ($products as $product)
                <tr>
                    <td>
                        {{ $product->id }}
                    </td>
                    <td>
                        @if(empty($product->cover_image))
                            <img src="https://placehold.it/74x74" alt="" />
                        @else
                            <img src="<?php echo App\Http\Controllers\MediaController::getImageFromId($product->cover_image) ?>?s=74x74" alt="" />
                        @endif
                    </td>
                    <td style="text-align:left;padding-left:20px;">
                        <a href="{{ url('dashboard/catalogue/view/'.base64_encode($product->id)) }}" class="edit" title="Edit">{{{ $product->name}}}</a>
                    </td>
                    <td>
                        @if ($product->brand)
                            {{{ $product->brand->name }}}
                        @else
                            N/A
                        @endif
                    </td>
                    <td>
                        @if($product->model)
                            {{{ $product->model }}}
                        @else
                            <i>N/A</i>
                        @endif
                    </td>
                    <td>
                        @if ($product->category)
                            {{{ $product->category->name }}}
                        @else
                            N/A
                        @endif
                    </td>
                    <td>
                        @if ($product->partners->first())
                            {{{ $product->partners->first()->name }}}
                        @else
                            <i>N/A</i>
                        @endif
                    </td>
                    <td>
                        @if($product->display_online == 1)
                            <span class="ico-yes"></span>
                        @else
                            <span class="ico-no"></span>
                        @endif
                    </td>
                    <td>
                        @if(I::can('view_products'))
                            <a href="{{ url('dashboard/catalogue/view/'.base64_encode($product->id)) }}" class="edit" title="View">View</a>
                        @endif

                        @if(I::can('delete_products'))
                            <a href="{{ url('dashboard/catalogue/delete/'.base64_encode($product->id)) }}" class="delete" title="Delete">Delete</a>
                        @endif
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="9">No Items Found</td>
            </tr>
        @endif
        </tbody>
    </table>

    {{ $products->setPath('catalogue')->appends(Input::except('page'))->links() }}

</div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-catalogue');
    </script>
@stop