{{-- Assign new Segment --}}
<div id="dialog-add-segment" style="width:300px;">
    <h4>Associate to Segment</h4>

    {{ Form::open(array('class' => "pure-form pure-form-stacked")) }}
        {{ Form::select('segment_id', $segments, null, array('class' => 'pure-input-1')) }}

        <a href="/dashboard/catalogue/link_segment/{{ base64_encode($product->id) }}" class="pure-button pure-button-primary attribute-action" data-target="#segment-listing">
            Save
        </a>
    {{ Form::close() }}
</div>
{{-- /Assign new Segment --}}