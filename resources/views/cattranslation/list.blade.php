@if(count($cattranslations) > 0)
  @foreach($cattranslations as $cattranslation)
    <tr>
        <td>{{ $cattranslation->id }}</td>
        <td>{{ $cattranslation->name }}</td>
        <?php
            $language = App\Language::find($cattranslation->lang_id);
            echo "<td>" . $language->name ."</td>";
        ?>
        <td>
            @if(!I::can('delete_categories') && !I::can('edit_categories'))
                N/A
            @endif
            @if(I::can('edit_categories'))
                <a href="{{ url('dashboard/cattranslation/edit/'.base64_encode($cattranslation->id)) }}" class="edit-cattranslation" title="Edit">Edit</a>
            @endif
            @if(I::can('delete_categories'))
                <a href="{{ url('dashboard/cattranslation/delete/'.base64_encode($cattranslation->id)) }}" class="delete-cattranslation" title="Delete">Delete</a>
            @endif
        </td>
    </tr>
  @endforeach
@else
    <tr class="empty">
        <td colspan="4">No cattranslation defined</td>
    </tr>
@endif
