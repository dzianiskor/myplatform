
{{ Form::open(array('url' => url('dashboard/cattranslation/update/'.base64_encode($cattranslation->id)), 'method' => 'post', 'class' => "pure-form pure-form-stacked", 'id' => 'new-cattranslation-form')) }}
    
    @include('partials.errors')                         

    <div class="pure-g">
        <div class="pure-u-1-3">
            {{ Form::label('cattranslation_lang', 'Language') }}
            {{ Form::select('cattranslation_lang',$languages, array('class' => 'pure-input-1-2')) }}
            
            {{ Form::label('cattranslation_name', 'Name') }}
           
            
            
        </div>
 {{ Form::text('cattranslation_name', $cattranslation->name, array('class' => 'pure-input-1 required')) }}
    </div>

    {{-- Dialog Buttons --}}
    <div class="form-buttons">
        <div class="left">
            <a href="#" class="pure-button close-dialog">Cancel</a>
        </div>
        <div class="right">
            <a href="#" class="pure-button pure-button-primary save-new-cattranslation">Save</a>
        </div>
        <div class="clearfix"></div>
    </div>
    {{-- /Dialog Buttons --}}

{{ Form::close() }}