@extends('layouts.dashboard')

@section('js_globals')
    var MEMBER_ID = {{ $admin->id }};
@stop

@section('title')
    @if($admin->draft)
        Admin &raquo; New  | BLU
    @else
        Admin &raquo; {{{ $admin->last_name }}}, {{{ $admin->first_name }}}  | BLU
    @endif
@stop

@section('dialogs')
    {{-- Assign new Partner --}}
    <div id="dialog-add-partner" style="width:300px;">
        <h4>Associate to Partner</h4>

        {{ Form::open(array('class' => "pure-form pure-form-stacked")) }}
            {{ Form::select('partner_id', $partners, null, array('class' => 'pure-input-1')) }}

            <a href="/dashboard/partners/attachadmin/{{ base64_encode($admin->id) }}" class="pure-button pure-button-primary attribute-action" data-target="#partner-listing">
                Save
            </a>
        {{ Form::close() }}
    </div>

    {{-- Assign new Role --}}
    <div id="dialog-add-role" style="width:300px;">
        <h4>Assign Role</h4>
        <?php unset($roles[4]); ?>
        {{ Form::open(array('class' => "pure-form pure-form-stacked")) }}
            {{ Form::select('role_id', $roles, null, array('class' => 'pure-input-1')) }}

            <a href="/dashboard/roles/attachadmin/{{ base64_encode($admin->id) }}" class="pure-button pure-button-primary attribute-action" data-target="#role-listing">
                Save
            </a>
        {{ Form::close() }}
    </div>




@stop

@section('body')
    <div class="padding">

        {{-- Page Header --}}
        <div class="content-head pure-g">
            <div class="title pure-u-1-2">
                @if($admin->draft)
                    <h1><i class="fa fa-user fa-fw"></i>Admin &raquo; New</h1>
                @else
                    <h1><i class="fa fa-user fa-fw"></i>Admin &raquo; {{ $admin->first_name }} {{ $admin->last_name }}</h1>
                @endif
            </div>

            <div class="controls pure-u-1-2">
                <a href="{{ url('dashboard/admins') }}" class="pure-button">All Admins</a>
            </div>
        </div>
        {{-- /Page Header --}}

        {{ Form::open(array('url' 		   => url('/dashboard/admins/update/'.base64_encode($admin->id)),
                            'files' 	   => true,
                            'method' 	   => 'post',
                            'class' 	   => "pure-form pure-form-stacked form-width",
							'autocomplete' => 'off',
                            'id'           => 'memberForm')) }}

            @include('partials.errors')

            {!! Form::hidden('redirect_to', URL::previous()) !!}

            <div class="pure-g">
                <div class="pure-u-1-2">
                    <fieldset>
                        <legend>Basic Details</legend>

                        {{ Form::label('status', 'Status') }}
                        {{ Form::select('status', $statuses, $admin->status, array('class' => 'pure-input-1-3')) }}

                        {{ Form::label('ucid', 'Unique Customer ID') }}
                        {{ Form::text('ucid', $admin->ucid, array('class' => 'pure-input-1', 'maxlength' => 9, 'disabled' => 'disabled')) }}

<!--                        <a href="#" title="View Balance Information" class="balance-tooltip padded-href" data-id="{{ $admin->id }}">
                            <i class="fa fa-dot-circle-o"></i> See Network Balances
                        </a>-->
			{{ Form::label('parent_id', 'Parent') }}
                        {{ Form::select('parent_id', $managed_admins, $admin->parent_id, array('class' => 'pure-input-1-2')) }}

                        {{ Form::label('title', 'Title') }}
                        {{ Form::select('title', Meta::titleList(), $admin->title, array('class' => 'pure-input-1-3')) }}

                        {{ Form::label('first_name', 'First Name') }}
                        {{ Form::text('first_name', $admin->first_name, array('class' => 'pure-input-1 required')) }}

                        {{ Form::label('middle_name', 'Middle Name') }}
                        {{ Form::text('middle_name', $admin->middle_name, array('class' => 'pure-input-1')) }}

                        {{ Form::label('last_name', 'Last Name') }}
                        {{ Form::text('last_name', $admin->last_name, array('class' => 'pure-input-1 required')) }}

                        {{ Form::label('pref_lang', 'Preferred Language') }}
                        {{ Form::select('pref_lang', $languages, $admin->pref_lang, array('class' => 'pure-input-1-2')) }}



                        <div class="image-rail" style="width:340px;">
                            <table style="margin-top:20px;margin-bottom:20px;">
                                <tr>
                                    <td width="80">
                                        @if(empty($admin->profile_image))
                                            <img src="http://placehold.it/74x74" alt="" />
                                        @else
                                            <img src="<?php echo App\Http\Controllers\MediaController::getImageFromId($admin->profile_image)?>?s=74x74" alt="" style="width:80px;"/>
                                        @endif
                                    </td>
                                    <td style="padding-left:10px;">
                                        {{ Form::label('image', 'Profile Image') }}
                                        {{ Form::file('image') }}
                                    </td>
                                </tr>
                            </table>
                        </div>                            
                    </fieldset>

                    <fieldset>
                        <legend>Contact Details</legend>



                        {{ Form::label('email', 'Email Address') }}
                        {{ Form::text('email', $admin->email, array('class' => 'pure-input-1 required', 'id'=>'email')) }}

                        <div id="email-error" class="inline-error">
                            The email provided is already in use by another account.
                        </div>
					</fieldset>

<!--                    <fieldset>
                        <legend>Additional Details</legend>


                        {{ Form::label('gender', 'Gender') }}
                        {{ Form::select('gender', Meta::genderList(), $admin->gender, array('class' => 'pure-input-1-3')) }}

                        {{ Form::label('year', 'Date of Birth') }}

                        <div class="pure-g">
                            <div class="pure-u-1-3">
                                {{ Form::select('day', Meta::dayArray(), Meta::dateComponent($admin->dob, 'day'), array('class' => 'pure-input-1')) }}
                            </div>

                            <div class="pure-u-1-3">
                                <div class="padding-lr-10">
                                    {{ Form::select('month', Meta::monthArray(), Meta::dateComponent($admin->dob, 'month'), array('class' => 'pure-input-1')) }}
                                </div>
                            </div>

                            <div class="pure-u-1-3">
                                {{ Form::select('year', Meta::yearArray(), Meta::dateComponent($admin->dob, 'year'), array('class' => 'pure-input-1')) }}
                            </div>
                        </div>

                        <div class="clearfix"></div>


                        {{ Form::label('tag', 'Special Attributes') }}
                        {{ Form::text('tag', $admin->tag, array('class' => 'pure-input-1')) }}
                    </fieldset>-->

                    <fieldset>
                        <legend>Address Details</legend>

                        {{ Form::label('country_id', 'Country') }}
                        {{ Markup::country_select('country_id', $admin->country_id, array('class' => 'pure-input-1-3')) }}

                        {{ Form::label('area_id', 'Area') }}
                        {{ Markup::area_select('area_id', $admin->country_id, $admin->area_id, array('class' => 'pure-input-1-3')) }}

                        {{ Form::label('city_id', 'City') }}
                        {{ Markup::city_select('city_id', $admin->area_id, $admin->city_id, array('class' => 'pure-input-1-3')) }}

                        {{ Form::label('address_1', 'Address') }}
                        {{ Form::text('address_1', $admin->address_1, array('class' => 'pure-input-1')) }}

                        {{ Form::label('address_2', 'Address (Continued)') }}
                        {{ Form::text('address_2', $admin->address_2, array('class' => 'pure-input-1')) }}
                    </fieldset>

                    <fieldset>
                        <legend>Admin Authentication Details</legend>

                        <span class="info">
                            Only complete this section if you want to allow this user to log into the backend.
                        </span>

                        {{ Form::label('username', 'Username') }}
                        {{ Form::text('username', $admin->username, array('class' => 'pure-input-1')) }}

                        {{ Form::label('password', 'Password (Current password not shown)') }}
                        {{ Form::label('password', 'Password must have minimum 8 characters, 1 special symbol, and 1 letter in uppercase') }}
                        {{ Form::password('password', null, array('class' => 'pure-input-1 required', 'id'=>'password')) }}

                        {{ Form::label('password_confirmation', 'Confirm Password') }}
                        {{ Form::password('password_confirmation', null, array('class' => 'pure-input-1 required', 'id'=>'confirm_password')) }}

                        {{ Form::label('api_key', 'API Key') }}
                        {{ Form::text('username', $admin->api_key, array('class' => 'pure-input-1', 'disabled' => 'disabled')) }}

                        {{ Form::label('static_ip', 'Static IP') }}
                        {{ Form::text('static_ip', $admin->static_ip, array('class' => 'pure-input-1')) }}
                    </fieldset>

                </div>
                <div class="pure-u-1-2 attributes">
                    <div class="padding-left-40">



                        {{-- Partner Listing --}}
                        <h3>
                            @if($admin->id != Auth::User()->id && I::can_edit('admins', $admin))
                                <a href="#dialog-add-partner" title="New Partner" class="dialog">
                                    Partners <i class="fa fa-plus-circle fa-fw"></i>
                                </a>
                            @else
                                Partners
                            @endif
                        </h3>
                        <ul id="partner-listing">
                            @if($admin->partners->count() > 0)
                                {{-- */$first = true;/* --}}
                                @foreach($admin->partners as $p)
                                   @if(I::am('BLU Admin'))
										<li>
                                           	{{ $p->name }} <a href="/dashboard/partners/unlinkadmin/{{ base64_encode($admin->id) }}/{{ base64_encode($p->id) }}" class="delete-ajax" data-id="{{ $p->id }}" data-target="#partner-listing">[Delete]</a>
										</li>
                                   @else
										@if(Auth::User()->canSeePartner($p->id))
	                                        <li>
                                                {{ $p->name }}
                                                @if($admin->id != Auth::User()->id && I::can('delete_admins'))
                                                    <a href="/dashboard/partners/unlinkadmin/{{ base64_encode($admin->id) }}/{{ base64_encode($p->id)}}" class="delete-ajax" data-id="{{ $p->id }}" data-target="#partner-listing">[Delete]</a>
                                                @endif
	                                        </li>
	                                    @endif
                                    @endif

                                {{-- */$first = false;/* --}}
                                @endforeach
                            @else
                                <li class="empty">No Partner Associated</li>
                            @endif
                        </ul>
                        {{-- /Partner Listing --}}



                        {{-- Role Listing --}}
                        <h3>
                            @if(I::can_edit('admins', $admin))

                                @if(I::am('BLU Admin'))
                                    <a href="#dialog-add-role" title="New Role" class="dialog">
                                        Admin Type <i class="fa fa-plus-circle fa-fw"></i>
                                    </a>
                                @elseif(I::am('Partner Admin') && $admin->id != Auth::User()->id)
                                    <a href="#dialog-add-role" title="New Role" class="dialog">
                                        Admin Type <i class="fa fa-plus-circle fa-fw"></i>
                                    </a>
                                @else
                                    Admin Type
                                @endif

                            @else
                                Admin Type
                            @endif
                        </h3>
                        <ul id="role-listing">
                            @if(count($admin->roles) > 0)
                                @foreach($admin->roles as $r)
                                    <li>
                                        {{ $r->name }}

                                        @if(!I::am('BLU Admin'))
                                            @if(I::can('delete_admins') && $admin->id > 1)
                                                <a href="/dashboard/roles/unlinkadmin/{{ base64_encode($admin->id) }}/{{ base64_encode($r->id) }}" class="delete-ajax" data-id="{{ $r->id }}" data-target="#role-listing">
                                                    [Delete]
                                                </a>
                                            @endif
                                        @else
                                            <a href="/dashboard/roles/unlinkadmin/{{ base64_encode($admin->id) }}/{{ base64_encode($r->id) }}" class="delete-ajax" data-id="{{ $r->id }}" data-target="#role-listing">
                                                [Delete]
                                            </a>
                                        @endif
                                    </li>
                                @endforeach
                            @else
                                <li class="empty">No Roles Associated</li>
                            @endif
                        </ul>
                        {{-- /Role Listing --}}

                    </div>
                </div>
            </div>

            <!-- Form Buttons -->
            <div class="form-buttons">
                <div class="left">
                    <a href="{{ url('dashboard/admins') }}" class="pure-button">Cancel</a>
                </div>
                @if($admin->draft)
                    @if(I::can('create_admins'))
                        <div class="right">
                            <button type="submit" class="pure-button pure-button-primary">Create Admin</button>
                        </div>
                    @endif
                @else
                    @if(I::can('edit_admins'))
                        <div class="right">
                            <button type="submit" class="pure-button pure-button-primary">Save Admin</button>
                        </div>
                    @endif
                @endif
                <div class="clearfix"></div>
            </div>
            <!-- /Form Buttons -->
        {{ Form::close() }}
    </div>
@stop

@section('page_script')

    @if(!I::can('create_admins') && !I::can('edit_admins'))
         <script>
             disableForm('#memberForm')
         </script>
    @endif

    <script>
        set_menu('mnu-admins');

        // Verify email/mobile uniqueness
        $(document).on('blur','#mobile, #email', function(){
            _this = $(this);

            if (!_this.val()) {
                $('#'+_this.attr('name')+'-error').hide();
                $('.form-buttons .pure-button-primary').prop('disabled', false);
                return;
            }

            $.get('/dashboard/admins/verify/'+_this.attr('name')+'/'+$(this).val()+'/'+MEMBER_ID, function(json){
                if(!json.unique){
                    $('#'+_this.attr('name')+'-error').show();
                    $('.form-buttons .pure-button-primary').prop('disabled', true);
                } else {
                    $('#'+_this.attr('name')+'-error').hide();
                    $('.form-buttons .pure-button-primary').prop('disabled', false);
                }
            });
        });

        $(document).on('click', ".form-buttons .pure-button-primary", function(e){
            if ($('ul#partner-listing li.empty').html() == 'No Partner Associated') {
                e.preventDefault();

                alert('A valid partner entity must be associated with a member account before saving.');

                return;
            }
        });
    </script>
@stop