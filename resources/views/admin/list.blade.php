@extends('layouts.dashboard')

@section('title')
    Admins | BLU
@stop

@section('body')
    <div class="padding">

        <div class="content-head pure-g">
            <div class="title pure-u-1-2">
                <h1><i class="fa fa-user fa-fw"></i>Admins ({{ number_format($admins->total()) }})</h1>
            </div>

            <div class="controls pure-u-1-2">
                <form action="{{ Filter::searchUrl() }}" method="get">
                    <input type="text" placeholder="Search" name="q" value="{{ Input::get('q') }}" />
                    <button type="submit" title="" class="list-search"></button>
                </form>
                @if(I::can('create_admins'))
                    <a href="{{ url('dashboard/admins/new') }}" class="pure-button pure-button-primary">Add Admin</a>
                @endif
            </div>

            <div id="catalog-filter" class="pure-u-1-2">
                <input id="filter_route" type="hidden" value="admins">
                <form id="filter-catalogue" action="{{ Filter::searchUrl() }}" method="get">

                    <div class="filter-block filter-block-pricing-width">
                        <span class="filter-label">Partner:</span>
                        <select style="width:100%;" name="partner_list[]" id="partner_list_select" multiple="multiple">
                            @foreach ($lists['partners'] as $key => $value)
                                <option value="{{ $key }}">{{ $value }}</option>
                            @endforeach
                        </select>
                        <input id="filterPartners" type="hidden" value="{{ Input::get('partner_list') }}">
                    </div>

                    <div class="filter-block filter-block-pricing-width">
                        <span class="filter-label">Type:</span>
                        <select style="width:100%;" name="role_list[]" data-filter-catalogue="role_list" data-filter-catalogue-title="Select Type" multiple="multiple">
                            @foreach ($lists['roles'] as $key => $value)
                                <option value="{{ $key }}">{{ $value }}</option>
                            @endforeach
                        </select>
                        <input data-filter-catalogue="role_list" type="hidden" value="{{ Input::get('role_list') }}">
                    </div>

                    <div class="submit-filter-block disable-width">
                        <div style="float:right;">
                            <a href="<?php echo e(url('dashboard/admins')); ?>" title="Clear Filters" class="pure-button pure-button-primary">
                                Clear Filters <i class="fa fa-ban fa-fw"></i>
                            </a>
                        </div>
                        <button class="pure-button pure-button-primary" type="submit" title="" style="float:right;margin-right:15px;">
                            Submit Filters
                        </button>
                    </div>
                </form>
            </div>
        </div>

        <table class="pure-table">
            <thead>
                <tr>
                    <th width="50"><a href="{{ Filter::baseUrl('sort=id') }}">ID</a></th>
                    <!--<th width="90">Image</th>-->
                    <th style="text-align:left;padding-left:20px;"><a href="{{ Filter::baseUrl('sort=last_name') }}">Name</a></th>
                    <th>Country</th>
                    <th><a href="{{ Filter::baseUrl('sort=email') }}">Email</a></th>
                    
                    
                    <th><a href="{{ Filter::baseUrl('sort=status') }}">Active</a></th>
                    
                    <th width="150">Actions</th>
                </tr>
            </thead>

            <tbody>
                @if(count($admins) > 0)
                    @foreach($admins as $m)
                        <tr>
                            <td>
                                {{{ $m->id }}}
                            </td>
<!--                            <td>
                                @if(empty($m->profile_image))
                                    <img src="http://placehold.it/74x74" alt="" />
                                @else
                                    <img src="/media/image/{{ $m->profile_image }}?s=74x74" alt="" />
                                @endif
                            </td>-->
                            <td style="text-align:left;padding-left:20px;">
                                <a href="/dashboard/admins/view/{{ base64_encode($m->id) }}" title="View Member">{{{ $m->first_name }}} {{{ $m->last_name }}}</a>
                            </td>
                            <td>
                                @if ($m->country)
                                    {{{ $m->country->name }}}
                                @else
                                    <i>N/A</i>
                                @endif
                            </td>
                            <td>
                                @if($m->email)
                                    <a href="mailto:{{ $m->email }}" title="Email Member">{{{ $m->email }}}</a>
                                @else
                                    <i>N/A</i>
                                @endif
                            </td>
                            
                            
                            <td>
                                @if($m->status == 'active')
                                    <span class="ico-yes"></span>
                                @else
                                    <span class="ico-no"></span>
                                @endif
                            </td>
                            
                            <td>
                                @if(I::am('BLU Admin'))
                                    <a href="/dashboard/admins/view/{{ base64_encode($m->id) }}" class="edit" title="View">View</a>
                                
                                    @if($m->id > 1)
                                        <a href="/dashboard/admins/delete/{{ base64_encode($m->id)}}" class="delete" title="Edit">Delete</a>
                                    @endif
                                @else
                                    @if($m->id > 1)
                                        @if(I::can('view_admins'))
                                            <a href="/dashboard/admins/view/{{ base64_encode($m->id) }}" class="edit" title="View">View</a>
                                        @endif

                                        @if(I::can('delete_admins'))
                                            <a href="/dashboard/admins/delete/{{ base64_encode($m->id) }}" class="delete" title="Edit">Delete</a>
                                        @endif   
                                    @else
                                        N/A                                     
                                    @endif
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="10">No Members Found</td>
                    </tr>
                @endif
            </tbody>
        </table>

        {{ $admins->setPath('admins')->appends(Input::except('page'))->links() }}

    </div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-admins');
    </script>
@stop