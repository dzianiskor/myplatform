@extends('layouts.dashboard')

@section('title')
    Affiliates | BLU
@stop

@section('body')
    <div class="padding">

        <div class="content-head pure-g">
            <div class="title pure-u-1-2">
                <h1><i class="fa fa-user fa-fw"></i>Affiliates ({{ number_format($affiliates->total()) }})</h1>
            </div>

            <div class="controls pure-u-1-2">
                <form action="{{ Filter::searchUrl() }}" method="get">
                    <input type="text" placeholder="Search" name="q" value="{{ Input::get('q') }}" />
                    <button type="submit" title="" class="list-search"></button>
                </form>
                @if(I::can('add_affiliates'))
                    <a href="{{ url('dashboard/affiliates/new') }}" class="pure-button pure-button-primary">Add Affiliate</a>
                @endif
            </div>


            <div id="catalog-filter" class="pure-u-1-2">
                <input id="filter_route" type="hidden" value="affiliates">
                <form id="filter-catalogue" action="{{ Filter::searchUrl() }}" method="get">

                    <div class="filter-block filter-block-pricing-width">
                        <span class="filter-label">Partner:</span>
                        <select style="width:100%;" name="affiliate_partner_list[]" data-filter-catalogue="affiliate_partner_list" data-filter-catalogue-title="Select Partner" multiple="multiple">
                            @foreach ($lists['partners'] as $key => $value)
                                <option value="{{ $key }}">{{ $value }}</option>
                            @endforeach
                        </select>
                        <input data-filter-catalogue="affiliate_partner_list" type="hidden" value="{{ Input::get('affiliate_partner_list') }}">
                    </div>

                    <div class="filter-block filter-block-pricing-width">
                        <span class="filter-label">Country:</span>
                        <select style="width:100%;" name="affiliate_country_list[]" data-filter-catalogue="affiliate_country_list" data-filter-catalogue-title="Select Country" multiple="multiple">
                            @foreach ($lists['countries'] as $key => $value)
                                <option value="{{ $key }}">{{ $value }}</option>
                            @endforeach
                        </select>
                        <input data-filter-catalogue="affiliate_country_list" type="hidden" value="{{ Input::get('affiliate_country_list') }}">
                    </div>

                    <div class="filter-block filter-block-pricing-width">
                        <span class="filter-label">Category:</span>
                        <select style="width:100%;" name="affiliate_category_list[]" data-filter-catalogue="affiliate_category_list" data-filter-catalogue-title="Select Category" multiple="multiple">
                            @foreach ($lists['categories'] as $parent)
                                @if ($parent->children->count())
                                    @foreach ($parent->children as $child)
                                        @if ($child->children->count())
                                            @foreach ($child->children as $parchild)
                                                <option value="{{ $parchild->id }}">{{ $parchild->name }}</option>
                                            @endforeach
                                        @else
                                            <option value="{{ $child->id }}">{{ $child->name }}</option>
                                        @endif
                                    @endforeach
                                @else
                                    <option value="{{ $parent->id }}">{{ $parent->name }}</option>
                                @endif
                            @endforeach
                        </select>
                        <input data-filter-catalogue="affiliate_category_list" type="hidden" value="{{ Input::get('affiliate_category_list') }}">
                    </div>

                    <div class="submit-filter-block disable-width">
                        <div style="float:right;">
                            <a href="<?php echo e(url('dashboard/affiliates')); ?>" title="Clear Filters" class="pure-button pure-button-primary">
                                Clear Filters <i class="fa fa-ban fa-fw"></i>
                            </a>
                        </div>
                        <button class="pure-button pure-button-primary" type="submit" title="" style="float:right;margin-right:15px;">
                            Submit Filters
                        </button>
                    </div>
                </form>
            </div>
        </div>

        <table class="pure-table">
            <thead>
                <tr>
                    <th width="50"><a href="{{ Filter::baseUrl('sort=id') }}">ID</a></th>
                    <th width="90">Image</th>
                    <th>Name</th>
                    <th>Enabled</th>
                    <th>Category</th>
                    <th>Country</th>
                    <th width="150">Actions</th>
                </tr>
            </thead>

            <tbody>
                @if(count($affiliates) > 0)
                    @foreach($affiliates as $affiliate)
                        <tr>
                            <td>
                                {{{ $affiliate->id }}}
                            </td>
                            <td>
                                @if(empty($affiliate->image))
                                    <img src="https://placehold.it/74x74" alt="" />
                                @else
                                    <img src="<?php echo App\Http\Controllers\MediaController::getImageFromId( $affiliate->image)?>?s=74x74" alt="" style="width:80px;"/>
                                @endif
                            </td>
                            <td style="text-align:center;padding-left:20px;">
                                <a href="/dashboard/affiliates/view/{{ base64_encode($affiliate->id) }}" title="View Affiliate">{{{ $affiliate->name }}}</a>
                            </td>
                            <td>
                                @if($affiliate->status == 'enabled')
                                    <span class="ico-yes"></span>
                                @else
                                    <span class="ico-no"></span>
                                @endif
                            </td>
                            <td>
                                @if(!empty($affiliate->category_id))
                                    {{{ App\Category::find($affiliate->category_id)->name }}}
                                @else
                                    {{ "N/A" }}
                                @endif
                            </td>
                            <td>
                                @if(!empty($affiliate->country_id))
                                    {{{ App\Country::find($affiliate->country_id)->name }}}
                                @else
                                    {{ "N/A" }}
                                @endif
                            </td>
                            

                            <td>
                                @if(I::am('BLU Admin'))
                                    <a href="/dashboard/affiliates/view/{{ base64_encode($affiliate->id) }}" class="edit" title="View">View</a>

                                    @if($affiliate->id > 1)
                                        <a href="/dashboard/affiliates/delete/{{ base64_encode($affiliate->id) }}" class="delete" title="Edit">Delete</a>
                                    @endif
                                @else
                                        @if(I::can('view_affiliates'))
                                            <a href="/dashboard/affiliates/view/{{ base64_encode($affiliate->id) }}" class="edit" title="View">View</a>
                                        @endif

                                        @if(I::can('delete_affiliates'))
                                            <a href="/dashboard/affiliates/delete/{{ base64_encode($affiliate->id) }}" class="delete" title="Edit">Delete</a>
                                        @endif
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="10">No Affiliates Found</td>
                    </tr>
                @endif
            </tbody>
        </table>

        {{ $affiliates->setPath('affiliates')->appends(Input::except('page'))->links() }}

    </div>
@stop

@section('page_script')
    <script>
        set_menu('mnu-affiliates');
    </script>
@stop