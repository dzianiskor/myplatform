@if(count($affiliate->partners) > 0)
    @foreach($affiliate->partners as $p)
        <li>
            {{ $p->name }}
            @if(I::can('delete_affiliates'))
                @if (Auth::User()->partners->contains($p) || I::am("BLU Admin"))
                    <a href="/dashboard/affiliates/unlinkaffiliate/{{ base64_encode($affiliate->id) }}/{{ base64_encode($p->id) }}" class="delete-ajax" data-id="{{ $p->id }}" data-target="#partner-listing">
                        [Delete]
                    </a>
                @endif
            @endif
        </li>
    @endforeach
@else
    <li class="empty">No Partner Associated</li>
@endif
