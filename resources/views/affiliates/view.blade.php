@extends('layouts.dashboard')

@section('js_globals')
    var AFFILIATE_ID = {{ $affiliate->id }};
@stop

@section('title')
    @if($affiliate->draft)
        Affiliate &raquo; New  | BLU
    @else
        Affiliate &raquo; {{{ $affiliate->name }}}  | BLU
    @endif
@stop

@section('dialogs')
    {{-- Assign new Partner --}}
    <div id="dialog-add-partner" style="width:300px;">
        <h4>Associate to Partner</h4>

        {{ Form::open(array('class' => "pure-form pure-form-stacked")) }}
        {{ Form::select('partner_id', $partners, null, array('class' => 'pure-input-1')) }}

        <a href="/dashboard/affiliates/attachaffiliate/{{ base64_encode($affiliate->id) }}" class="pure-button pure-button-primary attribute-action" data-target="#partner-listing">
            Save
        </a>
        {{ Form::close() }}
    </div>
@stop

@section('body')
    <div class="padding">

        {{-- Page Header --}}
        <div class="content-head pure-g">
            <div class="title pure-u-1-2">
                @if($affiliate->draft)
                    <h1><i class="fa fa-user fa-fw"></i>Affiliate &raquo; New</h1>
                @else
                    <h1><i class="fa fa-user fa-fw"></i>Affiliate &raquo; {{ $affiliate->name }}</h1>
                @endif
            </div>

            <div class="controls pure-u-1-2">
                <a href="{{ url('dashboard/affiliates') }}" class="pure-button">All Affiliates</a>
            </div>
        </div>
        {{-- /Page Header --}}

        {{ Form::open(array('url' 		   => url('/dashboard/affiliates/update/'.base64_encode($affiliate->id)),
                            'files' 	   => true,
                            'method' 	   => 'post',
                            'class' 	   => "pure-form pure-form-stacked form-width",
							'autocomplete' => 'off',
                            'id'           => 'memberForm')) }}

        @include('partials.errors')

        {!! Form::hidden('redirect_to', URL::previous()) !!}

        <div class="pure-g">
            <div class="pure-u-1-2">
                <fieldset>
                    <legend>Basic Details</legend>

                    {{ Form::label('status', 'Status') }}
                    {{ Form::select('status', $statuses, $affiliate->status, array('class' => 'pure-input-1-3')) }}

                    {{ Form::label('ucid', 'Unique Affiliate ID (Auto-generated)') }}
                    {{ Form::text('ucid', $affiliate->ucid, array('class' => 'pure-input-1', 'maxlength' => 9, 'disabled' => 'disabled')) }}

                    {{ Form::label('name', 'Affiliate Name') }}
                    {{ Form::text('name', $affiliate->name, array('class' => 'pure-input-1 required')) }}
                    <div class="pure-g">
                        <div class="pure-u-1-2">
                            <div class="padding-right-10">
                                {{ Form::label('contact_name', 'Contact Name') }}
                                {{ Form::text('contact_name', $affiliate->contact_name, array('class' => 'pure-input-1')) }}
                            </div>
                        </div>
                        <div class="pure-u-1-2">
                            {{ Form::label('contact_email', 'Contact Email') }}
                            {{ Form::text('contact_email', $affiliate->contact_email, array('class' => 'pure-input-1')) }}
                        </div>
                    </div>
                    <div class="pure-g">
                        <div class="pure-u-1-2">
                            <div class="padding-right-10">
                                {{ Form::label('contact_number', 'Contact Number') }}
                                {{ Form::text('contact_number', $affiliate->contact_number, array('class' => 'pure-input-1')) }}
                            </div>
                        </div>
                        <div class="pure-u-1-2">
                            {{ Form::label('contact_website', 'Contact Website') }}
                            {{ Form::text('contact_website', $affiliate->contact_website, array('class' => 'pure-input-1')) }}
                        </div>
                    </div>
                    {{ Form::label('category_id', 'Category', array('for'=>'category_id')) }}
                    {{ Form::select('category_id', $filter_categories, $affiliate->category_id, array('id' => 'category-list', 'class' => 'pure-input-1 required')) }}

                    {{ Form::label('description', 'Description', array('for'=>'description')) }}
                    {{ Form::textarea('description', $affiliate->description, array('class'=>'pure-input-1')) }}
                    <div class="image-rail" style="width:340px;">
                        <table style="margin-top:20px;margin-bottom:20px;">
                            <tr>
                                <td width="80">
                                    @if(empty($affiliate->image))
                                        <img src="https://placehold.it/74x74" alt=""/>
                                    @else
                                        <img src="<?php echo App\Http\Controllers\MediaController::getImageFromId($affiliate->image)?>?s=74x74" alt="" style="width:80px;"/>
                                    @endif
                                </td>
                                <td style="padding-left:10px;">
                                    {{ Form::label('image', 'Image') }}
                                    {{ Form::file('image') }}
                                </td>
                            </tr>
                        </table>
                    </div>
                </fieldset>

                <fieldset>
                    <legend>Address Details</legend>

                    {{ Form::label('country_id', 'Country') }}
                    {{ Markup::country_select('country_id', $affiliate->country_id, array('class' => 'pure-input-1-3')) }}

                    {{ Form::label('area_id', 'Area') }}
                    {{ Markup::area_select('area_id', $affiliate->country_id, $affiliate->area_id, array('class' => 'pure-input-1-3')) }}

                    {{ Form::label('city_id', 'City') }}
                    {{ Markup::city_select('city_id', $affiliate->area_id, $affiliate->city_id, array('class' => 'pure-input-1-3')) }}

                    {{ Form::label('address_1', 'Address') }}
                    {{ Form::text('address_1', $affiliate->address_1, array('class' => 'pure-input-1')) }}

                    {{ Form::label('address_2', 'Address (Continued)') }}
                    {{ Form::text('address_2', $affiliate->address_2, array('class' => 'pure-input-1')) }}
                </fieldset>
            </div>
            <div class="pure-u-1-2 attributes">
                <div class="padding-left-40">
                    {{-- Partner Listing --}}
                    <h3>
                        @if(I::can('edit_affiliates') || I::can('add_affiliates'))
                            <a href="#dialog-add-partner" title="New Partner" class="dialog">
                                Partners <i class="fa fa-plus-circle fa-fw"></i>
                            </a>
                        @else
                            Partners
                        @endif
                    </h3>
                    <ul id="partner-listing">
                        @if($affiliate->partners->count() > 0)
                            {{-- */$first = true;/* --}}
                            @foreach($affiliate->partners as $p)
                                @if (!$partners->has($p->id))
                                    @continue
                                @endif
                                @if(I::am('BLU Admin'))
                                    <li>
                                        {{ $p->name }}
                                        <a href="/dashboard/affiliates/unlinkaffiliate/{{ base64_encode($affiliate->id) }}/{{ base64_encode($p->id) }}" class="delete-ajax" data-id="{{ $p->id }}" data-target="#partner-listing">[Delete]</a>
                                    </li>
                                @else
                                    @if(in_array($p->id, $managedPartners))
                                        <li>
                                            {{ $p->name }}
                                            @if(I::can('delete_affiliates'))
                                                <a href="/dashboard/affiliates/unlinkaffiliate/{{ base64_encode($affiliate->id) }}/{{ base64_encode($p->id) }}" class="delete-ajax" data-id="{{ $p->id }}" data-target="#partner-listing">[Delete]</a>
                                            @endif
                                        </li>
                                    @else
                                        <li>
                                            <span style="font-style:italic;">{{ $p->name }}</span>
                                        </li>
                                    @endif
                                @endif

                                {{-- */$first = false;/* --}}
                            @endforeach
                        @else
                            <li class="empty">No Partner Associated</li>
                        @endif
                    </ul>
                    {{-- /Partner Listing --}}
                </div>
            </div>
        </div>

        <!-- Form Buttons -->
        <div class="form-buttons">
            <div class="left">
                <a href="{{ url('dashboard/affiliates') }}" class="pure-button">Cancel</a>
            </div>
            @if($affiliate->draft)
                @if(I::can('add_affiliates'))
                    <div class="right">
                        <button type="submit" class="pure-button pure-button-primary">Create Affiliate</button>
                    </div>
                @endif
            @else
                @if(I::can('edit_affiliates'))
                    <div class="right">
                        <button type="submit" class="pure-button pure-button-primary">Save Affiliate</button>
                    </div>
                @endif
            @endif
            <div class="clearfix"></div>
        </div>
        <!-- /Form Buttons -->
        {{ Form::close() }}
    </div>
@stop

@section('page_script')

    @if(!I::can('add_affiliates') && !I::can('edit_affiliates'))
        <script>
            disableForm('#memberForm')
        </script>
    @endif

    <script>
        set_menu('mnu-affiliates');
        $(document).on('click', ".form-buttons .pure-button-primary", function (e) {
            if ($('ul#partner-listing li.empty').html() == 'No Partner Associated') {
                e.preventDefault();

                alert('A valid partner entity must be associated with an affiliate before saving.');

                return;
            }
        });
    </script>
@stop
