<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

use Carbon\Carbon;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Admin::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'username' => $faker->userName,
        'email' => $faker->unique()->safeEmail,
        'first_name' => $faker->firstName(),
        'last_name' => $faker->lastName,
        'verified' => 1,
        'status' => 'active',
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Role::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->text(30),
        'partner_id' => 1,
    ];
});

$factory->define(App\Partner::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name(),
        'ucid' => uniqid(),
        'status' => 'Enabled',
    ];
});

$factory->define(App\Ticket::class, function () {
    return [
        'status'       => 'In Progress',
        'query_source' => 'Member',
        'user_id'      => 1,
        'partner_id'   => 1,
        'draft'        => false,
    ];
});

$factory->define(App\TransactionDate::class, function () {
    return [
        'transaction_date' => Carbon::today(),
        'trx_reward'       => random_int(1, 1000),
        'points_rewarded'  => random_int(1, 1000000),
        'points_redeemed'  => random_int(1, 1000000),
    ];
});

$factory->define(App\TransactionPoint::class, function () {
    return [
        'date_from'       => Carbon::today()->subDays(10),
        'date_to'         => Carbon::today()->addDays(10),
        'user_id'         => 1,
        'points_rewarded' => random_int(1, 1000000),
        'points_redeemed' => random_int(1, 1000000),
    ];
});

$factory->define(App\Network::class, function () {
    return [
        'parent_network_id' => 1,
        'draft'             => true,
        'name'              => "New Network",
        'affiliate_country' => 9,
        'affiliate_city'    => 672,
        'affiliate_area'    => 76
    ];
});

$factory->define(App\PartnerCategory::class, function (Faker\Generator $faker) {
    return [
        'draft' => 0,
        'name' => $faker->text(30),
    ];
});

$factory->define(App\Country::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->country,
        'short_code' => $faker->countryCode,
        'telephone_code' => '+966',
        'alpha2_code' => 'SA',
    ];
});

$factory->define(App\Area::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->word,
    ];
});

$factory->define(App\City::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->word,
    ];
});

$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'username' => $faker->userName,
        'email' => $faker->unique()->safeEmail,
        'first_name' => $faker->firstName(),
        'last_name' => $faker->lastName,
        'verified' => 1,
        'status' => 'active',
        'password' => $password ?: $password = bcrypt('secret'),
        'mobile' => '+971' . $faker->randomNumber(8),
        'normalized_mobile' => $faker->randomNumber(8),
        'telephone_code' => '+971',
        'address_1' => $faker->text(30),
        'address_2' => $faker->text(30),
        'draft' => 0,
        'blocked' => 0,
    ];
});

$factory->define(App\Segment::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->text(20),
        'partner_id' => 1,
        'draft' => 0,
    ];
});

$factory->define(App\Attribute::class, function () {
    return [
        'name' => 'user.email',
        'json_data' => '{"name":"user.email","email":"@gmail.com"}',
    ];
});

$factory->define(App\Supplier::class, function (Faker\Generator $faker) {
    return [
        'ucid' => uniqid(),
        'name' => $faker->text(20),
        'description' => $faker->text(200),
        'email' => $faker->unique()->safeEmail,
        'contact_name' => $faker->name(),
        'contact_email' => $faker->unique()->safeEmail,
        'status' => 'enabled',
        'draft' => 0,
    ];
});

$factory->define(App\PartnerCategory::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->text(20),
        'draft' => 0,
    ];
});

$factory->define(App\Supplierstore::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->text(20),
        'lng' => $faker->randomFloat(4, -180, 180),
        'lat' => $faker->randomFloat(4, -90, 90),
        'draft' => 0,
    ];
});

$factory->define(App\Store::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->text(20),
        'lng' => $faker->randomFloat(4, -180, 180),
        'lat' => $faker->randomFloat(4, -90, 90),
        'draft' => 0,
        'mapping_id' => 0,
    ];
});

$factory->define(App\Address::class, function (Faker\Generator $faker) {
    return [
        'street' => $faker->text(20),
        'floor' => 4,
        'building' => 1,
    ];
});

$factory->define(App\Tier::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->text(20),
        'partner_id' => 1,
        'start_month' => 1,
        'promotion_in_months' => 12,
        'demotion_in_months' => 12,
        'draft' => 0,
    ];
});

$factory->define(App\Tiercriteria::class, function (Faker\Generator $faker) {
    return [
        'criterion' => 'RewardedPoints',
        'name' => $faker->text(20),
        'start_value' => $faker->randomFloat(0, 1, 50),
        'end_value' => $faker->randomFloat(0, 51, 500),
        'draft' => 0,
    ];
});

$factory->define(App\Affiliate::class, function (Faker\Generator $faker) {
    return [
        'ucid' => uniqid(),
        'name' => $faker->text(20),
        'description' => $faker->text(200),
        'contact_email' => $faker->unique()->safeEmail,
        'contact_name' => $faker->name(),
        'contact_number' => $faker->randomNumber(5),
        'contact_website' => $faker->word . '.com',
        'status' => 'enabled',
        'draft' => 0,
    ];
});

$factory->define(App\Category::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->text(20),
        'draft' => 0,
    ];
});

$factory->define(App\Cattranslation::class, function (Faker\Generator $faker) {
    return [
        'category_id' => factory(\App\Category::class)->create()->id,
        'lang_id' => factory(\App\Language::class)->create()->id,
        'name' => $faker->text(20),
        'draft' => 0,
    ];
});

$factory->define(App\AffiliateProgram::class, function (Faker\Generator $faker) {
    return [
        'ucid' => uniqid(),
        'name' => $faker->text(20),
        'description' => $faker->text(200),
        'status' => 'enabled',
        'draft' => 0,
    ];
});

$factory->define(App\Loyalty::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->text(20),
        'description' => $faker->text(200),
        'ranking' => 1,
        'valid_from' => $faker->date(),
        'valid_from_time' => $faker->time(),
        'valid_to' => date('Y-m-d', time() + 100000),
        'valid_to_time' => $faker->time(),
        'approved' => 1,
        'draft' => 0,
    ];
});

$factory->define(App\Currency::class, function (Faker\Generator $faker) {
    $currency = 't' . $faker->unique()->currencyCode;

    return [
        'short_code' => $currency,
        'name' => $currency,
        'draft' => 0,
    ];
});

$factory->define(App\CurrencyTranslation::class, function (Faker\Generator $faker) {
    return [
        'currency_id' => factory(\App\Currency::class)->create()->id,
        'lang_id' => factory(\App\Language::class)->create()->id,
        'name' => $faker->text(20),
        'draft' => 0,
    ];
});

$factory->define(App\CurrencyPricing::class, function (Faker\Generator $faker) {
    return [
        'date' => $faker->date(),
        'rate' => $faker->randomFloat(),
    ];
});

$factory->define(App\LoyaltyRule::class, function (Faker\Generator $faker) {
    return [
        'type' => 'Points',
        'reward_pts' => $faker->numberBetween(100, 10000),
        'reward_usd' => 1,
        'rule_type' => 'Country',
        'rule_value' => 0,
        'currency_id' => factory(\App\Currency::class)->create()->id,
        'approved' => 1,
        'deleted' => 0,
    ];
});

$factory->define(App\LoyaltyExceptionrule::class, function () {
    return [
        'operator' => 'and',
        'type' => 'Country',
        'rule_value' => factory(\App\Country::class)->create()->id,
        'comparison_operator' => 'equal',
        'deleted' => 0,
    ];
});

$factory->define(App\AutoGeneratedReports::class, function (Faker\Generator $faker) {
    return [
        'name'   => $faker->text(20),
        'slug'   => $faker->text(20),
        'status' => 1,
        'draft'  => 0,
    ];
});

$factory->define(App\Batch::class, function (Faker\Generator $faker) {
    return [
        'name'            => $faker->text(20),
        'partner_id'      => factory(\App\Partner::class)->create()->id,
        'country_id'      => factory(\App\Country::class)->create()->id,
        'draft'           => 0,
        'type'            => 'Coupons',
        'price'           => '0.00',
        'valid_from'      => Carbon::today()->subDays(10),
        'valid_to'        => Carbon::today()->addDays(10),
        'number_of_items' => $faker->numberBetween(1, 10),
    ];
});

$factory->define(App\Coupon::class, function (Faker\Generator $faker) {
    $batch = factory(\App\Batch::class)->create();
    $coupon_code =  $batch->id . '-' . CouponHelper::generateRandomString();

    return [
        'partner_id'  => factory(\App\Partner::class)->create()->id,
        'batch_id'    => factory(\App\Batch::class)->create()->id,
        'coupon_code' => $coupon_code,
        'coupon_type' => 'value',
    ];
});

$factory->define(App\Brand::class, function (Faker\Generator $faker) {
    return [
        'name'   => $faker->text(20),
        'draft'  => 0,
    ];
});

$factory->define(App\Brandtranslation::class, function (Faker\Generator $faker) {
    return [
        'brand_id' => factory(\App\Brand::class)->create()->id,
        'lang_id' => factory(\App\Language::class)->create()->id,
        'name' => $faker->text(20),
        'draft' => 0,
    ];
});

$factory->define(App\Product::class, function (Faker\Generator $faker) {
    return [
        'name'                  => $faker->text(20),
        'brand_id'              => factory(\App\Brand::class)->create()->id,
        'category_id'           => factory(\App\Category::class)->create()->id,
        'draft'                 => 0,
        'end_date'              => Carbon::today()->addDays(10),
        'qty'                   => random_int(1, 10),
        'original_price'        => random_int(1, 10),
        'original_retail_price' => random_int(1, 10),
        'original_sales_tax'    => random_int(1, 10),
        'weight'                => random_int(1, 10),
        'volume'                => random_int(1, 10),
        'delivery_charges'      => random_int(1, 10),
        'status'                => 'active'
    ];
});

$factory->define(App\ProductChannel::class, function (Faker\Generator $faker) {
    return [
        'channel_id' => factory(\App\Partner::class)->create()->id
    ];
});

$factory->define(App\Notification::class, function (Faker\Generator $faker) {
    return [
        'partner_id' => factory(\App\Partner::class)->create(),
        'name'   => $faker->text(20),
        'description' => $faker->text(100),
        'type' => 'Email',
        'draft'  => 0,
        'invoked' => 0,
        'sent' => 0,
        'created_by' => factory(\App\Admin::class)->create(),
        'count' => 0,
        'start_date' => date(time() + 10000),
        'end_date' => date(time() + 20000),

    ];
});

$factory->define(App\Offer::class, function (Faker\Generator $faker) {
    return [
        'name'   => $faker->text(20),
        'short_description' => $faker->text(100),
        'description' => $faker->text(200),
        'display_online' => 1,
        'category_id' => factory(\App\Category::class)->create()->id,
        'logged_in'  => 1,
        'logged_out'  => 1,
        'draft'  => 0,
        'deleted'  => 0,
        'status' => 'Active',
    ];
});

$factory->define(App\Offertranslation::class, function (Faker\Generator $faker) {
    return [
        'offer_id' => factory(\App\Offer::class)->create()->id,
        'lang_id' => factory(\App\Language::class)->create()->id,
        'name'   => $faker->text(20),
        'short_description' => $faker->text(100),
        'description' => $faker->text(200),
        'draft' => 0,
    ];
});

$factory->define(App\Estatement::class, function (Faker\Generator $faker) {
    return [
        'title'   => $faker->text(20),
        'description' => $faker->text(200),
        'partner_id' => factory(\App\Partner::class)->create()->id,
        'delivery_date' => Carbon::today()->addDays(10),
        'status' => 'Active',
        'time' => Carbon::today()->format('H:m:i'),
        'draft'  => 0,
        'deleted'  => 0,
    ];
});

$factory->define(App\Estatementtranslation::class, function (Faker\Generator $faker) {
    return [
        'estatement_id' => factory(\App\Estatement::class)->create()->id,
        'lang_id' => factory(\App\Language::class)->create()->id,
        'Title'   => $faker->text(20),
        'description' => $faker->text(200),
    ];
});

$factory->define(App\Language::class, function (Faker\Generator $faker) {
    return [
        'name'       => $faker->text(10),
        'short_code' => $faker->languageCode(),
    ];
});

$factory->define(App\Airport::class, function (Faker\Generator $faker) {
    return [
        'code' => $faker->word(3),
        'name' => $faker->text(20),
        'city' => $faker->text(20),
        'state' => $faker->text(20),
        'country' => $faker->text(20),
    ];
});

$factory->define(App\Carrier::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->text(20),
        'alternative_name' => $faker->text(20),
        'iata_code' => $faker->word(2),
        'icao_code' => $faker->word(3),
        'callsign' => $faker->text(120),
        'country' => $faker->text(20),
        'is_active' => 'Y',
    ];
});

$factory->define(App\Prodtranslation::class, function (Faker\Generator $faker) {
    return [
    ];
});

$factory->define(App\ProductPartnerRedemption::class, function (Faker\Generator $faker) {
    return [
    ];
});

$factory->define(App\ProductPartnerReward::class, function (Faker\Generator $faker) {
    return [
    ];
});

$factory->define(App\ProductUpc::class, function (Faker\Generator $faker) {
    return [
    ];
});