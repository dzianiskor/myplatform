<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAutoGeneratedReportsDraftFields extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('auto_generated_reports', function($table)
		{
                   $table->boolean('draft')->default(true);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('auto_generated_reports', function($table)
		{
		    $table->dropColumn('draft');
		});             
	}

}