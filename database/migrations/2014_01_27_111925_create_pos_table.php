<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('pos', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name');
            $table->string('mapping_id');
            $table->integer('store_id')->unsigned();
			$table->timestamps();

			$table->index('mapping_id');                      
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('pos');
	}

}