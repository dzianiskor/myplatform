<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBillingAmountAndCurrencyFields extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('transaction', function($table)
		{
		    $table->decimal('partner_amount', 10,2)->nullable()->default(0);
                    $table->integer('partner_currency_id')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('transaction', function($table)
		{
		    $table->dropColumn('partner_amount');
                    $table->dropColumn('partner_currency_id');
		});
	}

}
