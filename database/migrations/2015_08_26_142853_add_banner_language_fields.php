<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBannerLanguageFields extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('banner', function($table)
		{
		    $table->integer('lang_id')->unsigned()->references('id')->on('localization_language');
                    $table->index('lang_id');  
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('banner', function($table)
		{
		    $table->dropColumn('lang_id');

		});
	}

}