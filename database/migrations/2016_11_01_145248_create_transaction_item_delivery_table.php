<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionItemDeliveryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('transaction_item_delivery', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('trx_id');
            $table->integer('trx_item_id');
            $table->integer('delivery_cost');
            $table->integer('network_id')->default(1);
            $table->decimal('amt_delivery', 10, 2);
            $table->integer('currency_id')->default(6);
            $table->boolean('reversed')->default(false);
            $table->date('reversed_at');
            $table->timestamps();

            $table->index('trx_id');
            $table->index('trx_item_id');
        });

        
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        
        Schema::dropIfExists('transaction_item_delivery');
	}

}
