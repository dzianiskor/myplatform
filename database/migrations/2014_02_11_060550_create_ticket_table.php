<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_type', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name', 100);
            $table->timestamps();
        });

        Schema::create('ticket_activity', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('ticket_id')->unsigned()->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            $table->text('description');
            $table->timestamps();

            $table->index('ticket_id');          
            $table->index('user_id');          
        });        

        Schema::create('ticket', function(Blueprint $table)
        {
            $table->increments('id');
            $table->enum('status', Meta::ticketStatuses());
            $table->integer('ticket_type_id')->unsigned()->nullable();
            $table->enum('query_source', Meta::ticketSources());
            $table->integer('source_id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('partner_id')->unsigned()->nullable();
            $table->text('ticket');
            $table->boolean('draft')->default(true);
            $table->timestamps();

            $table->index('status');          
            $table->index('partner_id');          
            $table->index('draft');          
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket_type');
        Schema::dropIfExists('ticket_activity');
        Schema::dropIfExists('ticket');
    }

}