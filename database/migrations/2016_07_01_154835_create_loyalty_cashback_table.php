<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoyaltyCashbackTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		

		Schema::create('loyalty_cashback', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('loyalty_id')->unsigned()->nullable();			
			$table->foreign('loyalty_id')->references('id')->on('loyalty')->onDelete('cascade');
			
			$table->integer('product_id')->unsigned();
			$table->timestamps();

			$table->index('loyalty_id');
                        $table->index('product_id');
		});		
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		
		Schema::dropIfExists('loyalty_cashback');
	}

}
