<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuditTrailUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('audit_trail_users', function(Blueprint $table)
        {
	        $table->increments('id');
	        $table->integer('partner_id')->unsigned();
                $table->integer('user_id')->unsigned();
	        $table->text('detail')->nullable();
	        $table->timestamps();

	        $table->index('partner_id');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('audit_trail_users');
	}

}
