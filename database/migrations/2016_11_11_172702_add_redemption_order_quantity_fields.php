<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRedemptionOrderQuantityFields extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
            Schema::table('redemption_order', function($table)
            {
                $table->integer('quantity')->nullable()->default(0);
            });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('redemption_order', function($table)
		{
		    $table->dropColumn('quantity');
		});
	}

}