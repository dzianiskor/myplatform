<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBannerTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('banner', function(Blueprint $table)
        {
            $table->increments('id');
            $table->boolean('draft')->default(true);
            $table->string('name', 100);
            $table->string('description', 100)->nullable();
            $table->string('title', 100)->nullable();
            $table->string('link', 255)->nullable();
            $table->enum('status', Meta::bannerStatuses())->default('offline');
            $table->integer('partner_id')->nullable();
            $table->integer('media_id')->nullable();
            $table->timestamps();

            $table->index('partner_id');
        });

        Schema::create('banner_country', function(Blueprint $table)
        {
            $table->integer('banner_id')->unsigned();
            $table->integer('country_id')->unsigned();

            $table->index('banner_id');
            $table->index('country_id');
        });

        Schema::create('banner_page', function(Blueprint $table)
        {
            $table->integer('banner_id')->unsigned();
            $table->string('page', 50);

            $table->index('banner_id');
            $table->index('page');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::dropIfExists('banner_segment');
        Schema::dropIfExists('banner');
	}

}
