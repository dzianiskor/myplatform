<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('admins', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('email');
			$table->string('username', 128);
			$table->string('password', 60);
			$table->boolean('verified')->default(false);
			$table->string('title')->nullable();
			$table->string('first_name', 60);
			$table->string('middle_name', 60);
			$table->string('last_name', 60);
			$table->enum('status', array('active', 'inactive', 'suspended'))->default('inactive');			
			$table->date('dob');
			$table->enum('gender', array('m', 'f'));
			$table->string('address_1')->nullable();
			$table->string('address_2')->nullable();
			$table->integer('country_id')->nullable();
			$table->integer('city_id')->nullable();
			$table->integer('area_id')->nullable();
			$table->integer('profile_image')->nullable();
			$table->string('tag')->nullable();
			$table->boolean('draft')->default(true);

			$table->string('api_key', 100);
			
			$table->timestamps();

			$table->index('api_key');
			$table->index('email');                                
			$table->index('verified');                
			$table->index('draft');                             
		});

		Schema::create('partner_admin', function(Blueprint $table)
                {
                    $table->increments('id');
                    $table->integer('partner_id');
                    $table->integer('admin_id');
                    $table->string('type', 100); // static | calculated

                    $table->index('partner_id');          
                    $table->index('admin_id');          
                });
                Schema::create('role_admin', function(Blueprint $table)
                {
                    $table->integer('role_id');
                    $table->integer('admin_id');

                    $table->index('role_id');          
                    $table->index('admin_id');                      
                });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('admins');

	}
	

}
