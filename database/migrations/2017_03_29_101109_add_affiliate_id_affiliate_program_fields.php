<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAffiliateIdAffiliateProgramFields extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('affiliate_program', function($table)
		{
		    $table->integer('affiliate_id')->default(0);
		});		
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('affiliate_program', function($table)
		{
		    $table->dropColumn('affiliate_id');
		});		
			
	}

}
