<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('cart', function(Blueprint $table)
        {
	        $table->increments('id');
	        $table->integer('user_id')->unsigned();
	        $table->integer('product_id')->unsigned();
	        $table->integer('price_in_points')->unsigned()->default(0);
	        $table->integer('quantity')->unsigned()->default(1);
            $table->enum('delivery_options', Meta::deliveryOptions());
	        $table->integer('delivery_charges')->unsigned()->default(0);
	        $table->timestamps();

	        $table->index('user_id');
	        $table->index('product_id');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('cart');
	}

}
