<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMobileGameTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
            Schema::create('mobile_installations', function(Blueprint $table)
            {
                $table->increments('id');
                $table->integer('user_id')->unsigned()->references('id')->on('users');
                $table->string('registration_id', 300);
                $table->string('type', 100);
                $table->timestamps();
                $table->index('user_id');
            });
            Schema::create('mobile_game_preferences', function(Blueprint $table)
            {
                $table->increments('id');
                $table->timestamp('start_date');
                $table->timestamp('end_date');
                $table->integer('media_id')->nullable();
                $table->integer('timer');
                $table->decimal('percentage', 10,2)->default(0);
                $table->integer('max_points_to_grant')->default(0);
                $table->integer('max_points_per_user')->default(0);
                $table->timestamps();
                $table->index('media_id');
            });
            Schema::create('mobile_game_ads', function(Blueprint $table)
            {
                $table->increments('id');
                $table->string('title', 300); 
                $table->integer('media_id')->nullable();
                $table->string('description', 300); 
                $table->string('url', 300); 
                $table->timestamps();
                $table->index('media_id');
            });
            Schema::create('mobile_user_games', function(Blueprint $table)
            {
                $table->increments('id');
                $table->integer('user_id')->unsigned()->references('id')->on('users');
                $table->integer('game_id')->unsigned()->references('id')->on('mobile_game_preferences');
                $table->timestamp('game_time');
                $table->integer('points_won')->unsigned();
                $table->boolean('winner')->default(false);
                $table->boolean('draw')->default(false);
                $table->timestamps();
                $table->index('user_id');
                $table->index('game_id');
            });
            Schema::create('mobile_push_notification', function(Blueprint $table)
            {
                $table->increments('id');
                $table->timestamp('push_time');
                $table->string('message', 300); 
                $table->integer('media_id')->nullable();
                $table->string('activity', 120);
                $table->timestamps();
            });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('mobile_installations');
                Schema::dropIfExists('mobile_game_preferences');
                Schema::dropIfExists('mobile_game_ads');
                Schema::dropIfExists('mobile_user_games');
                Schema::dropIfExists('mobile_push_notification');
	}

}