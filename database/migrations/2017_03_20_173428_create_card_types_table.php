<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardTypesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('partner_card_types', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('partner_card');
            $table->text('description');
            $table->integer('partner_id');
            $table->timestamps(); 

            $table->index('partner_card');
            $table->index('partner_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Scheme::dropIfExists('partner_card_types');
    }

}
