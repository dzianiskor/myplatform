<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOfferTranslationPromoTextFields extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('offer_translation', function($table)
		{
                    $table->text('email_text_promo_1')->nullable();
                    $table->text('email_text_promo_2')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('offer_translation', function($table)
		{
		    $table->dropColumn('email_text_promo_1');
		    $table->dropColumn('email_text_promo_2');
		});
	}

}
