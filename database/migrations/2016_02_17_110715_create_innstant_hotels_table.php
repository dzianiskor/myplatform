<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInnstantHotelsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
            Schema::create('innstant_hotels', function(Blueprint $table)
            {
                $table->increments('id');
                $table->integer('hotel_id');
                $table->string('hotel_name', 300);
                $table->string('hotel_seo_name', 300);
                $table->integer('city_id');
                $table->string('address', 300);
                $table->string('zip', 20);
                $table->string('phone', 300);
                $table->string('fax', 300);
                $table->string('longitude', 20);
                $table->string('latitude', 20);
                $table->integer('rating');
                $table->string('zone', 300);
                $table->integer('property_type');

            });

            Schema::create('innstant_descriptions', function(Blueprint $table)
            {
                $table->increments('id');
                $table->integer('hotel_id');
                $table->text('description');

            });

            Schema::create('innstant_facilities', function(Blueprint $table)
            {
                $table->increments('id');
                $table->integer('facility_id');
                $table->text('title');

            });

            Schema::create('innstant_hotel_facilities', function(Blueprint $table)
            {
                $table->increments('id');
                $table->integer('hotel_id');
                $table->text('facility_ids');

            });

            Schema::create('innstant_events', function(Blueprint $table)
            {
                $table->increments('id');
                $table->integer('event_id');
                $table->string('name', 300);
                $table->integer('type');
                $table->date('start_date');
                $table->date('end_date');
                $table->integer('city_id');
                $table->text('description');
                $table->integer('venue_id');
                $table->string('venue_name', 500);
                $table->string('venue_zip', 20);
                $table->string('venue_phone', 30);
                $table->text('main_image');
                $table->string('latitude', 20);
                $table->string('longitude', 20);
            });

            Schema::create('innstant_cities', function(Blueprint $table)
            {
                $table->increments('id');
                $table->integer('city_id');
                $table->string('city_name', 300);
                $table->string('city_seo_name', 300);
                $table->string('country_code', 300);
                $table->string('country_name', 300);
                $table->string('city_state', 300);
                $table->integer('priority');
            });

            Schema::create('innstant_images', function(Blueprint $table)
            {
                $table->increments('id');
                $table->integer('is_main');
                $table->integer('hotel_id');
                $table->string('title', 300)->nullable();
                $table->string('url', 300);
            });


	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
                Schema::dropIfExists('innstant_hotels');
                Schema::dropIfExists('innstant_descriptions');
                Schema::dropIfExists('innstant_facilities');
                Schema::dropIfExists('innstant_hotel_facilities');
                Schema::dropIfExists('innstant_events');
                Schema::dropIfExists('innstant_cities');
                Schema::dropIfExists('innstant_images');

	}

}
