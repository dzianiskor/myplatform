<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoyaltyTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('loyalty', function(Blueprint $table)
		{
			$table->increments('id');
			$table->enum('status', Meta::loyaltyStatuses());
			$table->string('name');
			$table->text('description')->nullable();
			$table->text('reward_sms')->nullable();
			$table->text('redemption_sms')->nullable();
			$table->integer('priority');
			$table->integer('partner_id')->unsigned()->nullable();
			$table->foreign('partner_id')->references('id')->on('partner')->onDelete('cascade');
			$table->integer('ranking');
            $table->date('valid_from');
            $table->time('valid_from_time');
            $table->date('valid_to');
            $table->time('valid_to_time');
            $table->boolean('draft')->default(true);
			$table->decimal('reward_pts', 5, 2)->nullable();
			$table->decimal('reward_usd')->nullable();
            $table->decimal('redemption_pts', 5, 2)->nullable();
			$table->decimal('redemption_usd')->nullable();
			$table->timestamps();

            $table->index('partner_id');
            $table->index('status');
		});

		Schema::create('loyalty_rule', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('loyalty_id')->unsigned()->nullable();			
			$table->foreign('loyalty_id')->references('id')->on('loyalty')->onDelete('cascade');
			$table->enum('type', Meta::loyaltyRuleTypes());
			$table->decimal('reward_pts', 5, 2)->nullable();
			$table->decimal('reward_usd')->nullable();
			$table->enum('rule_type', Meta::loyaltyExceptionRuleTypes());
			$table->integer('rule_value');
			$table->timestamps();

			$table->index('loyalty_id');			
		});		
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('loyalty_rule');
		Schema::dropIfExists('loyalty');
	}

}
