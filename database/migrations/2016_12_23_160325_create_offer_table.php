<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfferTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

        Schema::create('offer', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name');
            $table->text('short_description')->nullable();
            $table->text('description')->nullable();
            $table->text('terms_and_conditions')->nullable();
            $table->integer('cover_image')->unsigned()->nullable();
            $table->boolean('display_online')->default(true);
            $table->integer('category_id')->unsigned()->nullable();
            $table->date('validity_start_date')->nullable();
            $table->date('validity_end_date')->nullable();
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->boolean('logged_in');
            $table->boolean('logged_out');
            $table->text('email_text_promo_1')->nullable();
            $table->text('email_text_promo_2')->nullable();
            $table->text('external_link')->nullable();
            $table->boolean('draft')->default(true);
            $table->boolean('deleted')->default(false);
            $table->softDeletes();
            $table->timestamps();

            $table->index('category_id');
            $table->index('draft');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::dropIfExists('offer');
	}

}