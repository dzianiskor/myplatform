<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTransactionItemPriceInPointsNetworkIdFields extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('transaction_item', function($table)
		{
		    $table->integer('price_in_points')->nullable();
                    $table->integer('network_id')->default(0);
                    
                    
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('transaction_item', function($table)
		{
		    $table->dropColumn('price_in_points');
		    $table->dropColumn('network_id');
                    
		    
		});
	}

}
