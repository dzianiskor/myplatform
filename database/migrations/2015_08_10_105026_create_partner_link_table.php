<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartnerLinkTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
            
            Schema::create('partner_link', function(Blueprint $table)
            {
                $table->increments('id');
                $table->integer('partner_id')->unsigned()->references('id')->on('partner');
                $table->integer('partner_linked_id')->unsigned()->references('id')->on('partner');
                
            });
            
            
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
                Schema::dropIfExists('partner_link');
                
	}

}