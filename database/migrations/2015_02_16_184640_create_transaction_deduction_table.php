<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionDeductionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
            Schema::create('transaction_deduction', function(Blueprint $table)
            {
                $table->increments('id');
                $table->integer('trx_id')->unsigned()->references('id')->on('transaction');
                $table->integer('trx_ref_id')->unsigned()->references('id')->on('transaction');
                $table->boolean('trx_used')->default(false);
                $table->decimal('points_used', 10,2)->nullable()->default(0);
                $table->decimal('points_remaining', 10,2)->nullable()->default(0);
                $table->date('used_date');
                $table->timestamps();
            });
            
            
            
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('transaction_deduction');

                
	}

}