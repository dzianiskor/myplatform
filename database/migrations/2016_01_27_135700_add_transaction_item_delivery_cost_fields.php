<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTransactionItemDeliveryCostFields extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
            Schema::table('transaction_item', function($table)
            {
                $table->integer('delivery_cost')->nullable()->default(0);

            });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('transaction_item', function($table)
		{
		    $table->dropColumn('delivery_cost');

		});
	}

}