<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCountryCurrencySettings extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mobile', function($table)
		{
		    $table->mobileSettingsCountryCurrency();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mobile', function($table)
		{
		    $table->dropColumn('country_id');
                    $table->dropColumn('currency_id');
		});		

					
	}

}
