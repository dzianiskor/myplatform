<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductRewardTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
            Schema::create('product_reward', function(Blueprint $table)
            {
                $table->increments('id');
                $table->integer('partner_id')->unsigned()->references('id')->on('partner');
                $table->integer('product_id')->unsigned()->references('id')->on('product');
                $table->decimal('points_rewarded', 10,2)->nullable()->default(0);
                $table->date('start_date');
                $table->date('end_date');
                $table->timestamps();
            });
            Schema::create('user_invoice', function(Blueprint $table)
            {
                $table->increments('id');
                $table->integer('user_id')->unsigned()->references('id')->on('users');
                $table->integer('partner_id')->unsigned()->references('id')->on('partner');
                $table->date('invoice_date');
                $table->boolean('viewed');
                $table->boolean('inserted');
                $table->boolean('draft')->default(true);
                $table->timestamps();
            });
            Schema::create('media_invoice', function($table)
            {
                $table->increments('id');
                $table->integer('user_invoice_id')->unsigned()->references('id')->on('user_invoice');
                $table->integer('media_id')->unsigned();

                $table->index('user_invoice_id');            
                $table->index('media_id');            
            });
            
            
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('product_reward');
                Schema::dropIfExists('user_invoice');
                Schema::dropIfExists('media_invoice');
	}

}