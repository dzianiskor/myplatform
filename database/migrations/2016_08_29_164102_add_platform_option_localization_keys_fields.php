<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPlatformOptionLocalizationKeysFields extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('localization_keys', function($table)
		{
                    $table->boolean('is_platform_key')->default(false);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('localization_keys', function($table)
		{
		    $table->dropColumn('is_platform_key');
		});             
	}

}