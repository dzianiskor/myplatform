<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardprintTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('cardprint', function(Blueprint $table)
        {
            $table->increments('id');
            $table->boolean('draft')->default(true);
            $table->string('name', 100);
            $table->string('description', 100)->nullable();
            $table->integer('top')->default(0);
            $table->integer('left')->default(0);
            $table->integer('partner_id')->nullable();
            $table->integer('media_id')->nullable();
            $table->timestamps();

            $table->index('partner_id');
        });

        
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        
        Schema::dropIfExists('cardprint');
	}

}
