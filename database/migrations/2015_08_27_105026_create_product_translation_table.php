<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductTranslationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
            
            Schema::create('product_translation', function(Blueprint $table)
            {
                $table->increments('id');
                $table->integer('product_id')->unsigned()->references('id')->on('product');
                $table->integer('lang_id')->unsigned()->references('id')->on('localization_language');
                $table->string('name');
                $table->string('model', 100)->nullable();
                $table->string('sub_model', 100)->nullable();
                $table->text('description')->nullable();
                $table->boolean('draft')->default(true);
                $table->timestamps();
                
                $table->index('product_id'); 
                $table->index('lang_id'); 
                
            });
            
            
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
                Schema::dropIfExists('product_translation');
                
	}

}