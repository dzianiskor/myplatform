<?php

use Illuminate\Database\Migrations\Migration;

class AddPartnerCashbackMilesValuesFields extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up(){
            Schema::table('partner', function($table){
                $table->integer('currency_id')->nullable();
                $table->decimal('cashback_value_in_currency', 10, 3)->nullable();
                $table->decimal('miles_value_in_currency', 10, 3)->nullable();
            });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down(){
            Schema::table('partner', function($table){
                $table->dropColumn('currency_id')->nullable();
		$table->dropColumn('cashback_value_in_currency');
                $table->dropColumn('miles_value_in_currency');
            });
	}

}