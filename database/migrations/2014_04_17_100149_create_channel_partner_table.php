<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChannelPartnerTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('channel_partner', function(Blueprint $table)
        {
	        $table->increments('id');
	        $table->integer('partner_id')->unsigned();
	        $table->integer('channel_id')->unsigned();
	        $table->timestamps();

	        $table->index('partner_id');
	        $table->index('channel_id');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('channel_partner');
	}

}
