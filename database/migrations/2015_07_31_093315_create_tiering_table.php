<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTieringTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('tier', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name', 120);
            $table->string('description', 250);
            $table->integer('partner_id')->unsigned()->references('id')->on('partner')->onDelete('cascade');
            $table->integer('promotion_in_months')->default(0);
            $table->integer('demotion_in_months')->default(0);
            $table->integer('start_month')->default(1);
            $table->timestamps();
            $table->boolean('draft')->default(true);
            $table->index('partner_id');
        });
        Schema::create('tier_criteria', function(Blueprint $table)
        {
            $table->increments('id');
            $table->enum('criterion', array('RewardedPoints', 'NumTransactions', 'NumVisits'))->default('RewardedPoints');
            $table->string('name', 120);
            $table->integer('tier_id')->unsigned()->references('id')->on('tier')->onDelete('cascade');
            $table->integer('start_value')->default(0);
            $table->integer('end_value')->default(0);
            $table->timestamps();
            $table->boolean('draft')->default(true);
            
            $table->index('tier_id');

        });
        Schema::create('tiercriteria_user', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('tiercriteria_id');
            $table->integer('user_id');
            $table->timestamps();

            $table->index('tiercriteria_id');          
            $table->index('user_id');          
        });	
        


	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
            Schema::dropIfExists('tier');
            Schema::dropIfExists('tier_criteria');
            Schema::dropIfExists('tiercriteria_user');
            
	}

}
