<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocalizationProductTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
            Schema::create('localization_product', function(Blueprint $table)
            {
                $table->increments('id');
                $table->integer('localization_lang_id')->unsigned()->references('id')->on('localization_language');
                $table->integer('product_id')->unsigned()->references('id')->on('product');
                $table->string('title', 300);
                $table->text('description')->nullable();
                $table->string('model', 100)->nullable();
                $table->string('sub_model', 100)->nullable();
                $table->timestamps();
            });
           
            
            
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('localization_product');
                
	}

}