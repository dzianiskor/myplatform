<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSoftDeleteLoyaltyExceptionFields extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('loyalty_exceptionrule', function($table)
		{
                    $table->integer('rule_value_currency')->nullable();
		    $table->boolean('deleted')->default(0);
		    $table->integer('deleted_by')->nullable();
		    $table->timestamp('deleted_at')->nullable();
		});		
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('loyalty_exceptionrule', function($table)
		{
                    $table->dropColumn('rule_value_currency');
		    $table->dropColumn('deleted');
		    $table->dropColumn('deleted_by');
		    $table->dropColumn('deleted_at');
		});		
			
	}

}
