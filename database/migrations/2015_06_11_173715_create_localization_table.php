<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocalizationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
            Schema::create('localization_language', function(Blueprint $table)
            {
                $table->increments('id');
                $table->string('name', 128);
                $table->string('short_code',5);
                $table->timestamps();
            });
            Schema::create('localization_keys', function(Blueprint $table)
            {
                $table->increments('id');
                $table->string('key', 128);
                $table->string('description', 255);
                
                $table->timestamps();
            });
            Schema::create('localization_lang_keys', function($table)
            {
                $table->increments('id');
                $table->integer('lang_id')->unsigned()->references('id')->on('localization_language');
                $table->integer('key_id')->unsigned()->references('id')->on('localization_keys');
                $table->text('value')->nullable();
                $table->timestamps();
                
                $table->index('lang_id');            
                $table->index('key_id');            
            });
            
            
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('localization_language');
                Schema::dropIfExists('localization_keys');
                Schema::dropIfExists('localization_lang_keys');
	}

}