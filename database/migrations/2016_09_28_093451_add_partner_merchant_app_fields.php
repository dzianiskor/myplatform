<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPartnerMerchantAppFields extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('partner', function($table)
		{
		    $table->enum('merchant_app', array('default', '3-steps barcode'))->default('default');
                    
                    
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('partner', function($table)
		{
		    $table->dropColumn('merchant_app');
                    
		    
		});
	}

}
