<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPartnerColorFields extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
            Schema::table('partner', function($table)
            {
                $table->string('color', 128)->nullable();
                $table->string('prefix', 128);
                //$table->unique('prefix');

            });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('partner', function($table)
		{
		    $table->dropColumn('color');
                    $table->dropColumn('prefix');

		});
	}

}