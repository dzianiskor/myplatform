<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoyaltyExceptionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		

		Schema::create('loyalty_exceptionrule', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('loyalty_rule_id')->unsigned()->nullable();			
			$table->foreign('loyalty_rule_id')->references('id')->on('loyalty_rule')->onDelete('cascade');
                        $table->enum('operator', array('and', 'or'))->default('and');
			$table->enum('type', Meta::loyaltyExceptionRuleTypes());
                        $table->integer('rule_value');
			
			$table->timestamps();

			$table->index('loyalty_rule_id');			
		});		
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('loyalty_exceptionrule');
	}

}
