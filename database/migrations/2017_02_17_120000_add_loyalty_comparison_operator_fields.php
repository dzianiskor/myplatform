<?php

use Illuminate\Database\Migrations\Migration;

class AddLoyaltyComparisonOperatorFields extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up(){
            Schema::table('loyalty_exceptionrule', function($table){
                $table->enum('comparison_operator', array('equal', 'different', 'less_than', 'greater_than', 'less_or_equal', 'greater_or_equal'))->default('equal');
            });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down(){
            Schema::table('loyalty_exceptionrule', function($table){
                $table->dropColumn('comparison_operator');
            });
	}

}