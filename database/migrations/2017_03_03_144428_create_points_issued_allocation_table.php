<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePointsIssuedAllocationTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('points_issued_allocation', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('partner_id');
            $table->integer('trx_deduction_id')->unsigned()->nullable();
            $table->integer('trx_item_id')->unsigned()->nullable();
            $table->decimal('points_used', 10,2)->nullable()->default(0);
            $table->timestamps(); 

            $table->index('partner_id');
            $table->index('trx_item_id');
            $table->index('trx_deduction_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Scheme::dropIfExists('points_issued_allocation');
    }

}
