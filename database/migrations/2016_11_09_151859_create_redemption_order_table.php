.<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRedemptionOrderTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
            
            Schema::create('redemption_order', function(Blueprint $table)
            {
                $table->increments('id');
                $table->integer('trx_id');
                $table->integer('trx_item_id');
                $table->string('type');
                $table->integer('product_id')->nullable()->default(0);
                $table->string('product_name')->nullable();
                $table->string('product_model')->nullable();
                $table->string('product_sub_model')->nullable();
                $table->decimal('full_price_in_usd', 8, 2)->nullable()->default(0);
                $table->integer('delivery_cost_currency')->nullable()->default(0);
                $table->integer('supplier_id')->nullable()->default(0);
                
                $table->text('notes')->nullable();
                $table->string('passenger_name')->nullable();
                $table->string('company')->nullable();
                $table->string('start_booking_date')->nullable();
                $table->date('end_booking_date')->nullable();
                $table->string('airline_fees')->nullable();
                
                $table->decimal('cost', 8, 2)->nullable()->default(0);
                $table->integer('cost_currency')->nullable()->default(0);
                $table->decimal('delivery_cost', 8, 2)->nullable()->default(0);
                
                $table->decimal('payment_gateway_cost', 8, 2)->nullable()->default(0);
                $table->string('tracking_number')->nullable();
                $table->string('order_status')->nullable();
                $table->date('order_date')->nullable();
                $table->timestamps();
                
                $table->index('trx_id'); 
                $table->index('trx_item_id'); 
                $table->index('supplier_id'); 
                
            });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
                Schema::dropIfExists('redemption_order');
                
	}

}