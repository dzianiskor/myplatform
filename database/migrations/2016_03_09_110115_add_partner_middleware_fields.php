<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPartnerMiddlewareFields extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('partner', function($table)
		{
                    $table->boolean('has_middleware')->default(false);
                    $table->string('link_middleware_api');
                    $table->string('middleware_api_key', 64);

                    $table->index('middleware_api_key');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('partner', function($table)
		{
		    $table->dropColumn('has_middleware');
                    $table->dropColumn('link_middleware_api');
                    $table->dropColumn('middleware_api_key');

		});
	}

}