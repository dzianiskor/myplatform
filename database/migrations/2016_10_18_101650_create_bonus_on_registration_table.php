<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBonusOnRegistrationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('bonus_on_registration', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('partner_id');
            $table->integer('issued_by');
            $table->integer('points');
            $table->boolean('active')->default(false);
            $table->dateTime('valid_from')->nullable();
            $table->dateTime('valid_to')->nullable();
            $table->text('segment')->nullable();
            $table->timestamps();

            $table->index('partner_id');          
            $table->index('id');          
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('bonus_on_registration');
	}

}