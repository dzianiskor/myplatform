-<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ResyncIndexes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users', function(Blueprint $table)
		{
			$table->index('email');                
			$table->index('passcode');                
			$table->index('verified');                
			$table->index('draft');                
			$table->index('ref_account');          
		});

		Schema::table('balance', function(Blueprint $table)
		{
			$table->index('user_id');                
			$table->index('network_id');     
		});	

        Schema::table('currency_pricing', function(Blueprint $table)
        {
            $table->index('currency_id');                
        });	

        Schema::table('partner', function(Blueprint $table)
        {
            $table->index('parent_id');                
            $table->index('status');
        });        				

		Schema::table('network', function(Blueprint $table)
		{
            $table->index('status');    
            $table->index('draft');    
        });        

        Schema::table('network_partner', function(Blueprint $table)
        {
            $table->index('partner_id');    
            $table->index('network_id');    
        });        

        Schema::table('network_user', function(Blueprint $table)
        {
            $table->index('user_id');    
            $table->index('network_id');                
        });   

        Schema::table('product', function(Blueprint $table)
        {
            $table->index('brand_id');          
            $table->index('hot_deal');          
            $table->index('status');          
            $table->index('draft');          
        });    

		Schema::table('segment', function(Blueprint $table)
		{
            $table->index('partner_id');          
		});  

        Schema::table('segment_user', function(Blueprint $table)
        {
            $table->index('segment_id');          
            $table->index('user_id');          
        });		 

        Schema::table('area', function(Blueprint $table)
        {
            $table->index('country_id');            
        });                              

        Schema::table('city', function(Blueprint $table)
        {
            $table->index('area_id');            
        });        

        Schema::table('telephone_area_code', function(Blueprint $table)
        {
            $table->index('country_id');          
        });        

		Schema::table('media', function($table)
		{
		    $table->index('partner_id');            
		});   

        Schema::table('media_product', function($table)
        {
            $table->index('product_id');            
            $table->index('media_id');            
        });		     

        Schema::table('address', function(Blueprint $table)
        {
			$table->index('mapping_key');            
        });       

        Schema::table('store', function(Blueprint $table)
        {
			$table->index('mapping_id');            
			$table->index('partner_id');            
        });         

        Schema::table('pos', function(Blueprint $table)
        {
			$table->index('mapping_id');                      
        });    

        Schema::table('role', function(Blueprint $table)
        {
            $table->index('partner_id');          
        });            

        Schema::table('permission_role', function(Blueprint $table)
        {
            $table->index('role_id');          
            $table->index('permission_id');          
        });        

        Schema::table('role_user', function(Blueprint $table)
        {
            $table->index('role_id');          
            $table->index('user_id');                      
        });

		Schema::table('card', function(Blueprint $table)
		{
			$table->index('number');          
			$table->index('user_id');          
			$table->index('batch_id');          
		});        

        Schema::table('partner_user', function(Blueprint $table)
        {
            $table->index('partner_id');          
            $table->index('user_id');          
        });		

        Schema::table('transaction', function(Blueprint $table)
        {
            $table->index('partner_id');          
            $table->index('store_id');          
            $table->index('pos_id');          
            $table->index('user_id');          
            $table->index('trx_reward');          
            $table->index('trx_redeem');          
            $table->index('rule_id');          
            $table->index('voided');          
            $table->index('country_id');          
            $table->index('city_id');          
            $table->index('area_id');
        });

        Schema::table('transaction_item', function(Blueprint $table)
        {
            $table->index('transaction_id');          
            $table->index('product_id');          
            $table->index('product_model');          
            $table->index('product_brand_id');          
            $table->index('product_category_id');          
        });

        Schema::table('attribute', function(Blueprint $table)
        {
            $table->index('mapping_key');          
        });	

        Schema::table('ticket_activity', function(Blueprint $table)
        {
            $table->index('ticket_id');          
            $table->index('user_id');          
        }); 

        Schema::table('ticket', function(Blueprint $table)
        {
            $table->index('status');          
            $table->index('partner_id');          
            $table->index('draft');          
        });        

        Schema::table('batch', function(Blueprint $table)
        {
            $table->index('partner_id');                        
            $table->index('draft');  
            $table->index('valid_from');          
            $table->index('valid_to');          
        });

        Schema::table('coupon', function(Blueprint $table)
        {
            $table->index('batch_id');                                    
            $table->index('partner_id');                                    
        });   

		Schema::table('reference', function(Blueprint $table)
		{
			$table->index('number');			
			$table->index('user_id');			
		});     

		Schema::table('loyalty', function(Blueprint $table)
		{
            $table->index('partner_id');
            $table->index('status');
		});		 

		Schema::table('loyalty_rule', function(Blueprint $table)
		{
			$table->index('loyalty_id');			
		});

        Schema::table('country_product', function(Blueprint $table)
        {
            $table->index('country_id');
            $table->index('product_id');
        });	

        Schema::table('network_product', function(Blueprint $table)
        {
            $table->index('network_id');
            $table->index('product_id');            
        });        				       

        Schema::table('partner_product', function(Blueprint $table)
        {
            $table->index('partner_id');
            $table->index('product_id');            
        });

        Schema::table('product_segment', function(Blueprint $table)
        {
            $table->index('segment_id');
            $table->index('product_id');            
        });        

        Schema::table('channel_product', function(Blueprint $table)
        {
            $table->index('channel_id');
            $table->index('product_id');            
        }); 

		Schema::table('notification', function(Blueprint $table)
		{
			$table->index('partner_id');
			$table->index('draft');
			$table->index('invoked');
			$table->index('sent');
		});	    

        Schema::table('notification_segment', function(Blueprint $table)
        {
            $table->index('segment_id');
            $table->index('notification_id');
        });      

        Schema::table('banner', function(Blueprint $table)
        {
            $table->index('partner_id');
        });   

        Schema::table('banner_country', function(Blueprint $table)
        {
            $table->index('banner_id');
            $table->index('country_id');
        });

        Schema::table('banner_page', function(Blueprint $table)
        {
            $table->index('banner_id');
            $table->index('page');
        }); 

        Schema::table('mobile', function(Blueprint $table)
        {
            $table->index('uid');
        });   

        Schema::table('favorite', function(Blueprint $table)
        {
            $table->index('user_id');
            $table->index('product_id');
        });   

        Schema::table('cart', function(Blueprint $table)
        {
            $table->index('user_id');
            $table->index('product_id');
        });        

        Schema::table('order', function(Blueprint $table)
        {
            $table->index('user_id');
            $table->index('transaction_id');
        });  

        Schema::table('channel_partner', function(Blueprint $table)
        {
            $table->index('partner_id');
            $table->index('channel_id');
        });                       


	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}