<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRedemptionOrderAirlineFeesCurrencyFields extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
            Schema::table('redemption_order', function($table)
            {
                $table->integer('airline_fees_currency')->default(6);
            });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('redemption_order', function($table)
		{
		    $table->dropColumn('airline_fees_currency');
		});
	}

}