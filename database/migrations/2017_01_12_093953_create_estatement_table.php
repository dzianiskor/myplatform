<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstatementTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

            Schema::create('estatement', function(Blueprint $table)
            {
                $table->increments('id');
                $table->string('title');
                $table->text('description');
                $table->integer('partner_id')->unsigned();
                $table->integer('delivery_date')->unsigned();
                $table->enum('status', Meta::offerStatuses());
                $table->time('time');
                $table->boolean('draft')->default(true);
                $table->boolean('deleted')->default(false);
                $table->softDeletes();
                $table->timestamps();

                $table->index('partner_id'); 
                $table->index('draft');
            });
            
            Schema::create('estatement_history', function(Blueprint $table)
            {
                $table->increments('id');
                $table->string('title');
                $table->text('description');
                $table->integer('estatement_id')->unsigned();
                $table->integer('partner_id')->unsigned();
                $table->integer('delivery_date')->unsigned();
                $table->enum('status', Meta::offerStatuses());
                $table->time('time');
                $table->boolean('draft')->default(true);
                $table->boolean('deleted')->default(false);
                $table->text('coutries')->nullable();
                $table->text('segments')->nullable();
                $table->text('tiers')->nullable();
                $table->text('items')->nullable(); //Named Items because it may be a product or an offer
                $table->timestamps();
                
                $table->index('partner_id'); 
                $table->index('draft');
            });
            
            Schema::create('country_estatement', function(Blueprint $table)
            {
                $table->integer('country_id')->unsigned()->references('id')->on('country')->onDelete('cascade');
                $table->integer('estatement_id')->unsigned()->references('id')->on('estatement')->onDelete('cascade');

                $table->index('country_id');
                $table->index('estatement_id');
            });

            Schema::create('tier_estatement', function(Blueprint $table)
            {
                $table->integer('tier_id')->unsigned()->references('id')->on('tier')->onDelete('cascade');
                $table->integer('estatement_id')->unsigned()->references('id')->on('estatement')->onDelete('cascade');

                $table->index('tier_id');
                $table->index('estatement_id');            
            });
            
            Schema::create('estatement_segment', function(Blueprint $table)
            {
                $table->integer('segment_id')->unsigned()->references('id')->on('segment')->onDelete('cascade');
                $table->integer('estatement_id')->unsigned()->references('id')->on('estatement')->onDelete('cascade');

                $table->index('segment_id');
                $table->index('estatement_id');            
            });
            
            Schema::create('estatement_items', function(Blueprint $table) //Named Items because it may be a product or an offer
            {
                $table->increments('id');
                $table->integer('estatement_id')->unsigned()->references('id')->on('estatement')->onDelete('cascade');
                $table->string('type');
                $table->integer('item_id')->unsigned();

                $table->index('item_id');
                $table->index('estatement_id');            
            });
            
            Schema::create('estatement_translation', function(Blueprint $table)
            {
                $table->increments('id');
                $table->integer('estatement_id')->unsigned()->references('id')->on('estatement');
                $table->integer('lang_id')->unsigned()->references('id')->on('localization_language');
                $table->string('Title');
                $table->text('description')->nullable();
                $table->timestamps();
                
                $table->index('estatement_id'); 
                $table->index('lang_id'); 
                
            });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::dropIfExists('estatement');
        Schema::dropIfExists('estatement_history');
        Schema::dropIfExists('country_estatement');
        Schema::dropIfExists('tier_estatement');
        Schema::dropIfExists('estatement_segment');
        Schema::dropIfExists('estatement_items');
        Schema::dropIfExists('estatement_translation');
	}

}