<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAffiliatesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('affiliate', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id');
            $table->integer('image')->nullable();
            $table->string('ucid', 9);
            $table->enum('status', array('enabled', 'disabled'))->default('enabled');
            $table->string('name', 128);
            $table->text('description');
            $table->string('contact_name', 128);
            $table->string('contact_email');
            $table->string('contact_number');
            $table->string('contact_website');
            $table->string('address_1')->nullable();
            $table->string('address_2')->nullable();
            $table->integer('country_id')->nullable();
            $table->integer('city_id')->nullable();
            $table->integer('area_id')->nullable();
            $table->boolean('draft')->default(true);
            $table->timestamps(); 

            $table->index('id');
            $table->index('category_id');
            $table->index('draft');
        });
        
        Schema::create('affiliate_partner', function(Blueprint $table)
            {
                $table->increments('id');
                $table->integer('affiliate_id');
                $table->integer('partner_id');
                
                $table->index('affiliate_id'); 
                $table->index('partner_id');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Scheme::dropIfExists('affiliate');
        Scheme::dropIfExists('affiliate_partner');
    }

}
