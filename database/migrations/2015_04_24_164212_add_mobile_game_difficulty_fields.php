<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMobileGameDifficultyFields extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mobile_game_preferences', function($table)
		{
		    $table->enum('difficulty', array('easy', 'medium', 'hard'))->default('easy');
                    
                    
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mobile_game_preferences', function($table)
		{
		    $table->dropColumn('difficulty');
                    
		    
		});
	}

}
