<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusGameFields extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mobile_game_preferences', function($table)
		{
		    $table->boolean('enabled')->default(false);
                    
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mobile_game_preferences', function($table)
		{
		    $table->dropColumn('enabled');
                    
		    
		});
	}

}
