<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMerchantCategoryClassificationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
            Schema::create('mcccodes', function(Blueprint $table){
                $table->increments('id');
                $table->string('mcc', 4)->unique();
                $table->string('edited_description', 200)->nullable();
                $table->string('combined_description', 200)->nullable();
                $table->string('usda_description', 200)->nullable();
                $table->string('irs_description', 200)->nullable();
                $table->string('irs_reportable', 200)->nullable();

                $table->timestamps();  
            });

            

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{   
            Schema::dropIfExists('mcccodes');
            
            
	}

}