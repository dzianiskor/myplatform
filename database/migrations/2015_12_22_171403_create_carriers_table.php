<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarriersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('carriers', function(Blueprint $table)
		{
			$table->increments('id');
                        $table->string('name', 120);
                        $table->string('alternative_name', 120)->nullable();
			$table->string('iata_code',2)->nullable();
                        $table->string('icao_code',3)->nullable();
                        $table->string('callsign', 120)->nullable();
                        $table->string('country', 120)->nullable();
                        $table->enum('is_active', array('Y', 'N'))->default('N');
			$table->timestamps();               
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('carriers');
	}

}