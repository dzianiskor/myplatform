<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRefNumberTransactionItemFields extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('transaction_item', function($table)
		{
		    $table->text('ref_number')->nullable();
		});
                
                Schema::table('transaction_item_delivery', function($table)
		{
                    $table->text('ref_number')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
            Schema::table('transaction_item', function($table)
            {
                $table->dropColumn('ref_number');
            });

            Schema::table('transaction_item_delivery', function($table)
            {
                $table->dropColumn('ref_number');
            });
	}

}
