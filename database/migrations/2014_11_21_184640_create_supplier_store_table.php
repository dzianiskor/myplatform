<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupplierStoreTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('supplier_store', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name');
            $table->decimal('lng', 18, 14)->nullable();
            $table->decimal('lat', 18, 14)->nullable();
            $table->integer('supplier_id')->unsigned()->references('id')->on('supplier');
            $table->boolean('draft')->default(true);
            $table->timestamps();

            $table->index('supplier_id');            
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('supplier_store');
	}

}