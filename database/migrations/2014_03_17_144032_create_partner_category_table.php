<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartnerCategoryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('partnercategory', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name', 100);
			$table->boolean('draft')->default(true);
			$table->timestamps();
      	});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Scheme::dropIfExists('partner_categories');
	}

}
