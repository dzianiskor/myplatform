<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupplierPartnerLinkTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        
            Schema::create('supplier_partner', function(Blueprint $table)
            {
                    $table->increments('id');
                    $table->integer('supplier_id')->unsigned()->references('id')->on('supplier');
                    $table->integer('partner_id')->unsigned()->references('id')->on('partner');

                    $table->index('supplier_id');
                    $table->index('partner_id');

            });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('supplier_partner');
                
	}

}
