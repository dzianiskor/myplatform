<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionTypesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
            Schema::create('transaction_types', function(Blueprint $table){
                $table->increments('id');
                $table->string('trx_code', 2)->unique();
                $table->string('name', 200)->nullable();

                $table->timestamps();  
            });

            

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{   
            Schema::dropIfExists('transaction_types');
            
            
	}

}