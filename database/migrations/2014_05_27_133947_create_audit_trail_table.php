<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuditTrailTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('audit_trail', function(Blueprint $table)
        {
	        $table->increments('id');
	        $table->integer('partner_id')->unsigned();
	        $table->text('detail')->nullable();
	        $table->timestamps();

	        $table->index('partner_id');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('audit_trail');
	}

}
