<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('country', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('name', 100);
            $table->string('short_code', 5);
            $table->string('telephone_code', 10);
            $table->string('alpha2_code', 2);
		});

        Schema::create('area', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('country_id')->unsigned();
            $table->string('name', 100);

            $table->index('country_id');            
        });

        Schema::create('city', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('area_id')->unsigned();
            $table->string('name', 100);

            $table->index('area_id');            
        });

        Schema::create('telephone_area_code', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('country_id')->unsigned();
            $table->string('area_code', 10);

            $table->index('country_id');          
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::dropIfExists('country');
        Schema::dropIfExists('city');
        Schema::dropIfExists('area');
        Schema::dropIfExists('telephone_area_code');
	}

}