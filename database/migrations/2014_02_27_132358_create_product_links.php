<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductLinks extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('country_product', function(Blueprint $table)
        {
            $table->integer('country_id')->unsigned()->references('id')->on('country')->onDelete('cascade');
            $table->integer('product_id')->unsigned()->references('id')->on('product')->onDelete('cascade');

            $table->index('country_id');
            $table->index('product_id');
        });

        Schema::create('network_product', function(Blueprint $table)
        {
            $table->integer('network_id')->unsigned()->references('id')->on('network')->onDelete('cascade');
            $table->integer('product_id')->unsigned()->references('id')->on('product')->onDelete('cascade');

            $table->index('network_id');
            $table->index('product_id');            
        });

        Schema::create('partner_product', function(Blueprint $table)
        {
            $table->integer('partner_id')->unsigned()->references('id')->on('partner')->onDelete('cascade');
            $table->integer('product_id')->unsigned()->references('id')->on('product')->onDelete('cascade');

            $table->index('partner_id');
            $table->index('product_id');            
        });

        Schema::create('product_segment', function(Blueprint $table)
        {
            $table->integer('segment_id')->unsigned()->references('id')->on('segment')->onDelete('cascade');
            $table->integer('product_id')->unsigned()->references('id')->on('product')->onDelete('cascade');

            $table->index('segment_id');
            $table->index('product_id');            
        });

        Schema::create('channel_product', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('product_id');
            $table->integer('channel_id');

            $table->index('channel_id');
            $table->index('product_id');            
        });        
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::dropIfExists('country_product');
        Schema::dropIfExists('network_product');
        Schema::dropIfExists('partner_product');
        Schema::dropIfExists('product_segment');
        Schema::dropIfExists('channel_product');
	}

}
