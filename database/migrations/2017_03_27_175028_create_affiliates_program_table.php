<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAffiliatesProgramTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('affiliate_program', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('image')->nullable();
            $table->string('ucid', 9);
            $table->enum('status', array('enabled', 'disabled'))->default('enabled');
            $table->string('name', 128);
            $table->text('description');
            $table->boolean('draft')->default(true);
            $table->timestamps(); 

            $table->index('id');
            $table->index('draft');
        });
        
        Schema::create('affiliate_program_partner', function(Blueprint $table)
            {
                $table->increments('id');
                $table->integer('affiliate_program_id');
                $table->integer('partner_id');
                
                $table->index('affiliate_program_id'); 
                $table->index('partner_id');
            });
        
        Schema::create('affiliate_programs_memberships', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable();
            $table->string('membership_number', 128);
            $table->timestamps(); 

            $table->index('id');
            $table->index('user_id');
        });    
            
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Scheme::dropIfExists('affiliate_program');
        Scheme::dropIfExists('affiliate_program_partner');
        Scheme::dropIfExists('affiliate_programs_memberships');
    }

}
