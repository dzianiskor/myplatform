<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPartnerAdminStaticIpFields extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
            // add enforce_static_ip column to partner as boolean
		Schema::table('partner', function($table)
		{
		   $table->boolean('enforce_static_ip')->default(1);
		});
            // add static_ip column to admins as string default as varchar 255
                Schema::table('admins', function($table)
		{
		  $table->string('static_ip')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('partner', function($table)
		{
		    $table->dropColumn('enforce_static_ip');
		});

                Schema::table('admins', function($table)
		{
		  $table->dropColumn('static_ip');
		});
	}

}