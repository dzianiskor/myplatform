<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuppliersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        
        Schema::create('supplier', function(Blueprint $table)
        {
	        $table->increments('id');
                $table->string('ucid', 9);
                $table->string('name', 128);
                $table->integer('image')->nullable()->default(null);
                $table->text('description')->nullable();
                $table->string('email', 255)->nullable();
                $table->string('website', 255)->nullable();
                $table->string('phone_number',100)->nullable();
                $table->string('contact_name', 128)->nullable();
                $table->string('contact_email', 120)->nullable();
                $table->string('contact_number', 100)->nullable();
                $table->string('address_1')->nullable();
                $table->string('address_2')->nullable();
                $table->integer('country_id')->nullable();
                $table->integer('city_id')->nullable();
                $table->integer('area_id')->nullable();
                $table->enum('status', Meta::productStatuses());
                $table->integer('parent_id')->nullable();
                $table->integer('category_id')->nullable()->unsigned()->references('id')->on('partnercategory');
                $table->timestamps();
                
                $table->index('parent_id');  
                $table->index('status'); 
                

        });
        Schema::table('product', function($table)
        {
            $table->integer('supplier_id')->nullable()->unsigned()->references('id')->on('supplier');
	});
        
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('supplier');
                
	}

}
