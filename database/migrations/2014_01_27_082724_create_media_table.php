<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMediaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('media', function($table)
		{
		    $table->increments('id');
		    $table->string('name');
		    $table->bigInteger('size');
		    $table->integer('partner_id');
		    $table->string('type');
		    $table->string('ext');
		    $table->timestamps();

		    $table->index('partner_id');            
		});

        Schema::create('media_product', function($table)
        {
            $table->increments('id');
            $table->integer('product_id')->unsigned();
            $table->integer('media_id')->unsigned();

            $table->index('product_id');            
            $table->index('media_id');            
        });
    }

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('media');
		Schema::dropIfExists('media_product');
	}

}