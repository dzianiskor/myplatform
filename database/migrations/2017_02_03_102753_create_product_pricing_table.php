<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductPricingTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

            Schema::create('product_partner_reward', function(Blueprint $table)
            {
                $table->increments('id');
                $table->integer('product_id')->unsigned();
                $table->integer('partner_id')->unsigned();
                $table->string('partner_mapping_id');
                $table->string('sku');
                $table->string('partnumber');
                
                $table->index('product_id'); 
                $table->index('partner_id'); 
                $table->index('partner_mapping_id'); 
            });
            
            Schema::create('product_partner_redemption', function(Blueprint $table)
            {
                $table->increments('id');
                $table->integer('product_id')->unsigned();
                $table->integer('partner_id')->unsigned();
                $table->integer('supplier_id')->unsigned();
                $table->boolean('display_online')->default(true);
                $table->boolean('hot_deal')->default(false);
                $table->boolean('show_offline')->default(true);
                $table->integer('qty')->default(0);
		$table->decimal('original_price', 8, 2)->nullable();
                $table->decimal('original_retail_price', 8, 2)->nullable();
                $table->integer('currency_id')->nullable();
                $table->integer('price_in_points')->nullable();
                $table->decimal('price_in_usd', 8, 2)->nullable();
                $table->decimal('retail_price_in_usd', 8, 2)->nullable();
                $table->boolean('draft')->default(true);
                $table->date('start_date')->nullable();
                $table->date('end_date')->nullable();
                
                $table->index('partner_id'); 
                $table->index('product_id'); 
                $table->index('draft');
            });
            
            Schema::create('product_redemption_display_channel', function(Blueprint $table)
            {
                $table->increments('id');
                $table->integer('product_redemption_id')->unsigned()->references('id')->on('product_redemption')->onDelete('cascade');
                $table->integer('partner_id')->unsigned()->references('id')->on('partner')->onDelete('cascade');
                $table->integer('product_id')->unsigned()->references('id')->on('product')->onDelete('cascade');

                $table->index('product_redemption_id');
                $table->index('partner_id');
                $table->index('product_id');
            });

            Schema::create('product_redemption_segments', function(Blueprint $table)
            {
                $table->increments('id');
                $table->integer('product_redemption_id')->unsigned()->references('id')->on('product_redemption')->onDelete('cascade');
                $table->integer('segment_id')->unsigned()->references('id')->on('segment')->onDelete('cascade');
                $table->integer('product_id')->unsigned()->references('id')->on('product')->onDelete('cascade');

                $table->index('product_redemption_id');
                $table->index('segment_id');            
                $table->index('product_id');            
            });
            
            Schema::create('product_redemption_countries', function(Blueprint $table)
            {
                $table->increments('id');
                $table->integer('product_redemption_id')->unsigned()->references('id')->on('product_redemption')->onDelete('cascade');
                $table->integer('product_id')->unsigned()->references('id')->on('product')->onDelete('cascade');
                $table->integer('partner_id')->unsigned()->references('id')->on('partner')->onDelete('cascade');
                $table->integer('country_id')->unsigned()->references('id')->on('country')->onDelete('cascade');
                $table->decimal('custom_tarrif_taxes', 8, 2)->nullable()->default(0);
                $table->enum('delivery_options', Meta::deliveryOptions());
                $table->enum('status', Meta::productStatuses());
                $table->decimal('delivery_charges', 8, 2)->nullable()->default(0);
                $table->integer('currency_id')->nullable();
                $table->text('pickup_address')->nullable();

                $table->index('product_id');
                $table->index('product_redemption_id');            
                $table->index('country_id');            
                $table->index('partner_id');            
            });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::dropIfExists('product_reward');
        Schema::dropIfExists('product_redemption');
        Schema::dropIfExists('product_redemption_display_channel');
        Schema::dropIfExists('product_redemption_segments');
        Schema::dropIfExists('product_redemption_countries');
	}

}