<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCouponCouponTypeFields extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
            Schema::table('coupon', function($table)
            {
                $table->enum('coupon_type', array('value', 'percentage'))->default('value');
            });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('coupon', function($table)
		{
		    $table->dropColumn('coupon_type');

		});
	}

}