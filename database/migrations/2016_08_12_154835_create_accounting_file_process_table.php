<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountingFileProcessTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		

		Schema::create('accounting_file', function(Blueprint $table)
		{
			$table->increments('id');
                        $table->string('filename', 255)->nullable();
                        $table->timestamp('last_posted_at');
                        $table->timestamps();
		});		
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		
		Schema::dropIfExists('accounting_file');
	}

}
