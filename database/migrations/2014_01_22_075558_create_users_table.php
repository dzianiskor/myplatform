<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('email');
			$table->string('username', 128);
			$table->string('password', 60);
			$table->string('passcode', 4);
			$table->boolean('verified')->default(false);
			$table->boolean('confirm_passcode')->default(false);
			$table->string('title')->nullable();
			$table->string('first_name', 60);
			$table->string('middle_name', 60);
			$table->string('last_name', 60);
			$table->string('marital_status', 30)->nullable();
			$table->enum('status', array('active', 'inactive', 'suspended'))->default('inactive');
			$table->string('mobile');
			$table->string('normalized_mobile');
			$table->string('telephone_code');			
			$table->date('dob');
			$table->enum('gender', array('m', 'f'));
			$table->string('address_1')->nullable();
			$table->string('address_2')->nullable();
			$table->integer('country_id')->nullable();
			$table->integer('city_id')->nullable();
			$table->integer('area_id')->nullable();
			$table->string('income_bracket', 30)->nullable();
			$table->integer('num_children')->default(0);
			$table->integer('profile_image')->nullable();
			$table->string('ucid', 9);
			$table->string('tag')->nullable();
			$table->boolean('draft')->default(true);
			$table->boolean('ref_account')->default(false);
			$table->integer('occupation_id')->unsigned()->nullable();
			$table->string('api_key', 100);
			
			$table->timestamps();

			$table->index('api_key');
			$table->index('email');                
			$table->index('passcode');                
			$table->index('verified');                
			$table->index('draft');                
			$table->index('ref_account');                
		});

		Schema::create('balance', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->default(0);
			$table->integer('network_id')->default(0);
			$table->integer('balance')->default(0);
			$table->timestamps();

			$table->index('user_id');                
			$table->index('network_id');                
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
		Schema::drop('balance');
	}

}