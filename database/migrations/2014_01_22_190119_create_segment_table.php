<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSegmentTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('segment', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 150);
			$table->integer('partner_id');
			$table->dateTime('last_run');
			$table->boolean('draft')->default(true);
            $table->timestamps();

            $table->index('partner_id');          
		});

        Schema::create('segment_user', function(Blueprint $table)
        {
            $table->integer('segment_id');
            $table->integer('user_id');

            $table->index('segment_id');          
            $table->index('user_id');          
        });		
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('segment_user');
		Schema::dropIfExists('segment');
	}

}