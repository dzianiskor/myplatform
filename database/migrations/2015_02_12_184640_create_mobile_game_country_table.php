<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMobileGameCountryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
            Schema::create('country_mobile_game', function(Blueprint $table)
            {
                $table->increments('id');
                $table->integer('mobile_game_id')->unsigned()->references('id')->on('mobile_game_preferences');
                $table->integer('country_id')->unsigned()->references('id')->on('country');
                
            });
            Schema::create('mobile_game_partner', function(Blueprint $table)
            {
                $table->increments('id');
                $table->integer('mobile_game_id')->unsigned()->references('id')->on('mobile_game_preferences');
                $table->integer('partner_id')->unsigned()->references('id')->on('partner');
                
            });
            
            
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('country_mobile_game');
                Schema::dropIfExists('mobile_game_partner');
                
	}

}