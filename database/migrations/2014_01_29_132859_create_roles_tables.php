<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolesTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('role', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name');
            $table->integer('partner_id');

            $table->index('partner_id');          
        });

        Schema::create('permission', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('group', 50);
            $table->string('slug', 50);
            $table->string('name');
        });

        Schema::create('permission_role', function(Blueprint $table)
        {
            $table->integer('role_id');
            $table->integer('permission_id');

            $table->index('role_id');          
            $table->index('permission_id');          
        });

        Schema::create('role_user', function(Blueprint $table)
        {
            $table->integer('role_id');
            $table->integer('user_id');

            $table->index('role_id');          
            $table->index('user_id');                      
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('role');
		Schema::dropIfExists('permission');
		Schema::dropIfExists('permission_role');
	}

}