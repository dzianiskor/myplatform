<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMakerCheckerFields extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// admins table
		Schema::table('admins', function($table)
		{
			$table->integer('parent_id')->nullable();
		});

		// loyalty_rule table
		Schema::table('loyalty', function($table)
		{
			$table->boolean('approved')->default(0);
			$table->integer('approved_by')->nullable();
		});

		// loyalty_rule table
		Schema::table('loyalty_rule', function($table)
		{
			$table->boolean('approved')->default(0);
			$table->integer('approved_by')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('admins', function($table)
		{
		    $table->dropColumn('parent_id');
		});

		Schema::table('loyalty', function($table)
		{
		    $table->dropColumn('approved');
            $table->dropColumn('approved_by');
		});

		Schema::table('loyalty_rule', function($table)
		{
		    $table->dropColumn('approved');
			$table->dropColumn('approved_by');
		});
	}

}