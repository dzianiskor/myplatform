<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfferLinks extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('country_offer', function(Blueprint $table)
        {
            $table->integer('country_id')->unsigned()->references('id')->on('country')->onDelete('cascade');
            $table->integer('offer_id')->unsigned()->references('id')->on('offer')->onDelete('cascade');

            $table->index('country_id');
            $table->index('offer_id');
        });

        Schema::create('tier_offer', function(Blueprint $table)
        {
            $table->integer('tier_id')->unsigned()->references('id')->on('tier')->onDelete('cascade');
            $table->integer('offer_id')->unsigned()->references('id')->on('offer')->onDelete('cascade');

            $table->index('tier_id');
            $table->index('offer_id');            
        });

        Schema::create('partner_offer', function(Blueprint $table)
        {
            $table->integer('partner_id')->unsigned()->references('id')->on('partner')->onDelete('cascade');
            $table->integer('offer_id')->unsigned()->references('id')->on('offer')->onDelete('cascade');

            $table->index('partner_id');
            $table->index('offer_id');            
        });

        Schema::create('offer_segment', function(Blueprint $table)
        {
            $table->integer('segment_id')->unsigned()->references('id')->on('segment')->onDelete('cascade');
            $table->integer('offer_id')->unsigned()->references('id')->on('offer')->onDelete('cascade');

            $table->index('segment_id');
            $table->index('offer_id');            
        });

        Schema::create('channel_offer', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('offer_id');
            $table->integer('channel_id');

            $table->index('channel_id');
            $table->index('offer_id');            
        });        
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::dropIfExists('country_offer');
        Schema::dropIfExists('tier_offer');
        Schema::dropIfExists('partner_offer');
        Schema::dropIfExists('offer_segment');
        Schema::dropIfExists('channel_offer');
	}

}
