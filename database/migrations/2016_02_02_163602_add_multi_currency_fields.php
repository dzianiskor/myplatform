<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMultiCurrencyFields extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
                // product table
		Schema::table('product', function($table)
		{
			$table->integer('currency_id')->nullable();
		    $table->decimal('original_price', 8, 2)->nullable();
			$table->decimal('original_retail_price', 8, 2)->nullable();
			$table->decimal('original_sales_tax', 8, 2)->nullable()->default(0);
		});

		// loyalty table
		Schema::table('loyalty', function($table)
		{
			$table->integer('currency_id')->nullable();
			$table->decimal('original_reward')->nullable();
			$table->decimal('original_redemption')->nullable();
		});

		// loyalty_rule table
		Schema::table('loyalty_rule', function($table)
		{
			$table->integer('currency_id')->nullable();
			$table->decimal('original_reward')->nullable();
		});

		// transaction table
		Schema::table('transaction', function($table)
		{
			$table->decimal('original_total_amount', 10,2)->nullable()->default(0);
		});

		// transaction_item table
		Schema::table('transaction_item', function($table)
		{
			$table->decimal('original_total_amount', 10,2)->nullable()->default(0);
			$table->integer('currency_id')->nullable();
		});

		// network table
		Schema::table('network', function($table)
		{
		    $table->integer('currency_id')->nullable();
                    $table->decimal('value_in_currency', 10, 3)->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('product', function($table)
		{
		    $table->dropColumn('currency_id');
			$table->dropColumn('original_price');
			$table->dropColumn('original_retail_price');
			$table->dropColumn('original_sales_tax');

		});

		Schema::table('loyalty', function($table)
		{
		    $table->dropColumn('currency_id');
                    $table->dropColumn('original_reward');
                    $table->dropColumn('original_redemption');
		});

		Schema::table('loyalty_rule', function($table)
		{
		    $table->dropColumn('currency_id');
			$table->dropColumn('original_reward');
		});

		Schema::table('transaction', function($table)
		{
		    $table->dropColumn('original_total_amount');
		});

		Schema::table('transaction_item', function($table)
		{
		    $table->dropColumn('original_total_amount');
			$table->dropColumn('currency_id');
		});

		Schema::table('network', function($table)
		{
		    $table->dropColumn('currency_id');
			$table->dropColumn('value_in_currency');
		});
	}

}