<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOfferValueFields extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up(){
            Schema::table('offer', function($table){
                $table->integer('value');
            });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down(){
            Schema::table('offer', function($table){
                $table->dropColumn('value');
            });
	}
}
