<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('transaction', function(Blueprint $table)
        {
            $table->increments('id');

            $table->integer('partner_id')->nullable();
            $table->integer('store_id')->nullable();
            $table->integer('pos_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->text('ref_number')->nullable();
            $table->string('invoice_number', 128)->nullable();
            $table->decimal('total_amount', 10,2)->nullable()->default(0);
            $table->decimal('points_rewarded', 10,2)->nullable()->default(0);
            $table->decimal('points_redeemed', 10,2)->nullable()->default(0);
            $table->decimal('points_balance', 20,2)->nullable()->default(0);
            $table->integer('delivery_cost')->default(0);
            $table->integer('trx_reward')->nullable()->default(0);
            $table->integer('trx_redeem')->nullable()->default(0);
            $table->decimal('amt_reward', 10, 2)->nullable()->default(0);
            $table->decimal('amt_redeem', 10, 2)->nullable()->default(0);
            $table->integer('rule_id')->nullable();
            $table->integer('currency_id')->nullable();
            $table->decimal('exchange_rate', 10, 4)->nullable();
            $table->boolean('voided')->default(false);
            $table->integer('product_id')->nullable();
            $table->string('product_model')->nullable();
            $table->integer('product_brand_id')->nullable();
            $table->string('product_sub_model')->nullable();
            $table->integer('product_category_id')->nullable();
            $table->integer('country_id')->nullable();
            $table->integer('city_id')->nullable();
            $table->integer('area_id')->nullable();
            $table->integer('segment_id')->nullable();
            $table->integer('category_id')->nullable();
            $table->integer('quantity')->default(0);
            $table->integer('auth_role_id')->nullable()->default(null);
            $table->integer('auth_staff_id')->nullable()->default(null);
            $table->integer('network_id')->default(1);
            $table->text('notes')->nullable();
            $table->text('source')->nullable();
            $table->text('meta_data')->nullable();
            $table->enum('trans_class', Meta::transactionClasses())->default('Items');
            $table->integer('coupon_id')->unsigned()->nullable();

            $table->timestamps();

            $table->index('partner_id');          
            $table->index('store_id');          
            $table->index('pos_id');          
            $table->index('user_id');          
            $table->index('trx_reward');          
            $table->index('trx_redeem');          
            $table->index('rule_id');          
            $table->index('voided');          
            $table->index('country_id');          
            $table->index('city_id');          
            $table->index('area_id');
        });

        Schema::create('transaction_item', function(Blueprint $table)
        {
            $table->increments('id');

            $table->decimal('total_amount', 10,2)->nullable()->default(0);
            $table->integer('transaction_id')->nullable();
            $table->integer('product_id')->nullable();
            $table->string('product_model')->nullable();
            $table->integer('product_brand_id')->nullable();
            $table->string('product_sub_model')->nullable();
            $table->integer('product_category_id')->nullable();
            $table->string('delivery_option')->nullable();
            $table->integer('quantity')->default(1);
            
            $table->timestamps();

            $table->index('transaction_id');          
            $table->index('product_id');          
            $table->index('product_model');          
            $table->index('product_brand_id');          
            $table->index('product_category_id');          
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::dropIfExists('transaction_item');
		Schema::dropIfExists('transaction');
	}

}