<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCurrencyTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('currency', function(Blueprint $table)
		{
            $table->increments('id');
            $table->string('short_code', 3)->unique();
            $table->string('name', 100);
            $table->boolean('draft')->default(true);
            $table->timestamps();
		});

        Schema::create('currency_pricing', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('currency_id')->unsigned();
            $table->date('date');
            $table->decimal('rate', 10, 4);
            $table->timestamps();

            $table->index('currency_id');                
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('currency_pricing');
		Schema::dropIfExists('currency');
	}

}