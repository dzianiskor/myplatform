<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPartnerCanRewardInactiveFields extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('partner', function($table)
		{
		    $table->boolean('can_reward_inactive')->default(false);
                    
                    
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('partner', function($table)
		{
		    $table->dropColumn('can_reward_inactive');
                    
		    
		});
	}

}
