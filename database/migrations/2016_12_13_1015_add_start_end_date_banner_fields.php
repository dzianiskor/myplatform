<?php
/**migration
 * Start End Date banner 
 *
 * Important:
 *
 * Added new fields to the banner table to create a
 * to save the start and end dates for the validity
 *
 * @category   Migrations
 * @package    BLU
 * @author     Magid <magid.mroueh@bluloyalty.com>
 * @copyright  2016 BLU
 * @link       http://blupoints.com
 */

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStartEndDateBannerFields extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('banner', function($table)
		{
		    $table->date('start_date')->nullable();
                    $table->date('end_date')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('banner', function($table)
		{
		    $table->dropColumn('start_date');
		    $table->dropColumn('end_date');
		});
	}

}
