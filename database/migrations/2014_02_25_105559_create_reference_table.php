<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReferenceTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('reference', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('number');
			$table->integer('user_id');
			$table->integer('pin');
			$table->integer('batch_id')->unsigned()->nullable();
			$table->timestamps();
			$table->unique('number');

			$table->index('number');			
			$table->index('user_id');			
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('reference');
	}

}
