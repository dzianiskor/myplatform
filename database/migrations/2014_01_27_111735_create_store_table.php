<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('store', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name');
            $table->decimal('lng', 18, 14)->nullable();
            $table->decimal('lat', 18, 14)->nullable();
            $table->integer('mapping_id')->nullable();
            $table->integer('partner_id')->unsigned()->references('id')->on('partner');
            $table->boolean('draft')->default(true);
			$table->timestamps();

			$table->index('mapping_id');            
			$table->index('partner_id');            
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('store');
	}

}