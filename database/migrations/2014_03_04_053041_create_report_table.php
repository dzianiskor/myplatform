<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('report_tier', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
		});	

		Schema::create('report', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('description');
			$table->boolean('has_chart')->default(false);
			$table->integer('report_tier_id')->unsigned()->nullable();			
			$table->foreign('report_tier_id')->references('id')->on('report')->onDelete('cascade');			
			$table->boolean('enabled')->default(true);
		});	
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Scheme::dropIfExists('report');
		Scheme::dropIfExists('report_tier');
	}

}
