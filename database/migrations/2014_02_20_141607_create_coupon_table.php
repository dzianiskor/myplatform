<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('batch', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name', 100);
            $table->date('valid_from');
            $table->date('valid_to');
            $table->decimal('price');
            $table->integer('partner_id')->unsigned()->nullable();
            $table->integer('country_id')->unsigned()->nullable();
            $table->integer('segment_id')->unsigned()->nullable();
            $table->integer('number_of_items');
            $table->enum('type', Meta::batchTypes());
            $table->text('terms_and_conditions');
            $table->boolean('draft')->default(true);
            $table->string('opt_in_sms', 160);
            $table->string('redeem_sms', 160);
            $table->timestamps();

            $table->index('partner_id');                        
            $table->index('draft');  
            $table->index('valid_from');          
            $table->index('valid_to');          
        });

        Schema::create('coupon', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('coupon_code', 20);
            $table->integer('batch_id')->unsigned()->nullable();
            $table->integer('partner_id')->unsigned()->nullable();
            $table->foreign('batch_id')->references('id')->on('batch')->onDelete('cascade');
            $table->integer('transaction_id')->unsigned()->nullable();
            $table->timestamps();

            $table->index('batch_id');                                    
            $table->index('partner_id');                                    
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::dropIfExists('coupon');
        Schema::dropIfExists('batch');
	}

}
