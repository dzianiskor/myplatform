<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExpiryPointsFields extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tier_criteria', function($table)
		{
		    $table->integer('expiry_in_months')->default(12);
		});
                Schema::table('transaction', function($table)
		{
		    $table->boolean('expired')->default(false);
                    $table->timestamp('expired_at')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tier_criteria', function($table)
		{
		    $table->dropColumn('expiry_in_months');

		});
                Schema::table('transaction', function($table)
		{
		    $table->dropColumn('expired');
                    $table->dropColumn('expired_at');
		});
	}

}