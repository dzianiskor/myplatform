<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationTierTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('notification_tier', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('tier_id')->unsigned()->nullable();
            $table->integer('notification_id')->unsigned()->nullable();
            $table->timestamps();

            $table->index('tier_id');
            $table->index('notification_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Scheme::dropIfExists('notification_tier');
    }

}
