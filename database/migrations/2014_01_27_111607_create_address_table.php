<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('address', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('country_id')->unsigned()->nullable()->references('id')->on('country');
			$table->integer('area_id')->unsigned()->nullable()->references('id')->on('area');
			$table->integer('city_id')->unsigned()->nullable()->references('id')->on('city');
			$table->string('street')->nullable();
			$table->string('floor')->nullable();
			$table->integer('mapping_key');
			$table->string('building')->nullable();
			$table->text('details')->nullable();
			$table->timestamps();

			$table->index('mapping_key');            
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('address');
	}

}