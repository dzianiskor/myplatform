<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartnerTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('partner', function(Blueprint $table)
        {
            $table->increments('id');

            // Details
            $table->integer('parent_id')->nullable()->unsigned()->references('id')->on('partner');
            $table->integer('category_id')->nullable()->unsigned()->references('id')->on('category');
            $table->integer('image')->nullable()->default(null);
            $table->string('ucid', 9);
            $table->enum('status', Meta::partnerStatuses())->default('Enabled');
            $table->string('name', 100)->nullable()->default('New Partner');
            $table->text('description')->nullable();
            $table->string('smsgate_username', 100)->nullable();
            $table->string('smsgate_password', 100)->nullable();
            $table->string('smsgate', 100)->nullable();
            $table->string('smsgate_number', 100)->nullable();
            $table->string('contact_name', 100)->nullable();
            $table->string('contact_email', 100)->nullable();
            $table->string('contact_number', 100)->nullable();
            $table->string('contact_website', 100)->nullable();
            $table->string('general_manager', 100)->nullable();
            $table->string('marketing_manager', 100)->nullable();
            $table->string('address_1')->nullable();
            $table->string('address_2')->nullable();
            $table->integer('country_id')->nullable();
            $table->integer('city_id')->nullable();
            $table->integer('area_id')->nullable();

            // Entity Settings
            $table->integer('threshold_12_months')->default(250);
            $table->integer('threshold_24_months')->default(500);
            $table->integer('device_settings_id')->default(0);
            $table->boolean('reward')->default(false);
            $table->integer('reward_max')->default(10000);
            $table->integer('redeem_unit')->default(1);
            $table->boolean('mobile')->default(true);
            $table->boolean('display_online')->default(true);
            $table->boolean('card')->default(true);
            $table->boolean('can_create_users')->default(true);
            $table->boolean('request_pin')->default(true);
            $table->boolean('redeem')->default(true);
            $table->integer('redeem_max')->default(100000);
            $table->boolean('void_operations')->default(true);
            $table->boolean('barcode')->default(true);
            $table->boolean('coupon')->default(false);
            $table->boolean('draft')->default(true);

            $table->timestamps();

            $table->index('parent_id');                
            $table->index('status');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::dropIfExists('partner');
	}

}