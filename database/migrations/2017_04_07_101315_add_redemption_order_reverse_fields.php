<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRedemptionOrderReverseFields extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
            Schema::table('redemption_order', function($table)
            {
                $table->boolean('reversed')->default(FALSE);
                $table->boolean('reversed_item')->default(FALSE);
                $table->boolean('reversed_delivery')->default(FALSE);
                $table->date('reversed_at');
            });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('redemption_order', function($table)
		{
		    $table->dropColumn('reversed');
		    $table->dropColumn('reversed_item');
		    $table->dropColumn('reversed_delivery');
		    $table->dropColumn('reversed_at');
		});
	}

}