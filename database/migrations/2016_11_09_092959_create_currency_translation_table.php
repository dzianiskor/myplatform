<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCurrencyTranslationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
            
            Schema::create('currency_translation', function(Blueprint $table)
            {
                $table->increments('id');
                $table->integer('currency_id')->unsigned()->references('id')->on('currency');
                $table->integer('lang_id')->unsigned()->references('id')->on('localization_language');
                $table->string('name');
                $table->boolean('draft')->default(true);
                $table->timestamps();
                
                $table->index('currency_id'); 
                $table->index('lang_id'); 
                
            });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
                Schema::dropIfExists('currency_translation');
                
	}

}