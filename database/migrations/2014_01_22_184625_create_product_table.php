<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('brand', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name', 100);
            $table->boolean('draft')->default(true);
            $table->timestamps();
        });

        Schema::create('category', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name', 100);
            $table->boolean('draft')->default(true);
            $table->integer('parent_category_id')->unsigned()->nullable();
            $table->timestamps();
        });

        Schema::create('product', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('ucid', 9);
            $table->string('name');
            $table->integer('brand_id')->unsigned()->nullable();
            $table->string('model', 100)->nullable();
            $table->string('sub_model', 100)->nullable();
            $table->integer('cover_image')->unsigned()->nullable();
            $table->text('description')->nullable();
            $table->boolean('display_online')->default(true);
            $table->boolean('hot_deal')->default(false);
            $table->integer('category_id')->unsigned()->nullable();
            $table->integer('qty')->default(0);
            $table->integer('price_in_points')->nullable();
            $table->decimal('price_in_usd', 8, 2)->nullable();
            $table->decimal('retail_price_in_usd', 8, 2)->nullable();
            $table->decimal('sales_tax_in_usd', 8, 2)->nullable()->default(0);
            $table->decimal('weight', 8, 2)->nullable()->default(0);
            $table->decimal('volume', 8, 2)->nullable()->default(0);
            $table->enum('delivery_options', Meta::deliveryOptions());
            $table->enum('status', Meta::productStatuses());
            $table->decimal('delivery_charges', 8, 2)->nullable()->default(0);
            $table->text('pickup_address')->nullable();
            $table->boolean('draft')->default(true);
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->timestamps();

            $table->index('brand_id');
            $table->index('hot_deal');
            $table->index('status');
            $table->index('draft');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::dropIfExists('category');
        Schema::dropIfExists('brand');
        Schema::dropIfExists('product');
	}

}