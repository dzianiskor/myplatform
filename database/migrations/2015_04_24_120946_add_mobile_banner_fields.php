<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMobileBannerFields extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('banner', function($table)
		{
		    $table->enum('mob_status', Meta::bannerStatuses())->default('Offline');
                    $table->integer('mob_media_id')->nullable();
                    $table->string('mob_link', 255)->nullable();
                    
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('banner', function($table)
		{
		    $table->dropColumn('mob_status');
                    $table->dropColumn('mob_media_id');
                    $table->dropColumn('mob_link');
		    
		});
	}

}
