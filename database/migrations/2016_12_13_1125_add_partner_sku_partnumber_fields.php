<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPartnerSkuPartnumberFields extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('partner_product', function($table)
		{
		    $table->string('sku')->nullable();
                    $table->string('partnumber')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('partner_product', function($table)
		{
		    $table->dropColumn('sku');
		    $table->dropColumn('partnumber');
		});
	}

}
