<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBrandPartnerTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
            

            Schema::create('brand_partner', function(Blueprint $table){
                $table->integer('partner_id')->unsigned();
                $table->integer('brand_id')->unsigned();
                $table->string('partner_map_id',100);
                $table->index('partner_id');    
                $table->index('brand_id');    
            });
            
            

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{   
            Schema::dropIfExists('brand_partner');
            
	}

}