<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfferTranslationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
            
            Schema::create('offer_translation', function(Blueprint $table)
            {
                $table->increments('id');
                $table->integer('offer_id')->unsigned()->references('id')->on('offer');
                $table->integer('lang_id')->unsigned()->references('id')->on('localization_language');
                $table->string('name');
                $table->text('short_description')->nullable();
                $table->text('description')->nullable();
                $table->text('terms_and_condition')->nullable();
                $table->boolean('draft')->default(true);
                $table->timestamps();
                
                $table->index('offer_id'); 
                $table->index('lang_id'); 
                
            });
            
            
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
            Schema::dropIfExists('offer_translation');
                
	}

}