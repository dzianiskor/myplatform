<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartnerUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('partner_user', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('partner_id');
            $table->integer('user_id');
            $table->string('type', 100); // static | calculated

            $table->index('partner_id');          
            $table->index('user_id');          
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('partner_user');
	}

}