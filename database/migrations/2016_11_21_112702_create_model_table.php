<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModelTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
            Schema::create('prodmodel', function(Blueprint $table){
                $table->increments('id');
                $table->string('name', 120);
                $table->boolean('draft')->default(true);

                $table->timestamps();
                $table->index('draft');    
            });

            Schema::create('prodmodel_partner', function(Blueprint $table){
                $table->integer('partner_id')->unsigned();
                $table->integer('prodmodel_id')->unsigned();
                $table->string('partner_map_id',100);
                $table->index('partner_id');    
                $table->index('prodmodel_id');    
            });
            
            Schema::create('prodmodel_translation', function(Blueprint $table)
            {
                $table->increments('id');
                $table->integer('prodmodel_id')->unsigned()->references('id')->on('prodmodel');
                $table->integer('lang_id')->unsigned()->references('id')->on('localization_language');
                $table->string('name');
                $table->boolean('draft')->default(true);
                $table->timestamps();
                
                $table->index('prodmodel_id'); 
                $table->index('lang_id'); 
                
            });
            
            Schema::create('prodsubmodel', function(Blueprint $table){
                $table->increments('id');
                $table->string('name', 120);
                $table->boolean('draft')->default(true);

                $table->timestamps();
                $table->index('draft');    
            });

            Schema::create('prodsubmodel_partner', function(Blueprint $table){
                $table->integer('partner_id')->unsigned();
                $table->integer('prodsubmodel_id')->unsigned();
                $table->string('partner_map_id',100);
                $table->index('partner_id');
                $table->index('prodsubmodel_id');
            });
            
            Schema::create('prodsubmodel_translation', function(Blueprint $table)
            {
                $table->increments('id');
                $table->integer('prodsubmodel_id')->unsigned()->references('id')->on('prodsubmodel');
                $table->integer('lang_id')->unsigned()->references('id')->on('localization_language');
                $table->string('name');
                $table->boolean('draft')->default(true);
                $table->timestamps();
                
                $table->index('prodsubmodel_id'); 
                $table->index('lang_id'); 
                
            });

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{   
            Schema::dropIfExists('prodmodel_translation');
            Schema::dropIfExists('prodsubmodel_translation');
            Schema::dropIfExists('prodmodel_partner');
            Schema::dropIfExists('prodmodel');
            Schema::dropIfExists('prodsubmodel_partner');
            Schema::dropIfExists('prodsubmodel');
            
	}

}