<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReverseTransactionItemFields extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('transaction_item', function($table)
		{
		    $table->boolean('reversed')->default(false);
		    $table->boolean('reversed_delivery')->default(false);
                    $table->date('reversed_at');
                    
                    
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('transaction_item', function($table)
		{
		    $table->dropColumn('reversed');
		    $table->dropColumn('reversed_delivery');
		    $table->dropColumn('reversed_at');
                    
		    
		});
	}

}
