<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBannerLoggedInOutFields extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('banner', function($table)
		{
		   $table->boolean('logged_in');
		   $table->boolean('logged_out');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('banner', function($table)
		{
		    $table->dropColumn('logged_in');
		    $table->dropColumn('logged_out');

		});
	}

}