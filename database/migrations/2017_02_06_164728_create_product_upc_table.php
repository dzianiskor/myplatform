<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductUpcTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

		Schema::create('product_upc', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('product_id')->unsigned();			
			$table->string('upc');			

                        $table->unique('upc');
			$table->index('product_id');
			$table->index('upc');
		});			
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Scheme::dropIfExists('product_upc');
	}

}
