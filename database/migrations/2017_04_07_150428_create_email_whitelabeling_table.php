<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailWhitelabelingTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('email_whitelabeling', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('partner_id');
            $table->string('lang')->nullable();
            $table->enum('field', array('program_name', 'partner_name_subject', 'partner_name_content', 'copyright', 'support_email', 'website_url', 'image', 'image_optional'))->default('program_name');
            $table->text('value')->nullable();
            $table->timestamps(); 

            $table->index('id');
            $table->index('lang');
            $table->index('field');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Scheme::dropIfExists('email_whitelabeling');
    }

}