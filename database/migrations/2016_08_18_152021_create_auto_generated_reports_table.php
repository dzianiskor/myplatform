<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAutoGeneratedReportsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		

		Schema::create('auto_generated_reports', function(Blueprint $table)
		{
			$table->increments('id');
                        $table->string('name', 255);
                        $table->string('slug', 255);
                        $table->boolean('status');
		});		
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		
		Schema::dropIfExists('auto_generated_reports');
	}

}
