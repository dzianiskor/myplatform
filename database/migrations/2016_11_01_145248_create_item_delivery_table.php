<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemDeliveryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('item_delivery', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('trx_id');
            $table->integer('trx_item_id');
            $table->integer('delivery_cost');
            $table->integer('network_id')->default(1);
            $table->integer('amt_delivery');
            $table->integer('currency_id')->default(6);
            $table->boolean('reversed')->default(false);
            $table->date('reversed_at');
            $table->timestamps();

            $table->index('trx_id');
            $table->index('trx_item_id');
        });

        
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        
        Schema::dropIfExists('item_delivery');
	}

}
