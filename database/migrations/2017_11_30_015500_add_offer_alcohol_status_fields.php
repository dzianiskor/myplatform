<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOfferAlcoholStatusFields extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up(){
            Schema::table('offer', function($table){
                $table->boolean('alcohol_status');
            });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down(){
            Schema::table('offer', function($table){
                $table->dropColumn('alcohol_status');
            });
	}
}
