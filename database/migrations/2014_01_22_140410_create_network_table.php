<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNetworkTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('network', function(Blueprint $table)
		{
            $table->increments('id');
            $table->enum('status', array('Active', 'Inactive', 'Suspended'))->default('Active');
            $table->string('name', 100);
            $table->integer('image')->nullable()->default(null);
            $table->string('description', 100);
            $table->string('currency', 100);
            $table->boolean('do_points_conversion');
            $table->integer('parent_network_id')->unsigned()->nullable();
            $table->float('network_point');
            $table->float('blu_point');
            $table->string('affiliate_name', 100)->nullable();
            $table->string('affiliate_telephone', 100)->nullable();
            $table->string('affiliate_address_1', 100)->nullable();
            $table->string('affiliate_address_2', 100)->nullable();
            $table->integer('affiliate_country')->nullable();
            $table->integer('affiliate_city')->nullable();
            $table->integer('affiliate_area')->nullable();
            $table->string('affiliate_main_telephone', 100)->nullable();
            $table->string('affiliate_email', 100)->nullable();
            $table->string('affiliate_contact_name', 100)->nullable();
            $table->string('affiliate_contact_telephone', 100)->nullable();
            $table->string('affiliate_contact_ext', 100)->nullable();
            $table->string('affiliate_contact_mobile', 100)->nullable();
            $table->string('affiliate_contact_email', 100)->nullable();     
            $table->boolean('draft')->default(true);

            $table->timestamps();

            $table->index('status');    
            $table->index('draft');    
        });

        Schema::create('network_partner', function(Blueprint $table)
        {
            $table->integer('partner_id')->unsigned();
            $table->integer('network_id')->unsigned();

            $table->index('partner_id');    
            $table->index('network_id');    
        });

        Schema::create('network_user', function(Blueprint $table)
        {
            $table->integer('user_id')->unsigned();
            $table->integer('network_id')->unsigned();

            $table->index('user_id');    
            $table->index('network_id');                
        });        
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::dropIfExists('partner_network');
        Schema::dropIfExists('network');
	}

}