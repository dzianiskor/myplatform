<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoryBrandTranslationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
            
            Schema::create('category_translation', function(Blueprint $table)
            {
                $table->increments('id');
                $table->integer('category_id')->unsigned()->references('id')->on('category');
                $table->integer('lang_id')->unsigned()->references('id')->on('localization_language');
                $table->string('name');
                $table->boolean('draft')->default(true);
                $table->timestamps();
                
                $table->index('category_id'); 
                $table->index('lang_id'); 
                
            });
            Schema::create('brand_translation', function(Blueprint $table)
            {
                $table->increments('id');
                $table->integer('brand_id')->unsigned()->references('id')->on('brand');
                $table->integer('lang_id')->unsigned()->references('id')->on('localization_language');
                $table->string('name');
                $table->boolean('draft')->default(true);
                $table->timestamps();
                
                $table->index('brand_id'); 
                $table->index('lang_id'); 
                
            });
            
            
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
                Schema::dropIfExists('category_translation');
                Schema::dropIfExists('brand_translation');
                
	}

}