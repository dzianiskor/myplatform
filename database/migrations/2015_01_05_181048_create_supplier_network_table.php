<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupplierNetworkTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        
            Schema::create('supplier_network', function(Blueprint $table)
            {
                    $table->increments('id');
                    $table->integer('supplier_id')->unsigned()->references('id')->on('supplier');
                    $table->integer('network_id')->unsigned()->references('id')->on('network');

                    $table->index('supplier_id');
                    $table->index('network_id');

            });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('supplier_network');
                
	}

}
