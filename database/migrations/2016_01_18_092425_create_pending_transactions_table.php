<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePendingTransactionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pending_transactions', function(Blueprint $table)
		{
			$table->increments('id');
			$table->text('serialized_data');
			$table->enum('type', array('cart', 'travel'))->default('cart');
			$table->enum('status', array('sent to bank', 'processing at BLU', 'failed at bank', 'failed at BLU', 'completed'))->default('sent to bank');
			$table->text('return_url')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pending_transactions');
	}

}