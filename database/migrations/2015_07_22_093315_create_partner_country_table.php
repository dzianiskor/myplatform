<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartnerCountryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('partner_country', function(Blueprint $table)
        {
			$table->increments('id');
            $table->integer('country_id')->unsigned()->references('id')->on('country')->onDelete('cascade');
            $table->integer('partner_id')->unsigned()->references('id')->on('partner')->onDelete('cascade');

            $table->index('country_id');
            $table->index('partner_id');
        });


	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::dropIfExists('partner_country');
	}

}
