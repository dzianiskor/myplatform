<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttributeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('attribute', function(Blueprint $table)
        {
        	$table->increments('id');
            $table->integer('mapping_key');
            $table->string('name', 150);
            $table->text('json_data');

            $table->index('mapping_key');          
        });				
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('attribute');
	}

}
