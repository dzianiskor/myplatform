<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAffiliateProgramIdAffiliateProgramMembershipsFields extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('affiliate_programs_memberships', function($table)
		{
		    $table->integer('program_id')->default(0);
		});		
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('affiliate_programs_memberships', function($table)
		{
		    $table->dropColumn('program_id');
		});		
			
	}

}
