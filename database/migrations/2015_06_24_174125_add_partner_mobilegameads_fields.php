<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPartnerMobilegameadsFields extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
	    if (!Schema::hasColumn('mobile_game_ads', 'partner_id')) {
            Schema::table('mobile_game_ads', function ($table) {
                $table->integer('partner_id')->unsigned()->references('id')->on('partner');
                $table->index('partner_id');
            });
        }
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mobile_game_ads', function($table)
		{
		    $table->dropColumn('partner_id');
		    
		});
	}

}
