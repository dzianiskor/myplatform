<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('order', function(Blueprint $table)
        {
	        $table->increments('id');
	        $table->integer('user_id')->unsigned();
	        $table->integer('transaction_id')->unsigned();
	        $table->timestamps();

	        $table->index('user_id');
	        $table->index('transaction_id');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('order');
	}

}
