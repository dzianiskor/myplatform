<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCardReferencePartnerField extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('card', function($table)
		{
                    $table->integer('partner_id')->unsigned()->nullable();
		});
                Schema::table('reference', function($table)
		{
                    $table->integer('partner_id')->unsigned()->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('card', function($table)
		{
		    $table->dropColumn('partner_id');
		});
		Schema::table('reference', function($table)
		{
		    $table->dropColumn('partner_id');
		});                
	}

}