<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSupplierPartnerFields extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
            Schema::table('supplier', function($table)
            {
                $table->integer('partner_id')->unsigned();
                $table->index('partner_id');

            });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('supplier', function($table)
		{
		    $table->dropColumn('partner_id');

		});
	}

}