<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('notification', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('partner_id')->unsigned()->nullable();			
			$table->string('name');
			$table->text('description')->nullable();
			$table->enum('type', Meta::notificationTypes());
			$table->text('body')->nullable();
			$table->text('subject')->nullable();
			$table->boolean('draft')->default(true);
			$table->boolean('invoked')->default(false);
			$table->boolean('sent')->default(false);
			$table->integer('created_by')->unsigned();
			$table->integer('count')->unsigned()->default(0);			
			$table->dateTime('start_date')->default(date("Y-m-d 08:00", time()));
			$table->dateTime('end_date')->default(date("Y-m-d 17:00", time()));
			$table->timestamps();

			$table->index('partner_id');
			$table->index('draft');
			$table->index('invoked');
			$table->index('sent');
		});	

		Schema::create('notification_segment', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('segment_id')->unsigned()->nullable();			
			$table->integer('notification_id')->unsigned()->nullable();			
			$table->timestamps();

			$table->index('segment_id');
			$table->index('notification_id');
		});			
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Scheme::dropIfExists('notification_segment');
		Schema::dropIfExists('notification');
	}

}
