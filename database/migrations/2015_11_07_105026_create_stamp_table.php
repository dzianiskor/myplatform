<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStampTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
            Schema::table('partner', function($table)
            {
                $table->boolean('stamp_partner')->default(FALSE);

            });
            
            Schema::create('stamp', function(Blueprint $table)
            {
                $table->increments('id');
                $table->integer('partner_id')->unsigned()->references('id')->on('partner');
                $table->integer('user_id')->unsigned()->references('id')->on('users');
                $table->integer('admin_id')->unsigned()->references('id')->on('admins');
                $table->integer('store_id')->unsigned()->references('id')->on('store');
                $table->boolean('verified')->default(FALSE);
                
                $table->timestamps();
                
                $table->index('partner_id'); 
                $table->index('user_id'); 
                
            });
            Schema::create('stamp_reward', function(Blueprint $table)
            {
                $table->increments('id');
                $table->integer('partner_id')->unsigned()->references('id')->on('partner');
                $table->string('name', 300);
                $table->integer('stamp_number')->default("0");
                $table->integer('image')->nullable();
                $table->boolean('display_online')->default(FALSE);
                
                $table->timestamps();
                $table->index('partner_id');  
                
            });
            
            Schema::create('stamp_transaction', function(Blueprint $table)
            {
                $table->increments('id');
                $table->integer('user_id')->unsigned()->references('id')->on('users');
                $table->integer('partner_id')->unsigned()->references('id')->on('partner');
                $table->integer('store_id')->nullable();
                $table->integer('stamp_reward_id');
                $table->integer('stamp_number')->default("0");
                $table->integer('admin_id');
                $table->timestamps();
                
                $table->index('user_id');
                $table->index('partner_id');          
                $table->index('store_id');          
                $table->index('pos_id'); 
                $table->index('stamp_reward_id');
                
            });
            
            
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
                Schema::dropIfExists('stamp');
                Schema::dropIfExists('stamp_reward');
                Schema::dropIfExists('stamp_transaction');
                
	}

}