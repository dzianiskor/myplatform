<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCashbackLoyaltyDurationField extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('loyalty', function($table)
		{
                    $table->integer('cashback_validity')->unsigned()->nullable();
		});
                
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('loyalty', function($table)
		{
		    $table->dropColumn('cashback_validity');
		});
		                
	}

}