<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRedemptionOrderPaidCashFields extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
            Schema::table('redemption_order', function($table)
            {
                $table->decimal('cash_payment', 10, 2)->default(0);
                $table->integer('cash_payment_currency')->default(6);
                $table->integer('payment_gateway_cost_currency')->default(6);
            });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('redemption_order', function($table)
		{
		    $table->dropColumn('cash_payment');
		    $table->dropColumn('cash_payment_currency');
		    $table->dropColumn('payment_gateway_cost_currency');
		});
	}

}