<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMobilePushNotificationSegmentTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

		Schema::create('mobile_push_notification_segment', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('segment_id')->unsigned()->nullable();			
			$table->integer('notification_id')->unsigned()->nullable();			

			$table->index('segment_id');
			$table->index('notification_id');
		});			
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Scheme::dropIfExists('mobile_push_notification_segment');
	}

}
