<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAirportsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('airports', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('code', 3);
			$table->float('lat')->nullable();
			$table->float('lon')->nullable();
			$table->string('name', 120);
			$table->string('city', 120);
			$table->string('state', 120);
			$table->string('country', 120);
			$table->string('woeid', 120)->nullable();
			$table->string('tz', 120)->nullable();
                        $table->string('phone', 120)->nullable();
			$table->string('type', 120)->nullable();
			$table->string('email', 120)->nullable();
			$table->string('url', 255)->nullable();
                        $table->integer('runway_length')->nullable();
			$table->integer('elev')->nullable();
                        $table->string('icao', 120)->nullable();
			$table->integer('direct_flights')->nullable();
			$table->integer('carriers')->nullable();
			$table->timestamps();               
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('airports');
	}

}