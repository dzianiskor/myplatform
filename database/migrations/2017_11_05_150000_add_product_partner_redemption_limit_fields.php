<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProductPartnerRedemptionLimitFields extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
            Schema::table('product_partner_redemption', function($table)
            {
                $table->integer('redemption_limit');
            });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('product_partner_redemption', function($table)
		{
		    $table->dropColumn('redemption_limit');
		});
	}

}