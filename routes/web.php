<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function () {
   return Redirect::to("auth/login");
});
Route::get('login', ['as' => 'login', 'uses' => 'AuthController@showLogin']);



 // ImportController
 Route::get('import/', 'ImportController@getIndex');
 Route::get('import/reverseimport', 'ImportController@getReverseimport');
 Route::post('import/import', 'ImportController@postImport');

// MonthlyrewardController
Route::get('monthlyemail/', 'MonthlyrewardController@getIndex');
Route::post('monthlyemail/monthlymail', 'MonthlyrewardController@postMonthlymail');

   // RemindersController
Route::get('password/remind', 'RemindersController@getRemind')->name('password.remind');
Route::post('password/remind', 'RemindersController@postRemind')->name('password.remind');
Route::get('password/reset/{token?}', 'RemindersController@getReset')->name('password.reset');

Route::post('password/reset', 'RemindersController@postReset')->name('password.reset');
 Route::group(array('prefix' => 'auth'), function () {
   Route::get("/login",  "AuthController@showLogin");
    Route::post("/login", "AuthController@doLogin");
    Route::get("/logout", "AuthController@doSignout");
});

 Route::group(array('prefix' => 'media'), function () {
   Route::get("/image/{id}",    "MediaController@renderImage");
    Route::get("/download/{id}", "MediaController@downloadFile");
    Route::get("/delete/{id}",   "MediaController@deleteFile");
});

Route::group(array('prefix' => 'uploads'), function () {
   Route::get("/view/{name}",    "MediaController@renderImage");
});

 Route::group(array('prefix' => 'dashboard', 'middleware' => 'auth'), function () {
    Route::get("/", "DashboardController@showIndex");
    Route::get("/network_partners/{id}", "DashboardController@getNetworkPartners");
    Route::group(array('prefix' => 'mobilegame'), function(){
       Route::get("/",                                             "MobileGameController@listMobilegames");
        Route::post("/link_country/{id}",                           "MobileGameController@postLink_country");
        Route::get("/unlink_country/{mobilegame_id}/{country_id}",  "MobileGameController@getUnlink_country");
        Route::post("/link_partner/{id}",                           "MobileGameController@postLink_partner");
        Route::get("/unlink_partner/{mobilegame_id}/{partner_id}",  "MobileGameController@getUnlink_partner");
        Route::get("/edit/{mobilegame_id}",                         "MobileGameController@getEdit");
        Route::get("/delete/{id}",                                  "MobileGameController@getDelete");
        Route::post("/update/{id}",                                 "MobileGameController@postUpdate");
        Route::get("/create",                                       "MobileGameController@getCreate");
    });


    // MobileGameAdsController
    Route::get('mobilegameads/', 'MobileGameAdsController@getIndex');
    Route::get('mobilegameads/create', 'MobileGameAdsController@getCreate');
    Route::get('mobilegameads/edit/{enc_id}', 'MobileGameAdsController@getEdit');
    Route::get('mobilegameads/delete/{enc_id}', 'MobileGameAdsController@getDelete');
    Route::post('mobilegameads/update/{enc_id}', 'MobileGameAdsController@postUpdate');

    // MobilePushController
    Route::get('mobilepush/', 'MobilePushController@getIndex');
    Route::get('mobilepush/create', 'MobilePushController@getCreate');
    Route::get('mobilepush/edit/{enc_id}', 'MobilePushController@getEdit');
    Route::get('mobilepush/delete/{enc_id}', 'MobilePushController@getDelete');
    Route::post('mobilepush/update/{enc_id}', 'MobilePushController@postUpdate');
    Route::post('mobilepush/link/{enc_notification_id}', 'MobilePushController@postLink');
    Route::get('mobilepush/unlink/{enc_notification_id}/{enc_segment_id}', 'MobilePushController@getUnlink');
    Route::group(array('prefix' => 'stampreward'), function(){
       Route::get("/",                                         "StampRewardController@listStamprewards");
        Route::get("/edit/{stampreward_id}",                    "StampRewardController@getEdit");
        Route::get("/delete/{id}",                              "StampRewardController@getDelete");
        Route::post("/update/{id}",                             "StampRewardController@postUpdate");
        Route::get("/create",                                   "StampRewardController@getCreate");
    });

        Route::group(array('prefix' => 'suppliers'), function () {
       Route::get("/",                                          "SupplierController@listSuppliers");
        Route::get("/new",                                       "SupplierController@newSupplier");
        Route::get("/view/{id}",                                 "SupplierController@viewSupplier");
        Route::get("/delete/{id}",                               "SupplierController@deleteSupplier");
        Route::post("/update/{id}",                              "SupplierController@updateSupplier");
        Route::post("/attach/network/{supplier_id}",             "SupplierController@attachToNetwork");
        Route::get("/detach/network/{supplier_id}/{network_id}", "SupplierController@detachFromNetwork");
        Route::get("/resetnetworks/{supplier_id}/{parent_id}",   "SupplierController@getResetNetworks");
    });

    Route::group(array('prefix' => 'supplierstore'), function () {
        Route::get("/new/{supplier_id}",                         "SupplierstoreController@newSupplierstore");
        Route::get("/edit/{supplierstore_id}",                   "SupplierstoreController@editSupplierstore");
        Route::get("/list/{supplier_id}",                        "SupplierstoreController@listSupplierstores");
        Route::get("/delete/{supplier_id}/{supplierstore_id}",   "SupplierstoreController@deleteSupplierstore");
        Route::post("/update/{supplierstore_id}",                "SupplierstoreController@updateSupplierstore");
    });

    Route::group(array('prefix' => 'tiers'), function () {
        Route::get("/",                                         "TierController@listTiers");
        Route::get("/new",                                      "TierController@newTier");
        Route::get("/view/{id}",                                "TierController@viewTier");
        Route::get("/delete/{id}",                              "TierController@deleteTier");
        Route::post("/update/{id}",                             "TierController@updateTier");
        Route::post("/attach/network/{tier_id}",                "TierController@attachToNetwork");
        Route::get("/detach/network/{tier_id}/{network_id}",    "TierController@detachFromNetwork");
        Route::get("/resetnetworks/{tier_id}/{parent_id}",      "TierController@getResetNetworks");
    });

    Route::group(array('prefix' => 'tiercriteria'), function () {
        Route::get("/new/{tier_id}",                         "TiercriteriaController@newTiercriteria");
        Route::get("/edit/{tiercriteria_id}",                "TiercriteriaController@editTiercriteria");
        Route::get("/list/{tier_id}",                        "TiercriteriaController@listTiercriterias");
        Route::get("/delete/{tiercriteria_id}",              "TiercriteriaController@deleteTiercriteria");
        Route::post("/update/{tiercriteria_id}",             "TiercriteriaController@updateTiercriteria");
    });

    Route::group(array('prefix' => 'partners'), function () {
        Route::get("/",                                             "PartnerController@listPartners");
        Route::get("/new",                                          "PartnerController@newPartner");
        Route::get("/view/{id}",                                    "PartnerController@viewPartner");
        Route::get("/delete/{id}",                                  "PartnerController@deletePartner");
        Route::post("/update/{id}",                                 "PartnerController@updatePartner");
        Route::post("/attachsupplier/{supplier_id}",                "PartnerController@attachSupplierPartner");
        Route::get("/unlinksupplier/{supplier_id}/{partner_id}",    "PartnerController@unlinkSupplierPartner");
        Route::post("/attach/{user_id}",                            "PartnerController@attachPartner");
        Route::get("/unlink/{user_id}/{partner_id}",                "PartnerController@unlinkPartner");
        Route::post("/attachadmin/{admin_id}",                      "PartnerController@attachAdminPartner");
        Route::get("/unlinkadmin/{admin_id}/{partner_id}",          "PartnerController@unlinkAdminPartner");
        Route::post("/attach/network/{partner_id}",                 "PartnerController@attachToNetwork");
        Route::get("/detach/network/{partner_id}/{network_id}",     "PartnerController@detachFromNetwork");
        Route::get("/resetnetworks/{partner_id}/{parent_id}",       "PartnerController@getResetNetworks");
        Route::get("/detach/channel/{partner_id}/{channel_id}",     "PartnerController@unlinkChannel");
        Route::post("/attach/channel/{partner_id}",                 "PartnerController@linkChannel");
        Route::get("/detach/partnerlink/{partner_id}/{partner_link_id}", "PartnerController@unlinkPartnerLinked");
        Route::post("/attach/partnerlink/{partner_id}",             "PartnerController@linkPartnerLinked");
        Route::post("/link_country/{id}",                           "PartnerController@postLink_country");
        Route::get("/unlink_country/{partner_id}/{country_id}",     "PartnerController@getUnlink_country");
        Route::post("/attach/agreport/{partner_id}",                "PartnerController@linkAgReport");
        Route::get("/detach/agreport/{partner_id}/{report_id}",     "PartnerController@unlinkAgReport");
        Route::post("/update_name/{partner_id}/{name}",             "PartnerController@updateDraftPartnerName");
    });

    Route::group(array('prefix' => 'countries'), function () {
        Route::get("/",                                         "CountryController@listCountries");
        Route::get("/new",                                      "CountryController@newCountry");
        Route::post("/new",                                     "CountryController@createNewCountry");
        Route::get("/view/{id}",                                "CountryController@viewCountry");
        Route::get("/delete/{id}",                              "CountryController@deleteCountry");
        Route::get("/update/{id}",                              "CountryController@updateCountry");
        Route::post("/update/{id}",                             "CountryController@saveCountry");
    });

    Route::group(array('prefix' => 'languages'), function () {
        Route::get("/",                                         "LanguageController@listLanguages");
        Route::get("/new",                                      "LanguageController@newLanguage");
        Route::post("/new",                                     "LanguageController@createNewLanguage");
        Route::get("/view/{id}",                                "LanguageController@viewLanguage");
        Route::get("/delete/{id}",                              "LanguageController@deleteLanguage");
        Route::get("/update/{id}",                              "LanguageController@updateLanguage");
        Route::post("/update/{id}",                             "LanguageController@saveLanguage");
    });

    Route::group(array('prefix' => 'localizationkeys'), function () {
        Route::get("/",                                         "LocalizationkeyController@listLocalizationkeys");
        Route::get("/new",                                      "LocalizationkeyController@newLocalizationkey");
        Route::post("/new",                                     "LocalizationkeyController@createNewLocalizationkey");
        Route::get("/view/{id}",                                "LocalizationkeyController@viewLocalizationkey");
        Route::get("/delete/{id}",                              "LocalizationkeyController@deleteLocalizationkey");
        Route::get("/update/{id}",                              "LocalizationkeyController@updateLocalizationkey");
        Route::post("/update/{id}",                             "LocalizationkeyController@saveLocalizationkey");
    });

    Route::group(array('prefix' => 'langkey'), function () {
        Route::get("/new/{lang_id}",                         "LangKeyController@newLangKey");
        Route::get("/edit/{langkey_id}",                     "LangKeyController@editLangKey");
        Route::get("/list/{lang_id}",                        "LangKeyController@listLangKeys");
        Route::get("/delete/{langkey_id}",                   "LangKeyController@deleteLangKey");
        Route::post("/update/{langkey_id}",                  "LangKeyController@updateLangKey");
        Route::get("/keys",                                  "LangKeyController@localizationkeys");
    });

    Route::group(array('prefix' => 'categories'), function () {
        Route::get("/",                                             "CategoryController@listCategories");
        Route::get("/new",                                          "CategoryController@newCategory");
        Route::post("/new",                                         "CategoryController@createNewCategory");
        Route::get("/view/{id}",                                    "CategoryController@viewCategory");
        Route::get("/delete/{id}",                                  "CategoryController@deleteCategory");
        Route::get("/update/{id}",                                  "CategoryController@updateCategory");
        Route::post("/update/{id}",                                 "CategoryController@saveCategory");
        Route::get("/newPopover",                                   "CategoryController@newPopoverCategory");
        Route::post("/updatePopover/{id}",                          "CategoryController@savePopoverCategory");
        Route::get("/getList",                                      "CategoryController@getCategoriesList");
        Route::post("/link_partner/{id}",                           "CategoryController@postLink_partner");
        Route::get("/unlink_partner/{category_id}/{partner_id}",    "CategoryController@getUnlink_partner");
    });

    Route::group(array('prefix' => 'prodmodels'), function () {
        Route::get("/",                                         "ProdmodelController@listProdmodels");
        Route::get("/new",                                      "ProdmodelController@newProdmodel");
        Route::post("/new",                                     "ProdmodelController@createNewProdmodel");
        Route::get("/view/{id}",                                "ProdmodelController@viewProdmodel");
        Route::get("/delete/{id}",                              "ProdmodelController@deleteProdmodel");
        Route::get("/update/{id}",                              "ProdmodelController@updateProdmodel");
        Route::post("/update/{id}",                             "ProdmodelController@saveProdmodel");
        Route::get("/newPopover",                               "ProdmodelController@newPopoverProdmodel");
        Route::post("/updatePopover/{id}",                      "ProdmodelController@savePopoverProdmodel");
        Route::get("/getList",                                  "ProdmodelController@getProdmodelsList");
    });

    Route::group(array('prefix' => 'prodsubmodels'), function () {
       Route::get("/",                                         "ProdsubmodelController@listProdsubmodels");
        Route::get("/new",                                      "ProdsubmodelController@newProdsubmodel");
        Route::post("/new",                                     "ProdsubmodelController@createNewProdsubmodel");
        Route::get("/view/{id}",                                "ProdsubmodelController@viewProdsubmodel");
        Route::get("/delete/{id}",                              "ProdsubmodelController@deleteProdsubmodel");
        Route::get("/update/{id}",                              "ProdsubmodelController@updateProdsubmodel");
        Route::post("/update/{id}",                             "ProdsubmodelController@saveProdsubmodel");
        Route::get("/newPopover",                               "ProdsubmodelController@newPopoverProdsubmodel");
        Route::post("/updatePopover/{id}",                      "ProdsubmodelController@savePopoverProdsubmodel");
        Route::get("/getList",                                  "ProdsubmodelController@getProdsubmodelsList");
    });

    Route::group(array('prefix' => 'store'), function () {
        Route::get("/new/{partner_id}",                         "StoreController@newStore");
        Route::get("/edit/{store_id}",                          "StoreController@editStore");
        Route::get("/list/{partner_id}",                        "StoreController@listStores");
        Route::get("/delete/{store_id}",                        "StoreController@deleteStore");
        Route::post("/update/{store_id}",                       "StoreController@updateStore");
        Route::get("/check-pos-mapping-id/{partner_id}/{mapping_id}/{store_id}", "StoreController@checkPosMappingId");
    });

    Route::group(array('prefix' => 'prodtranslation'), function () {
        Route::post("/create",                                  "ProdtranslationController@createProdtranslation");
        Route::get("/new/{product_id}",                         "ProdtranslationController@newProdtranslation");
        Route::get("/edit/{prodtranslation_id}",                "ProdtranslationController@editProdtranslation");
        Route::get("/list/{product_id}",                        "ProdtranslationController@listProdtranslations");
        Route::get("/delete/{prodtranslation_id}",              "ProdtranslationController@deleteProdtranslation");
        Route::post("/update/{prodtranslation_id}",             "ProdtranslationController@updateProdtranslation");
    });

    Route::group(array('prefix' => 'prodmodeltranslation'), function () {
        Route::get("/new/{product_id}",                         "ProdmodeltranslationController@newProdmodeltranslation");
        Route::get("/edit/{prodtranslation_id}",                "ProdmodeltranslationController@editProdmodeltranslation");
        Route::get("/list/{product_id}",                        "ProdmodeltranslationController@listProdmodeltranslations");
        Route::get("/delete/{prodtranslation_id}",              "ProdmodeltranslationController@deleteProdmodeltranslation");
        Route::post("/update/{prodtranslation_id}",             "ProdmodeltranslationController@updateProdmodeltranslation");
    });

            Route::group(array('prefix' => 'prodsubmodeltranslation'), function () {
       Route::get("/new/{product_id}",                         "ProdsubmodeltranslationController@newProdsubmodeltranslation");
        Route::get("/edit/{prodtranslation_id}",                "ProdsubmodeltranslationController@editProdsubmodeltranslation");
        Route::get("/list/{product_id}",                        "ProdsubmodeltranslationController@listProdsubmodeltranslations");
        Route::get("/delete/{prodtranslation_id}",              "ProdsubmodeltranslationController@deleteProdsubmodeltranslation");
        Route::post("/update/{prodtranslation_id}",             "ProdsubmodeltranslationController@updateProdsubmodeltranslation");
    });

    Route::group(array('prefix' => 'cattranslation'), function () {
        Route::get("/new/{category_id}",                         "CattranslationController@newCattranslation");
        Route::get("/edit/{cattranslation_id}",                  "CattranslationController@editCattranslation");
        Route::get("/list/{category_id}",                        "CattranslationController@listCattranslations");
        Route::get("/delete/{cattranslation_id}",                "CattranslationController@deleteCattranslation");
        Route::post("/update/{cattranslation_id}",               "CattranslationController@updateCattranslation");
    });

    Route::group(array('prefix' => 'currencytranslation'), function () {
        Route::get("/new/{currency_id}",                         "CurrencytranslationController@newCurrencyTranslation");
        Route::get("/edit/{currencyTranslation_id}",             "CurrencytranslationController@editCurrencyTranslation");
        Route::get("/list/{currency_id}",                        "CurrencytranslationController@listCurrencyTranslations");
        Route::get("/delete/{currencyTranslation_id}",           "CurrencytranslationController@deleteCurrencyTranslation");
        Route::post("/update/{currencyTranslation_id}",          "CurrencytranslationController@updateCurrencyTranslation");
    });

            Route::group(array('prefix' => 'brandtranslation'), function () {
       Route::get("/new/{brand_id}",                         "BrandtranslationController@newBrandtranslation");
        Route::get("/edit/{brandtranslation_id}",             "BrandtranslationController@editBrandtranslation");
        Route::get("/list/{brand_id}",                        "BrandtranslationController@listBrandtranslations");
        Route::get("/delete/{brandtranslation_id}",           "BrandtranslationController@deleteBrandtranslation");
        Route::post("/update/{brandtranslation_id}",          "BrandtranslationController@updateBrandtranslation");
    });

        Route::group(array('prefix' => 'userinvoice'), function () {
       Route::get("/edit/{store_id}",              "InvoiceController@editInvoice");
        Route::get("/list/",                        "InvoiceController@listInvoices");
        Route::get("/delete/{store_id}",            "InvoiceController@deleteInvoice");
        Route::post("/update/{store_id}",           "InvoiceController@updateInvoice");
    });

        Route::group(array('prefix' => 'travel'), function () {		        Route::get("/flightsearch",                         "TravelController@flightsearch");
        Route::get("/list",                                 "TravelController@listflights");
        Route::get("/flights",                              "TravelController@flights");
        Route::post("/poll_flights",                        "TravelController@poll_flights");
        Route::get("/confirm_flight",                       "TravelController@confirm_flight");
        Route::post("/do_confirm",							"TravelController@do_confirm");
		Route::get("/search_flight_locations",				"TravelController@search_flight_locations");
				Route::get("/hotelsearch",							"TravelController@hotelsearch");
		Route::get("/search_locations",						"TravelController@search_locations");
		Route::get("/hotels",								"TravelController@hotels");
		Route::post("/poll_hotels",							"TravelController@poll_hotels");
		Route::get("/hotel_info/{hotel_id}",					"TravelController@hotel_info");
		Route::get("/confirm_hotel",							"TravelController@confirm_hotel");
				Route::get("/carsearch",								"TravelController@carsearch");
		Route::get("/ajax_cities",							"TravelController@ajax_cities");
		Route::get("/ajax_locations",						"TravelController@ajax_locations");
		Route::get("/cars",									"TravelController@cars");
		Route::post("/confirm_car",							"TravelController@confirm_car");
    });

    Route::group(array('prefix' => 'members'), function () {
        Route::get("/",                                         "MemberController@listMembers");
        Route::get("/new",                                      "MemberController@newMember");
        Route::get("/view/{id}",                                "MemberController@viewMember");
        Route::post("/update/{id}",                             "MemberController@updateMember");
        Route::get("/delete/{id}",                              "MemberController@deleteMember");
        Route::get("/verify/{field}/{value}/{exclusion_id}",    "MemberController@verifyUniqueField");
        Route::get("/sendpin/{id}",                             "MemberController@getSendPin");
        Route::get("/balances/{id}",                            "MemberController@getBalances");
        Route::get("/sendsms/{user_id}/{partner_id}/{smstype}", "MemberController@getSendsms");
        Route::post("/createmembership/{member_id}",            "MemberController@createAffiliateProgramMembership");
        Route::get("/deletemembership/{member_id}/{apm_id}",    "MemberController@deleteAffiliateProgramMembership");
        Route::get("/viewmembership/{program_id}",               "MemberController@viewAffiliateProgramMembership");
        Route::post("/updatemembership/{program_id}/{member_id}",    "MemberController@updateAffiliateProgramMembership");
    });

        Route::group(array('prefix' => 'admins'), function () {
       Route::get("/",                                         "AdminController@listAdmins");
        Route::get("/new",                                      "AdminController@newAdmin");
        Route::get("/view/{id}",                                "AdminController@viewAdmin");
        Route::post("/update/{id}",                             "AdminController@updateAdmin");
        Route::get("/delete/{id}",                              "AdminController@deleteAdmin");
        Route::get("/verify/{field}/{value}/{exclusion_id}",    "AdminController@verifyUniqueField");
        Route::get("/updatelang/{id}",                          "AdminController@updatePrefLang");
    });

        Route::group(array('prefix' => 'cards'), function () {
       Route::post("/save/{id}",                               "CardController@saveCard");
        Route::get("/unlink/{user_id}/{id}",                    "CardController@unlinkCard");
    });

    Route::group(array('prefix' => 'roles'), function () {
        Route::get("/",                                         "RoleController@listRoles");
        Route::get("/new",                                      "RoleController@newRole");
        Route::get("/view/{id}",                                "RoleController@viewRole");
        Route::get("/delete/{id}",                              "RoleController@deleteRole");
        Route::post("/update/{id}",                             "RoleController@updateRole");
        Route::post("/attach/{user_id}",                        "RoleController@attachRole");
        Route::get("/unlink/{user_id}/{role_id}",               "RoleController@unlinkRole");
        Route::post("/attachadmin/{admin_id}",                  "RoleController@attachAdminRole");
        Route::get("/unlinkadmin/{admin_id}/{role_id}",         "RoleController@unlinkAdminRole");
    });

     Route::group(array('prefix' => 'segments'), function() {
         Route::get("/",                                           "SegmentController@listSegments");
         Route::get("/new",                                        "SegmentController@newSegment");
         Route::get("/new_manual",                                 "SegmentController@newManualSegment");
         Route::get("/view/{id}",                                  "SegmentController@viewSegment");
         Route::get("/view_manual/{id}",                           "SegmentController@viewManualSegment");
         Route::post("/update/{id}",                               "SegmentController@updateSegment");
         Route::post("/manual_update/{id}",                        "SegmentController@updateManualSegment");
         Route::get("/delete/{id}",                                "SegmentController@deleteSegment");
         Route::get("/ejs/{template}.ejs",                         "SegmentController@loadEJSTemplate");
         Route::get("/delete_criteria/{segment_id}/{criteria_id}", "SegmentController@deleteSegmentCriteria");
     });

     Route::group(array('prefix' => 'segmentuser'), function () {
         Route::get("/new/{segment_id}",                         "SegmentController@newSegmentuser");
         Route::get("/list/{segment_id}",                        "SegmentController@listSegmentuser");
         Route::get("/delete/{user_id}/{segmentuser_id}",        "SegmentController@deleteSegmentuser");
         Route::post("/create",                                  "SegmentController@createSegmentuser");
     });

     Route::group(array('prefix' => 'brands'), function () {
        Route::get("/",                                         "BrandController@listBrands");
        Route::get("/new",                                      "BrandController@newBrand");
        Route::post("/new",                                     "BrandController@createNewBrand");
        Route::get("/view/{id}",                                "BrandController@viewBrand");
        Route::get("/delete/{id}",                              "BrandController@deleteBrand");
        Route::get("/update/{id}",                              "BrandController@updateBrand");
        Route::post("/update/{id}",                             "BrandController@saveBrand");
        Route::get("/newPopover",                               "BrandController@newPopoverBrand");
        Route::post("/updatePopover/{id}",                      "BrandController@savePopoverBrand");
        Route::get("/getList",                                  "BrandController@getBrandsList");
    });

    Route::group(array('prefix' => 'catalogue'), function () {
        Route::get("/",                                         "ProductController@listProducts");
        Route::get("/new",                                      "ProductController@newProduct");
        Route::get("/view/{id}",                                "ProductController@viewProduct");
        Route::get("/delete/{id}",                              "ProductController@deleteProduct");
        Route::any("/update/{id}",                              "ProductController@saveProduct");
        Route::post("/link_partner/{id}",                       "ProductController@postLink_partner");
        Route::get("/unlink_partner/{product_id}/{partner_id}", "ProductController@getUnlink_partner");
        Route::post("/link_network/{id}",                       "ProductController@postLink_network");
        Route::get("/unlink_network/{product_id}/{network_id}", "ProductController@getUnlink_network");
        Route::post("/link_segment/{id}",                       "ProductController@postLink_segment");
        Route::get("/unlink_segment/{product_id}/{segment_id}", "ProductController@getUnlink_segment");
        Route::post("/link_country/{id}",                       "ProductController@postLink_country");
        Route::get("/unlink_country/{product_id}/{country_id}", "ProductController@getUnlink_country");
        Route::post("/link_channel/{id}",                       "ProductController@postLink_channel");
        Route::get("/unlink_channel/{product_id}/{partner_id}", "ProductController@getUnlink_channel");
    });

    Route::group(array('prefix' => 'productpricing'), function () {
        Route::get("/",                                                         "ProductPricingController@listProducts");
        Route::get("/new",                                                      "ProductPricingController@newProduct");
        Route::get("/view/{id}",                                                "ProductPricingController@viewProduct");
        Route::get("/delete/{id}",                                              "ProductPricingController@deleteProduct");
        Route::any("/update/{id}",                                             "ProductPricingController@saveProduct");
        Route::post("/link_partner/{id}",                                       "ProductPricingController@postLink_partner");
        Route::get("/unlink_partner/{product_id}/{partner_id}",                 "ProductPricingController@getUnlink_partner");
        Route::post("/link_network/{id}",                                       "ProductPricingController@postLink_network");
        Route::get("/unlink_network/{product_id}/{network_id}",                 "ProductPricingController@getUnlink_network");
        Route::post("/link_segment/{id}",                                       "ProductPricingController@postLink_segment");
        Route::get("/unlink_segment/{product_id}/{segment_id}",                 "ProductPricingController@getUnlink_segment");
        Route::post("/link_country/{id}",                                       "ProductPricingController@postLink_country");
        Route::get("/unlink_country/{product_id}/{country_id}",                 "ProductPricingController@getUnlink_country");
        Route::post("/link_channel/{id}/{product_id}",                          "ProductPricingController@postLink_channel");
        Route::get("/unlink_channel/{product_id}/{partner_id}",                 "ProductPricingController@getUnlink_channel");
        Route::post("/link_redemption_partner/{id}",                            "ProductPricingController@postLink_redemption_partner");
        Route::get("/unlink_redemption_partner/{product_id}/{partner_id}",      "ProductPricingController@getUnlink_redemption_partner");
        Route::post("/link_reward_partner/{id}",                                "ProductPricingController@postLink_reward_partner");
        Route::post("/update_reward_partner/{id}/{product_id}",                 "ProductPricingController@postUpdate_reward_partner");
        Route::get("/unlink_reward_partner/{product_id}/{reward_partner_id}",   "ProductPricingController@getUnlink_reward_partner");
        Route::get("/edit_reward_partner/{reward_partner_id}",                  "ProductPricingController@getEdit_reward_partner");
        Route::get("/view_redemption_partner/{redemption_partner_id}",          "ProductPricingController@getViewProductRedemptionPartner");
        Route::post("/save_redemption_partner/{redemption_partner_id}",         "ProductPricingController@saveProductRedemption");
        Route::post("/link_upc/{product_id}",                                   "ProductPricingController@postLink_upc");
        Route::post("/update_upc/{id}/{product_id}",                            "ProductPricingController@postUpdate_upc");
        Route::get("/unlink_upc/{product_id}/{id}",                             "ProductPricingController@getUnlink_upc");
        Route::get("/edit_upc/{id}",                                            "ProductPricingController@getEdit_upc");
        Route::get("/edit_country_details/{id}",                                "ProductPricingController@getEdit_country");
        Route::post("/update_country/{id}/{product_id}",                        "ProductPricingController@postUpdate_country");
        Route::get("/channels_segments/{id}",                                   "ProductPricingController@getSegmentsByChannels");
        Route::get("/get_prefix/{id}",                                          "ProductPricingController@getPrefixByPartner");
    });

            Route::group(array('prefix' => 'offer'), function () {
       Route::get("/",                                         "OfferController@listOffers");
        Route::get("/new",                                      "OfferController@newOffer");
        Route::get("/view/{id}",                                "OfferController@viewOffer");
        Route::get("/delete/{id}",                              "OfferController@deleteOffer");
        Route::post("/update/{id}",                             "OfferController@saveOffer");
        Route::post("/link_partner/{id}",                       "OfferController@postLink_partner");
        Route::get("/unlink_partner/{offer_id}/{partner_id}",   "OfferController@getUnlink_partner");
        Route::post("/link_segment/{id}",                       "OfferController@postLink_segment");
        Route::get("/unlink_segment/{offer_id}/{segment_id}",   "OfferController@getUnlink_segment");
        Route::post("/link_country/{id}",                       "OfferController@postLink_country");
        Route::get("/unlink_country/{offer_id}/{country_id}",   "OfferController@getUnlink_country");
        Route::post("/link_channel/{id}",                       "OfferController@postLink_channel");
        Route::get("/unlink_channel/{offer_id}/{partner_id}",   "OfferController@getUnlink_channel");
        Route::post("/link_tier/{id}",                          "OfferController@postLink_tier");
        Route::get("/unlink_tier/{offer_id}/{tier_id}",         "OfferController@getUnlink_tier");
    });

            Route::group(array('prefix' => 'offertranslation'), function () {
       Route::get("/new/{offer_id}",                           "OffertranslationController@newOffertranslation");
        Route::get("/edit/{offertranslation_id}",               "OffertranslationController@editOffertranslation");
        Route::get("/list/{offer_id}",                          "OffertranslationController@listOffertranslations");
        Route::get("/delete/{offertranslation_id}",             "OffertranslationController@deleteOffertranslation");
        Route::post("/update/{offertranslation_id}",            "OffertranslationController@updateOffertranslation");
        Route::post("/create",                                  "OffertranslationController@createOffertranslation");
    });

    Route::group(array('prefix' => 'estatement'), function () {
        Route::get("/",                                             "EstatementController@listEstatements");
        Route::get("/new",                                          "EstatementController@newEstatement");
        Route::get("/view/{id}",                                    "EstatementController@viewEstatement");
        Route::get("/view_history/{id}",                            "EstatementController@viewEstatementHistory");
        Route::get("/delete/{id}",                                  "EstatementController@deleteEstatement");
        Route::post("/update/{id}",                                 "EstatementController@saveEstatement");
        Route::post("/link_segment/{id}",                           "EstatementController@postLink_segment");
        Route::get("/unlink_segment/{estatement_id}/{segment_id}",  "EstatementController@getUnlink_segment");
        Route::post("/link_country/{id}",                           "EstatementController@postLink_country");
        Route::get("/unlink_country/{estatement_id}/{country_id}",  "EstatementController@getUnlink_country");
        Route::post("/link_tier/{id}",                              "EstatementController@postLink_tier");
        Route::get("/unlink_tier/{estatement_id}/{tier_id}",        "EstatementController@getUnlink_tier");
        Route::post("/linkuser/{id}",                               "EstatementController@postLinkuser");
        Route::get("/unlinkuser/{estatement_id}/{user_id}",         "EstatementController@getUnlinkuser");
        Route::post("/linktier/{id}",                               "EstatementController@postLinktier");
        Route::get("/unlinktier/{estatement_id}/{tier_id}",         "EstatementController@getUnlinktier");
        Route::get("/sendnow/{estatement_id}/{lang}",               "EstatementController@getSendNow");
        Route::get("/sendBulkEstatement/{estatement_id}/{lang}",    "EstatementController@sendBulkEstatement");
        Route::get("/testrtl/{estatement_id}/{lang}",               "EstatementController@sendEmailsRtl");
        Route::get("/sendestatement/{estatement_id}/{lang}",        "EstatementController@fireBulkEstatement");
        Route::get("/tiersbypartner/{partner_id}",                  "EstatementController@getTiersByPartner");
    });

            Route::group(array('prefix' => 'estatementtranslation'), function () {
       Route::get("/new/{estatement_id}",                           "EstatementtranslationController@newEstatementtranslation");
        Route::get("/edit/{estatementtranslation_id}",               "EstatementtranslationController@editEstatementtranslation");
        Route::get("/list/{estatement_id}",                          "EstatementtranslationController@listEstatementtranslations");
        Route::get("/delete/{estatementtranslation_id}",             "EstatementtranslationController@deleteEstatementtranslation");
        Route::post("/update/{estatementtranslation_id}",            "EstatementtranslationController@updateEstatementtranslation");
        Route::post("/create",                                       "EstatementtranslationController@createEstatementtranslation");
    });

            Route::group(array('prefix' => 'estatementitem'), function () {
       Route::get("/new/{estatement_id}",                           "EstatementController@newEstatementitem");
        Route::get("/edit/{estatementitem_id}",                      "EstatementController@editEstatementitem");
        Route::get("/list/{estatement_id}",                          "EstatementController@listEstatementitems");
        Route::get("/delete/{estatementitem_id}",                    "EstatementController@deleteEstatementitem");
        Route::post("/update/{estatementitem_id}",                   "EstatementController@updateEstatementitem");
        Route::post("/create",                                       "EstatementController@createEstatementitem");
    });

            Route::group(array('prefix' => 'locale'), function () {
       Route::get("/countries",                                "LocaleController@listCountries");
        Route::get("/areas/{id?}",                              "LocaleController@listAreas");
        Route::get("/cities/{id?}",                             "LocaleController@listCities");
    });

        Route::group(array('prefix' => 'call_center'), function () {
       Route::get("/",                                             "TicketController@listTickets")->name('tickets');
        Route::get("/new",                                          "TicketController@newTicket");
        Route::get("/view/{id}",                                    "TicketController@viewTicket")->name('call-center-view');
        Route::get("/delete/{id}",                                  "TicketController@deleteTicket");
        Route::post("/update/{id}",                                 "TicketController@saveTicket");
        Route::post("/search",                                      "TicketController@postSearchMembers");
        Route::post("/load_profile",                                "TicketController@postLoadProfile");
        Route::post("/update_member/{id}" ,                         "TicketController@postUpdateMember");
        Route::post("/update_partner/{id}",                         "TicketController@postUpdatePartner");
        Route::get("/load_products/{search}/{network_id}",          "TicketController@getLoadProducts");
        Route::get("/load_products-new/{search}/{network_id}/{country_id}/{channel_id}",      "TicketController@getLoadProductsNew");
        Route::post("/redeem_item",                                 "TicketController@postRedeemItem");
        Route::post("/redeem_item-new",                             "TicketController@postRedeemItemNew");
        Route::get("/balance/{user_id}",                            "TicketController@getUserBalance");
        Route::get("/reverse_trx/{trx_id}",                         "TicketController@getReverseTransaction");
        Route::get("/reverse_module/{trx_id}",                      "TicketController@getReverseModule");
        Route::post("/reverse_trx_item/{trxItem_id}/{qty}",         "TicketController@postReverseTransactionItem");
        Route::post("/reverse_trx_delivery/{trxDelivery_id}/{qty}", "TicketController@postReverseTransactionDelivery");
        Route::get("/reverse-trx-approve/{code}",                   "TicketController@approveTransaction")->name('reverse-trx-approve');
        Route::get("/reverse-trx-decline/{code}",                   "TicketController@declineTransaction")->name('reverse-trx-decline');
    });

        Route::group(array('prefix' => 'airports'), function () {
       Route::get("/",                                         "AirportController@listAirports");
        Route::get("/new",                                      "AirportController@newAirport");
        Route::post("/new",                                     "AirportController@createNewAirport");
        Route::get("/view/{id}",                                "AirportController@viewAirport");
        Route::get("/delete/{id}",                              "AirportController@deleteAirport");
        Route::get("/update/{id}",                              "AirportController@updateAirport");
        Route::post("/update/{id}",                             "AirportController@saveAirport");
    });

        Route::group(array('prefix' => 'carriers'), function () {
       Route::get("/",                                         "CarrierController@listCarriers");
        Route::get("/new",                                      "CarrierController@newCarrier");
        Route::post("/new",                                     "CarrierController@createNewCarrier");
        Route::get("/view/{id}",                                "CarrierController@viewCarrier");
        Route::get("/delete/{id}",                              "CarrierController@deleteCarrier");
        Route::get("/update/{id}",                              "CarrierController@updateCarrier");
        Route::post("/update/{id}",                             "CarrierController@saveCarrier");
    });

            Route::group(array('prefix' => 'redemption_order'), function () {
       Route::get("/",                                         "RedemptionOrderController@listRedemptionOrders");
        Route::get("/view/{id}",                                "RedemptionOrderController@viewRedemptionOrder");
        Route::post("/update_order/{id}",                       "RedemptionOrderController@updateRedemptionOrder");
    });

         Route::group(array('prefix' => 'affiliate_program'), function () {
       Route::get("/",                                                 "AffiliateProgramController@listAffiliateProgram");
        Route::get("/new",                                              "AffiliateProgramController@newAffiliateProgram");
        Route::get("/view/{id}",                                        "AffiliateProgramController@viewAffiliateProgram");
        Route::post("/update/{id}",                                     "AffiliateProgramController@updateAffiliateProgram");
        Route::get("/delete/{id}",                                      "AffiliateProgramController@deleteAffiliateProgram");
        Route::post("/attachpartner/{affiliatePartner_id}",             "AffiliateProgramController@attachAffiliateProgramPartner");
        Route::get("/unlinkpartner/{affiliatePartner_id}/{partner_id}", "AffiliateProgramController@unlinkAffiliateProgramPartner");
    });

            Route::group(array('prefix' => 'affiliates'), function () {
       Route::get("/",                                             "AffiliateController@listAffiliates");
        Route::get("/new",                                          "AffiliateController@newAffiliate");
        Route::get("/view/{id}",                                    "AffiliateController@viewAffiliate");
        Route::post("/update/{id}",                                 "AffiliateController@updateAffiliate");
        Route::get("/delete/{id}",                                  "AffiliateController@deleteAffiliate");
        Route::post("/attachaffiliate/{affiliate_id}",              "AffiliateController@attachAffiliatePartner");
        Route::get("/unlinkaffiliate/{ffiliate_id}/{partner_id}",   "AffiliateController@unlinkAffiliatePartner");
    });

        Route::group(array('prefix' => 'rewardtypes'), function(){
       Route::get("/",                                         "RewardTypesController@listRewardTypes");
        Route::get("/edit/{id}",                                "RewardTypesController@getEdit");
        Route::get("/delete/{id}",                              "RewardTypesController@getDelete");
        Route::post("/update/{id}",                             "RewardTypesController@postUpdate");
        Route::get("/create",                                   "RewardTypesController@getCreate");
    });

        Route::group(array('prefix' => 'triggers'), function () {
       Route::get("/",                                         "TriggersController@listTriggers");
        Route::get("/new",                                      "TriggersController@newTrigger");
        Route::get("/view/{id}",                                "TriggersController@viewTrigger");
        Route::get("/delete/{id}",                              "TriggersController@deleteTrigger");
        Route::post("/update/{id}",                             "TriggersController@saveTrigger");
        Route::post("/link_partner/{id}",                       "TriggersController@postLink_partner");
        Route::get("/unlink_partner/{offer_id}/{partner_id}",   "TriggersController@getUnlink_partner");
        Route::post("/link_segment/{id}",                       "TriggersController@postLink_segment");
        Route::get("/unlink_segment/{offer_id}/{segment_id}",   "TriggersController@getUnlink_segment");
        Route::post("/link_channel/{id}",                       "TriggersController@postLink_channel");
        Route::get("/unlink_channel/{offer_id}/{partner_id}",   "TriggersController@getUnlink_channel");
        Route::post("/link_tier/{id}",                          "TriggersController@postLink_tier");
        Route::get("/unlink_tier/{offer_id}/{tier_id}",         "TriggersController@getUnlink_tier");
    });

     // BatchController
     Route::get('coupons/', 'BatchController@getIndex');
     Route::get('coupons/create', 'BatchController@getCreate');
     Route::get('coupons/edit/{enc_id?}', 'BatchController@getEdit');
     Route::post('coupons/update/{enc_id?}', 'BatchController@postUpdate');
     Route::get('coupons/destroy/{enc_id}', 'BatchController@getDestroy');
     Route::get('coupons/verify', 'BatchController@getVerify');
     Route::get('coupons/verifycoupon/{enc_id}', 'BatchController@getVerifycoupon');
     Route::get('coupons/download/{enc_id?}/{format?}', 'BatchController@getDownload');
     Route::get('coupons/partnercountries/{enc_partner_id}', 'BatchController@getPartnercountries');

     // GeneratorController
     Route::get('generator/', 'GeneratorController@getIndex');
     Route::get('generator/create', 'GeneratorController@getCreate');
     Route::get('generator/edit/{enc_id?}', 'GeneratorController@getEdit');
     Route::post('generator/update/{enc_id?}', 'GeneratorController@postUpdate');
     Route::get('generator/download/{enc_id?}/{format?}', 'GeneratorController@getDownload');
     Route::get('generator/listbatch/{enc_id?}', 'GeneratorController@getListbatch');
     Route::get('generator/printcard/{enc_id?}', 'GeneratorController@getPrintcard');

     // LoyaltyController
     Route::get('loyalty/', 'LoyaltyController@getIndex');
     Route::get('loyalty/create', 'LoyaltyController@getCreate');
     Route::get('loyalty/edit/{enc_id?}', 'LoyaltyController@getEdit');
     Route::get('loyalty/delete/{enc_id?}', 'LoyaltyController@getDelete');
     Route::post('loyalty/update/{enc_id?}', 'LoyaltyController@postUpdate');
     Route::post('loyalty/rule/{enc_id?}', 'LoyaltyController@postRule');
     Route::get('loyalty/unlink/{enc_id?}', 'LoyaltyController@getUnlink');
     Route::post('loyalty/prioritize', 'LoyaltyController@postPrioritize');
     Route::get('loyalty/rules-history/{enc_id?}', 'LoyaltyController@getRulesHistory');
     Route::get('loyalty/approveprioritychanges/{approval_id}', 'LoyaltyController@getApproveprioritychanges');
     Route::get('loyalty/declineprioritychanges/{approval_id}', 'LoyaltyController@getDeclineprioritychanges');
     Route::get('loyalty/priority/{program_id}', 'LoyaltyController@getPriority');
     Route::get('loyalty/approvechanges/{approval_id}', 'LoyaltyController@getApprovechanges');
     Route::get('loyalty/approveruleschanges/{approval_id}', 'LoyaltyController@getApproveruleschanges');
     Route::get('loyalty/declinechanges/{approval_id}', 'LoyaltyController@getDeclinechanges');
     Route::get('loyalty/declineruleschanges/{approval_id}', 'LoyaltyController@getDeclineruleschanges');
     Route::get('loyalty/approverulesdelete/{approval_id}', 'LoyaltyController@getApproverulesdelete');
     Route::get('loyalty/declinerulesdelete/{approval_id}', 'LoyaltyController@getDeclinerulesdelete');
     Route::get('loyalty/approveloyaltydelete/{approval_id}', 'LoyaltyController@getApproveloyaltydelete');
     Route::get('loyalty/declineloyaltydelete/{approval_id}', 'LoyaltyController@getDeclineloyaltydelete');
     Route::get('loyalty/products', 'LoyaltyController@getProducts');
     Route::get('loyalty/mcccodes', 'LoyaltyController@getMcccodes');
     Route::post('loyalty/cashbackeligible/{enc_id?}', 'LoyaltyController@postCashbackeligible');
     Route::get('loyalty/approvecashbackchanges/{approval_id}', 'LoyaltyController@getApprovecashbackchanges');
     Route::get('loyalty/declinecashbackchanges/{approval_id}', 'LoyaltyController@getDeclinecashbackchanges');
     Route::get('loyalty/unlinkcashback/{enc_id?}', 'LoyaltyController@getUnlinkcashback');
     Route::get('loyalty/approvecashbackdelete/{approval_id}', 'LoyaltyController@getApprovecashbackdelete');
     Route::get('loyalty/declinecashbackdelete/{approval_id}', 'LoyaltyController@getDeclinecashbackdelete');
     Route::get('loyalty/trxtypes', 'LoyaltyController@getTrxtypes');
     Route::get('loyalty/editrule/{enc_id?}', 'LoyaltyController@getEditRule');
     Route::post('loyalty/updaterule/{enc_id?}', 'LoyaltyController@postUpdateRule');

     // NotificationController
     Route::get('notifications/', 'NotificationController@getIndex');
     Route::get('notifications/new', 'NotificationController@getNew');
     Route::get('notifications/view/{enc_id?}', 'NotificationController@getView');
     Route::get('notifications/delete/{enc_id?}', 'NotificationController@getDelete');
     Route::post('notifications/update/{enc_id?}', 'NotificationController@postUpdate');
     Route::post('notifications/test/{enc_id?}', 'NotificationController@postTest');
     Route::get('notifications/testing/{enc_id?}/{partner_id}', 'NotificationController@getTesting');
     Route::post('notifications/link/{enc_notification_id}', 'NotificationController@postLink');
     Route::get('notifications/unlink/{enc_notification_id}/{enc_segment_id}', 'NotificationController@getUnlink');
     Route::post('notifications/linkuser/{enc_notification_id}', 'NotificationController@postLinkuser');
     Route::get('notifications/unlinkuser/{enc_notification_id}/{enc_user_id}', 'NotificationController@getUnlinkuser');
     Route::post('notifications/send/{enc_id?}', 'NotificationController@postSend');
     Route::post('notifications/linktier/{enc_notification_id}', 'NotificationController@postLinktier');
     Route::get('notifications/unlinktier/{enc_notification_id}/{enc_tier_id}', 'NotificationController@getUnlinktier');

     // ReportController
     Route::get('reports/', 'ReportController@getIndex');
     Route::get('reports/tiers/{enc_tier_id?}', 'ReportController@getTiers');
     Route::get('reports/view/{enc_tier_id?}', 'ReportController@getView');
     Route::get('reports/members', 'ReportController@getMembers');
     Route::get('reports/references', 'ReportController@getReferences');

     // ReferenceController
     Route::post('reference/link/{user_id?}', 'ReferenceController@postLink');
     Route::get('reference/unlink/{user_id?}/{reference_id?}', 'ReferenceController@getUnlink');

     // NetworkController
     
     Route::get('network/view/{enc_id?}', 'NetworkController@getView');
     Route::get('network/new', 'NetworkController@getNew');
     Route::get('network/delete/{enc_id?}', 'NetworkController@getDelete');
     Route::post('network/update/{enc_id?}', 'NetworkController@postUpdate');
     Route::get('network/', 'NetworkController@getIndex');

     // PartnerCategoryController
     Route::get('partnercategory/', 'PartnerCategoryController@getIndex');
     Route::get('partnercategory/new', 'PartnerCategoryController@getNew');
     Route::get('partnercategory/view/{enc_id?}', 'PartnerCategoryController@getView');
     Route::get('partnercategory/delete/{enc_id?}', 'PartnerCategoryController@getDelete');
     Route::post('partnercategory/save/{enc_id?}', 'PartnerCategoryController@postSave');

     // CurrencyController
     Route::get('currencies/', 'CurrencyController@getIndex');
     Route::get('currencies/create', 'CurrencyController@getCreate');
     Route::get('currencies/edit/{enc_id}', 'CurrencyController@getEdit');
     Route::get('currencies/delete/{enc_id}', 'CurrencyController@getDelete');
     Route::post('currencies/update/{enc_id}', 'CurrencyController@postUpdate');

     // BannerController
     Route::get('banners/', 'BannerController@getIndex');
     Route::get('banners/create', 'BannerController@getCreate');
     Route::get('banners/edit/{enc_id?}', 'BannerController@getEdit');
     Route::post('banners/update/{enc_id?}', 'BannerController@postUpdate');
     Route::get('banners/delete/{enc_id?}', 'BannerController@getDelete');
     Route::post('banners/link-country/{enc_banner_id}', 'BannerController@postLink_country');
     Route::get('banners/unlink-country/{enc_banner_id}/{enc_country_id}', 'BannerController@getUnlink_country');
     Route::post('banners/link-page/{enc_banner_id}', 'BannerController@postLink_page');
     Route::get('banners/unlink-page/{enc_banner_id}/{enc_page_mapping_id}', 'BannerController@getUnlink_page');
     Route::post('banners/link-segment/{enc_banner_id}', 'BannerController@postLink_segment');
     Route::get('banners/unlink-segment/{enc_banner_id}/{enc_segment_id}', 'BannerController@getUnlink_segment');

         // CardPrintController
         Route::get('cardprints/', 'CardPrintController@getIndex');
         Route::get('cardprints/create', 'CardPrintController@getCreate');
         Route::get('cardprints/edit/{enc_id?}', 'CardPrintController@getEdit');
         Route::post('cardprints/update/{enc_id?}', 'CardPrintController@postUpdate');
         Route::get('cardprints/delete/{enc_id?}', 'CardPrintController@getDelete');
});

Route::group(array('prefix' => 'api', 'middleware' => 'auth', 'as' => 'api.'), function () {
    Route::get('report1', 'Api\\ApiReportsController@report1')->name('reports.report1');
    Route::get('report1totals', 'Api\\ApiReportsController@report1Totals')->name('reports.report1totals');
    Route::get('report4', 'Api\\ApiReportsController@report4')->name('reports.report4');
    Route::get('report4totals', 'Api\\ApiReportsController@report4Totals')->name('reports.report4totals');
    Route::get('report22', 'Api\\ApiReportsController@report22')->name('reports.report22');
    Route::get('report22totals', 'Api\\ApiReportsController@report22Totals')->name('reports.report22totals');
    Route::get('report23', 'Api\\ApiReportsController@report23')->name('reports.report23');
    Route::get('report23totals', 'Api\\ApiReportsController@report23Totals')->name('reports.report23totals');
    Route::get('report24', 'Api\\ApiReportsController@report24')->name('reports.report24');
    Route::get('report24totals', 'Api\\ApiReportsController@report24Totals')->name('reports.report24totals');
    Route::get('report25', 'Api\\ApiReportsController@report25')->name('reports.report25');
    Route::get('report25totals', 'Api\\ApiReportsController@report25Totals')->name('reports.report25totals');
    Route::get('report39', 'Api\\ApiReportsController@report39')->name('reports.report39');
    Route::get('report39totals', 'Api\\ApiReportsController@report39Totals')->name('reports.report39totals');
    Route::get('report40', 'Api\\ApiReportsController@report40')->name('reports.report40');
    Route::get('report40totals', 'Api\\ApiReportsController@report40Totals')->name('reports.report40totals');
    Route::get('report44', 'Api\\ApiReportsController@report44')->name('reports.report44');
    Route::get('report44totals', 'Api\\ApiReportsController@report44Totals')->name('reports.report44totals');
    Route::get('report45', 'Api\\ApiReportsController@report45')->name('reports.report45');
    Route::get('report45totals', 'Api\\ApiReportsController@report45Totals')->name('reports.report45totals');
    Route::get('report46', 'Api\\ApiReportsController@report46')->name('reports.report46');
    Route::get('report46totals', 'Api\\ApiReportsController@report46Totals')->name('reports.report46totals');
    Route::get('report47', 'Api\\ApiReportsController@report47')->name('reports.report47');
    Route::get('report47totals', 'Api\\ApiReportsController@report47Totals')->name('reports.report47totals');
    Route::get('report48', 'Api\\ApiReportsController@report48')->name('reports.report48');
    Route::get('report48totals', 'Api\\ApiReportsController@report48Totals')->name('reports.report48totals');
    Route::get('report50', 'Api\\ApiReportsController@report50')->name('reports.report50');
    Route::get('report50totals', 'Api\\ApiReportsController@report50Totals')->name('reports.report50totals');
    Route::get('report54', 'Api\\ApiReportsController@report54')->name('reports.report54');
    Route::get('report54totals', 'Api\\ApiReportsController@report54Totals')->name('reports.report54totals');
    Route::get('report55', 'Api\\ApiReportsController@report55')->name('reports.report55');
    Route::get('report55totals', 'Api\\ApiReportsController@report55Totals')->name('reports.report55totals');

    Route::get('get-report-stores', 'Api\\ApiReportsController@getStores')->name('reports.partials.stores');
    Route::get('get-report-partners', 'Api\\ApiReportsController@getPartners')->name('reports.partials.partners');
});

 // APIController
 Route::get('api/sendsms/{user1}/{partner_id?}/{sms_type?}/{sms_content?}', 'APIController@getSendsms');
 Route::post('api/authenticate-user-middleware', 'APIController@postAuthenticateUserMiddleware');
 Route::post('api/authenticate-user-reference', 'APIController@postAuthenticateUserReference');
 Route::post('api/authenticate-user', 'APIController@postAuthenticateUser');
 Route::post('api/create-verified-user', 'APIController@postCreateVerifiedUser');
 Route::post('api/verify-user', 'APIController@postVerifyUser');
 Route::post('api/create-draft-user', 'APIController@postCreateDraftUser');
 Route::post('api/update-user/{user_id}', 'APIController@postUpdateUser');
 Route::post('api/update-user-by-reference', 'APIController@postUpdateUserByReference');
 Route::post('api/activate-user', 'APIController@postActivateUser');
 Route::get('api/user/{user_id}', 'APIController@getUser');
 Route::get('api/user-middleware/{user_id}', 'APIController@getUserMiddleware');
 Route::post('api/block-member', 'APIController@postBlockMember');
 Route::get('api/validate-pin/{user_id}/{pin}', 'APIController@getValidatePin');
 Route::get('api/validate-pin-middleware/{user_id}/{pin}', 'APIController@getValidatePinMiddleware');
 Route::get('api/user-balance/{user_id?}', 'APIController@getUserBalance');
 Route::get('api/user-transactions/{user_id?}', 'APIController@getUserTransactions');
 Route::get('api/recover-account/{country_code?}/{mobile?}', 'APIController@getRecoverAccount');
 Route::get('api/recover-account-middleware/{credential?}', 'APIController@getRecoverAccountMiddleware');
 Route::get('api/recover-account-reference/{reference_id}', 'APIController@getRecoverAccountReference');
 Route::post('api/link-user-cards', 'APIController@postLinkUserCards');
 Route::post('api/change-pin/{user_id}', 'APIController@postChangePin');
 Route::get('api/unique-mobile/{mobile}', 'APIController@getUniqueMobile');
 Route::get('api/unique-email/{email}', 'APIController@getUniqueEmail');
 Route::post('api/transfer-points', 'APIController@postTransferPoints');
 Route::post('api/cashback-points', 'APIController@postCashbackPoints');
 Route::post('api/miles-points', 'APIController@postMilesPoints');
 Route::post('api/add-favourite', 'APIController@postAddFavourite');
 Route::get('api/pull-cashback', 'APIController@getPullCashback');
 Route::get('api/cashback-points-test', 'APIController@getCashbackPointsTest');
 Route::get('api/move-to-favorites', 'APIController@getMoveToFavorites');
 Route::get('api/move-to-cart', 'APIController@getMoveToCart');
 Route::get('api/remove-from-favorites', 'APIController@getRemoveFromFavorites');
 Route::get('api/favorites/{user_id?}', 'APIController@getFavorites');
 Route::post('api/add-cart', 'APIController@postAddCart');
 Route::get('api/fetch-user-cart', 'APIController@getFetchUserCart');
 Route::get('api/empty-user-cart', 'APIController@getEmptyUserCart');
 Route::get('api/remove-from-cart', 'APIController@getRemoveFromCart');
 Route::get('api/partner-products/{partner_id}', 'APIController@getPartnerProducts');
 Route::get('api/partner-products-translated/{partner_id}', 'APIController@getPartnerProductsTranslated');
 Route::get('api/fetch-products', 'APIController@getFetchProducts');
 Route::get('api/partner-channels', 'APIController@getPartnerChannels');
 Route::get('api/partner-channels-mobile', 'APIController@getPartnerChannelsMobile');
 Route::get('api/partner-profile', 'APIController@getPartnerProfile');
 Route::get('api/product/{product_id}', 'APIController@getProduct');
 Route::get('api/product-categories', 'APIController@getProductCategories');
 Route::post('api/redeem-items', 'APIController@postRedeemItems');
 Route::get('api/stores', 'APIController@getStores');
 Route::get('api/partner-stores', 'APIController@getPartnerStores');
 Route::get('api/pos', 'APIController@getPos');
 Route::get('api/locales', 'APIController@getLocales');
 Route::get('api/countries', 'APIController@getCountries');
 Route::get('api/areas', 'APIController@getAreas');
 Route::get('api/cities', 'APIController@getCities');
 Route::get('api/currencies', 'APIController@getCurrencies');
 Route::get('api/points-for-spend', 'APIController@getPointsForSpend');
 Route::post('api/void-transaction', 'APIController@postVoidTransaction');
 Route::get('api/banners', 'APIController@getBanners');
 Route::get('api/mobile-banners', 'APIController@getMobileBanners');
 Route::get('api/occupations', 'APIController@getOccupations');
 Route::get('api/country-name', 'APIController@getCountryName');
 Route::get('api/area-name', 'APIController@getAreaName');
 Route::get('api/city-name', 'APIController@getCityName');
 Route::get('api/country', 'APIController@getCountry');
 Route::post('api/travel-transaction/{user_id}', 'APIController@postTravelTransaction');
 Route::post('api/transaction-user/{user_id}', 'APIController@postTransactionUser');
 Route::post('api/customer-data-no-sensitive', 'APIController@postCustomerDataNoSensitive');
 Route::get('api/user-by-mobile/{mobile_number}', 'APIController@getUserByMobile');
 Route::get('api/user-by-card/{card_number}', 'APIController@getUserByCard');
 Route::get('api/user-by-card-om/{card_number}', 'APIController@getUserByCardOm');
 Route::get('api/partner-list', 'APIController@getPartnerList');
 Route::get('api/stores-nearby', 'APIController@getStoresNearby');
 Route::get('api/product-by-category/{category_id}', 'APIController@getProductByCategory');
 Route::post('api/import-file', 'APIController@postImportFile');
 Route::post('api/insert-bulk-balances-om', 'APIController@postInsertBulkBalancesOm');
 Route::post('api/insert-bulk-transactions-om', 'APIController@postInsertBulkTransactionsOm');
 Route::post('api/insert-bulk-customers-om', 'APIController@postInsertBulkCustomersOm');
 Route::get('api/game-settings/{user_id}', 'APIController@getGameSettings');
 Route::post('api/game-points', 'APIController@postGamePoints');
 Route::post('api/mobile-app-installs', 'APIController@postMobileAppInstalls');
 Route::get('api/mobile-app-pushes', 'APIController@getMobileAppPushes');
 Route::get('api/mobile-app-pushes-by-user', 'APIController@getMobileAppPushesByUser');
 Route::get('api/skyscan-location', 'APIController@getSkyscanLocation');
 Route::get('api/skyscan-location-info', 'APIController@getSkyscanLocationInfo');
 Route::post('api/skyscan-results', 'APIController@postSkyscanResults');
 Route::post('api/skyscan-result-info', 'APIController@postSkyscanResultInfo');
 Route::get('api/search-hotels-locations', 'APIController@getSearchHotelsLocations');
 Route::post('api/search-hotel-location', 'APIController@postSearchHotelLocation');
 Route::post('api/search-hotel-pro', 'APIController@postSearchHotelPro');
 Route::get('api/hotel-types', 'APIController@getHotelTypes');
 Route::post('api/hotel-room-info', 'APIController@postHotelRoomInfo');
 Route::post('api/hotel-room-info-hotel-pro', 'APIController@postHotelRoomInfoHotelPro');
 Route::get('api/room-types', 'APIController@getRoomTypes');
 Route::get('api/hotel-pro-details-facilities/{code}', 'APIController@getHotelProDetailsFacilities');
 Route::get('api/facilites/{code}', 'APIController@getFacilites');
 Route::post('api/hotel-pro-details', 'APIController@postHotelProDetails');
 Route::get('api/hotel-pro-details/{code}', 'APIController@getHotelProDetails');
 Route::get('api/hotel-pro-hotel-types', 'APIController@getHotelProHotelTypes');
 Route::get('api/hotel-pro-data/{data_name}/{code?}', 'APIController@getHotelProData');
 Route::post('api/check-provision-hotel-pro/{product_code}', 'APIController@postCheckProvisionHotelPro');
 Route::post('api/send-booking-hotel-pro/{product_code}/{room}', 'APIController@postSendBookingHotelPro');
 Route::post('api/cancel-booking-hotel-pro/{booking_code}', 'APIController@postCancelBookingHotelPro');
 Route::get('api/search-by-destination', 'APIController@getSearchByDestination');
 Route::post('api/get-countries/{code}', 'APIController@postGetCountries');
 Route::get('api/rc-country-list/{language?}', 'APIController@getRcCountryList');
 Route::get('api/rc-city-list/{country}', 'APIController@getRcCityList');
 Route::post('api/rc-location-list', 'APIController@postRcLocationList');
 Route::post('api/rc-get-cars', 'APIController@postRcGetCars');
 Route::post('api/hotel-booking/{user_id}', 'APIController@postHotelBooking');
 Route::post('api/payment-cash/{user_id}', 'APIController@postPaymentCash');
 Route::get('api/language/{lang}', 'APIController@getLanguage');
 Route::get('api/mobile-language/{lang}', 'APIController@getMobileLanguage');
 Route::post('api/image-invoice', 'APIController@postImageInvoice');
 Route::post('api/checkcash-payment', 'APIController@postCheckcashPayment');
 Route::post('api/checkcash-payment-new', 'APIController@postCheckcashPaymentNew');
 Route::get('api/stamp-partners', 'APIController@getStampPartners');
 Route::get('api/stamp-partners-user/{user_id}', 'APIController@getStampPartnersUser');
 Route::get('api/stamp-user-rewards/{user_id}', 'APIController@getStampUserRewards');
 Route::get('api/reward-stamp/{user_id}', 'APIController@getRewardStamp');
 Route::get('api/redeem-stamp/{user_id}', 'APIController@getRedeemStamp');
 Route::get('api/stamp-stores-nearby', 'APIController@getStampStoresNearby');
 Route::get('api/airports-search', 'APIController@getAirportsSearch');
 Route::get('api/airport-by-code', 'APIController@getAirportByCode');
 Route::get('api/carriers-search', 'APIController@getCarriersSearch');
 Route::get('api/send-error', 'APIController@getSendError');
 Route::get('api/send-booking-confirmation', 'APIController@getSendBookingConfirmation');
 Route::get('api/createpending-transaction', 'APIController@getCreatependingTransaction');
 Route::post('api/createpending-transaction', 'APIController@postCreatependingTransaction');
 Route::get('api/changepending-transactionstatus', 'APIController@getChangependingTransactionstatus');
 Route::get('api/pendingtransaction-data', 'APIController@getPendingtransactionData');
 Route::post('api/middleware-customer-data', 'APIController@postMiddlewareCustomerData');
 Route::post('api/middleware-changes-data', 'APIController@postMiddlewareChangesData');
 Route::post('api/middleware-transaction-data', 'APIController@postMiddlewareTransactionData');
 Route::post('api/test-anything', 'APIController@postTestAnything');
 Route::get('api/search-hotels-locations-innstant', 'APIController@getSearchHotelsLocationsInnstant');
 Route::get('api/search-hotels-test-data', 'APIController@getSearchHotelsTestData');
 Route::post('api/search-hotel-location-innstant', 'APIController@postSearchHotelLocationInnstant');
 Route::post('api/hotel-room-info-innstant', 'APIController@postHotelRoomInfoInnstant');
 Route::get('api/booking-info-innstant', 'APIController@getBookingInfoInnstant');
 Route::post('api/hotel-booking-cancellation/{reservation_id}', 'APIController@postHotelBookingCancellation');
 Route::post('api/fares-calendar-results', 'APIController@postFaresCalendarResults');
 Route::post('api/fares-results', 'APIController@postFaresResults');
 Route::post('api/cancel-booking/{record_locator}', 'APIController@postCancelBooking');
 Route::post('api/retrieve-pnr', 'APIController@postRetrievePnr');
 Route::post('api/seats-maps-results', 'APIController@postSeatsMapsResults');
 Route::post('api/book-fare', 'APIController@postBookFare');
 Route::post('api/seats-reservation', 'APIController@postSeatsReservation');
 Route::post('api/flight-terms-conditions', 'APIController@postFlightTermsConditions');
 Route::post('api/transfer-points-reference', 'APIController@postTransferPointsReference');
 Route::post('api/get-rule-information-text', 'APIController@postGetRuleInformationText');
 Route::post('api/payment-methods', 'APIController@postPaymentMethods');
 Route::post('api/boarding-details', 'APIController@postBoardingDetails');
 Route::post('api/new-travel-transaction/{user_id}', 'APIController@postNewTravelTransaction');
 Route::post('api/new-travel-transaction-test/{user_id}', 'APIController@postNewTravelTransactionTest');
 Route::get('api/amount-to-points', 'APIController@getAmountToPoints');
 Route::get('api/middleware-otp', 'APIController@getMiddlewareOtp');
 Route::get('api/member-redemptions', 'APIController@getMemberRedemptions');
 Route::get('api/member-redemptions-new', 'APIController@getMemberRedemptionsNew');
 Route::get('api/member-redemptions-new/{user_id}', 'APIController@getMemberRedemptionsNew');
 Route::post('api/update-user-photo/{user_id}', 'APIController@postUpdateUserPhoto');
 Route::get('api/rate-usd', 'APIController@getRateUsd');
 Route::get('api/cashback-points', 'APIController@getCashbackPoints');
 Route::get('api/update-quantity', 'APIController@getUpdateQuantity');
 Route::post('api/save-login-token', 'APIController@postSaveLoginToken');
 Route::get('api/login-token', 'APIController@getLoginToken');
 Route::get('api/value-in-currency-partner', 'APIController@getValueInCurrencyPartner');
 Route::post('api/check-user-pin/{user_id}', 'APIController@postCheckUserPin');
 Route::get('api/cashback-points-xml', 'APIController@getCashbackPointsXml');
 Route::post('api/middleware-upload-fileblock', 'APIController@postMiddlewareUploadFileblock');
 Route::post('api/middleware-process-file', 'APIController@postMiddlewareProcessFile');
 Route::post('api/middleware-transaction-datafromfile/{transactions}/{is_bonus}/{arr_item_chosen}/{loy_by_partner}/{partners_by_mapping_id}/{partner_prefix}/{network_id}/{arr_currencies}/{arr_transactions_file}', 'APIController@postMiddlewareTransactionDatafromfile');
 Route::post('api/middleware-changes-datafrom-file/{changes}', 'APIController@postMiddlewareChangesDatafromFile');
 Route::post('api/middleware-customer-datafrom-file/{customers}/{send_sensitive?}', 'APIController@postMiddlewareCustomerDatafromFile');
 Route::post('api/email-test', 'APIController@postEmailTest');
 Route::post('api/update-profile-middleware/{user_id}', 'APIController@postUpdateProfileMiddleware');
 Route::post('api/test-anything-magid', 'APIController@postTestAnythingMagid');
 Route::post('api/send-email-json/{json_data}/{subject?}', 'APIController@postSendEmailJson');
 Route::post('api/send-email-json-magid/{json_data}/{subject?}', 'APIController@postSendEmailJsonMagid');
 Route::post('api/send-email-json-rabih/{json_data}/{subject?}', 'APIController@postSendEmailJsonRabih');
 Route::get('api/partner-products-new/{partner_id}', 'APIController@getPartnerProductsNew');
 Route::get('api/product-new/{product_id}', 'APIController@getProductNew');
 Route::post('api/add-cart-new', 'APIController@postAddCartNew');
 Route::get('api/fetch-user-cart-new', 'APIController@getFetchUserCartNew');
 Route::get('api/move-to-cart-new', 'APIController@getMoveToCartNew');
 Route::get('api/favorites-new/{user_id?}', 'APIController@getFavoritesNew');
 Route::post('api/redeem-items-new', 'APIController@postRedeemItemsNew');
 Route::get('api/fetch-products-new', 'APIController@getFetchProductsNew');
 Route::get('api/partner-offers/{partner_id}', 'APIController@getPartnerOffers');
 Route::get('api/offer/{offer_id}', 'APIController@getOffer');
 Route::get('api/user-reference/{user_id}', 'APIController@getUserReference');
 Route::get('api/partner-card-types', 'APIController@getPartnerCardTypes');
 Route::post('api/sms-test', 'APIController@postSmsTest');
 Route::get('api/frequent-flyer-number', 'APIController@getFrequentFlyerNumber');
 Route::get('api/customer-cards', 'APIController@getCustomerCards');
 Route::get('api/redemption-cashback/{partner_id}/{ref_id}/{trx_id}/{vcl_account}/{card_number}/{pts_redeemed}/{pts_value}', 'APIController@getRedemptionCashback');
 Route::get('api/send-test', 'APIController@getSendTest');
 Route::get('api/send-test-sms', 'APIController@getSendTestSms');
 Route::get('api/deduct-transaction-max', 'APIController@getDeductTransactionMax');
 Route::post('api/register-customer', 'APIController@postRegisterCustomer');
 Route::post('api/send-new-sendgrid-email', 'APIController@postSendNewSendgridEmail');
 Route::post('api/create-redemption-order', 'APIController@postCreateRedemptionOrder');
 Route::post('api/send-notifications-transaction', 'APIController@postSendNotificationsTransaction');
 Route::post('api/get-balance-from-credentials', 'APIController@getGetBalanceFromCredentials');
 Route::get('api/generate-middleware-otp', 'APIController@getGenerateMiddlewareOtp');
 Route::get('api/middleware-validate-otp', 'APIController@getMiddlewareValidateOtp');
 Route::post('api/redeem-points-middleware', 'APIController@postRedeemPointsMiddleware');
 Route::post('api/reverse-transaction', 'APIController@getReverseTransaction');
 Route::get('api/alinma-daily', 'APIController@getAlinmaDaily');
 Route::post('api/cashback-points-with-cards', 'APIController@postCashbackPointsWithCards');
 Route::post('api/zero-bluid-users-bsf', 'APIController@postZeroBluidUsersBsf');
 Route::post('api/loop-zero-bluid-users-bsf', 'APIController@postLoopZeroBluidUsersBsf');
 Route::post('api/process-bsf-cashback', 'APIController@postProcessBsfCashback');
 Route::post('api/middleware-upload-and-process-txt-file', 'APIController@postMiddlewareUploadAndProcessFile');

// API2Controller
Route::get('api2/test-anything', 'API2Controller@getTestAnything');
Route::post('api2/import-file', 'API2Controller@postImportFile');
Route::get('api2/test', 'API2Controller@getTest');
Route::get('api2/user-balance-by-id/{user_id}', 'API2Controller@getUserBalanceById');
Route::get('api2/user-balance-by-card/{card}', 'API2Controller@getUserBalanceByCard');
Route::get('api2/test-bsf-reports/{start_date}/{end_date}/{partner_id}', 'API2Controller@getTestBsfReports');
Route::get('api2/bsf-reports/{start_date}/{end_date}/{partner_id}', 'API2Controller@getBsfReports');
Route::get('api2/bsf-reports-customers/{start_date}/{end_date}/{partner_id}', 'API2Controller@getBsfReportsCustomers');
Route::get('api2/bsf-reports2/{start_date}/{end_date}/{partner_id}', 'API2Controller@getBsfReports2');
Route::get('api2/bsf-reports-customers-ralph/{start_date}/{end_date}/{partner_id}', 'API2Controller@getBsfReportsCustomersRalph');
Route::post('api2/reward-points', 'API2Controller@postRewardPoints');
Route::post('api2/redeem-points', 'API2Controller@postRedeemPoints');
Route::post('api2/update-transaction-item-info', 'API2Controller@postUpdateTransactionItemInfo');
Route::post('api2/create-new-user', 'API2Controller@postCreateNewUser');
Route::post('api2/reward-by-product', 'API2Controller@postRewardByProduct');
Route::get('api2/balance-by-reference', 'API2Controller@getBalanceByReference');
Route::post('api2/reward-by-product-json', 'API2Controller@postRewardByProductJson');
Route::post('api2/import-product-json', 'API2Controller@postImportProductJson');
Route::post('api2/import-products-file', 'API2Controller@postImportProductsFile');
Route::get('api2/bsf-reports-accounting-ralph/{start_date}/{end_date}/{partner_id}', 'API2Controller@getBsfReportsAccountingRalph');
Route::get('api2/bsf-reports-transaction-reward/{start_date}/{end_date}/{partner_id}', 'API2Controller@getBsfReportsTransactionReward');
Route::get('api2/miles-extraction', 'API2Controller@getMilesExtraction');
Route::post('api2/migrate-loyalty', 'API2Controller@postMigrateLoyalty');
Route::post('api2/migrate-productpricing', 'API2Controller@postMigrateProductpricing');
Route::get('api2/cashback-file-bisb', 'API2Controller@getCashbackFileBisb');
Route::get('api2/test-loyalty', 'API2Controller@getTestLoyalty');
Route::post('api2/import-filexaaddcciiwd', 'API2Controller@postImportFilexaaddcciiwd');

Route::post('apitest/middleware-upload-file', 'APItestController@postMiddlewareUploadFile');

// APIv2Controller
Route::post('apiv2/initialize-session', 'APIv2Controller@postInitializeSession');
Route::post('apiv2/user-balance', 'APIv2Controller@postUserBalance');
Route::post('apiv2/member-redemptions', 'APIv2Controller@postMemberRedemptions');
Route::post('apiv2/partner-products-new', 'APIv2Controller@postPartnerProductsNew');
Route::post('apiv2/partner-card-types', 'APIv2Controller@postPartnerCardTypes');
Route::post('apiv2/frequent-flyer-number', 'APIv2Controller@postFrequentFlyerNumber');
Route::post('apiv2/transfer-points', 'APIv2Controller@postTransferPoints');
Route::post('apiv2/cashback-points', 'APIv2Controller@postCashbackPoints');
Route::post('apiv2/programs', 'APIv2Controller@postPrograms');
Route::post('apiv2/miles-points', 'APIv2Controller@postMilesPoints');

// AutogeneratedReportController
Route::get('autogeneratedreports/', 'AutogeneratedReportController@getIndex');
Route::get('autogeneratedreports/create', 'AutogeneratedReportController@getCreate');
Route::get('autogeneratedreports/edit/{enc_id?}', 'AutogeneratedReportController@getEdit');
Route::post('autogeneratedreports/update/{enc_id?}', 'AutogeneratedReportController@postUpdate');
Route::get('autogeneratedreports/delete/{enc_id?}', 'AutogeneratedReportController@getDelete');

 // SoapController
 Route::post('soap/test', 'SoapController@postTest');
 Route::post('soap/authenticate-user', 'SoapController@postAuthenticateUser');
 Route::post('soap/create-draft-user', 'SoapController@postCreateDraftUser');
 Route::post('soap/update-user/{user_id}', 'SoapController@postUpdateUser');
 Route::post('soap/activate-user', 'SoapController@postActivateUser');
 Route::get('soap/user/{user_id}', 'SoapController@getUser');
 Route::post('soap/block-member', 'SoapController@postBlockMember');
 Route::get('soap/validate-pin/{user_id}/{pin}', 'SoapController@getValidatePin');
 Route::get('soap/user-balance/{user_id?}', 'SoapController@getUserBalance');
 Route::get('soap/user-transactions/{user_id?}', 'SoapController@getUserTransactions');
 Route::get('soap/recover-account/{country_code?}/{mobile?}', 'SoapController@getRecoverAccount');
 Route::post('soap/link-user-cards', 'SoapController@postLinkUserCards');
 Route::post('soap/change-pin/{user_id}', 'SoapController@postChangePin');
 Route::get('soap/unique-mobile/{mobile}', 'SoapController@getUniqueMobile');
 Route::get('soap/unique-email/{email}', 'SoapController@getUniqueEmail');
 Route::post('soap/transfer-points', 'SoapController@postTransferPoints');
 Route::post('soap/add-favourite', 'SoapController@postAddFavourite');
 Route::get('soap/move-to-favorites', 'SoapController@getMoveToFavorites');
 Route::get('soap/move-to-cart', 'SoapController@getMoveToCart');
 Route::get('soap/remove-from-favorites', 'SoapController@getRemoveFromFavorites');
 Route::get('soap/favorites/{user_id?}', 'SoapController@getFavorites');
 Route::post('soap/add-cart', 'SoapController@postAddCart');
 Route::get('soap/fetch-user-cart', 'SoapController@getFetchUserCart');
 Route::get('soap/empty-user-cart', 'SoapController@getEmptyUserCart');
 Route::get('soap/remove-from-cart', 'SoapController@getRemoveFromCart');
 Route::get('soap/partner-products/{partner_id}', 'SoapController@getPartnerProducts');
 Route::get('soap/fetch-products', 'SoapController@getFetchProducts');
 Route::get('soap/partner-channels', 'SoapController@getPartnerChannels');
 Route::get('soap/partner-profile', 'SoapController@getPartnerProfile');
 Route::get('soap/product/{product_id}', 'SoapController@getProduct');
 Route::get('soap/product-categories', 'SoapController@getProductCategories');
 Route::post('soap/redeem-items', 'SoapController@postRedeemItems');
 Route::get('soap/stores', 'SoapController@getStores');
 Route::get('soap/pos', 'SoapController@getPos');
 Route::get('soap/locales', 'SoapController@getLocales');
 Route::get('soap/countries', 'SoapController@getCountries');
 Route::get('soap/areas', 'SoapController@getAreas');
 Route::get('soap/cities', 'SoapController@getCities');
 Route::get('soap/currencies', 'SoapController@getCurrencies');
 Route::get('soap/points-for-spend', 'SoapController@getPointsForSpend');
 Route::post('soap/void-transaction', 'SoapController@postVoidTransaction');
 Route::get('soap/banners', 'SoapController@getBanners');
 Route::get('soap/occupations', 'SoapController@getOccupations');
 Route::get('soap/country-name', 'SoapController@getCountryName');
 Route::get('soap/area-name', 'SoapController@getAreaName');
 Route::get('soap/city-name', 'SoapController@getCityName');
 Route::get('soap/country', 'SoapController@getCountry');
 Route::post('soap/travel-transaction/{user_id}', 'SoapController@postTravelTransaction');
 Route::post('soap/transaction-user/{user_id}', 'SoapController@postTransactionUser');
 Route::get('soap/user-by-mobile/{mobile_number}', 'SoapController@getUserByMobile');
 Route::get('soap/user-by-card/{card_number}', 'SoapController@getUserByCard');
 Route::get('soap/partner-list', 'SoapController@getPartnerList');
 Route::get('soap/stores-nearby', 'SoapController@getStoresNearby');
 Route::get('soap/product-by-category/{category_id}', 'SoapController@getProductByCategory');
 Route::post('soap/insert-bulk-transactions', 'SoapController@postInsertBulkTransactions');

 // TestController
 Route::get('test/', 'TestController@getIndex');
 Route::post('test/reward', 'TestController@postReward');
 Route::post('test/reward-product', 'TestController@postRewardProduct');
 Route::post('test/redemption', 'TestController@postRedemption');
 Route::get('test/direct', 'TestController@getDirect');
 Route::get('test/test-messages', 'TestController@getTestMessages');
 Route::get('test/test-email', 'TestController@getTestEmail');

 // MobileBarcodeController
 Route::get('mobile_barcode/', 'MobileBarcodeController@getIndex');
 Route::get('mobile_barcode/menu', 'MobileBarcodeController@getMenu');
 Route::get('mobile_barcode/logout', 'MobileBarcodeController@getLogout');
 Route::get('mobile_barcode/settings', 'MobileBarcodeController@getSettings');
 Route::post('mobile_barcode/settings', 'MobileBarcodeController@postSettings');
 Route::get('mobile_barcode/storesforpartner/{partner_id}', 'MobileBarcodeController@getStoresforpartner');
 Route::get('mobile_barcode/posforstore/{store_id}', 'MobileBarcodeController@getPosforstore');
 Route::get('mobile_barcode/scan', 'MobileBarcodeController@getScan');
 Route::get('mobile_barcode/error', 'MobileBarcodeController@getError');
 Route::post('mobile_barcode/scan', 'MobileBarcodeController@postScan');
 Route::get('mobile_barcode/mobile', 'MobileBarcodeController@getMobile');
 Route::post('mobile_barcode/mobile', 'MobileBarcodeController@postMobile');
 Route::get('mobile_barcode/newmemberwithcard/{card_id}', 'MobileBarcodeController@getNewmemberwithcard');
 Route::get('mobile_barcode/newmemberwithmobile/{code}/{mobile}', 'MobileBarcodeController@getNewmemberwithmobile');
 Route::post('mobile_barcode/member/{id}', 'MobileBarcodeController@postMember');
 Route::post('mobile_barcode/showmemberinfo', 'MobileBarcodeController@postShowmemberinfo');
 Route::get('mobile_barcode/txinfo/{member_id}', 'MobileBarcodeController@getTxinfo');
 Route::get('mobile_barcode/omtxinfo/{member_id}', 'MobileBarcodeController@getOmtxinfo');
 Route::post('mobile_barcode/txinfo', 'MobileBarcodeController@postTxinfo');
 Route::post('mobile_barcode/txinfoom', 'MobileBarcodeController@postTxinfoom');
 Route::post('mobile_barcode/txtype', 'MobileBarcodeController@postTxtype');
 Route::get('mobile_barcode/transactions', 'MobileBarcodeController@getTransactions');
 Route::get('mobile_barcode/print-transactions', 'MobileBarcodeController@getPrintTransactions');
 Route::get('mobile_barcode/transaction/{tx_id}', 'MobileBarcodeController@getTransaction');
 Route::get('mobile_barcode/print-transaction/{tx_id}', 'MobileBarcodeController@getPrintTransaction');
 Route::get('mobile_barcode/voidtx/{tx_id}', 'MobileBarcodeController@getVoidtx');
 Route::get('mobile_barcode/configxml', 'MobileBarcodeController@getConfigxml');
 Route::get('mobile_barcode/cordova', 'MobileBarcodeController@getCordova');
 Route::post('mobile_barcode/rewardbyscan', 'MobileBarcodeController@postRewardbyscan');
 Route::get('/config.xml', 'MobileController@getConfigxml');
Route::get('/mobile/config.xml', 'MobileController@getConfigxml');
Route::get('/mobile/cordova.js', 'MobileController@getCordova');
Route::get('/mobile_barcode/cordova.js', 'MobileBarcodeController@getCordova');

// MobileController
Route::get('mobile/', 'MobileController@getIndex');
Route::get('mobile/menu', 'MobileController@getMenu');
Route::get('mobile/logout', 'MobileController@getLogout');
Route::get('mobile/settings', 'MobileController@getSettings');
Route::post('mobile/settings', 'MobileController@postSettings');
Route::get('mobile/storesforpartner/{partner_id}', 'MobileController@getStoresforpartner');
Route::get('mobile/posforstore/{store_id}', 'MobileController@getPosforstore');
Route::get('mobile/scan', 'MobileController@getScan');
Route::get('mobile/error', 'MobileController@getError');
Route::post('mobile/scan', 'MobileController@postScan');
Route::get('mobile/mobile', 'MobileController@getMobile');
Route::post('mobile/mobile', 'MobileController@postMobile');
Route::get('mobile/newmemberwithcard/{card_id}', 'MobileController@getNewmemberwithcard');
Route::get('mobile/newmemberwithmobile/{code}/{mobile}', 'MobileController@getNewmemberwithmobile');
Route::post('mobile/member/{id}', 'MobileController@postMember');
Route::post('mobile/showmemberinfo', 'MobileController@postShowmemberinfo');
Route::get('mobile/txinfo/{member_id}', 'MobileController@getTxinfo');
Route::get('mobile/omtxinfo/{member_id}', 'MobileController@getOmtxinfo');
Route::post('mobile/txinfo', 'MobileController@postTxinfo');
Route::post('mobile/txinfoom', 'MobileController@postTxinfoom');
Route::post('mobile/txtype', 'MobileController@postTxtype');
Route::get('mobile/transactions', 'MobileController@getTransactions');
Route::get('mobile/print-transactions', 'MobileController@getPrintTransactions');
Route::get('mobile/transaction/{tx_id}', 'MobileController@getTransaction');
Route::get('mobile/print-transaction/{tx_id}', 'MobileController@getPrintTransaction');
Route::get('mobile/voidtx/{tx_id}', 'MobileController@getVoidtx');
Route::get('mobile/configxml', 'MobileController@getConfigxml');
Route::get('mobile/cordova', 'MobileController@getCordova');


// APIController
 Route::get('api4/sendsms/{user1}/{partner_id?}/{sms_type?}/{sms_content?}', 'API4Controller@getSendsms');
 Route::post('api4/authenticate-user-middleware', 'API4Controller@postAuthenticateUserMiddleware');
 Route::post('api4/authenticate-user-reference', 'API4Controller@postAuthenticateUserReference');
 Route::post('api4/authenticate-user', 'API4Controller@postAuthenticateUser');
 Route::post('api4/create-verified-user', 'API4Controller@postCreateVerifiedUser');
 Route::post('api4/verify-user', 'API4Controller@postVerifyUser');
 Route::post('api4/create-draft-user', 'API4Controller@postCreateDraftUser');
 Route::post('api4/update-user', 'API4Controller@postUpdateUser');
 Route::post('api4/update-user-by-reference', 'API4Controller@postUpdateUserByReference');
 Route::post('api4/activate-user', 'API4Controller@postActivateUser');
 Route::get('api4/user', 'API4Controller@getUser');
 Route::get('api4/user-middleware', 'API4Controller@getUserMiddleware');
 Route::post('api4/block-member', 'API4Controller@postBlockMember');
 Route::get('api4/validate-pin/{pin}', 'API4Controller@getValidatePin');
 Route::get('api4/validate-pin-middleware/{pin}', 'API4Controller@getValidatePinMiddleware');
 Route::get('api4/user-balance', 'API4Controller@getUserBalance');
 Route::get('api4/user-transactions', 'API4Controller@getUserTransactions');
 Route::get('api4/recover-account/{country_code?}/{mobile?}', 'API4Controller@getRecoverAccount');
 Route::get('api4/recover-account-middleware/{credential?}', 'API4Controller@getRecoverAccountMiddleware');
 Route::post('api4/link-user-cards', 'API4Controller@postLinkUserCards');
 Route::post('api4/change-pin', 'API4Controller@postChangePin');
 Route::get('api4/unique-mobile/{mobile}', 'API4Controller@getUniqueMobile');
 Route::get('api4/unique-email/{email}', 'API4Controller@getUniqueEmail');
 Route::post('api4/transfer-points', 'API4Controller@postTransferPoints');
 Route::post('api4/cashback-points', 'API4Controller@postCashbackPoints');
 Route::post('api4/miles-points', 'API4Controller@postMilesPoints');
 Route::post('api4/add-favourite', 'API4Controller@postAddFavourite');
 Route::get('api4/pull-cashback', 'API4Controller@getPullCashback');
 Route::get('api4/move-to-favorites', 'API4Controller@getMoveToFavorites');
 Route::get('api4/move-to-cart', 'API4Controller@getMoveToCart');
 Route::get('api4/remove-from-favorites', 'API4Controller@getRemoveFromFavorites');
 Route::get('api4/favorites/{user_id?}', 'API4Controller@getFavorites');
 Route::post('api4/add-cart', 'API4Controller@postAddCart');
 Route::get('api4/fetch-user-cart', 'API4Controller@getFetchUserCart');
 Route::get('api4/empty-user-cart', 'API4Controller@getEmptyUserCart');
 Route::get('api4/remove-from-cart', 'API4Controller@getRemoveFromCart');
 Route::get('api4/partner-products/{partner_id}', 'API4Controller@getPartnerProducts');
 Route::get('api4/partner-products-translated/{partner_id}', 'API4Controller@getPartnerProductsTranslated');
 Route::get('api4/fetch-products', 'API4Controller@getFetchProducts');
 Route::get('api4/partner-channels', 'API4Controller@getPartnerChannels');
 Route::get('api4/partner-channels-mobile', 'API4Controller@getPartnerChannelsMobile');
 Route::get('api4/partner-profile', 'API4Controller@getPartnerProfile');
 Route::get('api4/product/{product_id}', 'API4Controller@getProduct');
 Route::get('api4/product-categories', 'API4Controller@getProductCategories');
 Route::post('api4/redeem-items', 'API4Controller@postRedeemItems');
 Route::get('api4/stores', 'API4Controller@getStores');
 Route::get('api4/partner-stores', 'API4Controller@getPartnerStores');
 Route::get('api4/pos', 'API4Controller@getPos');
 Route::get('api4/locales', 'API4Controller@getLocales');
 Route::get('api4/countries', 'API4Controller@getCountries');
 Route::get('api4/areas', 'API4Controller@getAreas');
 Route::get('api4/cities', 'API4Controller@getCities');
 Route::get('api4/currencies', 'API4Controller@getCurrencies');
 Route::get('api4/points-for-spend', 'API4Controller@getPointsForSpend');
 Route::post('api4/void-transaction', 'API4Controller@postVoidTransaction');
 Route::get('api4/banners', 'API4Controller@getBanners');
 Route::get('api4/mobile-banners', 'API4Controller@getMobileBanners');
 Route::get('api4/occupations', 'API4Controller@getOccupations');
 Route::get('api4/country-name', 'API4Controller@getCountryName');
 Route::get('api4/area-name', 'API4Controller@getAreaName');
 Route::get('api4/city-name', 'API4Controller@getCityName');
 Route::get('api4/country', 'API4Controller@getCountry');
 Route::post('api4/travel-transaction', 'API4Controller@postTravelTransaction');
 Route::post('api4/transaction-user', 'API4Controller@postTransactionUser');
 Route::post('api4/customer-data-no-sensitive', 'API4Controller@postCustomerDataNoSensitive');
 Route::get('api4/user-by-mobile/{mobile_number}', 'API4Controller@getUserByMobile');
 Route::get('api4/user-by-card/{card_number}', 'API4Controller@getUserByCard');
 Route::get('api4/user-by-card-om/{card_number}', 'API4Controller@getUserByCardOm');
 Route::get('api4/partner-list', 'API4Controller@getPartnerList');
 Route::get('api4/stores-nearby', 'API4Controller@getStoresNearby');
 Route::get('api4/product-by-category/{category_id}', 'API4Controller@getProductByCategory');
 Route::post('api4/import-file', 'API4Controller@postImportFile');
 Route::post('api4/insert-bulk-balances-om', 'API4Controller@postInsertBulkBalancesOm');
 Route::post('api4/insert-bulk-transactions-om', 'API4Controller@postInsertBulkTransactionsOm');
 Route::post('api4/insert-bulk-customers-om', 'API4Controller@postInsertBulkCustomersOm');
 Route::get('api4/game-settings', 'API4Controller@getGameSettings');
 Route::post('api4/game-points', 'API4Controller@postGamePoints');
 Route::post('api4/mobile-app-installs', 'API4Controller@postMobileAppInstalls');
 Route::get('api4/mobile-app-pushes', 'API4Controller@getMobileAppPushes');
 Route::get('api4/mobile-app-pushes-by-user', 'API4Controller@getMobileAppPushesByUser');
 Route::get('api4/skyscan-location', 'API4Controller@getSkyscanLocation');
 Route::get('api4/skyscan-location-info', 'API4Controller@getSkyscanLocationInfo');
 Route::post('api4/skyscan-results', 'API4Controller@postSkyscanResults');
 Route::post('api4/skyscan-result-info', 'API4Controller@postSkyscanResultInfo');
 Route::get('api4/search-hotels-locations', 'API4Controller@getSearchHotelsLocations');
 Route::post('api4/search-hotel-location', 'API4Controller@postSearchHotelLocation');
 Route::post('api4/search-hotel-pro', 'API4Controller@postSearchHotelPro');
 Route::get('api4/hotel-types', 'API4Controller@getHotelTypes');
 Route::post('api4/hotel-room-info', 'API4Controller@postHotelRoomInfo');
 Route::post('api4/hotel-room-info-hotel-pro', 'API4Controller@postHotelRoomInfoHotelPro');
 Route::get('api4/room-types', 'API4Controller@getRoomTypes');
 Route::get('api4/hotel-pro-details-facilities/{code}', 'API4Controller@getHotelProDetailsFacilities');
 Route::get('api4/facilites/{code}', 'API4Controller@getFacilites');
 Route::post('api4/hotel-pro-details', 'API4Controller@postHotelProDetails');
 Route::get('api4/hotel-pro-details/{code}', 'API4Controller@getHotelProDetails');
 Route::get('api4/hotel-pro-hotel-types', 'API4Controller@getHotelProHotelTypes');
 Route::get('api4/hotel-pro-data/{data_name}/{code?}', 'API4Controller@getHotelProData');
 Route::post('api4/check-provision-hotel-pro/{product_code}', 'API4Controller@postCheckProvisionHotelPro');
 Route::post('api4/send-booking-hotel-pro/{product_code}/{room}', 'API4Controller@postSendBookingHotelPro');
 Route::post('api4/cancel-booking-hotel-pro/{booking_code}', 'API4Controller@postCancelBookingHotelPro');
 Route::get('api4/search-by-destination', 'API4Controller@getSearchByDestination');
 Route::post('api4/get-countries/{code}', 'API4Controller@postGetCountries');
 Route::get('api4/rc-country-list/{language?}', 'API4Controller@getRcCountryList');
 Route::get('api4/rc-city-list/{country}', 'API4Controller@getRcCityList');
 Route::post('api4/rc-location-list', 'API4Controller@postRcLocationList');
 Route::post('api4/rc-get-cars', 'API4Controller@postRcGetCars');
 Route::post('api4/hotel-booking', 'API4Controller@postHotelBooking');
 Route::post('api4/payment-cash', 'API4Controller@postPaymentCash');
 Route::get('api4/language/{lang}', 'API4Controller@getLanguage');
 Route::get('api4/mobile-language/{lang}', 'API4Controller@getMobileLanguage');
 Route::post('api4/image-invoice', 'API4Controller@postImageInvoice');
 Route::post('api4/checkcash-payment', 'API4Controller@postCheckcashPayment');
 Route::post('api4/checkcash-payment-new', 'API4Controller@postCheckcashPaymentNew');
 Route::get('api4/stamp-partners', 'API4Controller@getStampPartners');
 Route::get('api4/stamp-partners-user', 'API4Controller@getStampPartnersUser');
 Route::get('api4/stamp-user-rewards', 'API4Controller@getStampUserRewards');
 Route::get('api4/reward-stamp', 'API4Controller@getRewardStamp');
 Route::get('api4/redeem-stamp', 'API4Controller@getRedeemStamp');
 Route::get('api4/stamp-stores-nearby', 'API4Controller@getStampStoresNearby');
 Route::get('api4/airports-search', 'API4Controller@getAirportsSearch');
 Route::get('api4/airport-by-code', 'API4Controller@getAirportByCode');
 Route::get('api4/carriers-search', 'API4Controller@getCarriersSearch');
 Route::get('api4/send-error', 'API4Controller@getSendError');
 Route::get('api4/send-booking-confirmation', 'API4Controller@getSendBookingConfirmation');
 Route::get('api4/createpending-transaction', 'API4Controller@getCreatependingTransaction');
 Route::post('api4/createpending-transaction', 'API4Controller@postCreatependingTransaction');
 Route::get('api4/changepending-transactionstatus', 'API4Controller@getChangependingTransactionstatus');
 Route::get('api4/pendingtransaction-data', 'API4Controller@getPendingtransactionData');
 Route::post('api4/middleware-customer-data', 'API4Controller@postMiddlewareCustomerData');
 Route::post('api4/middleware-changes-data', 'API4Controller@postMiddlewareChangesData');
 Route::post('api4/middleware-transaction-data', 'API4Controller@postMiddlewareTransactionData');
 Route::post('api4/test-anything', 'API4Controller@postTestAnything');
 Route::get('api4/search-hotels-locations-innstant', 'API4Controller@getSearchHotelsLocationsInnstant');
 Route::get('api4/search-hotels-test-data', 'API4Controller@getSearchHotelsTestData');
 Route::post('api4/search-hotel-location-innstant', 'API4Controller@postSearchHotelLocationInnstant');
 Route::post('api4/hotel-room-info-innstant', 'API4Controller@postHotelRoomInfoInnstant');
 Route::get('api4/booking-info-innstant', 'API4Controller@getBookingInfoInnstant');
 Route::post('api4/hotel-booking-cancellation/{reservation_id}', 'API4Controller@postHotelBookingCancellation');
 Route::post('api4/fares-calendar-results', 'API4Controller@postFaresCalendarResults');
 Route::post('api4/fares-results', 'API4Controller@postFaresResults');
 Route::post('api4/cancel-booking/{record_locator}', 'API4Controller@postCancelBooking');
 Route::post('api4/retrieve-pnr', 'API4Controller@postRetrievePnr');
 Route::post('api4/seats-maps-results', 'API4Controller@postSeatsMapsResults');
 Route::post('api4/book-fare', 'API4Controller@postBookFare');
 Route::post('api4/seats-reservation', 'API4Controller@postSeatsReservation');
 Route::post('api4/flight-terms-conditions', 'API4Controller@postFlightTermsConditions');
 Route::post('api4/transfer-points-reference', 'API4Controller@postTransferPointsReference');
 Route::post('api4/get-rule-information-text', 'API4Controller@postGetRuleInformationText');
 Route::post('api4/payment-methods', 'API4Controller@postPaymentMethods');
 Route::post('api4/boarding-details', 'API4Controller@postBoardingDetails');
 Route::post('api4/new-travel-transaction', 'API4Controller@postNewTravelTransaction');
 Route::post('api4/new-travel-transaction-test', 'API4Controller@postNewTravelTransactionTest');
 Route::get('api4/amount-to-points', 'API4Controller@getAmountToPoints');
 Route::get('api4/middleware-otp', 'API4Controller@getMiddlewareOtp');
 Route::get('api4/member-redemptions', 'API4Controller@getMemberRedemptions');
 Route::get('api4/member-redemptions-new', 'API4Controller@getMemberRedemptionsNew');
 Route::get('api4/member-redemptions-new', 'API4Controller@getMemberRedemptionsNew');
 Route::post('api4/update-user-photo', 'API4Controller@postUpdateUserPhoto');
 Route::get('api4/rate-usd', 'API4Controller@getRateUsd');
 Route::get('api4/cashback-points', 'API4Controller@getCashbackPoints');
 Route::get('api4/update-quantity', 'API4Controller@getUpdateQuantity');
 Route::post('api4/save-login-token', 'API4Controller@postSaveLoginToken');
 Route::get('api4/login-token', 'API4Controller@getLoginToken');
 Route::get('api4/value-in-currency-partner', 'API4Controller@getValueInCurrencyPartner');
 Route::post('api4/check-user-pin', 'API4Controller@postCheckUserPin');
 Route::get('api4/cashback-points-xml', 'API4Controller@getCashbackPointsXml');
 Route::post('api4/middleware-upload-fileblock', 'API4Controller@postMiddlewareUploadFileblock');
 Route::post('api4/middleware-process-file', 'API4Controller@postMiddlewareProcessFile');
 Route::post('api4/middleware-transaction-datafromfile/{transactions}/{is_bonus}/{arr_item_chosen}/{loy_by_partner}/{partners_by_mapping_id}/{partner_prefix}/{network_id}/{arr_currencies}/{arr_transactions_file}', 'API4Controller@postMiddlewareTransactionDatafromfile');
 Route::post('api4/middleware-changes-datafrom-file/{changes}', 'API4Controller@postMiddlewareChangesDatafromFile');
 Route::post('api4/middleware-customer-datafrom-file/{customers}/{send_sensitive?}', 'API4Controller@postMiddlewareCustomerDatafromFile');
 Route::post('api4/email-test', 'API4Controller@postEmailTest');
 Route::post('api4/update-profile-middleware', 'API4Controller@postUpdateProfileMiddleware');
 Route::post('api4/test-anything-magid', 'API4Controller@postTestAnythingMagid');
 Route::post('api4/send-email-json/{json_data}/{subject?}', 'API4Controller@postSendEmailJson');
 Route::post('api4/send-email-json-magid/{json_data}/{subject?}', 'API4Controller@postSendEmailJsonMagid');
 Route::post('api4/send-email-json-rabih/{json_data}/{subject?}', 'API4Controller@postSendEmailJsonRabih');
 Route::get('api4/partner-products-new/{partner_id}', 'API4Controller@getPartnerProductsNew');
 Route::get('api4/product-new/{product_id}', 'API4Controller@getProductNew');
 Route::post('api4/add-cart-new', 'API4Controller@postAddCartNew');
 Route::get('api4/fetch-user-cart-new', 'API4Controller@getFetchUserCartNew');
 Route::get('api4/move-to-cart-new', 'API4Controller@getMoveToCartNew');
 Route::get('api4/favorites-new/{user_id?}', 'API4Controller@getFavoritesNew');
 Route::post('api4/redeem-items-new', 'API4Controller@postRedeemItemsNew');
 Route::get('api4/fetch-products-new', 'API4Controller@getFetchProductsNew');
 Route::get('api4/partner-offers/{partner_id}', 'API4Controller@getPartnerOffers');
 Route::get('api4/offer/{offer_id}', 'API4Controller@getOffer');
 Route::get('api4/user-reference', 'API4Controller@getUserReference');
 Route::get('api4/partner-card-types', 'API4Controller@getPartnerCardTypes');
 Route::post('api4/sms-test', 'API4Controller@postSmsTest');
 Route::get('api4/frequent-flyer-number', 'API4Controller@getFrequentFlyerNumber');
 Route::get('api4/customer-cards', 'API4Controller@getCustomerCards');
 Route::get('api4/redemption-cashback/{partner_id}/{ref_id}/{trx_id}/{vcl_account}/{card_number}/{pts_redeemed}/{pts_value}', 'API4Controller@getRedemptionCashback');
 Route::get('api4/send-test', 'API4Controller@getSendTest');
 Route::get('api4/send-test-sms', 'API4Controller@getSendTestSms');
 Route::get('api4/deduct-transaction-max', 'API4Controller@getDeductTransactionMax');
 Route::post('api4/register-customer', 'API4Controller@postRegisterCustomer');
 Route::post('api4/send-new-sendgrid-email', 'API4Controller@postSendNewSendgridEmail');
 Route::post('api4/create-redemption-order', 'API4Controller@postCreateRedemptionOrder');
 Route::post('api4/send-notifications-transaction', 'API4Controller@postSendNotificationsTransaction');
 Route::post('api4/get-balance-from-credentials', 'API4Controller@getGetBalanceFromCredentials');
 Route::get('api4/generate-middleware-otp', 'API4Controller@getGenerateMiddlewareOtp');
 Route::get('api4/middleware-validate-otp', 'API4Controller@getMiddlewareValidateOtp');
 Route::post('api4/redeem-points-middleware', 'API4Controller@postRedeemPointsMiddleware');
 Route::post('api4/reverse-transaction', 'API4Controller@getReverseTransaction');
 Route::get('api4/alinma-daily', 'API4Controller@getAlinmaDaily');
 Route::get('api4/cashback-points-test', 'API4Controller@getCashbackPointsTest');