<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(array('prefix' => 'bsf-middleware'), function () {
    Route::post('upload-change-file', 'BsfMiddlewareController@postUploadFileChange');
    Route::post('simulation/receive-change-file', 'BsfMiddlewareController@postReceiveFileChange');
    Route::get('get-all-users', 'BsfMiddlewareController@getAllUsers');
    Route::get('get-all-user-ids', 'BsfMiddlewareController@getAllUserIds');
    Route::post('get-users-by-id', 'BsfMiddlewareController@getUsersById');
    Route::get('get-range-of-users', 'BsfMiddlewareController@getRangeOfUsers');
    Route::get('get-users-with-different-pins', 'BsfMiddlewareController@getUsersWithDifferentPinCodes');
    Route::get('update-users-with-different-pins', 'BsfMiddlewareController@updateUsersWithDifferentPinCodes');
});


Route::post('git-update', 'GitUpdateController@index');
