<?php
namespace App;
use App\BluCollection;
use Eloquent;
/**
 * Transaction Item Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Ralph Nader <ralph@bluloyalty.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class TransactionDeduction extends Eloquent 
{
    public $table = 'transaction_deduction';

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------
    public function transaction() 
    { 
            return $this->belongsTo('App\Transaction'); 
    }   

} // EOC