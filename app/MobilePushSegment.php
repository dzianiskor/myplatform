<?php
namespace App;
use App\BluCollection;
use Eloquent;
/**
 * Notification Segment Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Magid Mroueh <magid.mroueh@bluloyalty.com>
 * @copyright  2017 BLU
 * @link       http://blupoints.com
 */
class MobilePushSegment extends Eloquent 
{
    public $table = 'mobile_push_notification_segment';
    public $timestamps = false;
 
} // EOC