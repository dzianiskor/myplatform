<?php
namespace App;
use App\BluCollection;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * E-Statement Model
 *
 * @category   Models
 * @package    BLU
 * @author     Magid Mroueh <magid.mroueh@bluloyalty.com>
 * @copyright  2017 BLU
 * @link       http://blupoints.com
 */
class Estatement extends BaseModel
{
    public $table         = 'estatement';
    protected $guarded    = array();
    protected $dates      = ['deleted_at'];
    protected $softDelete = true;

    # --------------------------------------------------------------------------
    #
    # Relationships
    #
    # --------------------------------------------------------------------------

    public function estatementtranslations()
    {
        return $this->hasMany('App\Estatementtranslation');
    }
    public function segments()
    {
        return $this->belongsToMany('App\Segment');
    }
    
    public function tiers()
    {
        return $this->belongsToMany('App\Tiercriteria', 'tier_estatement');
    }

    public function countries()
    {
        return $this->belongsToMany('App\Country');
    }
    
    public function users()   
    { 
        return $this->belongsToMany('App\User'); 
    }

} // EOC