<?php
namespace App;
use App\BluCollection;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Offer Model
 *
 * @category   Models
 * @package    BLU
 * @author     Magid Mroueh <magid.mroueh@bluloyalty.com>
 * @copyright  2017 BLU
 * @link       http://blupoints.com
 */
class Offer extends BaseModel
{
    public $table         = 'offer';
    protected $guarded    = array();
    protected $dates      = ['deleted_at'];
    protected $softDelete = true;

    # --------------------------------------------------------------------------
    #
    # Relationships
    #
    # --------------------------------------------------------------------------

    public function offertranslations()
    {
        return $this->hasMany('App\Offertranslation')->where('draft', false);
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function partners()
    {
        return $this->belongsToMany('App\Partner', 'partner_offer');
    }

    public function media()
    {
        return $this->belongsToMany('App\Media');
    }

    public function segments()
    {
        return $this->belongsToMany('App\Segment');
    }

    public function tiers()
    {
        return $this->belongsToMany('App\Tier', 'tier_offer');
    }

    public function countries()
    {
        return $this->belongsToMany('App\Country');
    }

    public function channels()
    {
        return $this->hasMany('App\OfferChannel', 'offer_id');
    }

    public function rewardTypes() {
        return $this->belongsTo('RewardTypes', 'reward_type');
    }

    # --------------------------------------------------------------------------
    #
    # Attributes
    #
    # --------------------------------------------------------------------------

    /**
     * Return a top level partner for an offer
     *
     * @return void
     */
    public function topLevelPartner()
    {
        return $this->partners()->first();
    }

} // EOC