<?php
namespace App;
/**
 * Banner Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class Localizationkey extends BaseModel
{
	public $table = 'localization_keys';
} // EOC