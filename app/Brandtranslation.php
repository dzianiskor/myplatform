<?php
namespace App;
use App\BluCollection;
use Eloquent;
/**
 * Store Model
 * 
 * @brand   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class Brandtranslation extends Eloquent 
{
    public $table = 'brand_translation';

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------
    
    public function brand()
    {
        return $this->belongsTo('App\Brand');
    }

    

    # --------------------------------------------------------------------------
    # 
    # Helpers
    # 
    # --------------------------------------------------------------------------
    
    /**
     * Return a list of stores for the specified partner
     * 
     * @param int $brand_id
     * @return array
     */
    public static function brandTranslations($brand_id)     
    { 
        return Brandtranslation::where('brand_id', $brand_id)->orderBy('name')->get();
    }

} // EOC