<?php
namespace App;
use App\BluCollection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\SoftDeletes;
use View;
use Config;
use I;
use App\Http\Controllers\NotificationController as NotificationController;

/**
 * Product Model
 *
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class Product extends BaseModel
{
    public $table         = 'product';
    protected $guarded    = array();
    protected $dates      = ['deleted_at'];
    protected $softDelete = true;

    # --------------------------------------------------------------------------
    #
    # Relationships
    #
    # --------------------------------------------------------------------------

    public function prodtranslations()
    {
        return $this->hasMany('App\Prodtranslation')->where('draft', false);
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function brand()
    {
        return $this->belongsTo('App\Brand');
    }

    public function partners()
    {
        return $this->belongsToMany('App\Partner');
    }
    
    public function media()
    {
        return $this->belongsToMany('App\Media');
    }

    public function networks()
    {
        return $this->belongsToMany('App\Network');
    }

    public function segments()
    {
        return $this->belongsToMany('App\Segment');
    }


    public function redemptionSegments()
    {
        return $this->hasMany(ProductRedemptionSegments::class);
    }

    public function countries()
    {
        return $this->belongsToMany('App\Country');
    }

    public function channels()
    {
        return $this->hasMany('App\ProductChannel', 'product_id');
    }
    
    public function ProductPartnerRedemption()
    {
        return $this->hasMany('App\ProductPartnerRedemption', 'product_id');
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class);
    }

    public function deliveryCurrency()
    {
        return $this->belongsTo(Currency::class, 'delivery_currency_id');
    }
    
    public function ProductPartnerReward()
    {
        return $this->hasMany('App\ProductPartnerReward', 'product_id');
    }
    
    public function upcs()
    {
        return $this->hasMany('App\ProductUpc', 'product_id');
    }

    # --------------------------------------------------------------------------
    #
    # Attributes
    #
    # --------------------------------------------------------------------------

    /**
     * Return a top level partner for a product
     *
     * @return void
     */
    public function topLevelPartner()
    {
        return $this->partners()->first();
    }
    
    /**
     * Send a stock warning notification to BLU admins
     *
     * @return void
     */
    public function sendStockLevelWarning()
    {

        $email_template = View::make("emails.stock-warning", array(
            'product' => $this
        ))->render();

        NotificationController::sendEmailNotification($email_template, 'BLU Points', Config::get('blu.email_from'), array(null), array(Config::get('blu.admin_email')), "[BLU] Stock Level Warning");
    }
    /**
     * Return a top level partner for a product
     *
     * @return void
     */
    public function getTopLevelPartner()
    {
        $firstLevelChildren		= array();
        $highestLevelParents	= array();
        $sameLevelPartners		= array();
        $partners = array();
        foreach ($this->partners as $partner){
                $partners[] = $partner->id;
        }

        foreach ($partners as $partner_id){
                $partner	= Partner::find($partner_id);
                $partnerParent	= $partner->parent_id;
                if($partner->is_top_level_partner){
                    return Partner::find($partner_id);
                }elseif(empty($partnerParent)){
                 return Partner::find($partner_id);
                }else {
                    if(in_array($partnerParent, $partners)){
                       $highestLevelParents[$partnerParent][] = $partner_id;
//				$firstLevelParents[] = $partner_id;
                    }
                    if($partnerParent == 1){
                            $firstLevelChildren[]	= $partner_id;
                    }else{
                            $sameLevelPartners[]	= $partner_id;
                    }
           }
        }

        if(!empty($firstLevelChildren)){
           sort($firstLevelChildren);
           return Partner::find($firstLevelChildren[0]);
        }
        elseif(!empty($highestLevelParents)){
           $highestParents	= array_keys($highestLevelParents);
           sort($highestParents);
           return Partner::find($highestParents[0]);
        }
        elseif(!empty($sameLevelPartners)){
           sort($sameLevelPartners);
           return Partner::find($sameLevelPartners[0]);
        }
        else{
            return null;
        }
    }
    /**
     * Return a top level partner for a product
     *
     * @return void
     */
    public function getDisplayTopLevelPartner()
    {
        $firstLevelChildren		= array();
        $highestLevelParents	= array();
        $sameLevelPartners		= array();
        $partners = array();
        foreach ($this->partners as $partner){
                $partners[] = $partner->id;
        }
        $adminPartners = Auth::user()->managedPartners();
        $adminPartnerIds = array();
        foreach ($adminPartners as $adminPartner){
            $adminPartnerIds[] = $adminPartner->id;
        }
        sort($adminPartnerIds);

        foreach($adminPartnerIds as $adminPartnerId){
            if(in_array($adminPartnerId, $partners) && !I::belong(1)){
                return Partner::find($adminPartnerId);
            }
        }
        
        foreach ($partners as $partner_id){
                $partner	= Partner::find($partner_id);
                $partnerParent	= $partner->parent_id;
                if(Auth::User()->managedPartners()->contains($partner)){
                    
                }
                if($partner->is_top_level_partner){
                    if(I::belong(1) && $partner_id == 1 && count($partners) > 1){
                        continue;
                    }
                    return Partner::find($partner_id);
                }elseif(empty($partnerParent)){
                    return Partner::find($partner_id);
                }else {
                    if(in_array($partnerParent, $partners)){
                       $highestLevelParents[$partnerParent][] = $partner_id;
//				$firstLevelParents[] = $partner_id;
                    }
                    if($partnerParent == 1){
                            $firstLevelChildren[]	= $partner_id;
                    }else{
                            $sameLevelPartners[]	= $partner_id;
                    }
           }
        }

        if(!empty($firstLevelChildren)){
           sort($firstLevelChildren);
           return Partner::find($firstLevelChildren[0]);
        }
        elseif(!empty($highestLevelParents)){
           $highestParents	= array_keys($highestLevelParents);
           sort($highestParents);
           return Partner::find($highestParents[0]);
        }
        elseif(!empty($sameLevelPartners)){
           sort($sameLevelPartners);
           return Partner::find($sameLevelPartners[0]);
        }
        else{
            return null;
        }
    }

    public function translate($language)
    {
        if ($this->prodtranslations && $this->prodtranslations->contains('lang_id', $language->id)) {
            $prodTranslation = $this->prodtranslations->firstWhere('lang_id', $language->id);
            $this->name = $prodTranslation->name;
            $this->model = $prodTranslation->model;
            $this->sub_model = $prodTranslation->sub_model;
            $this->description = $prodTranslation->description;
        }

        return $this;
    }

} // EOC