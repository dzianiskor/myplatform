<?php
namespace App;
use App\BluCollection;
use Eloquent;
/**
 * Gateway Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class Gateway extends Eloquent 
{
    public $table = 'gateway';
    
} // EOC