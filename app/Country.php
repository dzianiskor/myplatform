<?php
namespace App;
/**
 * Country Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class Country extends BaseModel 
{
    public $table      = 'country';
    public $timestamps = false;
    protected $guarded = array();

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------
    
    public function products() 
    { 
        return $this->belongsToMany('App\Product'); 
    }

    public function areas() 
    { 
        return $this->hasMany('App\Area');          
    }

    public static function orderedList()
    {
        return Country::orderBy('name')->get();
    }

} // EOC