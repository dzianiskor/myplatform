<?php
namespace App;
use App\BluCollection;
use Eloquent;
/**
 * Transaction Item Delivery Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Firas Boukarroum <firas.boukarroum@bluloyalty.com>
 * @copyright  2016 BLU
 * @link       http://blupoints.com
 */
class TransactionItemDelivery extends Eloquent 
{
    public $table = 'transaction_item_delivery';

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------
    
    public function transaction_item() 
    { 
        return $this->belongsTo('App\TransactionItem'); 
    }   

} // EOC