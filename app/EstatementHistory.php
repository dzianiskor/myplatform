<?php
namespace App;
use App\BluCollection;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * E-Statement Model
 *
 * @category   Models
 * @package    BLU
 * @author     Magid Mroueh <magid.mroueh@bluloyalty.com>
 * @copyright  2017 BLU
 * @link       http://blupoints.com
 */
class EstatementHistory extends BaseModel
{
    public $table         = 'estatement_history';

    # --------------------------------------------------------------------------
    #
    # Relationships
    #
    # --------------------------------------------------------------------------

} // EOC