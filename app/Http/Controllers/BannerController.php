<?php
namespace App\Http\Controllers;
use App\Media;
use View;
use Input;
use Validator;
use I;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Language as Language;
use App\Banner as Banner;
use App\BannerCountry as BannerCountry;
use App\BannerPage as BannerPage;
use App\BannerSegment as BannerSegment;
use ManagedObjectsHelper;
use App\Country;
use Meta;
use Illuminate\Support\Facades\DB;
use BannerHelper;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Banner Controller
 *
 * @category   Controllers
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class BannerController extends BaseController
{
    protected $layout = "layouts.dashboard";

    /**
     * Fetch the contextual lists
     *
     * @return array
     */
    private function getAllLists() {
        return array(
            'partners' => ManagedObjectsHelper::managedPartners()->sortBy('name')->pluck('name', 'id'),
            'languages' => Language::orderBy('name')->pluck('name','id'),
            'countries' => Country::orderBy('name')->pluck('name', 'id'),
            'segments' => ManagedObjectsHelper::managedSegments()->orderBy('name')->pluck('name', 'id'),
            'statuses' => Meta::bannerStatuses()
        );
    }

    /**
     * Display a list of the banners
     *
     * @return void
     */
    public function getIndex() {
        if (!I::can('view_banners')) {
            return Redirect::to(url("dashboard"));
        }

        $banners = Banner::where('draft', false)->orderBy('created_at');

        if (Input::has('partner_banner_list') && Input::get('partner_banner_list') != '') {
            $partners = explode(',', Input::get('partner_banner_list'));
        } else {
            $partners = Auth::User()->managedPartnerIDs();
        }

        $banners = $banners->whereHas('Partner', function ($q) use ($partners) {
            $q->whereIn('partner_id', $partners);
        });

        if (Input::has('mobile_status_banner_list') && Input::get('mobile_status_banner_list') != '') {
            $q1 = explode(',', Input::get('mobile_status_banner_list'));
            $banners = $banners->whereIn('mob_status', $q1);
        }

        if(Input::has('status_banner_list') && Input::get('status_banner_list') != '') {
            $q1 = explode(',', Input::get('status_banner_list'));
            $banners = $banners->whereIn('status', $q1);
        }

        if (Input::has('q') && Input::get('q') != '') {
            $banners = $banners->where('name', 'like', '%'.self::sanitizeText(Input::get('q')).'%');
        }

        $banners = $banners->paginate(15);

        return View::make("banner.list", [
            'banners' => $banners->appends(Input::except('page')),
            'lists' => [
                'partners' => ManagedObjectsHelper::managedPartners()->sortBy('name')->pluck('name', 'id'),
                'statuses' => Meta::bannerStatuses()
            ]
        ]);
    }

    /**
   	 * Create a new banner.
   	 *
   	 * @return Response
   	 */
    public function getCreate() {
        if (!I::can('create_banners')) {
                return Redirect::to(url("dashboard"));
            }
        $first_partner = array_values(ManagedObjectsHelper::managedPartners()->pluck('id')->toArray())[0];

		// Create banner

        $banner = new Banner();

        $banner->partner_id = $first_partner;
        $banner->save();

		// Automatically associate to page

		$pages = array();

        foreach (BannerHelper::availablePages() as $page) {
			$pages[] = array(
                'page' => $page,
				'banner_id' => $banner->id
			);
		}

		DB::table('banner_page')->insert($pages);

		// Associate worldwide
		$bc = new BannerCountry();

//		$bc->country_id = 0;
        $bc->banner_id = $banner->id;
		$bc->save();
                $bannerId = base64_encode($banner->id);
        return Redirect::to(url("dashboard/banners/edit/{$bannerId}"));
   	}

    /**
   	 * Edit an existing banner
   	 *
   	 * @return Response
   	 */
    public function getEdit($enc_id = null)
    {
        $id = base64_decode($enc_id);
        if (!I::can('view_banners')) {
            return Redirect::to(url("dashboard"));
        }
        $banner = Banner::find($id);

        if (!$banner) {
           App::abort(404);
        }
        $managedPartners = Auth::User()->managedPartners();
        $can_view_banner = false;
        foreach ($managedPartners as $manP) {
            if ($manP->id == $banner->partner_id) {
                $can_view_banner = true;
                break;
            }
        }
        if ($can_view_banner == false && $banner->draft == false) {
            return Redirect::to(url("dashboard"));
        }

        return View::make("banner.view", array_merge(
           array('banner' => $banner), $this->getAllLists()
        ));
   	}

    /**
     * Update a banner
     *
     * @param int $id
     * @return Response
     */
    public function postUpdate($enc_id = null) {
        $id = base64_decode($enc_id);
        if (!I::can('create_banners') && !I::can('edit_banners')) {
            return Redirect::to(url("dashboard"));
        }
        $banner = Banner::find($id);

        if (!$banner) {
            App::abort(404);
        }
        $input = Input::all();
        foreach ($input as $k => $i) {
            $input[$k] = self::sanitizeText($i);
        }
        $validator = Validator::make($input, array(
            'name' => 'required|min:2',
        ));

        if ($validator->fails()) {
            return Redirect::to(url("dashboard/banners/view/$enc_id"))->withErrors($validator);
        }

        $banner->name = self::sanitizeText(Input::get('name'));
        $banner->description = self::sanitizeText(Input::get('description'));
        $banner->title = self::sanitizeText(Input::get('title'));
        $banner->status = self::sanitizeText(Input::get('status'));
        $banner->link = self::sanitizeText(Input::get('link'));
        $banner->mob_status = self::sanitizeText(Input::get('mob_status'));
        $banner->mob_link = self::sanitizeText(Input::get('mob_link'));
        $banner->partner_id = self::sanitizeText(Input::get('partner_id'));
        $banner->draft = false;
        $banner->lang_id = self::sanitizeText(Input::get('lang_id'));

        $banner->logged_in = self::sanitizeText(Input::get('logged_in', 0));
        $banner->logged_out = self::sanitizeText(Input::get('logged_out', 0));

        if ($banner->logged_in != 1 && $banner->logged_out != 1) { //check if not selected segments
			return Redirect::to(url("dashboard/banners/edit/$enc_id"))->withErrors(['you have to choose if this banner is for logged in/out users.'])->withInput();
		}

        if ($banner->logged_in == 1 && count($banner->segments) < 1) { //check if not selected segments
			return Redirect::to(url("dashboard/banners/edit/$enc_id"))->withErrors(['you have to select at least one segment if the banner is for Logged In users.'])->withInput();
		}

        if ($banner->status == 'Online' && !Input::hasFile('image') && !$banner->image()) {
			return Redirect::to(url("dashboard/banners/edit/$enc_id"))->withErrors(["The banner status is 'Online' and no image uploaded."])->withInput();
		}

        if ($banner->mob_status == 'Online' && !Input::hasFile('mob_image') && !$banner->mobimage()) {
			return Redirect::to(url("dashboard/banners/edit/$enc_id"))->withErrors(["The banner mobile status is 'Online' and no image uploaded."])->withInput();
		}

        if (Input::hasFile('image')) {
            $file = Input::file('image');
            $path = public_path('uploads');
            $ext = strtolower($file->getClientOriginalExtension());
            $size = $file->getSize();
            $type = $file->getMimeType();
            $name = md5(time()) . ".{$ext}";

            if (in_array($ext, Meta::allowedImageExts())) {
                Input::file('image')->move($path, $name);

                $f = new Media();
                $f->name = $name;
                $f->size = $size;
                $f->type = $type;
                $f->ext = $ext;
                $f->save();

                $banner->media_id = $f->id;
            }
        }
        if (Input::hasFile('mob_image')) {
            $file = Input::file('mob_image');
            $path = public_path('uploads');
            $ext = strtolower($file->getClientOriginalExtension());
            $size = $file->getSize();
            $type = $file->getMimeType();
            // add char to the string to differentiate the name from image name for website above <fboukarroum>
            $name = md5(time()) . "0.{$ext}";

            if (in_array($ext, Meta::allowedImageExts())) {
                Input::file('mob_image')->move($path, $name);

                $f = new Media();
                $f->name = $name;
                $f->size = $size;
                $f->type = $type;
                $f->ext = $ext;
                $f->save();

                $banner->mob_media_id = $f->id;
            }
        }

        $banner->save();

        return Redirect::to(Input::get('redirect_to', 'dashboard/banners'));
    }

    /**
     * Delete a banner
     *
     * @param int $id
     * @return Response
     */
    public function getDelete($enc_id = null) {
        $id = base64_decode($enc_id);

        if (!I::can('delete_banners')) {
            App::abort(400);
        }

		DB::table('banner_segment')->where('banner_id', $id)->delete();

        Banner::find($id)->delete();

        return Redirect::to('dashboard/banners');
    }

	// -------- Country Attributes -------- //

    /**
     * Create a small LI list with banner segments
     *
     * @param int $banner_id
     * @return Response
     */
    private function listCountries($enc_banner_id) {
            $banner_id = base64_decode($enc_banner_id);
		$banner = Banner::find($banner_id);

        return View::make('banner.ejs.countries', array(
            'banner' => $banner,
        ));
	}

    /**
     * link a country to a banner
     *
     * @param int $banner_id
     * @return void
     */
    public function postLink_country($enc_banner_id) {
            $banner_id = base64_decode($enc_banner_id);
		$bc = new BannerCountry();

		$bc->country_id = self::sanitizeText(Input::get('country_id'));
        $bc->banner_id = $banner_id;
		$bc->save();

        return $this->listCountries($enc_banner_id);
	}

    /**
     * unlink a segment from a banner
     *
     * @param int $banner id, $segment_id
     * @return void
     */
    public function getUnlink_country($enc_banner_id, $enc_country_id) {
        $banner_id = base64_decode($enc_banner_id);
        $country_id = base64_decode($enc_country_id);
        $banner = Banner::find($banner_id);

		DB::table('banner_country')->where('banner_id', $banner_id)->where('country_id', $country_id)->delete();

        return $this->listCountries($enc_banner_id);
    }

	// -------- Page Attributes -------- //

    /**
     * Create a small LI list with banner pages
     *
     * @param int $banner_id
     * @return Response
     */
    private function listPages($enc_banner_id) {
            $banner_id = base64_decode($enc_banner_id);
            $banner = Banner::find($banner_id);

            return View::make('banner.ejs.pages', array(
                'banner' => $banner
            ));
	}

    /**
     * Link a page to a banner
     *
     * @param int $banner_id
     * @return void
     */
    public function postLink_page($enc_banner_id)
    {
        $banner_id = base64_decode($enc_banner_id);
        $page = self::sanitizeText(Input::get('page', 'home')); // default to home

        if (BannerPage::where('banner_id', $banner_id)->where('page', $page)->count() == 0) {
            $bp = new BannerPage();
            $bp->banner_id = $banner_id;
            $bp->page = $page;
            $bp->save();
        }

        return $this->listPages($enc_banner_id);
    }

    /**
     * Unlink a page from a banner
     *
     * @param int $banner_id
     * @param int $page_mapping_id
     * @return void
     */
    public function getUnlink_page($enc_banner_id, $enc_page_mapping_id) {
        $banner_id = base64_decode($enc_banner_id);
        $page_mapping_id = base64_decode($enc_page_mapping_id);
		DB::table('banner_page')->where('banner_id', $banner_id)->where('id', $page_mapping_id)->delete();

        return $this->listPages($enc_banner_id);
    }

    /**
     * Create a small LI list with banner  segments
     *
     * @param int $banner_id
     * @return Response
     */
    private function listSegments($enc_banner_id) {
            $banner_id = base64_decode($enc_banner_id);
		$banner = Banner::find($banner_id);

        return View::make('banner.ejs.segments', array(
            'banner' => $banner,
        ));
	}

    /**
     * link a segment to a banner
     *
     * @param int $banner_id
     * @return void
     */
    public function postLink_segment($enc_banner_id) {
            $banner_id = base64_decode($enc_banner_id);
		$bs = new BannerSegment();

		$bs->segment_id = self::sanitizeText(Input::get('segment_id'));
        $bs->banner_id = $banner_id;
		$bs->save();

        return $this->listSegments($enc_banner_id);
	}

    /**
     * unlink a segment from a banner
     *
     * @param int $banner id, $segment_id
     * @return void
     */
    public function getUnlink_segment($enc_banner_id, $enc_segment_id) {
        $banner_id = base64_decode($enc_banner_id);
        $segment_id = base64_decode($enc_segment_id);
        $banner = Banner::find($banner_id);

		DB::table('banner_segment')->where('banner_id', $banner_id)->where('segment_id', $segment_id)->delete();

        return $this->listSegments($enc_banner_id);
    }

    /**
     * Create a small LI list with banner segments
     *
     * @param int $banner_id
     * @return Response
     */
	/*
    private function listSegments($banner_id)
    {
        $banner = Banner::find($banner_id);

        return View::make('banner.segmentList', array(
            'banner'  => $banner,
        ));
    }
	*/

    /**
     * link a segment to a banner
     *
     * @param int $banner id
     * @return void
     */
	/*
    public function postLink_segment($id)
    {
      $segment_id = self::sanitizeText(Input::get('segment_id'));
        $banner = Banner::find($id);
        $segment = Segment::find($segment_id);

        if (!$banner->segments->contains($segment)) {
            $banner->segments()->attach($segment);
        }
        return $this->listSegments($id);
    }
	*/

    /**
     * unlink a segment from a banner
     *
     * @param int $banner id, $segment_id
     * @return void
     */
	/*
    public function getUnlink_segment($banner_id, $segment_id)
    {
        $banner = Banner::find($banner_id);

        $banner->segments()->detach($segment_id);

        return $this->listSegments($banner_id);
    }
	*/
}

// EOC
