<?php
namespace App\Http\Controllers;
/**
 * Travel Controller
 *
 * @category   Controllers
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class TravelController extends BaseController
{
    /**
     * Fetch the select drop down data for the store popup
     *
     * @return array
     */
    private function getSelectData()
    {
        return array(
            'countries' => DB::table('country')->orderBy('name', 'asc')->get(),
            'areas'     => DB::table('area')->orderBy('name', 'asc')->get(),
            'cities'    => DB::table('city')->orderBy('name', 'asc')->get()
        );
    }



    /**
     * Display the new flightsearch
     *
     * @return View
     */
    public function flightsearch()
    {
		$user = Auth::User();
        $data = array(
            'user_id'   => self::sanitizeText(Input::get('user_id')),
			'api_key'	=> $user->api_key
        );

        return View::make("travel.flightsearch", $data);
    }

    /**
     * Display the edit store page
     *
     * @return View
     */
    public function listflights()
    {
        $defaults = array();

        if(Input::has('country')){
            $defaults['country'] = self::sanitizeText(Input::get('country'));
        }
        if(Input::has('currency')){
            $defaults['currency'] = self::sanitizeText(Input::get('currency'));
        }
        if(Input::has('currency')){
            $defaults['currency'] = "en-GB";
        }

    /* @param POST country
     * @param POST currency
     * @param POST locale
     * @param POST originplace
     * @param POST destinationplace
     * @param POST outbounddate
     * @param POST inbounddate
     * @param POST cabinclass
     * @param POST adults
     * @param POST children*/

        $url = "http://partners.api.skyscanner.net/apiservices/pricing/v1.0";

	$fields_arr = array();

	foreach($defaults as $key => $value) {
		$fields_arr[] = $key.'='.$value.'&';
	}

	$fields_string = implode("&", $fields_arr);

	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_POST, count($defaults));
	curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
	curl_setopt($ch, CURLOPT_HEADER, 1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_TIMEOUT, 30);

	$result  = curl_exec($ch);
	curl_close($ch);
        $data = array(
            'user_id'   => self::sanitizeText(Input::get('user_id')),
        );

        return View::make("store.form", array_merge(
            $data, $this->getSelectData()
        ));
    }

    /**
     * Create a new store resource
     *
     * @param int $store_id
     * @return void
     */
    public function updateTravel($store_id = null)
    {
        $rules = array(
            'store_name' => 'required|min:1'
        );

        $validator = Validator::make(Input::all(), $rules);

        if($validator->fails()) {
            return Response::json(array(
                'failed' => true,
                'errors' => "Please ensure that you provide a valid store name"
            ));
        }

        $store = Travel::find($store_id);

        // Details
        $store->name       = self::sanitizeText(Input::get('store_name'));
        $store->lng        = self::sanitizeText(Input::get('store_longitude'));
        $store->lat        = self::sanitizeText(Input::get('store_latitude'));
        $store->mapping_id = self::sanitizeText(Input::get('mapping_id'));
        $store->draft      = false;

        // Address
        $store->address->country_id = self::sanitizeText(Input::get('store_country', 0));
        $store->address->area_id    = self::sanitizeText(Input::get('store_area', 0));
        $store->address->city_id    = self::sanitizeText(Input::get('store_city', 0));
        $store->address->street     = self::sanitizeText(Input::get('store_street', null));
        $store->address->floor      = self::sanitizeText(Input::get('store_floor', null));
        $store->address->building   = self::sanitizeText(Input::get('store_building', null));

        // POS
        Pos::where('store_id', $store_id)->delete();

        $pos_names    = self::sanitizeText(Input::get('pos_name'));
        $pos_mappings = self::sanitizeText(Input::get('pos_mapping'));

        foreach($pos_names as $k => $v) {
            if(!empty($pos_names[$k])) {
                $pos             = new Pos();
                $pos->name       = $pos_names[$k];
                $pos->mapping_id = $pos_mappings[$k];

                $store->pointOfSales()->save($pos);
            }
        }

        $store->push();

        return Response::json(array(
            'failed' => false,
            'view_url' => url('dashboard/store/list/'.$store->partner_id)
        ));
    }

    /**
     * Delete the specified store
     *
     * @return View
     */
    public function deleteTravel($store_id = null)
    {
        Travel::find($store_id)->delete();

        return Response::json(array(
            'failed' => false
        ));
    }

    /**
     * Render a list of stores relevant to the Partner
     *
     * @param int $partner_id
     * @return View
     */
    public function listTravels($partner_id = null)
    {
        $data = array(
            'stores' => DB::table('store')->where('partner_id', $partner_id)->where('draft', false)->get()
        );

        return View::make("store.list", $data);
    }

	/**
	 * Handle the flight search
	 *
	 * @return void
	 */
	public function flights()
	{
            $flight['sk_api_key']  = "bp528651548469742696677473232465";
            $flight['sk_currency'] = "USD";
            $flight['sk_locale']   = "en-GB";
            $defaults = array();


            $defaults['apikey'] = $flight['sk_api_key'];

            if(Input::has('country') ) {
                    $defaults['country'] = self::sanitizeText(Input::get('country'));
            }

            if(Input::has('currency')) {
                    $defaults['currency'] = self::sanitizeText(Input::get('currency'));
            }

            if(Input::has('locale')) {
                    $defaults['locale'] = self::sanitizeText(Input::get('locale'));
            }

            if(Input::has('originplace')) {
                   $origin	= explode('-', self::sanitizeText(Input::get('originplace') ));
                            $defaults['originplace'] = $origin[1];
            }

            if(Input::has('destinationplace')) {
                            $dest	= explode('-', self::sanitizeText(Input::get('destinationplace') ));
                    $defaults['destinationplace'] = $dest[1];
            }

            if(Input::has('outbounddate')) {
                    $time = strtotime(self::sanitizeText(Input::get('outbounddate')));
                    $defaults['outbounddate'] = date("Y-m-d", $time);
            }

            if(Input::has('inbounddate')) {
                    $time = strtotime(self::sanitizeText(Input::get('inbounddate')));
                    $defaults['inbounddate'] = date("Y-m-d", $time);
            }

            if(Input::has('cabinclass')) {
                    $defaults['cabinclass'] = self::sanitizeText(Input::get('cabinclass'));
            }

            if(Input::has('adults')) {
                    $defaults['adults'] = self::sanitizeText(Input::get('adults'));
            }

            if(Input::has('children')) {
                    $defaults['children'] = self::sanitizeText(Input::get('children'));
            }

            if(Input::has('infants')) {
                    $defaults['infants'] = self::sanitizeText(Input::get('infants'));
                    }

                    $defaults = self::sk_session_request_params($defaults);

                    $url = "http://partners.api.skyscanner.net/apiservices/pricing/v1.0";

                    $fields_string = '';

                    foreach($defaults as $key => $value) {
                            $fields_string .= $key.'='.$value.'&';
                    }

                    $fields_string = rtrim($fields_string, '&');
//                    echo $fields_string;
//                    $flds = 'apikey=bp528651548469742696677473232465&country=LB&originplace=BEY&destinationplace=LCA&outbounddate=2015-12-09&inbounddate=2015-12-10&cabinclass=Economy&adults=1&children=0&infants=0&currency=USD&locale=en-US&locationschema=Iata';
                    $ch = curl_init();

                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_POST, count($defaults));
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
                    curl_setopt($ch, CURLOPT_HEADER, 1);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_TIMEOUT, 30);

                    $result  = curl_exec($ch);

                    curl_close($ch);
//                    echo "<pre>";
//                    var_dump($result);
//                    exit();
                    // Fetch the Location value from the headers

                    $matches = array();

                    preg_match("#http://[a-zA-Z./0-9_]+#", $result, $matches);

                    if(!empty($matches)) {
                            $res =  $matches[0];
                    } else {
                            $res = '';
                    }

                            $res = $res . "?apiKey=" . $flight['sk_api_key'];
                            $session_defaults = array(
                                    'sorttype'                => self::sanitizeText(Input::get('sorttype')),
                                    'stops'                   => self::sanitizeText(Input::get('stops')),
                                    'outbounddepartstarttime' => self::sanitizeText(Input::get('outbounddepartstarttime')),
                                    'outbounddepartendtime'   => self::sanitizeText(Input::get('outbounddepartendtime')),
                                    'inbounddepartstarttime'  => self::sanitizeText(Input::get('inbounddepartstarttime')),
                                    'inbounddepartendtime'    => self::sanitizeText(Input::get('inbounddepartendtime')),
                                    'duration'                => self::sanitizeText(Input::get('duration')),
                                    'pageindex'               => self::sanitizeText(Input::get('pageindex'))
                            );

                            $defaults_session = http_build_query(self::sk_poll_request_params($session_defaults));

                            // Perform the request
                            $ch1 = curl_init();

                            curl_setopt($ch1, CURLOPT_URL, "{$res}&{$defaults_session}");
                            curl_setopt($ch1, CURLOPT_RETURNTRANSFER, 1);
                            curl_setopt($ch1, CURLOPT_TIMEOUT, 45);
                            //curl_setopt($ch, CURLOPT_HEADER, 1);

                            $results = curl_exec($ch1);

                            $skyscan_results = $results;
                            $sky_results = json_decode($skyscan_results);
                            if(property_exists($sky_results,'Itineraries')){
                                    if(count($sky_results->Itineraries) < 30){
                                            sleep(5);
                                            $results = curl_exec($ch1);
                                            $skyscan_results = $results;
                                            $sky_results = json_decode($skyscan_results);
                                    }
                            }
                            else{
                                    sleep(8);
                                    $results = curl_exec($ch1);
                                    $skyscan_results = $results;
                                    $sky_results = json_decode($skyscan_results);
                            }
                            if(property_exists($sky_results,'Itineraries')){
                                    if(count($sky_results->Itineraries) < 30){
                                            sleep(5);
                                            $results = curl_exec($ch1);
                                            $skyscan_results = $results;
                                            $sky_results = json_decode($skyscan_results);
                                    }
                            }
                            else{
                                    sleep(8);
                                    $results = curl_exec($ch1);
                                    $skyscan_results = $results;
                                    $sky_results = json_decode($skyscan_results);
                            }

                    curl_close($ch1);

                    $data = array(
                'user_id'   => self::sanitizeText(Input::get('user_id')),
                            'results'	=> $skyscan_results
            );

            return View::make("travel.flightlist", $data)->render();

	}


	private function sk_session_request_params($defaults = array())
	{


			if(!isset($defaults['apikey']) || empty($defaults['apiKey'])) {
					$defaults['apikey'] = "bp528651548469742696677473232465";
			}

			if(!isset($defaults['country']) || empty($defaults['country'])) {
					$defaults['country'] = "LB";
			}

			if(!isset($defaults['currency']) || empty($defaults['currency'])) {
					$defaults['currency'] = "USD";
			}

			if(!isset($defaults['locale']) || empty($defaults['locale'])) {
					$defaults['locale'] = "en-US";
			}

			if(!isset($defaults['originplace']) || empty($defaults['originplace'])) {
					$defaults['originplace'] = 'BEY';
			} else {
					$defaults['originplace'] = substr($defaults['originplace'], -3);
			}

			if(!isset($defaults['destinationplace']) || empty($defaults['destinationplace'])) {
					$defaults['destinationplace'] = 'IST';
			} else {
					$defaults['destinationplace'] = substr($defaults['destinationplace'], -3);
			}

			if(!isset($defaults['outbounddate']) || empty($defaults['outbounddate'])) {
					$defaults['outbounddate'] = date("Y-m-d", time());
			} else {
					$defaults['outbounddate'] = date("Y-m-d", strtotime($defaults['outbounddate']));
			}

			if(!isset($defaults['inbounddate']) || empty($defaults['inbounddate'])) {
					//$defaults['inbounddate'] = date("Y-m-d", strtotime("+1 day"));
			} else {
					$defaults['inbounddate'] = date("Y-m-d", strtotime($defaults['inbounddate']));
			}

			if(!isset($defaults['locationschema']) || empty($defaults['locationschema'])) {
					$defaults['locationschema'] = 'Iata';
			}

			if(!isset($defaults['cabinclass']) || empty($defaults['cabinclass'])) {
					$defaults['cabinclass'] = 'Economy';
			}

			if(!isset($defaults['adults']) || empty($defaults['adults'])) {
					$defaults['adults'] = 1;
			}

			if(!isset($defaults['children']) || empty($defaults['children'])) {
					$defaults['children'] = 0;
			}

			if(!isset($defaults['infants']) || empty($defaults['infants'])) {
					$defaults['infants'] = 0;
			}

			return $defaults;
	}

	/**
	* Create the default set of parameters for a poll request to SkyScanner
	*
	* @param array $defaults
	* @return array
	*/
	private function sk_poll_request_params($defaults = array())
	{

		   if(!isset($defaults['carrierschema']) || empty($defaults['carr'])) {
				   $defaults['carrierschema'] = 'Skyscanner';
		   }

		   if(!isset($defaults['sorttype']) || empty($defaults['sorttype'])) {
				   $defaults['sorttype'] = 'price';
		   }

		   if(!isset($defaults['sortorder']) || empty($defaults['sortorder'])) {
				   $defaults['sortorder'] = 'asc';
		   }

		   /*
		   if(!isset($defaults['stops']) || empty($defaults['stops'])) {
				   $defaults['stops'] = 0;
		   }
		   */

		   if(!isset($defaults['outbounddepartstarttime']) || empty($defaults['outbounddepartstarttime'])) {
				   $defaults['outbounddepartstarttime'] = date('H:i', strtotime("00:00"));
		   }

		   if(!isset($defaults['outbounddepartendtime']) || empty($defaults['outbounddepartendtime'])) {
				   $defaults['outbounddepartendtime'] = date('H:i', strtotime("23:59"));
		   }

		   if(!isset($defaults['inbounddepartstarttime']) || empty($defaults['inbounddepartstarttime'])) {
				   $defaults['inbounddepartstarttime'] = date('H:i', strtotime("00:00"));
		   }

		   if(!isset($defaults['inbounddepartendtime']) || empty($defaults['inbounddepartendtime'])) {
				   $defaults['inbounddepartendtime'] = date('H:i', strtotime("23:59"));
		   }

		   if(!isset($defaults['duration']) || empty($defaults['duration'])) {
				   $defaults['duration'] = 1800;  // 30 hours
		   }

		   if(!isset($defaults['pagesize']) || empty($defaults['pagesize'])) {
				   $defaults['pagesize'] = 200;
		   }

		   if(!isset($defaults['pageindex']) || empty($defaults['pageindex'])) {
				   $defaults['pageindex'] = 0;
		   }

		   /*
		   if(isset($defaults['skipCarrierLookup']) && !empty($defaults['skipCarrierLookup'])) {
				   $defaults['skipCarrierLookup'] = implode(';', $$defaults['skipCarrierLookup']);
		   }

		   if(isset($defaults['skipCarrierLookup']) && empty($defaults['skipCarrierLookup'])) {
				   unset($defaults['skipCarrierLookup']);
		   }
		   */
		   /*
		   if(!isset($defaults['includeBookingDetailsLink'])) {
				   $defaults['includeBookingDetailsLink'] = true;
		   }
		   */

		   return $defaults;
	}

	/**
	* Poll the SkyScanner session for results until complete
	*
	* @param string $session_url
	* @return stdClass
	*/
	function poll_sk_session( $params = array())
	{
	   $user = Auth::User();
	   $session_url = "http://54.187.85.96/api/skyscan-results?api_key=". $user->api_key;


	   $defaults = http_build_query(self::sk_poll_request_params($params));

	   // Perform the request
	   $ch = curl_init();
	   curl_setopt($ch, CURLOPT_URL, $session_url);
	   curl_setopt($ch, CURLOPT_POST, count($params));
	   curl_setopt($ch, CURLOPT_POSTFIELDS, $defaults);
	   curl_setopt($ch, CURLOPT_HEADER, 1);
	   curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	   curl_setopt($ch, CURLOPT_TIMEOUT, 30);

	   //curl_setopt($ch, CURLOPT_HEADER, 1);

	   $result = curl_exec($ch);

	   curl_close($ch);
	   return json_decode($result);
	}



	/**
	* Perform the flight poll request
	*
	* @return void
	*/
	public function poll_flights()
	{
		$flight['sk_api_key']  = "bp528651548469742696677473232465";
		$flight['sk_currency'] = "USD";
		$flight['sk_locale']   = "en-GB";
		$defaults = array();


		$defaults['apikey'] = $flight['sk_api_key'];

		if(Input::has('country') ) {
				$defaults['country'] = self::sanitizeText(Input::get('country'));
		}

		if(Input::has('currency')) {
				$defaults['currency'] = self::sanitizeText(Input::get('currency'));
		}

		if(Input::has('locale')) {
				$defaults['locale'] = self::sanitizeText(Input::get('locale'));
		}

		if(Input::has('originplace')) {
			   $origin	= explode('-', self::sanitizeText(Input::get('originplace') ));
			$defaults['originplace'] = $origin[1];
		}

		if(Input::has('destinationplace')) {
			$dest	= explode('-', self::sanitizeText(Input::get('destinationplace') ));
				$defaults['destinationplace'] = $dest[1];
		}

		if(Input::has('outbounddate')) {
				$time = strtotime(self::sanitizeText(Input::get('outbounddate')));
				$defaults['outbounddate'] = date("Y-m-d", $time);
		}

		if(Input::has('inbounddate')) {
				$time = strtotime(self::sanitizeText(Input::get('inbounddate')));
				$defaults['inbounddate'] = date("Y-m-d", $time);
		}

		if(Input::has('cabinclass')) {
				$defaults['cabinclass'] = self::sanitizeText(Input::get('cabinclass'));
		}

		if(Input::has('adults')) {
				$defaults['adults'] = self::sanitizeText(Input::get('adults'));
		}

		if(Input::has('children')) {
				$defaults['children'] = self::sanitizeText(Input::get('children'));
		}

		if(Input::has('infants')) {
				$defaults['infants'] = self::sanitizeText(Input::get('infants'));
		}

	   $results = self::poll_sk_session($defaults);
//		   var_dump($results);
//		   exit;
//		   $this->setJSONOutput();

	   echo json_encode($results);
	}

   /**
	* Show the confirm flight page
	*
	* @return void
	*/
   public function confirm_flight()
   {
	   $user = Auth::User();
		$data = array(
			'meta'   => base64_encode(json_encode($_GET)),
			'api_key'	=> $user->api_key
		);
		return View::make("travel.confirm_flight", $data);
   }


   /**
	* Perform a travel booking confirmation
	*
	* @return void
	*/
   public function do_confirm(){
	   $authUser	= Auth::User();
	   if( $authUser->id <= 0 ) {
		   exit;
	   }

	   if(!headers_sent()) {
			header('Content-type: application/json');
		}
	   // Perform form validation
		if(self::sanitizeText(Input::get('type'))!="hotel"){
				$tx	= array();
				$tx['points']			= self::sanitizeText(Input::get('points'));
				$tx['type']				= self::sanitizeText(Input::get('type'));
				$tx['meta']				= self::sanitizeText(Input::get('meta'));
				$tx['first_name']		= self::sanitizeText(Input::get('first_name'));
				$tx['last_name']			= self::sanitizeText(Input::get('last_name'));
				$tx['email']				= self::sanitizeText(Input::get('email'));
				$tx['dob']				= self::sanitizeText(Input::get('dob'));
				$tx['passport_country']	= self::sanitizeText(Input::get('passport_country'));
				$tx['mobile']			= self::sanitizeText(Input::get('mobile'));

				$validator = Validator::make($tx, array(
							 'points'			=> 'required',
							 'type'				=> 'required',
							 'meta'				=> 'required',
							 'first_name'		=> 'required',
							 'last_name'			=> 'required',
							 'email'				=> 'required|email',
							 'dob'				=> 'required',
							 'passport_country'	=> 'required',
							 'mobile'			=> 'required'
					 ));

		}else{
				$tx	= array();
				$tx['points']				= self::sanitizeText(Input::get('points'));
				$tx['type']					= self::sanitizeText(Input::get('type'));
				$tx['meta']					= self::sanitizeText(Input::get('meta'));
				$tx['book_first_name']		= self::sanitizeText(Input::get('book_first_name'));
				$tx['book_last_name']		= self::sanitizeText(Input::get('book_last_name'));
				$tx['book_email']			= self::sanitizeText(Input::get('book_email'));
				$tx['confirm_email']		= self::sanitizeText(Input::get('confirm_email'));
				$tx['book_address_1']		= self::sanitizeText(Input::get('book_address_1'));
				$tx['book_city']			= self::sanitizeText(Input::get('book_city'));
				$tx['book_country_code']	= self::sanitizeText(Input::get('book_country_code'));
				$tx['book_postal_code']		= self::sanitizeText(Input::get('book_postal_code'));
				$tx['book_mobile']			= self::sanitizeText(Input::get('book_mobile'));
//                            $tx['pin']					= self::sanitizeText(Input::get('pin');

				$validator = Validator::make($tx, array(
							 'points'				=> 'required',
							 'type'					=> 'required',
							 'meta'					=> 'required',
							 'book_first_name'		=> 'required',
							 'book_last_name'		=> 'required',
							 'book_email'			=> 'required|email',
							 'book_email'			=> 'required|email',
							 'book_address_1'		=> 'required',
							 'book_city'			=> 'required',
							 'book_country_code'	=> 'required',
							 'book_postal_code'		=> 'required',
							 'book_mobile'			=> 'required',
//                                         'pin'					=> 'required'
					 ));

		}

		if($validator->fails())
		{
			echo "Validator Fails";
			exit;
		}

		$meta = json_decode(base64_decode(self::sanitizeText(Input::get('meta'))));

		if(!$meta) {
			echo json_encode(array(
				'success'  => false,
				'messages' => array("The transaction could not be completed.")
			));
			exit;
		}

	   // Perform the redemption transaction

		$price     = null;
		$reference = null;
		$notes     = null;

		switch(self::sanitizeText(Input::get('type'))) {
			case "flight":
				 $price		= $meta->price;
				 $reference	= "BLU Travel - Flight";
				 $notes		= json_encode($meta);
				break;
			case "hotel":

				 $reference	= "BLU Travel - Hotel";
				 $notes		= json_encode($meta);
				 $diff		= strtotime($meta->departureDate) - strtotime($meta->arrivalDate);
				 $price		= (intval($meta->points) * floor(abs($diff))/(60*60*24)) * 1.21;
				 break;
			case "car":
				$price		= $meta->points * 1.1;
				$reference	= "BLU Travel - Car";
				$notes		= json_encode($meta);
				break;
		}
		$user_travel = self::sanitizeText(Input::get('user_travel_id'));
		$response = self::perform_travel_transaction($price, $reference, $notes, self::sanitizeText(Input::get('type')), $user_travel);

		$tick_id = session::get('user_travel_ticket');
		if(isset($tick_id))
		{
			$ticket = Ticket::find($tick_id);
		}

		 if(!$ticket) {
			return Response::json(array(
				'failed'  => true,
				'message' => $response->message
			));
		 }

		if($response->status == Transaction::TRANS_SUCCESS) {
			$data['ticket_id'] = $tick_id;
			return $data;
//			return Redirect::to('dashboard/call_center/view/'.$tick_id.'?load');
		} else {
			$data['ticket_id'] = $tick_id;
			return $data;
//			return Redirect::to('dashboard/call_center/view/'. $tick_id .'?load');
		}

   }


   /**
	* Perform a travel item redemption transaction
	*
	* @param
	* @return void
	*/
   function perform_travel_transaction($points, $reference, $note, $type, $user_travel_id)
   {
//		   $ci = &get_instance();

	   $user_id = $user_travel_id;

	   $response	= self::postTravelTransaction($points, $reference, $note, $type, $user_id);

	   return $response;
   }

	/**
     * Perform a travel transaction confirmation
     *
     *
	 * @param POST points
	 * @param POST reference
	 * @param POST note
	 * @param POST travel_type [Flight, Hotel, Car]
	 * @param POST source
	 * @param GET user_id
     * @return Response
     */
	public function postTravelTransaction($points, $reference, $note, $type, $user_id)
	{
		$user = User::where('id', $user_id)->first();

        if (!$user) {
            return self::respondWith(
                500, "Could not locate user with id {$user_id} in database"
            );
        }
		$memberDetails = User::find($user_id);
		$partner_id = '';
		foreach($memberDetails->partners as $p){
			if (Auth::User()->partners->contains($p)){
				$partner_id = $p->id;
			}
		}

		$pos_id = '';
		if(Input::has('store_id')){
			if(Input::has('POS_id')){
				$pos_id= self::sanitizeText(Input::get('POS_id'));
			}
			else{
				$store = Store::find(self::sanitizeText(Input::get('store_id')));
				$pos_id = $store->pointOfSales->first()->id;
			}
		}else{
			$storeDetails	= Store::where("partner_id", $partner_id)->first();
			$store			= Store::find($storeDetails->id);
			$pos_id			= $store->pointOfSales->first()->id;
		}
//			print_r( self::sanitizeText(Input::get('note') );
//			exit;
		$result = Transaction::doRedemption(array(
			'user_id'   => $user->id,
			'partner'   => $partner_id,
            'store_id'  => self::sanitizeText(Input::get('store_id')),
            'pos_id'    => $pos_id,
			'points'    => $points,
			'source'    => self::sanitizeText(Input::get('source')),
			'reference' => $reference,
			'notes'	    => $note
		), self::sanitizeText(Input::get('type', 'Flight')));

		return $result;
	}

	/**
     * Display the new hotelsearch
     *
     * @return View
     */
    public function hotelsearch()
    {
		$user = Auth::User();
        $data = array(
            'user_id'   => self::sanitizeText(Input::get('source_id')),
			'api_key'	=> $user->api_key
        );

        return View::make("travel.hotelsearch", $data);
    }

	/**
	 * Perform a search of airports for named matches
	 *
	 * @param
	 * @return void
	 */
	public function search_locations()
	{
//		$this->setJSONOutput();

		$url = "http://suggest.expedia.com/hint/es/v1/ac/en_GB/".urlencode(self::sanitizeText(Input::get('term')));

		$results = trim(file_get_contents($url));
		$results = substr($results, 1, strlen($results) - 2);
		$results = str_replace(array('<B>', '</B>'), '', $results);
		$results = json_decode($results);

		$matches = array();

		foreach($results->r as $place) {
			$matches[] = $place->d;
		}

		echo json_encode($matches);
	}

	/**
	 * Handle the hotel search
	 *
	 * @return void
	 */
	public function hotels()
	{
		if(Input::has('arrivalDate') && Input::has('departureDate')){
			$arrivaldate	= date('m/d/Y', strtotime(self::sanitizeText(Input::get('arrivalDate'))));
			$departuredate	= date('m/d/Y', strtotime(self::sanitizeText(Input::get('departureDate'))));

		}
		else{
			$arrivaldate = str_replace('-','/',self::sanitizeText(Input::get('arrivaldate')));
			$departuredate = str_replace('-','/',self::sanitizeText(Input::get('departuredate')));

			$arrivaldate	= date('m/d/Y', strtotime(self::sanitizeText(Input::get('arrivaldate'))));
			$departuredate	= date('m/d/Y', strtotime(self::sanitizeText(Input::get('departuredate'))));
		}


		if(self::sanitizeText(Input::get('cacheKey')) && self::sanitizeText(Input::get('cacheLocation'))){
			$url = "http://book.api.ean.com/ean-services/rs/hotel/v3/list?cid=55505&minorRev=26".
				"&apiKey=rjqr2vfvkkn3v7p8hqzkfcqq".
				"&customerUserAgent=Mozilla%2F5.0%2B%28X11%3B%2BUbuntu%3B%2BLinux%2Bi686%3B%2Brv%3A35.0%29%2BGecko%2F20100101%2BFirefox%2F35.0".
				"&customerSessionId=" . self::sanitizeText(Input::get('customerSessionId')) . "&customerIpAddress=&locale=en_US&currencyCode=USD".
				"&cacheKey=" . self::sanitizeText(Input::get('cacheKey')) . "&cacheLocation=" . self::sanitizeText(Input::get('cacheLocation'));
		}
		else{		//$url = "http://suggest.expedia.com/hint/es/v1/ac/en_GB/".urlencode(self::sanitizeText(Input::get('city'));
			$url = "http://book.api.ean.com/ean-services/rs/hotel/v3/list?cid=55505&minorRev=26".
				"&apiKey=rjqr2vfvkkn3v7p8hqzkfcqq&locale=en_EN&currencyCode=USD&customerIpAddress=10.184.2.9".
				"&customerUserAgent=Mozilla%2F5.0%2B%28X11%3B%2BUbuntu%3B%2BLinux%2Bi686%3B%2Brv%3A35.0%29%2BGecko%2F20100101%2BFirefox%2F35.0".
				"&customerSessionId=&destinationString=" . urlencode(self::sanitizeText(Input::get('city'))) .
				"&arrivalDate=" . $arrivaldate ."&departureDate=" . $departuredate ."&room1=" . intval(self::sanitizeText(Input::get('adults'))) . "&numberOfResults=198";
		}

		$results = trim(file_get_contents($url));

		$results = json_decode($results);

		$data = array(
                'user_id'   => self::sanitizeText(Input::get('user_id')),
                'results'	=> $results
            );

            return View::make("travel.hotellist", $data)->render();
	}

	/**
	 * Poll the hotels according to criteria
	 *
	 * @return void
	 */
	public function poll_hotels()
	{
		if(Input::has('arrivalDate') && Input::has('departureDate')){
                $arrivaldate = str_replace('-','/',self::sanitizeText(Input::get('arrivalDate')));
                $departuredate = str_replace('-','/',self::sanitizeText(Input::get('departureDate')));

//                $arrivaldate	= date('m/d/Y', strtotime(self::sanitizeText(Input::get('arrivalDate')));
//                $departuredate	= date('m/d/Y', strtotime(self::sanitizeText(Input::get('departureDate')));

            }
            else{
                $arrivaldate = str_replace('-','/',self::sanitizeText(Input::get('arrivaldate')));
                $departuredate = str_replace('-','/',self::sanitizeText(Input::get('departuredate')));

//				$arrivaldate	= date('m/d/Y', strtotime(self::sanitizeText(Input::get('arrivaldate')));
//                $departuredate	= date('m/d/Y', strtotime(self::sanitizeText(Input::get('departuredate')));
            }


            if(self::sanitizeText(Input::get('cacheKey')) && self::sanitizeText(Input::get('cacheLocation'))){
                $url = "http://book.api.ean.com/ean-services/rs/hotel/v3/list?cid=55505&minorRev=26".
                    "&apiKey=rjqr2vfvkkn3v7p8hqzkfcqq".
                    "&customerUserAgent=Mozilla%2F5.0%2B%28X11%3B%2BUbuntu%3B%2BLinux%2Bi686%3B%2Brv%3A35.0%29%2BGecko%2F20100101%2BFirefox%2F35.0".
                    "&customerSessionId=" . self::sanitizeText(Input::get('customerSessionId')) . "&customerIpAddress=&locale=en_US&currencyCode=USD".
                    "&cacheKey=" . self::sanitizeText(Input::get('cacheKey')) . "&cacheLocation=" . self::sanitizeText(Input::get('cacheLocation'));
            }
            else{		//$url = "http://suggest.expedia.com/hint/es/v1/ac/en_GB/".urlencode(self::sanitizeText(Input::get('city'));
                $url = "http://book.api.ean.com/ean-services/rs/hotel/v3/list?cid=55505&minorRev=26".
                    "&apiKey=rjqr2vfvkkn3v7p8hqzkfcqq&locale=en_EN&currencyCode=USD&customerIpAddress=10.184.2.9".
                    "&customerUserAgent=Mozilla%2F5.0%2B%28X11%3B%2BUbuntu%3B%2BLinux%2Bi686%3B%2Brv%3A35.0%29%2BGecko%2F20100101%2BFirefox%2F35.0".
                    "&customerSessionId=&destinationString=" . urlencode(self::sanitizeText(Input::get('destinationString'))) .
                    "&arrivalDate=" . $arrivaldate ."&departureDate=" . $departuredate ."&room1=" . intval(self::sanitizeText(Input::get('adults'))) . "&numberOfResults=198";
            }
//            echo "$url";
//            exit();
		$results = trim(file_get_contents($url));

		$results = json_decode($results);

		echo json_encode($results);
	}

	/**
	* Create the defaults for searching hotels
	*
	* @return array
	*/
   function hotel_request_defaults()
   {
	   $adult_count = (int)(Input::post('adults'));
	   $adult_count = $adult_count + (int)(Input::post('children'));
	   $adult_count = $adult_count + (int)(Input::post('infants'));

	   $params =array(
			'api_key'		=> 'c4ca4238a0b923820dcc509a6f75849b',
			'city'			=> Input::post('destinationString'),
			'arrivaldate'	=> Input::post('arrivalDate'),
			'departuredate'	=> Input::post('departureDate'),
			'adults'		=> $adult_count
		);

	   return $params;
   }
	/**
	 * Perform a search of airports for named matches of flights
	 *
	 * @param
	 * @return void
	 */
	public function search_flight_locations()
	{
                $apiKey="c4ca4238a0b923820dcc509a6f75849b";
		$url = "https://bluai.com/api/skyscan-location?api_key=".$apiKey."&query=".urlencode(self::sanitizeText(Input::get('term')));

		$results = trim(file_get_contents($url));
		$resultsdecoded = json_decode($results);
		$matches = array();

		foreach($resultsdecoded->locations->Places as $place) {
                        $countrySym = substr($place->PlaceId, 0, -4);
			$matches[] = $place->PlaceName.", ".$place->CountryName." - ". $countrySym; //Beirut, Lebanon - BEY
		}

		echo json_encode($matches);
	}

	/**
	 * Load up the hotel information
	 *
	 * @param int $hotel_id
	 * @return void
	 */
	public function hotel_info($hotel_id)
	{
			if(Input::has('arrivalDate') && Input::has('departureDate')){
                $arrivaldate = str_replace('-','/',self::sanitizeText(Input::get('arrivalDate')));
                $departuredate = str_replace('-','/',self::sanitizeText(Input::get('departureDate')));
            }
            else{
                $arrivaldate = str_replace('-','/',self::sanitizeText(Input::get('arrivaldate')));
                $departuredate = str_replace('-','/',self::sanitizeText(Input::get('departuredate')));
            }
                $url_info = "http://book.api.ean.com/ean-services/rs/hotel/v3/info?cid=55505&minorRev=26";
                $url_avail = "http://book.api.ean.com/ean-services/rs/hotel/v3/avail?cid=55505&minorRev=26";

                $url_params = "&apiKey=rjqr2vfvkkn3v7p8hqzkfcqq&locale=en_EN&currencyCode=USD&customerIpAddress=10.184.2.9".
                    "&customerUserAgent=Mozilla%2F5.0%2B%28X11%3B%2BUbuntu%3B%2BLinux%2Bi686%3B%2Brv%3A35.0%29%2BGecko%2F20100101%2BFirefox%2F35.0".
                    "&customerSessionId=&hotelId=" . urlencode($hotel_id) .
                    "&arrivalDate=" . $arrivaldate ."&departureDate=" . $departuredate ."&includeDetails=true".
                    "&includeRoomImages=true&room1=" . intval(self::sanitizeText(Input::get('adults')));

		$results = trim(file_get_contents($url_info . $url_params));
                $resultsavail = trim(file_get_contents($url_avail . $url_params));
		$all_results = json_encode(array_merge(json_decode($results, true),json_decode($resultsavail, true)));
		$all_results_obj = json_decode($all_results);

		$data = array(
                'user_id'   => self::sanitizeText(Input::get('user_id')),
                'results'	=> $all_results_obj,
                'hotel_image'	=> self::sanitizeText(Input::get('hotelimg'))
            );

            return View::make("travel.hotelinfo", $data)->render();
	}

	/**
	* Show the confirm flight page
	*
	* @return void
	*/
	public function confirm_hotel()
	{
		$user = Auth::User();
		 $data = array(
		 'meta' => base64_encode(json_encode(self::sanitizeText(Input::get()))),
		 'form' => self::sanitizeText(Input::get())
	 );
		 return View::make("travel.confirm_hotel", $data);
	}


	/**
     * Display the new car
     *
     * @return View
     */
    public function carsearch()
    {
		$user = Auth::User();
        $data = array(
			'api_key'	=> $user->api_key,

        );

        return View::make("travel.carsearch", $data);
    }

	/**
	 * Fetch a list of cities
	 *
	 * @return void
	 */
	public function ajax_cities()
	{
		$language	 = self::sanitizeText(Input::get('language', 'en'));
		$country	 = self::sanitizeText(Input::get('country'));
		$rc_username = 'blupoints';
		$rc_password = 'blupoints123';
		$xml = new DOMDocument();

		$xml = '<PickUpCityListRQ preflang="'.$language.'"><Credentials username="' . $rc_username . '" password="' . $rc_password . '" remoteIp="rentalcars.com" /><Country>' . $country . '</Country></PickUpCityListRQ>';
		$response = simplexml_load_string(self::exec_rc_request($xml, "POST"));

		$buffer = array();

		foreach($response->CityList->City as $name) {
				$buffer[] = (string)$name;
		}

		$response_json = json_encode($buffer);
		$response_decode = json_decode($response_json,true);

		$buffer = array();
		for($i=0;$i<count($response_decode);$i++) {
			$s_i = (string)$i;

			 if(isset($response_decode[$s_i])){
				$buffer[] = (string)$response_decode[$s_i];
			 }
			 else{
				 break;
			 }
		}
		foreach($buffer as $k) {
			echo '<option value="'.$k.'">'.$k.'</option>';
		}
	}

	/**
	 * Fetch a list of locations
	 *
	 * @return void
	 */
	public function ajax_locations()
	{
		 $country= self::sanitizeText(Input::get('country'));
		$city = self::sanitizeText(Input::get('city'));
		$language = self::sanitizeText(Input::get('language', 'en'));
		$rc_username = 'blupoints';
		$rc_password = 'blupoints123';

		$xml = '<PickUpLocationListRQ preflang="'.$language.'"><Credentials username="' . $rc_username . '" password="' . $rc_password . '" remoteIp="rentalcars.com" /><Country>' . $country . '</Country><City>' . $city .'</City></PickUpLocationListRQ>';
		$response = simplexml_load_string(self::exec_rc_request($xml, "POST"));

		$buffer = array();

		foreach($response->LocationList->Location as $location) {
				$buffer[(int)$location['id']] = (string)$location;
		}

		$response_json = json_encode($buffer);
        $response_decode = json_decode($response_json,true);

		foreach($response_decode as $k => $v) {
			echo '<option value="'.$k.'">'.$v.'</option>';
		}
	}

	/**
	* Fetch a list of countries supported
	*
	* @return void
	*/
	public static function rc_country_list(){
		$rc_username = 'blupoints';
			$rc_password = 'blupoints123';
			$language		= 'en';
			$xml = new DOMDocument();

			$pickupCountry = $xml->createElement("PickUpCountryListRQ");
			$preflang_attr = $xml->createAttribute('preflang');
			$preflang_attr->value = $language;
			$pickupCountry->appendChild($preflang_attr);

			$credentials = $xml->createElement("Credentials");
			$credentials->setAttribute('username', $rc_username);
			$credentials->setAttribute('password', $rc_password);
			$credentials->setAttribute('remoteIp', $_SERVER['REMOTE_ADDR']);

			$pickupCountry->appendChild($credentials);

			$xml->appendChild($pickupCountry);

			$response = simplexml_load_string(self::exec_rc_request($xml->saveXML(), "POST"));

			$buffer = array();

			foreach($response->CountryList->Country as $name) {
					$response2[] = (string)$name;
			}

			  $response_json = json_encode($response2);
                $response_decode = json_decode($response_json,true);
                for($i=0;$i<count($response_decode);$i++) {
                    $s_i = (string)$i;

                     if(isset($response_decode[$s_i])){
                        $buffer[] = (string)$response_decode[$s_i];
                     }
                     else{
                         break;
                     }
                }

		return $buffer;
	}

	/**
	* Execute a rental cars request
	*
	* @param string $xml
	* @param string $method
	* @return void
	*/
   private static function exec_rc_request($xml, $method = "GET")
   {
		   $xml = "xml=$xml";

		   $ch = curl_init();

		   if($method == 'POST') {
				   $url = "https://xml.rentalcars.com/service/ServiceRequest.do";

				   curl_setopt($ch, CURLOPT_URL, $url);
				   curl_setopt($ch, CURLOPT_POST, 1);
				   curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
		   } else {
				   $url = "https://xml.rentalcars.com/service/ServiceRequest.do?$xml";

				   curl_setopt($ch, CURLOPT_URL, $url);
		   }

		   curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		   curl_setopt($ch, CURLOPT_TIMEOUT, 30);

		   $result  = curl_exec($ch);

		   curl_close($ch);
		   return $result;
   }


	/**
	 * Fetch a list of cars based on post input
	 *
	 *
	 * @param POST pickuptime
	 * @param POST dropofftime
	 * @param POST location
	 * @param POST location_drop
	 * @param POST driver_age
	 *
	 * @return array
	 */
	public function cars()
	{
		$rc_username = 'blupoints';
		$rc_password = 'blupoints123';

		$xml = new DOMDocument();
		$pickUpTime = strtotime(self::sanitizeText(Input::get('pickup')));
		$dropOffTime = strtotime(self::sanitizeText(Input::get('dropoff')));

		$search = $xml->createElement("SearchRQ");

		$search->setAttribute("supplierInfo", "true");
		$search->setAttribute("prefcurr", "USD");
		$search->setAttribute("preflang", "en");

		$credentials = $xml->createElement("Credentials");
		$credentials->setAttribute('username', $rc_username);
		$credentials->setAttribute('password', $rc_password);
		$credentials->setAttribute('remoteIp', $_SERVER['REMOTE_ADDR']);

		$pickup = $xml->createElement('PickUp');

		$pickupLocation = $xml->createElement('Location');
		$pickupLocation->setAttribute('id', self::sanitizeText(Input::get('location')));

		$pickupDate = $xml->createElement('Date');
		$pickupDate->setAttribute('year', date("Y", $pickUpTime));
		$pickupDate->setAttribute('month', date("m", $pickUpTime));
		$pickupDate->setAttribute('day', date("d", $pickUpTime));
		$pickupDate->setAttribute('hour', date("G", $pickUpTime));
		$pickupDate->setAttribute('minute', date("i", $pickUpTime));

		$pickup->appendChild($pickupLocation);
		$pickup->appendChild($pickupDate);

		$search->appendChild($credentials);
		$search->appendChild($pickup);

		if(Input::has('sameCarLocation')) {
				$dropoff = $xml->createElement('DropOff');

				$dropoffLocation = $xml->createElement('Location');
				$dropoffLocation->setAttribute('id', self::sanitizeText(Input::get('location')));

				$dropoffDate = $xml->createElement('Date');
				$dropoffDate->setAttribute('year', date("Y", $dropOffTime));
				$dropoffDate->setAttribute('month', date("m", $dropOffTime));
				$dropoffDate->setAttribute('day', date("d", $dropOffTime));
				$dropoffDate->setAttribute('hour', date("G", $dropOffTime));
				$dropoffDate->setAttribute('minute', date("i", $dropOffTime));

				$dropoff->appendChild($dropoffLocation);
				$dropoff->appendChild($dropoffDate);
				$search->appendChild($dropoff);
		} else{
				$dropoff = $xml->createElement('DropOff');

				$dropoffLocation = $xml->createElement('Location');
				$dropoffLocation->setAttribute('id', self::sanitizeText(Input::get('location_drop')));

				$dropoffDate = $xml->createElement('Date');
				$dropoffDate->setAttribute('year', date("Y", $dropOffTime));
				$dropoffDate->setAttribute('month', date("m", $dropOffTime));
				$dropoffDate->setAttribute('day', date("d", $dropOffTime));
				$dropoffDate->setAttribute('hour', date("G", $dropOffTime));
				$dropoffDate->setAttribute('minute', date("i", $dropOffTime));

				$dropoff->appendChild($dropoffLocation);
				$dropoff->appendChild($dropoffDate);
				$search->appendChild($dropoff);
		}

		$age = $xml->createElement('DriverAge', self::sanitizeText(Input::get('age')));
		$search->appendChild($age);

		$xml->appendChild($search);
		$xmlstring = $xml->saveXML($search);
		$xmlstring = urlencode($xmlstring);

		$response = simplexml_load_string(self::exec_rc_request($xmlstring));
		$results	= json_encode($response);
		$response_decode = json_decode($results,true);
		$view_data = array(
		'cars' => $response_decode
		);

		return View::make("travel.carlist", $view_data)->render();
	}

	/**
	 * Confirm the car booking
	 *
	 * @return void
	 */
	public function confirm_car($data = array())
	{
		$view_data = array();
		$view_data = array(
			'meta' => base64_encode(json_encode(self::sanitizeText(Input::get()))),
		);

		return View::make("travel.confirm_car", $view_data);
	}

} // EOC
