<?php
namespace App\Http\Controllers;
use App\Classes\PathHelper;
use I;
use View;
/**
 * Import Controller
 *
 * @category   Controllers
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class ImportController extends Controller
{
    /**
     * Ensure that we're running as admin
     *
     * @return void
     */
    public function __construct()
    {
//        if(!I::am('BLU Admin')) {
//            App::abort(404);
//        }
    }

    /**
     * Get the file to import
     *
     * @return void
     */
    public function getIndex()
    {
        if(!I::can('view_import')){
            return Redirect::to(url("dashboard"));
        }
        return View::make("import.view");
    }
    /**
     * Reverse file import
     *
     * @return void
     */
    public function getReverseimport()
    {
        if(!I::can('view_import')){
            return Redirect::to(url("dashboard"));
        }
        $name1 = self::sanitizeText(Input::get('filename'));
        $res_imported = DB::table('transaction')->where('ref_number', 'LIKE', '%'.$name1 .'%')->get();
        foreach($res_imported as $res_import){
            $user = User::where('id', $res_import->user_id)->first();
            $user->updateBalance($res_import->points_rewarded, $res_import->network_id, 'subtract');
            DB::table('transaction')->where('id', $res_import->id)->delete();
        }


        return Redirect::to('import')->with('success','File Reverted Successfully!');
    }

    /*
     * Perform the import
     *
     * @return void
     */
    public function postImport()
    {
        if(!I::can('view_import')){
            return Redirect::to(url("dashboard"));
        }
        if(Input::hasFile('file')) {

            $file = Input::file('file');

            $ext  = $file->getClientOriginalExtension();
            $fileNameOriginal = $file->getClientOriginalName();

            $fileNameGenerated = PathHelper::generateFileBaseName($fileNameOriginal, $ext) . '.' . $ext;
            $folderPath = PathHelper::incoming('misc');

            if($ext == 'xls') {
                Input::file('file')->move($folderPath, $fileNameGenerated);
                $filePath = $folderPath.'/'.$fileNameGenerated;

				// Cache
                $cacheMethod   = PHPExcel_CachedObjectStorageFactory::cache_to_phpTemp;
                $cacheSettings = array( 'memoryCacheSize' => '32MB');
                PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

                // Read file
                $reader = PHPExcel_IOFactory::createReaderForFile($filePath);
                $reader->setReadDataOnly(true);
                $excel = $reader->load($filePath);

				ExcelImport::import($excel);
            }

            if($ext == 'TXT' || $ext == 'txt' && strstr($fileNameOriginal,'FNB') ){
                $res_imported = DB::table('transaction')->where('ref_number', 'LIKE', '%'.$fileNameOriginal .'%')->distinct()->get();

                
                if(!empty($res_imported)){
                    return Redirect::to('import')->with('revert',$fileNameOriginal)->with('success', 'File Already Exists. To delete the existing file please press the button below.');
                }


                Input::file('file')->move($folderPath, $fileNameGenerated);
                $filePath = $folderPath.'/'.$fileNameGenerated;
                $contents = File::get($filePath);
                $contentLines = explode("\n", $contents);
                $arrContent = array();
                foreach ($contentLines as $lineread){
                    $arrContent[] = explode(",", $lineread);
                }
                if(!empty($arrContent)){
                foreach($arrContent as $row){
                    if($row[0] != ''){
                        $tx                    = array();
                        $tx['id']              = 'NULL';
                        $tx['ref_number']      = $fileNameOriginal;
                        $tx['original_total_amount']    = $row[2];
                        $tx['total_amount']    = $row[2];
                        $tx['store_id']        = '4446';
                        $refnum = ltrim($row[0],'0');
                        $user = 0;
                        if(is_numeric($refnum)){
                            $user=$refnum;
                        }else{
                            $reference = Reference::where('number', $refnum)->first();
                            $user = $reference->user_id;
                        }
                        $tx['user_id']=$user;
                        $tx['points_redeemed'] = 0;
                        $tx['partner_id']      = '2020';
                        $tx['trx_reward']      = 1;

                        $unhandledDate = $row[3];
                        $arrunhandledDate = explode("/",$unhandledDate);
                        $date_unhandled = $arrunhandledDate[1] . "/" . $arrunhandledDate[0] . "/" . $arrunhandledDate[2];

                        $date = strtotime($date_unhandled);
                        $new_date = date('Y-m-d H:i:s', $date);

                        $tx['created_at']      = $new_date;
                        $tx['updated_at']      = $new_date;

                        // Map Currency
                        if($row[1]) {
                            $currency = Currency::where('short_code', $row[1])->first();

                            if($currency) {
                                $tx['currency_id'] = $currency->id;
                            }

                            $currencyRate = CurrencyPricing::where('currency_id',$row[1])->first();
                            if($currencyRate){
                                $tx['exchange_rate']	= floatval($currencyRate->rate);
								$tx['total_amount']		= $row[2] / $currencyRate;
                            }
                            else{
                                $tx['exchange_rate'] = 1;
                            }
                        }

                        $loyalty = Loyalty::where('partner_id',$tx['partner_id'])->first();
                        if($loyalty){
                            $reward_usd = floatval($loyalty->reward_usd);
                            $reward_pts = floatval($loyalty->reward_pts);
                            $total = floatval($tx['total_amount']);
                            $totalpoints = $total*$reward_pts;
                            $totalpoints = $totalpoints/$reward_usd;
                            $tx['points_rewarded'] = $totalpoints;
                        }


                        $validator = Validator::make($tx, array(
                            //'id'         => 'required|numeric',
                            'partner_id' => 'required|numeric',
                            'store_id'   => 'required|numeric',
                            'user_id'    => 'required|numeric'
                        ));
                        if(!$validator->fails()) {

                                $params = array(
                                    'user_id'     => $tx['user_id'],
                                    'amount'      => $tx['total_amount'],
                                    'original_amount'      => $tx['original_total_amount'],
                                    'currency'    => $row[1],
                                    'partner'     => $tx['partner_id'],
                                    'store_id'    => $tx['store_id'], // Optional
                                    'notes'       => "Points Received", // Optional
                                    'created_at'  => $tx['created_at'],
                                    'updated_at'  => $tx['updated_at'],
                                    'reference'   => $fileNameOriginal
                                );

                            Transaction::doReward($params, 'Amount');
                        }

//                        if(!$validator->fails()) {
//                            DB::table('transaction')->insert($tx);
//                        }
                    }

                }
                return Redirect::to('import')->with('success','File Imported Successfully!');
                }

                return Redirect::to('import')->with('success','File Is EMPTY!');
            }
            if($ext == 'TXT' || $ext == 'txt' && strstr($fileNameOriginal,'OM_Mobile_Phones') ){
                Input::file('file')->move($folderPath, $fileNameGenerated);
                $filePath = $folderPath.'/'.$fileNameGenerated;
                $contents = File::get($filePath);
                $contents = str_replace(array("\r\n", "\r"), "\n", $contents);
                $contentLines = explode("\n", $contents);
                $arrContent = array();
                foreach ($contentLines as $lineread){
                    $arrContent[] = explode(",", $lineread);
                }
                if(!empty($arrContent)){
                    foreach($arrContent as $row){
                        if(isset($row[0]) && $row[0] !== ''){
                            //echo "<pre>";
                            //var_dump($row);

                            $user= User::find($row[1]);
                            $userChange['normalized_mobile'] = "+" . $row[0];
                            $userChange['mobile'] = substr($userChange['normalized_mobile'],4);
                            $userChange['telephone_code'] = "+961";

                            $updateChange = User::where('id',$row[1])->update($userChange);
                            //exit();

                        }
                    }
                }
            }


            return Redirect::to('import')->with('success','File Imported Successfully!');


        }
    }

} // EOC
