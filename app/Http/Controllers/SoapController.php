<?php
namespace App\Http\Controllers;
/**
 * API Controller
 *
 * @category   Controllers
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class SOAPController extends BaseController
{
	private $networks;
	private $partner;
	private $user;
        private $server;

    /**
     * Initiate the controller and ensure the partner request is authenticated
     *
     * @return void
     */
    public function __construct()
    {
		$api_key = Input::get('api_key', null);

		if(empty($api_key)) {
			App::abort(401);
		}

		$this->user = User::where('api_key', $api_key)->first();

		if(!$this->user) {
			App::abort(404);
		} else {
			Auth::loginUsingId($this->user->id);

			// Assign scopes

			$this->partner = $this->user->partners()->first();
			$this->network = $this->partner->firstNetwork();

			if(!$this->partner) {
				App::abort(403, "No partner associated to request $api_key");
			}
		}
//                $server = new soap_server(true);
//
//
//                //register a function that works on server
//                $server->register('Message');

    }

    //TRYING OUT SOAP
    public function Message($your_name)
    {
        if(!$your_name){
            return new soap_fault('Client','','Put Your Name!');
        }
        $result = "Welcome to ".$your_name .". Thanks for Your First Web Service Using PHP with SOAP";
        $server->service($HTTP_RAW_POST_DATA);
        exit();
        return $result;
    }

    public function postTest(){
        $ralph = Input::all();
        $file = Input::file('file1');
        $ext  = $file->getClientOriginalExtension();
        $fileNameOriginal = $file->getClientOriginalName();

        $fileNameGenerated = PathHelper::generateFileBaseName($fileNameOriginal, $ext) . '.' . $ext;
        $folderPath = PathHelper::incoming('misc');

        Input::file('file1')->move($folderPath, $fileNameGenerated);
        $filePath = $folderPath.'/'.$fileNameGenerated;
        $contents = File::get($filePath);
        echo "<pre>";
        var_dump($ralph);
        var_dump($contents);

        exit();
        //Give it value at parameter
        $param = array( 'your_name' => 'Monotosh Roy');
        //Create object that referer a web services
        $client = new soapclient('http://www.bluuniverse.com/soap/');
        //Call a function at server and send parameters too
        $response = $client->call('Message',$param);
        //Process result
        if($client->fault)
        {
            echo "FAULT: <p>Code: (".$client->faultcode."</p>";
            echo "String: ".$client->faultstring;
        }
        else
        {
            echo $response;
        }
    }

    /**
     * Generic response handler for API requests
     *
     * @param int $status
     * @param string $message
     * @param object $objects
     * @param string $class_name
     * @return Response
     */
    private static function respondWith($status, $message, $object = null, $class_name = '')
    {

        $response = array();

        if($message) {
            $response['message'] = $message;
        }

		if(!empty($object)) {
	        if($object && is_object($object)) {

				if(get_class($object) != 'stdClass') {
					$class_name = get_class($object);
				}

				if(method_exists($object, 'toArray')) {
	            	$response[$class_name] = $object->toArray();
				} else {
		            $response[$class_name] = $object;
				}
	        } else {
	            $response[$class_name] = $object;
	        }
		} else {
			$response[$class_name] = array();
		}

		$response['status'] = $status;

        if($status == 500) {
            Log::warning($message);
        }

        return Response::make($response, $status);
    }

	/**
	 * Helper method to retrieve a member using the varienty of credentials
	 *
	 * @param string $credential
	 * @param int $pincode
	 * @return Mixed
	 */
    private function memberFromCredential($credential, $pincode)
    {
    	$user = null;

		if(InputHelper::isEmail($credential)) {
			$user = User::where('email', $credential)->where('passcode', $pincode)->where('draft', false)->first();
		}

		if(InputHelper::isReference($credential)) {
			$user = User::where('passcode', $pincode)->where('draft', false)->whereHas('references', function($q) use($credential) {
				$q->where('number', $credential);
			})->first();
		}

		// Last attempt as mobile number

		if(empty($user)) {
                        if(substr($credential,0,2) == '00'){
                            $credential = ltrim($credential,'0');
                            $credential = '+' . $credential;
                        }
			if($credential[0] == '+') {
				$user = User::where('normalized_mobile', $credential)->where('passcode', $pincode)->where('draft', false)->first();
			} else {
				$user = User::where('mobile', InputHelper::sanitizeMobile($credential))->where('passcode', $pincode)->where('draft', false)->first();
			}
		}

		return $user;
    }


    /**
     * Helper method to retrieve a member using the varienty of credentials
     *
     * @param $lat1 as latitude of place 1
     * @param $lon1 as longitude of place 1
     * @param $lat2 as latitude of place 2
     * @param $lon2 as longitude of place 2
     * @param $unit "K" for kilometers and "m" for miles
     * @return decimal distance
     */
    private function distance($lat1, $lon1, $lat2, $lon2, $unit = "K")
        {
           $theta = $lon1 - $lon2;
           $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
           $dist = acos($dist);
           $dist = rad2deg($dist);
           $miles = $dist * 60 * 1.1515;
           $unit = strtoupper($unit);

           if ($unit == "K")
           {
              return ($miles * 1.609344);
           }
           else
           {
              return $miles;
           }
        }

    /**
     * Perform a user authentication and return the profile
     *
     * POST /api/authenticate-user
     *
     * @return Response
     */
	public function postAuthenticateUser()
	{
		$credential = Input::get('credential');
		$pincode    = Input::get('pincode');

		$user = $this->memberFromCredential($credential, $pincode);

		// Check for general success

		if(!$user) {
            return self::respondWith(
                404, "We could not locate the specified account"
            );
		}

        $balance = $user->balance($this->partner->firstNetwork()->id);

        $user = $user->toArray();

        $user['balance'] = $balance;

        return self::respondWith(
            200, null, $user, 'user'
        );
	}

    /**
     * Create a new user account in draft state
     *
     * POST /api/create-draft-user
     *
     * @param POST mobile
     * @param POST country_id
	 * @return Response
	 */
	public function postCreateDraftUser()
	{
		$country = Country::find(Input::get('country_id'));

		$user = new User();

        $user->ucid     = UCID::newCustomerID();
		$user->draft    = true;
		$user->passcode = PinCode::newCode();
		$user->mobile   = InputHelper::sanitizeMobile(Input::get('mobile'));

		if($country) {
			$user->normalized_mobile = InputHelper::normalizeMobileNumber($user->mobile, $country->telephone_code);
			$user->telephone_code    = $country->telephone_code;
			$user->country_id 		 = $country->id;
		}

		$user->country;

		$user->save();

		$user = $user->toArray();

		$user['balance'] = 0;

        return self::respondWith(
            200, null, $user, 'user'
        );
	}

    /**
     * Update an existing user account & notify the members if in draft state
     *
     * POST /api/update-user/{user_id}
     *
     * @param POST first_name
     * @param POST last_name
     * @param POST email
     * @param POST gender
     * @param POST dob_day
     * @param POST dob_month
     * @param POST dob_year
     * @param POST country_id
     * @param POST city_id
     * @param POST area_id
     * @param POST marital_status
     * @param POST num_children
     * @param POST income_bracket
     * @param POST occupation_id
     * @return Response
     */
    public function postUpdateUser($user_id)
    {
        $validator = Validator::make(Input::all(), array(
            'first_name'     => 'required|min:2',
            'last_name'      => 'required|min:2',
			'email' 		 => 'required|email',
			'dob_day' 	     => 'required',
			'dob_month' 	 => 'required',
			'dob_year' 		 => 'required',
			'city_id'	     => 'required',
			'area_id'     	 => 'required'
        ));

    	if($validator->fails()) {
            return self::respondWith(
                400, null, $validator->messages()->all(), 'errors'
            );
	    }

        $dob = date("Y-m-d", strtotime(
            Input::get('dob_day').'-'.Input::get('dob_month').'-'.Input::get('dob_year')
        ));

        $user = User::find($user_id);

        if(Input::has('telephone_code')) {
			$user->telephone_code = Input::get('telephone_code');
        }

        if(Input::has('mobile')) {
			$user->mobile = Input::get('mobile');
        }

        if(Input::has('country_id')) {
			$user->country_id = Input::get('country_id');
        }

		$user->first_name     = Input::get('first_name');
		$user->last_name      = Input::get('last_name');
		$user->email          = Input::get('email');
		$user->gender         = Input::get('gender', 'm');
		$user->area_id        = Input::get('area_id');
		$user->city_id        = Input::get('city_id');
		$user->marital_status = Input::get('marital_status', 'single');
		$user->num_children   = Input::get('num_children', 0);
		$user->income_bracket = Input::get('income_bracket', '$500 - $1,000');
		$user->occupation_id  = Input::get('occupation_id', 0);
		$user->dob            = $dob;
		$user->status         = 'active';

		$user->normalized_mobile = InputHelper::normalizeMobileNumber($user->mobile, $user->telephone_code);

		if($user->draft) {
	        $user->sendWelcomeMessage();
			$user->draft = false;

			// Automatically associate to partners

			/* BLU */
			DB::table('partner_user')->insert(array(
				'partner_id' => 1,
				'user_id'	 => $user_id
			));

			/* WHITELABEL PARTNER */
			if($this->partner->id != 1) {
				DB::table('partner_user')->insert(array(
					'partner_id' => $this->partner->id,
					'user_id'	 => $user_id
				));
			}
		} else {
			AuditHelper::record(
				$this->partner->id, "'{$user->id} / {$user->email}' Member profile updated via website"
			);
		}

		$user->ref_account = false;

        $user->save();

        $balance = $user->balance($this->partner->firstNetwork()->id);

        $user = $user->toArray();

        $user['balance'] = $balance;

        return self::respondWith(
            200, null, $user, 'user'
        );
    }

    /**
     * Activate a user account
     *
     * POST /api/activate-user
     *
     * @param POST user_id
     * @param POST passcode
     * @return Response
     */
    public function postActivateUser()
    {
		$user_id = Input::get('user_id');

        $user = User::where('id', $user_id)->first();

        if(!$user) {
            return self::respondWith(
                500, "Could not locate user with id {$user_id} in database"
            );
        }

        $passcode = Input::get('passcode');

        if($user->passcode != $passcode) {
            return self::respondWith(
                500, "Invalid passcode provided"
            );
        }

		$user->verified = true;
		$user->draft    = false;
		$user->status   = 'active';
        $user->save();

        // Fetch the network balance

        $balance = $user->balance($this->partner->firstNetwork()->id);

        $user = $user->toArray();

        $user['balance'] = $balance;

        return self::respondWith(
            200, null, $user, 'user'
        );
    }

    /**
     * Fetch a user profile
     *
     * GET /api/user/{user_id}
     *
     * @param int $user_id
     * @return Response
     */
    public function getUser($user_id)
    {
        $user = User::find('id', $user_id)->with('country', 'city', 'area', 'cards');

        if(!$user) {
            return self::respondWith(
                500, "Could not locate user with id {$user_id} in database"
            );
        }

		// Split birthday into components

		$birthday = array(
			'day' 	=> date('j', strtotime($user->dob)),
			'month' => date('n', strtotime($user->dob)),
			'year'  => date('Y', strtotime($user->dob)),
		);

		// Ammend personal attribution

		$personal = array(
			'full_name'   => $user->name,
			'full_number' => $user->normalizedContactNumber()
		);

		// Ammend network balance

        $balance = $user->balance($this->partner->firstNetwork()->id);

		$results = $user->toArray();

		$results['personal'] = $personal;
		$results['birthday'] = $birthday;
		$results['balance']  = $balance;

        return self::respondWith(
            200, null, $results, 'user'
        );
    }


	/**
	 * Temporarily block a member from attempting to authenticate
	 *
	 * POST /api/block-member
	 *
	 * @param POST credential
	 * @return Response
	 */
	public function postBlockMember()
	{
		$credential = Input::get('credential');

		if(InputHelper::isEmail($credential)) {
			$user = User::where('email', $credential)->where('draft', false)->first();
		}

		if(InputHelper::isReference($credential)) {
			$user = User::where('draft', false)->whereHas('references', function($q) use($credential) {
				$q->where('number', $credential);
			})->first();
		}

		// Last attempt as mobile number

		if(empty($user)) {
			if($credential[0] == '+') {
				$user = User::where('normalized_mobile', $credential)->where('draft', false)->first();
			} else {
				if((int)$credential > 0) {
					$user = User::where('mobile', InputHelper::sanitizeMobile($credential))->where('draft', false)->first();
				}
			}
		}

        if (!$user) {
            return self::respondWith(
                500, "Could not locate user with credential {$credential} in database"
            );
        }

        // Set the block params

		$user->blocked      = true;
		$user->blocked_date = date("Y-m-d H:i:s", time());

		$user->save();

		// Notify the user

		$sms_body = View::make("sms.account-blocked", array(
			'user' => $user,
		))->render();

		SMSHelper::send($sms_body, $user, $user->firstPartner());

		// Send the Email
        $sendgrid = new SendGrid(
            Config::get('sendgrid.username'), Config::get('sendgrid.password')
        );

        $email_template = View::make("emails.account-blocked", array(
            'user' => $user
        ))->render();

        $mail = new SendGrid\Email();

        $mail->addTo($user->email);
        $mail->setFrom(Config::get('blu.email_from'));
		$mail->setFromName('BLU Points');
        $mail->setSubject("[BLU] Account Temporarily Locked");
        $mail->setHtml($email_template);

        $sendgrid->send($mail);

		return self::respondWith(
            200, null, null
        );
	}

    /**
     * Validate a user pin code
     *
     * GET /api/validate-pin
     *
	 * @param GET user_id
	 * @param GET pin
     * @return Response
     */
	public function getValidatePin($user_id, $pin)
	{
		$user = User::where('id', $user_id)->where('passcode', $pin);

		if($user->count() > 0) {
			return self::respondWith(
	            200, null, array('valid' => true), 'user'
	        );
		} else {
			return self::respondWith(
	            200, null, array('valid' => false), 'user'
	        );
		}
	}

    /**
     * Request the user balance
     *
     * GET /api/user-balance/{user_id}
     *
	 * @param GET user_id
     * @return Response
     */
	public function getUserBalance($user_id = null)
	{
		$user = User::find($user_id);

        if (!$user) {
            return self::respondWith(
                500, "The PIN provided is not valid"
            );
        }

        return self::respondWith(
            200, null, array('balance' => $user->balance($this->partner->firstNetwork()->id)), 'user'
        );
	}

    /**
     * Fetch a list of all user transactions
     *
     * GET /api/user-transactions/{user_id}?day_range=30
     *
	 * @param GET user_id
     * @return Response
     */
	public function getUserTransactions($user_id = null)
	{
		$user = User::where('id', $user_id)->first();

        if (!$user) {
            return self::respondWith(
                500, "Could not locate user with id {$user_id} in database"
            );
        }

		$day_range = Input::get('day_range', 30);

		$trans_date = date("Y-m-d H:i:s", strtotime("-$day_range days", time()));

		$transactions = Transaction::where('created_at', '>=', $trans_date)->where('user_id', $user_id)->with('partner', 'currency')->orderBy('id', 'DESC');

		return self::respondWith(
            200, null, $transactions->get()->toArray(), 'transactions'
        );
	}

    /**
     * Perform the account recovery action
     *
     * GET /api/recover-account/{country_code}/{mobile}
     *
	 * @param GET int_mobile
     * @return Response
     */
	public function getRecoverAccount($country_code = null, $mobile = null)
	{
		$normalized_mobile = InputHelper::normalizeMobileNumber($mobile, $country_code);

		$user = User::where('normalized_mobile', $normalized_mobile)->first();

        if (!$user) {
            return self::respondWith(
                500, "Could not locate user with number {$normalized_mobile} in database"
            );
        }

		$user->sendAccountPin();

        return self::respondWith(
            200, null
        );
	}

    /**
     * Link an existing card to a user account
     *
     * POST /api/link-user-cards
     *
     * @param POST $user_id
     * @return Response
     */
    public function postLinkUserCards()
    {
		$user_id = Input::get('user_id');

        $user = User::find($user_id);

        if(!$user) {
            return self::respondWith(
                500, "Could not locate user with id {$user_id} in database"
            );
        }

        DB::beginTransaction();

		DB::table('card')->where('user_id', $user_id)->update(array(
			'user_id' => null
		));

		$cards = Input::get('cards');

		$valid_cards = array();
		$invalid_cards = array();

		foreach($cards as $c) {
			$card = Card::where('number', $c)->first();

			if($card) {
		        $card->user_id = $user->id;
		        $card->save();

				$valid_cards[] = $c;
			} else {
				$invalid_cards[] = $c;
			}
		}

        DB::commit();

        $buffer = array(
        	'valid_cards'   => $valid_cards,
        	'invalid_cards' => $invalid_cards
        );

        $count = count($buffer['valid_cards']);

        if($count > 0) {
			AuditHelper::record(
				$this->partner->id, "'{$user->id} / {$user->email}' {$count} Cards successfully associated to member account"
			);
        }

        return self::respondWith(
            200, null, $buffer, 'cards'
        );
    }

    /**
     * Perform a change pin request
     *
     * POST /api/change-pin/{user_id}
     *
	 * @param GET user_id
     * @return Response
     */
	public function postChangePin($user_id)
	{
		$old_pin = Input::get('old_pin');
		$new_pin = Input::get('new_pin');

        DB::beginTransaction();

		$user = User::where('passcode', $old_pin)->where('id', $user_id)->first();

        if (!$user) {
            return self::respondWith(
                500, "The PIN provided is not valid"
            );
        }

		// Update the pins

		$user->passcode         = $new_pin;
		$user->confirm_passcode = true;
		$user->save();

		if($new_pin != $old_pin) {

			// Send the SMS
			$sms_body = View::make("sms.pin-change", array(
				'user' => $user,
			))->render();

			SMSHelper::send($sms_body, $user, $user->firstPartner());

			// Send the Email

	        $sendgrid = new SendGrid(
	            Config::get('sendgrid.username'), Config::get('sendgrid.password')
	        );

	        $email_template = View::make("emails.pin-changed", array(
	            'user' => $user
	        ))->render();

	        $mail = new SendGrid\Email();

	        $mail->addTo($user->email);
	        $mail->setFrom(Config::get('blu.email_from'));
			$mail->setFromName('BLU Points');
	        $mail->setSubject("[BLU] PIN Change Notification");
	        $mail->setHtml($email_template);

	        $sendgrid->send($mail);

	        // Record the activity

			AuditHelper::record(
				$this->partner->id, "'{$user->id} / {$user->email}' updated their own PIN code successfully via the website"
			);
		}

        DB::commit();

        return self::respondWith(
            200, null
        );
	}

    /**
     * Determine if the mobile number is unique
     *
     * GET /api/unique-mobile/{mobile}
     *
	 * @param GET user_id (Optional)
     * @return Response
     */
    public function getUniqueMobile($mobile)
    {
		$user_id = Input::get('user_id');

		$count = 0;

		if(!empty($user_id)) {
			$count = DB::table('users')->where('normalized_mobile', "+$mobile")->where('id', '!=', $user_id)->where('draft', false)->count();
		} else {
			$count = DB::table('users')->where('normalized_mobile', "+$mobile")->where('draft', false)->count();
		}

		if($count > 0) {
	        return self::respondWith(
	            200, null, array('unique' => false), 'response'
	        );
		} else {
	        return self::respondWith(
	            200, null, array('unique' => true), 'response'
	        );
		}
    }

    /**
     * Determine if the email address is unique
     *
     * GET /api/unique-email/{email}
     *
     * @return Response
     */
    public function getUniqueEmail($email)
    {
		$count = DB::table('users')->where('email', $email)->where('draft', false)->count();

		if($count > 0) {
	        return self::respondWith(
	            200, null, array('unique' => false), 'response'
	        );
		} else {
	        return self::respondWith(
	            200, null, array('unique' => true), 'response'
	        );
		}
    }

    /**
     * Transfer a members points to another user
     *
     * POST /api/transfer-points
     *
     * @return Response
     */
	public function postTransferPoints()
	{
		$user_id      = Input::get('user_id');
		$user_pin     = Input::get('user_pin');
		$trans_amount = Input::get('trans_amount', 0);
		$dest_mobile  = Input::get('dest_mobile', 0);

        $user = User::find($user_id);

        if(!$user) {
            return self::respondWith(
                500, "Could not locate user with id {$user_id} in database"
            );
        }

		if($trans_amount == 0) {
            return self::respondWith(
                500, "Please provide an amount larger than 0 to transfer"
            );
		}

		$tax = round($trans_amount * 0.05);

		if($user->balance($this->partner->firstNetwork()->id) < ($trans_amount + $tax)) {
            return self::respondWith(
                500, "User balance is insufficient for the requested transaction"
            );
		}

		if($user->passcode != $user_pin) {
            return self::respondWith(
                500, "The user PIN is invalid"
            );
		}

		$destination = User::where('normalized_mobile', '=', "+$dest_mobile")->first();

		if(!$destination) {
            return self::respondWith(
                500, "The destination member does not exist in the system"
            );
		}

		if($destination->id == $user->id) {
            return self::respondWith(
                500, "You are unable to transfer points to yourself"
            );
		}

		if($destination->status != 'active') {
            return self::respondWith(
                500, "The destination member is not currently active"
            );
		}

		// Sender (Redemption)

		$sender_partner = $user->getTopLevelPartner();

        $params = array(
            'user_id'   => $user->id,
            'partner'   => $sender_partner->id,
            'points'    => ($trans_amount + $tax),
            'source'    => "Website",
            'reference' => "Transfer points to {$destination->name}",
            'notes'     => "Points Transfer"
        );

        Transaction::doRedemption($params, "Points");

		// Receiver (Reward)

		$receiver_partner = $destination->getTopLevelPartner();

        $params = array(
            'user_id'     => $destination->id,
            'points'      => $trans_amount,
            'partner'     => $receiver_partner->id,
            'reference'   => "Transfer points from {$user->name}",
            'notes'       => "Points Received"
        );

        Transaction::doReward($params, 'Amount');

        $balance = $user->balance($this->partner->firstNetwork()->id);

        $user = $user->toArray();

        $user['balance'] = $balance;

		AuditHelper::record(
			$this->partner->id, "'{$user->id} / {$user->email}' Transferred {$trans_amount} points to '{$destination->id} / {$destination->email}'"
		);

        return self::respondWith(
            200, null, $user, 'user'
        );
	}

    /**
     * Map a product to the user as a favourite
     *
     * POST /api/add-favourite
     * @param POST user_id
     * @param POST product_id
     *
     * @return Response
     */
	public function postAddFavourite()
	{
		$favorite = new Favorite();

		$favorite->user_id    = Input::get('user_id');
		$favorite->product_id = Input::get('product_id');


		$favorite->save();

        return self::respondWith(
            200, null, $favorite->toArray(), 'favorite'
        );
	}

	/**
     * Retrieve a list of favourites
     *
     * GET /api/move-to-favorites?user_id=1&product_id=1
     *
	 * @param GET user_id
	 * @param GET product_id
     * @return Response
     */
	public function getMoveToFavorites()
	{
		$user_id    = Input::get('user_id');
		$product_id = Input::get('product_id');

        $user = User::where('id', $user_id)->first();

        if(!$user) {
            return self::respondWith(
                404, "Could not locate user with id {$user_id} in database"
            );
        }

		// Remove from cart
		DB::table('cart')->where('user_id', $user_id)->where('product_id', $product_id)->delete();

		// Add to favorites
		$favorite = new Favorite();

		$favorite->user_id    = $user_id;
		$favorite->product_id = $product_id;

		$favorite->save();

		return self::respondWith(
            200, null, $favorite->toArray(), 'favorite'
        );
	}

	/**
     * Retrieve a list of favourites
     *
     * GET /api/move-to-cart?user_id=1&product_id=1
     *
	 * @param GET user_id
	 * @param GET product_id
         * @param GET quantity
     * @return Response
     */
	public function getMoveToCart()
	{
		$user_id    = Input::get('user_id');
		$product_id = Input::get('product_id');

                $quantity = 1 ;
                if(Input::has('quantity')){
                    $quantity = Input::get('quantity');
                }


        $user = User::where('id', $user_id)->first();

        if(!$user) {
            return self::respondWith(
                404, "Could not locate user with id {$user_id} in database"
            );
        }

		$product = Product::find($product_id);

        if(!$product) {
            return self::respondWith(
                404, "Could not locate product with id {$user_id} in database"
            );
        }

		// Remove from cart
		DB::table('favorite')->where('user_id', $user_id)->where('product_id', $product_id)->delete();

		// Add to Cart

		$cart = new Cart();

		$cart->user_id    	    = $user_id;
		$cart->product_id 	    = $product_id;
		$cart->price_in_points  = $product->price_in_points;
		$cart->delivery_charges = $product->delivery_charges;
		$cart->delivery_options = $product->delivery_options;
		$cart->quantity 		= $quantity; // Default to 1, allow later modification

		$cart->save();

		return self::respondWith(
            200, null, $cart->toArray(), 'cart'
        );
	}

	/**
     * Remove an item from a users list of favorites
     *
     * GET /api/remove-from-favorites?user_id=1&product_id=1
     *
	 * @param GET user_id
	 * @param GET product_id
     * @return Response
     */
	public function getRemoveFromFavorites()
	{
		$user_id    = Input::get('user_id');
		$product_id = Input::get('product_id');

        $user = User::where('id', $user_id)->first();

        if(!$user) {
            return self::respondWith(
                404, "Could not locate user with id {$user_id} in database"
            );
        }

		DB::table('favorite')->where('user_id', $user_id)->where('product_id', $product_id)->delete();

		return self::respondWith(
            200, null
        );
	}

    /**
     * Retrieve a list of favourites
     *
     * GET /api/favorites/{user_id}
     *
     * @return Response
     */
	public function getFavorites($user_id = null)
	{
        $user = User::where('id', $user_id)->first();

        if(!$user) {
            return self::respondWith(
                404, "Could not locate user with id {$user_id} in database"
            );
        }

		$favorites = $user->favorites();
		$favorites->join('product', 'favorite.product_id', '=', 'product.id');
		$favorites->orderBy('favorite.created_at', 'ASC');

		return self::respondWith(
            200, null, $favorites->get()->toArray(), 'favorites'
        );
	}

    /**
     * Add item to the users global cart
     *
     * GET /api/add-cart?user_id=1
     *
     * @param POST product_id
     * @param POST user_id
     * @param POST quantity
     * @return Response
     */
	public function postAddCart()
	{
		$product_id = Input::get('product_id');
		$user_id    = Input::get('user_id');

                $quantity = 1;
                if(Input::has('quantity')){
                    $quantity = Input::get('quantity');
                }

        $product    = Product::where('id', $product_id)->first();

        if(!$product) {
            return self::respondWith(
                404, "Could not locate product with id {$product_id} in database"
            );
        }

        DB::beginTransaction();

		$cart = new Cart();

		$cart->user_id    	    = $user_id;
		$cart->product_id 	    = $product_id;
		$cart->price_in_points  = $product->price_in_points;
		$cart->delivery_charges = $product->delivery_charges;
		$cart->delivery_options = $product->delivery_options;
		$cart->quantity 		= $quantity; // Default to 1, allow later modification

		$cart->save();

		DB::commit();

        return self::respondWith(
            200, null, $cart->toArray(), 'cart'
        );
	}

    /**
     * Fetch a users cart contents
     *
     * GET /api/fetch-user-cart?user_id=1
     *
     * @return Response
     */
	public function getFetchUserCart()
	{
		$user_id = Input::get('user_id');

        $user = User::find($user_id);

        if(!$user) {
            return self::respondWith(
                404, "Could not locate user with id {$user_id} in database"
            );
        }

		$results = $user->cartItems();

		$results->select(
			'product.id',
			'product.name',
			'product.model',
			'product.sub_model',
			'product.cover_image',
			'product.description',
			'cart.price_in_points',
			'cart.quantity',
			'cart.delivery_options',
			'cart.delivery_charges'
		);

		$results->join('product', 'cart.product_id', '=', 'product.id');
		$results->orderBy('cart.created_at');

		return self::respondWith(
            200, null, $results->get()->toArray(), 'items'
        );
	}

    /**
     * Delete the items from the users cart
     *
     * GET /api/delete-user-cart?user_id=1
     *
     * @return Response
     */
	public function getEmptyUserCart()
	{
		$user_id = Input::get('user_id');

		DB::table('cart')->where('user_id', $user_id)->delete();

		return self::respondWith(
            200, null
        );
	}

    /**
     * Delete the items from the users cart
     *
     * GET /api/remove-from-cart?user_id={1}&item_id={id}
     *
	 * @param GET $user_id
	 * @param GET $product_id
     * @return Response
     */
	public function getRemoveFromCart()
	{
		$user_id    = Input::get('user_id');
		$product_id = Input::get('product_id');

		DB::table('cart')->where('user_id', $user_id)->where('product_id', $product_id)->delete();

		return self::respondWith(
            200, null
        );
	}

    /**
     * Fetch a list of partner products and its hierarchy
     *
     * GET /api/partner-products/{partner_id}?page=1
     * GET /api/partner-products/{partner_id}?page=1&minp=1
     * GET /api/partner-products/{partner_id}?page=1&maxp=10
     * GET /api/partner-products/{partner_id}?page=1&q=query
     * GET /api/partner-products/{partner_id}?page=1&cat=1,2,3,4
     * GET /api/partner-products/{partner_id}?page=1&sort=id DESC
     * GET /api/partner-products/{partner_id}?page=1&country_id=1
     *
     * @param int $partner_id
     * @return Response
     */
    public function getPartnerProducts($partner_id)
    {
        $partner = Partner::find($partner_id);

        if(!$partner) {
            return self::respondWith(
                500, "Could not locate user with id {$partner_id} in database"
            );
        }
                $results = Product::leftJoin('media', 'product.cover_image', '=', 'media.id');
                $results->select('product.*', 'media.name as image_name');
		$results->where('draft', false);
		$results->where('display_online', true);
		$results->where('status', 'active');

		// Restrict to partner hierarcy

		$results->whereHas('partners', function($q) use($partner) {
			$q->whereIn('partner_id', $partner->childHierarchy());
		});

		if(Input::get('section') == 'deals') {
			$results->where('hot_deal', true);
		} else {
			$results->where('hot_deal', false);
		}

		// Add parameters

		if(Input::has('cat')) {
			$results->whereIn('product.category_id', explode(',', urldecode(Input::get('cat'))));
		}

		if(Input::has('sort')) {
			$sorting = explode(',', urldecode(Input::get('sort')));

			foreach($sorting as $s) {
				switch($s) {
					case "newest":
						$results->orderBy('id', 'DESC');
						break;
					case "oldest":
						$results->orderBy('id', 'ASC');
						break;
					case "alphabetical_asc":
						$results->orderBy('name', 'ASC');
						break;
					case "alphabetical_desc":
						$results->orderBy('name', 'DESC');
						break;
					case "points_asc":
						$results->orderBy('price_in_points', 'ASC');
						break;
					case "points_asc":
						$results->orderBy('price_in_points', 'DESC');
						break;
				}
			}
		}

		if(Input::has('q')) {
			$results->where('product.name', 'like', '%'.Input::get('q').'%');
		}

		if(Input::has('minp')) {
			$results->where('price_in_points', '>=', Input::get('minp'));
		}

		if(Input::has('maxp')) {
			$results->where('price_in_points', '<=', Input::get('maxp'));
		}

		// Factor in location

		if(Input::has('country_id')) {
			if(Input::get('country_id') != 0) {
				$results->whereHas('countries', function($q) {
				    $q->where('country_id', Input::get('country_id'));
				});
			}
		}

		// Factor in display channels

		$results->whereHas('channels', function($q) {
		    $q->where('channel_id', $this->partner->id);
		});

		// Construct the response with automated pagination

		$buffer = $results->paginate(16);

		$result_set = new \stdClass();
		$result_set->results 	  = null;
		$result_set->currentPage  = $buffer->getCurrentPage();
		$result_set->lastPage     = $buffer->getLastPage();
		$result_set->totalResults = $buffer->getTotal();

        return self::respondWith(
            200, null, $buffer->toArray(), 'results'
        );
    }

    /**
     * Fetch a list of products from an array of provided ID's
     *
     * GET /api/fetch-products
     *
	 * @param GET $ids
     * @return Response
     */
	public function getFetchProducts()
	{
		$ids = Input::get('ids');

		$results = DB::table('product');
                $results->leftJoin('media', 'product.cover_image', '=', 'media.id');
                $results->select('product.*', 'media.name as image_name');
		$results->whereIn('product.id', $ids);
		$results->orderBy('name', 'ASC');
		$results->where('display_online', true);
		$results->where('status', 'active');

        return self::respondWith(
            200, null, $results->get(), 'products'
        );
	}

    /**
     * Fetch a list of partners that can be displayed on another partners site
     *
     * GET /api/partner-channels?partner_id={id}
     *
	 * @param GET $partner_id
     * @return Response
     */
	public function getPartnerChannels()
	{
		$partner_id = Input::get('partner_id');

        $partner = Partner::where('id', $partner_id)->first();

        if(!$partner) {
            return self::respondWith(
                500, "Could not locate user with id {$partner_id} in database"
            );
        }

		$results = $partner->channels();

		$results->join('partner', 'channel_partner.channel_id', '=', 'partner.id');
		$results->where('partner.draft', false);
		$results->where('partner.display_online', true);
		$results->where('partner.status', "Enabled");

		if(Input::has('sort')) {

			$sorting = explode(',', urldecode(Input::get('sort')));

			foreach($sorting as $s) {
				switch($s) {
					case "newest":
						$results->orderBy('partner.id', 'ASC');
						break;
					case "oldest":
						$results->orderBy('partner.id', 'DESC');
						break;
					case "alphabetical_asc":
						$results->orderBy('partner.name', 'ASC');
						break;
					case "alphabetical_desc":
						$results->orderBy('partner.name', 'DESC');
						break;
				}
			}
		}

		if(Input::has('q')) {
			$results->where('name', 'like', '%'.Input::get('q').'%');
		}

		$buffer = $results->paginate(16);

		$result_set = new \stdClass();
		$result_set->results 	  = null;
		$result_set->currentPage  = $buffer->getCurrentPage();
		$result_set->lastPage     = $buffer->getLastPage();
		$result_set->totalResults = $buffer->getTotal();

        return self::respondWith(
            200, null, $buffer->toArray(), 'results'
        );
	}

    /**
     * Retrieve a partner profile using their ID
     *
     * GET /api/partner-profile?partner_id={id}
     *
	 * @param GET $partner_id
     * @return Response
     */
	public function getPartnerProfile()
	{
		$partner_id = Input::get('partner_id');

		$partner = Partner::with('stores.address')->find($partner_id);

        if(!$partner) {
            return self::respondWith(
                404, "Could not locate partner with id {$partner_id} in database"
            );
        }

        return self::respondWith(
            200, null, $partner->toArray(), 'partner'
        );
	}

    /**
     * Fetch a complete profile of an individual product
     *
     * GET /api/product/{product_id}
     *
	 * @param int $product_id
     * @return Response
     */
	public function getProduct($product_id)
	{
        $product = Product::where('id', $product_id)->with('category')->with('brand')->first();

        if(!$product) {
            return self::respondWith(
                500, "Could not locate product with id {$product_id} in database"
            );
        }

        return self::respondWith(
            200, null, $product->toArray(), 'product'
        );
	}

    /**
     * Return a list of all product categories
     *
     * GET /api/product-categories
     *
     * @return Response
     */
    public function getProductCategories()
    {
        $categories = CategoryHelper::hierarcy();

        return self::respondWith(
            200, null, $categories, 'categories'
        );
    }

    /**
     * Perform a redemption transaction
     *
     * POST /api/redeem-items
     *
     * @param POST user_id
     * @param POST qty
     * @param POST ids
     * @param POST address_1
     * @param POST address_2
     * @param POST partner_id
     * @return Response
     */
	public function postRedeemItems()
	{
		$user_id = Input::get('user_id');

        $user = User::find($user_id);

        if(!$user) {
            return self::respondWith(
                500, "Could not locate user with id {$user_id} in database"
            );
        }

		// Update user details

		$user->address_1 = Input::get('address_1');
		$user->address_2 = Input::get('address_2');
		$user->save();

        $partner_id = Input::get('partner_id');

		// Create transaction

		$ids = Input::get('ids');
		$qty = Input::get('qty');

		$payload = array();

		foreach($ids as $k => $v) {
			$product = new \stdClass();

			$product->id  = $ids[$k];
			$product->qty = $qty[$k];

			$payload[] = $product;
		}

		$params = array(
			'user_id'   => $user->id,
			'partner'   => $partner_id,
			'items'     => $payload,
			'source'    => 'Online Catalogue',
			'reference' => "Online Catalogue Purchase"
		);

		$result = Transaction::doRedemption($params, "Items");

		if($result->status == 200) {
			DB::table('cart')->where('user_id', $user_id)->delete();
		}

        return self::respondWith(
            $result->status, $result->message, $result->transaction, 'transaction'
        );
	}

    /**
     * This returns a list of all the stores for the current user
     *
     * GET /api/stores
     *
     * @return Response
     */
    public function getStores()
    {
        $stores = ManagedObjectsHelper::managedStores();

        return self::respondWith(
            200, null, $stores->toArray(), 'stores'
        );
    }

    /**
     * List all point of sales
     *
     * GET /api/pos?store_id=1
     *
     * @return Response
     */
    public function getPos()
    {
        $store_id = Input::get('store_id');

        $pos = ManagedObjectsHelper::managedPointOfSales();

        if ($store_id) {
            $store = Store::where('id', $store_id)->first();

            if(!$store) {
                return self::respondWith(
                    500, "Could not locate store with id {$store_id} in database"
                );
            }

            $pos = $pos->fieldEqualsValue('store_id', $store_id);
        }

        return self::respondWith(
            200, null, $pos->toArray(), 'pos'
        );
    }

    /**
     * Fetch the structured list of supported locales
     *
     * GET /api/locales
     *
     * @return Response
     */
	public function getLocales()
	{
		$countries = Country::with('areas', 'areas.cities')->orderBy('country.name')->get();

		return self::respondWith(
		    200, null, $countries->toArray(), 'locales'
		);
	}

    /**
     * Return a list of all countries in the system
     *
     * GET /api/countries
     *
     * @return Response
     */
    public function getCountries()
    {
        $countries = Country::orderBy('name')->get();

        return self::respondWith(
            200, null, $countries->toArray(), 'countries'
        );
    }

    /**
     * Return a list of all areas per selected country
     *
     * GET /api/areas?country_id=1
     *
     * @return Response
     */
	public function getAreas()
	{
		$country_id = Input::get('country_id');

        $areas = Area::where('country_id', $country_id)->orderBy('name')->get();

        return self::respondWith(
            200, null, $areas->toArray(), 'areas'
        );
	}

    /**
     * Return a list of all cities per selected area
     *
     * GET /api/cities?area_id=1
     *
     * @return Response
     */
	public function getCities()
	{
		$area_id = Input::get('area_id');

        $cities = City::where('area_id', $area_id)->orderBy('name')->get();

        return self::respondWith(
            200, null, $cities->toArray(), 'cities'
        );
	}

    /**
     * Return a list of all currencies and their exchange rates
     *
     * GET /api/currencies
     *
     * @return Response
     */
    public function getCurrencies()
    {
        $buffer = array();

        foreach(Currency::orderBy('name')->get() as $c) {
            $buffer[] = array(
                'name'       => $c->name,
                'short_code' => $c->short_code,
                'rate'       => $c->latestPrice()->rate
            );
        }

        return self::respondWith(
            200, null, $buffer, 'currencies'
        );
    }

    /**
     * This returns return the number of points that can be earned for a specific currency value
     *
     * GET /api/points-for-spend?short_code=LBP&spend_amount=1000
     * GET /api/points-for-spend?short_code=USD&spend_amount=50
     *
     * @return Response
     */
    public function getPointsForSpend()
    {
        $short_code   = Input::get('short_code');
        $spend_amount = Input::get('spend_amount');

        if(!$spend_amount) {
            return self::respondWith(
                500, "Please specify spend_amount"
            );
        }

        $currency = Currency::where('short_code', $short_code)->first();

        if(!$currency) {
            return self::respondWith(
                500, "Could not locate currency with short code {$short_code} in database"
            );
        }

        $rate    = $currency->latestRate();
        $dollars = $spend_amount / $rate;
        $points  = $dollars * Config::get('blu.points_rate');

        return self::respondWith(
            200, null, $points, "points"
        );
    }

    /**
     * Void a transaction
     *
     * GET /api/void-transaction?transaction_id=1
     *
     * @return Response
     */
    public function postVoidTransaction()
    {
        DB::beginTransaction();

        $transaction_id = Input::get('transaction_id');
        $transaction    = Transaction::find($transaction_id);

        if (!$transaction){
            return self::respondWith(
                500, "Could not locate transaction with id {$transaction_id} in database"
            );
        }

        $transaction->voided = true;
        $transaction->save();

		DB::commit();

        return self::respondWith(
            200, "Transaction successfully voided"
        );
    }

    /**
     * Fetch a list of banner relevant to a partner
     *
     * GET /api/banners?partner_id=1
     * GET /api/banners?partner_id=1&country_id=7
     * GET /api/banners?partner_id=1&country_id=7&page=home
     *
     * @return Response
     */
    public function getBanners()
    {
        $partner_id = Input::get('partner_id');
		$country_id = Input::get('country_id', 0);
		$page 		= Input::get('page', 'home');
        $partner    = Partner::where('id', $partner_id)->first();

        if (!$partner) {
            return self::respondWith(
                500, "Could not locate partner with id {$partner_id} in database"
            );
        }

		$banners = array();

		if($country_id != 0) {
			$banners = $partner->banners()->whereHas('countries', function($q) use($country_id){
				$q->where('country_id', $country_id);
				$q->orWhere('country_id', 0);
			});
		} else {
			$banners = $partner->banners();
		}

		// Fetch per page
		$banners->whereHas('pages', function($q) use($page){
			$q->where('page', $page);
		});

        $buffer = array();

        foreach($banners->get() as $b) {
            $buffer[] = array(
				'id'    => $b->media_id,
            	'url'   => url("/media/image/{$b->media_id}"),
				'title' => $b->title,
				'link'  => $b->link,
				'desc'  => $b->description
            );
        }

        return self::respondWith(
            200, null, $buffer, 'banners'
        );
    }

    /**
     * Fetch a list of occupations
     *
     * GET /api/occupations
     *
     * @return Response
     */
	public function getOccupations()
	{
		$list = Occupation::orderBy('name')->get();

        $buffer = array();

        foreach($list as $i) {
            $buffer[] = array(
				'id'   => $i->id,
            	'name' => $i->name
            );
        }

        return self::respondWith(
            200, null, $buffer, 'occupations'
        );
	}

    /**
     * Fetch the name of a country using it's ID
     *
     * GET /api/country-name
     *
	 * @param GET country_id
     * @return Response
     */
	public function getCountryName()
	{
		$country_id = Input::get('country_id');

		$country = Country::where('id', $country_id)->first();

        if (!$country) {
            return self::respondWith(
                500, "Could not locate country with id {$country_id} in database"
            );
        }

        return self::respondWith(
            200, null, array('name' => $country->name), 'country'
        );
	}

    /**
     * Fetch the name of a country using it's ID
     *
     * GET /api/area-name
     *
	 * @param GET area_id
     * @return Response
     */
	public function getAreaName()
	{
		$area_id = Input::get('area_id');

		$area = Area::where('id', $area_id)->first();

        if (!$area) {
            return self::respondWith(
                500, "Could not locate area with id {$area_id} in database"
            );
        }

        return self::respondWith(
            200, null, array('name' => $area->name), 'area'
        );
	}

    /**
     * Fetch the name of a city using it's ID
     *
     * GET /api/city-name
     *
	 * @param GET city_id
     * @return Response
     */
	public function getCityName()
	{
		$city_id = Input::get('city_id');

		$city = Area::where('id', $city_id)->first();

        if (!$city) {
            return self::respondWith(
                500, "Could not locate city with id {$city_id} in database"
            );
        }

        return self::respondWith(
            200, null, array('name' => $city->name), 'city'
        );
	}

    /**
     * Fetch the short ISO2 country code
     *
     * GET /api/country
     *
	 * @param GET country_id
     * @return Response
     */
	public function getCountry()
	{
		$country_id = Input::get('country_id');

		$country = Country::where('id', $country_id)->first();

		if(!$country) {
            return self::respondWith(
                500, "Could not locate country with id {$country_id} in database"
            );
		}

		return self::respondWith(
            200, null, $country->toArray(), 'country'
        );
	}

    /**
     * Perform a travel transaction confirmation
     *
     * GET /api/travel-transaction/{user_id}
     *
	 * @param POST points
	 * @param POST reference
	 * @param POST note
	 * @param POST travel_type [Flight, Hotel, Car]
	 * @param POST source
	 * @param GET user_id
     * @return Response
     */
	public function postTravelTransaction($user_id)
	{
		$user = User::where('id', $user_id)->first();

        if (!$user) {
            return self::respondWith(
                500, "Could not locate user with id {$user_id} in database"
            );
        }

		$result = Transaction::doRedemption(array(
			'user_id'   => $user->id,
			'partner'   => $this->partner->id,
			'points'    => Input::get('points'),
			'source'    => Input::get('source'),
			'reference' => Input::get('reference'),
			'notes'	    => Input::get('note')
		), Input::get('travel_type', 'Flight'));

		return self::respondWith(
            200, null, $result, 'transaction'
        );
	}






    /**
     * Perform a travel transaction confirmation
     *
     * GET /api/transaction-user/{user_id}
     *
	 * @param POST partner_id (int)
         * @param POST store_id (int)
         * @param POST pos_id (int)
         * @param POST invoiced_at (date)
	 * @param POST reference (string)
         * @param POST total_amount (Decimal)
         * @param POST reward_redeem (Reward = 1 Redeem = 2)
         * @param POST currency (3 character)
	 * @param GET user_id
     * @return Response
     */
	public function postTransactionUser($user_id)
	{
        $tx                    = array();
        $tx['ref_number']      = Input::get('reference');
        $tx['total_amount']    = floatval(Input::get('total_amount'));
        $tx['store_id']        = Input::get('store_id');
        $tx['user_id']         = floatval($user_id);
        $tx['pos_id']          = Input::get('pos_id');
        $tx['partner_id']      = Input::get('partner_id');



        $tx['created_at']      = Input::get('invoiced_at');
        $tx['updated_at']      = Input::get('invoiced_at');



        $user = User::where('id',$user_id)->first();
        $user1 = $user->toArray();
        $tx['user_id']=$user1['id'];

        $currency_code = Input::get('currency');



            $loyalty = Loyalty::where('partner_id',$tx['partner_id'])->where('draft','0')->first();
            $reward_redeem = Input::get('reward_redeem');
            if($reward_redeem == 1){
                if($loyalty){
                    $reward_usd = floatval($loyalty->reward_usd);
                    if($reward_usd == 0){
                        $reward_usd =1;
                    }
                    $reward_pts = floatval($loyalty->reward_pts);
                    if($reward_pts == 0){
                        $reward_pts =1;
                    }
                    $total = $tx['total_amount'];
                    $totalpoints = $total*$reward_pts;
                    $totalpoints = $totalpoints/$reward_usd;
                    $tx['points_rewarded'] = $totalpoints;
                    $tx['trx_reward']      = 1;
                    $tx['trx_redeem']      = 0;
                    $tx['points_redeemed'] = 0;
                }
            }
            elseif($reward_redeem == 2){
                if($loyalty){
                    $reward_usd = floatval($loyalty->reward_usd);
                    $reward_pts = floatval($loyalty->reward_pts);
                    $total = floatval($tx['total_amount']);
                    $totalpoints = $total/0.01;
                    $tx['trx_reward']      = 0;
                    $tx['points_rewarded'] = 0;
                    $tx['trx_redeem']      = 1;
                    $tx['points_redeemed'] = $totalpoints;
                }
            }


            $validator = Validator::make($tx, array(
                //'id'         => 'required|numeric',
                'partner_id' => 'required|numeric',
                'store_id'   => 'required|numeric',
                'user_id'    => 'required|numeric',
                'pos_id'     => 'required|numeric'
            ));

            if(!$validator->fails()) {
                $params = array(
                    'reference'     =>    $tx['ref_number'],
                    'amount'        =>    $tx['total_amount'],
                    'store_id'      =>    $tx['store_id'],
                    'user_id'       =>    $tx['user_id'],
                    'pos_id'        =>    $tx['pos_id'],
                    'partner_id'    =>    $tx['partner_id'],
                    'created_at'    =>    $tx['created_at'],
                    'updated_at'    =>    $tx['updated_at'],
                    'user_id'       =>    $tx['user_id'],
                    'currency'      =>    $currency_code,
                    //'points'        =>    $tx['points_rewarded'],
                    'trx_reward'    =>    $tx['trx_reward'],
                    'trx_redeem'    =>    $tx['trx_redeem'],
                    'points_redeemed'    =>    $tx['points_redeemed']
            );
                $result = array();
                if($tx['trx_reward']==1){
                    $result = Transaction::doReward($params, 'Amount');
                }
                if($tx['trx_redeem']==1){
                    $result = Transaction::doRedemption($params);
                }
                //DB::table('transaction')->insert($tx);
                if($result){
                    return self::respondWith(
                        200, 'OK', $result
                    );
                }
            }


        }
         /**
     * GET User by mobile
     *
     * GET /api/user-by-mobile/{MobileNormalized}
     *
     * @param GET Mobile number 9617xxxxxxx
     * @return Response
     */
	public function getUserByMobile($mobile_number)
	{
            $user = Member::where('normalized_mobile', "+". $mobile_number)->where('draft', false)->first();

            $userProfile = $this->getUser($user->id);
            if($userProfile){
                    return self::respondWith(
                        200, 'OK', $userProfile
                    );
                }
        }
    /**
     * GET User by card
     *
     * GET /api/user-by-card/{card_number}
     *
     * @param GET Card number 1111111111
     * @return Response
     */
	public function getUserByCard($card_number)
	{
            $card = Card::where('number', $card_number)->first();

            if(!$card) {
                return Redirect::to('mobile/error')->with('message', 'Invalid card number entered');
            }

            $member = $card->user;

            $userProfile = $this->getUser($member->id);
            if($userProfile){
                    return self::respondWith(
                        200, 'OK', $userProfile
                    );
                }
        }

    /**
     * GET Partner List
     *
     * GET /api/partner-list
     *
     * @return Response
     */
	public function getPartnerList()
	{
            $buffer = array();
            $partners =  DB::table('partner')->where('draft',0)->get();
            foreach($partners as $part){
                $buffer[] = $part;
            }

            if($buffer){
                    return self::respondWith(
                        200, 'OK' , $buffer, 'partners'
                    );
                }
        }

    /**
     * This returns a list of all the stores by location
     *
     * GET /api/stores-nearby
     *
     * @param GET userlat
     * @param GET userlon
     * @param GET unit
     * @return Response
     */
    public function getStoresNearby()
    {
        $userLat = Input::get('userlat');
        $userLon = Input::get('userlon');
        $unit = Input::get('unit');
        $storeLoc = array();
        $counterStore = 0;
        $stores =  DB::table('store')->where('draft',0)->get();
        foreach($stores as $onestr){
            $nearby_dist = self::distance($userLat,$userLon,$onestr->lat,$onestr->lng,$unit);
            $storeLoc[$counterStore][0] = $onestr;
            $storeLoc[$counterStore][1] = $nearby_dist;
            $arraySortCounter[$counterStore] = $nearby_dist;
            $counterStore += 1;
        }
        asort($arraySortCounter);
        $storeLoc1 = array();
        $counterLoc = 0;
        foreach($arraySortCounter as $key => $val){
            $storeLoc1[$counterLoc] = $storeLoc[$key];
            $counterLoc +=1;

        }
        return self::respondWith(
            200, null, $storeLoc1
        );
    }

    /**
     * This returns a list of products by category
     *
     * GET /api/product-by-category/{category_id}
     *
     * @param GET category
     *
     * @return Response
     */
    public function getProductByCategory($category_id)
    {
        $results = DB::table('product');
        $results->leftJoin('media', 'product.cover_image', '=', 'media.id');
        $results->select('product.*', 'media.name as image_name');
        $results->orderBy('name', 'ASC');
        $results->where('display_online', true);
        $results->where('status', 'active');
        $results->where('category_id',$category_id);

        return self::respondWith(
            200, null, $results->get(), 'products'
        );
    }


    /**
     * This will insert Bulk Transactions
     *
     * POST /api/insert-bulk-transactions
     * @param GET xmlfiletest
     *
     * @return Response
     */
    public function postInsertBulkTransactions(){
        $xmlTemp = SoapServer::handle();
        $xml = new SimpleXMLElement($xmlTemp);
        var_dump($xml);
        var_dump($xmlTemp);
        exit();
    }

} // EOC