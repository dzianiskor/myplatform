<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Validator;
use App\Store as Store;
use App\Address as Address;
use App\Partner as Partner;
use App\Pos as Pos;
use View;

/**
 * Store Controller
 *
 * @category   Controllers
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class StoreController extends BaseController
{
    /**
     * Fetch the select drop down data for the store popup
     *
     * @return array
     */
    private function getSelectData()
    {
        return array(
            'countries' => DB::table('country')->orderBy('name', 'asc')->get(),
            'areas'     => DB::table('area')->orderBy('name', 'asc')->get(),
            'cities'    => DB::table('city')->orderBy('name', 'asc')->get()
        );
    }

    /**
     * Create and return a default draft store
     *
     * @param int $partner_id
     * @return Result
     */
    private function getStoreDraft($partner_id)
    {
        $store = new Store();

        $store->name       = 'New Store';
        $store->lat        = '25.0738579';
        $store->lng        = '55.2298444';
        $store->draft      = true;
        $store->partner_id = $partner_id;
        $store->mapping_id = 0;

        $store->save();

        $address = new Address();

        $address->country_id = 9;
        $address->area_id    = 76;
        $address->city_id    = 672;
        $address->street     = null;
        $address->floor      = null;
        $address->building   = null;

        $store->address()->save($address);

        return $store;
    }

    /**
     * Display the new store page
     *
     * @return View
     */
    public function newStore($partner_id = null)
    {
        $data = array(
            'store'   => $this->getStoreDraft($partner_id),
            'rnd_map' => 'gmap'
        );

        return View::make("store.form", array_merge(
            $data, $this->getSelectData()
        ));
    }

    /**
     * Display the edit store page
     *
     * @return View
     */
    public function editStore($enc_store_id = null)
    {
        $store_id = base64_decode($enc_store_id);
        $store = Store::find($store_id);
        if (!$store) {
            App::abort(404);
        }

        $data = array(
            'store'   => Store::find($store_id),
            'rnd_map' => 'gmap'
        );

        return View::make("store.form", array_merge(
            $data, $this->getSelectData()
        ));
    }

    /**
     * Create a new store resource
     *
     * @param int $store_id
     * @return void
     */
    public function updateStore($enc_store_id = null)
    {
        $store_id = base64_decode($enc_store_id);
        $rules = array(
            'store_name' => 'required|min:1'
        );
        $input = Input::all();
        foreach($input as $k=>$i){
            $input[$k] = self::sanitizeText($i);
        }
        $validator = Validator::make($input, $rules);

        if($validator->fails()) {
            return Response::json(array(
                'failed' => true,
                'errors' => "Please ensure that you provide a valid store name"
            ));
        }

        $store = Store::find($store_id);
        if (!$store) {
            App::abort(404);
        }

        // Details
        $store->name            = self::sanitizeText(Input::get('store_name'));
        $store->display_online  = self::sanitizeText(Input::get('display_online'));
        $store->lng             = self::sanitizeText(Input::get('store_longitude'));
        $store->lat             = self::sanitizeText(Input::get('store_latitude'));
        $store->mapping_id      = self::sanitizeText(Input::get('mapping_id'));
        $store->draft           = false;

        // Address
        $store->address->country_id = self::sanitizeText(Input::get('store_country', 0));
        $store->address->area_id    = self::sanitizeText(Input::get('store_area', 0));
        $store->address->city_id    = self::sanitizeText(Input::get('store_city', 0));
        $store->address->street     = self::sanitizeText(Input::get('store_street', null));
        $store->address->floor      = self::sanitizeText(Input::get('store_floor', null));
        $store->address->building   = self::sanitizeText(Input::get('store_building', null));

        $pos_ids                    = self::sanitizeText(Input::get('pos_id'));
        // POS
        if (!empty($pos_ids) )
        {
            Pos::where('store_id', $store_id)->whereNotIn('id', $pos_ids)->delete();
        }

        $pos_names	= self::sanitizeText(Input::get('pos_name'));
        $pos_mappings	= self::sanitizeText(Input::get('pos_mapping'));

        if ($pos_names) foreach($pos_names as $k => $v) {
            if(!empty($pos_names[$k])) {
                $pos_id			   = (!empty($pos_ids[$k])) ? $pos_ids[$k] : 0;
                $pos			   = array();
                $pos['name']	   = $pos_names[$k];
                $pos['mapping_id'] = (isset($pos_mappings[$k]) && !empty($pos_mappings[$k])) ? $pos_mappings[$k] : 0;
                
                $pos_obj		     = new Pos();
                $pos_obj->name		 = $pos_names[$k];
                $pos_obj->mapping_id = (isset($pos_mappings[$k]) && !empty($pos_mappings[$k])) ? $pos_mappings[$k] : 0;

                if ($pos_id == 0) {
                    $store->pointOfSales()->save($pos_obj);
                } else {
                    $store->pointOfSales()->where('id', $pos_id)->update($pos);
                }
            }
        }

        $store->push();

        return Response::json(array(
            'failed' => false,
            'view_url' => url('dashboard/store/list/'.$store->partner_id)
        ));
    }

    /**
     * Delete the specified store
     *
     * @return View
     */
    public function deleteStore($enc_store_id = null)
    {
        $store_id = base64_decode($enc_store_id);
        $store = Store::find($store_id);
        if (!$store) {
            App::abort(404);
        }

        Store::find($store_id)->delete();

        return Response::json(array(
            'failed' => false
        ));
    }

    /**
     * Render a list of stores relevant to the Partner
     *
     * @param int $partner_id
     * @return View
     */
    public function listStores($partner_id = null)
    {
        $data = array(
            'stores' => DB::table('store')->where('partner_id', $partner_id)->where('draft', false)->get()
        );

        return View::make("store.list", $data);
    }
    
    /**
     * check if the mapping used twice
     * at same partner
     *
     * @param int $partner_id
     * @return View
     */
    public function checkPosMappingId($partner_id = null, $mapping_id, $store_id)
    {
        $partnerStores  = Partner::find($partner_id)->stores;
        
        $storesArray = array();
        foreach ($partnerStores as $partnerStore) {
            $storesArray[] = $partnerStore->id;
        }
        $pos_mapping	= $mapping_id;
        $repeated = Pos::where('mapping_id', $pos_mapping)->whereIn('store_id', $storesArray)->get();
        if($repeated->count() > 0){
            $pos = Pos::where('mapping_id', $pos_mapping)->where('store_id', $store_id)->first();
            if(!$pos){
                return Response::json(array(
                   'failed' => true,
                   'errors' => "Please use the maping-id one time"
                ));
            }else{
                return Response::json(array(
                    'failed' => false,
                    'errors' => ""
                 )); 
            }
        }else{
            return Response::json(array(
                'failed' => false,
                'errors' => ""
            ));
        }
    }

} // EOC