<?php
namespace App\Http\Controllers;
use View;
use Input;
use Validator;
use I;
use Illuminate\Support\Facades\Redirect;
use App\Currency as Currency;
use App\CurrencyPricing as CurrencyPricing;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Currency Controller
 * 
 * @category   Controllers
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class CurrencyController extends BaseController
{
    protected $layout = "layouts.dashboard";

    /**
     * Fetch the validations
     *
     * @return array
     */
    private function getValidations()
    {
        return array(
            'name'       => 'required',
            'short_code' => 'required',
        );
    }

    /**
     * Display a list of the currencies
     *
     * @return void
     */
    public function getIndex()
    {
        if(!I::can('view_currencies')){
            return Redirect::to(url("dashboard"));
        }
        $this->ormClass      = 'App\Currency';
        $this->search_fields = array('name');

        $currencies = $this->getList();

        $page = Input::get('page');
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $currencies = new LengthAwarePaginator($currencies->forPage($page, 15), $currencies->count(), 15, $page);

        return View::make("currency.list", array(
            'currencies' => $currencies
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getCreate()
    {
        if(!I::can('create_currencies')){
            return Redirect::to(url("dashboard"));
        }
        $currency           = new Currency();
        $currency->name     = "New Currency";
        $currency->draft    = true;
        $currency->save();

        $currency->short_code = $currency->id;
        $currency->save();
        $currencyId = base64_encode($currency->id);
        return Redirect::to(url("dashboard/currencies/edit/$currencyId"));
    }

    /**
     * Show the currency edit page
     *
     * @param integer $currency_id
     * @return Response
     */
    public function getEdit($enc_id)
    {
        $id = base64_decode($enc_id);
        if(!I::can('view_currencies')){
            return Redirect::to(url("dashboard"));
        }
        $currency = Currency::find($id);
        
        if(!$currency) {
            \App::abort(404);
        }

        return View::make("currency.view", array('currency'=>$currency));
    }

    /**
     * Delete the currency
     *
     * @param integer $currency_id
     * @return void
     */
    public function getDelete($enc_id)
    {
        $id = base64_decode($enc_id);
        if(!I::can('delete_currencies')){
            return Redirect::to(url("dashboard"));
        }
        $currency = Currency::find($id);

        if(!$currency) {
            \App::abort(404);
        }

        $currency->delete();

        return Redirect::to(url("dashboard/currencies"));
    }

    /**
     * apply changes
     *
     * @param integer $currency_id
     * @return Response
     */
    public function postUpdate($enc_id)
    {
        $id = base64_decode($enc_id);
        if(!I::can('edit_currencies') && !I::can('create_currencies')) {
            return Redirect::to(url("dashboard"));
        }
        $currency = Currency::find($id);

        if(!$currency) {
            \App::abort(404);
        }
        $input = Input::all();
        foreach($input as $k=>$i){
            $input[$k] = self::sanitizeText($i);
        }
        $validator = Validator::make($input, $this->getValidations());

        if($validator->fails()) {
            return Redirect::to("dashboard/currencies/edit/$enc_id")->withErrors($validator)->withInput();
        }

        $currency->name       = self::sanitizeText(Input::get('name'));
        $currency->short_code = self::sanitizeText(Input::get('short_code'));
        $currency->draft      = false;

        $currency->save();

        $latest = $currency->latestPrice();

        if(!$latest) {
            $latest = new CurrencyPricing();

            $latest->currency_id = $currency->id;
            $latest->rate        = self::sanitizeText(Input::get('latest_rate'));
            $latest->save();
        } else {
            $latest->currency_id = $currency->id;
            $latest->rate        = self::sanitizeText(Input::get('latest_rate'));
            $latest->save();
        }

        return Redirect::to(Input::get('redirect_to', 'dashboard/currencies'));
    }
} // EOC