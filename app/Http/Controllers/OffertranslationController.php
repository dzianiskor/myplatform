<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;
use View;
use Illuminate\Support\Facades\Validator;
use App\Offertranslation;
use Response;
use Meta;
use App\Language;
/**
 * Offer Translation Controller
 * 
 * @category   Controllers
 * @package    BLU
 * @author     Magid Mroueh <magid.mroueh@bluloyalty.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class OffertranslationController extends BaseController {

    /**
     * Fetch the select drop down data for the offertranslation popup
     *
     * @return array
     */
    private function getSelectData() {
        return array(
            'languages' => Language::orderBy('name')->pluck('name', 'id')
        );
    }

    /**
     * Create and return a default draft offertranslation
     *
     * @param int $offer_id
     * @return Result
     */
//    private function getOffertranslationDraft($offer_id)
//    {
//        $offertranslation = new Offertranslation();
//
//        $offertranslation->name       = 'New Offertranslation';
//        $offertranslation->offer_id = $offer_id;
//
//        $offertranslation->save();
//
//        return $offertranslation;
//    }

    /**
     * Display the new offertranslation page
     *
     * @return View
     */
    public function newOffertranslation($enc_offer_id = null) {
        $offer_id = base64_decode($enc_offer_id);
        $data = array(
//            'offertranslation'   => $this->getOffertranslationDraft($offer_id)
            'offer_id' => $offer_id
        );

        return View::make("offertranslation.new", array_merge(
                                $data, $this->getSelectData()
        ));
    }

    /**
     * Display the edit offertranslation page
     *
     * @return View
     */
    public function editOffertranslation($enc_offertranslation_id = null) {
        $offertranslation_id = base64_decode($enc_offertranslation_id);
        $data = array(
            'offertranslation' => Offertranslation::find($offertranslation_id)
        );

        return View::make("offertranslation.form", array_merge(
                                $data, $this->getSelectData()
        ));
    }

    /**
     * Create a new offertranslation resource
     *
     * @param int $offertranslation_id
     * @return void
     */
    public function updateOffertranslation($enc_offertranslation_id = null) {
        $offertranslation_id = base64_decode($enc_offertranslation_id);
        $rules = array(
            'offertranslation_name' => 'required|min:1'
        );
        $input = Input::all();
        foreach($input as $k=>$i){
            $input[$k] = self::sanitizeText($i);
        }
        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return Response::json(array(
                        'failed' => true,
                        'errors' => "Please ensure that you provide a valid offertranslation name"
            ));
        }

        $offertranslation = Offertranslation::find($offertranslation_id);

        // Details
        $offertranslation->name = self::sanitizeText(Input::get('offertranslation_name'));
        $offertranslation->lang_id = self::sanitizeText(Input::get('offertranslation_lang'));
        $offertranslation->short_description = self::sanitizeText(Input::get('offertranslation_short_description'));
        $offertranslation->description = self::sanitizeText(Input::get('offertranslation_description'));
        $offertranslation->terms_and_condition = self::sanitizeText(Input::get('offertranslation_terms_and_condition'));
        $offertranslation->email_text_promo_1 = self::sanitizeText(Input::get('offertranslation_email_text_promo_1'));
        $offertranslation->email_text_promo_2 = self::sanitizeText(Input::get('offertranslation_email_text_promo_2'));
        $offertranslation->draft = false;
        $offertranslation->updated_at = date('Y-m-d H:i:s');

        $offertranslation->save();

        return Response::json(array(
                    'failed' => false,
                    'view_url' => url('dashboard/offertranslation/list/' . base64_encode($offertranslation->offer_id))
        ));
    }

    /**
     * Create a new prodtranslation resource
     *
     * @param int $prodtranslation_id
     * @return void
     */
    public function createOffertranslation($offertranslation_id = null) {
        $rules = array(
            'offertranslation_name' => 'required|min:1'
        );
        $input = Input::all();
        foreach($input as $k=>$i){
            $input[$k] = self::sanitizeText($i);
        }
        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return Response::json(array(
                        'failed' => true,
                        'errors' => "Please ensure that you provide a valid offertranslation name"
            ));
        }

        $offertranslation = new Offertranslation();
        
        // Details
        $offertranslation->offer_id = self::sanitizeText(Input::get('offertranslation_oid'));
        $offertranslation->lang_id = self::sanitizeText(Input::get('offertranslation_lang'));
        $offertranslation->name = self::sanitizeText(Input::get('offertranslation_name'));
        $offertranslation->short_description = self::sanitizeText(Input::get('offertranslation_short_description'));
        $offertranslation->description = self::sanitizeText(Input::get('offertranslation_description'));
        $offertranslation->terms_and_condition = self::sanitizeText(Input::get('offertranslation_terms_and_condition'));
        $offertranslation->email_text_promo_1 = self::sanitizeText(Input::get('offertranslation_email_text_promo_1'));
        $offertranslation->email_text_promo_2 = self::sanitizeText(Input::get('offertranslation_email_text_promo_2'));
        $offertranslation->draft = false;
        $offertranslation->created_at = date('Y-m-d H:i:s');
        $offertranslation->updated_at = date('Y-m-d H:i:s');

        $offertranslation->save();

        return Response::json(array(
                    'failed' => false,
                    'view_url' => url('dashboard/offertranslation/list/' . base64_encode($offertranslation->offer_id))
        ));
    }

    /**
     * Delete the specified offertranslation
     *
     * @return View
     */
    public function deleteOffertranslation($enc_offertranslation_id = null) {
        $offertranslation_id = base64_decode($enc_offertranslation_id);
        Offertranslation::find($offertranslation_id)->delete();

        return Response::json(array(
                    'failed' => false
        ));
    }

    /**
     * Render a list of offertranslation relevant to the Partner
     *
     * @param int $offer_id
     * @return View
     */
    public function listoffertranslations($enc_offer_id = null) {
        $offer_id = base64_decode($enc_offer_id);
        $data = array(
            'offertranslations' => Offertranslation::where('offer_id', $offer_id)->where('draft', false)->get(),
            'itemTypes' => Meta::itemType()
        );

        return View::make("offertranslation.list", $data);
    }

}

// EOC