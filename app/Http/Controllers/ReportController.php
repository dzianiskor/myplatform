<?php

namespace App\Http\Controllers;

use App\Reference;
use App\Repositories\Report\RepositoryReport25;
use App\Repositories\Report\RepositoryReport26;
use App\Repositories\Report\RepositoryReport40;
use App\Repositories\RepositoryUser;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use I;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Partner as Partner;
use App\Currency as Currency;
use App\Network as Network;
use App\Report as Report;
use App\User as User;
use App\Transaction as Transaction;
use App\TransactionDeduction as TransactionDeduction;
use App\TransactionItem as TransactionItem;
use App\Admin as Admin;
use App\Trail as Trail;
use App\Product as Product;
use App\Balance as Balance;
use App\Store as Store;
use App\TrailUser as TrailUser;
use ProductsHelper;
use PointsHelper;
use CurrencyHelper;
use App\Supplier as Supplier;
use \ReportHelper;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Report Controller
 *
 * Important:
 *
 * These reporting methods (loadReport_1 etc) map directly
 * to the ID's of the reports in the database.
 *
 * @category   Controllers
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class ReportController extends BaseController
{
    protected $layout = "layouts.dashboard";

    const PAGINATION_COUNT  = 40;

    /**
     * List a1ll report tiers
     *
     * @return void
     */
    public function getIndex()
    {
        if (!I::can('view_report_tiers')) {
            return Redirect::to(url("dashboard"));
        }

        if (Input::has('q') && Input::get('q')) {
            return $this->getSearchResults(Input::get('q'));
        }

        $this->ormClass = 'App\ReportTier';
        $items = $this->getList();
        $page = Input::get('page');
        $page = $page ? : (Paginator::resolveCurrentPage() ? : 1);
        $items = new LengthAwarePaginator($items->forPage($page, 15), $items->count(), 15, $page);

        return View::make('report.list', array(
                    'items' => $items,
                    'type' => 'tiers',
                    'title' => 'Tiers'
        ));
    }

    /**
     * Display the available reports per tier
     *
     * @param string $enc_tier_id
     * @return void
     */
    public function getTiers($enc_tier_id = null)
    {
        if (!I::can('view_report_tiers')) {
            return Redirect::to(url("dashboard"));
        }
        $id = base64_decode($enc_tier_id);
        if (!I::can('view_basic_reports') && $id == 1) {
            return Redirect::to(url("dashboard"));
        }
        if (!I::can('view_premium_reports') && $id == 2) {
            return Redirect::to(url("dashboard"));
        }
        if (!I::can('view_full_reports') && $id == 3) {
            return Redirect::to(url("dashboard"));
        }
        if (!I::can('view_risk_reports') && $id == 4) {
            return Redirect::to(url("dashboard"));
        }
        if (!I::can('view_activity_summary_reports') && $id == 5) {
            return Redirect::to(url("dashboard"));
        }
        if (!I::can('view_redemption_partner_rewarded_points_reports') && $id == 6) {
            return Redirect::to(url("dashboard"));
        }
        $tier = DB::table('report_tier')->find($id);
        $items = DB::table('report')->where('report_tier_id', $id)->where('enabled', true)->get();
        $page = Input::get('page');
        $page = $page ? : (Paginator::resolveCurrentPage() ? : 1);
        $items = new LengthAwarePaginator($items->forPage($page, 15), $items->count(), 15, $page);

        return View::make('report.list', array(
                    'items' => $items,
                    'type' => 'view',
                    'title' => $tier->name
        ));
    }

    /**
     * Display the available reports by search query
     *
     * @param string $searchQuery
     * @return void
     */
    public function getSearchResults($searchQuery = null)
    {
        if (!I::can('view_report_tiers')) {
            return Redirect::to(url("dashboard"));
        }
        if (!I::can('view_basic_reports') && $id == 1) {
            return Redirect::to(url("dashboard"));
        }
        if (!I::can('view_premium_reports') && $id == 2) {
            return Redirect::to(url("dashboard"));
        }
        if (!I::can('view_full_reports') && $id == 3) {
            return Redirect::to(url("dashboard"));
        }
        if (!I::can('view_risk_reports') && $id == 4) {
            return Redirect::to(url("dashboard"));
        }
        if (!I::can('view_activity_summary_reports') && $id == 5) {
            return Redirect::to(url("dashboard"));
        }
        if (!I::can('view_redemption_partner_rewarded_points_reports') && $id == 6) {
            return Redirect::to(url("dashboard"));
        }

        $items = DB::table('report')
                   ->where('enabled', true)
                   ->where('name', 'LIKE', '%' . $searchQuery . '%')
                   ->whereNotIn('id', [27, 36, 39, 40, 44, 45])
                   ->get();

        $page = Input::get('page');
        $page = $page ? : (Paginator::resolveCurrentPage() ? : 1);
        $items = new LengthAwarePaginator($items->forPage($page, 15), $items->count(), 15, $page);

        return View::make('report.list', array(
                    'items' => $items,
                    'type' => 'view',
                    'title' => 'Search Results "' . $searchQuery . '"'
        ));
    }

    /**
     * Display the report for the actual tier
     *
     * @param string $enc_tier_id
     * @return void
     */
    public function getView($enc_tier_id = null)
    {
        if (!I::can('view_report_tiers')) {
            return Redirect::to(url("dashboard"));
        }
        $report = Report::find(base64_decode($enc_tier_id));
        $id = $report->report_tier_id;
        if (!I::can('view_basic_reports') && $id == 1) {
            return Redirect::to(url("dashboard"));
        }
        if (!I::can('view_premium_reports') && $id == 2) {
            return Redirect::to(url("dashboard"));
        }
        if (!I::can('view_full_reports') && $id == 3) {
            return Redirect::to(url("dashboard"));
        }
        if (!I::can('view_risk_reports') && $id == 4) {
            return Redirect::to(url("dashboard"));
        }
        if (!I::can('view_activity_summary_reports') && $id == 5) {
            return Redirect::to(url("dashboard"));
        }
        if (!I::can('view_redemption_partner_rewarded_points_reports') && $id == 6) {
            return Redirect::to(url("dashboard"));
        }

        $action = "loadReport_{$report->id}";

        if (is_callable(array($this, $action))) {
            return $this->$action($report);
        } else {
            App::abort(404);
        }
    }

    protected function listSearchByReference($searchPhrase, $partnerIds, $limit = null)
    {
        return $searchPhrase ?
            Reference::select(['reference.*'])
                     ->where('reference.number', 'like', '%' . $searchPhrase . '%')
                     ->whereIn('partner_id', $partnerIds)
                     ->where(function (Builder $query) use ($searchPhrase, $partnerIds) {
                         $query->orWhereHas('user',
                             function ($query) use ($searchPhrase, $partnerIds) {
                                 return $query->where('draft', 0)
                                              ->where('verified', 1)
                                              ->whereIn('users.id', function ($query) use ($partnerIds) {
                                                  $query->from('partner_user')
                                                        ->select(['user_id'])
                                                        ->whereIn('partner_id', is_array($partnerIds) ? $partnerIds : [$partnerIds]);
                                              });
                             });
                     })->limit($limit)->get()
            : [];
    }

    /**
     * Perform a basic search for managed references under the current user context
     *
     * @param \App\Repositories\RepositoryUser $repositoryUser
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getReferences(RepositoryUser $repositoryUser)
    {
        $searchPhrase = self::sanitizeText(Input::get('term'));

        if (!$searchPhrase) {
            exit;
        }

        $partnerIds = Auth::User()->managedPartnerIDs();
        $references = $this->listSearchByReference($searchPhrase, $partnerIds, 10);

        $buffer = $references->transform(function($references) {

            $transformed = new \stdClass;

            $transformed->id = $references->user_id;
            $transformed->label = $references->number;
            $transformed->value = $references->number;
            $transformed->name = $references->user->name;
            $transformed->balance = number_format($references->user->balance(), 0, '.', ',');

            return $transformed;
        })->toArray();

        return Response::json($buffer);
    }

    /**
     * Perform a basic search for managed members under the current user context
     *
     * @param \App\Repositories\RepositoryUser $repositoryUser
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMembers(RepositoryUser $repositoryUser)
    {
        $searchPhrase = self::sanitizeText(Input::get('term'));
        $searchType = self::sanitizeText(Input::get('search-type'));

        if (!$searchPhrase) {
            exit;
        }

        // We're manually converting to JSON to control attributes
        $buffer = [];
        $admin_partner = Auth::User()->getTopLevelPartner();

        if ($admin_partner->has_middleware != 1) {
            $partnerIds = Auth::User()->managedPartnerIDs();

            switch ($searchType) {
                case 'name':
                    $users = $repositoryUser->listSearchByUserData($searchPhrase, $partnerIds, 10);
                    break;
                default:
                    $users = $repositoryUser->listBySearchPhrase($searchPhrase, $partnerIds, 10);
                    break;
            }

            $buffer = $users->transform(function($user) use ($searchType) {

                $transformed = new \stdClass;

                $transformed->id = $user->id;
                $transformed->label = $user->name;
                $transformed->value = $user->name;
                $transformed->balance = number_format($user->balance(), 0, '.', ',');
                $transformed->reference = $user->references()->first()->number ?? '';

                return $transformed;
            })->toArray();

        } else {
            $Users_info = ReportHelper::getMiddlewareSearchSensitiveInfo($searchPhrase);

            foreach ($Users_info as $member) {
                $memberFullName     = $member->first_name ." " . $member->last_name;
                if (trim($memberFullName) == 'na na') {
                    $memberFullName = "N/A";
                }

                $u = new \stdClass();
                $u->id    = $member->user_id;
                $u->label = $memberFullName;
                $u->value = $memberFullName;

                $buffer[] = $u;
            }
        }

        return Response::json($buffer);
    }

    # --------------------------------------------------------------------------
    #
    # Report Loaders
    #
    # --------------------------------------------------------------------------

    /**
     * Load all data for the 'Store Transactions in Points' report
     *
     * @param Report $report
     * @return View
     */
    private function loadReport_1($report)
    {
        if (!I::can('view_basic_reports')) {
            return Redirect::to(url("dashboard"));
        }

        $defaults = ReportHelper::normalizeDefaults();
        $currencyId = 6;
        if ($defaults['network_currency'] != 'not') {
            $networkId  = $defaults['network_currency'];
        } else {
            $networkId  = 1;
        }
        $networkCurrencyId = Network::find($networkId)->currency_id;
        $network_currency = Currency::where('id', $networkCurrencyId)->first();
        $network_exchangeRate   = $network_currency->latestRate();
        $exchangeRate   = 1;
        $currencyShortCode  = 'USD';
        if ($defaults['currency'] > 0) {
            $currencyId = $defaults['currency'];
            $currency = Currency::where('id', $currencyId)->first();
            $exchangeRate   = $currency->latestRate();
            $currencyShortCode  = $currency->short_code;
        }

        $tot['exchangeRate'] = $exchangeRate;
        $tot['currencyShortCode'] = $currencyShortCode;
        $tot['currency'] = $currencyId;

        $results = (isset($defaults['params']) && $defaults['params'] == 1) ? [] : null;

        $tot['network_currency'] = $networkCurrencyId;
        $tot['network_exchangeRate'] = $network_exchangeRate;

        if (Input::has('export')) {
            $buffer[] = array("ID", "Date & Time", "Store", "Points Rewarded", "Points Redeemed", "Value of Pts Rewarded", "Value of Pts Redeemed", "Trx Amount($currencyShortCode)");

            $query = Transaction::whereIn('transaction.store_id', $defaults['entities'])
                                ->whereBetween('transaction.created_at', [$defaults['start_date'], $defaults['end_date']])
                                ->where('transaction.network_id', $networkId);

            $data = $query->leftJoin('partner', 'transaction.partner_id', '=', 'partner.id')
                          ->leftJoin('store', 'transaction.store_id', '=', 'store.id')
                          ->select(
                              'transaction.id',
                              'transaction.created_at',
                              'partner.name as partner_name',
                              'store.name as store_name',
                              'transaction.points_rewarded',
                              'transaction.points_redeemed',
                              'transaction.amt_reward',
                              'transaction.amt_redeem',
                              'transaction.total_amount',
                              'transaction.original_total_amount',
                              'transaction.currency_id'
                          )
                          ->orderBy('transaction.id', 'desc')
                          ->get()
                          ->toArray();

            foreach ($data as $item) {
                $buffer[] = array(
                    $item['id'],
                    $item['created_at'],
                    $item['store_name'],
                    number_format($item['points_rewarded']),
                    number_format($item['points_redeemed']),
                    $item['amt_redeem'],
                    $item['amt_redeem'],
                    $item['total_amount']
                );
            }

            return ReportHelper::sendDownload($report, $buffer);
        } else {
            return ReportHelper::sendView($report, $defaults, $results, $tot);
        }
    }

    /**
     * Load all data for the 'All Members in Excel' report
     *
     * @param Report $report
     * @return array
     */
    private function loadReport_2($report)
    {
        if (!I::can('view_basic_reports')) {
            return Redirect::to(url("dashboard"));
        }

        $defaults = ReportHelper::normalizeDefaults();

        // Fetch report data

        $results = array();
        $arr_partner_ids = array();
        $has_middleware = false;
        $url="";
        $Users_info = array();
        $userTopLevelPartnerId  = Auth::User()->getTopLevelPartner()->id;
        foreach ($defaults['filter1'] as $partn) {
            $partner1 = Partner::find($partn);
            if ($partner1) {
                if ($partner1->has_middleware==1 && $userTopLevelPartnerId == $partner1->id) {
                    $has_middleware = true;
                    $url = $partner1->link_middleware_api . '?method=sensitiveInfo&api_key='.$partner1->middleware_api_key;
                    $arr_partner_ids[] = $partner1->id;
                    break;
                }
            }
        }
        if ($defaults['filter1'][0] != -1  && $has_middleware == false) {
            $results = User::where('draft', false)->where('ref_account', false)->whereHas('partners', function ($q) use ($defaults) {
                $q->whereIn('partner.id', $defaults['filter1']);
            })->orderBy('id', 'DESC')->distinct();
        } else {
            $results = User::whereHas('segments', function ($q) use ($defaults) {
                $q->whereIn('id', $defaults['entities']);
            })->orderBy('id', 'DESC')->distinct();
            $user_ids = array();

            foreach ($results->get() as $res1) {
                if (!in_array($res1->id, $user_ids)) {
                    $user_ids[] = $res1->id;
                }
            }

            $json_user_ids = json_encode($user_ids);
            $post_fields = "user_ids=" . $json_user_ids ;
            //$url = $url."&user_ids=" . $json_user_ids;

            $curl = curl_init();
            // Set some options - we are passing in a useragent too here
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => $url,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => $post_fields,
                CURLOPT_USERAGENT => 'Testing Sensitive Info'
            ));
            // Send the request & save response to $resp
            $resp = curl_exec($curl);
            $resp_curl = json_decode($resp);

            $Users_info = $resp_curl;
        }

        $sensitive['has_middleware'] = $has_middleware;
        $sensitive['user_info'] = $Users_info;
        $sensitive['partner_ids'] = $arr_partner_ids;
        // Construct response

        if (Input::has('export')) {
            $buffer[] = array('id','First Name', 'Last Name', 'Mobile', 'Email', 'Gender', 'DOB', 'Created At' );
            $arr_users = array();
            foreach ($results->get() as $u) {
                if (!in_array($u->id, $arr_users)) {
                    $arr_users[] = $u->id;
                    if ($sensitive['has_middleware'] ==1) {
                        $found = false;
                        foreach ($sensitive['user_info'] as $u_info) {
                            if ($u->id == $u_info->blu_id) {
                                $found = true;
                                $first_name = $u_info->first_name;
                                $last_name = $u_info->last_name;
                                $mobile = $u_info->mobile;
                                $email = $u_info->email;
                            }
                        }
                        if ($found == false) {
                            $first_name = $u->first_name;
                            $last_name = $u->last_name;
                            $mobile = $u->normalizedContactNumber();
                            $email = $u->email;
                        }
                    } else {
                        $first_name = $u->first_name;
                        $last_name = $u->last_name;
                        $mobile = $u->normalizedContactNumber();
                        $email = $u->email;
                    }
                    if ($first_name == "na") {
                        $first_name = "N/A";
                    }
                    if ($last_name == "na") {
                        $last_name = "N/A";
                    }
                    $buffer[] = array(
                        $u->id,
                        $first_name,
                        $last_name,
                        $mobile,
                        $email,
                        $u->gender,
                        $u->dob,
                        $u->created_at->format("Y-m-d H:i:s")
                    );
                }
            }
//            echo "<pre>";
//            var_dump($buffer);
//            exit();
            return ReportHelper::sendDownload($report, $buffer);
        } else {
            return ReportHelper::sendView($report, $defaults, $results, null, $sensitive);
        }
    }

    /**
     * Load all data for the 'All Transactions' report
     *
     * @param Report $report
     * @return array
     */
    private function loadReport_3($report)
    {
        if (!I::can('view_basic_reports')) {
            return Redirect::to(url("dashboard"));
        }

        $defaults = ReportHelper::normalizeDefaults();
        $networkId  = $defaults['network_currency'];
        // Currency
        $exchangeRate   = 1;
        $currencyShortCode  = 'USD';
        $currencyId         = 6;
        if ($defaults['currency'] > 0) {
            $currencyId = $defaults['currency'];
            $currency = Currency::where('id', $currencyId)->first();
            $exchangeRate   = $currency->latestRate();
            $currencyShortCode  = $currency->short_code;
        }
        // Fetch report data

        $results = Transaction::whereIn('partner_id', $defaults['entities']);

        $results->where('created_at', '>=', $defaults['start_date']);
        $results->where('created_at', '<=', $defaults['end_date']);
        $results->where('network_id', $networkId);
        $results->orderBy('created_at', 'DESC');

        $tot                        = array();
        $tot['exchangeRate']        = $exchangeRate;
        $tot['currencyShortCode']   = $currencyShortCode;
        $tot['currency']            = $currencyId;
        // Construct Response

        if (Input::has('export')) {
            $buffer[] = array('Date & Time', 'Store Name', 'Loyalty', 'Points Rewarded', 'Points Redeemed', 'Amount Spent');

            foreach ($results->get() as $u) {
                $buffer[] = array(
                    ReportHelper::xlsDate($u->created_at),
                    ReportHelper::defaultEmpty($u->store, 'name'),
                    ReportHelper::defaultEmpty($u->loyalty, 'name'),
                    number_format($u->points_rewarded),
                    number_format($u->points_redeemed),
                    number_format($u->total_amount * $exchangeRate, 2)
                );
            }

            return ReportHelper::sendDownload($report, $buffer);
        } else {
            return ReportHelper::sendView($report, $defaults, $results, $tot);
        }
    }

    /**
     * Load all data for the 'Users Transactions' report
     *
     * @param Report $report
     * @return array
     */
    private function loadReport_4($report)
    {
        if (!I::can('view_basic_reports')) {
            return Redirect::to(url("dashboard"));
        }

        $defaults = ReportHelper::normalizeDefaults();
        // Currency
        $exchangeRate   = 1;
        $currencyShortCode  = 'USD';
        $currencyId         = 6;
        if ($defaults['currency'] > 0) {
            $currencyId = $defaults['currency'];
            $currency = Currency::where('id', $currencyId)->first();
            $exchangeRate   = $currency->latestRate();
            $currencyShortCode  = $currency->short_code;
        }
        $networkId  = $defaults['network_currency'];

        // Fetch report data
        $results = (isset($defaults['params']) && $defaults['params'] == 1) ? [] : null;
        $totals['exchangeRate'] = $exchangeRate;
        $totals['currencyShortCode'] = $currencyShortCode;
        $totals['currency'] = $currencyId;

        // Construct Response

        if (Input::has('export')) {
            $buffer[] = [
                'TRX ID',
                'Date',
                'Partner',
                'Type',
                'Points',
                'Original Amount',
                'Amount'
            ];

            $query = Transaction::select('transaction.*', 'partner.name as partner_name', 'currency.short_code as currency_code')
                                ->join('partner', 'partner.id', '=', 'transaction.partner_id')
                                ->join('currency', 'currency.id', '=', 'transaction.currency_id')
                                ->where('transaction.created_at', '>=', $defaults['start_date'])
                                ->where('transaction.created_at', '<=', $defaults['end_date'])
                                ->where('transaction.network_id', $networkId);

            if ($defaults['member_id']) {
                $query->whereIn('user_id', $defaults['member_id']);
            }

            $query->orderBy('created_at', 'DESC');

            foreach ($query->get() as $u) {
                $buffer[] = [
                    $u->id,
                    Carbon::parse($u->created_at)->format('Y-m-d'),
                    ReportHelper::defaultEmpty($u->partner_name, 'name'),
                    $u->trx_reward ? 'Reward' : 'Redemption',
                    $u->trx_reward ? '+' . $u->points_rewarded : '-' . $u->points_redeemed,
                    $u->currency_code . ' ' . $u->original_total_amount,
                    number_format($u->original_total_amount * $exchangeRate, 2),
                ];
            }

            return ReportHelper::sendDownload($report, $buffer);
        } else {
            return ReportHelper::sendView($report, $defaults, $results, $totals);
        }
    }

    /**
     * Load all data for the 'Store Transactions in Amount' report
     *
     * @param Report $report
     * @return array
     */
    private function loadReport_5($report)
    {
        if (!I::can('view_basic_reports')) {
            return Redirect::to(url("dashboard"));
        }

        $defaults = ReportHelper::normalizeDefaults();
        $networkId  = $defaults['network_currency'];
        // Currency
            $exchangeRate   = 1;
            $currencyShortCode  = 'USD';
            $currencyId         = 6;
        if ($defaults['currency'] > 0) {
            $currencyId = $defaults['currency'];
            $currency = Currency::where('id', $currencyId)->first();
            $exchangeRate   = $currency->latestRate();
            $currencyShortCode  = $currency->short_code;
        }
        // Fetch report data

        $results = Transaction::whereIn('store_id', $defaults['entities']);

        $results->where('created_at', '>=', $defaults['start_date']);
        $results->where('created_at', '<=', $defaults['end_date']);
        $results->where('network_id', $networkId);

        $results1 = clone $results;
        $results2 = clone $results;
        $results1->where('trx_reward', '1');
        $results2->where('trx_redeem', '1');
        $results->orderBy('created_at', 'DESC');
        // Construct Totals
        $tot['pts_reward'] = $results1->sum('total_amount');
        $tot['pts_redeem'] = $results2->sum('total_amount');
//        $tot['amt_reward'] = $results1->sum('total_amount');
        $tot['amt_reward'] = $results1->sum('amt_reward');
        $tot['amt_redeem'] = $results2->sum('amt_redeem');
        $tot['trx_reward'] = $results->sum('trx_reward');
        $tot['trx_redeem'] = $results->sum('trx_redeem');
        $tot['exchangeRate']        = $exchangeRate;
        $tot['currencyShortCode']   = $currencyShortCode;
        $tot['currency'] = $currencyId;
        // Construct response

        if (Input::has('export')) {
            $buffer[] = array('Date & Time', 'Store Name', 'Amount Rewarded', 'Amount Redeemed', 'Period Balance', 'Total Amount');

            foreach ($results->get() as $u) {
                $buffer[] = array(
                    ReportHelper::xlsDate($u->created_at),
                    ReportHelper::defaultEmpty($u->store, 'name'),
                    number_format($u->amt_reward * $exchangeRate, 2),
                    number_format($u->points_redeemed),
                    number_format($u->amt_redeem * $exchangeRate, 2),
                    number_format($u->total_amount * $exchangeRate, 2)
                );
            }
            return ReportHelper::sendDownload($report, $buffer);
        } else {
            return ReportHelper::sendView($report, $defaults, $results, $tot);
        }
    }

    /**
     * Load all data for the 'Top Points Earners' report
     *
     * @param Report $report
     * @return array
     */
    private function loadReport_6($report)
    {
        if (!I::can('view_premium_reports')) {
            return Redirect::to(url("dashboard"));
        }

        $defaults = ReportHelper::normalizeDefaults();
        $networkId  = $defaults['network_currency'];
        // Currency
            $exchangeRate   = 1;
            $currencyShortCode  = 'USD';
        if ($defaults['currency'] > 0) {
            $currencyId = $defaults['currency'];
            $currency = Currency::where('id', $currencyId)->first();
            $exchangeRate   = $currency->latestRate();
            $currencyShortCode  = $currency->short_code;
        }
        // Fetch report data
        $arr_partner_ids = array();
        $has_middleware = false;
        $arr_user_ids = array();
        $url="";
        $Users_info = array();
        $userTopLevelPartnerId  = Auth::User()->getTopLevelPartner()->id;
        foreach ($defaults['entities'] as $partn) {
            $partner1 = Partner::find($partn);
            if ($partner1) {
                if ($partner1->has_middleware==1 && $userTopLevelPartnerId == $partner1->id) {
                    $has_middleware = true;
                    $url = $partner1->link_middleware_api . '?method=sensitiveInfo&api_key='.$partner1->middleware_api_key;
                    $arr_partner_ids[] = $partner1->id;
                    break;
                }
            }
        }
        $results = Transaction::select(DB::raw('user_id,SUM(points_rewarded) AS pts_rewarded, SUM(total_amount) AS tot_amount'))
                ->where('voided', false)
                ->whereIn('partner_id', $defaults['entities'])
                ->where('points_rewarded', '>', 0)
                ->where('created_at', '>=', $defaults['start_date'])
                ->where('created_at', '<=', $defaults['end_date'])
                ->where('network_id', $networkId);
        //continue Query
        $cln_results = clone $results;


        $results->groupBy('user_id');
        $results->orderBy('pts_rewarded', 'DESC');
        $cln2_results = clone $results;

        foreach ($cln2_results->get() as $res2) {
            if (!in_array($res2->user_id, $arr_user_ids)) {
                $arr_user_ids[] = $res2->user_id;
            }
        }
        if ($has_middleware == true) {
            $json_user_ids = json_encode($arr_user_ids);
            //$url = $url."&user_ids=" . $json_user_ids;
            $post_fields = "user_ids=" . $json_user_ids ;
            $curl = curl_init();
            // Set some options - we are passing in a useragent too here
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => $url,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => $post_fields,
                CURLOPT_USERAGENT => 'Testing Sensitive Info'
            ));
            // Send the request & save response to $resp
            $resp = curl_exec($curl);
            $resp_curl = json_decode($resp);

            $Users_info = $resp_curl;
        }
        $sensitive = array();
        $sensitive['has_middleware'] = $has_middleware;
        $sensitive['url'] = $url ;
        $sensitive['user_ids'] = $arr_user_ids;
        $sensitive['user_info'] = $Users_info;
        $sensitive['partner_ids'] = $arr_partner_ids;

        // Construct Totals
        // Construct Totals
        $tot['pts_reward'] = $cln_results->sum('points_rewarded');
        //$tot['pts_redeem'] = $results->sum('points_redeemed');
        $tot['amt_reward'] = $cln_results->sum('total_amount');
        //$tot['amt_redeem'] = $results->sum('amt_redeem');
        $tot['trx_reward'] = $cln_results->sum('trx_reward');
        //$tot['trx_redeem'] = $results->sum('trx_redeem');
        $tot['exchangeRate']        = $exchangeRate;
        $tot['currencyShortCode']   = $currencyShortCode;


        // Construct Response


        if (Input::has('export')) {
            $buffer[] = array('Period', 'Member Name', 'Total Points Rewarded', 'Total Amount Paid('. $tot['currencyShortCode'] .')');

            foreach ($results->get() as $u) {
                $arr_users_ids = array();
                if ($sensitive['has_middleware'] ==1) {
                    foreach ($sensitive['user_info'] as $u_info) {
                        if ($u->user_id == $u_info->blu_id) {
                            $arr_users_ids[] = $u->user_id;
                            $u->first_name = $u_info->first_name;
                            $u->last_name = $u_info->last_name;
                        }
                    }
                    $userName  = $u->first_name . ', ' . $u->last_name;
                    if ($userName != ', ') {
                        $name   = $userName . " (" . $u->user_id . ")";
                    } else {
                        $name = 'N/A';
                    }
                } else {
                    $name = ReportHelper::defaultEmpty($u->user, 'name');
                }


                if ($name == 'na, na') {
                    $name = 'N/A';
                }
                $buffer[] = array(
                    ReportHelper::periodString($defaults),
                    $name,
                    number_format($u->pts_rewarded),
                    number_format($u->tot_amount * $exchangeRate, 2)
                );
            }

            return ReportHelper::sendDownload($report, $buffer);
        } else {
            return ReportHelper::sendView($report, $defaults, $results, $tot, $sensitive);
        }
    }

    /**
     * Load all data for the 'Top Redeemers' report
     *
     * @param Report $report
     * @return array
     */
    private function loadReport_7($report)
    {
        if (!I::can('view_premium_reports')) {
            return Redirect::to(url("dashboard"));
        }

        $defaults = ReportHelper::normalizeDefaults();
        $networkId  = $defaults['network_currency'];
        // Currency
            $exchangeRate   = 1;
            $currencyShortCode  = 'USD';
        if ($defaults['currency'] > 0) {
            $currencyId = $defaults['currency'];
            $currency = Currency::where('id', $currencyId)->first();
            $exchangeRate   = $currency->latestRate();
            $currencyShortCode  = $currency->short_code;
        }
        // Fetch report data
        $arr_partner_ids = array();
        $has_middleware = false;
        $arr_user_ids = array();
        $url="";
        $Users_info = array();
        $userTopLevelPartnerId  = Auth::User()->getTopLevelPartner()->id;
        foreach ($defaults['entities'] as $partn) {
            $partner1 = Partner::find($partn);
            if ($partner1) {
                if ($partner1->has_middleware==1 && $userTopLevelPartnerId == $partner1->id) {
                    $has_middleware = true;
                    $url = $partner1->link_middleware_api . '?method=sensitiveInfo&api_key='.$partner1->middleware_api_key;
                    $arr_partner_ids[] = $partner1->id;
                    break;
                }
            }
        }

        $results = Transaction::where('voided', false);
        $results->select(
            'user_id',
            DB::raw('SUM(total_amount) AS tot_amount'),
            DB::raw('SUM(points_redeemed) AS points_redeemed')
        );
        $results->whereIn('partner_id', $defaults['entities']);
        $results->where('points_redeemed', '>', 0);
        $results->where('created_at', '>=', $defaults['start_date']);
        $results->where('created_at', '<=', $defaults['end_date']);
        $results->where('network_id', $networkId);

        $cln_results = clone $results;
        //Continue Query
        $results->groupBy('user_id');
        $results->orderBy('points_redeemed', 'DESC');
        $cln2_results = clone $results;

        foreach ($cln2_results->get() as $res2) {
            if (!in_array($res2->user_id, $arr_user_ids)) {
                $arr_user_ids[] = $res2->user_id;
            }
        }
        if ($has_middleware == true) {
            $json_user_ids = json_encode($arr_user_ids);
            //$url = $url."&user_ids=" . $json_user_ids;
            $post_fields = "user_ids=" . $json_user_ids ;
            $curl = curl_init();
            // Set some options - we are passing in a useragent too here
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => $url,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => $post_fields,
                CURLOPT_USERAGENT => 'Testing Sensitive Info'
            ));
            // Send the request & save response to $resp
            $resp = curl_exec($curl);
            $resp_curl = json_decode($resp);

            $Users_info = $resp_curl;
        }
        $sensitive = array();
        $sensitive['has_middleware'] = $has_middleware;
        $sensitive['url'] = $url ;
        $sensitive['user_ids'] = $arr_user_ids;
        $sensitive['user_info'] = $Users_info;
        $sensitive['partner_ids'] = $arr_partner_ids;

        // Construct Totals
        //$tot['pts_reward'] = $results->sum('points_rewarded');
        $tot['pts_redeem'] = $cln_results->sum('points_redeemed');
        //$tot['amt_reward'] = $results->sum('amt_reward');
        $tot['amt_redeem'] = $cln_results->sum('amt_redeem');
        //$tot['trx_reward'] = $results->sum('trx_reward');
        $tot['trx_redeem'] = $cln_results->sum('trx_redeem');
        $tot['exchangeRate']        = $exchangeRate;
        $tot['currencyShortCode']   = $currencyShortCode;


        // Construct Response

        if (Input::has('export')) {
            $buffer[] = array('Period', 'Member Name', 'Total Points Redeemed', 'Total Amount Paid('. $tot['currencyShortCode'] .')');

            foreach ($results->get() as $u) {
                if ($sensitive['has_middleware'] ==1) {
                    foreach ($sensitive['user_info'] as $u_info) {
                        if ($u->user_id == $u_info->blu_id) {
                            $arr_users_ids[] = $u->user_id;
                            $u->first_name = $u_info->first_name;
                            $u->last_name = $u_info->last_name;
                        }
                    }

                    $userName  = $u->first_name . ', ' . $u->last_name;
                    if ($userName != ', ') {
                        $name   = $userName . " (" . $u->user_id . ")";
                    } else {
                        $name = 'N/A';
                    }
                } else {
                    $name = ReportHelper::defaultEmpty($u->user, 'name');
                }


                if ($name == 'na, na') {
                    $name = 'N/A';
                }
                $buffer[] = array(
                    ReportHelper::periodString($defaults),
                    $name,
                    number_format($u->points_redeemed),
                    number_format($u->tot_amount * $exchangeRate, 2)
                );
            }

            return ReportHelper::sendDownload($report, $buffer);
        } else {
            return ReportHelper::sendView($report, $defaults, $results, $tot, $sensitive);
        }
    }

    /**
     * Load all data for the 'Store Comparison' report
     *
     * @param Report $report
     * @return array
     */
    private function loadReport_8($report)
    {
        if (!I::can('view_premium_reports')) {
            return Redirect::to(url("dashboard"));
        }

        $defaults = ReportHelper::normalizeDefaults();
        $networkId  = $defaults['network_currency'];
        // Currency
            $exchangeRate   = 1;
            $currencyShortCode  = 'USD';
            $currencyId         = 6;
        if ($defaults['currency'] > 0) {
            $currencyId = $defaults['currency'];
            $currency = Currency::where('id', $currencyId)->first();
            $exchangeRate   = $currency->latestRate();
            $currencyShortCode  = $currency->short_code;
        }

        // Fetch Report Data

        $results = Transaction::whereIn('store_id', $defaults['entities']);

        $results->select(
            'store_id',
            DB::raw('COUNT(id) AS trx_count'),
            DB::raw('SUM(amt_reward) AS amt_reward'),
            DB::raw('SUM(amt_redeem) AS amt_redeem'),
            DB::raw('SUM(trx_reward) AS trx_reward'),
            DB::raw('SUM(trx_redeem) AS trx_redeem'),
            DB::raw('SUM(points_rewarded) AS points_rewarded'),
            DB::raw('SUM(points_redeemed) AS points_redeemed'),
            DB::raw('SUM(total_amount) AS total_amount')
        );

        $results->where('created_at', '>=', $defaults['start_date']);
        $results->where('created_at', '<=', $defaults['end_date']);
        $results->where('network_id', $networkId);
        $results->groupBy('store_id');
        $results->orderBy('points_rewarded', 'DESC');

        $tot                        = array();
        $tot['exchangeRate']        = $exchangeRate;
        $tot['currencyShortCode']   = $currencyShortCode;
        $tot['currency'] = $currencyId;

        // Construct Response

        if (Input::has('export')) {
            $buffer[] = array(
                'Period',
                'Store Name',
                'Reward Trx',
                'Redeem Trx',
                'Points Rewarded',
                'Points Redeemed',
                'Amount Rewarded',
                'Amount Redeemed'
            );

            foreach ($results->get() as $u) {
                $buffer[] = array(
                    ReportHelper::periodString($defaults),
                    ReportHelper::defaultEmpty($u->store, 'name'),
                    number_format($u->trx_reward),
                    number_format($u->trx_redeem),
                    number_format($u->points_rewarded),
                    number_format($u->points_redeemed),
                    number_format($u->amt_reward * $exchangeRate, 2),
                    number_format($u->amt_redeem * $exchangeRate, 2)
                );
            }

            return ReportHelper::sendDownload($report, $buffer);
        } else {
            return ReportHelper::sendView($report, $defaults, $results, $tot);
        }
    }

    /**
     * Load all data for the 'Loyalty Programs Comparison' report
     *
     * @param Report $report
     * @return array
     */
    private function loadReport_9($report)
    {
        if (!I::can('view_premium_reports')) {
            return Redirect::to(url("dashboard"));
        }

        $defaults = ReportHelper::normalizeDefaults();
        $networkId  = $defaults['network_currency'];
        // Currency
            $exchangeRate   = 1;
            $currencyShortCode  = 'USD';
            $currencyId         = 6;
        if ($defaults['currency'] > 0) {
            $currencyId = $defaults['currency'];
            $currency = Currency::where('id', $currencyId)->first();
            $exchangeRate   = $currency->latestRate();
            $currencyShortCode  = $currency->short_code;
        }
        // Fetch Report Data

        $results = Transaction::whereIn('rule_id', $defaults['entities']);

        $results->select(
            'rule_id',
            DB::raw('COUNT(id) AS trx_count'),
            DB::raw('SUM(amt_reward) AS amt_reward'),
            DB::raw('SUM(amt_redeem) AS amt_redeem'),
            DB::raw('SUM(trx_reward) AS trx_reward'),
            DB::raw('SUM(trx_redeem) AS trx_redeem'),
            DB::raw('SUM(points_rewarded) AS points_rewarded'),
            DB::raw('SUM(points_redeemed) AS points_redeemed'),
            DB::raw('SUM(total_amount) AS total_amount')
        );

        $results->where('created_at', '>=', $defaults['start_date']);
        $results->where('created_at', '<=', $defaults['end_date']);
        $results->where('network_id', $networkId);
        $results->groupBy('rule_id');
        $results->orderBy('points_rewarded', 'DESC');

        $tot                        = array();
        $tot['exchangeRate']        = $exchangeRate;
        $tot['currencyShortCode']   = $currencyShortCode;
        $tot['currency'] = $currencyId;
        // Construct Response

        if (Input::has('export')) {
            $buffer[] = array(
                'Period',
                'Loyalty Program',
                'Reward Trx',
                'Redeem Trx',
                'Points Rewarded',
                'Points Redeemed',
                'Amount Rewarded',
                'Amount Redeemed'
            );

            foreach ($results->get() as $u) {
                $buffer[] = array(
                    ReportHelper::periodString($defaults),
                    ReportHelper::defaultEmpty($u->loyalty, 'name'),
                    number_format($u->trx_reward),
                    number_format($u->trx_redeem),
                    number_format($u->points_rewarded),
                    number_format($u->points_redeemed),
                    number_format($u->amt_reward * $exchangeRate, 2),
                    number_format($u->amt_redeem * $exchangeRate, 2)
                );
            }

            return ReportHelper::sendDownload($report, $buffer);
        } else {
            return ReportHelper::sendView($report, $defaults, $results, $tot);
        }
    }

    /**
     * Load all data for the 'Segment Comparison' report
     *
     * @param Report $report
     * @return array
     */
    private function loadReport_10($report)
    {
        if (!I::can('view_premium_reports')) {
            return Redirect::to(url("dashboard"));
        }

        $defaults = ReportHelper::normalizeDefaults();
        $networkId  = $defaults['network_currency'];
        // Currency
            $exchangeRate   = 1;
            $currencyShortCode  = 'USD';
            $currencyId         = 6;
        if ($defaults['currency'] > 0) {
            $currencyId = $defaults['currency'];
            $currency = Currency::where('id', $currencyId)->first();
            $exchangeRate   = $currency->latestRate();
            $currencyShortCode  = $currency->short_code;
        }

        // Fetch Report Data

        $results = Transaction::whereIn('segment_id', $defaults['entities']);

        $results->select(
            'segment_id',
            DB::raw('COUNT(id) AS trx_count'),
            DB::raw('SUM(amt_reward) AS amt_reward'),
            DB::raw('SUM(amt_redeem) AS amt_redeem'),
            DB::raw('SUM(trx_reward) AS trx_reward'),
            DB::raw('SUM(trx_redeem) AS trx_redeem'),
            DB::raw('SUM(points_rewarded) AS points_rewarded'),
            DB::raw('SUM(points_redeemed) AS points_redeemed'),
            DB::raw('SUM(total_amount) AS total_amount')
        );

        $results->where('created_at', '>=', $defaults['start_date']);
        $results->where('created_at', '<=', $defaults['end_date']);
        $results->where('network_id', $networkId);
        $results->groupBy('segment_id');
        $results->orderBy('points_rewarded', 'DESC');

        $tot                        = array();
        $tot['exchangeRate']        = $exchangeRate;
        $tot['currencyShortCode']   = $currencyShortCode;
        $tot['currency'] = $currencyId;

        // Construct Response

        if (Input::has('export')) {
            $buffer[] = array(
                'Period',
                'Segment',
                'Reward Trx',
                'Redeem Trx',
                'Points Rewarded',
                'Points Redeemed',
                'Amount Rewarded',
                'Amount Redeemed'
            );

            foreach ($results->get() as $u) {
                $buffer[] = array(
                    ReportHelper::periodString($defaults),
                    ReportHelper::defaultEmpty($u->segment, 'name'),
                    number_format($u->trx_reward),
                    number_format($u->trx_redeem),
                    number_format($u->points_rewarded),
                    number_format($u->points_redeemed),
                    number_format($u->amt_reward * $exchangeRate, 2),
                    number_format($u->amt_redeem * $exchangeRate, 2)
                );
            }

            return ReportHelper::sendDownload($report, $buffer);
        } else {
            return ReportHelper::sendView($report, $defaults, $results, $tot);
        }
    }

    /**
     * Load all data for the 'Country Comparison' report
     *
     * @param Report $report
     * @return array
     */
    private function loadReport_11($report)
    {
        if (!I::can('view_full_reports')) {
            return Redirect::to(url("dashboard"));
        }

        $defaults = ReportHelper::normalizeDefaults();
        $networkId  = $defaults['network_currency'];
        // Currency
            $exchangeRate   = 1;
            $currencyShortCode  = 'USD';
            $currencyId         = 6;
        if ($defaults['currency'] > 0) {
            $currencyId = $defaults['currency'];
            $currency = Currency::where('id', $currencyId)->first();
            $exchangeRate   = $currency->latestRate();
            $currencyShortCode  = $currency->short_code;
        }

        // Fetch Report Data

        $results = Transaction::whereIn('country_id', $defaults['entities']);

        $results->select(
            'country_id',
            DB::raw('COUNT(id) AS trx_count'),
            DB::raw('SUM(amt_reward) AS amt_reward'),
            DB::raw('SUM(amt_redeem) AS amt_redeem'),
            DB::raw('SUM(trx_reward) AS trx_reward'),
            DB::raw('SUM(trx_redeem) AS trx_redeem'),
            DB::raw('SUM(points_rewarded) AS points_rewarded'),
            DB::raw('SUM(points_redeemed) AS points_redeemed'),
            DB::raw('SUM(total_amount) AS total_amount')
        );

        $results->whereIn('partner_id', Auth::User()->managedPartnerIDs());
        $results->where('created_at', '>=', $defaults['start_date']);
        $results->where('created_at', '<=', $defaults['end_date']);
        $results->where('network_id', $networkId);
        $results->groupBy('country_id');
        $results->orderBy('points_rewarded', 'DESC');

        $tot                        = array();
        $tot['exchangeRate']        = $exchangeRate;
        $tot['currencyShortCode']   = $currencyShortCode;
        $tot['currency'] = $currencyId;

        // Construct Response

        if (Input::has('export')) {
            $buffer[] = array(
                'Period',
                'Country',
                'Reward Trx',
                'Redeem Trx',
                'Points Rewarded',
                'Points Redeemed',
                'Amount Rewarded',
                'Amount Redeemed'
            );

            foreach ($results->get() as $u) {
                $buffer[] = array(
                    ReportHelper::periodString($defaults),
                    ReportHelper::defaultEmpty($u->country, 'name'),
                    number_format($u->trx_reward),
                    number_format($u->trx_redeem),
                    number_format($u->points_rewarded),
                    number_format($u->points_redeemed),
                    number_format($u->amt_reward * $exchangeRate, 2),
                    number_format($u->amt_redeem * $exchangeRate, 2)
                );
            }

            return ReportHelper::sendDownload($report, $buffer);
        } else {
            return ReportHelper::sendView($report, $defaults, $results, $tot);
        }
    }

    /**
     * Load all data for the 'Category Comparison' report
     *
     * @param Report $report
     * @return array
     */
    private function loadReport_12($report)
    {
        if (!I::can('view_full_reports')) {
            return Redirect::to(url("dashboard"));
        }

        $defaults = ReportHelper::normalizeDefaults();
        $networkId  = $defaults['network_currency'];
        // Currency
            $exchangeRate   = 1;
            $currencyShortCode  = 'USD';
        if ($defaults['currency'] > 0) {
            $currencyId = $defaults['currency'];
            $currency = Currency::where('id', $currencyId)->first();
            $exchangeRate   = $currency->latestRate();
            $currencyShortCode  = $currency->short_code;
        }

        // Fetch Report Data

        $results= TransactionItem::whereIn('transaction_item.product_category_id', $defaults['entities']);

        $results->leftJoin('transaction', 'transaction_item.transaction_id', '=', 'transaction.id');

        $results->select(
            'transaction_item.product_category_id AS category_id',
            DB::raw('COUNT(transaction.id) AS trx_count'),
            DB::raw('SUM(transaction.amt_reward) AS amt_reward'),
            DB::raw('SUM(transaction.amt_redeem) AS amt_redeem'),
            DB::raw('SUM(transaction.trx_reward) AS trx_reward'),
            DB::raw('SUM(transaction.trx_redeem) AS trx_redeem'),
            DB::raw('SUM(transaction.points_rewarded) AS points_rewarded'),
            DB::raw('SUM(transaction.points_redeemed) AS points_redeemed'),
            DB::raw('SUM(transaction.total_amount) AS total_amount')
        );
//		echo "<pre>";
//		print_r($results->get());
//		exit;
        $results->whereIn('transaction.partner_id', Auth::User()->managedPartnerIDs());
        $results->where('transaction.created_at', '>=', $defaults['start_date']);
        $results->where('transaction.created_at', '<=', $defaults['end_date']);
        $results->where('transaction.network_id', $networkId);
        $results->groupBy('transaction_item.product_category_id');
        $results->orderBy('transaction.points_rewarded', 'DESC');

        $tot                        = array();
        $tot['exchangeRate']        = $exchangeRate;
        $tot['currencyShortCode']   = $currencyShortCode;
        // Construct Response

        if (Input::has('export')) {
            $buffer[] = array(
                'Period',
                'Category',
                'Reward Trx',
                'Redeem Trx',
                'Points Rewarded',
                'Points Redeemed',
                'Amount Rewarded',
                'Amount Redeemed'
            );

            foreach ($results->get() as $u) {
                $buffer[] = array(
                    ReportHelper::periodString($defaults),
                    ReportHelper::defaultEmpty($u->category, 'name'),
                    number_format($u->trx_reward),
                    number_format($u->trx_redeem),
                    number_format($u->points_rewarded),
                    number_format($u->points_redeemed),
                    number_format($u->amt_reward * $exchangeRate, 2),
                    number_format($u->amt_redeem * $exchangeRate, 2)
                );
            }

            return ReportHelper::sendDownload($report, $buffer);
        } else {
            return ReportHelper::sendView($report, $defaults, $results, $tot);
        }
    }

    /**
     * Load all data for the 'Item Comparison' report
     *
     * @param Report $report
     * @return array
     */
    private function loadReport_13($report)
    {
        if (!I::can('view_full_reports')) {
            return Redirect::to(url("dashboard"));
        }

        $defaults = ReportHelper::normalizeDefaults();
        $networkId  = $defaults['network_currency'];
        // Currency
            $exchangeRate   = 1;
            $currencyShortCode  = 'USD';
        if ($defaults['currency'] > 0) {
            $currencyId = $defaults['currency'];
            $currency = Currency::where('id', $currencyId)->first();
            $exchangeRate   = $currency->latestRate();
            $currencyShortCode  = $currency->short_code;
        }

        // Fetch Report Data

        $results= TransactionItem::whereIn('transaction_item.product_id', $defaults['entities']);

        $results->leftJoin('transaction', 'transaction_item.transaction_id', '=', 'transaction.id');

        $results->select(
            'transaction_item.product_id',
            DB::raw('COUNT(transaction.id) AS trx_count'),
            DB::raw('SUM(transaction.amt_reward) AS amt_reward'),
            DB::raw('SUM(transaction.amt_redeem) AS amt_redeem'),
            DB::raw('SUM(transaction.trx_reward) AS trx_reward'),
            DB::raw('SUM(transaction.trx_redeem) AS trx_redeem'),
            DB::raw('SUM(transaction.points_rewarded) AS points_rewarded'),
            DB::raw('SUM(transaction.points_redeemed) AS points_redeemed'),
            DB::raw('SUM(transaction.total_amount) AS total_amount')
        );

        $results->whereIn('transaction.partner_id', Auth::User()->managedPartnerIDs());
        $results->where('transaction.created_at', '>=', $defaults['start_date']);
        $results->where('transaction.created_at', '<=', $defaults['end_date']);
        $results->where('transaction.network_id', $networkId);
        $results->groupBy('transaction_item.product_id');
        $results->orderBy('transaction.points_rewarded', 'DESC');

        $tot                        = array();
        $tot['exchangeRate']        = $exchangeRate;
        $tot['currencyShortCode']   = $currencyShortCode;

        // Construct Response

        if (Input::has('export')) {
            $buffer[] = array(
                'Period',
                'Item',
                'Reward Trx',
                'Redeem Trx',
                'Points Rewarded',
                'Points Redeemed',
                'Amount Rewarded',
                'Amount Redeemed'
            );

            foreach ($results->get() as $u) {
                $buffer[] = array(
                    ReportHelper::periodString($defaults),
                    ReportHelper::defaultEmpty($u->product, 'name'),
                    number_format($u->trx_reward),
                    number_format($u->trx_redeem),
                    number_format($u->points_rewarded),
                    number_format($u->points_redeemed),
                    number_format($u->amt_reward * $exchangeRate, 2),
                    number_format($u->amt_redeem * $exchangeRate, 2)
                );
            }

            return ReportHelper::sendDownload($report, $buffer);
        } else {
            return ReportHelper::sendView($report, $defaults, $results, $tot);
        }
    }

    /**
     * Load all data for the 'POS Comparison' report
     *
     * @param Report $report
     * @return array
     */
    private function loadReport_14($report)
    {
        if (!I::can('view_full_reports')) {
            return Redirect::to(url("dashboard"));
        }

        $defaults = ReportHelper::normalizeDefaults();
        $networkId  = $defaults['network_currency'];
        // Currency
            $exchangeRate   = 1;
            $currencyShortCode  = 'USD';
            $currencyId         = 6;
        if ($defaults['currency'] > 0) {
            $currencyId = $defaults['currency'];
            $currency = Currency::where('id', $currencyId)->first();
            $exchangeRate   = $currency->latestRate();
            $currencyShortCode  = $currency->short_code;
        }

        // Fetch Report Data

        $results = Transaction::whereIn('pos_id', $defaults['entities']);

        $results->select(
            'store_id',
            'pos_id',
            DB::raw('COUNT(id) AS trx_count'),
            DB::raw('SUM(amt_reward) AS amt_reward'),
            DB::raw('SUM(amt_redeem) AS amt_redeem'),
            DB::raw('SUM(trx_reward) AS trx_reward'),
            DB::raw('SUM(trx_redeem) AS trx_redeem'),
            DB::raw('SUM(points_rewarded) AS points_rewarded'),
            DB::raw('SUM(points_redeemed) AS points_redeemed'),
            DB::raw('SUM(total_amount) AS total_amount')
        );

        $results->whereIn('partner_id', Auth::User()->managedPartnerIDs());
        $results->where('created_at', '>=', $defaults['start_date']);
        $results->where('created_at', '<=', $defaults['end_date']);
        $results->where('network_id', $networkId);
        $results->groupBy('pos_id');
        $results->groupBy('store_id');
        $results->orderBy('points_rewarded', 'DESC');

        $tot                        = array();
        $tot['exchangeRate']        = $exchangeRate;
        $tot['currencyShortCode']   = $currencyShortCode;
        $tot['currency'] = $currencyId;
        // Construct Response

        if (Input::has('export')) {
            $buffer[] = array(
                'Period',
                'Store',
                'POS',
                'Reward Trx',
                'Redeem Trx',
                'Points Rewarded',
                'Points Redeemed',
                'Amount Rewarded',
                'Amount Redeemed'
            );

            foreach ($results->get() as $u) {
                $buffer[] = array(
                    ReportHelper::periodString($defaults),
                    ReportHelper::defaultEmpty($u->store, 'name'),
                    ReportHelper::defaultEmpty($u->category, 'name'),
                    number_format($u->trx_reward),
                    number_format($u->trx_redeem),
                    number_format($u->points_rewarded),
                    number_format($u->points_redeemed),
                    number_format($u->amt_reward * $exchangeRate, 2),
                    number_format($u->amt_redeem * $exchangeRate, 2)
                );
            }

            return ReportHelper::sendDownload($report, $buffer);
        } else {
            return ReportHelper::sendView($report, $defaults, $results, $tot);
        }
    }

    /**
     * Load all data for the 'Staff Comparison' report
     *
     * @param Report $report
     * @return array
     */
    private function loadReport_15($report)
    {
        if (!I::can('view_full_reports')) {
            return Redirect::to(url("dashboard"));
        }

        $defaults = ReportHelper::normalizeDefaults();
        $networkId  = $defaults['network_currency'];
        // Currency
            $exchangeRate   = 1;
            $currencyShortCode  = 'USD';
            $currencyId         = 6;
        if ($defaults['currency'] > 0) {
            $currencyId = $defaults['currency'];
            $currency = Currency::where('id', $currencyId)->first();
            $exchangeRate   = $currency->latestRate();
            $currencyShortCode  = $currency->short_code;
        }

        // Fetch Report Data

        $results = Transaction::whereIn('store_id', $defaults['entities']);

        $results->select(
            'store_id',
            'auth_staff_id',
            DB::raw('COUNT(id) AS trx_count'),
            DB::raw('SUM(amt_reward) AS amt_reward'),
            DB::raw('SUM(amt_redeem) AS amt_redeem'),
            DB::raw('SUM(trx_reward) AS trx_reward'),
            DB::raw('SUM(trx_redeem) AS trx_redeem'),
            DB::raw('SUM(points_rewarded) AS points_rewarded'),
            DB::raw('SUM(points_redeemed) AS points_redeemed'),
            DB::raw('SUM(total_amount) AS total_amount')
        );

        $results->whereIn('partner_id', Auth::User()->managedPartnerIDs());
        $results->whereNotNull('auth_role_id');
        $results->where('created_at', '>=', $defaults['start_date']);
        $results->where('created_at', '<=', $defaults['end_date']);
        $results->where('network_id', $networkId);
        $results->groupBy('auth_staff_id');
        $results->groupBy('store_id');
        $results->orderBy('points_rewarded', 'DESC');

        $tot                        = array();
        $tot['exchangeRate']        = $exchangeRate;
        $tot['currencyShortCode']   = $currencyShortCode;
        $tot['currency'] = $currencyId;
        // Construct Response

        if (Input::has('export')) {
            $buffer[] = array(
                'Period',
                'Store',
                'Staff',
                'Reward Trx',
                'Redeem Trx',
                'Points Rewarded',
                'Points Redeemed',
                'Amount Rewarded',
                'Amount Redeemed'
            );

            foreach ($results->get() as $u) {
                $buffer[] = array(
                    ReportHelper::periodString($defaults),
                    ReportHelper::defaultEmpty($u->store, 'name'),
                    ReportHelper::defaultEmpty($u->staff, 'name'),
                    number_format($u->trx_reward),
                    number_format($u->trx_redeem),
                    number_format($u->points_rewarded),
                    number_format($u->points_redeemed),
                    number_format($u->amt_reward * $exchangeRate, 2),
                    number_format($u->amt_redeem * $exchangeRate, 2)
                );
            }

            return ReportHelper::sendDownload($report, $buffer);
        } else {
            return ReportHelper::sendView($report, $defaults, $results, $tot);
        }
    }

    /**
     * Load all data for the 'Audit Trail' report
     *
     * @param Report $report
     * @return View
     */
    private function loadReport_16($report)
    {
        if (!I::can('view_risk_reports')) {
            return Redirect::to(url("dashboard"));
        }

        $defaults = ReportHelper::normalizeDefaults();

        // Fetch report data

        $results = Trail::whereIn('partner_id', $defaults['entities']);

        $results->where('created_at', '>=', $defaults['start_date']);
        $results->where('created_at', '<=', $defaults['end_date']);
        $results->orderBy('created_at', 'DESC');

        // Construct response

        if (Input::has('export')) {
            $buffer[] = array('Date & Time', 'Partner', 'Detail');

            foreach ($results->get() as $u) {
                $buffer[] = array(
                    ReportHelper::xlsDate($u->created_at),
                    ReportHelper::defaultEmpty($u->partner, 'name'),
                    $u->detail
                );
            }
            return ReportHelper::sendDownload($report, $buffer);
        } else {
            return ReportHelper::sendView($report, $defaults, $results);
        }
    }

    /**
     * Load all data for the 'Access Rights Report' report
     *
     * @param Report $report
     * @return View
     */
    private function loadReport_17($report)
    {
        if (!I::can('view_risk_reports')) {
            return Redirect::to(url("dashboard"));
        }

        $defaults = ReportHelper::normalizeDefaults();

        // Fetch report data

        $results = Admin::where('draft', false)->whereHas('partners', function ($q) use ($defaults) {
            $q->whereIn('partner.id', $defaults['entities']);
        });

        $results->whereHas('roles', function ($q) use ($defaults) {
            $q->whereIn('role.id', array(1,2,3));
        });

        $results->orderBy('id', 'DESC')->distinct();

        // Construct response

        if (Input::has('export')) {
            $buffer[] = array('First Name', 'Last Name', 'Mobile', 'Email', 'Gender', 'DOB');

            foreach ($results->get() as $u) {
                $firstName  = ($u->first_name != 'na') ? $u->first_name : 'N/A';
                $lastName   = ($u->last_name != 'na') ? $u->first_name : 'N/A';
                $buffer[] = array(
                    $firstName,
                    $lastName,
                    $u->normalizedContactNumber(),
                    $u->email,
                    $u->gender,
                    $u->dob
                );
            }

            return ReportHelper::sendDownload($report, $buffer);
        } else {
            return ReportHelper::sendView($report, $defaults, $results);
        }
    }

    /**
     * Load all data for the 'Removed Customers or Products' report
     *
     * @param Report $report
     * @return View
     */
    private function loadReport_18($report)
    {
        if (!I::can('view_risk_reports')) {
            return Redirect::to(url("dashboard"));
        }

        $defaults = ReportHelper::normalizeDefaults();

        $results = null;

        if (self::sanitizeText(Input::get('filter1')) == 'member') {
            //$results = Auth::User()->managedRemovedMembers();
            $results = User::onlyTrashed()->where('draft', false);
        } else {
//            $results = Auth::User()->managedRemovedProducts();
            $results = Product::onlyTrashed()->where('draft', false);
        }

            $results->where('deleted_at', '>=', $defaults['start_date']);
            $results->where('deleted_at', '<=', $defaults['end_date']);
            $results->orderBy('deleted_at', 'DESC');

        // Construct response

        if (Input::has('export')) {
            $buffer = array();

            if (Input::has('filter1')) {
                $buffer[] = array('Date & Time Remove', ucfirst(self::sanitizeText(Input::get('filter1'))));

                switch (self::sanitizeText(Input::get('filter1'))) {
                    case "member":
                        foreach ($results->get() as $u) {
                            $buffer[] = array(
                                ReportHelper::xlsDate($u->deleted_at),
                                ReportHelper::defaultEmpty($u, 'name')
                            );
                        }
                        break;
                    case "product":
                        foreach ($results->get() as $u) {
                            $buffer[] = array(
                                ReportHelper::xlsDate($u->deleted_at),
                                ReportHelper::defaultEmpty($u, 'name')
                            );
                        }
                        break;
                }
            }

            return ReportHelper::sendDownload($report, $buffer);
        } else {
            return ReportHelper::sendView($report, $defaults, $results);
        }
    }

    /**
     * Load all data for the 'Blocked Member Accounts' report
     *
     * @param Report $report
     * @return View
     */
    private function loadReport_19($report)
    {
        if (!I::can('view_risk_reports')) {
            return Redirect::to(url("dashboard"));
        }

        $defaults = ReportHelper::normalizeDefaults();

        // Fetch report data

        $timestamp = date("Y-m-d H:i:s", time() - 86400);

        $results = User::where('blocked', true)->where('blocked_date', '>', $timestamp)->whereHas('partners', function ($q) use ($defaults) {
            $q->whereIn('partner.id', $defaults['entities']);
        })->orderBy('id', 'DESC')->distinct();

        // Construct response

        if (Input::has('export')) {
            $buffer[] = array('First Name', 'Last Name', 'Mobile', 'Email', 'Gender', 'DOB');

            foreach ($results->get() as $u) {
                $firstName  = ($u->first_name != 'na') ? $u->first_name : 'N/A';
                $lastName   = ($u->last_name != 'na') ? $u->first_name : 'N/A';

                $buffer[] = array(
                    $firstName,
                    $lastName,
                    $u->normalizedContactNumber(),
                    $u->email,
                    $u->gender,
                    $u->dob
                );
            }

            return ReportHelper::sendDownload($report, $buffer);
        } else {
            return ReportHelper::sendView($report, $defaults, $results);
        }
    }

    /**
     * Load all data for the 'Members with default PIN codes' report
     *
     * @param Report $report
     * @return View
     */
    private function loadReport_20($report)
    {
        if (!I::can('view_risk_reports')) {
            return Redirect::to(url("dashboard"));
        }

        $defaults = ReportHelper::normalizeDefaults();

        // Fetch report data

        $results = User::where('confirm_passcode', false)->whereHas('partners', function ($q) use ($defaults) {
            $q->whereIn('partner.id', $defaults['entities']);
        })->orderBy('created_at', 'DESC')->distinct();

        // Construct response

        if (Input::has('export')) {
            $buffer[] = array('First Name', 'Last Name', 'Mobile', 'Email', 'Gender', 'DOB');
            $admin_partner = Auth::User()->getTopLevelPartner();
            $has_middleware = false;
            $user_info = array();

            foreach ($results->get() as $u) {
                if ($admin_partner->has_middleware ==1) {
                    $user_ids_used = array();
                    $has_middleware = true;
                    $user_ids_used[] = $u->id;
                    $term = "";
                    $url = $admin_partner->link_middleware_api . '?method=sensitiveInfo&api_key='.$admin_partner->middleware_api_key;
                    $json_ids = json_encode($user_ids_used);
                    //$url .= "&user_ids=" . $json_ids ;
                    $post_fields = "user_ids=" . $json_ids ;
                    $curl = curl_init();
                    // Set some options - we are passing in a useragent too here
                    curl_setopt_array($curl, array(
                        CURLOPT_RETURNTRANSFER => 1,
                        CURLOPT_URL => $url,
                        CURLOPT_SSL_VERIFYPEER => false,
                        CURLOPT_POST => true,
                        CURLOPT_POSTFIELDS => $post_fields,
                        CURLOPT_USERAGENT => 'Testing Sensitive Info'
                    ));
                    // Send the request & save response to $resp
                    $resp = curl_exec($curl);
                    $resp_curl = json_decode($resp);

                    $Users_info = $resp_curl;

                    foreach ($Users_info as $member) {
                        if (in_array($member->blu_id, $user_ids_used)) {
                            $first_name = $member->first_name;
                            $last_name = $member->last_name;
                            $mobile = $member->mobile;
                            $email = $member->email;
                        }
                    }
                } else {
                    $first_name = $u->first_name;
                    $last_name = $u->last_name;
                    $mobile = $u->normalizedContactNumber();
                    $email = $u->email;
                }
                $first_name  = ($first_name != 'na') ? $first_name : 'N/A';
                $last_name   = ($last_name != 'na') ? $last_name : 'N/A';
                $buffer[] = array(
                    $first_name,
                    $last_name,
                    $mobile,
                    $email,
                    $u->gender,
                    $u->dob
                );
            }

            return ReportHelper::sendDownload($report, $buffer);
        } else {
            return ReportHelper::sendView($report, $defaults, $results);
        }
    }
    /**
     * Load all data for the 'Members with default PIN codes' report
     *
     * @param Report $report
     * @return View
     */
    private function loadReport_21($report)
    {
        if (!I::can('view_blu_internal_reports')) {
            return Redirect::to(url("dashboard"));
        }

        $defaults = ReportHelper::normalizeDefaults();

        // Fetch report data

        $results['tot_balances'] = Balance::whereIn('network_id', $defaults['network_ids'])->sum('balance');
        $results['tot_redeem'] = Transaction::whereIn('network_id', $defaults['network_ids'])->where('trx_redeem', 1)->sum('points_redeemed');
        $results['tot_reward'] = Transaction::whereIn('network_id', $defaults['network_ids'])->where('trx_reward', 1)->sum('points_rewarded');
        $results['tot_trx'] = $results['tot_reward'] - $results['tot_redeem'];
        $results['tot_num_trx_reward'] = Transaction::whereIn('network_id', $defaults['network_ids'])->where('trx_reward', 1)->count();
        $results['tot_num_trx_redeem'] = Transaction::whereIn('network_id', $defaults['network_ids'])->where('trx_redeem', 1)->count();

        // Construct response

            return View::make("report.ejs.report_{$report->id}", array(
               'defaults'    => $defaults,
               'report'      => $report,
               'results'     => $results//,
            //'raw_results' => $raw_results,
            //'totals'      => $totals
            ));
        //return ReportHelper::sendView($report, $defaults, $results);
    }

    /**
     * get totals by period by segment / partner
     *
     * @param Report $report
     * @return array
     */
    private function loadReport_22($report)
    {
        if (!I::can('view_activity_summary_reports')) {
            return Redirect::to(url("dashboard"));
        }

        $defaults = ReportHelper::normalizeDefaults();

        $results = (isset($defaults['params']) && $defaults['params'] == 1) ? [] : null;

        // Fetch report data

        $tot = [];

        if (Input::has('export')) {
            $query = Transaction::whereBetween('transaction.created_at', [$defaults['start_date'], $defaults['end_date']])
                ->whereIn('partner_id', $defaults['entities']);

            if ($defaults['daterange'] != '') {
                if (strcmp($defaults['daterange'], 'daily') == 0) {
                    $query->select(DB::raw('DATE(transaction.created_at) creation, network_id, sum(points_rewarded) preward, sum(points_redeemed) predeem, sum(trx_reward) trx_rew, sum(trx_redeem) trx_red'))
                        ->groupBy('creation')
                        ->orderBy('creation');
                }
                if (strcmp($defaults['daterange'], 'monthly') == 0) {
                    $query->select(DB::raw('YEAR(transaction.created_at) creation, MONTH(transaction.created_at) crmonth, network_id, sum(points_rewarded) preward, sum(points_redeemed) predeem, sum(trx_reward) trx_rew, sum(trx_redeem) trx_red'))
                        ->groupBy('creation')
                        ->groupBy('crmonth')
                        ->orderBy('creation', 'DESC')
                        ->orderBy('crmonth', 'DESC');
                }
                if (strcmp($defaults['daterange'], 'yearly') == 0) {
                    $query->select(DB::raw('YEAR(transaction.created_at) creation, network_id, sum(points_rewarded) preward, sum(points_redeemed) predeem, sum(trx_reward) trx_rew, sum(trx_redeem) trx_red'))
                        ->groupBy('creation')
                        ->orderBy('creation', 'DESC');
                }
            } else {
                $query->select(DB::raw('DATE(transaction.created_at) creation, network_id, sum(points_rewarded) preward, sum(points_redeemed) predeem, sum(trx_reward) trx_rew, sum(trx_redeem) trx_red, amt_reward, amt_redeem'))
                    ->groupBy('creation')
                    ->orderBy('creation', 'DESC');
            }

            if (!empty($defaults['network_ids'])) {
                $query->whereIn('network_id', $defaults['network_ids']);
            }

            $data = $query->get()->toArray();

            foreach ($data as &$row) {
                $row['activity'] = number_format($row['preward'] - $row['predeem']);
                $row['preward'] = number_format($row['preward']);
                $row['trx_rew'] = number_format($row['trx_rew']);
                $row['predeem'] = number_format($row['predeem']);
                $row['trx_red'] = number_format($row['trx_red']);
                $row['date'] = $row['creation'];
                if (isset($row['crmonth'])) {
                    $dateObj   = \DateTime::createFromFormat('!m', $row['crmonth']);
                    $row['date'] = $dateObj->format('F') . ' ' . $row['creation'];
                }
                if (strlen($row['creation']) > 4) {
                    $dateObj = new \DateTime($row['creation']);
                    $dateformatted = $dateObj->format('F j, Y');
                    $row['date'] = $dateformatted;
                }
            }

            $buffer[] = array('Date', 'Pts Rewarded', 'Reward TRXs', 'Pts Redeemed', 'Redemption TRXs', 'Net Pts Activity' );

            foreach ($data as $u) {
                $buffer[] = array(
                    $u['date'],
                    $u['preward'],
                    $u['trx_rew'],
                    $u['predeem'],
                    $u['trx_red'],
                    $u['activity']
                );
            }
            return ReportHelper::sendDownload($report, $buffer);
        } else {
            return ReportHelper::sendView($report, $defaults, $results, $tot);
        }
    }

    /**
     * get totals by period by member
     *
     * @param Report $report
     * @return array
     */
    private function loadReport_23($report)
    {
        if (!I::can('view_activity_summary_reports')) {
            return Redirect::to(url("dashboard"));
        }

        $defaults = ReportHelper::normalizeDefaults();

        $results = (isset($defaults['params']) && $defaults['params'] == 1) ? [] : null;

        // Fetch report data

        $tot = [];

        if (Input::has('export')) {
            $query = Transaction::whereBetween('transaction.created_at', [$defaults['start_date'], $defaults['end_date']])
                ->join('partner', 'transaction.partner_id', '=', 'partner.id')
                ->where('partner_id', 1);

            if ($defaults['member_id']) {
                $query->whereIn('user_id', $defaults['member_id']);
            }

            if ($defaults['daterange'] != '') {
                if (strcmp($defaults['daterange'], 'daily') == 0) {
                    $query->select(DB::raw('DATE(transaction.created_at) creation, network_id, partner_id, partner.name as partner_name, sum(points_rewarded) preward, sum(points_redeemed) predeem, sum(trx_reward) trx_rew, sum(trx_redeem) trx_red'))
                        ->groupBy('creation')
                        ->orderBy('creation');
                }
                if (strcmp($defaults['daterange'], 'monthly') == 0) {
                    $query->select(DB::raw('YEAR(transaction.created_at) creation, MONTH(transaction.created_at) crmonth, network_id, partner_id, partner.name as partner_name, sum(points_rewarded) preward, sum(points_redeemed) predeem, sum(trx_reward) trx_rew, sum(trx_redeem) trx_red'))
                        ->groupBy('creation')
                        ->groupBy('crmonth')
                        ->orderBy('creation', 'DESC')
                        ->orderBy('crmonth', 'DESC');
                }
                if (strcmp($defaults['daterange'], 'yearly') == 0) {
                    $query->select(DB::raw('YEAR(transaction.created_at) creation, network_id, partner_id, partner.name as partner_name, sum(points_rewarded) preward, sum(points_redeemed) predeem, sum(trx_reward) trx_rew, sum(trx_redeem) trx_red'))
                        ->groupBy('creation')
                        ->orderBy('creation', 'DESC');
                }
            } else {
                $query->select(DB::raw('DATE(transaction.created_at) creation, network_id, partner_id, partner.name as partner_name, sum(points_rewarded) preward, sum(points_redeemed) predeem, sum(trx_reward) trx_rew, sum(trx_redeem) trx_red, amt_reward, amt_redeem'))
                    ->groupBy('creation')
                    ->orderBy('creation', 'DESC');
            }

            if ($defaults['bypartner'] != 'not') {
                $query->groupBy('partner_id');
            }

            if (!empty($defaults['network_ids'])) {
                $query->whereIn('network_id', $defaults['network_ids']);
            }

            $data = $query->get()->toArray();

            foreach ($data as &$row) {
                $row['activity'] = number_format($row['preward'] - $row['predeem']);
                $row['preward'] = number_format($row['preward']);
                $row['trx_rew'] = number_format($row['trx_rew']);
                $row['predeem'] = number_format($row['predeem']);
                $row['trx_red'] = number_format($row['trx_red']);
                $row['date'] = $row['creation'];
                if (isset($row['crmonth'])) {
                    $dateObj   = \DateTime::createFromFormat('!m', $row['crmonth']);
                    $monthName = $dateObj->format('M');
                    $row['date'] = $monthName . ". ";
                }
                if (strlen($row['creation']) > 4) {
                    $dateObj = new \DateTime($row['creation']);
                    $dateformatted = $dateObj->format('F j, Y');
                    $row['date'] = $dateformatted;
                }
            }

            $buffer[] = array('Date', 'Network / Partner', 'Pts Rewarded', 'Reward TRXs', 'Pts Redeemed', 'Redemption TRXs', 'Net Pts Activity' );

            foreach ($data as $u) {
                $buffer[] = array(
                    $u['date'],
                    $u['partner_name'],
                    $u['preward'],
                    $u['trx_rew'],
                    $u['predeem'],
                    $u['trx_red'],
                    $u['activity']
                );
            }
            return ReportHelper::sendDownload($report, $buffer);
        } else {
            return ReportHelper::sendView($report, $defaults, $results, $tot);
        }
    }

    /**
     * get totals by period by store
     *
     * @param Report $report
     * @return array
     */
    private function loadReport_24($report)
    {
        if (!I::can('view_activity_summary_reports')) {
            return Redirect::to(url("dashboard"));
        }

        $defaults = ReportHelper::normalizeDefaults();

        $results = (isset($defaults['params']) && $defaults['params'] == 1) ? [] : null;

        // Fetch report data

        $tot = [];

        if (Input::has('export')) {
            $query = Transaction::whereBetween('transaction.created_at', [$defaults['start_date'], $defaults['end_date']])
                ->join('partner', 'transaction.partner_id', '=', 'partner.id')
                ->join('store', 'transaction.store_id', '=', 'store.id')
                ->whereIn('store_id', $defaults['entities'])
                ->whereIn('transaction.partner_id', $defaults['parent_ids'])
                ->groupBy('store_id');

            if ($defaults['daterange'] != '') {
                if (strcmp($defaults['daterange'], 'daily') == 0) {
                    $query->select(DB::raw('DATE(transaction.created_at) creation, transaction.partner_id, transaction.store_id,
                    partner.name as partner_name, store.name as store_name, sum(points_rewarded) preward, sum(points_redeemed) predeem, sum(trx_reward) trx_rew, sum(trx_redeem) trx_red'))
                        ->groupBy('creation')
                        ->orderBy('creation');
                }
                if (strcmp($defaults['daterange'], 'monthly') == 0) {
                    $query->select(DB::raw('YEAR(transaction.created_at) creation, MONTH(transaction.created_at) crmonth, transaction.partner_id, transaction.store_id,
                    partner.name as partner_name, store.name as store_name, sum(points_rewarded) preward, sum(points_redeemed) predeem, sum(trx_reward) trx_rew, sum(trx_redeem) trx_red'))
                        ->groupBy('creation')
                        ->groupBy('crmonth')
                        ->orderBy('creation', 'DESC')
                        ->orderBy('crmonth', 'DESC');
                }
                if (strcmp($defaults['daterange'], 'yearly') == 0) {
                    $query->select(DB::raw('YEAR(transaction.created_at) creation, transaction.partner_id, transaction.store_id,
                    partner.name as partner_name, store.name as store_name, sum(points_rewarded) preward, sum(points_redeemed) predeem, sum(trx_reward) trx_rew, sum(trx_redeem) trx_red'))
                        ->groupBy('creation')
                        ->orderBy('creation', 'DESC');
                }
            } else {
                $query->select(DB::raw('DATE(transaction.created_at) creation, transaction.partner_id, transaction.store_id,
                partner.name as partner_name, store.name as store_name,
                sum(points_rewarded) preward, sum(points_redeemed) predeem, sum(trx_reward) trx_rew, sum(trx_redeem) trx_red, amt_reward, amt_redeem'))
                    ->groupBy('creation')
                    ->orderBy('creation', 'DESC');
            }

            if (!empty($defaults['network_ids'])) {
                $query->whereIn('network_id', $defaults['network_ids']);
            }

            $data = $query->get()->toArray();

            foreach ($data as &$row) {
                $row['activity'] = number_format($row['preward'] - $row['predeem']);
                $row['preward'] = number_format($row['preward']);
                $row['trx_rew'] = number_format($row['trx_rew']);
                $row['predeem'] = number_format($row['predeem']);
                $row['trx_red'] = number_format($row['trx_red']);
                $row['date'] = $row['creation'];
                if (isset($row['crmonth'])) {
                    $dateObj   = \DateTime::createFromFormat('!m', $row['crmonth']);
                    $monthName = $dateObj->format('M');
                    $row['date'] = $monthName . ". ";
                }
                if (strlen($row['creation']) > 4) {
                    $dateObj = new \DateTime($row['creation']);
                    $dateformatted = $dateObj->format('F j, Y');
                    $row['date'] = $dateformatted;
                }
            }

            $buffer[] = array('Date', 'Partner / Branch', 'Pts Rewarded', 'Reward TRXs', 'Pts Redeemed', 'Redemption TRXs', 'Net Pts Activity' );

            foreach ($data as $u) {
                $buffer[] = array(
                    $u['date'],
                    $u['partner_name'] . ', ' . $u['store_name'],
                    $u['preward'],
                    $u['trx_rew'],
                    $u['predeem'],
                    $u['trx_red'],
                    $u['activity']
                );
            }
            return ReportHelper::sendDownload($report, $buffer);
        } else {
            return ReportHelper::sendView($report, $defaults, $results, $tot);
        }
    }

    /**
     * get totals by period by network
     *
     * @param Report $report
     * @return array
     */
    private function loadReport_25($report)
    {
        if (!I::can('view_activity_summary_reports')) {
            return Redirect::to(url("dashboard"));
        }

        $defaults = ReportHelper::normalizeDefaults();

        // Fetch report data

        $results = [];
        $tot = [];

        if (Input::has('export')) {
            $partners = Auth::User()->managedPartnerIDs();
            $results = Transaction::whereBetween('transaction.created_at', [$defaults['start_date'], $defaults['end_date']])
                                ->join('partner', 'transaction.partner_id', '=', 'partner.id')
                                ->join('network', 'transaction.network_id', '=', 'network.id')
                                ->whereIn('transaction.network_id', $defaults['network_ids'])
                                ->whereIn('transaction.partner_id', $partners);

            if ($defaults['daterange'] != '') {
                if (strcmp($defaults['daterange'], 'daily') == 0) {
                    $results->select(
                        DB::raw('DATE(transaction.created_at) creation'),
                        'partner.name as partner_name',
                        'network.name as network_name',
                        DB::raw('sum(points_rewarded) preward'),
                        DB::raw('sum(points_redeemed) predeem'),
                        DB::raw('sum(trx_reward) trx_rew'),
                        DB::raw('sum(trx_redeem) trx_red')
                    )
                          ->groupBy('network_id')
                          ->groupBy('creation')
                          ->orderBy('creation', 'DESC');
                }
                if (strcmp($defaults['daterange'], 'monthly') == 0) {
                    $results->select(DB::raw('YEAR(transaction.created_at) creation, MONTH(transaction.created_at) crmonth, partner.name as partner_name, network.name as network_name, sum(points_rewarded) preward, sum(points_redeemed) predeem, sum(trx_reward) trx_rew, sum(trx_redeem) trx_red'))
                          ->groupBy('network_name')
                          ->groupBy('creation')
                          ->groupBy('crmonth')
                          ->orderBy('creation', 'DESC')
                          ->orderBy('crmonth', 'DESC');
                }
                if (strcmp($defaults['daterange'], 'yearly') == 0) {
                    $results->select(DB::raw('YEAR(transaction.created_at) creation, partner.name as partner_name, network.name as network_name, sum(points_rewarded) preward, sum(points_redeemed) predeem, sum(trx_reward) trx_rew, sum(trx_redeem) trx_red'))
                          ->orderBy('creation', 'DESC')
                          ->groupBy('network_name')
                          ->groupBy('creation');
                }
            } else {
                $results->select(DB::raw('DATE(transaction.created_at) creation, partner.name as partner_name, network.name as network_name, sum(points_rewarded) preward, sum(points_redeemed) predeem, sum(trx_reward) trx_rew, sum(trx_redeem) trx_red'))
                      ->groupBy('network_name')
                      ->groupBy('creation')
                      ->orderBy('creation', 'DESC');
            }

            if (!empty($defaults['network_ids'])) {
                $results->whereIn('network_id', $defaults['network_ids']);
            }

            if ($defaults['bypartner'] != 'not') {
                $results->groupBy('partner_name');
            }

            $buffer[] = array('Date', 'Network / Partner', 'Pts Rewarded', 'Reward TRXs', 'Pts Redeemed', 'Redemption TRXs', 'Net Pts Activity' );

            foreach ($results->get() as $u) {
                $date = '';
                $secondColumn = '';

                if (isset($u->crmonth)) {
                    $date = $u->crmonth . ' / ' . $u->creation;
                } else {
                    $date = $u->creation;
                }

                if ($defaults['bypartner'] != 'not') {
                    $secondColumn = $u->network_name . ' / ' . $u->partner_name;
                } else {
                    $secondColumn = $u->network_name;
                }
                $net_pts_activity = floatval($u->preward) - floatval($u->predeem);


                $buffer[] = array(
                    $date,
                    $secondColumn,
                    $u->preward,
                    $u->trx_rew,
                    $u->predeem,
                    $u->trx_red,
                    $net_pts_activity
                );
            }
            return ReportHelper::sendDownload($report, $buffer);
        } else {
            return ReportHelper::sendView($report, $defaults, $results, $tot);
        }
    }

    /**
     * get total redemptions by period by partner
     *
     * @param Report $report
     * @return array
     */
    private function loadReport_26($report)
    {
        $repository = new RepositoryReport26;

        $defaults = ReportHelper::normalizeDefaults();

        $networkId  = $defaults['network_currency'];

        $results =$repository->startDate($defaults['start_date'])
                             ->endDate($defaults['end_date'])
                             ->networkId($networkId)
                             ->partnerId(array_intersect($defaults['entities'] ?: [], Auth::user()->managedPartnerIds() ?: []))
                             ->groupPeriod($defaults['daterange'] ?: 'daily')
                             ->getData();

        if (Input::get('export'))
        {
            $buffer[] = [
                'Date',
                'Partner',
                'Pts Used',
            ];

            foreach ($results as $u)
            {
                if (isset($u->crmonth))
                {
                    $date = $u->crmonth . ' / ' . $u->creation;
                } else
                {
                    $date = $u->creation;
                }

                $net_pts_activity = floatval($u->ptsused);

                $buffer[] = [
                    $date,
                    $u->partner_name,
                    $net_pts_activity,
                ];
            }

            return ReportHelper::sendDownload($report, $buffer);
        } else
        {
            return View::make('report.ejs.report_' . $report->id, [
                'defaults' => $defaults,
                'report' => $report,
                'resultRalph' => $results,
            ]);
        }
    }

    /**
     * get total redemptions by period by member
     *
     * @param Report $report
     * @return array
     */
    private function loadReport_27($report)
    {
        if (!I::can('view_redemption_partner_rewarded_points_reports')) {
            return Redirect::to(url("dashboard"));
        }

        $defaults = ReportHelper::normalizeDefaults();
        $networkId  = $defaults['network_currency'];
        $partners = Auth::User()->managedPartnerIDs();

        //$userTopLevelPartnerId	= Auth::User()->getTopLevelPartner()->id;
        $resTrxRedeem = Transaction::select(DB::raw('id'))
                    ->where('created_at', '>=', $defaults['start_date'])
                    ->where('created_at', '<=', $defaults['end_date'])
                    ->where('trx_redeem', 1)
                    ->whereIn('user_id', $defaults['entities'])
                    ->where('network_id', $networkId);


        $rtrx_redeem = array();
        foreach ($resTrxRedeem->get() as $rtrx) {
            $rtrx_redeem[] = $rtrx->id;
        }
        if (empty($rtrx_redeem)) {
            $rtrx_redeem[] = '-1';
        }

        $resTrxDeduction = TransactionDeduction::select(DB::raw('trx_id'))
                        ->whereIn('trx_ref_id', $rtrx_redeem);

        $rtrx_deduction_reward = array();
        foreach ($resTrxDeduction->get() as $resTD) {
            $rtrx_deduction_reward[] = $resTD->trx_id;
        }
        if (empty($rtrx_deduction_reward)) {
            $rtrx_deduction_reward[] = '-1';
        }
        // Fetch report data

        $results = array();
        $resTrxReward = Transaction::select(DB::raw('id, partner_id'))
                    ->whereIn('id', $rtrx_deduction_reward)
                    ->whereIn('user_id', $defaults['entities'])
                    ->where('network_id', $networkId)
                ->whereIn('partner_id', $partners);
        $rtrx_partner = array();
        foreach ($resTrxReward->get() as $rtrx) {
            if (isset($rtrx_partner[$rtrx->partner_id])) {
                $rtrx_partner[$rtrx->partner_id] = $rtrx_partner[$rtrx->partner_id] . "," . $rtrx->id;
            } else {
                $rtrx_partner[$rtrx->partner_id] =  $rtrx->id;
            }
        }
        foreach ($rtrx_partner as $key => $value) {
            $arr_rtrx_id = explode(",", $value);
//            if($defaults['daterange'] != '') {
//                if(strcmp($defaults['daterange'], 'daily') == 0){
//                    $results[$key] = TransactionDeduction::select(DB::raw('DATE(created_at) creation, sum(points_used) ptsused'))
//                        ->where('created_at', '>=', $defaults['start_date'])
//                        ->where('created_at', '<=', $defaults['end_date'])
//                        ->whereIn('trx_id', $arr_rtrx_id)
//                        ->orderBy('creation', 'DESC')->groupBy('creation')->get();
//                }
//                if(strcmp($defaults['daterange'], 'monthly') == 0){
//                    $results[$key] = TransactionDeduction::select(DB::raw('YEAR(created_at) creation, MONTH(created_at) crmonth, sum(points_used) ptsused'))
//                        ->where('created_at', '>=', $defaults['start_date'])
//                        ->where('created_at', '<=', $defaults['end_date'])
//                        ->whereIn('trx_id', $arr_rtrx_id)
//                        ->orderBy('creation', 'DESC')->orderBy('crmonth','DESC')->groupBy('creation')->groupBy('crmonth')->get();
//                }
//                if(strcmp($defaults['daterange'], 'yearly') == 0){
//                    $results[$key] = TransactionDeduction::select(DB::raw('YEAR(created_at) creation, sum(points_used) ptsused'))
//                        ->where('created_at', '>=', $defaults['start_date'])
//                        ->where('created_at', '<=', $defaults['end_date'])
//                        ->whereIn('trx_id', $arr_rtrx_id)
//                        ->orderBy('creation', 'DESC')->groupBy('creation')->get();
//                }
//
//
//            }
//            else{
                $results[$key] = TransactionDeduction::select(DB::raw('DATE(created_at) creation,trx_ref_id, sum(points_used) ptsused'))
                        ->where('created_at', '>=', $defaults['start_date'])
                        ->where('created_at', '<=', $defaults['end_date'])
                        ->whereIn('trx_id', $arr_rtrx_id)
                        ->orderBy('creation', 'DESC')->groupBy('trx_ref_id')->get();
//            }
        }



        if (Input::has('export')) {
            $buffer[] = array('Date', 'Partner', 'Pts Used' );

            foreach ($results as $partner_id => $result) {
                foreach ($result as $u) {
                    $date = '';
                    $secondColumn = '';

                    if (isset($u->crmonth)) {
                        $date = $u->crmonth . ' / ' . $u->creation;
                    } else {
                        $date = $u->creation;
                    }
//					$network = Network::find($u->network_id);
                    if ($defaults['bypartner'] != 'not') {
                        $partner = Partner::find($partner_id);
                        $secondColumn =  $partner->name;
                    } else {
//						$secondColumn = $network->name;
                        $partner = Partner::find($partner_id);
                        $secondColumn =  $partner->name;
                    }

                    $net_pts_activity = floatval($u->ptsused);


                    $buffer[] = array(
                        $date,
                        $secondColumn,
                        $net_pts_activity
                    );
                }
            }

            return ReportHelper::sendDownload($report, $buffer);
        } else {
            return View::make("report.ejs.report_{$report->id}", array(
            'defaults'    => $defaults,
            'report'      => $report,
            //'results'     => $obj_results,
            'resultRalph' => $results//,
            //'raw_results' => $raw_results,
            //'totals'      => $totals
            ));
            //return ReportHelper::sendView($report, $defaults, $obj_results);
        }
    }
    /**
     * get total redemptions by period by transaction
     *
     * @param Report $report
     * @return array
     */
    private function loadReport_28($report)
    {
        if (!I::can('view_redemption_partner_rewarded_points_reports')) {
            return Redirect::to(url("dashboard"));
        }

        $defaults = ReportHelper::normalizeDefaults();
        $networkId  = $defaults['network_currency'];
        //$partners = Auth::User()->managedPartnerIDs();
        $resTrxRedeem = Transaction::select(DB::raw('id'))
                    ->where('created_at', '>=', $defaults['start_date'])
                    ->where('created_at', '<=', $defaults['end_date'])
                    ->where('trx_redeem', 1)
                    ->where('network_id', $networkId);
        $rtrx_redeem = array();
        foreach ($resTrxRedeem->get() as $rtrx) {
            $rtrx_redeem[] = $rtrx->id;
        }
        if (empty($rtrx_redeem)) {
            $rtrx_redeem[] = '-1';
        }
        $resTrxDeduction = TransactionDeduction::select(DB::raw('trx_id, trx_ref_id'))
                        ->whereIn('trx_ref_id', $rtrx_redeem);

        $rtrx_deduction_reward = array();
        foreach ($resTrxDeduction->get() as $resTD) {
            if (isset($rtrx_deduction_reward[$resTD->trx_ref_id])) {
                $rtrx_deduction_reward[$resTD->trx_ref_id] = $rtrx_deduction_reward[$resTD->trx_ref_id] . "," . $resTD->trx_id;
            } else {
                $rtrx_deduction_reward[$resTD->trx_ref_id] =  $resTD->trx_id;
            }
        }
        if (empty($rtrx_deduction_reward)) {
            $rtrx_deduction_reward[] = '-1';
        }
        // Fetch report data

        $results = array();
        $rtrx_partner = array();
        foreach ($rtrx_deduction_reward as $key1 => $val1) {
            $rtrx_deduction_rew1 = explode(",", $val1);
            $resTrxReward = Transaction::select(DB::raw('id, partner_id'))
                        ->whereIn('id', $rtrx_deduction_rew1)
                        ->whereIn('partner_id', $defaults['entities'])
                        ->where('network_id', $networkId);

            foreach ($resTrxReward->get() as $rtrx) {
                if (isset($rtrx_partner[$key1][$rtrx->partner_id])) {
                    $rtrx_partner[$key1][$rtrx->partner_id] = $rtrx_partner[$key1][$rtrx->partner_id] . "," . $rtrx->id;
                } else {
                    $rtrx_partner[$key1][$rtrx->partner_id] =  $rtrx->id;
                }
            }
        }
        foreach ($rtrx_partner as $key2 => $val2) {
            foreach ($val2 as $key3 => $value) {
                $arr_rtrx_id = explode(",", $value);
                if ($defaults['daterange'] != '') {
                    if (strcmp($defaults['daterange'], 'daily') == 0) {
                        $results[$key2][$key3] = TransactionDeduction::select(DB::raw('DATE(created_at) creation, sum(points_used) ptsused'))
                            ->where('created_at', '>=', $defaults['start_date'])
                            ->where('created_at', '<=', $defaults['end_date'])
                            ->whereIn('trx_id', $arr_rtrx_id)
                            ->orderBy('creation', 'DESC')->groupBy('creation')->get();
                    }
                    if (strcmp($defaults['daterange'], 'monthly') == 0) {
                        $results[$key2][$key3] = TransactionDeduction::select(DB::raw('YEAR(created_at) creation, MONTH(created_at) crmonth, sum(points_used) ptsused'))
                            ->where('created_at', '>=', $defaults['start_date'])
                            ->where('created_at', '<=', $defaults['end_date'])
                            ->whereIn('trx_id', $arr_rtrx_id)
                            ->orderBy('creation', 'DESC')->orderBy('crmonth', 'DESC')->groupBy('creation')->groupBy('crmonth')->get();
                    }
                    if (strcmp($defaults['daterange'], 'yearly') == 0) {
                        $results[$key2][$key3] = TransactionDeduction::select(DB::raw('YEAR(created_at) creation, sum(points_used) ptsused'))
                            ->where('created_at', '>=', $defaults['start_date'])
                            ->where('created_at', '<=', $defaults['end_date'])
                            ->whereIn('trx_id', $arr_rtrx_id)
                            ->orderBy('creation', 'DESC')->groupBy('creation')->get();
                    }
                } else {
                    $results[$key2][$key3] = TransactionDeduction::select(DB::raw('DATE(created_at) creation, sum(points_used) ptsused'))
                            ->where('created_at', '>=', $defaults['start_date'])
                            ->where('created_at', '<=', $defaults['end_date'])
                            ->whereIn('trx_id', $arr_rtrx_id)
                            ->orderBy('creation', 'DESC')->groupBy('creation')->get();
                }
            }
        }

        if (Input::has('export')) {
            $buffer[] = array('Date', 'Redemption Transaction', 'Partner', 'Pts used' );
//			$buffer[] = array('Date', 'Redemption Transaction', 'Partner', 'Pts Rewarded', 'Reward TRXs', 'Pts Redeemed', 'Redemption TRXs', 'Net Pts Activity' );

            foreach ($results as $trans => $result) {
                foreach ($result as $partner_id => $a) {
                    foreach ($a as $u) {
                        $date = '';
                        $secondColumn = '';

                        if (isset($u->crmonth)) {
                            $date = $u->crmonth . ' / ' . $u->creation;
                        } else {
                            $date = $u->creation;
                        }
        //                $network = Network::find($u->network_id);
                        if ($defaults['bypartner'] != 'not') {
                            $partner = Partner::find($partner_id);
                            $secondColumn =  $partner->name;
                        } else {
    //						$secondColumn = $network->name;
                            $partner = Partner::find($partner_id);
                            $secondColumn =  $partner->name;
                        }

                        $net_pts_activity = floatval($u->ptsused);


                        $buffer[] = array(
                            $date,
                            $trans,
                            $secondColumn,
                            $net_pts_activity
                        );
//						$buffer[] = array(
//							$date,
//							$trans,
//							$secondColumn,
//							$u->preward,
//							$u->trx_rew,
//							$u->predeem,
//							$u->trx_red,
//							$net_pts_activity
//						);
                    }
                }
            }

            return ReportHelper::sendDownload($report, $buffer);
        } else {
            return View::make("report.ejs.report_{$report->id}", array(
            'defaults'    => $defaults,
            'report'      => $report,
            //'results'     => $obj_results,
            'resultRalph' => $results//,
            //'raw_results' => $raw_results,
            //'totals'      => $totals
            ));
            //return ReportHelper::sendView($report, $defaults, $obj_results);
        }
    }


    /**
     * Load all data for the 'All Members and balances' report
     *
     * @param Report $report
     * @return array
     */
    private function loadReport_29($report)
    {
        if (!I::can('view_basic_reports')) {
            return Redirect::to(url("dashboard"));
        }

        $defaults = ReportHelper::normalizeDefaults();

        // Fetch report data
        $results = array();
        $arr_partner_ids = array();
        $has_middleware = false;
        $url="";
        $Users_info = array();
        $userTopLevelPartnerId  = Auth::User()->getTopLevelPartner()->id;
        foreach ($defaults['filter1'] as $partn) {
            $partner1 = Partner::find($partn);
            if ($partner1) {
                if ($partner1->has_middleware==1 && $userTopLevelPartnerId == $partner1->id) {
                    $has_middleware = true;
                    $url = $partner1->link_middleware_api . '?method=sensitiveInfo&api_key='.$partner1->middleware_api_key;
                    $arr_partner_ids[] = $partner1->id;
                    break;
                }
            }
        }

        if ($defaults['zerobalance'] != 'not') {
            $defaults['operator'] = ">=";
        } else {
            $defaults['operator'] = ">";
        }

        if ($defaults['filter1'][0] != -1 && $has_middleware == false) {
            $results = User::where('draft', false)->where('ref_account', false)->whereHas('partners', function ($q) use ($defaults) {
                $q->whereIn('partner.id', $defaults['filter1']);
            })->whereHas('userbalances', function ($q) use ($defaults) {
                $q->whereIn('balance.network_id', $defaults['network_ids'])->where('balance.balance', $defaults['operator'], "0");
            })->orderBy('id', 'DESC')->distinct();

        } elseif ($has_middleware == true) {
            $results = User::where('draft', false)->where('ref_account', false)->whereHas('partners', function ($q) use ($arr_partner_ids) {
                $q->whereIn('partner.id', $arr_partner_ids);
            })->whereHas('userbalances', function ($q) use ($defaults) {
                $q->whereIn('balance.network_id', $defaults['network_ids'])->where('balance.balance', $defaults['operator'], "0");
            })->orderBy('id', 'DESC')->distinct();

            $user_ids = array();
            foreach ($results->get() as $res1) {
                if (!in_array($res1->id, $user_ids)) {
                    $user_ids[] = $res1->id;
                }
            }
            $json_user_ids = json_encode($user_ids);
            //$url = $url."&user_ids=" . $json_user_ids;
            $post_fields = "user_ids=" . $json_user_ids ;
            $curl = curl_init();
            // Set some options - we are passing in a useragent too here
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => $url,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => $post_fields,
                CURLOPT_USERAGENT => 'Testing Sensitive Info'
            ));
            // Send the request & save response to $resp
            $resp = curl_exec($curl);
            $resp_curl = json_decode($resp);

            $Users_info = $resp_curl;
        }
        $sensitive['has_middleware'] = $has_middleware;
        $sensitive['user_info'] = $Users_info;
        $sensitive['partner_ids'] = $arr_partner_ids;
        // Construct response

        if (Input::has('export')) {
            $buffer[] = array('ID', 'Reference', 'Member Name', 'Mobile', 'Email', 'Status', 'Balance');

            foreach ($results->get() as $u) {
                $mobile = 'N/a';
                $email = 'N/a';
                if ($sensitive['has_middleware'] ==1) {
                    foreach ($sensitive['user_info'] as $u_info) {
                        if ($u->id == $u_info->blu_id) {
                            $first_name = $u_info->first_name;
                            $last_name = $u_info->last_name;
                            $mobile = $u_info->mobile;
                            $email = $u_info->email;
                        }
                    }
                } else {
                    $first_name = $u->first_name;
                    $last_name = $u->last_name;
                    $mobile = $u->normalizedContactNumber();
                    $email = $u->email;
                }

                $namebal = '';
                foreach ($u->balances() as $bal) {
                    if (in_array($bal->network_id, $defaults['network_ids'])) {
                        $netw = Network::find($bal->network_id);
                        $namebal =  $bal->balance;
                    }
                }

                if (empty($first_name) || $first_name == 'na') {
                    $first_name = 'N/A';
                }
                if (empty($last_name) || $last_name == 'na') {
                    $last_name = 'N/A';
                }
                $buffer[] = array(
                    $u->id,
                    $u->references()->where('partner_id', $userTopLevelPartnerId)->pluck('number')->implode(', '),
                    $first_name . ' ' . $last_name,
                    $mobile,
                    $email,
                    $u->status,
                    $namebal
                );
            }
//            echo "<pre>";
//            var_dump($buffer);
//            exit();
            return ReportHelper::sendDownload($report, $buffer);
        } else {
            return ReportHelper::sendView($report, $defaults, $results, null, $sensitive, ['userTopLevelPartnerId' => $userTopLevelPartnerId]);
        }
    }

    /**
     * Load all data for the 'Audit Trail user' report
     *
     * @param Report $report
     * @return View
     */
    private function loadReport_30($report)
    {
        if (!I::can('view_risk_reports')) {
            return Redirect::to(url("dashboard"));
        }

        $defaults = ReportHelper::normalizeDefaults();

        // Fetch report data

        $results = TrailUser::whereIn('audit_trail_users.partner_id', $defaults['entities']);

        $results->where('audit_trail_users.created_at', '>=', $defaults['start_date']);
        $results->where('audit_trail_users.created_at', '<=', $defaults['end_date']);

        if (!empty($defaults['network_ids']) && current($defaults['network_ids']) != -1) {
            $results->leftJoin('partner', 'partner.id', '=', 'audit_trail_users.partner_id');
            $results->leftJoin('network_partner', function ($join) use ($defaults) {
                $join->on('partner.id', '=', 'partner.id')
                     ->whereIn('network_partner.network_id', $defaults['network_ids']);
            });
            $results->whereNotNull('network_partner.network_id');
        }

        $results->orderBy('audit_trail_users.created_at', 'DESC');
        // Construct response

        if (Input::has('export')) {
            $buffer[] = array('ID', 'Date & Time', 'Partner', 'Member', 'Detail');

            foreach ($results->get() as $u) {
                $admin_partner = Auth::User()->getTopLevelPartner();
                $has_middleware = false;
                $user_info = array();
                if ($admin_partner->has_middleware == '1') {
                    $user_ids_used = array();
                    $has_middleware = true;
                    $user_ids_used[] = $u->user->id;
                    $term = "";
                    $url = $admin_partner->link_middleware_api . '?method=sensitiveInfo&api_key='.$admin_partner->middleware_api_key;
                    $json_ids = json_encode($user_ids_used);
                //$url .= "&user_ids=" . $json_ids ;
                    $post_fields = "user_ids=" . $json_ids ;
                    $curl = curl_init();
                // Set some options - we are passing in a useragent too here
                    curl_setopt_array($curl, array(
                    CURLOPT_RETURNTRANSFER => 1,
                    CURLOPT_URL => $url,
                    CURLOPT_SSL_VERIFYPEER => false,
                    CURLOPT_POST => true,
                    CURLOPT_POSTFIELDS => $post_fields,
                    CURLOPT_USERAGENT => 'Testing Sensitive Info'
                    ));
                // Send the request & save response to $resp
                    $resp = curl_exec($curl);
                    $resp_curl = json_decode($resp);

                    $Users_info = $resp_curl;

                    foreach ($Users_info as $member) {
                        if (in_array($member->blu_id, $user_ids_used)) {
                            $id = $member->blu_id;
                            $name = $member->first_name . ' ' . $member->last_name . ' (' . $member->blu_id . ')';
                        }
                    }
                } else {
                    $id         = $u->id;
                    $name = $u->first_name . ' ' . $u->last_name . ' (' . $u->id . ')';
                }
                $fullName   = $u->first_name . ' ' . $u->last_name;
                if ($fullName == 'na na') {
                    $name = 'N/A';
                }
                $buffer[] = array(
                    $id,
                    ReportHelper::xlsDate($u->created_at),
                    ReportHelper::defaultEmpty($u->partner, 'name'),
                    $name,
                    $u->detail
                );
            }
            return ReportHelper::sendDownload($report, $buffer);
        } else {
            return ReportHelper::sendView($report, $defaults, $results);
        }
    }


    /**
     * get points redeemed at supplier
     *
     * @param Report $report
     * @return array
     */
    private function loadReport_31($report)
    {
        if (!I::can('view_redemption_partner_rewarded_points_reports')) {
            return Redirect::to(url("dashboard"));
        }

        $defaults = ReportHelper::normalizeDefaults();

        // Currency
            $exchangeRate   = 1;
            $currencyShortCode  = 'USD';
        if ($defaults['currency'] > 0) {
            $currencyId = $defaults['currency'];
            $currency = Currency::where('id', $currencyId)->first();
            $exchangeRate   = $currency->latestRate();
            $currencyShortCode  = $currency->short_code;
        }

        $resTrxRedeem = Transaction::select('transaction.id', 'transaction.created_at', 'transaction.user_id', 'product.price_in_usd', 'product.supplier_id as supplierId', 'product.retail_price_in_usd as retailPrice', 'product.name as name', 'transaction_item.quantity as quantity')
                               ->where('transaction.created_at', '>=', $defaults['start_date'])
                               ->where('transaction.created_at', '<=', $defaults['end_date'])
                               ->where('transaction.trx_redeem', 1)
                               ->whereIn('product.supplier_id', $defaults['suppliers']);

        $resTrxRedeem->leftjoin('transaction_item', 'transaction.id', '=', 'transaction_item.transaction_id');
        $resTrxRedeem->leftjoin('product', 'transaction_item.product_id', '=', 'product.id');
        $results    = $resTrxRedeem->get();


        $totals = array('retailPrice'=>0, 'qty'=>0, 'exchangeRate'=>1, 'currencyShortCode'=>'usd');
        $sortDate = array();
        $totalRetailPrice   = 0;
        foreach ($results as $result) {
            $retailByQty            = $result->retailPrice * $result->quantity;
            $totalRetailPrice       += $retailByQty;
        }
//		$totals['retailPrice']			= $results->sum('retailPrice');
        $totals['retailPrice']          = $totalRetailPrice;
        $totals['qty']                  = $results->sum('quantity');
        $totals['exchangeRate']         = $exchangeRate;
        $totals['currencyShortCode']    = $currencyShortCode;

        if (Input::has('export')) {
            $buffer[] = array('Date', 'Transaction ID', 'User ID', 'First Name', 'Last Name', 'Supplier',  'Points Redeemed', 'cash', 'Amount Redeemed');

            foreach ($resultsAll as $u) {
                $user = User::find($u['user_id']);

                $buffer[] = array(
                    ReportHelper::xlsDate($u['date']),
                    $u['transaction_id'],
                    $u['user_id'],
                    ReportHelper::defaultEmpty($user, 'first_name'),
                    ReportHelper::defaultEmpty($user, 'last_name'),
                    $u['supplier'],

                    $u['points_redeemed'],
                    $u['cash_payment'] * $exchangeRate,
                    $u['amount'] * $exchangeRate
                );
            }
            return ReportHelper::sendDownload($report, $buffer);
        } else {
            return View::make("report.ejs.report_{$report->id}", array(
            'defaults'    => $defaults,
            'report'      => $report,
            'results' => $results,
            'totals'      => $totals
            ));
        }
    }

    /**
     * Load all data for the 'Points Issued By Partner for Partner Members' report
     *
     * @param Report $report
     * @return array
     */
    private function loadReport_32($report)
    {
        if (!I::can('view_premium_reports')) {
            return Redirect::to(url("dashboard"));
        }

        $defaults = ReportHelper::normalizeDefaults();

        // Fetch report data

        $partners_ids_str = implode(",", $defaults['entities']);
            $resIssuedPoints = DB::select(DB::raw("SELECT
            t.user_id,
                sum(t.points_rewarded) pts_reward,
                sum(td.p_used) pts_used,
                    sum(t.points_redeemed) pts_redeem
        FROM
            transaction t
                    left join
                (select trx_id, sum(points_used) p_used from transaction_deduction group by trx_id) td ON t.id = td.trx_id

        where
            t.partner_id in ($partners_ids_str) and t.created_at >= '".$defaults['start_date'] . "'and t.created_at <= '".$defaults['end_date'] . "'
        group by t.user_id "));
        $tot = array();
        $tot['pts_redeem'] = 0;
        $tot['pts_reward'] = 0;
                $tot['outstanding_balance'] = 0;

        foreach ($resIssuedPoints as $resIss) {
                    $temp = Transaction::select(DB::raw('sum(points_rewarded) pts_reward'))
                            ->where('user_id', $resIss->user_id)->whereIn('partner_id', $defaults['entities']);
                    $temp2 = DB::select(DB::raw("select
                        sum(points_used) pts_used
                    from
                        transaction_deduction
                    where
                        trx_id in (select
                                id
                            from
                                transaction
                            where
                                user_id = " . $resIss->user_id . " and partner_id in (" . $partners_ids_str . "))"));
                    $temp1 = $temp->first();
            $user = User::find($resIss->user_id);
                    $resIss->user = $user;
                    $resIss->refs = DB::table('reference')->where('user_id', $resIss->user_id)->get();

            if ($temp1->pts_reward !== null && !empty($temp1->pts_reward)) {
                $resIss->balance = $temp1->pts_reward - $temp2[0]->pts_used;
            } else {
                $resIss->balance = 0;
            }
                    $tot['pts_redeem'] += $resIss->pts_used;
                    $tot['pts_reward'] += $resIss->pts_reward;
                    $tot['outstanding_balance'] += $resIss->balance;
        }

//		$resIssuedPoints = Transaction::select('transaction.user_id', 'balance.balance',
//                                    DB::raw('sum(transaction.points_rewarded) as points_rewarded'),
//                                    DB::raw('sum(transaction_deduction.points_used) as points_used'))
//                       ->where('transaction.created_at', '>=', $defaults['start_date'])
//                       ->where('transaction.created_at', '<=', $defaults['end_date'])
//                       ->whereIn('transaction.partner_id', $defaults['entities']);
//
//		$resIssuedPoints->leftjoin('transaction_deduction', 'transaction.id','=', 'transaction_deduction.trx_id');
//		$resIssuedPoints->leftjoin('balance', 'transaction.user_id','=', 'balance.user_id');
//		$resIssuedPoints->groupBy('transaction.user_id');
        $results    = $resIssuedPoints;
//		$results->groupBy('transaction.user_id');
//		$deduction = TransactionDeduction::select('transaction_deduction.points_used')->get();
//		$queries = DB::getQueryLog();
//		$last_query = end($queries);
//		echo "<pre>";
//		print_r($last_query);
//		exit;

        // Construct Totals

        // Construct Response

        if (Input::has('export')) {
            $buffer[] = array('User ID', 'Member Name','Reference', 'Points Rewarded', 'Points Used', 'Outstanding Balance');
//            echo "<pre>";
//            var_dump($results);
//            exit();
            foreach ($results as $u) {
                $refs = "";
                if (isset($u->refs[0])) {
                    $refs = $u->refs[0]->number;
                }
                $buffer[] = array(
                    $u->user_id,
                    $u->user->first_name . " " . $u->user->last_name,
                    $refs,
                    number_format($u->pts_reward),
                    number_format($u->pts_used),
                    number_format($u->balance)
                );
            }

            return ReportHelper::sendDownload($report, $buffer);
        } else {
            return View::make("report.ejs.report_{$report->id}", array(
            'defaults'    => $defaults,
            'report'      => $report,
            'results' => $results,
            'tot'     => $tot
            ));
        }
    }

    /**
     * Load all data for the 'points redeemed by redemption category' report
     *
     * @param Report $report
     * @return array
     */
    private function loadReport_34($report)
    {
        if(!I::can('view_redemption_partner_rewarded_points_reports')){
            return Redirect::to(url("dashboard"));
        }

        $defaults = ReportHelper::normalizeDefaults();
        $redemptionCategories	= $defaults['redemptioncategories'];
        $all_partners = $defaults['entities'];

        // Currency
        $exchangeRate	= 1;
        $currencyShortCode	= 'USD';
        if($defaults['currency'] > 0){
            $currencyId	= $defaults['currency'];
            $currency = Currency::where('id', $currencyId)->first();
            $exchangeRate	= $currency->latestRate();
            $currencyShortCode	= $currency->short_code;
        }

        $results = Transaction::select('transaction.id','transaction.created_at', 'transaction.user_id', 'transaction.points_redeemed', 'transaction.total_amount', 'transaction.cash_payment', 'transaction.amt_redeem','transaction.ref_number','transaction.notes', 'transaction.trans_class')
                              ->where('transaction.created_at', '>=', $defaults['start_date'])
                              ->where('transaction.created_at', '<=', $defaults['end_date'])
                              ->where ('transaction.trx_redeem',1);

        $res = $results->get();
        $arr_items = array();
        $arr_points = array();
        $arr_amounts = array();
        $arr_flights  = array();
        $arr_hotels = array();
        $arr_cars = array();
        $arr_trx_ids = array(-1);
        $arr_trx_ref_ids = array(-1);

        foreach($res as $res1){
            $trx_class = $res1->trans_class;
            switch ($trx_class){
                case "Points":
                    $arr_points[] = $res1;
                    break;
                case "Amount":
                    $arr_amounts[] = $res1;
                    break;
                case "Items":
                    $arr_items[] = $res1;
                    break;
                case "Flight":
                    $arr_flights[] = $res1;
                    break;
                case "Hotel":
                    $arr_hotels[] = $res1;
                    break;
                case "Car":
                    $arr_cars[] = $res1;
                    break;
                default:
                    break;
            }
        }

        $prod_part_supp = array();
        $prod_supp = array();
        $arr_cashback = array();
        $arr_transfer = array();
        foreach($arr_points as $arr_pts){
            if(stripos($arr_pts->notes,"Cashback") !== False){
                $arr_cashback[] = $arr_pts;
            }
            elseif(stripos($arr_pts->notes,"Points Transfer") !== False){
                $arr_transfer[] = $arr_pts;
            }
        }

        // Construct Response
        $tot_cat = array();
        $tot = array();
        $tot['points'] = 0;
        $tot['amount'] = 0;
        $tot['total_trxs'] = 0;
        $tot['total_cash_payment'] = 0;

        $temp_tot_pts_redeemed = 0;
        $temp_tot_cash_payments = 0;

        //Cashback
        $total_trx_cashback = 0;
        foreach($arr_cashback as $cashback){
            $temp_tot_pts_redeemed += $cashback->points_redeemed;
            $temp_tot_cash_payments += $cashback->cash_payment;
            $total_trx_cashback = $total_trx_cashback + 1 ;
        }

        $temp_tot_amount_redeemed = PointsHelper::pointsToAmount($temp_tot_pts_redeemed);
        $tot['points'] += $temp_tot_pts_redeemed;
        $tot['amount'] += $temp_tot_amount_redeemed * $exchangeRate;
        $tot['total_trxs']+= $total_trx_cashback;
        $tot['total_cash_payment']+= $temp_tot_cash_payments * $exchangeRate;
        $tot_cat['cashback']['points'] = $temp_tot_pts_redeemed;
        $tot_cat['cashback']['amount'] = $temp_tot_amount_redeemed * $exchangeRate;
        $tot_cat['cashback']['total_trx'] = $total_trx_cashback;
        $tot_cat['cashback']['cash_payment'] = $temp_tot_cash_payments * $exchangeRate;

        $temp_tot_pts_redeemed = 0;
        $temp_tot_cash_payments = 0;

        //Points transfer
        $total_trx_transfer = 0;
        foreach($arr_transfer as $transfer){
            $temp_tot_pts_redeemed += $transfer->points_redeemed;
            $temp_tot_cash_payments += $transfer->cash_payment;
            $total_trx_transfer = $total_trx_transfer + 1 ;
        }
        $temp_tot_amount_redeemed = PointsHelper::pointsToAmount($temp_tot_pts_redeemed);
        $tot['points'] += $temp_tot_pts_redeemed;
        $tot['amount'] += $temp_tot_amount_redeemed * $exchangeRate;
        $tot['total_trxs']+= $total_trx_transfer;
        $tot['total_cash_payment']+= $temp_tot_cash_payments * $exchangeRate;
        $tot_cat['transfer']['points'] = $temp_tot_pts_redeemed;
        $tot_cat['transfer']['amount'] = $temp_tot_amount_redeemed * $exchangeRate;
        $tot_cat['transfer']['total_trx'] = $total_trx_transfer;
        $tot_cat['transfer']['cash_payment'] = $temp_tot_cash_payments * $exchangeRate;

        $temp_tot_pts_redeemed = 0;
        $temp_tot_cash_payments = 0;

        //Flight
        $total_trx_flight = 0;
        foreach($arr_flights as $flight){
            $temp_tot_pts_redeemed += $flight->points_redeemed;
            $temp_tot_cash_payments += $flight->cash_payment;
            $total_trx_flight = $total_trx_flight + 1;
        }
        $temp_tot_amount_redeemed = PointsHelper::pointsToAmount($temp_tot_pts_redeemed);
        $tot['points'] += $temp_tot_pts_redeemed;
        $tot['amount'] += $temp_tot_amount_redeemed * $exchangeRate;
        $tot['total_trxs']+= $total_trx_flight;
        $tot['total_cash_payment']+= $temp_tot_cash_payments * $exchangeRate;
        $tot_cat['flight']['points'] = $temp_tot_pts_redeemed;
        $tot_cat['flight']['amount'] = $temp_tot_amount_redeemed * $exchangeRate;
        $tot_cat['flight']['total_trx'] = $total_trx_flight;
        $tot_cat['flight']['cash_payment'] = $temp_tot_cash_payments * $exchangeRate;

        $temp_tot_pts_redeemed = 0;
        $temp_tot_cash_payments = 0;

        //Hotel
        $total_trx_hotel = 0;
        foreach($arr_hotels as $hotel){
            $temp_tot_pts_redeemed += $hotel->points_redeemed;
            $temp_tot_cash_payments += $hotel->cash_payment;
            $total_trx_hotel = $total_trx_hotel + 1;
        }
        $temp_tot_amount_redeemed = PointsHelper::pointsToAmount($temp_tot_pts_redeemed);
        $tot['points'] += $temp_tot_pts_redeemed;
        $tot['amount'] += $temp_tot_amount_redeemed * $exchangeRate;
        $tot['total_trxs']+= $total_trx_hotel;
        $tot['total_cash_payment']+= $temp_tot_cash_payments * $exchangeRate;
        $tot_cat['hotel']['points'] = $temp_tot_pts_redeemed;
        $tot_cat['hotel']['amount'] = $temp_tot_amount_redeemed * $exchangeRate;
        $tot_cat['hotel']['total_trx'] = $total_trx_hotel;
        $tot_cat['hotel']['cash_payment'] = $temp_tot_cash_payments * $exchangeRate;

        $temp_tot_pts_redeemed = 0;
        $temp_tot_cash_payments = 0;

        //Car
        $total_trx_car = 0;
        foreach($arr_cars as $car){
            $temp_tot_pts_redeemed += $car->points_redeemed;
            $temp_tot_cash_payments += $car->cash_payment;
            $total_trx_car = $total_trx_car + 1;
        }
        $temp_tot_amount_redeemed = PointsHelper::pointsToAmount($temp_tot_pts_redeemed);
        $tot['points'] += $temp_tot_pts_redeemed;
        $tot['amount'] += $temp_tot_amount_redeemed * $exchangeRate;
        $tot['total_trxs']+= $total_trx_car;
        $tot['total_cash_payment']+= $temp_tot_cash_payments * $exchangeRate;
        $tot_cat['car']['points'] = $temp_tot_pts_redeemed;
        $tot_cat['car']['amount'] = $temp_tot_amount_redeemed * $exchangeRate;
        $tot_cat['car']['total_trx'] = $total_trx_car;
        $tot_cat['car']['cash_payment'] = $temp_tot_cash_payments * $exchangeRate;

        $temp_tot_amount_redeemed = 0;
        $temp_tot_cash_payments = 0;
        $temp_delivery_costs = 0;

        //partner supplier items
        $total_trx_supp = 0;
        $prev_trx_ded_ids = array();

        foreach($prod_part_supp as $part_supp){
            $trx_deductionsss = TransactionDeduction::where('trx_ref_id',$part_supp->transaction_id)->get();
            $temp_delivery_costs += $part_supp->delivery_cost;
            foreach($trx_deductionsss as $trxdeduct){
                $trx_rewards = Transaction::find($trxdeduct->trx_id);
                if(in_array($trx_rewards->partner_id, $all_partners) && !in_array($trxdeduct->id, $prev_trx_ded_ids) ){
                    $ptstoamountused = PointsHelper::pointsToAmount($trxdeduct->points_used);
                    $temp_tot_amount_redeemed += $ptstoamountused;
                    $temp_tot_cash_payments += $part_supp->cash_payment;
                    $total_trx_supp = $total_trx_supp + 1 ;
                    $prev_trx_ded_ids[]= $trxdeduct->id;
                }
            }
        }


        $temp_tot_pts_redeemed = PointsHelper::amountToPoints($temp_tot_amount_redeemed);
        $tot['points'] += $temp_tot_pts_redeemed + $temp_delivery_costs;
        $tot['amount'] += $temp_tot_amount_redeemed * $exchangeRate;
        $tot['total_trxs']+= $total_trx_supp;
        $tot['total_cash_payment']+= $temp_tot_cash_payments * $exchangeRate;
        $tot_cat['part_supp']['points'] = $temp_tot_pts_redeemed + $temp_delivery_costs;
        $tot_cat['part_supp']['amount'] = $temp_tot_amount_redeemed * $exchangeRate;
        $tot_cat['part_supp']['total_trx'] = $total_trx_supp;
        $tot_cat['part_supp']['cash_payment'] = $temp_tot_cash_payments * $exchangeRate;

        $temp_tot_amount_redeemed = 0;
        $temp_tot_cash_payments = 0;
        $temp_delivery_costs = 0;

        //items at other partner items
        $total_trx_oth_supp = 0;
        $prev_trx_dedec_ids = array();
        foreach($prod_supp as $oth_supp){
            $trx_deductionss = TransactionDeduction::where('trx_ref_id',$oth_supp->transaction_id)->get();
            foreach($trx_deductionss as $trxdeduct){
                $trx_rewards = Transaction::find($trxdeduct->trx_id);
                if(in_array($trx_rewards->partner_id, $all_partners) && !(in_array($trxdeduct->id, $prev_trx_dedec_ids)) ){
                    $ptstoamountused = PointsHelper::pointsToAmount($trxdeduct->points_used);
                    $temp_tot_amount_redeemed += $ptstoamountused;
                    $temp_tot_cash_payments += $oth_supp->cash_payment;
                    $total_trx_oth_supp = $total_trx_oth_supp + 1 ;
                    $prev_trx_dedec_ids[] = $trxdeduct->id;
                }
            }
        }
        $temp_tot_pts_redeemed = PointsHelper::amountToPoints($temp_tot_amount_redeemed);
        $tot['points'] += $temp_tot_pts_redeemed + $temp_delivery_costs;
        $tot['amount'] += $temp_tot_amount_redeemed * $exchangeRate;
        $tot['total_trxs']+= $total_trx_oth_supp;
        $tot['total_cash_payment']+= $temp_tot_cash_payments * $exchangeRate;
        $tot_cat['oth_supp']['points'] = $temp_tot_pts_redeemed + $temp_delivery_costs;
        $tot_cat['oth_supp']['amount'] = $temp_tot_amount_redeemed * $exchangeRate;
        $tot_cat['oth_supp']['total_trx'] =$total_trx_oth_supp;
        $tot_cat['oth_supp']['cash_payment'] = $temp_tot_cash_payments * $exchangeRate;

        if(Input::has('export')) {
            $buffer[] = array('CATEGORY', 'TOTAL TRXS', 'TOTAL PTS REDEEMED', 'TOTAL CASH PAID', 'TOTAL AMOUNT REDEEMED');

            $buffer[] = [
                'Cashback',
                number_format($tot_cat['cashback']['total_trx']),
                number_format($tot_cat['cashback']['points']),
                number_format($tot_cat['cashback']['cash_payment']),
                number_format($tot_cat['cashback']['amount']),
            ];

            $buffer[] = [
                'Flights',
                number_format($tot_cat['flight']['total_trx']),
                number_format($tot_cat['flight']['points']),
                number_format($tot_cat['flight']['cash_payment']),
                number_format($tot_cat['flight']['amount']),
            ];

            $buffer[] = [
                'Hotels',
                number_format($tot_cat['hotel']['total_trx']),
                number_format($tot_cat['hotel']['points']),
                number_format($tot_cat['hotel']['cash_payment']),
                number_format($tot_cat['hotel']['amount']),
            ];

            $buffer[] = [
                'Cars',
                number_format($tot_cat['car']['total_trx']),
                number_format($tot_cat['car']['points']),
                number_format($tot_cat['car']['cash_payment']),
                number_format($tot_cat['car']['amount']),
            ];

            $buffer[] = [
                'Items at Suppliers',
                number_format($tot_cat['oth_supp']['total_trx']),
                number_format($tot_cat['oth_supp']['points']),
                number_format($tot_cat['oth_supp']['cash_payment']),
                number_format($tot_cat['oth_supp']['amount']),
            ];

            $buffer[] = [
                'Items at selected partners',
                number_format($tot_cat['part_supp']['total_trx']),
                number_format($tot_cat['part_supp']['points']),
                number_format($tot_cat['part_supp']['cash_payment']),
                number_format($tot_cat['part_supp']['amount']),
            ];

            $buffer[] = [
                'Transfers',
                number_format($tot_cat['transfer']['total_trx']),
                number_format($tot_cat['transfer']['points']),
                number_format($tot_cat['transfer']['cash_payment']),
                number_format($tot_cat['transfer']['amount']),
            ];

//            foreach($results->get() as $u) {
//                $buffer[] = array(
//                    $u->user_id,
//                    number_format($u->points_rewarded),
//                    number_format($u->points_used),
//                    number_format($u->points_rewarded - $u->points_used)
//                );
//            }

            return ReportHelper::sendDownload($report, $buffer);
        } else {
            return View::make("report.ejs.report_{$report->id}", array(
                'defaults'    => $defaults,
                'report'      => $report,
                'results' => $tot_cat,
                'tot'	  => $tot
            ));
        }
    }


         /**
     * Load all data for the 'points redeemed by redemption category' report
     *
     * @param Report $report
     * @return array
     */
    private function loadReport_35($report)
    {
        if (!I::can('view_redemption_partner_rewarded_points_reports')) {
            return Redirect::to(url("dashboard"));
        }

         $defaults = ReportHelper::normalizeDefaults();

         $networkId = $defaults['network_currency'];
        // Currency
        $exchangeRate   = 1;
        $currencyShortCode  = 'USD';
        if ($defaults['currency'] > 0) {
                $currencyId = $defaults['currency'];
                $currency = Currency::where('id', $currencyId)->first();
                $exchangeRate   = $currency->latestRate();
                $currencyShortCode  = $currency->short_code;
        }

        $resTrxRedeem = Transaction::select(
            'product.supplier_id as supplierId',
            DB::raw('transaction.points_redeemed as total_pts_redeemed'),
            DB::raw('transaction.delivery_cost as total_delivery_pts_redeemed'),
            DB::raw('transaction.amt_redeem as total_amount_redeemed'),
            DB::raw('transaction.cash_payment as total_cash_paid'),
            DB::raw('transaction.id as trxId')
        )->where('transaction.created_at', '>=', $defaults['start_date'])
                ->where('transaction.created_at', '<=', $defaults['end_date'])
                ->where('transaction.trx_redeem', 1)
                ->where('transaction.points_redeemed', '>', 0)
                ->whereIn('product.supplier_id', $defaults['suppliers']);
        $resTrxRedeem->leftjoin('transaction_item', 'transaction.id', '=', 'transaction_item.transaction_id');
        $resTrxRedeem->leftjoin('product', 'transaction_item.product_id', '=', 'product.id');
//        $resTrxRedeem->groupBy('product.supplier_id');
        $query = $resTrxRedeem->toSql();
        $resultsObj = $resTrxRedeem->get();

        $results    = new BluCollection();
        if ($resultsObj->count() > 0) {
            foreach ($resultsObj as $result) {
                $suplierId  = $result->supplierId;
                $trxId      = $result->trxId;
                $transactionItems   = TransactionItem::where('transaction_id', $trxId)->get();

                foreach ($transactionItems as $transactionItem) {
                    $productDetails     = Product::find($transactionItem->product_id);
                    if (!$productDetails) {
                        continue;
                        unset($transactionItem);
                    }
                    $productSupplierId  = $productDetails->supplier_id;
//                    $productPrice       = $productDetails->price_in_points;
                    $productPrice       = PointsHelper::amountToPoints($transactionItem->original_total_amount, $transactionItem->currency_id, $networkId);
                    $currency           = Currency::where('id', $transactionItem->currency_id)->first();
                    $currency_shortcode =  $currency->short_code;
                    $productDelivery       = PointsHelper::pointsToAmount($transactionItem->delivery_cost, $currency_shortcode, $networkId);
//                    $productDelivery       = $productDetails->delivery_charges;

                    $total_pts_redeemed = $result->total_pts_redeemed;
                    $result->original_total_points_redeemed  = $total_pts_redeemed;
                    $result->total_pts_redeemed -= $result->delivery_cost;

                    if ($suplierId != $productSupplierId) {
                            $result->total_pts_redeemed -= $productPrice;
                            $result->total_pts_redeemed -= $transactionItem->delivery_cost;
                            $result->total_delivery_pts_redeemed -=  $transactionItem->delivery_cost;
                            $result->total_amount_redeemed -=  $transactionItem->original_total_amount;
                            $result->total_amount_redeemed -=  $productDelivery;
                    }
                    $result->currency_id =  $transactionItem->currency_id;
                }
            }

            foreach ($defaults['suppliers'] as $key => $supplier) {
                $transobj = new Transaction();

                foreach ($resultsObj as $result) {
                    if ($result->supplierId == $supplier) {
                        $currency = Currency::where('id', $result->currency_id)->first();
                        $currency_shortcode =  $currency->short_code;
                        $deliveryAmount   = PointsHelper::pointsToAmount($result->total_delivery_pts_redeemed, $currency_shortcode, $networkId);
                        $totPtsRedeemd  = $result->total_pts_redeemed - $result->total_delivery_pts_redeemed;
                        $totAmtRedeemed = $result->total_amount_redeemed - $deliveryAmount;

                        $transobj->supplierId                 = $result->supplierId;
                        $transobj->total_pts_redeemed        += $totPtsRedeemd;
                        $transobj->total_amount_redeemed     += $totAmtRedeemed;
                        $transobj->total_cash_paid           += $result->total_cash_paid;

                        //calculate cash payments
                        if ($transobj->total_cash_paid > 0) {
                            $pricePercentage = ($transobj->total_pts_redeemed * 100) / $result->original_total_points_redeemed;
                            $transobj->total_cash_paid = ( $result->total_cash_paid * $pricePercentage ) / 100;
                        }
                    }
                }
                $results->push($transobj);
            }
        }

//        $resTrxRedeem = Transaction::select('product.supplier_id as supplierId',
//                DB::raw('sum(transaction.points_redeemed) as total_pts_redeemed'),
//                DB::raw('sum(transaction.delivery_cost) as total_delivery_pts_redeemed'),
//                DB::raw('sum(transaction.amt_redeem) as total_amount_redeemed'),
//                DB::raw('sum(transaction.cash_payment) as total_cash_paid')
//                )->where('transaction.created_at', '>=', $defaults['start_date'])
//                ->where('transaction.created_at', '<=', $defaults['end_date'])
//                ->where ('transaction.trx_redeem',1)
//                ->whereIn('product.supplier_id', $defaults['suppliers']);
//        $resTrxRedeem->leftjoin('transaction_item', 'transaction.id','=', 'transaction_item.transaction_id');
//        $resTrxRedeem->leftjoin('product', 'transaction_item.product_id', '=','product.id' );
//        $resTrxRedeem->groupBy('product.supplier_id');
//        $query = $resTrxRedeem->toSql();
//        $results	= $resTrxRedeem->get();

        $totals = array( 'totalpointsredeemed'=>0, 'totalcashpayments'=>0, 'totalamounredeemed'=>0);
//		$totalRetailPrice	= 0;
//		foreach( $results as $result){
//			$retailByQty			= $result->retailPrice * $result->quantity;
//			$totalRetailPrice		+= $retailByQty;
//		}
//		$totals['retailPrice']			= $results->sum('retailPrice');
//        if(isset($results['supplierId']) && !empty($results['supplierId']) ){
            $totals['totalpointsredeemed']      = $results->sum('total_pts_redeemed') - $results->sum('total_delivery_pts_redeemed');
            $totals['totalamounredeemed']               = ( $results->sum('total_amount_redeemed') + $results->sum('total_cash_paid') ) * $exchangeRate;
            $totals['totalcashpayments']        = ($results->sum('total_cash_paid')) * $exchangeRate;
//        }
        $totals['exchangeRate']     = $exchangeRate;

        if (Input::has('export')) {
            $buffer[] = array('Supplier', 'Total Pts Redeemed', 'Total Cash Paid', 'Total Amount Redeemed');

            foreach ($results as $key => $resMa) {
                $suppliername = Supplier::find($resMa->supplierId)->name;
                $buffer[] = array(
                    $suppliername ,
                    $resMa['total_pts_redeemed'] -  $resMa['total_delivery_pts_redeemed'],
                    number_format($resMa['total_cash_paid'] * $exchangeRate, 2),
                    number_format(( $resMa['totalamounredeemed'] +  $resMa['totalamounredeemed'] ) * $exchangeRate, 2)
                 );
            }
            return ReportHelper::sendDownload($report, $buffer);
        } else {
            return View::make("report.ejs.report_{$report->id}", array(
            'defaults'    => $defaults,
            'report'      => $report,
            'results' => $results,
            'totals'      => $totals,
               'query'=> $query
            ));
        }
    }


    /**
     * Redemption of Partner Issued Points per Transaction
     *
     * @param Report $report
     * @return array
     */
    private function loadReport_36($report)
    {
        if (!I::can('view_redemption_partner_rewarded_points_reports')) {
            return Redirect::to(url("dashboard"));
        }

        $defaults = ReportHelper::normalizeDefaults();
        //$partners = Auth::User()->managedPartnerIDs();

        // Fetch report data
        $results = Transaction::select(DB::raw('DATE(transaction.created_at) creation, transaction.id as trx_ref_id,
            users.first_name, users.last_name, sum(points_redeemed) ptsused'))
        ->join('users', 'transaction.user_id', '=', 'users.id')
        ->whereBetween('transaction.created_at', [$defaults['start_date'], $defaults['end_date']])
        ->where('transaction.trx_redeem', 1)
        ->where('transaction.partner_id', 1)
        ->orderBy('trx_ref_id', 'DESC')
        ->groupBy('trx_ref_id')
        ->get();

        if (Input::has('export')) {
            $buffer[] = array('Date','Transaction,', 'Member', 'Pts Redeemed' );

            foreach ($results as $u) {
                if (isset($u->crmonth)) {
                    $date = $u->crmonth . ' / ' . $u->creation;
                } else {
                    $date = $u->creation;
                }

                $name = $u->first_name . ' ' . $u->last_name;
                if (trim($name) != 'na na') {
                    $name = $name;
                } else {
                    $name = "N/A";
                }

                $net_pts_activity = floatval($u->ptsused);

                $buffer[] = array(
                    $date,
                    $u->trx_ref_id,
                    $name,
                    $net_pts_activity,
                );
            }

            return ReportHelper::sendDownload($report, $buffer);
        } else {
            return View::make("report.ejs.report_{$report->id}", array(
            'defaults'    => $defaults,
            'report'      => $report,
            //'results'     => $obj_results,
            'resultRalph' => $results//,
            //'raw_results' => $raw_results,
            //'totals'      => $totals
            ));
            //return ReportHelper::sendView($report, $defaults, $obj_results);
        }
    }

    /**
     * The report will show the discrepancies between the member balance and the points rewarded - points redeemed.
     *
     * @param Report $report
     * @return array
     */
    private function loadReport_37($report)
    {
        if (!I::can('view_blu_internal_reports')) {
            return Redirect::to(url("dashboard"));
        }

        $results        = array();
        $res            = array();
        $totalCount     = 0;
        $defaults = ReportHelper::normalizeDefaults();

        $networkIds = $defaults['network_ids'];

        if (!in_array('-1', $networkIds)) {
            foreach ($networkIds as $networkId) {
                $res = Transaction::select(DB::raw('balance.user_id, (coalesce(SUM(transaction.points_rewarded),0) - coalesce(SUM(transaction.points_redeemed),0)) AS trx_balance, balance.balance AS balance, balance.network_id AS network_id'))
                        ->whereRaw('transaction.network_id = balance.network_id')
                        ->where('balance.network_id', $networkId)
                        ->where('balance.balance', '<>', 0);
                $res->join('balance', 'balance.user_id', '=', 'transaction.user_id');
                $res->groupBy('balance.user_id');
                $res->havingRaw('balance <> trx_balance');

                foreach ($res->get() as $result) {
                    $userExpiredPtsObj = Transaction::select(DB::raw('sum(points_rewarded) pts_rewarded'))->where('expired', 1)->where('user_id', $result->user_id)->where('network_id', $result->network_id)->first();
                    $userExpiredPts = $userExpiredPtsObj->pts_rewarded;
                    if (!is_numeric($userExpiredPts)) {
                        $userExpiredPts = 0;
                    }
                    $result->trx_balance                                                = $result->trx_balance - $userExpiredPts;
                    $user                                                               = User::find($result->user_id);

                    $results[$result->user_id][$result->network_id]['user_id']          = $result->user_id;
                    $results[$result->user_id][$result->network_id]['trx_balance']      = $result->trx_balance;
                    $results[$result->user_id][$result->network_id]['balance']          = $result->balance;
                    $results[$result->user_id][$result->network_id]['network']          = Network::find($result->network_id)->name;
                    $results[$result->user_id][$result->network_id]['expired_points']   = $userExpiredPts;
//                    $results[$result->user_id][$result->network_id]['member']           = $user->first_name . ' ' . $user->last_name;
                    $results[$result->user_id][$result->network_id]['discrepancy']     = $result->trx_balance - $result->balance;
                }
//                $totalCount += count($res->get());
            }
        }
        $perPage    = 20;
        $numberOfPages  = ceil($totalCount / $perPage);

        if (Input::has('export')) {
            $buffer[] = array('User ID', 'Member', 'Network', 'Balance', 'Points Rewarded - Points Redeemed', 'Discrepancy' );
            foreach ($results as $user_id => $result) {
                foreach ($result as $networkId => $res) {
                        $userId = $res['user_id'];
                        $user   = User::find($userId);

                    if (!$user || $res['discrepancy'] == 0) {
                        continue;
                    }

                        $userName   = $user->first_name . ' ' . $user->last_name;
                        $trx_balance = $res['trx_balance'];
                        $balance = $res['balance'];
                        $network = $res['network'];
                        $discrepancy = $res['discrepancy'];

                        $buffer[] = array(
                                $userId,
                                $userName,
                                $network,
                                $balance,
                                $trx_balance,
                                $discrepancy
                        );
                }
            }

            return ReportHelper::sendDownload($report, $buffer);
        } else {
            return View::make("report.ejs.report_{$report->id}", array(
                                'defaults'  => $defaults,
                                'report'    => $report,
                                'results'   => $results,
                                'numberOfPages'   => $numberOfPages
                            ));
        }
    }

    /**
     * The report will show the discrepancies between the member balance and the points rewarded - points redeemed.
     *
     * @param Report $report
     * @return array
     */
    private function loadReport_38($report)
    {
        if (!I::can('view_blu_internal_reports')) {
            return Redirect::to(url("dashboard"));
        }

        $results        = array();
        $res            = array();
        $totalCount     = 0;
        $defaults = ReportHelper::normalizeDefaults();

        $networkIds = $defaults['network_ids'];

        if (!in_array('-1', $networkIds)) {
              $res = Transaction::select(DB::raw('transaction.user_id as user_id, CONCAT_WS(" ", u.first_name, u.last_name) AS Cust_Name, SUM(td.p_used) as sum_pts_used, SUM(transaction.points_redeemed) as sum_pts_redeemed, (SELECT SUM(transaction.points_redeemed) - SUM(td.p_used)) AS discrepancy, transaction.network_id as network'))
                      ->whereIn('transaction.network_id', $networkIds);
                $res->leftJoin('users as u', 'transaction.user_id', '=', 'u.id');
                $res->leftJoin(DB::raw('(SELECT td.trx_id, SUM(td.points_used) p_used from transaction_deduction as td group by td.trx_id) as td'), function ($join) {
                    $join->on('transaction.id', '=', 'td.trx_id');
                });
                $res->groupBy('transaction.user_id');
                $res->havingRaw('(SUM(td.p_used) <> SUM(transaction.points_redeemed) OR SUM(td.p_used) is NULL OR SUM(transaction.points_redeemed) IS NULL) AND !(SUM(td.p_used) is NULL AND SUM(transaction.points_redeemed) = 0)');
//                        echo "<pre>";
//        print_r($res->toSql());
//        print_r($res->getBindings());
//        exit;
            foreach ($res->get() as $result) {
                $results[$result->user_id]['user_id']       = $result->user_id;
                $results[$result->user_id]['user_name']     = $result->Cust_Name;
                $results[$result->user_id]['network']       = Network::find($result->network)->name;
                $results[$result->user_id]['pts_redeemed']  = $result->sum_pts_redeemed;
                $results[$result->user_id]['pts_used']      = $result->sum_pts_used;
                $results[$result->user_id]['discrepancy']  = $result->discrepancy;
            }
        }

        if (Input::has('export')) {
            $buffer[] = array('User ID', 'Member', 'Network', ' Points Redeemed', 'Points Used', 'Discrepancy' );
            foreach ($results as $user_id => $result) {
                        $userId = $result['user_id'];
                        $user   = User::find($userId);

                if (!$user || $result['discrepancy'] == 0) {
                    continue;
                }

                        $userName   = $result['user_name'];
                        $trx_balance = $result['trx_balance'];
                        $ptsRedeemed= $result['pts_redeemed'];
                        $ptsUsed= $result['pts_used'];
                        $network = $result['network'];
                        $discrepancy = $result['discrepancy'];

                        $buffer[] = array(
                                $userId,
                                $userName,
                                $network,
                                $ptsRedeemed,
                                $ptsUsed,
                                $discrepancy
                        );
            }

            return ReportHelper::sendDownload($report, $buffer);
        } else {
            return View::make("report.ejs.report_{$report->id}", array(
                                'defaults'  => $defaults,
                                'report'    => $report,
                                'results'   => $results
                            ));
        }
    }

    /**
     * Load all data for the 'All Reward Transactions' report
     *
     * @param Report $report
     * @return array
     */
    private function loadReport_39($report)
    {
        if (!I::can('view_basic_reports')) {
            return Redirect::to(url("dashboard"));
        }

        $defaults = ReportHelper::normalizeDefaults();

        $networkId = 1;
        // Currency
        $exchangeRate    = 1;
        $currencyShortCode    = 'USD';
        $currencyId        = 6;
        if ($defaults['currency'] > 0) {
            $currencyId    = $defaults['currency'];
            $currency = Currency::where('id', $currencyId)->first();
            $exchangeRate    = $currency->latestRate();
            $currencyShortCode    = $currency->short_code;
        }

        $tot['exchangeRate']        = $exchangeRate;
        $tot['currencyShortCode']    = $currencyShortCode;
        $tot['currency']            = $currencyId;

        $tot['partner_id'] = Auth::User()->getTopLevelPartner()->id;
        $tot['admin_id'] = Auth::id();

        // Construct Response
        $results = (isset($defaults['params']) && $defaults['params'] == 1) ? [] : null;

        if (Input::has('export')) {
            $buffer[] = array('ID', 'Reference' , 'Member', 'Date & Time', 'Store Name', 'Loyalty', 'Event Name', 'Points Rewarded', 'Amount Spent'.' ('.$tot['currencyShortCode'].')', 'Original Total Amount');

            $query = Transaction::select(
                'transaction.id',
                \DB::raw('DATE_FORMAT(transaction.created_at, "%Y-%m-%d") created_at_date'),
                'transaction.created_at',
                'transaction.total_amount',
                'transaction.original_total_amount',
                'transaction.currency_id',
                'transaction.points_rewarded',
                'transaction.ref_number',
                'store.name as store_name',
                'loyalty.name as loyalty_name',
                'product.name as product_name',
                'currency.short_code as tcurrency_short_code',
                DB::raw('(select number FROM reference as r WHERE r.partner_id = transaction.partner_id and r.user_id = transaction.user_id order by r.id limit 1) as reference_number'),
                DB::raw('(select number FROM reference as r WHERE r.partner_id = partner.parent_id order by r.id limit 1) as reference_parent_number'),
                'users.first_name as user_first_name',
                'users.last_name as user_last_name'
            );

            if (is_array($defaults['member_id']) && current($defaults['member_id']) != 0) {
                $query->whereIn('transaction.user_id', $defaults['member_id']);
            }

            if (is_array($defaults['entities']) && current($defaults['entities']) != -1) {
                $query->whereIn('transaction.partner_id', $defaults['entities']);
            }

            $query->leftJoin('users', 'transaction.user_id', '=', 'users.id')
                  ->leftJoin('store', 'transaction.store_id', '=', 'store.id')
                  ->leftJoin('loyalty', 'transaction.rule_id', '=', 'loyalty.id')
                  ->leftJoin('product', 'transaction.product_id', '=', 'product.id')
                  ->join('partner', 'transaction.partner_id', '=', 'partner.id')
                  ->leftJoin('currency', 'transaction.currency_id', '=', 'currency.id');

            $query->whereBetween('transaction.created_at', [$defaults['start_date'], $defaults['end_date']])
                  ->where('trx_reward', 1);

            if (!empty($defaults['network_ids'])) {
                $query->whereIn('network_id', $defaults['network_ids']);
            }

            $results = $query->orderBy('transaction.id', 'desc')
                             ->get()
                             ->toArray();

            foreach ($results as $u) {
                $name = $u['user_first_name'] . ' ' . $u['user_last_name'];
                if ($name == 'na na' || $name == ' ') {
                    $name = 'N/A';
                }

                $trx_amount = $u['total_amount'] * $tot['exchangeRate'];

                $buffer[] = array(
                    $u['id'],
                    (isset($u['reference_number']) && !empty($u['reference_number'])) ? $u['reference_number'] : $u['reference_parent_number'],
                    $name,
                    ReportHelper::xlsDate($u['created_at_date']),
                    ($u['store_name'] != '') ? $u['store_name'] : 'N/A',
                    ($u['loyalty_name'] != '') ? $u['loyalty_name'] : 'N/A',
                    ($u['product_name'] != '') ? $u['product_name'] : 'N/A',
                    number_format($u['points_rewarded']),
                    number_format($trx_amount),
                    $tot['currencyShortCode'] . " " .number_format($u['original_total_amount'], 2)
                );
            }

            return ReportHelper::sendDownload($report, $buffer);
        } else {
            return ReportHelper::sendView($report, $defaults, $results, $tot);
        }
    }

    /**
     * Load all data for the 'All Redeem Transactions' report
     *
     * @param Report $report
     * @return string
     */
    private function loadReport_40($report)
    {
        if (!I::can('view_redemption_partner_rewarded_points_reports')) {
            return Redirect::to(url("dashboard"));
        }

        $repository = new RepositoryReport40;

        $exchangeRate = 1;
        $currencyShortCode = 'USD';
        $currencyId = 6;

        $defaults = ReportHelper::normalizeDefaults();

        $memberId = null;
        $partnerIds = Auth::user()->managedPartnerIDs() ?: [];

        if ($defaults['member_id'][0] ?? null)
        {
            $memberId = $defaults['member_id'][0];
        } else
        {
            $partnerIds = $defaults['entities'];
        }

        if ($defaults['currency'] > 0)
        {
            $currencyId = $defaults['currency'];
            $currency = Currency::where('id', $currencyId)->first();
            $exchangeRate = $currency->latestRate();
            $currencyShortCode = $currency->short_code;
        }

        $tot = [];
        $tot['exchangeRate'] = $exchangeRate;
        $tot['currencyShortCode'] = $currencyShortCode;
        $tot['currency'] = $currencyId;
        $results = (isset($defaults['params']) && $defaults['params'] == 1) ? [] : null;
        $networkId = $defaults['network_currency'] != 'not' ? $defaults['network_currency'] : 1;

        if (Input::has('export')) {
            $buffer[] = array('ID', 'Reference' , 'Member', 'Mobile', 'Date & Time', 'Category', 'Item', 'Quantity', 'Pts Redeemed', 'Delivery Charges', 'Cash Paid');

            $limit = 1000;
            $offset = 0;

            $data = $repository->startDate($defaults ['start_date'])
                               ->endDate($defaults['end_date'])
                               ->memberId($memberId)
                               ->networkId($networkId)
                               ->partnerId(array_intersect($partnerIds, Auth::user()->managedPartnerIds() ?: []))
                               ->limit($limit)
                               ->offset($offset)
                               ->getData();

            foreach ($data as &$row) {
                $row->refID = $row->refID ?? 'N/A';
                $itemName = '';

                $row->name = $row->first_name . ' ' . $row->last_name;
                if ($row->name == 'na na') {
                    $row->name = 'N/A';
                }

                if ($row->category == 'Items' || ($row->category == 'Reversal' && !empty($row->item))) {
                    $product = Product::find($row->item);
                    $transactionItem = TransactionItem::where('product_id', $row->item)
                                                      ->where('transaction_id', $row->trx_id)
                                                      ->first();

                    if (!$product) {
                        $itemName = 'N/A';
                    } else {
                        $itemName    = $product->name;
                    }
                    $quantity = $row->quantity;
//                $ptsRedeemed = $row->points_redeemed + $row->delivery_cost;
//                $ptsRedeemed = $row->trx_pts_redeemed;
                    $ptsRedeemed = $row->paid_in_points;

                    if ($transactionItem && $transactionItem->delivery_cost) {
                        $row->delivery_cost = $transactionItem->delivery_cost;
                    } else {
                        $row->delivery_cost = 0;
                    }

                    $deliveryAmount = \PointsHelper::pointsToAmount($row->delivery_cost, $tot['currencyShortCode'], $row->network_id);
//                    $deliveryAmount = \PointsHelper::pointsToAmount($product->delivery_cost, $tot['currencyShortCode'], $row->network_id);
                    if ($tot['currency'] == $row->ti_currency_id) {
                        $amountSpentArr['amount'] = $row->ti_original_total_amount;
                    } else {
                        $amountSpentArr = \CurrencyHelper::convertFromUSD($row->amount_spent, $tot['currencyShortCode']);
                    }

                    $amountSpent = $amountSpentArr['amount'] + $deliveryAmount;
                    $row->cash_payment = $row->paid_in_cash;
                } else {
                    $trx = Transaction::find($row->ref_number);
                    if ($trx) {
                        $itemNameArr = explode('|', $trx->ref_number);
                        if (!empty($itemNameArr)) {
                            $itemName   = $itemNameArr['0'];
                        } else {
                            $itemName   = $trx->ref_number;
                        }
                    }

                    if ($row->category == 'Flight') {
                        $flightDetails = json_decode($row->notes, true);
                        $inDepart = 'N/A';
                        $inArrive = 'N/A';
                        $outDepart = 'N/A';
                        $outArrive = 'N/A';
                        if (!empty($flightDetails['in_depart_station_name'])) {
                            $inDepart = $flightDetails['in_depart_station_name'];
                        }
                        if (!empty($flightDetails['in_arrive_station_name'])) {
                            $inArrive = $flightDetails['in_arrive_station_name'];
                        }
                        if (!empty($flightDetails['out_depart_station_name'])) {
                            $outDepart = $flightDetails['out_depart_station_name'];
                        }
                        if (!empty($flightDetails['out_arrive_station_name'])) {
                            $outArrive = $flightDetails['out_arrive_station_name'];
                        }
                        $itemName = $inDepart . ' - ' . $inArrive . ' / ' . $outDepart . ' - ' . $outArrive;
                    } elseif ($row->category == 'Car') {
                        $carDetails = json_decode($row->notes, true);
                        if (!empty($carDetails['vehicle'])) {
                            $itemName = $carDetails['vehicle'];
                        }
                    } elseif ($row->category == 'Hotel') {
                        $hotelDetails = json_decode($row->notes, true);
                        if (!empty($hotelDetails['hotelName'])) {
                            $itemName = $hotelDetails['hotelName'];
                        }
                    } elseif ($row->category == 'Points' || $row->category == 'Amount') {
                        $itemNameArr = explode('|', $row->ref_number);
                        if (!empty($itemNameArr)) {
                            $itemName   = $itemNameArr['0'];
                            $category   = 'Transfer';
                        } else {
                            $itemName   = $row->ref_number;
                        }
                    }

                    if ($row->reversed == 1 && $row->trx_reward == 1) {
                        $category = 'Reversed Reward';
                    }

                    if (strpos($row->ref_number, 'converted')) {
                        $category = 'Cashback';
                        $itemName = 'Converted Points';
                    }
                    $quantity = $row->trx_quantity;
                    $ptsRedeemed = $row->trx_pts_redeemed;
                    $deliveryAmount = \PointsHelper::pointsToAmount($row->delivery_cost, $tot['currencyShortCode'], 1);
                    if ($tot['currency'] == $row->t_currency_id) {
                        $amountSpentArr['amount'] = $row->t_original_total_amount;
                    } else {
                        $amountSpentArr = CurrencyHelper::convertFromUSD($row->trx_total_amount, $tot['currencyShortCode']);
                    }

                    $amountSpent = $amountSpentArr['amount'] + $deliveryAmount;
                }
                $row->itemName = $itemName;
                $row->quantity = $quantity;
                $row->ptsRedeemed = $ptsRedeemed;
                $row->amountSpent = number_format($amountSpent, 2);
            }

            foreach ($data as $u) {
                $buffer[] = array(
                    $u->trx_id,
                    $u->refID,
                    $u->name,
                    $u->normalized_mobile,
                    ReportHelper::xlsDate($u->date),
                    $u->category,
                    $u->itemName,
                    $u->quantity,
                    $u->ptsRedeemed,
                    $u->delivery_cost,
                    $u->cash_payment,
                );
            }

            return ReportHelper::sendDownload($report, $buffer);
        } else {
            return ReportHelper::sendView($report, $defaults, $results, $tot);
        }
    }

    /**
     * Load all data for the 'Shipment Status Report ' report
     *
     * @param Report $report
     * @return array
     */
    private function loadReport_41($report)
    {
        if (!I::can('view_redemption_partner_rewarded_points_reports')) {
            return Redirect::to(url("dashboard"));
        }

        $results    = array();
        $defaults = ReportHelper::normalizeDefaults();
        if (!in_array('-1', $defaults['entities'])) {
            $defaults['member_id'] = 0;
        }
        // Fetch report data
            $results = RedemptionOrder::select(DB::raw('redemption_order.trx_id as trx_id, redemption_order.user_id as user_id, redemption_order.order_date as order_date, redemption_order.type as type, redemption_order.product_name as product_name, redemption_order.shipping_company as shipping_company, redemption_order.order_status as order_status, redemption_order.created_at as created_at, t.partner_id as partner_id'));

            $results->where('redemption_order.created_at', '>=', $defaults['start_date']);
            $results->where('redemption_order.created_at', '<=', $defaults['end_date']);
            $results->where('redemption_order.type', 'Item');
//            $results->whereIn('t.partner_id', $defaults['entities']);
        if ($defaults['member_id'] != 0) {
            $results->whereIn('t.user_id', $defaults['member_id']);
        } else {
            $results->whereIn('t.partner_id', $defaults['entities']);
        }

            $results->leftJoin('transaction as t', 'redemption_order.trx_id', '=', 't.id');
            $results->orderBy('redemption_order.created_at', 'DESC');
//        echo "<pre>";
//        print_r($results->toSql());
//        print_r($results->getBindings());
//        exit;
        $tot                = array();

        // Construct Response

        if (Input::has('export')) {
            $buffer[] = array('ID', 'Reference' , 'Member', 'Date & Time', 'Item', 'Shipping Company', 'Order Status');

            foreach ($results->get() as $u) {
                $refID = 'N/A';
                $ref = DB::table('reference')->where('user_id', $u->user_id)->where('partner_id', Auth::User()->getTopLevelPartner()->id)->first();
                if (!empty($ref)) {
                    $refID = $ref->number;
                }

                $admin_partner = Auth::User()->getTopLevelPartner();
                $has_middleware = false;
                $user_info = array();
                if ($admin_partner->has_middleware == '1') {
                    $user_ids_used = array();
                    $has_middleware = true;
                    $user_ids_used[] = $u->user_id;
                    $term = "";
                    $url = $admin_partner->link_middleware_api . '?method=searchSensitive&api_key='.$admin_partner->middleware_api_key;
                    $json_ids = json_encode($user_ids_used);
                    $url .= "&user_ids=" . $json_ids . "&term='" . $term . "'";
                    $curl = curl_init();
                    // Set some options - we are passing in a useragent too here
                    curl_setopt_array($curl, array(
                        CURLOPT_RETURNTRANSFER => 1,
                        CURLOPT_URL => $url,
                        CURLOPT_USERAGENT => 'Testing Sensitive Info'
                    ));
                    // Send the request & save response to $resp
                    $resp = curl_exec($curl);
                    $resp_curl = json_decode($resp);

                    $Users_info = $resp_curl;

                    foreach ($Users_info as $member) {
                        if (in_array($member->blu_id, $user_ids_used)) {
                            $user_info['first_name'] = $member->first_name;
                            $user_info['last_name'] = $member->last_name;
                            $memberName = $user_info['first_name'] . ' ' . $user_info['last_name'];
                        }
                    }
                } else {
                    $user = User::find($u->user_id);
                    $memberName = $user->name;
                    if ($memberName == 'na na' || $memberName == '') {
                        $memberName = 'N/A';
                    }
                }

                if (!empty($u->product_name)) {
                    $itemName = $u->product_name;
                } else {
                    $itemName = 'N/A';
                }

                if (!empty($u->shipping_company)) {
                    $shippingCompany =  $u->shipping_company;
                } else {
                    $shippingCompany =  'N/A';
                }

                if (!empty($u->order_status)) {
                    $orderStatus = $u->order_status;
                } else {
                    $orderStatus = 'N/A';
                }

                $buffer[] = array(
                    $u->trx_id,
                    $refID,
                    $memberName,
                    ReportHelper::xlsDate($u->created_at),
                    $itemName,
                    $shippingCompany,
                    $orderStatus
                );
            }

            return ReportHelper::sendDownload($report, $buffer);
        } else {
            return ReportHelper::sendView($report, $defaults, $results, $tot);
        }
    }

    /**
     * Load all data for the 'Enrolement Report ' report
     *
     * @param Report $report
     * @return array
     */
    private function loadReport_42($report)
    {
        if (!I::can('view_risk_reports')) {
            return Redirect::to(url("dashboard"));
        }

        $defaults = ReportHelper::normalizeDefaults();

        if (Input::has('member_id') && Input::get('member_id') != 0)
        {
            $defaults['member_id'] = [Input::get('member_id')];
        }

        //$defaults['member_id'] = [143828];

        $totalLimit = Input::has('export') ? 5000 : ReportHelper::PAGINATION_COUNT;
        $adminPartner = Auth::User()->getTopLevelPartner();
        $managedPartnerIds = Auth::user()->managedPartnerIDs();


        //@todo move all database access staff to Repository classes
        //don't drop inline queries, this very optimised query has been tested in different ways. Don't use eloquent eager loading with many in() user ids in it.
        $users = DB::table('users as u')
                ->select(['u.first_name', 'u.last_name', 'u.created_at as registration_date', 'u.id as user_id', 'u.normalized_mobile as normalized_mobile', 'pos.name as pos_name', 'ref.number as ref_number' ])
                //->selectRaw('min(t.created_at) as first_transaction_date')
                ->selectRaw('(SELECT detail from audit_trail_users where audit_trail_users.user_id = u.id order by created_at limit 1) as details')
                ->selectRaw('(SELECT min(created_at) from transaction where transaction.user_id = u.id group by user_id) as first_transaction_date')
                ->selectRaw('(SELECT number from reference as r where r.user_id = u.id and partner_id = ' . $adminPartner->id . ' limit 1) as ref_number')
                ->leftJoin('pos', 'u.registration_source_pos', '=', 'pos.id')
                ->leftJoin('reference as ref', 'ref.user_id', '=', 'u.id')
                ->where('u.draft', false)
                ->whereIn('u.id', function($query) use ($managedPartnerIds){
                    $query->select('user_id')
                        ->from('partner_user')->whereIn('partner_id', $managedPartnerIds);
                })
                ->whereBetween('u.created_at', [$defaults['start_date'], $defaults['end_date']]);

        if ((!is_array($defaults['member_id']) && $defaults['member_id'] != 0) || (is_array($defaults['member_id']) && current($defaults['member_id']) != 0))
        {
            $users->whereIn('u.id', $defaults['member_id']);
        }

        if (Input::has('entity') || Input::has('parent_ids')) {
            $stores = explode(',', Input::get('entity') . ',' . Input::has('parent_ids'));
            $users->whereIn('pos.store_id', $stores);
        }

        $users = $users->paginate($totalLimit);

        //get user's first and last names by middleware
        if ($adminPartner->has_middleware == '1') {
            $url = $adminPartner->link_middleware_api . '?method=sensitiveInfo&api_key=' . $adminPartner->middleware_api_key;
            $json_ids = json_encode($users->pluck('user_id')->toArray());
            $post_fields = 'user_ids=' . $json_ids ;
            $curl = curl_init();
            // Set some options - we are passing in a useragent too here
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => $url,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => $post_fields,
                CURLOPT_USERAGENT => 'Testing Sensitive Info'
            ));
            // Send the request
            $middlewareUsers = curl_exec($curl);
            //update result set
            foreach ($users as $user){
                foreach ($middlewareUsers as $middlewareUser) {
                    if ($user->user_id == $middlewareUser->blu_id) {
                        $user->first_name = $middlewareUser->first_name;
                        $user->last_name = $middlewareUser->last_name;
                    }
                }
            }
        }

        $users->transform(function($user){
            //calculate first_login, first_login_date and first_login_channel
            $user->first_login = 'N/A';
            $user->first_login_date = 'N/A';
            $user->first_login_channel = 'N/A';

            if ($details = $user->details){
                $detailsParts = explode('User Logged in at', $details);
                $loginTimeAndChannelParts = $detailsParts['1'] ? explode('From', $detailsParts['1']) : [];

                $user->first_login = (in_array('(First Login) ', $detailsParts) || in_array('(First Login)', $detailsParts)) ? 'Yes' : 'No';

                if ($loginTimeAndChannelParts[0] ?? null)
                    $user->first_login_date = $loginTimeAndChannelParts[0];

                if ($loginTimeAndChannelParts[1] ?? null)
                {
                    $firstLoginChannelParts = explode( '(', $loginTimeAndChannelParts[1]);
                    if ($firstLoginChannelParts[0] ?? null)
                        $user->first_login_channel = $firstLoginChannelParts[0];
                }
            }

            //concatenate user's names
            $memberName = $user->first_name . ' ' . $user->last_name;
            if ($memberName == 'na na' || $memberName == '')
                $memberName = 'N/A';

            $user->member_name = $memberName;
            return $user;
        });

        // Construct Response
        if (Input::has('export')) {
            $buffer[] = array('Registration Date', 'Registration Source', 'Reference' , 'Member', 'Mobile', 'First Time Login', 'First Time Login Date', 'Channel', 'First Transaction Posted');
            foreach ($users as $user) {
                $buffer[] = array(
                    $user->registration_date,
                    $user->pos_name,
                    $user->ref_number,
                    $user->member_name,
                    $user->normalized_mobile,
                    $user->first_login,
                    $user->first_login_date,
                    $user->first_login_channel,
                    $user->first_transaction_date,
                );
            }

            return ReportHelper::sendDownload($report, $buffer);
        } else {

            return View::make('report.ejs.report_42', [
                'defaults'    => $defaults,
                'report'      => $report,
                'results'     => $users,
            ]);
        }
    }

    /**
     * Period comparison - points and amounts
     *
     * @param Report $report
     * @return array
     */
    private function loadReport_43($report)
    {
        if (!I::can('view_full_reports')) {
            return Redirect::to(url("dashboard"));
        }

        $defaults = ReportHelper::normalizeDefaults();
        $currencyId = 6;
        if ($defaults['network_currency'] != 'not') {
                $networkId  = $defaults['network_currency'];
        } else {
                $networkId  = 1;
        }
        $exchangeRate   = 1;
        $currencyShortCode  = 'USD';
        $currencyId         = 6;
        if ($defaults['currency'] > 0) {
                $currencyId = $defaults['currency'];
                $currency = Currency::where('id', $currencyId)->first();
                $exchangeRate   = $currency->latestRate();
                $currencyShortCode  = $currency->short_code;
        }
        $tot = array();
        $tot['exchangeRate'] = $exchangeRate;
        $tot['currencyShortCode'] = $currencyShortCode;
        $tot['currency'] = $currencyId;
        // Fetch report data

        $results = array();

        if ($defaults['monyeardaterange'] != '') {
            if (strcmp($defaults['monyeardaterange'], 'monthly') == 0) {
                if ($defaults['trxtype'] == 'reward') {
                    $results = Transaction::select(DB::raw('DATE_FORMAT(created_at, "%Y-%m") creation, sum(points_rewarded) p_sum, count(*) trx_number, sum(total_amount) as total_amount'))
                    ->where('created_at', '>=', $defaults['start_date'])
                    ->where('created_at', '<=', $defaults['end_date'])
                    ->whereIn('partner_id', $defaults['entities'])
                        ->where('network_id', $defaults['network_currency'])
                        ->where('trx_reward', 1)
                    ->orderBy('creation', 'DESC')->groupBy(DB::raw('DATE_FORMAT(created_at, "%Y-%m")'));
                } elseif ($defaults['trxtype'] == 'redeem') {
                    $results = TransactionDeduction::select(DB::raw('DATE_FORMAT(created_at, "%Y-%m") creation, SUM(points_used) p_sum, count(DISTINCT trx_ref_id) trx_number'))
                    ->where('created_at', '>=', $defaults['start_date'])
                    ->where('created_at', '<=', $defaults['end_date'])
                    ->whereIn('trx_id', function ($query) use ($defaults) {
                                $query->select('id')
                                ->from(with(new Transaction)->getTable())
                                ->where('trx_reward', 1)
                                ->whereIn('partner_id', $defaults['entities'])
                                ->where('network_id', $defaults['network_currency']);
                    })
                    ->orderBy('creation', 'DESC')->groupBy(DB::raw('DATE_FORMAT(created_at, "%Y-%m")'));
                }
            }
            if (strcmp($defaults['monyeardaterange'], 'yearly') == 0) {
                if ($defaults['trxtype'] == 'reward') {
                    $results = Transaction::select(DB::raw('DATE_FORMAT(created_at, "%Y") creation, sum(points_rewarded) p_sum, count(*) trx_number, sum(total_amount) as total_amount'))
                    ->where('created_at', '>=', $defaults['start_date'])
                    ->where('created_at', '<=', $defaults['end_date'])
                    ->whereIn('partner_id', $defaults['entities'])
                        ->where('network_id', $defaults['network_currency'])
                        ->where('trx_reward', 1)
                    ->orderBy('creation', 'DESC')->groupBy(DB::raw('DATE_FORMAT(created_at, "%Y-%m")'));
                } elseif ($defaults['trxtype'] == 'redeem') {
                     $results = TransactionDeduction::select(DB::raw('DATE_FORMAT(created_at, "%Y") creation, SUM(points_used) p_sum, count(DISTINCT trx_ref_id) trx_number'))
                    ->where('created_at', '>=', $defaults['start_date'])
                    ->where('created_at', '<=', $defaults['end_date'])
                    ->whereIn('trx_id', function ($query) use ($defaults) {
                                $query->select('id')
                                ->from(with(new Transaction)->getTable())
                                ->where('trx_reward', 1)
                                ->whereIn('partner_id', $defaults['entities'])
                                ->where('network_id', $defaults['network_currency']);
                    })
                    ->orderBy('creation', 'DESC')->groupBy(DB::raw('DATE_FORMAT(created_at, "%Y-%m")'));
                }
            }
        } else {
        }
        if (Input::has('export')) {
            $buffer[] = array('Period', 'Points', 'Transaction Number', 'Amount('.$tot['currencyShortCode'].')');

            foreach ($results->get() as $t) {
                $monthName  = '';
                if (!empty($t->crmonth)) {
                    $monthNum  = $t->crmonth;
                    $dateObj   = DateTime::createFromFormat('!m', $monthNum);
                    $monthName = $dateObj->format('M') . ' - ';
                }

                if ($defaults['trxtype'] == 'reward') {
                    $totAmountArray = CurrencyHelper::convertFromUSD($t->total_amount, $tot['currencyShortCode']);
                    $totAmount = $totAmountArray['amount'];
                } else {
                    $totAmount =  PointsHelper::pointsToAmount($t->p_sum, $tot['currencyShortCode'], $defaults['network_currency']);
                }
                if ($defaults['monyeardaterange'] == 'monthly') {
                    $creation = date('M-Y', strtotime($t->creation));
                } else {
                     $creation = $t->creation;
                }

                $buffer[] = array(
                    $creation,
                    $t->p_sum,
                    $t->trx_number,
                    number_format($totAmount, 2)
                );
            }

            return ReportHelper::sendDownload($report, $buffer);
        } else {
            return ReportHelper::sendView($report, $defaults, $results, $tot);
        }
    }

    private function loadReport_44($report)
    {
        if (!I::can('view_basic_reports')) {
            return Redirect::to(url("dashboard"));
        }

        $defaults = ReportHelper::normalizeDefaults();

        $currencyId        = 17;
        $exchangeRate      = 1;
        $currencyShortCode = 'USD';
        if ($defaults['currency'] > 0) {
            $currencyId        = $defaults['currency'];
            $currency          = Currency::find($currencyId);
            $exchangeRate      = $currency->latestRate();
            $currencyShortCode = $currency->short_code;
        }
        $tot['exchangeRate']      = $exchangeRate;
        $tot['currencyShortCode'] = $currencyShortCode;
        $tot['currency']          = $currencyId;
        $tot['admin_id'] = Auth::id();


        $results = (isset($defaults['params']) && $defaults['params'] == 1) ? [] : null;

        if (Input::has('export')) {

            $query = Transaction::whereIn('transaction.store_id', $defaults['entities'])
                                ->whereBetween('transaction.created_at', [$defaults['start_date'], $defaults['end_date']])
                                ->whereIn('transaction.network_id', $defaults['network_ids']);

            $data = $query->leftJoin('partner', 'transaction.partner_id', '=', 'partner.id')
                          ->leftJoin('users', 'transaction.user_id', '=', 'users.id')
                          ->leftJoin('reference', 'transaction.user_id', '=', 'reference.user_id')
                          ->select(
                              'transaction.id',
                              \DB::raw('DATE(transaction.created_at) created_at_date'),
                              'transaction.points_rewarded',
                              'transaction.amt_reward',
                              'transaction.total_amount',
                              'transaction.original_total_amount',
                              'transaction.currency_id',
                              'users.first_name as user_first_name',
                              'users.last_name as user_last_name',
                              'reference.number as reference_number'
                          )
                          ->orderBy('transaction.id', 'desc')
                          ->get()
                          ->each(function ($transaction) {
                              $transaction->points_rewarded = number_format($transaction->points_rewarded, 0);
                              $transaction->user_name = $transaction->user_first_name . ' ' . $transaction->user_last_name;
                              $transaction->reference_number = $transaction->reference_number ?? 'N/A';
                          })
                          ->toArray();

            foreach ($data as &$item) {
                $item['total_amount'] = round($item['total_amount'] * $exchangeRate, 2);
            }

            $buffer[] = array("ID", "Date", "Reference Number", "Name", "Points Rewarded", "Value of Pts Rewarded", "Trx Amount($currencyShortCode)");

            foreach ($data as $u) {
                $buffer[] = array(
                    $u['id'],
                    ReportHelper::xlsDate($u['created_at_date']),
                    $u['reference_number'],
                    $u['user_name'],
                    $u['points_rewarded'],
                    $u['amt_reward'],
                    $u['total_amount']
                );
            }

            return ReportHelper::sendDownload($report, $buffer);
        } else {
            return ReportHelper::sendView($report, $defaults, $results, $tot);
        }
    }

    private function loadReport_45($report)
    {
        if (!I::can('view_basic_reports')) {
            return Redirect::to(url("dashboard"));
        }

        $defaults = ReportHelper::normalizeDefaults();

        $currencyId        = 6;
        $exchangeRate      = 1;
        $currencyShortCode = 'USD';
        if ($defaults['currency'] > 0) {
            $currencyId        = $defaults['currency'];
            $currency          = Currency::find($currencyId);
            $exchangeRate      = $currency->latestRate();
            $currencyShortCode = $currency->short_code;
        }
        $tot['exchangeRate']      = $exchangeRate;
        $tot['currencyShortCode'] = $currencyShortCode;
        $tot['currency']          = $currencyId;

        $results = (isset($defaults['params']) && $defaults['params'] == 1) ? [] : null;

        if (Input::has('export')) {

            $query = Transaction::whereIn('transaction.store_id', $defaults['entities'])
                                ->whereBetween('transaction.created_at', [$defaults['start_date'], $defaults['end_date']]);
//                            ->whereIn('transaction.network_id', $defaults['network_ids']);

            $query->leftJoin('partner', 'transaction.partner_id', '=', 'partner.id')
                  ->leftJoin('users', 'transaction.user_id', '=', 'users.id')
                  ->leftJoin('reference', 'transaction.user_id', '=', 'reference.user_id')
                  ->select(
                      'transaction.id',
                      \DB::raw('DATE(transaction.created_at) created_at_date'),
                      'transaction.points_redeemed',
                      'transaction.amt_redeem',
                      'transaction.total_amount',
                      'transaction.original_total_amount',
                      'transaction.currency_id',
                      'users.first_name as user_first_name',
                      'users.last_name as user_last_name',
                      'reference.number as reference_number',
                      'transaction.network_id'
                  );

            if (!empty($defaults['network_ids'])) {
                $query->whereIn('network_id', $defaults['network_ids']);
            }

//            $totals = $query->count('transaction.id');

            $query->orderBy('transaction.id', 'desc');



            $buffer[] = array("ID", "Date & Time", "Store", "Points Rewarded", "Points Redeemed", "Value of Pts Rewarded", "Value of Pts Redeemed", "Trx Amount($currencyShortCode)");

            foreach ($query->get() as $u) {
                $currency = Currency::where('id', $defaults['currency'])->first();
                $amtRedeem  = 0;
                $amtReward  = 0;

                $network                = Network::find($u->network_id);
                $netCurrency            = Currency::where('id', $network->currency_id)->first();
                $exchangeRate           = $netCurrency->latestRate();
                $amtRewardInUSD         = $u->amt_reward / $exchangeRate;
                $amtRewardInDefCurrency = $amtRewardInUSD * $tot['exchangeRate'];
                $amtRedeemdInUSD        = $u->amt_redeem / $exchangeRate;
                $amtRedeemInDefCurrency = $amtRedeemdInUSD * $tot['exchangeRate'];
                if ($network->currency_id == $tot['currency']) {
                    $amtReward = number_format($u->amt_reward, 2);
                    $amtRedeem = number_format($u->amt_redeem, 2);
                    $totalAmount = number_format($u->original_total_amount, 2);
                } else {
                    $amtReward = number_format($amtRewardInDefCurrency, 2);
                    $amtRedeem = number_format($amtRedeemInDefCurrency, 2);
                    $totalAmount = number_format($u->total_amount * $tot['exchangeRate'], 2);
                }
                $buffer[] = array(
                    $u->id,
                    ReportHelper::xlsDate($u->created_at),
                    ReportHelper::defaultEmpty($u->store, 'name'),
                    number_format($u->points_rewarded),
                    number_format($u->points_redeemed),
                    $amtReward,
                    $amtRedeem,
                    $totalAmount
                );
            }
            return ReportHelper::sendDownload($report, $buffer);
        } else {
            return ReportHelper::sendView($report, $defaults, $results, $tot);
        }
    }

    private function loadReport_46($report)
    {
        if (!I::can('view_basic_reports')) {
            return Redirect::to(url("dashboard"));
        }

        $defaults = ReportHelper::normalizeDefaults();

        $currencyId        = 6;
        $exchangeRate      = 1;
        $currencyShortCode = 'USD';
        if ($defaults['currency'] > 0) {
            $currencyId        = $defaults['currency'];
            $currency          = Currency::find($currencyId);
            $exchangeRate      = $currency->latestRate();
            $currencyShortCode = $currency->short_code;
        }
        $tot['exchangeRate']      = $exchangeRate;
        $tot['currencyShortCode'] = $currencyShortCode;
        $tot['currency']          = $currencyId;

        $results = (isset($defaults['params']) && $defaults['params'] == 1) ? [] : null;

        if (Input::has('export')) {
            $query = Transaction::whereIn('transaction.store_id', $defaults['entities'])
                                ->whereBetween('transaction.created_at', [$defaults['start_date'], $defaults['end_date']]);
//                            ->whereIn('transaction.network_id', $defaults['network_ids']);

            $query->leftJoin('partner', 'transaction.partner_id', '=', 'partner.id')
                  ->leftJoin('product', 'transaction.product_id', '=', 'product.id')
                  ->leftJoin('category', 'product.category_id', '=', 'category.id')
                  ->select(
                      'category.id as category_id',
                      \DB::raw('SUM(transaction.points_rewarded) as points_rewarded'),
                      \DB::raw('SUM(transaction.amt_reward) as amt_reward'),
                      \DB::raw('SUM(transaction.total_amount) as total_amount'),
                      \DB::raw('SUM(transaction.original_total_amount) as original_total_amount'),
                      \DB::raw('COUNT(transaction.id) as transactions_total'),
                      'transaction.currency_id',
                      'category.name as category_name',
                      'transaction.network_id'
                  );

            if (!empty($defaults['network_ids'])) {
                $query->whereIn('transaction.network_id', $defaults['network_ids']);
            }

            $results = $query->get();

            $totals = count($results);

            $buffer[] = array("ID", "Date & Time", "Store", "Points Rewarded", "Points Redeemed", "Value of Pts Rewarded", "Value of Pts Redeemed", "Trx Amount($currencyShortCode)");

            foreach ($results as $u) {
                $currency = Currency::where('id', $defaults['currency'])->first();
                $amtRedeem  = 0;
                $amtReward  = 0;

                $network                = Network::find($u->network_id);
                $netCurrency            = Currency::where('id', $network->currency_id)->first();
                $exchangeRate           = $netCurrency->latestRate();
                $amtRewardInUSD         = $u->amt_reward / $exchangeRate;
                $amtRewardInDefCurrency = $amtRewardInUSD * $tot['exchangeRate'];
                $amtRedeemdInUSD        = $u->amt_redeem / $exchangeRate;
                $amtRedeemInDefCurrency = $amtRedeemdInUSD * $tot['exchangeRate'];
                if ($network->currency_id == $tot['currency']) {
                    $amtReward = number_format($u->amt_reward, 2);
                    $amtRedeem = number_format($u->amt_redeem, 2);
                    $totalAmount = number_format($u->original_total_amount, 2);
                } else {
                    $amtReward = number_format($amtRewardInDefCurrency, 2);
                    $amtRedeem = number_format($amtRedeemInDefCurrency, 2);
                    $totalAmount = number_format($u->total_amount * $tot['exchangeRate'], 2);
                }
                $buffer[] = array(
                    $u->id,
                    ReportHelper::xlsDate($u->created_at),
                    ReportHelper::defaultEmpty($u->store, 'name'),
                    number_format($u->points_rewarded),
                    number_format($u->points_redeemed),
                    $amtReward,
                    $amtRedeem,
                    $totalAmount
                );
            }
            return ReportHelper::sendDownload($report, $buffer);
        } else {
            return ReportHelper::sendView($report, $defaults, $results, $tot);
        }
    }

    private function loadReport_47($report)
    {
        if (!I::can('view_basic_reports')) {
            return Redirect::to(url("dashboard"));
        }

        $defaults = ReportHelper::normalizeDefaults();

        $currencyId        = 6;
        $exchangeRate      = 1;
        $currencyShortCode = 'USD';
        if ($defaults['currency'] > 0) {
            $currencyId        = $defaults['currency'];
            $currency          = Currency::find($currencyId);
            $exchangeRate      = $currency->latestRate();
            $currencyShortCode = $currency->short_code;
        }
        $tot['exchangeRate']      = $exchangeRate;
        $tot['currencyShortCode'] = $currencyShortCode;
        $tot['currency']          = $currencyId;

        $results = (isset($defaults['params']) && $defaults['params'] == 1) ? [] : null;

        if (Input::has('export')) {
            $query = Transaction::whereIn('transaction.store_id', $defaults['entities'])
                                ->whereBetween('transaction.created_at', [$defaults['start_date'], $defaults['end_date']])
                                ->whereIn('transaction.network_id', $defaults['network_ids']);

            $data = $query->leftJoin('partner', 'transaction.partner_id', '=', 'partner.id')
                          ->leftJoin('product', 'transaction.product_id', '=', 'product.id')
                          ->leftJoin('category', 'product.category_id', '=', 'category.id')
                          ->select(
                              'category.id as category_id',
                              \DB::raw('SUM(transaction.points_redeemed) as points_redeemed'),
                              \DB::raw('SUM(transaction.amt_redeem) as amt_redeem'),
                              \DB::raw('SUM(transaction.total_amount) as total_amount'),
                              \DB::raw('SUM(transaction.original_total_amount) as original_total_amount'),
                              \DB::raw('COUNT(transaction.id) as transactions_total'),
                              'transaction.currency_id',
                              'category.name as category_name'
                          )
                          ->groupBy('category_name')
                          ->get()
                          ->each(function ($group) {
                              $group->points_redeemed = number_format($group->points_redeemed, 0);
                              $group->amt_redeem = number_format($group->amt_redeem, 2);
                              $group->category_id = $group->category_id ?? 'N/A';
                              $group->category_name = $group->category_name ?? 'N/A';
                          })
                          ->toArray();

            $buffer[] = array("Category", "Total Points Redeemed", "Total value of PTS Redeemed", "Total Transactions");

            foreach ($data as $u) {
                $buffer[] = array(
                    $u['category_name'],
                    $u['points_redeemed'],
                    $u['amt_redeem'],
                    $u['transactions_total']
                );
            }

            return ReportHelper::sendDownload($report, $buffer);
        } else {
            return ReportHelper::sendView($report, $defaults, $results, $tot);
        }
    }

    private function loadReport_48($report)
    {
        if (!I::can('view_basic_reports')) {
            return Redirect::to(url("dashboard"));
        }

        $defaults = ReportHelper::normalizeDefaults();

        $currencyId        = 6;
        $exchangeRate      = 1;
        $currencyShortCode = 'USD';
        if ($defaults['currency'] > 0) {
            $currencyId        = $defaults['currency'];
            $currency          = Currency::find($currencyId);
            $exchangeRate      = $currency->latestRate();
            $currencyShortCode = $currency->short_code;
        }
        $tot['exchangeRate']      = $exchangeRate;
        $tot['currencyShortCode'] = $currencyShortCode;
        $tot['currency']          = $currencyId;

        $results = (isset($defaults['params']) && $defaults['params'] == 1) ? [] : null;

        if (Input::has('export')) {
            $query = Transaction::whereIn('transaction.store_id', $defaults['entities'])
                                ->whereBetween('transaction.created_at', [$defaults['start_date'], $defaults['end_date']])
                                ->whereIn('transaction.network_id', $defaults['network_ids'])
                                ->where('points_rewarded', '<', 0);

            $data = $query->leftJoin('partner', 'transaction.partner_id', '=', 'partner.id')
                          ->leftJoin('users', 'transaction.user_id', '=', 'users.id')
                          ->leftJoin('reference', 'transaction.user_id', '=', 'reference.user_id')
                          ->select(
                              \DB::raw('DATE(transaction.created_at) created_at_date'),
                              \DB::raw('SUM(transaction.points_rewarded) as points_rewarded'),
                              \DB::raw('SUM(transaction.amt_reward) as amt_reward'),
                              'transaction.currency_id',
                              'users.first_name as user_first_name',
                              'users.last_name as user_last_name',
                              'reference.number as reference_number',
                              \DB::raw('COUNT(transaction.id) as transactions_total')
                          )
                          ->groupBy('created_at_date')
                          ->groupBy('users.id')
                          ->get()
                          ->each(function ($transaction) {
                              $transaction->points_rewarded = number_format(abs($transaction->points_rewarded), 0);
                              $transaction->amt_reward = number_format(abs($transaction->amt_reward), 2);
                              $transaction->user_name = $transaction->user_first_name . ' ' . $transaction->user_last_name;
                              $transaction->reference_number = $transaction->reference_number ?? 'N/A';
                          })
                          ->toArray();

            $buffer[] = array("Date", "Reference Number", "Name", "Points Expired", "Value of Pts Expired", "Transaction Total");

            foreach ($data as $u) {
                $buffer[] = array(
                    ReportHelper::xlsDate($u['created_at_date']),
                    $u['reference_number'],
                    $u['user_name'],
                    $u['points_rewarded'],
                    $u['amt_reward'],
                    $u['transactions_total'],
                );
            }
            return ReportHelper::sendDownload($report, $buffer);
        } else {
            return ReportHelper::sendView($report, $defaults, $results, $tot);
        }
    }
    private function loadReport_50($report)
    {
        if (!I::can('view_basic_reports')) {
            return Redirect::to(url("dashboard"));
        }

        $defaults = ReportHelper::normalizeDefaults();

        $currencyId        = 6;
        $exchangeRate      = 1;
        $currencyShortCode = 'USD';
        if ($defaults['currency'] > 0) {
            $currencyId        = $defaults['currency'];
            $currency          = Currency::find($currencyId);
            $exchangeRate      = $currency->latestRate();
            $currencyShortCode = $currency->short_code;
        }
        $tot['exchangeRate']      = $exchangeRate;
        $tot['currencyShortCode'] = $currencyShortCode;
        $tot['currency']          = $currencyId;

        $results = (isset($defaults['params']) && $defaults['params'] == 1) ? [] : null;

        if (Input::has('export')) {
            $results = DB::table('users')
                       ->select(
                           DB::raw('SQL_CALC_FOUND_ROWS users.id'),
                           DB::raw('DATE_FORMAT(users.created_at, "%Y-%m-%d") as created_at_date'),
                           'reference.number as reference_number',
                           DB::raw('users.first_name || " " || users.last_name as name'),
                           'pos.name as pos_name',
                           'network_partner.network_id as network_id'
                       )
                       ->leftJoin('reference', 'users.id', '=', 'reference.user_id')
                       ->leftJoin('pos', 'pos.id', '=', 'users.registration_source_pos')
                       ->leftJoin('partner_user', 'partner_user.user_id', '=', 'users.id')
                       ->leftJoin('network_partner', 'network_partner.partner_id', '=', 'partner_user.partner_id')
                       ->whereBetween('users.created_at', [$defaults['start_date'], $defaults['end_date']])
                       ->whereIn('pos.store_id', $defaults['entities'])
                       ->whereIn('network_partner.network_id', $defaults['network_ids'])
                       ->groupBy('users.id');

            $buffer[] = array("DATE", "REFERENCE NUMBER", "NAME", "SOURCE OF ENROLLMENT");

            foreach ($results->get() as $u) {
                $buffer[] = array(
                    $u->created_at_date,
                    $u->reference_number,
                    $u->name,
                    $u->pos_name,
                );
            }
            return ReportHelper::sendDownload($report, $buffer);
        } else {
            return ReportHelper::sendView($report, $defaults, $results, $tot);
        }
    }

    /**
     * Load all data for the 'Branch Points Transaction Report by Posted Date' report
     *
     * @param Report $report
     * @return View
     */
    private function loadReport_54($report)
    {
        if (!I::can('view_basic_reports')) {
            return Redirect::to(url("dashboard"));
        }

        $defaults = ReportHelper::normalizeDefaults();
        $currencyId = 6;
        if ($defaults['network_currency'] != 'not') {
            $networkId  = $defaults['network_currency'];
        } else {
            $networkId  = 1;
        }
        $networkCurrencyId = Network::find($networkId)->currency_id;
        $network_currency = Currency::where('id', $networkCurrencyId)->first();
        $network_exchangeRate   = $network_currency->latestRate();
        $exchangeRate   = 1;
        $currencyShortCode  = 'USD';
        if ($defaults['currency'] > 0) {
            $currencyId = $defaults['currency'];
            $currency = Currency::where('id', $currencyId)->first();
            $exchangeRate   = $currency->latestRate();
            $currencyShortCode  = $currency->short_code;
        }

        $tot['exchangeRate'] = $exchangeRate;
        $tot['currencyShortCode'] = $currencyShortCode;
        $tot['currency'] = $currencyId;

        $results = (isset($defaults['params']) && $defaults['params'] == 1) ? [] : null;

        $tot['network_currency'] = $networkCurrencyId;
        $tot['network_exchangeRate'] = $network_exchangeRate;

        if (Input::has('export')) {
            $buffer[] = array("ID", "Date & Time", "Store", "Points Rewarded", "Points Redeemed", "Value of Pts Rewarded", "Value of Pts Redeemed", "Trx Amount($currencyShortCode)");

            foreach ($results->get() as $u) {
                $currency = Currency::where('id', $defaults['currency'])->first();
                $amtRedeem  = 0;
                $amtReward  = 0;

                $network = Network::find($u->network_id);
                $netCurrency = Currency::where('id', $network->currency_id)->first();
                $exchangeRate   = $netCurrency->latestRate();
                $amtRewardInUSD = $u->amt_reward / $exchangeRate;
                $amtRewardInDefCurrency = $amtRewardInUSD * $tot['exchangeRate'];
                $amtRedeemdInUSD = $u->amt_redeem / $exchangeRate;
                $amtRedeemInDefCurrency = $amtRedeemdInUSD * $tot['exchangeRate'];
                if ($network->currency_id == $tot['currency']) {
                    $amtReward = number_format($u->amt_reward, 2);
                    $amtRedeem = number_format($u->amt_redeem, 2);
                    $totalAmount = number_format($u->original_total_amount, 2);
                } else {
                    $amtReward = number_format($amtRewardInDefCurrency, 2);
                    $amtRedeem = number_format($amtRedeemInDefCurrency, 2);
                    $totalAmount = number_format($u->total_amount * $tot['exchangeRate'], 2);
                }
                $buffer[] = array(
                    $u->id,
                    ReportHelper::xlsDate($u->posted_at),
                    ReportHelper::defaultEmpty($u->store, 'name'),
                    number_format($u->points_rewarded),
                    number_format($u->points_redeemed),
                    $amtReward,
                    $amtRedeem,
                    $totalAmount
                );
            }
            return ReportHelper::sendDownload($report, $buffer);
        } else {
            return ReportHelper::sendView($report, $defaults, $results, $tot);
        }
    }

    /**
     * Partner Reward Transactions
     *
     * @param Report $report
     * @return View
     */
    private function loadReport_55($report)
    {
        if (!I::can('view_basic_reports')) {
            return Redirect::to(url("dashboard"));
        }

        $defaults = ReportHelper::normalizeDefaults();
        $currencyId        = 6;
        $exchangeRate      = 1;
        $currencyShortCode = 'USD';
        if ($defaults['currency'] > 0) {
            $currencyId        = $defaults['currency'];
            $currency          = Currency::find($currencyId);
            $exchangeRate      = $currency->latestRate();
            $currencyShortCode = $currency->short_code;
        }
        $tot['exchangeRate']      = $exchangeRate;
        $tot['currencyShortCode'] = $currencyShortCode;
        $tot['currency']          = $currencyId;
        $results = (isset($defaults['params']) && $defaults['params'] == 1) ? [] : null;

        if (Input::has('export')) {
            $buffer[] = array("ID", "Reference", "Member", "Date & Time", "Description", "Pts Rewarded", "Amount of Pts Rewarded", "Transaction Amount & Currency", "Billing Amount & Currency");

            $results = DB::table('transaction')
                       ->leftJoin('partner', 'transaction.partner_id', '=', 'partner.id')
                       ->leftJoin('users', 'users.id', '=', 'transaction.user_id')
                       ->leftJoin('store', 'transaction.store_id', '=', 'store.id')
                       ->leftJoin('reference', 'transaction.user_id', '=', 'reference.user_id')
                       ->leftJoin('currency', 'currency.id', '=', 'transaction.currency_id')
                       ->leftJoin('currency as partner_currency', 'partner_currency.id', '=', 'transaction.partner_currency_id')
                       ->select(
                           'transaction.id',
                           'reference.number as reference_number',
                           'users.first_name',
                           'users.last_name',
                           'transaction.created_at',
                           'transaction.ref_number',
                           'transaction.points_rewarded',
                           'transaction.amt_reward',
                           'transaction.original_total_amount',
                           'currency.short_code as currency_short_code',
                           'transaction.partner_amount',
                           'partner_currency.short_code as partner_currency_short_code'
                       )
                       ->where('transaction.trx_reward ', 1)
                       ->whereBetween('transaction.created_at', [$defaults['start_date'], $defaults['end_date']])
                       ->orderBy('transaction.id', 'desc');

            if (is_array($defaults['member_id']) && current($defaults['member_id']) != 0) {
                $results->whereIn('transaction.user_id', $defaults['member_id']);
            }

            if (!empty($defaults['network_ids'])) {
                $results->whereIn('transaction.network_id', $defaults['network_ids']);
            }

            if (!empty($defaults['entities'])) {
                $results->whereIn('transaction.partner_id', $defaults['entities']);
            }

            $buffer[] = array("DATE", "REFERENCE NUMBER", "NAME", "SOURCE OF ENROLLMENT");

            foreach ($results->get() as $u) {
                $buffer[] = array(
                    $u->id,
                    $u->reference_number,
                    $u->first_name . ' ' . $u->last_name,
                    $u->created_at,
                    $u->ref_number,
                    $u->points_rewarded,
                    $u->amt_reward,
                    $u->original_total_amount . ' ' . $u->currency_short_code,
                    $u->partner_amount . ' ' . $u->partner_currency_short_code,
                );
            }
            return ReportHelper::sendDownload($report, $buffer);
        } else {
            return ReportHelper::sendView($report, $defaults, $results, $tot);
        }
    }

} // EOC
