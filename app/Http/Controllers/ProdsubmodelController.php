<?php
namespace App\Http\Controllers;
/**
 * Prodsubmodel Controller
 * 
 * @category   Controllers
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class ProdsubmodelController extends BaseController
{
    protected $layout = "layouts.dashboard";

    /**
     * Return the prodsubmodel validations
     *
     * @return array
     */
    private function getValidations() 
    {
        return array(
            'name' => 'required|min:2'
        );
    }

    /**
     * Display a list of the Prodsubmodels
     *
     * @return void
     */
    public function listProdsubmodels()
    {
        if(!I::can('view_prodsubmodels')){
            return Redirect::to(url("dashboard"));
        }
        $this->ormClass      = 'App\Prodsubmodel';
        $this->search_fields = array('name');

        return View::make("prodsubmodel.list", array(
            'prodsubmodels' => $this->getList()
        ));
    }

    /**
     * Fetch previous prodsubmodels for the new prodsubmodel form
     *
     * @return void
     */
    public function newProdsubmodel()
    {
        if(!I::can('create_prodsubmodels')){
            return Redirect::to(url("dashboard"));
        }
        $data['prodsubmodels'] = Prodsubmodel::where('draft', false)->orderBy('name')->pluck('name', 'id');

        return View::make("prodsubmodel.new", $data);
    }

    /**
     * Create the new prodsubmodel
     *
     * @return void
     */
    public function createNewProdsubmodel()
    {
        if(!I::can('create_prodsubmodels')){
            return Redirect::to(url("dashboard"));
        }
        $validator = Validator::make(Input::all(), $this->getValidations());

        if($validator->fails()) {
            return Redirect::to('dashboard/prodsubmodels/new')->withErrors($validator)->withInput();
        }

        $prodsubmodel                     = new Prodsubmodel();
        $prodsubmodel->name               = self::sanitizeText(Input::get('name'));
        
        $prodsubmodel->draft              = false;

        $prodsubmodel->save();        

        return Redirect::to('dashboard/prodsubmodels');
    }

    /**
     * View a single Prodsubmodel resource
     *
     * @param int $id
     * @return void
     */
    public function viewProdsubmodel($id = null)
    {
        if(!I::can('view_prodsubmodels')){
            return Redirect::to(url("dashboard"));
        }
        $prodsubmodel = Prodsubmodel::find($id);

        if(!$prodsubmodel) {
            App::abort(404);
        }

        return View::make("prodsubmodel.view", array(
            'prodsubmodel'   => $prodsubmodel,
            'prodsubmodels' => Prodsubmodel::where('draft', false)->orderBy('name')->pluck('name', 'id')
        ));
    }

    /**
     * Delete a Prodsubmodel instance
     *
     * @param int $id
     * @return void
     */
    public function deleteProdsubmodel($id = null)
    {
        if(!I::can('delete_prodsubmodels')){
            return Redirect::to(url("dashboard"));
        }
        $prodsubmodel = Prodsubmodel::find($id);

        if(!$prodsubmodel) {
            App::abort(404);
        }

        $prodsubmodel->delete();

        return Redirect::to('dashboard/prodsubmodels');
    }

    /**
     * Update a prodsubmodel resource
     *
     * @param int $id
     * @return void
     */
    public function updateProdsubmodel($id = null)
    {
        if(!I::can('edit_prodsubmodels')){
            return Redirect::to(url("dashboard"));
        }
        return View::make("prodsubmodel.update", array(
            'id' => $id
        ));
    }

    /**
     * Update a prodsubmodel resource
     *
     * @param int $id
     * @return void
     */
    public function saveProdsubmodel($id = null)
    {
        if(!I::can('edit_prodsubmodels')){
            return Redirect::to(url("dashboard"));
        }
        $prodsubmodel = Prodsubmodel::find($id);

        if(!$prodsubmodel) {
            App::abort(404);
        }

        $validator = Validator::make(Input::all(), $this->getValidations());

        if($validator->fails()) {
            return Redirect::to('dashboard/prodsubmodels/new')->withErrors($validator)->withInput();
        }
        $input = Input::all();
        foreach($input as $k=>$i){
            $input[$k] = self::sanitizeText($i);
        }
        $prodsubmodel->update($input);

        return Redirect::to('dashboard/prodsubmodels');
    }

    /**
     * Load the new prodsubmodel popup
     *
     * @return void
     */
    public function newPopoverProdsubmodel()
    {
        if(!I::can('create_prodsubmodels')){
            return Redirect::to(url("dashboard"));
        }
        $prodsubmodel       = new Prodsubmodel();
        $prodsubmodel->name = "New Prodsubmodel";

        $prodsubmodel->save();
        
        return View::make("prodsubmodel.new_popover", array(
            'prodsubmodel'   => $prodsubmodel,
            'prodsubmodels' => Prodsubmodel::where('draft', false)->orderBy('name')->pluck('name', 'id')
            )
        );
    }

    /**
     * Save the popup prodsubmodel resource
     *
     * @param int $id
     * @return void
     */
    public function savePopoverProdsubmodel($id = null)
    {
        if(!I::can('edit_prodsubmodels')){
            return Redirect::to(url("dashboard"));
        }
        $prodsubmodel = prodsubmodel::find($id);

        if(!$prodsubmodel) {
            App::abort(404);
        }        

        $validator = Validator::make(Input::all(), $this->getValidations());

        if($validator->fails()) {
            return Response::json(array(
                'failed' => true,
                'errors' => "Please ensure that you provide a valid prodsubmodel name"
            ));
        }

        $prodsubmodel->name               = self::sanitizeText(Input::get('name'));
        
        $prodsubmodel->draft              = false;
        $prodsubmodel->save();

        return Response::json(array(
            'failed'   => false,
            'view_url' => url('dashboard/prodsubmodels/getList/')
        ));
    }

    /**
     * Return the select list of prodsubmodels
     *
     * @return void
     */
    public function getProdsubmodelsList()
    {
        if(!I::can('view_prodsubmodels')){
            return Redirect::to(url("dashboard"));
        }
        return View::make("prodsubmodel.selectList", array(
            'prodsubmodels' => Prodsubmodel::where('draft', false)->orderBy('name')->pluck('name', 'id')
        ));
    }
    
} // EOC