<?php
namespace App\Http\Controllers;
use App\Mobile as Mobile;
use Illuminate\Support\Facades\Auth;
use Security;
use ManagedObjectsHelper;
use Illuminate\Support\Facades\Redirect;
use I;
use Input;
use View;
use App\BluCollection as BluCollection;
use App\Card as Card;
use App\User as User;
use App\Member as Member;
use InputHelper;
use Illuminate\Support\Facades\DB;
use App\Partner as Partner;
use App\Transaction as Transaction;
use PointsHelper;
use App\Pos as Pos;
use App\Coupon as Coupon;
use CouponHelper;
use App\Currency as Currency;
use App\Store as Store;
use Response;
use Illuminate\Support\Facades\Session;


/**
 * Mobile Controller
 *
 * @category   Controllers
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class MobileController extends BaseController
{
    private $mobile_uid;
    private $mobile;
    private $partner;

    /**
     * Ensure that we have a user and setup for this instance
     *
     * @return void
     */
    public function __construct()
    {
        $user = Auth::User();
        if ($user) {
            $mobile = Mobile::where('user_id', Auth::user()->id)->first();

            if (!$mobile) {
                if (Auth::user()) {
                    $mobile = Mobile::firstOrCreate(array(
                        //'uid'     => $this->mobile_uid,
                        'user_id' => Auth::user()->id
                    ));
                }
            } else {
                if(!Auth::check())
                {
                    Auth::login($mobile->user);

                    Security::configureRoles();
                }
                $this->partner = $mobile->partner;
            }

            $this->mobile = $mobile;
        }
    }

    private function checkMobile()
    {
        $user = Auth::User();

        if ($user) {
            $mobile = Mobile::where('user_id', $user->id)->first();

            if (!$mobile) {
                if (Auth::user()) {
                    $mobile = Mobile::firstOrCreate(array('user_id' => $user->id));
                }
            } else {
                if (!Auth::check()) {
                    Auth::login($mobile->user);
                    Security::configureRoles();
                }
                $this->partner = $mobile->partner;
            }

            $this->mobile = $mobile;
        }
    }

    /**
     * Fetch all lists required for the interface
     *
     * @return array
     */
    private function getAllLists()
    {
        $mobile = Mobile::where('user_id', Auth::user()->id)->first();
        $currencies = Currency::where('draft', false)->orderBy('name', 'ASC')->get();
        return array(
            'partners' => ManagedObjectsHelper::managedPartners()->orderBy('name', 'ASC')->pluck('name','id'),
            'stores'   => ManagedObjectsHelper::managedStores(),
            'pos'      => ManagedObjectsHelper::managedPointOfSales(),
            'mobile'   => $mobile,
            'currencies'=> $currencies
        );
    }

    /**
     * Show the index page
     *
     * @return void
     */
    public function getIndex()
    {
        if(Auth::check()==false) {
            Auth::logout();
            return Redirect::to("auth/login")->with(
                "flash_error", "Please Login!"
            );
        }
        if(!I::can('use_apps'))
        {
            Auth::logout();

            return View::make("mobile.invalid_user");
        }

        $userPartner    = Auth::User()->getTopLevelPartner();
        switch ($userPartner->merchant_app){
            case "default";
                continue;
            case "3-steps barcode";
               return Redirect::to("/mobile_barcode");
            default:
                    continue;
        }

//        return View::make("mobile.index");
        return View::make("mobile.identify.mobile", $this->getAllLists());
    }
    
    /**
     * Show the index page
     *
     * @return void
     */
    public function getMenu()
    {
        if(Auth::check()==false) {
            Auth::logout();
            return Redirect::to("auth/login")->with(
                "flash_error", "Please Login!"
            );
        }
        if(!I::can('use_apps'))
        {
            Auth::logout();

            return View::make("mobile.invalid_user");
        }

        return View::make("mobile.index");
    }

    /**
     * Handle the logout request
     *
     * @return void
     */
    public function getLogout()
    {
        Auth::logout();

        return Redirect::to(url("mobile"));
    }

    /**
     * Display the settings page
     *
     * @return void
     */
    public function getSettings()
    {
        if(!I::can('use_apps'))
        {
            Auth::logout();

            return View::make("mobile.invalid_user");
        }

        $this->checkMobile();
        if(!$this->mobile) {
            return Redirect::to('mobile/error')->with('message', 'Unable to determine the device ID');
        }

        return View::make("mobile.settings", $this->getAllLists());
    }

    /**
     * Update the setings
     *
     * @return void
     */
    public function postSettings()
    {
        if(!I::can('use_apps'))
        {
            Auth::logout();

            return View::make("mobile.invalid_user");
        }
        $mobile = Mobile::where('user_id', Auth::user()->id)->first();

        $mobile->partner_id = self::sanitizeText(Input::get('partner_id'));
        $mobile->store_id   = self::sanitizeText(Input::get('store_id'));
        $mobile->pos_id     = self::sanitizeText(Input::get('pos_id'));
        $mobile->country_id = self::sanitizeText(Input::get('country_id'));
        $mobile->currency_id = self::sanitizeText(Input::get('currency_id'));

        $mobile->save();

        return Redirect::to(url("mobile"));
    }

    /**
     * Fetch stores for the specified partner
     *
     * @param int $partner_id
     * @return View
     */
    public function getStoresforpartner($partner_id)
    {
        if(!I::can('use_apps'))
        {
            Auth::logout();

            return View::make("mobile.invalid_user");
        }
        $stores = ManagedObjectsHelper::managedStores();
        $stores = new BluCollection($stores);
        $stores = $stores->fieldEqualsValue('partner_id', $partner_id);
        $stores = $stores->pluck('name', 'id');

        return View::make('mobile.storesList', array('stores'=>$stores));
    }

    /**
     * Fetch POS resources for the specified store
     *
     * @param int $store_id
     * @return View
     */
    public function getPosforstore($store_id)
    {
        if(!I::can('use_apps'))
        {
            Auth::logout();

            return View::make("mobile.invalid_user");
        }
        $pos = ManagedObjectsHelper::managedPointOfSales();
        $pos = new BluCollection($pos);
        $pos = $pos->fieldEqualsValue('store_id', $store_id);
        $pos = $pos->pluck('name', 'id');

        return View::make('mobile.posList', array('pos'=>$pos));
    }

    /**
     * Start the barcode scan process
     *
     * @return View
     */
    public function getScan()
    {
        if(!I::can('use_apps'))
        {
            Auth::logout();

            return View::make("mobile.invalid_user");
        }

        $this->checkMobile();
        if(!$this->mobile ) {
            return Redirect::to('mobile/error')->with('message', 'Unable to determine the device ID');
        }

        return View::make('mobile.identify.scan');
    }

    /**
     * Display the generic error page
     *
     * @return void
     */
    public function getError()
    {
        if(!I::can('use_apps'))
        {
            Auth::logout();

            return View::make("mobile.invalid_user");
        }
        return View::make('mobile.error', array('error' => Session::get('message')));
    }

    /**
     * Process the scanned card number
     *
     * @return void
     */
    public function postScan()
    {
        if(!I::can('use_apps'))
        {
            Auth::logout();

            return View::make("mobile.invalid_user");
        }
        $scan = self::sanitizeText(Input::get('scan'));
        $card = Card::where('number', $scan)->first();

        if(!$card) {
            return Redirect::to('mobile/error')->with('message', 'Invalid card number entered');
        }

        $member = $card->user;

        if (!$member) {
            return Redirect::to(url("/mobile/newmemberwithcard/$card->id"));
        }

        return Redirect::to(url('/mobile/txinfo/'.$member->id));
    }

    /**
     * Show the identify via mobile number screen
     *
     * @return void
     */
    public function getMobile()
    {
        if(!I::can('use_apps'))
        {
            Auth::logout();

            return View::make("mobile.invalid_user");
        }

        $this->checkMobile();
        if(!$this->mobile) {
            return Redirect::to('mobile/error')->with('message', 'Unable to determine the device ID');
        }

        return View::make('mobile.identify.mobile', $this->getAllLists());
    }

    /**
     * Perform the identification via mobile
     *
     * @return void
     */
    public function postMobile()
    {
        $userTopLevelPartner  = Auth::User()->getTopLevelPartner();
        $userTopLevelPartnerId  = $userTopLevelPartner->id;

        if(!I::can('use_apps'))
        {
            Auth::logout();

            return View::make("mobile.invalid_user");
        }
        $code   = self::sanitizeText(Input::get('telephone_code'));
        $mobile = self::sanitizeText(Input::get('mobile'));
        $number = InputHelper::normalizeMobileNumber($mobile, $code);
        $reference = self::sanitizeText(Input::get('reference'));
        $barcode = self::sanitizeText(Input::get('barcode'));
        $skipstatus= array();
        if(!$userTopLevelPartner->can_reward_inactive){
            $skipstatus[] = 'inactive';
        }
        $skipstatus[] = 'closed';

        if(!empty($mobile)){
             $member = Member::select('users.id')
                    ->where('users.normalized_mobile', $number)
                    ->where('users.draft', false)
                    ->whereNotIn('status', $skipstatus)
                    ->first();

            if(!$member) {
                return Redirect::to(url("/mobile/newmemberwithmobile/$code/$mobile"));
            }
        } elseif (!empty($reference)) {
            $member = Member::select('users.id')
                    ->where('reference.number', $reference)
                    ->where('users.draft', false)                  
                    ->LeftJoin('reference', 'users.id', '=','reference.user_id')
                    ->first();
            if(!$member) {
                    return Redirect::to(url("/mobile"))->withErrors(['We could not locate the specified account.'])->withInput();
            }
        }elseif (!empty($barcode)) {
           $member = Member::select('users.id')
                    ->where('card.number', $barcode)
                    ->where('users.draft', false)
                    ->LeftJoin('card', 'users.id', '=','card.user_id')
                    ->first(); 
           if(!$member) {
                    return Redirect::to(url("/mobile"))->withErrors(['We could not locate the specified account.'])->withInput();
            }
        } else {
            return Redirect::to(url("/mobile"))->withErrors(['Please, fill mobile number or reference.'])->withInput();
        }
        
        if($userTopLevelPartnerId == 3){
                return Redirect::to(url('/mobile/omtxinfo/'.$member->id));
        }
        else{
                return Redirect::to(url('/mobile/txinfo/'.$member->id));
        }
    }

    /**
     * Fetch a member using their card
     *
     * @param int $card_id
     * @return View
     */
    public function getNewmemberwithcard($card_id)
    {
        if(!I::can('use_apps'))
        {
            Auth::logout();

            return View::make("mobile.invalid_user");
        }
        $card = Card::find($card_id);

        $this->checkMobile();
        if(!$card){
            return Redirect::to('mobile/error')->with('message', 'Unable to locate the specified card number');
        }

        $member = User::createNewUser();
        $member->registration_source_pos            = $this->mobile->pos_id;
        $member->save();
        
        $card->user_id = $member->id;
        $card->save();

        return View::make('mobile.member',
            array('member' => $member)
        );
    }

    /**
     * Create a new member
     * @param
     * @return void
     */
    public function getNewmemberwithmobile($code, $mobile)
    {
        if(!I::can('use_apps'))
        {
            Auth::logout();

            return View::make("mobile.invalid_user");
        }
        $mobile = InputHelper::sanitizeMobile($mobile);
        $code   = InputHelper::sanitizeTelephoneCode($code);
        $normalizedMobile = InputHelper::normalizeMobileNumber($mobile, $code);
        $member = User::createNewUser('inactive');

        $member->telephone_code    = $code;
        $member->normalized_mobile = $normalizedMobile;
        $member->mobile            = $mobile;
        $member->save();

        return View::make('mobile.member', compact('member'));
    }

    /**
     * Update a member account
     *
     * @return Mixed
     */
    public function postMember($id)
    {
        if(!I::can('use_apps'))
        {
            Auth::logout();

            return View::make("mobile.invalid_user");
        }

        $user = User::find($id);

        $user->first_name = self::sanitizeText(Input::get('first_name'));
        $user->last_name  = self::sanitizeText(Input::get('last_name'));
        $user->gender     = self::sanitizeText(Input::get('gender'));
        $user->email      = self::sanitizeText(Input::get('email'));
        $user->verified   = false;
        if(!empty(self::sanitizeText(Input::get('first_name'))) && !empty(self::sanitizeText(Input::get('last_name'))) && !empty(self::sanitizeText(Input::get('gender'))) && !empty(self::sanitizeText(Input::get('email')))){
            $user->status   = 'active';
        } else {
            $user->status   = 'inactive';
        }

        if($user->draft) {
            if($user->first_name) {
                $partnerId = Auth::User()->getTopLevelPartner()->id;

//                if($partnerId == 4226){
//                    $user->sendWelcomeMessage('4226', 'new-user-smile-4226');
//                } else {
//                    $user->sendWelcomeMessage();
//                }
            }

            if ($user->status == 'active') {
                $user->draft = false;
            }

            $user->save();

            return Redirect::to(url('/mobile/txinfo/'.$user->id));
        } else {
            $user->save();

            $coupon_code = self::sanitizeText(Input::get('coupon_code'));
            $amount      = self::sanitizeText(Input::get('amount'));
            $currency    = self::sanitizeText(Input::get('currency'));
            $points    = self::sanitizeText(Input::get('points'));

            return $this->doTxInfo($id, $coupon_code, $amount, $currency, $points);
        }
    }

    /**
     * Show member profile information
     *
     * @return View
     */
    public function postShowmemberinfo()
    {
        if(!I::can('use_apps'))
        {
            Auth::logout();

            return View::make("mobile.invalid_user");
        }
        $member      = User::find(self::sanitizeText(Input::get('member_id')));
        $coupon_code = self::sanitizeText(Input::get('coupon_code'));
        $amount      = self::sanitizeText(Input::get('amount'));
        $currency    = self::sanitizeText(Input::get('currency'));

        return View::make('mobile.member',
            array(
                'member'      => $member,
                'coupon_code' => $coupon_code,
                'currency'    => $currency,
                'amount'      => $amount
            )
        );
    }


    /**
     * Display the transaction information for a user
     *
     * @param int $member_id
     * @return View
     */
    public function getTxinfo($member_id)
    {
        if(!I::can('use_apps'))
        {
            Auth::logout();

            return View::make("mobile.invalid_user");
        }
        $member = User::find($member_id);
        $mobile = Mobile::where('user_id', Auth::user()->id)->first();

        $currencies = Currency::where('draft', false)->orderBy('name', 'ASC')->get();

        return View::make('mobile.txinfo',
            array(
                'member'     => $member,
                'currencies' => $currencies,
                'mobile' => $mobile
            )
        );
    }

    /**
     * Display the transaction information for a user
     *
     * @param int $member_id
     * @return View
     */
    public function getOmtxinfo($member_id)
    {
        if(!I::can('use_apps'))
        {
            Auth::logout();

            return View::make("mobile.invalid_user");
        }
        $member = User::find($member_id);
        $mobile = Mobile::where('user_id', Auth::user()->id)->first();

        $currencies = Currency::where('draft', false)->orderBy('name', 'ASC')->get();

        return View::make('mobile.omtxinfo',
            array(
                'member'     => $member,
                'currencies' => $currencies,
                'mobile' => $mobile
            )
        );
    }

    /**
     * Display the transaction information request
     *
     * @param int $member_od
     * @param string $coupon_code
     * @param float $amount
     * @param string $currency_shortcode
     * @return View
     */
    private function doTxInfo($member_id, $coupon_code=NULL, $amount = 0, $currency_shortcode = 'USD', $points = 0)
    {
        if (!I::can('use_apps'))        {
            Auth::logout();

            return View::make("mobile.invalid_user");
        }

        $member           = Member::find($member_id);
        $partnerId        = Auth::User()->getTopLevelPartner()->id;
        $couponPrice      = 0;
        $couponCurrencyId = 6;

        if (!$member) {
            return Redirect::to('mobile/error')->with('message', 'Unable to locate specified member');
        }

        if ($coupon_code) {
            $coupon = Coupon::where('coupon_code', $coupon_code)->first();

            if (!$coupon) {
                return Redirect::to('mobile/error')->with('message', 'Unable to locate the specified coupon code');
            }

            $couponDetails    = CouponHelper::CouponPrice($coupon_code, $amount, $partnerId);
            $couponPrice    = $couponDetails['price'];
            $couponCurrencyId = $couponDetails['currency'];
        }

        $this->checkMobile();
        $store = $this->mobile->store;

        if (!$store) {
            return Redirect::to('mobile/error')->with('message', 'Unable to locate the specified store');
        }

        if (empty($amount) || $amount == 0) {
            $amount = $couponPrice;
        }
        $reward_points  = PointsHelper::rewardPoints($amount,  $member->id, $store->id, array(),$currency_shortcode, $couponPrice, $couponCurrencyId);
        $program_name = PointsHelper::programName($store->id);
        $redeem_points  = PointsHelper::redeemPoints($amount, $currency_shortcode, $store->id, $couponPrice, $couponCurrencyId);
        $bonusPoints  = $points;

        return View::make('mobile.txtype',
            array(
                'member'        => $member,
                'amount'        => $amount,
                'currency'      => $currency_shortcode,
                'store'         => $store,
                'reward_points' => $reward_points,
                'redeem_points' => $redeem_points,
                'coupon_code'   => $coupon_code,
                'program_name'  => $program_name,
                'partner'       => $this->partner->id,
                'couponPrice'   => $couponPrice,
                'couponCurrency'=> $couponCurrencyId,
                'couponType'  => CouponHelper::CouponType($coupon_code),
                'bonusPoints'   => $bonusPoints
            )
        );
    }

    /**
     * Perform the transaction info request
     *
     * @return void
     */
    public function postTxinfo()
    {
        if(!I::can('use_apps'))
        {
            Auth::logout();

            return View::make("mobile.invalid_user");
        }
        $member_id   = self::sanitizeText(Input::get('member_id'));
        $coupon_code = self::sanitizeText(Input::get('coupon_code'));
        $amount      = self::sanitizeText(Input::get('amount'));
        $currency    = self::sanitizeText(Input::get('currency_id'));
        $points    = self::sanitizeText(Input::get('points'));

        return $this->doTxInfo($member_id, $coupon_code, $amount, $currency, $points);
    }
    /**
     * Perform the transaction info request
     *
     * @return void
     */
    public function postTxinfoom()
    {
        if(!I::can('use_apps'))
        {
            Auth::logout();

            return View::make("mobile.invalid_user");
        }
        $member_id   = self::sanitizeText(Input::get('member_id'));
        $coupon_code = self::sanitizeText(Input::get('coupon_code'));
        $amount      = self::sanitizeText(Input::get('amount'));
        $currency    = self::sanitizeText(Input::get('currency_id'));
        $points    = self::sanitizeText(Input::get('points'));

        return $this->doTxInfoom($member_id, $coupon_code, $amount, $currency, $points);
    }

    /**
     * Perform the transaction
     *
     * @return void
     */
    public function postTxtype()
    {
        if(!I::can('use_apps'))
        {
            Auth::logout();

            return View::make("mobile.invalid_user");
        }
        $member     = Member::find(self::sanitizeText(Input::get('member_id')));
        foreach($member->segments as $segment){
                $memberSegments[] = $segment->id;
        }

        $this->checkMobile();
        $amount     = self::sanitizeText(Input::get('amount'));
        $currency   = self::sanitizeText(Input::get('currency'));
        $coupon_code            = self::sanitizeText(Input::get('coupon_code'));
        $store      = $this->mobile->store;
        $type     = self::sanitizeText(Input::get('type'));
        $pin      = self::sanitizeText(Input::get('pin'));
        $partner    = Partner::find($store->partner_id);
        $points     = self::sanitizeText(Input::get('points'));
        
        $partnerId              = Auth::User()->getTopLevelPartner()->id;
        $couponDetails    = CouponHelper::CouponPrice($coupon_code, $amount, $partnerId);
        $couponPrice    = $couponDetails['price'];
        $couponCurrencyId = $couponDetails['currency'];
        $couponCurrencyobj      = Currency::where('id', $couponCurrencyId)->first();
        $couponCurrency         = $couponCurrencyobj->short_code;
        $couponType             = CouponHelper::CouponType($coupon_code);
        
        if(!empty($coupon_code)){
            $coupon = Coupon::where('coupon_code', $coupon_code)->first();
            $couponSegment    = $coupon->segmentId(); 
            $coupnValidFrom = $coupon->valid_from();
            $coupnValidTo = $coupon->valid_to();
            $couponPartner  = $coupon->partner_id;
            $currentDate  = date('Y-m-d');
        }
        if(!empty($couponCurrencyId)){
                $couponCurrencyDetails    = Currency::where('id', $couponCurrencyId)->first();
                $couponPriceInUsd   = $couponPrice / $couponCurrencyDetails->latestPrice()->rate;
                $couponCurrencyShortCode  = $couponCurrencyDetails->short_code;
        }
        else{
                $couponPriceInUsd   = $couponPrice;
                $couponCurrencyShortCode  = 'USD';
        }

       
        
        
        if($type == 'reward')
        {
            $params = array(
                'user_id'     => $member->id,
                'partner'     => $store->partner_id,
                'amount'      => $amount,
                'currency'    => $currency,
                'store_id'    => $store->id,
                'pos_id'      => $this->mobile->pos_id,
                'coupon_code' => $coupon_code,
                'reference'   => "{$store->name} Transaction",
                'network_id'  => $partner->firstNetwork()->id,
                'from_merchant_app' => 1,
                'posted_at' => date('Y-m-d H:i:s')
            );

            $response = Transaction::doReward($params, 'Amount');
        }
        else if($type == 'redeem')
        {

            $redeemPoints  = PointsHelper::redeemPoints($amount, $currency, $store->id, $couponPrice, $couponCurrencyId);

            if(Input::has('pin')) {

                if(self::sanitizeText(Input::get('pin')) == $member->passcode)
                {
                    if($partner->id == 3){
                        if($coupon_code){
                            $params = array(
                                'user_id'   => $member->id,
                                'partner'   => $store->partner_id,
                                'amount'    => 0,
                                'store_id'    => $store->id,
                                'pos_id'    => $this->mobile->pos_id,
                                'currency'    => $currency,
                                'coupon_code'           => $coupon_code,
                                'reference'   => "{$store->name} Transaction",
                                'from_merchant_app'     => 1,
                                'posted_at' => date('Y-m-d H:i:s')
                            );

                            $response = Transaction::doRedemption($params, 'Amount');
                            if($response->status == 500){
                                    return View::make('mobile.txcomplete', array(
                                            'response' => $response,
                                            'member'   => $member,
                                            'store'    => $store,
                                            'type'     => $type
                                    ));
                            }
                        }
                        if($amount != 0 && !empty($amount)){
                            $params = array(
                             'user_id'   => $member->id,
                             'partner'   => $store->partner_id,
                             'amount'    => $amount,
                             'redeem_points' => $redeemPoints,
                             'store_id'  => $store->id,
                             'pos_id'    => $this->mobile->pos_id,
                             'currency'  => $currency,
                             'reference' => "{$store->name} Transaction",
                             'from_merchant_app'     => 1,
                             'posted_at' => date('Y-m-d H:i:s')
                         );

                                $response = Transaction::doRedemption($params, 'Amount');

                            if($response->status != 200){
                                    DB::table('coupon')->where('coupon_code', $params['coupon_code'])->update(array(
                                        'transaction_id' => ''
                                    ));
                            }
                        }
                    }
                    else{
                        $params = array(
                            'user_id'           => $member->id,
                            'partner'           => $store->partner_id,
                            'amount'            => $amount,
                            'store_id'          => $store->id,
                            'redeem_points'     => $redeemPoints,
                            'pos_id'            => $this->mobile->pos_id,
                            'currency'          => $currency,
                            'coupon_code'       => $coupon_code,
                            'reference'         => "{$store->name} Transaction",
                            'network_id'        => $partner->firstNetwork()->id,
                            'from_merchant_app' => 1,
                            'posted_at' => date('Y-m-d H:i:s')
                        );
                        
                        $response = Transaction::doRedemption($params, 'Amount');
                    }

                } else {
                    return View::make('mobile.passcode',
                        array(
                            'partner_id' =>$partner->id,
                            'member'            => $member,
                            'amount'            => $amount,
                            'currency'          => $currency,
                            'coupon_code'       => $coupon_code,
                            'type'              => $type,
                            'couponPrice'       => $couponPrice,
                            'couponCurrency'    => $couponCurrency,
                            'couponType'        => $couponType,
                            'message'           => "The PIN provided is incorrect",
                            'error'             => 0
                        )
                    );
                }


            } else {
                $message    = "";
                $error      = 0;
                if(!empty($coupon_code)){
                    if( strtotime($coupnValidFrom) <= strtotime($currentDate) && strtotime($currentDate) <= strtotime($coupnValidTo)){
                            $message    = "";
                    }
                    else{
                            $message    = "The coupon is Expired!";
                            $error      = 1;
                    }

                    if($couponPartner != $partner->id){
//                        $message    = "This coupon is issued by " . Partner::find($couponPartner)->name;
                        $message    = "This coupon is issued by another partner";
                        $error      = 1;
                    }

                    if($couponSegment > 0 && !in_array($couponSegment, $memberSegments)){
                        $message    = "The user is not in the corresponding segment. ";
                        $error      = 1;
                    }

                    if(!empty($coupon->transaction_id)) {
                        $message    = "The coupon is already used for another transaction!";
                        $error      = 1;
                    }
                }
                return View::make('mobile.passcode',
                    array(
                        'partner_id'        =>$partner->id,
                        'member'            => $member,
                        'amount'            => $amount,
                        'currency'          => $currency,
                        'coupon_code'       => $coupon_code,
                        'type'              => $type,
                        'couponPrice'       => $couponPrice,
                        'couponCurrency'    => $couponCurrency,
                        'couponType'        => $couponType,
                        'message'           => $message,
                        'error'             => $error,
                    )
                );
            }
        }
    else if($type == 'bonus'){
      $params = array(
                'user_id'   => $member->id,
                'partner'   => $store->partner_id,
        'pos_id'    => $this->mobile->pos_id,
        'store_id'    => $store->id,
                'amount'    => 0,
                'currency'    => $currency,
        'notes'     => 'Bonus Points',
        'type'      => 'bonus',
        'points'    => $points,
                'reference'   => "{$partner->name} Bonus POINTS",
                'network_id'  => $partner->firstNetwork()->id,
                        'posted_at' => date('Y-m-d H:i:s')
            );

            $response = Transaction::doReward($params, 'Amount');
    }

        return View::make('mobile.txcomplete', array(
            'response' => $response,
            'member'   => $member,
            'store'    => $store,
            'type'     => $type
        ));
    }

    /**
     * Fetch a list of today's transaction for a particular store
     *
     * @return View
     */
    public function getTransactions()
    {
        if(!I::can('use_apps'))
        {
            Auth::logout();

            return View::make("mobile.invalid_user");
        }

        $this->checkMobile();
        $store = Store::find($this->mobile->store_id);

        if(!$store) {
            return Redirect::to('mobile/error')->with('message', 'Unable to locate application store');
        }

        $transactions = $store->transactions()->where('created_at', '>=', date('Y-m-d',time()))->paginate(20);

        return View::make('mobile.todaytx', array('transactions' => $transactions));
    }

    /**
     * Fetch a list of today's transaction for a particular store in print format
     *
     * @return View
     */
    public function getPrintTransactions()
    {
        if(!I::can('use_apps'))
        {
            Auth::logout();

            return View::make("mobile.invalid_user");
        }

        $this->checkMobile();
        $store = Store::find($this->mobile->store_id);

        if(!$store) {
            return Redirect::to('mobile/error')->with('message', 'Unable to locate application store');
        }

        $transactions = $store->transactions()->where('created_at', '>=', date('Y-m-d',time()))->get();

        return View::make('mobile.todayprint', array('transactions' => $transactions));
    }


    /**
     * Fetch a particular transaction
     *
     * @param int $tx_id
     * @return View
     */
    public function getTransaction($tx_id)
    {
        if(!I::can('use_apps'))
        {
            Auth::logout();

            return View::make("mobile.invalid_user");
        }
        $transaction = Transaction::find($tx_id);

        if(!$transaction) {
            return Redirect::to('mobile/error')->with('message', "Unable to find transaction with id {{$tx_id}}");
        }

        return View::make('mobile.transaction', array('transaction' => $transaction));
    }

    /**
     * Fetch a particular transaction
     *
     * @param int $tx_id
     * @return View
     */
    public function getPrintTransaction($tx_id)
    {
        if(!I::can('use_apps'))
        {
            Auth::logout();

            return View::make("mobile.invalid_user");
        }
        $transaction = Transaction::find($tx_id);

        if(!$transaction) {
            return Redirect::to('mobile/error')->with('message', "Unable to find transaction with id {{$tx_id}}");
        }

        return View::make('mobile.transactionprint', array('transaction' => $transaction));
    }

    /**
     * Perform a transaction void request
     *
     * @param int $tx_id
     * @return View
     */
    public function getVoidtx($tx_id)
    {
        if(!I::can('use_apps'))
        {
            Auth::logout();

            return View::make("mobile.invalid_user");
        }
        $transaction = Transaction::voidTransaction($tx_id);

        if(!$transaction) {
            return Redirect::to('mobile/error')->with('message', "Unable to find transaction with id {{$tx_id}}");
        }

        return Redirect::to(url("mobile/transactions"));
    }

    /**
     * Load up the phonegap config XML
     *
     * @return XML Response
     */
    public function getConfigxml()
    {
        return Response::download(base_path('phonegap')."/www/config.xml");
    }

    /**
     * Load up the cordova javascript file
     *
     * @return Javascript Response
     */
    public function getCordova()
    {
        return Response::download(base_path('public')."/cordova.js", 'cordova.js', array(
            'Content-Type', 'application/javascript'
        ));
    }
    
    /**
     * Display the transaction information request
     *
     * @param int $member_od
     * @param string $coupon_code
     * @param float $amount
     * @param string $currency_shortcode
     * @return View
     */
    private function doTxInfoom($member_id, $coupon_code=NULL, $amount = 0, $currency_shortcode = 'USD', $points = 0)
    {
        if(!I::can('use_apps'))
        {
            Auth::logout();

            return View::make("mobile.invalid_user");
        }

        $member     = Member::find($member_id);
        $partnerId    = Auth::User()->getTopLevelPartner()->id;
        $couponPrice    = 0;
        $couponCurrencyId = 6;
        if(!$member) {
            return Redirect::to('mobile/error')->with('message', 'Unable to locate specified member');
        }

        if($coupon_code) {
            $coupon = Coupon::where('coupon_code', $coupon_code)->first();

            if (!$coupon) {
                return Redirect::to('mobile/error')->with('message', 'Unable to locate the specified coupon code');
            }

            $couponDetails  = CouponHelper::CouponPrice($coupon_code, $amount, $partnerId);
            $couponPrice  = $couponDetails['price'];
            $couponCurrencyId = $couponDetails['currency'];
        }

        $this->checkMobile();
        $store = $this->mobile->store;

        if(!$store) {
            return Redirect::to('mobile/error')->with('message', 'Unable to locate the specified store');
        }

    if(empty($amount) || $amount == 0){
      $amount = $couponPrice;
    }
        $reward_points  = PointsHelper::rewardPoints($amount,  $member->id, $store->id,array(),$currency_shortcode, $couponPrice, $couponCurrencyId);
        $program_name = PointsHelper::programName($store->id);
        $redeem_points  = PointsHelper::redeemPoints($amount, $currency_shortcode, $store->id, $couponPrice, $couponCurrencyId);
        $bonusPoints  = $points;

            $params = array(

                'member_id'        => $member->id,
                'amount'        => $amount,
                'currency'      => $currency_shortcode,
                'store'         => $store,
                'reward_points' => $reward_points,
                'redeem_points' => $redeem_points,
                'coupon_code'   => $coupon_code,
                'program_name'  => $program_name,
                'partner'       => $this->partner,
                'couponPrice'   => $couponPrice,
                'couponCurrency'=> $couponCurrencyId,
                'couponType'  => CouponHelper::CouponType($coupon_code),
                'bonusPoints'   => $bonusPoints,
                'type'          => 'redeem'
            );
            
            return $this->postTxtypeom($params);
    }
    
    /**
     * Perform the transaction
     *
     * @return void
     */
    private function postTxtypeom($params)
    {
        if(!I::can('use_apps'))
        {
            Auth::logout();

            return View::make("mobile.invalid_user");
        }

        $member     = Member::find(self::sanitizeText(Input::get('member_id', $params['member_id'])));
        foreach($member->segments as $segment){
                $memberSegments[] = $segment->id;
        }
        $amount     = self::sanitizeText(Input::get('amount', $params['amount']));
        $currency   = self::sanitizeText(Input::get('currency', $params['currency']));

        $this->checkMobile();
        $store = $this->mobile->store;
        $coupon_code            = self::sanitizeText(Input::get('coupon_code', $params['coupon_code']));
        $type     = self::sanitizeText(Input::get('type', $params['type']));
        $pin      = self::sanitizeText(Input::get('pin'));
        $partner    = Partner::find($store->partner_id);
        
        $points     = self::sanitizeText(Input::get('points'));
 
        $couponPrice    = (!empty($params['couponPrice'])) ? $params['couponPrice'] : 0;
        $couponCurrencyId = (!empty($params['couponCurrency'])) ? $params['couponCurrency'] : 6;
        $couponType             = (!empty($params['couponType'])) ? $params['couponType'] : 0;
        $couponCurrencyobj      = Currency::where('id', $couponCurrencyId)->first();
        $couponCurrency         = $couponCurrencyobj->short_code;
        
        if(!empty($coupon_code)){
            $coupon = Coupon::where('coupon_code', $coupon_code)->first();
            $couponSegment    = $coupon->segmentId();
            $coupnValidFrom = $coupon->valid_from();
            $coupnValidTo = $coupon->valid_to();
            $couponPartner  = $coupon->partner_id;
            $currentDate  = date('Y-m-d');
        }
        if(!empty($couponCurrencyId)){
                $couponCurrencyDetails    = Currency::where('id', $couponCurrencyId)->first();
                $couponPriceInUsd   = $couponPrice / $couponCurrencyDetails->latestPrice()->rate;
                $couponCurrencyShortCode  = $couponCurrencyDetails->short_code;
        }
        else{
                $couponPriceInUsd   = $couponPrice;
                $couponCurrencyShortCode  = 'USD';
        }

        
        
        if($type == 'reward')
        {
            $params = array(
                'user_id'     => $member->id,
                'partner'     => $store->partner_id,
                'amount'      => $amount,
                'currency'    => $currency,
                'store_id'    => $store->id,
                'pos_id'      => $this->mobile->pos_id,
                'coupon_code' => $coupon_code,
                'reference'   => "{$store->name} Transaction",
                'network_id'  => $partner->firstNetwork()->id,
                'posted_at' => date('Y-m-d H:i:s')
            );

            $response = Transaction::doReward($params, 'Amount');
        }
        else if($type == 'redeem')
        {
            if(Input::has('pin')) {

                if(self::sanitizeText(Input::get('pin')) == $member->passcode)
                {
                    if($partner->id == 3){
                        if($coupon_code){
                            $params = array(
                                'user_id'   => $member->id,
                                'partner'   => $store->partner_id,
                                'amount'    => 0,
                                'store_id'    => $store->id,
                                'pos_id'    => $this->mobile->pos_id,
                                'currency'    => $currency,
                                'coupon_code'           => $coupon_code,
                                'reference'   => "{$store->name} Transaction",
                                'from_merchant_app'     => 1,
                                'posted_at' => date('Y-m-d H:i:s')
                            );

                            $response1 = Transaction::doRedemption($params, 'Amount');
                            if($response1->status == 500){
                                    return View::make('mobile.txcomplete', array(
                                            'response' => $response1,
                                            'member'   => $member,
                                            'store'    => $store,
                                            'type'     => $type
                                    ));
                            }
                        }
                        
        if($amount != 0 && !empty($amount)){
                            $params = array(
                            'user_id'   => $member->id,
                            'partner'   => $store->partner_id,
                            'amount'    => $amount,
                            'store_id'  => $store->id,
                            'pos_id'    => $this->mobile->pos_id,
                            'currency'  => $currency,
                            'reference' => "{$store->name} Transaction",
                            'from_merchant_app'     => 1,
                            'posted_at' => date('Y-m-d H:i:s')
                            );

                            $response = Transaction::doRedemption($params, 'Amount');

                            if($response->status != 200){
                                DB::table('coupon')->where('coupon_code', $params['coupon_code'])->update(array(
                                'transaction_id' => ''
                                ));
                            }
                        }
                    }
                    else{
                        $params = array(
                            'user_id'   => $member->id,
                            'partner'   => $store->partner_id,
                            'amount'    => $amount,
                            'store_id'    => $store->id,
                            'pos_id'    => $this->mobile->pos_id,
                            'currency'    => $currency,
                            'coupon_code' => $coupon_code,
                            'reference'   => "{$store->name} Transaction",
                            'network_id'  => $partner->firstNetwork()->id,
                            'from_merchant_app'     => 1,
                            'posted_at' => date('Y-m-d H:i:s')
                        );

                        $response = Transaction::doRedemption($params, 'Amount');
          }

                } else {
                    return View::make('mobile.passcode',
                        array(
                            'partner_id'        =>$partner->id,
                            'member'            => $member,
                            'amount'            => $amount,
                            'currency'          => $currency,
                            'coupon_code'       => $coupon_code,
                            'type'              => $type,
                            'couponPrice'       => $couponPrice,
                            'couponCurrency'  => $couponCurrency,
                            'couponType'        => $couponType,
                            'message'           => "The PIN provided is incorrect"
                            
                        )
                    );
                }


            } else {
                $message    = "";
                $error      = 0;
                if(!empty($coupon_code)){
                    if( strtotime($coupnValidFrom) <= strtotime($currentDate) && strtotime($currentDate) <= strtotime($coupnValidTo)){
                            $message    = "";
                    }
                    else{
                            $message    = "The coupon is Expired!";
                            $error      = 1;
                    }

                    if($couponPartner != $partner->id){
//                        $message    = "This coupon is issued by " . Partner::find($couponPartner)->name;
                        $message    = "This coupon is issued by another partner";
                        $error      = 1;
                    }

                    if($couponSegment > 0 && !in_array($couponSegment, $memberSegments)){
                        $message    = "The user is not in the corresponding segment. ";
                        $error      = 1;
                    }

                    if(!empty($coupon->transaction_id)) {
                        $message    = "The coupon is already used for another transaction!";
                        $error      = 1;
                    }
                }
                return View::make('mobile.passcode',
                    array(
                        'partner_id'        =>$partner->id,
                        'member'            => $member,
                        'amount'            => $amount,
                        'currency'          => $currency,
                        'coupon_code'       => $coupon_code,
                        'type'              => $type,
                        'couponPrice'       => $couponPrice,
                        'couponCurrency'    => $couponCurrency,
                        'couponType'        => $couponType,
                        'message'           => $message,
                        'error'             => $error,
                        
                    )
                );
            }
        }
    else if($type == 'bonus'){
      $params = array(
                'user_id'   => $member->id,
                'partner'   => $store->partner_id,
        'pos_id'    => $this->mobile->pos_id,
        'store_id'    => $store->id,
                'amount'    => 0,
                'currency'    => $currency,
        'notes'     => 'Bonus Points',
        'type'      => 'bonus',
        'points'    => $points,
                'reference'   => "{$partner->name} Bonus POINTS",
                'network_id'  => $partner->firstNetwork()->id,
                        'posted_at' => date('Y-m-d H:i:s')
            );

            $response = Transaction::doReward($params, 'Amount');
    }

        return View::make('mobile.txcomplete', array(
            'response' => $response,
            'member'   => $member,
            'store'    => $store,
            'type'     => $type
        ));
    }
} // EOC
