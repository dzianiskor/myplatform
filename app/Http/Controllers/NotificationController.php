<?php
namespace App\Http\Controllers;
use Carbon\Carbon;
use ManagedObjectsHelper;
use View;
use Input;
use Validator;
use I;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Notification as Notification;
use App\Tier as Tier;
use App\Segment as Segment;
use App\Partner as Partner;
use App\User as User;
use App\NotificationSegment as NotificationSegment;
use App\NotificationUser as NotificationUser;
use AuditHelper;
use Config;
use Response;
use SMSHelper;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use SendGrid;

/**
 * Notification Controller
 *
 * @category   Controllers
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class NotificationController extends BaseController
{
    protected $layout = "layouts.dashboard";

    private $auth_user = null;

    public function __construct()
    {
        parent::__construct();

        $this->auth_user = Auth::User();
    }

    /**
     * Fetch the contextual lists
     *
     * @return array
     */
    private function getAllLists()
    {
        return [
            'partnerData' => ManagedObjectsHelper::managedPartners()->sortBy('name')->pluck('name', 'id'),
            'typeData' =>  array('Email' => 'Email', 'SMS' => 'Sms'),
        ];
    }

    /**
     * Fetch the validations
     *
     * @return array
     */
    private function getValidations()
    {
        return array(
            'name'       => 'required',
            'partner_id' => 'required|integer',
            'start_date' => 'required',
        );
    }

    /**
     * List all contextual notifications
     *
     * @return void
     */
    public function getIndex()
    {
        if(!I::can('view_notifications')){
            return Redirect::to(url("dashboard"));
        }
        $this->ormClass      = 'App\Notification';
        $this->search_fields = array('name');

        $notifications = $this->getList();

        if (Input::has('type_list')) {
            $notifications = $notifications->whereIn('type', explode(',', Input::get('type_list')));
        }

        $page = Input::get('page');
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $notifications = new LengthAwarePaginator($notifications->forPage($page, 15), $notifications->count(), 15, $page);

        return View::make('notification.list', [
            'notifications' => $notifications,
            'lists' => $this->getAllLists(),
        ]);
    }

    /**
     * Create a new email or SMS notification
     *
     * @return void
     */
    public function getNew()
    {
        if(!I::can('create_notifications')){
            return Redirect::to(url("dashboard"));
        }
        $notification = new Notification();

        $notification->name       = "New Notification";
        $notification->type       = ucfirst(self::sanitizeText(Input::get('type', 'Email')));
        $notification->created_by = Auth::User()->id;
        $notification->start_date = date("Y-m-d H:i", strtotime('+1 Day'));
        $notification->subject    = "New Notification";

        if($notification->type == 'Email') {
            $notification->body = View::make('notification.emails.base')->render();
        }

        $notification->save();

        return Redirect::to(url(
            "/dashboard/notifications/view/".base64_encode($notification->id)
        ));
    }

    /**
     * Display the notification
     *
     * @param string $enc_id
     * @return void
     */
    public function getView($enc_id = null)
    {
        if(!I::can('view_notifications')){
            return Redirect::to(url("dashboard"));
        }
        $notification = Notification::find(base64_decode($enc_id));

        if(!$notification) {
            \App::abort(404);
        }
        $managedPartners = Auth::User()->managedPartners();
        $can_view_notification = false;
        foreach($managedPartners as $manP){
            if($manP->id == $notification->partner_id){
                $can_view_notification = true;
                break;
            }
        }
        if($can_view_notification== false && $notification->draft == false){
            return Redirect::to(url("dashboard"));
        }
        $partner = Auth::User()->getTopLevelPartner()->id;
        return View::make("notification.view", array(
            'notification' => $notification,
            'partners'     => Auth::User()->managedPartners()->sortBy('name'),
            'segments'     => Auth::User()->managedSegments(),
            'tiers'     => Tier::where('draft', false)->whereHas('Partner', function($q) use($partner) {
                        $q->where('partner_id', $partner);
                    })->orderBy('name')->pluck('name', 'id')
        ));
    }

    /**
     * Delete the notification
     *
     * @param string $enc_id
     * @return void
     */
    public function getDelete($enc_id = null)
    {
        if(!I::can('delete_notifications')){
            return Redirect::to(url("dashboard"));
        }
        $notification = Notification::find(base64_decode($enc_id));

        if(!$notification) {
            \App::abort(404);
        }

        //AuditHelper::record(
        //    Auth::User()->firstPartner()->id, "Notification '{$notification->name}' deleted by '{Auth::User()->name}'"
        //);

        $notification->delete();

        return Redirect::to(url(
            "/dashboard/notifications"
        ));
    }

    /**
     * Handle the notification update
     *
     * @param string $enc_id
     * @return void
     */
    public function postUpdate($enc_id = null)
    {
        if(!I::can('edit_notifications') && !I::can('create_notifications')){
            return Redirect::to(url("dashboard"));
        }
        $notification = Notification::find(base64_decode($enc_id));

        if(!$notification) {
            \App::abort(404);
        }

        // Save this before validation
        $notification->body    = self::sanitizeText(Input::get('body'));
        $notification->subject = self::sanitizeText(Input::get('subject'));
        $notification->save();
        $input = Input::all();
        foreach($input as $k=>$i){
            $input[$k] = self::sanitizeText($i);
        }
        $validator = Validator::make($input, $this->getValidations());

        if($validator->fails()) {
            return Redirect::to("dashboard/notifications/view/{$enc_id}")->withErrors($validator)->withInput();
        }

        if(!$notification->draft) {
            //AuditHelper::record(
            //    Auth::User()->firstPartner()->id, "Notification '{$notification->name}' updated by '{Auth::User()->name}'"
            //);
        }

        $start_date = date("Y-m-d H:i", strtotime(self::sanitizeText(Input::get('start_date'))));

        $notification->name       = self::sanitizeText(Input::get('name'));
        $notification->partner_id = self::sanitizeText(Input::get('partner_id'));
        $notification->invoked    = true;
        $notification->draft      = false;
        $notification->start_date = $start_date;
        $notification->save();

        return Redirect::to(Input::get('redirect_to', 'dashboard/notifications'));
    }

     /**
     * Return the CSS needed for the email templates
     *
     * @param string $enc_id
     * @return Response
     */
    public function postTest($enc_id = null)
    {
        $notification = Notification::find(base64_decode($enc_id));

        if(!$notification) {
            \App::abort(404);
        }

        $partner = Partner::find(self::sanitizeText(Input::get('partner_id')));

        if(!$partner) {
            return Response::json(array(
                'failed'  => true,
                'message' => 'Please select a valid partner'
            ));
        }
        $respRalph = '';
        switch($notification->type) {
            case "Email":
                    $sendgrid = new SendGrid(
                        Config::get('sendgrid.username'), Config::get('sendgrid.password')
                    );

                    $mail = new SendGrid\Email();
                    $respRalph = '1';
                    $mail->addTo(Auth::User()->email);
                    $respRalph = '2';
                    $mail->setFrom(Config::get('blu.email_from'));
					$mail->setFromName('BLU Points');
                    $respRalph = '3';
                    $mail->setSubject("Preview: {$notification->name}");
                    $respRalph = '4';
                    $mail->setHtml(self::sanitizeText(Input::get('body')));
                    $respRalph = '5';

                    $sendgrid->send($mail);
                    $respRalph = '6';
                break;
            case "SMS":
                $respRalph = SMSHelper::send(self::sanitizeText(Input::get('body')), Auth::User(), $partner);
                break;
        }

        return Response::json(array(
            'failed' => false,
            'respRalph' => $respRalph
        ));
    }





    /**
     * Return the CSS needed for the email templates
     *
     * @param string $enc_id
     * @return Response
     */
    public function getTesting($enc_id = null,$partner_id)
    {
        $notification = Notification::find(base64_decode($enc_id));

        if(!$notification) {
            \App::abort(404);
        }

        $partner = Partner::find($partner_id);

        if(!$partner) {
            return Response::json(array(
                'failed'  => true,
                'message' => 'Please select a valid partner'
            ));
        }
        $ralphoutput = array();
        switch($notification->type) {
            case "Email":
                    $sendgrid = new SendGrid(
                        Config::get('sendgrid.username'), Config::get('sendgrid.password')
                    );

                    $mail = new SendGrid\Email();

                    $mail->addTo(Auth::User()->email);
                    $mail->setFrom(Config::get('blu.email_from'));
					$mail->setFromName('BLU Points');
                    $mail->setSubject("Preview: {$notification->name}");
                    $mail->setHtml(self::sanitizeText(Input::get('body')));

                    $sendgrid->send($mail);
                break;
            case "SMS":
                $ralphoutput[0] = SMSHelper::send('test this from notification controller', Auth::User()->normalized_mobile, $partner);
                break;
        }

        return Response::json(array(
            'failed' => false,
            'ralph' => $ralphoutput
        ));
    }
    /**
     * Link the segment to this notification
     *
     * @param string $enc_notification_id
     * @return void
     */
    public function postLink($enc_notification_id)
    {
        $notification = Notification::find(base64_decode($enc_notification_id));
        $segment = Segment::find(base64_decode(self::sanitizeText(Input::get('segment_id'))));

        if(!$notification || !$segment){
            \App::abort(404);
        }

        if(!$notification->segments->contains($segment)) {
            $mapping = new NotificationSegment();

            $mapping->notification_id = $notification->id;
            $mapping->segment_id      = $segment->id;
            $mapping->save();
        }

        //AuditHelper::record(
        //    Auth::User()->firstPartner()->id, "Notification '{$notification->name}' segment({$segment->name}) linked by '{Auth::User()->name}'"
        //);

        return $this->listNotificationSegments($notification->id);
    }

    /**
     * Unlink the segment to this notification
     *
     * @param string $enc_notification_id
     * @param strign $enc_segment_id
     * @return View
     */
    public function getUnlink($enc_notification_id, $enc_segment_id)
    {
        $notification_id = base64_decode($enc_notification_id);
        $segment_id      = base64_decode($enc_segment_id);

        $mapping = NotificationSegment::where('notification_id', $notification_id)->where('segment_id', $segment_id);
        $mapping->delete();

        // Get list
        $notification = Notification::find($notification_id);
        $segment      = Segment::find($segment_id);

        if(!$notification || !$segment) {
            \App::abort(404);
        }

        if($segment) {
            //AuditHelper::record(
            //    Auth::User()->firstPartner()->id, "Notification '{$notification->name}' segment({$segment->name}) unlinked by '{Auth::User()->name}'"
            //);
        }

        return $this->listNotificationSegments($notification->id);
    }

    /**
     * List the segments that the notification belongs to
     *
     * @return View
     */
    private function listNotificationSegments($notification_id)
    {
        $notification = Notification::find($notification_id);

        return View::make("notification.segments_list", array(
            'notification' => $notification
        ));
    }

    /**
     * Link the user to this notification
     *
     * @param string $enc_notification_id
     * @return void
     */
    public function postLinkuser($enc_notification_id)
    {
        $notification = Notification::find(base64_decode($enc_notification_id));
        $user = User::find(self::sanitizeText(Input::get('user_id')));

        if(!$notification || !$user){
            \App::abort(404);
        }

        if(!$notification->users->contains($user)) {
            $mapping = new NotificationUser();

            $mapping->notification_id = $notification->id;
            $mapping->user_id      = $user->id;
            $mapping->save();
        }

        //AuditHelper::record(
        //    Auth::User()->firstPartner()->id, "Notification '{$notification->name}' User({$user->name}) linked by '{Auth::User()->name}'"
        //);

        return $this->listNotificationUsers($notification->id);
    }

    /**
     * Unlink the user to this notification
     *
     * @param string $enc_notification_id
     * @param strign $enc_user_id
     * @return View
     */
    public function getUnlinkuser($enc_notification_id, $enc_user_id)
    {
        $notification_id = base64_decode($enc_notification_id);
        $user_id      = base64_decode($enc_user_id);

        $mapping = NotificationUser::where('notification_id', $notification_id)->where('user_id', $user_id);
        $mapping->delete();

        // Get list
        $notification = Notification::find($notification_id);
        $user     = User::find($user_id);

        if(!$notification || !$user) {
            \App::abort(404);
        }

        if($user) {
            //AuditHelper::record(
            //    Auth::User()->firstPartner()->id, "Notification '{$notification->name}' User ({$user->name}) unlinked by '{Auth::User()->name}'"
            //);
        }

        return $this->listNotificationUsers($notification->id);
    }

    /**
     * List the users that the notification belongs to
     *
     * @return View
     */
    private function listNotificationUsers($notification_id)
    {
        $notification = Notification::find($notification_id);

        return View::make("notification.users_list", array(
            'notification' => $notification
        ));
    }

     /**
     * Return the CSS needed for the email templates
     *
     * @param string $enc_id
     * @return Response
     */
    public function postSend($enc_id = null)
    {
        $notification = Notification::find(base64_decode($enc_id));

        if(!$notification) {
            \App::abort(404);
        }

        $targets = array();

        foreach ($notification->segments as $segment) {
                foreach($segment->users as $u) {

                        if($notification->type == 'Email') {
                                if(!in_array($u->email, $targets)) {
                                        $targets[] = $u->email;
                                }
                        }

                        if($notification->type == 'SMS') {
                                $number = $u->normalizedContactNumber();

                                if(!in_array($number, $targets)) {
                                        $targets[] = $u;
                                }
                        }

                }
        }

        foreach ($notification->users as $user) {
            if($notification->type == 'Email') {
                    if(!in_array($user->email, $targets)) {
                            $targets[] = $user->email;
                    }
            }

            if($notification->type == 'SMS') {
                    $number = $user->normalizedContactNumber();

                    if(!in_array($number, $targets)) {
                            $targets[] = $user;
                    }
            }
        }

        $partner = $notification->partner;
        $parent  = $partner->topLevelPartner();

        if(!$partner) {
            return Response::json(array(
                'failed'  => true,
                'message' => 'Please select a valid partner'
            ));
        }
        $ralphoutput = array();
        if($partner) {
            switch($notification->type) {
                    case "Email":
                        $sendgrid = new SendGrid(
                            Config::get('sendgrid.username'), Config::get('sendgrid.password')
                        );

                        $mail = new SendGrid\Email();

                        $mail->setTos($targets);
                        $mail->setFrom(Config::get('blu.email_from'));
                                            $mail->setFromName('BLU Points');
                        $mail->setSubject($notification->subject);
                        $mail->setHtml($notification->body);

                        $sendgrid->send($mail);
                        break;
                    case "SMS":
                        foreach($targets as $t) {
                            if($t === reset($targets)){
                                $notification->sent  = true;
                                $notification->save();
                            }
                            $balance   = User::find($t->id)->balance();
                            $smsString = $notification->body;
                            $smsString = str_replace("{title}", $t->title, $smsString);
                            $smsString = str_replace("{first_name}", $t->first_name, $smsString);
                            $smsString = str_replace("{last_name}", $t->last_name, $smsString);
                            $smsString = str_replace("{pin_code}", $t->passcode, $smsString);
                            $smsString = str_replace("{balance}", $balance, $smsString);
                            $smsString = str_ireplace('{amount}', $t->amt_redeem, $smsString);
                            $smsString = str_ireplace('{date}', Carbon::parse($t->created_at)->format('Y-m-d'), $smsString);
                            $smsString = str_ireplace('{time}', Carbon::parse($t->created_at)->format('H:i:s'), $smsString);
                            $ralphoutput[] = SMSHelper::send(htmlentities($smsString), $t, $partner);
                        }
                        break;
            }
        }
        $notification->count = count($targets);
        $notification->sent  = true;
        $notification->save();

        return Response::json(array(
            'failed' => false,
            'ralph' => $ralphoutput
        ));
    }

    /**
     * Link the tier to this notification
     *
     * @param string $enc_notification_id
     * @return void
     */
    public function postLinktier($enc_notification_id)
    {
        $notification = Notification::find(base64_decode($enc_notification_id));

        if(!$notification){
            \App::abort(404);
        }

        $tier = Tier::find(self::sanitizeText(Input::get('tier_id')));

        if(!$notification->users->contains($tier)) {
            $mapping = new NotificationUser();

            $mapping->notification_id = $notification->id;
            $mapping->tier_id      = $tier->id;
            $mapping->save();
        }

        //AuditHelper::record(
        //    Auth::User()->firstPartner()->id, "Notification '{$notification->name}' Tier({$tier->name}) linked by '{Auth::User()->name}'"
        //);

        return $this->listNotificationTiers($notification->id);
    }

    /**
     * Unlink the tier to this notification
     *
     * @param string $enc_notification_id
     * @param strign $enc_tier_id
     * @return View
     */
    public function getUnlinktier($enc_notification_id, $enc_tier_id)
    {
        $notification_id = base64_decode($enc_notification_id);
        $tier_id      = base64_decode($enc_tier_id);

        $mapping = NotificationTier::where('notification_id', $notification_id)->where('tier_id', $tier_id);
        $mapping->delete();

        // Get list
        $notification = Notification::find($notification_id);
        $tier     = Tier::find($tier_id);

        if(!$notification) {
            \App::abort(404);
        }

        if($tier) {
            //AuditHelper::record(
            //    Auth::User()->firstPartner()->id, "Notification '{$notification->name}' Tier ({$tier->name}) unlinked by '{Auth::User()->name}'"
            //);
        }

        return $this->listNotificationiTers($notification->id);
    }

    /**
     * List the tiers that the notification belongs to
     *
     * @return View
     */
    private function listNotificationiTers($notification_id)
    {
        $notification = Notification::find($notification_id);

        return View::make("notification.tiers_list", array(
            'notification' => $notification
        ));
    }

    public static function sendEmailNotification($template, $from_name, $from_email, $to_name, $to_email, $subject, $attachment_name = null, $attachment_file = null) {
        $to_name_arr	= array();
		$to_email_arr	= array();

		if (!is_array($to_name)) {
			$to_name_arr = array($to_name);
		}
		else {
			$to_name_arr = $to_name;
		}

		if (!is_array($to_email)) {
			$to_email_arr = array($to_email);
		}
		else {
			$to_email_arr = $to_email;
		}

        $apiKey		= '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';
		$mail		= new SendGrid\Mail();
		$from		= new SendGrid\Email($from_name, $from_email);
		$to			= new SendGrid\Email($to_name_arr[0], $to_email_arr[0]);
		$content	= new SendGrid\Content("text/html", $template);

		$mail		= new SendGrid\Mail($from, $subject, $to, $content);

        if (is_array($to_email_arr) && count($to_email_arr) > 1) {
			$personalization = new SendGrid\Personalization();

			foreach (array_slice($to_email_arr, 1) AS $to) {
				$toEmail = new SendGrid\Email(null, $to);
				$personalization->addTo($toEmail);
			}
			$mail->addPersonalization($personalization);
		}


		if (!empty($attachment_file)) {
			$file_encoded	= base64_encode(file_get_contents($attachment_file));
			$attachment		= new SendGrid\Attachment();

			$attachment->setContent($file_encoded);
//			$attachment->setType("application/pdf");
			$attachment->setDisposition("attachment");
			$attachment->setFilename($attachment_name);
		}

		$sg = new SendGrid($apiKey);
		$response = $sg->client->mail()->send()->post($mail);

		return $response;
	}

} // EOC