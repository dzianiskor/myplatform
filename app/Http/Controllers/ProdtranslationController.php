<?php
namespace App\Http\Controllers;
use App\Prodtranslation as Prodtranslation;
use View;
use App\Language as Language;
use Validator;
use Input;
use Response;

/**
 * Product Translation Controller
 * 
 * @category   Controllers
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class ProdtranslationController extends BaseController
{
    /**
     * Fetch the select drop down data for the prodtranslation popup
     *
     * @return array
     */
    private function getSelectData()
    {
        return array(
            'languages'  => Language::orderBy('name')->pluck('name','id')
            );
    }

    /**
     * Create and return a default draft prodtranslation
     *
     * @param int $product_id
     * @return Result
     */
    private function getProdtranslationDraft($product_id)
    {
        $prodtranslation = new Prodtranslation();

        $prodtranslation->name       = 'New Prodtranslation';
        $prodtranslation->product_id = $product_id;

        $prodtranslation->save();

        return $prodtranslation;
    }

    /**
     * Display the new prodtranslation page
     *
     * @return View
     */
    public function newProdtranslation($product_id = null)
    {
        $data = array(
//            'prodtranslation'   => $this->getProdtranslationDraft($product_id)
            'product_id' => $product_id
        );

        return View::make("prodtranslation.new", array_merge(
            $data, $this->getSelectData()
        ));
    }
    
    /**
     * Display the edit prodtranslation page
     *
     * @return View
     */
    public function editProdtranslation($prodtranslation_id = null)
    {
        $data = array(
            'prodtranslation' => Prodtranslation::find($prodtranslation_id)
        );

        return View::make("prodtranslation.form", array_merge(
            $data, $this->getSelectData()
        ));
    }

    /**
     * Create a new prodtranslation resource
     *
     * @param int $prodtranslation_id
     * @return void
     */
    public function updateProdtranslation($prodtranslation_id = null)
    {
        $productTranslation = \App\Prodtranslation::find($prodtranslation_id);

        if (!$productTranslation) {
            \App::abort(404);
        }

        $rules = array(
            'prodtranslation_name' => 'required|min:1'
        );
        $input = Input::all();
        foreach($input as $k=>$i){
            $input[$k] = self::sanitizeText($i);
        }
        $validator = Validator::make($input, $rules);

        if($validator->fails()) {
            return Response::json(array(
                'failed' => true,
                'errors' => "Please ensure that you provide a valid prodtranslation name"
            ));
        }

        $prodtranslation = Prodtranslation::find($prodtranslation_id);

        // Details
        $prodtranslation->name       = self::sanitizeText(Input::get('prodtranslation_name'));
        $prodtranslation->model = self::sanitizeText(Input::get('prodtranslation_model'));
        $prodtranslation->sub_model        = self::sanitizeText(Input::get('prodtranslation_sub_model'));
        $prodtranslation->description        = self::sanitizeText(Input::get('prodtranslation_description'));
        $prodtranslation->draft      = false;
        $prodtranslation->lang_id        = self::sanitizeText(Input::get('prodtranslation_lang'));
        

        $prodtranslation->push();

        return Response::json(array(
            'failed' => false,
            'view_url' => url('dashboard/prodtranslation/list/'.$prodtranslation->product_id)
        ));
    }
    
    /**
     * Create a new prodtranslation resource
     *
     * @param int $prodtranslation_id
     * @return void
     */
    public function createProdtranslation($prodtranslation_id = null)
    {
        $rules = array(
            'prodtranslation_name' => 'required|min:1'
        );
        $input = Input::all();
        foreach($input as $k=>$i){
            $input[$k] = self::sanitizeText($i);
        }
        $validator = Validator::make($input, $rules);

        if($validator->fails()) {
            return Response::json(array(
                'failed' => true,
                'errors' => "Please ensure that you provide a valid prodtranslation name"
            ));
        }

        $product_id = self::sanitizeText(Input::get('prodtranslation_pid'));
        $product = \App\Product::find($product_id);

        if (!$product) {
            \App::abort(404);
        }

        $prodtranslation = new Prodtranslation();
        // Details
        $prodtranslation->product_id  = self::sanitizeText(Input::get('prodtranslation_pid'));
        $prodtranslation->name        = self::sanitizeText(Input::get('prodtranslation_name'));
        $prodtranslation->model       = self::sanitizeText(Input::get('prodtranslation_model'));
        $prodtranslation->sub_model   = self::sanitizeText(Input::get('prodtranslation_sub_model'));
        $prodtranslation->description = self::sanitizeText(Input::get('prodtranslation_description'));
        $prodtranslation->draft       = false;
        $prodtranslation->lang_id     = self::sanitizeText(Input::get('prodtranslation_lang'));

        $prodtranslation->push();

        return Response::json(array(
            'failed' => false,
            'view_url' => url('dashboard/prodtranslation/list/'.$prodtranslation->product_id)
        ));
    }

    /**
     * Delete the specified prodtranslation
     *
     * @return View
     */
    public function deleteProdtranslation($prodtranslation_id = null)
    {
        $productTranslation = \App\Prodtranslation::find($prodtranslation_id);

        if (!$productTranslation) {
            \App::abort(404);
        }

        Prodtranslation::find($prodtranslation_id)->delete();

        return Response::json(array(
            'failed' => false
        ));
    }

    /**
     * Render a list of prodtranslations relevant to the Partner
     *
     * @param int $product_id
     * @return View
     */
    public function listProdtranslations($product_id = null)
    {
        $data = array(
            'prodtranslations' => Prodtranslation::where('product_id', $product_id)->where('draft', false)->get()
        );

        return View::make("prodtranslation.list", $data);
    }

} // EOC