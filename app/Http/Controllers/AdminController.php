<?php
namespace App\Http\Controllers;
use App\Media;
use Illuminate\Support\Facades\App;
use View;
use Input;
use Validator;
use Session;
use I;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use ManagedObjectsHelper;
use App\PasswordHistory as PasswordHistory;
use AuditHelper;
use App\Country;
use App\Admin as Admin;
use App\Role as Role;
use App\City as City;
use App\Area as Area;
use Meta;
use App\Http\Requests\AdminRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Response;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Admin Controller
 *
 * @category   Controllers
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class AdminController extends BaseController
{
    protected $layout = "layouts.dashboard";

    private $auth_user = null;

    public function __construct()
    {
        parent::__construct();

        $this->auth_user = Auth::User();
    }

    /**
     * Fetch the data for the partner view
     *
     * @return array
     */
    private function getData()
    {
        $admins = array();
        foreach (Auth::User()->managedAdmins() as $admin) {
            if (!empty($admin->first_name) || !empty($admin->last_name)) {
                $admins[$admin->id] = $admin->first_name . ' ' . $admin->last_name;
            }
        }
        asort($admins);
        $admins = array(0 => 'Select One') + $admins;

        return array(
            'countries' => Country::orderBy('name')->pluck('name', 'id'),
            'cities' => City::orderBy('name')->pluck('name', 'id'),
            'areas' => Area::orderBy('name')->pluck('name', 'id'),
            'statuses' => Meta::memberStatuses(),
            'managed_admins' => $admins,
            'languages' => \App\Language::orderBy('name')->pluck('name', 'short_code')
        );
    }

    /**
     * Get all list data
     *
     * @return array
     */
    private function getAllLists()
    {
        $roles = Auth::User()->availableRoles()->sortBy('name')->pluck('name', 'id')->toArray();
        return [
            'partners'  => ManagedObjectsHelper::managedPartners()->sortBy('name')->pluck('name', 'id'),
            'roles' => $roles,
        ];
    }

    /**
     * Verify that the values passed are unique
     *
     * @param string $field
     * @param string $value
     * @param int $exclusion_id
     * @return Response
     */
    public function verifyUniqueField($field = null, $value = null, $exclusion_id = null)
    {
        if(!in_array($field, array('email', 'username', 'mobile'))) {
            App::abort(400);
        }

        $row = DB::table('users')->where($field, '=', $value)->first();

        if(!$row) {
            return Response::json(array('unique' => true));
        }

        if($row->id == $exclusion_id) {
            return Response::json(array('unique' => true));
        } else {
            return Response::json(array('unique' => false));
        }
    }

    /**
     * List all admins
     *
     * @return void
     */
    public function listAdmins()
    {
        if(!I::can('view_admins')){
            return Redirect::to(url("dashboard"));
        }

        $admins = Admin::where('draft', false)->orderBy('created_at');

        if (Input::has('q') && Input::get('q') != '') {
            $admins = $admins->where('first_name', 'like', '%' . self::sanitizeText(Input::get('q')) . '%')->orWhere('last_name', 'LIKE', '%' . self::sanitizeText(Input::get('q')) . '%');
        }

        //We show only admins who belong to partners managed by authenticated user
        if (! I::am('BLU Admin')) {
            $admins->whereHas('partners', function ($query) {
                $query->whereIn('partner_id', Auth::user()->managedPartners()->pluck('id'));
            });
        }

        if (Input::has('partner_list') && Input::has('partner_list') != '') {
            $partnerIds = explode(',', Input::get('partner_list'));
            $admins = $admins->whereHas('partners', function ($u) use ($partnerIds) {
                $u->whereIn('partner_id', $partnerIds);
            });
        }

        if (Input::has('role_list') && Input::has('role_list') != '') {
            $roleIds = explode(',', Input::get('role_list'));
            $admins = $admins->whereHas('roles', function ($u) use ($roleIds) {
                $u->whereIn('role_id', $roleIds);
            });
        }

        $admins = $admins->paginate(15);

        return View::make('admin.list', array(
            'admins' => $admins->appends(Input::except('page')),
            'lists' => $this->getAllLists(),
        ));
    }

    /**
     * Create a new admin draft and redirect to it
     *
     * @return Response
     */
    public function newAdmin()
    {
        if(!I::can('create_admins')){
            return Redirect::to(url("dashboard"));
        }
        $user = Admin::createNewUser();
        $userId = base64_encode($user->id);
        return Redirect::to(url("dashboard/admins/view/{$userId}"));
    }

    /**
     * Display the admin view page
     *
     * @param int $id
     * @return View
     */
    public function viewAdmin($enc_id = null)
    {
        $id = base64_decode($enc_id);
        if(!I::can('view_admins')){
            return Redirect::to(url("dashboard"));
        }
        $admin = Admin::find($id);

        if(!$admin) {
            App::abort(404);
        }
        $bool_ismember_partner = false;
        if(I::am("BLU Admin")){
            $bool_ismember_partner = true;
        }
        $mem_partners = $admin->partners()->get();

        foreach($mem_partners as $mem_part){
            if(in_array($mem_part->id,Auth::User()->managedPartnerIDs())){
                $bool_ismember_partner = true;
                break;
            }
        }

        if($bool_ismember_partner == false){
            return Redirect::to(url("dashboard"));
        }

        $roles   = Session::get('user_role_ids');
        $role_id = min($roles);
        $roles   = Role::where('id', '>=', $role_id)->orderBy('name')->pluck('name', 'id');

        $view_data = array(
            'admin'          => $admin,
            'roles'           => $roles,
            'partners'        => ManagedObjectsHelper::managedPartners()->sortBy('name')->pluck('name', 'id'),
            'managedPartners' => Auth::User()->managedParentPartnerIDs()
        );

        return View::make('admin.view', array_merge($view_data, $this->getData()));
    }

    /**
     * Update a admin account
     *
     * @param int $id
     * @return Response
     */
    public function updateAdmin(AdminRequest $request, $enc_id = null)
    {
        $id = base64_decode($enc_id);
        if(!I::can('edit_admins')){
            return Redirect::to(url("dashboard"));
        }
        $user = Admin::find($id);

        if(!$user) {
            App::abort(404);
        }
        $input = Input::all();
        foreach($input as $k=>$i){
            $input[$k] = self::sanitizeText($i);
        }

        $rules = $request->rules();
        $rules['email'] = 'required|email|unique:admins,email,' . $id;
        $messages = [
            'password.regex' => 'You need to have 8 minimum characters, 1 special symbol (#?!@$%^&*-), and 1 letter in uppercase in your password'
        ];

        $validator = Validator::make($input, $rules, $messages);

        if($validator->fails()) {
            return Redirect::to(url("dashboard/admins/view/$enc_id"))->withErrors($validator);
        }
        // Format DOB
        $dob = strtotime(
            self::sanitizeText(Input::get('day')).'-'.self::sanitizeText(Input::get('month')).'-'.self::sanitizeText(Input::get('year'))
        );

        if(!$user->draft) {
            $auth_user = Auth::User();

            //AuditHelper::record(
            //    Auth::User()->firstPartner()->id, "Admin '{$user->id} / {$user->email}' profile updated by '{Auth::User()->name}'"
            //);                              
        }

        $user->parent_id      = self::sanitizeText(Input::get('parent_id'));
        $user->status         = self::sanitizeText(Input::get('status'));
        $user->title          = self::sanitizeText(Input::get('title'));
        $user->first_name     = self::sanitizeText(Input::get('first_name'));
        $user->middle_name    = self::sanitizeText(Input::get('middle_name'));
        $user->last_name      = self::sanitizeText(Input::get('last_name'));
        $user->email          = self::sanitizeText(Input::get('email'));
        $user->gender         = self::sanitizeText(Input::get('gender'));
        $user->tag            = self::sanitizeText(Input::get('tag'));
        $user->country_id     = self::sanitizeText(Input::get('country_id'));
        $user->area_id        = self::sanitizeText(Input::get('area_id'));
        $user->city_id        = self::sanitizeText(Input::get('city_id'));
        $user->dob            = date("Y-m-d H:i:s", $dob);
        $user->pref_lang      = self::sanitizeText(Input::get('pref_lang'));


        if(Input::hasFile('image')) {
            $file = Input::file('image');
            $path = public_path('uploads');
            $ext  = strtolower($file->getClientOriginalExtension());
            $size = $file->getSize();
            $type = $file->getMimeType();
            $name = md5(time()).".{$ext}";

            if(in_array($ext, Meta::allowedImageExts() )) {
                Input::file('image')->move($path, $name);

                $f = new Media();
                $f->name = $name;
                $f->size = $size;
                $f->type = $type;
                $f->ext  = $ext;
                $f->save();

                $user->profile_image = $f->id;
            }
        }
        $user->draft = '0';
        if (Input::has('username')) {
            $user->username = self::sanitizeText(Input::get('username'));
        }

        if(Input::has('password')) {
            $user->password = Hash::make(self::sanitizeText(Input::get('password')));

            if(PasswordHistory::isEqualOldPassowrds(Input::get('password'), $user->id)) {
                return Redirect::to(url("dashboard/admins/view/$enc_id"))->withErrors('Password should not equal any of the last 4 passwords');
            }
            
        }
         if(Input::has('static_ip')) {
             $user->static_ip = self::sanitizeText(Input::get('static_ip'));
         }

        $user->save();

        PasswordHistory::savePassword($user);
        
        return Redirect::to(Input::get('redirect_to', 'dashboard/admins'));
    }

    /**
     * Delete a admin account
     *
     * @param int $id
     * @return Response
     */
    public function deleteAdmin($enc_id = null)
    {
        $id = base64_decode($enc_id);
        if(!I::can('delete_admins')){
            return Redirect::to(url("dashboard"));
        }
        if (!I::am('BLU Admin')) {
            App::abort(400);
        }
        $user = Admin::find($id);

        if(!$user) {
            App::abort(404);
        }

        Admin::find($id)->delete();

        return Redirect::to('dashboard/admins');
    }


    /**
     * Update admin preffered language
     *
     * @param int $userId
     * @param string $lang
     * 
     * @return Response
     */
    public function updatePrefLang($enc_userId = null)
    {
        $userId = $enc_userId;
        $lang = self::sanitizeText(Input::get('language', 'en'));
        $user = Admin::find($userId);

        if(!$user) {
            App::abort(404);
        }
        
        $user->pref_lang      = $lang;
        $user->save();

        return Redirect::back()->with('message','Preferred language changed.');
    }

} // EOC