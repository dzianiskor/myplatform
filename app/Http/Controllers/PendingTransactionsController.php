<?php
namespace App\Http\Controllers;
/**
 * Carrier Controller
 *
 * @category   Controllers
 * @package    BLU
 * @author     Magid Mroueh
 * @copyright  2016 BLU
 * @link       http://blupoints.com
 */
class PendingTrnsactionsController extends BaseController
{
    protected $layout = "layouts.dashboard";

	/**
     * create new record in pending_transactions
     *
     * @return Response
     */
    public static function createPendingTransaction()
    {
//        if(!I::can('create_currencies')){
//            return Redirect::to(url("dashboard"));
//        }

//        $pendingTransaction				= new PendingTransactions();
//        $currency->serialized_data		= self::sanitizeText(Input::get('serialized_data'));
//        $currency->type					= self::sanitizeText(Input::get('type'));
//        $currency->status				= self::sanitizeText(Input::get('status'));
//        $currency->return_url			= self::sanitizeText(Input::get('return_url'));
//        $currency->save();

		$pendingTransaction						= array();
		$pendingTransaction['serialized_data']	= self::sanitizeText(Input::get('serialized_data'));
		$pendingTransaction['type']				= self::sanitizeText(Input::get('type'));
		$pendingTransaction['status']			= self::sanitizeText(Input::get('status'));
		$pendingTransaction['return_url']		= self::sanitizeText(Input::get('return_url'));
		$pendingTransaction['created_at']		= date();
		$pendingTransaction['updated_at']		= date();

		$result									= DB::table('pending_transactions')->insert($pendingTransaction);

		return $result;

    }


	/**
     * create new record in pending_transactions
     *
     * @return Response
     */
    public static function changePendingTransactionStatus( $pendingTransactionId = 0, $newStatus = null )
    {
		$result	= DB::table('pending_transactions')->where('id', $pendingTransactionId)->update(array('status' => $newStatus, 'updated_at' => date()));

		return $result;
    }


} // EOC