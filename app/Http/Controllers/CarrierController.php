<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\App;
use View;
use Input;
use Validator;
use Illuminate\Support\Facades\Redirect;
use App\Carrier as Carrier;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Carrier Controller
 * 
 * @category   Controllers
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class CarrierController extends BaseController
{
    protected $layout = "layouts.dashboard";

    /**
     * generate a reusable array with all the validations needed
     * for the Carriers screen
     *
     * @return array
     */
    private function getValidations() {
        $rules = array(
            'name'           => 'required',
//            'iata_code' => 'required|min:2|max:2',
//            'icao_code' => 'required|min:3|max:3',
            'country' => 'required'
        );

        return $rules;
    }

    /**
     * Display a list of the Carriers
     *
     * @return void
     */
    public function listCarriers()
    {
        $this->ormClass      = 'App\Carrier';
        $this->search_fields = array('name');

        $carriers = $this->getList();

        $page = Input::get('page');
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $carriers = new LengthAwarePaginator($carriers->forPage($page, 15), $carriers->count(), 15, $page);

        return View::make("carrier.list", array(
            'carriers' => $carriers
        ));

    }

    /**
     * get fields for new Carriers
     *
     * @return void
     */
    public function newCarrier()
    {
        return View::make("carrier.new");
    }

    /**
     * get fields for new Carrier
     *
     * @return void
     */
    public function createNewCarrier()
    {
        $input = Input::all();
        foreach($input as $k=>$i){
            $input[$k] = self::sanitizeText($i);
        }
        $validator = Validator::make($input, $this->getValidations());

        if($validator->fails()) {
            return Redirect::to('dashboard/carriers/new')->withErrors($validator)->withInput();
        }

        Carrier::create($input);
        return Redirect::to('dashboard/carriers');
    }

    /**
     * View a single Carrier entry
     *
     * @param int $id
     * @return void
     */
    public function viewCarrier($id = null)
    {
        $carrier = Carrier::find($id);

        if(!$carrier) {
            App::abort(404);
        }

        return View::make("carrier.view", array(
            'carrier' => $carrier
        ));
    }

    /**
     * Delete a carrier instance
     *
     * @param int $id
     * @return void
     */
    public function deleteCarrier($id = null)
    {
        $carrier = Carrier::find($id);

        if(!$carrier) {
            App::abort(404);
        }

        $carrier->delete();

        return Redirect::to('dashboard/carriers');
    }

    /**
     * update a carrier instance
     *
     * @param int $id
     * @return void
     */
    public function updateCarrier($id = null)
    {
        return View::make("carrier.view", array(
            'id' => $id
        ));
    }

    /**
     * update a carrier instance
     *
     * @param int $id
     * @return void
     */
    public function saveCarrier($id = null)
    {
        $carrier = Carrier::find($id);

        if(!$carrier) {
            App::abort(404);
        }
        $input = Input::all();
        foreach($input as $k=>$i){
            $input[$k] = self::sanitizeText($i);
        }
        $validator = Validator::make($input, $this->getValidations());

        if($validator->fails()) {
            return Redirect::to('dashboard/carriers/view/' . $id)->withErrors($validator)->withInput();
        }

        $carrier->update($input);

        return Redirect::to(Input::get('redirect_to', 'dashboard/carriers'));
    }

} // EOC