<?php
namespace App\Http\Controllers;
use View;
use Illuminate\Support\Facades\Redirect;

use ManagedObjectsHelper;

/**
 * Reward Types Controller
 *
 * @category   Controllers
 * @package    BLU
 * @author     Ralph Nader <ralph@bluloyalty.com>
 * @copyright  2015 BLU
 * @link       http://blupoints.com
 */
class RewardTypesController extends BaseController {

    protected $layout = "layouts.dashboard";

    private function getValidations() {
        return array(
            'name' => 'required'
        );
    }

    /**
     * Get all list data
     *
     * @return array
     */
    private function getAllLists() {
        return array(
            'partners' => ManagedObjectsHelper::managedPartners()->sortBy('name')->pluck('name', 'id'),
        );
    }

    /**
     * Display a list of the currencies
     *
     * @return void
     */
    public function listRewardTypes() {
        return View::make("rewardtypes.list", array(
                    'rewardtypes' => '' //$this->getList()
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getCreate() {
        return Redirect::to(url("dashboard/rewardtypes/edit/0"));
    }

    /**
     * Show the currency edit page
     *
     * @param integer $currency_id
     * @return Response
     */
    public function getEdit($id1) {
        return View::make("rewardtypes.view");
    }

    /**
     * Delete the currency
     *
     * @param integer $currency_id
     * @return void
     */
    public function getDelete($id1) {
        return Redirect::to(url("dashboard/rewardtypes"));
    }

    /**
     * apply changes
     *
     * @param integer $currency_id
     * @return Response
     */
    public function postUpdate($id1) {
        return Redirect::to(url("dashboard/rewardtypes"));
    }

}

// EOC