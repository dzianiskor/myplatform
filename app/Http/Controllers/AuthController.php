<?php

/**
 * Authentication Controller
 * 
 * @category   Controllers
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
namespace App\Http\Controllers;
use App\Admin;
use View;
use Input;
use Validator;
use Session;
use Security;
use I;
use Illuminate\Support\Facades\Auth;
use BluCollection;
use AuditHelper;
use Illuminate\Support\Facades\Redirect;
class AuthController extends BaseController
{
    protected $layout = "layouts.empty";

    /**
     * Display the login page
     *
     * @return void
     */
    public function showLogin() 
    {
		$data = array(
			'body_class' =>'login'
		);
		
        return View::make("auth.login", $data);
    }

    /**
     * Perform the authentication request
     *
     * @return void
     */
    public function doLogin()
    {
        $rules = array(
            "username" => "required",
            "password" => "required"
        );

        $input = Input::all();
        foreach($input as $k=>$i){
            $input[$k] = self::sanitizeText($i);
        }
        $validator = Validator::make($input, $rules);

        if($validator->fails()) {
            return Redirect::to("auth/login")->withErrors($validator);
        }

        $credentials = Input::only("username", "password");
        /*if(Input::has('g-recaptcha-response')){
            $recaptcha_response = self::sanitizeText(Input::get('g-recaptcha-response'));
            $url = "https://www.google.com/recaptcha/api/siteverify";
            $post_fields="secret=6LcBXCEUAAAAANmYLZLKtjg46qqonWqQHm5MP_mG&response=".$recaptcha_response;
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL,$url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS,$post_fields);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $responseServer = curl_exec ($ch);
            curl_close ($ch);
            $json_responseServer = json_decode($responseServer);
            if(!$json_responseServer->success == TRUE){
                return Redirect::to("auth/login")->with(
                    "flash_error", "Access Denied!"
                );	
            }
            
        }
        else{
            if(!empty(Session::get('flash_error'))){
                return Redirect::to("auth/login")->with(
                        "flash_error", "Access Denied!"
                    );
            }
        }*/

        if(Auth::attempt($credentials)) {
            Security::configureRoles();
			
			if(I::am('Member')) {
				
				Auth::logout();
				AuditHelper::record(Auth::User()->managedPartners()->first()->id, json_encode($credentials['username']) . " failed to login at " . date('Y-m-d H:i:s')." as this user is a member and not admin");
                            return Redirect::to("auth/login")->with(
                                "flash_error", "Access Denied!"
                            );				
			}
                        else {
                            $isPartnerStaticIp = Auth::User()->managedPartners()->first()->enforce_static_ip;                            
                            if($isPartnerStaticIp){                                
                                $ipAddress = '';
                                // Check for X-Forwarded-For headers and use those if found
                                if (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && ('' !== trim($_SERVER['HTTP_X_FORWARDED_FOR']))) {
                                    $ipAddress = trim($_SERVER['HTTP_X_FORWARDED_FOR']);
                                } else {
                                    if (isset($_SERVER['REMOTE_ADDR']) && ('' !== trim($_SERVER['REMOTE_ADDR']))) {
                                        $ipAddress = trim($_SERVER['REMOTE_ADDR']);
                                    }
                                }
                                $adminStaticIP = Auth::User()->static_ip;
//                                if(isset($adminStaticIP) && !empty($adminStaticIP) ){
//                                    $subject ="IPADRESS ".$ipAddress." admin IP = ".$adminStaticIP;
//                                    $body ="IPADRESS = ".$ipAddress." admin IP = ".$adminStaticIP;
//                                }else{
//                                    $subject ="IPADRESS ".$ipAddress;
//                                    $body ="IPADRESS = ".$ipAddress;                               
//                                }
//                                $to="firas.boukarroum@bluloyalty.com";
//                                $mail = new SendGrid\Email();
//                                $mail->addTo($to);
//                                $mail->setFrom(Config::get('blu.email_from'));
//                                $mail->setFromName('BLU Points');
//                                $mail->setSubject($subject);
//                                $mail->setText($body);
//                                $userName = Config::get('sendgrid.username');
//                                $password = Config::get('sendgrid.password');
//                                $sendGrid = new SendGrid($userName, $password);
//                                $sendGrid->send($mail);
                                
                                if(isset($adminStaticIP) && !empty($adminStaticIP) ){
                                    if(trim($ipAddress) != trim($adminStaticIP) ){
                                        AuditHelper::record(Auth::User()->managedPartners()->first()->id, json_encode($credentials['username']) . " failed to login at " . date('Y-m-d H:i:s')." since his ip ".$ipAddress."is different from the admin static ip ".$adminStaticIP);
                                        Auth::logout();
                                        return Redirect::to("auth/login")->with(
                                            "flash_error", "Access Denied!"
                                        );	
                                    } 
                                }
                            }
                            
                            AuditHelper::record(Auth::User()->managedPartners()->first()->id, json_encode($credentials['username']) . " Logged in at " . date('Y-m-d H:i:s'));
                            return Redirect::intended("dashboard");
			}
        }

        // Verify partner ID
        $partner_id = 1;

        AuditHelper::record($partner_id, " User with username = ".json_encode($credentials['username']) . " failed to login at " . date('Y-m-d H:i:s'));	
        return Redirect::to("auth/login")->with(
            "flash_error", "Username or Password is wrong"
        );
    }

    /**
     * Sign the user out of the session
     *
     * @return void
     */
    public function doSignout()
    {
        Auth::logout();

        return Redirect::to("auth/login");
    }

} // EOC