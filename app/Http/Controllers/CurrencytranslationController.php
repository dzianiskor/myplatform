<?php
namespace App\Http\Controllers;
use App\Language;
use App\CurrencyTranslation as CurrencyTranslation;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Validator;
use View;

/**
 * Category Translation Controller
 * 
 * @category   Controllers
 * @package    BLU
 * @author     Magid Mroueh <magid.mroueh@bluloyalty.com>
 * @copyright  2016 BLU
 * @link       http://blupoints.com
 */
class CurrencytranslationController extends BaseController
{
    /**
     * Fetch the select drop down data for the currency translation popup
     *
     * @return array
     */
    private function getSelectData()
    {
        return array(
            'languages'  => Language::orderBy('name')->pluck('name','id')
            );
    }

    /**
     * Create and return a default draft currency translation
     *
     * @param int $currency_id
     * @return Result
     */
    private function getCurrecyTranslationDraft($enc_currency_id)
    {
        $currency_id = base64_decode($enc_currency_id);
        $currencyTranslation = new CurrencyTranslation();
        
        $currencyTranslation->name       = 'New Currency translation';
        $currencyTranslation->currency_id = $currency_id;
        
        $currencyTranslation->save();
        
        return $currencyTranslation;
    }

    /**
     * Display the new currency translation page
     *
     * @return View
     */
    public function newCurrencyTranslation($enc_currency_id = null)
    {
        $data = array(
            'currencyTranslation'   => $this->getCurrecyTranslationDraft($enc_currency_id)
        );

        return View::make("currencytranslation.form", array_merge(
            $data, $this->getSelectData()
        ));
    }

    /**
     * Display the edit currency translation page
     *
     * @return View
     */
    public function editCurrencyTranslation($enc_currencyTranslation_id = null)
    {
        $currencyTranslation_id = base64_decode($enc_currencyTranslation_id);
        $data = array(
            'currencyTranslation'   => CurrencyTranslation::find($currencyTranslation_id)
        );

        return View::make("currencytranslation.form", array_merge(
            $data, $this->getSelectData()
        ));
    }

    /**
     * Create a new currency translation resource
     *
     * @param int $currencyTranslation_id
     * @return void
     */
    public function updateCurrencyTranslation($enc_currencyTranslation_id = null)
    {
        $currencyTranslation_id = base64_decode($enc_currencyTranslation_id);
        $rules = array(
            'currencytranslation_name' => 'required|min:1'
        );

        $input = Input::all();
        foreach ($input as $k => $i) {
            $input[$k] = self::sanitizeText($i);
        }
        $validator = Validator::make($input, $rules);

        if($validator->fails()) {
            return Response::json(array(
                'failed' => true,
                'errors' => "Please ensure that you provide a valid currency translation name"
            ));
        }

        $currencyTranslation = CurrencyTranslation::find($currencyTranslation_id);

        // Details
        $currencyTranslation->name       = self::sanitizeText(Input::get('currencytranslation_name'));
        $currencyTranslation->draft      = false;
        $currencyTranslation->lang_id    = self::sanitizeText(Input::get('currencytranslation_lang'));

        $currencyTranslation->push();

        return Response::json(array(
            'failed' => false,
            'view_url' => url('dashboard/currencytranslation/list/' . base64_encode($currencyTranslation->currency_id))
        ));
    }

    /**
     * Delete the specified currency translation
     *
     * @return View
     */
    public function deleteCurrencyTranslation($enc_currencyTranslation_id = null)
    {
        $currencyTranslation_id = base64_decode($enc_currencyTranslation_id);
        CurrencyTranslation::find($currencyTranslation_id)->delete();

        return Response::json(array(
            'failed' => false
        ));
    }

    /**
     * Render a list of currency translations relevant to the Partner
     *
     * @param int $category_id
     * @return View
     */
    public function listCurrencyTranslations($enc_currency_id = null)
    {
        $currency_id = base64_decode($enc_currency_id);
        $data = array(
            'currencyTranslations' => CurrencyTranslation::where('currency_id', $currency_id)->where('draft', false)->get()
        );

        return View::make("currencytranslation.list", $data);
    }

} // EOC