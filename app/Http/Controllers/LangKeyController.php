<?php
namespace App\Http\Controllers;
use View;
use Input;
use Validator;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;

use App\LangKey as LangKey;
use App\Localizationkey as Localizationkey;
use Illuminate\Support\Facades\DB;
/**
 * LangKey Controller
 * 
 * @category   Controllers
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class LangKeyController extends BaseController
{
    /**
     * Fetch the select drop down data for the langkey popup
     *
     * @return array
     */
    private function getSelectData()
    {
        return array(
            'countries' => DB::table('country')->orderBy('name', 'asc')->get(),
            'areas'     => DB::table('area')->orderBy('name', 'asc')->get(),
            'cities'    => DB::table('city')->orderBy('name', 'asc')->get()
        );
    }

    /**
     * Create and return a default draft langkey
     *
     * @param int $lang_id
     * @return Result
     */
    private function getLangKeyDraft($lang_id)
    {
        
        $langkey = new LangKey();

        $langkey->key_id       = '1';
        $langkey->lang_id      = $lang_id;


        $langkey->save();

        

        return $langkey;
    }
    /**
     * get the language keys to 
     * autocomplete field
     *
     * @return Result
     */
    public function localizationkeys()
    {
        $term = self::sanitizeText(Input::get('term'));
        
        if(!$term) {
            exit;
        }
        
        $langkeys = Localizationkey::select('id', 'key')->where('key', 'like', "%".$term."%")->get();

        foreach($langkeys as $langkey) {
            $p = new \stdClass();

            $p->label = $langkey->key;
            $p->value = $langkey->key;
            $p->id = $langkey->id;

            $buffer[] = $p;
        }
        return Response::json($buffer);
    }

    /**
     * Display the new langkey    {
 page
     *
     * @return View
     */
    public function newLangKey($enc_lang_id = null){
        $lang_id = base64_decode($enc_lang_id);
        $data = array(
            'langkey'   => self::getLangKeyDraft($lang_id),
            'lockeys'   => Localizationkey::get()
        );
        
        return View::make("langkey.form", $data);
    }

    /**
     * Display the edit langkey page
     *
     * @return View
     */
    public function editLangKey($enc_langkey_id = null)
    {
        $langkey_id = base64_decode($enc_langkey_id);
        $data = array(
            'langkey'   => LangKey::find($langkey_id),
            'lockeys'   => Localizationkey::get()
            
        );

        return View::make("langkey.form", array_merge(
            $data, $this->getSelectData()
        ));
    }

    /**
     * Create a new langkey resource
     *
     * @param int $langkey_id
     * @return void
     */
    public function updateLangKey($enc_langkey_id = null)
    {
        $langkey_id = base64_decode($enc_langkey_id);
        $rules = array(
            'key' => 'required|min:1'
        );
        $input = Input::all();
        foreach($input as $k=>$i){
            $input[$k] = self::sanitizeText($i);
        }
        $validator = Validator::make($input, $rules);

        if($validator->fails()) {
            return Response::json(array(
                'failed' => true,
                'errors' => "Please ensure that you provide a valid langkey ID"
            ));
        }

        $langkey = LangKey::find($langkey_id);

        // Details
        $langkey->key_id       = self::sanitizeText(Input::get('key'));
        $langkey->value        = self::sanitizeText(Input::get('value'));
        //$langkey->lang_id      = self::sanitizeText(Input::get('lang_id'));
       

        $langkey->push();
        
        $langId = base64_encode($langkey->lang_id);
        return Response::json(array(
            'failed' => false,
            'view_url' => url('dashboard/langkey/list/'.$langId)
        ));
    }

    /**
     * Delete the specified langkey
     *
     * @return View
     */
    public function deleteLangKey($enc_langkey_id = null)
    {
        $langkey_id = base64_decode($enc_langkey_id);
        LangKey::find($langkey_id)->delete();

        return Response::json(array(
            'failed' => false
        ));
    }

    /**
     * Render a list of langkeys relevant to the Partner
     *
     * @param int $lang_id
     * @return View
     */
    public function listLangKeys($enc_lang_id = null)
    {
        $lang_id = base64_decode($enc_lang_id);
        $data = array(
            'langkeys' => LangKey::where('lang_id', $lang_id)->orderBy('id', 'DESC')->get()
        );

        return View::make("langkey.list", $data);
    }

} // EOC