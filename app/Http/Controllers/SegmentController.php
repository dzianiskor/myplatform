<?php
namespace App\Http\Controllers;
use App\Attribute;
use App\BannerSegment;
use App\SegmentUser;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Response;
use View;
use Input;
use Validator;
use Session;
use Security;
use I;
use Illuminate\Support\Facades\Auth;
use BluCollection;
use AuditHelper;
use Illuminate\Support\Facades\Redirect;
use Transaction;
use App\Partner as Partner;
use Notification;
use Ticket;
use Language;
use Localizationkey;
use LangKey;
use Network;
use ManagedObjectsHelper;
use User;
use App\Country;
use App\Admin as Admin;
use App\Role as Role;
use App\City as City;
use App\Area as Area;
use Meta;
use Illuminate\Support\Facades\DB;
use App\Segment as Segment;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Segment Controller
 * 
 * @category   Controllers
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class SegmentController extends BaseController {

    protected $layout = "layouts.dashboard";

    /**
     * Load an EJS template from the segments directory
     * 
     * @param string $template
     * @return Response
     */
    public function loadEJSTemplate($template = null)
    {
        $input = Input::all();
        foreach($input as $k=>$i){
            $input[$k] = self::sanitizeText($i);
        }
        
        return View::make("segment.ejs.$template", $input);
    }

    /**
     * Get all list data
     *
     * @return array
     */
    private function getAllLists()
    {
        return array(
            'partners' => ManagedObjectsHelper::managedPartners()->sortBy('name')->pluck('name', 'id')
        );
    }

    /**
     * Show a list of available segments
     *
     * @return void
     */
    public function listSegments()
    {
        if (!I::can('view_segments')) {
            return Redirect::to(url("dashboard"));
        }

        $userId = Auth::User()->id;
        Session::put($userId . '_index_redirect_url', $_SERVER['QUERY_STRING']);

        $partner = Auth::User()->managedPartnerIDs();
        $segments = Segment::where('draft', false)->where('deleted', 0)->whereIn('partner_id', $partner);

        if (Input::has('q')) {
            $name = self::sanitizeText(Input::get('q'));
            $segments = $segments->where('name', 'like', "%{$name}%");
        }

        if (Input::has('partner') && Input::has('partner') != '') {
            $partnerIds = explode(',', Input::get('partner'));
            $segments = $segments->whereIn('partner_id', $partnerIds);
        }

        $segments = $segments->orderBy('name', 'asc');
        $segments = $segments->paginate(15);

        return View::make("segment.list", array(
            'segments' => $segments->appends(Input::except('page')),
            'lists' => $this->getAllLists()
        ));
    }

    /**
     * Create a list of supported attributes
     * 
     * @return void
     */
    public function getProperties()
    {
        return array(
            "Member" => array(
                'user.email',
                'user.role',
                'user.verified',
                'user.title',
                'user.status',
                'user.first_name',
                'user.last_name',
                'user.gender',
                'user.marital_status',
                'user.age',
                'user.country',
                'user.city',
                'user.area',
                'user.income_bracket',
                'user.occupation',
                'user.num_children',
                'user.tags',
                'user.balance',
            ),
            "Partner" => array(
                'partner.name'
            ),
            "Transactions" => array(
                'transactions.points_earned',
                'transactions.points_redeemed',
                'transactions.spend',                
                'transactions.product.name',
                'transactions.product.brand',
                'transactions.product.model',
                'transactions.location.city',
                'transactions.location.area',
                'transactions.location.country',
            )
        );
    }

    /**
     * Display the new segment window
     *
     * @return Response
     */
    public function newSegment()
    {
        if(!I::can('create_segments')){
            return Redirect::to(url("dashboard"));
        }
        // @TODO: Assign the ID of the current partner access level
        // if partner admin, add the highest hierarchy ID
        // if blu admin, add the blu partner id
        
        $segment                = new Segment();
        $segment->partner_id    = 1; //link it to BLU
        $segment->name          = "New Segment";
        $segment->save();
        
        $segmentId = base64_encode($segment->id);

        return Redirect::to(url(
            "/dashboard/segments/view/{$segmentId}"
        ));
    }

    /**
     * Display the new manual segment window
     *
     * @return Response
     */
    public function newManualSegment()
    {
        if (!I::can('create_segments')) {
            return Redirect::to(url("dashboard"));
        }
        $segment             = new Segment();
        $segment->partner_id = 1;
        $segment->name       = "New Segment";
        $segment->manual     = 1;
        $segment->save();

        $segmentId = base64_encode($segment->id);

        return Redirect::to(url(
            "/dashboard/segments/view_manual/{$segmentId}"
        ));
    }

    /**
     * View a segment
     *
     * @param int $id
     * @return Response
     */
    public function viewSegment($enc_id = null)
    {
        $id = base64_decode($enc_id);
        if(!I::can('view_segments')){
            return Redirect::to(url("dashboard"));
        }
        $segment = Segment::find($id);

        if(!$segment) {
            App::abort(404);
        }
        $managedPartners = Auth::User()->managedPartners();
        $can_view_segment = false;
        foreach($managedPartners as $manP){
            if($manP->id == $segment->partner_id){
                $can_view_segment = true;
                break;
            }
        }
        if($can_view_segment== false && $segment->draft == false){
            return Redirect::to(url("dashboard"));
        }

        return View::make("segment.view", array(
            'segment'    => $segment,
            'properties' => $this->getProperties(),
            'roles'      => DB::table('role')->get(),
            'partners'   => Auth::User()->managedPartners()->sortBy('name'),
            'brands'     => DB::table('brand')->get(),
        ));        
    }

    /**
     * View a manual segment
     *
     * @param string $enc_id
     * @return Response
     */
    public function viewManualSegment($enc_id = null)
    {
        $id = base64_decode($enc_id);
        if (!I::can('view_segments')) {
            return Redirect::to(url("dashboard"));
        }
        $segment = Segment::find($id);

        if (!$segment) {
            App::abort(404);
        }
        $managedPartners = Auth::User()->managedPartners();
        $can_view_segment = false;
        foreach ($managedPartners as $manP) {
            if ($manP->id == $segment->partner_id) {
                $can_view_segment = true;
                break;
            }
        }
        if ($can_view_segment== false && $segment->draft == false) {
            return Redirect::to(url("dashboard"));
        }

        return View::make("segment.view_manual", array(
            'segment'  => $segment,
            'partners' => Auth::User()->managedPartners(),
        ));
    }

    /**
     * Update the specified segment
     *
     * @param int $id
     * @return Response
     */
    public function updateSegment($enc_id = null)
    {
        $id = base64_decode($enc_id);
        if (!I::can('create_segments') && !I::can('edit_segments')) {
            return Redirect::to(url("dashboard"));
        }
        $segment = Segment::find($id);

        if(!$segment) {
            App::abort(404);
        }

        $validator = Validator::make(Input::all(), array(
            'name' => 'required|min:2',
        ));

        if($validator->fails()) {
            return Redirect::to(url("dashboard/segments/view/$enc_id"))->withErrors($validator);
        }

        $criteria = self::sanitizeText(Input::get('criteria'));

        if(empty($criteria)) {
            return Redirect::to(url("dashboard/segments"))->with('alert_error', 'A segment cannot have no criteria');
        }

        $segment->name  = self::sanitizeText(Input::get('name'));
        $segment->draft = 0;
        $segment->partner_id = self::sanitizeText(Input::get('partner_id'));
        $segment->save();

        // Attributes

        DB::table('attribute')->where('mapping_key', $segment->id)->delete();

        $attribute_batch = array();

        foreach($criteria as $field => $values) {

            $keys = array_keys($values);

            for($i = 0; $i < count($values['name']); $i++) {

                $group = array();

                foreach($keys as $k) {
                    $group[$k] = $values[$k][$i];
                }

                $attribute_batch[] = array(
                    'name'        => $field,
                    'mapping_key' => $segment->id,
                    'json_data'   => json_encode($group)
                );                
            }
        }

        DB::table('attribute')->insert($attribute_batch);

        return Redirect::to(Input::get('redirect_to', 'dashboard/segments'));
    }

    /**
     * Update the specified segment
     *
     * @param string $enc_id
     * @return Response
     */
    public function updateManualSegment($enc_id = null)
    {
        $id = base64_decode($enc_id);
        if (!I::can('create_segments') && !I::can('edit_segments')) {
            return Redirect::to(url("dashboard"));
        }
        $segment = Segment::find($id);

        if (!$segment) {
            \App::abort(404);
        }

        $segment->name  = self::sanitizeText(Input::get('name'));
        $segment->draft = 0;
        $segment->partner_id = (int)Input::get('partner_id');
        $segment->save();

        if (Input::hasFile('import_file')) {
            $file = Input::file('import_file');

            if (($handle = fopen($file, "r")) !== false) {
                $data = fgetcsv($handle, 10000, ",");
                if ($data[0] == 'reference_id') {
                    while (($data = fgetcsv($handle, 10000, ",")) !== false) {
                        if ($data[0] == 'reference_id') {
                            continue;
                        }

                        $referenceNumber = $data[0];
                        $reference = \App\Reference::where('number', $referenceNumber)->first();
                        if ($reference) {
                            $userSegment = DB::table('segment_user')
                                             ->where('segment_id', $segment->id)
                                             ->where('user_id', $reference->user_id)
                                             ->get()->toArray();
                        }

                        if ($reference && !$userSegment) {
                            $segment_user = new SegmentUser();
                            $segment_user->user_id = $reference->user_id;
                            $segment_user->segment_id = $segment->id;
                            $segment_user->save();
                        }
                    }
                    fclose($handle);
                }
            }
        }

        $userId = Auth::User()->id;
        $indexUrl = Session::get($userId . '_index_redirect_url');

        return Redirect::to('dashboard/segments' . '?' . $indexUrl);
    }

    /**
     * Delete a segment
     * 
     * @param int $id
     * @return Response
     */
    public function deleteSegment($enc_id = null)
    {
        $id = base64_decode($enc_id);
        if (!I::can('delete_segments')) {
            return Redirect::to(url("dashboard"));
        }
        $segment = Segment::find($id);
        if (!$segment) {
            \App::abort(404);
        }

        if ($segment->manual == 1) {
            $segment->deleted = 1;
            $segment->save();
        } else {
            DB::table('attribute')->where('mapping_key', $segment->id)->delete();
            $banner = BannerSegment::where('segment_id', $segment->id)->get();
            if ($banner) {
                $segment->banners()->detach();
            }

            $segment->delete();
        }

        $userId = Auth::User()->id;
        $indexUrl = Session::get($userId . '_index_redirect_url');

        return Redirect::to('dashboard/segments' . '?' . $indexUrl)->with('alert_success', "The specified segment has been deleted");
    }

    /**
     * Delete a segment criteria
     *
     * @param int $segment_id
     * @param int $criteria_id
     * @return Response
     */
    public function deleteSegmentCriteria($segment_id, $criteria_id)
    {
        if (!I::can('delete_segments')) {
            return Redirect::to(url("dashboard"));
        }

        $criteria = Attribute::where('id', $criteria_id)->where('mapping_key', $segment_id)->first();
        if (!$criteria) {
            \App::abort(404);
        }

        $criteria->delete();

        return response()->json(['result' => 'ok']);
    }

    /**
     * Display the new Segment User page
     *
     * @param null $enc_segment_id
     * @return mixed
     */
    public function newSegmentuser($enc_segment_id = null)
    {
        $segment_id = base64_decode($enc_segment_id);
        $data = [
            'segment_id' => $segment_id
        ];

        return View::make("segmentuser.new", [
            'data' => $data
        ]);
    }


    /**
     * Create a new Segmentuser
     *
     * @return void
     */
    public function createSegmentuser()
    {
        $rules = [
            'user_id' => 'required'
        ];

        $input = Input::all();
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            return Response::json([
                'failed' => true,
                'errors' => "Please ensure that you provide a valid user"
            ]);
        }

        $segmentUsers = SegmentUser::where('segment_id', Input::get('segment_id'))->pluck('user_id')->toArray();
        if (in_array(Input::get('user_id'), $segmentUsers)) {
            return Response::json(array(
                'failed' => true,
                'errors' => "Please choose another user. Current user already exists"
            ));
        }

        $segmentUser = new SegmentUser();
        $segmentUser->segment_id = Input::get('segment_id');
        $segmentUser->user_id = Input::get('user_id');
        $segmentUser->save();

        $segmentId = base64_encode($segmentUser->segment_id);

        return Response::json(array(
            'failed' => false,
            'view_url' => url('dashboard/segmentuser/list/' . $segmentId)
        ));
    }

    /**
     * Render a list of estatementitem relevant to the Partner
     *
     * @param null $enc_segment_id
     * @return mixed
     */
    public function listSegmentuser($enc_segment_id = null)
    {
        $segmentId = base64_decode($enc_segment_id);
        $data = [
            'segmentusers' => SegmentUser::where('segment_id', $segmentId)->get(),
        ];

        return View::make("segmentuser.list", $data);
    }

    /**
     * Delete the specified Segmentuser
     *
     * @param $enc_user_id
     * @param $enc_segment_id
     * @return mixed
     */
    public function deleteSegmentuser($enc_user_id, $enc_segment_id)
    {
        $userId = base64_decode($enc_user_id);
        $segmentId = base64_decode($enc_segment_id);

        SegmentUser::where('user_id', $userId)->where('segment_id', $segmentId)->delete();

        return Response::json(array(
            'failed' => false
        ));
    }
} // EOC