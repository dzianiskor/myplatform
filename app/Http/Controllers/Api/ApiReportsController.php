<?php

namespace App\Http\Controllers\Api;

use App\Repositories\Report\RepositoryReport25;
use App\Repositories\Report\RepositoryReport40;
use App\TransactionItem;
use App\User;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use \ReportHelper;
use \CurrencyHelper;
use \PointsHelper;
use App\Currency;
use App\Admin;
use App\Network;
use App\Product;
use App\Transaction;
use App\Datatables\SSP;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ApiReportsController extends Controller
{
    public function getStores(Request $request) {
        $defaults = [
            'parent_ids' => explode(',', $request->get('parent_ids')),
            'entities' => explode(',', $request->get('entity')),
        ];

        return View::make('report.partials.stores', [
            'networkId' => $request->get('network_id'),
            'defaults' => $defaults,
        ]);
    }

    public function getPartners(Request $request) {
        $defaults = [
            'parent_ids' => explode(',', $request->get('parent_ids')),
            'entities' => explode(',', $request->get('entity')),
        ];

        return View::make('report.partials.partners', [
            'networkId' => $request->get('network_id'),
            'defaults' => $defaults,
        ]);
    }

    /**
     * @param $defaults
     * @param $query
     */
    protected function applyPartnersConstraints($query)
    {
        $admin = Admin::find(request()->admin_id);
        //We do not show all transactions for non BLU Admin users
        if (! $admin->roles->contains('name', 'BLU Admin')) {
//            $query->whereHas('reference', function ($query) use ($admin) {
//                return $query->whereIn('reference.partner_id', $admin->managedPartners()->pluck('id')->toArray());
//            });
        }
    }

    public function report1(Request $request)
    {
        $defaults = ReportHelper::normalizeDefaults();
        $currencyId = 6;
        if ($defaults['network_currency'] != 'not') {
            $networkId  = $defaults['network_currency'];
        } else {
            $networkId  = 1;
        }
        $networkCurrencyId = Network::find($networkId)->currency_id;
        $network_currency = Currency::find($networkCurrencyId);
        $network_exchangeRate   = $network_currency->latestRate();
        $exchangeRate   = 1;
        $currencyShortCode  = 'USD';
        if ($defaults['currency'] > 0) {
            $currencyId = $defaults['currency'];
            $currency = Currency::find($currencyId);
            $exchangeRate   = $currency->latestRate();
            $currencyShortCode  = $currency->short_code;
        }

        $query = Transaction::whereIn('transaction.store_id', $defaults['entities'])
                            ->whereBetween('transaction.created_at', [$defaults['start_date'], $defaults['end_date']])
                            ->where('transaction.network_id', $networkId)
                            ->whereIn('transaction.partner_id', $defaults['parent_ids']);
        $totals = $query->count('transaction.id');

        $data = $query->leftJoin('partner', 'transaction.partner_id', '=', 'partner.id')
                      ->leftJoin('store', 'transaction.store_id', '=', 'store.id')
                      ->leftJoin('currency_pricing', function ($q) use ($currencyId) {
                          $q->where('currency_pricing.currency_id', '=', $currencyId)
                            ->where('currency_pricing.date', '>=', DB::raw('DATE(transaction.created_at)'))
                            ->orderBy('currency_pricing.date', 'DESC')
                            ->limit(1);
                      })
                      ->select(
                          'transaction.id',
                          'transaction.created_at',
                          'partner.name as partner_name',
                          'store.name as store_name',
                          'transaction.points_rewarded',
                          'transaction.points_redeemed',
                          'transaction.amt_reward',
                          'transaction.amt_redeem',
                          'transaction.total_amount',
                          'transaction.original_total_amount',
                          'transaction.currency_id',
                          'currency_pricing.rate as exchange_rate'
                      )
                      ->limit($request['length'])
                      ->offset($request['start'])
                      ->orderBy('transaction.id', 'desc')
                      ->groupBy('transaction.id')
                      ->get()
                      ->each(function ($item) use ($exchangeRate) {
                          $item->trx_amount = $item->exchange_rate ? $item->total_amount * $item->exchange_rate : $item->total_amount * $exchangeRate;
                      })
                      ->toArray();

        return SSP::make($request, $totals, $data);
    }

    public function report1Totals(Request $request)
    {
        $defaults = ReportHelper::normalizeDefaults();

        if ($defaults['network_currency'] != 'not') {
            $networkId  = $defaults['network_currency'];
        } else {
            $networkId  = 1;
        }
        $currencyShortCode  = 'USD';
        if ($defaults['currency'] > 0) {
            $currencyId = $defaults['currency'];
            $currency = Currency::find($currencyId);
            $currencyShortCode  = $currency->short_code;
        }

        $totals = Transaction::whereIn('store_id', $defaults['entities'])
                             ->whereBetween('created_at', [$defaults['start_date'], $defaults['end_date']])
                             ->where('network_id', $networkId)
                             ->whereIn('transaction.partner_id', $defaults['parent_ids'])
                             ->select(
                                 DB::raw('SUM(points_rewarded) as pts_reward'),
                                 DB::raw('SUM(points_redeemed) as pts_redeem'),
                                 DB::raw('SUM(total_amount) as amt_reward'),
                                 DB::raw('SUM(amt_redeem) as amt_redeem'),
                                 DB::raw('SUM(trx_reward) as trx_reward'),
                                 DB::raw('SUM(trx_redeem) as trx_redeem')
                             )
                             ->first()
                             ->toArray();

        $totals['trx_reward'] = number_format($totals['trx_reward']);
        $totals['trx_redeem'] = number_format($totals['trx_redeem']);
        $totals['pts_reward'] = number_format($totals['pts_reward'], 2);
        $totals['pts_redeem'] = number_format($totals['pts_redeem']);
        $totals['amt_reward'] = number_format($totals['amt_reward'] * $request->exchange_rate, 2);
        $totals['amt_redeem'] = number_format($totals['amt_redeem'] * $request->exchange_rate, 2);
        $totals['currencyShortCode'] = $currencyShortCode;

        return $totals;
    }

    public function report4(Request $request)
    {
        $defaults = ReportHelper::normalizeDefaults();

        $exchangeRate   = 1;
        $currencyShortCode  = 'USD';
        $currencyId         = 6;
        if ($defaults['currency'] > 0) {
            $currencyId = $defaults['currency'];
            $currency = Currency::where('id', $currencyId)->first();
            $exchangeRate   = $currency->latestRate();
            $currencyShortCode  = $currency->short_code;
        }
        $networkId  = $defaults['network_currency'];

        $query = Transaction::select('transaction.*', 'partner.name as partner_name', 'currency.short_code as currency_code')
                            ->join('partner', 'partner.id', '=', 'transaction.partner_id')
                            ->join('currency', 'currency.id', '=', 'transaction.currency_id')
                            ->where('transaction.created_at', '>=', $defaults['start_date'])
                            ->where('transaction.created_at', '<=', $defaults['end_date'])
                            ->where('transaction.network_id', $networkId);

        if ($defaults['member_id']) {
            $query->whereIn('transaction.user_id', $defaults['member_id']);
        }

        $query->orderBy('transaction.created_at', 'DESC');

        $totals = $query->count('transaction.id');

        $data = $query->limit($request['length'])
                      ->offset($request['start'])
                      ->orderBy('transaction.id', 'desc')
                      ->get()
                      ->each(function ($t) use ($exchangeRate, $currencyId) {
                          $t->store_name = $t->store ? $t->store->name : 'N/A';
                          $t->date = Carbon::parse($t->created_at)->format('Y-m-d');
                          $t->type = $t->trx_reward ? 'Reward' : 'Redemption';
                          $t->points = $t->trx_reward ?
                              '+' . number_format($t->points_rewarded, 0, '.', ',') :
                              '-' . number_format($t->points_redeemed, 0, '.', ',');
                          $t->original_amount = $t->currency_code . ' ' . number_format($t->original_total_amount, 2, '.', ',');

                          if ($t->total_amount || $t->original_total_amount) {
                              if ($currencyId == $t->currency_id && $t->original_total_amount != 0) {
                                  $t->amount = number_format($t->original_total_amount, 2, '.', ',');
                              } else {
                                  $t->amount = number_format($t->original_total_amount * $exchangeRate, 2, '.', ',');
                              }
                          } else {
                              $t->amount = 0.00;
                          }
                      })
                      ->toArray();

        return SSP::make($request, $totals, $data);
    }

    public function report4Totals(Request $request)
    {
        $defaults = ReportHelper::normalizeDefaults();

        $exchangeRate   = 1;
        $currencyShortCode  = 'USD';
        $currencyId         = 6;
        if ($defaults['currency'] > 0) {
            $currencyId = $defaults['currency'];
            $currency = Currency::where('id', $currencyId)->first();
            $exchangeRate   = $currency->latestRate();
            $currencyShortCode  = $currency->short_code;
        }
        $networkId  = $defaults['network_currency'];

        $query = Transaction::where('created_at', '>=', $defaults['start_date'])
                            ->where('created_at', '<=', $defaults['end_date'])
                            ->where('network_id', $networkId);

        if ($defaults['member_id']) {
            $query->whereIn('user_id', $defaults['member_id']);
        }

        $query1 = clone $query;
        $query2 = clone $query;
        $query1->where('trx_reward', '1');
        $query2->where('trx_redeem', '1');
        $query->orderBy('created_at', 'DESC');
        // Construct Totals
        $totals['pts_reward'] = number_format($query->sum('points_rewarded'), 0, '.', ',');
        $totals['pts_redeem'] = number_format($query->sum('points_redeemed'), 0, '.', ',');
        $totals['amt_reward'] = number_format($query1->sum('total_amount'), 2, '.', ',');
        $totals['amt_redeem'] = number_format($query2->sum('amt_redeem'), 2, '.', ',');
        $totals['trx_reward'] = $query->sum('trx_reward');
        $totals['trx_redeem'] = $query->sum('trx_redeem');
        $totals['exchangeRate']        = $exchangeRate;
        $totals['currencyShortCode']   = $currencyShortCode;
        $totals['currency'] = $currencyId;

        return $totals;
    }

    public function report22(Request $request)
    {
        $defaults = ReportHelper::normalizeDefaults();

        $query = Transaction::whereBetween('transaction.created_at', [$defaults['start_date'], $defaults['end_date']])
        ->whereIn('partner_id', $defaults['entities']);

        if ($defaults['daterange'] != '') {
            if (strcmp($defaults['daterange'], 'daily') == 0) {
                $query->select(DB::raw('DATE(transaction.created_at) creation, network_id, sum(points_rewarded) preward, sum(points_redeemed) predeem, sum(trx_reward) trx_rew, sum(trx_redeem) trx_red'))
                    ->groupBy('creation')
                    ->orderBy('creation');
            }
            if (strcmp($defaults['daterange'], 'monthly') == 0) {
                $query->select(DB::raw('YEAR(transaction.created_at) creation, MONTH(transaction.created_at) crmonth, network_id, sum(points_rewarded) preward, sum(points_redeemed) predeem, sum(trx_reward) trx_rew, sum(trx_redeem) trx_red'))
                    ->groupBy('creation')
                    ->groupBy('crmonth')
                    ->orderBy('creation', 'DESC')
                    ->orderBy('crmonth', 'DESC');
            }
            if (strcmp($defaults['daterange'], 'yearly') == 0) {
                $query->select(DB::raw('YEAR(transaction.created_at) creation, network_id, sum(points_rewarded) preward, sum(points_redeemed) predeem, sum(trx_reward) trx_rew, sum(trx_redeem) trx_red'))
                    ->groupBy('creation')
                    ->orderBy('creation', 'DESC');
            }
        } else {
            $query->select(DB::raw('DATE(transaction.created_at) creation, network_id, sum(points_rewarded) preward, sum(points_redeemed) predeem, sum(trx_reward) trx_rew, sum(trx_redeem) trx_red, amt_reward, amt_redeem'))
                ->groupBy('creation')
                ->orderBy('creation', 'DESC');
        }

        if (!empty($defaults['network_ids'])) {
            $query->whereIn('network_id', $defaults['network_ids']);
        }

        $data = $query->get()->toArray();
        $totals = count($data);

        foreach ($data as &$row) {
            $row['activity'] = number_format($row['preward'] - $row['predeem']);
            $row['preward'] = number_format($row['preward']);
            $row['trx_rew'] = number_format($row['trx_rew']);
            $row['predeem'] = number_format($row['predeem']);
            $row['trx_red'] = number_format($row['trx_red']);
            $row['date'] = $row['creation'];
            if (isset($row['crmonth'])) {
                $dateObj   = \DateTime::createFromFormat('!m', $row['crmonth']);
                $row['date'] = $dateObj->format('F') . ' ' . $row['creation'];
            }
            if (strlen($row['creation']) > 4) {
                $dateObj = new \DateTime($row['creation']);
                $dateformatted = $dateObj->format('F j, Y');
                $row['date'] = $dateformatted;
            }
        }

        return SSP::make($request, $totals, $data);
    }

    public function report22Totals(Request $request)
    {
        $defaults = ReportHelper::normalizeDefaults();

        // Construct response
        $results = Transaction::whereBetween('created_at', [$defaults['start_date'], $defaults['end_date']])
            ->whereIn('partner_id', $defaults['entities'])
            ->select(DB::raw('trx_reward, trx_redeem, count(*) as aggregate'))
            ->get();
        $totals = [
            'num_reward_trx' => number_format($results->where('trx_reward', 1)->sum('aggregate')),
            'num_redeem_trx' => number_format($results->where('trx_redeem', 1)->sum('aggregate')),
        ];

        return $totals;
    }

    public function report23(Request $request)
    {
        $defaults = ReportHelper::normalizeDefaults();

        $query = Transaction::whereBetween('transaction.created_at', [$defaults['start_date'], $defaults['end_date']])
        ->join('partner', 'transaction.partner_id', '=', 'partner.id')
        ->where('partner_id', 1);

        if ($defaults['member_id']) {
            $query->whereIn('user_id', $defaults['member_id']);
        }

        if ($defaults['daterange'] != '') {
            if (strcmp($defaults['daterange'], 'daily') == 0) {
                $query->select(DB::raw('DATE(transaction.created_at) creation, network_id, partner_id, partner.name as partner_name, sum(points_rewarded) preward, sum(points_redeemed) predeem, sum(trx_reward) trx_rew, sum(trx_redeem) trx_red'))
                    ->groupBy('creation')
                    ->orderBy('creation');
            }
            if (strcmp($defaults['daterange'], 'monthly') == 0) {
                $query->select(DB::raw('YEAR(transaction.created_at) creation, MONTH(transaction.created_at) crmonth, network_id, partner_id, partner.name as partner_name, sum(points_rewarded) preward, sum(points_redeemed) predeem, sum(trx_reward) trx_rew, sum(trx_redeem) trx_red'))
                    ->groupBy('creation')
                    ->groupBy('crmonth')
                    ->orderBy('creation', 'DESC')
                    ->orderBy('crmonth', 'DESC');
            }
            if (strcmp($defaults['daterange'], 'yearly') == 0) {
                $query->select(DB::raw('YEAR(transaction.created_at) creation, network_id, partner_id, partner.name as partner_name, sum(points_rewarded) preward, sum(points_redeemed) predeem, sum(trx_reward) trx_rew, sum(trx_redeem) trx_red'))
                    ->groupBy('creation')
                    ->orderBy('creation', 'DESC');
            }
        } else {
            $query->select(DB::raw('DATE(transaction.created_at) creation, network_id, partner_id, partner.name as partner_name, sum(points_rewarded) preward, sum(points_redeemed) predeem, sum(trx_reward) trx_rew, sum(trx_redeem) trx_red, amt_reward, amt_redeem'))
                ->groupBy('creation')
                ->orderBy('creation', 'DESC');
        }

        if ($defaults['bypartner'] != 'not') {
            $query->groupBy('partner_id');
        }

        if (!empty($defaults['network_ids'])) {
            $query->whereIn('network_id', $defaults['network_ids']);
        }

        $totals = (clone $query)->get()->count();

        $data = $query->limit($request['length'])
                      ->offset($request['start'])
                      ->orderBy('transaction.id', 'desc')
                      ->get()
                      ->toArray();

        foreach ($data as &$row) {
            $row['activity'] = number_format($row['preward'] - $row['predeem']);
            $row['preward'] = number_format($row['preward']);
            $row['trx_rew'] = number_format($row['trx_rew']);
            $row['predeem'] = number_format($row['predeem']);
            $row['trx_red'] = number_format($row['trx_red']);
            $row['date'] = $row['creation'];
            if (isset($row['crmonth'])) {
                $dateObj   = \DateTime::createFromFormat('!m', $row['crmonth']);
                $monthName = $dateObj->format('M');
                $row['date'] = $monthName . ". ";
            }
            if (strlen($row['creation']) > 4) {
                $dateObj = new \DateTime($row['creation']);
                $dateformatted = $dateObj->format('F j, Y');
                $row['date'] = $dateformatted;
            }
        }

        return SSP::make($request, $totals, $data);
    }

    public function report23Totals(Request $request)
    {
        $defaults = ReportHelper::normalizeDefaults();

        // Construct response
        $results = Transaction::whereBetween('created_at', [$defaults['start_date'], $defaults['end_date']])
            ->where('partner_id', 1)
            ->select(DB::raw('trx_reward, trx_redeem, count(*) as aggregate'))
            ->get();

        if ($defaults['member_id']) {
            $results->whereIn('user_id', $defaults['member_id']);
        }

        $totals = [
            'num_reward_trx' => number_format($results->where('trx_reward', 1)->sum('aggregate')),
            'num_redeem_trx' => number_format($results->where('trx_redeem', 1)->sum('aggregate')),
        ];

        return $totals;
    }

    public function report24(Request $request)
    {
        $defaults = ReportHelper::normalizeDefaults();

        $query = Transaction::whereBetween('transaction.created_at', [$defaults['start_date'], $defaults['end_date']])
            ->join('partner', 'transaction.partner_id', '=', 'partner.id')
            ->join('store', 'transaction.store_id', '=', 'store.id')
            ->whereIn('store_id', $defaults['entities'])
            ->whereIn('transaction.partner_id', $defaults['parent_ids'])
            ->groupBy('store_id');

        if ($defaults['daterange'] != '') {
            if (strcmp($defaults['daterange'], 'daily') == 0) {
                $query->select(DB::raw('DATE(transaction.created_at) creation, transaction.partner_id, transaction.store_id,
                    partner.name as partner_name, store.name as store_name, sum(points_rewarded) preward, sum(points_redeemed) predeem, sum(trx_reward) trx_rew, sum(trx_redeem) trx_red'))
                    ->groupBy('creation')
                    ->orderBy('creation');
            }
            if (strcmp($defaults['daterange'], 'monthly') == 0) {
                $query->select(DB::raw('YEAR(transaction.created_at) creation, MONTH(transaction.created_at) crmonth, transaction.partner_id, transaction.store_id,
                    partner.name as partner_name, store.name as store_name, sum(points_rewarded) preward, sum(points_redeemed) predeem, sum(trx_reward) trx_rew, sum(trx_redeem) trx_red'))
                    ->groupBy('creation')
                    ->groupBy('crmonth')
                    ->orderBy('creation', 'DESC')
                    ->orderBy('crmonth', 'DESC');
            }
            if (strcmp($defaults['daterange'], 'yearly') == 0) {
                $query->select(DB::raw('YEAR(transaction.created_at) creation, transaction.partner_id, transaction.store_id,
                    partner.name as partner_name, store.name as store_name, sum(points_rewarded) preward, sum(points_redeemed) predeem, sum(trx_reward) trx_rew, sum(trx_redeem) trx_red'))
                    ->groupBy('creation')
                    ->orderBy('creation', 'DESC');
            }
        } else {
            $query->select(DB::raw('DATE(transaction.created_at) creation, transaction.partner_id, transaction.store_id,
                partner.name as partner_name, store.name as store_name,
                sum(points_rewarded) preward, sum(points_redeemed) predeem, sum(trx_reward) trx_rew, sum(trx_redeem) trx_red, amt_reward, amt_redeem'))
                ->groupBy('creation')
                ->orderBy('creation', 'DESC');
        }

        if (!empty($defaults['network_ids'])) {
            $query->whereIn('network_id', $defaults['network_ids']);
        }

        $totals = (clone $query)->get()->count();

        $data = $query->limit($request['length'])
                      ->offset($request['start'])
                      ->orderBy('transaction.id', 'desc')
                      ->get()
                      ->toArray();
//        $totals = count($data);

        foreach ($data as &$row) {
            $row['activity'] = number_format($row['preward'] - $row['predeem']);
            $row['preward'] = number_format($row['preward']);
            $row['trx_rew'] = number_format($row['trx_rew']);
            $row['predeem'] = number_format($row['predeem']);
            $row['trx_red'] = number_format($row['trx_red']);
            $row['date'] = $row['creation'];
            if (isset($row['crmonth'])) {
                $dateObj   = \DateTime::createFromFormat('!m', $row['crmonth']);
                $monthName = $dateObj->format('M');
                $row['date'] = $monthName . ". ";
            }
            if (strlen($row['creation']) > 4) {
                $dateObj = new \DateTime($row['creation']);
                $dateformatted = $dateObj->format('F j, Y');
                $row['date'] = $dateformatted;
            }
        }

        return SSP::make($request, $totals, $data);
    }

    public function report24Totals(Request $request)
    {
        $defaults = ReportHelper::normalizeDefaults();

        // Construct response
        $results = Transaction::whereBetween('created_at', [$defaults['start_date'], $defaults['end_date']])
            ->whereIn('store_id', $defaults['entities'])
            ->whereIn('partner_id', $defaults['parent_ids'])
            ->whereIn('network_id', $defaults['network_ids'])
            ->select(DB::raw('trx_reward, trx_redeem, count(*) as aggregate'))
            ->get();
        $totals = [
            'num_reward_trx' => number_format($results->where('trx_reward', 1)->sum('aggregate')),
            'num_redeem_trx' => number_format($results->where('trx_redeem', 1)->sum('aggregate')),
        ];

        return $totals;
    }

    public function report25(Request $request)
    {
        $defaults = ReportHelper::normalizeDefaults();
        $partners = Admin::find($request['user_id']);

        if (is_null($partners)) {
            return SSP::make($request, $totals = 0, $data = []);
        }

        $partners = $partners->managedPartnerIDs();
        $query = Transaction::whereBetween('transaction.created_at', [$defaults['start_date'], $defaults['end_date']])
            ->join('partner', 'transaction.partner_id', '=', 'partner.id')
            ->join('network', 'transaction.network_id', '=', 'network.id')
            ->whereIn('transaction.network_id', $defaults['network_ids'])
            ->whereIn('transaction.partner_id', $partners);

        if ($defaults['daterange'] != '') {
            if (strcmp($defaults['daterange'], 'daily') == 0) {
                $query->select(
                    DB::raw('DATE(transaction.created_at) creation'),
                    'partner.name as partner_name',
                    'network.name as network_name',
                    DB::raw('sum(points_rewarded) preward'),
                    DB::raw('sum(points_redeemed) predeem'),
                    DB::raw('sum(trx_reward) trx_rew'),
                    DB::raw('sum(trx_redeem) trx_red')
                )
                    ->groupBy('network_id')
                    ->groupBy('creation')
                    ->orderBy('creation', 'DESC');
            }
            if (strcmp($defaults['daterange'], 'monthly') == 0) {
                $query->select(DB::raw('YEAR(transaction.created_at) creation, MONTH(transaction.created_at) crmonth, partner.name as partner_name, network.name as network_name, sum(points_rewarded) preward, sum(points_redeemed) predeem, sum(trx_reward) trx_rew, sum(trx_redeem) trx_red'))
                    ->groupBy('network_name')
                    ->groupBy('creation')
                    ->groupBy('crmonth')
                    ->orderBy('creation', 'DESC')
                    ->orderBy('crmonth', 'DESC');
            }
            if (strcmp($defaults['daterange'], 'yearly') == 0) {
                $query->select(DB::raw('YEAR(transaction.created_at) creation, partner.name as partner_name, network.name as network_name, sum(points_rewarded) preward, sum(points_redeemed) predeem, sum(trx_reward) trx_rew, sum(trx_redeem) trx_red'))
                    ->orderBy('creation', 'DESC')
                    ->groupBy('network_name')
                    ->groupBy('creation');
            }
        } else {
            $query->select(DB::raw('DATE(transaction.created_at) creation, partner.name as partner_name, network.name as network_name, sum(points_rewarded) preward, sum(points_redeemed) predeem, sum(trx_reward) trx_rew, sum(trx_redeem) trx_red'))
                    ->groupBy('network_name')
                    ->groupBy('creation')
                    ->orderBy('creation', 'DESC');
        }

        if (!empty($defaults['network_ids'])) {
            $query->whereIn('network_id', $defaults['network_ids']);
        }

        if ($defaults['bypartner'] != 'not') {
            $query->groupBy('partner_name');
        }

        $totals = (clone $query)->get()->count();

        $data = $query->limit($request['length'])
            ->offset($request['start'])
            ->orderBy('transaction.id', 'desc')
            ->get()
            ->toArray();

        foreach ($data as &$row) {
            $row['activity'] = number_format($row['preward'] - $row['predeem']);
            $row['preward'] = number_format($row['preward']);
            $row['trx_rew'] = number_format($row['trx_rew']);
            $row['predeem'] = number_format($row['predeem']);
            $row['trx_red'] = number_format($row['trx_red']);
            $row['date'] = $row['creation'];
            if (isset($row['crmonth'])) {
                $dateObj   = \DateTime::createFromFormat('!m', $row['crmonth']);
                $monthName = $dateObj->format('M');
                $row['date'] .= ' '.$monthName . ". ";
            }
            if (strlen($row['creation']) > 4) {
                $dateObj = new \DateTime($row['creation']);
                $dateformatted = $dateObj->format('F j, Y');
                $row['date'] = $dateformatted;
            }
        }

        return SSP::make($request, $totals, $data);
    }

    public function report25totals(Request $request)
    {
        $defaults = ReportHelper::normalizeDefaults();
        $partners = Admin::find($request['user_id']);

        if (is_null($partners)) {
            return [
                'num_reward_trx' => 'N/A',
                'num_redeem_trx' => 'N/A',
            ];
        }

        $partners = $partners->managedPartnerIDs();
        $results = Transaction::whereBetween('created_at', [$defaults['start_date'], $defaults['end_date']])
            ->whereIn('transaction.network_id', $defaults['network_ids'])
            ->whereIn('transaction.partner_id', $partners)
            ->select(DB::raw('sum(trx_reward) as trx_reward, sum(trx_redeem) as trx_redeem'))
            ->get()
            ->toArray();

        $results = array_collapse($results);

        $totals = [
            'num_reward_trx' => number_format($results['trx_reward']),
            'num_redeem_trx' => number_format($results['trx_redeem']),
        ];

        return $totals;
    }

    public function report39(Request $request)
    {
        $defaults = ReportHelper::normalizeDefaults();

        $query = Transaction::select(
            'transaction.id',
            \DB::raw('DATE_FORMAT(transaction.created_at, "%Y-%m-%d") created_at_date'),
            'transaction.created_at',
            'transaction.total_amount',
            'transaction.original_total_amount',
            'transaction.currency_id',
            'transaction.points_rewarded',
            'transaction.ref_number',
            'store.name as store_name',
            'loyalty.name as loyalty_name',
            'product.name as product_name',
            'currency.short_code as tcurrency_short_code',
            DB::raw('(select number FROM reference as r WHERE r.partner_id = transaction.partner_id and r.user_id = transaction.user_id order by r.id limit 1) as reference_number'),
            DB::raw('(select number FROM reference as r WHERE r.partner_id = partner.parent_id and r.user_id = transaction.user_id order by r.id limit 1) as reference_parent_number'),
            'users.first_name as user_first_name',
            'users.last_name as user_last_name'
        );

        if (is_array($defaults['member_id']) && current($defaults['member_id']) != 0) {
            $query->whereIn('transaction.user_id', $defaults['member_id']);
        }

        if (is_array($defaults['entities']) && current($defaults['entities']) != -1) {
            $query->whereIn('transaction.partner_id', $defaults['entities']);
        }

        $query->leftJoin('users', 'transaction.user_id', '=', 'users.id')
              ->leftJoin('store', 'transaction.store_id', '=', 'store.id')
              ->leftJoin('loyalty', 'transaction.rule_id', '=', 'loyalty.id')
              ->leftJoin('product', 'transaction.product_id', '=', 'product.id')
              ->join('partner', 'transaction.partner_id', '=', 'partner.id')
              ->leftJoin('currency', 'transaction.currency_id', '=', 'currency.id');

        $query->whereBetween('transaction.created_at', [$defaults['start_date'], $defaults['end_date']])
              ->where('trx_reward', 1);

        if (!empty($defaults['network_ids'])) {
            $query->whereIn('network_id', $defaults['network_ids']);
        }

        $totals = $query->count();

        $data = $query->limit($request['length'])
                      ->offset($request['start'])
                      ->orderBy('transaction.id', 'desc')
                      ->get();

        return SSP::make($request, $totals, $data->toArray());
    }

    public function report39Totals(Request $request)
    {
        $defaults = ReportHelper::normalizeDefaults();

        $query = Transaction::whereBetween('transaction.created_at', [$defaults['start_date'], $defaults['end_date']])
                            ->where('trx_reward', 1);

        if (is_array($defaults['member_id']) && current($defaults['member_id']) != 0) {
            $query->whereIn('transaction.user_id', $defaults['member_id']);
        }

        if (is_array($defaults['entities']) && current($defaults['entities']) != -1) {
            $query->whereIn('transaction.partner_id', $defaults['entities']);
        }


        if (!empty($defaults['network_ids'])) {
            $query->whereIn('network_id', $defaults['network_ids']);
        }

        $admin = Admin::find(request()->admin_id);
        if (!$admin->roles->contains('name', 'BLU Admin')) {
            $query->whereHas('references', function($query) use ($admin) {
                return $query->whereIn('reference.partner_id', $admin->managedPartners()->pluck('id')->toArray());
            });
        }

        $exchangeRate = '';
        if ($defaults['currency'] > 0 && $defaults['currency'] != 6) {
            $currency = Currency::where('id', $defaults['currency'])->first();
            $exchangeRate = $currency->latestRate();
        }

        $totals = $query->select(
            DB::raw('COUNT(*) as nb_of_rewards'),
            DB::raw('SUM(points_rewarded) as pts_reward'),
            DB::raw('SUM(total_amount) as amount_reward')
        )
                        ->first()
                        ->toArray();

        $totals['nb_of_rewards'] = number_format($totals['nb_of_rewards']);
        $totals['pts_reward'] = number_format($totals['pts_reward'], 2);

        if ($exchangeRate) {
            $totals['amount_reward'] = number_format($totals['amount_reward'] * $exchangeRate, 2);
        } else {
            $totals['amount_reward'] = number_format($totals['amount_reward'], 2);
        }

        return $totals;
    }

    public function report40(Request $request, RepositoryReport40 $repository)
    {
        $exchangeRate = 1;
        $currencyShortCode = 'USD';
        $currencyId = 6;

        $defaults = ReportHelper::normalizeDefaults();
        $limit = $request['length'];
        $offset = $request['start'];

        $partnerIds = Auth::user()->managedPartnerIDs() ?: [];

        if (!$memberId = $defaults['member_id'][0] ?? null)
        {
            $partnerIds = $defaults['entities'];
        }

        if ($defaults['currency'] > 0)
        {
            $currencyId = $defaults['currency'];
            $currency = Currency::where('id', $currencyId)->first();
            $exchangeRate = $currency->latestRate();
            $currencyShortCode = $currency->short_code;
        }

        $networkId = $defaults['network_currency'] != 'not' ? $defaults['network_currency'] : 1;

        $data = $repository->startDate($defaults ['start_date'])
                           ->endDate($defaults['end_date'])
                           ->memberId($memberId)
                           ->networkId($networkId)
                           ->partnerId(array_intersect($partnerIds, Auth::user()->managedPartnerIds() ?: []))
                           ->limit($limit)
                           ->offset($offset)
                           ->getData();

        $tot = [];
        $tot['exchangeRate'] = $exchangeRate;
        $tot['currencyShortCode'] = $currencyShortCode;
        $tot['currency'] = $currencyId;

        foreach ($data as &$row)
        {
            $row->date = date("Y-m-d", strtotime($row->date));
            $row->refID = isset($row->refID) && !empty($row->refID) ? $row->refID : ($row->parentRefID ?? 'N/A');

            $row->name = $row->first_name . ' ' . $row->last_name;

            if ($row->name == 'na na')
            {
                $row->name = 'N/A';
            }

            if ($row->category == 'Items' || ($row->category == 'Reversal' && !empty($row->item)))
            {
                $product = Product::find($row->item);
                $transactionItem = TransactionItem::where('product_id', $row->item)
                                                  ->where('transaction_id', $row->trx_id)
                                                  ->first();
                if (!$product)
                {
                    $itemName = 'N/A';
                } else
                {
                    $itemName = $product->name;
                }

                $quantity = $row->quantity;
//                $ptsRedeemed = $row->points_redeemed + $row->delivery_cost;
//                $ptsRedeemed = $row->trx_pts_redeemed;
                $ptsRedeemed = $row->paid_in_points;
                if ($transactionItem && $transactionItem->delivery_cost) {
                    $row->delivery_cost = $transactionItem->delivery_cost;
                } else {
                    $row->delivery_cost = 0;
                }
                $deliveryAmount = \PointsHelper::pointsToAmount($row->delivery_cost, $tot['currencyShortCode'], $row->network_id);

                if ($tot['currency'] == $row->ti_currency_id)
                {    $amountSpentArr['amount'] = $row->ti_original_total_amount;
                } else
                {    $amountSpentArr = \CurrencyHelper::convertFromUSD($row->amount_spent, $tot['currencyShortCode']);
                }

                $amountSpent = $amountSpentArr['amount'] + $deliveryAmount;
                $row->cash_payment = $row->paid_in_cash;
            } else
            {
                $trx = Transaction::find($row->ref_number);
                if ($trx)
                {
                    $itemNameArr = explode('|', $trx->ref_number);
                    if (!empty($itemNameArr))
                    {
                        $itemName = $itemNameArr['0'];
                    } else
                    {
                        $itemName = $trx->ref_number;
                    }
                }

                if ($row->category == 'Flight')
                {
                    $flightDetails = json_decode($row->notes, true);
                    $inDepart = 'N/A';
                    $inArrive = 'N/A';
                    $outDepart = 'N/A';
                    $outArrive = 'N/A';
                    if (!empty($flightDetails['in_depart_station_name']))
                    {
                        $inDepart = $flightDetails['in_depart_station_name'];
                    }
                    if (!empty($flightDetails['in_arrive_station_name']))
                    {
                        $inArrive = $flightDetails['in_arrive_station_name'];
                    }
                    if (!empty($flightDetails['out_depart_station_name']))
                    {
                        $outDepart = $flightDetails['out_depart_station_name'];
                    }
                    if (!empty($flightDetails['out_arrive_station_name']))
                    {
                        $outArrive = $flightDetails['out_arrive_station_name'];
                    }
                    $itemName = $inDepart . ' - ' . $inArrive . ' / ' . $outDepart . ' - ' . $outArrive;
                } elseif ($row->category == 'Car')
                {
                    $carDetails = json_decode($row->notes, true);
                    if (!empty($carDetails['vehicle']))
                    {
                        $itemName = $carDetails['vehicle'];
                    }
                } elseif ($row->category == 'Hotel')
                {
                    $hotelDetails = json_decode($row->notes, true);
                    if (!empty($hotelDetails['hotelName']))
                    {
                        $itemName = $hotelDetails['hotelName'];
                    }
                } elseif ($row->category == 'Points' || $row->category == 'Amount')
                {
                    $itemNameArr = explode('|', $row->ref_number);
                    if (!empty($itemNameArr))
                    {
                        $itemName = $itemNameArr['0'];
                        $category = 'Transfer';
                    } else
                    {
                        $itemName = $row->ref_number;
                    }
                }

                if ($row->reversed == 1 && $row->trx_reward == 1)
                {
                    $category = 'Reversed Reward';
                }

                if (strpos($row->ref_number, 'converted'))
                {
                    $category = 'Cashback';
                    $itemName = 'Converted Points';
                }
                $quantity = $row->trx_quantity;
                $ptsRedeemed = $row->trx_pts_redeemed;
                $deliveryAmount = \PointsHelper::pointsToAmount($row->delivery_cost, $tot['currencyShortCode'], 1);
                if ($tot['currency'] == $row->t_currency_id)
                {
                    $amountSpentArr['amount'] = $row->t_original_total_amount;
                } else
                {
                    $amountSpentArr = CurrencyHelper::convertFromUSD($row->trx_total_amount, $tot['currencyShortCode']);
                }

                $amountSpent = $amountSpentArr['amount'] + $deliveryAmount;
            }
            $row->itemName = mb_strimwidth($itemName, 0, 77, '...');
            $row->normalized_mobile = strlen($row->normalized_mobile) > 13 ? mb_substr(trim($row->normalized_mobile), 0, 13) . '...' : $row->normalized_mobile;
            $row->quantity = $quantity;
            $row->ptsRedeemed = number_format($ptsRedeemed, 0, '.', ',');
            $row->amountSpent = number_format($amountSpent, 2);
        }

        return SSP::make($request, $repository->total(), $data);
    }

    public function report40Totals(RepositoryReport40 $repository)
    {
        $defaults = ReportHelper::normalizeDefaults();

        $memberId = null;
        $partnerIds = Auth::user()->managedPartnerIDs() ?: [];

        if ($defaults['member_id'][0] ?? null)
        {
            $memberId = $defaults['member_id'][0];
        } else
        {
            $partnerIds = array_intersect($defaults['entities'] ?: [], Auth::user()->managedPartnerIDs() ?: []);
        }

        $networkId = $defaults['network_currency'] != 'not' ? $defaults['network_currency'] : 1;

        $totals = $repository->startDate($defaults ['start_date'])
                             ->endDate($defaults['end_date'])
                             ->memberId($memberId)
                             ->networkId($networkId)
                             ->partnerId($partnerIds)
                             ->getTotals();

        return [
            'total_points_redeemed' => number_format($totals->total_points_redeemed),
            'total_cash_payment'    => number_format($totals->total_cash_payment, 2),
            'total_delivery_cost'   => number_format($totals->total_delivery_cost, 0),
            'total_amt_redeem'      => number_format($totals->total_amt_redeem, 2),
        ];
    }

    public function report44(Request $request)
    {
        $defaults = ReportHelper::normalizeDefaults();

        $currencyId = 17;
        $exchangeRate = 1;
        if ($defaults['currency'] > 0) {
            $currencyId = $defaults['currency'];
            $currency = Currency::find($currencyId);
            $exchangeRate = $currency->latestRate();
        }

        $query = Transaction::whereIn('transaction.store_id', $defaults['entities'])
                            ->whereBetween('transaction.created_at', [$defaults['start_date'], $defaults['end_date']])
                            ->whereIn('transaction.network_id', $defaults['network_ids']);


        $this->applyPartnersConstraints($query);

        $totals = $query->count('transaction.id');

        $data = $query->leftJoin('partner', 'transaction.partner_id', '=', 'partner.id')
                      ->leftJoin('users', 'transaction.user_id', '=', 'users.id')
                      ->leftJoin('reference', 'transaction.user_id', '=', 'reference.user_id')
                      ->select(
                          'transaction.id',
                          \DB::raw('DATE(transaction.created_at) created_at_date'),
                          'transaction.points_rewarded',
                          'transaction.amt_reward',
                          'transaction.total_amount',
                          'transaction.original_total_amount',
                          'transaction.currency_id',
                          'users.first_name as user_first_name',
                          'users.last_name as user_last_name',
                          'reference.number as reference_number'
                      )
                      ->limit($request['length'])
                      ->offset($request['start'])
                      ->orderBy('transaction.id', 'desc')
                      ->get()
                      ->each(function ($transaction) {
                          $transaction->points_rewarded = number_format($transaction->points_rewarded, 0);
                          $transaction->user_name = $transaction->user_first_name . ' ' . $transaction->user_last_name;
                          $transaction->reference_number = $transaction->reference_number ?? 'N/A';
                      })
                      ->toArray();

        foreach ($data as &$item) {
            $item['total_amount'] = round($item['total_amount'] * $exchangeRate, 2);
        }

        return SSP::make($request, $totals, $data);
    }

    public function report44Totals(Request $request)
    {
        $defaults = ReportHelper::normalizeDefaults();

        $query = Transaction::whereIn('store_id', $defaults['entities'])
                             ->whereBetween('created_at', [$defaults['start_date'], $defaults['end_date']])
                             ->whereIn('network_id', $defaults['network_ids']);
        $this->applyPartnersConstraints($query);

        $totals = $query->select(
                                 DB::raw('SUM(points_rewarded) as pts_reward'),
                                 DB::raw('SUM(points_redeemed) as pts_redeem'),
                                 DB::raw('SUM(total_amount) as amt_reward'),
                                 DB::raw('SUM(amt_redeem) as amt_redeem'),
                                 DB::raw('SUM(trx_reward) as trx_reward'),
                                 DB::raw('SUM(trx_redeem) as trx_redeem')
                             )
                             ->first()
                             ->toArray();

        $totals['trx_reward'] = number_format($totals['trx_reward']);
        $totals['trx_redeem'] = number_format($totals['trx_redeem']);
        $totals['pts_reward'] = number_format($totals['pts_reward'], 2);
        $totals['pts_redeem'] = number_format($totals['pts_redeem']);
        $totals['amt_reward'] = number_format($totals['amt_reward'] * $request->exchange_rate, 2);
        $totals['amt_redeem'] = number_format($totals['amt_redeem'] * $request->exchange_rate, 2);

        return $totals;
    }

    public function report45(Request $request)
    {
        $defaults = ReportHelper::normalizeDefaults();

        $query = Transaction::whereIn('transaction.store_id', $defaults['entities'])
                            ->whereBetween('transaction.created_at', [$defaults['start_date'], $defaults['end_date']]);
//                            ->whereIn('transaction.network_id', $defaults['network_ids']);
        $this->applyPartnersConstraints($query);

        $query->leftJoin('partner', 'transaction.partner_id', '=', 'partner.id')
              ->leftJoin('users', 'transaction.user_id', '=', 'users.id')
              ->leftJoin('reference', 'transaction.user_id', '=', 'reference.user_id')
              ->select(
                  'transaction.id',
                  \DB::raw('DATE(transaction.created_at) created_at_date'),
                  'transaction.points_redeemed',
                  'transaction.amt_redeem',
                  'transaction.total_amount',
                  'transaction.original_total_amount',
                  'transaction.currency_id',
                  'users.first_name as user_first_name',
                  'users.last_name as user_last_name',
                  'reference.number as reference_number'
              );

        if (!empty($defaults['network_ids'])) {
            $query->whereIn('network_id', $defaults['network_ids']);
        }

        $totals = $query->count('transaction.id');

        $data = $query->limit($request['length'])
                      ->offset($request['start'])
                      ->orderBy('transaction.id', 'desc')->get()
                      ->each(function ($transaction) {
                          $transaction->points_redeemed = number_format($transaction->points_redeemed, 0);
                          $transaction->user_name = $transaction->user_first_name . ' ' . $transaction->user_last_name;
                          $transaction->reference_number = $transaction->reference_number ?? 'N/A';
                      })
                      ->toArray();

        return SSP::make($request, $totals, $data);
    }

    public function report45Totals(Request $request)
    {
        $defaults = ReportHelper::normalizeDefaults();

        $totals = Transaction::whereIn('store_id', $defaults['entities'])
                             ->whereBetween('created_at', [$defaults['start_date'], $defaults['end_date']])
                             ->whereIn('network_id', $defaults['network_ids'])
                             ->select(
                                 DB::raw('SUM(points_rewarded) as pts_reward'),
                                 DB::raw('SUM(points_redeemed) as pts_redeem'),
                                 DB::raw('SUM(total_amount) as amt_reward'),
                                 DB::raw('SUM(amt_redeem) as amt_redeem'),
                                 DB::raw('SUM(trx_reward) as trx_reward'),
                                 DB::raw('SUM(trx_redeem) as trx_redeem')
                             )
                             ->first()
                             ->toArray();

        $totals['trx_reward'] = number_format($totals['trx_reward']);
        $totals['trx_redeem'] = number_format($totals['trx_redeem']);
        $totals['pts_reward'] = number_format($totals['pts_reward'], 2);
        $totals['pts_redeem'] = number_format($totals['pts_redeem']);
        $totals['amt_reward'] = number_format($totals['amt_reward'] * $request->exchange_rate, 2);
        $totals['amt_redeem'] = number_format($totals['amt_redeem'] * $request->exchange_rate, 2);

        return $totals;
    }

    public function report46(Request $request)
    {
        $defaults = ReportHelper::normalizeDefaults();
        $query = Transaction::whereIn('transaction.store_id', $defaults['entities'])
                            ->whereBetween('transaction.created_at', [$defaults['start_date'], $defaults['end_date']]);
//                            ->whereIn('transaction.network_id', $defaults['network_ids']);

        $query->leftJoin('partner', 'transaction.partner_id', '=', 'partner.id')
              ->leftJoin('product', 'transaction.product_id', '=', 'product.id')
              ->leftJoin('category', 'product.category_id', '=', 'category.id')
              ->select(
                  'category.id as category_id',
                  \DB::raw('SUM(transaction.points_rewarded) as points_rewarded'),
                  \DB::raw('SUM(transaction.amt_reward) as amt_reward'),
                  \DB::raw('SUM(transaction.total_amount) as total_amount'),
                  \DB::raw('SUM(transaction.original_total_amount) as original_total_amount'),
                  \DB::raw('COUNT(transaction.id) as transactions_total'),
                  'transaction.currency_id',
                  'category.name as category_name'
              );

        if (!empty($defaults['network_ids'])) {
            $query->whereIn('transaction.network_id', $defaults['network_ids']);
        }

        $data = $query->limit($request['length'])
                      ->offset($request['start'])
                      ->groupBy('category_name')
                      ->get()
                      ->each(function ($group) {
                          $group->points_rewarded = number_format($group->points_rewarded, 0);
                          $group->amt_reward = number_format($group->amt_reward, 2);
                          $group->category_id = $group->category_id ?? 'N/A';
                          $group->category_name = $group->category_name ?? 'N/A';
                          $group->transactions_total = number_format($group->transactions_total, 0);
                      })
                      ->toArray();

        $totals = count($data);

        return SSP::make($request, $totals, $data);
    }

    public function report46Totals(Request $request)
    {
        $defaults = ReportHelper::normalizeDefaults();

        $totals = Transaction::whereIn('store_id', $defaults['entities'])
                             ->whereBetween('created_at', [$defaults['start_date'], $defaults['end_date']])
                             ->whereIn('network_id', $defaults['network_ids'])
                             ->select(
                                 DB::raw('SUM(points_rewarded) as pts_reward'),
                                 DB::raw('SUM(points_redeemed) as pts_redeem'),
                                 DB::raw('SUM(total_amount) as amt_reward'),
                                 DB::raw('SUM(amt_redeem) as amt_redeem'),
                                 DB::raw('SUM(trx_reward) as trx_reward'),
                                 DB::raw('SUM(trx_redeem) as trx_redeem')
                             )
                             ->first()
                             ->toArray();

        $totals['trx_reward'] = number_format($totals['trx_reward']);
        $totals['trx_redeem'] = number_format($totals['trx_redeem']);
        $totals['pts_reward'] = number_format($totals['pts_reward'], 2);
        $totals['pts_redeem'] = number_format($totals['pts_redeem']);
        $totals['amt_reward'] = number_format($totals['amt_reward'] * $request->exchange_rate, 2);
        $totals['amt_redeem'] = number_format($totals['amt_redeem'] * $request->exchange_rate, 2);

        return $totals;
    }

    public function report47(Request $request)
    {
        $defaults = ReportHelper::normalizeDefaults();
        $query = Transaction::whereIn('transaction.store_id', $defaults['entities'])
                            ->whereBetween('transaction.created_at', [$defaults['start_date'], $defaults['end_date']])
                            ->whereIn('transaction.network_id', $defaults['network_ids']);

        $data = $query->leftJoin('partner', 'transaction.partner_id', '=', 'partner.id')
                      ->leftJoin('product', 'transaction.product_id', '=', 'product.id')
                      ->leftJoin('category', 'product.category_id', '=', 'category.id')
                      ->select(
                          'category.id as category_id',
                          \DB::raw('SUM(transaction.points_redeemed) as points_redeemed'),
                          \DB::raw('SUM(transaction.amt_redeem) as amt_redeem'),
                          \DB::raw('SUM(transaction.total_amount) as total_amount'),
                          \DB::raw('SUM(transaction.original_total_amount) as original_total_amount'),
                          \DB::raw('COUNT(transaction.id) as transactions_total'),
                          'transaction.currency_id',
                          'category.name as category_name'
                      )
                      ->limit($request['length'])
                      ->offset($request['start'])
                      ->groupBy('category_name')
                      ->get()
                      ->each(function ($group) {
                          $group->points_redeemed = number_format($group->points_redeemed, 0);
                          $group->amt_redeem = number_format($group->amt_redeem, 2);
                          $group->category_id = $group->category_id ?? 'N/A';
                          $group->category_name = $group->category_name ?? 'N/A';
                      })
                      ->toArray();

        $totals = count($data);

        return SSP::make($request, $totals, $data);
    }

    public function report47Totals(Request $request)
    {
        $defaults = ReportHelper::normalizeDefaults();

        $totals = Transaction::whereIn('store_id', $defaults['entities'])
                             ->whereBetween('created_at', [$defaults['start_date'], $defaults['end_date']])
                             ->whereIn('network_id', $defaults['network_ids'])
                             ->select(
                                 DB::raw('SUM(points_rewarded) as pts_reward'),
                                 DB::raw('SUM(points_redeemed) as pts_redeem'),
                                 DB::raw('SUM(total_amount) as amt_reward'),
                                 DB::raw('SUM(amt_redeem) as amt_redeem'),
                                 DB::raw('SUM(trx_reward) as trx_reward'),
                                 DB::raw('SUM(trx_redeem) as trx_redeem')
                             )
                             ->first()
                             ->toArray();

        $totals['trx_reward'] = number_format($totals['trx_reward']);
        $totals['trx_redeem'] = number_format($totals['trx_redeem']);
        $totals['pts_reward'] = number_format($totals['pts_reward'], 2);
        $totals['pts_redeem'] = number_format($totals['pts_redeem']);
        $totals['amt_reward'] = number_format($totals['amt_reward'] * $request->exchange_rate, 2);
        $totals['amt_redeem'] = number_format($totals['amt_redeem'] * $request->exchange_rate, 2);

        return $totals;
    }

    public function report48(Request $request)
    {
        $defaults = ReportHelper::normalizeDefaults();

        $query = Transaction::whereIn('transaction.store_id', $defaults['entities'])
                            ->whereBetween('transaction.created_at', [$defaults['start_date'], $defaults['end_date']])
                            ->whereIn('transaction.network_id', $defaults['network_ids'])
                            ->where('points_rewarded', '<', 0);

        $data = $query->leftJoin('partner', 'transaction.partner_id', '=', 'partner.id')
                      ->leftJoin('users', 'transaction.user_id', '=', 'users.id')
                      ->leftJoin('reference', 'transaction.user_id', '=', 'reference.user_id')
                      ->select(
                          \DB::raw('DATE(transaction.created_at) created_at_date'),
                          \DB::raw('SUM(transaction.points_rewarded) as points_rewarded'),
                          \DB::raw('SUM(transaction.amt_reward) as amt_reward'),
                          'transaction.currency_id',
                          'users.first_name as user_first_name',
                          'users.last_name as user_last_name',
                          'reference.number as reference_number',
                          \DB::raw('COUNT(transaction.id) as transactions_total')
                      )
                      ->limit($request['length'])
                      ->offset($request['start'])
                      ->groupBy('created_at_date')
                      ->groupBy('users.id')
                      ->get()
                      ->each(function ($transaction) {
                          $transaction->points_rewarded = number_format(abs($transaction->points_rewarded), 0);
                          $transaction->amt_reward = number_format(abs($transaction->amt_reward), 2);
                          $transaction->user_name = $transaction->user_first_name . ' ' . $transaction->user_last_name;
                          $transaction->reference_number = $transaction->reference_number ?? 'N/A';
                      })
                      ->toArray();

        $totals = count($data);

        return SSP::make($request, $totals, $data);
    }

    public function report48Totals(Request $request)
    {
        $defaults = ReportHelper::normalizeDefaults();

        $totals = Transaction::whereIn('store_id', $defaults['entities'])
                             ->whereBetween('created_at', [$defaults['start_date'], $defaults['end_date']])
                             ->whereIn('network_id', $defaults['network_ids'])
                             ->where('points_rewarded', '<', 0)
                             ->select(
                                 DB::raw('SUM(points_rewarded) as pts_reward'),
                                 DB::raw('SUM(amt_reward) as amt_reward'),
                                 DB::raw('COUNT(id) as transactions_total')
                             )
                             ->first()
                             ->toArray();

        $totals['pts_reward'] = number_format(abs($totals['pts_reward']), 2);
        $totals['amt_reward'] = number_format(abs($totals['amt_reward']), 2);

        return $totals;
    }

    public function report50(Request $request)
    {
        $defaults = ReportHelper::normalizeDefaults();

        $query = DB::table('users')
                   ->select(
                       DB::raw('SQL_CALC_FOUND_ROWS users.id'),
                       DB::raw('DATE_FORMAT(users.created_at, "%Y-%m-%d") as created_at_date'),
                       'reference.number as reference_number',
                       DB::raw('users.first_name || " " || users.last_name as name'),
                       'pos.name as pos_name'
                   )
                   ->leftJoin('reference', 'users.id', '=', 'reference.user_id')
                   ->leftJoin('pos', 'pos.id', '=', 'users.registration_source_pos')
                   ->leftJoin('partner_user', 'partner_user.user_id', '=', 'users.id')
                   ->leftJoin('network_partner', 'network_partner.partner_id', '=', 'partner_user.partner_id')
                   ->whereBetween('users.created_at', [$defaults['start_date'], $defaults['end_date']])
                   ->whereIn('pos.store_id', $defaults['entities'])
                   ->whereIn('network_partner.network_id', $defaults['network_ids'])
                   ->groupBy('users.id')
                   ->limit($request['length'])
                   ->offset($request['start']);

        $data = $query->get()->toArray();

        $data = array_slice($data, $request['start'], $request['length']);

        $totals = current(DB::select(DB::raw("SELECT FOUND_ROWS() AS total")))->total;

        return SSP::make($request, $totals, $data);
    }

    public function report50Totals(Request $request)
    {
        $defaults = ReportHelper::normalizeDefaults();

        $totals['customers_registered'] = User::whereBetween('users.created_at', [$defaults['start_date'], $defaults['end_date']])
                                              ->leftJoin('pos', 'users.registration_source_pos', '=', 'pos.id')
                                              ->leftJoin('partner_user', 'partner_user.user_id', '=', 'users.id')
                                              ->leftJoin('network_partner', 'network_partner.partner_id', '=', 'partner_user.partner_id')
                                              ->whereIn('network_partner.network_id', $defaults['network_ids'])
                                              ->whereIn('pos.store_id', $defaults['entities'])
                                              ->groupBy('users.id')
                                              ->get()
                                              ->count();

        return $totals;
    }

    public function report54(Request $request)
    {
        $defaults = ReportHelper::normalizeDefaults();
        $currencyId = 6;
        if ($defaults['network_currency'] != 'not') {
            $networkId  = $defaults['network_currency'];
        } else {
            $networkId  = 1;
        }
        $networkCurrencyId = Network::find($networkId)->currency_id;
        $network_currency = Currency::find($networkCurrencyId);
        $network_exchangeRate   = $network_currency->latestRate();
        $exchangeRate   = 1;
        $currencyShortCode  = 'USD';
        if ($defaults['currency'] > 0) {
            $currencyId = $defaults['currency'];
            $currency = Currency::find($currencyId);
            $exchangeRate   = $currency->latestRate();
            $currencyShortCode  = $currency->short_code;
        }

        $query = Transaction::whereIn('transaction.store_id', $defaults['entities'])
                            ->whereBetween('transaction.created_at', [$defaults['start_date'], $defaults['end_date']])
                            ->where('transaction.network_id', $networkId);
        $totals = $query->count('transaction.id');

        $data = $query->leftJoin('partner', 'transaction.partner_id', '=', 'partner.id')
                      ->leftJoin('store', 'transaction.store_id', '=', 'store.id')
                      ->select(
                          'transaction.id',
                          'transaction.posted_at',
                          'partner.name as partner_name',
                          'store.name as store_name',
                          'transaction.points_rewarded',
                          'transaction.points_redeemed',
                          'transaction.amt_reward',
                          'transaction.amt_redeem',
                          'transaction.total_amount',
                          'transaction.original_total_amount',
                          'transaction.currency_id'
                      )
                      ->limit($request['length'])
                      ->offset($request['start'])
                      ->orderBy('transaction.id', 'desc')
                      ->get()
                      ->toArray();

        return SSP::make($request, $totals, $data);
    }

    public function report55(Request $request)
    {
        $defaults = ReportHelper::normalizeDefaults();

        $query = DB::table('transaction')
                   ->leftJoin('partner', 'transaction.partner_id', '=', 'partner.id')
                   ->leftJoin('users', 'users.id', '=', 'transaction.user_id')
                   ->leftJoin('store', 'transaction.store_id', '=', 'store.id')
                   ->leftJoin('reference', 'transaction.user_id', '=', 'reference.user_id')
                   ->leftJoin('currency', 'currency.id', '=', 'transaction.currency_id')
                   ->leftJoin('currency as partner_currency', 'partner_currency.id', '=', 'transaction.partner_currency_id')
                   ->select(
                       'transaction.id',
                       'reference.number as reference_number',
                       'users.first_name',
                       'users.last_name',
                       'transaction.created_at',
                       'transaction.ref_number',
                       'transaction.points_rewarded',
                       'transaction.amt_reward',
                       'transaction.original_total_amount',
                       'currency.short_code as currency_short_code',
                       'transaction.partner_amount',
                       'partner_currency.short_code as partner_currency_short_code'
                   )
                   ->where('transaction.trx_reward', 1)
                   ->whereBetween('transaction.created_at', [$defaults['start_date'], $defaults['end_date']]);

        if (is_array($defaults['member_id']) && current($defaults['member_id']) != 0) {
            $query->whereIn('transaction.user_id', $defaults['member_id']);
        }

        if (!empty($defaults['network_ids'])) {
            $query->whereIn('transaction.network_id', $defaults['network_ids']);
        }

        if (!empty($defaults['entities'])) {
            $query->whereIn('transaction.partner_id', $defaults['entities']);
        }

        $data = $query->limit($request['length'])
                      ->offset($request['start'])
                      ->orderBy('transaction.id', 'desc')
                      ->get()
                      ->toArray();

//        $data = $query->get()->toArray();

        $data = array_slice($data, $request['start'], $request['length']);

        $totals = current(DB::select(DB::raw("SELECT FOUND_ROWS() AS total")))->total;

        return SSP::make($request, $totals, $data);
    }

    public function report55totals()
    {
        $defaults = ReportHelper::normalizeDefaults();

        $query = DB::table('transaction')
                   ->leftJoin('partner', 'transaction.partner_id', '=', 'partner.id')
                   ->leftJoin('users', 'users.id', '=', 'transaction.user_id')
                   ->leftJoin('store', 'transaction.store_id', '=', 'store.id')
                   ->leftJoin('reference', 'transaction.user_id', '=', 'reference.user_id')
                   ->leftJoin('currency', 'currency.id', '=', 'transaction.currency_id')
                   ->leftJoin('currency as partner_currency', 'partner_currency.id', '=', 'transaction.partner_currency_id')
                   ->select(
                       DB::raw('SUM(trx_reward) as total_reward_transactions'),
                       DB::raw('SUM(points_rewarded) as total_points_rewarded')
                   )
                   ->where('transaction.trx_reward', 1)
                   ->whereBetween('transaction.created_at', [$defaults['start_date'], $defaults['end_date']]);

        if (is_array($defaults['member_id']) && current($defaults['member_id']) != 0) {
            $query->whereIn('transaction.user_id', $defaults['member_id']);
        }

        if (!empty($defaults['network_ids'])) {
            $query->whereIn('transaction.network_id', $defaults['network_ids']);
        }

        if (!empty($defaults['entities'])) {
            $query->whereIn('transaction.partner_id', $defaults['entities']);
        }

        $results = $query->first();

        $totals = [
            'total_reward_transactions' => number_format($results->total_reward_transactions),
            'total_points_rewarded' => number_format($results->total_points_rewarded),
        ];

        return $totals;
    }
}
