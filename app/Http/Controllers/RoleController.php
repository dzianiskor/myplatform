<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\App;
use View;
use Input;
use Validator;
use I;
use Illuminate\Support\Facades\Auth;
use AuditHelper;
use App\User as User;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use App\Role as Role;
use App\Admin as Admin;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Role Controller
 * 
 * @category   Controllers
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class RoleController extends BaseController
{
    private $auth_user = null;

    public function __construct()
    {
        parent::__construct();

        $this->auth_user = Auth::User();
    }    

    /**
     * Create a new role and redirect to it
     *
     * @return Response
     */
    public function newRole()
    {
        if(!I::can('create_roles')){
            return Redirect::to(url("dashboard"));
        }
        // @TODO: Check if the user type, save role according to partner admin status
        
        $role = new Role();
        $role->name = "New Role";
        $role->save();
        $roleId = base64_encode($role->id);
        return Redirect::to(url("dashboard/roles/view/{$roleId}"));
    }

    /**
     * View/Edit the specified role
     *
     * @param int $id
     * @return Response
     */
    public function viewRole($enc_id = null)
    {
        $id = base64_decode($enc_id);
        if(!I::can('view_roles')){
            return Redirect::to(url("dashboard"));
        }
        $role = Role::find($id);

        if(!$role) {
            App::abort(404);
        }

        $permissions = array();
        $checked     = array_pluck($role->permissions->toArray(), 'id');

        foreach(DB::table('permission')->get() as $p) {
            $permissions[$p->group][] = array(
                'name'    => $p->name,
                'id'      => $p->id,
                'checked' => (in_array($p->id, $checked))? true : false
            );
        }

        return View::make('role.view', array(
            'role'        => $role,
            'permissions' => $permissions
        ));
    }

    /**
     * Update an existing role
     *
     * @param int $id
     * @return Response
     */
    public function updateRole($enc_id = null)
    {
        $id = base64_decode($enc_id);
        if(!I::can('edit_roles')){
            return Redirect::to(url("dashboard"));
        }
        $role = Role::find($id);

        if(!$role) {
            App::abort(404);
        }
        $input = Input::all();
        foreach($input as $k=>$i){
            $input[$k] = self::sanitizeText($i);
        }
        $validator = Validator::make($input, array(
            'name' => 'required|min:2'
        ));

        if($validator->fails()) {
            return Redirect::to(url("dashboard/roles/view/$id"))->withErrors($validator);
        }

        $role->name = $input['name'];
        $role->save();

        $inserts = isset($input['permission']) ? $input['permission'] : [];
        $role->permissions()->sync($inserts);

        return Redirect::to(Input::get('redirect_to', 'dashboard/roles'))->with('message', 'Role successfully updated');
    }

    /**
     * Delete a role
     *
     * @param int $id
     * @return Response
     */
    public function deleteRole($enc_id = null)
    {
        $id = base64_decode($enc_id);
        if(!I::can('delete_roles')){
            return Redirect::to(url("dashboard"));
        }
        $role = Role::find($id);

        if(!$role) {
            App::abort(404);
        }

        // Delete permissions
        $role->permissions()->detach();

        //AuditHelper::record(
        //    Auth::User()->firstPartner()->id, "Role '{$role->name}' deleted by '{Auth::User()->name}'"
        //);                                                              

        // Delete the actual role
        $role->delete();

        return Redirect::to(url('dashboard/roles'))->with('message', 'Role successfully deleted');
    }

    /**
     * Attach a role to a user account
     *
     * @param int $user_id
     * @return Response
     */
    public function attachRole($enc_user_id = null)
    {
        $user_id = base64_decode($enc_user_id);
        if(!I::can('view_roles')){
            return Redirect::to(url("dashboard"));
        }
        $role = Role::find(self::sanitizeText(Input::get('role_id')));

        if(!$role) {
            \App::abort(404);
        }

        $user = User::find($user_id);

        if(!$user) {
            \App::abort(404);
        }

        if(!$user->roles->contains($role)) {
            $user->roles()->attach($role);
        }

        //AuditHelper::record(
        //    Auth::User()->firstPartner()->id, "Role '{$role->name}' assigned to '{$user->id} / {$user->email}' by '{Auth::User()->name}'"
        //);                                                              

        return $this->listUserRoles($enc_user_id);
    }
    /**
     * Attach a role to a user account
     *
     * @param int $user_id
     * @return Response
     */
    public function attachAdminRole($enc_admin_id = null)
    {
        $admin_id = base64_decode($enc_admin_id);
        if(!I::can('view_roles')){
            return Redirect::to(url("dashboard"));
        }
        $role = Role::find(self::sanitizeText(Input::get('role_id')));

        if(!$role) {
            App::abort(404);
        }

        $user = Admin::find($admin_id);

        if(!$user) {
            App::abort(404);
        }

        if(!$user->roles->contains($role)) {
            $user->roles()->attach($role);
        }

        //AuditHelper::record(
        //    Auth::User()->firstPartner()->id, "Role '{$role->name}' assigned to '{$user->id} / {$user->email}' by '{Auth::User()->name}'"
        //);                                                              

        return $this->listAdminRoles($enc_admin_id);
    }

    /**
     * List all available roles
     *
     * @return Response
     */
    public function listRoles()
    {
        if(!I::can('view_roles')){
            return Redirect::to(url("dashboard"));
        }
        // @TODO: Fetch roles based on partner relationship

        $roles = DB::table('role');

        if (Input::has('q')) {
            $name = self::sanitizeText(Input::get('q'));
            $roles = $roles->where('name', 'like', "%{$name}%");
        }

        $roles = $roles->paginate(15);

        return View::make('role.list', [
            'roles' => $roles->appends(Input::except('page')),
        ]);
    }

    /**
     * Create a small LI list with user cards
     *
     * @param int $user_id
     * @return Response
     */
    public function listUserRoles($enc_user_id)
    {
        $user_id = base64_decode($enc_user_id);
        if(!I::can('view_roles')){
            return Redirect::to(url("dashboard"));
        }
        $user = User::find($user_id);

        return View::make('role.userlist', array(
            'user' => $user
        ));
    }
    
    /**
     * Create a small LI list with user cards
     *
     * @param int $user_id
     * @return Response
     */
    public function listAdminRoles($enc_admin_id)
    {
        $admin_id = base64_decode($enc_admin_id);
        if(!I::can('view_roles')){
            return Redirect::to(url("dashboard"));
        }
        $user = Admin::find($admin_id);

        return View::make('role.adminlist', array(
            'user' => $user
        ));
    }

    /**
     * Unlink the specified role from the user account
     *
     * @param int $user_id
     * @param int $card_id
     * @return Response
     */
    public function unlinkRole($enc_user_id = null, $enc_role_id = null)
    {
        $user_id = base64_decode($enc_user_id);
        $role_id = base64_decode($enc_role_id);

        if (!I::can('delete_roles')) {
            return Redirect::to(url("dashboard"));
        }
        $user = User::find($user_id);
        $role = Role::find($role_id);

        if(!$user) {
            \App::abort(404);
        }

        $user->roles()->detach($role_id);

        //AuditHelper::record(
        //    Auth::User()->firstPartner()->id, "Role '{$role->name}' revoked from '{$user->id} / {$user->email}' by '{Auth::User()->name}'"
        //);                                                                      

        return $this->listUserRoles($enc_user_id);
    }
    
    /**
     * Unlink the specified role from the user account
     *
     * @param int $user_id
     * @param int $card_id
     * @return Response
     */
    public function unlinkAdminRole($enc_admin_id = null, $enc_role_id = null)
    {
        $admin_id = base64_decode($enc_admin_id);
        $role_id = base64_decode($enc_role_id);
        if(!I::can('view_roles')){
            return Redirect::to(url("dashboard"));
        }
        $user = Admin::find($admin_id);
        $role = Role::find($role_id);

        if(!$user) {
            App::abort(404);
        }

        $user->roles()->detach($role_id);

        //AuditHelper::record(
        //    Auth::User()->firstPartner()->id, "Role '{$role->name}' revoked from '{$user->id} / {$user->email}' by '{Auth::User()->name}'"
        //);                                                                      

        return $this->listAdminRoles($enc_admin_id);
    }

} // EOC