<?php

namespace App\Http\Controllers;

use View;
use Input;
use Validator;
use Illuminate\Support\Facades\Password;
use App\Admin as Admin;
use App\PasswordHistory as PasswordHistory;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Hash;

/**
 * Reminders Controller
 *
 * @category   Controllers
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class RemindersController extends BaseController
{

    /**
     * Display the password reminder view.
     *
     * @return Response
     */
    public function getRemind()
    {
        $data = array(
            'body_class' => 'login'
        );

        return View::make('password.remind', $data);
    }

    /**
     * Handle a POST request to remind a user of their password.
     *
     * @return Response
     */
    public function postRemind()
    {
        $input = Input::all();
        foreach ($input as $k => $i) {
            $input[$k] = self::sanitizeText($i);
        }
        $validator = Validator::make($input, array(
            'email' => 'required|email'
        ));

        if ($validator->fails()) {
            return Redirect::to(url("password/remind"))->withErrors($validator);
        }

        $response = Password::sendResetLink(Input::only('email'));

        switch ($response) {
            case Password::INVALID_USER:
                return Redirect::back()->with('flash_error', Lang::get($response));
            case Password::RESET_LINK_SENT:
                return Redirect::back()->with('flash_status', Lang::get($response));
        }
    }

    /**
     * Display the password reset view for the given token.
     *
     * @param  string $token
     * @return Response
     */
    public function getReset($token = null)
    {
        if (is_null($token)) {
            App::abort(404);
        }

        return View::make('password.reset')->with('token', $token);
    }

    /**
     * Handle a POST request to reset a user's password.
     *
     * @return Response
     */
    public function postReset()
    {
        $credentials = Input::only(
            'email', 'password', 'password_confirmation', 'token'
        );
        $input = Input::all();
        foreach ($input as $k => $i) {
            $input[$k] = self::sanitizeText($i);
        }
        $validator = Validator::make($input, array(
            'email' => 'required|email',
            'password' => 'required',
            'password_confirmation' => 'required',
            'token' => 'required'
        ));

        if ($validator->fails()) {
            return Redirect::to(url("password/reset/" . $credentials['token']))->withErrors($validator);
        }

        $user = Admin::where('email', Input::get('email'))->first();

        if(PasswordHistory::isEqualOldPassowrds(Input::get('password'), $user->id)) {
            return Redirect::to(url("password/reset/" .$credentials['token']))->withErrors('Password should not equal any of the last 4 passwords');
        }

        $response = Password::reset($credentials, function ($user, $password) {
            $user->password = Hash::make($password);

            $user->save();
        });

        switch ($response) {
            case Password::INVALID_PASSWORD:
            case Password::INVALID_TOKEN:
            case Password::INVALID_USER:
                return Redirect::back()->with('error', Lang::get($response));

            case Password::PASSWORD_RESET:
                PasswordHistory::savePassword($user);
                return Redirect::to('/auth/login')->with('flash_status', 'Your password has been reset');
        }
    }

}