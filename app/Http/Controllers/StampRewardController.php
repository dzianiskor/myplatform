<?php
namespace App\Http\Controllers;
use View;
use Input;
use Validator;
use I;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use ManagedObjectsHelper;
use App\StampReward as StampReward;
use App\Stamp as Stamp;
/**
 * Stamp Controller
 *
 * @category   Controllers
 * @package    BLU
 * @author     Ralph Nader <ralph@bluloyalty.com>
 * @copyright  2015 BLU
 * @link       http://blupoints.com
 */
class StampRewardController extends BaseController
{
    protected $layout = "layouts.dashboard";

    private function getValidations()
    {
        return array(
            'name'       => 'required'
        );
    }

    /**
     * Get all list data
     *
     * @return array
     */
    private function getAllLists()
    {

        return array(
            'partners'         => ManagedObjectsHelper::managedPartners()->sortBy('name')->pluck('name', 'id'),
            

        );
    }

    /**
     * Display a list of the currencies
     *
     * @return void
     */
    public function listStamprewards()
    {
        if(!I::can('view_stampreward')){
            return Redirect::to(url("dashboard"));
        }
        $this->ormClass      = 'App\StampReward';
        $this->search_fields = array('name');
        
        $all_stamprewards = $this->getList();
        $all_managed_partners = ManagedObjectsHelper::managedPartners()->sortBy('name')->pluck('id');
        $allowed_stamprewards = array();
        $unique_stamprewards = array();
        
        //var_dump($all_managed_partners);
        foreach($all_stamprewards as $stamp_rew){
//            echo "<pre>";
//            var_dump($stamp_rew);
//            exit();

                if(in_array($stamp_rew->partner_id,$all_managed_partners->toArray())){
                    if(!in_array($stamp_rew->id, $unique_stamprewards)){
                        $allowed_stamprewards[] = $stamp_rew;
                        $unique_stamprewards[] = $stamp_rew->id;
                    }
                }
            
            //echo "<br><br><br>";
            
        }
        
        //exit();
        return View::make("stampreward.list", array(
            'stamprewards' => $allowed_stamprewards //$this->getList()
        ));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getCreate()
    {
        if(!I::can('create_stampreward')){
            return Redirect::to(url("dashboard"));
        }
        $stampreward           = new StampReward();
        $stampreward->name     = "New Stamp Reward";
        $stampreward->save();
        $id = base64_encode($stampreward->id);
        return Redirect::to(url("dashboard/stampreward/edit/$id"));
    }

    /**
     * Show the currency edit page
     *
     * @param integer $currency_id
     * @return Response
     */
    public function getEdit($id1)
    {
        if(!I::can('edit_stampreward')){
            return Redirect::to(url("dashboard"));
        }
        $id = base64_decode($id1);
        $stampreward = StampReward::find($id);

        if(!$stampreward) {
            App::abort(404);
        }
        
        $arr_stampreward = array('stampreward'=> $stampreward);
        $arr_results = array_merge( $this->getAllLists(), $arr_stampreward );

        return View::make("stampreward.view", $arr_results);
    }

    /**
     * Delete the currency
     *
     * @param integer $currency_id
     * @return void
     */
    public function getDelete($id1)
    {
        if(!I::can('delete_stampreward')){
            return Redirect::to(url("dashboard"));
        }
        $id = base64_decode($id1);
        $stampreward = StampReward::find($id);

        if(!$stampreward) {
            App::abort(404);
        }

        $stampreward->delete();

        return Redirect::to(url("dashboard/stampreward"));
    }

    /**
     * apply changes
     *
     * @param integer $currency_id
     * @return Response
     */
    public function postUpdate($id1)
    {
        if(!I::can('edit_stampreward')){
            return Redirect::to(url("dashboard"));
        }
        $id = base64_decode($id1);
        $stampreward = StampReward::find($id);

        if(!$stampreward) {
            App::abort(404);
        }

//        $validator = Validator::make(Input::all(), $this->getValidations());
//
//        if($validator->fails()) {
//            return Redirect::to("dashboard/stampreward/edit/$id")->withErrors($validator)->withInput();
//        }

        $stampreward->name           = self::sanitizeText(Input::get('name'));
        $stampreward->partner_id      = self::sanitizeText(Input::get('partner_id'));
        $stampreward->stamp_number      = self::sanitizeText(Input::get('stamp_number',0));

        if(Input::has('display_online')){
            $stampreward->display_online = self::sanitizeText(Input::get('display_online'));
        }
        else{
            $stampreward->display_online = '0';
        }

                


        if(Input::hasFile('image')) {
            $file = Input::file('image');
            $path = public_path('uploads');
            $ext  = strtolower($file->getClientOriginalExtension());
            $size = $file->getSize();
            $type = $file->getMimeType();
            $name = md5(time()).".{$ext}";

            if(in_array($ext, Meta::allowedImageExts() )) {
                Input::file('image')->move($path, $name);

                $f = new Media();
                $f->name = $name;
                $f->size = $size;
                $f->type = $type;
                $f->ext  = $ext;
                $f->save();

                $stampreward->image = $f->id;
            }
        }

        //$stampreward->media_id       = self::sanitizeText(Input::get('media_id'));


        $stampreward->save();

        return Redirect::to(url("dashboard/stampreward"));
    }

    
} // EOC