<?php
namespace App\Http\Controllers;
/**
 * Test Controller
 * 
 * @category   Controllers
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class TestController extends BaseController {

    /**
     * Only allow this to run if enable debugging is enabled
     * 
     * @return void
     */
    public function __construct()
    {
        if(!Config::get('blu.enable_debugging'))  {
            exit;
        }
    }

    /**
     * Load the index testing page
     * 
     * @return void
     */
    public function getIndex()
    {
        return View::make("testing.index", array(
            'members'    => $this->getMembers(),
            'partners'   => $this->getPartners(),
            'stores'     => $this->getStores(),
            'products'   => $this->getProducts(),
            'currencies' => $this->getCurrencies()
        ));
    }

    /**
     * Fetch a list of all members
     * 
     * @return void
     */
    private function getMembers()
    {
        return User::where('draft', false)->where('status', 'active')->orderBy('first_name')->get();
    }

    /**
     * Fetch a list of all partners 
     * 
     * @return void
     */
    private function getPartners()
    {
        return Partner::where('draft', false)->where('status', 'Enabled')->orderBy('name')->get();
    }    

    /**
     * Fetch a list of all stores
     * 
     * @return void
     */
    private function getStores()
    {
        return Store::orderBy('name')->get();
    }    

    /**
     * Fetch a list of all products
     * 
     * @return void
     */
    private function getProducts()
    {
        return Product::where('draft', false)->where('display_online', true)->orderBy('name')->get();
    }    

    /**
     * Fetch a list of all available currencies
     * 
     * @return void
     */
    private function getCurrencies()
    {
        return Currency::where('draft', false)->orderBy('name')->get();
    }        

    /**
     * Perform a reward transaction
     * 
     * @return void
     */
    public function postReward()
    {
        /*
        $result = Transaction::rewardTransaction(
            Input::get('amount', 0),
            Input::get('currency_code', 'USD'),
            Input::get('user_id'),
            Input::get('partner_id'),
            Input::get('items', array()),
            Input::get('store_id'),
            Input::get('pos_id'),
            Input::get('coupon_code'),
            Input::get('reference'),
            Input::get('invoice_num'),
            Input::get('notes'),
            Input::get('staff_id')
        );
        */

        $params = array(
            'user_id'     => Input::get('user_id'),
            'amount'      => Input::get('amount', 0),
            'currency'    => Input::get('currency_code', 'USD'),
            'partner'     => Input::get('partner_id'),
            'store_id'    => Input::get('store_id'), // Optional
            'pos_id'      => Input::get('pos_id'), // Optional
            'coupon_code' => Input::get('coupon_code'), // Optional
            'reference'   => Input::get('reference'), // Optional
            'invoice_num' => Input::get('invoice_num'), // Optional
            'notes'       => Input::get('notes'), // Optional
            'staff_id'    => Input::get('staff_id'), // Optional
        );

        $result = Transaction::doReward($params, 'Amount');

        echo "<pre>";
        print_r($result);
        echo "</pre>";
        exit;
    }

    /**
     * Perform a reward transaction
     * 
     * @return void
     */
    public function postRewardProduct()
    {
        /*
        $items = array(Input::get('product_id'));

        $result = Transaction::rewardTransaction(
            Input::get('amount', 0),
            Input::get('currency_code', 'USD'),
            Input::get('user_id'),
            Input::get('partner_id'),
            Input::get('items', $items),
            Input::get('store_id'),
            Input::get('pos_id'),
            Input::get('coupon_code'),
            Input::get('reference'),
            Input::get('invoice_num'),
            Input::get('notes'),
            Input::get('staff_id')
        );
        */
       
        $item = new \stdClass();
        $item->id = Input::get('product_id');
        $item->qty = 1;

        $params = array(
            'user_id'     => Input::get('user_id'),
            'items'      => array($item),
            'partner'     => Input::get('partner_id'),
            'store'       => Input::get('store_id'), // Optional
            'pos_id'      => Input::get('pos_id'), // Optional
            'coupon_code' => Input::get('coupon_code'), // Optional
            'reference'   => Input::get('reference'), // Optional
            'invoice_num' => Input::get('invoice_num'), // Optional
            'notes'       => Input::get('notes'), // Optional
            'staff_id'    => Input::get('staff_id'), // Optional
        );

        $result = Transaction::doReward($params, 'Amount');       

        echo "<pre>";
        print_r($result);
        echo "</pre>";
        exit;
    }    

    /**
     * Perform a redemption transactions
     * 
     * @return void
     */
    public function postRedemption()
    {
		// Get partner
		# $store = Store::find(Input::get('store_id'));
		# $partner = $store->partner;
	
		// Construct payload
		
		# $payload = array();
		
		# $product	  = new \stdClass();
		# $product->id  = Input::get('product_id');
		# $product->qty = Input::get('quantity');
		
		# $payload[] = $product;
		
		// Perform transaction
		
		# $result = Transaction::redemptionTransaction(
		# 	Input::get('user_id'), $payload, $partner->id, Input::get('source'), Input::get('reference'), Input::get('notes'), $store->id, Input::get('pos_id'), Input::get('staff_id')
		# );
    
        $store_id = Input::get('store_id');
        $store    = null;
        $partner  = null;

        if(Input::has('store_id')) {
            $store   = Store::find(Input::get('store_id'));
            $partner = $store->partner;        
        } else {
            $user    = User::find(Input::get('user_id'));
            $partner = $user->partners()->first();
        }

        if(!$partner) {
            echo "<pre>";
            print_r(array(
                'status' => 500,
                'message'=> "The selected store has no valid partners associated"
            ));
            echo "</pre>";
            die();            
        }

        // Create the product payload
        
        $product = new \stdClass();

        $product->id  = Input::get('product_id');
        $product->qty = Input::get('quantity');

        $params = array(
            'user_id'   => Input::get('user_id'),
            'partner'   => $partner->id,
            'items'     => array($product),
            'source'    => Input::get('source'), 
            'reference' => Input::get('reference'), 
            'notes'     => Input::get('notes'),
            'store_id'  => Input::get('store_id'),
            'pos_id'    => Input::get('pos_id'), 
            'staff_id'  => Input::get('staff_id')
        );

        $result = Transaction::doRedemption($params, "Items");

        echo "<pre>";
        print_r($result);
        echo "</pre>";
        die();
    }

    public function getDirect()
    {
        $store_id = 4528;
        $store    = null;
        $partner  = null;
        $store   = Store::find($store_id);
        $partner = $store->partner;   

        if(!$partner) {
            echo "<pre>";
            print_r(array(
                'status' => 500,
                'message'=> "The selected store has no valid partners associated"
            ));
            echo "</pre>";
            die();            
        }

        // Create the product payload
        
        $product = new \stdClass();

        $product->id  = 8282;
        $product->qty = 1;

        $params = array(
            'user_id'   => 27407,
            'partner'   => $partner->id,
            'items'     => array($product),
            'source'    => '', 
            'reference' => 'Transaction Test Tool', 
            'notes'     => '',
            'store_id'  => $store_id,
            'pos_id'    => null, 
            'staff_id'  => null
        );

        $result = Transaction::doRedemption($params, "Items");

        echo "<pre>";
        print_r($result);
        echo "</pre>";
        die();       
    }
    
    /**
     * Test the SMS Messaging Services
     * 
     */
    public function getTestMessages()
    {
        $sms = new EDS('blupoints', 'Blu@1234', 'Blupoints');

        $resp = $sms->send('+971506246221', "Test SMS from EDS - Server 1");


       // SMSHelper::send("Test Message", "+971506246221", $partner);

        /*
        $sms = new LibanCall(
            "Blue Solution", "Bl0ue", 525, "Test", "Test"
        );

        $resp = $sms->send('+971506246221', "Test SMS from LibanCall - Server 1");
        */

        echo "<pre>";
        print_r($resp);
        echo "</pre>";
        die();
    }

	public function getTestEmail()
	{
        return View::make("emails.redeem", array(
            'transaction'    => Transaction::first(),
            'user'   => User::first()
        ));
	}

} // EOC