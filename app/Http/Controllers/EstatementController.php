<?php
namespace App\Http\Controllers;
use App\Estatementtranslation;
use App\Offertranslation;
use App\Prodtranslation;
use App\Transaction;
use App\EstatementUser;
use App\Tier;
use App\TransactionDeduction;
use App\User;
use App\Product as Product;
use Illuminate\Support\Facades\App;
use View;
use Input;
use Validator;
use I;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Offer as Offer;
use App\Tiercriteria as Tiercriteria;
use ManagedObjectsHelper;
use Meta;
use App\Partner as Partner;
use App\Segment as Segment;
use App\Country as Country;
use App\Estatement as Estatement;
use App\Estatementitem as Estatementitem;
use App\EstatementHistory as EstatementHistory;
use Illuminate\Support\Facades\DB;
use Response;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use SendGrid;

/**
 * E-Statement Controller
 *
 * @category   Controllers
 * @package    BLU
 * @author     Magid Mroueh <magid.mroueh@bluloyalty.com>
 * @copyright  2016 BLU
 * @link       http://blupoints.com
 */
class EstatementController extends BaseController {

    protected $layout = "layouts.dashboard";

    /**
     * Get all list data
     *
     * @return array
     */
    private function getAllLists() {
//        $partner = Auth::User()->getTopLevelPartner()->id;
        $partners = Auth::User()->managedPartnerIDs();
        $arr_tiers = [];
        $tiersNames = [];
        $tiers1 = Tier::where('draft', false)->whereHas('Partner', function($q) use($partners) {
                    $q->whereIn('partner_id', $partners);
                })->orderBy('name')->get(['id', 'name']);
        if ($tiers1) {
            foreach ($tiers1 as $k => $v):
                array_push($arr_tiers, $v['id']);
                $tiersNames[$v['id']] = $v['name'];
            endforeach;
        }
        $tiers = Tiercriteria::where('draft', false)->whereHas('tier', function($q) use($arr_tiers) {
                    $q->whereIn('tier_id', $arr_tiers);
                })->orderBy('name')->get(['name', 'id', 'tier_id']);
        $tierCriteria = [];
        foreach ($tiers as $key => $value):
            $tierCriteria[$value['id']] = $value['name'] . " ( " . $tiersNames[$value['tier_id']] . " )";
        endforeach;
        return array(
            'partners' => ManagedObjectsHelper::managedPartners()->sortBy('name')->pluck('name', 'id'),
            'countries' => Country::orderBy('name')->pluck('name', 'id'),
            'segments' => ManagedObjectsHelper::managedSegments()->sortBy('name')->pluck('name', 'id'),
            'tiers' => $tiers,
            'itemTypes' => Meta::itemType(),
            'tiers2' => $tierCriteria,
            'tiersNames' => $tiersNames,
            'monthDays' => Meta::monthDays(),
            'statuses' => Meta::offerStatuses(),
        );
    }

    /**
     * Return the E-Statement validations
     *
     * @return array
     */
    private function getValidations() {
        return array(
            'title' => 'required',
        );
    }
    /**
     * Return Tiers by partner
     *
     * @return array
     */
    public function getTiersByPartner($partner = 1) {
        $html = '';
        $tiers1 = Tier::where('draft', false)->whereHas('Partner', function($q) use($partner) {
                    $q->where('partner_id', $partner);
                })->orderBy('name')->pluck('id');

        if ($tiers1) {
            $arr_tiers = $tiers1;
        }
        $tiers = Tiercriteria::where('draft', false)->whereHas('tier', function($q) use($arr_tiers) {
                    $q->whereIn('tier_id', $arr_tiers);
                })->orderBy('name')->pluck('name', 'id');
        foreach ($tiers as $id => $name) {
            $html .= '<option value = "' . $id . '">' . $name . '</option>';
        }
        return $html;
    }

    /**

    /**
     * Display a list of the estatements
     *
     * @return void
     */
    public function listEstatements() {
        if (!I::can('view_estatement')) {
            return Redirect::to(url("dashboard"));
        }

        $this->ormClass = 'App\Estatement';
        $this->defaultSort = 'id';
        $this->search_fields = array('title');
        if (I::am('BLU Admin') || I::belong(1)) {
            $eStatements = $this->getList();
            $this->ormClass = 'App\EstatementHistory';
            $this->defaultSort = 'id';
            $this->search_fields = array('title');
            $eStatementHistory = $this->getList();

            $page = Input::get('page');
            $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
            $eStatements = new LengthAwarePaginator($eStatements->forPage($page, 15), $eStatements->count(), 15, $page);
        } else {
            $partner = Auth::User()->getTopLevelPartner()->id;
            $eStatement = Estatement::select('estatement.*')->where('draft', 0)->where('deleted', 0);
            $eStatements = $eStatement->where('partner_id', $partner);

            $eStatementHistory = EstatementHistory::select('estatement_history.*')->where('draft', 0);
            $eStatementHistory = $eStatementHistory->where('partner_id', $partner);

//                        echo "<pre>";
//            print_r($estatements->toSql());
//            exit;
            if (Input::has('q')) {
                $q = self::sanitizeText(Input::get('q'));
                $eStatements = $eStatement->where('estatement.title', 'like', '%' . $q . '%');
            }

            if (Input::has('q_history')) {
                $q_history = self::sanitizeText(Input::get('q_history'));
                $eStatementHistory = $eStatementHistory->where('estatement.title', 'like', '%' . $q_history . '%');
            }

            // filter country_id
            if (Input::has('country_list')) {
                $q = explode(',', Input::get('country_list'));
                $eStatements = $eStatement->whereHas('countries', function($u) use($q) {
                    $u->whereIn('country_id', $q);
                });
            }

            // filter Status
            if (Input::has('estatement_status')) {
                $q = explode(',', Input::get('estatement_status'));
                $eStatements = $eStatement->whereIn('status', $q);
            }

            // filter partner
            if (Input::has('partner_estatement_list')) {
                $q = explode(',', Input::get('partner_estatement_list'));
                $eStatements = $eStatement->whereIn('partner_id', $q);
            }

            $eStatements = $eStatement->paginate(15);
            $eStatementHistory = $eStatementHistory->paginate(15);
        }

        return View::make("estatement.list", array(
                    'estatements' => $eStatements,
                    'estatementHistory' => $eStatementHistory,
                    'lists' => $this->getAllLists()
        ));
    }

    /**
     * get fields for new E-Statement
     *
     * @return void
     */
    public function newEstatement() {
        if (!I::can('create_estatement')) {
            return Redirect::to(url("dashboard"));
        }
        $partner = Auth::User()->getTopLevelPartner();

        $eStatement = new Estatement();

        $eStatement->title = 'Draft E-Statement';
        $eStatement->description = 'Draft Description';
        $eStatement->partner_id = $partner->id;
        $eStatement->delivery_date = 1;
        $eStatement->time = '00:00';
        $eStatement->draft = TRUE;
        $eStatement->status = 'active';
        $eStatement->created_at = date('Y-m-d H:i:s');
        $eStatement->updated_at = date('Y-m-d H:i:s');

        $eStatement->save();

        // Associate to partner location
        DB::table('country_estatement')->insert(array(
            'country_id' => $partner->country->id,
            'estatement_id' => $eStatement->id
        ));
        $eStatementId = base64_encode($eStatement->id);
        return Redirect::to(url("dashboard/estatement/view/{$eStatementId}"));
    }

    /**
     * View a single E-Statement entry
     *
     * @param int $id
     * @return void
     */
    public function viewEstatement($enc_id = null) {
        $id = base64_decode($enc_id);
        if (!I::can('view_estatement')) {
            return Redirect::to(url("dashboard"));
        }
        $eStatement = Estatement::find($id);

        if (!$eStatement) {
            App::abort(404);
        }

        $managedPartners = Auth::User()->managedPartners();
        $can_view_estatement = false;
        foreach ($managedPartners as $manP) {
            if ($manP->id == $eStatement->partner_id) {
                $can_view_estatement = true;
                break;
            }
        }
        if ($can_view_estatement == false && $eStatement->draft == false) {
            return Redirect::to(url("dashboard"));
        }
//        $eStatementItems = DB::table('estatement_items')->where('estatement_id', $id)->orderBy('rank', 'asc')->get();
        $eStatementItems = DB::table('estatement_items')->where('estatement_id', $id)->get();

        return View::make("estatement.view", array_merge(
                                $this->getAllLists(), array('estatement' => $eStatement, 'estatementitems' => $eStatementItems)
        ));
    }

    /**
     * View a single E-Statement History entry
     *
     * @param int $id
     * @return void
     */
    public function viewEstatementHistory($enc_id = null) {
        $id = base64_decode($enc_id);
        if (!I::can('view_estatement')) {
            return Redirect::to(url("dashboard"));
        }
        $eStatementHistory = EstatementHistory::find($id);

        if (!$eStatementHistory) {
            App::abort(404);
        }

        $managedPartners = Auth::User()->managedPartners();
        $can_view_estatement_history = false;
        foreach ($managedPartners as $manP) {
            if ($manP->id == $eStatementHistory->partner_id) {
                $can_view_estatement_history = true;
                break;
            }
        }
        if ($can_view_estatement_history == false && $eStatementHistory->draft == false) {
            return Redirect::to(url("dashboard"));
        }

        return View::make("estatement.view_history", array_merge(
                                $this->getAllLists(), array('estatementHistory' => $eStatementHistory)
        ));
    }

    /**
     * Delete a E-Statement instance
     *
     * @param int $id
     * @return void
     */
    public function deleteEstatement($enc_id = null) {
        $id = base64_decode($enc_id);
        if (!I::can('delete_estatement')) {
            return Redirect::to(url("dashboard"));
        }
        $eStatement = Estatement::find($id);

        if (!$eStatement) {
            \App::abort(404);
        }

//        $estatement->delete();
        $eStatement->deleted = 1;
        $eStatement->deleted_at = date('Y-m-d H:i:s');
        $eStatement->save();

        return Redirect::to('dashboard/estatement');
    }

    /**
     * Update a E-Statement
     *
     * @param int $id
     * @return void
     */
    public function saveEstatement($enc_id) {
        $id = base64_decode($enc_id);
        if (!I::can('create_estatement') && !I::can('edit_estatement')) {
            return Redirect::to(url("dashboard"));
        }
        $eStatement = Estatement::find($id);

        if (!$eStatement) {
            App::abort(404);
        }
        $input = Input::all();
        unset($input['redirect_to']);
        foreach ($input as $k => $i) {
            $input[$k] = self::sanitizeText($i);
        }
        $validator = Validator::make($input, $this->getValidations());

        if ($validator->fails()) {
            return Redirect::to(url("dashboard/estatement/view/{$enc_id}"))->withErrors($validator)->withInput();
        }

        foreach ($input as $k => $v) {
            if ($v == '') {
                unset($input[$k]);
            }
        }

        $eStatement->draft = false;
        $eStatement->update($input);

        return Redirect::to(Input::get('redirect_to', 'dashboard/estatement'));
    }

    /**
     * Create a small LI list with estatement segments
     *
     * @param int $estatement_id
     * @return Response
     */
    private function listSegments($enc_estatement_id) {
        $estatement_id = base64_decode($enc_estatement_id);
        $estatement = Estatement::find($estatement_id);
        return View::make('estatement.list.segmentList', array(
                    'estatement' => $estatement,
        ));
    }

    /**
     * link a segment to a estatement
     *
     * @param int $estatement id
     * @return void
     */
    public function postLink_segment($enc_id) {
        $id = base64_decode($enc_id);
        $segment_id = self::sanitizeText(Input::get('segment_id'));
        $estatement = Estatement::find($id);
        $segment = Segment::find($segment_id);

        if (!$estatement || !$segment) {
            App::abort(404);
        }

        if (!$estatement->segments->contains($segment)) {
            $estatement->segments()->attach($segment);
        }
        return $this->listSegments($enc_id);
    }

    /**
     * unlink a segment from a estatement
     *
     * @param int $estatement id, $segment_id
     * @return void
     */
    public function getUnlink_segment($enc_estatement_id, $enc_segment_id) {
        $estatement_id = base64_decode($enc_estatement_id);
        $segment_id = base64_decode($enc_segment_id);
        $estatement = Estatement::find($estatement_id);
        $segment = Segment::find($segment_id);

        if (!$estatement || !$segment) {
            App::abort(404);
        }

        $estatement->segments()->detach($segment_id);
        return $this->listSegments($enc_estatement_id);
    }

    /**
     * Create a small LI list with estatement countries
     *
     * @param int $estatement_id
     * @return Response
     */
    private function listCountries($enc_estatement_id) {
        $estatement_id = base64_decode($enc_estatement_id);
        $estatement = Estatement::find($estatement_id);

        return View::make('estatement.list.countryList', array(
                    'estatement' => $estatement,
        ));
    }

    /**
     * link a country to a estatement
     *
     * @param int $estatement id
     * @return void
     */
    public function postLink_country($enc_id) {
        $id = base64_decode($enc_id);
        $country_id = self::sanitizeText(Input::get('country_id'));
        $estatement = Estatement::find($id);
        $country = Country::find($country_id);

        if (!$estatement || !$country) {
            App::abort(404);
        }

        if (!$estatement->countries->contains($country)) {
            $estatement->countries()->attach($country_id);
        }
        return $this->listCountries($enc_id);
    }

    /**
     * unlink a country from a estatement
     *
     * @param int $estatement id, $country_id
     * @return void
     */
    public function getUnlink_country($enc_estatement_id, $enc_country_id) {
        $estatement_id = base64_decode($enc_estatement_id);
        $country_id = base64_decode($enc_country_id);
        $estatement = Estatement::find($estatement_id);
        $country = Country::find($country_id);

        if (!$estatement || !$country) {
            App::abort(404);
        }

        $estatement->countries()->detach($country_id);
        return $this->listCountries($enc_estatement_id);
    }

    /**
     * Load a list of Tiers associated to the specified estatement
     *
     * @param int $estatement_id
     * @return View
     */
    private function listTiers($enc_estatement_id) {
        $estatement_id = base64_decode($enc_estatement_id);
        $estatement = Estatement::find($estatement_id);
        $partners = Auth::User()->managedPartnerIDs();
        $tiersNames = [];
        $tiers1 = Tier::where('draft', false)->whereHas('Partner', function($q) use($partners) {
                    $q->whereIn('partner_id', $partners);
                })->orderBy('name')->get(['id', 'name']);
        if ($tiers1) {
            foreach ($tiers1 as $k => $v):
                $tiersNames[$v['id']] = $v['name'];
            endforeach;
        }

        return View::make('estatement.list.tierList', array(
                    'estatement' => $estatement,
                    'tiersNames' => $tiersNames
        ));
    }

    /**
     * Link the estatement to a partner tier
     *
     * @param int $estatement
     * @return View
     */
    public function postLink_tier($enc_estatement_id) {
        $estatement_id = base64_decode($enc_estatement_id);
        $tier_id = self::sanitizeText(Input::get('tier_id'));
        $estatement = Estatement::find($estatement_id);
        $tier = Tier::find($tier_id);

        if (!$estatement || !$tier) {
            App::abort(404);
        }

        DB::table('tier_estatement')->insert(array(
            'estatement_id' => $estatement_id,
            'tiercriteria_id' => $tier_id
        ));

        return $this->listTiers($enc_estatement_id);
    }

    /**
     * unlink a Tier from a estatement
     *
     * @param int $estatement id
     * @param int $tier_id
     * @return View
     */
    public function getUnlink_tier($enc_estatement_id, $enc_tier_id) {
        $estatement_id = base64_decode($enc_estatement_id);
        $tier_id = base64_decode($enc_tier_id);
        $estatement = Estatement::find($estatement_id);
        $tier = Tier::find($tier_id);

        if (!$estatement || !$tier) {
            App::abort(404);
        }

        DB::table('tier_estatement')->where('estatement_id', $estatement_id)
                ->where('tiercriteria_id', $tier_id)
                ->delete();

        return $this->listTiers($enc_estatement_id);
    }

    /**
     * Link the user to this E-statement
     *
     * @param string $enc_estatement_id
     * @return void
     */
    public function postLinkuser($enc_estatement_id) {
        $estatement = Estatement::find(base64_decode($enc_estatement_id));
        $user = User::find(self::sanitizeText(Input::get('user_id')));

        if (!$estatement || !$user) {
            App::abort(404);
        }

        if (!$estatement->users->contains($user)) {
            $mapping = new EstatementUser();

            $mapping->estatement_id = $estatement->id;
            $mapping->user_id = $user->id;
            $mapping->save();
        }

        return $this->listEstatementUsers($estatement->id);
    }

    /**
     * Unlink the user to this E-statement
     *
     * @param string $enc_estatement_id
     * @param strign $enc_user_id
     * @return View
     */
    public function getUnlinkuser($enc_estatement_id, $enc_user_id) {
        $estatement_id = base64_decode($enc_estatement_id);
        $user_id = base64_decode($enc_user_id);

        $mapping = EstatementUser::where('estatement_id', $estatement_id)->where('user_id', $user_id);
        $mapping->delete();

        // Get list
        $estatement = Estatement::find($estatement_id);
        $user = User::find($user_id);

        if (!$estatement || !$user) {
            App::abort(404);
        }

        return $this->listEstatementUsers($estatement->id);
    }

    /**
     * List the users that the notification belongs to
     *
     * @return View
     */
    private function listEstatementUsers($estatement_id) {
        $estatement = Estatement::find($estatement_id);

        return View::make("estatement.list.userList", array(
                    'estatement' => $estatement
        ));
    }

//E-Statement Items

    /**
     * Fetch the select drop down data for the E-Statment Items popup
     *
     * @return array
     */
    private function getItemSelectData() {
        $partner = Auth::User()->getTopLevelPartner()->id;
        return array(
            'itemTypes' => Meta::itemType(),
            'offers' => Offer::where('draft', false)->whereHas('partners', function($q) use($partner) {
                        $q->where('partner_id', $partner);
                    })->orderBy('name')->pluck('name', 'id')
        );
    }

    /**
     * Display the new estatementItem page
     *
     * @return View
     */
    public function newEstatementitem($enc_estatement_id = null) {
        $estatement_id = base64_decode($enc_estatement_id);
        $data = array(
            'estatement_id' => $estatement_id
        );

        return View::make("estatementitem.new", array(
                    'data' => $data, 'lists' => $this->getItemSelectData()
        ));
    }

    /**
     * Create a new prodtranslation resource
     *
     * @param int $prodtranslation_id
     * @return void
     */
    public function createEstatementitem() {
        $rules = array(
            'estatementitem_type' => 'required'
        );
        $input = Input::all();
        foreach ($input as $k => $i) {
            $input[$k] = self::sanitizeText($i);
        }
        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return Response::json(array(
                        'failed' => true,
                        'errors' => "Please ensure that you provide a valid estatementitem type"
            ));
        }

        $estatementranks = Estatementitem::where('estatement_id', self::sanitizeText(Input::get('estatementitem_eid')))->pluck('rank')->toArray();
        if (in_array(self::sanitizeText(Input::get('estatementitem_rank')), $estatementranks) || self::sanitizeText(Input::get('estatementitem_rank')) < 1) {
            return Response::json(array(
                        'failed' => true,
                        'errors' => "Please choose another rank"
            ));
        }

        $estatementitem = new Estatementitem();

        // Details
        $estatementitem->estatement_id = self::sanitizeText(Input::get('estatementitem_eid'));
        $estatementitem->type = self::sanitizeText(Input::get('estatementitem_type'));
        if (self::sanitizeText(Input::get('estatementitem_type')) == 'product') {
            $estatementitem->item_id = self::sanitizeText(Input::get('product_id'));
        } else {
            $estatementitem->item_id = self::sanitizeText(Input::get('offer_id'));
        }
        $estatementitem->rank = self::sanitizeText(Input::get('estatementitem_rank'));

        $estatementitem->save();
        $estatementItemId = base64_encode($estatementitem->estatement_id);
        return Response::json(array(
                    'failed' => false,
                    'view_url' => url('dashboard/estatementitem/list/' . $estatementItemId)
        ));
    }

    /**
     * Delete the specified estatementitem
     *
     * @return View
     */
    public function deleteEstatementitem($enc_estatementitem_id = null) {
        $estatementitem_id = base64_decode($enc_estatementitem_id);
        Estatementitem::find($estatementitem_id)->delete();

        return Response::json(array(
                    'failed' => false
        ));
    }

    /**
     * Render a list of estatementitem relevant to the Partner
     *
     * @param int $estatement_id
     * @return View
     */
    public function listEstatementitems($enc_estatement_id = null) {
        $estatement_id = base64_decode($enc_estatement_id);
        $data = array(
            'estatementitems' => Estatementitem::where('estatement_id', $estatement_id)->get(),
            'itemTypes' => Meta::itemType()
        );

        return View::make("estatementitem.list", $data);
    }

    //Send Test Email

    /**
     * Send Test Email
     *
     * @param int $estatement_id
     * @return View
     */
    public function getSendNow($enc_estatement_id = null, $lang = 'en', $test = 0) {
        $estatement_id = base64_decode($enc_estatement_id);
        $estatement = Estatement::find($estatement_id);
        $partner_id = $estatement->partner_id;
        $arr_users = array();
        foreach ($estatement->users as $u) {
            $arr_users[] = $u->id;
        }
        EstatementController::sendEmails($arr_users, $partner_id, $estatement_id);
        return 1;
//        return View::make("email.estatement_email", $data);
    }

    public static function getMiddlewareSensitiveInfo($userIds, $partner_id) {
        //$response = array();
        $admin_partner = Partner::find($partner_id);
        //$user_ids_for_middleware = $userIds;
        $all_middleware_users = array();
        $url = $admin_partner->link_middleware_api . '?method=sensitiveInfo&api_key=' . $admin_partner->middleware_api_key;

        $json_ids = json_encode($userIds);
        APIController::postSendEmailJson($json_ids);
        $post_fields = "user_ids=" . $json_ids;
        $curl = curl_init();
        // Set some options - we are passing in a useragent too here
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url,
            CURLOPT_SSL_VERIFYPEER => False,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $post_fields,
            CURLOPT_USERAGENT => 'Testing Sensitive Info'
        ));
        // Send the request & save response to $resp
        $resp = curl_exec($curl);
        $response = json_decode($resp, TRUE);

        return $response;
    }

    public static function sendEmails($users_all, $partner_id, $estatement_id) {
        $users1 = array_chunk($users_all, 900);
        foreach ($users1 as $users) {

            $image_url = "https://www.bluai.com";
            $mail = new SendGrid\Mail();
            $emailWhiteLabel = array();
            $partner = Partner::find($partner_id);
            $emailWhitelabelingData = $partner->emailwhitelabelings;
            $blu_partner = Partner::find(1);
            $bluEmailWhitelabelingData = $blu_partner->emailwhitelabelings;
            foreach ($emailWhitelabelingData as $emailWhitelabeling) {
                $emailWhiteLabel[$emailWhitelabeling->field][$emailWhitelabeling->lang] = $emailWhitelabeling->value;
            }

            foreach ($bluEmailWhitelabelingData as $emailWhitelabeling) {
                if (array_key_exists($emailWhitelabeling->field, $emailWhiteLabel) && array_key_exists($emailWhitelabeling->lang, $emailWhiteLabel[$emailWhitelabeling->field]) && $emailWhiteLabel[$emailWhitelabeling->field][$emailWhitelabeling->lang] !== "") {
                    continue;
                } else {
                    $emailWhiteLabel[$emailWhitelabeling->field][$emailWhitelabeling->lang] = $emailWhitelabeling->value;
                }
            }

            $templateId = "047ea374-325a-401a-b753-d37b8d0ed09c";
            $bsfTemplateId = "577e2910-deea-4946-b177-0bbdd84e4e58";

//            $mail->setTemplateId("577e2910-deea-4946-b177-0bbdd84e4e58"); //blu sendgrid rtl

            if ($partner_id == '4206') {
                $mail->setTemplateId($bsfTemplateId); //bsf
//                $mail->setTemplateId($bsfLiveTemplateId); //bsf
            } else {
//                $mail->setTemplateId($templateId); //Dynamic
                   $mail->setTemplateId($templateId); //bsf
            }

            $lang = 'en';
            $arr_members = array();
            $user_ids_used = $users;
            if ($partner->has_middleware == true) {
                //$user_ids_used = array();

                $resp_curl = self::getMiddlewareSensitiveInfo($user_ids_used, $partner_id);

                $Users_info = $resp_curl;

                foreach ($Users_info as $member) {
                    if (in_array($member['blu_id'], $user_ids_used)) {
                        $random = rand(0, 3);
                        //$email_random = $random_email[$random];
                        $arr_members[$member['blu_id']]['name'] = $member['first_name'] . " " . $member['last_name'];
                        $arr_members[$member['blu_id']]['lang'] = $member['preferred_language'];
                        $arr_members[$member['blu_id']]['email'] = $member['email'];
                        //$arr_members[$member['blu_id']]['email'] = 'yasmina.sayegh@gmail.com';
                        try {
                            $arr_temp = self::getUserDetails($member['blu_id'], $partner_id);
                            $arr_members[$member['blu_id']] = array_merge($arr_members[$member['blu_id']], $arr_temp);
                        } catch (Exception $ex) {
                            APIController::postSendEmailJson(json_encode(array($ex->getMessage(), $ex->getLine(), $member['blu_id'])), "Test 001");
                        }
                    }
                }
            } else {
                $Users_info = User::whereIn('id', $user_ids_used)->get();
                foreach ($Users_info as $member) {
                    $random = rand(0, 3);
                    //$email_random = $random_email[$random];
                    $arr_members[$member->id]['name'] = $member->first_name . " " . $member->last_name;
                    $arr_members[$member->id]['lang'] = "en";
                    $arr_members[$member->id]['email'] = $member->email;
                    //$arr_members[$member->id]['email'] = "yasmina.sayegh@gmail.com";
                    $arr_temp = self::getUserDetails($member->id, $partner_id);
                    $arr_members[$member->id] = array_merge($arr_members[$member->id], $arr_temp);
                }
            }

            $arr_not_sent = array();
            $arr_sent_email = array();
            foreach ($users as $u) {
                if (isset($arr_members[$u])) {
                    $user = $arr_members[$u];
                    if ($user['email'] !== "NA") {
                        //                  $lang = "ar";
                        $lang = $user['lang'];

                        if (strtolower($lang) == "ar") {
                            $align = "right";
                            $otheralign = "left";
                            $standard = array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9");
                            $arabic = array("٠", "١", "٢", "٣", "٤", "٥", "٦", "٧", "٨", "٩");
                            $months = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
                            $amonths = array("ليناير", "لفبراير", "لمارس", "لأبريل", "لمايو", "ليونيو", "ليوليو", "لأغسطس", "لسبتمبر", "لأكتوبر", "لنوفمبر", "لديسمبر");
                        } else {
                            $align = "left";
                            $otheralign = "right";
                        }

                        if (strtolower($lang) == 'en') {
                            $eStatementDetails = Estatement::find($estatement_id);
                            $eStatementItems = Estatementitem::where('estatement_id', $estatement_id)->orderBy('rank', 'asc')->get();
                        } else {
                            $eStatementDetails = Estatement::find($estatement_id);
                            $eStatementDetailsTranslation = Estatementtranslation::where('estatement_id', $eStatementDetails->id)->where('lang_id', 2)->first();
                            if ($eStatementDetailsTranslation) {
                                $eStatementDetails->title = $eStatementDetailsTranslation->Title;
                                $eStatementDetails->description = $eStatementDetailsTranslation->description;
                            }
                            $eStatementItems = Estatementitem::where('estatement_id', $estatement_id)->orderBy('rank', 'asc')->get();
                        }
                        //            $data = array('eStatement' => $eStatementDetails, 'items' => $eStatementItems, 'user' => $user, 'lang' => $lang);
                        $item = $eStatementItems{0};
                        $itemTitle = '';
                        $itemBody = '';
                        $itemImage = '';
                        if ($item->type == 'product') {
                            $product = Product::find($item->item_id);
                            if (strtolower($lang) != 'en') {
                                $productTranslation = Prodtranslation::where('product_id', $product->id)->where('lang_id', 2)->first();
                                if ($productTranslation) {
                                    $product->name = $productTranslation->name;
                                    $product->model = $productTranslation->model;
                                    $product->sub_model = $productTranslation->sub_model;
                                    $product->description = $productTranslation->description;
                                }
                            }
                            $itemTitle = $product->name;
                            $itemBody = $product->description;
                            $itemImage = $product->cover_image;
                        } else {
                            $offer = Offer::find($item->item_id);
                            if (strtolower($lang) != 'en') {
                                $offerTranslation = Offertranslation::where('offer_id', $offer->id)->where('lang_id', 2)->first();
                                if ($offerTranslation) {
                                    $offer->name = $offerTranslation->name;
                                    $offer->short_description = $offerTranslation->short_description;
                                    $offer->description = $offerTranslation->description;
                                    $offer->terms_and_condition = $offerTranslation->terms_and_condition;
                                    $offer->email_text_promo_1 = $offerTranslation->email_text_promo_1;
                                    $offer->email_text_promo_2 = $offerTranslation->email_text_promo_2;
                                }
                            }
                            $itemTitle = $offer->name;
                            $itemBody = $offer->email_text_promo_1;
                            $itemImage = $offer->cover_image;
                        }


                        $personalization = new SendGrid\Personalization();

                        $email = new SendGrid\Email(null, $user['email']);
                        $personalization->addTo($email);

                        $personalization->addSubstitution("@membername@", DashboardController::platformTranslateKeysForLanguage('lbl-estat-membername', strtolower($lang)));
                        $personalization->addSubstitution("@pointsbalance@", DashboardController::platformTranslateKeysForLanguage('lbl-estat-pointsbalance', strtolower($lang)));
                        if ($partner_id == '4206') {
                            $personalization->addSubstitution("@pointseligable@", DashboardController::platformTranslateKeysForLanguage('lbl-estat-eligiblepoints', strtolower($lang)));
                        }
                        if ($partner_id == '4173') {
                            $personalization->addSubstitution("@imageWidthLogo@", "width:180px;");
                        }else{
                              $personalization->addSubstitution("@imageWidthLogo@", "width:110px;");
                        }

                        $personalization->addSubstitution("@accountstatus@", DashboardController::platformTranslateKeysForLanguage('lbl-estat-status', strtolower($lang)));
                        $personalization->addSubstitution("@periodlabel@", DashboardController::platformTranslateKeysForLanguage('lbl-estat-period', strtolower($lang)));
                        $personalization->addSubstitution("@openingbalancelabel@", DashboardController::platformTranslateKeysForLanguage('lbl-estat-openingbalance', strtolower($lang)));
                        $personalization->addSubstitution("@pointsearnedlabel@", DashboardController::platformTranslateKeysForLanguage('lbl-estat-pointsearned', strtolower($lang)));
                        $personalization->addSubstitution("@pointsredeemedlabel@", DashboardController::platformTranslateKeysForLanguage('lbl-estat-pointsredeemed', strtolower($lang)));
                        $personalization->addSubstitution("@pointsexpiredpreviouslabel@", DashboardController::platformTranslateKeysForLanguage('lbl-estat-pointsexpired', strtolower($lang)));
                        $personalization->addSubstitution("@pointsexpirednextlabel@", DashboardController::platformTranslateKeysForLanguage('lbl-estat-nextexpiry', strtolower($lang)));
                        $personalization->addSubstitution("@pointsstatementlabel@", DashboardController::platformTranslateKeysForLanguage('lbl-estat-pointsstatement', strtolower($lang)));
                        $personalization->addSubstitution("@viewaccountlabel@", DashboardController::platformTranslateKeysForLanguage('lbl-estat-viewaccount', strtolower($lang)));
                        $personalization->addSubstitution("@align@", $align);
                        $personalization->addSubstitution("@otheralign@", $otheralign);
                        if (strtolower($lang) == "ar") {
                            if ($partner_id == '4206') {
                                $personalization->addSubstitution("@eligable-points@", str_replace($standard, $arabic, number_format($user['cashback_balance'], 0)));
                            }
                            $personalization->addSubstitution("@period@", str_replace($standard, $arabic, date("m/Y", strtotime("first day of previous month"))));
                            $personalization->addSubstitution("@opening-balance@", str_replace($standard, $arabic, number_format($user['opening_balance'], 0)));
                            $personalization->addSubstitution("@points-earned@", str_replace($standard, $arabic, number_format($user['sum_rewarded_points'], 0)));
                            $personalization->addSubstitution("@points-redeemed@", str_replace($standard, $arabic, number_format($user['sum_redeemed_points'], 0)));
                            $personalization->addSubstitution("@points-expired-month@", str_replace($standard, $arabic, number_format($user['sum_expired_points'], 0)));
                            $personalization->addSubstitution("@next-period-points@", str_replace($standard, $arabic, number_format($user['sum_expiring_points'], 0)));
                            $personalization->addSubstitution("@points-balance@", str_replace($standard, $arabic, number_format($user['balance'], 0)));
                            $personalization->addSubstitution("@next-period-month@", str_replace('January 1st', '١ يناير', $user['next_expiry']) . ' ' . str_replace($standard, $arabic, date('Y', strtotime('+1 year'))));
                            $personalization->addSubstitution("@previous-month@", str_replace($standard, $arabic, date("m/Y", strtotime("first day of previous month"))));
                            $personalization->addSubstitution("@direction-rtl@", " html, body{direction:rtl; font-family: 'adobe-arabic' !important}");
                            $personalization->addSubstitution("@style-rtl@", "direction: rtl;");
                            $personalization->addSubstitution("@style-rtl2@", "dir: rtl");
                            $personalization->addSubstitution("@style-rtl1@", "style='direction: rtl;'");
                            $personalization->addSubstitution("@direction-outlook-rtl@", " html, body{direction:ltr; font-family: 'adobe-arabic' !important}");
                            $personalization->addSubstitution("@copyRightPartner@", $emailWhiteLabel['copyright']['en']);
                            $personalization->addSubstitution("@partnerName@", $emailWhiteLabel['program_name']['ar']);
                        } else {
                            $personalization->addSubstitution("@period@", date("m/Y", strtotime("first day of previous month")));
                            $personalization->addSubstitution("@points-balance@", number_format($user['balance'], 0));
                            if ($partner_id == '4206') {
                                $personalization->addSubstitution("@eligable-points@", number_format($user['cashback_balance'], 0));
                            }
                            $personalization->addSubstitution("@opening-balance@", number_format($user['opening_balance'], 0));
                            $personalization->addSubstitution("@points-earned@", number_format($user['sum_rewarded_points'], 0));
                            $personalization->addSubstitution("@points-redeemed@", number_format($user['sum_redeemed_points'], 0));
                            $personalization->addSubstitution("@points-expired-month@", number_format($user['sum_expired_points'], 0));
                            $personalization->addSubstitution("@next-period-points@", number_format($user['sum_expiring_points'], 0));
                            $personalization->addSubstitution("@next-period-month@", $user['next_expiry'] . ' ' . date('Y', strtotime('+1 year')));
                            $personalization->addSubstitution("@previous-month@", date("m/Y", strtotime("first day of previous month")));
                            $personalization->addSubstitution("@style-rtl@", " ");
                            $personalization->addSubstitution("@style-rtl1@", " ");
                            $personalization->addSubstitution("@direction-rtl@", "");
                            $personalization->addSubstitution("@style-rtl2@", "");
                            $personalization->addSubstitution("@direction-outlook-rtl@", "");
                            $personalization->addSubstitution("@copyRightPartner@", $emailWhiteLabel['copyright']['en']);
                            $personalization->addSubstitution("@partnerName@", $emailWhiteLabel['program_name']['en']);
                        }
                        $personalization->addSubstitution("@partnerLogo1@", "https://www.bluai.com/media/image/" . $emailWhiteLabel['image']['en']);

                        if (!empty($partner->color)) {
                            $personalization->addSubstitution("@primaryColor@", $partner->color);
                        } else {
                            $personalization->addSubstitution("@primaryColor@", $blu_partner->color);
                        }
                        if (!empty($partner->secondary_color)) {
                            $personalization->addSubstitution("@secondaryColor@", $partner->secondary_color);
                        } else {
                            $personalization->addSubstitution("@secondaryColor@", $blu_partner->secondary_color);
                        }
                        $personalization->addSubstitution("@name@", $user['name']);
                        $personalization->addSubstitution("@account-status@", $user['status']);
                        $personalization->addSubstitution("@title-1@", $itemTitle);
                        $personalization->addSubstitution("@body-1@", $itemBody);
                        $personalization->addSubstitution("@image-1@", $image_url . "/media/image/" . $itemImage);
                        $personalization->addSubstitution("@title-2@", $eStatementDetails->title);
                        $personalization->addSubstitution("@body-2@", $eStatementDetails->description);
                        $personalization->addSubstitution("@view-account-link@", $emailWhiteLabel['website_url']['en'] . "/account/statement");

                        $personalization->addSubstitution("@logo-1@", "https://www.fransijana.com.sa/public/img/logos/Jana-Logo.png");
                        $personalization->addSubstitution("@logo-2@", "https://www.fransijana.com.sa/public/img/logos/BSF-logo-website.png");

                        $i = 1;
                        foreach ($eStatementItems as $item) {
                            if ($item->rank != 1) {
                                if ($item->type == 'product') {
                                    $product = Product::find($item->item_id);
                                    if (strtolower($lang) != 'en') {
                                        $productTranslation = Prodtranslation::where('product_id', $product->id)->where('lang_id', 2)->first();
                                        if ($productTranslation) {
                                            $product->name = $productTranslation->name;
                                            $product->model = $productTranslation->model;
                                            $product->sub_model = $productTranslation->sub_model;
                                            $product->description = $productTranslation->description;
                                        }
                                    }
                                    $itemTitle = $product->name;
                                    $itemBody = $product->description;
                                    $itemImage = $product->cover_image;
                                } else {
                                    $offer = Offer::find($item->item_id);
                                    if (strtolower($lang) != 'en') {
                                        $offerTranslation = Offertranslation::where('offer_id', $offer->id)->where('lang_id', 2)->first();
                                        if ($offerTranslation) {
                                            $offer->name = $offerTranslation->name;
                                            $offer->short_description = $offerTranslation->short_description;
                                            $offer->description = $offerTranslation->description;
                                            $offer->terms_and_condition = $offerTranslation->terms_and_condition;
                                            $offer->email_text_promo_1 = $offerTranslation->email_text_promo_1;
                                            $offer->email_text_promo_2 = $offerTranslation->email_text_promo_2;
                                        }
                                    }
                                    $itemTitle = $offer->name;
                                    $itemBody = $offer->email_text_promo_2;
                                    $itemImage = $offer->cover_image;
                                }

                                $personalization->addSubstitution("@offers-title" . $i . "@", $itemTitle);
                                $personalization->addSubstitution("@offers-description" . $i . "@", $itemBody);
                                $personalization->addSubstitution("@offers-image" . $i . "@", $image_url . "/media/image/" . $itemImage);
                                $i++;
                            } else {
                                continue;
                            }
                        }
                        $mail->addPersonalization($personalization);

//                        $email2 = new SendGrid\Email((strtolower($lang) == "ar") ? $emailWhiteLabel['partner_name_content']['ar'] . " Rewards" : $emailWhiteLabel['partner_name_content']['en'] . " Rewards", $emailWhiteLabel['support_email']['en']);
                        $email2 = new SendGrid\Email($emailWhiteLabel['partner_name_content']['en'] . " Rewards", $emailWhiteLabel['support_email']['en']);
                        $mail->setFrom($email2);

                        if (strtolower($lang) == 'en') {
                            $personalization->setSubject($emailWhiteLabel['partner_name_subject']['en'] . ' ' . date("F Y", strtotime("first day of previous month")) . " E-Statement");
                        } else {
                           $personalization->setSubject("كشف حساب نقاط " . $emailWhiteLabel['partner_name_subject']['ar'] . " " . str_replace($standard, $arabic, str_replace($months, $amonths, date("F Y", strtotime("first day of previous month")))));
                        }
                    }
                    $arr_sent_email[] = $u;
                } else {
                    $arr_not_sent[] = $u;
                }
            }
            APIController::postSendEmailJson(json_encode($arr_sent_email), "Users estatement sent");
            APIController::postSendEmailJson(json_encode($arr_not_sent), "Users where estatement not sent");
            //        $sendgrid = new SendGrid(
            //                Config::get('sendgrid.username'), Config::get('sendgrid.password')
            //        );
            $apiKey = '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'; //blu api
            //        $apiKey = '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';  //rabih api
            $sendgrid = new SendGrid($apiKey);
            $response = $sendgrid->client->mail()->send()->post($mail);
            unset($mail);
        }
        return 1;
    }

    public static function sendEmailsRtl($enc_estatement_id = null, $lang = 'en', $test = 0) {

        $estatement_id = base64_decode($enc_estatement_id);
        $estatement = Estatement::find($estatement_id);
        $partner_id = $estatement->partner_id;

        $image_url = "https://bluai.com";
        $mail = new SendGrid\Mail();

//      $mail->setTemplateId("9ac5e6fd-944a-48b2-a8fc-f2404d226fdd"); //rabih sendgrid
        $mail->setTemplateId("0217f113-6a0b-4851-b80d-8bb304dfc200"); //rabih sendgrid
//      $mail->setTemplateId("f0036b25-8b1e-48a5-b103-688d35b7a738"); //rabih sendgrid test
//        $mail->setTemplateId("047ea374-325a-401a-b753-d37b8d0ed09c"); blu sendgrid
        $lang = 'en';

        foreach ($estatement->users as $u) {
            if (gettype($estatement->users) == "array") {
                $user = self::getUserDetails($u, $partner_id);
            } else {
                $user = self::getUserDetails($u->id, $partner_id);
            }

            if ($user['email'] !== "NA") {
//                  $lang = "en";
                $lang = $user['lang'];

                if (strtolower($lang) == "ar") {
                    $align = "right";
                    $otheralign = "left";
                    $standard = array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9");
                    $arabic = array("٠", "١", "٢", "٣", "٤", "٥", "٦", "٧", "٨", "٩");
                    $months = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
                    $amonths = array("ليناير", "لفبراير", "لمارس", "لأبريل", "لمايو", "ليونيو", "ليوليو", "لأغسطس", "لسبتمبر", "لأكتوبر", "لنوفمبر", "لديسمبر");
                } else {
                    $align = "left";
                    $otheralign = "right";
                }

                if (strtolower($lang) == 'en') {
                    $eStatementDetails = Estatement::find($estatement_id);
                    $eStatementItems = Estatementitem::where('estatement_id', $estatement_id)->orderBy('rank', 'asc')->get();
                } else {
                    $eStatementDetails = Estatement::find($estatement_id);
                    $eStatementDetailsTranslation = Estatementtranslation::where('estatement_id', $eStatementDetails->id)->where('lang_id', 2)->first();
                    if ($eStatementDetailsTranslation) {
                        $eStatementDetails->title = $eStatementDetailsTranslation->Title;
                        $eStatementDetails->description = $eStatementDetailsTranslation->description;
                    }
                    $eStatementItems = Estatementitem::where('estatement_id', $estatement_id)->orderBy('rank', 'asc')->get();
                }
//            $data = array('eStatement' => $eStatementDetails, 'items' => $eStatementItems, 'user' => $user, 'lang' => $lang);
                $item = $eStatementItems{0};
                $itemTitle = '';
                $itemBody = '';
                $itemImage = '';
                if ($item->type == 'product') {
                    $product = Product::find($item->item_id);
                    if (strtolower($lang) != 'en') {
                        $productTranslation = Prodtranslation::where('product_id', $product->id)->where('lang_id', 2)->first();
                        if ($productTranslation) {
                            $product->name = $productTranslation->name;
                            $product->model = $productTranslation->model;
                            $product->sub_model = $productTranslation->sub_model;
                            $product->description = $productTranslation->description;
                        }
                    }
                    $itemTitle = $product->name;
                    $itemBody = $product->description;
                    $itemImage = $product->cover_image;
                } else {
                    $offer = Offer::find($item->item_id);
                    if (strtolower($lang) != 'en') {
                        $offerTranslation = Offertranslation::where('offer_id', $offer->id)->where('lang_id', 2)->first();
                        if ($offerTranslation) {
                            $offer->name = $offerTranslation->name;
                            $offer->short_description = $offerTranslation->short_description;
                            $offer->description = $offerTranslation->description;
                            $offer->terms_and_condition = $offerTranslation->terms_and_condition;
                            $offer->email_text_promo_1 = $offerTranslation->email_text_promo_1;
                            $offer->email_text_promo_2 = $offerTranslation->email_text_promo_2;
                        }
                    }
                    $itemTitle = $offer->name;
                    $itemBody = $offer->email_text_promo_1;
                    $itemImage = $offer->cover_image;
                }


                $personalization = new SendGrid\Personalization();

                $email = new SendGrid\Email(null, $user['email']);
                $personalization->addTo($email);

                $personalization->addSubstitution("@membername@", DashboardController::platformTranslateKeysForLanguage('lbl-estat-membername', strtolower($lang)));
                $personalization->addSubstitution("@pointsbalance@", DashboardController::platformTranslateKeysForLanguage('lbl-estat-pointsbalance', strtolower($lang)));
                $personalization->addSubstitution("@pointseligable@", DashboardController::platformTranslateKeysForLanguage('lbl-estat-eligiblepoints', strtolower($lang)));
                $personalization->addSubstitution("@accountstatus@", DashboardController::platformTranslateKeysForLanguage('lbl-estat-status', strtolower($lang)));
                $personalization->addSubstitution("@periodlabel@", DashboardController::platformTranslateKeysForLanguage('lbl-estat-period', strtolower($lang)));
                $personalization->addSubstitution("@openingbalancelabel@", DashboardController::platformTranslateKeysForLanguage('lbl-estat-openingbalance', strtolower($lang)));
                $personalization->addSubstitution("@pointsearnedlabel@", DashboardController::platformTranslateKeysForLanguage('lbl-estat-pointsearned', strtolower($lang)));
                $personalization->addSubstitution("@pointsredeemedlabel@", DashboardController::platformTranslateKeysForLanguage('lbl-estat-pointsredeemed', strtolower($lang)));
                $personalization->addSubstitution("@pointsexpiredpreviouslabel@", DashboardController::platformTranslateKeysForLanguage('lbl-estat-pointsexpired', strtolower($lang)));
                $personalization->addSubstitution("@pointsexpirednextlabel@", DashboardController::platformTranslateKeysForLanguage('lbl-estat-nextexpiry', strtolower($lang)));
                $personalization->addSubstitution("@pointsstatementlabel@", DashboardController::platformTranslateKeysForLanguage('lbl-estat-pointsstatement', strtolower($lang)));
                $personalization->addSubstitution("@viewaccountlabel@", DashboardController::platformTranslateKeysForLanguage('lbl-estat-viewaccount', strtolower($lang)));
                $personalization->addSubstitution("@align@", $align);
                $personalization->addSubstitution("@otheralign@", $otheralign);
                if (strtolower($lang) == "ar") {
                    $personalization->addSubstitution("@eligable-points@", str_replace($standard, $arabic, number_format($user['cashback_balance'], 0)));
                    $personalization->addSubstitution("@period@", str_replace($standard, $arabic, date("m/Y", strtotime("first day of previous month"))));
                    $personalization->addSubstitution("@opening-balance@", str_replace($standard, $arabic, number_format($user['opening_balance'], 0)));
                    $personalization->addSubstitution("@points-earned@", str_replace($standard, $arabic, number_format($user['sum_rewarded_points'], 0)));
                    $personalization->addSubstitution("@points-redeemed@", str_replace($standard, $arabic, number_format($user['sum_redeemed_points'], 0)));
                    $personalization->addSubstitution("@points-expired-month@", str_replace($standard, $arabic, number_format($user['sum_expired_points'], 0)));
                    $personalization->addSubstitution("@next-period-points@", str_replace($standard, $arabic, number_format($user['sum_expiring_points'], 0)));
                    $personalization->addSubstitution("@points-balance@", str_replace($standard, $arabic, number_format($user['balance'], 0)));
                    $personalization->addSubstitution("@eligable-points@", str_replace($standard, $arabic, number_format($user['cashback_balance'], 0)));
                    $personalization->addSubstitution("@next-period-month@", str_replace('January 1st', '١ يناير', $user['next_expiry']) . ' ' . str_replace($standard, $arabic, date('Y', strtotime('+1 year'))));
                    $personalization->addSubstitution("@previous-month@", str_replace($standard, $arabic, date("m/Y", strtotime("first day of previous month"))));
                    $personalization->addSubstitution("@direction-rtl@", " html, body{direction:rtl; font-family: 'adobe-arabic' !important}");
                    $personalization->addSubstitution("@style-rtl@", "direction: rtl;");
                    $personalization->addSubstitution("@style-rtl2@", "dir: rtl");
                    $personalization->addSubstitution("@style-rtl1@", "style='direction: rtl;'");
                } else {
                    $personalization->addSubstitution("@period@", date("m/Y", strtotime("first day of previous month")));
                    $personalization->addSubstitution("@points-balance@", number_format($user['balance'], 0));
                    $personalization->addSubstitution("@eligable-points@", number_format($user['cashback_balance'], 0));
                    $personalization->addSubstitution("@opening-balance@", number_format($user['opening_balance'], 0));
                    $personalization->addSubstitution("@points-earned@", number_format($user['sum_rewarded_points'], 0));
                    $personalization->addSubstitution("@points-redeemed@", number_format($user['sum_redeemed_points'], 0));
                    $personalization->addSubstitution("@points-expired-month@", number_format($user['sum_expired_points'], 0));
                    $personalization->addSubstitution("@next-period-points@", number_format($user['sum_expiring_points'], 0));
                    $personalization->addSubstitution("@next-period-month@", $user['next_expiry'] . ' ' . date('Y', strtotime('+1 year')));
                    $personalization->addSubstitution("@previous-month@", date("m/Y", strtotime("first day of previous month")));
                    $personalization->addSubstitution("@style-rtl@", " ");
                    $personalization->addSubstitution("@style-rtl1@", " ");
                    $personalization->addSubstitution("@direction-rtl@", "");
                    $personalization->addSubstitution("@style-rtl2@", "");
                }
                $personalization->addSubstitution("@name@", $user['name']);
                $personalization->addSubstitution("@account-status@", $user['status']);
                $personalization->addSubstitution("@title-1@", $itemTitle);
                $personalization->addSubstitution("@body-1@", $itemBody);
                $personalization->addSubstitution("@image-1@", $image_url . "/media/image/" . $itemImage);
                $personalization->addSubstitution("@title-2@", $eStatementDetails->title);
                $personalization->addSubstitution("@body-2@", $eStatementDetails->description);
                $personalization->addSubstitution("@view-account-link@", "http://fransijana.com.sa/account/statement");
                $personalization->addSubstitution("@logo-1@", "https://www.fransijana.com.sa/public/img/logos/Jana-Logo.png");
                $personalization->addSubstitution("@logo-2@", "https://www.fransijana.com.sa/public/img/logos/BSF-logo-website.png");

                $i = 1;
                foreach ($eStatementItems as $item) {
                    if ($item->rank != 1) {
                        if ($item->type == 'product') {
                            $product = Product::find($item->item_id);
                            if (strtolower($lang) != 'en') {
                                $productTranslation = Prodtranslation::where('product_id', $product->id)->where('lang_id', 2)->first();
                                if ($productTranslation) {
                                    $product->name = $productTranslation->name;
                                    $product->model = $productTranslation->model;
                                    $product->sub_model = $productTranslation->sub_model;
                                    $product->description = $productTranslation->description;
                                }
                            }
                            $itemTitle = $product->name;
                            $itemBody = $product->description;
                            $itemImage = $product->cover_image;
                        } else {
                            $offer = Offer::find($item->item_id);
                            if (strtolower($lang) != 'en') {
                                $offerTranslation = Offertranslation::where('offer_id', $offer->id)->where('lang_id', 2)->first();
                                if ($offerTranslation) {
                                    $offer->name = $offerTranslation->name;
                                    $offer->short_description = $offerTranslation->short_description;
                                    $offer->description = $offerTranslation->description;
                                    $offer->terms_and_condition = $offerTranslation->terms_and_condition;
                                    $offer->email_text_promo_1 = $offerTranslation->email_text_promo_1;
                                    $offer->email_text_promo_2 = $offerTranslation->email_text_promo_2;
                                }
                            }
                            $itemTitle = $offer->name;
                            $itemBody = $offer->email_text_promo_2;
                            $itemImage = $offer->cover_image;
                        }

                        $personalization->addSubstitution("@offers-title" . $i . "@", $itemTitle);
                        $personalization->addSubstitution("@offers-description" . $i . "@", $itemBody);
                        $personalization->addSubstitution("@offers-image" . $i . "@", $image_url . "/media/image/" . $itemImage);
                        $i++;
                    } else {
                        continue;
                    }
                }
                $mail->addPersonalization($personalization);

                $email2 = new SendGrid\Email('JANA Rewards ', "fransijanasupport@alfransi.com.sa");
                $mail->setFrom($email2);

                if (strtolower($lang) == 'en') {
                    $personalization->setSubject("JANA Rewards Test " . date("F Y", strtotime("first day of previous month")) . " E-Statement");
                } else {
                    $personalization->setSubject("كشف حساب نقاط جنى " . str_replace($standard, $arabic, str_replace($months, $amonths, date("F Y", strtotime("first day of previous month")))));
                }
            }
        }
//        $sendgrid = new SendGrid(
//                Config::get('sendgrid.username'), Config::get('sendgrid.password')
//        );
//      $apiKey = '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'; blu api
        $apiKey = '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';  //rabih api
        $sendgrid = new SendGrid($apiKey);
        $response = $sendgrid->client->mail()->send()->post($mail);
        return 1;
    }

    /**
     * Send Test Email
     *
     * @param int $estatement_id
     * @return View
     */
    public function sendBulkEstatement($enc_estatement_id = null, $lang = 'en', $test = 0) {
        $estatement_id = base64_decode($enc_estatement_id);
        $estatement = Estatement::find($estatement_id);
        $partner_id = $estatement->partner_id;

        return EstatementController::sendEmails($estatement->users, $partner_id, $estatement_id);
    }

    public static function fireBulkEstatement($enc_estatement_id = null, $lang = 'en', $test = 0) {
        $estatement_id = base64_decode($enc_estatement_id);
        $estatement = Estatement::find($estatement_id);
        $partner_id = $estatement->partner_id;
        $partner = Partner::find($partner_id);
        $part_users = $partner->members;

        $segments = $estatement->segments;
        $arr_segments = array();
        if ($segments) {
            foreach ($segments as $s) {
                $arr_segments[] = $s->id;
//            $seg = Segment::find($s->id);
//            $seg_users = $seg->users;
            }
        }
        $empty_segments = false;
        if (count($arr_segments) <= 0) {
            $empty_segments = True;
        }
        $countries = $estatement->countries;
        $arr_countries = array();
        $empty_countries = false;
        if ($countries) {
            foreach ($countries as $countr) {
                $arr_countries[] = $countr->id;
            }
        }
        if (count($arr_countries) <= 0) {
            $empty_countries = True;
        }

        $tiers = $estatement->tiers;
        $arr_tiers = array();
        $empty_tiers = false;
        if ($tiers) {
            foreach ($tiers as $tier) {
                $arr_tiers[] = $tier->id;
            }
        }
        if (count($arr_tiers) <= 0) {
            $empty_tiers = True;
        }
        $arr_pu = array();
        $count_users = 0;


        foreach ($part_users as $su) {

            $pass_countries = false;
            $pass_segments = false;
            $pass_tiers = false;
            if (array_search($su->id, $arr_pu) === False) {

                $user = User::find($su->id);
                $user_tiers = $user->tiercriteria;
                $user_segments = $user->segments;
                if ($empty_countries === true) {
                    $pass_countries = true;
                } elseif (in_array($su->country_id, $arr_countries)) {
                    $pass_countries = true;
                }

                if ($empty_segments === true) {
                    $pass_segments = true;
                } elseif ($empty_segments === false) {
                    foreach ($user_segments as $us) {
                        if (in_array($us->id, $arr_segments)) {
                            $pass_segments = true;
                            break;
                        }
                    }
                }
                if ($empty_tiers === true) {
                    $pass_tiers = true;
                } elseif ($empty_tiers === false) {
                    foreach ($user_tiers as $ut) {
                        if (in_array($ut->id, $arr_tiers)) {
                            $pass_tiers = true;
                        }
                    }
                }
                if ($pass_segments === true) { //if ($pass_countries === true && $pass_segments === true && $pass_tiers === true) {
                    $arr_pu[] = $su->id;
                }
                $count_users +=1;
            }
        }
        APIController::postSendEmailJson(json_encode($arr_pu), "Array PU");

        return EstatementController::sendEmails($arr_pu, $partner_id, $estatement_id);
    }

    //Send out estatement

    /**
     * Send out estatement
     *
     * @param int $estatement_id
     * @return View
     */
    public static function getSendOutEstatement($enc_estatement_id = null, $lang = 'en', $test = 0) {
        $estatement_id = base64_decode($enc_estatement_id);
        $estatement = Estatement::find($estatement_id);
        $partner_id = $estatement->partner_id;
        $partner = Partner::find($partner_id);
        $part_users = $partner->members;

        $segments = $estatement->segments;
        $arr_segments = array();
        foreach ($segments as $s) {
            $arr_segments[] = $s->id;
//            $seg = Segment::find($s->id);
//            $seg_users = $seg->users;
        }
        $empty_segments = false;
        if (count($arr_segments) <= 0) {
            $empty_segments = True;
        }
        $countries = $estatement->countries;
        $arr_countries = array();
        $empty_countries = false;
        foreach ($countries as $countr) {
            $arr_countries[] = $countr->id;
        }
        if (count($arr_countries) <= 0) {
            $empty_countries = True;
        }

        $tiers = $estatement->tiers;
        $arr_tiers = array();
        $empty_tiers = false;
        foreach ($tiers as $tier) {
            $arr_tiers[] = $tier->id;
        }
        if (count($arr_tiers) <= 0) {
            $empty_tiers = True;
        }
        $arr_pu = array();
        $count_users = 0;
        foreach ($part_users as $su) {
            $pass_countries = false;
            $pass_segments = false;
            $pass_tiers = false;
            if (array_search($su->id, $arr_pu) === False) {

                $user = User::find($su->id);
                $user_tiers = $user->tiers;
                $user_segments = $user->segments;
                if ($empty_countries === true) {
                    $pass_countries = true;
                } elseif (in_array($su->country_id, $arr_countries)) {
                    $pass_countries = true;
                }
                if ($empty_segments === true) {
                    $pass_segments = true;
                } elseif ($empty_segments === false) {
                    foreach ($user_segments as $us) {
                        if (in_array($us->id, $arr_segments)) {
                            $pass_segments = true;
                            break;
                        }
                    }
                }
                if ($empty_tiers === true) {
                    $pass_tiers = true;
                } elseif ($empty_tiers === false) {
                    foreach ($user_tiers as $ut) {
                        if (in_array($ut->id, $arr_tiers)) {
                            $pass_tiers = true;
                        }
                    }
                }
                if ($pass_countries === true && $pass_segments === true && $pass_tiers === true) {
                    $arr_pu[] = $su->id;
                }
                $count_users +=1;
            }
        }

        foreach ($arr_pu as $u) {
            $user = self::getUserDetails($u, $partner_id);

            $lang = $user['lang'];
            if (strtolower($lang) == 'en') {
                $eStatementDetails = Estatement::find($estatement_id);
                $eStatementItems = Estatementitem::where('estatement_id', $estatement_id)->orderBy('rank', 'asc')->get();
            } else {
                $eStatementDetails = Estatement::find($estatement_id);
                $eStatementDetailsTranslation = Estatementtranslation::where('estatement_id', $eStatementDetails->id)->where('lang_id', 2)->first();
                if ($eStatementDetailsTranslation) {
                    $eStatementDetails->title = $eStatementDetailsTranslation->Title;
                    $eStatementDetails->description = $eStatementDetailsTranslation->description;
                }
                $eStatementItems = Estatementitem::where('estatement_id', $estatement_id)->orderBy('rank', 'asc')->get();
            }
            $data = array('eStatement' => $eStatementDetails, 'items' => $eStatementItems, 'user' => $user, 'lang' => $lang);
//            $json_data = json_encode($data);
//            APIController::postSendEmailJson($json_data);
            if ($user['email'] !== "NA") {
                //exit();

                $sendgrid = new SendGrid(
                        Config::get('sendgrid.username'), Config::get('sendgrid.password')
                );
                $mail = new SendGrid\Email();
                $email_template = View::make("emails.estatement_email", $data)->render();
                //$mail->setTos(array($user['email']));
                $mail->setTos(array('adminit@bluloyalty.com'));
                //$mail->setFrom(Config::get('blu.email_from'));
                $mail->setFrom("fransijanasupport@alfransi.com.sa");
                $mail->setFromName('JANA Rewards');
                if (strtolower($lang) == 'en') {
                    $mail->setSubject("JANA Rewards " . date("F Y", strtotime("first day of previous month")) . " E-Statement");
                } else {
                    $standard = array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9");
                    $arabic = array("٠", "١", "٢", "٣", "٤", "٥", "٦", "٧", "٨", "٩");
                    $months = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
                    $amonths = array("ليناير", "لفبراير", "لمارس", "لأبريل", "لمايو", "ليونيو", "ليوليو", "لأغسطس", "لسبتمبر", "لأكتوبر", "لنوفمبر", "لديسمبر");
                    $mail->setSubject("كشف حساب نقاط جنى " . str_replace($standard, $arabic, str_replace($months, $amonths, date("F Y", strtotime("first day of previous month")))));
                }
                $mail->setHtml($email_template);
                $sendgrid->send($mail);
            }
        }
        //$arr_sent = array('count'=>count($arr_pu));
        //$json_data = json_encode($arr_sent);
        //APIController::postSendEmailJson($json_data);
        return 1;
//      return View::make("email.estatement_email", $data);
    }

    public static function getUserDetails($user_id, $partner_id) {

        $partner = Partner::find($partner_id);
        $user = User::find($user_id);



//            if($partner->has_middleware == 1){
//                $arr_user[] = $user_id;
//                $res_middleware_user = App\classes\ReportHelper::getMiddlewareSensitiveInfo($arr_user);
//                $first_name = $res_middleware_user['first_name'];
//                $last_name = $res_middleware_user['last_name'];
//            }
        $partners = $partner->managedObjects();
        $partners_children = array();
        foreach ($partners as $part) {
            $partners_children[] = $part->id;
        }
//            $partners_children = array(4317, 4325, 4326, 4327);
//            if (!$user) {
//                return self::respondWith(
//                    500, "The PIN provided is not valid"
//                );
//            }
        $month_back_date = date('Y-m-01');
        $now_date = date('Y-m-d');
        $start_month_date = date("Y-m-d", strtotime("first day of previous month"));
        $end_month_date = date("Y-m-d", strtotime("last day of previous month"));

        $arr_response = array();
        $balance = $user->balance($partner->firstNetwork()->id);

        $sum_current_redeemed_points = Transaction::select(DB::raw('sum(points_redeemed) pts_redeemed'))->where('trx_redeem', 1)->where('user_id', $user_id)->where('network_id', $partner->firstNetwork()->id)->where('created_at', '>=', $month_back_date)->where('created_at', '<=', $now_date)->first();
        $sum_current_rewarded_points = Transaction::select(DB::raw('sum(points_rewarded) pts_rewarded'))->where('expired', 0)->where('user_id', $user_id)->where('network_id', $partner->firstNetwork()->id)->where('created_at', '>=', $month_back_date)->where('created_at', '<=', $now_date)->first();

        $sum_expired_points = Transaction::select(DB::raw('sum(points_rewarded) pts_rewarded'))->where('expired', 1)->where('user_id', $user_id)->where('network_id', $partner->firstNetwork()->id)->first();
        $sum_redeemed_points = Transaction::select(DB::raw('sum(points_redeemed) pts_redeemed'))->where('trx_redeem', 1)->where('user_id', $user_id)->where('network_id', $partner->firstNetwork()->id)->where('created_at', '>=', $start_month_date)->where('created_at', '<=', $now_date)->first();
        $sum_rewarded_points = Transaction::select(DB::raw('sum(points_rewarded) pts_rewarded'))->where('expired', 0)->where('user_id', $user_id)->where('network_id', $partner->firstNetwork()->id)->where('created_at', '>=', $start_month_date)->where('created_at', '<=', $now_date)->first();
        $sum_rewarded_points_end_of_lastmonth = Transaction::select(DB::raw('sum(points_rewarded) pts_rewarded'))->where('expired', 0)->where('user_id', $user_id)->where('network_id', $partner->firstNetwork()->id)->where('created_at', '>=', $start_month_date)->where('created_at', '<=', $end_month_date)->first();
        $sum_redeemed_points_end_of_lastmonth = Transaction::select(DB::raw('sum(points_redeemed) pts_redeemed'))->where('trx_redeem', 1)->where('user_id', $user_id)->where('network_id', $partner->firstNetwork()->id)->where('created_at', '>=', $start_month_date)->where('created_at', '<=', $end_month_date)->first();
        $user_tier = $user->tiercriteria()->get();

        $tierC_expiry = 0;
        if (!is_numeric($sum_expired_points->pts_rewarded)) {
            $sum_expired_points->pts_rewarded = 0;
        }
        if (!is_numeric($sum_current_rewarded_points->pts_rewarded)) {
            $sum_current_rewarded_points->pts_rewarded = 0;
        }
        if (!is_numeric($sum_current_redeemed_points->pts_redeemed)) {
            $sum_current_redeemed_points->pts_redeemed = 0;
        }
        if (!is_numeric($sum_rewarded_points->pts_rewarded)) {
            $sum_rewarded_points->pts_rewarded = 0;
        }
        if (!is_numeric($sum_redeemed_points->pts_redeemed)) {
            $sum_redeemed_points->pts_redeemed = 0;
        }

        $monthName = 'January 1st';
        $composed_date = '';
        $max_date = date('Y-m-d', strtotime('-1 year'));
        if (isset($user_tier)) {
//                    var_dump($user_tier);
//                    exit();
            foreach ($user_tier as $tierCriterion) {


                if (!is_null($tierCriterion->expiry_in_months)) {
                    $parent_tier = Tier::find($tierCriterion->tier_id);

                    if (!isset($parent_tier->start_month)) {
                        continue;
                    }

                    $monthNum = $parent_tier->start_month;
                    $dateObj = DateTime::createFromFormat('!m', $monthNum);
                    $current_mday = intval(date('md'));
                    $tier_mday = intval($monthNum . "01");
                    if ($current_mday > $tier_mday) {
                        $year = date('Y', strtotime('+1 year'));
                    } else {
                        $year = date('Y');
                    }
                    $monthDisplay = $monthNum;
                    if (strlen($monthDisplay) < 2) {
                        $monthDisplay = "0" . $monthDisplay;
                    }
                    $composed_date = $year . "-" . $monthDisplay . "-01";
                    $monthName = $dateObj->format('F') . " 1st"; // March


                    $tierC_expiry = $tierCriterion->expiry_in_months;
                    $max_timestamp = strtotime("-" . $tierC_expiry . " month", strtotime($composed_date));
                    $max_date = date("Y-m-d", $max_timestamp);
                    $arr_response['expiry_period'] = $tierC_expiry;
                } else {
                    $arr_response['expiry_period'] = 0;
                    $arr_response['next_expiry'] = 'January 1st';
                }
            }
        } else {
            $arr_response['expiry_period'] = 0;
            $arr_response['next_expiry'] = 'January 1st';
        }

        $sum_expiring_points = Transaction::select(DB::raw('sum(points_rewarded) pts_rewarded'))->where('expired', 0)->where('user_id', $user_id)->where('network_id', $partner->firstNetwork()->id)->where('created_at', '<', $max_date)->first();
        if (is_null($sum_expiring_points->pts_rewarded)) {
            $sum_expiring_points->pts_rewarded = 0;
        }

        $arr_response['balance'] = $balance - ($sum_current_rewarded_points->pts_rewarded - $sum_current_redeemed_points->pts_redeemed);
        $arr_response['sum_expired_points'] = $sum_expired_points->pts_rewarded;
        $arr_response['sum_redeemed_points'] = $sum_redeemed_points_end_of_lastmonth->pts_redeemed;
        $arr_response['expiry_period'] = $tierC_expiry;
        $arr_response['next_expiry'] = $monthName;
        $arr_response['sum_expiring_points'] = $sum_expiring_points->pts_rewarded;
        $arr_response['status'] = $user->status;
        $arr_response['sum_rewarded_points'] = $sum_rewarded_points_end_of_lastmonth->pts_rewarded;

        $arr_response['opening_balance'] = $balance - ($sum_rewarded_points->pts_rewarded - $sum_redeemed_points->pts_redeemed);
        if ($partner->has_middleware == true) {
            $arr_response['cashback_balance'] = self::getCashbackPoints($user_id, $partner_id);
        }

        return $arr_response;
    }

    public static function getCashbackPoints($user_id, $partner_id) {
        $partner1 = Partner::find($partner_id);
        $partners = $partner1->managedObjects();
        $arr_trxs_user_reward_ids = array();
        $end_of_last_month_date = date("Y-m-d", strtotime("last day of previous month"));
        $cashback_balance = 0;
        foreach ($partners as $part) {
            $product_ids = array();
            $cashback_validity = 0;
            if ($part->loyaltyPrograms->count() > 0) {
                foreach ($part->loyaltyPrograms as $program) {
                    $cashback_validity = $program->cashback_validity;
                    foreach ($program->cashbacks as $cashback) {
                        $product_ids[] = $cashback->product_id;
                    }
                }

                if (count($product_ids) >= 1) {
                    $tempString = "-" . $cashback_validity . ' months';
                    $now_date = date("Y-m-d");
                    $new_timestamp = strtotime($tempString, strtotime($end_of_last_month_date));
                    $start_date = date("Y-m-d", $new_timestamp);
                    $balance_upto_newest_cashback = 0;
                    $newest_cashback_created_at = '';

                    $trx_cashback_newest_created_at = Transaction::select(DB::raw('created_at,ref_number'))->where('notes', 'like', "%cashback%")->where('partner_id', $part->id)->where('user_id', $user_id)->where('created_at', '<=', $newest_cashback_created_at)->where('created_at', '<=', $end_of_last_month_date)->orderBy('created_at', 'DESC')->first();

                    if ($trx_cashback_newest_created_at) {
                        $arr_trx_cashback_newest_created_at = $trx_cashback_newest_created_at->toArray();
                        $decode_newest_cashback = json_decode($arr_trx_cashback_newest_created_at['ref_number'], TRUE);

                        $newest_cashback_created_at = $arr_trx_cashback_newest_created_at['created_at'];
                        $trxs_user_reward_ids = Transaction::select(DB::raw('id'))->where('expired', 0)->where('partner_id', $part->id)->where('user_id', $user_id)->whereIn('product_id', $product_ids)->where('created_at', '>', $start_date)->where('created_at', '<=', $newest_cashback_created_at)->get();

                        //$arr_trxs_user_reward_ids_1 = $trxs_user_reward_ids->toArray();
                        foreach ($trxs_user_reward_ids as $rew_id) {
                            $arr_trxs_user_reward_ids[] = $rew_id['id'];
                        }

                        if (count($arr_trxs_user_reward_ids) == 0) {
                            $arr_trxs_user_reward_ids[] = "-1";
                        }
                        $trx_deduction_ids = array();
                        $trx_deduction_ids_1 = TransactionDeduction::select(DB::raw('trx_id'))->whereIn('trx_id', $arr_trxs_user_reward_ids)->get();
                        if ($trx_deduction_ids_1) {
                            foreach ($trx_deduction_ids_1 as $rew_id) {
                                $trx_deduction_ids[] = $rew_id['trx_id'];
                            }
                        }
                        if (count($trx_deduction_ids_1) == 0) {
                            $trx_deduction_ids = array(-1);
                        }


                        $trxs_user_sum = Transaction::select(DB::raw('sum(points_rewarded) pts_rew'))->where('expired', 0)->whereNotIn('id', $trx_deduction_ids)->where('partner_id', $part->id)->where('user_id', $user_id)->whereIn('product_id', $product_ids)->where('created_at', '>', $start_date)->where('created_at', '<=', $newest_cashback_created_at)->first();
                        $trx_deduction_ids2 = array();
                        $trx_deduction_ids_3 = TransactionDeduction::select(DB::raw('trx_id'))->whereIn('trx_id', $arr_trxs_user_reward_ids)->where('trx_used', 1)->get();
                        if ($trx_deduction_ids_3) {
                            foreach ($trx_deduction_ids_3 as $rew_id) {
                                $trx_deduction_ids2[] = $rew_id['trx_id'];
                            }
                        }
                        if (count($trx_deduction_ids_3) == 0) {
                            $trx_deduction_ids2 = array(-1);
                        }

                        $trx_deduction_ids_4 = TransactionDeduction::select(DB::raw('points_remaining'))->whereIn('trx_id', $arr_trxs_user_reward_ids)->whereNotIn('trx_id', $trx_deduction_ids2)->first();
                        $temp_points_remaining = 0;
                        if ($trx_deduction_ids_4) {
                            $temp_points_remaining = $trx_deduction_ids_4->points_remaining;
                        }

                        if ($trxs_user_sum->pts_rew > $decode_newest_cashback[1]) {
                            $balance_upto_newest_cashback = $decode_newest_cashback[1] + $temp_points_remaining;
                        } else {
                            $balance_upto_newest_cashback = $trxs_user_sum->pts_rew + $temp_points_remaining;
                        }
                    } else {
//                            $newest_cashback_created_at = $now_date;
                        $newest_cashback_created_at = $end_of_last_month_date;
                    }


                    $trxs_user_reward_ids_1 = Transaction::select(DB::raw('id'))->where('expired', 0)->where('partner_id', $part->id)->where('user_id', $user_id)->whereIn('product_id', $product_ids)->where('created_at', '>', $start_date)->where('created_at', '<=', $newest_cashback_created_at)->get();
                    $arr_trxs_user_reward_ids = array();

                    //$arr_trxs_user_reward_ids = $trxs_user_reward_ids->toArray();
                    foreach ($trxs_user_reward_ids_1 as $rew_id) {
                        $arr_trxs_user_reward_ids[] = $rew_id->id;
                    }
                    if (count($arr_trxs_user_reward_ids) == 0) {
                        $arr_trxs_user_reward_ids = array(-1);
                    }


                    $trx_deduction_ids_1 = TransactionDeduction::select(DB::raw('trx_id'))->whereIn('trx_id', $arr_trxs_user_reward_ids)->get();
                    $trx_deduction_ids = array();
                    if ($trx_deduction_ids_1) {
                        foreach ($trx_deduction_ids_1 as $rew_id) {
                            $trx_deduction_ids[] = $rew_id->trx_id;
                        }
                    }
                    if (count($trx_deduction_ids) == 0) {
                        $trx_deduction_ids = array(-1);
                    }

                    if ($balance_upto_newest_cashback == 0) {
                        $newest_cashback_created_at = $start_date;
                    }
                    $trxs_user_sum_after_cashback = Transaction::select(DB::raw('sum(points_rewarded) pts_rew'))->where('expired', 0)->where('partner_id', $part->id)->where('user_id', $user_id)->whereIn('product_id', $product_ids)->where('created_at', '>', $newest_cashback_created_at)->where('created_at', '<=', $end_of_last_month_date)->first();
                    $trx_deduction_ids5 = array();
                    $trx_deduction_ids_6 = TransactionDeduction::select(DB::raw('trx_id'))->whereIn('trx_id', $arr_trxs_user_reward_ids)->where('trx_used', 1)->get();
                    if ($trx_deduction_ids_6) {
                        foreach ($trx_deduction_ids_6 as $rew_id) {
                            $trx_deduction_ids5[] = $rew_id['trx_id'];
                        }
                    }
                    if (count($trx_deduction_ids_6) == 0) {
                        $trx_deduction_ids5 = array(-1);
                    }

                    $trx_deduction_ids_7 = TransactionDeduction::select(DB::raw('points_remaining'))->whereIn('trx_id', $arr_trxs_user_reward_ids)->whereNotIn('trx_id', $trx_deduction_ids5)->first();
                    $temp_points_remaining2 = 0;
                    if ($trx_deduction_ids_7) {
                        $temp_points_remaining2 = $trx_deduction_ids_7->points_remaining;
                        if (is_null($temp_points_remaining2)) {
                            $temp_points_remaining2 = 0;
                        }
                    }
                    $trx_sum_points_cashback = Transaction::select(DB::raw('sum(points_redeemed) pts_red'))->where('user_id', $user_id)->where('notes', 'like', '%cashback%')->where('created_at', '>', $newest_cashback_created_at)->where('created_at', '<=', $end_of_last_month_date)->first();
                    //                    $queries = DB::getQueryLog();
                    //			$last_query = end($queries);
                    //			print_r($last_query);
                    //                    exit();
//                        echo "<pre>";
//                        var_dump($trxs_user_sum_after_cashback->pts_rew);
//                        var_dump($trx_sum_points_cashback->pts_red);
//                        var_dump($balance_upto_newest_cashback);
//                        var_dump($temp_points_remaining2);
//                        $sum_points_eligible = intval($trxs_user_sum_after_cashback->pts_rew) + $balance_upto_newest_cashback +$temp_points_remaining2;
                    $sum_points_eligible = intval($trxs_user_sum_after_cashback->pts_rew) - intval($trx_sum_points_cashback->pts_red);
                    $cashback_balance += $sum_points_eligible;
//                        var_dump($cashback_balance);
                }
            }
        }

        $user = User::find($user_id);
        $balance = $user->balance($partner1->firstNetwork()->id);

        if ($balance <= $cashback_balance) {
            $cashback_balance = $balance;
        }
        if ($cashback_balance < 0) {
            $cashback_balance = 0;
        }
//            var_dump($cashback_balance);
//            exit();

        return $cashback_balance;
    }

    public static function saveEstatementToHistory($estatementId) {
        $estatement = Estatement::find($estatementId);
        $segments = $estatement->segments;
        $arr_segments = array();
        foreach ($segments as $s) {
            $arr_segments[] = $s->id;
        }
        $segmentsJson = json_encode($arr_segments);

        $countries = $estatement->countries;
        $arr_countries = array();
        foreach ($countries as $countr) {
            $arr_countries[] = $countr->id;
        }
        $countriesJson = json_encode($arr_countries);

        $tiers = $estatement->tiers;
        $arr_tiers = array();
        foreach ($tiers as $tier) {
            $arr_tiers[] = $tier->id;
        }
        $tiersJson = json_encode($arr_tiers);

        $items = DB::table('estatement_items')->where('estatement_id', $estatementId)->orderBy('rank', 'asc')->get();
        $itemsArray = array();
        $i = 0;
        foreach ($items as $item) {
            $itemsArray[$i]['type'] = $item->type;
            $itemsArray[$i]['id'] = $item->item_id;
            $i++;
        }
        $itemsJson = json_encode($itemsArray);

        $estatementHistory = new EstatementHistory();

        $estatementHistory->title = $estatement->title;
        $estatementHistory->description = $estatement->description;
        $estatementHistory->estatement_id = $estatementId;
        $estatementHistory->partner_id = $estatement->partner_id;
        $estatementHistory->delivery_date = $estatement->delivery_date;
        $estatementHistory->status = $estatement->status;
        $estatementHistory->time = $estatement->time;
        $estatementHistory->draft = $estatement->draft;
        $estatementHistory->deleted = $estatement->deleted;
        $estatementHistory->coutries = $countriesJson;
        $estatementHistory->segments = $segmentsJson;
        $estatementHistory->tiers = $tiersJson;
        $estatementHistory->items = $itemsJson;

        $res = $estatementHistory->save();
        return $res;
    }

}

// EOC
