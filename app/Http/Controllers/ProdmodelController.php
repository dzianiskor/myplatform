<?php
namespace App\Http\Controllers;
/**
 * Prodmodel Controller
 * 
 * @category   Controllers
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class ProdmodelController extends BaseController
{
    protected $layout = "layouts.dashboard";

    /**
     * Return the prodmodel validations
     *
     * @return array
     */
    private function getValidations() 
    {
        return array(
            'name' => 'required|min:2'
        );
    }

    /**
     * Display a list of the Prodmodels
     *
     * @return void
     */
    public function listProdmodels()
    {
        if(!I::can('view_prodmodels')){
            return Redirect::to(url("dashboard"));
        }
        $this->ormClass      = 'App\Prodmodel';
        $this->search_fields = array('name');

        return View::make("prodmodel.list", array(
            'prodmodels' => $this->getList()
        ));
    }

    /**
     * Fetch previous prodmodels for the new prodmodel form
     *
     * @return void
     */
    public function newProdmodel()
    {
        if(!I::can('create_prodmodels')){
            return Redirect::to(url("dashboard"));
        }
        $data['prodmodels'] = Prodmodel::where('draft', false)->orderBy('name')->pluck('name', 'id');

        return View::make("prodmodel.new", $data);
    }

    /**
     * Create the new prodmodel
     *
     * @return void
     */
    public function createNewProdmodel()
    {
        if(!I::can('create_prodmodels')){
            return Redirect::to(url("dashboard"));
        }
        $validator = Validator::make(Input::all(), $this->getValidations());

        if($validator->fails()) {
            return Redirect::to('dashboard/prodmodels/new')->withErrors($validator)->withInput();
        }

        $prodmodel                     = new Prodmodel();
        $prodmodel->name               = self::sanitizeText(Input::get('name'));
        $prodmodel->draft              = false;

        $prodmodel->save();        

        return Redirect::to('dashboard/prodmodels');
    }

    /**
     * View a single Prodmodel resource
     *
     * @param int $id
     * @return void
     */
    public function viewProdmodel($id = null)
    {
        if(!I::can('view_prodmodels')){
            return Redirect::to(url("dashboard"));
        }
        $prodmodel = Prodmodel::find($id);

        if(!$prodmodel) {
            App::abort(404);
        }

        return View::make("prodmodel.view", array(
            'prodmodel'   => $prodmodel,
            'prodmodels' => Prodmodel::where('draft', false)->orderBy('name')->pluck('name', 'id')
        ));
    }

    /**
     * Delete a Prodmodel instance
     *
     * @param int $id
     * @return void
     */
    public function deleteProdmodel($id = null)
    {
        if(!I::can('delete_prodmodels')){
            return Redirect::to(url("dashboard"));
        }
        $prodmodel = Prodmodel::find($id);

        if(!$prodmodel) {
            App::abort(404);
        }

        $prodmodel->delete();

        return Redirect::to('dashboard/prodmodels');
    }

    /**
     * Update a prodmodel resource
     *
     * @param int $id
     * @return void
     */
    public function updateProdmodel($id = null)
    {
        if(!I::can('edit_prodmodels')){
            return Redirect::to(url("dashboard"));
        }
        return View::make("prodmodel.update", array(
            'id' => $id
        ));
    }

    /**
     * Update a prodmodel resource
     *
     * @param int $id
     * @return void
     */
    public function saveProdmodel($id = null)
    {
        if(!I::can('edit_prodmodels')){
            return Redirect::to(url("dashboard"));
        }
        $prodmodel = Prodmodel::find($id);

        if(!$prodmodel) {
            App::abort(404);
        }

        $validator = Validator::make(Input::all(), $this->getValidations());

        if($validator->fails()) {
            return Redirect::to('dashboard/prodmodels/new')->withErrors($validator)->withInput();
        }
        $input = Input::all();
        foreach($input as $k=>$i){
            $input[$k] = self::sanitizeText($i);
        }
        $prodmodel->update($input);

        return Redirect::to('dashboard/prodmodels');
    }

    /**
     * Load the new prodmodel popup
     *
     * @return void
     */
    public function newPopoverProdmodel()
    {
        if(!I::can('create_prodmodels')){
            return Redirect::to(url("dashboard"));
        }
        $prodmodel       = new Prodmodel();
        $prodmodel->name = "New Prodmodel";

        $prodmodel->save();
        
        return View::make("prodmodel.new_popover", array(
            'prodmodel'   => $prodmodel,
            'prodmodels' => Prodmodel::where('draft', false)->orderBy('name')->pluck('name', 'id')
            )
        );
    }

    /**
     * Save the popup prodmodel resource
     *
     * @param int $id
     * @return void
     */
    public function savePopoverProdmodel($id = null)
    {
        if(!I::can('edit_prodmodels')){
            return Redirect::to(url("dashboard"));
        }
        $prodmodel = prodmodel::find($id);

        if(!$prodmodel) {
            App::abort(404);
        }        

        $validator = Validator::make(Input::all(), $this->getValidations());

        if($validator->fails()) {
            return Response::json(array(
                'failed' => true,
                'errors' => "Please ensure that you provide a valid prodmodel name"
            ));
        }

        $prodmodel->name               = self::sanitizeText(Input::get('name'));
        
        $prodmodel->draft              = false;
        $prodmodel->save();

        return Response::json(array(
            'failed'   => false,
            'view_url' => url('dashboard/prodmodels/getList/')
        ));
    }

    /**
     * Return the select list of prodmodels
     *
     * @return void
     */
    public function getProdmodelsList()
    {
        if(!I::can('view_prodmodels')){
            return Redirect::to(url("dashboard"));
        }
        return View::make("prodmodel.selectList", array(
            'prodmodels' => Prodmodel::where('draft', false)->orderBy('name')->pluck('name', 'id')
        ));
    }
    
} // EOC