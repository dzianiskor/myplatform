<?php

namespace App\Http\Controllers;

use App\Classes\PathHelper;
use App\Classes\ReportImportHelper;
use App\Mail\MailImportChangesFailed;
use App\Mail\MailImportChangesSucceed;
use App\Mail\MailImportCustomerFailed;
use App\Mail\MailImportCustomerSucceed;
use App\Mail\MailImportTransactionFailed;
use App\Mail\MailImportTransactionSucceed;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use RewardHelper;
use SendGrid;
use SMSHelper;
use View;
use Input;
use Request;
use File;
use Validator;
use App\CurrencyPricing as CurrencyPricing;
use Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use I;
use Illuminate\Support\Facades\Auth;
use BluCollection;
use AuditHelper;
use PinCode;
use App\Reference as Reference;
use Illuminate\Support\Facades\Redirect;
use ManagedObjectsHelper;
use App\User as User;
use App\Role as Role;
use App\AffiliateProgram as AffiliateProgram;
use App\AffiliateProgramsMemberships as AffiliateProgramsMemberships;
use App\City as City;
use App\Area as Area;
use App\Occupation as Occupation;
use Meta;
use InputHelper;
use App\LoyaltyRule as LoyaltyRule;
use App\Loyalty as Loyalty;
use App\Admin as Admin;
use App\Transaction as Transaction;
use App\Partner as Partner;
use App\Language as Language;
use App\Country as Country;
use App\ProductPartnerReward as ProductPartnerReward;
use App\Category as Category;
use App\Network as Network;
use App\Currency as Currency;
use App\Product as Product;
use App\Tier as Tier;
use PointsHelper;
use App\Store as Store;
use App\Pos as Pos;
use UCID;
use TrailUser as TrailUser;
use APP\CategoryPartner as CategoryPartner;
use XTEA;

/**
 * API2 Controller
 *
 * @category   Controllers
 * @package    BLU
 * @author     Ralph Nader <adminit@bluloyalty.com>
 * @copyright  2016 BLU
 * @link       http://blupoints.com
 */
class API2Controller extends BaseController {

    private $networks;
    private $partner;
    private $user;

    /**
     * Initiate the controller and ensure the partner request is authenticated
     *
     * @return void
     */
    public function __construct() {
        $api_key = Input::get('api_key', null);

        if (empty($api_key)) {
            App::abort(401);
        }

        $this->user = Admin::where('api_key', "$api_key")->first();

        if (!$this->user) {
            App::abort(404);
        } else {
            Auth::loginUsingId($this->user->id);

            // Assign scopes

            $this->partner = $this->user->partners()->first();
            $this->network = $this->partner->firstNetwork();

            if (!$this->partner) {
                App::abort(403, "No partner associated to request $api_key");
            }
        }
    }

    /**
     * Generic response handler for API requests
     *
     * @param int $status
     * @param string $message
     * @param object $objects
     * @param string $class_name
     * @return Response
     */
    private static function respondWith($status, $message, $object = null, $class_name = '') {
        $response = array();

        if ($message) {
            $response['message'] = $message;
        }

        if (!empty($object)) {
            if ($object && is_object($object)) {
                if (get_class($object) != 'stdClass') {
                    $class_name = get_class($object);
                }

                if (method_exists($object, 'toArray')) {
                    $response[$class_name] = $object->toArray();
                } else {
                    $response[$class_name] = $object;
                }
            } else {
                $response[$class_name] = $object;
            }
        } else {
            $response[$class_name] = array();
        }

        $response['status'] = $status;

        if ($status == 500) {
            Log::warning($message);
        }

        return Response::make($response, $status);
    }

    public function postSendSmsBsfUsers() {
        //str_users = "28260,BSF-100192479,Njg1MA==,C25538||28261,BSF-100661854,ODE2OQ==,500282";
        $str_users = "";
        $arr_users = explode("||", $str_users);
        $site_sms = "https://fransijana.com.sa";
        foreach ($arr_users as $a_user) {
            $user_data = explode(",", $a_user);
            $url = "https://bluprd.alfransi.com.sa/BLU/api.php?method=RegistrationSMS&api_key=1be83d735510848b88a08e43c2e114d9";
            $post_fields = 'cpt=' . $user_data[3] . "&bsfusername=" . $user_data[1] . "&pin=" . base64_decode($user_data[2]) . "&site=" . $site_sms;

            $curl = curl_init();
            // Set some options - we are passing in a useragent too here
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => $url,
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => $post_fields,
                CURLOPT_SSL_VERIFYPEER => False,
                CURLOPT_USERAGENT => 'Testing Sensitive Info'
            ));
            // Send the request & save response to $resp
            $resp = curl_exec($curl);
            $results[] = $resp;
        }


        var_dump($results);
        exit();
    }

    public function getTestAnything() {

        $string = "12345678901234567890";
        echo $this->partner->id;
        echo str_pad($string, "30", "x");
        echo "<br>";
        echo substr(str_pad($string, "10", "x"), 0, 10);
        $arr_file = array();
        $arr_file[] = "H000000094";
        $transactions = Transaction::where('notes', 'like', "%\"airline\":\"3\"%")->where('partner_id', $this->partner->id)->get();
        echo "<pre>";
        echo "<transactions >";
        var_dump($transactions);
        echo "</pre>";
        foreach ($transactions as $trx) {
            $temp_arr = array();
            $decoded_trx_notes = json_decode($trx->notes, TRUE);
            $country = Country::find($trx->country_id);
            $country_to_show = $country->name;
            $currency = Currency::find($trx->currency_id);
            $currency_to_show = substr($currency->short_code, 0, 2);
            $user = User::find($trx->user_id);
            $user_name = ""; //$user->first_name . " " . $user->last_name;
            $temp_arr[] = substr(str_pad($decoded_trx_notes['frequent_flyer_number'], "9", "0", STR_PAD_LEFT), 0, 9);
            $temp_arr[] = substr(str_pad(str_replace("-", "", $trx->created_at), "8", "0", STR_PAD_LEFT), 0, 8);
            $temp_arr[] = substr(str_pad("", "10", " ", STR_PAD_LEFT), 0, 10);
            $temp_arr[] = substr(str_pad(str_replace("-", "", $trx->id), "14", " ", STR_PAD_LEFT), 0, 14);
            $temp_arr[] = substr(str_pad($country_to_show, "10", " ", STR_PAD_LEFT), 0, 10);
            $temp_arr[] = substr(str_pad("", "16", " ", STR_PAD_LEFT), 0, 16);
            $temp_arr[] = substr(str_pad("", "2", "0", STR_PAD_LEFT), 0, 2);
            $temp_arr[] = substr(str_pad("", "12", "0", STR_PAD_LEFT), 0, 12);
            $temp_arr[] = substr(str_pad("", "12", "0", STR_PAD_LEFT), 0, 12);
            $temp_arr[] = substr(str_pad($currency_to_show, "2", " ", STR_PAD_LEFT), 0, 2);
            $temp_arr[] = substr(str_pad($user_name, "20", " ", STR_PAD_RIGHT), 0, 20);
            $temp_arr[] = substr(str_pad(intval($trx->points_redeemed), "9", "0", STR_PAD_LEFT), 0, 9);
            $temp_arr[] = substr(str_pad("", "9", "0", STR_PAD_LEFT), 0, 9);
            $arr_file[] = implode('', $temp_arr);
        }

        echo "<pre>";
        echo "<arr_file >";
        var_dump($arr_file);
        echo "</pre>";
        $arr_file[] = "999999999";

        $count_lines = count($arr_file) - 2;
        $json_data = implode('\r\n', $arr_file);
        $data = array();
        $data['html'] = "QA miles conversion";
        $response1 = Mail::send('emails.bsfaccounting', $data, function($message1) use ( $json_data, $count_lines) {
                    $filename = "QA" . date('ymd') . '.a01';
                    //$message1->attach(new Swift_Message_Attachment(file_get_contents($filepath), $filename, 'application/vnd.ms-excel'));
                    $message1->attachData($json_data, $filename);
                    $message1->to(array('rabih.malaeb@bluloyalty.com'))->subject('Ralph DEV Qatar Airways - ' . $count_lines);
                });
        //APIController::postSendEmailJson($json_data);


        $arr_file1 = array();
        $transactions1 = Transaction::where('notes', 'like', "%\"airline\":\"4\"%")->where('partner_id', $this->partner->id)->get();

        echo "<pre>";
        echo "<transactions1 >";
        var_dump($transactions1);
        echo "</pre>";

        $arr_file[] = "999999999";
        foreach ($transactions1 as $trx) {
            $temp_arr = array();
            $decoded_trx_notes = json_decode($trx->notes, TRUE);
            $user = User::find($trx->user_id);
            $user_name = ""; //$user->first_name . " " . $user->last_name;
            $temp_arr[] = substr($decoded_trx_notes['frequent_flyer_number'], 0, 11);
            $temp_arr[] = "XXXXXX1CCA";
            $trx_date = $newDate = date("d/m/Y", strtotime($trx->created_at));
            $temp_arr[] = substr($trx_date, 0, 10);
            $temp_arr[] = substr(str_pad(intval($trx->points_redeemed), "5", "0", STR_PAD_LEFT), 0, 5);
            $temp_arr[] = ""; //substr($user->first_name,0,1);
            $temp_arr[] = ""; //substr($user->last_name,0,25);
            $arr_file1[] = implode(',', $temp_arr);
        }


        echo "<pre>";
        echo "<arr_file1 >";
        var_dump($arr_file1);
        echo "</pre>";

        $json_data2 = implode('\r\n', $arr_file1);
        $data['html'] = "Gulf Air miles conversion";
        $count_lines = count($arr_file1) - 2;
        $response1 = Mail::send('emails.bsfaccounting', $data, function($message1) use ( $json_data2, $count_lines) {
                    $filename = "GA" . date('ymd') . '.A01';
                    //$message1->attach(new Swift_Message_Attachment(file_get_contents($filepath), $filename, 'application/vnd.ms-excel'));
                    $message1->attachData($json_data2, $filename);
                    $message1->to(array('rabih.malaeb@bluloyalty.com'))->subject('Ralph DEV Gulf Air - ' . $count_lines);
                });
        //APIController::postSendEmailJson($json_data2);

        exit();
    }

    protected function transactionImportSucceed($subjectSuffix, $baseFileName) {
        Mail::send(new MailImportTransactionSucceed($subjectSuffix));

        return $this->responseImportSucceed($baseFileName);
    }

    protected function transactionImportFailed($errors, $baseFileName, $subjectSuffix) {
        $errorCount = count($errors);

        Mail::send(
                new MailImportTransactionFailed(
                $realFileName = ReportImportHelper::makeErrorReportFile(
                        $errors, $baseFileName, $folder = 'error-transaction-files'
                ), $subjectSuffix . '-' . $errorCount
                )
        );

        return $this->responseImportFiled($errors, $baseFileName, $errorCount, $realFileName);
    }

    protected function customerImportSucceed($subjectSuffix, $baseFileName) {
        Mail::send(new MailImportCustomerSucceed($subjectSuffix));

        return $this->responseImportSucceed($baseFileName);
    }

    protected function customerImportFailed($errors, $baseFileName, $subjectSuffix) {
        $errorCount = count($errors);

        Mail::send(
                new MailImportCustomerFailed(
                $realFileName = ReportImportHelper::makeErrorReportFile(
                        $errors, $baseFileName, $folder = 'error-customer-files'
                ), $subjectSuffix . '-' . $errorCount
                )
        );

        return $this->responseImportFiled($errors, $baseFileName, $errorCount, $realFileName);
    }

    protected function changesImportSucceed($subjectSuffix, $baseFileName) {
        Mail::send(new MailImportChangesSucceed($subjectSuffix));

        return $this->responseImportSucceed($baseFileName);
    }

    protected function changesImportFailed($errors, $baseFileName, $subjectSuffix) {
        $errorCount = count($errors);

        Mail::send(
                new MailImportChangesFailed(
                $realFileName = ReportImportHelper::makeErrorReportFile(
                        $errors, $baseFileName, $folder = 'error-changes-files'
                ), $subjectSuffix . '-' . $errorCount
                )
        );

        return $this->responseImportFiled($errors, $baseFileName, $errorCount, $realFileName);
    }

    protected function responseImportFiled($errors, $baseFileName, $errorCount, $realFileName) {
        AuditHelper::record(
                4, 'Finished processing ' . $baseFileName . '.txt with ' . $errorCount . ' errors. For details look into the file ' . $realFileName
        );

        return self::respondWith(
                        200, 'NOT OK', $errors, 'errors'
        );
    }

    protected function responseImportSucceed($baseFileName) {
        AuditHelper::record(
                4, 'Finished processing ' . $baseFileName . '.txt with 0 errors.'
        );

        return self::respondWith(
                        200, "OK", [], 'errors'
        );
    }

    protected function transactionImportPatch() {
        return PathHelper::incoming('transaction-files');
    }

    protected function customerImportPatch() {
        return PathHelper::incoming('customer-files');
    }

    protected function changesImportPatch() {
        return PathHelper::incoming('changes-files');
    }

    protected function generateImportFileBaseName($fileName, $ext) {
        return PathHelper::generateFileBaseName($fileName, $ext);
    }

    /**
     * Perform Import Using api
     *
     * POST /api/import-file
     *
     * @param POST file (file)
     * @return Response
     */
    public function postImportFile() {
        if (Input::hasFile('file')) {
            $ip = Request::getClientIp();
            $result = FALSE;
            $errResult = array();
            $auth_user = $this->user;

            $file = Input::file('file');
            $ext = $file->getClientOriginalExtension();
            $name1 = $file->getClientOriginalName();
            $generatedFileBaseName = $this->generateImportFileBaseName($name1, $ext);
            $generatedFileName = $generatedFileBaseName . '.' . $ext;
            //-----------------------------------------------------BISBTransactions

            if (($ext == 'TXT' || $ext == 'txt') && strstr($name1, 'BISBTransactions') && strcmp($this->user->api_key, "092c7dfffab8aef9449663c52504bbd7") == 0) {
                //Reference ID|PostingDate|Product ID|BonusPts|Billing Amount|Type|MCC|
                //  0               1           2           3       4           5   6
                //Billing Currency|Transaction currency|Transaction Amount|Transaction Sign|Line number
                //        7               8                       9               10              11
                $tx = array();
                $count = 0;
                $counterrors = 0;
                $errBool = True;
                $errors = array();
                $params = array();
                $passes = 0;

                $path = $this->transactionImportPatch();
                $file->move($path, $generatedFileName);
                $inputFile = $path . '/' . $generatedFileName;

                AuditHelper::record(
                        4, $auth_user->name . " started uploading file: " . $generatedFileName
                );

                $contents = File::get($inputFile);
                $contents = nl2br($contents, FALSE);
                $contentLines = explode("<br>", $contents);
                $errors = array();


                $countContentLines = count($contentLines) - 1;
                $test = $contentLines[$countContentLines];
                $test1 = $contentLines[$countContentLines - 1];
                $ret_eof_error = true;
                if (trim($test) === "EOF") {
                    $ret_eof_error = false;
                } elseif (trim($test1) === "EOF") {
                    $ret_eof_error = false;
                }
                if ($ret_eof_error == true) {
                    return self::respondWith(
                                    200, 'EOF NOT FOUND', $errors, 'errors'
                    );
                }
                $arrContent = array();
                foreach ($contentLines as $lineread) {
                    if (trim($lineread) !== 'EOF') {
                        $arrContent[] = explode("|", $lineread);
                    }
                }
                $partner = $this->partner;
                $network_id = $partner->firstNetwork()->id;
                $partner_id = $partner->id;
                $partners = $partner->managedObjects();
                $partners_children = array();
                foreach ($partners as $part) {
                    $partners_children[] = $part->id;
                }
                $loy_by_partner = RewardHelper::loyaltyByPartner($partners_children);

                $partner_prefix = $partner->prefix;
                $itemchosen = ProductPartnerReward::where('partner_id', $partner_id)->get();
                $arr_item_chosen = array();
                foreach ($itemchosen as $ic) {
                    $arr_item_chosen[$ic->partner_mapping_id] = $ic->product_id;
                }


                $transactions_file = Transaction::select('ref_number')->where('ref_number', 'like', $name1 . "%")->get();
                $arr_transactions_file = $transactions_file->toArray();

//                $ref_number = $name1."1";
//                echo "<pre>";
//                var_dump(array_search($ref_number, array_column($arr_transactions_file, 'ref_number')));
//                exit();
                $arr_currencies = array();
                $currencies = Currency::all();
                foreach ($currencies as $curr) {
                    $arr_currencies[$curr->short_code] = $curr->id;
                }
                if (count($arr_transactions_file) > 0) {
                    return self::respondWith(
                                    200, 'NOT OK File Already Exists', null, 'errors'
                    );
                }

                $store_res = $partner->stores->first();
                if ($store_res) {
                    $pos_id = $store_res->pointOfSales->first()->id;
                    $tx['store_id'] = $store_res->id;
                    $tx['pos_id'] = $pos_id;
                }

//                echo "<pre>";
//                var_dump($arrContent);
//                exit();
                foreach ($arrContent as $row) {
                    $lineNumber = 0;
                    $posted_at = date('Y-m-d H:i:s');
                    if ($count != 0) {
                        if (trim($row[0]) != '' && trim($row[0]) != 'EOF') {

                            $unhandled_date = explode("/", $row[1]);

                            if (!isset($row[11])) {
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-001";
                                $errors[] = $row;
//                                echo $row[0] . "001";
//                                var_dump($row);
//                                exit();
                                continue;
                            } elseif (substr(trim($row[0]), 0, 5) !== "BISB-" || trim($row[0]) === "" || $row[0] === " ") {
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-002";
                                $errors[] = $row;
//                                echo $row[0] . "002";
//                                var_dump($row);
//                                exit();
                                continue;
                            }///self::respondwith('400', ,null);
                            elseif ($row[1] === "" || $row[1] === " " || !checkdate($unhandled_date[1], $unhandled_date[0], $unhandled_date[2])) {
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-003";
                                $errors[] = $row;
//                                echo $row[0] . "003";
//                                var_dump($row);
//                                exit();
                                continue;
                            } elseif ($row[2] === "" || $row[2] === " " || !is_numeric($row[2])) {
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-004";
                                $errors[] = $row;
//                                echo $row[0] . "004";
//                                var_dump($row);
//                                exit();
                                continue;
                            } elseif ($row[3] === "" || $row[3] === " " || is_numeric($row[3]) === FALSE) {
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-005";
                                $errors[] = $row;
//                                echo $row[0] . "005";
//                                var_dump($row);
//                                exit();
                                continue;
                            } elseif ($row[4] === "" || $row[4] === " " || !is_numeric($row[4])) {
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-006";
                                $errors[] = $row;
//                                echo $row[0] . "006";
//                                var_dump($row);
//                                exit();
                                continue;
                            } elseif ($row[9] === "" || $row[9] === " " || !is_numeric($row[9])) {
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-006.1";
                                $errors[] = $row;
//                                echo $row[0] . "007";
//                                var_dump($row);
//                                exit();
                                continue;
                            } elseif ($row[7] === "" || $row[7] === " " || strlen($row[7]) !== 3) {
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-007";
                                $errors[] = $row;
//                                echo $row[0] . "008";
//                                var_dump($row);
//                                exit();
                                continue;
                            } elseif ($row[8] === "" || $row[8] === " " || strlen($row[8]) !== 3) {
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-007.2";
                                $errors[] = $row;
//                                echo $row[0] . "009";
//                                var_dump($row);
//                                exit();
                                continue;
                            } elseif ($row[5] === "" || $row[5] === " " || strlen($row[5]) !== 2) {
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-007.1";
                                $errors[] = $row;
//                                echo $row[0] . "010";
//                                var_dump($row);
//                                exit();
                                continue;
                            } elseif ($row[6] === "" || $row[6] === " ") {
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-008";
                                $errors[] = $row;
//                                echo $row[0] . "011";
//                                var_dump($row);
//                                exit();
                                continue;
                            } elseif (trim($row[10]) !== "C" && trim($row[10]) !== "D") {
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-010";
                                $errors[] = $row;
//                                echo $row[0] . "012";
//                                var_dump($row);
//                                exit();
                                continue;
                            } elseif (!is_numeric(trim($row[11]))) {
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-009";
                                $errors[] = $row;
//                                echo $row[0] . "012";
//                                var_dump($row);
//                                exit();
                                continue;
                            }

                            if (isset($row[11])) {
                                $lineNumber = $row[11];
                            }
                            $ref_number = $name1 . $lineNumber;
                            $ref_number = rtrim($ref_number);
//                                echo "<pre>";
//                                echo $name1 . $lineNumber . "<br>";
//                                var_dump($row);
//                                var_dump($errors);
//                                exit();
//                            $transact = Transaction::where('ref_number',$ref_number)->first();

                            if (array_search($ref_number, array_column($arr_transactions_file, 'ref_number')) === FALSE) {
                                $coefficient = 1;
                                if ($row[10] == 'C') {
                                    $coefficient = intval(-1);
                                }
                                $row[0] = trim($row[0]);
                                $tx['id'] = 'NULL';
                                $tx['ref_number'] = $ref_number;

                                $tx['original_total_amount'] = $row[9] * $coefficient;
                                $tx['partner_amount'] = $row[4] * $coefficient;
                                $tx['partner_trxtype'] = $row[5];
                                $tx['mcc'] = $row[6];


                                $tx['user_id'] = 1;
                                $tx['points_redeemed'] = 0;
                                $tx['points_rewarded'] = 0;
                                $tx['partner_id'] = $partner->id;
                                $tx['network_id'] = $network_id;
                                $tx['trx_reward'] = 1;

                                $reference = Reference::where('number', $row[0])->first();
                                if (isset($reference->user_id)) {
                                    $user = $reference->user_id;
                                } else {
                                    $errBool = true;
                                    $counterrors = $counterrors + 1;
                                    $errors[] = $row;
                                    continue;
                                }

                                $tx['user_id'] = $user;

                                $userInfo = User::find($user);
                                if ($userInfo->status == 'closed') {
                                    $errBool = true;
                                    $counterrors = $counterrors + 1;
                                    $errors[] = $row;
                                    continue;
                                }
//                                $unhandledDate = $row[1];
//                                $arrunhandledDate = explode("/",$unhandledDate);
                                $date_unhandled = $unhandled_date[1] . "/" . $unhandled_date[0] . "/" . $unhandled_date[2];
                                //$date_unhandled = $row[1];
                                $date = strtotime($date_unhandled);
                                $new_date = date('Y-m-d H:i:s', $date);

                                $tx['created_at'] = $new_date;
                                $tx['updated_at'] = $new_date;
                                $tx['posted_at'] = $posted_at;
                                // Map Currency
//                                $loyalty = Loyalty::where('partner_id',$tx['partner_id'])->first();
                                //$itemchosen = ProductPartnerReward::where('partner_mapping_id',$partner_prefix.$row[2])->first();
                                if (isset($arr_item_chosen[$partner_prefix . $row[2]])) {
                                    $tx['product_id'] = $arr_item_chosen[$partner_prefix . $row[2]];
                                } else {
                                    $tx['product_id'] = NULL;
                                }




                                $validator = Validator::make($tx, array(
                                            //'id'         => 'required|numeric',
                                            'partner_id' => 'required|numeric',
                                            'store_id' => 'required|numeric',
                                            'user_id' => 'required|numeric'
                                ));

                                if (!$validator->fails()) {


                                    //DB::table('transaction')->insert($tx);
                                    //                                echo $passes . "<pre>";
                                    //                                var_dump($params);
                                    //                                var_dump($arr_result);
                                    //                                die();
                                    //ReferenceID|Postingdate|Card type|Bonuspts|Amount|Currency
                                    //  0           1           2           3       4       5

                                    if (is_numeric($row[3])) {
                                        $txbonus = array(
                                            'partner_id' => $tx['partner_id'],
                                            'store_id' => $tx['store_id'],
                                            'pos_id' => $tx['pos_id'],
                                            'user_id' => $tx['user_id'],
                                            'ref_number' => "Bisb ONLINE Bonus POINTS",
                                            'total_amount' => 0,
                                            'points_rewarded' => round($row[3], 0, PHP_ROUND_HALF_UP) * $coefficient,
                                            'amt_reward' => PointsHelper::pointsToAmount(round($row[3], 0, PHP_ROUND_HALF_UP), "BHD", $network_id) * $coefficient,
                                            'trx_reward' => 1,
                                            'currency_id' => 6,
                                            'exchange_rate' => 1,
                                            'country_id' => 8,
                                            'quantity' => 0,
                                            'notes' => 'Bonus Points',
                                            'trans_class' => "Amount",
                                            'network_id' => $network_id,
                                            'created_at' => $new_date,
                                            'updated_at' => $new_date,
                                            'posted_at' => $posted_at
                                        );

                                        if ($txbonus['points_rewarded'] != 0) {
                                            DB::table('transaction')->insert($txbonus);
                                            $user = User::find($txbonus['user_id']);
                                            $user->updateBalance($row[3], $txbonus['network_id'], 'add');
                                            $partner = Partner::find($txbonus['partner_id']);
                                        }
                                        //$template = View::make('sms.bonus-points', array('bonusPoints' => $row[3]))->render();
                                        //SMSHelper::send($template, $user, $partner);
                                    }

                                    $passes++;

                                    if (is_numeric($tx['points_rewarded'])) {
//                                            $params = array(
//                                                'user_id'		=> $tx['user_id'],
//                                                'amount'		=> floatval($tx['partner_amount']),
//                                                'currency'		=> $tx['partner_currency_id'],
//                                                'partner'		=> $tx['partner_id'],
//                                                'store_id'		=> $tx['store_id'], // Optional
//                                                'pos_id'		=> $tx['pos_id'], // Optional
//                                                //'notes'		=> $tx['notes'], // Optional
//                                                'created_at'	=> $tx['created_at'],
//                                                'updated_at'	=> $tx['updated_at']
//                                            );
                                        $params = array(
                                            'user_id' => $tx['user_id'],
                                            'partner' => $tx['partner_id'],
                                            'pos_id' => $tx['pos_id'],
                                            'store_id' => $tx['store_id'],
                                            'amount' => floatval($tx['partner_amount']),
                                            'currency' => $row[8],
                                            //'notes'		=> 'Reward By Product',
                                            'type' => 'reward',
                                            'reference' => $tx['ref_number'],
                                            'network_id' => $partner->firstNetwork()->id,
                                            'product_id' => $tx['product_id'],
                                            'partner_currency' => $row[7],
                                            'original_transaction_amount' => floatval($tx['original_total_amount']),
                                            'use_partner_currency' => True,
                                            'mcc' => $tx['mcc'],
                                            'partner_trxtype' => $tx['partner_trxtype'],
                                            'created_at' => $new_date,
                                            'updated_at' => $new_date
                                        );

//                                            $user = User::find($tx['user_id']);
//                                            APIController::postSendEmailJson(json_encode($params),"BISB Params - ". $tx['ref_number']);
                                        try {
                                            $result_do_reward = RewardHelper::doRewardNewLoyalty($params, 'Amount', $loy_by_partner);
                                        } catch (Exception $ex) {
                                            //$resp = array($ex);
                                            $json_data = json_encode(array("Exception" => $ex));
                                            APIController::postSendEmailJson($json_data);
                                        }

//                                            $json_data = $result_do_reward;
//                                                APIController::postSendEmailJson($json_data);
                                        //$trx_id = DB::table('transaction')->insertGetId($tx);
                                        //$trx_inserted = Transaction::find($trx_id);
//                                            try{
//                                                $trx_after_loyalty = Loyalty::processMatchingRewardExceptionRule($trx_inserted);
//                                            } catch (Exception $ex) {
//                                                $resp= array();
////                                                $resp[] = $trx_after_loyalty;
//                                                $resp[] = $ex;
//                                                $json_data = json_encode($resp);
//                                                APIController::postSendEmailJson($json_data);
//
//                                            }
//                                                $json_data = json_encode($trx_inserted->toArray());
//                                                APIController::postSendEmailJson($json_data);
//                                                $arr_trx_after_loyalty = $trx_after_loyalty->toArray();
//                                                DB::table('transaction')->where('id',$trx_id)->update($arr_trx_after_loyalty);
//                                            $user->updateBalance($trx_after_loyalty->points_rewarded, $trx_after_loyalty->network_id, 'add');
//                                            $tx['points_balance'] = $user->balance($tx['network_id']);
//                                            $partner  = Partner::find(4298);
//                                            //$template = View::make('sms.reward', array('bonusPoints' => $row[3]))->render();
//                                            if(!empty($rewardsms) &&  floatval($tx['points_rewarded']) > 0){
//                                                $sms_body = $rewardsms;
//                                                $sms_body = str_ireplace('{title}', $user->title, $sms_body);
//                                                $sms_body = str_ireplace('{first_name}', $user->first_name, $sms_body);
//                                                $sms_body = str_ireplace('{last_name}', $user->last_name, $sms_body);
//                                                $sms_body = str_ireplace('{balance}', $user->balance($tx['network_id']), $sms_body);
//
//
//                                                    $sms_body = str_ireplace('{points}', $tx['points_rewarded'], $sms_body);
//                                                    //SMSHelper::send($sms_body, $user, $partner);
//                                            }
                                    }
                                    //$arr_result = Transaction::doReward($params, 'Amount');
                                }
                            }
                        }
                    }

                    $count++;
                }

                return (count($errors) > 0) ?
                        $this->transactionImportFailed($errors, $generatedFileBaseName, 'BISB Bank') : $this->transactionImportSucceed('BISB Bank', $generatedFileBaseName);
            } elseif (($ext == 'TXT' || $ext == 'txt') && strstr($name1, 'BISBCustomers') && strcmp($this->user->api_key, "092c7dfffab8aef9449663c52504bbd7") == 0) {
                //ReferenceID|Title|FirstName|LastName|Email|Mobile|DOB|Gender|MaritalStatus|Occupation|Status
                //0             1   2           3       4       5    6     7        8           9         10
//                echo "ralph";
                $path = $this->customerImportPatch();
                $file->move($path, $generatedFileName);
                $inputFile = $path . '/' . $generatedFileName;

                AuditHelper::record(
                        4, $auth_user->name . " started uploading file: " . $generatedFileName
                );

                $tx = array();

                $contents = File::get($inputFile);
                $contents = nl2br($contents, FALSE);
                $contentLines = explode("<br>", $contents);

                $arrContent = array();
                foreach ($contentLines as $lineread) {
                    $arrContent[] = explode("|", $lineread);
                }
                $count = 0;
                $counterrors = 0;
                $errBool = True;
                $errors = array();
                foreach ($arrContent as $row) {
                    if ($count != 0) {
                        if (trim($row[0]) != '' && trim($row[0]) !== 'EOF') {
                            $row[0] = trim($row[0]);
                            $errBool = True;
//                            $fullname = explode(' ', $row[2]);
//                            $fname = $fullname[0];
//                            $fullname[0]='';
//                            $lname = implode(' ', $fullname);
                            $user = array();
                            $user['id'] = 'NULL';
                            $user['first_name'] = $row[2];
                            $user['last_name'] = $row[3];
                            if (empty($row[3])) {
                                $user['last_name'] = "Empty";
                            }
                            $mobile = $row[5];
                            $mobile = ltrim($mobile, '0');
                            $tomobile = str_replace('BISB-', '', $row[0]);
                            //echo $mobile . "---<br>";
                            if (!is_numeric($mobile) || strlen($mobile) < 8) {
                                $mobile = "4209" . $tomobile;
                                $user['mobile'] = $mobile;
                                $user['normalized_mobile'] = '+973' . $mobile;
                                $user['telephone_code'] = '+973';
                            } elseif (InputHelper::isPhone($mobile)) {
                                $countc = InputHelper::isPhone($mobile);
                                $pos = strpos($mobile, $countc);
                                if ($pos !== false) {
                                    $mobile = substr_replace($mobile, "", $pos, strlen($countc));
                                } else {
                                    $mobile = str_replace($countc, "", $mobile);
                                }
                                $user['mobile'] = $mobile;
                                $user['normalized_mobile'] = '+' . $countc . $mobile;
                                $user['telephone_code'] = '+' . $countc;
                            } else {
                                $user['mobile'] = "";
                                $user['normalized_mobile'] = '';
                                $user['telephone_code'] = '';
                            }

                            //echo "****" . InputHelper::isPhone($mobile) . "+++<br>" . $mobile . "====<br>";
                            $email1 = trim($row[4]);
                            $email3 = '';
                            if ($email1 == '' || !InputHelper::isEmail($email1)) {
                                $email3 = $row[0] . '@BISBCust.com';
                            } else {
                                $email3 = $email1;
                            }

                            $user['email'] = $email3;

                            $unhandledDate = $row[6];
                            if ($unhandledDate == '' || $unhandledDate == ' ') {
                                $unhandledDate = "01/01/1902";
                            }

                            $arrunhandledDate = explode("/", $unhandledDate);
                            $date_unhandled = $arrunhandledDate[1] . "/" . $arrunhandledDate[0] . "/" . $arrunhandledDate[2];

                            $date = strtotime($date_unhandled);
                            $new_date = date('Y-m-d H:i:s', $date);
                            $user['dob'] = $new_date;

                            $user['balance'] = 0;
                            $user['marital_status'] = $row[8];
                            $user['confirm_passcode'] = false;
                            $user['passcode'] = $arrunhandledDate[0] . $arrunhandledDate[1]; //PinCode::newCode();
                            $user['title'] = $row[1];
                            $user['draft'] = false;
                            $user['verified'] = 1;
                            $user['status'] = 'active';
//                            if($row[10] == 'D'){
//                                $user['status'] = "inactive";
//                            }
//                            elseif($row[10]== 'C'){
//                                $user['status'] = "suspended";
//                            }

                            $user['created_at'] = date("Y-m-d H:i:s");
                            $user['updated_at'] = date("Y-m-d H:i:s");
                            $user['ucid'] = UCID::newCustomerID();
                            $user['country_id'] = 2;
                            $user['city_id'] = 97;
                            $user['area_id'] = 15;
                            //$user['password']       = Hash::make($row[22]);
                            // Validate gender
                            switch ($row[7]) {
                                case "Male":
                                    $user['gender'] = 'm';
                                    break;
                                case "Female":
                                    $user['gender'] = 'f';
                                    break;
                                case "M":
                                    $user['gender'] = 'm';
                                    break;
                                case "F":
                                    $user['gender'] = 'f';
                                    break;
                                default:
                                    $user['gender'] = 'm';
                            }

                            // Validate balance
                            if ($user['balance'] == null) {
                                $user['balance'] = 0;
                            }

                            // Validate dob
                            if ($user['dob'] == null) {
                                $user['dob'] = date("Y-m-d H:i:s", strtotime("1 January 1902"));
                            }

                            //set the registration source POS
                            $storeDetails = Store::where('partner_id', 4209)->where('name', 'Registration through Customer File')->first();
                            $posDetails = Pos::where('store_id', $storeDetails->id)->where('name', 'Registration through Customer File POS')->first();
                            $user['registration_source_pos'] = $posDetails->id;

                            $validator = Validator::make($user, array(
                                        'first_name' => 'required',
                                        'last_name' => 'required',
                                        'email' => 'required|email|unique:users',
                                        'mobile' => 'required|numeric',
                                        'dob' => 'required',
                                        'balance' => 'required|numeric'
                            ));

                            $skipstatus = array();
                            $skipstatus[] = 'inactive';
                            $skipstatus[] = 'closed';
                            $countmobileunique = DB::table('users')->where('normalized_mobile', $user['normalized_mobile'])->whereNotIn('status', $skipstatus)->count();

                            $count = DB::table('users')->where('email', $user['email'])->where('draft', false)->count();
                            $countmobile = DB::table('users')->where('normalized_mobile', $user['normalized_mobile'])->count();


                            $reference = Reference::where('number', $row[0])->first();
                            if (!$validator->fails() && $countmobileunique <= 0) {

                                if (!isset($reference)) {
                                    // Create the user
                                    $last_id = DB::table('users')->insertGetId($user);
                                    //$last_id = DB::table('users')->getPdo()->lastInsertId();
                                    // Assign the role
                                    DB::table('role_user')->insert(array(
                                        'role_id' => 4,
                                        'user_id' => $last_id
                                    ));
                                    DB::table('reference')->insert(array(
                                        'number' => $row[0],
                                        'partner_id' => 4209,
                                        'user_id' => $last_id,
                                        'created_at' => date('Y-m-d H:i:s', time()),
                                        'updated_at' => date('Y-m-d H:i:s', time())
                                    ));
                                    DB::table('partner_user')->insert(array(
                                        'partner_id' => 4209,
                                        'user_id' => $last_id
                                    ));
                                    DB::table('partner_user')->insert(array(
                                        'partner_id' => 1,
                                        'user_id' => $last_id
                                    ));
                                    DB::table('segment_user')->insert(array(
                                        'segment_id' => Partner::find(4209)->segments->first()->id,
                                        'user_id' => $last_id
                                    ));

                                    //create default balance
                                    $createdUser = User::find($last_id);
                                    $createdUser->updateBalance(0, $this->partner->firstNetwork()->id, 'add');
                                    //Default Tier affiliation to Member
                                    $defaultTier = Tier::where('partner_id', 4209)->where('name', 'like', 'Default Tier%')->first();
                                    if (!empty($defaultTier)) {
                                        $defaultTierCriteria = Tier::where('tier_id', $defaultTier->id)->where('name', 'like', 'Default Tier Criteria%')->first();
                                        if (!empty($defaultTierCriteria)) {
                                            DB::table('tiercriteria_user')->insert(array(
                                                'tiercriteria_id' => $defaultTierCriteria->id,
                                                'user_id' => $last_id
                                            ));
                                        }
                                    }

                                    $userSMS = User::find($last_id);
                                    $custRef = $row[0];
                                    $template = View::make('sms.new-user-bisb', array('ref' => $custRef, 'user' => $userSMS))->render();
                                    $partner = Partner::find('4209');
                                    SMSHelper::send($template, $userSMS, $partner);
                                    //$userSMS->sendWelcomeMessageNew('4209');
                                    //$userSMS->sendAccountPin();
                                    $errBool = False;
                                    //DB::table('transaction')->insert($tx);
                                } elseif (isset($reference)) {

                                    $user1 = $reference->user_id;
                                    //
                                    //                                    $userChange = array();
                                    //                                    if($row[10] == 'A'){
                                    //                                        $userChange['status'] = 'active';
                                    //                                    }
                                    //                                    elseif($row[10] == 'D'){
                                    //                                        $userChange['status'] = "inactive";
                                    //                                    }
                                    //                                    elseif($row[10]== 'C'){
                                    //                                        $userChange['status'] = "suspended";
                                    //                                    }
                                    //                                    User::where('id',$user)->update($userChange);
                                    //                                    $errBool = False;
                                    unset($user['id']);
                                    unset($user['passcode']);
                                    DB::table('partner_user')->insert(array(
                                        'partner_id' => 4209,
                                        'user_id' => $user1
                                    ));

                                    User::where('id', $user1)->update($user);
                                    $errBool = False;
                                }
                            } else {
                                //$reference = Reference::where('number', $row[0])->first();
                                if (isset($reference) && $countmobileunique <= 0) {

                                    $user1 = $reference->user_id;
//
//                                    $userChange = array();
//                                    if($row[10] == 'A'){
//                                        $userChange['status'] = 'active';
//                                    }
//                                    elseif($row[10] == 'D'){
//                                        $userChange['status'] = "inactive";
//                                    }
//                                    elseif($row[10]== 'C'){
//                                        $userChange['status'] = "suspended";
//                                    }
//                                    User::where('id',$user)->update($userChange);
//                                    $errBool = False;
                                    unset($user['id']);
                                    unset($user['passcode']);
                                    DB::table('partner_user')->insert(array(
                                        'partner_id' => 4209,
                                        'user_id' => $user1
                                    ));
                                    User::where('id', $user1)->update($user);
                                    $errBool = False;
                                } else {
                                    if ($countmobileunique <= 0) {
                                        $count = DB::table('users')->where('email', $user['email'])->where('draft', false)->count();
                                        $countmobile = DB::table('users')->where('normalized_mobile', $user['normalized_mobile'])->where('draft', false)->count();

                                        if ($count == 0 && empty($user['email'])) {
                                            $email3 = $row[0] . '@BISBCust.com';
                                            $user['email'] = $email3;
                                            $validator = Validator::make($user, array(
                                                        'first_name' => 'required',
                                                        'last_name' => 'required',
                                                        'email' => 'required|email|unique:users',
                                                        'mobile' => 'required|numeric',
                                                        'dob' => 'required',
                                                        'balance' => 'required|numeric'
                                            ));
                                            if (!$validator->fails()) {
                                                $last_id = DB::table('users')->insertGetId($user);
                                                //$last_id = DB::table('users')->getPdo()->lastInsertId();
                                                // Assign the role
                                                DB::table('role_user')->insert(array(
                                                    'role_id' => 4,
                                                    'user_id' => $last_id
                                                ));
                                                DB::table('reference')->insert(array(
                                                    'number' => $row[0],
                                                    'partner_id' => 4209,
                                                    'user_id' => $last_id,
                                                    'created_at' => date('Y-m-d H:i:s', time()),
                                                    'updated_at' => date('Y-m-d H:i:s', time())
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => 4209,
                                                    'user_id' => $last_id
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => 1,
                                                    'user_id' => $last_id
                                                ));
                                                DB::table('segment_user')->insert(array(
                                                    'segment_id' => Partner::find(4209)->segments->first()->id,
                                                    'user_id' => $last_id
                                                ));

                                                //Default Tier affiliation to Member
                                                $defaultTier = Tier::where('partner_id', 4209)->where('name', 'like', 'Default Tier%')->first();
                                                if (!empty($defaultTier)) {
                                                    $defaultTierCriteria = Tier::where('tier_id', $defaultTier->id)->where('name', 'like', 'Default Tier Criteria%')->first();
                                                    if (!empty($defaultTierCriteria)) {
                                                        DB::table('tiercriteria_user')->insert(array(
                                                            'tiercriteria_id' => $defaultTierCriteria->id,
                                                            'user_id' => $last_id
                                                        ));
                                                    }
                                                }

                                                $userSMS = User::find($last_id);

                                                //$userSMS->sendWelcomeMessage();
                                                //$userSMS->sendAccountPin();
                                                $errBool = False;
                                            } elseif ($validator->fails()) {
                                                if ($countmobile == 0 || empty($user['normalized_mobile'])) {
                                                    $mobile = "4209" . $tomobile;
                                                    $user['mobile'] = $mobile;
                                                    $user['normalized_mobile'] = '+973' . $mobile;
                                                    $user['telephone_code'] = '+973';
                                                    $validator = Validator::make($user, array(
                                                                'first_name' => 'required',
                                                                'last_name' => 'required',
                                                                'email' => 'required|email|unique:users',
                                                                'mobile' => 'required|numeric',
                                                                'dob' => 'required',
                                                                'balance' => 'required|numeric'
                                                    ));
                                                    if (!$validator->fails()) {
                                                        $last_id = DB::table('users')->insertGetId($user);
                                                        //$last_id = DB::table('users')->getPdo()->lastInsertId();
                                                        // Assign the role
                                                        DB::table('role_user')->insert(array(
                                                            'role_id' => 4,
                                                            'user_id' => $last_id
                                                        ));
                                                        DB::table('reference')->insert(array(
                                                            'number' => $row[0],
                                                            'partner_id' => 4209,
                                                            'user_id' => $last_id,
                                                            'created_at' => date('Y-m-d H:i:s', time()),
                                                            'updated_at' => date('Y-m-d H:i:s', time())
                                                        ));
                                                        DB::table('partner_user')->insert(array(
                                                            'partner_id' => 4209,
                                                            'user_id' => $last_id
                                                        ));
                                                        DB::table('partner_user')->insert(array(
                                                            'partner_id' => 1,
                                                            'user_id' => $last_id
                                                        ));
                                                        DB::table('segment_user')->insert(array(
                                                            'segment_id' => Partner::find(4209)->segments->first()->id,
                                                            'user_id' => $last_id
                                                        ));

                                                        //Default Tier affiliation to Member
                                                        $defaultTier = Tier::where('partner_id', 4209)->where('name', 'like', 'Default Tier%')->first();
                                                        if (!empty($defaultTier)) {
                                                            $defaultTierCriteria = Tier::where('tier_id', $defaultTier->id)->where('name', 'like', 'Default Tier Criteria%')->first();
                                                            if (!empty($defaultTierCriteria)) {
                                                                DB::table('tiercriteria_user')->insert(array(
                                                                    'tiercriteria_id' => $defaultTierCriteria->id,
                                                                    'user_id' => $last_id
                                                                ));
                                                            }
                                                        }
                                                        $userSMS = User::find($last_id);

                                                        //$userSMS->sendWelcomeMessage();
                                                        //$userSMS->sendAccountPin();
                                                        $errBool = False;
                                                    }
                                                }
                                            }
                                        } elseif ($count > 0) {

                                            $userByEmail = DB::table('users')->where('email', $user['email'])->where('draft', false)->first();
                                            $userRefAvailable = User::where('id', $userByEmail->id)->where('draft', false)->whereHas('references', function($q) {
                                                        $q->where('number', 'like', "%BISB-%");
                                                    })->count();
                                            if ($userRefAvailable == 0) {
                                                DB::table('reference')->insert(array(
                                                    'number' => $row[0],
                                                    'partner_id' => 4209,
                                                    'user_id' => $userByEmail->id,
                                                    'created_at' => date('Y-m-d H:i:s', time()),
                                                    'updated_at' => date('Y-m-d H:i:s', time())
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => 4209,
                                                    'user_id' => $userByEmail->id
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => 1,
                                                    'user_id' => $last_id
                                                ));
                                                $errBool = False;
                                            } else {
                                                $email3 = $row[0] . '@BISBCust.com';
                                                $user['email'] = $email3;
                                                $last_id = DB::table('users')->insertGetId($user);
                                                DB::table('role_user')->insert(array(
                                                    'role_id' => 4,
                                                    'user_id' => $last_id
                                                ));
                                                DB::table('reference')->insert(array(
                                                    'number' => $row[0],
                                                    'partner_id' => 4209,
                                                    'user_id' => $last_id,
                                                    'created_at' => date('Y-m-d H:i:s', time()),
                                                    'updated_at' => date('Y-m-d H:i:s', time())
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => 4209,
                                                    'user_id' => $last_id
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => 1,
                                                    'user_id' => $last_id
                                                ));
                                                DB::table('segment_user')->insert(array(
                                                    'segment_id' => Partner::find(4209)->segments->first()->id,
                                                    'user_id' => $last_id
                                                ));

                                                //Default Tier affiliation to Member
                                                $defaultTier = Tier::where('partner_id', 4209)->where('name', 'like', 'Default Tier%')->first();
                                                if (!empty($defaultTier)) {
                                                    $defaultTierCriteria = Tier::where('tier_id', $defaultTier->id)->where('name', 'like', 'Default Tier Criteria%')->first();
                                                    if (!empty($defaultTierCriteria)) {
                                                        DB::table('tiercriteria_user')->insert(array(
                                                            'tiercriteria_id' => $defaultTierCriteria->id,
                                                            'user_id' => $last_id
                                                        ));
                                                    }
                                                }
                                                $errBool = False;
                                            }
                                        } elseif ($countmobile > 0) {
                                            $userByMobile = DB::table('users')->where('normalized_mobile', $user['normalized_mobile'])->where('draft', false)->first();
                                            $userRefAvailable = User::where('id', $userByMobile->id)->where('draft', false)->whereHas('references', function($q) {
                                                        $q->where('number', 'like', "%BISB-%");
                                                    })->count();
                                            if ($userRefAvailable == 0) {
                                                DB::table('reference')->insert(array(
                                                    'number' => $row[0],
                                                    'partner_id' => 4209,
                                                    'user_id' => $userByMobile->id,
                                                    'created_at' => date('Y-m-d H:i:s', time()),
                                                    'updated_at' => date('Y-m-d H:i:s', time())
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => 4209,
                                                    'user_id' => $userByMobile->id
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => 1,
                                                    'user_id' => $last_id
                                                ));
                                                $errBool = False;
                                            } else {
                                                $tomobile = str_replace('BISB-', '', $row[0]);
                                                $mobile = "4209" . $tomobile;
                                                $user['mobile'] = $mobile;
                                                $user['normalized_mobile'] = '+973' . $mobile;
                                                $user['telephone_code'] = '+973';
                                                $last_id = DB::table('users')->insertGetId($user);
                                                DB::table('role_user')->insert(array(
                                                    'role_id' => 4,
                                                    'user_id' => $last_id
                                                ));
                                                DB::table('reference')->insert(array(
                                                    'number' => $row[0],
                                                    'partner_id' => 4209,
                                                    'user_id' => $last_id,
                                                    'created_at' => date('Y-m-d H:i:s', time()),
                                                    'updated_at' => date('Y-m-d H:i:s', time())
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => 4209,
                                                    'user_id' => $last_id
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => 1,
                                                    'user_id' => $last_id
                                                ));
                                                DB::table('segment_user')->insert(array(
                                                    'segment_id' => Partner::find(4209)->segments->first()->id,
                                                    'user_id' => $last_id
                                                ));
                                                //Default Tier affiliation to Member
                                                $defaultTier = Tier::where('partner_id', 4209)->where('name', 'like', 'Default Tier%')->first();
                                                if (!empty($defaultTier)) {
                                                    $defaultTierCriteria = Tier::where('tier_id', $defaultTier->id)->where('name', 'like', 'Default Tier Criteria%')->first();
                                                    if (!empty($defaultTierCriteria)) {
                                                        DB::table('tiercriteria_user')->insert(array(
                                                            'tiercriteria_id' => $defaultTierCriteria->id,
                                                            'user_id' => $last_id
                                                        ));
                                                    }
                                                }

                                                $errBool = False;
                                            }
                                        }
                                    } else {
                                        $userByMobile = DB::table('users')->where('normalized_mobile', $user['normalized_mobile'])->where('draft', false)->first();
                                        $userRefAvailable = User::where('id', $userByMobile->id)->where('draft', false)->whereHas('references', function($q) {
                                                    $q->where('number', 'like', "%BISB-%");
                                                })->count();
                                        if ($userRefAvailable == 0) {
                                            DB::table('reference')->insert(array(
                                                'number' => $row[0],
                                                'partner_id' => 4209,
                                                'user_id' => $userByMobile->id,
                                                'created_at' => date('Y-m-d H:i:s', time()),
                                                'updated_at' => date('Y-m-d H:i:s', time())
                                            ));
                                            DB::table('partner_user')->insert(array(
                                                'partner_id' => 4209,
                                                'user_id' => $userByMobile->id
                                            ));
                                            DB::table('partner_user')->insert(array(
                                                'partner_id' => 1,
                                                'user_id' => $last_id
                                            ));
                                            $errBool = False;
                                        } else {
                                            $tomobile = str_replace('BISB-', '', $row[0]);
                                            $mobile = "4209" . $tomobile;
                                            $user['mobile'] = $mobile;
                                            $user['normalized_mobile'] = '+973' . $mobile;
                                            $user['telephone_code'] = '+973';
                                            $last_id = DB::table('users')->insertGetId($user);
                                            DB::table('role_user')->insert(array(
                                                'role_id' => 4,
                                                'user_id' => $last_id
                                            ));
                                            DB::table('partner_user')->insert(array(
                                                'partner_id' => 4209,
                                                'user_id' => $last_id
                                            ));
                                            DB::table('partner_user')->insert(array(
                                                'partner_id' => 1,
                                                'user_id' => $last_id
                                            ));
                                            DB::table('segment_user')->insert(array(
                                                'segment_id' => Partner::find(4209)->segments->first()->id,
                                                'user_id' => $last_id
                                            ));
                                            //Default Tier affiliation to Member
                                            $defaultTier = Tier::where('partner_id', 4209)->where('name', 'like', 'Default Tier%')->first();
                                            if (!empty($defaultTier)) {
                                                $defaultTierCriteria = Tier::where('tier_id', $defaultTier->id)->where('name', 'like', 'Default Tier Criteria%')->first();
                                                if (!empty($defaultTierCriteria)) {
                                                    DB::table('tiercriteria_user')->insert(array(
                                                        'tiercriteria_id' => $defaultTierCriteria->id,
                                                        'user_id' => $last_id
                                                    ));
                                                }
                                            }
                                            $errBool = False;
                                            $counterrors = $counterrors + 1;
                                            $errors[] = $row;
                                        }
                                    }
                                }
                                if ($errBool == True) {
                                    $counterrors = $counterrors + 1;
                                    $errors[] = $row;
//                                    echo "<pre>1";
//                                    var_dump($errors);
//                                    exit();
                                }
                            }
                        }
                    }
                    $count++;
                }

                return (count($errors) > 0) ?
                        $this->customerImportFailed($errors, $generatedFileBaseName, 'BISB Bank') : $this->customerImportSucceed('BISB Bank', $generatedFileBaseName);
            } elseif (($ext == 'TXT' || $ext == 'txt') && strstr($name1, 'ABBCustomer') && strcmp($this->user->api_key, "f96d7e8fbe3dfc4da3a50e0c131f3038") == 0) {
                //ReferenceID|Title|FirstName|LastName|Email|Mobile|DOB|Gender|MaritalStatus|Occupation|Status
                //0             1   2           3       4       5    6     7        8           9         10
                $path = $this->customerImportPatch();
                $file->move($path, $generatedFileName);
                $inputFile = $path . '/' . $generatedFileName;

                AuditHelper::record(
                        4, $auth_user->name . " started uploading file: " . $generatedFileName
                );

                $tx = array();

                $contents = File::get($inputFile);
                $contents = nl2br($contents, FALSE);
                $contentLines = explode("<br>", $contents);

                $arrContent = array();
                foreach ($contentLines as $lineread) {
                    $arrContent[] = explode("|", $lineread);
                }
                $count = 0;
                $counterrors = 0;
                $errBool = True;
                $errors = array();
                foreach ($arrContent as $row) {
                    if ($count != 0) {
                        if (trim($row[0]) != '' && trim($row[0]) !== 'EOF') {
                            $row[0] = trim($row[0]);
                            $errBool = True;
//                            $fullname = explode(' ', $row[2]);
//                            $fname = $fullname[0];
//                            $fullname[0]='';
//                            $lname = implode(' ', $fullname);
                            $user = array();
                            $user['id'] = 'NULL';
                            $user['first_name'] = $row[2];
                            $user['last_name'] = $row[3];
                            if (empty($row[3])) {
                                $user['last_name'] = "Empty";
                            }
                            $mobile = $row[5];
                            $mobile = ltrim($mobile, '0');
                            $tomobile = str_replace('ABB-', '', $row[0]);
                            //echo $mobile . "---<br>";
                            if (!is_numeric($mobile) || strlen($mobile) < 8) {
                                $mobile = "4208" . $tomobile;
                                $user['mobile'] = $mobile;
                                $user['normalized_mobile'] = '+973' . $mobile;
                                $user['telephone_code'] = '+973';
                            } elseif (InputHelper::isPhone($mobile)) {
                                $countc = InputHelper::isPhone($mobile);
                                $pos = strpos($mobile, $countc);
                                if ($pos !== false) {
                                    $mobile = substr_replace($mobile, "", $pos, strlen($countc));
                                } else {
                                    $mobile = str_replace($countc, "", $mobile);
                                }
                                $user['mobile'] = $mobile;
                                $user['normalized_mobile'] = '+' . $countc . $mobile;
                                $user['telephone_code'] = '+' . $countc;
                            } else {
                                $user['mobile'] = "";
                                $user['normalized_mobile'] = '';
                                $user['telephone_code'] = '';
                            }

                            //echo "****" . InputHelper::isPhone($mobile) . "+++<br>" . $mobile . "====<br>";
                            $email1 = trim($row[4]);
                            $email3 = '';
                            if ($email1 == '' || !InputHelper::isEmail($email1)) {
                                $email3 = $row[0] . '@ABBCust.com';
                            } else {
                                $email3 = $email1;
                            }

                            $user['email'] = $email3;

                            $unhandledDate = $row[6];
                            if ($unhandledDate == '' || $unhandledDate == ' ') {
                                $unhandledDate = "01/01/1902";
                            }

                            $arrunhandledDate = explode("/", $unhandledDate);
                            $date_unhandled = $arrunhandledDate[1] . "/" . $arrunhandledDate[0] . "/" . $arrunhandledDate[2];

                            $date = strtotime($date_unhandled);
                            $new_date = date('Y-m-d H:i:s', $date);
                            $user['dob'] = $new_date;

                            $user['balance'] = 0;
                            $user['marital_status'] = $row[8];
                            $user['confirm_passcode'] = false;
                            $user['passcode'] = $arrunhandledDate[0] . $arrunhandledDate[1]; //PinCode::newCode();
                            $user['title'] = $row[1];
                            $user['draft'] = false;
                            $user['verified'] = 1;
                            $user['status'] = 'active';
//                            if($row[10] == 'D'){
//                                $user['status'] = "inactive";
//                            }
//                            elseif($row[10]== 'C'){
//                                $user['status'] = "suspended";
//                            }

                            $user['created_at'] = date("Y-m-d H:i:s");
                            $user['updated_at'] = date("Y-m-d H:i:s");
                            $user['ucid'] = UCID::newCustomerID();
                            $user['country_id'] = 6;
                            //$user['password']       = Hash::make($row[22]);
                            // Validate gender
                            switch ($row[7]) {
                                case "Male":
                                    $user['gender'] = 'm';
                                    break;
                                case "Female":
                                    $user['gender'] = 'f';
                                    break;
                                default:
                                    $user['gender'] = 'm';
                            }

                            // Validate balance
                            if ($user['balance'] == null) {
                                $user['balance'] = 0;
                            }

                            // Validate dob
                            if ($user['dob'] == null) {
                                $user['dob'] = date("Y-m-d H:i:s", strtotime("1 January 1902"));
                            }

                            //set the registration source POS
                            $storeDetails = Store::where('partner_id', 4208)->where('name', 'Registration through Customer File')->first();
                            $posDetails = Pos::where('store_id', $storeDetails->id)->where('name', 'Registration through Customer File POS')->first();
                            $user['registration_source_pos'] = $posDetails->id;

                            $validator = Validator::make($user, array(
                                        'first_name' => 'required',
                                        'last_name' => 'required',
                                        'email' => 'required|email|unique:users',
                                        'mobile' => 'required|numeric',
                                        'dob' => 'required',
                                        'balance' => 'required|numeric'
                            ));

                            $skipstatus = array();
                            $skipstatus[] = 'inactive';
                            $skipstatus[] = 'closed';
                            $countmobileunique = DB::table('users')->where('normalized_mobile', $user['normalized_mobile'])->whereNotIn('status', $skipstatus)->count();

                            $count = DB::table('users')->where('email', $user['email'])->where('draft', false)->count();
                            $countmobile = DB::table('users')->where('normalized_mobile', $user['normalized_mobile'])->count();


                            $reference = Reference::where('number', $row[0])->first();
                            if (!$validator->fails() && $countmobileunique <= 0) {

                                if (!isset($reference)) {
                                    // Create the user
                                    $last_id = DB::table('users')->insertGetId($user);
                                    //$last_id = DB::table('users')->getPdo()->lastInsertId();
                                    // Assign the role
                                    DB::table('role_user')->insert(array(
                                        'role_id' => 4,
                                        'user_id' => $last_id
                                    ));
                                    DB::table('reference')->insert(array(
                                        'number' => $row[0],
                                        'partner_id' => 4208,
                                        'user_id' => $last_id,
                                        'created_at' => date('Y-m-d H:i:s', time()),
                                        'updated_at' => date('Y-m-d H:i:s', time())
                                    ));
                                    DB::table('partner_user')->insert(array(
                                        'partner_id' => 4208,
                                        'user_id' => $last_id
                                    ));
                                    DB::table('partner_user')->insert(array(
                                        'partner_id' => 1,
                                        'user_id' => $last_id
                                    ));
                                    DB::table('segment_user')->insert(array(
                                        'segment_id' => Partner::find(4208)->segments->first()->id,
                                        'user_id' => $last_id
                                    ));

                                    //create default balance
                                    $createdUser = User::find($last_id);
                                    $createdUser->updateBalance(0, $this->partner->firstNetwork()->id, 'add');
                                    //Default Tier affiliation to Member
                                    $defaultTier = Tier::where('partner_id', 4208)->where('name', 'like', 'Default Tier%')->first();
                                    if (!empty($defaultTier)) {
                                        $defaultTierCriteria = Tier::where('tier_id', $defaultTier->id)->where('name', 'like', 'Default Tier Criteria%')->first();
                                        if (!empty($defaultTierCriteria)) {
                                            DB::table('tiercriteria_user')->insert(array(
                                                'tiercriteria_id' => $defaultTierCriteria->id,
                                                'user_id' => $last_id
                                            ));
                                        }
                                    }

                                    $userSMS = User::find($last_id);
                                    $custRef = $row[0];
                                    $template = View::make('sms.new-user-aib', array('ref' => $custRef, 'user' => $userSMS))->render();
                                    $partner = Partner::find('4208');
                                    SMSHelper::send($template, $userSMS, $partner);
                                    //$userSMS->sendWelcomeMessageNew('4208');
                                    //$userSMS->sendAccountPin();
                                    $errBool = False;
                                    //DB::table('transaction')->insert($tx);
                                } elseif (isset($reference)) {

                                    $user1 = $reference->user_id;
                                    //
                                    //                                    $userChange = array();
                                    //                                    if($row[10] == 'A'){
                                    //                                        $userChange['status'] = 'active';
                                    //                                    }
                                    //                                    elseif($row[10] == 'D'){
                                    //                                        $userChange['status'] = "inactive";
                                    //                                    }
                                    //                                    elseif($row[10]== 'C'){
                                    //                                        $userChange['status'] = "suspended";
                                    //                                    }
                                    //                                    User::where('id',$user)->update($userChange);
                                    //                                    $errBool = False;
                                    unset($user['id']);
                                    unset($user['passcode']);
                                    DB::table('partner_user')->insert(array(
                                        'partner_id' => 4208,
                                        'user_id' => $user1
                                    ));

                                    User::where('id', $user1)->update($user);
                                    $errBool = False;
                                }
                            } else {
                                //$reference = Reference::where('number', $row[0])->first();
                                if (isset($reference) && $countmobileunique <= 0) {

                                    $user1 = $reference->user_id;
//
//                                    $userChange = array();
//                                    if($row[10] == 'A'){
//                                        $userChange['status'] = 'active';
//                                    }
//                                    elseif($row[10] == 'D'){
//                                        $userChange['status'] = "inactive";
//                                    }
//                                    elseif($row[10]== 'C'){
//                                        $userChange['status'] = "suspended";
//                                    }
//                                    User::where('id',$user)->update($userChange);
//                                    $errBool = False;
                                    unset($user['id']);
                                    unset($user['passcode']);
                                    DB::table('partner_user')->insert(array(
                                        'partner_id' => 4208,
                                        'user_id' => $user1
                                    ));
                                    User::where('id', $user1)->update($user);
                                    $errBool = False;
                                } else {
                                    if ($countmobileunique <= 0) {
                                        $count = DB::table('users')->where('email', $user['email'])->where('draft', false)->count();
                                        $countmobile = DB::table('users')->where('normalized_mobile', $user['normalized_mobile'])->where('draft', false)->count();

                                        if ($count == 0 && empty($user['email'])) {
                                            $email3 = $row[0] . '@ABBCust.com';
                                            $user['email'] = $email3;
                                            $validator = Validator::make($user, array(
                                                        'first_name' => 'required',
                                                        'last_name' => 'required',
                                                        'email' => 'required|email|unique:users',
                                                        'mobile' => 'required|numeric',
                                                        'dob' => 'required',
                                                        'balance' => 'required|numeric'
                                            ));
                                            if (!$validator->fails()) {
                                                $last_id = DB::table('users')->insertGetId($user);
                                                //$last_id = DB::table('users')->getPdo()->lastInsertId();
                                                // Assign the role
                                                DB::table('role_user')->insert(array(
                                                    'role_id' => 4,
                                                    'user_id' => $last_id
                                                ));
                                                DB::table('reference')->insert(array(
                                                    'number' => $row[0],
                                                    'partner_id' => 4208,
                                                    'user_id' => $last_id,
                                                    'created_at' => date('Y-m-d H:i:s', time()),
                                                    'updated_at' => date('Y-m-d H:i:s', time())
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => 4208,
                                                    'user_id' => $last_id
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => 1,
                                                    'user_id' => $last_id
                                                ));
                                                DB::table('segment_user')->insert(array(
                                                    'segment_id' => Partner::find(4208)->segments->first()->id,
                                                    'user_id' => $last_id
                                                ));

                                                //Default Tier affiliation to Member
                                                $defaultTier = Tier::where('partner_id', 4208)->where('name', 'like', 'Default Tier%')->first();
                                                if (!empty($defaultTier)) {
                                                    $defaultTierCriteria = Tier::where('tier_id', $defaultTier->id)->where('name', 'like', 'Default Tier Criteria%')->first();
                                                    if (!empty($defaultTierCriteria)) {
                                                        DB::table('tiercriteria_user')->insert(array(
                                                            'tiercriteria_id' => $defaultTierCriteria->id,
                                                            'user_id' => $last_id
                                                        ));
                                                    }
                                                }

                                                $userSMS = User::find($last_id);
                                                //$userSMS->sendWelcomeMessage();
//                                                $userSMS->sendAccountPin();
                                                $errBool = False;
                                            } elseif ($validator->fails()) {
                                                if ($countmobile == 0 || empty($user['normalized_mobile'])) {
                                                    $mobile = "4208" . $tomobile;
                                                    $user['mobile'] = $mobile;
                                                    $user['normalized_mobile'] = '+973' . $mobile;
                                                    $user['telephone_code'] = '+973';
                                                    $validator = Validator::make($user, array(
                                                                'first_name' => 'required',
                                                                'last_name' => 'required',
                                                                'email' => 'required|email|unique:users',
                                                                'mobile' => 'required|numeric',
                                                                'dob' => 'required',
                                                                'balance' => 'required|numeric'
                                                    ));
                                                    if (!$validator->fails()) {
                                                        $last_id = DB::table('users')->insertGetId($user);
                                                        //$last_id = DB::table('users')->getPdo()->lastInsertId();
                                                        // Assign the role
                                                        DB::table('role_user')->insert(array(
                                                            'role_id' => 4,
                                                            'user_id' => $last_id
                                                        ));
                                                        DB::table('reference')->insert(array(
                                                            'number' => $row[0],
                                                            'partner_id' => 4208,
                                                            'user_id' => $last_id,
                                                            'created_at' => date('Y-m-d H:i:s', time()),
                                                            'updated_at' => date('Y-m-d H:i:s', time())
                                                        ));
                                                        DB::table('partner_user')->insert(array(
                                                            'partner_id' => 4208,
                                                            'user_id' => $last_id
                                                        ));
                                                        DB::table('partner_user')->insert(array(
                                                            'partner_id' => 1,
                                                            'user_id' => $last_id
                                                        ));
                                                        DB::table('segment_user')->insert(array(
                                                            'segment_id' => Partner::find(4208)->segments->first()->id,
                                                            'user_id' => $last_id
                                                        ));

                                                        //Default Tier affiliation to Member
                                                        $defaultTier = Tier::where('partner_id', 4208)->where('name', 'like', 'Default Tier%')->first();
                                                        if (!empty($defaultTier)) {
                                                            $defaultTierCriteria = Tier::where('tier_id', $defaultTier->id)->where('name', 'like', 'Default Tier Criteria%')->first();
                                                            if (!empty($defaultTierCriteria)) {
                                                                DB::table('tiercriteria_user')->insert(array(
                                                                    'tiercriteria_id' => $defaultTierCriteria->id,
                                                                    'user_id' => $last_id
                                                                ));
                                                            }
                                                        }
                                                        $userSMS = User::find($last_id);
                                                        //$userSMS->sendWelcomeMessage();
//                                                        $userSMS->sendAccountPin();
                                                        $errBool = False;
                                                    }
                                                }
                                            }
                                        } elseif ($count > 0) {

                                            $userByEmail = DB::table('users')->where('email', $user['email'])->where('draft', false)->first();
                                            $userRefAvailable = User::where('id', $userByEmail->id)->where('draft', false)->whereHas('references', function($q) {
                                                        $q->where('number', 'like', "%ABB-%");
                                                    })->count();
                                            if ($userRefAvailable == 0) {
                                                DB::table('reference')->insert(array(
                                                    'number' => $row[0],
                                                    'partner_id' => 4208,
                                                    'user_id' => $userByEmail->id,
                                                    'created_at' => date('Y-m-d H:i:s', time()),
                                                    'updated_at' => date('Y-m-d H:i:s', time())
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => 4208,
                                                    'user_id' => $userByEmail->id
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => 1,
                                                    'user_id' => $last_id
                                                ));
                                                $errBool = False;
                                            } else {
                                                $email3 = $row[0] . '@ABBCust.com';
                                                $user['email'] = $email3;
                                                $last_id = DB::table('users')->insertGetId($user);
                                                DB::table('role_user')->insert(array(
                                                    'role_id' => 4,
                                                    'user_id' => $last_id
                                                ));
                                                DB::table('reference')->insert(array(
                                                    'number' => $row[0],
                                                    'partner_id' => 4208,
                                                    'user_id' => $last_id,
                                                    'created_at' => date('Y-m-d H:i:s', time()),
                                                    'updated_at' => date('Y-m-d H:i:s', time())
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => 4208,
                                                    'user_id' => $last_id
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => 1,
                                                    'user_id' => $last_id
                                                ));
                                                DB::table('segment_user')->insert(array(
                                                    'segment_id' => Partner::find(4208)->segments->first()->id,
                                                    'user_id' => $last_id
                                                ));

                                                //Default Tier affiliation to Member
                                                $defaultTier = Tier::where('partner_id', 4208)->where('name', 'like', 'Default Tier%')->first();
                                                if (!empty($defaultTier)) {
                                                    $defaultTierCriteria = Tier::where('tier_id', $defaultTier->id)->where('name', 'like', 'Default Tier Criteria%')->first();
                                                    if (!empty($defaultTierCriteria)) {
                                                        DB::table('tiercriteria_user')->insert(array(
                                                            'tiercriteria_id' => $defaultTierCriteria->id,
                                                            'user_id' => $last_id
                                                        ));
                                                    }
                                                }
                                                $errBool = False;
                                            }
                                        } elseif ($countmobile > 0) {
                                            $userByMobile = DB::table('users')->where('normalized_mobile', $user['normalized_mobile'])->where('draft', false)->first();
                                            $userRefAvailable = User::where('id', $userByMobile->id)->where('draft', false)->whereHas('references', function($q) {
                                                        $q->where('number', 'like', "%ABB-%");
                                                    })->count();
                                            if ($userRefAvailable == 0) {
                                                DB::table('reference')->insert(array(
                                                    'number' => $row[0],
                                                    'partner_id' => 4208,
                                                    'user_id' => $userByMobile->id,
                                                    'created_at' => date('Y-m-d H:i:s', time()),
                                                    'updated_at' => date('Y-m-d H:i:s', time())
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => 4208,
                                                    'user_id' => $userByMobile->id
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => 1,
                                                    'user_id' => $last_id
                                                ));
                                                $errBool = False;
                                            } else {
                                                $tomobile = str_replace('ABB-', '', $row[0]);
                                                $mobile = "4208" . $tomobile;
                                                $user['mobile'] = $mobile;
                                                $user['normalized_mobile'] = '+973' . $mobile;
                                                $user['telephone_code'] = '+973';
                                                $last_id = DB::table('users')->insertGetId($user);
                                                DB::table('role_user')->insert(array(
                                                    'role_id' => 4,
                                                    'user_id' => $last_id
                                                ));
                                                DB::table('reference')->insert(array(
                                                    'number' => $row[0],
                                                    'partner_id' => 4208,
                                                    'user_id' => $last_id,
                                                    'created_at' => date('Y-m-d H:i:s', time()),
                                                    'updated_at' => date('Y-m-d H:i:s', time())
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => 4208,
                                                    'user_id' => $last_id
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => 1,
                                                    'user_id' => $last_id
                                                ));
                                                DB::table('segment_user')->insert(array(
                                                    'segment_id' => Partner::find(4208)->segments->first()->id,
                                                    'user_id' => $last_id
                                                ));
                                                //Default Tier affiliation to Member
                                                $defaultTier = Tier::where('partner_id', 4208)->where('name', 'like', 'Default Tier%')->first();
                                                if (!empty($defaultTier)) {
                                                    $defaultTierCriteria = Tier::where('tier_id', $defaultTier->id)->where('name', 'like', 'Default Tier Criteria%')->first();
                                                    if (!empty($defaultTierCriteria)) {
                                                        DB::table('tiercriteria_user')->insert(array(
                                                            'tiercriteria_id' => $defaultTierCriteria->id,
                                                            'user_id' => $last_id
                                                        ));
                                                    }
                                                }

                                                $errBool = False;
                                            }
                                        }
                                    } else {
                                        $userByMobile = DB::table('users')->where('normalized_mobile', $user['normalized_mobile'])->where('draft', false)->first();
                                        $userRefAvailable = User::where('id', $userByMobile->id)->where('draft', false)->whereHas('references', function($q) {
                                                    $q->where('number', 'like', "%ABB-%");
                                                })->count();
                                        if ($userRefAvailable == 0) {
                                            DB::table('reference')->insert(array(
                                                'number' => $row[0],
                                                'partner_id' => 4208,
                                                'user_id' => $userByMobile->id,
                                                'created_at' => date('Y-m-d H:i:s', time()),
                                                'updated_at' => date('Y-m-d H:i:s', time())
                                            ));
                                            DB::table('partner_user')->insert(array(
                                                'partner_id' => 4208,
                                                'user_id' => $userByMobile->id
                                            ));
                                            DB::table('partner_user')->insert(array(
                                                'partner_id' => 1,
                                                'user_id' => $last_id
                                            ));
                                            $errBool = False;
                                        } else {
                                            $tomobile = str_replace('ABB-', '', $row[0]);
                                            $mobile = "4208" . $tomobile;
                                            $user['mobile'] = $mobile;
                                            $user['normalized_mobile'] = '+973' . $mobile;
                                            $user['telephone_code'] = '+973';
                                            $last_id = DB::table('users')->insertGetId($user);
                                            DB::table('role_user')->insert(array(
                                                'role_id' => 4,
                                                'user_id' => $last_id
                                            ));
                                            DB::table('partner_user')->insert(array(
                                                'partner_id' => 4208,
                                                'user_id' => $last_id
                                            ));
                                            DB::table('partner_user')->insert(array(
                                                'partner_id' => 1,
                                                'user_id' => $last_id
                                            ));
                                            DB::table('segment_user')->insert(array(
                                                'segment_id' => Partner::find(4208)->segments->first()->id,
                                                'user_id' => $last_id
                                            ));
                                            //Default Tier affiliation to Member
                                            $defaultTier = Tier::where('partner_id', 4208)->where('name', 'like', 'Default Tier%')->first();
                                            if (!empty($defaultTier)) {
                                                $defaultTierCriteria = Tier::where('tier_id', $defaultTier->id)->where('name', 'like', 'Default Tier Criteria%')->first();
                                                if (!empty($defaultTierCriteria)) {
                                                    DB::table('tiercriteria_user')->insert(array(
                                                        'tiercriteria_id' => $defaultTierCriteria->id,
                                                        'user_id' => $last_id
                                                    ));
                                                }
                                            }
                                            $errBool = False;
                                            $counterrors = $counterrors + 1;
                                            $errors[] = $row;
                                        }
                                    }
                                }
                                if ($errBool == True) {
                                    $counterrors = $counterrors + 1;
                                    $errors[] = $row;
//                                    echo "<pre>1";
//                                    var_dump($errors);
//                                    exit();
                                }
                            }
                        }
                    }
                    $count++;
                }

                return (count($errors) > 0) ?
                        $this->customerImportFailed($errors, $generatedFileBaseName, 'Al Baraka Bank') : $this->customerImportSucceed('Al Baraka Bank', $generatedFileBaseName);
            } elseif (($ext == 'TXT' || $ext == 'txt') && strstr($name1, 'ABBTransactions') && strcmp($this->user->api_key, "f96d7e8fbe3dfc4da3a50e0c131f3038") == 0) {
                //Reference ID|PostingDate|Product ID|BonusPts|Billing Amount|Type|MCC|
                //  0               1           2           3       4           5   6
                //Billing Currency|Transaction currency|Transaction Amount|Transaction Sign|Line number
                //        7               8                       9               10              11
                $path = $this->transactionImportPatch();
                $file->move($path, $generatedFileName);
                $inputFile = $path . '/' . $generatedFileName;

                AuditHelper::record(
                        4, $auth_user->name . " started uploading file: " . $generatedFileName
                );

                $tx = array();
                $count = 0;
                $counterrors = 0;
                $errBool = True;
                $errors = array();
                $params = array();
                $passes = 0;
                //$counterrors = 0;
                $contents = File::get($inputFile);
                $contents = nl2br($contents, FALSE);
                $contentLines = explode("<br>", $contents);
                $errors = array();
                $countContentLines = count($contentLines) - 1;
                $test = $contentLines[$countContentLines];
                $test1 = $contentLines[$countContentLines - 1];
                $ret_eof_error = true;
                if (trim($test) === "EOF") {
                    $ret_eof_error = false;
                } elseif (trim($test1) === "EOF") {
                    $ret_eof_error = false;
                }
                if ($ret_eof_error == true) {
                    return self::respondWith(
                                    200, 'EOF NOT FOUND', $errors, 'errors'
                    );
                }
                $arrContent = array();
                foreach ($contentLines as $lineread) {
                    if (trim($lineread) !== 'EOF') {
                        $arrContent[] = explode("|", $lineread);
                    }
                }
                $partner = $this->partner;
                $network_id = $partner->firstNetwork()->id;
                $partner_id = $partner->id;
                $partners = $partner->managedObjects();
                $partners_children = array();
                foreach ($partners as $part) {
                    $partners_children[] = $part->id;
                }
                $loy_by_partner = RewardHelper::loyaltyByPartner($partners_children);

                $partner_prefix = $partner->prefix;
                $itemchosen = ProductPartnerReward::where('partner_id', $partner_id)->get();
                $arr_item_chosen = array();
                foreach ($itemchosen as $ic) {
                    $arr_item_chosen[$ic->partner_mapping_id] = $ic->product_id;
                }

                $transactions_file = Transaction::select('ref_number')->where('ref_number', 'like', $name1 . "%")->get();
                $arr_transactions_file = $transactions_file->toArray();
                $arr_currencies = array();
                $currencies = Currency::all();
                foreach ($currencies as $curr) {
                    $arr_currencies[$curr->short_code] = $curr->id;
                }
                if (count($arr_transactions_file) > 0) {
                    return self::respondWith(
                                    200, 'NOT OK File Already Exists', null, 'errors'
                    );
                }

                $store_res = $partner->stores->first();
                if ($store_res) {
                    $pos_id = $store_res->pointOfSales->first()->id;
                    $tx['store_id'] = $store_res->id;
                    $tx['pos_id'] = $pos_id;
                }

                foreach ($arrContent as $row) {
                    $lineNumber = 0;
                    $posted_at = date('Y-m-d H:i:s');

                    if ($count != 0) {
                        if (trim($row[0]) != '' && trim($row[0]) != 'EOF') {

                            $unhandled_date = explode("/", $row[1]);

                            if (!isset($row[11])) {
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-001";
                                $errors[] = $row;
                                continue;
                            } elseif (substr(trim($row[0]), 0, 4) !== "ABB-" || trim($row[0]) === "" || $row[0] === " ") {
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-002";
                                $errors[] = $row;
                                continue;
                            } elseif ($row[1] === "" || $row[1] === " " || !checkdate($unhandled_date[1], $unhandled_date[0], $unhandled_date[2])) {
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-003";
                                $errors[] = $row;
                                continue;
                            } elseif ($row[2] === "" || $row[2] === " " || !is_numeric($row[2])) {
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-004";
                                $errors[] = $row;
                                continue;
                            } elseif ($row[3] === "" || $row[3] === " " || is_numeric($row[3]) === FALSE) {
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-005";
                                $errors[] = $row;
                                continue;
                            } elseif ($row[4] === "" || $row[4] === " " || !is_numeric($row[4])) {
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-006";
                                $errors[] = $row;
                                continue;
                            } elseif ($row[9] === "" || $row[9] === " " || !is_numeric($row[9])) {
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-006.1";
                                $errors[] = $row;
                                continue;
                            } elseif ($row[7] === "" || $row[7] === " " || strlen($row[7]) !== 3) {
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-007";
                                $errors[] = $row;
                                continue;
                            } elseif ($row[8] === "" || $row[8] === " " || strlen($row[8]) !== 3) {
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-007.2";
                                $errors[] = $row;
                                continue;
                            } elseif ($row[5] === "" || $row[5] === " " || strlen($row[5]) !== 2) {
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-007.1";
                                $errors[] = $row;
                                continue;
                            } elseif ($row[6] === "" || $row[6] === " ") {
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-008";
                                $errors[] = $row;
                                continue;
                            } elseif (trim($row[10]) !== "C" && trim($row[10]) !== "D") {
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-010";
                                $errors[] = $row;
                                continue;
                            } elseif (!is_numeric(trim($row[11]))) {
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-009";
                                $errors[] = $row;
                                continue;
                            }

                            if (isset($row[11])) {
                                $lineNumber = $row[11];
                            }
                            $ref_number = $name1 . $lineNumber;
                            $ref_number = rtrim($ref_number);
//                            $transact = Transaction::where('ref_number',$ref_number)->first();

                            if (array_search($ref_number, array_column($arr_transactions_file, 'ref_number')) === FALSE) {
                                $coefficient = 1;
                                if ($row[10] == 'C') {
                                    $coefficient = intval(-1);
                                }
                                $row[0] = trim($row[0]);
                                $tx['id'] = 'NULL';
                                $tx['ref_number'] = $ref_number;

                                $tx['original_total_amount'] = $row[9] * $coefficient;
                                $tx['partner_amount'] = $row[4] * $coefficient;
                                $tx['partner_trxtype'] = $row[5];
                                $tx['mcc'] = $row[6];


                                $tx['user_id'] = 1;
                                $tx['points_redeemed'] = 0;
                                $tx['points_rewarded'] = 0;
                                $tx['partner_id'] = $partner->id;
                                $tx['network_id'] = $network_id;
                                $tx['trx_reward'] = 1;

                                $reference = Reference::where('number', $row[0])->first();
                                if (isset($reference->user_id)) {
                                    $user = $reference->user_id;
                                } else {
                                    $errBool = true;
                                    $counterrors = $counterrors + 1;
                                    $errors[] = $row;
                                    continue;
                                }

                                $tx['user_id'] = $user;

                                $userInfo = User::find($user);
                                if ($userInfo->status == 'closed') {
                                    $errBool = true;
                                    $counterrors = $counterrors + 1;
                                    $errors[] = $row;
                                    continue;
                                }
//                                $unhandledDate = $row[1];
//                                $arrunhandledDate = explode("/",$unhandledDate);
                                $date_unhandled = $unhandled_date[1] . "/" . $unhandled_date[0] . "/" . $unhandled_date[2];
                                //$date_unhandled = $row[1];
                                $date = strtotime($date_unhandled);
                                $new_date = date('Y-m-d H:i:s', $date);

                                $tx['created_at'] = $new_date;
                                $tx['updated_at'] = $new_date;
                                $tx['posted_at'] = $posted_at;

//                                $loyalty = Loyalty::where('partner_id',$tx['partner_id'])->first();
                                //$itemchosen = ProductPartnerReward::where('partner_mapping_id',$partner_prefix.$row[2])->first();
                                if (isset($arr_item_chosen[$partner_prefix . $row[2]])) {
                                    $tx['product_id'] = $arr_item_chosen[$partner_prefix . $row[2]];
                                } else {
                                    $tx['product_id'] = NULL;
                                }

                                $validator = Validator::make($tx, array(
                                            //'id'         => 'required|numeric',
                                            'partner_id' => 'required|numeric',
                                            'store_id' => 'required|numeric',
                                            'user_id' => 'required|numeric'
                                ));

                                if (!$validator->fails()) {
                                    if (is_numeric($row[3])) {
                                        $txbonus = array(
                                            'partner_id' => $tx['partner_id'],
                                            'store_id' => $tx['store_id'],
                                            'pos_id' => $tx['pos_id'],
                                            'user_id' => $tx['user_id'],
                                            'ref_number' => "Al Baraka Bank ONLINE Bonus POINTS",
                                            'total_amount' => 0,
                                            'points_rewarded' => round($row[3], 0, PHP_ROUND_HALF_UP) * $coefficient,
                                            'amt_reward' => PointsHelper::pointsToAmount(round($row[3], 0, PHP_ROUND_HALF_UP), "BHD", $network_id) * $coefficient,
                                            'trx_reward' => 1,
                                            'currency_id' => 6,
                                            'exchange_rate' => 1,
                                            'country_id' => 8,
                                            'quantity' => 0,
                                            'notes' => 'Bonus Points',
                                            'trans_class' => "Amount",
                                            'network_id' => $network_id,
                                            'created_at' => $new_date,
                                            'updated_at' => $new_date,
                                            'posted_at' => $posted_at
                                        );

                                        if ($txbonus['points_rewarded'] != 0) {
                                            DB::table('transaction')->insert($txbonus);
                                            $user = User::find($txbonus['user_id']);
                                            $user->updateBalance($row[3], $txbonus['network_id'], 'add');
                                            $partner = Partner::find($txbonus['partner_id']);
                                        }
                                    }

                                    $passes++;

                                    if (is_numeric($tx['points_rewarded'])) {

                                        $params = array(
                                            'user_id' => $tx['user_id'],
                                            'partner' => $tx['partner_id'],
                                            'pos_id' => $tx['pos_id'],
                                            'store_id' => $tx['store_id'],
                                            'amount' => floatval($tx['partner_amount']),
                                            'currency' => $row[8],
                                            //'notes'		=> 'Reward By Product',
                                            'type' => 'reward',
                                            'reference' => $tx['ref_number'],
                                            'network_id' => $partner->firstNetwork()->id,
                                            'product_id' => $tx['product_id'],
                                            'partner_currency' => $row[7],
                                            'original_transaction_amount' => floatval($tx['original_total_amount']),
                                            'use_partner_currency' => True,
                                            'mcc' => $tx['mcc'],
                                            'partner_trxtype' => $tx['partner_trxtype'],
                                            'created_at' => $new_date,
                                            'updated_at' => $new_date,
                                            'posted_at' => $posted_at
                                        );

                                        $user = User::find($tx['user_id']);
                                        try {
                                            $result_do_reward = RewardHelper::doRewardNewLoyalty($params, 'Amount', $loy_by_partner);
                                        } catch (Exception $ex) {
                                            //$resp = array($ex);
                                            $json_data = json_encode(array("Exception" => $ex));
                                            APIController::postSendEmailJson($json_data);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    $count++;
                }

                return (count($errors) > 0) ?
                        $this->transactionImportFailed($errors, $generatedFileBaseName, 'Al Baraka') : $this->transactionImportSucceed('Al Baraka', $generatedFileBaseName);
            } elseif (($ext == 'TXT' || $ext == 'txt') && strstr($name1, 'ABBChanges') && strcmp($this->user->api_key, "f96d7e8fbe3dfc4da3a50e0c131f3038") == 0) {
//                RefID|New Mobile Number|New Email|New Status
//                  0  |        1        |    2    |    3
                $path = $this->changesImportPatch();
                $file->move($path, $generatedFileName);
                $inputFile = $path . '/' . $generatedFileName;

                AuditHelper::record(
                        4, $auth_user->name . " started uploading file: " . $generatedFileName
                );

                $auth_user = Auth::User();
                $tx = array();
                $contents = File::get($inputFile);
                $contents = nl2br($contents, FALSE);
                $contentLines = explode("<br>", $contents);
                $arrContent = array();
                foreach ($contentLines as $lineread) {
                    $arrContent[] = explode("|", $lineread);
                }
                $errIDs = array();
                $count = 0;
                $errCount = 0;
                foreach ($arrContent as $row) {
                    if ($count != 0) {
                        if (trim($row[0]) != '' && $row[0] != "EOF") {
                            $row[0] = trim($row[0]);
                            $reference = Reference::where('number', $row[0])->first();
                            if (isset($reference)) {
                                $user = $reference->user_id;
                                $mobile = $row[1];
                                $userChange = array();
                                if ($mobile != 'Not Changed' && is_numeric($mobile)) {
                                    $arr_countries = array("441624", "441534", "441481", "35818", "1876", "1869", "1868", "1809", "1787", "1784", "1767", "1758", "1684", "1671", "1670", "1664", "1649", "1473", "1441", "1345", "1340", "1284", "1268", "1264", "1246", "1242", "998", "996", "995", "994", "993", "992", "977", "976", "975", "974", "973", "972", "971", "970", "968", "967", "966", "965", "964", "963", "962", "961", "960", "886", "880", "870", "856", "855", "853", "852", "850", "692", "691", "690", "689", "688", "687", "686", "685", "683", "682", "681", "680", "679", "678", "677", "676", "675", "674", "673", "672", "670", "599", "599", "599", "598", "597", "596", "595", "594", "593", "592", "591", "590", "590", "590", "509", "508", "507", "506", "505", "504", "503", "502", "501", "500", "423", "421", "420", "389", "387", "386", "385", "382", "381", "380", "379", "378", "377", "376", "375", "374", "373", "372", "371", "370", "359", "358", "357", "356", "355", "354", "353", "352", "351", "350", "299", "298", "297", "291", "290", "269", "268", "267", "266", "265", "264", "263", "262", "262", "261", "260", "258", "257", "256", "255", "254", "253", "252", "251", "250", "249", "248", "246", "245", "244", "243", "242", "241", "240", "239", "238", "237", "236", "235", "234", "233", "232", "231", "230", "229", "228", "227", "226", "225", "224", "223", "222", "221", "220", "218", "216", "213", "212", "212", "211", "98", "95", "94", "93", "92", "91", "90", "86", "84", "82", "81", "66", "65", "64", "63", "62", "61", "61", "61", "60", "58", "57", "56", "55", "54", "53", "52", "51", "49", "48", "47", "47", "46", "45", "44", "43", "41", "40", "39", "36", "34", "33", "32", "31", "30", "27", "20", "7", "7", "1", "1", "1");
                                    foreach ($arr_countries as $ctry) {
                                        if (substr($mobile, 0, strlen($ctry)) === $ctry) {
                                            $mobile = substr($mobile, strlen($ctry));
                                            $ismobileUnique = User::where('mobile', $mobile)->where('id', '!=', $user)->count();
                                            if ($ismobileUnique > 0) {
                                                $errCount = $errCount + 1;
                                                $errIDs[] = $row;
                                                continue;
                                            }
                                            $userChange['mobile'] = $mobile;
                                            $userChange['normalized_mobile'] = "+" . $ctry . $mobile;
                                            $userChange['telephone_code'] = '+' . $ctry;
                                            break;
                                        }
                                    }
                                }

                                if (!isset($userChange['mobile']) && $mobile != 'Not Changed') {
                                    $errCount = $errCount + 1;
                                    $errIDs[] = $row;
                                }

                                if ($row[2] != 'Not Changed') {
                                    $userChange['email'] = $row[2];
                                }

                                $userOb = User::find($user);

                                if ($row[3] != '' && $row[3] != 'Not Changed') {
                                    if ($row[3] == 'C') {
                                        $userChange['status'] = "closed";
                                    } elseif ($row[3] == 'S') {
                                        $userChange['status'] = "suspended";
                                    } elseif ($row[3] == 'A') {
                                        $userChange['status'] = "active";
                                    }
                                } else {
                                    $userChange['status'] = $userOb->status;
                                }

                                if ($userChange['status'] != $userOb->status && $row[3] != 'Not Changed') {
                                    $closeUserAllowed = true;
                                    foreach ($userOb->partners as $part) {
                                        // can not set status as closed for a user that has another partners
                                        if ($part->id != 4208 && $part->id != 1) {
                                            $closeUserAllowed = false;
                                        }
                                    }
                                    if ($userOb->status == "closed") {
                                        $closeUserAllowed = false;
                                    }
//                                    $errIDs[] ="Partner is not allowed to change the status of this user as it is active for other partners";
//                                $errIDs[] = $row;

                                    if (!$closeUserAllowed) {
                                        $errIDs[] = $row;
                                    } else {
                                        $updateChange = User::where('id', $user)->update($userChange);
                                    }
                                } else {
                                    $updateChange = User::where('id', $user)->update($userChange);
                                }
                            } else {
                                $errCount = $errCount + 1;
                                $errIDs[] = $row;
                            }
                        }
                    }
                    $count++;
                }

                AuditHelper::record(
                        4, $name1 . " was uploaded by " . $auth_user->name
                );
                return self::respondWith(
                                200, "OK", $errIDs, 'errors'
                ); //with('success','ABB Customers Imported Successfully!');
            } elseif (($ext == 'TXT' || $ext == 'txt') && strstr($name1, 'ALMTransactions') && strcmp($this->user->api_key, "30f79859799d476c5529cc54d43e3573") == 0) {
                //Reference ID|PostingDate|Product ID|BonusPts|Billing Amount|Type|MCC|Country|
                //      0           1           2        3          4           5   6    7
                //Billing Currency|Transaction currency|Transaction Amount|Transaction Sign|Line number
                //        8                9                    10              11             12
                $tx = array();
                $count = 0;
                $counterrors = 0;
                $errBool = True;
                $errors = array();
                $params = array();
                $passes = 0;
                //$counterrors = 0;

                AuditHelper::record(
                        4, $auth_user->name . " started uploading file: " . $generatedFileName
                );

                $path = $this->transactionImportPatch();
                $file->move($path, $generatedFileName);
                $inputFile = $path . '/' . $generatedFileName;
                $contents = File::get($inputFile);
                $contents = nl2br($contents, FALSE);
                $contentLines = explode("<br>", $contents);
                $errors = array();
                $countContentLines = count($contentLines) - 1;
                $test = $contentLines[$countContentLines];
                $test1 = $contentLines[$countContentLines - 1];
                $ret_eof_error = true;
                if (trim($test) === "EOF") {
                    $ret_eof_error = false;
                } elseif (trim($test1) === "EOF") {
                    $ret_eof_error = false;
                }
                if ($ret_eof_error == true) {
                    return self::respondWith(
                                    200, 'EOF NOT FOUND', $errors, 'errors'
                    );
                }
                $arrContent = array();
                foreach ($contentLines as $lineread) {
                    if (trim($lineread) !== 'EOF') {
                        $arrContent[] = explode("|", $lineread);
                    }
                }
                $partner = $this->partner;
                $network_id = $partner->firstNetwork()->id;
                $partner_id = $partner->id;
                $partners = $partner->managedObjects();
                $partners_children = array();
                foreach ($partners as $part) {
                    $partners_children[] = $part->id;
                }
                $loy_by_partner = RewardHelper::loyaltyByPartner($partners_children);

                $partner_prefix = $partner->prefix;
                $itemchosen = ProductPartnerReward::where('partner_id', $partner_id)->get();
                $arr_item_chosen = array();
                foreach ($itemchosen as $ic) {
                    $arr_item_chosen[$ic->partner_mapping_id] = $ic->product_id;
                }

                $transactions_file = Transaction::select('ref_number')->where('ref_number', 'like', $name1 . "%")->get();
                $arr_transactions_file = $transactions_file->toArray();
                $arr_currencies = array();
                $currencies = Currency::all();
                foreach ($currencies as $curr) {
                    $arr_currencies[$curr->short_code] = $curr->id;
                }
                if (count($arr_transactions_file) > 0) {
                    return self::respondWith(
                                    200, 'NOT OK File Already Exists', null, 'errors'
                    );
                }

                $store_res = $partner->stores->first();
                if ($store_res) {
                    $pos_id = $store_res->pointOfSales->first()->id;
                    $tx['store_id'] = $store_res->id;
                    $tx['pos_id'] = $pos_id;
                }

                foreach ($arrContent as $row) {
                    $lineNumber = 0;
                    $posted_at = date('Y-m-d H:i:s');

                    if ($count != 0) {
                        if (trim($row[0]) != '' && trim($row[0]) != 'EOF') {

                            $unhandled_date = explode("/", $row[1]);

                            if (!isset($row[12])) {
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-001";
                                $errors[] = $row;
                                continue;
                            } elseif (substr(trim($row[0]), 0, 4) !== "ALM-" || trim($row[0]) === "" || $row[0] === " ") {
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-002";
                                $errors[] = $row;
                                continue;
                            } elseif ($row[1] === "" || $row[1] === " " || !checkdate($unhandled_date[1], $unhandled_date[0], $unhandled_date[2])) {
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-003";
                                $errors[] = $row;
                                continue;
                            } elseif ($row[2] === "" || $row[2] === " " || !is_numeric($row[2])) {
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-004";
                                $errors[] = $row;
                                continue;
                            } elseif ($row[4] === "" || $row[4] === " " || !is_numeric($row[4])) {
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-006";
                                $errors[] = $row;
                                continue;
                            } elseif ($row[10] === "" || $row[10] === " " || !is_numeric($row[10])) {
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-006.1";
                                $errors[] = $row;
                                continue;
                            } elseif ($row[8] === "" || $row[8] === " " || strlen($row[8]) !== 3) {
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-007";
                                $errors[] = $row;
                                continue;
                            } elseif ($row[9] === "" || $row[9] === " " || strlen($row[9]) !== 3) {
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-007.2";
                                $errors[] = $row;
                                continue;
                            } elseif (trim($row[11]) !== "C" && trim($row[11]) !== "D") {
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-010";
                                $errors[] = $row;
                                continue;
                            } elseif (!is_numeric(trim($row[12]))) {
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-009";
                                $errors[] = $row;
                                continue;
                            }

                            if (isset($row[12])) {
                                $lineNumber = $row[12];
                            }
                            $ref_number = $name1 . $lineNumber;
                            $ref_number = rtrim($ref_number);

                            if (array_search($ref_number, array_column($arr_transactions_file, 'ref_number')) === FALSE) {
                                $coefficient = 1;

                                if ($row[11] == 'C') {
                                    $coefficient = intval(-1);
                                }

                                $row[0] = trim($row[0]);
                                $tx['id'] = 'NULL';
                                $tx['ref_number'] = $ref_number;
                                $tx['original_total_amount'] = $row[10] * $coefficient;
                                $tx['partner_amount'] = $row[4] * $coefficient;
                                $tx['partner_trxtype'] = (isset($row[5]) && !empty($row[5])) ? $row[5] : '';
                                $tx['mcc'] = (isset($row[6]) && !empty($row[6])) ? $row[6] : '';
                                $tx['user_id'] = 1;
                                $tx['points_redeemed'] = 0;
                                $tx['points_rewarded'] = 0;
                                $tx['partner_id'] = $partner->id;
                                $tx['network_id'] = $network_id;
                                $tx['trx_reward'] = 1;

                                $reference = Reference::where('number', $row[0])->first();
                                if (isset($reference->user_id)) {
                                    $user = $reference->user_id;
                                } else {
                                    $errBool = true;
                                    $counterrors = $counterrors + 1;
                                    $errors[] = $row;
                                    continue;
                                }

                                $tx['user_id'] = $user;

                                $userInfo = User::find($user);
                                if ($userInfo->status == 'closed') {
                                    $errBool = true;
                                    $counterrors = $counterrors + 1;
                                    $errors[] = $row;
                                    continue;
                                }

                                $date_unhandled = $unhandled_date[1] . "/" . $unhandled_date[0] . "/" . $unhandled_date[2];

                                $date = strtotime($date_unhandled);
                                $new_date = date('Y-m-d H:i:s', $date);

                                $tx['created_at'] = $new_date;
                                $tx['updated_at'] = $new_date;
                                $tx['posted_at'] = $posted_at;

                                if (isset($arr_item_chosen[$partner_prefix . $row[2]])) {
                                    $tx['product_id'] = $arr_item_chosen[$partner_prefix . $row[2]];
                                } else {
                                    $tx['product_id'] = NULL;
                                }

                                $validator = Validator::make($tx, array(
                                            'partner_id' => 'required|numeric',
                                            'store_id' => 'required|numeric',
                                            'user_id' => 'required|numeric'
                                ));

                                if (!$validator->fails()) {
                                    if (isset($row[3]) && !empty($row[3]) && is_numeric($row[3])) {
                                        $txbonus = array(
                                            'partner_id' => $tx['partner_id'],
                                            'store_id' => $tx['store_id'],
                                            'pos_id' => $tx['pos_id'],
                                            'user_id' => $tx['user_id'],
                                            'ref_number' => "Alinma Bank ONLINE Bonus POINTS",
                                            'total_amount' => 0,
                                            'points_rewarded' => round($row[3], 0, PHP_ROUND_HALF_UP) * $coefficient,
                                            'amt_reward' => PointsHelper::pointsToAmount(round($row[3], 0, PHP_ROUND_HALF_UP), "BHD", $network_id) * $coefficient,
                                            'trx_reward' => 1,
                                            'currency_id' => 6,
                                            'exchange_rate' => 1,
                                            'country_id' => (isset($row[7]) && !empty($row[7]) && is_numeric($row[7])) ? $row[7] : '',
                                            'quantity' => 0,
                                            'notes' => 'Bonus Points',
                                            'trans_class' => "Amount",
                                            'network_id' => $network_id,
                                            'created_at' => $new_date,
                                            'updated_at' => $new_date,
                                            'posted_at' => $posted_at
                                        );

                                        if ($txbonus['points_rewarded'] != 0) {
                                            DB::table('transaction')->insert($txbonus);
                                            $user = User::find($txbonus['user_id']);
                                            $user->updateBalance($row[3], $txbonus['network_id'], 'add');
                                            $partner = Partner::find($txbonus['partner_id']);
                                        }
                                    }

                                    $passes++;
                                    if (is_numeric($tx['points_rewarded'])) {
                                        $params = array(
                                            'user_id' => $tx['user_id'],
                                            'partner' => $tx['partner_id'],
                                            'pos_id' => $tx['pos_id'],
                                            'store_id' => $tx['store_id'],
                                            'amount' => floatval($tx['partner_amount']),
                                            'currency' => $row[9],
                                            'type' => 'reward',
                                            'reference' => $tx['ref_number'],
                                            'network_id' => $partner->firstNetwork()->id,
                                            'product_id' => $tx['product_id'],
                                            'partner_currency' => $row[8],
                                            'original_transaction_amount' => floatval($tx['original_total_amount']),
                                            'use_partner_currency' => true,
                                            'mcc' => $tx['mcc'],
                                            'partner_trxtype' => $tx['partner_trxtype'],
                                            'created_at' => $new_date,
                                            'updated_at' => $new_date,
                                            'posted_at' => $posted_at
                                        );

                                        $user = User::find($tx['user_id']);

                                        try {
                                            $result_do_reward = RewardHelper::doRewardNewLoyalty($params, 'Amount', $loy_by_partner);
                                        } catch (Exception $ex) {
                                            $json_data = json_encode(array("Exception" => $ex));
                                            APIController::postSendEmailJson($json_data);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    $count++;
                }

                return (count($errors) > 0) ?
                        $this->transactionImportFailed($errors, $generatedFileBaseName, 'Alinma') : $this->transactionImportSucceed('Alinma', $generatedFileBaseName);
            } elseif (($ext == 'TXT' || $ext == 'txt') && strstr($name1, 'ALMChanges') && strcmp($this->user->api_key, "30f79859799d476c5529cc54d43e3573") == 0) {
//                RefID|New Mobile Number|New Email|New Status
//                  0  |        1        |    2    |    3
                $auth_user = Auth::User();

                AuditHelper::record(
                        4, $auth_user->name . " started uploading file: " . $generatedFileName
                );

                $path = $this->changesImportPatch();
                $file->move($path, $generatedFileName);
                $inputFile = $path . '/' . $generatedFileName;

                $tx = array();
                $contents = File::get($inputFile);
                $contents = nl2br($contents, FALSE);
                $contentLines = explode("<br>", $contents);
                $arrContent = array();
                foreach ($contentLines as $lineread) {
                    $arrContent[] = explode("|", $lineread);
                }

                $errors = [];
                $count = 0;
                $errCount = 0;
                foreach ($arrContent as $row) {
                    if ($count != 0) {
                        if ($row === end($arrContent)) {
                            if (trim($row[0]) != "EOF") {
                                $errCount = $errCount + 1;
                                $row[0] = trim($row[0]);
                                $row[] = 104;
                                $errors[] = $row;
                                continue;
                            }
                        }
                        if (trim($row[0]) != '' && trim($row[0]) != "EOF") {
                            $row[0] = trim($row[0]);
                            $reference = Reference::where('number', $row[0])->first();
                            if (isset($reference)) {
                                $user = $reference->user_id;
                                $mobile = $row[1];
                                $userChange = [];
                                if ($mobile != 'Not Changed' && is_numeric($mobile)) {
                                    $arr_countries = ["441624", "441534", "441481", "35818", "1876", "1869", "1868", "1809", "1787", "1784", "1767", "1758", "1684", "1671", "1670", "1664", "1649", "1473", "1441", "1345", "1340", "1284", "1268", "1264", "1246", "1242", "998", "996", "995", "994", "993", "992", "977", "976", "975", "974", "973", "972", "971", "970", "968", "967", "966", "965", "964", "963", "962", "961", "960", "886", "880", "870", "856", "855", "853", "852", "850", "692", "691", "690", "689", "688", "687", "686", "685", "683", "682", "681", "680", "679", "678", "677", "676", "675", "674", "673", "672", "670", "599", "599", "599", "598", "597", "596", "595", "594", "593", "592", "591", "590", "590", "590", "509", "508", "507", "506", "505", "504", "503", "502", "501", "500", "423", "421", "420", "389", "387", "386", "385", "382", "381", "380", "379", "378", "377", "376", "375", "374", "373", "372", "371", "370", "359", "358", "357", "356", "355", "354", "353", "352", "351", "350", "299", "298", "297", "291", "290", "269", "268", "267", "266", "265", "264", "263", "262", "262", "261", "260", "258", "257", "256", "255", "254", "253", "252", "251", "250", "249", "248", "246", "245", "244", "243", "242", "241", "240", "239", "238", "237", "236", "235", "234", "233", "232", "231", "230", "229", "228", "227", "226", "225", "224", "223", "222", "221", "220", "218", "216", "213", "212", "212", "211", "98", "95", "94", "93", "92", "91", "90", "86", "84", "82", "81", "66", "65", "64", "63", "62", "61", "61", "61", "60", "58", "57", "56", "55", "54", "53", "52", "51", "49", "48", "47", "47", "46", "45", "44", "43", "41", "40", "39", "36", "34", "33", "32", "31", "30", "27", "20", "7", "7", "1", "1", "1"];
                                    foreach ($arr_countries as $ctry) {
                                        if (substr($mobile, 0, strlen($ctry)) === $ctry) {
                                            $mobile = substr($mobile, strlen($ctry));
                                            $ismobileUnique = User::where('mobile', $mobile)->where('id', '!=', $user)->count();
                                            if ($ismobileUnique > 0) {
                                                $errCount = $errCount + 1;
                                                $errors[] = $row;
                                                continue;
                                            }
                                            $userChange['mobile'] = $mobile;
                                            $userChange['normalized_mobile'] = "+" . $ctry . $mobile;
                                            $userChange['telephone_code'] = '+' . $ctry;
                                            break;
                                        }
                                    }
                                }

                                if (!isset($userChange['mobile']) && $mobile != 'Not Changed') {
                                    $errCount = $errCount + 1;
                                    $errors[] = $row;
                                }

                                if ($row[2] != 'Not Changed') {
                                    $userChange['email'] = $row[2];
                                } else {
                                    $errCount = $errCount + 1;
                                    $row[] = 102;
                                    $errors[] = $row;
                                }

                                $userOb = User::find($user);

                                if ($row[3] != '' && $row[3] != 'Not Changed') {
                                    if ($row[3] == 'C') {
                                        $userChange['status'] = "closed";
                                    } elseif ($row[3] == 'S') {
                                        $userChange['status'] = "suspended";
                                    } elseif ($row[3] == 'A') {
                                        $userChange['status'] = "active";
                                    } else {
                                        $errCount = $errCount + 1;
                                        $row[] = 103;
                                        $errors[] = $row;
                                        continue;
                                    }
                                } else {
                                    $userChange['status'] = $userOb->status;
                                }

                                if ($userChange['status'] != $userOb->status && $row[3] != 'Not Changed') {
                                    $closeUserAllowed = true;
                                    foreach ($userOb->partners as $part) {
                                        // can not set status as closed for a user that has another partners
                                        if ($part->id != 4385 && $part->id != 1) {
                                            $closeUserAllowed = false;
                                        }
                                    }
                                    if ($userOb->status == "closed") {
                                        $closeUserAllowed = false;
                                    }

                                    if (!$closeUserAllowed) {
                                        $errCount = $errCount + 1;
                                        $row[] = 103;
                                        $errors[] = $row;
                                    } else {
                                        $updateChange = User::where('id', $user)->update($userChange);
                                    }
                                } else {
                                    $updateChange = User::where('id', $user)->update($userChange);
                                }
                            } else {
                                $errCount = $errCount + 1;
                                $row[] = 101;
                                $errors[] = $row;
                            }
                        }
                    }
                    $count++;
                }


                return (count($errors) > 0) ?
                        $this->changesImportFailed($errors, $generatedFileBaseName, 'BSL') : $this->changesImportSucceed('BSL', $generatedFileBaseName);
            } elseif (($ext == 'TXT' || $ext == 'txt') && strstr($name1, 'ABDCustomer') && strcmp($this->user->api_key, "07a77634a8af78bd28fd4068c565571b") == 0) {
                //Ref_id|Title|First Name|Last Name|Email|Mobile|DOB|Gender|Marital Status|Occupation|Status|Amount|source_id
                //0       1         2         3      4      5     6    7           8           9        10     11       12

                AuditHelper::record(
                        4, $auth_user->name . " started uploading file: " . $generatedFileName
                );

                $path = $this->customerImportPatch();
                $file->move($path, $generatedFileName);
                $inputFile = $path . '/' . $generatedFileName;

                $tx = array();
                $contents = File::get($inputFile);
                $contents = nl2br($contents, FALSE);
                $contentLines = explode("<br>", $contents);

                $arrContent = array();
                foreach ($contentLines as $lineread) {
                    $arrContent[] = explode("|", $lineread);
                }
                $count = 0;
                $counterrors = 0;
                $errBool = True;
                $errors = array();
                foreach ($arrContent as $row) {
                    if ($count != 0) {
                        if (trim($row[0]) != '' && trim($row[0]) !== 'EOF') {
                            $row[0] = trim($row[0]);
                            $errBool = True;

                            $user = array();
                            $user['id'] = 'NULL';
                            $user['first_name'] = $row[2];
                            $user['last_name'] = $row[3];
                            if (empty($row[3])) {
                                $user['last_name'] = "Empty";
                            }
                            $mobile = $row[5];
                            $mobile = ltrim($mobile, '0');
                            $tomobile = str_replace('ABD-', '', $row[0]);
                            //echo $mobile . "---<br>";
                            if (!is_numeric($mobile) || strlen($mobile) <= 8) {
                                $mobile = "4235" . $tomobile;
                                $user['mobile'] = $mobile;
                                $user['normalized_mobile'] = '+966' . $mobile;
                                $user['telephone_code'] = '+966';
                            } elseif (InputHelper::isPhone($mobile)) {
                                $countc = InputHelper::isPhone($mobile);
                                $pos = strpos($mobile, $countc);
                                if ($pos !== false) {
                                    $mobile = substr_replace($mobile, "", $pos, strlen($countc));
                                } else {
                                    $mobile = str_replace($countc, "", $mobile);
                                }
                                $user['mobile'] = $mobile;
                                $user['normalized_mobile'] = '+' . $countc . $mobile;
                                $user['telephone_code'] = '+' . $countc;
                            } else {
                                $user['mobile'] = "";
                                $user['normalized_mobile'] = '';
                                $user['telephone_code'] = '';
                            }

                            //echo "****" . InputHelper::isPhone($mobile) . "+++<br>" . $mobile . "====<br>";
                            $email1 = trim($row[4]);
                            $email3 = '';
                            if ($email1 == '' || !InputHelper::isEmail($email1)) {
                                $email3 = $row[0] . '@ABDCust.com';
                            } else {
                                $email3 = $email1;
                            }

                            $user['email'] = $email3;

                            $unhandledDate = $row[6];
                            if ($unhandledDate == '' || $unhandledDate == ' ') {
                                $unhandledDate = "01/01/1902";
                            }

                            $arrunhandledDate = explode("/", $unhandledDate);
                            $date_unhandled = $arrunhandledDate[1] . "/" . $arrunhandledDate[0] . "/" . $arrunhandledDate[2];

                            $date = strtotime($date_unhandled);
                            $new_date = date('Y-m-d H:i:s', $date);
                            $user['dob'] = $new_date;

                            $user['balance'] = $row[11];
                            $user['marital_status'] = $row[8];
                            $user['confirm_passcode'] = false;
                            $user['passcode'] = PinCode::newCode();
                            $user['title'] = $row[1];
                            $user['draft'] = false;
                            $user['verified'] = 1;
                            $user['status'] = 'inactive';
                            if ($row[10] == 'C') {
                                $user['status'] = "closed";
                            } elseif ($row[10] == 'S') {
                                $user['status'] = "suspended";
                            }


                            $user['created_at'] = date("Y-m-d H:i:s");
                            $user['updated_at'] = date("Y-m-d H:i:s");
                            $user['ucid'] = UCID::newCustomerID();
                            $user['country_id'] = 6;
                            //$user['password']       = Hash::make($row[22]);
                            // Validate gender
                            switch ($row[7]) {
                                case "M":
                                    $user['gender'] = 'm';
                                    break;
                                case "F":
                                    $user['gender'] = 'f';
                                    break;
                                default:
                                    $user['gender'] = 'Not Selected';
                            }

                            // Validate balance
                            if ($user['balance'] == null) {
                                $user['balance'] = 0;
                            }

                            // Validate dob
                            if ($user['dob'] == null) {
                                $user['dob'] = date("Y-m-d H:i:s", strtotime("1 January 1902"));
                            }

                            //set the registration source POS
                            if (!empty($row[12])) {
                                $partner = Partner::find(4235);
                                if (strpos($partner->prefix, '-')) {
                                    $prefix = $partner->prefix;
                                } else {
                                    $prefix = $partner->prefix . '-';
                                }

                                $partnerStores = $partner->stores;
                                $storesArray = array();
                                foreach ($partnerStores as $partnerStore) {
                                    $storesArray[] = $partnerStore->id;
                                }
                                $pos = Pos::where('mapping_id', $prefix . $row[12])->whereIn('store_id', $storesArray)->first();
                                $user['registration_source_pos'] = $pos->id;
                            } else {
                                $storeDetails = Store::where('partner_id', 4235)->where('name', 'Registration through Customer File')->first();
                                $posDetails = Pos::where('store_id', $storeDetails->id)->where('name', 'Registration through Customer File POS')->first();
                                $user['registration_source_pos'] = $posDetails->id;
                            }

                            $validator = Validator::make($user, array(
                                        'first_name' => 'required',
                                        'last_name' => 'required',
                                        'email' => 'required|email|unique:users',
                                        'mobile' => 'required|numeric',
                                        'dob' => 'required',
                                        'balance' => 'required|numeric'
                            ));

                            $skipstatus = array();
                            $skipstatus[] = 'inactive';
                            $skipstatus[] = 'closed';
                            $countmobileunique = DB::table('users')->where('normalized_mobile', $user['normalized_mobile'])->whereNotIn('status', $skipstatus)->count();

                            $count = DB::table('users')->where('email', $user['email'])->where('draft', false)->count();
                            $countmobile = DB::table('users')->where('normalized_mobile', $user['normalized_mobile'])->count();


                            $reference = Reference::where('number', $row[0])->first();
                            if (!$validator->fails() && $countmobileunique <= 0) {

                                if (!isset($reference)) {
                                    // Create the user
                                    $last_id = DB::table('users')->insertGetId($user);
                                    //$last_id = DB::table('users')->getPdo()->lastInsertId();
                                    // Assign the role
                                    DB::table('role_user')->insert(array(
                                        'role_id' => 4,
                                        'user_id' => $last_id
                                    ));
                                    DB::table('reference')->insert(array(
                                        'number' => $row[0],
                                        'partner_id' => 4235,
                                        'user_id' => $last_id,
                                        'created_at' => date('Y-m-d H:i:s', time()),
                                        'updated_at' => date('Y-m-d H:i:s', time())
                                    ));
                                    DB::table('partner_user')->insert(array(
                                        'partner_id' => 4235,
                                        'user_id' => $last_id
                                    ));
                                    DB::table('partner_user')->insert(array(
                                        'partner_id' => 1,
                                        'user_id' => $last_id
                                    ));
                                    DB::table('segment_user')->insert(array(
                                        'segment_id' => Partner::find(4235)->segments->first()->id,
                                        'user_id' => $last_id
                                    ));

                                    //Default Tier affiliation to Member
                                    $defaultTier = Tier::where('partner_id', 4235)->where('name', 'like', 'Default Tier%')->first();
                                    if (!empty($defaultTier)) {
                                        $defaultTierCriteria = Tier::where('tier_id', $defaultTier->id)->where('name', 'like', 'Default Tier Criteria%')->first();
                                        if (!empty($defaultTierCriteria)) {
                                            DB::table('tiercriteria_user')->insert(array(
                                                'tiercriteria_id' => $defaultTierCriteria->id,
                                                'user_id' => $last_id
                                            ));
                                        }
                                    }

                                    //Reward User
                                    $user = User::find($last_id);
                                    $partner = Partner::find(4235);
                                    if (!empty($pos)) {
                                        $posId = $pos->id;
                                        $storeId = $pos->store_id;
                                    } else {
                                        $storeDetails = Store::where('partner_id', 4235)->where('name', 'Registration through Customer File')->first();
                                        $posDetails = Pos::where('store_id', $storeDetails->id)->where('name', 'Registration through Customer File POS')->first();
                                        $storeId = $storeDetails->store_id;
                                        $posId = $posDetails->id;
                                    }
                                    $params = array(
                                        'user_id' => $user->id,
                                        'partner' => $partner->id,
                                        'pos_id' => $posId,
                                        'store_id' => $storeId,
                                        'amount' => floatval($row[11]),
                                        'currency' => 'SAR',
                                        'partner_currency' => 'SAR',
                                        'type' => 'reward',
                                        'reference' => 'ABD Customer File',
                                        'network_id' => $partner->firstNetwork()->id,
                                        'original_transaction_amount' => floatval($row[11]),
                                        'use_partner_currency' => True,
                                    );
                                    $result_do_reward = Transaction::doRewardNewLoyalty($params, 'Amount');

                                    $custRef = $row[0];
                                    $template = View::make('sms.new-user-abd', array('ref' => $custRef, 'user' => $user))->render();

                                    //SMSHelper::send($template, $user, $partner);
                                    $errBool = False;
                                    //DB::table('transaction')->insert($tx);
                                } elseif (isset($reference)) {

                                    $user1 = $reference->user_id;

                                    unset($user['id']);
                                    unset($user['passcode']);
                                    DB::table('partner_user')->insert(array(
                                        'partner_id' => 4235,
                                        'user_id' => $user1
                                    ));

                                    User::where('id', $user1)->update($user);

                                    //Reward User
                                    $user = User::find($user1);
                                    $partner = Partner::find(4235);
                                    if (!empty($pos)) {
                                        $posId = $pos->id;
                                        $storeId = $pos->store_id;
                                    } else {
                                        $storeDetails = Store::where('partner_id', 4235)->where('name', 'Registration through Customer File')->first();
                                        $posDetails = Pos::where('store_id', $storeDetails->id)->where('name', 'Registration through Customer File POS')->first();
                                        $storeId = $storeDetails->store_id;
                                        $posId = $posDetails->id;
                                    }
                                    $params = array(
                                        'user_id' => $user->id,
                                        'partner' => $partner->id,
                                        'pos_id' => $posId,
                                        'store_id' => $storeId,
                                        'amount' => floatval($row[11]),
                                        'currency' => 'SAR',
                                        'partner_currency' => 'SAR',
                                        'type' => 'reward',
                                        'reference' => 'ABD Customer File',
                                        'network_id' => $partner->firstNetwork()->id,
                                        'original_transaction_amount' => floatval($row[11]),
                                        'use_partner_currency' => True,
                                    );

                                    $result_do_reward = Transaction::doRewardNewLoyalty($params, 'Amount');

                                    $errBool = False;
                                }
                            } else {
                                //$reference = Reference::where('number', $row[0])->first();
                                if (isset($reference) && $countmobileunique <= 0) {

                                    $user1 = $reference->user_id;

                                    unset($user['id']);
                                    unset($user['passcode']);
                                    DB::table('partner_user')->insert(array(
                                        'partner_id' => 4235,
                                        'user_id' => $user1
                                    ));
                                    User::where('id', $user1)->update($user);
                                    //Reward User
                                    $user = User::find($user1);
                                    $partner = Partner::find(4235);
                                    if (!empty($pos)) {
                                        $posId = $pos->id;
                                        $storeId = $pos->store_id;
                                    } else {
                                        $storeDetails = Store::where('partner_id', 4235)->where('name', 'Registration through Customer File')->first();
                                        $posDetails = Pos::where('store_id', $storeDetails->id)->where('name', 'Registration through Customer File POS')->first();
                                        $storeId = $storeDetails->store_id;
                                        $posId = $posDetails->id;
                                    }
                                    $params = array(
                                        'user_id' => $user->id,
                                        'partner' => $partner->id,
                                        'pos_id' => $posId,
                                        'store_id' => $storeId,
                                        'amount' => floatval($row[11]),
                                        'currency' => 'SAR',
                                        'partner_currency' => 'SAR',
                                        'type' => 'reward',
                                        'reference' => 'ABD Customer File',
                                        'network_id' => $partner->firstNetwork()->id,
                                        'original_transaction_amount' => floatval($row[11]),
                                        'use_partner_currency' => True,
                                    );

                                    $result_do_reward = Transaction::doRewardNewLoyalty($params, 'Amount');
                                    $errBool = False;
                                } else {
                                    if ($countmobileunique <= 0) {
                                        $count = DB::table('users')->where('email', $user['email'])->where('draft', false)->count();
                                        $countmobile = DB::table('users')->where('normalized_mobile', $user['normalized_mobile'])->where('draft', false)->count();

                                        if ($count == 0 && empty($user['email'])) {
                                            $email3 = $row[0] . '@ABDCust.com';
                                            $user['email'] = $email3;
                                            $validator = Validator::make($user, array(
                                                        'first_name' => 'required',
                                                        'last_name' => 'required',
                                                        'email' => 'required|email|unique:users',
                                                        'mobile' => 'required|numeric',
                                                        'dob' => 'required',
                                                        'balance' => 'required|numeric'
                                            ));
                                            if (!$validator->fails()) {
                                                $last_id = DB::table('users')->insertGetId($user);
                                                //$last_id = DB::table('users')->getPdo()->lastInsertId();
                                                // Assign the role
                                                DB::table('role_user')->insert(array(
                                                    'role_id' => 4,
                                                    'user_id' => $last_id
                                                ));
                                                DB::table('reference')->insert(array(
                                                    'number' => $row[0],
                                                    'partner_id' => 4235,
                                                    'user_id' => $last_id,
                                                    'created_at' => date('Y-m-d H:i:s', time()),
                                                    'updated_at' => date('Y-m-d H:i:s', time())
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => 4235,
                                                    'user_id' => $last_id
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => 1,
                                                    'user_id' => $last_id
                                                ));
                                                DB::table('segment_user')->insert(array(
                                                    'segment_id' => Partner::find(4235)->segments->first()->id,
                                                    'user_id' => $last_id
                                                ));

                                                //Default Tier affiliation to Member
                                                $defaultTier = Tier::where('partner_id', 4235)->where('name', 'like', 'Default Tier%')->first();
                                                if (!empty($defaultTier)) {
                                                    $defaultTierCriteria = Tier::where('tier_id', $defaultTier->id)->where('name', 'like', 'Default Tier Criteria%')->first();
                                                    if (!empty($defaultTierCriteria)) {
                                                        DB::table('tiercriteria_user')->insert(array(
                                                            'tiercriteria_id' => $defaultTierCriteria->id,
                                                            'user_id' => $last_id
                                                        ));
                                                    }
                                                }

                                                $userSMS = User::find($last_id);

                                                //$userSMS->sendWelcomeMessageNew(4235);
                                                //$userSMS->sendAccountPin();
                                                $errBool = False;
                                            } elseif ($validator->fails()) {
                                                if ($countmobile == 0 || empty($user['normalized_mobile'])) {
                                                    $mobile = "4235" . $tomobile;
                                                    $user['mobile'] = $mobile;
                                                    $user['normalized_mobile'] = '+966' . $mobile;
                                                    $user['telephone_code'] = '+966';
                                                    $validator = Validator::make($user, array(
                                                                'first_name' => 'required',
                                                                'last_name' => 'required',
                                                                'email' => 'required|email|unique:users',
                                                                'mobile' => 'required|numeric',
                                                                'dob' => 'required',
                                                                'balance' => 'required|numeric'
                                                    ));
                                                    if (!$validator->fails()) {
                                                        $last_id = DB::table('users')->insertGetId($user);
                                                        //$last_id = DB::table('users')->getPdo()->lastInsertId();
                                                        // Assign the role
                                                        DB::table('role_user')->insert(array(
                                                            'role_id' => 4,
                                                            'user_id' => $last_id
                                                        ));
                                                        DB::table('reference')->insert(array(
                                                            'number' => $row[0],
                                                            'partner_id' => 4235,
                                                            'user_id' => $last_id,
                                                            'created_at' => date('Y-m-d H:i:s', time()),
                                                            'updated_at' => date('Y-m-d H:i:s', time())
                                                        ));
                                                        DB::table('partner_user')->insert(array(
                                                            'partner_id' => 4235,
                                                            'user_id' => $last_id
                                                        ));
                                                        DB::table('partner_user')->insert(array(
                                                            'partner_id' => 1,
                                                            'user_id' => $last_id
                                                        ));
                                                        DB::table('segment_user')->insert(array(
                                                            'segment_id' => Partner::find(4235)->segments->first()->id,
                                                            'user_id' => $last_id
                                                        ));

                                                        //Default Tier affiliation to Member
                                                        $defaultTier = Tier::where('partner_id', 4235)->where('name', 'like', 'Default Tier%')->first();
                                                        if (!empty($defaultTier)) {
                                                            $defaultTierCriteria = Tier::where('tier_id', $defaultTier->id)->where('name', 'like', 'Default Tier Criteria%')->first();
                                                            if (!empty($defaultTierCriteria)) {
                                                                DB::table('tiercriteria_user')->insert(array(
                                                                    'tiercriteria_id' => $defaultTierCriteria->id,
                                                                    'user_id' => $last_id
                                                                ));
                                                            }
                                                        }
                                                        $userSMS = User::find($last_id);

                                                        //Reward User
                                                        $user = User::find($last_id);
                                                        $partner = Partner::find(4235);
                                                        if (!empty($pos)) {
                                                            $posId = $pos->id;
                                                            $storeId = $pos->store_id;
                                                        } else {
                                                            $storeDetails = Store::where('partner_id', 4235)->where('name', 'Registration through Customer File')->first();
                                                            $posDetails = Pos::where('store_id', $storeDetails->id)->where('name', 'Registration through Customer File POS')->first();
                                                            $storeId = $storeDetails->store_id;
                                                            $posId = $posDetails->id;
                                                        }
                                                        $params = array(
                                                            'user_id' => $user->id,
                                                            'partner' => $partner->id,
                                                            'pos_id' => $posId,
                                                            'store_id' => $storeId,
                                                            'amount' => floatval($row[11]),
                                                            'currency' => 'SAR',
                                                            'partner_currency' => 'SAR',
                                                            'type' => 'reward',
                                                            'reference' => 'ABD Customer File',
                                                            'network_id' => $partner->firstNetwork()->id,
                                                            'original_transaction_amount' => floatval($row[11]),
                                                            'use_partner_currency' => True,
                                                        );

                                                        $result_do_reward = Transaction::doRewardNewLoyalty($params, 'Amount');
                                                        //$userSMS->sendWelcomeMessageNew(4235);
                                                        //$userSMS->sendWelcomeMessage();
                                                        //$userSMS->sendAccountPin();
                                                        $errBool = False;
                                                    }
                                                }
                                            }
                                        } elseif ($count > 0) {

                                            $userByEmail = DB::table('users')->where('email', $user['email'])->where('draft', false)->first();
                                            $userRefAvailable = User::where('id', $userByEmail->id)->where('draft', false)->whereHas('references', function($q) {
                                                        $q->where('number', 'like', "%ABD-%");
                                                    })->count();
                                            if ($userRefAvailable == 0) {
                                                DB::table('reference')->insert(array(
                                                    'number' => $row[0],
                                                    'partner_id' => 4235,
                                                    'user_id' => $userByEmail->id,
                                                    'created_at' => date('Y-m-d H:i:s', time()),
                                                    'updated_at' => date('Y-m-d H:i:s', time())
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => 4235,
                                                    'user_id' => $userByEmail->id
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => 1,
                                                    'user_id' => $last_id
                                                ));

                                                //Reward User
                                                $user = User::find($last_id);
                                                $partner = Partner::find(4235);
                                                if (!empty($pos)) {
                                                    $posId = $pos->id;
                                                    $storeId = $pos->store_id;
                                                } else {
                                                    $storeDetails = Store::where('partner_id', 4235)->where('name', 'Registration through Customer File')->first();
                                                    $posDetails = Pos::where('store_id', $storeDetails->id)->where('name', 'Registration through Customer File POS')->first();
                                                    $storeId = $storeDetails->store_id;
                                                    $posId = $posDetails->id;
                                                }
                                                $params = array(
                                                    'user_id' => $user->id,
                                                    'partner' => $partner->id,
                                                    'pos_id' => $posId,
                                                    'store_id' => $storeId,
                                                    'amount' => floatval($row[11]),
                                                    'currency' => 'SAR',
                                                    'partner_currency' => 'SAR',
                                                    'type' => 'reward',
                                                    'reference' => 'ABD Customer File',
                                                    'network_id' => $partner->firstNetwork()->id,
                                                    'original_transaction_amount' => floatval($row[11]),
                                                    'use_partner_currency' => True,
                                                );

                                                $result_do_reward = Transaction::doRewardNewLoyalty($params, 'Amount');
                                                $errBool = False;
                                            } else {
                                                $email3 = $row[0] . '@ABDCust.com';
                                                $user['email'] = $email3;
                                                $last_id = DB::table('users')->insertGetId($user);
                                                DB::table('role_user')->insert(array(
                                                    'role_id' => 4,
                                                    'user_id' => $last_id
                                                ));
                                                DB::table('reference')->insert(array(
                                                    'number' => $row[0],
                                                    'partner_id' => 4235,
                                                    'user_id' => $last_id,
                                                    'created_at' => date('Y-m-d H:i:s', time()),
                                                    'updated_at' => date('Y-m-d H:i:s', time())
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => 4235,
                                                    'user_id' => $last_id
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => 1,
                                                    'user_id' => $last_id
                                                ));
                                                DB::table('segment_user')->insert(array(
                                                    'segment_id' => Partner::find(4235)->segments->first()->id,
                                                    'user_id' => $last_id
                                                ));

                                                //Default Tier affiliation to Member
                                                $defaultTier = Tier::where('partner_id', 4235)->where('name', 'like', 'Default Tier%')->first();
                                                if (!empty($defaultTier)) {
                                                    $defaultTierCriteria = Tier::where('tier_id', $defaultTier->id)->where('name', 'like', 'Default Tier Criteria%')->first();
                                                    if (!empty($defaultTierCriteria)) {
                                                        DB::table('tiercriteria_user')->insert(array(
                                                            'tiercriteria_id' => $defaultTierCriteria->id,
                                                            'user_id' => $last_id
                                                        ));
                                                    }
                                                }
                                                //Reward User
                                                $user = User::find($last_id);
                                                $partner = Partner::find(4235);
                                                if (!empty($pos)) {
                                                    $posId = $pos->id;
                                                    $storeId = $pos->store_id;
                                                } else {
                                                    $storeDetails = Store::where('partner_id', 4235)->where('name', 'Registration through Customer File')->first();
                                                    $posDetails = Pos::where('store_id', $storeDetails->id)->where('name', 'Registration through Customer File POS')->first();
                                                    $storeId = $storeDetails->store_id;
                                                    $posId = $posDetails->id;
                                                }
                                                $params = array(
                                                    'user_id' => $user->id,
                                                    'partner' => $partner->id,
                                                    'pos_id' => $posId,
                                                    'store_id' => $storeId,
                                                    'amount' => floatval($row[11]),
                                                    'currency' => 'SAR',
                                                    'partner_currency' => 'SAR',
                                                    'type' => 'reward',
                                                    'reference' => 'ABD Customer File',
                                                    'network_id' => $partner->firstNetwork()->id,
                                                    'original_transaction_amount' => floatval($row[11]),
                                                    'use_partner_currency' => True,
                                                );

                                                $result_do_reward = Transaction::doRewardNewLoyalty($params, 'Amount');
                                                $errBool = False;
                                            }
                                        } elseif ($countmobile > 0) {
                                            $userByMobile = DB::table('users')->where('normalized_mobile', $user['normalized_mobile'])->where('draft', false)->first();
                                            $userRefAvailable = User::where('id', $userByMobile->id)->where('draft', false)->whereHas('references', function($q) {
                                                        $q->where('number', 'like', "%ABD-%");
                                                    })->count();
                                            if ($userRefAvailable == 0) {
                                                DB::table('reference')->insert(array(
                                                    'number' => $row[0],
                                                    'partner_id' => 4235,
                                                    'user_id' => $userByMobile->id,
                                                    'created_at' => date('Y-m-d H:i:s', time()),
                                                    'updated_at' => date('Y-m-d H:i:s', time())
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => 4235,
                                                    'user_id' => $userByMobile->id
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => 1,
                                                    'user_id' => $last_id
                                                ));
                                                $errBool = False;
                                            } else {
                                                $tomobile = str_replace('ABD-', '', $row[0]);
                                                $mobile = "4235" . $tomobile;
                                                $user['mobile'] = $mobile;
                                                $user['normalized_mobile'] = '+966' . $mobile;
                                                $user['telephone_code'] = '+966';
                                                $last_id = DB::table('users')->insertGetId($user);
                                                DB::table('role_user')->insert(array(
                                                    'role_id' => 4,
                                                    'user_id' => $last_id
                                                ));
                                                DB::table('reference')->insert(array(
                                                    'number' => $row[0],
                                                    'partner_id' => 4235,
                                                    'user_id' => $last_id,
                                                    'created_at' => date('Y-m-d H:i:s', time()),
                                                    'updated_at' => date('Y-m-d H:i:s', time())
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => 4235,
                                                    'user_id' => $last_id
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => 1,
                                                    'user_id' => $last_id
                                                ));
                                                DB::table('segment_user')->insert(array(
                                                    'segment_id' => Partner::find(4235)->segments->first()->id,
                                                    'user_id' => $last_id
                                                ));
                                                //Default Tier affiliation to Member
                                                $defaultTier = Tier::where('partner_id', 4235)->where('name', 'like', 'Default Tier%')->first();
                                                if (!empty($defaultTier)) {
                                                    $defaultTierCriteria = Tier::where('tier_id', $defaultTier->id)->where('name', 'like', 'Default Tier Criteria%')->first();
                                                    if (!empty($defaultTierCriteria)) {
                                                        DB::table('tiercriteria_user')->insert(array(
                                                            'tiercriteria_id' => $defaultTierCriteria->id,
                                                            'user_id' => $last_id
                                                        ));
                                                    }
                                                }

                                                //Reward User
                                                $user = User::find($last_id);
                                                $partner = Partner::find(4235);
                                                if (!empty($pos)) {
                                                    $posId = $pos->id;
                                                    $storeId = $pos->store_id;
                                                } else {
                                                    $storeDetails = Store::where('partner_id', 4235)->where('name', 'Registration through Customer File')->first();
                                                    $posDetails = Pos::where('store_id', $storeDetails->id)->where('name', 'Registration through Customer File POS')->first();
                                                    $storeId = $storeDetails->store_id;
                                                    $posId = $posDetails->id;
                                                }
                                                $params = array(
                                                    'user_id' => $user->id,
                                                    'partner' => $partner->id,
                                                    'pos_id' => $posId,
                                                    'store_id' => $storeId,
                                                    'amount' => floatval($row[11]),
                                                    'currency' => 'SAR',
                                                    'partner_currency' => 'SAR',
                                                    'type' => 'reward',
                                                    'reference' => 'ABD Customer File',
                                                    'network_id' => $partner->firstNetwork()->id,
                                                    'original_transaction_amount' => floatval($row[11]),
                                                    'use_partner_currency' => True,
                                                );

                                                $result_do_reward = Transaction::doRewardNewLoyalty($params, 'Amount');

                                                $errBool = False;
                                            }
                                        }
                                    } else {
                                        $userByMobile = DB::table('users')->where('normalized_mobile', $user['normalized_mobile'])->where('draft', false)->first();
                                        $userRefAvailable = User::where('id', $userByMobile->id)->where('draft', false)->whereHas('references', function($q) {
                                                    $q->where('number', 'like', "%ABD-%");
                                                })->count();
                                        if ($userRefAvailable == 0) {
                                            DB::table('reference')->insert(array(
                                                'number' => $row[0],
                                                'partner_id' => 4235,
                                                'user_id' => $userByMobile->id,
                                                'created_at' => date('Y-m-d H:i:s', time()),
                                                'updated_at' => date('Y-m-d H:i:s', time())
                                            ));
                                            DB::table('partner_user')->insert(array(
                                                'partner_id' => 4235,
                                                'user_id' => $userByMobile->id
                                            ));
                                            DB::table('partner_user')->insert(array(
                                                'partner_id' => 1,
                                                'user_id' => $last_id
                                            ));
                                            //Reward User
                                            $user = User::find($last_id);
                                            $partner = Partner::find(4235);
                                            if (!empty($pos)) {
                                                $posId = $pos->id;
                                                $storeId = $pos->store_id;
                                            } else {
                                                $storeDetails = Store::where('partner_id', 4235)->where('name', 'Registration through Customer File')->first();
                                                $posDetails = Pos::where('store_id', $storeDetails->id)->where('name', 'Registration through Customer File POS')->first();
                                                $storeId = $storeDetails->store_id;
                                                $posId = $posDetails->id;
                                            }
                                            $params = array(
                                                'user_id' => $user->id,
                                                'partner' => $partner->id,
                                                'pos_id' => $posId,
                                                'store_id' => $storeId,
                                                'amount' => floatval($row[11]),
                                                'currency' => 'SAR',
                                                'partner_currency' => 'SAR',
                                                'type' => 'reward',
                                                'reference' => 'ABD Customer File',
                                                'network_id' => $partner->firstNetwork()->id,
                                                'original_transaction_amount' => floatval($row[11]),
                                                'use_partner_currency' => True,
                                            );

                                            $result_do_reward = Transaction::doRewardNewLoyalty($params, 'Amount');

                                            $errBool = False;
                                        } else {
                                            $tomobile = str_replace('ABD-', '', $row[0]);
                                            $mobile = "4235" . $tomobile;
                                            $user['mobile'] = $mobile;
                                            $user['normalized_mobile'] = '+966' . $mobile;
                                            $user['telephone_code'] = '+966';
                                            $last_id = DB::table('users')->insertGetId($user);
                                            DB::table('role_user')->insert(array(
                                                'role_id' => 4,
                                                'user_id' => $last_id
                                            ));
                                            DB::table('partner_user')->insert(array(
                                                'partner_id' => 4235,
                                                'user_id' => $last_id
                                            ));
                                            DB::table('partner_user')->insert(array(
                                                'partner_id' => 1,
                                                'user_id' => $last_id
                                            ));
                                            DB::table('segment_user')->insert(array(
                                                'segment_id' => Partner::find(4235)->segments->first()->id,
                                                'user_id' => $last_id
                                            ));
                                            //Default Tier affiliation to Member
                                            $defaultTier = Tier::where('partner_id', 4235)->where('name', 'like', 'Default Tier%')->first();
                                            if (!empty($defaultTier)) {
                                                $defaultTierCriteria = Tier::where('tier_id', $defaultTier->id)->where('name', 'like', 'Default Tier Criteria%')->first();
                                                if (!empty($defaultTierCriteria)) {
                                                    DB::table('tiercriteria_user')->insert(array(
                                                        'tiercriteria_id' => $defaultTierCriteria->id,
                                                        'user_id' => $last_id
                                                    ));
                                                }
                                            }
                                            //Reward User
                                            $user = User::find($last_id);
                                            $partner = Partner::find(4235);
                                            if (!empty($pos)) {
                                                $posId = $pos->id;
                                                $storeId = $pos->store_id;
                                            } else {
                                                $storeDetails = Store::where('partner_id', 4235)->where('name', 'Registration through Customer File')->first();
                                                $posDetails = Pos::where('store_id', $storeDetails->id)->where('name', 'Registration through Customer File POS')->first();
                                                $storeId = $storeDetails->store_id;
                                                $posId = $posDetails->id;
                                            }
                                            $errBool = False;
                                            $counterrors = $counterrors + 1;
                                            $errors[] = $row;
                                        }
                                    }
                                }
                                if ($errBool == True) {
                                    $counterrors = $counterrors + 1;
                                    $errors[] = $row;
//                                    echo "<pre>1";
//                                    var_dump($errors);
//                                    exit();
                                }
                            }
                        }
                    }
                    $count++;
                }

                return (count($errors) > 0) ?
                        $this->customerImportFailed($errors, $generatedFileBaseName, 'Abdulwahed Bank') : $this->customerImportSucceed('Abdulwahed Bank', $generatedFileBaseName);
            } elseif (($ext == 'TXT' || $ext == 'txt') && strstr($name1, 'AJIBTransactions') && strcmp($this->user->api_key, "686c43270a8dae1d65a448e3c228f718") == 0) {
                //Reference ID|PostingDate|Product ID|BonusPts|Billing Amount|Type|MCC|
                //  0               1           2           3       4           5   6 
                //Billing Currency|Transaction currency|Transaction Amount|Transaction Sign|Line number
                //        7               8                       9               10              11
                $tx = array();
                $count = 0;
                $counterrors = 0;
                $errBool = True;
                $errors = array();
                $params = array();
                $passes = 0;

                AuditHelper::record(
                        4, $auth_user->name . " started uploading file: " . $generatedFileName
                );

                $path = $this->transactionImportPatch();
                $file->move($path, $generatedFileName);
                $inputFile = $path . '/' . $generatedFileName;

                $contents = File::get($inputFile);
                $contents = nl2br($contents, FALSE);
                $contentLines = explode("<br>", $contents);
                $errors = array();


                $countContentLines = count($contentLines) - 1;
                $test = $contentLines[$countContentLines];
                $test1 = $contentLines[$countContentLines - 1];
                $ret_eof_error = true;
                if (trim($test) === "EOF") {
                    $ret_eof_error = false;
                } elseif (trim($test1) === "EOF") {
                    $ret_eof_error = false;
                }
                if ($ret_eof_error == true) {
                    return self::respondWith(
                                    200, 'EOF NOT FOUND', $errors, 'errors'
                    );
                }
                $arrContent = array();
                foreach ($contentLines as $lineread) {
                    if (trim($lineread) !== 'EOF') {
                        $arrContent[] = explode("|", $lineread);
                    }
                }
                $partner = $this->partner;
                $network_id = $partner->firstNetwork()->id;
                $partner_id = $partner->id;
                $partners = $partner->managedObjects();
                $partners_children = array();
                foreach ($partners as $part) {
                    $partners_children[] = $part->id;
                }
                $loy_by_partner = RewardHelper::loyaltyByPartner($partners_children);

                $partner_prefix = $partner->prefix;
                $itemchosen = ProductPartnerReward::where('partner_id', $partner_id)->get();
                $arr_item_chosen = array();
                foreach ($itemchosen as $ic) {
                    $arr_item_chosen[$ic->partner_mapping_id] = $ic->product_id;
                }


                $transactions_file = Transaction::select('ref_number')->where('ref_number', 'like', $name1 . "%")->get();
                $arr_transactions_file = $transactions_file->toArray();

//                $ref_number = $name1."1";
//                echo "<pre>";
//                var_dump(array_search($ref_number, array_column($arr_transactions_file, 'ref_number')));
//                exit();
                $arr_currencies = array();
                $currencies = Currency::all();
                foreach ($currencies as $curr) {
                    $arr_currencies[$curr->short_code] = $curr->id;
                }
                if (count($arr_transactions_file) > 0) {
                    return self::respondWith(
                                    200, 'NOT OK File Already Exists', null, 'errors'
                    );
                }

                $store_res = $partner->stores->first();
                if ($store_res) {
                    $pos_id = $store_res->pointOfSales->first()->id;
                    $tx['store_id'] = $store_res->id;
                    $tx['pos_id'] = $pos_id;
                }

//                echo "<pre>";
//                var_dump($arrContent);
//                exit();
                foreach ($arrContent as $row) {
                    $lineNumber = 0;
                    $posted_at = date('Y-m-d H:i:s');
                    if ($count != 0) {
                        if (trim($row[0]) != '' && trim($row[0]) != 'EOF') {

                            $unhandled_date = explode("/", $row[1]);

                            if (!isset($row[11])) {
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-001";
                                $errors[] = $row;

                                continue;
                            } elseif (substr(trim($row[0]), 0, 5) !== "AJIB-" || trim($row[0]) === "" || $row[0] === " ") {
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-002";
                                $errors[] = $row;

                                continue;
                            }///self::respondwith('400', ,null);
                            elseif ($row[1] === "" || $row[1] === " " || !checkdate($unhandled_date[1], $unhandled_date[0], $unhandled_date[2])) {
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-003";
                                $errors[] = $row;

                                continue;
                            } elseif ($row[2] === "" || $row[2] === " " || !is_numeric($row[2])) {
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-004";
                                $errors[] = $row;

                                continue;
                            } elseif ($row[3] === "" || $row[3] === " " || is_numeric($row[3]) === FALSE) {
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-005";
                                $errors[] = $row;

                                continue;
                            } elseif ($row[4] === "" || $row[4] === " " || !is_numeric($row[4])) {
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-006";
                                $errors[] = $row;

                                continue;
                            } elseif ($row[9] === "" || $row[9] === " " || !is_numeric($row[9])) {
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-006.1";
                                $errors[] = $row;

                                continue;
                            } elseif ($row[7] === "" || $row[7] === " " || strlen($row[7]) !== 3 || !isset($arr_currencies[$row[8]])) {
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-007";
                                $errors[] = $row;

                                continue;
                            } elseif ($row[8] === "" || $row[8] === " " || strlen($row[8]) !== 3 || !isset($arr_currencies[$row[8]])) {
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-007.2";
                                $errors[] = $row;

                                continue;
                            } elseif ($row[5] === "" || $row[5] === " " || strlen($row[5]) !== 2) {
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-007.1";
                                $errors[] = $row;

                                continue;
                            } elseif ($row[6] === "" || $row[6] === " ") {
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-008";
                                $errors[] = $row;

                                continue;
                            } elseif (trim($row[10]) !== "C" && trim($row[10]) !== "D") {
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-010";
                                $errors[] = $row;

                                continue;
                            } elseif (!is_numeric(trim($row[11]))) {
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-009";
                                $errors[] = $row;

                                continue;
                            }

                            if (isset($row[11])) {
                                $lineNumber = $row[11];
                            }
                            $ref_number = $name1 . $lineNumber;
                            $ref_number = rtrim($ref_number);

                            if (array_search($ref_number, array_column($arr_transactions_file, 'ref_number')) === FALSE) {
                                $coefficient = 1;
                                if ($row[10] == 'C') {
                                    $coefficient = intval(-1);
                                }
                                $row[0] = trim($row[0]);
                                $tx['id'] = 'NULL';
                                $tx['ref_number'] = $ref_number;

                                $tx['original_total_amount'] = $row[9] * $coefficient;
                                $tx['partner_amount'] = $row[4] * $coefficient;
                                $tx['partner_trxtype'] = $row[5];
                                $tx['mcc'] = $row[6];


                                $tx['user_id'] = 1;
                                $tx['points_redeemed'] = 0;
                                $tx['points_rewarded'] = 0;
                                $tx['partner_id'] = $partner->id;
                                $tx['network_id'] = $network_id;
                                $tx['trx_reward'] = 1;

                                $reference = Reference::where('number', $row[0])->first();
                                if (isset($reference->user_id)) {
                                    $user = $reference->user_id;
                                } else {
                                    $errBool = true;
                                    $counterrors = $counterrors + 1;
                                    $errors[] = $row;
                                    continue;
                                }

                                $tx['user_id'] = $user;

                                $userInfo = User::find($user);
                                if ($userInfo->status == 'closed') {
                                    $errBool = true;
                                    $counterrors = $counterrors + 1;
                                    $errors[] = $row;
                                    continue;
                                }

                                $date_unhandled = $unhandled_date[1] . "/" . $unhandled_date[0] . "/" . $unhandled_date[2];

                                $date = strtotime($date_unhandled);
                                $new_date = date('Y-m-d H:i:s', $date);

                                $tx['created_at'] = $new_date;
                                $tx['updated_at'] = $new_date;
                                $tx['posted_at'] = $posted_at;
                                // Map Currency
//                                $loyalty = Loyalty::where('partner_id',$tx['partner_id'])->first();
                                //$itemchosen = ProductPartnerReward::where('partner_mapping_id',$partner_prefix.$row[2])->first();
                                if (isset($arr_item_chosen[$partner_prefix . $row[2]])) {
                                    $tx['product_id'] = $arr_item_chosen[$partner_prefix . $row[2]];
                                } else {
                                    $tx['product_id'] = NULL;
                                }




                                $validator = Validator::make($tx, array(
                                            //'id'         => 'required|numeric',
                                            'partner_id' => 'required|numeric',
                                            'store_id' => 'required|numeric',
                                            'user_id' => 'required|numeric'
                                ));

                                if (!$validator->fails()) {

                                    if (is_numeric($row[3])) {
                                        $txbonus = array(
                                            'partner_id' => $tx['partner_id'],
                                            'store_id' => $tx['store_id'],
                                            'pos_id' => $tx['pos_id'],
                                            'user_id' => $tx['user_id'],
                                            'ref_number' => "AJIB ONLINE Bonus POINTS",
                                            'total_amount' => 0,
                                            'points_rewarded' => round($row[3], 0, PHP_ROUND_HALF_UP) * $coefficient,
                                            'amt_reward' => PointsHelper::pointsToAmount(round($row[3], 0, PHP_ROUND_HALF_UP), "JOD", $network_id) * $coefficient,
                                            'trx_reward' => 1,
                                            'currency_id' => 6,
                                            'exchange_rate' => 1,
                                            'country_id' => 8,
                                            'quantity' => 0,
                                            'notes' => 'Bonus Points',
                                            'trans_class' => "Amount",
                                            'network_id' => $network_id,
                                            'created_at' => $new_date,
                                            'updated_at' => $new_date,
                                            'posted_at' => $posted_at
                                        );

                                        if ($txbonus['points_rewarded'] != 0) {
                                            DB::table('transaction')->insert($txbonus);
                                            $user = User::find($txbonus['user_id']);
                                            $user->updateBalance((round($row[3], 0, PHP_ROUND_HALF_UP) * $coefficient), $txbonus['network_id'], 'add');
                                            $partner = Partner::find($txbonus['partner_id']);
                                        }
                                        //$template = View::make('sms.bonus-points', array('bonusPoints' => $row[3]))->render();
                                        //SMSHelper::send($template, $user, $partner);
                                    }

                                    $passes++;

                                    if (is_numeric($tx['points_rewarded'])) {

                                        $params = array(
                                            'user_id' => $tx['user_id'],
                                            'partner' => $tx['partner_id'],
                                            'pos_id' => $tx['pos_id'],
                                            'store_id' => $tx['store_id'],
                                            'amount' => floatval($tx['partner_amount']),
                                            'currency' => $row[8],
                                            //'notes'		=> 'Reward By Product',
                                            'type' => 'reward',
                                            'reference' => $tx['ref_number'],
                                            'network_id' => $partner->firstNetwork()->id,
                                            'product_id' => $tx['product_id'],
                                            'partner_currency' => $row[7],
                                            'original_transaction_amount' => floatval($tx['original_total_amount']),
                                            'use_partner_currency' => True,
                                            'mcc' => $tx['mcc'],
                                            'partner_trxtype' => $tx['partner_trxtype'],
                                            'created_at' => $new_date,
                                            'updated_at' => $new_date
                                        );

                                        $user = User::find($tx['user_id']);
                                        try {
                                            $result_do_reward = RewardHelper::doRewardNewLoyalty($params, 'Amount', $loy_by_partner);
                                        } catch (Exception $ex) {
                                            //$resp = array($ex);
                                            $json_data = json_encode(array("Exception" => $ex));
                                            APIController::postSendEmailJson($json_data);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    $count++;
                }

                return (count($errors) > 0) ?
                        $this->transactionImportFailed($errors, $generatedFileBaseName, 'AJIB Bank') : $this->transactionImportSucceed('AJIB Bank', $generatedFileBaseName);
            }
            //AJIB Users New
            elseif (($ext == 'TXT' || $ext == 'txt') && strstr($name1, 'AJIBCustomer') && strcmp($this->user->api_key, "686c43270a8dae1d65a448e3c228f718") == 0) {
                //ReferenceID|Title|FirstName|LastName|Email|Mobile|DOB|Gender|MaritalStatus|Occupation|CustomerID|Status
                //0           1     2         3        4     5      6   7      8             9          11         10

                AuditHelper::record(
                        4, $auth_user->name . " started uploading file: " . $generatedFileName
                );

                $path = $this->customerImportPatch();
                $file->move($path, $generatedFileName);
                $inputFile = $path . '/' . $generatedFileName;
                $tx = array();

                $contents = File::get($inputFile);
                $contents = nl2br($contents, FALSE);
                $contentLines = explode("<br>", $contents);

                $arrContent = array();
                foreach ($contentLines as $lineread) {
                    $arrContent[] = explode("|", $lineread);
                }
                $count = 0;
                $counterrors = 0;
                $errBool = True;
                $errors = array();
                $partner_id = $this->partner->id;
                $storeDetails = Store::where('partner_id', $partner_id)->where('name', 'Registration through Customer File')->first();
                $posDetails = Pos::where('store_id', $storeDetails->id)->where('name', 'Registration through Customer File POS')->first();
                foreach ($arrContent as $row) {
                    if ($count != 0) {
                        if (trim($row[0]) != '' && trim($row[0]) !== 'EOF') {
                            $row[0] = trim($row[0]);
                            $errBool = True;

                            $user = array();
                            $user['id'] = 'NULL';
                            $user['first_name'] = $row[2];
                            $user['last_name'] = $row[3];
                            if (empty($row[3])) {
                                $user['last_name'] = "Empty";
                            }
                            $mobile = $row[5];
                            $countc = InputHelper::isPhone($mobile);
                            $pos = strpos($mobile, $countc);
                            if ($pos !== false) {
                                $mobile2 = substr_replace($mobile, "", $pos, strlen($countc));
                            } else {
                                $mobile2 = str_replace($countc, "", $mobile);
                            }
                            //$user_mobile_check = User::where("mobile", $mobile2)->count();
                            $mobile = ltrim($mobile, '0');
                            $tomobile = str_replace('AJIB-', '', $row[0]);
                            //echo $mobile . "---<br>";
                            if (!is_numeric($mobile)){ // || $user_mobile_check > 0) {
                                $mobile = $partner_id . $tomobile;
                                $user['mobile'] = $mobile;
                                $user['normalized_mobile'] = '+962' . $mobile;
                                $user['telephone_code'] = '+962';
                                $counterrors = $counterrors + 1;
                                $row['error'] = "Mobile already exists or not provided";
                                $errors[] = $row;
                            } elseif (InputHelper::isPhone($mobile)) {
                                $countc = InputHelper::isPhone($mobile);
                                $pos = strpos($mobile, $countc);
                                if ($pos !== false) {
                                    $mobile = substr_replace($mobile, "", $pos, strlen($countc));
                                } else {
                                    $mobile = str_replace($countc, "", $mobile);
                                }
                                $user['mobile'] = $mobile;
                                $user['normalized_mobile'] = '+' . $countc . $mobile;
                                $user['telephone_code'] = '+' . $countc;
                            } else {
                                $user['mobile'] = "";
                                $user['normalized_mobile'] = '';
                                $user['telephone_code'] = '';
                            }
                            //APIController::postSendEmailJson(json_encode(array($user_mobile_check,$user)), "AJIB Customer file");
                            //echo "****" . InputHelper::isPhone($mobile) . "+++<br>" . $mobile . "====<br>";
                            $email1 = trim($row[4]);
                            $email3 = '';
                            if ($email1 == '' || !InputHelper::isEmail($email1)) {
                                $email3 = $row[0] . '@AJIBCust.com';
                            } else {
                                $email3 = $email1;
                            }

                            $user['email'] = $email3;

                            $unhandledDate = $row[6];
                            if ($unhandledDate == '' || $unhandledDate == ' ') {
                                $unhandledDate = "01/01/1902";
                            }

                            $arrunhandledDate = explode("/", $unhandledDate);
                            $date_unhandled = $arrunhandledDate[0] . "/" . $arrunhandledDate[1] . "/" . $arrunhandledDate[2];

                            $date = strtotime($date_unhandled);
                            $new_date = date('Y-m-d H:i:s', $date);
                            $user['dob'] = $new_date;

                            $user['balance'] = 0;
                            $user['marital_status'] = $row[8];
                            $user['confirm_passcode'] = false;
                            $user['passcode'] = PinCode::newCode();
                            $user['title'] = $row[1];
                            $user['draft'] = false;
                            $user['verified'] = 1;
                            $user['status'] = 'inactive';

                            $user['created_at'] = date("Y-m-d H:i:s");
                            $user['updated_at'] = date("Y-m-d H:i:s");
                            $user['ucid'] = UCID::newCustomerID();
                            $user['country_id'] = 4;
                            $user['city_id'] = 215;
                            $user['area_id'] = 35;

                            // Validate gender
                            switch ($row[7]) {
                                case "Male":
                                case "M":
                                    $user['gender'] = 'm';
                                    break;
                                case "Female":
                                case "F":
                                    $user['gender'] = 'f';
                                    break;
                                default:
                                    $user['gender'] = 'not selected';
                            }

                            // Validate balance
                            if ($user['balance'] == null) {
                                $user['balance'] = 0;
                            }

                            // Validate dob
                            if ($user['dob'] == null) {
                                $user['dob'] = date("Y-m-d H:i:s", strtotime("1 January 1902"));
                            }

                            $user['otp'] = $row[11]; //AF: Changed this from row[10] to row[11] according to file columns
                            //set the registration source POS

                            $user['registration_source_pos'] = $posDetails->id;

                            $validator = Validator::make($user, array(
                                        'first_name' => 'required',
                                        'last_name' => 'required',
                                        'email' => 'required|email|unique:users',
                                        'mobile' => 'required|numeric',
                                        'dob' => 'required',
                                        'balance' => 'required|numeric'
                            ));

                            $skipstatus = array();
                            $skipstatus[] = 'inactive';
                            $skipstatus[] = 'closed';
                            $countmobileunique = DB::table('users')->where('normalized_mobile', $user['normalized_mobile'])->whereNotIn('status', $skipstatus)->count();

                            $count = DB::table('users')->where('email', $user['email'])->where('draft', false)->count();
                            $countmobile = DB::table('users')->where('normalized_mobile', $user['normalized_mobile'])->count();

                            $reference = Reference::where('number', $row[0])->first();

                            if (!$validator->fails() && $countmobileunique <= 0) {
                                if (!isset($reference)) {
                                    // Create the user
                                    $last_id = DB::table('users')->insertGetId($user);

                                    // Assign the role
                                    DB::table('role_user')->insert(array(
                                        'role_id' => 4,
                                        'user_id' => $last_id
                                    ));
                                    DB::table('reference')->insert(array(
                                        'number' => $row[0],
                                        'partner_id' => $partner_id,
                                        'user_id' => $last_id,
                                        'created_at' => date('Y-m-d H:i:s', time()),
                                        'updated_at' => date('Y-m-d H:i:s', time())
                                    ));
                                    DB::table('partner_user')->insert(array(
                                        'partner_id' => $partner_id,
                                        'user_id' => $last_id
                                    ));
                                    DB::table('partner_user')->insert(array(
                                        'partner_id' => 1,
                                        'user_id' => $last_id
                                    ));
                                    DB::table('segment_user')->insert(array(
                                        'segment_id' => Partner::find($partner_id)->segments->first()->id,
                                        'user_id' => $last_id
                                    ));

                                    //create default balance
                                    $createdUser = User::find($last_id);
                                    $createdUser->updateBalance(0, $this->partner->firstNetwork()->id, 'add');
                                    //Default Tier affiliation to Member
                                    $defaultTier = Tier::where('partner_id', $partner_id)->where('name', 'like', 'Default Tier%')->first();
                                    if (!empty($defaultTier)) {
                                        $defaultTierCriteria = Tier::where('tier_id', $defaultTier->id)->where('name', 'like', 'Default Tier Criteria%')->first();
                                        if (!empty($defaultTierCriteria)) {
                                            DB::table('tiercriteria_user')->insert(array(
                                                'tiercriteria_id' => $defaultTierCriteria->id,
                                                'user_id' => $last_id
                                            ));
                                        }
                                    }

                                    $userSMS = User::find($last_id);
                                    //$custRef = $row[0];
                                    //$template    = View::make('sms.new-user-ajib', array('ref' => $custRef, 'user' => $userSMS))->render();
                                    $partner = Partner::find($partner_id);
                                    $userSMS->sendWelcomeMessageNew($partner->id);
//                                  SMSHelper::send($template, $userSMS, $partner);

                                    $errBool = False;
                                    //DB::table('transaction')->insert($tx);
                                } elseif (isset($reference)) {

                                    $user1 = $reference->user_id;

                                    unset($user['id']);
                                    unset($user['passcode']);
                                    //AF 10-12-17:Added this to update the mobile only
                                    unset($user['first_name']);
                                    unset($user['last_name']);
                                    unset($user['email']);
                                    unset($user['dob']);
                                    unset($user['gender']);
                                    unset($user['marital_status']);
                                    unset($user['status']);
                                    //DB::table('partner_user')->insert(array(
                                    //    'partner_id' => $partner_id,
                                    //    'user_id' => $user1
                                    //));
                                    $user_mobile['mobile'] = $user['mobile'];
                                    $user_mobile['normalized_mobile'] = $user['normalized_mobile'];

                                    User::where('id', $user1)->update($user_mobile);
                                    $errBool = False;
                                }
                            } else {

                                //$reference = Reference::where('number', $row[0])->first();
                                if (isset($reference) && $countmobileunique <= 0) {
                                    $user1 = $reference->user_id;
                                    unset($user['id']);
                                    unset($user['passcode']);
                                    //DB::table('partner_user')->insert(array(
                                    //    'partner_id' => $partner_id,
                                    //    'user_id' => $user1
                                    //));
                                    $user_mobile['mobile'] = $user['mobile'];
                                    $user_mobile['normalized_mobile'] = $user['normalized_mobile'];

                                    User::where('id', $user1)->update($user_mobile);
                                    $errBool = False;
                                } else {
                                    if ($countmobileunique <= 0) {
                                        $count = DB::table('users')->where('email', $user['email'])->where('draft', false)->count();
                                        $countmobile = DB::table('users')->where('normalized_mobile', $user['normalized_mobile'])->where('draft', false)->count();

                                        if ($count == 0 && empty($user['email'])) {
                                            $email3 = $row[0] . '@ABBCust.com';
                                            $user['email'] = $email3;
                                            $validator = Validator::make($user, array(
                                                        'first_name' => 'required',
                                                        'last_name' => 'required',
                                                        'email' => 'required|email|unique:users',
                                                        'mobile' => 'required|numeric',
                                                        'dob' => 'required',
                                                        'balance' => 'required|numeric'
                                            ));
                                            if (!$validator->fails()) {
                                                $last_id = DB::table('users')->insertGetId($user);
                                                //$last_id = DB::table('users')->getPdo()->lastInsertId();
                                                // Assign the role
                                                DB::table('role_user')->insert(array(
                                                    'role_id' => 4,
                                                    'user_id' => $last_id
                                                ));
                                                DB::table('reference')->insert(array(
                                                    'number' => $row[0],
                                                    'partner_id' => $partner_id,
                                                    'user_id' => $last_id,
                                                    'created_at' => date('Y-m-d H:i:s', time()),
                                                    'updated_at' => date('Y-m-d H:i:s', time())
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => $partner_id,
                                                    'user_id' => $last_id
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => 1,
                                                    'user_id' => $last_id
                                                ));
                                                DB::table('segment_user')->insert(array(
                                                    'segment_id' => Partner::find($partner_id)->segments->first()->id,
                                                    'user_id' => $last_id
                                                ));

                                                //Default Tier affiliation to Member
                                                $defaultTier = Tier::where('partner_id', $partner_id)->where('name', 'like', 'Default Tier%')->first();
                                                if (!empty($defaultTier)) {
                                                    $defaultTierCriteria = Tier::where('tier_id', $defaultTier->id)->where('name', 'like', 'Default Tier Criteria%')->first();
                                                    if (!empty($defaultTierCriteria)) {
                                                        DB::table('tiercriteria_user')->insert(array(
                                                            'tiercriteria_id' => $defaultTierCriteria->id,
                                                            'user_id' => $last_id
                                                        ));
                                                    }
                                                }

                                                $userSMS = User::find($last_id);
                                                $partner = Partner::find($partner_id);
                                                $userSMS->sendWelcomeMessageNew($partner->id);
                                                //$userSMS->sendWelcomeMessage();
                                                //$userSMS->sendAccountPin();
                                                $errBool = False;
                                            } elseif ($validator->fails()) {
                                                if ($countmobile == 0 || empty($user['normalized_mobile'])) {
                                                    $mobile = $partner_id . $tomobile;
                                                    $user['mobile'] = $mobile;
                                                    $user['normalized_mobile'] = '+962' . $mobile;
                                                    $user['telephone_code'] = '+962';
                                                    $validator = Validator::make($user, array(
                                                                'first_name' => 'required',
                                                                'last_name' => 'required',
                                                                'email' => 'required|email|unique:users',
                                                                'mobile' => 'required|numeric',
                                                                'dob' => 'required',
                                                                'balance' => 'required|numeric'
                                                    ));
                                                    if (!$validator->fails()) {
                                                        $last_id = DB::table('users')->insertGetId($user);
                                                        //$last_id = DB::table('users')->getPdo()->lastInsertId();
                                                        // Assign the role
                                                        DB::table('role_user')->insert(array(
                                                            'role_id' => 4,
                                                            'user_id' => $last_id
                                                        ));
                                                        DB::table('reference')->insert(array(
                                                            'number' => $row[0],
                                                            'partner_id' => $partner_id,
                                                            'user_id' => $last_id,
                                                            'created_at' => date('Y-m-d H:i:s', time()),
                                                            'updated_at' => date('Y-m-d H:i:s', time())
                                                        ));
                                                        DB::table('partner_user')->insert(array(
                                                            'partner_id' => $partner_id,
                                                            'user_id' => $last_id
                                                        ));
                                                        DB::table('partner_user')->insert(array(
                                                            'partner_id' => 1,
                                                            'user_id' => $last_id
                                                        ));
                                                        DB::table('segment_user')->insert(array(
                                                            'segment_id' => Partner::find($partner_id)->segments->first()->id,
                                                            'user_id' => $last_id
                                                        ));

                                                        //Default Tier affiliation to Member
                                                        $defaultTier = Tier::where('partner_id', $partner_id)->where('name', 'like', 'Default Tier%')->first();
                                                        if (!empty($defaultTier)) {
                                                            $defaultTierCriteria = Tier::where('tier_id', $defaultTier->id)->where('name', 'like', 'Default Tier Criteria%')->first();
                                                            if (!empty($defaultTierCriteria)) {
                                                                DB::table('tiercriteria_user')->insert(array(
                                                                    'tiercriteria_id' => $defaultTierCriteria->id,
                                                                    'user_id' => $last_id
                                                                ));
                                                            }
                                                        }
                                                        $userSMS = User::find($last_id);
                                                        $partner = Partner::find($partner_id);
                                                        $userSMS->sendWelcomeMessageNew($partner->id);
                                                        //$userSMS->sendWelcomeMessage();
                                                        //$userSMS->sendAccountPin();
                                                        $errBool = False;
                                                    }
                                                }
                                            }
                                        } elseif ($count > 0) {

                                            $userByEmail = DB::table('users')->where('email', $user['email'])->where('draft', false)->first();
                                            $userRefAvailable = User::where('id', $userByEmail->id)->where('draft', false)->whereHas('references', function($q) {
                                                        $q->where('number', 'like', "%AJIB-%");
                                                    })->count();
                                            if ($userRefAvailable == 0) {
                                                DB::table('reference')->insert(array(
                                                    'number' => $row[0],
                                                    'partner_id' => $partner_id,
                                                    'user_id' => $userByEmail->id,
                                                    'created_at' => date('Y-m-d H:i:s', time()),
                                                    'updated_at' => date('Y-m-d H:i:s', time())
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => $partner_id,
                                                    'user_id' => $userByEmail->id
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => 1,
                                                    'user_id' => $last_id
                                                ));
                                                $errBool = False;
                                            } else {
                                                $email3 = $row[0] . '@ABBCust.com';
                                                $user['email'] = $email3;
                                                $last_id = DB::table('users')->insertGetId($user);
                                                DB::table('role_user')->insert(array(
                                                    'role_id' => 4,
                                                    'user_id' => $last_id
                                                ));
                                                DB::table('reference')->insert(array(
                                                    'number' => $row[0],
                                                    'partner_id' => $partner_id,
                                                    'user_id' => $last_id,
                                                    'created_at' => date('Y-m-d H:i:s', time()),
                                                    'updated_at' => date('Y-m-d H:i:s', time())
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => $partner_id,
                                                    'user_id' => $last_id
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => 1,
                                                    'user_id' => $last_id
                                                ));
                                                DB::table('segment_user')->insert(array(
                                                    'segment_id' => Partner::find($partner_id)->segments->first()->id,
                                                    'user_id' => $last_id
                                                ));

                                                //Default Tier affiliation to Member
                                                $defaultTier = Tier::where('partner_id', $partner_id)->where('name', 'like', 'Default Tier%')->first();
                                                if (!empty($defaultTier)) {
                                                    $defaultTierCriteria = Tier::where('tier_id', $defaultTier->id)->where('name', 'like', 'Default Tier Criteria%')->first();
                                                    if (!empty($defaultTierCriteria)) {
                                                        DB::table('tiercriteria_user')->insert(array(
                                                            'tiercriteria_id' => $defaultTierCriteria->id,
                                                            'user_id' => $last_id
                                                        ));
                                                    }
                                                }
                                                $errBool = False;
                                            }
                                        } elseif ($countmobile > 0) {
                                            $userByMobile = DB::table('users')->where('normalized_mobile', $user['normalized_mobile'])->where('draft', false)->first();
                                            $userRefAvailable = User::where('id', $userByMobile->id)->where('draft', false)->whereHas('references', function($q) {
                                                        $q->where('number', 'like', "%AJIB-%");
                                                    })->count();
                                            if ($userRefAvailable == 0) {
                                                DB::table('reference')->insert(array(
                                                    'number' => $row[0],
                                                    'partner_id' => $partner_id,
                                                    'user_id' => $userByMobile->id,
                                                    'created_at' => date('Y-m-d H:i:s', time()),
                                                    'updated_at' => date('Y-m-d H:i:s', time())
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => $partner_id,
                                                    'user_id' => $userByMobile->id
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => 1,
                                                    'user_id' => $last_id
                                                ));
                                                $errBool = False;
                                            } else {
                                                $tomobile = str_replace('AJIB-', '', $row[0]);
                                                $mobile = $partner_id . $tomobile;
                                                $user['mobile'] = $mobile;
                                                $user['normalized_mobile'] = '+962' . $mobile;
                                                $user['telephone_code'] = '+962';
                                                $last_id = DB::table('users')->insertGetId($user);
                                                DB::table('role_user')->insert(array(
                                                    'role_id' => 4,
                                                    'user_id' => $last_id
                                                ));
                                                DB::table('reference')->insert(array(
                                                    'number' => $row[0],
                                                    'partner_id' => $partner_id,
                                                    'user_id' => $last_id,
                                                    'created_at' => date('Y-m-d H:i:s', time()),
                                                    'updated_at' => date('Y-m-d H:i:s', time())
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => $partner_id,
                                                    'user_id' => $last_id
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => 1,
                                                    'user_id' => $last_id
                                                ));
                                                DB::table('segment_user')->insert(array(
                                                    'segment_id' => Partner::find($partner_id)->segments->first()->id,
                                                    'user_id' => $last_id
                                                ));
                                                //Default Tier affiliation to Member
                                                $defaultTier = Tier::where('partner_id', $partner_id)->where('name', 'like', 'Default Tier%')->first();
                                                if (!empty($defaultTier)) {
                                                    $defaultTierCriteria = Tier::where('tier_id', $defaultTier->id)->where('name', 'like', 'Default Tier Criteria%')->first();
                                                    if (!empty($defaultTierCriteria)) {
                                                        DB::table('tiercriteria_user')->insert(array(
                                                            'tiercriteria_id' => $defaultTierCriteria->id,
                                                            'user_id' => $last_id
                                                        ));
                                                    }
                                                }

                                                $errBool = False;
                                            }
                                        }
                                    } else {
                                        $userByMobile = DB::table('users')->where('normalized_mobile', $user['normalized_mobile'])->where('draft', false)->first();
                                        $userRefAvailable = User::where('id', $userByMobile->id)->where('draft', false)->whereHas('references', function($q) {
                                                    $q->where('number', 'like', "%AJIB-%");
                                                })->count();
                                        if ($userRefAvailable == 0) {
                                            DB::table('reference')->insert(array(
                                                'number' => $row[0],
                                                'partner_id' => $partner_id,
                                                'user_id' => $userByMobile->id,
                                                'created_at' => date('Y-m-d H:i:s', time()),
                                                'updated_at' => date('Y-m-d H:i:s', time())
                                            ));
                                            DB::table('partner_user')->insert(array(
                                                'partner_id' => $partner_id,
                                                'user_id' => $userByMobile->id
                                            ));
                                            DB::table('partner_user')->insert(array(
                                                'partner_id' => 1,
                                                'user_id' => $last_id
                                            ));
                                            $errBool = False;
                                        } elseif (isset($reference)) {
                                            $user1 = $reference->user_id;
                                            unset($user['id']);
                                            unset($user['passcode']);
                                            //AF 10-12-17:Added this to update the mobile only
                                            unset($user['first_name']);
                                            unset($user['last_name']);
                                            unset($user['email']);
                                            unset($user['dob']);
                                            unset($user['gender']);
                                            unset($user['marital_status']);
                                            unset($user['status']);
                                            //DB::table('partner_user')->insert(array(
                                            //    'partner_id' => $partner_id,
                                            //    'user_id' => $user1
                                            //));
                                            $user_mobile['mobile'] = $user['mobile'];
                                            $user_mobile['normalized_mobile'] = $user['normalized_mobile'];

                                            User::where('id', $user1)->update($user_mobile);
                                            $errBool = False;
                                        } else {
                                            $tomobile = str_replace('AJIB-', '', $row[0]);
                                            $mobile = $partner_id . $tomobile;
                                            $user['mobile'] = $mobile;
                                            $user['normalized_mobile'] = '+962' . $mobile;
                                            $user['telephone_code'] = '+962';
                                            $last_id = DB::table('users')->insertGetId($user);
                                            DB::table('reference')->insert(array(
                                                'number' => $row[0],
                                                'partner_id' => $partner_id,
                                                'user_id' => $last_id,
                                                'created_at' => date('Y-m-d H:i:s', time()),
                                                'updated_at' => date('Y-m-d H:i:s', time())
                                            ));
                                            DB::table('role_user')->insert(array(
                                                'role_id' => 4,
                                                'user_id' => $last_id
                                            ));
                                            DB::table('partner_user')->insert(array(
                                                'partner_id' => $partner_id,
                                                'user_id' => $last_id
                                            ));
                                            DB::table('partner_user')->insert(array(
                                                'partner_id' => 1,
                                                'user_id' => $last_id
                                            ));
                                            DB::table('segment_user')->insert(array(
                                                'segment_id' => Partner::find($partner_id)->segments->first()->id,
                                                'user_id' => $last_id
                                            ));
                                            //Default Tier affiliation to Member
                                            $defaultTier = Tier::where('partner_id', $partner_id)->where('name', 'like', 'Default Tier%')->first();
                                            if (!empty($defaultTier)) {
                                                $defaultTierCriteria = Tier::where('tier_id', $defaultTier->id)->where('name', 'like', 'Default Tier Criteria%')->first();
                                                if (!empty($defaultTierCriteria)) {
                                                    DB::table('tiercriteria_user')->insert(array(
                                                        'tiercriteria_id' => $defaultTierCriteria->id,
                                                        'user_id' => $last_id
                                                    ));
                                                }
                                            }
                                            $errBool = False;
                                            $counterrors = $counterrors + 1;
                                            $errors[] = $row;
                                        }
                                    }
                                }
                                if ($errBool == True) {
                                    $counterrors = $counterrors + 1;
                                    $errors[] = $row;
                                }
                            }
                        }
                    }
                    $count++;
                }

                return (count($errors) > 0) ?
                        $this->customerImportFailed($errors, $generatedFileBaseName, 'AJIB Bank') : $this->customerImportSucceed('AJIB Bank', $generatedFileBaseName);
            }//END of AJIB Customer Import
            elseif (($ext == 'TXT' || $ext == 'txt') && strstr($name1, 'AJIBChanges') && strcmp($this->user->api_key, "686c43270a8dae1d65a448e3c228f718") == 0) {
                AuditHelper::record(
                        4, $auth_user->name . " started uploading file: " . $generatedFileName
                );

                $path = $this->changesImportPatch();
                $file->move($path, $generatedFileName);
                $inputFile = $path . '/' . $generatedFileName;

                $tx = array();

                $contents = File::get($inputFile);
                $contents = nl2br($contents, FALSE);
                $contentLines = explode("<br>", $contents);

                $arrContent = array();
                foreach ($contentLines as $lineread) {
                    $arrContent[] = explode("|", $lineread);
                }
                $count = 0;
                $counterrors = 0;
                $errBool = True;
                $errors = array();
                $partner_id = $this->partner->id;
                foreach ($arrContent as $row) {
                    if ($count != 0) {
                        if (trim($row[0]) != '' && trim($row[0]) !== 'EOF') {
                            $ref_id = trim($row[0]);
                            $mobile_number = trim($row[1]);
                            $ref = Reference::where('number', $ref_id)->first();
                            if (isset($ref->user_id)) {
                                $user = User::find($ref->user_id);
                                $allow_mobile_change = true;
                                foreach ($user->partners as $part) {
                                    if ($part->id != 1 && $part->id != $partner_id) {
                                        $allow_mobile_change = false;
                                        break;
                                    }
                                }
                                if ($allow_mobile_change === true) {
                                    $countc = InputHelper::isPhone($mobile_number);
                                    $pos = strpos($mobile_number, $countc);
                                    if ($pos !== false) {
                                        $mobile = substr_replace($mobile_number, "", $pos, strlen($countc));
                                    } else {
                                        $mobile = str_replace($countc, "", $mobile_number);
                                    }
                                    $user->mobile = $mobile;
                                    $user->normalized_mobile = '+' . $countc . $mobile;
                                    $user->telephone_code = '+' . $countc;
                                    $user->save();
                                } else {
                                    $counterrors++;
                                    $row['error'] = "Mobile can't be changed for this user";
                                    $errors[] = $row;
                                    break;
                                }
                            }
                        }
                    }
                    $count++;
                }
                if ($counterrors > 0) {
                    return self::respondWith(
                                    400, 'NOT OK', $errors, 'errors'
                    );
                } else {
                    return self::respondWith(
                                    200, 'OK', $errors, 'errors'
                    );
                }
            } elseif (($ext == 'TXT' || $ext == 'txt') && strstr($name1, 'BMLCustomer') && strcmp($this->user->api_key, "9f2eeb2511c0df10f25b537b62e4d095") == 0) {
                //ReferenceID|Title|FirstName|LastName|Email|Mobile|DOB|Gender|MaritalStatus|Occupation|Status
                //0             1   2           3       4       5    6     7        8           9         10
                AuditHelper::record(
                        4, $auth_user->name . " started uploading file: " . $generatedFileName
                );

                $path = $this->customerImportPatch();
                $file->move($path, $generatedFileName);
                $inputFile = $path . '/' . $generatedFileName;

                $tx = array();
                $partner_id = $this->partner->id;
                $contents = File::get($inputFile);
                $contents = nl2br($contents, FALSE);
                $contentLines = explode("<br>", $contents);

                $arrContent = array();
                foreach ($contentLines as $lineread) {
                    $arrContent[] = explode("|", $lineread);
                }
                $count = 0;
                $counterrors = 0;
                $errBool = True;
                $errors = array();
                foreach ($arrContent as $row) {
                    if ($count != 0) {
                        if (trim($row[0]) != '' && trim($row[0]) !== 'EOF') {
                            $row[0] = trim($row[0]);
                            $errBool = True;
//                            $fullname = explode(' ', $row[2]);
//                            $fname = $fullname[0];
//                            $fullname[0]='';
//                            $lname = implode(' ', $fullname);
                            $user = array();
                            $user['id'] = 'NULL';
                            $user['first_name'] = $row[2];
                            $user['last_name'] = $row[3];
                            if (empty($row[3])) {
                                $user['last_name'] = "Empty";
                            }
                            $mobile = $row[5];
                            $mobile = ltrim($mobile, '0');
                            $tomobile = str_replace('BML-', '', $row[0]);
                            //echo $mobile . "---<br>";
                            $user_mobile_check = User::where("mobile", $mobile)->count();
                            //echo $mobile . "---<br>";
                            if (!is_numeric($mobile) || $user_mobile_check > 0) {
                                $mobile = $partner_id . $tomobile;
                                $user['mobile'] = $mobile;
                                $user['normalized_mobile'] = '+961' . $mobile;
                                $user['telephone_code'] = '+961';
                                $counterrors = $counterrors + 1;
                                $row['error'] = "Mobile already exists or not provided";
                                $errors[] = $row;
                            } elseif (!is_numeric($mobile) || strlen($mobile) <= 8) {
                                $mobile = $partner_id . $tomobile;
                                $user['mobile'] = $mobile;
                                $user['normalized_mobile'] = '+961' . $mobile;
                                $user['telephone_code'] = '+961';
                            } elseif (InputHelper::isPhone($mobile)) {
                                $countc = InputHelper::isPhone($mobile);
                                $pos = strpos($mobile, $countc);
                                if ($pos !== false) {
                                    $mobile = substr_replace($mobile, "", $pos, strlen($countc));
                                }
//                                $mobile = str_replace($countc,"",$mobile);
                                $user['mobile'] = $mobile;
                                $user['normalized_mobile'] = '+' . $countc . $mobile;
                                $user['telephone_code'] = '+' . $countc;
                            } else {
                                $user['mobile'] = "";
                                $user['normalized_mobile'] = '';
                                $user['telephone_code'] = '';
                            }

                            //echo "****" . InputHelper::isPhone($mobile) . "+++<br>" . $mobile . "====<br>";
                            $email1 = trim($row[4]);
                            $email3 = '';
                            if ($email1 == '' || !InputHelper::isEmail($email1)) {
                                $email3 = $row[0] . '@BMLCust.com';
                            } else {
                                $email3 = $email1;
                            }

                            $user['email'] = $email3;

                            $unhandledDate = $row[6];
                            if ($unhandledDate == '' || $unhandledDate == ' ') {
                                $unhandledDate = "01/01/1902";
                            }

                            $arrunhandledDate = explode("/", $unhandledDate);
                            $date_unhandled = $arrunhandledDate[1] . "/" . $arrunhandledDate[0] . "/" . $arrunhandledDate[2];

                            $date = strtotime($date_unhandled);
                            $new_date = date('Y-m-d H:i:s', $date);
                            $user['dob'] = $new_date;

                            $user['balance'] = 0;
                            $user['marital_status'] = $row[8];
                            $user['confirm_passcode'] = false;
                            $user['passcode'] = PinCode::newCode(); //PinCode::newCode();
                            $user['title'] = $row[1];
                            $user['draft'] = false;
                            $user['verified'] = 1;
                            $user['status'] = 'inactive';
//                            if($row[10] == 'D'){
//                                $user['status'] = "inactive";
//                            }
//                            elseif($row[10]== 'C'){
//                                $user['status'] = "suspended";
//                            }

                            $user['created_at'] = date("Y-m-d H:i:s");
                            $user['updated_at'] = date("Y-m-d H:i:s");
                            $user['ucid'] = UCID::newCustomerID();
                            $user['country_id'] = 6;
                            //$user['password']       = Hash::make($row[22]);
                            // Validate gender
                            switch ($row[7]) {
                                case "M":
                                    $user['gender'] = 'm';
                                    break;
                                case "F":
                                    $user['gender'] = 'f';
                                    break;
                                default:
                                    $user['gender'] = 'not selected';
                            }

                            // Validate balance
                            if ($user['balance'] == null) {
                                $user['balance'] = 0;
                            }

                            // Validate dob
                            if ($user['dob'] == null) {
                                $user['dob'] = date("Y-m-d", strtotime("1 January 1902"));
                            }

                            //set the registration source POS
                            $storeDetails = Store::where('partner_id', 4245)->where('name', 'Registration through Customer File')->first();
                            $posDetails = Pos::where('store_id', $storeDetails->id)->where('name', 'Registration through Customer File POS')->first();
                            $user['registration_source_pos'] = $posDetails->id;

                            $validator = Validator::make($user, array(
                                        'first_name' => 'required',
                                        'last_name' => 'required',
                                        'email' => 'required|email|unique:users',
                                        'mobile' => 'required|numeric',
                                        'dob' => 'required',
                                        'balance' => 'required|numeric'
                            ));

                            $skipstatus = array();
                            $skipstatus[] = 'inactive';
                            $skipstatus[] = 'closed';
                            $countmobileunique = DB::table('users')->where('normalized_mobile', $user['normalized_mobile'])->whereNotIn('status', $skipstatus)->count();

                            $count = DB::table('users')->where('email', $user['email'])->where('draft', false)->count();
                            $countmobile = DB::table('users')->where('normalized_mobile', $user['normalized_mobile'])->count();


                            $reference = Reference::where('number', $row[0])->first();
                            if (!$validator->fails() && $countmobileunique <= 0) {

                                if (!isset($reference)) {
                                    // Create the user
                                    $last_id = DB::table('users')->insertGetId($user);
                                    //$last_id = DB::table('users')->getPdo()->lastInsertId();
                                    // Assign the role
                                    DB::table('role_user')->insert(array(
                                        'role_id' => 4,
                                        'user_id' => $last_id
                                    ));
                                    DB::table('reference')->insert(array(
                                        'number' => $row[0],
                                        'partner_id' => 4245,
                                        'user_id' => $last_id,
                                        'created_at' => date('Y-m-d H:i:s', time()),
                                        'updated_at' => date('Y-m-d H:i:s', time())
                                    ));
                                    DB::table('partner_user')->insert(array(
                                        'partner_id' => 4245,
                                        'user_id' => $last_id
                                    ));
                                    DB::table('partner_user')->insert(array(
                                        'partner_id' => 1,
                                        'user_id' => $last_id
                                    ));
                                    DB::table('segment_user')->insert(array(
                                        'segment_id' => Partner::find(4245)->segments->first()->id,
                                        'user_id' => $last_id
                                    ));

                                    //create default balance
                                    $createdUser = User::find($last_id);
                                    $createdUser->updateBalance(0, $this->partner->firstNetwork()->id, 'add');
                                    //Default Tier affiliation to Member
                                    $defaultTier = Tier::where('partner_id', 4245)->where('name', 'like', 'Default Tier%')->first();
                                    if (!empty($defaultTier)) {
                                        $defaultTierCriteria = Tier::where('id', $defaultTier->id)->where('name', 'like', 'Default Tier Criteria%')->first();
                                        if (!empty($defaultTierCriteria)) {
                                            DB::table('tiercriteria_user')->insert(array(
                                                'tiercriteria_id' => $defaultTierCriteria->id,
                                                'user_id' => $last_id
                                            ));
                                        }
                                    }

                                    $userSMS = User::find($last_id);
                                    $custRef = $row[0];
                                    $template = View::make('sms.new-user-bml', array('ref' => $custRef, 'user' => $userSMS))->render();
                                    $partner = Partner::find('4245');
                                    SMSHelper::send($template, $userSMS, $partner);
                                    //$userSMS->sendWelcomeMessageNew('4245');
//                                    $userSMS->sendAccountPin();

                                    $errBool = False;
                                    //DB::table('transaction')->insert($tx);
                                } elseif (isset($reference)) {

                                    $user1 = $reference->user_id;
//
//                                    $userChange = array();
//                                    if($row[10] == 'A'){
//                                        $userChange['status'] = 'active';
//                                    }
//                                    elseif($row[10] == 'D'){
//                                        $userChange['status'] = "inactive";
//                                    }
//                                    elseif($row[10]== 'C'){
//                                        $userChange['status'] = "suspended";
//                                    }
//                                    User::where('id',$user)->update($userChange);
//                                    $errBool = False;
                                    unset($user['id']);
                                    unset($user['passcode']);
                                    DB::table('partner_user')->insert(array(
                                        'partner_id' => 4245,
                                        'user_id' => $user1
                                    ));

                                    User::where('id', $user1)->update($user);
                                    $errBool = False;
                                }
                            } else {
                                //$reference = Reference::where('number', $row[0])->first();
                                if (isset($reference) && $countmobileunique <= 0) {

                                    $user1 = $reference->user_id;
//
//                                    $userChange = array();
//                                    if($row[10] == 'A'){
//                                        $userChange['status'] = 'active';
//                                    }
//                                    elseif($row[10] == 'D'){
//                                        $userChange['status'] = "inactive";
//                                    }
//                                    elseif($row[10]== 'C'){
//                                        $userChange['status'] = "suspended";
//                                    }
//                                    User::where('id',$user)->update($userChange);
//                                    $errBool = False;
                                    unset($user['id']);
                                    unset($user['passcode']);
                                    DB::table('partner_user')->insert(array(
                                        'partner_id' => 4245,
                                        'user_id' => $user1
                                    ));
                                    User::where('id', $user1)->update($user);
                                    $errBool = False;
                                } else {
                                    if ($countmobileunique <= 0) {
                                        $count = DB::table('users')->where('email', $user['email'])->where('draft', false)->count();
                                        $countmobile = DB::table('users')->where('normalized_mobile', $user['normalized_mobile'])->where('draft', false)->count();

                                        if ($count == 0 && empty($user['email'])) {
                                            $email3 = $row[0] . '@BMLCust.com';
                                            $user['email'] = $email3;
                                            $validator = Validator::make($user, array(
                                                        'first_name' => 'required',
                                                        'last_name' => 'required',
                                                        'email' => 'required|email|unique:users',
                                                        'mobile' => 'required|numeric',
                                                        'dob' => 'required',
                                                        'balance' => 'required|numeric'
                                            ));
                                            if (!$validator->fails()) {
                                                $last_id = DB::table('users')->insertGetId($user);
                                                //$last_id = DB::table('users')->getPdo()->lastInsertId();
                                                // Assign the role
                                                DB::table('role_user')->insert(array(
                                                    'role_id' => 4,
                                                    'user_id' => $last_id
                                                ));
                                                DB::table('reference')->insert(array(
                                                    'number' => $row[0],
                                                    'partner_id' => 4245,
                                                    'user_id' => $last_id,
                                                    'created_at' => date('Y-m-d H:i:s', time()),
                                                    'updated_at' => date('Y-m-d H:i:s', time())
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => 4245,
                                                    'user_id' => $last_id
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => 1,
                                                    'user_id' => $last_id
                                                ));
                                                DB::table('segment_user')->insert(array(
                                                    'segment_id' => Partner::find(4245)->segments->first()->id,
                                                    'user_id' => $last_id
                                                ));

                                                //Default Tier affiliation to Member
                                                $defaultTier = Tier::where('partner_id', 4245)->where('name', 'like', 'Default Tier%')->first();
                                                if (!empty($defaultTier)) {
                                                    $defaultTierCriteria = Tier::where('id', $defaultTier->id)->where('name', 'like', 'Default Tier Criteria%')->first();
                                                    if (!empty($defaultTierCriteria)) {
                                                        DB::table('tiercriteria_user')->insert(array(
                                                            'tiercriteria_id' => $defaultTierCriteria->id,
                                                            'user_id' => $last_id
                                                        ));
                                                    }
                                                }

                                                //$userSMS = User::find($last_id);
                                                //$userSMS->sendWelcomeMessage();
//                                                $userSMS->sendAccountPin();
                                                $errBool = False;
                                            } elseif ($validator->fails()) {
                                                if ($countmobile == 0 || empty($user['normalized_mobile'])) {
                                                    $mobile = "4245" . $tomobile;
                                                    $user['mobile'] = $mobile;
                                                    $user['normalized_mobile'] = '+973' . $mobile;
                                                    $user['telephone_code'] = '+973';
                                                    $validator = Validator::make($user, array(
                                                                'first_name' => 'required',
                                                                'last_name' => 'required',
                                                                'email' => 'required|email|unique:users',
                                                                'mobile' => 'required|numeric',
                                                                'dob' => 'required',
                                                                'balance' => 'required|numeric'
                                                    ));
                                                    if (!$validator->fails()) {
                                                        $last_id = DB::table('users')->insertGetId($user);
                                                        //$last_id = DB::table('users')->getPdo()->lastInsertId();
                                                        // Assign the role
                                                        DB::table('role_user')->insert(array(
                                                            'role_id' => 4,
                                                            'user_id' => $last_id
                                                        ));
                                                        DB::table('reference')->insert(array(
                                                            'number' => $row[0],
                                                            'partner_id' => 4245,
                                                            'user_id' => $last_id,
                                                            'created_at' => date('Y-m-d H:i:s', time()),
                                                            'updated_at' => date('Y-m-d H:i:s', time())
                                                        ));
                                                        DB::table('partner_user')->insert(array(
                                                            'partner_id' => 4245,
                                                            'user_id' => $last_id
                                                        ));
                                                        DB::table('partner_user')->insert(array(
                                                            'partner_id' => 1,
                                                            'user_id' => $last_id
                                                        ));
                                                        DB::table('segment_user')->insert(array(
                                                            'segment_id' => Partner::find(4245)->segments->first()->id,
                                                            'user_id' => $last_id
                                                        ));

                                                        //Default Tier affiliation to Member
                                                        $defaultTier = Tier::where('partner_id', 4245)->where('name', 'like', 'Default Tier%')->first();
                                                        if (!empty($defaultTier)) {
                                                            $defaultTierCriteria = Tier::where('id', $defaultTier->id)->where('name', 'like', 'Default Tier Criteria%')->first();
                                                            if (!empty($defaultTierCriteria)) {
                                                                DB::table('tiercriteria_user')->insert(array(
                                                                    'tiercriteria_id' => $defaultTierCriteria->id,
                                                                    'user_id' => $last_id
                                                                ));
                                                            }
                                                        }
                                                        //$userSMS = User::find($last_id);
                                                        //$userSMS->sendWelcomeMessage();
//                                                        $userSMS->sendAccountPin();
                                                        $errBool = False;
                                                    }
                                                }
                                            }
                                        } elseif ($count > 0) {

                                            $userByEmail = DB::table('users')->where('email', $user['email'])->where('draft', false)->first();
                                            $userRefAvailable = User::where('id', $userByEmail->id)->where('draft', false)->whereHas('references', function($q) {
                                                        $q->where('number', 'like', "%BML-%");
                                                    })->count();
                                            if ($userRefAvailable == 0) {
                                                DB::table('reference')->insert(array(
                                                    'number' => $row[0],
                                                    'partner_id' => 4245,
                                                    'user_id' => $userByEmail->id,
                                                    'created_at' => date('Y-m-d H:i:s', time()),
                                                    'updated_at' => date('Y-m-d H:i:s', time())
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => 4245,
                                                    'user_id' => $userByEmail->id
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => 1,
                                                    'user_id' => $last_id
                                                ));
                                                $errBool = False;
                                            } else {
                                                $email3 = $row[0] . '@BMLCust.com';
                                                $user['email'] = $email3;
                                                $last_id = DB::table('users')->insertGetId($user);
                                                DB::table('role_user')->insert(array(
                                                    'role_id' => 4,
                                                    'user_id' => $last_id
                                                ));
                                                DB::table('reference')->insert(array(
                                                    'number' => $row[0],
                                                    'partner_id' => 4245,
                                                    'user_id' => $last_id,
                                                    'created_at' => date('Y-m-d H:i:s', time()),
                                                    'updated_at' => date('Y-m-d H:i:s', time())
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => 4245,
                                                    'user_id' => $last_id
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => 1,
                                                    'user_id' => $last_id
                                                ));
                                                DB::table('segment_user')->insert(array(
                                                    'segment_id' => Partner::find(4245)->segments->first()->id,
                                                    'user_id' => $last_id
                                                ));

                                                //Default Tier affiliation to Member
                                                $defaultTier = Tier::where('partner_id', 4245)->where('name', 'like', 'Default Tier%')->first();
                                                if (!empty($defaultTier)) {
                                                    $defaultTierCriteria = Tier::where('id', $defaultTier->id)->where('name', 'like', 'Default Tier Criteria%')->first();
                                                    if (!empty($defaultTierCriteria)) {
                                                        DB::table('tiercriteria_user')->insert(array(
                                                            'tiercriteria_id' => $defaultTierCriteria->id,
                                                            'user_id' => $last_id
                                                        ));
                                                    }
                                                }
                                                $errBool = False;
                                            }
                                        } elseif ($countmobile > 0) {
                                            $userByMobile = DB::table('users')->where('normalized_mobile', $user['normalized_mobile'])->where('draft', false)->first();
                                            $userRefAvailable = User::where('id', $userByMobile->id)->where('draft', false)->whereHas('references', function($q) {
                                                        $q->where('number', 'like', "%BML-%");
                                                    })->count();
                                            if ($userRefAvailable == 0) {
                                                DB::table('reference')->insert(array(
                                                    'number' => $row[0],
                                                    'partner_id' => 4245,
                                                    'user_id' => $userByMobile->id,
                                                    'created_at' => date('Y-m-d H:i:s', time()),
                                                    'updated_at' => date('Y-m-d H:i:s', time())
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => 4245,
                                                    'user_id' => $userByMobile->id
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => 1,
                                                    'user_id' => $last_id
                                                ));
                                                $errBool = False;
                                            } else {
                                                $tomobile = str_replace('BML-', '', $row[0]);
                                                $mobile = "4245" . $tomobile;
                                                $user['mobile'] = $mobile;
                                                $user['normalized_mobile'] = '+973' . $mobile;
                                                $user['telephone_code'] = '+973';
                                                $last_id = DB::table('users')->insertGetId($user);
                                                DB::table('role_user')->insert(array(
                                                    'role_id' => 4,
                                                    'user_id' => $last_id
                                                ));
                                                DB::table('reference')->insert(array(
                                                    'number' => $row[0],
                                                    'partner_id' => 4245,
                                                    'user_id' => $last_id,
                                                    'created_at' => date('Y-m-d H:i:s', time()),
                                                    'updated_at' => date('Y-m-d H:i:s', time())
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => 4245,
                                                    'user_id' => $last_id
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => 1,
                                                    'user_id' => $last_id
                                                ));
                                                DB::table('segment_user')->insert(array(
                                                    'segment_id' => Partner::find(4245)->segments->first()->id,
                                                    'user_id' => $last_id
                                                ));
                                                //Default Tier affiliation to Member
                                                $defaultTier = Tier::where('partner_id', 4245)->where('name', 'like', 'Default Tier%')->first();
                                                if (!empty($defaultTier)) {
                                                    $defaultTierCriteria = Tier::where('id', $defaultTier->id)->where('name', 'like', 'Default Tier Criteria%')->first();
                                                    if (!empty($defaultTierCriteria)) {
                                                        DB::table('tiercriteria_user')->insert(array(
                                                            'tiercriteria_id' => $defaultTierCriteria->id,
                                                            'user_id' => $last_id
                                                        ));
                                                    }
                                                }

                                                $errBool = False;
                                            }
                                        }
                                    } else {
                                        $userByMobile = DB::table('users')->where('normalized_mobile', $user['normalized_mobile'])->where('draft', false)->first();
                                        $userRefAvailable = User::where('id', $userByMobile->id)->where('draft', false)->whereHas('references', function($q) {
                                                    $q->where('number', 'like', "%BML-%");
                                                })->count();
                                        if ($userRefAvailable == 0) {
                                            DB::table('reference')->insert(array(
                                                'number' => $row[0],
                                                'partner_id' => 4245,
                                                'user_id' => $userByMobile->id,
                                                'created_at' => date('Y-m-d H:i:s', time()),
                                                'updated_at' => date('Y-m-d H:i:s', time())
                                            ));
                                            DB::table('partner_user')->insert(array(
                                                'partner_id' => 4245,
                                                'user_id' => $userByMobile->id
                                            ));
                                            DB::table('partner_user')->insert(array(
                                                'partner_id' => 1,
                                                'user_id' => $userByMobile->id
                                            ));
                                            $errBool = False;
                                        } else {
                                            $tomobile = str_replace('BML-', '', $row[0]);
                                            $mobile = "4245" . $tomobile;
                                            $user['mobile'] = $mobile;
                                            $user['normalized_mobile'] = '+973' . $mobile;
                                            $user['telephone_code'] = '+973';
                                            $last_id = DB::table('users')->insertGetId($user);
                                            DB::table('reference')->insert(array(
                                                'number' => $row[0],
                                                'partner_id' => 4245,
                                                'user_id' => $last_id,
                                                'created_at' => date('Y-m-d H:i:s', time()),
                                                'updated_at' => date('Y-m-d H:i:s', time())
                                            ));
                                            DB::table('role_user')->insert(array(
                                                'role_id' => 4,
                                                'user_id' => $last_id
                                            ));
                                            DB::table('partner_user')->insert(array(
                                                'partner_id' => 4245,
                                                'user_id' => $last_id
                                            ));
                                            DB::table('partner_user')->insert(array(
                                                'partner_id' => 1,
                                                'user_id' => $last_id
                                            ));
                                            DB::table('segment_user')->insert(array(
                                                'segment_id' => Partner::find(4245)->segments->first()->id,
                                                'user_id' => $last_id
                                            ));
                                            //Default Tier affiliation to Member
                                            $defaultTier = Tier::where('partner_id', 4245)->where('name', 'like', 'Default Tier%')->first();
                                            if (!empty($defaultTier)) {
                                                $defaultTierCriteria = Tier::where('id', $defaultTier->id)->where('name', 'like', 'Default Tier Criteria%')->first();
                                                if (!empty($defaultTierCriteria)) {
                                                    DB::table('tiercriteria_user')->insert(array(
                                                        'tiercriteria_id' => $defaultTierCriteria->id,
                                                        'user_id' => $last_id
                                                    ));
                                                }
                                            }
                                            $errBool = False;
                                            $counterrors = $counterrors + 1;
                                            $errors[] = $row;
                                        }
                                    }
                                }
                                if ($errBool == True) {
                                    $counterrors = $counterrors + 1;
                                    $errors[] = $row;
//                                    echo "<pre>1";
//                                    var_dump($errors);
//                                    exit();
                                }
                            }
                        }
                    }
                    $count++;
                }

                return (count($errors) > 0) ?
                        $this->customerImportFailed($errors, $generatedFileBaseName, 'BML') : $this->customerImportSucceed('BML', $generatedFileBaseName);
            } elseif (($ext == 'TXT' || $ext == 'txt') && strstr($name1, 'BMLTransactions') && strcmp($this->user->api_key, "9f2eeb2511c0df10f25b537b62e4d095") == 0) {
                //Reference ID|PostingDate|Product ID|BonusPts|Billing Amount|Type|MCC|Country|Billing Currency|Transaction currency|Transaction Amount|Transaction Sign|Line Number
                //  0               1           2        3          4          5    6     7         8                   9                   10                 11          12
                $tx = array();
                $count = 0;
                $counterrors = 0;
                $errBool = True;
                $errors = array();
                $params = array();
                $passes = 0;
                //$counterrors = 0;

                AuditHelper::record(
                        4, $auth_user->name . " started uploading file: " . $generatedFileName
                );

                $path = $this->transactionImportPatch();
                $file->move($path, $generatedFileName);
                $inputFile = $path . '/' . $generatedFileName;

                $contents = File::get($inputFile);
                $contents = nl2br($contents, FALSE);
                $contentLines = explode("<br>", $contents);
                $errors = array();


                $countContentLines = count($contentLines) - 1;
                $test = $contentLines[$countContentLines];
                $test1 = $contentLines[$countContentLines - 1];
                $ret_eof_error = true;
                if (trim($test) === "EOF") {
                    $ret_eof_error = false;
                } elseif (trim($test1) === "EOF") {
                    $ret_eof_error = false;
                }
                if ($ret_eof_error == true) {
                    return self::respondWith(
                                    200, 'EOF NOT FOUND', $errors, 'errors'
                    );
                }
                $arrContent = array();
                foreach ($contentLines as $lineread) {
                    if (trim($lineread) !== 'EOF') {
                        $arrContent[] = explode("|", $lineread);
                    }
                }
                $partner = $this->partner;
                $network_id = $partner->firstNetwork()->id;
                $partner_id = $partner->id;
                $partners = $partner->managedObjects();
                $partners_children = array();
                foreach ($partners as $part) {
                    $partners_children[] = $part->id;
                }
                $loy_by_partner = RewardHelper::loyaltyByPartner($partners_children);

                $partner_prefix = $partner->prefix;
                $itemchosen = ProductPartnerReward::where('partner_id', $partner_id)->get();
                $arr_item_chosen = array();
                foreach ($itemchosen as $ic) {
                    $arr_item_chosen[$ic->partner_mapping_id] = $ic->product_id;
                }


                $transactions_file = Transaction::select('ref_number')->where('ref_number', 'like', $name1 . "%")->get();
                $arr_transactions_file = $transactions_file->toArray();
                $arr_currencies = array();
                $currencies = Currency::all();
                foreach ($currencies as $curr) {
                    $arr_currencies[$curr->short_code] = $curr->id;
                }
                if (count($arr_transactions_file) > 0) {
                    return self::respondWith(
                                    200, 'NOT OK File Already Exists', null, 'errors'
                    );
                }

                $store_res = $partner->stores->first();
                if ($store_res) {
                    $pos_id = $store_res->pointOfSales->first()->id;
                    $tx['store_id'] = $store_res->id;
                    $tx['pos_id'] = $pos_id;
                }

                foreach ($arrContent as $row) {
                    $lineNumber = 0;
                    $posted_at = date('Y-m-d H:i:s');

                    if ($count != 0) {
                        $row[0] = trim($row[0]);
                        if (trim($row[0]) != '' && trim($row[0]) != 'EOF') {

                            $unhandled_date = explode("/", $row[1]);

                            if (!isset($row[12])) {
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-001";
                                $errors[] = $row;
//                                echo $row[0] . "001";
//                                var_dump($row);
//                                exit();
                                continue;
                            } elseif (substr(trim($row[0]), 0, 4) !== "BML-" || trim($row[0]) === "" || $row[0] === " ") {
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-002";
                                $errors[] = $row;
//                                echo $row[0] . "002";
//                                var_dump($row);
//                                exit();
                                continue;
                            }///self::respondwith('400', ,null);
                            elseif ($row[1] === "" || $row[1] === " " || !checkdate($unhandled_date[1], $unhandled_date[0], $unhandled_date[2])) {
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-003";
                                $errors[] = $row;
//                                echo $row[0] . "003";
//                                var_dump($row);
//                                exit();
                                continue;
                            }
//                            elseif($row[2] === "" || $row[2] === " " || !is_numeric($row[2])){
//                                $errBool = true;
//                                $counterrors = $counterrors + 1;
//                                $row['errornum'] = $counterrors . "-004";
//                                $errors[] = $row;
////                                echo $row[0] . "004";
////                                var_dump($row);
////                                exit();
//                                // continue;
//                            }
                            elseif ($row[3] === "" || $row[3] === " " || is_numeric($row[3]) === FALSE) {
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-005";
                                $errors[] = $row;
//                                echo $row[0] . "005";
//                                var_dump($row);
//                                exit();
                                continue;
                            } elseif ($row[4] === "" || $row[4] === " " || !is_numeric($row[4])) {
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-006";
                                $errors[] = $row;
//                                echo $row[0] . "006";
//                                var_dump($row);
//                                exit();
                                continue;
                            } elseif ($row[10] === "" || $row[10] === " " || !is_numeric($row[10])) {
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-006.1";
                                $errors[] = $row;
//                                echo $row[0] . "007";
//                                var_dump($row);
//                                exit();
                                continue;
                            } elseif ($row[8] === "" || $row[8] === " " || strlen($row[8]) !== 3) {
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-007";
                                $errors[] = $row;
//                                echo $row[0] . "008";
//                                var_dump($row);
//                                exit();
                                continue;
                            } elseif ($row[9] === "" || $row[9] === " " || strlen($row[9]) !== 3) {
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-007.2";
                                $errors[] = $row;
//                                echo $row[0] . "009";
//                                var_dump($row);
//                                exit();
                                // continue;
                            }
//                            elseif($row[5]=== "" || $row[5] === " " || strlen($row[5])!== 2 ){
//                                $errBool = true;
//                                $counterrors = $counterrors + 1;
//                                $row['errornum'] = $counterrors . "-007.1";
//                                $errors[] = $row;
////                                echo $row[0] . "010";
////                                var_dump($row);
////                                exit();
//                                // continue;
//                            }
//                            elseif($row[6]=== "" || $row[6] === " " ){
//                                $errBool = true;
//                                $counterrors = $counterrors + 1;
//                                $row['errornum'] = $counterrors . "-008";
//                                $errors[] = $row;
////                                echo $row[0] . "011";
////                                var_dump($row);
////                                exit();
//                                // continue;
//                            }
                            elseif (trim($row[11]) !== "C" && trim($row[11]) !== "D") {
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-010";
                                $errors[] = $row;
//                                echo $row[0] . "012";
//                                var_dump($row);
//                                exit();
                                continue;
                            } elseif (!is_numeric(trim($row[12]))) {
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-009";
                                $errors[] = $row;
//                                echo $row[0] . "012";
//                                var_dump($row);
//                                exit();
                                continue;
                            }

                            if (isset($row[12])) {
                                $lineNumber = $row[12];
                            }
                            $ref_number = $name1 . $lineNumber;
                            $ref_number = rtrim($ref_number);
//                                echo "<pre>";
//                                echo $name1 . $lineNumber . "<br>";
//                                var_dump($row);
//                                var_dump($errors);
//                                exit();
//                            $transact = Transaction::where('ref_number',$ref_number)->first();

                            if (array_search($ref_number, array_column($arr_transactions_file, 'ref_number')) === FALSE) {
                                $coefficient = 1;
                                if ($row[11] == 'C') {
                                    $coefficient = intval(-1);
                                }
                                $row[1] = trim($row[1]);
                                $tx['id'] = 'NULL';
                                $tx['ref_number'] = $ref_number;

                                $tx['original_total_amount'] = $row[10] * $coefficient;
                                $tx['partner_amount'] = $row[4] * $coefficient;
                                $tx['partner_trxtype'] = trim($row[5]);
                                $tx['mcc'] = trim($row[6]);


                                $tx['user_id'] = 1;
                                $tx['points_redeemed'] = 0;
                                $tx['points_rewarded'] = 0;
                                $tx['partner_id'] = $partner->id;
                                $tx['network_id'] = $network_id;
                                $tx['trx_reward'] = 1;

                                $reference = Reference::where('number', $row[0])->first();
                                if (isset($reference->user_id)) {
                                    $user = $reference->user_id;
                                } else {
                                    $errBool = true;
                                    $counterrors = $counterrors + 1;
                                    $errors[] = $row;
                                    continue;
                                }

                                $tx['user_id'] = $user;

                                $userInfo = User::find($user);
                                if ($userInfo->status == 'closed') {
                                    $errBool = true;
                                    $counterrors = $counterrors + 1;
                                    $errors[] = $row;
                                    continue;
                                }
//                                $unhandledDate = $row[1];
//                                $arrunhandledDate = explode("/",$unhandledDate);
                                $date_unhandled = $unhandled_date[1] . "/" . $unhandled_date[0] . "/" . $unhandled_date[2];
                                //$date_unhandled = $row[1];
                                $date = strtotime($date_unhandled);
                                $new_date = date('Y-m-d H:i:s', $date);

                                $tx['created_at'] = $new_date;
                                $tx['updated_at'] = $new_date;
                                $tx['posted_at'] = $posted_at;
                                // Map Currency
//                                $loyalty = Loyalty::where('partner_id',$tx['partner_id'])->first();
                                //$itemchosen = ProductPartnerReward::where('partner_mapping_id',$partner_prefix.$row[2])->first();
                                if (isset($arr_item_chosen[$partner_prefix . $row[2]])) {
                                    $tx['product_id'] = $arr_item_chosen[$partner_prefix . $row[2]];
                                } else {
                                    $tx['product_id'] = NULL;
                                }

                                $validator = Validator::make($tx, array(
                                            //'id'         => 'required|numeric',
                                            'partner_id' => 'required|numeric',
                                            'store_id' => 'required|numeric',
                                            'user_id' => 'required|numeric'
                                ));

                                if (!$validator->fails()) {




                                    if (is_numeric($row[3])) {
                                        $txbonus = array(
                                            'partner_id' => $tx['partner_id'],
                                            'store_id' => $tx['store_id'],
                                            'pos_id' => $tx['pos_id'],
                                            'user_id' => $tx['user_id'],
                                            'ref_number' => "BML ONLINE Bonus POINTS",
                                            'total_amount' => 0,
                                            'points_rewarded' => round($row[3], 0, PHP_ROUND_HALF_UP) * $coefficient,
                                            'amt_reward' => PointsHelper::pointsToAmount(round($row[3], 0, PHP_ROUND_HALF_UP), "USD", $network_id) * $coefficient,
                                            'trx_reward' => 1,
                                            'currency_id' => 6,
                                            'exchange_rate' => 1,
                                            'country_id' => 8,
                                            'quantity' => 0,
                                            'notes' => 'Bonus Points',
                                            'trans_class' => "Amount",
                                            'network_id' => $network_id,
                                            'created_at' => $new_date,
                                            'updated_at' => $new_date,
                                            'posted_at' => $posted_at
                                        );

                                        if ($txbonus['points_rewarded'] != 0) {
                                            DB::table('transaction')->insert($txbonus);
                                            $user = User::find($txbonus['user_id']);
                                            $user->updateBalance(round($row[3], 0, PHP_ROUND_HALF_UP) * $coefficient, $txbonus['network_id'], 'add');
                                            $partner = Partner::find($txbonus['partner_id']);
                                        }
                                    }

                                    $passes++;

                                    if (is_numeric($tx['points_rewarded'])) {

                                        $params = array(
                                            'user_id' => $tx['user_id'],
                                            'partner' => $tx['partner_id'],
                                            'pos_id' => $tx['pos_id'],
                                            'store_id' => $tx['store_id'],
                                            'amount' => floatval($tx['partner_amount']),
                                            'currency' => trim($row[9]),
                                            //'notes'		=> 'Reward By Product',
                                            'type' => 'reward',
                                            'reference' => $tx['ref_number'],
                                            'network_id' => $partner->firstNetwork()->id,
                                            'product_id' => $tx['product_id'],
                                            'partner_currency' => trim($row[8]),
                                            'original_transaction_amount' => floatval($tx['original_total_amount']),
                                            'use_partner_currency' => True,
                                            'mcc' => $tx['mcc'],
                                            'partner_trxtype' => $tx['partner_trxtype'],
                                            'created_at' => $new_date,
                                            'updated_at' => $new_date,
                                            'posted_at' => $posted_at
                                        );

                                        $user = User::find($tx['user_id']);
                                        try {
                                            $result_do_reward = RewardHelper::doRewardNewLoyalty($params, 'Amount', $loy_by_partner);
                                            $loyalty_sms = Loyalty::find($result_do_reward[1]['rule_id']);
                                            $sms_body = $loyalty_sms->reward_sms;

                                            if (!empty($sms_body)) {
                                                $sms_body = str_ireplace('{title}', $user->title, $sms_body);
                                                $sms_body = str_ireplace('{first_name}', $user->first_name, $sms_body);
                                                $sms_body = str_ireplace('{last_name}', $user->last_name, $sms_body);
                                                $sms_body = str_ireplace('{balance}', $user->balance($result_do_reward[1]['network_id']), $sms_body);
                                                $sms_body = str_ireplace('{points}', $result_do_reward[1]['points_rewarded'], $sms_body);
                                                $sms_body = str_ireplace('{amount}', $result_do_reward[1]['amt_redeem'] ?? '', $sms_body);
                                                $sms_body = str_ireplace('{date}', Carbon::parse($result_do_reward[1]['created_at'])->format('Y-m-d'), $sms_body);
                                                $sms_body = str_ireplace('{time}', Carbon::parse($result_do_reward[1]['created_at'])->format('H:i:s'), $sms_body);


                                                if (isset($result_do_reward[1]['store_id'])) {
                                                    $sms_body = str_ireplace('{store}', $result_do_reward[1]['store_id'], $sms_body);
                                                } else {
                                                    $sms_body = str_ireplace('{store}', '', $sms_body);
                                                }

                                                // Send the SMS

                                                $outputRalph = SMSHelper::send($sms_body, $user, $partner);
                                            }
                                        } catch (Exception $ex) {
                                            //$resp = array($ex);
                                            $json_data = json_encode(array("Exception" => $ex));
                                            APIController::postSendEmailJson($json_data);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    $count++;
                }

                return (count($errors) > 0) ?
                        $this->transactionImportFailed($errors, $generatedFileBaseName, 'BML') : $this->transactionImportSucceed('BML', $generatedFileBaseName);
            } elseif (($ext == 'TXT' || $ext == 'txt') && strstr($name1, 'BSLCustomers') && strcmp($this->user->api_key, "b3a7d0ef9f9dcb189431797bd5189d25") == 0) {

                //ReferenceID|Title|FirstName|LastName|Email|Mobile|DOB|Gender|MaritalStatus
                //      0       1        2      3       4       5    6     7        8
                AuditHelper::record(
                        4, $auth_user->name . " started uploading file: " . $generatedFileName
                );

                $path = $this->customerImportPatch();
                $file->move($path, $generatedFileName);
                $inputFile = $path . '/' . $generatedFileName;

                $tx = array();

                $contents = File::get($inputFile);
                $contents = nl2br($contents, FALSE);
                $contentLines = explode("<br>", $contents);

                $arrContent = array();
                foreach ($contentLines as $lineread) {
                    $arrContent[] = explode("|", $lineread);
                }
                $count = 0;
                $counterrors = 0;
                $errBool = True;
                $errors = array();

                $partner_id = Input::get('partner_id');
                $pos_id = Input::get('pos_id');
                $store_id = Input::get('store_id');

                // we can hardcode the partner id in a 1 place
                if (empty($partner_id)) {
                    $partner_id = 4246;
                }

                foreach ($arrContent as $row) {
                    if ($count != 0) {
                        if (trim($row[0]) != '' && trim($row[0]) !== 'EOF') {
                            $row[0] = trim($row[0]);
                            $errBool = True;

                            $user = array();
                            $user['id'] = 'NULL';
                            $user['first_name'] = trim($row[2]);
                            $user['last_name'] = trim($row[3]);

                            if (empty(trim($row[3]))) {
                                $user['last_name'] = "Empty";
                            }

                            $mobile = trim($row[5]);
                            $mobile = ltrim($mobile, '0');
                            $tomobile = str_replace('BSL-', '', trim($row[0]));

                            if (!is_numeric($mobile) || strlen($mobile) <= 8) {
                                $mobile = $partner_id . $tomobile;
                                $user['mobile'] = $mobile;
                                $user['normalized_mobile'] = '+961' . $mobile;
                                $user['telephone_code'] = '+961';
                            } elseif (InputHelper::isPhone($mobile)) {
                                $countc = InputHelper::isPhone($mobile);
                                $pos = strpos($mobile, $countc);
                                if ($pos !== false) {
                                    $mobile = substr_replace($mobile, "", $pos, strlen($countc));
                                }
                                $user['mobile'] = $mobile;
                                $user['normalized_mobile'] = '+' . $countc . $mobile;
                                $user['telephone_code'] = '+' . $countc;
                            } else {
                                $user['mobile'] = "";
                                $user['normalized_mobile'] = '';
                                $user['telephone_code'] = '';
                            }

                            $email1 = trim($row[4]);
                            $email3 = '';
                            if ($email1 == '' || !InputHelper::isEmail($email1)) {
                                $email3 = trim($row[0]) . '@BSLCust.com';
                            } else {
                                $email3 = $email1;
                            }
                            $user['email'] = $email3;

                            $unhandledDate = trim($row[6]);
                            $new_date = '';
                            if ($unhandledDate != '') {
                                $arrunhandledDate = explode("/", $unhandledDate);
                                $date_unhandled = $arrunhandledDate[1] . "/" . $arrunhandledDate[0] . "/" . $arrunhandledDate[2];

                                $date = strtotime($date_unhandled);
                                $new_date = date('Y-m-d H:i:s', $date);
                            }

                            $marital = (isset($row[8]) && !empty($row[8])) ? $row[8] : '';

                            $user['dob'] = $new_date;
                            $user['balance'] = 0;
                            $user['marital_status'] = $marital;
                            $user['confirm_passcode'] = false;
                            $user['passcode'] = PinCode::newCode();
                            $user['title'] = trim($row[1]);
                            $user['draft'] = false;
                            $user['verified'] = 1;
                            $user['status'] = 'inactive';
                            $user['created_at'] = date("Y-m-d H:i:s");
                            $user['updated_at'] = date("Y-m-d H:i:s");
                            $user['ucid'] = UCID::newCustomerID();
                            $user['country_id'] = 6;

                            // Validate gender
                            /* switch (trim($row[7])) {
                              case "Male":
                              $user['gender'] = 'm';
                              break;
                              case "Female":
                              $user['gender'] = 'f';
                              break;
                              default:
                              $user['gender'] = 'm';
                              } */

                            $user['gender'] = trim(strtolower($row[7]));

                            // Validate balance
                            if ($user['balance'] == null) {
                                $user['balance'] = 0;
                            }

                            //set the registration source POS
                            $storeDetails = Store::where('partner_id', $partner_id)->where('name', 'Registration through Customer File')->first();
                            if ($storeDetails) {
                                $posDetails = Pos::where('store_id', $storeDetails->id)->where('name', 'Registration through Customer File POS')->first();
                                $user['registration_source_pos'] = $posDetails->id;
                            }

                            $validator = Validator::make($user, array(
                                        'first_name' => 'required',
                                        'last_name' => 'required',
                                        'email' => 'required|email|unique:users',
                                        'mobile' => 'required|numeric',
                                        'balance' => 'required|numeric'
                            ));

                            $skipstatus = array();
                            $skipstatus[] = 'inactive';
                            $skipstatus[] = 'closed';

                            $countmobileunique = DB::table('users')->where('normalized_mobile', $user['normalized_mobile'])->whereNotIn('status', $skipstatus)->count();
                            $count = DB::table('users')->where('email', $user['email'])->where('draft', false)->count();
                            $countmobile = DB::table('users')->where('normalized_mobile', $user['normalized_mobile'])->count();
                            $reference = Reference::where('number', trim($row[0]))->first();

                            if (!$validator->fails() && $countmobileunique <= 0) {

                                if (!isset($reference)) {

                                    // Create the user
                                    $last_id = DB::table('users')->insertGetId($user);

                                    // Assign the role
                                    DB::table('role_user')->insert(array(
                                        'role_id' => 4,
                                        'user_id' => $last_id
                                    ));

                                    DB::table('reference')->insert(array(
                                        'number' => trim($row[0]),
                                        'partner_id' => $partner_id,
                                        'user_id' => $last_id,
                                        'created_at' => date('Y-m-d H:i:s', time()),
                                        'updated_at' => date('Y-m-d H:i:s', time())
                                    ));

                                    DB::table('partner_user')->insert(array(
                                        'partner_id' => $partner_id,
                                        'user_id' => $last_id
                                    ));
                                    DB::table('partner_user')->insert(array(
                                        'partner_id' => 1,
                                        'user_id' => $last_id
                                    ));

                                    DB::table('segment_user')->insert(array(
                                        'segment_id' => Partner::find($partner_id)->segments->first()->id,
                                        'user_id' => $last_id
                                    ));

                                    //create default balance
                                    $createdUser = User::find($last_id);
                                    $createdUser->updateBalance(0, $this->partner->firstNetwork()->id, 'add');

                                    //Default Tier affiliation to Member
                                    $defaultTier = Tier::where('partner_id', $partner_id)->where('name', 'like', 'Default Tier%')->first();
                                    if (!empty($defaultTier)) {

                                        $defaultTierCriteria = Tier::where('id', $defaultTier->id)->where('name', 'like', 'Default Tier Criteria%')->first();
                                        if (!empty($defaultTierCriteria)) {
                                            DB::table('tiercriteria_user')->insert(array(
                                                'tiercriteria_id' => $defaultTierCriteria->id,
                                                'user_id' => $last_id
                                            ));
                                        }
                                    }

                                    $userSMS = User::find($last_id);
                                    $custRef = trim($row[0]);
                                    $template = View::make('sms.new-user-bsl', array('ref' => $custRef, 'user' => $userSMS))->render();
                                    $partner = Partner::find($partner_id);
                                    //SMSHelper::send($template, $userSMS, $partner);

                                    $userSMS->sendWelcomeMessageNew($partner_id);
//                                    $userSMS->sendAccountPin();

                                    $errBool = False;
                                } elseif (isset($reference)) {

                                    $user1 = $reference->user_id;
                                    unset($user['id']);
                                    unset($user['passcode']);

                                    DB::table('partner_user')->insert(array(
                                        'partner_id' => $partner_id,
                                        'user_id' => $user1
                                    ));

                                    User::where('id', $user1)->update($user);
                                    $errBool = False;
                                }
                            } else {

                                if (isset($reference) && $countmobileunique <= 0) {
                                    $user1 = $reference->user_id;
                                    unset($user['id']);
                                    unset($user['passcode']);

                                    DB::table('partner_user')->insert(array(
                                        'partner_id' => $partner_id,
                                        'user_id' => $user1
                                    ));

                                    User::where('id', $user1)->update($user);
                                    $errBool = False;
                                } else {

                                    if ($countmobileunique <= 0) {
                                        $count = DB::table('users')->where('email', $user['email'])->where('draft', false)->count();
                                        $countmobile = DB::table('users')->where('normalized_mobile', $user['normalized_mobile'])->where('draft', false)->count();

                                        if ($count == 0 && empty($user['email'])) {
                                            $email3 = trim($row[0]) . '@BSLCust.com';
                                            $user['email'] = $email3;
                                            $validator = Validator::make($user, array(
                                                        'first_name' => 'required',
                                                        'last_name' => 'required',
                                                        'email' => 'required|email|unique:users',
                                                        'mobile' => 'required|numeric',
                                                        'balance' => 'required|numeric'
                                            ));

                                            if (!$validator->fails()) {
                                                $last_id = DB::table('users')->insertGetId($user);

                                                // Assign the role
                                                DB::table('role_user')->insert(array(
                                                    'role_id' => 4,
                                                    'user_id' => $last_id
                                                ));
                                                DB::table('reference')->insert(array(
                                                    'number' => trim($row[0]),
                                                    'partner_id' => $partner_id,
                                                    'user_id' => $last_id,
                                                    'created_at' => date('Y-m-d H:i:s', time()),
                                                    'updated_at' => date('Y-m-d H:i:s', time())
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => $partner_id,
                                                    'user_id' => $last_id
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => 1,
                                                    'user_id' => $last_id
                                                ));
                                                DB::table('segment_user')->insert(array(
                                                    'segment_id' => Partner::find($partner_id)->segments->first()->id,
                                                    'user_id' => $last_id
                                                ));

                                                //Default Tier affiliation to Member
                                                $defaultTier = Tier::where('partner_id', $partner_id)->where('name', 'like', 'Default Tier%')->first();
                                                if (!empty($defaultTier)) {
                                                    $defaultTierCriteria = Tier::where('id', $defaultTier->id)->where('name', 'like', 'Default Tier Criteria%')->first();
                                                    if (!empty($defaultTierCriteria)) {
                                                        DB::table('tiercriteria_user')->insert(array(
                                                            'tiercriteria_id' => $defaultTierCriteria->id,
                                                            'user_id' => $last_id
                                                        ));
                                                    }
                                                }

                                                $userSMS = User::find($last_id);
                                                $userSMS->sendWelcomeMessageNew($partner_id);
//                                                $userSMS->sendAccountPin();

                                                $errBool = False;
                                            } elseif ($validator->fails()) {

                                                if ($countmobile == 0 || empty($user['normalized_mobile'])) {
//                                                    $mobile = "4361". $tomobile;
                                                    $mobile = $partner_id . $tomobile;
                                                    $user['mobile'] = $mobile;
                                                    $user['normalized_mobile'] = '+961' . $mobile;
                                                    $user['telephone_code'] = '+961';

                                                    $validator = Validator::make($user, array(
                                                                'first_name' => 'required',
                                                                'last_name' => 'required',
                                                                'email' => 'required|email|unique:users',
                                                                'mobile' => 'required|numeric',
                                                                'balance' => 'required|numeric'
                                                    ));

                                                    if (!$validator->fails()) {
                                                        $last_id = DB::table('users')->insertGetId($user);

                                                        // Assign the role
                                                        DB::table('role_user')->insert(array(
                                                            'role_id' => 4,
                                                            'user_id' => $last_id
                                                        ));
                                                        DB::table('reference')->insert(array(
                                                            'number' => trim($row[0]),
                                                            'partner_id' => $partner_id,
                                                            'user_id' => $last_id,
                                                            'created_at' => date('Y-m-d H:i:s', time()),
                                                            'updated_at' => date('Y-m-d H:i:s', time())
                                                        ));
                                                        DB::table('partner_user')->insert(array(
                                                            'partner_id' => $partner_id,
                                                            'user_id' => $last_id
                                                        ));
                                                        DB::table('partner_user')->insert(array(
                                                            'partner_id' => 1,
                                                            'user_id' => $last_id
                                                        ));
                                                        DB::table('segment_user')->insert(array(
                                                            'segment_id' => Partner::find($partner_id)->segments->first()->id,
                                                            'user_id' => $last_id
                                                        ));

                                                        //Default Tier affiliation to Member
                                                        $defaultTier = Tier::where('partner_id', $partner_id)->where('name', 'like', 'Default Tier%')->first();
                                                        if (!empty($defaultTier)) {
                                                            $defaultTierCriteria = Tier::where('id', $defaultTier->id)->where('name', 'like', 'Default Tier Criteria%')->first();
                                                            if (!empty($defaultTierCriteria)) {
                                                                DB::table('tiercriteria_user')->insert(array(
                                                                    'tiercriteria_id' => $defaultTierCriteria->id,
                                                                    'user_id' => $last_id
                                                                ));
                                                            }
                                                        }

                                                        $userSMS = User::find($last_id);
                                                        $userSMS->sendWelcomeMessageNew($partner_id);
//                                                        $userSMS->sendAccountPin();

                                                        $errBool = False;
                                                    }
                                                }
                                            }
                                        } elseif ($count > 0) {

                                            $userByEmail = DB::table('users')->where('email', $user['email'])->where('draft', false)->first();
                                            $userRefAvailable = User::where('id', $userByEmail->id)->where('draft', false)->whereHas('references', function($q) {
                                                        $q->where('number', 'like', "%BSL-%");
                                                    })->count();

                                            if ($userRefAvailable == 0) {
                                                DB::table('reference')->insert(array(
                                                    'number' => trim($row[0]),
                                                    'partner_id' => $partner_id,
                                                    'user_id' => $userByEmail->id,
                                                    'created_at' => date('Y-m-d H:i:s', time()),
                                                    'updated_at' => date('Y-m-d H:i:s', time())
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => $partner_id,
                                                    'user_id' => $userByEmail->id
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => 1,
                                                    'user_id' => $last_id
                                                ));
                                                $errBool = False;
                                            } else {

                                                $email3 = trim($row[0]) . '@BSLCust.com';
                                                $user['email'] = $email3;
                                                $last_id = DB::table('users')->insertGetId($user);
                                                DB::table('role_user')->insert(array(
                                                    'role_id' => 4,
                                                    'user_id' => $last_id
                                                ));
                                                DB::table('reference')->insert(array(
                                                    'number' => trim($row[0]),
                                                    'partner_id' => $partner_id,
                                                    'user_id' => $last_id,
                                                    'created_at' => date('Y-m-d H:i:s', time()),
                                                    'updated_at' => date('Y-m-d H:i:s', time())
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => $partner_id,
                                                    'user_id' => $last_id
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => 1,
                                                    'user_id' => $last_id
                                                ));
                                                DB::table('segment_user')->insert(array(
                                                    'segment_id' => Partner::find($partner_id)->segments->first()->id,
                                                    'user_id' => $last_id
                                                ));

                                                //Default Tier affiliation to Member
                                                $defaultTier = Tier::where('partner_id', $partner_id)->where('name', 'like', 'Default Tier%')->first();
                                                if (!empty($defaultTier)) {
                                                    $defaultTierCriteria = Tier::where('id', $defaultTier->id)->where('name', 'like', 'Default Tier Criteria%')->first();
                                                    if (!empty($defaultTierCriteria)) {
                                                        DB::table('tiercriteria_user')->insert(array(
                                                            'tiercriteria_id' => $defaultTierCriteria->id,
                                                            'user_id' => $last_id
                                                        ));
                                                    }
                                                }
                                                $errBool = False;
                                            }
                                        } elseif ($countmobile > 0) {

                                            $userByMobile = DB::table('users')->where('normalized_mobile', $user['normalized_mobile'])->where('draft', false)->first();
                                            $userRefAvailable = User::where('id', $userByMobile->id)->where('draft', false)->whereHas('references', function($q) {
                                                        $q->where('number', 'like', "%BSL-%");
                                                    })->count();

                                            if ($userRefAvailable == 0) {
                                                DB::table('reference')->insert(array(
                                                    'number' => trim($row[0]),
                                                    'partner_id' => $partner_id,
                                                    'user_id' => $userByMobile->id,
                                                    'created_at' => date('Y-m-d H:i:s', time()),
                                                    'updated_at' => date('Y-m-d H:i:s', time())
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => $partner_id,
                                                    'user_id' => $userByMobile->id
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => 1,
                                                    'user_id' => $last_id
                                                ));
                                                $errBool = False;
                                            } else {

                                                $tomobile = str_replace('BSL-', '', trim($row[0]));
                                                $mobile = $partner_id . $tomobile;
                                                $user['mobile'] = $mobile;
                                                $user['normalized_mobile'] = '+961' . $mobile;
                                                $user['telephone_code'] = '+961';
                                                $last_id = DB::table('users')->insertGetId($user);

                                                DB::table('role_user')->insert(array(
                                                    'role_id' => 4,
                                                    'user_id' => $last_id
                                                ));
                                                DB::table('reference')->insert(array(
                                                    'number' => trim($row[0]),
                                                    'partner_id' => $partner_id,
                                                    'user_id' => $last_id,
                                                    'created_at' => date('Y-m-d H:i:s', time()),
                                                    'updated_at' => date('Y-m-d H:i:s', time())
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => $partner_id,
                                                    'user_id' => $last_id
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => 1,
                                                    'user_id' => $last_id
                                                ));
                                                DB::table('segment_user')->insert(array(
                                                    'segment_id' => Partner::find($partner_id)->segments->first()->id,
                                                    'user_id' => $last_id
                                                ));

                                                //Default Tier affiliation to Member
                                                $defaultTier = Tier::where('partner_id', $partner_id)->where('name', 'like', 'Default Tier%')->first();
                                                if (!empty($defaultTier)) {
                                                    $defaultTierCriteria = Tier::where('id', $defaultTier->id)->where('name', 'like', 'Default Tier Criteria%')->first();
                                                    if (!empty($defaultTierCriteria)) {
                                                        DB::table('tiercriteria_user')->insert(array(
                                                            'tiercriteria_id' => $defaultTierCriteria->id,
                                                            'user_id' => $last_id
                                                        ));
                                                    }
                                                }

                                                $errBool = False;
                                            }
                                        }
                                    } else {

                                        $userByMobile = DB::table('users')->where('normalized_mobile', $user['normalized_mobile'])->where('draft', false)->first();
                                        $userRefAvailable = User::where('id', $userByMobile->id)->where('draft', false)->whereHas('references', function($q) {
                                                    $q->where('number', 'like', "%BSL-%");
                                                })->count();

                                        if ($userRefAvailable == 0) {
                                            DB::table('reference')->insert(array(
                                                'number' => trim($row[0]),
                                                'partner_id' => $partner_id,
                                                'user_id' => $userByMobile->id,
                                                'created_at' => date('Y-m-d H:i:s', time()),
                                                'updated_at' => date('Y-m-d H:i:s', time())
                                            ));
                                            DB::table('partner_user')->insert(array(
                                                'partner_id' => $partner_id,
                                                'user_id' => $userByMobile->id
                                            ));
                                            DB::table('partner_user')->insert(array(
                                                'partner_id' => 1,
                                                'user_id' => $last_id
                                            ));
                                            $errBool = False;
                                        } else {

                                            $tomobile = str_replace('BSL-', '', trim($row[0]));
                                            $mobile = $partner_id . $tomobile;
                                            $user['mobile'] = $mobile;
                                            $user['normalized_mobile'] = '+961' . $mobile;
                                            $user['telephone_code'] = '+961';

                                            $last_id = DB::table('users')->insertGetId($user);
                                            DB::table('reference')->insert(array(
                                                'number' => trim($row[0]),
                                                'partner_id' => $partner_id,
                                                'user_id' => $last_id,
                                                'created_at' => date('Y-m-d H:i:s', time()),
                                                'updated_at' => date('Y-m-d H:i:s', time())
                                            ));
                                            DB::table('role_user')->insert(array(
                                                'role_id' => 4,
                                                'user_id' => $last_id
                                            ));
                                            DB::table('partner_user')->insert(array(
                                                'partner_id' => $partner_id,
                                                'user_id' => $last_id
                                            ));
                                            DB::table('partner_user')->insert(array(
                                                'partner_id' => 1,
                                                'user_id' => $last_id
                                            ));
                                            DB::table('segment_user')->insert(array(
                                                'segment_id' => Partner::find($partner_id)->segments->first()->id,
                                                'user_id' => $last_id
                                            ));

                                            //Default Tier affiliation to Member
                                            $defaultTier = Tier::where('partner_id', $partner_id)->where('name', 'like', 'Default Tier%')->first();
                                            if (!empty($defaultTier)) {
                                                $defaultTierCriteria = Tier::where('id', $defaultTier->id)->where('name', 'like', 'Default Tier Criteria%')->first();
                                                if (!empty($defaultTierCriteria)) {
                                                    DB::table('tiercriteria_user')->insert(array(
                                                        'tiercriteria_id' => $defaultTierCriteria->id,
                                                        'user_id' => $last_id
                                                    ));
                                                }
                                            }
                                            $errBool = False;
                                            $counterrors = $counterrors + 1;
                                            $errors[] = $row;
                                        }
                                    }
                                }

                                if ($errBool == True) {
                                    $counterrors = $counterrors + 1;
                                    $errors[] = $row;
                                }
                            }
                        }
                    }
                    $count++;
                }

                return (count($errors) > 0) ?
                        $this->customerImportFailed($errors, $generatedFileBaseName, 'BSL') : $this->customerImportSucceed('BSL', $generatedFileBaseName);
            } elseif (($ext == 'TXT' || $ext == 'txt') && strstr($name1, 'BSLTransactions') && strcmp($this->user->api_key, "b3a7d0ef9f9dcb189431797bd5189d25") == 0) {
                //Reference ID|Posting Dt|Prd|Bonus Pnts|Bil Amt|Merchant Description|MCC|Ctr|BCu|TCu|Trans. AMT|S |LinNO
                //      0           1      2      3         4            5             6   7   8   9      10     11  12
                $tx = array();
                $count = 0;
                $counterrors = 0;
                $errBool = True;
                $errors = array();
                $params = array();
                $passes = 0;
                //$counterrors = 0;

                AuditHelper::record(
                        4, $auth_user->name . " started uploading file: " . $generatedFileName
                );

                $path = $this->transactionImportPatch();
                $file->move($path, $generatedFileName);
                $inputFile = $path . '/' . $generatedFileName;

                $contents = File::get($inputFile);
                $contents = nl2br($contents, FALSE);
                $contentLines = explode("<br>", $contents);
                $errors = array();


                $countContentLines = count($contentLines) - 1;
                $test = $contentLines[$countContentLines];
                $test1 = $contentLines[$countContentLines - 1];
                $ret_eof_error = true;
                if (trim($test) === "EOF") {
                    $ret_eof_error = false;
                } elseif (trim($test1) === "EOF") {
                    $ret_eof_error = false;
                }
                if ($ret_eof_error == true) {
                    return self::respondWith(
                                    200, 'EOF NOT FOUND', $errors, 'errors'
                    );
                }
                $arrContent = array();
                foreach ($contentLines as $lineread) {
                    if (trim($lineread) !== 'EOF') {
                        $arrContent[] = explode("|", $lineread);
                    }
                }
                $partner = $this->partner;
                $network_id = $partner->firstNetwork()->id;
                $partner_id = $partner->id;
                $partners = $partner->managedObjects();
                $partners_children = array();
                foreach ($partners as $part) {
                    $partners_children[] = $part->id;
                }
                $loy_by_partner = RewardHelper::loyaltyByPartner($partners_children);

                $partner_prefix = $partner->prefix;
                $itemchosen = ProductPartnerReward::where('partner_id', $partner_id)->get();
                $arr_item_chosen = array();
                foreach ($itemchosen as $ic) {
                    $arr_item_chosen[$ic->partner_mapping_id] = $ic->product_id;
                }


                $transactions_file = Transaction::select('ref_number')->where('ref_number', 'like', $name1 . "%")->get();
                $arr_transactions_file = $transactions_file->toArray();
                $arr_currencies = array();
                $currencies = Currency::all();
                foreach ($currencies as $curr) {
                    $arr_currencies[$curr->short_code] = $curr->id;
                }
                if (count($arr_transactions_file) > 0) {
                    return self::respondWith(
                                    200, 'NOT OK File Already Exists', null, 'errors'
                    );
                }

                $store_res = $partner->stores->first();
                if ($store_res) {
                    $pos_id = $store_res->pointOfSales->first()->id;
                    $tx['store_id'] = $store_res->id;
                    $tx['pos_id'] = $pos_id;
                }

                foreach ($arrContent as $row) {
                    $lineNumber = 0;
                    $posted_at = date('Y-m-d H:i:s');

                    if ($count != 0) {
                        $row[0] = trim($row[0]);
                        if (trim($row[0]) != '' && trim($row[0]) != 'EOF') {

                            $row[3] = ltrim($row[3], '0');
                            $row[4] = ltrim($row[4], '0');
                            $row[6] = ltrim($row[6], '0');
                            $row[7] = ltrim($row[7], '0');
                            $row[10] = ltrim($row[10], '0');
                            $row[12] = ltrim($row[12], '0');

                            $unhandled_date = explode("/", $row[1]);

                            if (!isset($row[12])) {
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-001";
                                $errors[] = $row;
//                                echo $row[0] . "001";
//                                var_dump($row);
//                                exit();
                                continue;
                            } elseif (substr(trim($row[0]), 0, 4) !== "BSL-" || trim($row[0]) === "" || $row[0] === " ") {
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-002";
                                $errors[] = $row;
//                                echo $row[0] . "002";
//                                var_dump($row);
//                                exit();
                                continue;
                            }///self::respondwith('400', ,null);
                            elseif ($row[1] === "" || $row[1] === " " || !checkdate($unhandled_date[1], $unhandled_date[0], $unhandled_date[2])) {
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-003";
                                $errors[] = $row;
//                                echo $row[0] . "003";
//                                var_dump($row);
//                                exit();
                                continue;
                            }
//                            elseif($row[2] === "" || $row[2] === " " || !is_numeric($row[2])){
//                                $errBool = true;
//                                $counterrors = $counterrors + 1;
//                                $row['errornum'] = $counterrors . "-004";
//                                $errors[] = $row;
//                                echo $row[0] . "004";
//                                var_dump($row);
//                                exit();
//                                // continue;
//                            }
//                            elseif ($row[3] === "" || $row[3] === " " || is_numeric($row[3]) === FALSE) {
//                                $errBool = true;
//                                $counterrors = $counterrors + 1;
//                                $row['errornum'] = $counterrors . "-005";
//                                $errors[] = $row;
//                                echo $row[0] . "005";
//                                var_dump($row);
//                                exit();
//                                continue;
//                            }
                            elseif ($row[4] === "" || $row[4] === " " || !is_numeric($row[4])) {
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-006";
                                $errors[] = $row;
//                                echo $row[0] . "006";
//                                var_dump($row);
//                                exit();
                                continue;
                            } elseif ($row[10] === "" || $row[10] === " " || !is_numeric($row[10])) {
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-006.1";
                                $errors[] = $row;
//                                echo $row[0] . "007";
//                                var_dump($row);
//                                exit();
                                continue;
                            } elseif ($row[8] === "" || $row[8] === " " || strlen($row[8]) !== 3) {
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-007";
                                $errors[] = $row;
//                                echo $row[0] . "008";
//                                var_dump($row);
//                                exit();
                                continue;
                            } elseif ($row[9] === "" || $row[9] === " " || strlen($row[9]) !== 3) {
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-007.2";
                                $errors[] = $row;
//                                echo $row[0] . "009";
//                                var_dump($row);
//                                exit();
                                // continue;
                            }
//                            elseif($row[5]=== "" || $row[5] === " " || strlen($row[5])!== 2 ){
//                                $errBool = true;
//                                $counterrors = $counterrors + 1;
//                                $row['errornum'] = $counterrors . "-007.1";
//                                $errors[] = $row;
////                                echo $row[0] . "010";
////                                var_dump($row);
////                                exit();
//                                // continue;
//                            }
//                            elseif($row[6]=== "" || $row[6] === " " ){
//                                $errBool = true;
//                                $counterrors = $counterrors + 1;
//                                $row['errornum'] = $counterrors . "-008";
//                                $errors[] = $row;
////                                echo $row[0] . "011";
////                                var_dump($row);
////                                exit();
//                                // continue;
//                            }
                            elseif (trim($row[11]) !== "C" && trim($row[11]) !== "D") {
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-010";
                                $errors[] = $row;
//                                echo $row[0] . "012";
//                                var_dump($row);
//                                exit();
                                continue;
                            } elseif (!is_numeric(trim($row[12]))) {
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-009";
                                $errors[] = $row;
//                                echo $row[0] . "012";
//                                var_dump($row);
//                                exit();
                                continue;
                            }

                            if (isset($row[12])) {
                                $lineNumber = $row[12];
                            }
                            $ref_number = $name1 . $lineNumber;
                            $ref_number = rtrim($ref_number);
//                                echo "<pre>";
//                                echo $name1 . $lineNumber . "<br>";
//                                var_dump($row);
//                                var_dump($errors);
//                                exit();
//                            $transact = Transaction::where('ref_number',$ref_number)->first();

                            if (array_search($ref_number, array_column($arr_transactions_file, 'ref_number')) === FALSE) {
                                $coefficient = 1;
                                if ($row[11] == 'C') {
                                    $coefficient = intval(-1);
                                }
                                $row[1] = trim($row[1]);
                                $tx['id'] = 'NULL';
                                $tx['ref_number'] = $ref_number;
                                $arr_notes = array();
                                $arr_notes['store'] = trim($row[5]);
                                $tx['original_total_amount'] = $row[10] * $coefficient;
                                $tx['partner_amount'] = $row[4] * $coefficient;
                                $tx['notes'] = json_encode($arr_notes);
                                $tx['mcc'] = trim($row[6]);


                                $tx['user_id'] = 1;
                                $tx['points_redeemed'] = 0;
                                $tx['points_rewarded'] = 0;
                                $tx['partner_id'] = $partner->id;
                                $tx['network_id'] = $network_id;
                                $tx['trx_reward'] = 1;

                                $reference = Reference::where('number', $row[0])->first();
                                if (isset($reference->user_id)) {
                                    $user = $reference->user_id;
                                } else {
                                    $errBool = true;
                                    $counterrors = $counterrors + 1;
                                    $errors[] = $row;
                                    continue;
                                }

                                $tx['user_id'] = $user;

                                $userInfo = User::find($user);
                                if ($userInfo->status == 'closed') {
                                    $errBool = true;
                                    $counterrors = $counterrors + 1;
                                    $errors[] = $row;
                                    continue;
                                }
//                                $unhandledDate = $row[1];
//                                $arrunhandledDate = explode("/",$unhandledDate);
                                $date_unhandled = $unhandled_date[1] . "/" . $unhandled_date[0] . "/" . $unhandled_date[2];
                                //$date_unhandled = $row[1];
                                $date = strtotime($date_unhandled);
                                $new_date = date('Y-m-d H:i:s', $date);

                                $tx['created_at'] = $new_date;
                                $tx['updated_at'] = $new_date;
                                $tx['posted_at'] = $posted_at;
                                // Map Currency
//                                $loyalty = Loyalty::where('partner_id',$tx['partner_id'])->first();
                                //$itemchosen = ProductPartnerReward::where('partner_mapping_id',$partner_prefix.$row[2])->first();
                                if (isset($arr_item_chosen[$partner_prefix . $row[2]])) {
                                    $tx['product_id'] = $arr_item_chosen[$partner_prefix . $row[2]];
                                } else {
                                    $tx['product_id'] = NULL;
                                }

                                $validator = Validator::make($tx, array(
                                            //'id'         => 'required|numeric',
                                            'partner_id' => 'required|numeric',
                                            'store_id' => 'required|numeric',
                                            'user_id' => 'required|numeric'
                                ));

                                if (!$validator->fails()) {
                                    if (is_numeric($row[3])) {
                                        $txbonus = array(
                                            'partner_id' => $tx['partner_id'],
                                            'store_id' => $tx['store_id'],
                                            'pos_id' => $tx['pos_id'],
                                            'user_id' => $tx['user_id'],
                                            'ref_number' => "BSL ONLINE Bonus POINTS",
                                            'total_amount' => 0,
                                            'points_rewarded' => round($row[3], 0, PHP_ROUND_HALF_UP) * $coefficient,
                                            'amt_reward' => PointsHelper::pointsToAmount(round($row[3], 0, PHP_ROUND_HALF_UP), "USD", $network_id) * $coefficient,
                                            'trx_reward' => 1,
                                            'currency_id' => 6,
                                            'exchange_rate' => 1,
                                            'country_id' => 8,
                                            'quantity' => 0,
                                            'notes' => 'Bonus Points',
                                            'trans_class' => "Amount",
                                            'network_id' => $network_id,
                                            'created_at' => $new_date,
                                            'updated_at' => $new_date,
                                            'posted_at' => $posted_at
                                        );

                                        if ($txbonus['points_rewarded'] != 0) {
                                            DB::table('transaction')->insert($txbonus);
                                            $user = User::find($txbonus['user_id']);
                                            $user->updateBalance(round($row[3], 0, PHP_ROUND_HALF_UP) * $coefficient, $txbonus['network_id'], 'add');
                                            $partner = Partner::find($txbonus['partner_id']);
                                        }
                                    }

                                    $passes++;

                                    if (is_numeric($tx['points_rewarded'])) {

                                        $params = array(
                                            'user_id' => $tx['user_id'],
                                            'partner' => $tx['partner_id'],
                                            'pos_id' => $tx['pos_id'],
                                            'store_id' => $tx['store_id'],
                                            'amount' => floatval($tx['partner_amount']),
                                            'currency' => trim($row[9]),
                                            //'notes'		=> 'Reward By Product',
                                            'type' => 'reward',
                                            'reference' => $tx['ref_number'],
                                            'network_id' => $partner->firstNetwork()->id,
                                            'product_id' => $tx['product_id'],
                                            'partner_currency' => trim($row[8]),
                                            'original_transaction_amount' => floatval($tx['original_total_amount']),
                                            'use_partner_currency' => True,
                                            'mcc' => $tx['mcc'],
                                            'notes' => $tx['notes'],
                                            'created_at' => $new_date,
                                            'updated_at' => $new_date,
                                            'posted_at' => $posted_at
                                        );
                                        $user = User::find($tx['user_id']);
                                        try {
                                            $result_do_reward = RewardHelper::doRewardNewLoyalty($params, 'Amount', $loy_by_partner);

                                            $loyalty_sms = '';
                                            if (isset($result_do_reward[1]['rule_id']) && !empty($result_do_reward[1]['rule_id'])) {
                                                $loyalty_sms = Loyalty::find($result_do_reward[1]['rule_id']);
                                            }

                                            if ($loyalty_sms) {
                                                $sms_body = $loyalty_sms->reward_sms;

                                                if (!empty($sms_body)) {
                                                    $sms_body = str_ireplace('{title}', $user->title, $sms_body);
                                                    $sms_body = str_ireplace('{first_name}', $user->first_name, $sms_body);
                                                    $sms_body = str_ireplace('{last_name}', $user->last_name, $sms_body);
                                                    $sms_body = str_ireplace('{balance}', $user->balance($result_do_reward[1]['network_id']), $sms_body);
                                                    $sms_body = str_ireplace('{points}', $result_do_reward[1]['points_rewarded'], $sms_body);
                                                    $sms_body = str_ireplace('{amount}', $result_do_reward[1]['amt_redeem'] ?? '', $sms_body);
                                                    $sms_body = str_ireplace('{date}', Carbon::parse($result_do_reward[1]['created_at'])->format('Y-m-d'), $sms_body);
                                                    $sms_body = str_ireplace('{time}', Carbon::parse($result_do_reward[1]['created_at'])->format('H:i:s'), $sms_body);

                                                    if (isset($result_do_reward[1]['store_id'])) {
                                                        $sms_body = str_ireplace('{store}', $result_do_reward[1]['store_id'], $sms_body);
                                                    } else {
                                                        $sms_body = str_ireplace('{store}', '', $sms_body);
                                                    }

                                                    // Send the SMS
                                                    //$outputRalph = SMSHelper::send($sms_body, $user, $partner);
                                                }
                                            }
                                        } catch (Exception $ex) {
                                            //$resp = array($ex);
                                            var_dump($ex->getMessage());
                                            $json_data = json_encode(array("Exception" => $ex->getMessage()));
                                            APIController::postSendEmailJson($json_data);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    $count++;
                }

                return (count($errors) > 0) ?
                        $this->transactionImportFailed($errors, $generatedFileBaseName, 'BSL') : $this->transactionImportSucceed('BSL', $generatedFileBaseName);
            }
        }
    }

    public function getTest() {
        $test = Input::get('test'); //$cpt,$username, $pin,$site
        $url = "https://bluprd.alfransi.com.sa/BLU/api.php?method=sensitiveInfo&api_key=1be83d735510848b88a08e43c2e114d9";
        $post_fields = 'user_ids=' . $test;

        $curl = curl_init();
        // Set some options - we are passing in a useragent too here
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $post_fields,
            CURLOPT_SSL_VERIFYPEER => False,
            CURLOPT_USERAGENT => 'Testing Sensitive Info'
        ));
        // Send the request & save response to $resp
        $resp = curl_exec($curl);

        if (curl_errno($curl)) {
            var_dump(curl_error($curl));
            exit();
        }
        //curl_setopt($ch, CURLOPT_HEADER, 1);
        //var_dump($post_fields);
        echo "<pre>";
        var_dump($resp);
        exit();
    }

    /**
     * returns the balance of a user
     * in points and money
     *
     * GET /api/user-balance-by-id/{user_id}
     *
     * @param int $userId
     * @return Response
     */
    public function getUserBalanceById($userId) {
        $user = User::find($userId);

        if (!$user) {
            return self::respondWith(
                            500, "The user doesn't exist."
            );
        }

        $balance = $user->balance($this->partner->firstNetwork()->id);
        $balanceInUsd = PointsHelper::pointsToAmount($balance, 'USD');

        return self::respondWith(
                        200, null, array('balance' => $balance, 'balance_in_usd' => $balanceInUsd), 'user'
        );
    }

    /**
     * returns the balance of a user
     * in points and money
     *
     * GET /api/user-balance-by-card/{card}
     *
     * @param int $userId
     * @return Response
     */
    public function getUserBalanceByCard($card) {
//        $card = 'OM-' . $card;
//        echo $card;
        $userObj = Member::select('users.id')
                ->where('card.number', $card)
                ->where('users.draft', false)
                ->LeftJoin('card', 'users.id', '=', 'card.user_id')
                ->first();

        $user = User::find($userObj->id);

        if (!$user) {
            return self::respondWith(
                            500, "The user doesn't exist."
            );
        }

        $balance = $user->balance($this->partner->firstNetwork()->id);
        $balanceInUsd = PointsHelper::pointsToAmount($balance, 'USD');

        return self::respondWith(
                        200, null, array('balance' => $balance, 'balance_in_usd' => $balanceInUsd), 'user'
        );
    }

    /**
     * returns the balance of a user
     * in points and money
     *
     * GET /api/bsf-reports
     *
     * @param int $userId
     * @return Response
     */
    public static function getBsfReports($start_date, $end_date, $partner_id) {
        $partner = Partner::find($partner_id);
        $partners = $partner->managedObjects();
        $partners_children = array();
        $partners_children[] = $partner_id;
        foreach ($partners as $part) {
            $partners_children[] = $part->id;
        }
        $accountingFile = AutogeneratedReportController::ReportAccountingFile($start_date, $end_date, $partner_id);
        $connection = ssh2_connect('secureftp.alfransi.com.sa');
        if (ssh2_auth_password($connection, 'blusolutions', 'Za=uphE7ep!u')) {
            $sftp = ssh2_sftp($connection);
            $AccountingFilename = "./BSFAccountingFile_" . date('Ymd') . '.txt';

            if ($accountingFile !== FALSE) {
                $contentsAccounting = file_get_contents($accountingFile);
                file_put_contents("ssh2.sftp://{$sftp}/" . $AccountingFilename, $contentsAccounting);
            }
        } else {
            return self::respondWith(
                            400, "NOT OK", array('AccountingReport' => 'Unable to authenticate with server'), 'Reports'
            );
        }
//        $AllCustomers = AutogeneratedReportController::ReportAllCustomers($partner_id);
//        $connection1 = ssh2_connect('secureftp.alfransi.com.sa');
//        if(ssh2_auth_password($connection1, 'blusolutions', 'Za=uphE7ep!u')){
//            $sftp1 = ssh2_sftp($connection1);
//            $CustomersFilename = "./BSFLoyalt_CUST_". date('Ymd').'.txt';
//            $contents = file_get_contents($AllCustomers);
//            file_put_contents("ssh2.sftp://{$sftp1}/".$CustomersFilename, $contents);
//        }
//        else{
//            return self::respondWith(
//                400, "NOT OK", array('CustomerReport' => 'Unable to authenticate with server'), 'Reports'
//            );
//        }



        if ($accountingFile == TRUE) {
            $sms = new EDS(
                    "BLU KSA", 0
            );
            $number1 = "971569496232";
            $message = "Reports uploaded successfully";
            // Comma delimited mobile numbers prefixed with country code
            $number = str_replace('+', '', $number1);
            $outputralph = $sms->send($number, $message);

            return self::respondWith(
                            200, "OK", array('transactionReport' => 'Uploaded successfully'), 'Reports'
            );
        } else {
            $sms = new EDS(
                    "BLU KSA", 0
            );
            $number1 = "971569496232";
            $message = "Reports not uploaded successfully";
            // Comma delimited mobile numbers prefixed with country code
            $number = str_replace('+', '', $number1);
            $outputralph = $sms->send($number, $message);

            return self::respondWith(
                            400, "NOT OK", array('transactionReport' => 'NOT Uploaded successfully'), 'Reports'
            );
        }
    }

    public static function getBsfReportsTransactionReward($start_date, $end_date, $partner_id) {
        $partner = Partner::find($partner_id);
        $partners = $partner->managedObjects();
        $partners_children = array();
        $partners_children[] = $partner_id;
        foreach ($partners as $part) {
            $partners_children[] = $part->id;
        }

        $AllTransactionReward = AutogeneratedReportController::ReportAllTransactionsRewardRalph($start_date, $end_date, $partners_children);




        if ($AllTransactionReward == TRUE) {
            $sms = new EDS(
                    "BLU KSA", 0
            );
            $number1 = "971569496232";
            $message = "Reports uploaded successfully";
            // Comma delimited mobile numbers prefixed with country code
            $number = str_replace('+', '', $number1);
            $outputralph = $sms->send($number, $message);

            return self::respondWith(
                            200, "OK", array('transactionReport' => 'Uploaded successfully'), 'Reports'
            );
        } else {
            $sms = new EDS(
                    "BLU KSA", 0
            );
            $number1 = "971569496232";
            $message = "Reports not uploaded successfully";
            // Comma delimited mobile numbers prefixed with country code
            $number = str_replace('+', '', $number1);
            $outputralph = $sms->send($number, $message);

            return self::respondWith(
                            400, "NOT OK", array('transactionReport' => 'NOT Uploaded successfully'), 'Reports'
            );
        }
    }

    public static function getBsfReportsAccountingRalph($start_date, $end_date, $partner_id) {
        $partner = Partner::find($partner_id);
        $partners = $partner->managedObjects();
        $partners_children = array();
        $partners_children[] = $partner_id;
        foreach ($partners as $part) {
            $partners_children[] = $part->id;
        }

        $AllTransactionReward = AutogeneratedReportController::ReportAccountingFileRalph($start_date, $end_date, $partner_id);




        if ($AllTransactionReward == TRUE) {
            $sms = new EDS(
                    "BLU KSA", 0
            );
            $number1 = "971569496232";
            $message = "Reports uploaded successfully";
            // Comma delimited mobile numbers prefixed with country code
            $number = str_replace('+', '', $number1);
            $outputralph = $sms->send($number, $message);

            return self::respondWith(
                            200, "OK", array('transactionReport' => 'Uploaded successfully'), 'Reports'
            );
        } else {
            $sms = new EDS(
                    "BLU KSA", 0
            );
            $number1 = "971569496232";
            $message = "Reports not uploaded successfully";
            // Comma delimited mobile numbers prefixed with country code
            $number = str_replace('+', '', $number1);
            $outputralph = $sms->send($number, $message);

            return self::respondWith(
                            400, "NOT OK", array('transactionReport' => 'NOT Uploaded successfully'), 'Reports'
            );
        }
    }

    public static function getBsfReports2($start_date, $end_date, $partner_id) {
        $partner = Partner::find($partner_id);
        $partners = $partner->managedObjects();
        $partners_children = array();
        foreach ($partners as $part) {
            $partners_children[] = $part->id;
        }
        $AllCustomers = AutogeneratedReportController::ReportAllCustomersNew($start_date, $end_date, $partner_id);
        $connection1 = ssh2_connect('secureftp.alfransi.com.sa');
        if (ssh2_auth_password($connection1, 'blusolutions', 'Za=uphE7ep!u')) {
            $sftp1 = ssh2_sftp($connection1);
            $CustomersFilename = "./BSFLoyalt_CUST_" . date('Ymd') . '.txt';
            $contents = file_get_contents($AllCustomers);
            file_put_contents("ssh2.sftp://{$sftp1}/" . $CustomersFilename, $contents);
        } else {
            return self::respondWith(
                            400, "NOT OK", array('CustomerReport' => 'Unable to authenticate with server'), 'Reports'
            );
        }
//        $connection2 = ssh2_connect('secureftp.alfransi.com.sa');
//        if(ssh2_auth_password($connection2, 'blusolutions', 'Za=uphE7ep!u')){
//            $sftp2 = ssh2_sftp($connection2);
//            $AllTransactionRedeem = AutogeneratedReportController::ReportAllTransactionsRedemptions($start_date, $end_date, $partners_children);
//            $RedemptionFilename = "./BSFLoyalt_REDM_". date('Ymd').'.txt';
//            $contentsRedm = file_get_contents($AllTransactionRedeem);
//            file_put_contents("ssh2.sftp://{$sftp2}/".$RedemptionFilename, $contentsRedm);
//        }
//        else{
//            return self::respondWith(
//                400, "NOT OK", array('RedemptionReport' => 'Unable to authenticate with server'), 'Reports'
//            );
//        }
//        $connection3 = ssh2_connect('secureftp.alfransi.com.sa');
//        if(ssh2_auth_password($connection3, 'blusolutions', 'Za=uphE7ep!u')){
//            $sftp3 = ssh2_sftp($connection3);
//            $AllTransactionReward = AutogeneratedReportController::ReportAllTransactionsReward($start_date,$end_date,$partners_children);
//            $RewardFilename = "./BSFLoyalt_TRAN_". date('Ymd').'.txt';
//            $contentsRew = file_get_contents($AllTransactionReward);
//            file_put_contents("ssh2.sftp://{$sftp3}/".$RewardFilename, $contentsRew);
//
//
//            // Upload file
//            //echo "Connection successful, uploading file now...";
//
//        }
//        else{
//            return self::respondWith(
//                400, "NOT OK", array('transactionReport' => 'Unable to authenticate with server'), 'Reports'
//            );
//        }
//
//        if($AllTransactionReward == TRUE){
//            $sms = new EDS(
//                "BLU KSA",
//                   0
//            );
//            $number1 = "971569496232";
//            $message = "Reports uploaded successfully 2";
//            // Comma delimited mobile numbers prefixed with country code
//            $number = str_replace('+','',$number1);
//            $outputralph = $sms->send($number, $message);
//            return self::respondWith(
//                200, "OK", array('transactionReport' => 'Uploaded successfully'), 'Reports'
//            );
//        }
//        else{
//            $sms = new EDS(
//                "BLU KSA",
//                   0
//            );
//            $number1 = "971569496232";
//            $message = "Reports NOT uploaded successfully 2";
//            // Comma delimited mobile numbers prefixed with country code
//            $number = str_replace('+','',$number1);
//            $outputralph = $sms->send($number, $message);
        return self::respondWith(
                        200, "OK", array('customerReport' => 'Uploaded successfully'), 'Reports'
        );
//        }
    }

    public static function getBsfwwwReports($start_date, $end_date, $partner_id) {
        $partner = Partner::find($partner_id);
        $partners = $partner->managedObjects();
        $partners_children = array();
        $partners_children[] = $partner_id;
        foreach ($partners as $part) {
            $partners_children[] = $part->id;
        }

        $AllCustomers = AutogeneratedReportController::ReportwwwAllCustomers($partner_id);
        $connection1 = ssh2_connect('secureftp.alfransi.com.sa');
        if (ssh2_auth_password($connection1, 'blusolutions', 'Za=uphE7ep!u')) {
            $sftp1 = ssh2_sftp($connection1);
            $CustomersFilename = "./BSFLoyalt_CUST_" . date('Ymd') . '.txt';
            $contents = file_get_contents($AllCustomers);
            file_put_contents("ssh2.sftp://{$sftp1}/" . $CustomersFilename, $contents);
        } else {
            return self::respondWith(
                            400, "NOT OK", array('CustomerReport' => 'Unable to authenticate with server'), 'Reports'
            );
        }


        $accountingFile = AutogeneratedReportController::ReportwwwAccountingFile($start_date, $end_date, $partner_id);
        $connection = ssh2_connect('secureftp.alfransi.com.sa');
        if (ssh2_auth_password($connection, 'blusolutions', 'Za=uphE7ep!u')) {
            $sftp = ssh2_sftp($connection);
            $AccountingFilename = "./BSFAccountingFile_" . date('Ymd') . '.txt';

            if ($accountingFile !== FALSE) {
                $contentsAccounting = file_get_contents($accountingFile);
                file_put_contents("ssh2.sftp://{$sftp}/" . $AccountingFilename, $contentsAccounting);
            }
        } else {
            return self::respondWith(
                            400, "NOT OK", array('AccountingReport' => 'Unable to authenticate with server'), 'Reports'
            );
        }




        if ($AllCustomers == TRUE) {
            $sms = new EDS(
                    "BLU KSA", 0
            );
            $number1 = "971569496232";
            $message = "Reports uploaded successfully";
            // Comma delimited mobile numbers prefixed with country code
            $number = str_replace('+', '', $number1);
            $outputralph = $sms->send($number, $message);

            return self::respondWith(
                            200, "OK", array('transactionReport' => 'Uploaded successfully'), 'Reports'
            );
        } else {
            $sms = new EDS(
                    "BLU KSA", 0
            );
            $number1 = "971569496232";
            $message = "Reports not uploaded successfully";
            // Comma delimited mobile numbers prefixed with country code
            $number = str_replace('+', '', $number1);
            $outputralph = $sms->send($number, $message);

            return self::respondWith(
                            400, "NOT OK", array('transactionReport' => 'NOT Uploaded successfully'), 'Reports'
            );
        }
    }

    public static function getBsfRedemptionReport($start_date, $end_date, $partner_id) {
        $partner = Partner::find($partner_id);
        $partners = $partner->managedObjects();
        $partners_children = array();
        foreach ($partners as $part) {
            $partners_children[] = $part->id;
        }

        $AllTransactionRedeem = AutogeneratedReportController::ReportwwwAllTransactionsRedemptions($start_date, $end_date, $partners_children);
        echo $AllTransactionRedeem;
        var_dump(file_get_contents($AllTransactionRedeem));
        exit();
    }

    public static function getBsfwwwReports2($start_date, $end_date, $partner_id) {
        $partner = Partner::find($partner_id);
        $partners = $partner->managedObjects();
        $partners_children = array();
        foreach ($partners as $part) {
            $partners_children[] = $part->id;
        }

        $AllTransactionReward = AutogeneratedReportController::ReportwwwAllTransactionsReward($start_date, $end_date, $partners_children);

        $connection3 = ssh2_connect('secureftp.alfransi.com.sa');
        if (ssh2_auth_password($connection3, 'blusolutions', 'Za=uphE7ep!u')) {
            $sftp3 = ssh2_sftp($connection3);
            $RewardFilename = "./BSFLoyalt_TRAN_" . date('Ymd') . '.txt';
            $contentsRew = file_get_contents($AllTransactionReward);
            file_put_contents("ssh2.sftp://{$sftp3}/" . $RewardFilename, $contentsRew);
            // Upload file
            //echo "Connection successful, uploading file now...";
        } else {
            return self::respondWith(
                            400, "NOT OK", array('transactionReport' => 'Unable to authenticate with server'), 'Reports'
            );
        }


        $AllTransactionRedeem = AutogeneratedReportController::ReportwwwAllTransactionsRedemptions($start_date, $end_date, $partners_children);

        $connection2 = ssh2_connect('secureftp.alfransi.com.sa');
        if (ssh2_auth_password($connection2, 'blusolutions', 'Za=uphE7ep!u')) {
            $sftp2 = ssh2_sftp($connection2);
            $RedemptionFilename = "./BSFLoyalt_REDM_" . date('Ymd') . '.txt';
            $contentsRedm = file_get_contents($AllTransactionRedeem);
            file_put_contents("ssh2.sftp://{$sftp2}/" . $RedemptionFilename, $contentsRedm);
        } else {
            return self::respondWith(
                            400, "NOT OK", array('RedemptionReport' => 'Unable to authenticate with server'), 'Reports'
            );
        }



        if ($AllTransactionReward == TRUE) {
            $sms = new EDS(
                    "BLU KSA", 0
            );
            $number1 = "971569496232";
            $message = "Reports uploaded successfully 2";
            // Comma delimited mobile numbers prefixed with country code
            $number = str_replace('+', '', $number1);
            $outputralph = $sms->send($number, $message);
            return self::respondWith(
                            200, "OK", array('transactionReport' => 'Uploaded successfully'), 'Reports'
            );
        } else {
            $sms = new EDS(
                    "BLU KSA", 0
            );
            $number1 = "971569496232";
            $message = "Reports NOT uploaded successfully 2";
            // Comma delimited mobile numbers prefixed with country code
            $number = str_replace('+', '', $number1);
            $outputralph = $sms->send($number, $message);
            return self::respondWith(
                            400, "NOT OK", array('transactionReport' => 'NOT Uploaded successfully'), 'Reports'
            );
        }
    }

    public function postRewardPoints() {

        $partner = $this->partner;
        $store_id = Input::get('store');
        $user_credential = Input::get('credential');

//        $user_by_credential = Reference::where('number',  $user_credential);
//        if(!$user_by_credential){
        $user_by_credential = User::where('email', $user_credential)->first();
        if (!$user_by_credential) {
            $user_by_credential = User::where('normalized_mobile', $user_credential)->first();
            if (!$user_by_credential) {
                $user_by_credential = User::find($user_credential);
                if (!$user_by_credential) {
                    return self::respondWith('400', "User not found by credential!");
                }
            }
        }
//        }

        $reward_amount = Input::get('amount');
        $reward_currency = Input::get('currency');
        $tx = array();
        $tx['user_id'] = $user_by_credential->id;
        $tx['partner_id'] = $partner->id;
        $tx['store_id'] = $store_id;
        $store_res = Store::find($tx['store_id']);
        if ($store_res) {
            $pos_id = $store_res->pointOfSales->first()->id;
            $tx['pos_id'] = $pos_id;
        }
        $tx['total_amount'] = $reward_amount;
        $tx['currency'] = $reward_currency;
        $tx['created_at'] = date('Y-m-d H:i:s');
        $tx['updated_at'] = date('Y-m-d H:i:s');

        $validator = Validator::make($tx, array(
                    //'id'         => 'required|numeric',
                    'partner_id' => 'required|numeric',
                    'store_id' => 'required|numeric',
                    'user_id' => 'required|numeric'
        ));
        if (!$validator->fails()) {
            $params = array(
                'user_id' => $tx['user_id'],
                'amount' => $tx['total_amount'],
                'currency' => $tx['currency'],
                'partner' => $tx['partner_id'],
                'store_id' => $tx['store_id'], // Optional
                'pos_id' => $tx['pos_id'], // Optional
                //'notes'		=> $tx['notes'], // Optional
                'created_at' => $tx['created_at'],
                'updated_at' => $tx['updated_at']
            );
            $response = Transaction::doReward($params, 'Amount');
            unset($response->transaction['loyalty']);
            unset($response->transaction['partner']);
            unset($response->transaction['store']);

            return json_encode($response);
        } else {
            return self::respondWith(
                            400, "NOT OK", array('message' => 'Failure to create transaction'), 'Response'
            );
        }
    }

    public function postRedeemPoints() {

        $user_pin = Input::get('user_pin');
        $trans_amount = Input::get('amount', 0);
        $store_id = Input::get('store');
        $credential = Input::get('credentials');

        $partner = $this->partner;
        $reference = Reference::where('number', $credential)->first();
        if (is_object($reference)) {
            $user_id = $reference->user_id;
        } else {
            $temp_user = User::where('normalized_mobile', "+$credential")->first();
            if (is_object($temp_user)) {
                $user_id = $temp_user->id;
            } else {
                $temp_user = User::where('email', "$credential")->first();
                if (is_object($temp_user)) {
                    $user_id = $temp_user->id;
                }
            }
        }
        if (isset($user_id)) {
            $user = User::find($user_id);
        } else {
            return self::respondWith(
                            500, "Could not locate user"
            );
        }



        if (!$user) {
            return self::respondWith(
                            500, "Could not locate user with id {$user_id} in database"
            );
        }

        if ($user->status != 'active' || $user->verified != true) {
            $errormsg = "You are not allowed to use your points";
            return self::respondWith(
                            500, $errormsg
            );
        }

        if ($user->balance($this->partner->firstNetwork()->id) < ($trans_amount)) {
            $errormsg = "User balance is insufficient for the requested transaction";

            return self::respondWith(
                            500, $errormsg
            );
        }

        if ($user->passcode != $user_pin) {

            $errormsg = "The user PIN is invalid";

            return self::respondWith(
                            500, $errormsg
            );
        }

        //check network
        $senderNetwork = $this->partner->firstNetwork()->id;

        // Sender (Redemption)
        $partner_id = $partner->id;

        $store = Store::find($store_id);
        $pos_id = $store->pointOfSales->first()->id;

        $ptsToBeRedeemed = $trans_amount;

        $params = array(
            'user_id' => $user->id,
            'partner' => $partner_id,
            'points' => $ptsToBeRedeemed,
            'store_id' => $store_id,
            'pos_id' => $pos_id,
            'source' => "Points Redemption",
            'reference' => "Points Redemption from $partner->name",
            'notes' => "Points Redemption"
        );

        $redeemTrx = Transaction::doRedemption($params, "Points");




        $user1 = $user->toArray();


        $trx = $redeemTrx->transaction['trx'];
        $arr_trx = array();
        $arr_trx['points_redeemed'] = $trx['points_redeemed'];
        $arr_trx['cash_payment'] = $trx['cash_payment'];
        $arr_trx['store'] = $trx['store']['id'] . " - " . $trx['store']['name'];
        AuditHelper::record(
                $this->partner->id, "'{$user1['id']} / {$user1['email']}' Redeemed {$trans_amount} points "
        );
        unset($user1['passcode']);
        return self::respondWith(
                        200, null, $arr_trx, 'trx'
        );
    }

    /**
     * function to update price in points
     * and network ids in transaction item table
     *
     * POST /api/update-transaction-item-info/{user_id}
     * @return type
     */
    public function postUpdateTransactionItemInfo() {
        $result = array();
        // get redeem transactions
        $transactions = Transaction::where('trx_redeem', 1)->whereIn('trans_class', array('Items', 'Reversal', 'Reversal Item'))->get();

        foreach ($transactions as $transaction) {
            // get the trx network id
            $networkId = $transaction->network_id;
            $transactionItems = TransactionItem::where('transaction_id', $transaction->id)->get();
            if (empty($transactionItems)) {
                continue;
            }
            //for every item get its trx info
            foreach ($transactionItems as $transactionItem) {
                //calculate the original price
                $totalPriceInPoints = PointsHelper::amountToPoints($transactionItem->original_total_amount, $transactionItem->currency_id, $networkId);
                // update the trx item info
                $data = array('price_in_points' => $totalPriceInPoints, 'network_id' => $networkId);
                $result[] = TransactionItem::where('id', $transactionItem->id)->update($data);
            }
        }

        return json_encode($result);
    }

    /**
     * get balance by reference
     *
     * GET /api2/balance-by-reference
     *
     * @param GET reference
     * @return Response
     */
    public function getBalanceByReference() {
        $partner = $this->partner;
        $reference = Input::get('reference');
        if (strpos($partner->prefix, '-')) {
            $prefix = $partner->prefix;
        } else {
            $prefix = $partner->prefix . '-';
        }
        $ref = Reference::where('number', $prefix . $reference)->first();

        if (!$ref) {
            return self::respondWith(
                            400, "The reference provided is not valid"
            );
        }
        $user_id = $ref->user_id;

        $user = User::find($user_id);

        $partners = $partner->managedObjects();
        $partners_children = array();
        foreach ($partners as $part) {
            $partners_children[] = $part->id;
        }
        if (!$user) {
            return self::respondWith(
                            400, "The PIN provided is not valid"
            );
        }
        $arr_response = array();
        $balance = $user->balance($this->partner->firstNetwork()->id);
        if ($this->partner->has_middleware == 1 || Input::has('balancearr')) {
            $sum_expired_points = Transaction::select(DB::raw('sum(points_rewarded) pts_rewarded'))->where('expired', 1)->where('user_id', $user_id)->whereIn('partner_id', $partners_children)->first();
            $sum_redeemed_points = Transaction::select(DB::raw('sum(points_redeemed) pts_redeemed'))->where('trx_redeem', 1)->where('user_id', $user_id)->whereIn('partner_id', $partners_children)->first();
            $user_tier = $user->tiercriteria()->get();
            $tierC_expiry = 0;
            if (!is_numeric($sum_expired_points->pts_rewarded)) {
                $sum_expired_points->pts_rewarded = 0;
            }
            if (!is_numeric($sum_redeemed_points->pts_redeemed)) {
                $sum_redeemed_points->pts_redeemed = 0;
            }
            $monthName = 'January 1st';
            $composed_date = '';
            $max_date = date('Y-m-d', strtotime('-1 year'));
            if (isset($user_tier)) {
//                    var_dump($user_tier);
//                    exit();
                foreach ($user_tier as $tierCriterion) {


                    if (!is_null($tierCriterion->expiry_in_months)) {
                        $parent_tier = Tier::find($tierCriterion->tier_id);
                        $monthNum = $parent_tier->start_month;
                        $dateObj = DateTime::createFromFormat('!m', $monthNum);
                        $current_mday = intval(date('md'));
                        $tier_mday = intval($monthNum . "01");
                        if ($current_mday > $tier_mday) {
                            $year = date('Y', strtotime('+1 year'));
                        } else {
                            $year = date('Y');
                        }
                        $monthDisplay = $monthNum;
                        if (strlen($monthDisplay) < 2) {
                            $monthDisplay = "0" . $monthDisplay;
                        }
                        $composed_date = $year . "-" . $monthDisplay . "-01";
                        $monthName = $dateObj->format('F') . " 1st"; // March


                        $tierC_expiry = $tierCriterion->expiry_in_months;
                        $max_timestamp = strtotime("-" . $tierC_expiry . " month", strtotime($composed_date));
                        $max_date = date("Y-m-d", $max_timestamp);
                        $arr_response['expiry_period'] = $tierC_expiry;
                    } else {
                        $arr_response['expiry_period'] = 0;
                        $arr_response['next_expiry'] = 'January 1st';
                    }
                }
            }

            $sum_expiring_points = Transaction::select(DB::raw('sum(points_rewarded) pts_rewarded'))->where('expired', 0)->where('user_id', $user_id)->whereIn('partner_id', $partners_children)->where('created_at', '<', $max_date)->first();
            if (is_null($sum_expiring_points->pts_rewarded)) {
                $sum_expiring_points->pts_rewarded = 0;
            }
            $arr_response['balance'] = $balance;
            $arr_response['sum_expired_points'] = $sum_expired_points->pts_rewarded;
            $arr_response['sum_redeemed_points'] = $sum_redeemed_points->pts_redeemed;
            $arr_response['expiry_period'] = $tierC_expiry;
            $arr_response['next_expiry'] = $monthName;
            $arr_response['sum_expiring_points'] = $sum_expiring_points->pts_rewarded;
            return self::respondWith(
                            200, null, array('balance' => $arr_response), 'user'
            );
        }
        return self::respondWith(
                        200, null, array('balance' => $balance), 'user'
        );
    }

    /**
     * Reward By Product JSON
     *
     * POST /api2/import-product-json
     *
     * @author Ralph Nader <adminit@bluloyalty.com>
     *
     * @param String json_data
     *
     * SAMPLE:
     * {
      "upc": "521521",
      "description": "iPhone 7 - 128 GB",
      "sku": "4518215",
      "partnumber": "251125125122235232",
      "brand_id":"41211",
      "brand_desc" : "Apple",
      "major_id": "41515",
      "major_desc": "iPhone 7",
      "minor_id": "45145",
      "minor_desc": "128 GB",
      "cat_id" :"211",
      "cat_desc":"Mobile Phones"
      }
     *
     * @return Response
     */
    public function postImportProductJson() {
        $partner = $this->partner;
        $partner_prefix = $partner->prefix;
        $json_sent = self::sanitizeText(Input::get('json_data'));
        $decoded_json = json_decode($json_sent);
        //TODO: Check if SKU already exists for this partner
        $sku_exists = DB::table('product_partner_reward')->where('sku', $decoded_json->sku)->count();
        if ($sku_exists > 0) {
            $arr_returned['received'] = $decoded_json;
            $arr_returned["sku"] = $decoded_json->sku;
            $product = ProductPartnerReward::where('sku', $decoded_json->sku)->where('partner_id', $partner->id)->first();
            $upc_exists = DB::table('product_upc')->where('upc', $decoded_json->upc)->count();
            if ($upc_exists == 0) {
                DB::table('product_upc')->insert(array(
                    'upc' => $decoded_json->upc,
                    'product_id' => $product->product_id
                ));
            }
            return self::respondWith(
                            200, null, $arr_returned, 'Product'
            );
        }
        $product = new Product();
        //$product->reference = $decoded_json->upc;
        $product->name = $decoded_json->description;
        $product->draft = false;
        $product->save();
        if (!empty($decoded_json->upc)) {
            DB::table('product_upc')->insert(array(
                'upc' => $decoded_json->upc,
                'product_id' => $product->id
            ));
        }

        DB::table('partner_product')->insert(array(
            'partner_id' => $partner->id,
            'product_id' => $product->id
        ));

        DB::table('product_partner_reward')->insert(array(
            'partner_id' => $partner->id,
            'product_id' => $product->id,
            'sku' => $decoded_json->sku,
            'partnumber' => $decoded_json->partnumber,
            'partner_mapping_id' => $partner_prefix . $decoded_json->sku
        ));
        $model_id = $decoded_json->major_id;
        $model_description = $decoded_json->major_desc;
        $model_partner = ProdmodelPartner::where('partner_id', $partner->id)->where('partner_map_id', 'like', '%' . $model_id . '%')->first();
        if (!$model_partner) {
            $Last_model_id = DB::table('prodmodel')->insertGetId(array(
                'name' => $model_description,
                'draft' => false,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ));
            DB::table('prodmodel_partner')->insert(array(
                'partner_id' => $partner->id,
                'prodmodel_id' => $Last_model_id,
                'partner_map_id' => $model_id
            ));
            $product->model = $Last_model_id;
        } else {
            $product->model = $model_partner->prodmodel_id;
        }

        $submodel_id = $decoded_json->minor_id;
        $submodel_description = $decoded_json->minor_desc;
        $submodel_partner = ProdsubmodelPartner::where('partner_id', $partner->id)->where('partner_map_id', 'like', '%' . $submodel_id . '%')->first();
        if (!$submodel_partner) {
            $Last_submodel_id = DB::table('prodsubmodel')->insertGetId(array(
                'name' => $submodel_description,
                'draft' => false,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ));
            DB::table('prodsubmodel_partner')->insert(array(
                'partner_id' => $partner->id,
                'prodsubmodel_id' => $Last_submodel_id,
                'partner_map_id' => $submodel_id
            ));
            $product->sub_model = $Last_submodel_id;
        } else {
            $product->sub_model = $submodel_partner->prodsubmodel_id;
        }

        $category_id = $decoded_json->cat_id;
        $category_description = $decoded_json->cat_desc;
        $category_partner = CategoryPartner::where('partner_id', $partner->id)->where('partner_map_id', 'like', '%' . $category_id . '%')->first();
        if (!$category_partner) {
            $Last_category_id = DB::table('category')->insertGetId(array(
                'name' => $category_description,
                'draft' => false,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ));

            DB::table('category_partner')->insert(array(
                'partner_id' => $partner->id,
                'category_id' => $Last_category_id,
                'partner_map_id' => $category_id
            ));
            $product->category_id = $Last_category_id;
        } else {
            $product->category_id = $category_partner->category_id;
        }

        $brand_id = $decoded_json->brand_id;
        $brand_description = $decoded_json->brand_desc;
        $brand_partner = BrandPartner::where('partner_id', $partner->id)->where('partner_map_id', 'like', '%' . $brand_id . '%')->first();
        if (!$brand_partner) {
            $Last_brand_id = DB::table('brand')->insertGetId(array(
                'name' => $brand_description,
                'draft' => false,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ));

            DB::table('brand_partner')->insert(array(
                'partner_id' => $partner->id,
                'brand_id' => $Last_brand_id,
                'partner_map_id' => $brand_id
            ));
            $product->brand_id = $Last_brand_id;
        } else {
            $product->brand_id = $brand_partner->brand_id;
        }

        $product->save();
        $arr_returned['received'] = $decoded_json;
        $arr_returned["sku"] = $decoded_json->sku;

        return self::respondWith(
                        200, null, $arr_returned, 'Product'
        );
    }

    /**
     * Create a new user account in draft state
     *
     * POST /api2/create-new-user
     *
     * @param POST first_name
     * @param POST last_name
     * @param POST email
     * @param POST dob_day
     * @param POST dob_month
     * @param POST dob_year
     * @param POST city_id
     * @param POST area_id
     * @param POST mobile
     * @param POST card_id
     * @return Response
     */
    public function postCreateNewUser() {
        $partner = $this->partner;

        if (Input::has('partner')) {
            $partner = Partner::find(self::sanitizeText(Input::get('partner')));
        }

        //check required data
        $validator = Validator::make(Input::all(), array(
                    'first_name' => 'required|min:2',
                    'last_name' => 'required|min:2',
                    //'email'         => 'required|email',
                    'dob_day' => 'required',
                    'dob_month' => 'required',
                    'dob_year' => 'required',
                    'city_id' => 'required',
                    'area_id' => 'required',
                    'country_id' => 'required',
                    'mobile' => 'required'
        ));

        if ($validator->fails()) {
            return self::respondWith(
                            400, null, $validator->messages()->all(), 'errors'
            );
        }

        $dob = date("Y-m-d", strtotime(
                        self::sanitizeText(Input::get('dob_day')) . '-' . self::sanitizeText(Input::get('dob_month')) . '-' . self::sanitizeText(Input::get('dob_year'))
        ));


        //create draft user
        $country = Country::find(self::sanitizeText(Input::get('country_id')));
        $norm_mobile = InputHelper::normalizeMobileNumber(self::sanitizeText(Input::get('mobile')), $country->telephone_code);
        $count = DB::table('users')->where('normalized_mobile', "$norm_mobile")->where('draft', false)->count();
        if ($count > 0) {
            return self::respondWith(
                            400, null, "User Already Exists", 'message'
            );
        }
        if (strpos($partner->prefix, '-')) {
            $prefix = $partner->prefix;
        } else {
            $prefix = $partner->prefix . '-';
        }
        if (Input::has('reference')) {
            if (strpos(self::sanitizeText(Input::get('reference')), $partner->prefix)) {
                $refNumber = self::sanitizeText(Input::get('reference'));
            } else {
                $refNumber = $prefix . self::sanitizeText(Input::get('reference'));
            }
        } else {
            $refNumber = $prefix . RefID::newNumber();
        }
        $countRef = Reference::where('number', 'like', $refNumber)->count();
        if ($countRef > 0) {
            return self::respondWith(
                            400, null, "Reference Already Used", 'message'
            );
        }
        $user = new User();
        $user->ucid = UCID::newCustomerID();
        $user->draft = true;
        $user->passcode = PinCode::newCode();
        $user->mobile = InputHelper::sanitizeMobile(Input::get('mobile'));
        if ($country) {
            $user->normalized_mobile = InputHelper::normalizeMobileNumber($user->mobile, $country->telephone_code);
            $user->telephone_code = $country->telephone_code;
            $user->country_id = $country->id;
        }
        $user->country;

        //set the registration source POS
        if (!empty(self::sanitizeText(Input::get('pos_id')))) {
            $posMappingId = $partner->prefix . self::sanitizeText(Input::get('pos_id'));
            $posDetails = Pos::where('mapping_id', $posMappingId)->first();
            if (!$posDetails) {
                $storeDetails = Store::where('partner_id', $partner->id)->where('name', 'Real-Time Registration')->first();
                $posDetails = Pos::where('store_id', $storeDetails->id)->where('name', 'Real-Time Registration POS')->first();
                $user->registration_source_pos = $posDetails->id;
            }
            $user->registration_source_pos = $posDetails->id;
        } else {
            $storeDetails = Store::where('partner_id', $partner->id)->where('name', 'Real-Time Registration')->first();
            $posDetails = Pos::where('store_id', $storeDetails->id)->where('name', 'Real-Time Registration POS')->first();
            $user->registration_source_pos = $posDetails->id;
        }


        $user->save();

        //Default Tier affiliation to Member
        $defaultTier = Tier::where('partner_id', $partner->id)->where('name', 'like', 'Default Tier%')->first();
        if (!empty($defaultTier)) {
            $defaultTierCriteria = Tier::where('tier_id', $defaultTier->id)->where('name', 'like', 'Default Tier Criteria%')->first();
            if (!empty($defaultTierCriteria)) {
                DB::table('tiercriteria_user')->insert(array(
                    'tiercriteria_id' => $defaultTierCriteria->id,
                    'user_id' => $user->id
                ));
            }
        }

        $user = User::find($user->id);

        if (Input::has('telephone_code')) {
            $user->telephone_code = self::sanitizeText(Input::get('telephone_code'));
        }
        if (Input::has('mobile')) {
            $user->mobile = self::sanitizeText(Input::get('mobile'));
        }
        if (Input::has('country_id')) {
            $user->country_id = self::sanitizeText(Input::get('country_id'));
        }

        $user->first_name = self::sanitizeText(Input::get('first_name'));
        $user->last_name = self::sanitizeText(Input::get('last_name'));
        $email = "";
        if (Input::has('email')) {
            $email_exists = User::where('email', Input::get('email'))->count();
            if ($email_exists > 0) {
                $email = str_replace("-", "", $refNumber) . "@BLUCust.com";
            } else {
                $email = Input::get('email');
            }
        } else {
            $email = str_replace("-", "", $refNumber) . "@BLUCust.com";
        }
        $user->email = self::sanitizeText($email);
        $user->gender = self::sanitizeText(Input::get('gender', 'm'));
        $user->area_id = self::sanitizeText(Input::get('area_id'));
        $user->city_id = self::sanitizeText(Input::get('city_id'));
        $user->marital_status = self::sanitizeText(Input::get('marital_status', 'single'));
        $user->num_children = self::sanitizeText(Input::get('num_children', 0));
        $user->income_bracket = self::sanitizeText(Input::get('income_bracket', '$500 - $1,000'));
        $user->occupation_id = self::sanitizeText(Input::get('occupation_id', 0));
        $user->dob = $dob;
        if ($partner->id == 4337) {
            $user->status = 'inactive';
        } else {
            $user->status = 'active';
        }
        $user->normalized_mobile = InputHelper::normalizeMobileNumber($user->mobile, $user->telephone_code);
        $user->verified = true;
        $user->draft = false;

        /* BLU */
        DB::table('partner_user')->insert(array(
            'partner_id' => 1,
            'user_id' => $user->id
        ));

        /* WHITELABEL PARTNER */
        if ($this->partner->id != 1) {
            DB::table('partner_user')->insert(array(
                'partner_id' => $this->partner->id,
                'user_id' => $user->id
            ));
        }

        DB::table('role_user')->insert(array(
            'role_id' => 4,
            'user_id' => $user->id
        ));

        DB::table('segment_user')->insert(array(
            'segment_id' => $partner->segments->first()->id,
            'user_id' => $user->id
        ));

        //Default Tier affiliation to Member
        $defaultTier = Tier::where('partner_id', $partner->id)->where('name', 'like', 'Default Tier%')->first();
        if (!empty($defaultTier)) {
            $defaultTierCriteria = Tier::where('tier_id', $defaultTier->id)->where('name', 'like', 'Default Tier Criteria%')->first();
            if (!empty($defaultTierCriteria)) {
                DB::table('tiercriteria_user')->insert(array(
                    'tiercriteria_id' => $defaultTierCriteria->id,
                    'user_id' => $user->id
                ));
            }
        }
        //add refereence


        DB::table('reference')->insert(array(
            'number' => trim($refNumber),
            'partner_id' => trim($partner->id),
            'user_id' => $user->id
        ));

        AuditHelper::record(
                $this->partner->id, "'{$user->id} / {$user->email}' New Member Created " //{$userSentRalph}"
        );

        $user->ref_account = false;

        $user->save();

        //associate card
        if (!empty(self::sanitizeText(Input::get('card_id')))) {
            $card_id = self::sanitizeText(Input::get('card_id'));
            if (0 === strpos($card_id, $partner->prefix)) {
                $partner_prefix_card_id = $card_id;
            } else {
                $partner_prefix_card_id = $partner->prefix . $card_id;
            }
            $card = DB::table('card')->where('number', $partner_prefix_card_id)->first();

            if (!$card) {
                $card = new Card();
                $card->user_id = $user->id;
                $card->partner_id = $partner->id;
                $card->number = $partner_prefix_card_id;
                $card->save();
            } elseif ($card->user_id == 0) {
                DB::table('card')->where('number', $partner_prefix_card_id)->update(array(
                    'user_id' => $user->id,
                    'partner_id' => $partner->id
                ));
            }
        }

        if ($partner->id == 4337) {
            $user->sendWelcomeMessageNew($partner->id);
        } else {
            $user->sendWelcomeMessage();
        }

        $balance = $user->balance($partner->id);

        $user = $user->toArray();
        $user['balance'] = $balance;
        unset($user['passcode']);

        return self::respondWith(
                        200, null, $user, 'user'
        );
    }

    public function postImportProductsFile() {
        if (Input::hasFile('file')) {
            $ip = Request::getClientIp();
            $file = Input::file('file');
            $ext = $file->getClientOriginalExtension();
            $fileNameOriginal = $file->getClientOriginalName();

            $fileNameGenerated = PathHelper::generateFileBaseName($fileNameOriginal, $ext) . '.' . $ext;
            $folderPath = PathHelper::incoming('misc');

            $result = FALSE;
            $errResult = array();
            $auth_user = $this->user;
            Input::file('file')->move($folderPath, $fileNameGenerated);
            $filePath = $folderPath . '/' . $fileNameGenerated;
            $contents = File::get($filePath);
            $contents = nl2br($contents, FALSE);
            $contentLines = explode("<br>", $contents);

            $partner = $this->partner;
            $partner_prefix = $partner->prefix;
            $countContentLines = count($contentLines) - 1;
            $test = $contentLines[$countContentLines];
            $test1 = $contentLines[$countContentLines - 1];
            $ret_eof_error = true;
            if (trim($test) === "EOF") {
                $ret_eof_error = false;
            } elseif (trim($test1) === "EOF") {
                $ret_eof_error = false;
            }
            if ($ret_eof_error == true) {
                return self::respondWith(
                                200, 'EOF NOT FOUND', null
                );
            }
            $arrContent = array();
            foreach ($contentLines as $lineread) {
                if (trim($lineread) !== 'EOF') {
                    $arrContent[] = explode("||", $lineread);
                }
            }
            $count = 0;
            $arr_returned = array();
            foreach ($arrContent as $row) {
                if ($count != 0 && isset($row[2])) {
                    $sku_exists = DB::table('product_partner_reward')->where('sku', $row[2])->count();
                    if ($sku_exists > 0) {
                        $arr_returned[$count]['received'] = json_encode($row);
                        $arr_returned[$count]["sku"] = $row[2];
                        $product = ProductPartnerReward::where('sku', $row[2])->where('partner_id', $partner->id)->get();
                        $upc_exists = DB::table('product_upc')->where('upc', $row[0])->count();
                        if ($upc_exists == 0) {
                            DB::table('product_upc')->insert(array(
                                'upc' => $row[0],
                                'product_id' => $product->product_id
                            ));
                        }
                        return self::respondWith(
                                        200, null, $arr_returned, 'Product'
                        );
                    }
                    $product = new Product();
                    //$product->reference = $decoded_json->upc;
                    $product->name = $row[1];
                    $product->draft = false;
                    $product->save();
                    if (!empty($row[0])) {
                        DB::table('product_upc')->insert(array(
                            'upc' => $row[0],
                            'product_id' => $product->id
                        ));
                    }

                    DB::table('partner_product')->insert(array(
                        'partner_id' => $partner->id,
                        'product_id' => $product->id
                    ));

                    DB::table('product_partner_reward')->insert(array(
                        'partner_id' => $partner->id,
                        'product_id' => $product->id,
                        'sku' => $row[2],
                        'partnumber' => $row[3],
                        'partner_mapping_id' => $partner_prefix . $row[2]
                    ));
                    $model_id = $row[6];
                    $model_description = $row[7];
                    $model_partner = ProdmodelPartner::where('partner_id', $partner->id)->where('partner_map_id', 'like', '%' . $model_id . '%')->first();
                    if (!$model_partner) {
                        $Last_model_id = DB::table('prodmodel')->insertGetId(array(
                            'name' => $model_description,
                            'draft' => false,
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s')
                        ));
                        DB::table('prodmodel_partner')->insert(array(
                            'partner_id' => $partner->id,
                            'prodmodel_id' => $Last_model_id,
                            'partner_map_id' => $model_id
                        ));
                        $product->model = $Last_model_id;
                    } else {
                        $product->model = $model_partner->prodmodel_id;
                    }

                    $submodel_id = $row[8];
                    $submodel_description = $row[9];
                    $submodel_partner = ProdsubmodelPartner::where('partner_id', $partner->id)->where('partner_map_id', 'like', '%' . $submodel_id . '%')->first();
                    if (!$submodel_partner) {
                        $Last_submodel_id = DB::table('prodsubmodel')->insertGetId(array(
                            'name' => $submodel_description,
                            'draft' => false,
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s')
                        ));
                        DB::table('prodsubmodel_partner')->insert(array(
                            'partner_id' => $partner->id,
                            'prodsubmodel_id' => $Last_submodel_id,
                            'partner_map_id' => $submodel_id
                        ));
                        $product->sub_model = $Last_submodel_id;
                    } else {
                        $product->sub_model = $submodel_partner->prodsubmodel_id;
                    }

                    $brand_id = $row[4];
                    $brand_description = $row[5];
                    $brand_partner = BrandPartner::where('partner_id', $partner->id)->where('partner_map_id', 'like', $brand_id)->first();

                    if (!$brand_partner) {
                        $Last_brand_id = DB::table('brand')->insertGetId(array(
                            'name' => $brand_description,
                            'draft' => false,
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s')
                        ));

                        DB::table('brand_partner')->insert(array(
                            'partner_id' => $partner->id,
                            'brand_id' => $Last_brand_id,
                            'partner_map_id' => $brand_id
                        ));
                        $product->brand_id = $Last_brand_id;
                    } else {
                        $product->brand_id = $brand_partner->brand_id;
                    }
                    $category_id = $row[10];
                    $category_description = $row[11];
                    $category_partner = CategoryPartner::where('partner_id', $partner->id)->where('partner_map_id', 'like', $category_id)->first();

                    if (!$category_partner) {
                        $Last_category_id = DB::table('category')->insertGetId(array(
                            'name' => $category_description,
                            'draft' => false,
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s')
                        ));

                        DB::table('category_partner')->insert(array(
                            'partner_id' => $partner->id,
                            'category_id' => $Last_category_id,
                            'partner_map_id' => $category_id
                        ));
                        $product->category_id = $Last_category_id;
                    } else {
                        $product->category_id = $category_partner->category_id;
                    }

                    try {
                        $product->save();
                    } catch (Exception $ex) {
                        echo "<pre>";
                        var_dump($row);
                        var_dump($ex);
                        exit();
                    }


                    $arr_returned[$count]['received'] = json_encode($row);
                    $arr_returned[$count]["sku"] = $row[2];
                }
                $count += 1;
            }
        }
        return self::respondWith(
                        200, null, $arr_returned, 'Product'
        );
    }

    /**
     * Reward By Product JSON
     *
     * POST /api2/reward-by-product-json
     * @author Ralph Nader <adminit@bluloyalty.com>
     *
     * @param string json_data
     *
     * SAMPLE:
     * {
      "total_amount": 4000,
      "currency": "USD",
      "reference": "4518215",
      "store": "28",
      "invoice_number": "20161130101001",
      "items": [
      {
      "amount": "1522",
      "product": "1234567",
      "quantity": "1"
      },
      {
      "amount": "1200",
      "product": "1234568",
      "quantity": "2"
      },
      {
      "amount": "1200",
      "product": "1234569",
      "quantity": "1"
      },
      {
      "amount": "1200",
      "product": "1234570",
      "quantity": "1"
      }
      ]
      }
     *
     * @return Response
     */
    public function postRewardByProductJson() {
//        $storeId              = Input::get('store');
        $json_sent = self::sanitizeText(Input::get('json_data'));
        $decoded_json = json_decode($json_sent);

        $MappingStoreId = $decoded_json->store;
        $store = Store::where('mapping_id', $MappingStoreId)->where('partner_id', $this->partner->id)->first();
        if (!$store) {
            return self::respondWith(
                            400, "The Store provided is not valid"
            );
        }
        $storeId = $store->id;
        $reference = $decoded_json->reference;

        $currencyShortCode = $decoded_json->currency;

        $invoice_number = $decoded_json->invoice_number;

        //find partner
        //$store = Store::find($storeId);
        $partnerId = $store->partner_id;
        $posId = $store->pointOfSales->first()->id;
        $partner = Partner::find($partnerId);
        //find user
        if (strpos($partner->prefix, '-')) {
            $prefix = $partner->prefix;
        } else {
            $prefix = $partner->prefix . '-';
        }
        $ref = Reference::where('number', $prefix . $reference)->first();
//        echo "<pre>";
//        var_dump($ref);
//        exit();
        if (!$ref) {
            return self::respondWith(
                            400, "The Reference provided is not valid"
            );
        }
        $userId = $ref->user_id;
        $sum_rewarded = 0;
        $count = 0;
        $arr_status = array();
        foreach ($decoded_json->items as $item) {
            $qty = $item->quantity;
            $amount = floatval($item->amount) * intval($qty);
            $productref = $item->product;
            $product = Product::where('partner_mapping_id', $prefix . $productref)->first();
            if ($product) {
                $productId = $product->id;
            } else {
//                $productReward	= ProductReward::where('partnumber', $prefix . $productref)->first();
                $productReward = DB::table('product_partner_reward')->where('partner_mapping_id', $prefix . $productref)->first();
                if ($productReward) {
                    $productId = $productReward->product_id;
                } else {
                    $productId = 0;
                }
            }
            $ref_number = $partner->id . " - " . $invoice_number . $count;
            $trx_exists = Transaction::where('ref_number', $ref_number)->count();
            //$arr_status['ref_number'][] = $ref_number;

            if ($trx_exists == 0) {
                $params = array(
                    'user_id' => $userId,
                    'partner' => $partnerId,
                    'pos_id' => $posId,
                    'store_id' => $storeId,
                    'amount' => $amount,
                    'original_transaction_amount' => $amount,
                    'currency' => $currencyShortCode,
                    'partner_currency' => $currencyShortCode,
                    'notes' => 'Reward By Product',
                    'type' => 'reward',
                    'reference' => $ref_number,
                    'network_id' => $partner->firstNetwork()->id,
                    'product_id' => $productId,
                    'use_partner_currency' => True
                );


                $response = Transaction::doRewardNewLoyalty($params, 'Amount');
                //$arr_status['response'][] = $response;
                $sum_rewarded += $response->transaction['points_rewarded'];
                $status = 'received';
            } else {
                $status = 'already exists';
            }
            //$arr_status['status'][] = $status;
            //$arr_status['trx_exists'][] = $trx_exists;
            $count++;
        }
        $resp_reward = array();
        $resp_reward['reference'] = $prefix . $reference;
        $resp_reward['store'] = $MappingStoreId;
        $resp_reward['rewarded_points'] = $sum_rewarded;
        $resp_reward['currency'] = $currencyShortCode;
        $resp_reward['amount'] = $amount;
        $resp_reward['invoice_number'] = $invoice_number;
        $resp_reward['status'] = $status;

        return self::respondWith(
                        200, null, $resp_reward, 'Transaction'
        );
    }

    public function getMilesExtraction() {
        $string = "12345678901234567890";
        echo $this->partner->id;
        echo str_pad($string, "30", "x");
        echo "<br>";
        echo substr(str_pad($string, "10", "x"), 0, 10);
        $arr_file = array();
        $arr_file[] = "H000000094";
        $transactions = Transaction::where('notes', 'like', "%\"airline\":\"3\"%")->where('partner_id', $this->partner->id)->get();
        foreach ($transactions as $trx) {
            $temp_arr = array();
            $decoded_trx_notes = json_decode($trx->notes, TRUE);
            $country = Country::find($trx->country_id);
            $country_to_show = $country->name;
            $currency = Currency::find($trx->currency_id);
            $currency_to_show = substr($currency->short_code, 0, 2);
            $user = User::find($trx->user_id);
            $user_name = ""; //$user->first_name . " " . $user->last_name;
            $temp_arr[] = substr(str_pad($decoded_trx_notes['frequent_flyer_number'], "9", "0", STR_PAD_LEFT), 0, 9);
            $temp_arr[] = substr(str_pad(str_replace("-", "", $trx->created_at), "8", "0", STR_PAD_LEFT), 0, 8);
            $temp_arr[] = substr(str_pad("", "10", " ", STR_PAD_LEFT), 0, 10);
            $temp_arr[] = substr(str_pad(str_replace("-", "", $trx->id), "14", " ", STR_PAD_LEFT), 0, 14);
            $temp_arr[] = substr(str_pad($country_to_show, "10", " ", STR_PAD_LEFT), 0, 10);
            $temp_arr[] = substr(str_pad("", "16", " ", STR_PAD_LEFT), 0, 16);
            $temp_arr[] = substr(str_pad("", "2", "0", STR_PAD_LEFT), 0, 2);
            $temp_arr[] = substr(str_pad("", "12", "0", STR_PAD_LEFT), 0, 12);
            $temp_arr[] = substr(str_pad("", "12", "0", STR_PAD_LEFT), 0, 12);
            $temp_arr[] = substr(str_pad($currency_to_show, "2", " ", STR_PAD_LEFT), 0, 2);
            $temp_arr[] = substr(str_pad($user_name, "20", " ", STR_PAD_RIGHT), 0, 20);
            $temp_arr[] = substr(str_pad(intval($trx->points_redeemed), "9", "0", STR_PAD_LEFT), 0, 9);
            $temp_arr[] = substr(str_pad("", "9", "0", STR_PAD_LEFT), 0, 9);
            $arr_file[] = implode('', $temp_arr);
        }

        $arr_file[] = "999999999";

        $count_lines = count($arr_file) - 2;
        $json_data = implode('\r\n', $arr_file);
        $data = array();
        $data['html'] = "QA miles conversion";
        // $response1 = Mail::send('emails.bsfaccounting', $data, function($message1) use ( $json_data, $count_lines) {
        //             $filename = "QA" . date('ymd') . '.a01';
        //             //$message1->attach(new Swift_Message_Attachment(file_get_contents($filepath), $filename, 'application/vnd.ms-excel'));
        //             $message1->attachData($json_data, $filename);
        //             $message1->to(array('adminit@bluloyalty.com'))->subject('LIVE Qatar Airways - ' . $count_lines);
        //         });
        //APIController::postSendEmailJson($json_data);


        $arr_file1 = array();
        $transactions1 = Transaction::where('notes', 'like', "%\"airline\":\"4\"%")->where('partner_id', $this->partner->id)->get();
        foreach ($transactions1 as $trx) {
            $temp_arr = array();
            $decoded_trx_notes = json_decode($trx->notes, TRUE);
            $user = User::find($trx->user_id);
            $user_name = ""; //$user->first_name . " " . $user->last_name;
            $temp_arr[] = substr($decoded_trx_notes['frequent_flyer_number'], 0, 11);
            $temp_arr[] = "XXXXXX1CCA";
            $trx_date = $newDate = date("d/m/Y", strtotime($trx->created_at));
            $temp_arr[] = substr($trx_date, 0, 10);
            $temp_arr[] = substr(str_pad(intval($trx->points_redeemed), "5", "0", STR_PAD_LEFT), 0, 5);
            $temp_arr[] = ""; //substr($user->first_name,0,1);
            $temp_arr[] = ""; //substr($user->last_name,0,25);
            $arr_file1[] = implode(',', $temp_arr);
        }

        $json_data2 = implode('\r\n', $arr_file1);
        $data['html'] = "Gulf Air miles conversion";
        $count_lines = count($arr_file1) - 2;
        $response1 = Mail::send('emails.bsfaccounting', $data, function($message1) use ( $json_data2, $count_lines) {
                    $filename = "GA" . date('ymd') . '.A01';
                    //$message1->attach(new Swift_Message_Attachment(file_get_contents($filepath), $filename, 'application/vnd.ms-excel'));
                    $message1->attachData($json_data2, $filename);
                    $message1->to(array('adminit@bluloyalty.com'))->subject('LIVE Gulf Air - ' . $count_lines);
                });
        //APIController::postSendEmailJson($json_data2);

        exit();
    }

    /**
     * function to migrate loyalty programs
     * to the new tables
     *
     * POST /api2/migrate-loyalty
     *
     */
    public function postMigrateLoyalty() {
        $loyaltyExceptionRules = array();
        //get all loyalty Programs
        $loyaltyPrograms = Loyalty::where('draft', 0)->get();
        foreach ($loyaltyPrograms as $loyaltyProgram) {
            foreach ($loyaltyProgram->rules as $k => $rule) {
                $loyaltyExceptionRules[$rule->id]['loyalty_rule_id'] = $rule->id;
                $loyaltyExceptionRules[$rule->id]['operator'] = 'and';
                $loyaltyExceptionRules[$rule->id]['type'] = $rule->rule_type;
                $loyaltyExceptionRules[$rule->id]['rule_value'] = $rule->rule_value;
                $loyaltyExceptionRules[$rule->id]['created_at'] = $rule->created_at->toDateTimeString();
                $loyaltyExceptionRules[$rule->id]['updated_at'] = $rule->updated_at->toDateTimeString();
                $loyaltyExceptionRules[$rule->id]['comparison_operator'] = 'equal';
            }
        }

        //insert into loyalty exception tabel
        $i = 0;
        foreach ($loyaltyExceptionRules as $rule_id => $loyaltyExceptionRule) {
            echo "inserting Rule ID : " . $rule_id . "<br/>";
            DB::table('loyalty_exceptionrule')->insert($loyaltyExceptionRule);
            echo "Rules migrated: " . $i++ . "<br/>";
        }
    }

    /**
     * function to migrate loyalty programs
     * to the new tables
     *
     * POST /api2/migrate-loyalty
     *
     */
    public function postMigrateProductpricing() {
        ignore_user_abort(true);
        $products = Product::where('draft', '0')->get();
        foreach ($products as $p) {
            if (is_null($p->supplier_id)) {
                $p->supplier_id = 1;
            }
            $prod_Redemption = new ProductPartnerRedemption();
            $prod_Redemption->product_id = $p->id;
            $prod_Redemption->partner_id = 1;
            $prod_Redemption->supplier_id = $p->supplier_id;
            $prod_Redemption->display_online = $p->display_online;
            $prod_Redemption->hot_deal = $p->hot_deal;
            $prod_Redemption->show_offline = $p->show_offline;
            $prod_Redemption->qty = $p->qty;
            $prod_Redemption->original_price = $p->original_price;
            $prod_Redemption->original_retail_price = $p->original_retail_price;
            $prod_Redemption->currency_id = $p->currency_id;
            $prod_Redemption->price_in_points = $p->price_in_points;
            $prod_Redemption->price_in_usd = $p->price_in_usd;
            $prod_Redemption->retail_price_in_usd = $p->retail_price_in_usd;
            $prod_Redemption->draft = $p->draft;
            $prod_Redemption->start_date = $p->start_date;
            $prod_Redemption->end_date = $p->end_date;
            $prod_Redemption->save();
            foreach ($p->countries()->get() as $country) {

                $prod_country = new ProductRedemptionCountries();
                $prod_country->product_redemption_id = $prod_Redemption->id;
                $prod_country->product_id = $p->id;
                $prod_country->partner_id = 1;
                $prod_country->country_id = $country->id;
                $prod_country->delivery_options = $p->delivery_options;
                $prod_country->status = $p->status;
                $prod_country->delivery_charges = $p->delivery_charges;
                $prod_country->currency_id = $p->currency_id;
                $prod_country->pickup_address = $p->pickup_address;
                $prod_country->custom_tarrif_taxes = $p->original_sales_tax;
                $prod_country->save();
            }

            foreach ($p->channels()->get() as $channel) {
                $prod_channel = new ProductRedemptionChannel();
                $prod_channel->product_redemption_id = $prod_Redemption->id;
                $prod_channel->product_id = $p->id;
                $prod_channel->partner_id = $channel->channel_id;
                $prod_channel->save();
            }

            foreach ($p->segments()->get() as $segment) {

                if (!is_null($segment->id)) {
                    $prod_segment = new ProductRedemptionSegments();
                    $prod_segment->product_redemption_id = $prod_Redemption->id;
                    $prod_segment->product_id = $p->id;
                    $prod_segment->segment_id = $segment->id;
                    $prod_segment->save();
                }
            }
        }
        ignore_user_abort(false);
        $arr_data = array('msg' => 'done updating the products');
        $json_data = json_encode($arr_data);
        APIController::postSendEmailJson($json_data);
        echo "done with segments";
        exit();
    }

    /**
     * Get Cashback File Bisb
     *
     * POST /api2/cashback-file-bisb
     *
     * @return Response
     */
    public function getCashbackFileBisb() {
        $partner = $this->partner;
        $partner_id = $partner->id;

        if ($partner_id != '4209') {
            return self::respondWith("403", "Permission denied. Incorrect partner");
        }

        $partner_prefix = $partner->prefix;
        $count = 0;

        $trxs = Transaction::where('partner_id', $partner_id)
                ->where('ref_number', 'Cashback')
                ->where('trx_redeem', 1)
                ->get();

        $arr_lines = array();
        $arr_lines[] = "Trx ID | Date | Loyalty ID | BD Value | Card Number | Line Number";
        foreach ($trxs as $trx) {
            $ref_id = Reference::where('user_id', $trx->user_id)->where('number', 'like', $partner_prefix . "%")->first();
            $notes = json_decode($trx->notes);
            if (isset($notes->first_digits)) {
                $xtea = new XTEA($partner_id);
                $first_cipher = $xtea->decrypt($notes->first_digits);
                $last_cipher = $xtea->decrypt($notes->last_digits);
                $arr_line = array();
                $arr_line[] = $trx->id;
                $arr_line[] = $trx->created_at;
                if ($ref_id):
                    $arr_line[] = $ref_id->number;
                else:
                    $arr_line[] = "";
                endif;
                $arr_line[] = $trx->partner_amount;
                $arr_line[] = $first_cipher . "XXXXXX" . $last_cipher;
                $count++;
                $arr_line[] = $count;
                $arr_lines[] = implode("|", $arr_line);
            }
        }
        $arr_lines[] = "EOF";
        //Trx ID | Date | Loyalty ID | Product Type | BD Value | Card Number | Line Number
        $fileNameGenerated = PathHelper::generateFileBaseName('BISBCashbackTransactions') . '.txt';
        $filepath = PathHelper::outgoing('misc/' . $fileNameGenerated);

        $myfile = fopen($filepath, "w") or die("Unable to open file!");
        fwrite($myfile, implode("\r\n", $arr_lines));
        fclose($myfile);

        $html = "BISB Cashback File";
        //$response1 =  self::emailattachement('adminit@bluloyalty.com',$filepath,'Cashback FNB',$html);
        $data = array();
        $data['html'] = $html;
        $sendgrid = new SendGrid(
                Config::get('sendgrid.username'), Config::get('sendgrid.password')
        );
        $filesize = filesize($filepath);
        $contents_of_file = file_get_contents($filepath);
//        $mail = new SendGrid\Email();
//
//        $mail->setTos(array('adminit@bluloyalty.com'));
//        $mail->setFrom(Config::get('blu.email_from'));
//        $mail->setFromName('BLU Points');
//        $mail->setSubject("Cashback Transactions - " . $filesize . '-' . $count);
//        
//        $file_get_content_size = strlen(file_get_contents($filepath));
//        $mail->setHtml($html . "<br>" . $contents_of_file);
//        if($file_get_content_size != $filesize){
//            APIController::postSendEmailJson(json_encode(array($file_get_content_size,$filesize)));
//        }
//        
//        //$returned_error_file = "Errors_". $name1;
//        $mail->setAttachment($contents_of_file,"testralph_20170424_RN.txt");
//
//        $sendgrid->send($mail);

        try {
            $response1 = Mail::send('emails.fnbemail', $data, function($message1) use ($contents_of_file, $fileNameGenerated, $count) {
                        $filename = str_replace('/', '', $fileNameGenerated);
                        //$message1->attach(new Swift_Message_Attachment(file_get_contents($filepath), $filename, 'application/vnd.ms-excel'));
                        $message1->attachData($contents_of_file, $filename);
                        $message1->to(array('adminit@bluloyalty.com'))->subject("DEV BISB Cashback - " . $count);
                    });

            foreach ($trxs as $trx) {
                $trx->ref_number = 'Posted Cashback';
                $trx->save();
            }
        } catch (Exception $ex) {
            $json_data = json_encode($ex->getMessage());
            APIController::postSendEmailJson($json_data, "try catch exception email 0001");
        }


        return self::respondWith("200", "OK");
    }

    /*
     * Import files on behalf of Customers
     *
     * @author: Ralph Nader <adminit@bluloyalty.com>
     */

    public function postImportFilexaaddcciiwd() {
        if (Input::hasFile('file')) {
            $ip = Request::getClientIp();
            $file = Input::file('file');
            $ext = $file->getClientOriginalExtension();
            $fileNameOriginal = $file->getClientOriginalName();

            $fileNameGenerated = PathHelper::generateFileBaseName($fileNameOriginal, $ext) . '.' . $ext;
            $folderPath = PathHelper::incoming('misc');

            $result = FALSE;
            $errResult = array();
            $auth_user = $this->user;

            $count = 0;
            $counterrors = 0;
            $errBool = True;
            $errors = array();
            $params = array();
            $passes = 0;
//$counterrors = 0;
            Input::file('file')->move($folderPath, $fileNameGenerated);
            $filePath = $folderPath . '/' . $fileNameGenerated;
            $contents = File::get($filePath);
            $contents = nl2br($contents, FALSE);
            $contentLines = explode("<br>", $contents);
            $errors = array();
            $countContentLines = count($contentLines) - 1;
            $test = $contentLines[$countContentLines];
            $test1 = $contentLines[$countContentLines - 1];
            $ret_eof_error = true;
            if (trim($test) === "EOF") {
                $ret_eof_error = false;
            } elseif (trim($test1) === "EOF") {
                $ret_eof_error = false;
            }
            if ($ret_eof_error == true) {
                return self::respondWith(
                                200, 'EOF NOT FOUND', $errors, 'errors'
                );
            }
            $arrContent = array();
            foreach ($contentLines as $lineread) {
                if (trim($lineread) !== 'EOF') {
                    $arrContent[] = explode("|", $lineread);
                }
            }
            $partner = $this->partner;

            $network_id = $partner->firstNetwork()->id;
            $partner_id = $partner->id;
            $country_id = $partner->country_id;
            $city_id = $partner->city_id;
            $area_id = $partner->area_id;
            $arr_currencies = array();
            $currencies = Currency::all();
            foreach ($currencies as $curr) {
                $arr_currencies[$curr->short_code] = $curr->id;
            }
            $store_res = $partner->stores->first();
            if ($store_res) {
                $pos_id = $store_res->pointOfSales->first()->id;
                $store_id = $store_res->id;
            }
            foreach ($arrContent as $row) {

                $posted_at = date('Y-m-d H:i:s');
                if ($count != 0) {
                    if (trim($row[0]) != '' && trim($row[0]) != 'EOF') {
                        $lineNumber = trim($row[3]);
                        $points = $row[1];
                        $total_amount = PointsHelper::pointsToAmount($points, $row[2], $network_id);
                        $currency_id = $arr_currencies[$row[2]];
                        $currency = Currency::find($currency_id);
                        $rate = $currency->latestRate();
                        $user = User::find(trim($row[0]));

                        $trans_reversal = new Transaction();
                        $trans_reversal->user_id = trim($row[0]);
                        $trans_reversal->country_id = $country_id;
                        $trans_reversal->partner_id = $partner_id;
                        $trans_reversal->trans_class = "Reversal";
                        $trans_reversal->source = "Import of File API";
                        $trans_reversal->ref_number = $fileNameOriginal . $lineNumber;
//                        $trans_reversal->notes				= $un_pts_rew->notes;
                        $trans_reversal->store_id = $store_id;
                        $trans_reversal->pos_id = $pos_id;
                        $trans_reversal->auth_staff_id = "1";
                        $trans_reversal->network_id = $network_id;
//                        $trans_reversal->rule_id			= $un_pts_rew->rule_id;
                        $trans_reversal->total_amount = $total_amount * (-1);
                        $trans_reversal->original_total_amount = $total_amount * (-1);
                        $trans_reversal->partner_amount = $total_amount * (-1);
                        $trans_reversal->points_rewarded = $points * (-1);
                        $trans_reversal->points_redeemed = 0;
                        $trans_reversal->points_balance = null;
                        $trans_reversal->trx_reward = 1;
                        $trans_reversal->trx_redeem = 0;
                        $trans_reversal->amt_reward = $total_amount * (-1);
                        $trans_reversal->amt_redeem = 0;
                        $trans_reversal->currency_id = $currency_id;
                        $trans_reversal->partner_currency_id = $currency_id;
                        $trans_reversal->exchange_rate = $rate;
                        $trans_reversal->product_id = Null;
                        $trans_reversal->product_model = Null;
                        $trans_reversal->product_brand_id = Null;
                        $trans_reversal->product_sub_model = Null;
                        $trans_reversal->product_category_id = Null;
                        $trans_reversal->city_id = Null;
                        $trans_reversal->area_id = Null;
                        $trans_reversal->segment_id = Null;
                        $trans_reversal->category_id = Null;
                        $trans_reversal->quantity = 0;
                        $trans_reversal->auth_role_id = $auth_user;
                        $trans_reversal->delivery_cost = 0;
                        $trans_reversal->cash_payment = 0;
                        $trans_reversal->reversed = 0;
                        $trans_reversal->posted_at = date('Y-m-d H:i:s');
                        if ($user->balance($trans_reversal->network_id) >= $points) {
                            $user->updateBalance($trans_reversal->points_rewarded, $trans_reversal->network_id, 'add');
                            $trans_reversal->save();
                        } else {
                            $errors[] = $trans_reversal->toArray();
                        }
                    }
                }
                $count++;
            }
            return self::respondWith("200", "File Imported", $errors, "errors");
        }
    }

    public function getSensitiveInfoFromMiddleware() {
        $userIds = array(33129, 39869, 39870, 39872, 39873, 39874, 39875, 39876, 39877, 39878, 39880, 39881, 39882, 39883, 39884, 39885, 39886, 39887, 39888, 39889, 39890, 39891, 39892, 39893, 39894, 39895, 39896, 39897, 39898, 39899, 39900, 39901, 39902, 39903, 39904, 39907, 39908, 39909, 39910, 39911, 39913, 39914, 39915, 39919, 39920, 39921, 39922, 39923, 39924, 39925, 39926, 39927, 39928, 39930, 39931, 39937, 39938, 39939, 39940, 39941, 39942, 39943, 39944, 39948, 39955, 39957, 39958, 39962, 39963, 39965, 39966, 39971, 39972, 39973, 39974, 39975, 39976, 39977, 39978, 39980, 39981, 39982, 39983, 39984, 39985, 39986, 39987, 39988, 39989, 39991, 39993, 39994, 39995, 39996, 39997, 39998, 39999, 40000, 40001, 40002, 40003, 40004, 40005, 40006, 40008, 40010, 40011, 40012, 40013, 40015, 40019, 40022, 40023, 40024, 40035, 40036, 40037, 40038, 40039, 40040, 40041, 40042, 40044, 40045, 40046, 40047, 40048, 40049, 40051, 40052, 40053, 40054, 40055, 40056, 40057, 40058, 40059, 40060, 40061, 40062, 40063, 40064, 40065, 40066, 40067, 40068, 40069, 40371, 40372, 40373, 40374, 40375, 40376, 40377, 40378, 40379, 40380, 40381, 40382, 40383, 40384, 40385, 40386, 40387, 40388, 40389, 40390, 40391, 40392, 40393, 40394, 40395, 40396, 40397, 40398, 40399, 40400, 40406, 40407, 40408, 40409, 40410, 40411, 40412, 40413, 40414, 40416, 40417, 40418, 40419, 40420, 40421, 40422, 40423, 40424, 40425, 40426, 40427, 40430, 40431, 40432, 40433, 40434, 40435, 40436, 40437, 40438, 40439, 40440, 40443, 40444, 40445, 40446, 40457, 40458, 40459, 40460, 40461, 40462, 40463, 40464, 40465, 40466, 40467, 40468, 40470, 40471, 40472, 40473, 40474, 40475, 40476, 40477, 40478, 40479, 40480, 40481, 40482, 40483, 40484, 40485, 40486, 40487, 40488, 40489, 40490, 40491, 40492, 40494, 40495, 40497, 40498, 40499, 40500, 40501, 40502, 40503, 40504, 40505, 40506, 40509, 40512, 40514, 40515, 40516, 40517, 40518, 40519, 40520, 40521, 40522, 40523, 40524, 40525, 40526, 40527, 40528, 40529, 40531, 40532, 40533, 40534, 40536, 40537, 40538, 40539, 40540, 40541, 40543, 40544, 40545, 40546, 40547, 40548, 40551, 40552, 40553, 40554, 40555, 40556, 40557, 40558, 40559, 40560, 40561, 40562, 40563, 40564, 40565, 40566, 40567, 40568, 40569, 40570, 40571, 40572, 40573, 40574, 40575, 40576, 40577, 40578, 40579, 40580, 40581, 40582, 40583, 40584, 40585, 40586, 40587, 40588, 40589, 40590, 40591, 40592, 40593, 40594, 40595, 40596, 40600, 40601, 40602, 40603, 40604, 40605, 40606, 40607, 40608, 40609, 40610, 40611, 40613, 40618, 40621, 40623, 40624, 40625, 40629, 40681, 40682, 40683, 40684, 40685, 40686, 40687, 40688, 40689, 40690, 40691, 40692, 40693, 40694, 40696, 40697, 40701, 40702, 40703, 40704, 40705, 40706, 40707, 40709, 40710, 40713, 40716, 40717, 40718, 40719, 40726, 40730, 40731, 40732, 40733, 40734, 40735, 40736, 40737, 40738, 40739, 40741, 40744, 40745, 40746, 40747, 40748, 40751, 40752, 40753, 40754, 40755, 40756, 40757, 40758, 40760, 40761, 40762);
        $partner_id = 4206;
        $resp = EstatementController::getMiddlewareSensitiveInfo($userIds, $partner_id);

        var_dump($resp);
        exit();
    }

    public static function getBsfReportsTestCustomers($start_date, $end_date, $partner_id) {
        //$partner= Partner::find($partner_id);
//        $queryDBUsers = "SELECT distinct(user_id) FROM partner_user where partner_id = 4206;";
//        $all_users_partner = DB::select($queryDBUsers);
//        foreach($all_users_partner as $u){
//            echo "<pre>";
//            var_dump($u->user_id);
//            exit();
//        }


        $AllCustomers = AutogeneratedReportController::ReportAllCustomersNew($start_date, $end_date, $partner_id);
        return $AllCustomers;
    }

}
