<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\App;
use View;
use Input;
use Validator;
use I;
use Response;
use Illuminate\Support\Facades\Redirect;
use App\Category as Category;
use ManagedObjectsHelper;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Category Controller
 *
 * @category   Controllers
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class CategoryController extends BaseController
{
    protected $layout = "layouts.dashboard";

    /**
     * Return the category validations
     *
     * @return array
     */
    private function getValidations()
    {
        return array(
            'name' => 'required|min:2'
        );
    }

    /**
     * Display a list of the Categories
     *
     * @return void
     */
    public function listCategories()
    {
        if(!I::can('view_categories')){
            return Redirect::to(url("dashboard"));
        }

        $categories = \App\Category::where('draft', false);
        if (Input::has('q') && Input::get('q') != '') {
            $categories = $categories->where('name', 'like', '%'.self::sanitizeText(Input::get('q')).'%');
        }

        $categories = $categories->paginate(15);

        return View::make("category.list", array(
            'categories' => $categories
        ));
    }

    /**
     * Fetch previous categories for the new category form
     *
     * @return void
     */
    public function newCategory()
    {
        if(!I::can('create_categories')){
            return Redirect::to(url("dashboard"));
        }

        $category                     = new Category();
        $category->name               = '';
        $category->parent_category_id = NULL;
        $category->draft              = TRUE;

        $category->save();

        $category_id = base64_encode($category->id);
        return Redirect::to(url("dashboard/categories/view/{$category_id}"));
    }

    /**
     * Create the new category
     *
     * @return void
     */
    public function createNewCategory()
    {
        if(!I::can('create_categories')){
            return Redirect::to(url("dashboard"));
        }

        $input = Input::all();
        foreach($input as $k=>$i){
            $input[$k] = self::sanitizeText($i);
        }
        $validator = Validator::make($input, $this->getValidations());

        if($validator->fails()) {
            return Redirect::to('dashboard/categories/new')->withErrors($validator)->withInput();
        }

        $category                     = new Category();
        $category->name               = self::sanitizeText(Input::get('name'));
        $category->parent_category_id = self::sanitizeText(Input::get('parent_category_id'));
        $category->draft              = false;

        $category->save();

        return Redirect::to('dashboard/categories');
    }

    /**
     * View a single Category resource
     *
     * @param int $id
     * @return void
     */
    public function viewCategory($enc_id = null)
    {
        $id = base64_decode($enc_id);
        if(!I::can('view_categories')){
            return Redirect::to(url("dashboard"));
        }
        $category = Category::find($id);

        if(!$category) {
            App::abort(404);
        }

        return View::make("category.view", array(
            'category'   => $category,
            'categories' => Category::where('draft', false)->orderBy('name')->pluck('name', 'id')
        ));
    }

    /**
     * Delete a Category instance
     *
     * @param int $id
     * @return void
     */
    public function deleteCategory($enc_id = null)
    {
        $id = base64_decode($enc_id);
        if(!I::can('delete_categories')){
            return Redirect::to(url("dashboard"));
        }
        $category = Category::find($id);

        if(!$category) {
            App::abort(404);
        }

        $category->delete();

        return Redirect::to('dashboard/categories');
    }

    /**
     * Update a category resource
     *
     * @param int $id
     * @return void
     */
    public function updateCategory($enc_id = null)
    {
        $id = base64_decode($enc_id);
        if(!I::can('edit_categories')){
            return Redirect::to(url("dashboard"));
        }
        return View::make("category.update", array(
            'id' => $id
        ));
    }

    /**
     * Update a category resource
     *
     * @param int $id
     * @return void
     */
    public function saveCategory($enc_id = null)
    {
        $id = base64_decode($enc_id);
        if(!I::can('edit_categories')){
            return Redirect::to(url("dashboard"));
        }
        $category = Category::find($id);

        if(!$category) {
            App::abort(404);
        }

        $category->draft = false;
        $input = Input::all();
        unset($input['redirect_to']);
        foreach($input as $k=>$i){
            $input[$k] = self::sanitizeText($i);
        }
        $validator = Validator::make($input, $this->getValidations());

        if($validator->fails()) {
            return Redirect::to('dashboard/categories/view/' . $enc_id)->withErrors($validator)->withInput();
        }

        $category->update($input);

        return Redirect::to(Input::get('redirect_to', 'dashboard/categories'));
    }

    /**
     * Load the new category popup
     *
     * @return void
     */
    public function newPopoverCategory()
    {
        if(!I::can('create_categories')){
            return Redirect::to(url("dashboard"));
        }
        $category       = new Category();
        $category->name = "New Category";

        $category->save();

        return View::make("category.new_popover", array(
                'category'   => $category,
                'categories' => Category::where('draft', false)->orderBy('name')->pluck('name', 'id')
            )
        );
    }

    /**
     * Save the popup category resource
     *
     * @param int $id
     * @return void
     */
    public function savePopoverCategory($enc_id = null)
    {
        $id = base64_decode($enc_id);
        if(!I::can('edit_categories')){
            return Redirect::to(url("dashboard"));
        }
        $category = category::find($id);

        if(!$category) {
            App::abort(404);
        }
        $input = Input::all();
        foreach($input as $k=>$i){
            $input[$k] = self::sanitizeText($i);
        }
        $validator = Validator::make($input, $this->getValidations());

        if($validator->fails()) {
            return Response::json(array(
                'failed' => true,
                'errors' => "Please ensure that you provide a valid category name"
            ));
        }

        $category->name               = self::sanitizeText(Input::get('name'));
        $category->parent_category_id = self::sanitizeText(Input::get('parent_category_id'));
        $category->draft              = false;
        $category->save();

        return Response::json(array(
            'failed'   => false,
            'view_url' => url('dashboard/categories/getList/')
        ));
    }

    /**
     * Return the select list of categories
     *
     * @return void
     */
    public function getCategoriesList()
    {
        if(!I::can('view_categories')){
            return Redirect::to(url("dashboard"));
        }
        return View::make("category.selectList", array(
            'categories' => Category::where('draft', false)->orderBy('name')->pluck('name', 'id')
        ));
    }

} // EOC