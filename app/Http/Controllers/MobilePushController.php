<?php
namespace App\Http\Controllers;
use App\Media;
use I;
use View;
use ManagedObjectsHelper;
use Illuminate\Support\Facades\Auth;
use Meta;
use Input;
use Validator;

use Illuminate\Support\Facades\Redirect;
use App\MobilePush as MobilePush;
/**
 * Mobile Push Controller
 *
 * @category   Controllers
 * @package    BLU
 * @author     Ralph Nader <ralph@bluloyalty.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class MobilePushController extends BaseController
{
    protected $layout = "layouts.dashboard";
    
    private function getValidations()
    {
        return array(
            'push_time'       => 'required',
            'message' => 'required',
            'activity' => 'required',
        );
    }

    /**
     * Display a list of the currencies
     *
     * @return void
     */
    public function getIndex()
    {
        if(!I::can('view_push_notifications')){
            return Redirect::to(url("dashboard"));
        }
        $this->ormClass      = 'App\MobilePush';
        $this->search_fields = ['name', 'message'];

		$allMobilePush	= $this->getList();
		$allManagedPartners		= ManagedObjectsHelper::managedPartners()->sortBy('name')->pluck('id')->toArray();
            $allowedMobilePushes	= array();
		foreach ($allMobilePush as $mobilePushe) {
			if(in_array($mobilePushe->partner_id,$allManagedPartners)){
					$allowedMobilePushes[] = $mobilePushe;
                    }
            }
        return View::make("mobilepush.list", array(
            'mobilepushs' => $allowedMobilePushes
        ));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getCreate()
    {
        if (!I::can('view_push_notifications') && !I::can('create_push_notifications')) {
            return Redirect::to(url("dashboard"));
        }
        $mobilepush           = new MobilePush();
        $mobilepush->message     = "New Mobile Push";
        $mobilepush->partner_id = Auth::User()->getTopLevelPartner()->id;
        $mobilepush->draft = 1;
        $mobilepush->save();

        $mobilePushId   = base64_encode($mobilepush->id);
        return Redirect::to(url("dashboard/mobilepush/edit/$mobilePushId"));
    }

    /**
     * Show the currency edit page
     *
     * @param integer $currency_id
     * @return Response
     */
    public function getEdit($enc_id)
    {
        $id = base64_decode($enc_id);
        if (!I::can('view_push_notifications') && !I::can('edit_push_notifications')) {
            return Redirect::to(url("dashboard"));
        }
        $mobilepush = MobilePush::find($id);
        $partners	= Auth::User()->managedPartners();

        if(!$mobilepush) {
            \App::abort(404);
        }

        $managedPartners = Auth::User()->managedPartners();
        $can_view_game_push = false;
        foreach($managedPartners as $manP){
                if($manP->id == $mobilepush->partner_id){
                    $can_view_game_push = true;
                    break;
                }
        }
        if($can_view_game_push== false){
            return Redirect::to(url("dashboard"));
        }
        
        return View::make("mobilepush.view", array('mobilepush'=> $mobilepush, 'partners'=> $partners));
    }

    /**
     * Delete the currency
     *
     * @param integer $currency_id
     * @return void
     */
    public function getDelete($enc_id)
    {
        $id = base64_decode($enc_id);
        if(!I::can('delete_push_notifications')){
            return Redirect::to(url("dashboard"));
        }
        $mobilepush = MobilePush::find($id);

        if(!$mobilepush) {
            App::abort(404);
        }

        $mobilepush->delete();

        return Redirect::to(url("dashboard/mobilepush"));
    }

    /**
     * apply changes
     *
     * @param integer $currency_id
     * @return Response
     */
    public function postUpdate($enc_id)
    {
        $id = base64_decode($enc_id);
        if (!I::can('edit_push_notifications')) {
            return Redirect::to(url("dashboard"));
        }
        $mobilepush = MobilePush::find($id);

        if(!$mobilepush) {
            App::abort(404);
        }
        
        $managedPartners = Auth::User()->managedPartners();
        $can_view_game_push = false;
        foreach($managedPartners as $manP){
            if($manP->id == $mobilepush->partner_id){
                $can_view_game_push = true;
                break;
            }
        }
        if($can_view_game_push== false && $mobilepush->draft == false){
            return Redirect::to(url("dashboard"));
        }
        $input = Input::all();
        foreach($input as $k=>$i){
            $input[$k] = self::sanitizeText($i);
        }
        $validator = Validator::make($input, $this->getValidations());

        if($validator->fails()) {
            return Redirect::to("dashboard/mobilepush/edit/$id")->withErrors($validator)->withInput();
        }

        $mobilepush->message           = self::sanitizeText(Input::get('message'));
        if(Input::has('push_time')) {
            $mobilepush->push_time = date("Y-m-d H:i:s", strtotime(self::sanitizeText(Input::get('push_time'))));
        } else {
            $mobilepush->push_time = date("Y-m-d H:i:s", strtotime("+1 day", time()) );
        }

        if(Input::hasFile('image')) {
            $file = Input::file('image');
            $path = public_path('uploads');
            $ext  = strtolower($file->getClientOriginalExtension());
            $size = $file->getSize();
            $type = $file->getMimeType();
            $name = md5(time()).".{$ext}";

            if(in_array($ext, Meta::allowedImageExts() )) {
                Input::file('image')->move($path, $name);

                $f = new Media();
                $f->name = $name;
                $f->size = $size;
                $f->type = $type;
                $f->ext  = $ext;
                $f->save();

                $mobilepush->media_id = $f->id;
            }
        }

        //$mobilepush->media_id       = self::sanitizeText(Input::get('media_id'));
        $mobilepush->activity          = self::sanitizeText(Input::get('activity'));
        $mobilepush->partner_id        = self::sanitizeText(Input::get('partner_id'));
        $mobilepush->draft = false;
        $mobilepush->save();

        return Redirect::to(Input::get('redirect_to', 'dashboard/mobilepush'));
    }
} // EOC