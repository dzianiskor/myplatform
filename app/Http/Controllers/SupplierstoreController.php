<?php
namespace App\Http\Controllers;
use App\Address;
use App\Supplierstore;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

/**
 * Store Controller
 * 
 * @category   Controllers
 * @package    BLU
 * @author     Ralph Nader <ralph@bluloyalty.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class SupplierstoreController extends BaseController
{
    /**
     * Fetch the select drop down data for the supplierstore popup
     *
     * @return array
     */
    private function getSelectData()
    {
        return array(
            'countries' => DB::table('country')->orderBy('name', 'asc')->get(),
            'areas'     => DB::table('area')->orderBy('name', 'asc')->get(),
            'cities'    => DB::table('city')->orderBy('name', 'asc')->get()
        );
    }

    /**
     * Create and return a default draft supplier supplierstore
     *
     * @param int $supplier_id
     * @return Result
     */
    private function getSupplierstoreDraft($supplier_id)
    {
        $supplierstore = new Supplierstore();

        $supplierstore->name       = 'New Store';
        $supplierstore->lat        = '33.904250';
        $supplierstore->lng        = '35.577072';
        $supplierstore->draft      = true;
        $supplierstore->supplier_id = $supplier_id;
        

        $supplierstore->save();

        $address = new Address();

        $address->country_id = 9;
        $address->area_id    = 76;
        $address->city_id    = 672;
        $address->street     = null;
        $address->floor      = null;
        $address->building   = null;

        $supplierstore->address()->save($address);

        return $supplierstore;
    }

    /**
     * Display the new supplierstore page
     *
     * @return View
     */
    public function newSupplierstore($supplier_id = null)
    {
        $data = array(
            'supplierstore'   => $this->getSupplierstoreDraft($supplier_id),
            'rnd_map' => 'gmap'
        );

        return View::make("supplierstore.form", array_merge(
            $data, $this->getSelectData()
        ));
    }

    /**
     * Display the edit supplierstore page
     *
     * @return View
     */
    public function editSupplierstore($supplierstore_id = null)
    {
        $data = array(
            'supplierstore'   => Supplierstore::find($supplierstore_id),
            'rnd_map' => 'gmap'
        );

        return View::make("supplierstore.form", array_merge(
            $data, $this->getSelectData()
        ));
    }

    /**
     * Create a new supplierstore resource
     *
     * @param int $supplierstore_id
     * @return void
     */
    public function updateSupplierstore($supplierstore_id = null)
    {
        $rules = array(
            'supplierstore_name' => 'required|min:1'
        );
        $input = Input::all();
        foreach($input as $k=>$i){
            $input[$k] = self::sanitizeText($i);
        }
        $validator = Validator::make($input, $rules);

        if($validator->fails()) {
            return Response::json(array(
                'failed' => true,
                'errors' => "Please ensure that you provide a valid store name"
            ));
        }

        $supplierstore = Supplierstore::find($supplierstore_id);

        // Details
        $supplierstore->name       = self::sanitizeText(Input::get('supplierstore_name'));
        $supplierstore->lng        = self::sanitizeText(Input::get('supplierstore_longitude'));
        $supplierstore->lat        = self::sanitizeText(Input::get('supplierstore_latitude'));
        
        $supplierstore->draft      = false;

        // Address
        $supplierstore->address->country_id = self::sanitizeText(Input::get('supplierstore_country', 0));
        $supplierstore->address->area_id    = self::sanitizeText(Input::get('supplierstore_area', 0));
        $supplierstore->address->city_id    = self::sanitizeText(Input::get('supplierstore_city', 0));
        $supplierstore->address->street     = self::sanitizeText(Input::get('supplierstore_street', null));
        $supplierstore->address->floor      = self::sanitizeText(Input::get('supplierstore_floor', null));
        $supplierstore->address->building   = self::sanitizeText(Input::get('supplierstore_building', null));


        $supplierstore->push();

        return Response::json(array(
            'failed' => false,
            'view_url' => url('dashboard/supplierstore/list/'.$supplierstore->supplier_id)
        ));
    }

    /**
     * Delete the specified supplierstore
     *
     * @return View
     */
    public function deleteSupplierstore($supplier_id, $supplierstore_id)
    {
        $supplierstore_id = base64_decode($supplierstore_id);
        Supplierstore::find($supplierstore_id)->delete();

        return Redirect::to(url('dashboard/suppliers/view/'.$supplier_id));
    }

    /**
     * Render a list of supplierstores relevant to the Partner
     *
     * @param int $supplier_id
     * @return View
     */
    public function listSupplierstores($supplier_id)
    {
        $data = array(
            'supplier_id' => $supplier_id,
            'supplierstores' => DB::table('supplier_store')->where('supplier_id', $supplier_id)->where('draft', false)->get()
        );

        return View::make("supplierstore.list", $data);
    }

} // EOC