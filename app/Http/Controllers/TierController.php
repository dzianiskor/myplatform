<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\App;
use View;
use Input;
use Validator;
use I;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use ManagedObjectsHelper;
use Illuminate\Support\Facades\DB;
use App\Tier as Tier;
use App\Tiercriteria as Tiercriteria;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Tier Controller
 * 
 * @category   Controllers
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class TierController extends BaseController
{
    protected $layout = "layouts.dashboard";

    private $auth_user = null;

    public function __construct()
    {
        parent::__construct();

        $this->auth_user = Auth::User();
    }       

    /**
     * Fetch the data for the tier view
     *
     * @return array
     */
    private function getData($id)
    {
        return array(
            'tiers'  => Tier::where('draft',false)->orderBy('name')->pluck('name','id'),
            'partners'         => ManagedObjectsHelper::managedPartners()->sortBy('name')->pluck('name', 'id')
        );
    }

    /**
     * Show a list of tiers
     *
     * @return void
     */
    public function listTiers()
    {
        if(!I::can('view_tiers') && !I::am("BLU Admin")){
            return Redirect::to(url("dashboard"));
        }
        $this->ormClass      = 'App\Tier';
        $this->search_fields = array('name', 'id');

        if(I::am('BLU Admin')){
            $tiers   = $this->getList();
            $page = Input::get('page');
            $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);

            $tiers = new LengthAwarePaginator($tiers->forPage($page, 15), $tiers->count(), 15, $page);
        } else {
//            $partner    = Auth::User()->getTopLevelPartner()->id;
            $partners    = Auth::User()->managedPartnerIDs();
            $tier       = Tier::select('tier.*');
            $tiers = $tier->whereHas('Partner', function($q) use($partners){
                $q->whereIn('partner_id', $partners);
            });

            if(Input::has('q')) {
                $tiers = $tier->where('tier.name', 'like', '%'.self::sanitizeText(Input::get('q')).'%');
            }

            $tiers = $tier->paginate(15);
        }

        return View::make("tier.list", array(
            'tiers' => $tiers
        ));
    }

    /**
     * Create a new partner draft and redirect to it
     *
     * @return void
     */
    public function newTier()
    {
        if(!I::can('create_tiers') && !I::am("BLU Admin")){
            return Redirect::to(url("dashboard"));
        }
        $tier             = new Tier();
        $tier->name       = "New Tier";
        $tier->draft      = true;
        $tier->save();
        $tierId = base64_encode($tier->id);
        return Redirect::to(url("dashboard/tiers/view/{$tierId}"));
    }

    /**
     * View a partner resource
     *
     * @param int $id
     * @return void
     */
    public function viewTier($enc_id = null)
    {
        $id = base64_decode($enc_id);
        if(!I::can('view_tiers') && !I::am("BLU Admin")){
            return Redirect::to(url("dashboard"));
        }
        $tier = Tier::find($id);

        if(!$tier) {
            App::abort(404);
        }
        
        $managedPartners = Auth::User()->managedPartners();
        $can_view_tier = false;
        foreach($managedPartners as $manP){
//            foreach($tier->partners as $p){
                //var_dump($p->id);
                if($manP->id == $tier->partner_id){
                    $can_view_tier = true;
                    break;
                }
//            }

        }
        if($can_view_tier== false && $tier->draft == false){
            return Redirect::to(url("dashboard"));
        }
        
        $this->ormClass      = 'App\Tier';
        $data = array(
            'tier'  => $tier,
            'criterias' => DB::table('tier_criteria')->where('tier_id', $id)->where('draft', false)->get()
        );

        return View::make("tier.view", array_merge($data, $this->getData($id)));
    }

    /**
     * Delete a partner resource
     *
     * @param int $id
     * @return void
     */
    public function deleteTier($enc_id = null)
    {
        $id = base64_decode($enc_id);
        if(!I::can('delete_tiers') && !I::am("BLU Admin")){
            return Redirect::to(url("dashboard"));
        }

        $tier = Tier::find($id);

        if(!$tier) {
            App::abort(404);
        }

        DB::table('tier')->where('id', $id)->delete();

        return Redirect::to('dashboard/tiers');
    }

    /**
     * Update a partner resource
     *
     * @param int $id
     * @return void
     */
    public function updateTier($enc_id = null)
    {
        $id = base64_decode($enc_id);
        if(!I::can('create_tiers') && !I::can('edit_tiers')){
            return Redirect::to(url("dashboard"));
        }
        $input = Input::all();
        foreach($input as $k=>$i){
            $input[$k] = self::sanitizeText($i);
        }
        $validator = Validator::make($input, array(
            'name'        => 'required|min:2'
        ));

        if($validator->fails()) {
            return Redirect::to(url("dashboard/tiers/view/$id"))->withErrors($validator);
        }

        $tier = Tier::find($id);

        if(!$tier) {
            App::abort(404);
        }

        $tier->name                     = self::sanitizeText(Input::get('name'));
        $tier->description              = self::sanitizeText(Input::get('description'));
        $tier->promotion_in_months      = self::sanitizeText(Input::get('promotion_in_months'));
        $tier->demotion_in_months       = self::sanitizeText(Input::get('demotion_in_months'));
        $tier->start_month              = self::sanitizeText(Input::get('start_month'));
        $tier->partner_id               = self::sanitizeText(Input::get('partner_id'));
        $tier->draft                    = false;          

        $tier->save();
        
        if(Input::has('tiercriteria_expiry_in_months')){
            $tierCriteria = Tiercriteria::where('tier_id',$id)->get();
            foreach($tierCriteria as $tierCriterion){
                $tierCriterion->expiry_in_months = self::sanitizeText(Input::get('tiercriteria_expiry_in_months'));
                $tierCriterion->save();
            }
        }

        return Redirect::to(Input::get('redirect_to', 'dashboard/tiers'))->with('success', 'Tier Updated');
    }
} // EOC