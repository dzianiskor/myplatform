<?php
namespace App\Http\Controllers;
use View;
use Input;
use Validator;
use Illuminate\Support\Facades\Redirect;
use App\LangKey as LangKey;
use App\Localizationkey as Localizationkey;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Localizationkey Controller
 * 
 * @category   Controllers
 * @package    BLU
 * @author     Ralph Nader <ralph@bluloyalty.com>
 * @copyright  2015 BLU
 * @link       http://blupoints.com
 */
class LocalizationkeyController extends BaseController
{
    protected $layout = "layouts.dashboard";

    /**
     * generate a reusable array with all the validations needed
     * for the Localizationkeys screen
     *
     * @return array
     */
    private function getValidations() {
        $rules = array(
            'key'           => 'required|min:2'
        );

        return $rules;
    }

    /**
     * Display a list of the localizationkeys
     *
     * @return void
     */
    public function listLocalizationkeys()
    {
        $this->ormClass      = 'App\Localizationkey';
        $this->search_fields = array('key');

        $partners = $this->getList();

        $page = Input::get('page');
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $partners = new LengthAwarePaginator($partners->forPage($page, 15), $partners->count(), 15, $page);

        return View::make("localizationkey.list", array(
            'localizationkeys' => $partners
        ));

    }

    /**
     * get fields for new Localizationkey
     *
     * @return void
     */
    public function newLocalizationkey()
    {
        return View::make("localizationkey.new");
    }

    /**
     * get fields for new Localizationkey
     *
     * @return void
     */
    public function createNewLocalizationkey()
    {
        $arr_input = array();
        $arr_input['key'] = self::sanitizeText(Input::get('key'));
        $arr_input['description'] = self::sanitizeText(Input::get('description'));
        $validator = Validator::make($arr_input, $this->getValidations());

        if($validator->fails()) {
            return Redirect::to('dashboard/localizationkeys/new')->withErrors($validator)->withInput();
        }
        $localizationkey = new Localizationkey();

        $localizationkey->key = self::sanitizeText(Input::get('key'));
        $localizationkey->description = self::sanitizeText(Input::get('description'));
        if(Input::has('is_platform_key')){
            $localizationkey->is_platform_key = self::sanitizeText(Input::get('is_platform_key'));
        }
        $localizationkey->save();

        //Localizationkey::create(Input::all());
        return Redirect::to('dashboard/localizationkeys');
    }

    /**
     * View a single Localizationkey entry
     *
     * @param int $id
     * @return void
     */
    public function viewLocalizationkey($enc_id = null)
    {
        $id = base64_decode($enc_id);
        $localizationkey = Localizationkey::find($id);

        if(!$localizationkey) {
            App::abort(404);
        }

        return View::make("localizationkey.view", array(
            'localizationkey' => $localizationkey
        ));
    }

    /**
     * Delete a Localizationkey instance
     *
     * @param int $id
     * @return void
     */
    public function deleteLocalizationkey($enc_id = null)
    {
        $id = base64_decode($enc_id);
        $localizationkey = Localizationkey::find($id);

        if(!$localizationkey) {
            App::abort(404);
        }

        $localizationkey->delete();

        return Redirect::to('dashboard/localizationkey');
    }

    /**
     * update a Localizationkey instance
     *
     * @param int $id
     * @return void
     */
    public function updateLocalizationkey($enc_id = null)
    {
        $id = base64_decode($enc_id);
        return View::make("localizationkey.update", array(
            'id' => $id
        ));
    }

    /**
     * update a Localizationkey instance
     *
     * @param int $id
     * @return void
     */
    public function saveLocalizationkey($enc_id = null)
    {
        $id = base64_decode($enc_id);
        $localizationkey = Localizationkey::find($id);

        if(!$localizationkey) {
            App::abort(404);
        }
        $input = Input::all();
        foreach($input as $k=>$i){
            $input[$k] = self::sanitizeText($i);
        }
        $validator = Validator::make($input, $this->getValidations());

        if($validator->fails()) {
            return Redirect::to('dashboard/localizationkeys/new')->withErrors($validator)->withInput();
        }

        
        //$localizationkey = Localizationkey::find($id);
        $localizationkey->key = self::sanitizeText(Input::get('key'));
        $localizationkey->description = self::sanitizeText(Input::get('description'));
        $localizationkey->is_platform_key = self::sanitizeText(Input::get('is_platform_key'));
        $localizationkey->save();

        return Redirect::to(Input::get('redirect_to', 'dashboard/localizationkeys'));
    }

} // EOC