<?php
namespace App\Http\Controllers;
/**
 * Product Model Translation Controller
 * 
 * @category   Controllers
 * @package    BLU
 * @author     Ralph Nader <ralph@bluloyalty.com>
 * @copyright  2015 BLU
 * @link       http://blupoints.com
 */
class ProdsubmodeltranslationController extends BaseController
{
    /**
     * Fetch the select drop down data for the prodsubmodeltranslation popup
     *
     * @return array
     */
    private function getSelectData()
    {
        return array(
            'languages'  => Language::orderBy('name')->pluck('name','id')
            );
    }

    /**
     * Create and return a default draft prodsubmodeltranslation
     *
     * @param int $prodsubmodel_id
     * @return Result
     */
    private function getProdsubmodeltranslationDraft($prodsubmodel_id)
    {
        $prodsubmodeltranslation = new Prodsubmodeltranslation();

        $prodsubmodeltranslation->name       = 'New Prodsubmodeltranslation';
        $prodsubmodeltranslation->prodsubmodel_id = $prodsubmodel_id;

        $prodsubmodeltranslation->save();

        return $prodsubmodeltranslation;
    }

    /**
     * Display the new prodsubmodeltranslation page
     *
     * @return View
     */
    public function newProdsubmodeltranslation($prodsubmodel_id = null)
    {
        $data = array(
            'prodsubmodeltranslation'   => $this->getProdsubmodeltranslationDraft($prodsubmodel_id)
        );

        return View::make("prodsubmodeltranslation.form", array_merge(
            $data, $this->getSelectData()
        ));
    }

    /**
     * Display the edit prodsubmodeltranslation page
     *
     * @return View
     */
    public function editProdsubmodeltranslation($prodsubmodeltranslation_id = null)
    {
        $data = array(
            'prodsubmodeltranslation'   => Prodsubmodeltranslation::find($prodsubmodeltranslation_id)
        );

        return View::make("prodsubmodeltranslation.form", array_merge(
            $data, $this->getSelectData()
        ));
    }

    /**
     * Create a new prodsubmodeltranslation resource
     *
     * @param int $prodsubmodeltranslation_id
     * @return void
     */
    public function updateProdsubmodeltranslation($prodsubmodeltranslation_id = null)
    {
        $rules = array(
            'prodsubmodeltranslation_name' => 'required|min:1'
        );

        $validator = Validator::make(Input::all(), $rules);

        if($validator->fails()) {
            return Response::json(array(
                'failed' => true,
                'errors' => "Please ensure that you provide a valid prodsubmodeltranslation name"
            ));
        }

        $prodsubmodeltranslation = Prodsubmodeltranslation::find($prodsubmodeltranslation_id);

        // Details
        $prodsubmodeltranslation->name       = self::sanitizeText(Input::get('prodsubmodeltranslation_name'));
        $prodsubmodeltranslation->draft      = false;
        $prodsubmodeltranslation->lang_id        = self::sanitizeText(Input::get('prodsubmodeltranslation_lang'));
        

        $prodsubmodeltranslation->push();

        return Response::json(array(
            'failed' => false,
            'view_url' => url('dashboard/prodsubmodeltranslation/list/'.$prodsubmodeltranslation->prodsubmodel_id)
        ));
    }

    /**
     * Delete the specified prodsubmodeltranslation
     *
     * @return View
     */
    public function deleteProdsubmodeltranslation($prodsubmodeltranslation_id = null)
    {
        Prodsubmodeltranslation::find($prodsubmodeltranslation_id)->delete();

        return Response::json(array(
            'failed' => false
        ));
    }

    /**
     * Render a list of prodsubmodeltranslations relevant to the Partner
     *
     * @param int $prodsubmodel_id
     * @return View
     */
    public function listProdsubmodeltranslations($prodsubmodel_id = null)
    {
        $data = array(
            'prodsubmodeltranslations' => Prodsubmodeltranslation::where('prodsubmodel_id', $prodsubmodel_id)->where('draft', false)->get()
        );

        return View::make("prodsubmodeltranslation.list", $data);
    }

} // EOC