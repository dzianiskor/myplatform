<?php
namespace App\Http\Controllers;
use App\Media;
use Meta;
use View;
use Input;
use Validator;
use I;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use ManagedObjectsHelper;
use App\CardPrint as CardPrint;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * CardPrint Controller
 *
 * @category   Controllers
 * @package    BLU
 * @author     Ralph Nader <ralph@bluloyalty.com>
 * @copyright  2016 BLU
 * @link       http://blupoints.com
 */
class CardPrintController extends BaseController
{
    protected $layout = "layouts.dashboard";

    /**
     * Fetch the contextual lists
     *
     * @return array
     */
    private function getAllLists()
    {
        return array(
            'partners'  => ManagedObjectsHelper::managedPartners()->sortBy('name')->pluck('name', 'id')
        );
    }

    /**
     * Display a list of the cardprints
     *
     * @return void
     */
    public function getIndex()
    {
        if(!I::can('view_card_print')){
            return Redirect::to(url("dashboard"));
        }
        $this->ormClass = 'App\CardPrint';
        $this->search_fields = array('name');

        $cardprints = $this->getList();

        $page = Input::get('page');
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $cardprints = new LengthAwarePaginator($cardprints->forPage($page, 15), $cardprints->count(), 15, $page);

        return View::make("cardprint.list", array(
            'cardprints' => $cardprints,
            'lists'   => $this->getAllLists()
        ));
    }

    /**
     * Create a new cardprint.
     *
     * @return Response
     */
    public function getCreate()
    {
        if(!I::can('create_card_print')){
            return Redirect::to(url("dashboard"));
        }
        
        $first_partner = array_values(ManagedObjectsHelper::managedPartners()->pluck('id')->toArray())[0];

                // Create cardprint

        $cardprint = new CardPrint();

        $cardprint->partner_id = $first_partner;
        $cardprint->save();
        $cardPrintId = base64_encode($cardprint->id);
        return Redirect::to(url("dashboard/cardprints/edit/{$cardPrintId}"));
    }

    /**
     * Edit an existing cardprint
     *
     * @return Response
     */
    public function getEdit($enc_id = null)
    {
        $id = base64_decode($enc_id);
        if(!I::can('view_card_print')){
            return Redirect::to(url("dashboard"));
        }
        $cardprint = CardPrint::find($id);

        if(!$cardprint) {
           App::abort(404);
        }
        $managedPartners = Auth::User()->managedPartners();
        $can_view_cardprint = false;
        foreach($managedPartners as $manP){
            if($manP->id == $cardprint->partner_id){
                $can_view_cardprint = true;
                break;
            }
        }
        if($can_view_cardprint== false && $cardprint->draft == false){
            return Redirect::to(url("dashboard"));
        }

        return View::make("cardprint.view", array_merge(
           array('cardprint' => $cardprint), $this->getAllLists()
        ));
    }

    /**
     * Update a cardprint
     *
     * @param int $id
     * @return Response
     */
    public function postUpdate($enc_id = null)
    {
        $id = base64_decode($enc_id);
        if(!I::can('edit_cardprints') && !I::can('create_card_print')){
            return Redirect::to(url("dashboard"));
        }
        $cardprint = CardPrint::find($id);

        if(!$cardprint) {
            App::abort(404);
        }
        $input = Input::all();
        foreach($input as $k=>$i){
            $input[$k] = self::sanitizeText($i);
        }
        $validator = Validator::make($input, array(
            'name' => 'required|min:2',
        ));

        if($validator->fails()) {
            return Redirect::to(url("dashboard/cardprints/view/$enc_id"))->withErrors($validator);
        }

        $cardprint->name        = self::sanitizeText(Input::get('name'));
        $cardprint->description = self::sanitizeText(Input::get('description'));
        $cardprint->top = self::sanitizeText(Input::get('top'));
        $cardprint->left = self::sanitizeText(Input::get('left'));

        $cardprint->partner_id  = self::sanitizeText(Input::get('partner_id'));
        $cardprint->draft       = false;
	

        if(Input::hasFile('image')) {
            $file = Input::file('image');
            $path = public_path('uploads');
            $ext  = strtolower($file->getClientOriginalExtension());
            $size = $file->getSize();
            $type = $file->getMimeType();
            $name = md5(time()).".{$ext}";

            if(in_array($ext, Meta::allowedImageExts() )) {
                Input::file('image')->move($path, $name);

                $f = new Media();
                $f->name = $name;
                $f->size = $size;
                $f->type = $type;
                $f->ext  = $ext;
                $f->save();

                $cardprint->media_id = $f->id;
            }
        }
        

        $cardprint->save();

        return Redirect::to(Input::get('redirect_to', 'dashboard/cardprints'));
    }

    /**
     * Delete a cardprint
     *
     * @param int $id
     * @return Response
     */
    public function getDelete($enc_id = null)
    {
        $id = base64_decode($enc_id);
        if (!I::can('delete_cardprints')) {
            App::abort(400);
        }

		

        CardPrint::find($id)->delete();

        return Redirect::to('dashboard/cardprints');
    }

	

} // EOC