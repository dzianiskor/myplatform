<?php
namespace App\Http\Controllers;
use App\TempTransaction;

/**
 * Carrier Controller
 *
 * @category   Controllers
 * @package    BLU
 * @author     Magid Mroueh
 * @copyright  2016 BLU
 * @link       http://blupoints.com
 */
class TempTransactionController extends BaseController
{
    protected $layout = "layouts.dashboard";

	/**
     * create new record in pending_transactions
     *
     * @return Response
     */
    public static function createTempTransaction($trx_temp){
        $bool_inserted = false;
        $tempTransaction = new TempTransaction();
	$tempTransaction->trx_reward    = $trx_temp->trx_reward;
        $tempTransaction->trx_redeem    = $trx_temp->trx_redeem;
        $tempTransaction->quantity      = $trx_temp->quantity;
        $tempTransaction->trans_class   = $trx_temp->trans_class;
        $tempTransaction->user_id       = $trx_temp->user_id;
        $tempTransaction->country_id    = $trx_temp->country_id;
        $tempTransaction->partner_id    = $trx_temp->partner_id;
        $tempTransaction->source        = $trx_temp->source;
        $tempTransaction->ref_number    = $trx_temp->ref_number;
        $tempTransaction->notes         = $trx_temp->notes;
        $tempTransaction->store_id      = $trx_temp->store_id;
        $tempTransaction->pos_id        = $trx_temp->pos_id;
        $tempTransaction->auth_staff_id = $trx_temp->auth_staff_id;
        $tempTransaction->network_id    = $trx_temp->network_id;
        $tempTransaction->rule_id       = $trx_temp->rule_id;
        $tempTransaction->cash_payment  = $trx_temp->cash_payment;
        $tempTransaction->delivery_cost = $trx_temp->delivery_cost;
	$tempTransaction->currency_id	= $trx_temp->currency_id;
        try {
            $tempTransaction->save();
            $bool_inserted = true;
        }

          //catch exception
        catch(Exception $e) {
            $bool_inserted = false;
        }
        if($bool_inserted === true){
            return True;
        }
        else{
            return False;
        }
	//$result	= TempTransaction::insert($tempTransaction);

	

    }


	/**
     * create new record in pending_transactions
     *
     * @return Response
     */
    public static function deleteTempTransaction( $user_id )
    {
		$result	= TempTransaction::where('user_id', $user_id)->delete();

		return $result;
    }


} // EOC