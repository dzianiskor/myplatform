<?php
namespace App\Http\Controllers;
use App\Card;
use I;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;

/**
 * Card Controller
 * 
 * @category   Controllers
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class CardController extends BaseController {

    /**
     * Save a new card and associate it to the user account
     *
     * @param int $user_id
     * @return Response
     */
    public function saveCard($user_id = null)
    {
        // @TODO: ensure that the card has not been used 
        
        if(!I::can('create_cards')){
                return Redirect::to(url("dashboard"));
            }
//        $validator = Validator::make(
//            Input::all(), array('card_number' => 'required|digits:9')
//        );
//
//        if ($validator->fails()) {
//            $card_errors = $validator->messages()->toArray();
//
//            return Response::json(array(
//                'fails'   => true,
//                'message' => implode("\n", $card_errors['card_number'])
//            ));
//        }

        $card = Card::where('number', self::sanitizeText(Input::get('card_number')))->first();

        if(!$card) {
            return Response::json(array(
                'fails'   => true,
                'message' => "The card number provided does not match a valid Card"
            ));
        }

        if($card->user_id > 0 && $card->user_id != $user_id) {
            return Response::json(array(
                'fails'   => true,
                'message' => "The card number provided has already been used"
            ));
        }

        // Assign to this user
        $partner_id = Auth::User()->getTopLevelPartner()->id;
        $card->partner_id =$partner_id;
        $card->user_id = $user_id;
        $card->save();

        return $this->listCards($user_id);
    }

    /**
     * Create a small LI list with user cards
     *
     * @param int $user_id
     * @return Response
     */
    public function listCards($user_id)
    {
        if(!I::can('assign_cards')){
                return Redirect::to(url("dashboard"));
            }
        return View::make('card.userlist', array(
            'cards' => DB::table('card')->where('user_id', $user_id)->whereIn('partner_id', Auth::User()->managedPartnerIDs())->get()
        ));
    }

    /**
     * Unlink the specified card from the user account
     *
     * @param int $user_id
     * @param int $card_id
     * @return Response
     */
    public function unlinkCard($user_id = null, $card_id = null)
    {
        if(!I::can('assign_cards')){
                return Redirect::to(url("dashboard"));
            }
        $user = User::find($user_id);

        if(!$user) {
            App::abort(404);
        }

        $card = $user->cards()->find($card_id);

        if(!$card) {
            App::abort(404);
        }

        $card->delete();

        return $this->listCards($user_id);
    }

} // EOC