<?php
namespace App\Http\Controllers;
use View;
use Input;
use Validator;
use I;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Batch as Batch;
use App\CardPrint as CardPrint;
use App\Store as Store;
use App\Pos as Pos;
use UCID;
use PinCode;
use ManagedObjectsHelper;
use CardSecurity;
use Illuminate\Support\Facades\DB;
use CSVHelper;
use Response;
use RefID;
use App\User as User;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Generator Controller
 *
 * @category   Controllers
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class GeneratorController extends BaseController {

    /**
     * Fetch the validations
     *
     * @return array
     */
    private function getValidations()
    {
        return array(
            'generate-type' => 'required'
        );
    }

    /**
     * Fetch all contextual list data
     *
     * @return array
     */
    private function getAllLists()
    {
        return array(
            'partners' => ManagedObjectsHelper::managedPartners()->pluck('name', 'id'),
        );
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
        if(!I::can('view_batches')){
            return Redirect::to(url("dashboard"));
        }
        $constraint         = new \stdClass();
        $constraint->leftv  = 'type';
        $constraint->eq     = '!=';
        $constraint->rightv = 'Coupons';

        $this->ormClass      = 'App\Batch';
        $this->search_fields = array('name');
        $this->constraint    = $constraint;

        if(I::am('BLU Admin')){
            $batches   = $this->getList();

            $page = Input::get('page');
            $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
            $batches = new LengthAwarePaginator($batches->forPage($page, 15), $batches->count(), 15, $page);
        } else {
            $partner = Auth::User()->getTopLevelPartner()->id;
            $batche = Batch::select('batch.*');
            $batches = $batche->whereHas('Partner', function($q) use($partner){
                $q->where('partner_id', $partner);
            });
            
                          
            if(Input::has('q')) {
                $batches = $batche->where('batch.name', 'like', '%'.self::sanitizeText(Input::get('q')).'%');
            }
            
            $batches = $batches->paginate(15);
        }
        
        return View::make("generator.batch.list", array(
            'batches' => $batches
        ));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function getCreate()
	{
        $batch        = new Batch();
        $batch->draft = true;

        $batch->save();
        $batchId = base64_encode($batch->id);
        return Redirect::to(url("dashboard/generator/edit/{$batchId}"));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getEdit($enc_id = null)
	{
            $id = base64_decode($enc_id);
            if(!I::can('view_batches')){
                return Redirect::to(url("dashboard"));
            }
        $batch = Batch::find($id);

        if(!$batch) {
            \App::abort(404);
        }

        return View::make("generator.batch.view", array_merge(
            array('batch' => $batch), $this->getAllLists()
        ));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function postUpdate($enc_id = null)
	{
            $id = base64_decode($enc_id);
            if(!I::can('view_batches')){
                return Redirect::to(url("dashboard"));
            }
        $batch = Batch::find($id);

        if(!$batch) {
            \App::abort(404);
        }
        $input = Input::all();
        foreach($input as $k=>$i){
            $input[$k] = self::sanitizeText($i);
        }
        $validator = Validator::make($input, $this->getValidations());

        if($validator->fails()) {
            return Redirect::to('dashboard/generator/edit/'.$enc_id)->withErrors($validator)->withInput();
        }

        $batch->type  = self::sanitizeText(Input::get('generate-type'));
        $batch->draft = false;
        $partner_id = self::sanitizeText(Input::get('partner_id'));
        $partner_id_card = self::sanitizeText(Input::get('partner_id_card'));
        
        switch($batch->type) {

            # Format: 9 digit unique numbers
            case "Cards":
                $start_number = CardSecurity::newNumber();
                $end_number   = (int)($start_number + self::sanitizeText(Input::get('card_count')));

                $buffer = array();

                for($i = $start_number; $i < $end_number; $i++) {
                    $buffer[] = array(
                        'number'     => $i,
                        'batch_id'   => $id,
                        'created_at' => date('Y-m-d H:i:s', time()),
                        'updated_at' => date('Y-m-d H:i:s', time()),
                        'partner_id' => $partner_id_card,
                    );
                }

                DB::table('card')->insert($buffer);

                $batch->number_of_items = count($buffer);
                $batch->partner_id      = $partner_id_card;

                break;

            # Format: Prefix + 6 Digit unique numbers
            case "References":
                $start_number = RefID::newNumber();
                $end_number   = (int)($start_number + self::sanitizeText(Input::get('reference_count')));
                
                $buffer  = array();
                $members = array();

                for($i = $start_number; $i < $end_number; $i++) {

                    $user = new User();
                    $user->ref_account = true;
					$user->first_name  = self::sanitizeText(Input::get('prefix'));
					$user->last_name   = $i;
                    $user->passcode    = PinCode::newCode();
                    $user->ucid        = UCID::newCustomerID();
                    $user->country_id  = 6;
                    $user->area_id     = 46;
                    $user->city_id     = 3217;
                    $user->dob         = '1975-01-01';
                    $user->draft       = false;
                    $user->status      = 'active';
                    $user->verified = 1;
                    
                    //set the registration source POS
                    $storeDetails = Store::where('partner_id', $partner_id)->where('name', 'System Generated References')->first();
                    if($storeDetails){
                    $posDetails   = Pos::where('store_id', $storeDetails->id)->where('name', 'System Generated References POS')->first();
                        if($posDetails){
                            $user->registration_source_pos         = $posDetails->id;
                        }
                    }
                    $user->save();

                    DB::table('partner_user')->insert(array(
                        'partner_id' => $partner_id,
                        'user_id'    => $user->id,
                        'type'       => 'static'
                    ));
                    
                    DB::table('partner_user')->insert(array(
                        'partner_id' => 1,
                        'user_id'    => $user->id,
                        'type'       => 'static'
                    ));

                    $buffer[] = array(
                        'number'     => self::sanitizeText(Input::get('prefix')).$i,
                        'batch_id'   => $id,
                        'pin'        => $user->passcode,
                        'user_id'    => $user->id,
                        'created_at' => date('Y-m-d H:i:s', time()),
                        'updated_at' => date('Y-m-d H:i:s', time()),
                         'partner_id' => $partner_id,
                    );
                }

                // Save

                DB::table('reference')->insert($buffer);

                // Associate reference to user

                $batch->number_of_items = count($buffer);
                $batch->partner_id      = $partner_id;

                break;
        }

        $batch->save();

        return Redirect::to('dashboard/generator');
	}

    /**
     * Download a specified batch of items
     *
     * @param string $id
     * @param string $format [csv, xls]
     * @return void
     */
    public function getDownload($enc_id = null, $format = 'csv')
    {
        if(!I::can('view_batches')){
                return Redirect::to(url("dashboard"));
            }
        $id = base64_decode($enc_id);

        $batch = Batch::find($id);

        if(!$batch) {
            \App::abort(404);
        }

        $buffer = array();

        if($batch->type == 'Cards') {

            $buffer[] = array('Number', 'Created At');

            foreach($batch->cards->toArray() as $i) {
                $buffer[] = array_flatten(array_only($i, array('number', 'created_at')));
            }
        } else {

            $buffer[] = array('Number', 'Pin', 'Created At');

            foreach($batch->references->toArray() as $i) {
                $buffer[] = array_flatten(array_only($i, array('number', 'pin', 'created_at')));
            }
        }

        $file = storage_path()."/generator/batch_{$batch->type}_{$batch->id}.{$format}";

        switch($format)
        {
            case "csv":
                CSVHelper::writeToFile($buffer, $file);
                break;
            case "xls":
                CSVHelper::writeToFile($buffer, $file);
                break;
        }

        return Response::download($file);
    }
    
    /**
     * Card Print a specified batch of items
     *
     * @param string $id
     * @return void
     */
    public function getListbatch($enc_id = null)
    {
        if(!I::can('view_batches')){
                return Redirect::to(url("dashboard"));
            }
        $id = base64_decode($enc_id);

        $batch = Batch::find($id);

        if(!$batch) {
            \App::abort(404);
        }

        $list_cards = array();

        if($batch->type == 'Cards') {
            foreach($batch->cards->toArray() as $i) {
                $list_cards[] = $i;
            }
        }
        return View::make("generator.batch.listbatch", array('batch' => $list_cards));
    }
    
    /**
     * Card Print a specified batch of items
     *
     * @param string $id
     * @return void
     */
    public function getPrintcard($enc_id = null){
        if(!I::can('view_batches')){
                return Redirect::to(url("dashboard"));
            }
        $id = base64_decode($enc_id);

        $card = Card::find($id);

        if(!$card) {
            \App::abort(404);
        }
        $arr_card_details = array();
        $background = CardPrint::where('partner_id',$card->partner_id)->first();
        if($background){
            $arr_card_details['card_background'] = "/media/image/".$background->media_id;
            $arr_card_details['card_top'] = $background->top;
            $arr_card_details['card_left'] = $background->left;
            $arr_card_details['card_number'] = $card->number;
        }
        return View::make("generator.batch.cardprint", array('card_details'=>$arr_card_details));
    }

} // EOC