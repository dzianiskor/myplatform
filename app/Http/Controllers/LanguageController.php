<?php
namespace App\Http\Controllers;
use View;
use Input;
use Validator;

use Illuminate\Support\Facades\Redirect;

use App\Language as Language;

/**
 * Language Controller
 * 
 * @category   Controllers
 * @package    BLU
 * @author     Ralph Nader <ralph@bluloyalty.com>
 * @copyright  2015 BLU
 * @link       http://blupoints.com
 */
class LanguageController extends BaseController
{
    protected $layout = "layouts.dashboard";

    /**
     * generate a reusable array with all the validations needed
     * for the Language screen
     *
     * @return array
     */
    private function getValidations() {
        $rules = array(
            'name'           => 'required|min:2'
        );

        return $rules;
    }

    /**
     * Display a list of the languages
     *
     * @return void
     */
    public function listLanguages()
    {
        $this->ormClass      = 'App\Language';
        $this->search_fields = array('name');

        return View::make("language.list", array(
            'languages' => $this->getList()
        ));

    }

    /**
     * get fields for new Language
     *
     * @return void
     */
    public function newLanguage()
    {
        return View::make("language.new");
    }

    /**
     * get fields for new Language
     *
     * @return void
     */
    public function createNewLanguage()
    {
        $input = Input::all();
        foreach($input as $k=>$i){
            $input[$k] = self::sanitizeText($i);
        }
        $validator = Validator::make($input, $this->getValidations());

        if($validator->fails()) {
            return Redirect::to('dashboard/languages/new')->withErrors($validator)->withInput();
        }
        $language = new Language();

        $language->name = self::sanitizeText(Input::get('name'));
        $language->short_code = self::sanitizeText(Input::get('short_code'));
        $language->save();

        //Language::create(Input::all());
        return Redirect::to('dashboard/languages');
    }

    /**
     * View a single Language entry
     *
     * @param int $id
     * @return void
     */
    public function viewLanguage($enc_id = null)
    {
        $id = base64_decode($enc_id);
        $language = Language::find($id);

        if(!$language) {
            App::abort(404);
        }

        return View::make("language.view", array(
            'language' => $language
        ));
    }

    /**
     * Delete a Language instance
     *
     * @param int $id
     * @return void
     */
    public function deleteLanguage($enc_id = null)
    {
        $id = base64_decode($enc_id);
        $language = Language::find($id);

        if(!$language) {
            App::abort(404);
        }

        $language->delete();

        return Redirect::to('dashboard/language');
    }

    /**
     * update a Language instance
     *
     * @param int $id
     * @return void
     */
    public function updateLanguage($enc_id = null)
    {
        $id = base64_decode($enc_id);
        return View::make("language.update", array(
            'id' => $id
        ));
    }

    /**
     * update a Language instance
     *
     * @param int $id
     * @return void
     */
    public function saveLanguage($enc_id = null)
    {
        $id = base64_decode($enc_id);
        $language = Language::find($id);

        if(!$language) {
            App::abort(404);
        }
        $input = Input::all();
        foreach($input as $k=>$i){
            $input[$k] = self::sanitizeText($i);
        }
        $validator = Validator::make($input, $this->getValidations());

        if($validator->fails()) {
            return Redirect::to('dashboard/languages/new')->withErrors($validator)->withInput();
        }

        
        //$language = Language::find($id);
        $language->name = self::sanitizeText(Input::get('name'));
        $language->short_code = self::sanitizeText(Input::get('short_code'));
        $language->save();
        
        return Redirect::to('dashboard/languages');
    }

} // EOC