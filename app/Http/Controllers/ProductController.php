<?php
namespace App\Http\Controllers;
use App\Media;
use Illuminate\Support\Facades\Config;
use UCID;
use View;
use Input;
use Validator;
use Session;
use Security;
use I;
use Illuminate\Support\Facades\Auth;
use App\BluCollection as BluCollection;
use AuditHelper;
use Illuminate\Support\Facades\Redirect;
use Transaction;
use App\Partner as Partner;
use Notification;
use Ticket;
use Language;
use Localizationkey;
use LangKey;
use ManagedObjectsHelper;
use User;
use App\Country;
use App\Admin as Admin;
use App\Role as Role;
use App\City as City;
use App\Area as Area;
use Meta;
use App\ProductPartnerRedemption as ProductPartnerRedemption;
use App\ProductPartnerReward as ProductPartnerReward;
use App\Category as Category;
use App\Brand as Brand;
use App\Supplier as Supplier;
use App\Prodmodel as Prodmodel;
use App\ProdmodelPartner as ProdmodelPartner;
use App\Network as Network;
use App\Currency as Currency;
use App\Product as Product;
use App\Segment as Segment;
use App\Tier as Tier;
use App\Tiercriteria as Tiercriteria;
use App\ProductRedemptionCountries as ProductRedemptionCountries;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;

/**
 * Product Controller
 *
 * @category   Controllers
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class ProductController extends BaseController {

    protected $layout = "layouts.dashboard";

    /**
     * Get all list data
     *
     * @return array
     */
    private function getAllLists()
    {
        return array(
            'brands' => Brand::where('draft', false)->orderBy('name')->pluck('name', 'id'),
            'suppliers' => Supplier::where('draft', false)->orderBy('name')->pluck('name', 'id'),
            'categories' => Category::with('children')->where('parent_category_id', 1)->where('draft', false)->orderBy('name', 'asc')->pluck('name', 'id'),
            'networks' => Network::orderBy('name')->pluck('name', 'id'),
            'partners' => ManagedObjectsHelper::managedPartners()->sortBy('name')->pluck('name', 'id'),
            'countries' => Country::orderBy('name')->pluck('name', 'id'),
            'segments' => ManagedObjectsHelper::managedSegments()->sortBy('name')->pluck('name', 'id'),
            'delivery_options' => Meta::deliveryOptions(),
            'currencies' => Currency::where('draft', false)->orderBy('name')->pluck('name', 'id'),
            'prefixes' => ManagedObjectsHelper::managedPartners()->sortBy('prefix')->pluck('prefix', 'prefix')
        );
    }

    /**
     * Return the product validations
     *
     * @return array
     */
    private function getValidations() {
        return array(
            'name' => 'required',
            'brand_id' => 'required',
            'category_id' => 'required',
            'start_date' => 'nullable|date',
            'end_date' => 'nullable|date',
            'qty' => 'numeric|min:0',
            'original_price' => 'numeric|min:0',
            'original_retail_price' => 'numeric|min:0',
            'original_sales_tax' => 'numeric|min:0',
            'weight' => 'numeric|min:0',
            'volume' => 'numeric|min:0',
            'delivery_charges' => 'numeric|min:0',
        );
    }

    /**
     * Display a list of the products
     *
     * @return void
     */
    public function listProducts(Request $request)
    {
        if (!I::can('view_products')) {
            return Redirect::to(url("dashboard"));
        }

        $this->ormClass = 'App\Product';
        $this->defaultSort = 'id';
        $this->search_fields = array('name');

        if (I::am('BLU Admin Dev')) {
            $products = $this->getList();

            $page = Input::get('page');
            $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
            $products = new LengthAwarePaginator($products->forPage($page, 15), $products->count(), 15, $page);
        } else {
            $partner = Auth::User()->getTopLevelPartner()->id;

            $product = Product::select('product.*');
            $products = $product->whereHas('Partners', function($q) use($partner) {
                $q->where('partner_id', $partner);
            });

            if (Input::has('q')) {
                $q = self::sanitizeText(Input::get('q'));
                $products = $product->where('product.name', 'like', '%' . $q . '%')->orWhere('product.id', 'like', '%' . $q . '%');
            }

            // filter supplier_id
            if (Input::has('supplier_list')) {
                $q = explode(',', Input::get('supplier_list'));
                if (sizeof($q) == 1) {
                    $q = urldecode($q[0]);
                    $q = explode(',', $q);
                }

                $products = $product->whereIn('supplier_id', $q);
            }

            // filter country_id
            if (Input::has('country_list')) {

                $q = explode(',', Input::get('country_list'));
                if (sizeof($q) == 1) {
                    $q = urldecode($q[0]);
                    $q = explode(',', $q);
                }

                $products = $product->whereHas('countries', function($u) use($q) {
                    $u->whereIn('country_id', $q);
                });
            }

            // filter partner_id
            if (Input::has('partner_list')) {

                $q = explode(',', Input::get('partner_list'));
                if (sizeof($q) == 1) {
                    $q = urldecode($q[0]);
                    $q = explode(',', $q);
                }

                $products = $product->whereHas('Partners', function($u) use($q) {
                    $u->whereIn('partner_id', $q);
                });
            }

            // filter segment_id  && explode(',', Input::get('partner_id')) != '0'
            if (Input::has('displaychannel_list')) {
                $q = explode(',', Input::get('displaychannel_list'));
                if (sizeof($q) == 1) {
                    $q = urldecode($q[0]);
                    $q = explode(',', $q);
                }

                $products = $product->whereHas('channels', function($u) use($q) {
                    $u->whereIn('channel_id', $q);
                });
            }

            // filter supplier_id
            if (Input::has('brand_list')) {

                $q = explode(',', Input::get('brand_list'));

                if (sizeof($q) == 1) {
                    $q = urldecode($q[0]);
                    $q = explode(',', $q);
                }
                $products = $product->whereIn('brand_id', $q);
            }

            $products = $product->paginate(15);
        }

        $query_string = $request->getQueryString();

        return View::make("product.list", array(
            'products' => $products,
            'lists' => $this->getAllLists(),
            'query_string' => $query_string
        ));
    }

    /**
     * get fields for new product
     *
     * @return void
     */
    public function newProduct() {
        if (!I::can('create_products')) {
            return Redirect::to(url("dashboard"));
        }
        $product = new Product();

        $product->ucid = UCID::newProductID();
        $product->start_date = null;
        $product->end_date = null;
        $product->draft = true;
        $product->status = 'active';
        $product->price_in_usd = 0;

        $product->save();

        // Link to top level partner automtically

        $partner = Auth::User()->getTopLevelPartner();

        $product->partners()->attach($partner->id);

        // Associate to partner location

        DB::table('country_product')->insert(array(
            'country_id' => $partner->country->id,
            'product_id' => $product->id
        ));

        // Associate to partner as a channel

        DB::table('channel_product')->insert(array(
            'product_id' => $product->id,
            'channel_id' => $partner->id
        ));
        $productId = base64_encode($product->id);
        return Redirect::to(url("dashboard/catalogue/view/{$productId}"));
    }

    /**
     * View a single Product entry
     *
     * @param int $id
     * @return void
     */
    public function viewProduct($enc_id = null) {
        $id = base64_decode($enc_id);
        if (!I::can('view_products')) {
            return Redirect::to(url("dashboard"));
        }
        $product = Product::find($id);

        if (!$product) {
            \App::abort(404);
        }

        $managedPartners = Auth::User()->managedPartners();
        $can_view_product = false;
        foreach ($managedPartners as $manP) {
            foreach ($product->partners as $p) {
                //var_dump($p->id);
                if ($manP->id == $p->id) {
                    $can_view_product = true;
                    break;
                }
            }
        }
        if ($can_view_product == false && $product->draft == false) {
            return Redirect::to(url("dashboard"));
        }
        return View::make("product.view", array_merge(
            $this->getAllLists(), array('product' => $product)
        ));
    }

    /**
     * Delete a product instance
     *
     * @param int $id
     * @return void
     */
    public function deleteProduct($enc_id = null) {
        $id = base64_decode($enc_id);
        if (!I::can('delete_products')) {
            return Redirect::to(url("dashboard"));
        }
        $product = Product::find($id);

        if (!$product) {
            \App::abort(404);
        }

        $product->delete();

        return Redirect::to('dashboard/catalogue');
    }

    /**
     * Update a product
     *
     * @param int $id
     * @return void
     */
    public function saveProduct(Request $request, $enc_id)
    {
        if ($request->isMethod('get')) {
            return redirect('/dashboard/catalogue');
        }

        $id = base64_decode($enc_id);
        if (!I::can('edit_products') && !I::can('create_products')) {
            return Redirect::to(url("dashboard"));
        }
        $product = Product::find($id);

        if (!$product) {
            \App::abort(404);
        }
        $input = Input::all();
        unset($input['redirect_to']);
        foreach ($input as $k => $i) {
            $input[$k] = self::sanitizeText($i);
        }

        $validator = Validator::make($input, $this->getValidations());

        if ($validator->fails()) {
            return Redirect::to(url("dashboard/catalogue/view/{$enc_id}"))->withErrors($validator)->withInput();
        }

        // Check for cover image upload
        if (Input::hasFile('cover_image')) {
            $file = Input::file('cover_image');
            $path = public_path('uploads');
            $ext = strtolower($file->getClientOriginalExtension());
            $size = $file->getSize();
            $type = $file->getMimeType();
            $name = md5(time()) . ".{$ext}";

            if (in_array($ext, Meta::allowedImageExts())) {
                Input::file('cover_image')->move($path, $name);

                $f = new Media();
                $f->name = $name;
                $f->size = $size;
                $f->type = $type;
                $f->ext = $ext;
                $f->save();

                $input['cover_image'] = $f->id;
            }
        }

        // Check for multiple uploads
        if (Input::hasFile('image')) {
            $images = Input::file('image');

            if (!is_array($images)) {
                $images = array($images);
            }

            foreach ($images as $file) {
                $path = public_path('uploads');
                $ext = strtolower($file->getClientOriginalExtension());
                $size = $file->getSize();
                $type = $file->getMimeType();
                $name = md5(time()) . ".{$ext}";

                if (in_array($ext, Meta::allowedImageExts())) {
                    Input::file('image')->move($path, $name);

                    $f = new Media();
                    $f->name = $name;
                    $f->size = $size;
                    $f->type = $type;
                    $f->ext = $ext;
                    $f->save();

                    // Link to product
                    $product->media()->attach($f);
                }
            }
        }
        unset($input['image']);

        foreach ($input as $k => $v) {
            if ($v == '') {
                unset($input[$k]);
            }
        }

        // Display Control
        if (!Input::has('display_online')) {
            $input['display_online'] = false;
        }

        // Display offline
        if (!Input::has('show_offline')) {
            $input['show_offline'] = false;
        }

        // Voucher Control
        if (!Input::has('is_voucher')) {
            $input['is_voucher'] = false;
        }

        if (!Input::has('hot_deal')) {
            $input['hot_deal'] = false;
        }

        // original prices
        $input['currency_id'] = self::sanitizeText(Input::get('currency_id', 0));
        $input['original_price'] = floatval(self::sanitizeText(Input::get('original_price', 0)));
        $input['original_retail_price'] = floatval(self::sanitizeText(Input::get('original_retail_price', 0)));
        $input['original_sales_tax'] = floatval(self::sanitizeText(Input::get('original_sales_tax', 0)));

        //calculate prince in usd and points
        $currency = Currency::where('id', $input['currency_id'])->first();
        $input['price_in_usd'] = $input['original_price'] / $currency->latestRate();
        $input['retail_price_in_usd'] = $input['original_retail_price'] / $currency->latestRate();
        $input['sales_tax_in_usd'] = $input['original_sales_tax'] / $currency->latestRate();

        // Convert the points
        $input['price_in_points'] = floatval($input['price_in_usd']) / Config::get('blu.points_rate');

//		// Convert the points
//        $input['price_in_points'] = (float)self::sanitizeText(Input::get('price_in_usd', 0)) / Config::get('blu.points_rate');
        // Delivery Charges update
        $input['delivery_charges'] = floatval(self::sanitizeText(Input::get('delivery_charges', 0)));
        $input['delivery_currency_id'] = self::sanitizeText(Input::get('delivery_currency_id', 0));

        if (Input::has('start_date') && Input::has('end_date') && Input::get('end_date') != '' && Input::get('start_date') != '') {
            if(strtotime(self::sanitizeText(Input::get('end_date'))) < strtotime(self::sanitizeText(Input::get('start_date')))){
                return Redirect::to(url("dashboard/catalogue/view/{$enc_id}"))->withErrors(array('The end date must be a date after or equal start date.'))->withInput();
            }
            $input['start_date'] = date('Y-m-d', strtotime($input['start_date']));
            $input['end_date'] = date('Y-m-d', strtotime($input['end_date']));
        }
        $prefix = self::sanitizeText(Input::get('partner_prefix', ""));
        $mapping_id = self::sanitizeText(Input::get('partner_mapping_id', ""));
        $partnerMappingId = $prefix . $mapping_id;
        $input['partner_mapping_id'] = $partnerMappingId;
        unset($input['partner_prefix']);
        $product->draft = false;
        $product->update($input);

        return Redirect::to(Input::get('redirect_to', 'dashboard/catalogue'));
    }

    /**
     * Create a small LI list with product partners
     *
     * @param int $user_id
     * @return Response
     */
    private function listPartners($enc_product_id) {
        $product_id = base64_decode($enc_product_id);
        $product = Product::find($product_id);
        return View::make('product.list.partnerList', array(
            'product' => $product,
        ));
    }

    /**
     * link a partner to a product
     *
     * @param int $product id
     * @return void
     */
    public function postLink_partner($enc_id) {
        $id = base64_decode($enc_id);
        $partner_id = self::sanitizeText(Input::get('partner_id'));
        $product = Product::find($id);
        $partner = Partner::find($partner_id);

        if (!$product || !$partner) {
            \App::abort(404);
        }

        if (!$product->partners->contains($partner)) {
            $product->partners()->attach($partner);
        }
        return $this->listPartners($enc_id);
    }

    /**
     * unlink a partner froma product
     *
     * @param int $product id
     * @return void
     */
    public function getUnlink_partner($enc_product_id, $_enc_partner_id) {
        $product_id = base64_decode($enc_product_id);
        $partner_id = base64_decode($_enc_partner_id);
        $product = Product::find($product_id);
        $partner = Partner::find($partner_id);

        if (!$product || !$partner) {
            \App::abort(404);
        }

        $product->partners()->detach($partner_id);
        return $this->listPartners($enc_product_id);
    }

    /**
     * Create a small LI list with product networks
     *
     * @param int $product_id
     * @return Response
     */
    private function listNetworks($enc_product_id) {
        $product_id = base64_decode($enc_product_id);
        $product = Product::find($product_id);
        return View::make('product.list.networkList', array(
            'product' => $product,
        ));
    }

    /**
     * link a network to a product
     *
     * @param int $product id
     * @return void
     */
    public function postLink_network($id) {
        $network_id = self::sanitizeText(Input::get('network_id'));
        $product = Product::find($id);
        $network = Network::find($network_id);

        if (!$product->networks->contains($network)) {
            $product->networks()->attach($network);
        }
        return $this->listNetworks($id);
    }

    /**
     * unlink a network from a product
     *
     * @param int $product id, $network_id
     * @return void
     */
    public function getUnlink_network($enc_product_id, $enc_network_id) {
        $product_id = base64_decode($enc_product_id);
        $network_id = base64_decode($enc_network_id);
        $product = Product::find($product_id);

        $product->networks()->detach($network_id);
        return $this->listNetworks($enc_product_id);
    }

    /**
     * Create a small LI list with product segments
     *
     * @param int $product_id
     * @return Response
     */
    private function listSegments($enc_product_id)
    {
        $product_id = base64_decode($enc_product_id);
        $product = Product::find($product_id);
        return View::make('product.list.segmentList', array(
            'product' => $product,
        ));
    }

    /**
     * link a segment to a product
     *
     * @param int $product id
     * @return void
     */
    public function postLink_segment($enc_id)
    {
        $id = base64_decode($enc_id);
        $segment_id = self::sanitizeText(Input::get('segment_id'));
        $product = Product::find($id);
        $segment = Segment::find($segment_id);

        if (!$product || !$segment) {
            \App::abort(404);
        }

        if (!$product->segments->contains($segment)) {
            $product->segments()->attach($segment);
        }
        return $this->listSegments($enc_id);
    }

    /**
     * unlink a segment from a product
     *
     * @param int $product id, $segment_id
     * @return void
     */
    public function getUnlink_segment($enc_product_id, $enc_segment_id) {
        $product_id = base64_decode($enc_product_id);
        $segment_id = base64_decode($enc_segment_id);
        $product = Product::find($product_id);
        $segment = Segment::find($segment_id);

        if (!$product || !$segment) {
            \App::abort(404);
        }

        $product->segments()->detach($segment_id);
        return $this->listSegments($enc_product_id);
    }

    /**
     * Create a small LI list with product countries
     *
     * @param int $product_id
     * @return Response
     */
    private function listCountries($enc_product_id) {
        $product_id = base64_decode($enc_product_id);
        $product = Product::find($product_id);

        return View::make('product.list.countryList', array(
            'product' => $product,
        ));
    }

    /**
     * link a country to a product
     *
     * @param int $product id
     * @return void
     */
    public function postLink_country($enc_id) {
        $id = base64_decode($enc_id);
        $country_id = self::sanitizeText(Input::get('country_id'));
        $product = Product::find($id);
        $country = Country::find($country_id);

        if (!$product || !$country) {
            \App::abort(404);
        }

        if (!$product->countries->contains($country)) {
            $product->countries()->attach($country_id);
        }
        return $this->listCountries($enc_id);
    }

    /**
     * unlink a country from a product
     *
     * @param int $product id, $country_id
     * @return void
     */
    public function getUnlink_country($enc_product_id, $enc_country_id)
    {
        $product_id = base64_decode($enc_product_id);
        $country_id = base64_decode($enc_country_id);
        $product = Product::find($product_id);
        $country = Country::find($country_id);

        if (!$product || !$country) {
            \App::abort(404);
        }

        $product->countries()->detach($country_id);
        return $this->listCountries($enc_product_id);
    }

    /**
     * Load a list of channels associated to the specified product
     *
     * @param int $product_id
     * @return View
     */
    private function listChannels($enc_product_id) {
        $product_id = base64_decode($enc_product_id);
        $product = Product::find($product_id);

        return View::make('product.list.channelList', array(
            'product' => $product,
        ));
    }

    /**
     * Link the product to a partner channel
     *
     * @param int $product
     * @return View
     */
    public function postLink_channel($enc_product_id) {
        $product_id = base64_decode($enc_product_id);
        $partner_id = self::sanitizeText(Input::get('partner_id'));
        $product = Product::find($product_id);
        $partner = Partner::find($partner_id);

        if (!$product || !$partner) {
            \App::abort(404);
        }

        $count = DB::table('channel_product')
                   ->where('product_id', $product->id)
                   ->where('channel_id', $partner->id)
                   ->count();

        if ($count == 0) {
            DB::table('channel_product')->insert(array(
                'product_id' => $product_id,
                'channel_id' => $partner_id
            ));
        }

        return $this->listChannels($enc_product_id);
    }

    /**
     * unlink a country from a product
     *
     * @param int $product id
     * @param int $partner_id
     * @return View
     */
    public function getUnlink_channel($enc_product_id, $enc_partner_id)
    {
        $product_id = base64_decode($enc_product_id);
        $partner_id = base64_decode($enc_partner_id);
        $product = Product::find($product_id);
        $partner = Partner::find($partner_id);

        if (!$product || !$partner) {
            \App::abort(404);
        }

        DB::table('channel_product')->where('product_id', $product_id)
                ->where('channel_id', $partner_id)
                ->delete();

        return $this->listChannels($enc_product_id);
    }

}

// EOC