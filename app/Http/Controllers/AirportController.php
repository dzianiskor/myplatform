<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\App;
use View;
use Input;
use Validator;
use Illuminate\Support\Facades\Redirect;
use App\Airport as Airport;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Airport Controller
 * 
 * @category   Controllers
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class AirportController extends BaseController
{
    protected $layout = "layouts.dashboard";


    /**
     * generate a reusable array with all the validations needed
     * for the airports screen
     *
     * @return array
     */
    private function getValidations() {
        $rules = array(
            'name' => 'required',
            'code' => 'required|min:3|max:3',
            'city' => 'required',
            'state' => 'required',
            'country' => 'required'
        );

        return $rules;
    }

    /**
     * Display a list of the airports
     *
     * @return void
     */
    public function listAirports()
    {
        $this->ormClass      = 'App\Airport';
        $this->search_fields = array('name');

        $airports = $this->getList();

        $page = Input::get('page');
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $airports = new LengthAwarePaginator($airports->forPage($page, 15), $airports->count(), 15, $page);

        return View::make("airport.list", array(
            'airports' => $airports
        ));

    }

    /**
     * get fields for new airports
     *
     * @return void
     */
    public function newAirport()
    {
        return View::make("airport.new");
    }

    /**
     * get fields for new airport
     *
     * @return void
     */
    public function createNewAirport()
    {
        $input = Input::all();
        foreach($input as $k=>$i){
            $input[$k] = self::sanitizeText($i);
        }
        $validator = Validator::make($input, $this->getValidations());

        if($validator->fails()) {
            return Redirect::to('dashboard/airports/new')->withErrors($validator)->withInput();
        }

        Airport::create($input);
        return Redirect::to('dashboard/airports');
    }

    /**
     * View a single Airport entry
     *
     * @param int $id
     * @return void
     */
    public function viewAirport($id = null)
    {
        $airport = Airport::find($id);

        if(!$airport) {
            App::abort(404);
        }

        return View::make("airport.view", array(
            'airport' => $airport
        ));
    }

    /**
     * Delete a Airport instance
     *
     * @param int $id
     * @return void
     */
    public function deleteAirport($id = null)
    {
        $airport = Airport::find($id);

        if(!$airport) {
            App::abort(404);
        }

        $airport->delete();

        return Redirect::to('dashboard/airports');
    }

    /**
     * update a Airport instance
     *
     * @param int $id
     * @return void
     */
    public function updateAirport($id = null)
    {
        return View::make("airport.update", array(
            'id' => $id
        ));
    }

    /**
     * update a Airport instance
     *
     * @param int $id
     * @return void
     */
    public function saveAirport($id = null)
    {
        $airport = Airport::find($id);

        if(!$airport) {
            App::abort(404);
        }
        $input = Input::all();
        foreach($input as $k=>$i){
            $input[$k] = self::sanitizeText($i);
        }
        $validator = Validator::make($input, $this->getValidations());

        if($validator->fails()) {
            return Redirect::to('dashboard/airports/view/' . $id)->withErrors($validator)->withInput();
        }

        $airport->update($input);

        return Redirect::to(Input::get('redirect_to', 'dashboard/airports'));
    }

} // EOC