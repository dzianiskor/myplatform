<?php
namespace App\Http\Controllers;
use App\Language;
use View;
use Response;
use Validator;
use Input;
use App\Estatementtranslation;


/**
 * Estatement Translation Controller
 * 
 * @category   Controllers
 * @package    BLU
 * @author     Magid Mroueh <magid.mroueh@bluloyalty.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class EstatementtranslationController extends BaseController {

    /**
     * Fetch the select drop down data for the estatementtranslation popup
     *
     * @return array
     */
    private function getSelectData() {
        return array(
            'languages' => Language::orderBy('name')->pluck('name', 'id')
        );
    }

    /**
     * Display the new estatementtranslation page
     *
     * @return View
     */
    public function newEstatementtranslation($enc_estatement_id = null) {
        $estatement_id = base64_decode($enc_estatement_id);
        $data = array(
            'estatement_id' => $estatement_id
        );

        return View::make("estatementtranslation.new", array_merge(
                                $data, $this->getSelectData()
        ));
    }

    /**
     * Display the edit estatementtranslation page
     *
     * @return View
     */
    public function editEstatementtranslation($enc_estatementtranslation_id = null) {
        $estatementtranslation_id = base64_decode($enc_estatementtranslation_id);
        $data = array(
            'estatementtranslation' => Estatementtranslation::find($estatementtranslation_id)
        );

        return View::make("estatementtranslation.form", array_merge(
                                $data, $this->getSelectData()
        ));
    }

    /**
     * Create a new estatementtranslation resource
     *
     * @param int $estatementtranslation_id
     * @return void
     */
    public function updateEstatementtranslation($enc_estatementtranslation_id = null) {
        $estatementtranslation_id = base64_decode($enc_estatementtranslation_id);
        $rules = array(
            'estatementtranslation_Title' => 'required|min:1'
        );
        $input = Input::all();
        foreach($input as $k=>$i){
            $input[$k] = self::sanitizeText($i);
        }
        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return Response::json(array(
                        'failed' => true,
                        'errors' => "Please ensure that you provide a valid estatementtranslation Title"
            ));
        }

        $estatementtranslation = Estatementtranslation::find($estatementtranslation_id);

        // Details
        $estatementtranslation->Title = self::sanitizeText(Input::get('estatementtranslation_Title'));
        $estatementtranslation->lang_id = self::sanitizeText(Input::get('estatementtranslation_lang'));
        $estatementtranslation->description = self::sanitizeText(Input::get('estatementtranslation_description'));
        $estatementtranslation->updated_at = date('Y-m-d H:i:s');

        $estatementtranslation->save();
        $eStatementTranslationId = base64_encode($estatementtranslation->estatement_id);
        return Response::json(array(
                    'failed' => false,
                    'view_url' => url('dashboard/estatementtranslation/list/' . $eStatementTranslationId)
        ));
    }

    /**
     * Create a new prodtranslation resource
     *
     * @param int $prodtranslation_id
     * @return void
     */
    public function createEstatementtranslation($enc_estatementtranslation_id = null) {
        $estatementtranslation_id = base64_decode($enc_estatementtranslation_id);
        $rules = array(
            'estatementtranslation_Title' => 'required|min:1'
        );
        $input = Input::all();
        foreach($input as $k=>$i){
            $input[$k] = self::sanitizeText($i);
        }
        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return Response::json(array(
                        'failed' => true,
                        'errors' => "Please ensure that you provide a valid estatementtranslation Title"
            ));
        }

        $estatementtranslation = new Estatementtranslation();
        
        // Details
        $estatementtranslation->estatement_id = self::sanitizeText(Input::get('estatementtranslation_eid'));
        $estatementtranslation->lang_id = self::sanitizeText(Input::get('estatementtranslation_lang'));
        $estatementtranslation->Title = self::sanitizeText(Input::get('estatementtranslation_Title'));
        $estatementtranslation->description = self::sanitizeText(Input::get('estatementtranslation_description'));
        $estatementtranslation->created_at = date('Y-m-d H:i:s');
        $estatementtranslation->updated_at = date('Y-m-d H:i:s');

        $estatementtranslation->save();
        $eStatementTranslationId = base64_encode($estatementtranslation->estatement_id);
        return Response::json(array(
                    'failed' => false,
                    'view_url' => url('dashboard/estatementtranslation/list/' . $eStatementTranslationId)
        ));
    }

    /**
     * Delete the specified estatementtranslation
     *
     * @return View
     */
    public function deleteEstatementtranslation($enc_estatementtranslation_id = null) {
        $estatementtranslation_id = base64_decode($enc_estatementtranslation_id);
        Estatementtranslation::find($estatementtranslation_id)->delete();

        return Response::json(array(
                    'failed' => false
        ));
    }

    /**
     * Render a list of estatementtranslation relevant to the Partner
     *
     * @param int $estatement_id
     * @return View
     */
    public function listestatementtranslations($enc_estatement_id = null) {
        $estatement_id = base64_decode($enc_estatement_id);
        $data = array(
            'estatementtranslations' => Estatementtranslation::where('estatement_id', $estatement_id)->get()
        );

        return View::make("estatementtranslation.list", $data);
    }

}

// EOC