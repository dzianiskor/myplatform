<?php
namespace App\Http\Controllers;
use App\Admin;
use App\Repositories\RepositoryUser;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Validator;
use I;
use Illuminate\Support\Facades\Auth;
use Response;
use InputHelper;
use ProductsHelper;
use PointsHelper;
use BsfMembersMiddlewareHelper;
use App\TransactionDeduction as TransactionDeduction;
use App\TransactionItem as TransactionItem;
use App\RedemptionOrder as RedemptionOrder;
use App\TransactionItemDelivery as TransactionItemDelivery;
use Illuminate\Support\Facades\Redirect;
use App\Transaction as Transaction;
use App\Partner as Partner;
use App\Ticket as Ticket;
use App\TicketActivity as TicketActivity;
use App\User as User;
use App\Store as Store;
use App\Country as Country;
use App\Balance as Balance;
use App\Member as Member;
use App\Product as Product;
use App\Currency as Currency;
use Illuminate\Support\Facades\DB;
use App\Tier as Tier;
use DateTime;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Ticket Controller
 *
 * @category   Controllers
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class TicketController extends BaseController
{
    protected $layout = "layouts.dashboard";

    /**
     * Get all list data
     *
     * @return array
     */
    private function getAllLists()
    {
        return array(
            'statuses' => array('In Progress'=>'In Progress', 'Closed'=>'Closed', 'Escalated'=>'Escalated'),
            'customerTypes' => array('Member'=>'Member', 'Partner'=>'Partner'),
        );
    }

    /**
     * Return the validations
     *
     * @return array
     */
    private function getValidations($source_type)
    {
        $source_type = strtolower($source_type);

        if ($source_type== "Member") {
            $source_type = 'users';
        }
        return array(
            'ticket'    => 'required',
//          'source_id' => "exists:$source_type,id"
            'source_id' => "required"
        );
    }

    /**
     * Record a ticket change in activity for audit purposes
     *
     * @param int $ticket_id
     * @param string $descrption
     * @return void
     */
    private function recordActivity($enc_ticket_id, $description)
    {
        $ticket_id  = base64_decode($enc_ticket_id);
        $activity              = new TicketActivity();
        $activity->ticket_id   = $ticket_id;
        $activity->description = $description;
        $activity->user_id     = Auth::User()->id;
        $activity->save();

        return $activity;
    }

    /**
     * Perform a basic member profile search
     *
     * @param \App\Repositories\RepositoryUser $repositoryUser
     *
     * @return string
     */
    public function postSearchMembers(RepositoryUser $repositoryUser)
    {
        $searchPhrase  = self::sanitizeText(Input::get('name'));
        $partnerIds = Auth::User()->managedPartnerIDs();
        $admin_partner = Auth::User()->getTopLevelPartner();

        if($admin_partner->has_middleware == '1'){
            $userIds = $repositoryUser->allIds($partnerIds);
            $url = $admin_partner->link_middleware_api . '?method=searchSensitive&api_key='.$admin_partner->middleware_api_key;
            $json_ids = json_encode($userIds);
            //$url .= "&user_ids=" . $json_ids . "&term='" . $term . "'";
            $post_fields = "term=" . $searchPhrase . "&user_ids=" . $json_ids ;
            $curl = curl_init();
            // Set some options - we are passing in a useragent too here
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => $url,
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => $post_fields,
                CURLOPT_SSL_VERIFYPEER => False,
                CURLOPT_USERAGENT => 'Testing Sensitive Info'
            ));
            // Send the request & save response to $resp
            $resp = curl_exec($curl);
            $resp_curl = json_decode($resp);

            $Users_info = $resp_curl;

            foreach($Users_info as $member) {
                if(in_array($member->blu_id,$userIds)){
                    $user = new \stdClass();
                    $user->user_id    = $member->blu_id;
                    $user->first_name = $member->first_name;
                    $user->last_name = $member->last_name;

                    $users[] = $user;
                }
            }
        } elseif ($admin_partner->id == 4436) {
            $users = Auth::User()->managedMembers(false);
            $users = $users->leftJoin('reference', 'users.id', '=', 'reference.user_id');
            $users = $users->where(function($query) use ($searchPhrase) {
                $query->where('reference.number', 'like', "%{$searchPhrase}%");
            });
            $users = $users->groupBy('users.id');
            $users = $users->get();
            if ($users) {
                foreach ($users as $u) {
                    if (!isset($u->user_id) && isset($u->ucid)) {
                        $temp_user = User::where('ucid', $u->ucid)->first();
                        $u->user_id = $temp_user->id;
                    } elseif (!isset($u->user_id)) {
                        $u->user_id = $u->id;
                    }
                    $reference = $u->references->first();
                    if($reference && strstr($reference->number, 'SGBL-')) {
                        $reference = str_replace('SGBL-', '', $reference->number);
                        $reference = explode('-', $reference);
                        $customer_id = $reference[0];
                        $secondary_customer_id = $reference[1];
                        $external_id = $reference[2];
                        $middleware_user = SGBLController::customerInfo($customer_id, $secondary_customer_id, $external_id, 0);
                        $u->first_name = $middleware_user->customerName;
                        $u->last_name = "";
                        $u->email = $middleware_user->email;
                        $u->mobile = $middleware_user->phoneNumber;
                    }
                }
            }
        } else {

            $users = $repositoryUser->listBySearchPhrase($searchPhrase, $partnerIds)
                ->transform(function($user){
                    $user->user_id = $user->id;

                    return $user;
                });
        }
        return View::make("ticket.list_members", [
            'users' => $users
        ]);
    }

    /**
     * Load a partner or member profile
     *
     * @return void
     */
    public function postLoadProfile()
    {
        $transactions = [];
        $balances     = [];
        if (!empty(self::sanitizeText(Input::get('source_id')))) {
            $source_id = base64_decode(self::sanitizeText(Input::get('source_id')));
        } else if( !empty(Session::get('user_travel_ticket')) ){
            $source_id   =	Session::get('user_travel_ticket');
        } else {
            $source_id = base64_decode(self::sanitizeText(Input::get('source_id')));
        }

        if (!empty(self::sanitizeText(Input::get('source_type')))) {
            $source_type = strtolower(self::sanitizeText(Input::get('source_type')));
        } else if(!empty(session::get('source_type_travel_ticket'))) {
            $source_type   =	Session::get('source_type_travel_ticket');
        } else {
            $source_type = strtolower(self::sanitizeText(Input::get('source_type')));
        }

        Session::put('user_travel_id',$source_id);
        $ticket_id   = (int)self::sanitizeText(Input::get('ticket_id'));
        Session::put('user_travel_ticket',$ticket_id);
        Session::put('source_type_travel_ticket',$source_type);
        if($source_type == 'member'){
            $user = User::find($source_id);
            $admin_partner = Auth::User()->getTopLevelPartner();
            $user_ids_used = array($source_id);
            $term = '';
            
            // get user transactions 
            if(!I::am('BLU Admin')){
                $partner = Partner::find($admin_partner->id);
                $partners = $partner->managedObjects();
                $partners_children = array();
                foreach ($partners as $part) {
                    $partners_children[] = $part->id;
                }
                $transactions   = Transaction::where('user_id', $user->id)->whereIn('partner_id', $partners_children)->with('currency', 'store')->orderBy('id', 'DESC')->get();
                //$transactions   = Transaction::where('user_id', $user->id)->where('partner_id', $admin_partner->id)->orderBy('id', 'DESC')->get();
                $balances   = Balance::where('network_id', $admin_partner->firstNetwork()->id)->where('user_id', $user->id)->with('network')->first();
            }else{
                $transactions = Transaction::where('user_id', $user->id)->with('currency', 'store')->orderBy('id', 'DESC')->get();
                $balances   = $user->balances();
            }
            if($admin_partner->has_middleware == '1'){

                $Users_info = BsfMembersMiddlewareHelper::getUser($source_id, $admin_partner);

                foreach($Users_info as $member1) {
                   if(in_array($member1->blu_id,$user_ids_used)){
                       $user->first_name = $member1->first_name;
                       $user->last_name = $member1->last_name;
                       $user->email = $member1->email;
                       $user->mobile = $member1->mobile;
                       $user->title = $member1->title;
                   }
                }

                return View::make("ticket.view_$source_type", array(
                    'user'      => $user,
                    'ticket_id' => self::sanitizeText(Input::get('ticket_id')),
                    'transactions' => $transactions,
                    'balances' => $balances,
                    'languageKeysCtrl' => DashboardController::platformLanguageKeys()
                ));
            }
        }

        switch($source_type) {
            case "member":
                return View::make("ticket.view_$source_type", array(
                    'user'      => User::find($source_id),
                    'ticket_id' => self::sanitizeText(Input::get('ticket_id')),
                    'transactions' => $transactions,
                    'balances' => $balances,
                    'languageKeysCtrl'=> DashboardController::platformLanguageKeys()
                ));
            case "partner":
                return View::make("ticket.view_$source_type", array(
                    'partner'   => Partner::find($source_id),
                    'ticket_id' => self::sanitizeText(Input::get('ticket_id')),
                    'languageKeysCtrl'=> DashboardController::platformLanguageKeys()
                ));
            default :
                return View::make("ticket.view_member", array(
                    'user'      => User::find($source_id),
                    'ticket_id' => self::sanitizeText(Input::get('ticket_id')),
                    'transactions' => $transactions,
                    'balances' => $balances,
                    'languageKeysCtrl'=> DashboardController::platformLanguageKeys()
                ));
        }
    }

    /**
     * Update a member profile
     *
     * @param int $id
     * @return void
     */
    public function postUpdateMember($enc_user_id = null) {
        $user_id = base64_decode($enc_user_id);
        $member = Member::find($user_id);

        if(!$member) {
            return Response::json(array(
                'failed'  => true,
                'message' => "The member account could not be located"
            ));
        }

        $ticket_id = self::sanitizeText(Input::get('ticket_id'));

        $input = Input::except('ucid', 'password', 'balance', 'draft', 'ticket_id','network_id');

        $input['dob'] = date("Y-m-d", strtotime(self::sanitizeText(Input::get('dob'))));
        $input['normalized_mobile'] = InputHelper::normalizeMobileNumber($input['mobile'], $input['telephone_code']);
       
        $member->update($input);

        return Response::json(array(
            'failed' => false
        ));
    }

    /**
     * Handle the points update
     *
     * @param int $user_id
     * @return void
     */
    public function postUpdatePoints($enc_user_id = null) {
        $user_id = base64_decode($enc_user_id);
        $user = Member::find($user_id);

        if(!$user) {
            return Response::json(array(
                'failed'  => true,
                'message' => "The user account could not be located"
            ));
        }

        $network_id = self::sanitizeText(Input::get('network_id'));
        $points_update = self::sanitizeText(Input::get('points_update'));

        $user->updateBalance($points_update, $network_id, 'subtract');

        return Response::json(array(
            'failed' => false
        ));
    }

    /**
     * Update a partner profile
     *
     * @param int $id
     * @return void
     */
    public function postUpdatePartner($enc_id = null, $enc_ticket_id = null)
    {
        $id  = base64_decode($enc_id);
        $ticket_id  = base64_decode($enc_ticket_id);
        $partner = Partner::find($id);

        if(!$partner) {
            return Response::json(array(
                'failed'  => false,
                'message' => "The partner account could not be located"
            ));
        }

        $input = Input::except('ucid', 'draft', 'parent_id', 'ticket_id');

        $partner->update($input);

        $this->recordActivity($ticket_id, "Partner profile updated");

        return Response::json(array(
            'failed' => false
        ));
    }

    /**
     * Load a product using its UCID
     *
     * @param string $ucid
     * @return void
     */
    public function getLoadProducts($search = null, $networkId = 1)
    {
        if(I::am('BLU Admin')){
            $products = Product::where('draft', false)->where('display_online','1')
                             ->where(function($query) use ($search) {
                                $query->orWhere('ucid', $search)
                                      ->orWhere('model', 'like', "%$search%")
                                      ->orWhere('name', 'like', "%$search%")
                                      ->orWhereHas('brand', function($query) use ($search) {
                                        $query->where('name', 'like',"%$search%");
                                    });

                             })
                             ->take(40)
                             ->get();
        }
        else{
            $products = Product::where('draft', false)->where('display_online','1')
                             ->where(function($query) use ($search) {
                                $query->orWhere('ucid', $search)
                                      ->orWhere('model', 'like', "%$search%")
                                      ->orWhere('name', 'like', "%$search%")
                                      ->orWhereHas('brand', function($query) use ($search) {
                                        $query->where('name', 'like',"%$search%");
                                    });

                             })
                             ->whereHas('channels', function($q) {
                                        $q->where('channel_id', Auth::User()->getTopLevelPartner()->id);
                                    })
                             ->take(40)
                             ->get();
        }
        return View::make("ticket.view_product", array(
            'products' => $products,
			'network_id' => $networkId
        ));
    }

    /**
     * Fetch the user balance
     *
     * @param int $user_id
     * @return Response
     */
    public function getUserBalance($enc_user_id)
    {
        $user_id  = base64_decode($enc_user_id);
        $user = User::find($user_id);

        if(!$user) {
            \App::abort(404);
        }

        return Response::json(array(
            'balance' => $user->balance
        ));
    }

    /**
     * Reverse a transaction
     *
     * @param trx_id
     *
     * @return Response
     */
    public function getReverseTransaction($enc_trx_id)
    {
        $trx_id = base64_decode($enc_trx_id);

        $transaction = Transaction::find($trx_id);

        if (isset($transaction->trans_class) && strtolower($transaction->trans_class) == "hotel") {
            $reservationDetails = json_decode($transaction->notes, True);
            if (!empty($reservationDetails['reservation'])) {
                $reservationId = $reservationDetails['reservation']['@attributes']['id'];

                $res = APIController::postHotelBookingCancellation($reservationId);

                $resultHotel = $res->original;
                $resultsJson = json_encode(simplexml_load_string($resultHotel['cancellation'], null, LIBXML_NOCDATA | LIBXML_PARSEHUGE));
                $all_results_obj = json_decode($resultsJson, TRUE);

                if ($all_results_obj['cancellation']['@attributes']['status'] == 'failed') {
                    return Response::json(array(
                        'reversed' => 0,
                        'msg' => 'reservation not found',
                        'res' => $res
                    ));
                }
            }
        }
        if (isset($transaction->trans_class) && strtolower($transaction->trans_class) == "flight") {
            $bookingnotes = json_decode($transaction->notes, True);

            if (isset($bookingnotes['record_locator']) && !empty($bookingnotes['record_locator'])) {
                $recordLocator = $bookingnotes['record_locator'];
                $cancelResponse = APIController::postCancelBooking($recordLocator);
                $resultCancel = $cancelResponse->original;
                $cancelStatus = $resultCancel['cancelb'];
                if ($cancelStatus != 'sucess') {
                    return Response::json(array(
                        'reversed' => 0,
                        'msg' => 'reservation not found',
                        'res' => $res
                    ));
                }
            }
        }

        if (empty(Auth::User()->parentID())) {
            $result = Transaction::doReversal($trx_id);
        } else {
            $member = Member::find(self::sanitizeText($transaction->user_id));
            if ($transaction->trx_reward) {
                $transactionType = 'Reward';
                $reversedPoints = $transaction->points_rewarded;
            } else {
                $transactionType = 'Redemption';
                $reversedPoints = $transaction->points_redeemed;
            }

            $data = [
                'trx_id' => $trx_id,
                'reference_id' => $transaction->ref_number,
                'transaction_id' => $transaction->id,
                'transaction_type' => $transactionType,
                'reversed_points' => $reversedPoints,
                'user' => [
                    'first_name' => $member->first_name,
                    'last_name' => $member->last_name,
                ],
            ];

            $this->makeApproval('reversal', 'reversal_to_approve', 'Reversal Transaction Request', $data);

            return Response::json([
                'reversed' => 0,
                'show_message' => 'Transaction will reverse after approvement.',
            ]);
        }
//        $result = Transaction::doReversal($trx_id);

        if (substr($transaction->ref_number, 0, 8) === "Transfer") {
            $ref = explode("|", $transaction->ref_number);
            $otherTrxId = trim($ref['1']);
            $result1 = Transaction::doReversal($otherTrxId);
        }

        if ($result->transaction['trx_reversed'] > 0) {
            return Response::json(array(
                'reversed' => $result->transaction['trx_reversed'],
                'result' => $result,
                'cash' => $result->transaction['cash'],
                'currency' => $result->transaction['currency']
            ));
        } else {
            return Response::json(array(
                'reversed' => 0,
                'result' => $result
            ));
        }
    }

    /**
     * @param string $module
     * @param string $mailTemplate
     * @param string $subject
     * @param array $dataToBeApproved
     * @param int|null $requesterId
     * @param int|null $userToApprove
     */
    private function makeApproval($module, $mailTemplate, $subject, $dataToBeApproved, $requesterId = null, $userToApprove = null)
    {
        if (empty($requesterId)) {
            $requesterId = Auth::User()->id;
        }
        if (empty($userToApprove)) {
            $userToApprove = Auth::User()->parentID();
        }
        $jsonData = json_encode($dataToBeApproved);
        $requester = Admin::find($requesterId);
        $userToApprove = Admin::find($userToApprove);
        $code = md5(uniqid());

        DB::table('approvals')->insert([
            'module' => $module,
            'data' => $jsonData,
            'requester_id' => $requester->id,
            'approve_by' => $userToApprove->id,
            'code' => $code,
        ]);

        $data = [
            'userToApprove' => $userToApprove,
            'dataToBeApproved' => $dataToBeApproved,
            'approveUrl' => route('reverse-trx-approve', ['code' => $code]),
            'declineUrl' => route('reverse-trx-decline', ['code' => $code]),
            'requester' => $requester,
        ];

        self::sendApprovalEmail($userToApprove->email, $subject, $mailTemplate,  $data);
    }

    /**
     * @param string $to
     * @param string $subject
     * @param string $mailTemplate
     * @param array $data
     */
    private static function sendApprovalEmail($to, $subject, $mailTemplate, $data)
    {
        if (empty($params['admin'])) {
            $params['admin'] = array();
        }

        $email_template = View::make("emails.{$mailTemplate}", $data)->render();

        $emails = array($to);
        $from = 'test@test.com';
//        $from = Config::get('blu.email_from');

        Mail::send('emails.fnbemail', ['html' => $email_template], function ($message) use ($emails, $from, $subject) {
            $message->to($emails)->subject($subject)->from($from, 'BLU Points');
        });
    }

    /**
     * @param string $code
     *
     * @return mixed
     */
    public function approveTransaction($code)
    {
        $approvalData = DB::table('approvals')->where('code', $code)->first();
        if (!$approvalData) {
            App::abort(404);
        }

        $data = json_decode($approvalData->data, true);

        $success = false;
        $result = Transaction::doReversal($data['trx_id']);

        if ($result->status != 200) {
            //
        }

        if ($result->status == 600) {
            $message = "Could not reverse the transaction, the user has insufficient network balance.";
        } else if ($result->transaction['cash'] > 0) {
            $success = true;
            $message = "Transaction Number: " . $result->transaction['trx_reversed'] . " has been Reversed. Please note " . $result->transaction['currency'] . $result->transaction['cash'] . " should be credited back to the member\'s account";
        } else {
            $success = true;
            $message = "Transaction Number: " . $result->transaction['trx_reversed'] . " has been Reversed.";
        }

        $transaction = Transaction::find($data['trx_id']);
        $approveBy = Admin::find($approvalData->approve_by);
        $requester = Admin::find($approvalData->requester_id);
        $user = User::find($transaction->user_id);

        $to = $requester->email;
        $subject = 'Reversal Transaction Approved';
        $mailTemplate = 'reversal_approved';
        $mailData = [
            'userToApprove' => $approveBy,
            'requester' => $requester,
            'dataToBeApproved' => $data,
        ];

        if ($success) {
            self::sendApprovalEmail($to, $subject, $mailTemplate, $mailData);
            DB::table('approvals')->where('code', $code)->delete();
        }

        return View::make('ticket.reversal_transaction_complete', [
            'status' => $result->status,
            'message' => $message,
            'user' => $user,
            'transaction' => $transaction,
            'type' => $transaction->trx_reward ? 'reward' : 'redemption',
            'points' => $data['reversed_points'],
        ]);
    }

    /**
     * @param string $code
     *
     * @return mixed
     */
    public function declineTransaction($code)
    {
        $approvalData = DB::table('approvals')->where('code', $code)->first();
        if (!$approvalData) {
            App::abort(404);
        }

        $data = json_decode($approvalData->data, true);

        $transaction = Transaction::find($data['trx_id']);
        $approveBy = Admin::find($approvalData->approve_by);
        $requester = Admin::find($approvalData->requester_id);
        $user = User::find($transaction->user_id);

        $to = $requester->email;
        $subject = 'Reversal Transaction Declined';
        $mailTemplate = 'reversal_declined';
        $mailData = [
            'userToApprove' => $approveBy,
            'requester' => $requester,
            'dataToBeApproved' => $data,
        ];

        self::sendApprovalEmail($to, $subject, $mailTemplate, $mailData);

        DB::table('approvals')->where('code', $code)->delete();

        return View::make('ticket.reversal_transaction_complete', [
            'status' => 403,
            'message' => 'Reversal Transaction Declined',
            'user' => $user,
            'transaction' => $transaction,
            'type' => $transaction->trx_reward ? 'reward' : 'redemption',
            'points' => $data['reversed_points'],
        ]);
    }

    /**
     * Handle the redemption transaction
     *
     * @return void
     */
    public function postRedeemItem()
    {
        $member_id  = base64_decode(self::sanitizeText(Input::get('user_id')));
        $product_id = self::sanitizeText(Input::get('product_id'));
	$networkId	= self::sanitizeText(Input::get('network_id', 1));

        // Create the transaction

        $payload = array();

        $product	  = new \stdClass();
        $product->id  = $product_id;
        $product->qty = 1;//self::sanitizeText(Input::get('quantity'));

        $payload[] = $product;
        $memberDetails = User::find($member_id);
        $partner_id = '';
//			var_dump(Auth::User()->getTopLevelPartner()->id);
//		exit;
//        foreach($memberDetails->partners as $p){
//            if (Auth::User()->partners->contains($p)){
//                $partner_id = $p->id;
//            }
//        }
	$partner_id = Auth::User()->getTopLevelPartner()->id;
        //$partnerDetails = Partner::find($partner_id);
        $storeDetails	= Store::where("partner_id", $partner_id)->where('name', 'LIKE', '%call center%')->first();
        
	$store		= Store::find($storeDetails->id);
        $pos_id		= $store->pointOfSales->first()->id;

        $result = Transaction::doRedemption(array(
            'user_id'		=> $member_id,
            'items'		=> $payload,
            'reference'		=> "Call Center Redemption",
            'staff_id'		=> Auth::User()->id,
            'partner'		=> $partner_id,
            'store_id'		=> $storeDetails->id,
            'pos_id'		=> $pos_id,
            'network_id'	=> $networkId,
            'posted_at'         => date('Y-m-d H:i:s')
        ), "Items");

        if($result->status == Transaction::TRANS_SUCCESS) {
            return Response::json(array(
                'failed'  => false,
                'message' => "The item has been successfully redeemed on behalf of the user"
            ));
        } else {
            return Response::json(array(
                'failed'  => true,
                'message' => $result->message
            ));
        }
    }

    /**
     * Display a list of the Tickets
     *
     * @return void
     */
    /**
     * Display a list of the Tickets
     *
     * @return void
     */
    public function listTickets()
    {
        if (!I::can('view_tickets')) {
            return Redirect::to(url("dashboard"));
        }

        $partnerIds = Auth::User()->managedPartnerIDs();

        $tickets = Ticket::select(
                'ticket.*',
                'source_user.first_name as test',
                DB::raw('IF (ticket.query_source = "Member", CONCAT(source_user.first_name, " ", source_user.last_name), source_partner.name) as identity'),
                DB::raw('CONCAT(admins.first_name, " ", admins.last_name) as admin_name')
            )
             ->leftJoin('users as source_user', 'source_user.id', '=', 'ticket.source_id')
             ->leftJoin('partner as source_partner', 'source_partner.id', '=', 'ticket.source_id')
             ->leftJoin('admins', 'admins.id', '=', 'ticket.user_id')
             ->whereIn('ticket.partner_id', $partnerIds)
             ->where('ticket.draft', false)
             ->orderBy('ticket.id', 'desc');


        if ($searchPhrase = self::sanitizeText(Input::get('q')))
        {
            $tickets = $tickets->where(function ($query) use ($searchPhrase, $partnerIds) {
                $query->whereHas('user.references',
                    function ($query) use ($searchPhrase, $partnerIds) {
                        return $query->where('reference.number', 'like', '%' . $searchPhrase . '%')
                            ->whereIn('partner_id', $partnerIds);
                    })
                    ->orWhere(DB::raw('IF (ticket.query_source = "Member", CONCAT(source_user.first_name, " ", source_user.last_name), source_partner.name)'), 'like', '%' . $searchPhrase . '%')
                    ->orWhere('ticket.status', 'like', $searchPhrase . '%');
            });
        }

        if (Input::has('customer_type_list')) {
            $tickets = $tickets->whereIn('ticket.query_source', explode(',', Input::get('customer_type_list')));
        }

        if (Input::has('status_list')) {
            $tickets = $tickets->whereIn('ticket.status', explode(',', Input::get('status_list')));
        }

        $tickets = $tickets->paginate(15);

        return View::make("ticket.list", array(
            'tickets' => $tickets,
            'lists' => $this->getAllLists(),
        ));
    }

    /**
     * Create a new ticket resource
     *
     * @return void
     */
    public function newTicket()
    {
        if(!I::can('create_tickets')){
                return Redirect::to(url("dashboard"));
            }
        $ticket               = new Ticket();
        $ticket->status       = 'In Progress';
        $ticket->query_source = ucfirst(self::sanitizeText(Input::get('type', 'Member')));
        $ticket->user_id      = Auth::User()->id;
        $ticket->partner_id   = Auth::User()->firstPartner()->id;
        $ticket->save();

        // Record Activity
        $this->recordActivity($ticket->id, "Ticket Created");

        $ticketId = base64_encode($ticket->id);
        return Redirect::to(url("dashboard/call_center/view/{$ticketId}"));
    }

    /**
     * View a single ticket resource
     *
     * @param int $id
     * @return void
     */
    public function viewTicket($enc_id = null)
    {
        $id  = base64_decode($enc_id);
        if(!I::can('view_tickets')){
                return Redirect::to(url("dashboard"));
            }
        $ticket = Ticket::find($id);
        if(!$ticket) {
            \App::abort(404);
        }
        $managedPartners = Auth::User()->managedPartners();
        $can_view_ticket = false;
        foreach($managedPartners as $manP){
            if($manP->id == $ticket->partner_id){
                $can_view_ticket = true;
                break;
            }
        }
        if($can_view_ticket== false && $ticket->draft == false){
            return Redirect::to(url("dashboard"));
        }
        
        $networks = array();
        $networksObject   = Auth::User()->managedNetworks();
        foreach ($networksObject as $network) {
            $networks[$network->id] = $network->name;
        }
        return View::make("ticket.view", array(
            'ticket' => $ticket,
            'user'   => User::find($ticket->source_id),
            'networks' => $networks,
            'countries' => Country::orderBy('name')->pluck('name', 'id'),
            'languageKeysCtrl'=> DashboardController::platformLanguageKeys()
        ));
    }

    /**
     * Delete a Ticket instance
     *
     * @param int $id
     * @return void
     */
    public function deleteTicket($enc_id = null)
    {
        $id  = base64_decode($enc_id);
        if(!I::can('delete_tickets')){
                return Redirect::to(url("dashboard"));
            }
        $ticket = Ticket::find($id);

        if(!$ticket) {
            \App::abort(404);
        }

        $ticket->delete();

        return Redirect::to('dashboard/call_center');
    }

    /**
     * Persist ticket changes
     *
     * @param int $id
     * @return void
     */
    public function saveTicket($enc_id = null)
    {
        $id  = base64_decode($enc_id);
        if (!I::can('edit_tickets') && !I::can('create_tickets')) {
            return Redirect::to(url("dashboard"));
        }
        $ticket = Ticket::find($id);

        if(!$ticket) {
            \App::abort(404);
        }

        $validation_type = "users";

        switch (self::sanitizeText(Input::get('source_type'))) {
            case 'member':
                $validation_type = "user";
                break;
            case 'Partner':
                $validation_type = "partner";
                break;
        }
        $input = Input::all();
        foreach($input as $k=>$i){
            $input[$k] = self::sanitizeText($i);
        }
        $validator = Validator::make($input, $this->getValidations($validation_type));

        if($validator->fails()) {
            return Redirect::to("dashboard/call_center/view/{$enc_id}")->withErrors($validator)->withInput();
        }

        $ticket->status         = self::sanitizeText(Input::get('status'));
        $ticket->ticket_type_id = self::sanitizeText(Input::get('ticket_type_id'));
        $ticket->query_source   = self::sanitizeText(Input::get('source_type'));
        $ticket->ticket         = self::sanitizeText(Input::get('ticket'));
        $ticket->source_id      = base64_decode(self::sanitizeText(Input::get('source_id')));
        $ticket->draft          = false;
        $ticket->save();

        $this->recordActivity($ticket->id, "Ticket Updated");

		$from_travel	= self::sanitizeText(Input::get('from_travel', 0));
		if($from_travel){
			$data['msg'] = 'Ticket Saved';
			return $data;
		}else{
            return Redirect::to(Input::get('redirect_to', 'dashboard/call_center'));
		}
    }
    
    /**
     * Reverse a transaction
     *
     * @param trx_id
     *
     * @return Response
     */
    public function getReverseModule($enc_trx_id){
        $trx_id  = base64_decode($enc_trx_id);
        $canReverseTrx  = 1;
        $transaction		= Transaction::find($trx_id);
        if($transaction->reversed_item || $transaction->reversed_delivery){
            $canReverseTrx = 0;
        }
        $reversedTrxItemCount   = 0;
        //get trx items
        $trxItemsData = TransactionItem::where('transaction_id', $trx_id)->get();
        
        //get delivery
        $trxItemsDeliveryData = TransactionItemDelivery::where('trx_id', $trx_id)->where('ref_number', NULL)->get();
        
        $trxItems = View::make("ticket.view_reverse_items", array(
            'trxItems' => $trxItemsData,
            'trxId' => $trx_id,
            'reversedItemsFlag' => $transaction->reversed_item,
            'canReverseTrx' => $canReverseTrx
        ))->render();
        $trxItemsDelivery = View::make("ticket.view_reverse_delivery", array(
            'trxItemsDelivery' => $trxItemsDeliveryData,
            'reversedDeliveryFlag' => $transaction->reversed_delivery,
            'trxId' => $trx_id
        ))->render();
        return json_encode(array('trxItems'=>$trxItems, 'trxItemsDelivery'=>$trxItemsDelivery));
    }

    /**
     * Reverse a transaction Item
     *
     * @param trx_id
     *
     * @return Response
     */
    public function postReverseTransactionItem($enc_trxItem_id, $quantity){
        try{
            
        
        $trxItem_id                         = base64_decode($enc_trxItem_id);
        $json_data = json_encode(array($enc_trxItem_id,$trxItem_id));
        
        $transactionItem                    = TransactionItem::find($trxItem_id);
        $product                            = Product::find($transactionItem->product_id);
        $itemPriceInPoints                  = round($transactionItem->paid_in_points / $transactionItem->quantity);
        $itemPriceInCash                  = $transactionItem->paid_in_cash / $transactionItem->quantity;
        $itemPriceInUSD                     = $transactionItem->total_amount / $transactionItem->quantity;
        $itemPriceInOriginalCurrency        = $transactionItem->original_total_amount / $transactionItem->quantity;
        $itemPricefullpoints                = round($transactionItem->price_in_points/ $transactionItem->quantity);
        
        $transaction        = Transaction::find($transactionItem->transaction_id);
        $user               = User::find($transaction->user_id);
        $trx_id             = $transaction->id;
        $currency_short_code = Currency::find($transaction->currency_id)->short_code;
        // First Step is to make a reverse of the main transaction with the value of item (redemption of the negatif of the item)
        $trans_reversal = new Transaction();
//        DB::beginTransaction();
        $trans_reversal->user_id			= $transaction->user_id;
        $trans_reversal->country_id			= $transaction->country_id;
        $trans_reversal->partner_id			= $transaction->partner_id;
        $trans_reversal->trans_class			= 'Reversal Item';
        $trans_reversal->source				= $transaction->source;
        $trans_reversal->ref_number			= $transaction->id;
        $trans_reversal->notes				= $transaction->notes;
        $trans_reversal->store_id			= $transaction->store_id;
        $trans_reversal->pos_id				= $transaction->pos_id;
        $trans_reversal->auth_staff_id			= Auth::user()->id;
        $trans_reversal->network_id			= $transaction->network_id;
        $trans_reversal->rule_id			= $transaction->rule_id;
        $trans_reversal->total_amount			= $itemPriceInUSD * $quantity * (-1);
        $trans_reversal->original_total_amount		= $itemPriceInOriginalCurrency * $quantity * (-1);
        $trans_reversal->partner_amount                 = $itemPriceInOriginalCurrency * $quantity * (-1);
        $trans_reversal->points_rewarded		= 0;
        $trans_reversal->points_redeemed		= $itemPriceInPoints * $quantity * (-1);
        $trans_reversal->points_balance			= null;
        $trans_reversal->trx_reward			= $transaction->trx_reward;
        $trans_reversal->trx_redeem			= $transaction->trx_redeem;
        $trans_reversal->amt_reward			= $transaction->amt_reward * (-1);
        $trans_reversal->amt_redeem			= PointsHelper::pointsToAmount($trans_reversal->points_redeemed, $currency_short_code, $transaction->network_id);
        $trans_reversal->currency_id			= $transaction->currency_id;
        $trans_reversal->partner_currency_id		= $transaction->currency_id;
        $trans_reversal->exchange_rate			= $transaction->exchange_rate;
        $trans_reversal->product_id			= $transaction->product_id;
        $trans_reversal->product_model			= $transaction->product_model;
        $trans_reversal->product_brand_id		= $transaction->product_brand_id;
        $trans_reversal->product_sub_model		= $transaction->product_sub_model;
        $trans_reversal->product_category_id            = $transaction->product_category_id;
        $trans_reversal->city_id			= $transaction->city_id;
        $trans_reversal->area_id			= $transaction->area_id;
        $trans_reversal->segment_id			= $transaction->segment_id;
        $trans_reversal->category_id			= $transaction->category_id;
        $trans_reversal->quantity			= $quantity * (-1);
        $trans_reversal->auth_role_id			= $transaction->auth_role_id;
        // set the created at and updated as new default values not as old transaction
//        $trans_reversal->created_at			= $transaction->created_at;
//        $trans_reversal->updated_at			= $transaction->updated_at;
        $trans_reversal->delivery_cost			= 0;
        $trans_reversal->source				= $transaction->source;
        $trans_reversal->cash_payment                   = $itemPriceInCash * $quantity * (-1);
        $trans_reversal->reversed			= 0;
        $trans_reversal->posted_at			= date('Y-m-d H:i:s');
        
//        $trans_reversal->save();
//        $last_inserted_reversal = $trans_reversal->id;
        //return the qty to the product
        $product->qty = $product->qty + $quantity;
        $product->save();
        
        if($transaction->reversed_item == 1){
            $checkLastTrxDeduct = 1;
        }
        
        // Transaction deduction
        $points_to_deduct = $itemPriceInPoints * $quantity * (-1);
        DB::table('transaction')->where('id', $trx_id)->update(array( 'reversed_item' => 1));
        $user->updateBalance($trans_reversal->points_redeemed, $trans_reversal->network_id, 'subtract');
        $trans_reversal->save();
        $last_inserted_reversal = $trans_reversal->id;
        $trx_used_deduction = DB::table('transaction_deduction')->select(DB::raw('DISTINCT(trx_id)'))
            ->where('trx_ref_id',$trx_id)->orderBy('id', 'desc')->get();
        $trx_used_deduction_arr = array();
        $got_first = false;
        //APIController::postSendEmailJson(json_encode($trx_used_deduction->toArray()));
        $change_last_trx_unused = true;
        $count = 0;
        foreach($trx_used_deduction as $tud){
            $trx_deduction = new TransactionDeduction();
//            APIController::postSendEmailJson(json_encode($tud->trx_id),"reverse item - " . $count);
            $trx_deduction->trx_ref_id = $last_inserted_reversal;
            $trx_deduction_temp = TransactionDeduction::where('trx_id',$tud->trx_id)->where('trx_used',0)->orderBy('id','DESC')->first();
            $trx_deduction_temp_used = TransactionDeduction::where('trx_id',$tud->trx_id)->where('trx_used',1)->orderBy('id','DESC')->first();
            $trx_deduction_redemption = TransactionDeduction::where('trx_id',$tud->trx_id)->where('trx_ref_id',$trx_id)->orderBy('id','DESC')->first();
//            if($trx_deduction_temp){
//                APIController::postSendEmailJson(json_encode(array($trx_deduction_temp->toArray(), $trx_deduction_redemption->toArray())));
//                if($trx_deduction_redemption->points_used - $trx_deduction_temp->points_remaining  <= 0 && $trx_deduction_temp->trx_id != $trx_deduction_redemption->trx_id){
////                    continue;
//                }
//            }
            //APIController::postSendEmailJson(json_encode($trx_deduction_temp_used));
            if($trx_deduction_temp && !$trx_deduction_temp_used){
//                APIController::postSendEmailJson("gone if 001","ticket controller if 001" . $tud->trx_id);
                $change_last_trx_unused = false;
                $trx_int_deduction_pts_remaining = intval($trx_deduction_temp->points_remaining);
                $trx_int_deduction_pts_used = intval($trx_deduction_redemption->points_used) * (-1);
                $sql = "select sum(points_used) as pts_used from development.transaction_deduction where trx_id = " . $tud->trx_id . " and trx_ref_id in (select id from development.transaction where ref_number like '".$trx_id."') order by id desc limit 1";
                $pts_remaining_last_trx = DB::select($sql);
                $pts_remaining_last_trx_points_remaining =0;
                foreach($pts_remaining_last_trx as $pts_rem_last_trx){
                    $pts_remaining_last_trx_points_remaining = $pts_rem_last_trx->pts_used;
                }
                $available_for_reversal = $trx_deduction_redemption->points_used + $pts_remaining_last_trx_points_remaining;
//                $trx_deduction->points_remaining = $trx_int_deduction_pts_remaining - $trx_int_deduction_pts_used;
//                $trx_deduction->points_used = $trx_deduction_redemption->points_used * (-1);
                    APIController::postSendEmailJson(json_encode(array("trx_deduction_temp"=>$trx_deduction_temp->id,"trx_deduction->trx_ref_id"=>$trx_deduction->trx_ref_id, "trx_int_deduction_pts_remaining"=>$trx_int_deduction_pts_remaining, "redemption points used" => $trx_deduction_redemption->points_used, "points remaining last transaction"=> $pts_remaining_last_trx_points_remaining, "available for reversal"=>$available_for_reversal,"points to deduct"=>$points_to_deduct)), '001 Available Points '  . $tud->trx_id);
                if($available_for_reversal >= abs($points_to_deduct)){
                    $trx_deduction->points_remaining = $trx_int_deduction_pts_remaining - $points_to_deduct;
                    $trx_deduction->points_used =  $points_to_deduct;  
                    $points_to_deduct += $trx_deduction_redemption->points_used;
                }else{
                    $canBeDeducted  = $available_for_reversal;
                    $trx_deduction->points_remaining = $trx_int_deduction_pts_remaining + $canBeDeducted;
                    $trx_deduction->points_used = $canBeDeducted * (-1);  
                    $points_to_deduct += $canBeDeducted;
                }
                
                
                $trx_deduction->trx_id = $tud->trx_id;
                $trx_deduction->trx_used = 0;//0
                $trx_deduction->used_date = date("Y-m-d H:i:s");
                $trx_deduction->partner_id = Transaction::find($tud->trx_id)->partner_id;
                if($trx_deduction->points_used != 0){
                    $trx_deduction->save();

                    //reverse points issued allocation
                    $pointsIssuedAllocation = array();
                    $pointsIssuedAllocation['trx_deduction_id'] = $trx_deduction->id;
                    $pointsIssuedAllocation['trx_item_id']      = $transactionItem->id; 
                    $pointsIssuedAllocation['partner_id']       = $trx_deduction->partner_id;
                    $pointsIssuedAllocation['created_at']       = date("Y-m-d H:i:s");
                    $pointsIssuedAllocation['updated_at']       = date("Y-m-d H:i:s");
                    $pointsIssuedAllocation['points_used']      = $trx_deduction->points_used;
    //                APIController::postSendEmailJson(json_encode($pointsIssuedAllocation),'points_issued_allocation - '.$trx_deduction->id);
                    DB::table('points_issued_allocation')->insert($pointsIssuedAllocation);
                }
                if($points_to_deduct < 0){
                    continue;
                }else{
                    break;
                }
            }
            elseif($trx_deduction_temp_used && $trx_deduction_temp){
                $change_last_trx_unused = false;
                $trx_int_deduction_pts_remaining = intval($trx_deduction_temp_used->points_remaining);
                $trx_int_deduction_pts_used = intval($trx_deduction_redemption->points_used) ;
                $sql = "select sum(points_used) as pts_used from development.transaction_deduction where trx_id = " . $tud->trx_id . " and trx_ref_id in (select id from development.transaction where ref_number like '".$trx_id."') order by id desc limit 1";
                $pts_remaining_last_trx = DB::select($sql);
                $pts_remaining_last_trx_points_remaining =0;
                foreach($pts_remaining_last_trx as $pts_rem_last_trx){
                    $pts_remaining_last_trx_points_remaining = $pts_rem_last_trx->pts_used;
                }
                if(is_null($pts_remaining_last_trx_points_remaining)){
                    $pts_remaining_last_trx_points_remaining = 0;
                }
                $available_for_reversal = $trx_deduction_redemption->points_used + $pts_remaining_last_trx_points_remaining;
//                $trx_deduction->points_remaining = $trx_int_deduction_pts_used;
//                $trx_deduction->points_used = $trx_deduction_redemption->points_used * (-1);
                APIController::postSendEmailJson(json_encode(array("trx_deduction_temp"=>$trx_deduction_temp_used->id,"trx_deduction->trx_ref_id"=>$trx_deduction->trx_ref_id, "trx_int_deduction_pts_remaining"=>$trx_int_deduction_pts_remaining, "redemption points used" => $trx_deduction_redemption->points_used, "points remaining last transaction"=> $pts_remaining_last_trx_points_remaining, "available for reversal"=>$available_for_reversal,"points to deduct"=>$points_to_deduct)), '002 Available Points '  . $tud->trx_id);

                if($available_for_reversal >= abs($points_to_deduct)){
                    $trx_deduction->points_remaining = $trx_int_deduction_pts_remaining - $points_to_deduct;
                    $trx_deduction->points_used =  $points_to_deduct;  
                    $points_to_deduct += $trx_deduction_redemption->points_used;
                }else{
                    $canBeDeducted  = $available_for_reversal;
                    $trx_deduction->points_remaining = $trx_int_deduction_pts_remaining + $canBeDeducted;
                    $trx_deduction->points_used = $canBeDeducted * (-1);  
                    $points_to_deduct += $canBeDeducted;
                }
                
                $trx_deduction->trx_id = $tud->trx_id;
                $trx_deduction->trx_used = 0;//0
                $trx_deduction->used_date = date("Y-m-d H:i:s");
                $trx_deduction->partner_id = Transaction::find($tud->trx_id)->partner_id;
                if($trx_deduction->points_used != 0){
                    $trx_deduction->save();
                    $trx_deduction_temp_used->trx_used = 0;
                    $trx_deduction_temp_used->updated_at = date("Y-m-d H:i:s");
                    $trx_deduction_temp_used->save();
                    //reverse points issued allocation
                    $pointsIssuedAllocation = array();
                    $pointsIssuedAllocation['trx_deduction_id'] = $trx_deduction->id;
                    $pointsIssuedAllocation['trx_item_id']      = $transactionItem->id; 
                    $pointsIssuedAllocation['partner_id']       = $trx_deduction->partner_id;
                    $pointsIssuedAllocation['created_at']       = date("Y-m-d H:i:s");
                    $pointsIssuedAllocation['updated_at']       = date("Y-m-d H:i:s");
                    $pointsIssuedAllocation['points_used']      = $trx_deduction->points_used;
    //                APIController::postSendEmailJson(json_encode($pointsIssuedAllocation),'points_issued_allocation 002 - '. $tud->trx_id);
                    DB::table('points_issued_allocation')->insert($pointsIssuedAllocation);
                }
                if($points_to_deduct < 0){
                    continue;
                }else{
                    break;
                }
                
            }
            else{
//                APIContsroller::postSendEmailJson("gone if 003","ticket controller if 003" . $tud->trx_id);
                $change_last_trx_unused = false;
                $trx_int_deduction_pts_remaining =0;
                $trx_int_deduction_pts_used = intval($trx_deduction_redemption->points_used) ;
                $sql = "select sum(points_used) as pts_used from development.transaction_deduction where trx_id = " . $tud->trx_id . " and trx_ref_id in (select id from development.transaction where ref_number like '".$trx_id."') order by id desc limit 1";
                $pts_remaining_last_trx = DB::select($sql);
                $pts_remaining_last_trx_points_remaining =0;
                foreach($pts_remaining_last_trx as $pts_rem_last_trx){
                    $pts_remaining_last_trx_points_remaining = $pts_rem_last_trx->pts_used;
                }
                $available_for_reversal = $trx_deduction_redemption->points_used + $pts_remaining_last_trx_points_remaining;
//                $trx_deduction->points_remaining = $trx_int_deduction_pts_used;
//                $trx_deduction->points_used = $trx_deduction_redemption->points_used * (-1);
                if($available_for_reversal >= abs($points_to_deduct)){
                    $trx_deduction->points_remaining = $trx_int_deduction_pts_remaining - $points_to_deduct;
                    $trx_deduction->points_used =  $points_to_deduct;  
                    $points_to_deduct += $trx_deduction_redemption->points_used;
                }else{
                    $canBeDeducted  = $available_for_reversal;
                    $trx_deduction->points_remaining = $trx_int_deduction_pts_remaining + $canBeDeducted;
                    $trx_deduction->points_used = $canBeDeducted * (-1);  
                    $points_to_deduct += $canBeDeducted;
                }
                APIController::postSendEmailJson(json_encode(array("trx_deduction->trx_ref_id"=>$trx_deduction->trx_ref_id, "trx_int_deduction_pts_remaining"=>$trx_int_deduction_pts_remaining, "redemption points used" => $trx_deduction_redemption->points_used, "points remaining last transaction"=> $pts_remaining_last_trx_points_remaining, "available for reversal"=>$available_for_reversal,"points to deduct"=>$points_to_deduct)), '003 Available Points '  . $tud->trx_id);

                $trx_deduction->trx_id = $tud->trx_id;
                $trx_deduction->trx_used = 0;//0
                $trx_deduction->used_date = date("Y-m-d H:i:s");
                $trx_deduction->partner_id = Transaction::find($tud->trx_id)->partner_id;
                if($trx_deduction->points_used != 0){
                    $trx_deduction->save();
                    $trx_deduction_temp_used->trx_used = 0;
                    $trx_deduction_temp_used->updated_at = date("Y-m-d H:i:s");
                    $trx_deduction_temp_used->save();
                    //reverse points issued allocation
                    $pointsIssuedAllocation = array();
                    $pointsIssuedAllocation['trx_deduction_id'] = $trx_deduction->id;
                    $pointsIssuedAllocation['trx_item_id']      = $transactionItem->id; 
                    $pointsIssuedAllocation['partner_id']       = $trx_deduction->partner_id;
                    $pointsIssuedAllocation['created_at']       = date("Y-m-d H:i:s");
                    $pointsIssuedAllocation['updated_at']       = date("Y-m-d H:i:s");
                    $pointsIssuedAllocation['points_used']      = $trx_deduction->points_used;
    //                APIController::postSendEmailJson(json_encode($pointsIssuedAllocation),'points_issued_allocation 002 - '.$trx_deduction->id);
                    DB::table('points_issued_allocation')->insert($pointsIssuedAllocation);
                }
                if($points_to_deduct < 0){
                    continue;
                }else{
                    break;
                }
                //APIController::postSendEmailJson("he's at the else",'DEV reversal item 00001');
            }
            $count ++;
        }
        //APIController::postSendEmailJson("exited if 002", "exited if 002");
        //reverse item in transaction item table
        $reversed_item	= new TransactionItem();
        $reversed_item->total_amount            = $itemPriceInUSD * $quantity * (-1);
        $reversed_item->original_total_amount   = $itemPriceInOriginalCurrency * $quantity * (-1);
        $reversed_item->currency_id             = $transactionItem->currency_id;
        $reversed_item->product_id              = $transactionItem->product_id;
        $reversed_item->product_model           = $transactionItem->product_model;
        $reversed_item->product_brand_id        = $transactionItem->product_brand_id;
        $reversed_item->product_sub_model       = $transactionItem->product_sub_model;
        $reversed_item->product_category_id     = $transactionItem->product_category_id;
        $reversed_item->delivery_option         = $transactionItem->delivery_option;
        $reversed_item->delivery_cost           = 0;
        $reversed_item->quantity                = $quantity * -1;
        $reversed_item->transaction_id          = $last_inserted_reversal;
        $reversed_item->price_in_points         = $itemPricefullpoints * $quantity * (-1);
        $reversed_item->network_id              = $transactionItem->network_id;
        $reversed_item->ref_number              = $transactionItem->id;
        $reversed_item->paid_in_points          = ($transactionItem->paid_in_points / $transactionItem->quantity) * $quantity * -1;
        $reversed_item->paid_in_cash            = ($transactionItem->paid_in_cash / $transactionItem->quantity) * $quantity * -1;
        $reversed_item->cash_currency_id        = $transactionItem->cash_currency_id;

        $reversed_item->save();
//        APIController::postSendEmailJson(json_encode($transactionItem->product_id),'reversed item 003 - '.$transactionItem->id);
        //update main transaction item
        $transactionItem->reversed = 1;
        $transactionItem->reversed_at = date("Y-m-d H:i:s");
        $transactionItem->save();
                
        //reverse Full redemption order
        $params = array();
        $params['trx_id'] = $trx_id;
        $params['trx_item_id'] = $transactionItem->id;
        $params['trx_reversed_item_id'] = $reversed_item->id;
        $params['full_price_in_usd'] = $reversed_item->total_amount;
        $params['reversal_trx_id'] = $last_inserted_reversal;
        $params['qty'] = $quantity;
        $var_redemption_order = RedemptionOrder::reverseRedemptionOrder($params, 'Item');
        //APIController::postSendEmailJson(json_encode($var_redemption_order),'reverseRedemptionOrder 004 - '.$trx_id);
        //check iff all items and deliveries are reversed
        $mainQtyItems       = $transactionItem->quantity;
        $reversedItems  = TransactionItem::where('ref_number', 'like', '%'.$transactionItem->id.'%')->get();
        $itemReversedQty = 0;
        foreach($reversedItems as $reversedItem){
            $itemReversedQty    -= $reversedItem->quantity;
        }
        $remainingItems     = $mainQtyItems - $itemReversedQty;
        $itemDeliveryObj    = TransactionItemDelivery::where('trx_id', $transaction->id)->where('trx_item_id', $transactionItem->id)->where('ref_number', NULL)->first();
        $mainQtyDelivery    = $itemDeliveryObj->quantity;
        $reversedDeliveries  = TransactionItemDelivery::where('ref_number', 'like', '%'.$itemDeliveryObj->id.'%')->get();
        $deliveryReversedQty = 0;
        foreach($reversedDeliveries as $reversedDelivery){
            $deliveryReversedQty    -= $reversedDelivery->quantity;
        }
        $remainingDeliveries    = $mainQtyDelivery - $deliveryReversedQty;
//        APIController::postSendEmailJson(json_encode(array($remainingDeliveries,Auth::user()->id,$trx_id)),"Test 00000001");
//        if($remainingDeliveries == 0 && $remainingItems == 0 ){
//            DB::table('transaction')->where('id', $trx_id)->update(array( 'reversed' => 1, 'auth_staff_id' => Auth::user()->id));
//            DB::table('redemption_order')->where('trx_id', $trx_id)->update(array( 'reversed' => 1));
//        }
//        APIController::postSendEmailJson(json_encode(array($remainingDeliveries,$remainingItems)),'remainingDeliveries 005 - '.$trx_id);
        return Response::json(array(
            'failed'  => false,
            'currency' => Currency::find($reversed_item->cash_currency_id)->short_code,
            'cash'     => abs($reversed_item->paid_in_cash)
        ));
        } 
        catch (Exception $ex) {
            $json_data = json_encode(array($ex->getMessage(), $ex->getLine()));
            APIController::postSendEmailJson($json_data, "DEV issue with reversal ITEM Try Catch 001 ");
        }
//        DB::commit();
    }
    
    /**
     * Reverse a transaction Delivery
     *
     * @param trx_id
     *
     * @return Response
     */
    public function postReverseTransactionDelivery($enc_trxDelivery_id, $quantity){
        try{
        $trxDelivery_id = base64_decode($enc_trxDelivery_id);
        //APIController::postSendEmailJson(json_encode(array($enc_trxDelivery_id,$trxDelivery_id)),'Reverse Delivery 001');
        $transactionDelivery                = TransactionItemDelivery::find($trxDelivery_id);
        $transaction                        = Transaction::find($transactionDelivery->trx_id);
        $transactionItem                    = TransactionItem::find($transactionDelivery->trx_item_id);
        $deliveryCostInPoints               = $transactionDelivery->delivery_cost / $transactionDelivery->quantity;
        $deliveryCurrencyId                 = $transactionDelivery->currency_id;
        $currency                           = Currency::where('id', $deliveryCurrencyId)->first();
        $deliveryCostInUsdArray             = CurrencyHelper::convertToUSD($transactionDelivery->amt_delivery, $currency->short_code);
        $deliveryCostInUsd                  = $deliveryCostInUsdArray['amount']/ $transactionDelivery->quantity;
        $deliveryCostInOriginalCurrency     = $transactionDelivery->amt_delivery / $transactionDelivery->quantity;
        
        
        $user               = User::find($transaction->user_id);
        $trx_id             = $transaction->id;
        
        // First Step is to make a reverse of the main transaction with the value of item (redemption of the negatif of the item)
        $trans_reversal = new Transaction();
//        DB::beginTransaction();
        $trans_reversal->user_id			= $transaction->user_id;
        $trans_reversal->country_id			= $transaction->country_id;
        $trans_reversal->partner_id			= $transaction->partner_id;
        $trans_reversal->trans_class			= 'Reversal Delivery';
        $trans_reversal->source				= $transaction->source;
        $trans_reversal->ref_number			= $transaction->id;
        $trans_reversal->notes				= $transaction->notes;
        $trans_reversal->store_id			= $transaction->store_id;
        $trans_reversal->pos_id				= $transaction->pos_id;
        $trans_reversal->auth_staff_id			= Auth::user()->id;
        $trans_reversal->network_id			= $transaction->network_id;
        $trans_reversal->rule_id			= $transaction->rule_id;
        $trans_reversal->total_amount			= $deliveryCostInUsd * $quantity * (-1);
        $trans_reversal->original_total_amount		= $deliveryCostInOriginalCurrency * $quantity * (-1);
        $trans_reversal->partner_amount 		= $deliveryCostInOriginalCurrency * $quantity * (-1);
        $trans_reversal->points_rewarded		= 0;
        $trans_reversal->points_redeemed		= $deliveryCostInPoints * $quantity * (-1);
        $trans_reversal->points_balance			= null;
        $trans_reversal->trx_reward			= $transaction->trx_reward;
        $trans_reversal->trx_redeem			= $transaction->trx_redeem;
        $trans_reversal->amt_reward			= $transaction->amt_reward * (-1);
        $trans_reversal->amt_redeem			= $deliveryCostInOriginalCurrency * $quantity * (-1);
        $trans_reversal->currency_id			= $transaction->currency_id;
        $trans_reversal->partner_currency_id		= $transaction->currency_id;
        $trans_reversal->exchange_rate			= $transaction->exchange_rate;
        $trans_reversal->product_id			= $transaction->product_id;
        $trans_reversal->product_model			= $transaction->product_model;
        $trans_reversal->product_brand_id		= $transaction->product_brand_id;
        $trans_reversal->product_sub_model		= $transaction->product_sub_model;
        $trans_reversal->product_category_id            = $transaction->product_category_id;
        $trans_reversal->city_id			= $transaction->city_id;
        $trans_reversal->area_id			= $transaction->area_id;
        $trans_reversal->segment_id			= $transaction->segment_id;
        $trans_reversal->category_id			= $transaction->category_id;
        $trans_reversal->quantity			= 0;
        $trans_reversal->auth_role_id			= $transaction->auth_role_id;
        // set the created at and updated as new default values not as old transaction
//        $trans_reversal->created_at			= $transaction->created_at;
//        $trans_reversal->updated_at			= $transaction->updated_at;
        $trans_reversal->delivery_cost			= 0;
        $trans_reversal->source				= $transaction->source;
        $trans_reversal->cash_payment			= 0;
        $trans_reversal->reversed			= 0;
        $trans_reversal->posted_at			= date('Y-m-d H:i:s');
        
        $trans_reversal->save();
        $last_inserted_reversal = $trans_reversal->id;
//APIController::postSendEmailJson(json_encode(array($enc_trxDelivery_id,$trxDelivery_id)),'Reverse Delivery 001 $last_inserted_reversal - '. $last_inserted_reversal);
        
        //return the qty to the product
//        $product->qty = $product->qty + $quantity;
//        $product->save();
        
        // Transaction deduction
        $points_to_deduct = $deliveryCostInPoints * $quantity * (-1);
        DB::table('transaction')->where('id', $trx_id)->update(array( 'reversed_delivery' => 1));
        $user->updateBalance($trans_reversal->points_redeemed, $trans_reversal->network_id, 'subtract');
        $trans_reversal->save();
        //$last_inserted_reversal = $trans_reversal->id;
        $trx_used_deduction = DB::table('transaction_deduction')->select(DB::raw('DISTINCT(trx_id)'))
            ->where('trx_ref_id',$trx_id)->orderBy('id', 'desc')->get();
        $trx_used_deduction_arr = array();
        $got_first = false;
        
        $change_last_trx_unused = true;
        foreach($trx_used_deduction as $tud){
            $trx_deduction = new TransactionDeduction();
            $trx_deduction->trx_ref_id = $last_inserted_reversal;
            $trx_deduction_temp = TransactionDeduction::where('trx_id',$tud->trx_id)->where('trx_used',0)->orderBy('id','DESC')->first();
            $trx_deduction_temp_used = TransactionDeduction::where('trx_id',$tud->trx_id)->where('trx_used',1)->orderBy('id','DESC')->first();
            $trx_deduction_redemption = TransactionDeduction::where('trx_id',$tud->trx_id)->where('trx_ref_id',$trx_id)->orderBy('id','DESC')->first();
//            APIController::postSendEmailJson(json_encode($trx_deduction_temp->toArray()));
//            if($trx_deduction_temp){
//                APIController::postSendEmailJson(json_encode(array($trx_deduction_temp->toArray(), $trx_deduction_redemption->toArray())));
//                if($trx_deduction_redemption->points_used - $trx_deduction_temp->points_remaining  <= 0 && $trx_deduction_temp->id != $trx_deduction_redemption->id){
//                    continue;
//                }
//            }
            if($trx_deduction_temp && !$trx_deduction_temp_used){
//                APIController::postSendEmailJson(json_encode(array($trx_deduction_temp->toArray())),"001");
                $change_last_trx_unused = false;
                $trx_int_deduction_pts_remaining = intval($trx_deduction_temp->points_remaining);
                $trx_int_deduction_pts_used = intval($trx_deduction_redemption->points_used) * (-1);
                $sql = "select sum(points_used) as pts_used from development.transaction_deduction where trx_id = " . $tud->trx_id . " and trx_ref_id in (select id from development.transaction where ref_number like '".$trx_id."') order by id desc limit 1";
                $pts_remaining_last_trx = DB::select($sql);
                $pts_remaining_last_trx_points_remaining =0;
                foreach($pts_remaining_last_trx as $pts_rem_last_trx){
                    $pts_remaining_last_trx_points_remaining = $pts_rem_last_trx->pts_used;
                }
                $available_for_reversal = $trx_deduction_redemption->points_used + $pts_remaining_last_trx_points_remaining;
//                $trx_deduction->points_remaining = $trx_int_deduction_pts_remaining - $trx_int_deduction_pts_used;
//                $trx_deduction->points_used = $trx_deduction_redemption->points_used * (-1);
                if($available_for_reversal >= abs($points_to_deduct)){
                    $trx_deduction->points_remaining = $trx_int_deduction_pts_remaining - $points_to_deduct;
                    $trx_deduction->points_used = $points_to_deduct;  
                    $points_to_deduct += $trx_deduction_redemption->points_used;
                }else{
                    $canBeDeducted  = $available_for_reversal;
                    $trx_deduction->points_remaining = $trx_int_deduction_pts_remaining + $canBeDeducted;
                    $trx_deduction->points_used = $canBeDeducted * (-1);  
                    $points_to_deduct += $canBeDeducted;
                }
                
                
                $trx_deduction->trx_id = $tud->trx_id;
                $trx_deduction->trx_used = 0;//0
                $trx_deduction->used_date = date("Y-m-d H:i:s");
                $trx_deduction->partner_id = Transaction::find($tud->trx_id)->partner_id;
                if($trx_deduction->points_used != 0){
                    $trx_deduction->save();
                }
                
//                APIController::postSendEmailJson(json_encode(array($trx_deduction->toArray())),"002");
                //reverse points issued allocation
                $pointsIssuedAllocation = array();
                $pointsIssuedAllocation['trx_deduction_id'] = $trx_deduction->id;
                $pointsIssuedAllocation['trx_item_id']      = $transactionItem->id; 
                $pointsIssuedAllocation['partner_id']       = $trx_deduction->partner_id;
                $pointsIssuedAllocation['created_at']       = date("Y-m-d H:i:s");
                $pointsIssuedAllocation['updated_at']       = date("Y-m-d H:i:s");
                $pointsIssuedAllocation['points_used']      = $trx_deduction->points_used;
                if($trx_deduction->points_used != 0){
                    DB::table('points_issued_allocation')->insert($pointsIssuedAllocation);
                }
                if($points_to_deduct < 0){
                    continue;
                }else{
                    break;
                }
            }
            elseif($trx_deduction_temp_used && $trx_deduction_temp){
                $change_last_trx_unused = false;
                $trx_int_deduction_pts_remaining = intval($trx_deduction_temp_used->points_remaining);
                $trx_int_deduction_pts_used = intval($trx_deduction_redemption->points_used) ;
//                $trx_deduction->points_remaining = $trx_int_deduction_pts_used;
//                $trx_deduction->points_used = $trx_deduction_redemption->points_used * (-1);

                $sql = "select sum(points_used) as pts_used from development.transaction_deduction where trx_id = " . $tud->trx_id . " and trx_ref_id in (select id from development.transaction where ref_number like '".$trx_id."') order by id desc limit 1";
                $pts_remaining_last_trx = DB::select($sql);
                $pts_remaining_last_trx_points_remaining =0;
                foreach($pts_remaining_last_trx as $pts_rem_last_trx){
                    $pts_remaining_last_trx_points_remaining = $pts_rem_last_trx->pts_used;
                }
                $available_for_reversal = $trx_deduction_redemption->points_used + $pts_remaining_last_trx_points_remaining;
                if($available_for_reversal >= abs($points_to_deduct)){
                    $trx_deduction->points_remaining = $trx_int_deduction_pts_remaining - $points_to_deduct;
                    $trx_deduction->points_used = $points_to_deduct;  
                    $points_to_deduct += $trx_deduction_redemption->points_used;
                }else{
                    $canBeDeducted  = $available_for_reversal;
                    $trx_deduction->points_remaining = $trx_int_deduction_pts_remaining + $canBeDeducted;
                    $trx_deduction->points_used = $canBeDeducted * (-1);  
                    $points_to_deduct += $canBeDeducted;
                }
                    
                $trx_deduction->trx_id = $tud->trx_id;
                $trx_deduction->trx_used = 0;//0
                $trx_deduction->used_date = date("Y-m-d H:i:s");
                $trx_deduction->partner_id = Transaction::find($tud->trx_id)->partner_id;
                if($trx_deduction->points_used != 0){
                    $trx_deduction->save();
                }
                $trx_deduction_temp_used->trx_used = 0;
                $trx_deduction_temp_used->updated_at = date("Y-m-d H:i:s");
                if($trx_deduction->points_used != 0){
                    $trx_deduction_temp_used->save();
                }
//                 APIController::postSendEmailJson(json_encode(array($trx_deduction->toArray())),"003");
                //reverse points issued allocation
                $pointsIssuedAllocation = array();
                $pointsIssuedAllocation['trx_deduction_id'] = $trx_deduction->id;
                $pointsIssuedAllocation['trx_item_id']      = $transactionItem->id; 
                $pointsIssuedAllocation['partner_id']       = $trx_deduction->partner_id;
                $pointsIssuedAllocation['created_at']       = date("Y-m-d H:i:s");
                $pointsIssuedAllocation['updated_at']       = date("Y-m-d H:i:s");
                $pointsIssuedAllocation['points_used']      = $trx_deduction->points_used;
                if($trx_deduction->points_used != 0){
                    DB::table('points_issued_allocation')->insert($pointsIssuedAllocation);
                }
                if($points_to_deduct < 0){
                    continue;
                }else{
                    break;
                }
            }
            else{
                $change_last_trx_unused = false;
                $trx_int_deduction_pts_remaining = 0;
                $trx_int_deduction_pts_used = intval($trx_deduction_redemption->points_used) ;
//                $trx_deduction->points_remaining = $trx_int_deduction_pts_used;
//                $trx_deduction->points_used = $trx_deduction_redemption->points_used * (-1);
                $sql = "select sum(points_used) as pts_used from development.transaction_deduction where trx_id = " . $tud->trx_id . " and trx_ref_id in (select id from development.transaction where ref_number like '".$trx_id."') order by id desc limit 1";
                $pts_remaining_last_trx = DB::select($sql);
                $pts_remaining_last_trx_points_remaining =0;
                foreach($pts_remaining_last_trx as $pts_rem_last_trx){
                    $pts_remaining_last_trx_points_remaining = $pts_rem_last_trx->pts_used;
                }
                $available_for_reversal = $trx_deduction_redemption->points_used + $pts_remaining_last_trx_points_remaining;
                if($available_for_reversal >= abs($points_to_deduct)){
                    $trx_deduction->points_remaining = $trx_int_deduction_pts_remaining - $points_to_deduct;
                    $trx_deduction->points_used = $points_to_deduct;  
                    $points_to_deduct += $trx_deduction_redemption->points_used;
                }else{
                    $canBeDeducted  = $available_for_reversal;
                    $trx_deduction->points_remaining = $trx_int_deduction_pts_remaining + $canBeDeducted;
                    $trx_deduction->points_used = $canBeDeducted * (-1);  
                    $points_to_deduct += $canBeDeducted;
                }
                    
                $trx_deduction->trx_id = $tud->trx_id;
                $trx_deduction->trx_used = 0;//0
                $trx_deduction->used_date = date("Y-m-d H:i:s");
                $trx_deduction->partner_id = Transaction::find($tud->trx_id)->partner_id;
                if($trx_deduction->points_used != 0){
                    $trx_deduction->save();
                    
                    $trx_deduction_temp_used->trx_used = 0;
                    $trx_deduction_temp_used->updated_at = date("Y-m-d H:i:s");
                    $trx_deduction_temp_used->save();
                
//                    APIController::postSendEmailJson(json_encode(array($trx_deduction->toArray())),"004");
                    //reverse points issued allocation
                    $pointsIssuedAllocation = array();
                    $pointsIssuedAllocation['trx_deduction_id'] = $trx_deduction->id;
                    $pointsIssuedAllocation['trx_item_id']      = $transactionItem->id; 
                    $pointsIssuedAllocation['partner_id']       = $trx_deduction->partner_id;
                    $pointsIssuedAllocation['created_at']       = date("Y-m-d H:i:s");
                    $pointsIssuedAllocation['updated_at']       = date("Y-m-d H:i:s");
                    $pointsIssuedAllocation['points_used']      = $trx_deduction->points_used;
                    DB::table('points_issued_allocation')->insert($pointsIssuedAllocation);
                }
                if($points_to_deduct < 0){
                    continue;
                }else{
                    break;
                }
            }
        }
        //APIController::postSendEmailJson(json_encode(array($enc_trxDelivery_id,$trxDelivery_id)),'Reverse Delivery 002 - '. $last_inserted_reversal);
        $currency_temp = Currency::find($transactionDelivery->currency_id);
        //reverse item in transaction item table
        $reversed_item_delivery	= new TransactionItemDelivery();
        $reversed_item_delivery->trx_id         = $last_inserted_reversal;
        $reversed_item_delivery->trx_item_id    = $transactionDelivery->trx_item_id;
        $reversed_item_delivery->delivery_cost  = $deliveryCostInPoints * $quantity * (-1);
        $reversed_item_delivery->network_id     = $transactionDelivery->network_id;
        $reversed_item_delivery->amt_delivery   = PointsHelper::pointsToAmount($deliveryCostInPoints * $quantity * (-1), $currency_temp->short_code, $transactionDelivery->network_id);//$transactionDelivery->network_id;
        $reversed_item_delivery->currency_id    = $transactionDelivery->currency_id;
        $reversed_item_delivery->reversed       = 0;
        $reversed_item_delivery->quantity       = $quantity * (-1);
        $reversed_item_delivery->ref_number     = $transactionDelivery->id;

        $reversed_item_delivery->save();
        APIController::postSendEmailJson(json_encode(array($reversed_item_delivery->amt_delivery,$reversed_item_delivery->delivery_cost, $reversed_item_delivery->id)),'Reverse Delivery 003 - '. $last_inserted_reversal);
        
        //update main transaction item deliverry
        $transactionDelivery->reversed = 1;
        $transactionDelivery->reversed_at = date("Y-m-d H:i:s");
        $transactionDelivery->save();
        
        //update main transaction item
        $transactionItem->reversed_delivery = 1;
        $transactionItem->reversed_at = date("Y-m-d H:i:s");
        $transactionItem->save();
                
        //reverse Full redemption order
        $params = array();
        $params['trx_id'] = $trx_id;
        $params['trx_item_id'] = $transactionItem->id;
        $params['full_price_in_usd'] = $reversed_item_delivery->amt_delivery;
        $params['reversal_trx_id'] = $last_inserted_reversal;
        $params['qty'] = $quantity;
        RedemptionOrder::reverseRedemptionOrder($params, 'Delivery');
        
        //check iff all items and deliveries are reversed
        $mainQtyDelivery      = $transactionDelivery->quantity;
        $reversedDeliveries  = TransactionItemDelivery::where('ref_number', 'like', '%'.$transactionDelivery->id.'%')->get();
        $deliveryReversedQty = 0;
        foreach($reversedDeliveries as $reversedDeliveries){
            $deliveryReversedQty    -= $reversedDeliveries->quantity;
        }
        
        $remainingDeliveries     = $mainQtyDelivery - $deliveryReversedQty;
        //APIController::postSendEmailJson(json_encode(array($remainingDeliveries)),'Reverse Delivery 004 - '. $last_inserted_reversal);
        
        $trxItemObj    = TransactionItem::where('id', $transactionDelivery->trx_item_id)->first();
        $mainQtyItems    = $trxItemObj->quantity;
        $reversedItems  = TransactionItem::where('ref_number', 'like', '%'.$trxItemObj->id.'%')->get();
        $itemsReversedQty = 0;
        //APIController::postSendEmailJson(json_encode(array($mainQtyItems,$itemsReversedQty)),'Reverse Delivery 005 - '. $last_inserted_reversal);
        foreach($reversedItems as $reversedItem){
            $itemsReversedQty    -= $reversedItem->quantity;
        }
        $remainingItems    = $mainQtyItems - $itemsReversedQty;
        //APIController::postSendEmailJson(json_encode(array($remainingItems)),'Reverse Delivery 006 - '. $last_inserted_reversal);
//        if($remainingItems == 0 && $remainingDeliveries == 0){
//            DB::table('transaction')->where('id', $trx_id)->update(array( 'reversed' => 1, 'auth_staff_id' => Auth::user()->id));
//            DB::table('redemption_order')->where('trx_id', $trx_id)->where('trx_item_id', $transactionItem->id)->update(array( 'reversed' => 1));
//        }
        }
        catch (Exception $ex) {
            $json_data = json_encode(array($ex->getMessage(), $ex->getLine()));
            self::postSendEmailJson($json_data, "DEV issue with reversal DELIVERY Try Catch 001 ");
        }
//        DB::commit();
    }
    
    /** New product Pricing functions */
    
    /**
     * Load a product using its UCID
     *
     * @param string $ucid
     * @return void
     */
    public function getLoadProductsNew($search = null, $networkId = 1, $countryId = 6, $channelId = 1)
    {
            $products = ProductsHelper::searchProducts($search, $countryId, $channelId);
                             
        return View::make("ticket.view_product", array(
            'products' => $products,
			'network_id' => $networkId,
			'country_id' => $countryId,
			'channel_id' => $channelId,
            'languageKeysCtrl' => DashboardController::platformLanguageKeys()
        ));
    }
    
    /**
     * Handle the redemption transaction
     *
     * @return void
     */
    public function postRedeemItemNew()
    {
        $member_id  = base64_decode(self::sanitizeText(Input::get('user_id')));
        $product_id = self::sanitizeText(Input::get('product_id'));
	$networkId	= self::sanitizeText(Input::get('network_id', 1));
	$countryId	= self::sanitizeText(Input::get('country_id', 1));
	$channelId	= self::sanitizeText(Input::get('channel_id', 1));

        // Create the transaction

        $payload = array();

        $product	  = new \stdClass();
        $product->id  = $product_id;
        $product->qty = 1;//self::sanitizeText(Input::get('quantity'));

        $payload[] = $product;
        $memberDetails = User::find($member_id);
        $partner_id = '';
//			var_dump(Auth::User()->getTopLevelPartner()->id);
//		exit;
//        foreach($memberDetails->partners as $p){
//            if (Auth::User()->partners->contains($p)){
//                $partner_id = $p->id;
//            }
//        }
	$partner_id = Auth::User()->getTopLevelPartner()->id;
        //$partnerDetails = Partner::find($partner_id);
        $storeDetails	= Store::where("partner_id", $partner_id)->where('name', 'LIKE', '%call center%')->first();
        
	$store		= Store::find($storeDetails->id);
        $pos_id		= $store->pointOfSales->first()->id;

        $result = Transaction::doRedemptionNew(array(
            'user_id'		=> $member_id,
            'items'		=> $payload,
            'reference'		=> "Call Center Redemption",
            'staff_id'		=> Auth::User()->id,
            'partner'		=> $partner_id,
            'store_id'		=> $storeDetails->id,
            'pos_id'		=> $pos_id,
            'network_id'	=> $networkId,
            'posted_at'         => date('Y-m-d H:i:s'),
             'country' => $countryId,
            'channel' => $channelId
        ), "Items");

        if($result->status == Transaction::TRANS_SUCCESS) {
            return Response::json(array(
                'failed'  => false,
                'message' => "The item has been successfully redeemed on behalf of the user"
            ));
        } else {
            return Response::json(array(
                'failed'  => true,
                'message' => $result->message
            ));
        }
    }
    
} // EOC
