<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\View;
use Input;
use Validator;
use I;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use ManagedObjectsHelper;
use CSVHelper;
use App\Country as Country;
use App\Batch as Batch;
use App\Coupon as Coupon;
use CouponHelper;
/**
 * Batch Controller
 *
 * @category   Controllers
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class BatchController extends BaseController
{
    /**
     * Return the resource validations
     *
     * @return array
     */
    private function getValidations()
    {
        return array(
            'valid_from'      => 'date',
            'valid_to'        => 'date',
            'partner_id'      => 'required|integer',
            'country_id'      => 'integer',
            'number_of_items' => 'required|integer|min:1|max:2000',
            'type' => 'required'
        );
    }

    /**
     * Fetch the contextual lists
     *
     * @return array
     */
    private function getAllLists()
    {
        return array(
            'partners'  => ManagedObjectsHelper::managedPartners()->sortBy('name')->pluck('name', 'id'),
			'partner'   => Auth::User()->managedPartners(),
            'partnerData' => ManagedObjectsHelper::managedPartners()->sortBy('name')->pluck('name', 'id'),
            'segments'  => ManagedObjectsHelper::managedSegments()->sortBy('name')->pluck('name', 'id'),
            'countries' => Country::orderBy('name')->pluck('name', 'id'),
        );
    }

    /**
     * Override point to filter results before pagination
     *
     * @param $results
     * @return mixed
     */
    protected function paginate($results)
    {
        $results = $results->FieldEqualsValue('type','Coupons');

        return parent::paginate($results);
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
        if(!I::can('view_coupons')){
            return Redirect::to(url("dashboard"));
        }

        if (I::am('BLU Admin')) {
            $batches = Batch::where('draft', false)->where('type', 'Coupons')->orderBy('id');
        } else {
            $partnerIds = Auth::User()->managedPartnerIDs();
            $batches = Batch::where('draft', false)->where('type', 'Coupons')->whereIn('partner_id', $partnerIds);
        }

        if (Input::has('q')) {
            $name = self::sanitizeText(Input::get('q'));
            $batches = $batches->where('name', 'like', "%{$name}%");
        }

        if (Input::has('partner_list')) {
            $batches = $batches->whereIn('partner_id', explode(',', Input::get('partner_list')));
        }

        $batches = $batches->orderBy('id','desc');
        $batches = $batches->paginate(15);

        return View::make("coupon.batch.list", [
            'batches' => $batches,
            'lists' => $this->getAllLists(),
        ]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function getCreate()
	{
        if (!I::can('create_coupons')){
            return Redirect::to(url("dashboard"));
        }

        $batch             = new Batch();
        $batch->type       = 'Coupons';
        $batch->valid_from = date('Y-m-d H:i:s', time());
        $batch->valid_to   = date('Y-m-d H:i:s', time());
        $batch->save();
        $batchId = base64_encode($batch->id);

        return Redirect::to(url("dashboard/coupons/edit/{$batchId}"));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getEdit($enc_id = null)
	{
        $id = base64_decode($enc_id);
        if(!I::can('view_coupons')){
            return Redirect::to(url("dashboard"));
        }

        $batch = Batch::find($id);

        if(!$batch) {
            \App::abort(404);
        }

        $managedPartners = Auth::User()->managedPartners();
        $can_view_batch = false;
        foreach($managedPartners as $manP){
            if($manP->id == $batch->partner_id){
                $can_view_batch = true;
                break;
            }
        }

        if ($can_view_batch == false && $batch->draft == false) {
            return Redirect::to(url("dashboard"));
        }

        return View::make("coupon.batch.view", array_merge(
            array('batch' => $batch), $this->getAllLists()
        ));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param int $id
	 * @return Response
	 */
	public function postUpdate($enc_id = null)
	{
        $id = base64_decode($enc_id);
        if(!I::can('create_coupons') && !I::can('edit_coupons')){
            return Redirect::to(url("dashboard"));
        }
        $batch = Batch::find($id);

        if(!$batch) {
            \App::abort(404);
        }

        $validator = Validator::make(Input::all(), $this->getValidations());
        if($validator->fails()) {
            return Redirect::to('dashboard/coupons/edit/'.base64_encode($id))->withErrors($validator)->withInput();
        }

		$coupnType = self::sanitizeText(Input::get('type'));

        $batch->valid_from           = date('Y-m-d', strtotime( self::sanitizeText(Input::get('valid_from')) ));
        $batch->valid_to             = date('Y-m-d', strtotime( self::sanitizeText(Input::get('valid_to'))));
        $batch->name                 = self::sanitizeText(Input::get('name'));
        $batch->price                = self::sanitizeText(Input::get('price', 0));
        $batch->currency_id          = self::sanitizeText(Input::get('currencies'));
        $batch->country_id           = self::sanitizeText(Input::get('country_id'));
        $batch->segment_id           = self::sanitizeText(Input::get('segment_id'));

        if ($batch->draft) {
            $batch->number_of_items  = self::sanitizeText(Input::get('number_of_items'));
        }
        
        $batch->terms_and_conditions = self::sanitizeText(Input::get('terms_and_conditions'));
        $batch->redeem_sms           = self::sanitizeText(Input::get('redeem_sms'));
        $batch->partner_id           = self::sanitizeText(Input::get('partner_id'));
        $batch->draft                = false;
        $batch->save();
        
        // Generate the items
        for ($i = 0; $i < (int)$batch->number_of_items; $i++) {
            // Create the initial coupon
            $coupon             = new Coupon();
            $coupon->batch_id   = $batch->id;
            $coupon->partner_id = $batch->partner_id;
            $coupon->coupon_type = $coupnType;

            $coupon->save();
       
            // Create the code
            $coupon->coupon_code = $coupon->id.'-'.CouponHelper::generateRandomString();
            $coupon->save();
        }

        return Redirect::to(Input::get('redirect_to', 'dashboard/coupons'));
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getDestroy($enc_id)
	{
        $id = base64_decode($enc_id);
        if(!I::can('delete_coupons')){
            return Redirect::to(url("dashboard"));
        }
        $batch = Batch::find($id);

        if(!$batch) {
//            App::abort(404);
            return Redirect::to('dashboard/coupons')->with(
                            "flash_error", "Coupon not found."
            );
        }

        $batch->delete();

        return Redirect::to(url("dashboard/coupons"));
 	}

    /**
   	 * Display the verification screen
   	 *
   	 * @return
   	 */
   	public function getVerify()
   	{
        return View::make("coupon.verify");
    }

    /**
   	 * show the verify screen.
   	 *
   	 * @param $id
   	 * @return
   	 */
   	public function getVerifycoupon($enc_id)
   	{
        $id = base64_decode($enc_id);
        $coupon = Coupon::FieldEqualsValue('coupon_code', $id)->get()->first();

        if(!$coupon) {
            \App::abort(404);
        }

        return View::make("coupon.info", array(
            'coupon' => $coupon
        ));
    }

    /**
      * Download a specified batch of items
      *
      * @param int $id
      * @param string $format [csv, xls]
      * @return void
      */
    public function getDownload($enc_id = null, $format = 'csv')
    {
        $id = base64_decode($enc_id);
        if(!I::can('view_coupons')){
            return Redirect::to(url("dashboard"));
        }
        $batch = Batch::find($id);

        if(!$batch) {
            \App::abort(404);
        }

        $buffer = [];

        $fields = array('coupon_code', 'valid_from', 'valid_to', 'price', 'terms_and_conditions', 'created_at');

        foreach($batch->coupons as $coupon) {
            $row = [];
            $row[] = $coupon->coupon_code;
            $row[] = $coupon->batch->valid_from;
            $row[] = $coupon->batch->valid_to;
            $row[] = $coupon->batch->price;
            $row[] = $coupon->batch->terms_and_conditions;
            $row[] = $coupon->created_at->toDateTimeString();
            $buffer[] = $row;
        }

        switch($format) {
            case "csv":

                $file = storage_path() . "/excel/batch_{$batch->id}.csv";
                CSVHelper::writeToFile($buffer, $file);

                return Response::download($file);

            case "xls":
                array_unshift($buffer, $fields);

                $file = storage_path() . "/excel/batch_{$batch->id}.xls";
                CSVHelper::writeToFile($buffer, $file);

                return Response::download($file);
        }
     }

	/**
	 * get countries of a specific partner
	 *
	 * @param int $partnerId
	 * @return Response
	 */
	public function getPartnercountries( $enc_partnerId )
	{
        $partnerId = base64_decode($enc_partnerId);
		$return		= '';
		$partners	= Auth::User()->managedPartners();
        $countries	= Country::orderBy('name')->pluck('name', 'id');

        $partnerCountries = [];
		foreach ($partners as $p) {
		    if ($p->id == $partnerId) {
                if (count($p->countries) > 0 ) {
                   foreach($p->countries as $c) {
                       $partnerCountries[$c->country_id] = $countries[$c->country_id];
                   }
                } else {
                    $partnerCountries = $countries;
                }
			}
		}

		foreach($partnerCountries as $countryId => $countryName){
			$return .= '<option value="'. $countryId .'">'. $countryName .'</option>';
		}

		return $return;
	}

} // EOC