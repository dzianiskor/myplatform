<?php
namespace App\Http\Controllers;
use I;
use View;
use App\RedemptionOrder as RedemptionOrder;;
use Input;
use Illuminate\Support\Facades\DB;
use App\Supplier as Supplier;
use App\Currency as Currency;
use Meta;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

/**
 * Redemption Order Controller
 * 
 * @category   Controllers
 * @package    BLU
 * @author     Magid Mroueh <magid.mroueh@bluloyalty.com>
 * @copyright  2016 BLU
 * @link       http://blupoints.com
 */
class RedemptionOrderController extends BaseController {

    /**
     * Get all list data
     *
     * @return array
     */
    private function getAllLists() {
        return array(
            'suppliers' => Supplier::where('draft', false)->orderBy('name')->pluck('name', 'id'),
            'currencies' => Currency::where('draft', false)->orderBy('name')->pluck('name', 'id'),
            'orderStatuses' => Meta::orderStatuses(),
        );
    }

    /**
     * Fetch a list of items assigned to the particular user
     * 
     * @param $int $user_id
     * @return View
     */
    public function listRedemptionOrders() {
        if (!I::can('view_redemption_order')) {
            return Redirect::to(url("dashboard"));
        }
//        $redemptionOrder = DB::table('redemption_order');
        $redemptionOrder = new RedemptionOrder();

        if (Input::has('q')) {
            $term = self::sanitizeText(Input::get('q'));
            $redemptionOrder = $redemptionOrder->whereRaw('redemption_order.trx_id like "%' . $term . '%"');

            $users = Auth::User()->managedMembers(false);

//            $users->where('verified', true);
            $users->where(function($query) use ($term) {
                $query->orWhere('users.username', 'like', "%" . $term . "%")
                        ->orWhere('users.id', 'like', "%" . $term . "%")
                        ->orWhere('users.first_name', 'like', "%" . $term . "%")
                        ->orWhere('users.middle_name', 'like', "%" . $term . "%")
                        ->orWhere('users.last_name', 'like', "%" . $term . "%")
                        ->orWhere('reference.number', 'like', "%" . $term . "%");
            });
            $users->leftJoin('reference', 'users.id', '=', 'reference.user_id');
            $users->select('users.id');
            $usersArray = array(-1);
            foreach ($users->get() as $user) {
                $usersArray[] = $user->id;
            }
            $redemptionOrder = $redemptionOrder->orWhere(function($query) use ($usersArray) {
                $query->whereIn('redemption_order.user_id', $usersArray);
            });
        }

        $redemptionOrder = $redemptionOrder->groupBy('redemption_order.trx_id');
        $redemptionOrder = $redemptionOrder->orderBy('redemption_order.trx_id', 'DESC');
        $redemptionOrder = $redemptionOrder->paginate(15);

        return View::make('redemption_order.list', array(
                    'redemptionOrders' => $redemptionOrder
        ));
    }

    /**
     * Fetch Redemption Order to Edit
     * 
     * @param $int $redemptionOrderId
     * @return View
     */
    public function viewRedemptionOrder($enc_redemptionOrderId) {
        $redemptionOrderId = base64_decode($enc_redemptionOrderId);
        if (!I::can('view_redemption_order')) {
            return Redirect::to(url("dashboard"));
        }
        $redemptionOrders = DB::table('redemption_order')->where('trx_id', $redemptionOrderId)->get();
        $redemptionOrder    = DB::table('redemption_order')->where('trx_id', $redemptionOrderId)->first();
        $redemptionOrder = DB::table('redemption_order')->where('trx_id', $redemptionOrderId)->first();
        return View::make('redemption_order.view', array(
                    'redemptionOrders' => $redemptionOrders,
                    'redOrder' => $redemptionOrder,
                    'lists' => $this->getAllLists()
        ));
    }

    /**
     * Fetch Redemption Order to Edit
     * 
     * @param $int $redemptionOrderId
     * @return View
     */
    public function updateRedemptionOrder($enc_redemptionOrderId) {
        $redemptionOrderId = base64_decode($enc_redemptionOrderId);
        
            if(!I::can('edit_redemption_order')){
            return Redirect::to(url("dashboard"));
        }
        $redemptionOrder    = RedemptionOrder::find($redemptionOrderId);
        $input = Input::all();
        foreach($input as $k=>$i){
            $input[$k] = self::sanitizeText($i);
        }
        //Saving new info
        if ($redemptionOrder->type == 'Item') {
            $validator = Validator::make(Input::all(), array(
                        'item_cost' => 'required',
                        'currency' => 'required',
                        'delivery_cost' => 'required',
                        'delivery_currency' => 'required',
                        'payment_gateway_cost' => 'required',
                        'payment_gateway_cost_currency' => 'required',
                        'order_status' => 'required',
                        'suppliers' => 'required'
            ));

            if($validator->fails()) {
//                return Redirect::to('dashboard/redemption_order/view/'.$redemptionOrderId)->withErrors($validator);
                return Redirect::to('dashboard/redemption_order/view/' . base64_encode($redemptionOrder->trx_id))->withErrors($validator);
            }
            $redemptionOrder->cost = self::sanitizeText(Input::get('item_cost'));
            $redemptionOrder->cost_currency = self::sanitizeText(Input::get('currency'));
            $redemptionOrder->delivery_cost = self::sanitizeText(Input::get('delivery_cost'));
            $redemptionOrder->delivery_cost_currency = self::sanitizeText(Input::get('delivery_currency'));
            $redemptionOrder->payment_gateway_cost = self::sanitizeText(Input::get('payment_gateway_cost'));
            $redemptionOrder->payment_gateway_cost_currency = self::sanitizeText(Input::get('payment_gateway_cost_currency'));
            $redemptionOrder->order_status = self::sanitizeText(Input::get('order_status'));
            if(Input::has('order_date')){
            $redemptionOrder->order_date = date("Y-m-d", strtotime(self::sanitizeText(Input::get('order_date'))));
            }
            $redemptionOrder->tracking_number = self::sanitizeText(Input::get('tracking_number'));
            $redemptionOrder->shipping_company = self::sanitizeText(Input::get('shipping_company'));
            $redemptionOrder->supplier_id = self::sanitizeText(Input::get('suppliers'));
        } else {
            $validator = Validator::make(Input::all(), array(
                        'cost' => 'required',
                        'currency' => 'required',
                        'airline_fees' => 'required',
                        'airline_fees_currency' => 'required',
                        'payment_gateway_cost' => 'required',
                        'payment_gateway_cost_currency' => 'required',
                        'order_status' => 'required',
                        'suppliers' => 'required'
            ));

            if ($validator->fails()) {
                return Redirect::to('dashboard/redemption_order/view/' . base64_encode($redemptionOrder->trx_id))->withErrors($validator);
            }

            $redemptionOrder->cost = self::sanitizeText(Input::get('cost'));
            $redemptionOrder->cost_currency = self::sanitizeText(Input::get('currency'));
            $redemptionOrder->airline_fees = self::sanitizeText(Input::get('airline_fees'));
            $redemptionOrder->airline_fees_currency = self::sanitizeText(Input::get('airline_fees_currency'));
            $redemptionOrder->payment_gateway_cost = self::sanitizeText(Input::get('payment_gateway_cost'));
            $redemptionOrder->payment_gateway_cost_currency = self::sanitizeText(Input::get('payment_gateway_cost_currency'));
            $redemptionOrder->order_status = self::sanitizeText(Input::get('order_status'));
            $redemptionOrder->order_date = date("Y-m-d", strtotime(self::sanitizeText(Input::get('order_date'))));
            $redemptionOrder->supplier_id = self::sanitizeText(Input::get('suppliers'));
        }
        $redemptionOrder->save();
        $ROTrxId = base64_encode($redemptionOrder->trx_id);
        return Redirect::to('dashboard/redemption_order/view/' . $ROTrxId);
//        return View::make('redemption_order.view', array(
//            'redOrder' => $redemptionOrder,
//            'lists'             => $this->getAllLists()
//        ));
    }


} // EOC
