<?php
namespace App\Http\Controllers;
use View;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;

/**
 * Locale Controller
 * 
 * @category   Controllers
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class LocaleController extends BaseController
{
    /**
     * List all countries 
     * 
     * @return Response
     */
    public function listCountries()
    {
        return View::make("locale.countries", array(
            'countries' => DB::table('country')->orderBy('name', 'asc')->get()
        ));        
    }

    /**
     * List all areas or only areas specific to a country
     *
     * @param int $country_id
     * @return Response
     */
    public function listAreas($country_id = null)
    {
        $areas = null;

        if(!empty($country_id)) {
            $areas = DB::table('area')->where('country_id', $country_id)->orderBy('name', 'asc')->get();
        } else {
            $areas = DB::table('area')->orderBy('name', 'asc')->get();
        }

        return View::make("locale.areas", array(
            'areas' => $areas
        ));        
    }

    /**
     * List all cities or only cities specific to an area
     *
     * @param int $area_id
     * @return Response
     */
    public function listCities($area_id = null)
    {
        $cities = null;

        if(!empty($area_id)) {
            $cities = DB::table('city')->where('area_id', $area_id)->orderBy('name', 'asc')->get();
        } else {
            $cities = DB::table('city')->orderBy('name', 'asc')->get();
        }

        return View::make("locale.cities", array(
            'cities' => $cities
        ));        
    }

} // EOC