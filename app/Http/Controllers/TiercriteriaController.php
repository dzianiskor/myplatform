<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Response;
use View;
use Input;
use Validator;

use I;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Redirect;

use ManagedObjectsHelper;

use Illuminate\Support\Facades\DB;
use App\Tier as Tier;
use \App\Tiercriteria as Tiercriteria;

/**
 * Store Controller
 * 
 * @category   Controllers
 * @package    BLU
 * @author     Ralph Nader <ralph@bluloyalty.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class TiercriteriaController extends BaseController
{
    /**
     * Fetch the select drop down data for the tiercriteria popup
     *
     * @return array
     */
    private function getSelectData()
    {
        return array(
            'tiers' => DB::table('tier')->orderBy('name', 'asc')->get(),
        );
    }

    /**
     * Create and return a default draft tier tiercriteria
     *
     * @param int $tier_id
     * @return Result
     */
    private function getTiercriteriaDraft($tier_id)
    {
        $tiercriteria = new \App\Tiercriteria();

        $tiercriteria->name       = 'New Tier Criterion';
        $tiercriteria->tier_id    = $tier_id;

        $tiercriteria->save();

        return $tiercriteria;
    }

    /**
     * Display the new tiercriteria page
     *
     * @return View
     */
    public function newTiercriteria($enc_tier_id = null)
    {
        $data = array(
            'tiercriteria'   => $this->getTiercriteriaDraft($enc_tier_id),
        );

        return View::make("tiercriteria.form", array_merge(
            $data, $this->getSelectData()
        ));
    }

    /**
     * Display the edit tiercriteria page
     *
     * @return View
     */
    public function editTiercriteria($enc_tiercriteria_id = null)
    {
        $tiercriteria_id =  base64_decode($enc_tiercriteria_id);
        $data = array(
            'tiercriteria'   => \App\Tiercriteria::find($tiercriteria_id),
        );

        return View::make("tiercriteria.form", array_merge(
            $data, $this->getSelectData()
        ));
    }

    /**
     * Create a new tiercriteria resource
     *
     * @param int $tiercriteria_id
     * @return void
     */
    public function updateTiercriteria($enc_tiercriteria_id = null)
    {
        $tiercriteria_id =  base64_decode($enc_tiercriteria_id);
        $rules = array(
            'tiercriteria_name' => 'required|min:1'
        );
        $input = Input::all();
        foreach($input as $k=>$i){
            $input[$k] = self::sanitizeText($i);
        }
        $validator = Validator::make($input, $rules);

        if($validator->fails()) {
            return Response::json(array(
                'failed' => true,
                'errors' => "Please ensure that you provide a valid criteria name"
            ));
        }

        $tiercriteria = \App\Tiercriteria::find($tiercriteria_id);

        // Details
        $tiercriteria->criterion       = self::sanitizeText(Input::get('tiercriteria_criterion'));
        $tiercriteria->name        = self::sanitizeText(Input::get('tiercriteria_name'));
        $tiercriteria->start_value        = self::sanitizeText(Input::get('tiercriteria_start_value'));
        $tiercriteria->end_value        = self::sanitizeText(Input::get('tiercriteria_end_value'));
        $tiercriteria->expiry_in_months        = self::sanitizeText(Input::get('tiercriteria_expiry_in_months'));
        
        $tiercriteria->draft      = false;

        $tiercriteria->push();

        return Response::json(array(
            'failed' => false,
            'view_url' => url('dashboard/tiercriteria/list/'.$tiercriteria->tier_id)
        ));
    }

    /**
     * Delete the specified tiercriteria
     *
     * @return View
     */
    public function deleteTiercriteria($enc_tiercriteria_id = null)
    {
        $tiercriteria_id =  base64_decode($enc_tiercriteria_id);
        \App\Tiercriteria::find($tiercriteria_id)->delete();

        return Response::json(array(
            'failed' => false
        ));
    }

    /**
     * Render a list of tiercriterias relevant to the Partner
     *
     * @param int $tier_id
     * @return View
     */
    public function listTiercriterias($enc_tier_id = null)
    {
        if(is_numeric($enc_tier_id)){
            $tier_id = $enc_tier_id;
        }
        else{
            $tier_id =  base64_decode($enc_tier_id,TRUE);
        }
        
        //APIController::postSendEmailJson($tier_id, "list tier criteria 1");
        $data = array(
            'tiercriterias' => DB::table('tier_criteria')->where('tier_id', $tier_id)->where('draft', false)->get()
        );

        return View::make("tiercriteria.list", $data);
    }

} // EOC