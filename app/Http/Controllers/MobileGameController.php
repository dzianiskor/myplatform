<?php
namespace App\Http\Controllers;
use App\Media;
use Illuminate\Support\Facades\Auth;
use View;
use Input;
use Validator;
use Meta;
use ManagedObjectsHelper;
use App\Country as Country;
use App\MobileGame as MobileGame;
use App\Partner as Partner;
use Illuminate\Support\Facades\Redirect;
use I;

/**
 * Mobile Game Controller
 *
 * @category   Controllers
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class MobileGameController extends BaseController {

    protected $layout = "layouts.dashboard";

    private function getValidations() {
        return array(
            'name' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
        );
    }

    /**
     * Get all list data
     *
     * @return array
     */
    private function getAllLists() {

        return array(
            'partners'         => ManagedObjectsHelper::managedPartners()->sortBy('name')->pluck('name', 'id'),
            'countries'        => Country::orderBy('name')->pluck('name', 'id'),

        );
    }

    /**
     * Display a list of the currencies
     *
     * @return void
     */
    public function listMobilegames() {
        if (!I::can('view_mobile_game')) {
            return Redirect::to(url("dashboard"));
        }
        $this->ormClass = 'App\MobileGame';
        $this->search_fields = array('name');

        $all_mobilegames = $this->getList();

        $all_managed_partners = ManagedObjectsHelper::managedPartners()->sortBy('name')->pluck('id')->toArray();
            $allowed_mobilegames = array();
            $unique_mobilegames = array();

        foreach ($all_mobilegames as $mob_game) {

            foreach ($mob_game->partners as $part) {

                if (in_array($part->id, $all_managed_partners)) {
                    if (!in_array($mob_game->id, $unique_mobilegames)) {
                            $allowed_mobilegames[] = $mob_game;
                            $unique_mobilegames[] = $mob_game->id;
                        }
                    }
                }
            }
        

        return View::make("mobilegame.list", array(
            'mobilegames' => $allowed_mobilegames //$this->getList()
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getCreate() {
        if (!I::can('view_mobile_game')) {
            return Redirect::to(url("dashboard"));
        }
        $mobilegame = new MobileGame();
        $mobilegame->name = "New Mobile Game";
        $mobilegame->start_date = date('Y-m-d');
        $mobilegame->end_date = date('Y-m-d', strtotime("+1 day"));
        $mobilegame->save();

        $partner = Partner::find(Auth::User()->getTopLevelPartner()->id);

        if (!$mobilegame->partners->contains($partner)) {
            $mobilegame->partners()->attach($partner);
        }

        $mobileGameId = base64_encode($mobilegame->id);
        return Redirect::to(url("dashboard/mobilegame/edit/$mobileGameId"));
    }

    /**
     * Show the currency edit page
     *
     * @param $enc_id
     * @return Response
     */
    public function getEdit($enc_id) {
        $id = base64_decode($enc_id);
        if (!I::can('view_mobile_game')) {
            return Redirect::to(url("dashboard"));
        }
        
        $mobilegame = MobileGame::find($id);
        
        if (!$mobilegame) {
            App::abort(404);
        }

        $managedPartners = Auth::User()->managedPartners();
        $can_view_game = false;
        foreach ($managedPartners as $manP) {
            foreach ($mobilegame->partners as $part) {
                if ($manP->id == $part->id) {
                    $can_view_game = true;
                    break;
                }
            }
        }
        if ($can_view_game == false) {
            return Redirect::to(url("dashboard"));
        }
        
        return View::make("mobilegame.view", array_merge(
            $this->getAllLists(), array('mobilegame' => $mobilegame)
        ));
    }

    /**
     * Delete the currency
     *
     * @param $enc_id
     * @return void
     */
    public function getDelete($enc_id) {
        $id = base64_decode($enc_id);
        if (!I::can('view_mobile_game')) {
            return Redirect::to(url("dashboard"));
        }
        $mobilegame = MobileGame::find($id);

        if (!$mobilegame) {
            App::abort(404);
        }

        $mobilegame->delete();

        return Redirect::to(url("dashboard/mobilegame"));
    }

    /**
     * apply changes
     *
     * @param $enc_id
     * @return Response
     */
    public function postUpdate($enc_id) {
        $id = base64_decode($enc_id);
        if (!I::can('edit_mobile_game') && !I::can('create_mobile_game')) {
            return Redirect::to(url("dashboard"));
        }
        $mobilegame = MobileGame::find($id);

        if (!$mobilegame) {
            App::abort(404);
        }
        
        $managedPartners = Auth::User()->managedPartners();
        $can_view_game = false;
        foreach ($managedPartners as $manP) {
            foreach ($mobilegame->partners as $part) {
                if ($manP->id == $part->id) {
                    $can_view_game = true;
                    break;
                }
            }
        }
//        if($can_view_game== false && $mobilegame->draft == false){
        if ($can_view_game == false) {
            return Redirect::to(url("dashboard"));
        }
        $input = Input::all();
        foreach ($input as $k => $i) {
            $input[$k] = self::sanitizeText($i);
        }
        $validator = Validator::make($input, $this->getValidations());
        
        if ($validator->fails()) {
            return Redirect::to("dashboard/mobilegame/edit/$id")->withErrors($validator)->withInput();
        }

        $mobilegame->name = self::sanitizeText(Input::get('name'));
        if (Input::has('status')) {
            $mobilegame->enabled = self::sanitizeText(Input::get('status'));
        } else {
            $mobilegame->enabled = '0';
        }

        if (Input::has('start_date')) {
            $mobilegame->start_date = date("Y-m-d H:i:s", strtotime(self::sanitizeText(Input::get('start_date'))));
        } else {
            $mobilegame->start_date = date("Y-m-d H:i:s", strtotime("-1 month", time()));
        }

        if (Input::has('end_date')) {
            $mobilegame->end_date = date("Y-m-d H:i:s", strtotime(self::sanitizeText(Input::get('end_date'))));
        } else {
            $mobilegame->end_date = date("Y-m-d H:i:s", strtotime("+1 day", time()));
        }

        if (Input::hasFile('image')) {
            $file = Input::file('image');
            $path = public_path('uploads');
            $ext = strtolower($file->getClientOriginalExtension());
            $size = $file->getSize();
            $type = $file->getMimeType();
            $name = md5(time()) . ".{$ext}";

            if (in_array($ext, Meta::allowedImageExts())) {
                Input::file('image')->move($path, $name);

                $f = new Media();
                $f->name = $name;
                $f->size = $size;
                $f->type = $type;
                $f->ext = $ext;
                $f->save();

                $mobilegame->media_id = $f->id;
            }
        }

        //$mobilegame->media_id       = self::sanitizeText(Input::get('media_id');
        $mobilegame->timer = self::sanitizeText(Input::get('timer'));
        $mobilegame->percentage = self::sanitizeText(Input::get('percentage'));
        $mobilegame->max_points_to_grant = self::sanitizeText(Input::get('max_points_to_grant'));
        $mobilegame->max_points_per_user = self::sanitizeText(Input::get('max_points_per_user'));
        $mobilegame->difficulty = self::sanitizeText(Input::get('difficulty'));

        $mobilegame->save();

        return Redirect::to(Input::get('redirect_to', 'dashboard/mobilegame'));
    }

    /**
     * Create a small LI list with product countries
     *
     * @param $enc_mobilegame_id
     * @return Response
     */
    private function listCountries($enc_mobilegame_id) {
        $mobilegame_id = base64_decode($enc_mobilegame_id);
        $mobilegame = MobileGame::find($mobilegame_id);

        return View::make('mobilegame.list.countryList', array(
                    'mobilegame' => $mobilegame,
        ));
    }

    /**
     * link a country to a product
     *
     * @param $enc_id
     * @return Response
     */
    public function postLink_country($enc_id) {
        $id = base64_decode($enc_id);
        $country_id = self::sanitizeText(Input::get('country_id'));
        $mobilegame = MobileGame::find($id);
        $country = Country::find($country_id);

        if (!$mobilegame->countries->contains($country)) {
            $mobilegame->countries()->attach($country_id);
        }
        return $this->listCountries($enc_id);
    }

    /**
     * unlink a country from a product
     *
     * @param $enc_mobilegame_id
     * @param $enc_country_id
     * @return Response
     */
    public function getUnlink_country($enc_mobilegame_id, $enc_country_id) {
        $mobilegame_id = base64_decode($enc_mobilegame_id);
        $country_id = base64_decode($enc_country_id);
        $mobilegame = MobileGame::find($mobilegame_id);

        $mobilegame->countries()->detach($country_id);
        return $this->listCountries($enc_mobilegame_id);
    }

    /**
     * Create a small LI list with mobilegame partners
     *
     * @param string $enc_mobilegame_id
     * @return Response
     */
    private function listPartners($enc_mobilegame_id)
    {
        $mobilegame_id = base64_decode($enc_mobilegame_id);
        $mobilegame = MobileGame::find($mobilegame_id);
        return View::make('mobilegame.list.partnerList', array(
            'mobilegame' => $mobilegame,
            'partners' => ManagedObjectsHelper::managedPartners()->sortBy('name')->pluck('name', 'id'),
        ));
    }

    /**
     * link a partner to a mobilegame
     *
     * @param $enc_id
     * @return Response
     */
    public function postLink_partner($enc_id) {
        $id = base64_decode($enc_id);
        $partner_id = self::sanitizeText(Input::get('partner_id'));
        $mobilegame = MobileGame::find($id);
        $partner = Partner::find($partner_id);

        if (!$mobilegame->partners->contains($partner)) {
            $mobilegame->partners()->attach($partner);
        }
        return $this->listPartners($enc_id);
    }

    /**
     * unlink a partner froma mobilegame
     *
     * @param $enc_mobilegame_id
     * @param $enc_partner_id
     * @return Response
     */
    public function getUnlink_partner($enc_mobilegame_id, $enc_partner_id) {
        $mobilegame_id = base64_decode($enc_mobilegame_id);
        $partner_id = base64_decode($enc_partner_id);
        $mobilegame = MobileGame::find($mobilegame_id);

        $mobilegame->partners()->detach($partner_id);
        return $this->listPartners($enc_mobilegame_id);
    }

}

// EOC
