<?php

namespace App\Http\Controllers;

use App\Media;
use Input;
use Illuminate\Support\Facades\Response;

/**
 * Media Controller
 * 
 * @category   Controllers
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class MediaController extends BaseController {

    public static function getImageFromId($id = null) {
        $folder_path = "/uploads/";
        $media_path = null;
        $media = Media::where('id', $id)->first();
        if ($media) {
            $media_path = $folder_path . $media->name;
        }
        return $media_path;
    }

    /**
     * Render an image type resource with optional sizing
     *
     * GET '/media/image/{id}?s=44x44'
     *
     * @param int $id
     * @return Response
     */
    public function renderImage($id = null) {
        $media = null;

        if (empty($id)) {
            $media = new Media();
            $media->name = "__NONE__";
        } else {
            $media = Media::where('id', $id)->first();
        }


        $mediaPath = "./uploads/{$media->name}";

        if (!file_exists($mediaPath)) {
            $image = "https://placehold.it/" . self::sanitizeText(Input::get('s'));

            return Response::make($image, 200, array(
                        'Content-Type' => 'image/jpeg'
            ));
        }

        if (!in_array($media->ext, array('png', 'jpg', 'jpeg', 'gif'))) {
            App::abort(401, "Invalid image type");
        }

        // Check for size params
        if(Input::has('s')) {
            $image_sizes = explode('x', self::sanitizeText(Input::get('s')));
            $thumbnailPath = $media->getThumbnailPath($image_sizes[0], $image_sizes[1] ?? $image_sizes[0]);
            $mediaPath = file_exists($thumbnailPath) ? $thumbnailPath : $mediaPath;
                }


        return response()->file($mediaPath);

//            return Response::make($mediaPath, 200, array(
//                        'Content-Type' => 'image/jpeg',
//                        'Pragma' => 'public',
//                        'Cache-Control' => 'max-age=86400',
//                        'Expires' => gmdate('D, d M Y H:i:s \G\M\T', time() + 86400)
//            ));
    }

    /**
     * Force the download of a media item
     *
     * GET '/media/download/{id}'
     *
     * @param int $id
     * @return Response
     */
    public function downloadFile($id = null) {
        if (empty($id)) {
            exit;
        }
        $media = Media::where('id', $id)->first();
        if (!$media) {
            \App::abort(404);
        }
        $file_path = "./uploads/{$media->name}";
        if (!file_exists($file_path)) {
            \App::abort(404);
        }
        return Response::download($file_path);
    }

    /**
     * delete a media file
     *
     * GET '/media/delete/{id}'
     *
     * @param int $id
     * @return Response
     */
    public function deleteFile($id = null) {
        if (empty($id)) {
            exit;
        }
        $media = Media::where('id', $id)->first();
        if (!$media) {
            return Response::json(array(
                        'failed' => true,
                        'errors' => 'media not found'
            ));
        }
        $file_path = "./uploads/{$media->name}";
        if (file_exists($file_path)) {
            unlink($file_path);
        }
        $media->delete();
    }

}

// EOC
