<?php
namespace App\Http\Controllers;
/**
 * PHP INFO Controller
 *
 * @category   Controllers
 * @package    BLU
 * @author     Ralph Nader <ralph@bluloyalty.com>
 * @copyright  2016 BLU
 * @link       http://blupoints.com
 */
class PhpinfoController extends Controller
{
    /**
     * Ensure that we're running as admin
     *
     * @return void
     */
    public function __construct()
    {
        if(!I::am('BLU Admin')) {
            App::abort(404);
        }
    }

    /**
     * Get the file to import
     *
     * @return void
     */
    public function getIndex()
    {
        phpinfo();
    }
    

} // EOC
