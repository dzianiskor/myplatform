<?php
namespace App\Http\Controllers;
/**
 * API Controller
 *
 * @category   Controllers
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class APIv2Controller extends BaseController
{
	private $networks;
	private $partner;
	private $user;

    /**
     * Initiate the controller and ensure the partner request is authenticated
     *
     * @return void
     */
    public function __construct()
    {
		$api_key = Input::get('api_key', null);

		if(empty($api_key)) {
			App::abort(401);
		}

		$this->user = Admin::where('api_key', "$api_key")->first();

		if(!$this->user) {
			App::abort(404);
		} else {
//			Auth::loginUsingId($this->user->id);

			// Assign scopes

			$this->partner = $this->user->partners()->first();
			$this->network = $this->partner->firstNetwork();

			if(!$this->partner) {
				App::abort(403, "No partner associated to request $api_key");
			}
		}
    }

    /**
     * Generic response handler for API requests
     *
     * @param int $status
     * @param string $message
     * @param object $objects
     * @param string $class_name
     * @return Response
     */
    private static function respondWith($status, $message, $object = null, $class_name = '')
    {
        $response = array();

        if($message) {
            $response['message'] = $message;
        }

		if(!empty($object)) {
                    if($object && is_object($object)) {

                            if(get_class($object) != 'stdClass') {
                                $class_name = get_class($object);
                            }

                            if(method_exists($object, 'toArray')) {
                                $response[$class_name] = $object->toArray();
                            } else {
                                $response[$class_name] = $object;
                            }
                    } else {
                        $response[$class_name] = $object;
                    }
		} else {
			$response[$class_name] = array();
		}

		$response['status'] = $status;

        if($status == 500) {
            Log::warning($message);
        }

        return Response::make($response, $status);
    }
    public static function decryptSession($session_enc,$partner_id){
        $xtea = new XTEA($partner_id);
        $session = $xtea->decrypt($session_enc);
        $session_dec = json_decode($session);
        return $session_dec;
    }

    public function postInitializeSession(){
        $arr_session['ref'] = Input::get('reference');
        $partner_prefix = $this->partner->prefix;
        if(substr(trim($arr_session['ref']), 0, strlen($partner_prefix)) !== $partner_prefix){
            return self::respondWith(
                400, "NOT OK", array('message'=>"Error 400001"), 'Error'
            );
        }
        $ref_user = Reference::where('number',$arr_session['ref'])->first();
        if(!$ref_user){
            return self::respondWith(
                400, "NOT OK", array('message'=>"Error 400002"), 'Error'
            );
        }
        $arr_session['user_id'] = $ref_user->user_id;
        $arr_session['partner_id'] = $this->partner->id;
        $arr_session['store_id'] = Input::get('store_id');
        $arr_session['pos_id'] = Input::get('pos_id');
        $arr_session['api_key'] = Input::get('api_key');
        $session = json_encode($arr_session);
        $xtea = new XTEA($this->partner->id);
        $session_enc = $xtea->encrypt($session);
        return self::respondWith(
                200, "OK", array('val'=>$session_enc), 'Session'
            );
    }
    
    /**
     * @description: get the User balance
     *
     *
     * @method:  POST 
     * @link:    /apiv2/user-balance
     * 
     * @param post $session
     *
     * @return Response
     */
    
    public function postUserBalance(){
            $session = Input::get('session');
            $partner= $this->partner;
            $obj_session = self::decryptSession($session, $partner->id);
            $user_id = $obj_session->user_id;
            $user = User::find($user_id);
            $lang			= Input::get('lang','en');
            
            $partners = $partner->managedObjects();
            $partners_children = array();
            foreach($partners as $part){
                $partners_children[] = $part->id;
            }
            if (!$user) {
                return self::respondWith(
                    500, "The PIN provided is not valid"
                );
            }
            $arr_response = array();
            $balance = $user->balance($this->partner->firstNetwork()->id);

            $sum_expired_points = Transaction::select(DB::raw('sum(points_rewarded) pts_rewarded'))->where('expired',1)->where('user_id',$user_id)->whereIn('partner_id',$partners_children)->first();
            $sum_redeemed_points = Transaction::select(DB::raw('sum(points_redeemed) pts_redeemed'))->where('trx_redeem',1)->where('user_id',$user_id)->whereIn('partner_id',$partners_children)->first();
            $user_tier = $user->tiercriteria()->get();
            $tierC_expiry = 0;
            if(!is_numeric($sum_expired_points->pts_rewarded)){
                $sum_expired_points->pts_rewarded = 0;
            }
            if(!is_numeric($sum_redeemed_points->pts_redeemed)){
                $sum_redeemed_points->pts_redeemed = 0;
            }
            if($lang == "ar"){
                $monthName = '١ يناير';
            }else{
                $monthName = 'January 1st';
            }
            $composed_date = '';
            $max_date = date('Y-m-d', strtotime('-1 year'));
            if(isset($user_tier)){
//                    var_dump($user_tier);
//                    exit();
                foreach($user_tier as $tierCriterion){


                        if(!is_null($tierCriterion->expiry_in_months)){
                            $parent_tier = Tier::find($tierCriterion->tier_id);
                            $monthNum  = $parent_tier->start_month;
                            $dateObj   = DateTime::createFromFormat('!m', $monthNum);
                            $current_mday = intval(date('md'));
                            $tier_mday = intval($monthNum."01");
                            if($current_mday > $tier_mday){
                                $year = date('Y', strtotime('+1 year'));
                            }
                            else{
                                $year = date('Y');
                            }
                            $monthDisplay = $monthNum;
                            if (strlen($monthDisplay)< 2){
                                $monthDisplay = "0".$monthDisplay;
                            }
                            $composed_date = $year . "-" . $monthDisplay . "-01";
                            $monthName = $dateObj->format('F') . " 1st"; // March


                            $tierC_expiry = $tierCriterion->expiry_in_months;
                            $max_timestamp = strtotime ( "-".$tierC_expiry." month" , strtotime ( $composed_date ) ) ;
                            $max_date = date("Y-m-d",$max_timestamp);
                            $arr_response['expiry_period'] = $tierC_expiry;
                        }
                        else{
                            $arr_response['expiry_period'] = 0;
                            $arr_response['next_expiry'] = 'January 1st';
                        }
                }

            }

            $sum_expiring_points = Transaction::select(DB::raw('sum(points_rewarded) pts_rewarded'))->where('expired',0)->where('user_id',$user_id)->whereIn('partner_id',$partners_children)->where('created_at','<',$max_date)->first();
            if(is_null($sum_expiring_points->pts_rewarded)){
                $sum_expiring_points->pts_rewarded = 0;
            }
            $arr_response['balance'] = $balance;
            $arr_response['sum_expired_points'] = $sum_expired_points->pts_rewarded;
            $arr_response['sum_redeemed_points'] = $sum_redeemed_points->pts_redeemed;
            $arr_response['expiry_period'] = $tierC_expiry;
            $arr_response['next_expiry'] = $monthName;
            $arr_response['sum_expiring_points'] = $sum_expiring_points->pts_rewarded;
            return self::respondWith(
                200, null, array('balance' => $arr_response), 'user'
            );
            
	}
        
    /**
     * @description: get the User redemptions
     *
     *
     * @method:  POST 
     * @link:    /apiv2/member-redemptions
     * 
     * @param post session
     * @param post lang
     * @param post day_range
     *
     * @return Response
     */
        
        
    public function postMemberRedemptions(){
        $session = Input::get('session');
        $partner= $this->partner;
        $obj_session = self::decryptSession($session, $partner->id);
        $userId = $obj_session->user_id;
        

        $networkId	= $partner->firstNetwork()->id;
        $lang		= Input::get('lang','en');
        
        $day_range1 = Input::get('day_range', 365);
        $day_range = ($day_range1 =='undefined')? 365 : $day_range1;
        $trans_date = date("Y-m-d H:i:s", strtotime("-$day_range days", time()));
            
        $resTrxRedeem = Transaction::select(DB::raw('id'),DB::raw('trans_class'))

            ->where('created_at', '>=', $trans_date )
            ->where('trx_redeem',1)
            ->where('user_id',$userId)
            ->where('network_id', $networkId)
            ->where('reversed', 0.00)
            ->where('quantity', '>', -1)
            ->where('total_amount', '>=', 0)
            ->where('trans_class', '!=', 'Reversal')
            ->orderBy('created_at', 'DESC');

        $rtrx_redeem = array();
        foreach($resTrxRedeem->get() as $rtrx){
            if($rtrx->trans_class == 'Items'){
                $rtrx_redeem[] = $rtrx->id;
            }else{
                $nonItemRtrx_redeem[] = $rtrx->id;
            }
        }
        $type = 'all';
        if(Input::has('type') ){
            $type = Input::get('type');
        }
        $redemptions	= array();
        $currencyCode = 'USD';
        
        $languageData	= Language::where('short_code', $lang)->first();
        if(!empty($nonItemRtrx_redeem) && $type!='item'){
            foreach($nonItemRtrx_redeem as $nonItemTrxId){
                $nonItemTrx = Transaction::find($nonItemTrxId);
                $createdAt       = $nonItemTrx->created_at;
                $pointsRedeemed  = $nonItemTrx->points_redeemed;
                $transclass = $nonItemTrx->trans_class;                
                $currency = Currency::where('id',$nonItemTrx->currency_id)->first();
                if($lang != 'en') {
                    $curTrans = CurrencyTranslation::where('currency_id',$currency->id)->where('lang_id', $languageData->id)->first();
                }
                if(isset($curTrans)){
                   $currencyCode =  $curTrans->name;
                }else{
                    $currencyCode = $currency->short_code;
                }
                $realDate = substr($createdAt, 0, 10);
                $rate    = $currency->rateByDate($realDate);
                if(( $type=='cashback' || $type=='all' || $type=='undefined')  && (strpos($nonItemTrx->notes,'ashback') || strpos($nonItemTrx->ref_number,'ashback'))){
                     $redemptions["$createdAt"]["$nonItemTrx->id"]['productName']	= 'lbl-cashback';
                        $redemptions["$createdAt"]["$nonItemTrx->id"]['quantity']      = 1;
                        $redemptions["$createdAt"]["$nonItemTrx->id"]['price']		= $pointsRedeemed;
                        $redemptions["$createdAt"]["$nonItemTrx->id"]['cash_payment']		= $nonItemTrx->cash_payment * $rate;
                        $redemptions["$createdAt"]["$nonItemTrx->id"]['currency_code']		= $currencyCode;
                        $redemptions["$createdAt"]["$nonItemTrx->id"]['image']		= 'unavilable.png';
                        $redemptions["$createdAt"]["$nonItemTrx->id"]['date']		= $nonItemTrx->created_at;
                }elseif(( $type=='miles' || $type=='all' || $type=='undefined')  && strpos($nonItemTrx->notes,'iles')){
                     $redemptions["$createdAt"]["$nonItemTrx->id"]['productName']	= 'lnk-milesconversion';
                        $redemptions["$createdAt"]["$nonItemTrx->id"]['quantity']      = 1;
                        $redemptions["$createdAt"]["$nonItemTrx->id"]['price']		= $pointsRedeemed;
                        $redemptions["$createdAt"]["$nonItemTrx->id"]['cash_payment']		= $nonItemTrx->cash_payment * $rate;
                        $redemptions["$createdAt"]["$nonItemTrx->id"]['currency_code']		= $currencyCode;
                        $redemptions["$createdAt"]["$nonItemTrx->id"]['image']		= 'unavilable.png';
                        $redemptions["$createdAt"]["$nonItemTrx->id"]['date']		= $nonItemTrx->created_at;
                }elseif(($type=='transfer'|| $type=='all' || $type=='undefined')  && strpos($nonItemTrx->ref_number, 'ransfer') ){
                    $redemptions["$createdAt"]["$nonItemTrx->id"]['productName']	= $nonItemTrx->ref_number;
                    $redemptions["$createdAt"]["$nonItemTrx->id"]['quantity']      = 1;
                    $redemptions["$createdAt"]["$nonItemTrx->id"]['price']		= $pointsRedeemed;
                    $redemptions["$createdAt"]["$nonItemTrx->id"]['cash_payment']		= $nonItemTrx->cash_payment * $rate;
                    $redemptions["$createdAt"]["$nonItemTrx->id"]['currency_code']		= $currencyCode;
                    $redemptions["$createdAt"]["$nonItemTrx->id"]['image']		= 'unavilable.png';
                    $redemptions["$createdAt"]["$nonItemTrx->id"]['date']		= $nonItemTrx->created_at;                
                }elseif( ($type=='instore'|| $type=='all' || $type=='undefined')  && strtolower($transclass)=='amount' ) {
                        $redemptions["$createdAt"]["$nonItemTrx->id"]['productName']	= 'lbl-instore-redemption';
                        $redemptions["$createdAt"]["$nonItemTrx->id"]['quantity']      = 1;
                        $redemptions["$createdAt"]["$nonItemTrx->id"]['price']		= $pointsRedeemed;
                        $redemptions["$createdAt"]["$nonItemTrx->id"]['cash_payment']		= $nonItemTrx->cash_payment * $rate;
                        $redemptions["$createdAt"]["$nonItemTrx->id"]['currency_code']		= $currencyCode;
                        $redemptions["$createdAt"]["$nonItemTrx->id"]['image']		= 'unavilable.png';
                        $redemptions["$createdAt"]["$nonItemTrx->id"]['date']		= $nonItemTrx->created_at;
                }elseif(($type=='flight'|| $type=='all' || $type=='undefined')  && strtolower($transclass)=='flight'){
                        $redemptions["$createdAt"]["$nonItemTrx->id"]['productName']	= 'lbl-flight-redemption';
                        $redemptions["$createdAt"]["$nonItemTrx->id"]['quantity']      = 1;
                        $redemptions["$createdAt"]["$nonItemTrx->id"]['price']		= $pointsRedeemed;
                        $redemptions["$createdAt"]["$nonItemTrx->id"]['cash_payment']		= $nonItemTrx->cash_payment * $rate;
                        $redemptions["$createdAt"]["$nonItemTrx->id"]['currency_code']		= $currencyCode;
                        $redemptions["$createdAt"]["$nonItemTrx->id"]['image']		= 'unavilable.png';
                        $redemptions["$createdAt"]["$nonItemTrx->id"]['date']		= $nonItemTrx->created_at;
                }elseif(($type=='hotel' || $type=='all' || $type=='undefined')  && strtolower($transclass)=='hotel'){
                        $redemptions["$createdAt"]["$nonItemTrx->id"]['productName']	= 'lbl-hotel-redemption';
                        $redemptions["$createdAt"]["$nonItemTrx->id"]['quantity']      = 1;
                        $redemptions["$createdAt"]["$nonItemTrx->id"]['price']		= $pointsRedeemed;
                        $redemptions["$createdAt"]["$nonItemTrx->id"]['cash_payment']		= $nonItemTrx->cash_payment * $rate;
                        $redemptions["$createdAt"]["$nonItemTrx->id"]['currency_code']		= $currencyCode;
                        $redemptions["$createdAt"]["$nonItemTrx->id"]['image']		= 'unavilable.png';
                        $redemptions["$createdAt"]["$nonItemTrx->id"]['date']		= $nonItemTrx->created_at;
                }elseif(($type=='car' || $type=='all' || $type=='undefined')  && strtolower($transclass)=='car'){
                        $redemptions["$createdAt"]["$nonItemTrx->id"]['productName']	= 'lbl-car-redemption';
                        $redemptions["$createdAt"]["$nonItemTrx->id"]['quantity']      = 1;
                        $redemptions["$createdAt"]["$nonItemTrx->id"]['price']		= $pointsRedeemed;
                        $redemptions["$createdAt"]["$nonItemTrx->id"]['cash_payment']		= $nonItemTrx->cash_payment * $rate;
                        $redemptions["$createdAt"]["$nonItemTrx->id"]['currency_code']		= $currencyCode;
                        $redemptions["$createdAt"]["$nonItemTrx->id"]['image']		= 'unavilable.png';
                        $redemptions["$createdAt"]["$nonItemTrx->id"]['date']		= $nonItemTrx->created_at;
                }elseif($type=='all' || $type=='undefined'){
                        $redemptions["$createdAt"]["$nonItemTrx->id"]['productName']	= $nonItemTrx->ref_number;
                        $redemptions["$createdAt"]["$nonItemTrx->id"]['quantity']      = 1;
                        $redemptions["$createdAt"]["$nonItemTrx->id"]['price']		= $pointsRedeemed;
                        $redemptions["$createdAt"]["$nonItemTrx->id"]['cash_payment']		= $nonItemTrx->cash_payment * $rate;
                        $redemptions["$createdAt"]["$nonItemTrx->id"]['currency_code']		= $currencyCode;
                        $redemptions["$createdAt"]["$nonItemTrx->id"]['image']		= 'unavilable.png';
                        $redemptions["$createdAt"]["$nonItemTrx->id"]['date']		= $nonItemTrx->created_at;
                }
            }
        }
        if($type=='item' || $type=='all' || $type=='undefined'){
            if(empty($rtrx_redeem)){
                $rtrx_redeem[] = '-1';
            }
            $rtrx_items	= TransactionItem::select(DB::raw('id'),
                                DB::raw('product_id'),
                                DB::raw('transaction_id'),
                                DB::raw('quantity'),
                                DB::raw('delivery_cost'),
                                DB::raw('original_total_amount'),
                                DB::raw('currency_id'),
                                DB::raw('created_at'),
                                DB::raw('paid_in_points'),
                                DB::raw('paid_in_cash'),
                                DB::raw('cash_currency_id'))
                                ->whereIn('transaction_id', $rtrx_redeem)
                                ->orderBy('created_at', 'DESC');

            foreach($rtrx_items->get() as $key => $trxItem){
                $sum_item_price_in_points = 0;
                $product	= Product::find($trxItem->product_id);
                if(!$product || $product->price_in_points == 0){
                    $trx = Transaction::find($trxItem->transaction_id);
                    $product	= ProductsHelper::productRedemptionDetails($trxItem->product_id, $trx->country_id, $trx->partner_id);
                }
                
                
                if(!$product){
                        continue;
                }

                $currencyCode = 'USD';
                $currency = Currency::where('id',$trxItem->cash_currency_id)->first();
                $curTrans = CurrencyTranslation::where('currency_id',$currency->id)->where('lang_id', $languageData->id)->first();
                if(isset($curTrans)){
                   $currencyCode =  $curTrans->name;
                }else{
                     $currencyCode = $currency->short_code;
                }
                 $temp_prodTranslate = Prodtranslation::where('product_id',$trxItem->product_id)->where('lang_id', $languageData->id)->first();
                if(isset($temp_prodTranslate)){
                    $redemptions["$trxItem->created_at"]["$trxItem->id"]['productName']     =  $temp_prodTranslate->name;
                }else{
                    $redemptions["$trxItem->created_at"]["$trxItem->id"]['productName']     = $product->name;
                }
                
                $redemptions["$trxItem->created_at"]["$trxItem->id"]['quantity']        = $trxItem->quantity;
                $redemptions["$trxItem->created_at"]["$trxItem->id"]['price']           =  $trxItem->paid_in_points + $trxItem->delivery_cost;
                $redemptions["$trxItem->created_at"]["$trxItem->id"]['cash_payment']    = $trxItem->paid_in_cash;
                $redemptions["$trxItem->created_at"]["$trxItem->id"]['currency_code']    = $currencyCode;
                $redemptions["$trxItem->created_at"]["$trxItem->id"]['image']           = $product->cover_image;
                $redemptions["$trxItem->created_at"]["$trxItem->id"]['date']            = $trxItem->created_at;
            }
        }
        
        if(Input::has('sort')) {
                   $sorting = Input::get('sort');
                        switch($sorting) {
                                    case "newest":
                                            krsort($redemptions);
                                            break;
                                    case "oldest":
                                            ksort($redemptions);
                                            break;
                                    default:
                                            krsort($redemptions);
                                        break;
                            }                    
            }else{
                 krsort($redemptions);
            }
        return self::respondWith(
                200, 'OK', $redemptions, 'redemptions'
        );
    }
    
    
    
    /**
     * @description: get the Partner Products New
     *
     *
     * @method:  POST 
     * @link:    /apiv2/partner-products-new
     * 
     * @param get page
     * 
     * @param post country_id
     * @param post network_id
     * @param post lang
     * @param post cat
     * @param post q
     * @param post sort
     * @param post minp
     * @param post maxp
     * @param post combined_prices
     * @param post section
     * 
     * @return Response
     */
    
    
    public function postPartnerProductsNew(){
        $countryId = Input::get('country_id');
        $partner = $this->partner;
        $channelId = $partner->id;
        
        $partnerNetworkId = Input::get('network_id', $partner->firstNetwork()->id);
        $lang           = Input::get('lang','en');
        $languageData   = Language::where('short_code', $lang)->first();
        if(!$partner) {
            return self::respondWith(
                500, "Could not locate user with id {$partner->id} in database"
            );
        }
        $session = Input::get('session');

        $obj_session = self::decryptSession($session, $partner->id);
        $userId = $obj_session->user_id;
        if(isset($userId)){
            $member                 = User::find($userId);
            $userSegments           = array();
            foreach($member->segments as $s){
                $userSegments[] = $s->id;
            }
        }
        if(isset($userSegments) && !empty($userSegments)){
            $productUserSegments = array();

            $productsNoSegment = Product::select('id')->has('segments', '=', 0)->get();
            foreach($productsNoSegment as $productNoSegment){
                 $productUserSegments[] = $productNoSegment->id;
             }

            $productsUserSegments = Product::select('id')->whereHas('segments', function($q) use($userSegments){
                      $q->whereIn('segment_id', $userSegments);
              })->get();
            foreach($productsUserSegments as $productsUserSegment){
                 $productUserSegments[] = $productsUserSegment->id;
             }
        }
        
        //Get Products of Selected Country
        $productsByCountry = DB::table('product_redemption_countries')->where('country_id', $countryId)->pluck('product_id');
        if(empty($productsByCountry)){
            return self::respondWith(
                500, "There is no products in this Country"
            );
        }
        $productsByChannel = DB::table('product_redemption_display_channel')->where('partner_id', $channelId)->whereIn('product_id', $productsByCountry)->pluck('product_id');
        if(empty($productsByChannel)){
            return self::respondWith(
                500, "There is no products in this Channel"
            );
        }
        if(isset($userSegments) && !empty($userSegments)){
            foreach ($productsByChannel as $key => $id){
                $productSegments= DB::table('product_redemption_segments')->where('product_id', $id)->pluck('segment_id');
                if(!empty($productSegments)){
                    foreach($productSegments as $productSegment){
                        if(!in_array($productSegment, $userSegments)){
                            unset($productsByChannel[$key]);
                        }
                    }
                }
            }
        }
        $results = Product::leftJoin('media', 'product.cover_image', '=', 'media.id');
        if($lang != 'en'){
            $results->leftJoin('product_translation', 'product_translation.product_id', '=','product.id');
            $results->select('product.id', 'product.ucid', 'product.id', 'product.name', 'product.brand_id', 'product.model', 'product.sub_model', 'product.description', 'product.category_id', 'product.weight', 'product.volume', 'product.draft', 'product.created_at', 'product.updated_at', 'product.deleted_at', 'product.is_voucher',
            'product_partner_redemption.display_online', 'product_partner_redemption.hot_deal', 'product_partner_redemption.qty', 'product_partner_redemption.price_in_points', 'product_partner_redemption.price_in_usd', 'product_partner_redemption.retail_price_in_usd', 'product_partner_redemption.start_date', 'product_partner_redemption.end_date', 'product_partner_redemption.supplier_id', 'product_partner_redemption.currency_id', 'product_partner_redemption.original_price', 'product_partner_redemption.original_retail_price', 'product_partner_redemption.show_offline', 
            'product_redemption_countries.status', 'product_redemption_countries.delivery_charges', 'product_redemption_countries.custom_tarrif_taxes as original_sales_tax', 'product_redemption_countries.currency_id as delivery_currency_id', 
            'product.cover_image', 'product_translation.name as translated_name', 'product_translation.lang_id as lang_id', 'product_redemption_countries.country_id');
        }else{
            $results->select('product.id', 'product.ucid', 'product.id', 'product.name', 'product.brand_id', 'product.model', 'product.sub_model', 'product.description', 'product.category_id', 'product.weight', 'product.volume', 'product.draft', 'product.created_at', 'product.updated_at', 'product.deleted_at', 'product.is_voucher',
            'product_partner_redemption.display_online', 'product_partner_redemption.hot_deal', 'product_partner_redemption.qty', 'product_partner_redemption.price_in_points', 'product_partner_redemption.price_in_usd', 'product_partner_redemption.retail_price_in_usd', 'product_partner_redemption.start_date', 'product_partner_redemption.end_date', 'product_partner_redemption.supplier_id', 'product_partner_redemption.currency_id', 'product_partner_redemption.original_price', 'product_partner_redemption.original_retail_price', 'product_partner_redemption.show_offline', 
            'product_redemption_countries.status', 'product_redemption_countries.delivery_charges', 'product_redemption_countries.custom_tarrif_taxes as original_sales_tax', 'product_redemption_countries.currency_id as delivery_currency_id', 
            'product.cover_image');
        }
        $results->Join('product_partner_redemption', 'product_partner_redemption.product_id', '=','product.id');
        $results->Join('product_redemption_countries', function($join)
                        {
                          $join->on('product_redemption_countries.product_id', '=', 'product.id');
                          $join->on('product_redemption_countries.product_redemption_id', '=', 'product_partner_redemption.id');
                          $join->on('product_redemption_countries.partner_id', '=', 'product_partner_redemption.partner_id');

                        });
        $results->Join('product_redemption_display_channel', function($join)
                        {
                          $join->on('product_redemption_display_channel.product_id', '=', 'product.id');
                          $join->on('product_redemption_display_channel.product_redemption_id', '=', 'product_partner_redemption.id');

                        });
        
        $results->whereIn('product.id', $productsByChannel);
        $results->where('product.draft', false);
        $results->where('product_partner_redemption.display_online', true);
        $results->where('product_redemption_countries.status', 'active');
        $results->where('product_partner_redemption.qty',">", 0);
        $results->where(function($query) {
                    $query->where(function($u) {
                           $u->where('product_partner_redemption.start_date', '<=', date('Y-m-d'))
                             ->where('product_partner_redemption.end_date', '>=', date('Y-m-d'));
                        })
                        ->orWhere('product_partner_redemption.end_date', '=', '1970-01-01')
                        ->orWhereNull('product_partner_redemption.end_date');
                 });        
        $results->where('product_redemption_display_channel.partner_id', $partner->id);
        $results->where('product_redemption_countries.country_id', $countryId);
        
        if(Input::get('section') == 'deals') {
            $results->where('product_partner_redemption.hot_deal', true);
        } else {
            $results->where('product_partner_redemption.hot_deal', false);
        }
        $results->with('Category');
        $results->with('Brand');

        // Add parameters

        if(Input::has('cat')) {
                        $url_cats =  explode(',', urldecode(Input::get('cat')));
                        $subcat = Category::select(DB::raw('id'))->whereIn('parent_category_id',$url_cats)->get();
                        $subcat2 = array();
                        foreach($subcat as $sub){
                            $subcat2[] = $sub->id;
                        }
                        $subcat3 = array_merge($subcat2,$url_cats);
            $results->whereIn('product.category_id', $subcat3);
        }

        if(empty($userId)){
            $results->where('product_partner_redemption.show_offline', true);
        }
        if(Input::has('q')) {
            if($lang != 'en'){
             $results->where(function ($query) {
                     $query->where('product.name', 'like', '%'.Input::get('q').'%')
                           ->orWhere('product_translation.name', 'like', '%'.Input::get('q').'%');
                 });
            }else{
                $results->where(function ($query) {
                     $query->where('product.name', 'like', '%'.Input::get('q').'%');
                 });
            }
        }

                
                if(Input::has('sort')) {
            $sorting = explode(',', urldecode(Input::get('sort')));

            foreach($sorting as $s) {
                switch($s) {
                    case "newest":
                        $results->orderBy('product.id', 'DESC');
                        break;
                    case "oldest":
                        $results->orderBy('product.id', 'ASC');
                        break;
                                        case "alphabetical_asc":
                                                if(isset($lang) && !empty($lang)){
                                                     if(strtolower($lang) != 'en'){
                                                     $results->orderBy('translated_name', 'ASC');                                                 
                                                     }else{
                                                       $results->orderBy('product.name', 'ASC');  
                                                     }
                                                }else{
                                                      $results->orderBy('product.name', 'ASC');  
                                                }
                        break;
                    case "alphabetical_desc":
                                                if(isset($lang) && !empty($lang)){
                                                     if(strtolower($lang) != 'en'){
                                                     $results->orderBy('translated_name', 'DESC');                                                 
                                                     }else{
                                                       $results->orderBy('product.name', 'DESC');  
                                                     }
                                                }else{
                                                      $results->orderBy('product.name', 'DESC');  
                                                }
                        break;
                    case "points_asc":
                        $results->orderBy('product_partner_redemption.price_in_points', 'ASC');
                        break;
                    case "points_desc":
                        $results->orderBy('product_partner_redemption.price_in_points', 'DESC');
                        break;
                }
            }
        }
        $arrProductsDeals = array('2153','8328', '8374', '8392');
                $resultsItemCount = 0;
                if($userId){
                    $user_id = $userId;
                    $resultsItem = TransactionItem::whereIn('transaction_item.product_id', $arrProductsDeals);
                    $resultsItem->leftJoin('transaction', 'transaction_item.transaction_id', '=','transaction.id');
                    $resultsItem->where('transaction.user_id',$user_id);
                    $resultsItemCount = $resultsItem->count();
                }
        if($resultsItemCount > 0){
                    $results->whereNotIn('product.id',$arrProductsDeals);
                }
 
        $results    = $results->get();
        foreach($results as $key => $result){
            $originalPrice  = $result->original_price + ($result->original_sales_tax * $result->original_price /100);
            $originalcurrencyId = $result->currency_id;
            $originalDeliverycurrencyId = $result->delivery_currency_id;
            $priceInPoints  = $result->price_in_points + ($result->original_sales_tax * $result->price_in_points /100);

            $originalPriceInPoints  = PointsHelper::productPriceInPoints($priceInPoints, $originalPrice, $originalcurrencyId, $partnerNetworkId);
            $deliveryPriceInPoints      = PointsHelper::deliveryPriceInPoints($result->delivery_charges, $originalDeliverycurrencyId, $partnerNetworkId);
            $result->price_in_points = $originalPriceInPoints;
            $result->delivery_charges = $deliveryPriceInPoints;
            $result->combined_price = $result->price_in_points + $result->delivery_charges;
            //NEEDS FIXING
            if($partner->has_middleware==1){
                $pricewithdelivercharges = $originalPriceInPoints + $deliveryPriceInPoints;
                 if(Input::has('minp')) {
                     if( $pricewithdelivercharges < Input::get('minp')){
                         unset($results[$key]);
                     }
                 }
                 if(Input::has('maxp')) {
                     if( $pricewithdelivercharges > Input::get('maxp')){
                         unset($results[$key]);
                     }
                 }
            }else{
                 if(Input::has('minp')) {
                     if( $originalPriceInPoints < Input::get('minp')){
                         unset($results[$key]);
                     }
                 }
                 if(Input::has('maxp')) {
                     if( $originalPriceInPoints > Input::get('maxp')){
                         unset($results[$key]);
                     }
                 }
            }
            $langId = $languageData->id;

            if($lang != 'en' && $result->lang_id != $langId){
                if($result->lang_id != null){
                unset($results[$key]);
                }
            }

            if($lang == 'en' && (!empty($result->lang_id ) && $result->lang_id != $langId)){
                unset($results[$key]);
            }
        }
                
        //Sorting Of Combined prices
        if(Input::has('combined_prices') && Input::get('combined_prices') == 1) {
            if(Input::has('sort')) {
            $sorting = explode(',', urldecode(Input::get('sort')));
            foreach($sorting as $s) {
                        switch($s) {
                            case "points_asc":
                                $results->sortBy('combined_price');
                                break;
                            case "points_desc":
                                $results->sortByDesc('combined_price');
                                break;
                        }
                }
           }
        }
                                
        // Construct the response with automated pagination
        $buffer = $results->paginate(16);
        $temp_buffer = $buffer->toArray();
                
        $result_set = new \stdClass();
        $result_set->results      = null;
        $result_set->currentPage  = $buffer->getCurrentPage();
        $result_set->lastPage     = $buffer->getLastPage();
        $result_set->totalResults = $buffer->getTotal();
               
        return self::respondWith(
            200, null, $temp_buffer, 'results'
        );
    }
    
    
    /**
     * @description: get the Partner Card Types
     *
     *
     * @method:  POST 
     * @link:    /apiv2/partner-card-types
     * 
     *
     * @return Response
     */
    
    public function postPartnerCardTypes(){
        $partner= $this->partner;
                
        $partnerId = $partner->id;
        $partnerCardTypes = PartnerCardTypes::where('partner_id', $partnerId)->get();
        
        return self::respondWith(
                200, 'OK', $partnerCardTypes->toArray(), "CardTypes"
            );
    }
    
    
    /**
     * @description: get the Frequent Flyer Number
     *
     *
     * @method:  POST 
     * @link:    /apiv2/frequent-flyer-number
     * 
     * @param post $session
     * @param post airlines
     *
     * @return Response
     */
    
    
    public function postFrequentFlyerNumber() {
        $airline = self::sanitizeText(Input::get('airlines'));
        
        $programId = $airline;
        
        $session = Input::get('session');
        $partner= $this->partner;
        $obj_session = self::decryptSession($session, $partner->id);
        $userId = $obj_session->user_id;
        $affiliateProgramMemberships = AffiliateProgramsMemberships::where('user_id', $userId)->where('program_id', $programId)->first();
       
         return self::respondWith(
            200, null, $affiliateProgramMemberships->membership_number, 'number'
        );
    }
    
    /**
     * @description: transfer points to another user
     *
     *
     * @method:  POST 
     * @link:    /apiv2/transfer-points
     * 
     * @param post session
     * @param post trans_amount
     * @param post dest_mobile
     * @param post all_points
     * @param post lang
     *
     * @return Response
     */
    
    public function postTransferPoints()
	{

            
            $trans_amount = Input::get('trans_amount', 0);
            $dest_mobile  = Input::get('dest_mobile', 0);

            $lang = Input::get('lang','en');
            $credential = Input::get('credential');            
            $all_points = Input::get('all_points', 0);            
            
            $languageObj	= new Language();
            $languageData	= Language::where('short_code', $lang)->first();
            $languageId		= $languageData->id;
            $language		= $languageObj::find($languageId);

            $languageVars	= $language->keys()->get();

            $partner = $this->partner;
            $session = Input::get('session');

            $obj_session = self::decryptSession($session, $partner->id);
            $user_id = $obj_session->user_id;
            
            $user = User::find($user_id);
            $destination = User::where('normalized_mobile', '=', "+$dest_mobile")->first();

            if(!$user) {
                return self::respondWith(
                    500, "Could not locate user with id {$user_id} in database"
                );
            }
//			if($user->verified != true){
//
//				$errormsg = "The member is not verified";
//				foreach ( $languageVars as $languageVar ) {
//					if(trim( $languageVar->key['key']) === 'par-membernotverified'){
//						$errormsg = trim($languageVar->value);
//						break;
//					}
//				}
//						return self::respondWith(
//					500, $errormsg
//				);
//			}

        if($user->status != 'active' ||$user->verified != true ) {
            $errormsg = "You are not allowed to transfer points";
            foreach ( $languageVars as $languageVar ) {
                    if(trim( $languageVar->key['key']) === 'par-membernotallowedtotransfer'){
                            $errormsg = trim($languageVar->value);
                            break;
                    }
            }

            return self::respondWith(
                500, $errormsg
            );
        }

        if($trans_amount < 100) {
            $errormsg = "Please provide an amount larger than 100 to transfer";
                foreach ( $languageVars as $languageVar ) {
                    if(trim( $languageVar->key['key']) === 'par-amountgreaterthanhundred'){
                        $errormsg = trim($languageVar->value);
                        break;
                    }
              }
        return self::respondWith(
            500, $errormsg

        );
            }

        $tax = round($trans_amount * 0.05);
        
        if($all_points > 0){
                    if($user->balance($this->partner->firstNetwork()->id) < ($trans_amount)) {
                        $errormsg = "User balance is insufficient for the requested transaction";
                        foreach ( $languageVars as $languageVar ) {
                            if(trim( $languageVar->key['key']) === 'par-userbalanceinsufficient'){
                                $errormsg = trim($languageVar->value);
                                break;
                            }
                        }
                            return self::respondWith(
                                500, $errormsg
                            );
                    }
                }
                else{
                    if($user->balance($this->partner->firstNetwork()->id) < ($trans_amount + $tax)) {
                        $errormsg = "User balance is insufficient for the requested transaction";
                        foreach ( $languageVars as $languageVar ) {
                            if(trim( $languageVar->key['key']) === 'par-userbalanceinsufficient'){
                                $errormsg = trim($languageVar->value);
                                break;
                            }
                        }
                            return self::respondWith(
                                500, $errormsg
                            );
                    }
                }
                
        

        	

        if(!$destination) {

            $errormsg = "The destination member does not exist in the system";
            foreach ( $languageVars as $languageVar ) {
                if(trim( $languageVar->key['key']) === 'par-destinationmembernotexist'){
                    $errormsg = trim($languageVar->value);
                    break;
                }
            }
            return self::respondWith(
                500, $errormsg
            );
        }

        if($destination->id == $user->id) {

            $errormsg = "You are unable to transfer points to yourself";
            foreach ( $languageVars as $languageVar ) {
                if(trim( $languageVar->key['key']) === 'par-unabletransferpointsurself'){
                    $errormsg = trim($languageVar->value);
                    break;
                }
            }
                    return self::respondWith(
                500, $errormsg
            );

        }


        if($destination->verified != true){

                $errormsg = "The destination member is not verified";
                foreach ( $languageVars as $languageVar ) {
                        if(trim( $languageVar->key['key']) === 'par-destinationmembernotverified'){
                                $errormsg = trim($languageVar->value);
                                break;
                        }
                }
                                return self::respondWith(
                        500, $errormsg
                );
        }
        //check network
        $senderNetwork		= $this->partner->firstNetwork()->id;
        $destinationPartners	= $destination->partners()->get();
        $destinationNetworks	= array();
        foreach ($destinationPartners as $destinationPartner) {
            $destinationNetworks[]	= $destinationPartner->firstNetwork()->id;
        }

        if(!in_array($senderNetwork, $destinationNetworks)){
                $errormsg = "The destination member doesn't belong to your network";
                return self::respondWith(
                        500, $errormsg
                );
        }

        // Sender (Redemption)
        $partner_id = $partner->id;
        

        $pos_id = $obj_session->pos_id;
        
        $store_id = $obj_session->store_id;

        $ptsToBeRedeemed = 0;
        if($all_points > 0){
            $ptsToBeRedeemed = $trans_amount;
        }
        else{
            $ptsToBeRedeemed = round($trans_amount + $tax);
        }

        $params = array(
            'user_id'   => $user->id,
            'partner'   => $partner_id,
            'points'    => $ptsToBeRedeemed,
            'store_id'  => $store_id,
            'pos_id'    => $pos_id,
            'source'    => "Website",
            'reference' => "Transfer points to {$destination->name}",
            'notes'     => "Points Transfer",
            'type'		=> "transfer",
            'posted_at' => date('Y-m-d H:i:s')
        );
        
        $redeemTrx	= Transaction::doRedemption($params, "Points");

            $redeemTrxId	= $redeemTrx->transaction['trx']['id'];
            $redeemTrxRef	= $redeemTrx->transaction['trx']['ref_number'];
        
        // Receiver (Reward)
        if($senderNetwork == 1){
                $receiver_partner_id	= 1;// BLU
                //$receiver_store_id	= 4689;// BLU-dev
                $receiver_store_id		= 1;// BLU-dev
                $receiver_pos_id		= 249;// BLU-dev
        }
        else{
            //to use the reward by parent feature, make sure to send the flag used below from the website
            // no need to use this if you are in BLU Network
            if(Input::has('reward_by_parent') && Input::get('reward_by_parent') == 1){
                $senderPartner  = Partner::find($partner_id);
                $receiver_partner = $senderPartner->topLevelPartner();
                $partner_id = $receiver_partner->id;
                $store_id   = $receiver_partner->stores->first()->id;
                $store = Store::find($store_id);
                $pos_id = $store->pointOfSales->first()->id;
            }
            $receiver_partner_id	= $partner_id;
            $receiver_store_id	= $store_id;
            $receiver_pos_id	= $pos_id;
        }
        $ptsToBeRewarded = 0;
        if($all_points > 0){
            $ptsToBeRewarded = round($trans_amount / 1.05);
        }
        else{
            $ptsToBeRewarded = $trans_amount;
        }
        $params = array(
            'user_id'   => $destination->id,
            'points'    => $ptsToBeRewarded,
            'store_id'  => $receiver_store_id,
            'pos_id'    => $receiver_pos_id,
            'partner'   => $receiver_partner_id,
            'reference' => "Transferred from {$user->name} | $redeemTrxId",
            'notes'     => "Points Received",
            'type'		=> "transfer",
            'posted_at' => date('Y-m-d H:i:s')
        );
        
        $rewardTrx	= Transaction::doReward($params, 'Amount');

        $rewardTrxId	= $rewardTrx->transaction['id'];
        Transaction::where('id',$redeemTrxId)->update(array('ref_number'=> $redeemTrxRef . " | " . $rewardTrxId));

        $balance = $user->balance($this->partner->firstNetwork()->id);

        $user1 = $user->toArray();

        $user1['balance'] = $balance;

		AuditHelper::record(
			$this->partner->id, "'{$user1['id']} / {$user1['email']}' Transferred {$trans_amount} points to '{$destination->id} / {$destination->email}'"
		);
        unset($user1['passcode']);
        return self::respondWith(
            200, null, $user1, 'user'
        );

    }
    
    
    /**
     * @description: post Cashback Points
     *
     *
     * @method:  POST 
     * @link:    /apiv2/cashback-points
     * 
     * @param post session
     * @param post trans_amount
     * @param post cashback_balance
     * @param post card_type
     * @param post first_digits
     * @param post last_digits
     * @param post lang
     *
     * @return Response
     */
    public function postCashbackPoints(){
		$partner = $this->partner;
                $partner_id = $partner->id;
                $session = Input::get('session');

                $obj_session = self::decryptSession($session, $partner->id);
                $user_id = $obj_session->user_id;
		$trans_amount           = Input::get('trans_amount', 0);

		$store_id		= $obj_session->store_id;
		$pos_id			= $obj_session->pos_id;
                $eligable_balance       = Input::get('cashback_balance', 0);
                $card_type              = Input::get('card_type');
                $first_digits           = Input::get('first_digits');
                $last_digits            = Input::get('last_digits');
                $lang                   = Input::get('lang','en');
                $languageObj            = new Language();
                $languageData           = Language::where('short_code', $lang)->first();
                $languageId		= $languageData->id;
                $language		= $languageObj::find($languageId);
                $languageVars           = $language->keys()->get();
		$user			= User::find($user_id);
                $arr_cashback = array('user_id'=>$user_id,'card_type'=>$card_type);
                APIController::postSendEmailJson(json_encode($arr_cashback), "post cashback points");
		if(!$user) {
                    $errormsg = "Could not locate user in database with id ";
                    foreach ( $languageVars as $languageVar ) {
                        if(trim( $languageVar->key['key']) === 'par-nouserindb'){
                            $errormsg = trim($languageVar->value);
                            break;
                        }
                    }
			return self::respondWith(
				500, $errormsg.$user_id
			);
		}
                
                
                if($trans_amount < 1000) {
                    $errormsg = "The minimum balance to be converted is 1,000 BLU Points";
                    foreach ( $languageVars as $languageVar ) {
                        if(trim( $languageVar->key['key']) === 'par-minbalancepts'){
                            $errormsg = trim($languageVar->value);
                            break;
                        }
                    }
                        return self::respondWith(
                                500, $errormsg
                        );
                }
                
		//$tax = round($trans_amount * 0.05);

		if($user->balance($partner->firstNetwork()->id) < $trans_amount){// + $tax)) {
			$errormsg = "Balance exceeded";
                        foreach ( $languageVars as $languageVar ) {
                        if(trim( $languageVar->key['key']) === 'par-balanceexceeded'){
                            $errormsg = trim($languageVar->value);
                            break;
                        }
                    }
                    return self::respondWith(
				500, $errormsg
			);
		}

            
            
            
                
		if(!$user->verified) {
                     $errormsg = "The user is not verified";
                       foreach ( $languageVars as $languageVar ) {
                        if(trim( $languageVar->key['key']) === 'par-userisnotverified'){
                            $errormsg = trim($languageVar->value);
                            break;
                        }
                    }
			return self::respondWith(
				500, $errormsg
			);
		}

		if($user->status != 'active') {
                    $errormsg = "The user is not currently active";
                    foreach ( $languageVars as $languageVar ) {
                        if(trim( $languageVar->key['key']) === 'par-usernotcurrentlyacive'){
                            $errormsg = trim($languageVar->value);
                            break;
                        }
                    }
			return self::respondWith(
				500, $errormsg
			);
		}

		// Sender (Redemption)

		//$sender_partner = $user->getTopLevelPartner();

                    
                    $remaining_eligble = array();
                    
                    $remaining_cashback_balance = $eligable_balance - $trans_amount;
                    $remaining_eligble['converted points'] = ( $remaining_cashback_balance > 0 ) ? $remaining_cashback_balance : 0;
                    
                    if($partner->has_middleware == 1 && $card_type != ""){
                        $remaining_eligble['mask_credit_card_number'] = explode('-', $card_type)[0];
                        $remaining_eligble['vcl_account_number'] = explode('-', $card_type)[1];
                    }
                    
                    //$remaining_eligble[] = 'converted points';
                    //$remaining_cashback_balance = $eligable_balance - $trans_amount;
                    //$remaining_eligble[] = ( $remaining_cashback_balance > 0 ) ? $remaining_cashback_balance : 0;
                    
                    $params = array(
                        'user_id'   => $user->id,
                        'partner'   => $partner_id,
                        'points'    => $trans_amount,//($trans_amount + $tax),
                        'store_id'  => $store_id,
                        'pos_id'    => $pos_id,
                        'source'    => "Website",
                        'reference' => json_encode($remaining_eligble),
                        'notes'     => "Cashback",
                        'posted_at' => date('Y-m-d H:i:s')
                    );
                    
                    if($card_type != "" && $first_digits != "" && $last_digits != ""){
                        $card = PartnerCardTypes::getCartTypeForNumber($card_type);
                        $remaining_eligble['card_type'] = $card['partner_card'] . ' - ' . $card['description'];
                        $xtea = new XTEA($partner->id);
                        $first_cipher = $xtea->Encrypt($first_digits);
                        $last_cipher = $xtea->Encrypt($last_digits);
                        $remaining_eligble['first_digits'] = $first_cipher;
                        $remaining_eligble['last_digits'] = $last_cipher;
                        
                        $params = array(
                            'user_id'   => $user->id,
                            'partner'   => $partner_id,
                            'points'    => $trans_amount,
                            'store_id'  => $store_id,
                            'pos_id'    => $pos_id,
                            'source'    => "Website",
                            'reference' => "Cashback",
                            'notes'     => json_encode($remaining_eligble),
                            'posted_at' => date('Y-m-d H:i:s')
                        );
                    }

                    $test = Transaction::doRedemption($params, "Points", "cashback");
                    
                                 
                    $balance = $user->balance($partner->firstNetwork()->id);

                    $user1 = $user->toArray();
                    $user1['balance'] = $balance;

                    AuditHelper::record(
                            $this->partner->id, "'{$user1['id']} / {$user1['email']}' Converted {$trans_amount} points to Cash'"
                    );

                    unset($user1['passcode']);
                    return self::respondWith(
                        200, null, $user1, 'user'
                    );
	}
        
    /**
     * @description: get the frequent flyer programs
     *
     *
     * @method:  POST 
     * @link:    /apiv2/programs
     * 
     * @param post session
     *
     * @return Response
     */
    public function postPrograms(){
        $partner = $this->partner;
        $partner_id = $partner->id;
        $session = Input::get('session');
        $obj_session = self::decryptSession($session, $partner_id);
        $affiliate_programs = $partner->affiliateprograms;
        return self::respondWith(
            200, "OK", $affiliate_programs, 'programs'
        );

    }
    
    /**
     * @description: make a miles points conversion
     *
     *
     * @method:  POST 
     * @link:    /apiv2/miles-points
     * 
     * @param post session
     * @param post trans_amount
     * @param post cashback_balance
     * @param post airlines
     * @param post frequent_flyer_number
     * @param post lang
     *
     * @return Response
     */
    public function postMilesPoints(){
		$partner = $this->partner;
                $partner_id = $partner->id;
                $session = Input::get('session');
                $obj_session = self::decryptSession($session, $partner_id);
                $user_id = $obj_session->user_id;
		$trans_amount           = Input::get('trans_amount', 0);
		
		$store_id		= $obj_session->store_id;
		$pos_id			= $obj_session->pos_id;
                $eligable_balance       = Input::get('cashback_balance', 0);
                $airline                = Input::get('airlines');
                $frequent_flyer_number  = Input::get('frequent_flyer_number');
                $lang                   = Input::get('lang','en');
                $languageObj            = new Language();
                $languageData           = Language::where('short_code', $lang)->first();
                $languageId		= $languageData->id;
                $language		= $languageObj::find($languageId);
                $languageVars           = $language->keys()->get();

		$user			= User::find($user_id);

		if(!$user) {
                    $errormsg = "Could not locate user in database with id ";
                    foreach ( $languageVars as $languageVar ) {
                        if(trim( $languageVar->key['key']) === 'par-nouserindb'){
                            $errormsg = trim($languageVar->value);
                            break;
                        }
                    }
			return self::respondWith(
				500, $errormsg.$user_id
			);
		}

                
                if($trans_amount < 1000) {
                    $errormsg = "The minimum balance to be converted is 1,000 BLU Points";
                    foreach ( $languageVars as $languageVar ) {
                        if(trim( $languageVar->key['key']) === 'par-minbalancepts'){
                            $errormsg = trim($languageVar->value);
                            break;
                        }
                    }
                        return self::respondWith(
                                500, $errormsg
                        );
                }
                
		//$tax = round($trans_amount * 0.05);

		if($user->balance($partner->firstNetwork()->id) < $trans_amount){// + $tax)) {
			$errormsg = "Balance exceeded";
                        foreach ( $languageVars as $languageVar ) {
                        if(trim( $languageVar->key['key']) === 'par-balanceexceeded'){
                            $errormsg = trim($languageVar->value);
                            break;
                        }
                    }
                    return self::respondWith(
				500, $errormsg
			);
		}


            if(!$user->verified) {
                 $errormsg = "The user is not verified";
                   foreach ( $languageVars as $languageVar ) {
                    if(trim( $languageVar->key['key']) === 'par-userisnotverified'){
                        $errormsg = trim($languageVar->value);
                        break;
                    }
                }
                    return self::respondWith(
                            500, $errormsg
                    );
            }

            if($user->status != 'active') {
                $errormsg = "The user is not currently active";
                foreach ( $languageVars as $languageVar ) {
                    if(trim( $languageVar->key['key']) === 'par-usernotcurrentlyacive'){
                        $errormsg = trim($languageVar->value);
                        break;
                    }
                }
                    return self::respondWith(
                            500, $errormsg
                    );
            }

		// Sender (Redemption)

		//$sender_partner = $user->getTopLevelPartner();

                   
                    $remaining_eligble = array();
                    
                    $network                = Network::find($partner->firstNetwork()->id);
                    $networkCurrencyId      = $network->currency_id;
                    $networkCurrencyData    = Currency::where('id', $networkCurrencyId)->first();
                    $networkCurrency        = $networkCurrencyData->short_code;
                    $remaining_eligble['miles points'] = PointsHelper::pointsToAmount($trans_amount, $networkCurrency, $network->id, 'miles', $partner_id);
                    
                    $miles_details = array();
                    $miles_details["airline"] = $airline;
                    $miles_details["frequent_flyer_number"] = $frequent_flyer_number;
                    
                    $params = array(
                        'user_id'   => $user->id,
                        'partner'   => $partner_id,
                        'points'    => $trans_amount,//($trans_amount + $tax),
                        'store_id'  => $store_id,
                        'pos_id'    => $pos_id,
                        'source'    => "Website",
                        'reference' => json_encode($remaining_eligble),
                        'notes'     => json_encode($miles_details),
                        'posted_at' => date('Y-m-d H:i:s')
                    );
//                        var_dump($params);
//                        exit();

                    $test = Transaction::doRedemption($params, "Points", "miles");
//                    echo "<pre>";
//                    var_dump($test);
//                    exit();
                    
                    //save new membership
                    $affiliateProgramMemberships = AffiliateProgramsMemberships::where('user_id', $user->id)->where('program_id', $airline)->where('membership_number', $frequent_flyer_number)->count();
                    if($affiliateProgramMemberships == 0){
                         $affiliateProgramMembership = new AffiliateProgramsMemberships();
        
                        $affiliateProgramMembership->user_id              = $user->id;
                        $affiliateProgramMembership->membership_number    = $frequent_flyer_number;
                        $affiliateProgramMembership->created_at           = date('Y-m-d H:i:s');
                        $affiliateProgramMembership->updated_at           = date('Y-m-d H:i:s');
                        $affiliateProgramMembership->program_id           = $airline;
                        $affiliateProgramMembership->save();
                    }
                    
                    $balance = $user->balance($this->partner->firstNetwork()->id);

                    $user1 = $user->toArray();

                    $user1['balance'] = $balance;

                    AuditHelper::record(
                            $this->partner->id, "'{$user1['id']} / {$user1['email']}' Converted {$trans_amount} points to Miles'"
                    );

                    unset($user1['passcode']);
                    return self::respondWith(
                        200, null, $user1, 'user'
                    );
	}
    
} // EOC

