<?php
namespace App\Http\Controllers;
use App\Media;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Input;
use View;
use Validator;
use Session;
use Security;
use I;
use Illuminate\Support\Facades\Auth;
use App\BluCollection as BluCollection;
use AuditHelper;
use Illuminate\Support\Facades\Redirect;
use Transaction;
use App\Partner as Partner;
use Notification;
use Ticket;
use UCID;
use Language;
use Localizationkey;
use LangKey;
use ManagedObjectsHelper;
use User;
use App\Country;
use App\Admin as Admin;
use App\Role as Role;
use App\City as City;
use App\Area as Area;
use Meta;
use App\ProductPartnerRedemption as ProductPartnerRedemption;
use App\ProductPartnerReward as ProductPartnerReward;
use App\Category as Category;
use App\Brand as Brand;
use App\Supplier as Supplier;
use App\Prodmodel as Prodmodel;
use App\ProdmodelPartner as ProdmodelPartner;
use App\Network as Network;
use App\Currency as Currency;
use App\Product as Product;
use App\Segment as Segment;
use App\Tier as Tier;
use App\Tiercriteria as Tiercriteria;
use App\ProductRedemptionCountries as ProductRedemptionCountries;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use App\ProductUpc as ProductUpc;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Product Controller
 *
 * @category   Controllers
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class ProductPricingController extends BaseController {

    protected $layout = "layouts.dashboard";

    /**
     * Get all list data
     *
     * @return array
     */
    private function getAllLists()
    {
        return array(
            'brands' => Brand::where('draft', false)->orderBy('name')->pluck('name', 'id'),
            'suppliers' => Supplier::where('draft', false)->orderBy('name')->pluck('name', 'id'),
            'categories' => Category::with('children')->where('parent_category_id', 1)->where('draft', false)->orderBy('name', 'asc')->get(),
            'filter_categories' => Category::with('children')->where('parent_category_id', 1)->where('draft', false)->orderBy('name', 'asc')->pluck('name', 'id'),
            'networks' => Network::orderBy('name')->pluck('name', 'id'),
            'partners' => ManagedObjectsHelper::managedPartners()->sortBy('name')->pluck('name', 'id'),
            'countries' => Country::orderBy('name')->pluck('name', 'id'),
//            'segments' => ManagedObjectsHelper::managedSegments()->sortBy('name')->pluck('name', 'id'),
            'delivery_options' => Meta::deliveryOptions(),
            'currencies' => Currency::where('draft', false)->orderBy('name')->pluck('name', 'id'),
            'prefixes' => ManagedObjectsHelper::managedPartners()->sortBy('prefix')->pluck('prefix', 'prefix')
        );
    }

    /**
     * Return the product validations
     *
     * @return array
     */
    private function getValidations() {
        return array(
            'name' => 'required',
            'brand_id' => 'required',
            'category_id' => 'required',
            'end_date' => 'date',
            'start_date' => 'date',
            'qty' => 'integer',
            'original_price' => 'numeric|min:0',
            'original_retail_price' => 'numeric|min:0',
            'original_sales_tax' => 'numeric|min:0',
            'weight' => 'numeric|min:0',
            'volume' => 'numeric|min:0',
            'delivery_charges' => 'numeric|min:0',
        );
    }

    /**
     * Return the product validations
     *
     * @return array
     */
    private function getValidationsRedemption() {
        return array(
            'end_date' => 'date|nullable',
            'start_date' => 'date|nullable',
            'qty' => 'required|integer',
            'original_price' => 'required|numeric|min:0',
            'original_retail_price' => 'numeric|min:0',
            'weight' => 'numeric|min:0',
            'volume' => 'numeric|min:0',
            'delivery_charges' => 'numeric|min:0',
        );
    }

    /**
     * Display a list of the products
     *
     * @return void
     */
    public function listProducts(Request $request)
    {
        if (!I::can('view_products')) {
            return Redirect::to(url("dashboard"));
        }

        if (I::am('BLU Admin')) {
            $product = Product::select('product.*')->where('draft', false);
        } else {
            $partner = Auth::User()->getTopLevelPartner()->id;
            $product = Product::select('product.*');
            $products = $product->whereHas('Partners', function($q) use($partner) {
                $q->where('partner_id', $partner);
            });
        }

        if (Input::has('q')) {
            $q = self::sanitizeText(Input::get('q'));
            $products = $product->where('product.name', 'like', '%' . $q . '%')->orWhere('product.id', 'like', '%' . $q . '%');
        }

        // filter supplier_id
        if (Input::has('supplier_list')) {
            $q = explode(',', Input::get('supplier_list'));
            if (sizeof($q) == 1) {
                $q = urldecode($q[0]);
                $q = explode(',', $q);
            }

            $products = $product->whereIn('supplier_id', $q);
        }

        // filter country_id
        if (Input::has('country_list')) {

            $q = explode(',', Input::get('country_list'));
            if (sizeof($q) == 1) {
                $q = urldecode($q[0]);
                $q = explode(',', $q);
            }

            $products = $product->whereHas('countries', function($u) use($q) {
                $u->whereIn('country_id', $q);
            });
        }

        // filter country_id
        if (Input::has('partner_list')) {

            $q = explode(',', Input::get('partner_list'));
            if (sizeof($q) == 1) {
                $q = urldecode($q[0]);
                $q = explode(',', $q);
            }

            $products = $product->whereHas('Partners', function($u) use($q) {
                $u->whereIn('partner_id', $q);
            });
        }


        // filter segment_id  && explode(',', Input::get('partner_id')) != '0'
        if (Input::has('displaychannel_list')) {
            $q = explode(',', Input::get('displaychannel_list'));
            if (sizeof($q) == 1) {
                $q = urldecode($q[0]);
                $q = explode(',', $q);
            }

            $products = $product->whereHas('channels', function($u) use($q) {
                $u->whereIn('channel_id', $q);
            });
        }

        // filter supplier_id
        if (Input::has('brand_list')) {

            $q = explode(',', Input::get('brand_list'));

            if (sizeof($q) == 1) {
                $q = urldecode($q[0]);
                $q = explode(',', $q);
            }
            $products = $product->whereIn('brand_id', $q);
        }

        // filter category
        if (Input::has('cat_list')) {

            $q = explode(',', Input::get('cat_list'));

            if (sizeof($q) == 1) {
                $q = urldecode($q[0]);
                $q = explode(',', $q);
            }
            $products = $product->whereIn('category_id', $q);
        }

        $products = $product->orderBy('id','DESC');
        $products = $product->paginate(15);

        foreach ($products as $product) {
            // check display online
            $productPartnerRedemptions = ProductPartnerRedemption::where('product_id', $product->id)->get();
            if(!$productPartnerRedemptions){
                continue;
            }
            foreach ($productPartnerRedemptions as $productPartnerRedemption) {
                if($productPartnerRedemption->display_online == 1 ){
                    $product->display_online = 1;
                }
            }
        }

        $query_string = $request->getQueryString();

        return View::make("productpricing.list", array(
            'products' => $products,
            'lists' => $this->getAllLists(),
            'query_string' => $query_string
        ));
    }

    /**
     * get fields for new product
     *
     * @return void
     */
    public function newProduct() {
        if (!I::can('create_products')) {
            return Redirect::to(url("dashboard"));
        }
        $product = new Product();

        $product->ucid =  UCID::newProductID();
        $product->start_date = null;
        $product->end_date = null;
        $product->draft = true;
        $product->status = 'active';
        $product->price_in_usd = 0;

        $product->save();

        // Link to BLU partner automtically
        $product->partners()->attach(1);

        // Link to top level partner automtically

        $partners = Auth::User()->getTopHierarchyPartnerIds();
        $defaultPartners = array();
        foreach ($partners as $partner) {
            if ($partner != 1) {
                $product->partners()->attach($partner);
            }
            
            $defaultPartners[] = Partner::find($partner)->name;
        }

        $productId = base64_encode($product->id);
        //Audit Log for create new product
        AuditHelper::record(Auth::User()->getTopLevelPartner()->id,Auth::User()->id . " Admin of " . json_encode($defaultPartners) . " Created New Product with ID = " . $product->id  ." at " . date('Y-m-d H:i:s'));
        return Redirect::to(url("dashboard/productpricing/view/{$productId}"));
    }

    /**
     * View a single Product entry
     *
     * @param int $id
     * @return void
     */
    public function viewProduct($enc_id = null) {
        $id = base64_decode($enc_id);
        if (!I::can('view_products')) {
            return Redirect::to(url("dashboard"));
        }
        $product = Product::find($id);

        if (!$product) {
           \App::abort(404);
        }
        
        $disabled = 'disabled';
        $managedPartners = Auth::User()->managedPartners();
        $can_view_product = false;
        $managedPartnersArray = array();
        foreach ($managedPartners as $manP) {
            $managedPartnersArray[] = $manP->id;
            foreach ($product->partners as $p) {
                //var_dump($p->id);
                if ($manP->id == $p->id) {
                    $can_view_product = true;
                    $disabled = false;
                    break;
                }
            }
        }
        
        foreach ($product->ProductPartnerRedemption as $pr) {
                if (!in_array($pr->partner_id, $managedPartnersArray) && $pr->partner_id != 1) {
                    $disabled = 'disabled';
                    break;
                }else{
                    $disabled = false;
                }
            }
//        if ($can_view_product == false && $product->draft == false) {
//            return Redirect::to(url("dashboard"));
//        }

        return View::make("productpricing.view", array_merge(
                                $this->getAllLists(), array('product' => $product, 'disabled' => $disabled)
        ));
    }

    /**
     * Delete a product instance
     *
     * @param int $id
     * @return void
     */
    public function deleteProduct($enc_id = null) {
        $id = base64_decode($enc_id);
        if (!I::can('delete_products')) {
            return Redirect::to(url("dashboard"));
        }
        $product = Product::find($id);

        if (!$product) {
            \App::abort(404);
        }
        $productId = $product->id;
        // delete product from cart
        DB::table('cart')->where('product_id', $product->id)->delete();
        //Audit Log for delete product
        AuditHelper::record(Auth::User()->getTopLevelPartner()->id,Auth::User()->id . " Deleted Product with ID = " .  $product->id  ." at " . date('Y-m-d H:i:s'));
        $product->delete();
//        DB::table('product_redemption_countries')->where('product_id', $productId)->delete();
//        DB::table('product_redemption_display_channel')->where('product_id', $productId)->delete();
//        DB::table('product_redemption_segments')->where('product_id', $productId)->delete();
//        DB::table('product_partner_reward')->where('product_id', $productId)->delete();
//        DB::table('product_translation')->where('product_id', $productId)->delete();
        
        return Redirect::to('dashboard/productpricing');
    }

    /**
     * Update a product
     *
     * @param int $id
     * @return void
     */
    public function saveProduct(Request $request, $enc_id)
    {
        if ($request->isMethod('get')) {
            return redirect('/dashboard/productpricing');
        }

        $id = base64_decode($enc_id);
        if (!I::can('edit_products')) {
            return Redirect::to(url("dashboard"));
        }
        $product = Product::find($id);

        if (!$product) {
            \App::abort(404);
        }

        $validator = Validator::make(Input::all(), $this->getValidations());

        if ($validator->fails()) {
            return Redirect::to(url("dashboard/productpricing/view/{$enc_id}"))->withErrors($validator)->withInput();
        }

        $input = Input::all();
        foreach($input as $k=>$i){
            $input[$k] = self::sanitizeText($i);
        }

        // Check for cover image upload
        if (Input::hasFile('cover_image')) {
            $file = Input::file('cover_image');
            $path = public_path('uploads');
            $ext = strtolower($file->getClientOriginalExtension());
            $size = $file->getSize();
            $type = $file->getMimeType();
            $name = md5(time()) . ".{$ext}";

            if (in_array($ext, Meta::allowedImageExts())) {
                Input::file('cover_image')->move($path, $name);

                $f = new Media();
                $f->name = $name;
                $f->size = $size;
                $f->type = $type;
                $f->ext = $ext;
                $f->save();

                $input['cover_image'] = $f->id;
            }
        }

        // Check for multiple uploads
        if (Input::hasFile('image')) {
            $images = Input::file('image');

            if (!is_array($images)) {
                $images = array($images);
            }

            foreach ($images as $file) {
                $path = public_path('uploads');
                $ext = strtolower($file->getClientOriginalExtension());
                $size = $file->getSize();
                $type = $file->getMimeType();
                $name = md5(time()) . ".{$ext}";

                if (in_array($ext, Meta::allowedImageExts())) {
                    Input::file('image')->move($path, $name);

                    $f = new Media();
                    $f->name = $name;
                    $f->size = $size;
                    $f->type = $type;
                    $f->ext = $ext;
                    $f->save();

                    // Link to product
                    $product->media()->attach($f);
                }
            }

            unset($input['image']);
        }

        if (!empty($input)) {
            foreach ($input as $k => $v) {
                if ($v == '') {
                    unset($input[$k]);
                }
            }
        }

        $input['partner_mapping_id'] = '';
        $product->draft = false;

        $product->update($input);

        // delete product from cart
        if ($product->qty == 0 || $product->display_online == 0) {
            DB::table('cart')->where('product_id', $product->id)->delete();
        }

        //Audit Log for update product
        AuditHelper::record(Auth::User()->getTopLevelPartner()->id,Auth::User()->id . " Updated Product with ID = " .  $product->id  ." at " . date('Y-m-d H:i:s'));
        
        return Redirect::to(url("dashboard/productpricing/view/{$enc_id}"));
    }

    /**
     * Create a small LI list with product partners
     *
     * @param int $user_id
     * @return Response
     */
    private function listPartners($enc_product_id, $error = null) {
        $product_id = base64_decode($enc_product_id);
        $product = Product::find($product_id);
        return View::make('productpricing.list.partnerList', array(
            'product' => $product,
            'error' => $error
        ));
    }

    /**
     * link a partner to a product
     *
     * @param int $product id
     * @return void
     */
    public function postLink_partner($enc_id) {
        $id = base64_decode($enc_id);
        $partner_id = self::sanitizeText(Input::get('partner_id'));
        $product = Product::find($id);
        $partner = Partner::find($partner_id);

        if (!$product || !$partner) {
            \App::abort(404);
        }

        if (!$product->partners->contains($partner)) {
            $product->partners()->attach($partner);
        }
        
        //Audit Log for link partner
        AuditHelper::record(Auth::User()->getTopLevelPartner()->id,Auth::User()->id . " Linked the product with ID = " .  $product->id  ." to the partner with ID = ". $partner_id ." at " . date('Y-m-d H:i:s'));
        
        return $this->listPartners($enc_id);
    }

    /**
     * unlink a partner froma product
     *
     * @param int $product id
     * @return void
     */
    public function getUnlink_partner($enc_product_id, $_enc_partner_id)
    {
        $product_id = base64_decode($enc_product_id);
        $partner_id = base64_decode($_enc_partner_id);

        $product = Product::find($product_id);
        $partner = Partner::find($partner_id);

        if (!$product || !$partner) {
            \App::abort(404);
        }

        $productPartnersCount = $product->partners()->count();

        if($productPartnersCount == 1) {
            return $this->listPartners($enc_product_id, 'Should have at least 1 partner');
        }

        $product->partners()->detach($partner_id);
        
        //Audit Log for link partner
        AuditHelper::record(Auth::User()->getTopLevelPartner()->id,Auth::User()->id . " Unlinked the product with ID = " .  $product->id  ." from the partner with ID = ". $partner_id ." at " . date('Y-m-d H:i:s'));
        
        return $this->listPartners($enc_product_id);
    }

    /**
     * Create a small LI list with product networks
     *
     * @param int $product_id
     * @return Response
     */
    private function listNetworks($enc_product_id) {
        $product_id = base64_decode($enc_product_id);
        $product = Product::find($product_id);
        return View::make('productpricing.list.networkList', array(
                    'product' => $product,
        ));
    }

    /**
     * link a network to a product
     *
     * @param int $product id
     * @return void
     */
    public function postLink_network($id) {
        $network_id = self::sanitizeText(Input::get('network_id'));
        $product = Product::find($id);
        $network = Network::find($network_id);

        if (!$product->networks->contains($network)) {
            $product->networks()->attach($network);
        }
        
        //Audit Log for link Network
        AuditHelper::record(Auth::User()->getTopLevelPartner()->id,Auth::User()->id . " linked the product with ID = " .  $product->id  ." to the network with ID = ". $network_id ." at " . date('Y-m-d H:i:s'));
        
        return $this->listNetworks($id);
    }

    /**
     * unlink a network from a product
     *
     * @param int $product id, $network_id
     * @return void
     */
    public function getUnlink_network($enc_product_id, $enc_network_id) {
        $product_id = base64_decode($enc_product_id);
        $network_id = base64_decode($enc_network_id);
        $product = Product::find($product_id);

        $product->networks()->detach($network_id);
        
         //Audit Log for link Network
        AuditHelper::record(Auth::User()->getTopLevelPartner()->id,Auth::User()->id . " Unlinked the product with ID = " .  $product->id  ." from the network with ID = ". $network_id ." at " . date('Y-m-d H:i:s'));
        
        return $this->listNetworks($enc_product_id);
    }

    /**
     * Create a small LI list with product segments
     *
     * @param int $product_id
     * @return Response
     */
    private function listSegments($enc_product_redemption_id, $error = null) {
        $product_redemption_id = base64_decode($enc_product_redemption_id);
        $productRedemption = ProductPartnerRedemption::find($product_redemption_id);
        $product = Product::find($productRedemption->product_id);

        return View::make('productpricing.list.segmentList', array(
                    'productRedemption' => $productRedemption,
                    'product' => $product,
                    'error' => $error,
        ));
    }

    /**
     * link a segment to a product
     *
     * @param int $product id
     * @return void
     */
    public function postLink_segment($enc_id) {
        $id = base64_decode($enc_id);
        $segment_id = self::sanitizeText(Input::get('segment_id'));
        $productRedemption = ProductPartnerRedemption::find($id);
        
        $count = DB::table('product_redemption_segments')->where('product_redemption_id', $productRedemption->id)
                ->where('segment_id', $segment_id)
                ->where('product_id', $productRedemption->product_id)
                ->count();
        if($count > 0){
            return $this->listSegments($enc_id, 'Product is already linked to this segment');
        }
        
        DB::table('product_redemption_segments')->insert(array(
            'product_redemption_id' => $productRedemption->id,
            'product_id' => $productRedemption->product_id,
            'segment_id' => $segment_id
        ));

         //Audit Log for link Network
        AuditHelper::record(Auth::User()->getTopLevelPartner()->id,Auth::User()->id . " linked the product with ID = " .  $productRedemption->product_id  ." to the segment with ID = ". $segment_id ." at the Partner " . $productRedemption->partner_id . " at " . date('Y-m-d H:i:s'));
        
        
        return $this->listSegments($enc_id);
    }

    /**
     * unlink a segment from a product
     *
     * @param int $product id, $segment_id
     * @return void
     */
    public function getUnlink_segment($enc_product_redemption_id, $enc_segment_id) {
        $product_redemption_id = base64_decode($enc_product_redemption_id);
        $segment_id = base64_decode($enc_segment_id);
        $product = ProductPartnerRedemption::find($product_redemption_id);
        //Audit Log for link Network
        AuditHelper::record(Auth::User()->getTopLevelPartner()->id,Auth::User()->id . " Unlinked the product with ID = " .  $product->product_id  ." from the segment with ID = ". $segment_id ." at the Partner " . $product->partner_id . " at " . date('Y-m-d H:i:s'));
        
        $product->segments()->detach($segment_id);

        return $this->listSegments($enc_product_redemption_id);
    }

    /**
     * Create a small LI list with product countries
     *
     * @param int $product_id
     * @return Response
     */
    private function listCountries($enc_product_redemption_id, $error = null) {
        $product_redemption_id = base64_decode($enc_product_redemption_id);
        $productRedemption = ProductPartnerRedemption::find($product_redemption_id);
        $product = Product::find($productRedemption->product_id);

        return View::make('productpricing.list.countryList', array(
                    'productRedemption' => $productRedemption,
                    'product' => $product,
                    'error' => $error,
        ));
    }

    /**
     * link a country to a product
     *
     * @param int $product id
     * @return void
     */
    public function postLink_country($enc_id) {
        $id = base64_decode($enc_id);
        $country_id = self::sanitizeText(Input::get('country_id'));
        $custom_tarrif_taxes = self::sanitizeText(Input::get('custom_tarrif_taxes', 0));
        $delivery_charges = self::sanitizeText(Input::get('delivery_charges', 0));
        $currency_id = self::sanitizeText(Input::get('currency_id', 0));
        $status = self::sanitizeText(Input::get('status'));
        $productRedemption = ProductPartnerRedemption::find($id);
        $comparisonArray = array();

        $count = DB::table('product_redemption_countries')->where('product_redemption_id', $productRedemption->id)
                ->where('country_id', $country_id)
                ->where('product_id', $productRedemption->product_id)
                ->count();
        if($count > 0){
            return $this->listCountries($enc_id, 'Product is already linked to this country');
        }
        
        //check /product/channel/country
        $productRedemptions = ProductPartnerRedemption::where('product_id', $productRedemption->product_id)->get();
        foreach($productRedemptions as $productRedemption){
            $productCountriesObjs = DB::table('product_redemption_countries')->where('product_redemption_id', $productRedemption->id)->get();
            $productChannelsObjs = DB::table('product_redemption_display_channel')->where('product_redemption_id', $productRedemption->id)->get();
            foreach ($productCountriesObjs as $productCountriesObj) {
            $comparisonArray[$productRedemption->id]['countries'][] =  $productCountriesObj->country_id;
            }
            foreach ($productChannelsObjs as $productChannelsObj) {
            $comparisonArray[$productRedemption->id]['channels'][] =  $productChannelsObj->partner_id;
            }
        }
        foreach ($comparisonArray as $comparison) {
            if(!empty($comparison['countries'])){
            if(in_array($country_id, $comparison['countries'])){
                if(empty($comparisonArray[$id])){
                    continue;
                }
                foreach ($comparisonArray[$id]['channels'] as $redemptionChannel) {
                    if(in_array($redemptionChannel, $comparison['channels'])){
                        return $this->listCountries($enc_id, 'Product already exists in this Channel/Country');
                    }
                }
            }
            }
        }
        
        DB::table('product_redemption_countries')->insert(array(
            'product_redemption_id' => $id,
            'product_id' => $productRedemption->product_id,
            'partner_id' => $productRedemption->partner_id,
            'country_id' => $country_id,
            'custom_tarrif_taxes' => $custom_tarrif_taxes,
            'delivery_charges' => $delivery_charges,
            'currency_id' => $currency_id,
            'status' => $status
        ));
        
        //Audit Log for link Network
        AuditHelper::record(Auth::User()->getTopLevelPartner()->id,Auth::User()->id . " linked the product with ID = " .  $productRedemption->product_id  ." to the country with ID = ". $country_id ." at the Partner " . $productRedemption->partner_id . " at " . date('Y-m-d H:i:s'));
        
        return $this->listCountries($enc_id);
    }

    /**
     * unlink a country from a product
     *
     * @param int $product id, $country_id
     * @return void
     */
    public function getUnlink_country($enc_product_redemption_id, $enc_id) {
        $id = base64_decode($enc_id);
        
        $details = DB::table('product_redemption_countries')->where('id', $id)->first();
        //Audit Log for link Network
        AuditHelper::record(Auth::User()->getTopLevelPartner()->id,Auth::User()->id . " linked the product with ID = " .  $details->product_id  ." to the country with ID = ". $details->country_id ." at the Partner " . $details->partner_id . " at " . date('Y-m-d H:i:s'));
        
        DB::table('product_redemption_countries')->where('id', $id)
                ->delete();

        return $this->listCountries($enc_product_redemption_id);
    }

    /**
     * Load a list of channels associated to the specified product
     *
     * @param int $product_id
     * @return View
     */
    private function listChannels($enc_product_redemption_id, $error = null) {
        $product_redemption_id = base64_decode($enc_product_redemption_id);
        $productRedemption = ProductPartnerRedemption::find($product_redemption_id);
        $product = Product::find($productRedemption->product_id);
        return View::make('productpricing.list.channelList', array(
                    'productRedemption' => $productRedemption,
                    'product' => $product,
                    'error' => $error,
        ));
    }

    /**
     * Link the product to a partner channel
     *
     * @param int $product
     * @return View
     */
    public function postLink_channel($enc_id, $enc_product_id) {
        $id = base64_decode($enc_id);
        $product_id = base64_decode($enc_product_id);
        $partner_id = self::sanitizeText(Input::get('partner_id'));
        $comparisonArray = array();

        $count = DB::table('product_redemption_display_channel')->where('product_redemption_id', $id)
                ->where('partner_id', $partner_id)
                ->where('product_id', $product_id)
                ->count();
        if($count > 0){
            return $this->listChannels($enc_id, 'Product is already linked to the channel');
        }
        
        //check /product/channel/country
        $productRedemptions = ProductPartnerRedemption::where('product_id', $product_id)->get();
        foreach($productRedemptions as $productRedemption){
            $productCountriesObjs = DB::table('product_redemption_countries')->where('product_redemption_id', $productRedemption->id)->get();
            $productChannelsObjs = DB::table('product_redemption_display_channel')->where('product_redemption_id', $productRedemption->id)->get();
            foreach ($productCountriesObjs as $productCountriesObj) {
            $comparisonArray[$productRedemption->id]['countries'][] =  $productCountriesObj->country_id;
            }
            foreach ($productChannelsObjs as $productChannelsObj) {
            $comparisonArray[$productRedemption->id]['channels'][] =  $productChannelsObj->partner_id;
            }
        }
        foreach ($comparisonArray as $comparison) {
            if(!empty($comparison['channels'])){
            if(in_array($partner_id, $comparison['channels'])){
                if(empty($comparisonArray[$id])){
                    continue;
                }
                foreach ($comparisonArray[$id]['countries'] as $redemptionCountry) {
                    if(in_array($redemptionCountry, $comparison['countries'])){
                        return $this->listChannels($enc_id, 'Product already exists in this Channel/Country');
                    }
                }
            }
            }
        }

        DB::table('product_redemption_display_channel')->insert(array(
            'product_redemption_id' => $id,
            'product_id' => base64_decode($enc_product_id),
            'partner_id' => $partner_id
        ));
        
        //Audit Log for link Network
        AuditHelper::record(Auth::User()->getTopLevelPartner()->id,Auth::User()->id . " linked the product with ID = " .  $product_id  ." to the channel with ID = ". $partner_id ." at the Partner at " . date('Y-m-d H:i:s'));
        
        return $this->listChannels($enc_id);
    }

    /**
     * unlink a country from a product
     *
     * @param int $product id
     * @param int $partner_id
     * @return View
     */
    public function getUnlink_channel($enc_id, $enc_partner_id) {
        $id = base64_decode($enc_id);
        $partner_id = base64_decode($enc_partner_id);
        
        DB::table('product_redemption_display_channel')->where('product_redemption_id', $id)
                ->where('partner_id', $partner_id)
                ->delete();

        //Audit Log for link Network
        AuditHelper::record(Auth::User()->getTopLevelPartner()->id,Auth::User()->id . " Unlinked the product redemption with ID = " .  $id  ." from the channel with ID = ". $partner_id . " at " . date('Y-m-d H:i:s'));
        
        return $this->listChannels($enc_id);
    }

    /**
     * Create a small LI list with product partners
     *
     * @param int $user_id
     * @return Response
     */
    private function listRedemptionPartners($enc_product_id, $error = null) {
        $product_id = base64_decode($enc_product_id);
        $product = Product::find($product_id);
        return View::make('productpricing.list.redemptionPartnersList', array(
                    'product' => $product,
                    'error' => $error,
        ));
    }

    /**
     * link a partner to a product
     *
     * @param int $product id
     * @return void
     */
    public function postLink_redemption_partner($enc_id)
    {
        $id = base64_decode($enc_id);
        $partner_id = self::sanitizeText(Input::get('partner_id'));
        $product = Product::find($id);
        $partner = Partner::find($partner_id);

        if (!$product || !$partner) {
            \App::abort(404);
        }

        $count = ProductPartnerRedemption::where('product_id', $id)->where('partner_id', $partner_id)->count();

        if($count > 0){
            return $this->listRedemptionPartners($enc_id, 'This Partner is already linked to the product.');
        }

        $redemptioProduct = new ProductPartnerRedemption();

        if (!$product->ProductPartnerRedemption->contains($partner)) {
            $redemptioProduct->product_id = $id;
            $redemptioProduct->partner_id = $partner_id;
            $redemptioProduct->supplier_id = 0;
            $redemptioProduct->draft = 0;
            $redemptioProduct->save();
        }
        
        //Audit Log for link Network
        AuditHelper::record(Auth::User()->getTopLevelPartner()->id,Auth::User()->id . " linked the product redemption with ID = " .  $id  ." to the partner with ID = ". $partner_id . " at " . date('Y-m-d H:i:s'));
        
        return $this->listRedemptionPartners($enc_id);
    }

    /**
     * unlink a partner froma product
     *
     * @param int $product id
     * @return void
     */
    public function getUnlink_redemption_partner($enc_product_id, $_enc_partner_id)
    {
        $product_id = base64_decode($enc_product_id);
        $redemption_partner_id = base64_decode($_enc_partner_id);

        $product = Product::find($product_id);
        $partner = ProductPartnerRedemption::find($redemption_partner_id);

        if (!$product || !$partner) {
            \App::abort(404);
        }

        $redemptioProduct = ProductPartnerRedemption::where('id', $redemption_partner_id)->delete();

        //delete channels/segments/countries
        DB::table('product_redemption_countries')->where('product_redemption_id', $redemption_partner_id)->delete();
        DB::table('product_redemption_display_channel')->where('product_redemption_id', $redemption_partner_id)->delete();
        DB::table('product_redemption_segments')->where('product_redemption_id', $redemption_partner_id)->delete();

        return $this->listRedemptionPartners($enc_product_id);
    }

    /**
     * Create a small LI list with product partners
     *
     * @param int $user_id
     * @return Response
     */
    private function listRewardPartners($enc_product_id, $error = null) {
        $product_id = base64_decode($enc_product_id);
        $product = Product::find($product_id);
        return View::make('productpricing.list.rewardPartnersList', array(
                'product' => $product,
                'error' => $error,
        ));
    }

    /**
     * link a partner to a product
     *
     * @param int $product id
     * @return void
     */
    public function postLink_reward_partner($enc_id)
    {
        $id = base64_decode($enc_id);
        $partner_id = self::sanitizeText(Input::get('partner_id'));
        $product = Product::find($id);
        $partner = Partner::find($partner_id);
        $prefix = self::sanitizeText(Input::get('partner_prefix', ""));
        $mapping_id = self::sanitizeText(Input::get('partner_mapping_id', ""));
        $partnerMappingId = $prefix . $mapping_id;

        if (!$product || !$partner) {
            \App::abort(404);
        }

        // Check sku
        $sku = self::sanitizeText(Input::get('sku', ''));
        if(!empty($sku)){
            $count = ProductPartnerReward::where('partner_id', $partner_id)->where('sku', $sku)->count();
            if($count > 0){
                return $this->listRewardPartners($enc_id, 'SKU already used');
            }
        }

        $partCount = ProductPartnerReward::where('product_id', $id)->where('partner_id', $partner_id)->count();
        if($partCount > 0){
            return $this->listRewardPartners($enc_id, 'This Partner is already linked to the product.');
        }
        
        $rewardProduct = new ProductPartnerReward();
        if (!$product->ProductPartnerReward->contains($partner)) {
            $rewardProduct->product_id = $id;
            $rewardProduct->partner_id = $partner_id;
            $rewardProduct->partner_mapping_id = $partnerMappingId;
            $rewardProduct->sku = $sku;
            $rewardProduct->save();
        }
        
        //Audit Log for link Network
        AuditHelper::record(Auth::User()->getTopLevelPartner()->id,Auth::User()->id . " linked the product reward with ID = " .  $id  ." to the partner with ID = ". $partner_id . " at " . date('Y-m-d H:i:s'));
        
        return $this->listRewardPartners($enc_id);
    }

    /**
     * unlink a partner froma product
     *
     * @param int $product id
     * @return void
     */
    public function getUnlink_reward_partner($enc_product_id, $_enc_partner_id)
    {
        $reward_partner_id = base64_decode($_enc_partner_id);
        $reward_product_id = base64_decode($enc_product_id);

        $product = Product::find($reward_product_id);
        $partner = ProductPartnerReward::where('id', $reward_partner_id)->first();

        if (!$product || !$partner) {
            \App::abort(404);
        }

        ProductPartnerReward::where('id', $reward_partner_id)->delete();
        
        //Audit Log for link Network
        AuditHelper::record(Auth::User()->getTopLevelPartner()->id,Auth::User()->id . " linked the product reward with ID = " .  base64_decode($enc_product_id)  ." to the partner with ID = ". $reward_partner_id . " at " . date('Y-m-d H:i:s'));

        return $this->listRewardPartners($enc_product_id);
    }

    /**
     * Ediit a partner froma product
     *
     * @param int $enc_partnerRewardId id
     * @return void
     */
    public function getEdit_reward_partner($enc_partnerRewardId = null) {
        $partnerRewardId = base64_decode($enc_partnerRewardId);
        $data = array(
            'productPartnerReward' => ProductPartnerReward::find($partnerRewardId),
            'partners' => ManagedObjectsHelper::managedPartners()->sortBy('name')->pluck('name', 'id'),
            'prefixes' => ManagedObjectsHelper::managedPartners()->sortBy('prefix')->pluck('prefix', 'prefix')
        );

        return View::make("productpricing.dialog.edit_reward_partners", $data
        );
    }

    /**
     * link a partner to a product
     *
     * @param int $product id
     * @return void
     */
    public function postUpdate_reward_partner($enc_id, $enc_product_id) {
        $id = base64_decode($enc_id);

        $partner_id = self::sanitizeText(Input::get('partner_id'));

        $prefix = self::sanitizeText(Input::get('partner_prefix', ""));
        $mapping_id = self::sanitizeText(Input::get('partner_mapping_id', ""));
        $partnerMappingId = $prefix . $mapping_id;
        
        // Check sku
        $sku = self::sanitizeText(Input::get('sku', ''));
        if(!empty($sku)){
            $count = ProductPartnerReward::where('id', '!=', $id)->where('partner_id', $partner_id)->where('sku', $sku)->count();
            if($count > 0){
               return $this->listRewardPartners($enc_id, 'SKU already used'); 
            }
        }
        
        $rewardProduct = ProductPartnerReward::find($id);

        $rewardProduct->partner_id = $partner_id;
        $rewardProduct->partner_mapping_id = $partnerMappingId;
        $rewardProduct->sku = self::sanitizeText(Input::get('sku', ''));

        $rewardProduct->save();
        
        //Audit Log for link Network
        AuditHelper::record(Auth::User()->getTopLevelPartner()->id,Auth::User()->id . " Updated the product reward with ID = " .  $rewardProduct->id  . " at " . date('Y-m-d H:i:s'));
        
        
        return $this->listRewardPartners($enc_product_id);
    }

    /**
     * View a single Product redemption partner entry
     *
     * @param int $id
     * @return void
     */
    public function getViewProductRedemptionPartner($enc_id = null) {
        $id = base64_decode($enc_id);

        if (!I::can('view_products')) {
            return Redirect::to(url("dashboard"));
        }
        $productRedemption = ProductPartnerRedemption::find($id);
        $product = Product::find($productRedemption->product_id);

        if (!$product) {
            App::abort(404);
        }
        //get segments
        $channels = array(-1);
        if($productRedemption->channels()->count() > 0){
            foreach($productRedemption->channels as $c){
                $channels[] = $c->partner_id;
            }
        }
        $segments = Segment::whereIn('partner_id', $channels)->orderBy('name', 'ASC')->pluck('name', 'id');

        return View::make("productpricing.view_redemption_partner", array_merge(
                                $this->getAllLists(), array('productRedemption' => $productRedemption, 'product' => $product, 'segments' => $segments)
        ));
    }

    /**
     * Update a product
     *
     * @param int $id
     * @return void
     */
    public function saveProductRedemption($enc_id) {
        $id = base64_decode($enc_id);

        if (!I::can('edit_products')) {
            return Redirect::to(url("dashboard"));
        }
        $productRedemption = ProductPartnerRedemption::find($id);

        if (!$productRedemption) {
            App::abort(404);
        }

        $validator = Validator::make(Input::all(), $this->getValidationsRedemption());

        if ($validator->fails()) {
            return Redirect::to(url("dashboard/productpricing/view_redemption_partner/{$enc_id}"))->withErrors($validator)->withInput();
        }

        $input = Input::all();

        foreach($input as $k=>$i){
            $input[$k] = self::sanitizeText($i);
        }

        foreach ($input as $k => $v) {
            if ($v == '') {
                unset($input[$k]);
            }
        }

        // Display Control
        if (!Input::has('display_online')) {
            $input['display_online'] = false;
        }

        // Display offline
        if (!Input::has('show_offline')) {
            $input['show_offline'] = false;
        }

        if (!Input::has('hot_deal')) {
            $input['hot_deal'] = false;
        }

        $input['product_link'] = self::sanitizeText(Input::get('product_link', 0));
        
        // original prices
        $input['currency_id'] = self::sanitizeText(Input::get('currency_id', 0));
        $input['original_price'] = floatval(self::sanitizeText(Input::get('original_price', 0)));
        $input['original_retail_price'] = floatval(self::sanitizeText(Input::get('original_retail_price', 0)));

        //calculate prince in usd and points
        $currency = Currency::where('id', $input['currency_id'])->first();
        $input['price_in_usd'] = $input['original_price'] / $currency->latestRate();
        $input['retail_price_in_usd'] = $input['original_retail_price'] / $currency->latestRate();

        // Convert the points
        $input['price_in_points'] = floatval($input['price_in_usd']) / Config::get('blu.points_rate');
        $input['redemption_limit'] = self::sanitizeText(Input::get('redemption_limit', 0));

        if (!empty($input['start_date']) && !empty($input['end_date'])) {
            if(strtotime(self::sanitizeText(Input::get('end_date'))) < strtotime(self::sanitizeText(Input::get('start_date')))){
                return Redirect::to(url("dashboard/catalogue/view/{$enc_id}"))->withErrors(array('The end date must be a date after or equal start date.'))->withInput();
            }
            $input['start_date'] = date('Y-m-d', strtotime($input['start_date']));
            $input['end_date'] = date('Y-m-d', strtotime($input['end_date']));
        } else {
            $input['start_date'] = null;
            $input['end_date'] = null;
        }

        $productRedemption->update($input);

        // delete product from cart
        if ($productRedemption->qty == 0 || $productRedemption->display_online == 0) {
            DB::table('cart')->where('product_id', $productRedemption->product_id)->delete();
        }
        
        //Audit Log for link Network
        AuditHelper::record(Auth::User()->getTopLevelPartner()->id,Auth::User()->id . " Updated the product redemption with ID = " .  $productRedemption->id  . " at " . date('Y-m-d H:i:s'));
        $enc_product_id = base64_encode($productRedemption->product_id);
        return Redirect::to(url("dashboard/productpricing/view/{$enc_product_id}"));
    }

    /**
     * Create a small LI list with product partners
     *
     * @param int $user_id
     * @return Response
     */
    private function listUPCs($enc_product_id, $error = null)
    {
        $product_id = base64_decode($enc_product_id);
        $product = Product::find($product_id);

        return View::make('productpricing.list.upcList', array(
            'product' => $product,
            'error' => $error,
        ));
    }

    /**
     * link a partner to a product
     *
     * @param int $product id
     * @return void
     */
    public function postLink_upc($enc_product_id)
    {
        $product_id = base64_decode($enc_product_id);
        $upc = self::sanitizeText(Input::get('upc'));

        $product = Product::find($product_id);
        if (!$product || !$upc || $upc == '') {
            \App::abort(404);
        }

        $count = DB::table('product_upc')->where('upc', $upc)->count();
        if($count > 0) {
           return $this->listUPCs($enc_product_id, 'UPC already in use for other product.');
        }
        
        DB::table('product_upc')->insert(array(
            'product_id' => $product_id,
            'upc' => $upc
        ));

         //Audit Log for link Network
        AuditHelper::record(Auth::User()->getTopLevelPartner()->id,Auth::User()->id . " added the UPC = ". $upc ." to the product redemption with ID = " .  $product_id  . " at " . date('Y-m-d H:i:s'));

        return $this->listUPCs($enc_product_id);
    }

    /**
     * unlink a partner froma product
     *
     * @param int $product id
     * @return void
     */
    public function getUnlink_upc($enc_product_id, $enc_id)
    {
        $id = base64_decode($enc_id);
        $upc = ProductUpc::where('id', $id)->first();
        $product = Product::find(base64_decode($enc_product_id));
        if (!$product || !$upc) {
            \App::abort(404);
        }

         //Audit Log for link Network
        AuditHelper::record(Auth::User()->getTopLevelPartner()->id,Auth::User()->id . " deleted the UPC = ". $upc->upc ." from the product redemption with ID = " .  base64_decode($enc_product_id)  . " at " . date('Y-m-d H:i:s'));
        
        ProductUpc::where('id', $id)->delete();
        
        return $this->listUPCs($enc_product_id);
    }

    /**
     * Ediit a partner froma product
     *
     * @param int $enc_partnerRewardId id
     * @return void
     */
    public function getEdit_upc($enc_id = null)
    {
        $id = base64_decode($enc_id);
        $product = ProductUpc::find($id);
        if (!$product) {
            \App::abort(404);
        }

        $data = array(
            'productUpc' => ProductUpc::find($id),
        );
        
        return View::make("productpricing.dialog.edit_upc", $data);
    }

    /**
     * update upc 
     *
     * @param int $product id
     * @return void
     */
    public function postUpdate_upc($enc_id, $enc_product_id)
    {
        $id = base64_decode($enc_id);

        $newUPC = self::sanitizeText(Input::get('upc'));
        $oldUPC = ProductUpc::find($id);
        $product = Product::find(base64_decode($enc_product_id));

        if (!$oldUPC || !$product) {
            \App::abort(404);
        }

        DB::table('product_upc')->where('id', $id)->update(array(
            'upc' => $newUPC
        ));
        
        //Audit Log for link Network
        AuditHelper::record(Auth::User()->getTopLevelPartner()->id,Auth::User()->id . " upc the UPC from". $oldUPC->upc ." to ".$newUPC."from the product redemption with ID = " .  base64_decode($enc_product_id)  . " at " . date('Y-m-d H:i:s'));
        
        return $this->listUPCs($enc_product_id);
    }
    
    /**
     * Edit product country details
     *
     * @param int $enc_productCountryId
     * @return void
     */
    public function getEdit_country($enc_id = null) {
        $id = base64_decode($enc_id);
        $data = array(
            'productCountryDetails' => ProductRedemptionCountries::find($id),
            'countries' => Country::orderBy('name')->pluck('name', 'id'),
            'delivery_options' => Meta::deliveryOptions(),
            'currencies' => Currency::where('draft', false)->orderBy('name')->pluck('name', 'id'),
        );
        
        return View::make("productpricing.dialog.edit_country", $data
        );
    }
    
    /**
     * update country details
     *
     * @param int $product id
     * @return void
     */
    public function postUpdate_country($enc_id, $enc_product_id) {
        $id = base64_decode($enc_id);

        $country_id = self::sanitizeText(Input::get('country_id'));
        $custom_tarrif_taxes = self::sanitizeText(Input::get('custom_tarrif_taxes'));
        $delivery_charges = self::sanitizeText(Input::get('delivery_charges'));
        $currency_id = self::sanitizeText(Input::get('currency_id'));
        $delivery_options = self::sanitizeText(Input::get('delivery_options'));
        $pickup_address = self::sanitizeText(Input::get('pickup_address'));
        $status = self::sanitizeText(Input::get('status'));

        DB::table('product_redemption_countries')->where('id', $id)->update(array(
            'country_id' => $country_id,
            'custom_tarrif_taxes' => $custom_tarrif_taxes,
            'delivery_charges' => $delivery_charges,
            'currency_id' => $currency_id,
            'delivery_options' => $delivery_options,
            'pickup_address' => $pickup_address,
            'status' => $status
        ));
        
        //Audit Log for link Network
        AuditHelper::record(Auth::User()->getTopLevelPartner()->id,Auth::User()->id . " upc the country details of the product redemption countries with ID = " .  $id  . " at " . date('Y-m-d H:i:s'));
        
        return $this->listCountries($enc_product_id);
    }
    
    /**
     * update segment drop down
     *
     * @param int $product id
     * @return void
     */
    public function getSegmentsByChannels($enc_id) {
        $id = base64_decode($enc_id);
        $productRedemption = ProductPartnerRedemption::find($id);

        if (!$productRedemption) {
            App::abort(404);
        }

        //get segments
        $channels = array(-1);
        if($productRedemption->channels()->count() > 0){
            foreach($productRedemption->channels as $c){
                $channels[] = $c->partner_id;
            }
        }
        $segments = Segment::whereIn('partner_id', $channels)->orderBy('name', 'ASC')->pluck('name', 'id');
        
        return $segments;
    }
    
    /**
     * update segment drop down
     *
     * @param int $product id
     * @return void
     */
    public function getPrefixByPartner($enc_id) {
        $id = base64_decode($enc_id);
        
        $prefixes = Partner::where('id', $id)->orderBy('prefix')->pluck('prefix', 'prefix');
        
        return $prefixes;
    }

}

// EOC