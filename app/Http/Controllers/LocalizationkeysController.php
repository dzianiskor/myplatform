<?php
namespace App\Http\Controllers;
/**
 * Localizationkey Controller
 * 
 * @category   Controllers
 * @package    BLU
 * @author     Ralph Nader <ralph@bluloyalty.com>
 * @copyright  2015 BLU
 * @link       http://blupoints.com
 */
class LocalizationkeyController extends BaseController
{
    protected $layout = "layouts.dashboard";

    /**
     * generate a reusable array with all the validations needed
     * for the Localizationkeys screen
     *
     * @return array
     */
    private function getValidations() {
        $rules = array(
            'key'           => 'required|min:2'
        );

        return $rules;
    }

    /**
     * Display a list of the localizationkeys
     *
     * @return void
     */
    public function listLocalizationkeys()
    {
        $this->ormClass      = 'App\Localizationkey';
        $this->search_fields = array('key');

        return View::make("localizationkey.list", array(
            'localizationkeys' => $this->getList()
        ));

    }

    /**
     * get fields for new Localizationkey
     *
     * @return void
     */
    public function newLocalizationkey()
    {
        return View::make("localizationkey.new");
    }

    /**
     * get fields for new Localizationkey
     *
     * @return void
     */
    public function createNewLocalizationkey()
    {
        $input = Input::all();
        foreach($input as $k=>$i){
            $input[$k] = self::sanitizeText($i);
        }
        $validator = Validator::make($input, $this->getValidations());

        if($validator->fails()) {
            return Redirect::to('dashboard/localizationkeys/new')->withErrors($validator)->withInput();
        }
        $localizationkey = new Localizationkey();

        $localizationkey->key = self::sanitizeText(Input::get('key'));
        $localizationkey->description = self::sanitizeText(Input::get('description'));
        $localizationkey->save();

        //Localizationkey::create(Input::all());
        return Redirect::to('dashboard/localizationkeys');
    }

    /**
     * View a single Localizationkey entry
     *
     * @param int $id
     * @return void
     */
    public function viewLocalizationkey($id = null)
    {
        $localizationkey = Localizationkey::find($id);

        if(!$localizationkey) {
            App::abort(404);
        }

        return View::make("localizationkey.view", array(
            'localizationkey' => $localizationkey
        ));
    }

    /**
     * Delete a Localizationkey instance
     *
     * @param int $id
     * @return void
     */
    public function deleteLocalizationkey($id = null)
    {
        $localizationkey = Localizationkey::find($id);

        if(!$localizationkey) {
            App::abort(404);
        }

        $localizationkey->delete();

        return Redirect::to('dashboard/localizationkey');
    }

    /**
     * update a Localizationkey instance
     *
     * @param int $id
     * @return void
     */
    public function updateLocalizationkey($id = null)
    {
        return View::make("localizationkey.update", array(
            'id' => $id
        ));
    }

    /**
     * update a Localizationkey instance
     *
     * @param int $id
     * @return void
     */
    public function saveLocalizationkey($id = null)
    {
        $localizationkey = Localizationkey::find($id);

        if(!$localizationkey) {
            App::abort(404);
        }
        $input = Input::all();
        foreach($input as $k=>$i){
            $input[$k] = self::sanitizeText($i);
        }
        $validator = Validator::make($input, $this->getValidations());

        if($validator->fails()) {
            return Redirect::to('dashboard/localizationkeys/new')->withErrors($validator)->withInput();
        }

        
        //$localizationkey = Localizationkey::find($id);
        $localizationkey->key = self::sanitizeText(Input::get('key'));
        $localizationkey->description = self::sanitizeText(Input::get('description'));
        $localizationkey->save();
        
        return Redirect::to('dashboard/localizationkeys');
    }

} // EOC