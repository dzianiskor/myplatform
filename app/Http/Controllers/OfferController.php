<?php
namespace App\Http\Controllers;
use App\Media;
use Illuminate\Support\Facades\App;
use View;
use Input;
use Validator;
use App\Tiercriteria;
use I;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Offer as Offer;
use App\Category as Category;
use App\BluCollection as BluCollection;
use App\Tier as Tier;
use App\Partner as Partner;
use App\Segment as Segment;
use ManagedObjectsHelper;
use App\Country;
use Meta;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Offer Controller
 *
 * @category   Controllers
 * @package    BLU
 * @author     Magid Mroueh <magid.mroueh@bluloyalty.com>
 * @copyright  2016 BLU
 * @link       http://blupoints.com
 */
class OfferController extends BaseController {

    protected $layout = "layouts.dashboard";

    /**
     * Get all list data
     *
     * @return array
     */
    private function getAllLists()
    {
        $partners = Auth::User()->managedPartnerIDs();
        $arr_tiers = [];
        $tierNames = [];

        $tiers1 = Tier::where('draft', false)->whereHas('Partner', function($q) use($partners) {
            $q->whereIn('partner_id', $partners);
                })->orderBy('name')->get(['id', 'name']);

        if ($tiers1) {
            foreach ($tiers1 as $k => $v):
                array_push($arr_tiers, $v['id']);
                $tierNames[$v['id']] = $v['name'];
            endforeach;
        }
        
        $tiers = Tiercriteria::where('draft', false)->whereHas('tier', function($q) use($arr_tiers) {
                    $q->whereIn('tier_id', $arr_tiers);
                })->orderBy('name')->get(['name', 'id', 'tier_id']);


        $tierCriteria = [];
        foreach ($tiers as $key => $value):
            $tierCriteria[$value['id']] = $value['name'] . " ( " . $tierNames[$value['tier_id']] . " )";
        endforeach;

        return array(
            'categories' => Category::with('children')->where('parent_category_id', 1)->where('draft', false)->orderBy('name', 'asc')->pluck('name', 'id'),
            'partners' => ManagedObjectsHelper::managedPartners()->sortBy('name')->pluck('name', 'id'),
            'countries' => Country::orderBy('name')->pluck('name', 'id'),
            'segments' => ManagedObjectsHelper::managedSegments()->sortBy('name')->pluck('name', 'id'),
            'tiers' => $tiers,
            'tiers2' => $tierCriteria,
            'statuses' => Meta::offerStatuses(),
            'itemTypes' => Meta::itemType()
        );
    }

    /**
     * Return the offer validations
     *
     * @return array
     */
    private function getValidations() {
        return array(
            'name' => 'required',
            'category_id' => 'required',
        );
    }

    /**
     * Display a list of the offers
     *
     * @return void
     */
    public function listOffers() {
        if (!I::can('view_offer')) {
            return Redirect::to(url("dashboard"));
        }

        $this->ormClass = 'App\Offer';
        $this->defaultSort = 'id';
        $this->search_fields = array('name');
        if (I::am('BLU Admin') || I::belong(1)) {
            $offers = $this->getList();

            $page = Input::get('page');
            $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
            $offers = new LengthAwarePaginator($offers->forPage($page, 15), $offers->count(), 15, $page);
        } else {
            $partner = Auth::User()->getTopLevelPartner()->id;
            $offer = Offer::select('offer.*')->where('draft', 0)->where('deleted',0);
            $offers = $offer->whereHas('partners', function($q) use($partner) {
                $q->where('partner_id', $partner);
            });

            if (Input::has('q')) {
                $q = self::sanitizeText(Input::get('q'));
                $offers = $offer->where('offer.name', 'like', '%' . $q . '%');
            }

            // filter partner_id
            if (Input::has('partner_list')) {
                $q = explode(',', Input::get('partner_list'));
                $offers = $offer->whereHas('Partners', function($u) use($q) {
                    $u->whereIn('partner_id', $q);
                });
            }
            
            // filter country_id
            if (Input::has('country_list')) {
                $q = explode(',', Input::get('country_list'));
                $offers = $offer->whereHas('countries', function($u) use($q) {
                    $u->whereIn('country_id', $q);
                });
            }
            
             // filter channels
            if (Input::has('offer_displaychannel_list')) {
                $q = explode(',', Input::get('offer_displaychannel_list'));
                $offers = $offer->whereHas('channels', function($u) use($q) {
                    $u->whereIn('channel_id', $q);
                });
            }

             // filter Status
            if (Input::has('offer_status')) {
                $q = explode(',', Input::get('offer_status'));
                $offers = $offer->whereIn('status', $q);
            }

            $offers = $offer->paginate(15);
        }

        return View::make("offer.list", array(
                    'offers' => $offers,
                    'lists' => $this->getAllLists()
        ));
    }

    /**
     * get fields for new offer
     *
     * @return void
     */
    public function newOffer() {
        if (!I::can('create_offer')) {
            return Redirect::to(url("dashboard"));
        }

        $offer = new Offer();

        $offer->name = '';
        $offer->start_date = NULL;
        $offer->end_date = NULL;
        $offer->draft = TRUE;
        $offer->status = 'Active';
        $offer->logged_in = 0;
        $offer->logged_out = 1;

        $offer->save();

        // Link to top level partner automtically

        $partner = Auth::User()->getTopLevelPartner();

        $offer->partners()->attach($partner->id);

        // Associate to partner location
        DB::table('country_offer')->insert(array(
            'country_id' => $partner->country->id,
            'offer_id' => $offer->id
        ));

        // Associate to partner as a channel
        DB::table('channel_offer')->insert(array(
            'offer_id' => $offer->id,
            'channel_id' => $partner->id
        ));
        $offer_id = base64_encode($offer->id);
        return Redirect::to(url("dashboard/offer/view/{$offer_id}"));
    }

    /**
     * View a single Offer entry
     *
     * @param int $id
     * @return void
     */
    public function viewOffer($enc_id = null) {
        $id = base64_decode($enc_id);
        if (!I::can('view_offer')) {
            return Redirect::to(url("dashboard"));
        }
        $offer = Offer::find($id);

        if (!$offer) {
            App::abort(404);
        }

        $managedPartners = Auth::User()->managedPartners();
        $can_view_offer = false;
        foreach ($managedPartners as $manP) {
            foreach ($offer->partners as $p) {
                //var_dump($p->id);
                if ($manP->id == $p->id) {
                    $can_view_offer = true;
                    break;
                }
            }
        }
        if ($can_view_offer == false && $offer->draft == false) {
            return Redirect::to(url("dashboard"));
        }
        return View::make("offer.view", array_merge(
                                $this->getAllLists(), array('offer' => $offer)
        ));
    }

    /**
     * Delete a offer instance
     *
     * @param int $id
     * @return void
     */
    public function deleteOffer($enc_id = null) {
        $id = base64_decode($enc_id);
        if (!I::can('delete_offer')) {
            return Redirect::to(url("dashboard"));
        }
        $offer = Offer::find($id);

        if (!$offer) {
            App::abort(404);
        }

//        $offer->delete();
        $offer->deleted = 1;
        $offer->deleted_at = date('Y-m-d H:i:s');
        $offer->save();

        return Redirect::to('dashboard/offer');
    }

    /**
     * Update a offer
     *
     * @param int $id
     * @return void
     */
    public function saveOffer($enc_id) {
        $id = base64_decode($enc_id);
        if (!I::can('edit_offer') && !I::can('create_offer')) {
            return Redirect::to(url("dashboard"));
        }
        $offer = Offer::find($id);

        
        if (!$offer) {
            App::abort(404);
        }
        $input = Input::all();
        unset($input['redirect_to']);
        foreach ($input as $k=>$v) {
            if ($k == 'reward_type' || $k == 'expiry_period') {
                unset($input[$k]);
            }
        }
        $validator = Validator::make($input, $this->getValidations());

        if ($validator->fails()) {
            return Redirect::to(url("dashboard/offer/view/{$enc_id}"))->withErrors($validator)->withInput();
        }

        foreach($input as $k=>$i){
            $input[$k] = self::sanitizeText($i);
        }



        // Check for cover image upload
        if (Input::hasFile('cover_image')) {
            $file = Input::file('cover_image');
            $path = public_path('uploads');
            $ext = strtolower($file->getClientOriginalExtension());
            $size = $file->getSize();
            $type = $file->getMimeType();
            $name = md5(time()) . ".{$ext}";

            if (in_array($ext, Meta::allowedImageExts())) {
                Input::file('cover_image')->move($path, $name);

                $f = new Media();
                $f->name = $name;
                $f->size = $size;
                $f->type = $type;
                $f->ext = $ext;
                $f->save();

                $input['cover_image'] = $f->id;
            }
        }

        foreach ($input as $k => $v) {
            if ($v == '') {
                unset($input[$k]);
            }
        }

        // Display Control
        if (!Input::has('logged_in')) {
            $input['logged_in'] = false;
        }
        if($input['logged_in'] == 1 && count($offer->segments) < 1){ //check if not selected segments
            return Redirect::to(url("dashboard/offer/view/$enc_id"))->withErrors(['you have to select at least one segment if the offer is for Logged In users.'])->withInput();
        }
        
        // Display offline
        if (!Input::has('logged_out')) {
            $input['logged_out'] = false;
        }
        
        // Date
        if (Input::has('validity_start_date')) {
            if(strtotime(self::sanitizeText(Input::get('validity_start_date'))) > strtotime(self::sanitizeText(Input::get('validity_end_date')))){ //check if not selected segments
                return Redirect::to(url("dashboard/offer/view/$enc_id"))->withErrors(['the valid from date should be earlier than the valid to.'])->withInput();
            }
            $input['validity_start_date'] = date('Y-m-d', strtotime(self::sanitizeText(Input::get('validity_start_date'))));
        }
        // Date
        if (Input::has('validity_end_date')) {
            if(strtotime(self::sanitizeText(Input::get('validity_start_date'))) > strtotime(self::sanitizeText(Input::get('validity_end_date')))){ //check if not selected segments
                return Redirect::to(url("dashboard/offer/view/$enc_id"))->withErrors(['the valid from date should be earlier than the valid to.'])->withInput();
            }
            $input['validity_end_date'] = date('Y-m-d', strtotime(self::sanitizeText(Input::get('validity_end_date'))));
        }
        // Date
        if (Input::has('start_date')) {
            if(strtotime(self::sanitizeText(Input::get('start_date'))) > strtotime(self::sanitizeText(Input::get('end_date')))){ //check if not selected segments
                return Redirect::to(url("dashboard/offer/view/$enc_id"))->withErrors(['the start date should be earlier than the end date.'])->withInput();
            }
            $input['start_date'] = date('Y-m-d', strtotime(self::sanitizeText(Input::get('start_date'))));
        } else {
            $input['start_date'] = NULL;
        }
        // Date
        if (Input::has('end_date')) {
            if(strtotime(self::sanitizeText(Input::get('start_date'))) > strtotime(self::sanitizeText(Input::get('end_date')))){ //check if not selected segments
                return Redirect::to(url("dashboard/offer/view/$enc_id"))->withErrors(['the valid from should be earlier than the end date.'])->withInput();
            }
            $input['end_date'] = date('Y-m-d', strtotime(self::sanitizeText(Input::get('end_date'))));
        } else {
            $input['end_date'] = NULL;
        }
        $offer->draft = false;
        $offer->update($input);

        return Redirect::to(Input::get('redirect_to', 'dashboard/offer'));
    }

    /**
     * Create a small LI list with offer partners
     *
     * @param string $enc_offer_id
     * @return Response
     */
    private function listPartners($enc_offer_id)
    {
        $offer_id = base64_decode($enc_offer_id);
        $offer = Offer::find($offer_id);
        return View::make('offer.list.partnerList', array(
            'offer' => $offer,
            'partners' => ManagedObjectsHelper::managedPartners()->sortBy('name')->pluck('name', 'id'),
        ));
    }

    /**
     * link a partner to a offer
     *
     * @param int $offer id
     * @return void
     */
    public function postLink_partner($enc_id) {
        $id = base64_decode($enc_id);
        $partner_id = self::sanitizeText(Input::get('partner_id'));
        $offer = Offer::find($id);
        $partner = Partner::find($partner_id);

        if(!$partner || !$offer) {
            App::abort(404);
        }

        if (!$offer->partners->contains($partner)) {
            $offer->partners()->attach($partner);
        }
        return $this->listPartners($enc_id);
    }

    /**
     * unlink a partner from offer
     *
     * @param int $offer id
     * @return void
     */
    public function getUnlink_partner($enc_offer_id, $enc_partner_id) {
        $offer_id = base64_decode($enc_offer_id);
        $partner_id = base64_decode($enc_partner_id);
        $offer = Offer::find($offer_id);
        $partner = Partner::find($partner_id);

        if(!$partner || !$offer) {
            App::abort(404);
        }

        $offer->partners()->detach($partner_id);
        return $this->listPartners($enc_offer_id);
    }

    /**
     * Create a small LI list with offer segments
     *
     * @param string $enc_offer_id
     * @return Response
     */
    private function listSegments($enc_offer_id)
    {
        $offer_id = base64_decode($enc_offer_id);
        $offer = Offer::find($offer_id);
        return View::make('offer.list.segmentList', array(
            'offer' => $offer,
            'segments' => ManagedObjectsHelper::managedSegments()->sortBy('name')->pluck('name', 'id'),
        ));
    }

    /**
     * link a segment to a offer
     *
     * @param int $offer id
     * @return void
     */
    public function postLink_segment($enc_id) {
        $id = base64_decode($enc_id);
        $segment_id = self::sanitizeText(Input::get('segment_id'));
        $offer = Offer::find($id);
        $segment = Segment::find($segment_id);

        if(!$segment || !$offer) {
            App::abort(404);
        }

        if (!$offer->segments->contains($segment)) {
            $offer->segments()->attach($segment);
        }
        return $this->listSegments($enc_id);
    }

    /**
     * unlink a segment from a offer
     *
     * @param int $offer id, $segment_id
     * @return void
     */
    public function getUnlink_segment($enc_offer_id, $enc_segment_id) {
        $offer_id = base64_decode($enc_offer_id);
        $segment_id = base64_decode($enc_segment_id);
        $partner_id = self::sanitizeText(Input::get('partner_id'));
        $offer = Offer::find($offer_id);
        $segment = Segment::find($segment_id);

        if(!$segment || !$offer) {
            App::abort(404);
        }

        $offer->segments()->detach($segment_id);
        return $this->listSegments($enc_offer_id);
    }

    /**
     * Create a small LI list with offer countries
     *
     * @param int $offer_id
     * @return Response
     */
    private function listCountries($enc_offer_id) {
        $offer_id = base64_decode($enc_offer_id);
        $offer = Offer::find($offer_id);

        return View::make('offer.list.countryList', array(
                    'offer' => $offer,
        ));
    }

    /**
     * link a country to a offer
     *
     * @param int $offer id
     * @return void
     */
    public function postLink_country($enc_id) {
        $id = base64_decode($enc_id);
        $country_id = self::sanitizeText(Input::get('country_id'));
        $offer = Offer::find($id);
        $country = Country::find($country_id);

        if(!$country || !$offer) {
            App::abort(404);
        }

        if (!$offer->countries->contains($country)) {
            $offer->countries()->attach($country_id);
        }
        return $this->listCountries($enc_id);
    }

    /**
     * unlink a country from a offer
     *
     * @param int $offer id, $country_id
     * @return void
     */
    public function getUnlink_country($enc_offer_id, $enc_country_id) {
        $offer_id = base64_decode($enc_offer_id);
        $country_id = base64_decode($enc_country_id);
        $offer = Offer::find($offer_id);
        $country = Country::find($country_id);

        if(!$country || !$offer) {
            App::abort(404);
        }

        $offer->countries()->detach($country_id);
        return $this->listCountries($enc_offer_id);
    }

    /**
     * Load a list of channels associated to the specified offer
     *
     * @param string $enc_offer_id
     * @return View
     */
    private function listChannels($enc_offer_id)
    {
        $offer_id = base64_decode($enc_offer_id);
        $offer = Offer::find($offer_id);

        return View::make('offer.list.channelList', array(
            'offer' => $offer,
            'partners' => ManagedObjectsHelper::managedPartners()->sortBy('name')->pluck('name', 'id'),
        ));
    }

    /**
     * Link the offer to a partner channel
     *
     * @param int $offer
     * @return View
     */
    public function postLink_channel($enc_offer_id) {
        $offer_id = base64_decode($enc_offer_id);
        $partner_id = self::sanitizeText(Input::get('partner_id'));
        $offer = Offer::find($offer_id);
        $partner = Partner::find($partner_id);

        if(!$partner || !$offer) {
            App::abort(404);
        }

        DB::table('channel_offer')->insert(array(
            'offer_id' => $offer_id,
            'channel_id' => $partner_id
        ));

        return $this->listChannels($enc_offer_id);
    }

    /**
     * unlink a country from a offer
     *
     * @param int $offer id
     * @param int $partner_id
     * @return View
     */
    public function getUnlink_channel($enc_offer_id, $enc_partner_id) {
        $offer_id = base64_decode($enc_offer_id);
        $partner_id = base64_decode($enc_partner_id);
        $offer = Offer::find($offer_id);
        $partner = Partner::find($partner_id);

        if(!$partner || !$offer) {
            App::abort(404);
        }

        DB::table('channel_offer')->where('offer_id', $offer_id)
                ->where('channel_id', $partner_id)
                ->delete();

        return $this->listChannels($enc_offer_id);
    }

    /**
     * Load a list of Tiers associated to the specified offer
     *
     * @param int $offer_id
     * @return View
     */
    private function listTiers($enc_offer_id) {
        $offer_id = base64_decode($enc_offer_id);
        $offer = Offer::find($offer_id);

        return View::make('offer.list.tierList', array(
                    'offer' => $offer,
        ));
    }

    /**
     * Link the offer to a partner tier
     *
     * @param int $offer
     * @return View
     */
    public function postLink_tier($enc_offer_id) {
        $offer_id = base64_decode($enc_offer_id);
        $tier_id = self::sanitizeText(Input::get('tier_id'));
        $offer = Offer::find($offer_id);
        $tier = Tier::find($tier_id);

        if(!$tier || !$offer) {
            App::abort(404);
        }

        DB::table('tier_offer')->insert(array(
            'offer_id' => $offer_id,
            'tier_id' => $tier_id
        ));

        return $this->listTiers($enc_offer_id);
    }

    /**
     * unlink a Tier from a offer
     *
     * @param int $offer id
     * @param int $tier_id
     * @return View
     */
    public function getUnlink_tier($enc_offer_id, $enc_tier_id) {
        $offer_id = base64_decode($enc_offer_id);
        $tier_id = base64_decode($enc_tier_id);
        $offer = Offer::find($offer_id);
        $tier = Tier::find($tier_id);

        if(!$tier || !$offer) {
            App::abort(404);
        }

        DB::table('tier_offer')->where('offer_id', $offer_id)
                ->where('tier_id', $tier_id)
                ->delete();

        return $this->listTiers($enc_offer_id);
    }

}

// EOC