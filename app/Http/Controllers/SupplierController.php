<?php
namespace App\Http\Controllers;
use App\Media;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Response;
use View;
use Input;
use Validator;
use Session;
use Security;
use I;
use Illuminate\Support\Facades\Auth;
use BluCollection;
use AuditHelper;
use Illuminate\Support\Facades\Redirect;
use Transaction;
use Partner;
use Notification;
use Ticket;
use Language;
use Localizationkey;
use LangKey;
use ManagedObjectsHelper;
use Meta;
use Illuminate\Support\Facades\DB;
use App\Supplier as Supplier;
use App\PartnerCategory as PartnerCategory;
use App\Network as Network;
use UCID;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Supplier Controller
 * 
 * @category   Controllers
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class SupplierController extends BaseController
{
    protected $layout = "layouts.dashboard";
    private $auth_user = null;

    public function __construct()
    {
        parent::__construct();

        $this->auth_user = Auth::User();
    }       

    /**
     * Fetch the data for the supplier view
     *
     * @return array
     */
    private function getData()
    {
        return [
            'categories' => PartnerCategory::where('draft', false)->orderBy('name')->pluck('name', 'id'),
            'suppliers'  => Supplier::where('draft',false)->orderBy('name')->pluck('name','id'),
            'networks' => ManagedObjectsHelper::managedNetworks()->sortBy('name')->pluck('name', 'id'),
            'partners'   => ManagedObjectsHelper::managedPartners()->sortBy('name')->pluck('name', 'id'),
            'countries' => \App\Country::orderBy('name')->pluck('name', 'id'),
        ];
    }

    /**
     * Show a list of suppliers
     *
     * @return void
     */
    public function listSuppliers()
    {
        if(!I::can('view_suppliers')){
            return Redirect::to(url("dashboard"));
        }
        $this->ormClass      = 'App\Supplier';
        $this->search_fields = array('name', 'ucid');

        if (I::am('BLU Admin')) {
            $suppliers = $this->getList();

            $page = Input::get('page');
            $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
            $suppliers = new LengthAwarePaginator($suppliers->forPage($page, 15), $suppliers->count(), 15, $page);
        } else {
            $partner  = Auth::User()->getTopLevelPartner()->id;
            $supplier = Supplier::select('supplier.*');
            $suppliers = $supplier->whereHas('Partners', function($q) use($partner){
                $q->where('partner_id', $partner);
            });


            if(Input::has('q')) {
                $suppliers = $supplier->where('supplier.name', 'like', '%'.self::sanitizeText(Input::get('q')).'%');
            }

            $suppliers = $supplier->paginate(15);
        }

        return View::make("supplier.list", [
            'suppliers' => $suppliers,
            'lists' => $this->getData(),
        ]);
    }

    /**
     * Create a new partner draft and redirect to it
     *
     * @return void
     */
    public function newSupplier()
    {
        if(!I::can('view_suppliers')){
            return Redirect::to(url("dashboard"));
        }
        $supplier             = new Supplier();
        $supplier->ucid       = UCID::newCustomerID();
        $supplier->draft      = true;
        $supplier->country_id = 9;
        $supplier->area_id    = 76;
        $supplier->city_id    = 672;
        $supplier->save();
        
        $supplierId = base64_encode($supplier->id);
        return Redirect::to(url("dashboard/suppliers/view/{$supplierId}"));
    }

    /**
     * View a partner resource
     *
     * @param int $id
     * @return void
     */
    public function viewSupplier($enc_id = null)
    {
        $id = base64_decode($enc_id);
        if (!I::can('view_suppliers') && !I::can('edit_suppliers')) {
            return Redirect::to(url("dashboard"));
        }
        $supplier = Supplier::find($id);

        if(!$supplier) {
            \App::abort(404);
        }

        $managedPartners = Auth::User()->managedPartners();
        $can_view_supplier = false;
        foreach ($managedPartners as $manP) {
            foreach ($supplier->partners as $p) {
                //var_dump($p->id);
                if($manP->id == $p->id){
                    $can_view_supplier = true;
                    break;
                }
            }
        }
        if($can_view_supplier== false && $supplier->draft == false){
            return Redirect::to(url("dashboard"));
        }
        
        $this->ormClass      = 'App\Supplier';
        $data = array(
            'supplier'  => $supplier,
            'statuses' => Meta::supplierStatuses()
        );

        return View::make("supplier.view", array_merge($data, $this->getData($id)));
    }

    /**
     * Delete a partner resource
     *
     * @param int $id
     * @return void
     */
    public function deleteSupplier($enc_id = null)
    {
        $id = base64_decode($enc_id);
        if(!I::can('delete_suppliers')){
            return Redirect::to(url("dashboard"));
        }

        $supplier = Supplier::find($id);

        if(!$supplier) {
            App::abort(404);
        }

        DB::table('supplier')->where('id', $id)->delete();

        return Redirect::to('dashboard/suppliers');
    }

    /**
     * Update a partner resource
     *
     * @param int $id
     * @return void
     */
    public function updateSupplier($enc_id = null)
    {
        $id = base64_decode($enc_id);
        if (!I::can('create_suppliers') && !I::can('edit_suppliers')) {
            return Redirect::to(url("dashboard"));
        }
        $supplier = Supplier::find($id);

        if(!$supplier) {
            App::abort(404);
        }

        $input = Input::all();
        foreach ($input as $k => $i) {
            $input[$k] = self::sanitizeText($i);
        }
        $validator = Validator::make($input, array(
            'name' => 'required|min:2',
            'contact_email' => 'nullable|email',
            'category_id' => 'required|exists:partnercategory,id'
        ));

        if($validator->fails()) {
            return Redirect::to(url("dashboard/suppliers/view/" . base64_encode($id)))->withErrors($validator);
        }

        $supplier->name              = self::sanitizeText(Input::get('name'));
        $supplier->description       = self::sanitizeText(Input::get('description'));
        $supplier->contact_name      = self::sanitizeText(Input::get('contact_name'));
        $supplier->contact_email     = self::sanitizeText(Input::get('contact_email'));
        $supplier->contact_number    = self::sanitizeText(Input::get('contact_number'));
        $supplier->email             = self::sanitizeText(Input::get('email'));
        $supplier->website           = self::sanitizeText(Input::get('website'));
        $supplier->phone_number      = self::sanitizeText(Input::get('phone_number'));
        $supplier->status            = self::sanitizeText(Input::get('status'));
        $supplier->parent_id         = self::sanitizeText(Input::get('parent_id'));
        $supplier->category_id       = self::sanitizeText(Input::get('category_id'));
        $supplier->country_id          = self::sanitizeText(Input::get('country_id'));      
        $supplier->area_id             = self::sanitizeText(Input::get('area_id'));      
        $supplier->city_id             = self::sanitizeText(Input::get('city_id'));    
        $supplier->address_1           = self::sanitizeText(Input::get('address_1'));
        $supplier->address_2           = self::sanitizeText(Input::get('address_2'));
        $supplier->draft               = false;          
        $supplier->partner_id          = self::sanitizeText(Input::get('partner_id'));
        

        // Upload partner image (if present)
        if(Input::hasFile('image')) {
            $file = Input::file('image');
            $path = public_path('uploads');
            $ext  = strtolower($file->getClientOriginalExtension());
            $size = $file->getSize();
            $type = $file->getMimeType();
            $name = md5(time()).".{$ext}";

            if(in_array($ext, Meta::allowedImageExts() )) {
                Input::file('image')->move($path, $name);

                $f = new Media();
                $f->name = $name;
                $f->size = $size;
                $f->type = $type;
                $f->ext  = $ext;
                $f->save();

                $supplier->image = $f->id;
            }
        }

        $supplier->save();


        return Redirect::to('dashboard/suppliers')->with('success', 'Supplier Updated');
    }
    
    /**
     * Create a small LI list with supplier partners
     *
     * @param int $supplier_id
     * @return Response
     */
    private function listPartners($enc_supplier_id)
    {
        $supplier_id = base64_decode($enc_supplier_id);
        $supplier = Supplier::find($supplier_id);
        return View::make('supplier.list.partnerList', array(
            'supplier'  => $supplier
        ));
    }

    /**
     * link a partner to a supplier
     *
     * @param int $supplier_id
     * @return void
     */
    public function postLink_partner($enc_supplier_id)
    {
        $supplier_id = base64_decode($enc_supplier_id);
        $partner_id = self::sanitizeText(Input::get('partner_id'));
        $supplier = Supplier::find($supplier_id);
        $partner = Partner::find($partner_id);

        if (!$supplier->partners->contains($partner)) {
            $supplier->partners()->attach($partner);
        }
        return $this->listPartners($enc_supplier_id);
    }

    /**
     * unlink a partner froma supplier
     *
     * @param int $supplier_id
     * @return void
     */
    public function getUnlink_partner($enc_supplier_id, $enc_partner_id)
    {
        $supplier_id = base64_decode($enc_supplier_id);
        $partner_id = base64_decode($enc_partner_id);
        $supplier = Supplier::find($supplier_id);

        $supplier->partners()->detach($partner_id);
        return $this->listPartners($enc_supplier_id);
    }
    
    /**
     * Link the provided supplier to a network
     * 
     * @param
     * @return void
     */
    public function attachToNetwork($enc_supplier_id = null)
    {
        $supplier_id = base64_decode($enc_supplier_id);
        $supplier = Supplier::find($supplier_id);
        $network  = Network::find(self::sanitizeText(Input::get('network_id')));

        if(!$supplier || !$network) {
            App::abort(404);
        }

        $networks = $supplier->networks;

        if($networks->contains($network)) {
            return Response::json(array(
                'failed' => true,
                'message' => "Network already linked to supplier"
            ));
        } else {
            $supplier->networks()->attach($network);

            return $this->listSupplierNetworks($enc_supplier_id);
        }
    }

    /**
     * Unlink the provided supplier from a network
     * 
     * @param int $supplier_id
     * @return void
     */
    public function detachFromNetwork($enc_supplier_id = null, $enc_network_id = null)
    {
        $supplier_id = base64_decode($enc_supplier_id);
        $network_id = base64_decode($enc_network_id);
        $supplier = Supplier::find($supplier_id);

        if(!$supplier) {
            App::abort(404);
        }

        $supplier->networks()->detach($network_id);

        return $this->listSupplierNetworks($enc_supplier_id);
    }  

    /**
     * Generate the attribute list of supplier networks
     * 
     * @param int $supplier_id
     * @return View
     */
    private function listSupplierNetworks($enc_supplier_id)
    {
        $supplier_id = base64_decode($enc_supplier_id);
        $supplier = Supplier::find($supplier_id);

        if(!$supplier) {
            App::abort(404);
        }

        return View::make('supplier.networklist', array(
            'supplier' => $supplier
        ));        
    }  

    /**
     * Re-sync a suppliers inherit network
     * 
     * @param int $supplier_id
     * @param int $parent_id
     * @return View
     */
    public function getResetNetworks($enc_supplier_id, $enc_parent_id) 
    {
        $supplier_id = base64_decode($enc_supplier_id);
        $parent_id = base64_decode($enc_parent_id);
        $supplier = Supplier::find($supplier_id);
        $parent  = Supplier::find($parent_id);

        // Remove all current links to networks
        foreach ($supplier->networks as $network) {
            $supplier->networks()->detach($network->id);
        }

        // Link it to parent networks
        foreach ($parent->networks as $network) {
            $supplier->networks()->attach($network->id);
        }

        return $this->listSupplierNetworks($enc_supplier_id);
    }


} // EOC