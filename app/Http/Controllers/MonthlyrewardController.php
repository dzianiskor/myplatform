<?php
namespace App\Http\Controllers;
use I;
use View;
use ReportHelper;
use Input;
use Illuminate\Support\Facades\DB;
use App\Partner as Partner;
use SMSHelper;
use App\User as User;
use Illuminate\Support\Facades\Redirect;

/**
 * Import Controller
 *
 * @category   Controllers
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class MonthlyrewardController extends BaseController
{
    /**
     * Ensure that we're running as admin
     *
     * @return void
     */
    public function __construct()
    {
//        if(!I::am('BLU Admin')) {
//            App::abort(404);
//        }
    }

    /**
     * Get the file to import
     *
     * @return void
     */
    public function getIndex()
    {
        if(!I::can('view_summary_email')){
            return Redirect::to(url("dashboard"));
        }
        $defaults = ReportHelper::normalizeDefaults();
        return View::make("monthlyreward.view",array('defaults'    => $defaults));
    }




    /*
     * Perform the Monthly Email
     *
     * @return void
     */
    public function postMonthlymail()
    {
        if(!I::can('view_summary_email')){
            return Redirect::to(url("dashboard"));
        }
        $type = self::sanitizeText(Input::get('type'));
        
        $zerobalance = self::sanitizeText(Input::get('zerobalance'));
        $inactive = self::sanitizeText(Input::get('inactive'));
        $allusers = self::sanitizeText(Input::get('allusers'));
        $segments = array();
        $partners = array();

        $includezero = false;
        if($zerobalance === 'zerobalance'){
            $includezero = true;
        }
        $includeinactive = false;
        if($inactive === 'inactive'){
            $includeinactive = true;
        }

        $includeallusers = false;
        if($allusers === 'allusers'){
            $includeallusers = true;
        }
        $seg_users_ids = array();
        if($includeallusers == true){
            $partners=  array();
            $defaults['filter1'] = $partners;
            $defaults['start_date'] = date('m/d/Y',strtotime("now"));
            $defaults['end_date']= date('m/d/Y',strtotime("now"));
            $defaults['entities'] = array(-1);
             if(Input::has('filter_1')) {
                $partners = self::sanitizeText(Input::get('filter_1'));

            }
            else{
                return View::make("monthlyreward.view",array('defaults' => $defaults,'result' => "Please choose 1 partner"));
            }
            $partner_users = DB::table('partner_user')->whereIn('partner_id', $partners)->groupBy("user_id")->get();
            $pusers_ids = array(); 
            foreach($partner_users as $pu){
                $tempuser = User::find($pu->user_id);
                if(isset($tempuser)){
                    $pusers_ids[] = $pu->user_id;
                    $seg_users_ids[] = $tempuser;
                }
            }
//            echo"<pre>";
//            var_dump($pusers_ids);
//            exit();
            $errors = array();
            $balance = 0;
            $count_err = 0;
            foreach($seg_users_ids as $seg_use_id){
                $balance = 0;
                $partner  = Partner::find($partners[0]);
                if(!isset($seg_use_id->id) && $seg_use_id->id < 28040){
                    $errors[$count_err] = $seg_use_id;
                    $count_err +=1;
                    continue;
                }
                if($partners[0] === "4173"){
                    if($includeinactive === false){
                        if($seg_use_id->status === "active"){
                            
                            
                            if(null == $seg_use_id->balance($partner->firstNetwork()->id)){
                                $errors[$seg_use_id->id] = $seg_use_id->balance($partner->firstNetwork()->id) . " -- 1";
                                continue;
                            }
                            else{
                                $balance = $seg_use_id->balance($partner->firstNetwork()->id);
                            }
                            $monthly_reward_sms = View::make("sms.monthlyreward_Cedrus",
                            array(
                                    'balance'          =>  $balance
                                    )
                            )
                            ->render();
                            echo "3";
                            SMSHelper::send($monthly_reward_sms, $seg_use_id, $partner);
                        }
                    }
                    else{
                        $partner  = Partner::find($partners[0]);
                        if(null == $seg_use_id->balance($partner->firstNetwork()->id)){
                            $errors[$seg_use_id->id] = $seg_use_id->balance($partner->firstNetwork()->id) . " -- 2";
                            continue;
                        }
                        else{
                            $balance = $seg_use_id->balance($partner->firstNetwork()->id);
                        }
                        $monthly_reward_sms = View::make("sms.monthlyreward_Cedrus",
                        array(
                                'balance'          =>  $balance
                                )
                        )
                        ->render();
                        echo "4";
                        SMSHelper::send($monthly_reward_sms, $seg_use_id, $partner);
                    }
                }
                else{
                    if($includeinactive === false){
                        if($seg_use_id->status === "active"){
                            $partner  = Partner::find($partners[0]);
                            if(null == $seg_use_id->balance($partner->firstNetwork()->id)){
                                $errors[$seg_use_id->id] = $seg_use_id->balance($partner->firstNetwork()->id) . " -- 3";
                                continue;
                            }
                            else{
                                $balance = $seg_use_id->balance($partner->firstNetwork()->id);
                            }
                            $monthly_reward_sms = View::make("sms.monthlyreward2",
                            array(
                                    'balance'          =>  $balance
                                    )
                            )
                            ->render();
                            echo "5";
                            SMSHelper::send($monthly_reward_sms, $seg_use_id, $partner);
                        }
                    }
                    else{
                        $partner  = Partner::find($partners[0]);
                        if(null == $seg_use_id->balance($partner->firstNetwork()->id)){
                            $errors[$seg_use_id->id] = $seg_use_id->balance($partner->firstNetwork()->id) . " -- 4";
                            continue;
                        }
                        else{
                            $balance = $seg_use_id->balance($partner->firstNetwork()->id);
                        }
                        $monthly_reward_sms = View::make("sms.monthlyreward2",
                        array(
                                'balance'          =>  $balance
                                )
                        )
                        ->render();
                        echo "6";
                        SMSHelper::send($monthly_reward_sms, $seg_use_id, $partner);
                    }
                    
                }
            }
           echo "<pre>Errors: ";
           var_dump($errors);
           exit();
            return View::make("monthlyreward.view",array('defaults' => $defaults,'result' => "SMS To All Partner Users have been sent"));
        }
        else{
            $start_date = self::sanitizeText(Input::get('start_date'));
        $end_date = self::sanitizeText(Input::get('end_date'));
        if(Input::has('entity_id')) {
            $segments = self::sanitizeText(Input::get('entity_id'));

        }
        if(empty($segments)) {
            $segments = array(-1);
        }
        $segment_users = array();
        $segment_users = DB::table('segment_user')->whereIn('segment_id', $segments)->get();
            
        foreach($segment_users as $su){
            $seg_users_ids[] = $su->user_id;
        }


        if(Input::has('filter_1')) {
            $partners = self::sanitizeText(Input::get('filter_1'));

        }
        if(empty($partners)) {
            $partner_segments = DB::table('segment')->whereIn('id', $segments)->get();
            foreach($partner_segments as $ps){
                $partners [] = $ps->partner_id;
            }

        }
        if(empty($seg_users_ids) && !empty($partners)){
            $partner_users = DB::table('partner_user')->whereIn('partner_id', $partners)->get();
            foreach($partner_users as $pu){
                $seg_users_ids[] = $pu->user_id;
            }
        }
        $defaults['entities'] = $segments;
        if(empty($partners)){
            $partners = array(-1);
        }
        $defaults['filter1'] = $partners;
        $defaults['start_date'] = $start_date;
        $defaults["end_date"] = $end_date;


        $start_date_time = strtotime($start_date);
        $end_date_time = strtotime($end_date);
        $start_date_1 = date("Y-m-d",$start_date_time);
        $end_date_1 = date("Y-m-d", $end_date_time);

            $results = Transaction::where('voided', 0);
        $results->select(
            'user_id',
            DB::raw('SUM(points_rewarded) AS points_rewarded')
        );
        //$results->whereIn('partner_id', $partners);
        $results->whereIn('user_id',$seg_users_ids);
            //$results->where('points_rewarded', '>', 0);
        if($type == 'Email'){
            $results->where('created_at', '>=', $start_date_1);
            $results->where('created_at', '<=', $end_date_1);
        }
        $results->groupBy('user_id');
        $result_rewarded_points = $results->get();
        }

//                echo "<pre>";
//        var_dump(DB::getQueryLog());
//        exit();
//        
//        
//        
//        echo "<pre>";
//        echo $start_date_1;
//        echo " ------ " . $end_date_1;
//        var_dump($result_rewarded_points);
//        die();
        if($type == 'Email'){
            foreach($result_rewarded_points as $t){
                $monthly_reward = View::make("emails.monthlyreward",
                array(
                        'name'          => $t->user->name,
                        'points_rewarded'=> number_format($t->points_rewarded),
                        'start'         => $defaults['start_date'],
                        'end'           => $defaults["end_date"]
                        )
                )
                ->render();

                $mail = new SendGrid\Email();
                $mail->addTo($t->user->email);
                $mail->setFrom(Config::get('blu.email_from'));
				$mail->setFromName('BLU Points');
                $mail->setSubject("BLU Reward Summary : {$defaults['start_date']} - {$defaults["end_date"]}");
                $mail->setHtml($monthly_reward);

                $userName = Config::get('sendgrid.username');
                $password = Config::get('sendgrid.password');
                $sendGrid = new SendGrid($userName, $password);
                $sendGrid->send($mail);

    //            $monthly_reward_sms = View::make("sms.monthlyreward",
    //            array(
    //                    'name'          => $t->user->name,
    //                    'points_rewarded'=> number_format($t->points_rewarded),
    //                    'start'         => $defaults['start_date'],
    //                    'end'           => $defaults["end_date"]
    //                    )
    //            )
    //            ->render();
    //            $partner  = Partner::find(4);
    //            $user = $t->user;
    //            SMSHelper::send($monthly_reward_sms, $user, $partner);
            }
                return View::make("monthlyreward.view",array('defaults' => $defaults,'result' => "Emails sent successfully"));
        }
        elseif($type == "Sms"){
            if($partners[0] == "-1"){
                return View::make("monthlyreward.view",array('defaults' => $defaults,'result' => "For SMS at least 1 Partner should be selected"));
            }
            $partner  = Partner::find($partners[0]);
            
            
//            echo "<pre>";
//            var_dump($result_rewarded_points);
//            exit();
            
            $all_users = array();
            if($partners[0] === "4173"){
                foreach($result_rewarded_points as $t){
                    
                    $user = User::find($t->user_id);
                    if(!isset($user)){
                        continue;
                    }
                    $all_users[$t->user_id][0] = $user->status;
                    $all_users[$t->user_id][1] = $t->points_rewarded;
                    //echo "1";
                    if($includezero == TRUE ){
                        if($includeinactive === false){
            
                            if($user->status === "active"){
                                $monthly_reward_sms = View::make("sms.monthlyreward_Cedrus",
                                array(
                                        'balance'          =>  $t->points_rewarded
                                        )
                                )
                                ->render();
                                //echo "2";
                                SMSHelper::send($monthly_reward_sms, $user, $partner);
                            }
                        }
                        else{
                            $monthly_reward_sms = View::make("sms.monthlyreward_Cedrus",
                            array(
                                    'balance'          =>  $t->points_rewarded
                                    )
                            )
                            ->render();
                            //echo "3";
                            SMSHelper::send($monthly_reward_sms, $user, $partner);
                        }
                    }
                    else{
                        if($t->points_rewarded > 0){
                        if($includeinactive === false){
                            if($user->status === "active"){
                                $monthly_reward_sms = View::make("sms.monthlyreward_Cedrus",
                                array(
                                            'balance'          =>  $t->points_rewarded
                                        )
                                )
                                ->render();
                                    //echo "4";
                                SMSHelper::send($monthly_reward_sms, $user, $partner);
                            }
                        }
                        else{
                            $monthly_reward_sms = View::make("sms.monthlyreward_Cedrus",
                            array(
                                        'balance'          =>  $t->points_rewarded
                                    )
                            )
                            ->render();
                                //echo "5";
                            SMSHelper::send($monthly_reward_sms, $user, $partner);
                        }
                    }
                }
            }
            }
            else{
                foreach($result_rewarded_points as $t){
                    $user = User::find($t->user_id);
                    if($includezero == TRUE ){
                        if($includeinactive === false){
                            if($user->status === "active"){
                                $monthly_reward_sms = View::make("sms.monthlyreward2",
                                array(
                                        'balance'          =>  $t->points_rewarded
                                        )
                                )
                                ->render();
                                SMSHelper::send($monthly_reward_sms, $user, $partner);
                            }
                        }
                        else{
                            $monthly_reward_sms = View::make("sms.monthlyreward2",
                            array(
                                    'balance'          =>  $t->points_rewarded
                                    )
                            )
                            ->render();
                            SMSHelper::send($monthly_reward_sms, $user, $partner);
                        }
                    }
                    elseif($t->points_rewarded > 0){
                        if($includeinactive === false){
                            if($user->status === "active"){
                                $monthly_reward_sms = View::make("sms.monthlyreward2",
                                array(
                                        'balance'          =>  $t->points_rewarded
                                        )
                                )
                                ->render();
                                SMSHelper::send($monthly_reward_sms, $user, $partner);
                            }
                        }
                        else{
                            $monthly_reward_sms = View::make("sms.monthlyreward2",
                            array(
                                    'balance'          =>  $t->points_rewarded
                                    )
                            )
                            ->render();
                            SMSHelper::send($monthly_reward_sms, $user, $partner);
                        }
                    }
                }
            }
//            echo "<pre>";
//            var_dump($all_users);
//            exit();

            return View::make("monthlyreward.view",array('defaults' => $defaults,'result' => "SMS sent successfully"));

        }




//        echo "<pre>";
//        echo $start_date_1;
//        echo " ------ " . $end_date_1;
//        var_dump($result_rewarded_points);
//        die();
        //$results->orderBy('points_redeemed', 'DESC');
        //$defaults = App\classes\ReportHelper::normalizeDefaults();
//        return View::make("monthlyreward.view",array('defaults' => $defaults,'result' => "Emails & SMS sent successfully"));
    }

} // EOC
