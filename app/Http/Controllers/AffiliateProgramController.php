<?php
namespace App\Http\Controllers;
use App\Media;
use Illuminate\Support\Facades\App;
use View;
use Input;
use Validator;
use Session;
use Security;
use I;
use Illuminate\Support\Facades\Auth;
use App\BluCollection as BluCollection;
use AuditHelper;
use Illuminate\Support\Facades\Redirect;
use Transaction;
use App\Partner as Partner;
use Notification;
use Ticket;
use Language;
use Localizationkey;
use LangKey;
use Network;
use ManagedObjectsHelper;
use User;
use App\Country;
use App\Admin as Admin;
use App\Role as Role;
use App\City as City;
use App\Area as Area;
use Meta;
use Illuminate\Support\Facades\DB;
use App\Category as Category;
use App\Affiliate as Affiliate;
use App\AffiliateProgram as AffiliateProgram;
use UCID;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Affiliate Program Controller
 *
 * @category   Controllers
 * @package    BLU
 * @author     Magid Mroueh <team@wixelhq.com>
 * @copyright  2017 BLU
 * @link       http://blupoints.com
 */
class AffiliateProgramController extends BaseController
{
    protected $layout = "layouts.dashboard";

    private $auth_user = null;

    public function __construct()
    {
        parent::__construct();

        $this->auth_user = Auth::User();
    }

    /**
     * Get all list data
     *
     * @return array
     */
    private function getAllLists()
    {
        return array(
            'partners' => ManagedObjectsHelper::managedPartners()->sortBy('name')->pluck('name', 'id'),
            'statuses' => array('enabled'=>'Enabled', 'disabled'=>'Disabled'),
            'categories' => Category::with('children')->where('parent_category_id', 1)->where('draft', false)->orderBy('name', 'asc')->get(),
            'affiliates' => ManagedObjectsHelper::managedAffiliates()->sortBy('name')->pluck('name', 'id'),
        );
    }

    /**
     * List all Affiliates
     *
     * @return void
     */
    public function listAffiliateProgram()
    {
        if(!I::can('view_affiliate_program')){
            return Redirect::to(url("dashboard"));
        }
        $this->ormClass      = 'App\AffiliateProgram';
        $this->search_fields = array('name');

        $affiliatePrograms = $this->getList();

        $page = Input::get('page');
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $affiliatePrograms = new LengthAwarePaginator($affiliatePrograms->forPage($page, 15), $affiliatePrograms->count(), 15, $page);

        return View::make('affiliate_program.list', array(
            'affiliatePrograms' => $affiliatePrograms,
            'lists' => $this->getAllLists()
        ));
    }

    /**
     * Create a new admin draft and redirect to it
     *
     * @return Response
     */
    public function newAffiliateProgram()
    {
        if(!I::can('add_affiliate_program')){
            return Redirect::to(url("dashboard"));
        }
        $affiliateProgram = new AffiliateProgram();
        
        $affiliateProgram->ucid            = UCID::newAffiliateProgramID();
        $affiliateProgram->status          = 'enabled';
        $affiliateProgram->name            = 'New Name';
        $affiliateProgram->description     = 'New Description';
        $affiliateProgram->draft           = 1;
        $affiliateProgram->save();
        
        $affiliateProgramId = base64_encode($affiliateProgram->id);
        return Redirect::to(url("dashboard/affiliate_program/view/{$affiliateProgramId}"));
    }

    /**
     * Display the admin view page
     *
     * @param int $id
     * @return View
     */
    public function viewAffiliateProgram($enc_id = null)
    {
        $id = base64_decode($enc_id);
        if(!I::can('view_affiliate_program')){
            return Redirect::to(url("dashboard"));
        }
        $affiliateProgram = AffiliateProgram::find($id);

        if(!$affiliateProgram) {
            App::abort(404);
        }

        $view_data = array(
            'affiliateProgram' => $affiliateProgram,
            'partners'         => ManagedObjectsHelper::managedPartners()->pluck('name', 'id'),
            'managedPartners'  => Auth::User()->managedParentPartnerIDs()
        );

        return View::make('affiliate_program.view', array_merge($view_data, $this->getAllLists()));
    }

    /**
     * Update a Affiliate Program
     *
     * @param int $enc_id
     * @return Response
     */
    public function updateAffiliateProgram($enc_id = null)
    {
        $id = base64_decode($enc_id);
        if (!I::can('edit_affiliate_program') && !I::can('add_affiliate_program')) {
            return Redirect::to(url("dashboard"));
        }
        $affiliateProgram = AffiliateProgram::find($id);

        if(!$affiliateProgram) {
            App::abort(404);
        }

        $validator = Validator::make(Input::all(), array(
            'name' => 'required|min:2',
        ));

        if($validator->fails()) {
            return Redirect::to(url("dashboard/affiliate_program/view/$enc_id"))->withErrors($validator);
        }


        if(!$affiliateProgram->draft) {
            AuditHelper::record(
                Auth::User()->firstPartner()->id, "Affiliate Program '{$affiliateProgram->id} ' updated by '{Auth::User()->name}'"
            );
        }

        $affiliateProgram->status   = self::sanitizeText(Input::get('status'));
        $affiliateProgram->name     = self::sanitizeText(Input::get('name'));
        $affiliateProgram->affiliate_id    = self::sanitizeText(Input::get('affiliate_id'));
        $affiliateProgram->description     = self::sanitizeText(Input::get('description'));

        if(Input::hasFile('image')) {
            $file = Input::file('image');
            $path = public_path('uploads');
            $ext  = strtolower($file->getClientOriginalExtension());
            $size = $file->getSize();
            $type = $file->getMimeType();
            $name = md5(time()).".{$ext}";

            if(in_array($ext, Meta::allowedImageExts() )) {
                Input::file('image')->move($path, $name);

                $f = new Media();
                $f->name = $name;
                $f->size = $size;
                $f->type = $type;
                $f->ext  = $ext;
                $f->save();

                $affiliateProgram->image = $f->id;
            }
        }
        $affiliateProgram->draft = '0';
        $affiliateProgram->save();

        return Redirect::to(Input::get('redirect_to', 'dashboard/affiliate_program'));
    }

    /**
     * Delete a Affiliate Program account
     *
     * @param int $enc_id
     * @return Response
     */
    public function deleteAffiliateProgram($enc_id = null)
    {
        $id = base64_decode($enc_id);
        if(!I::can('delete_affiliate_program')){
            return Redirect::to(url("dashboard"));
        }

        $affiliateProgram = AffiliateProgram::find($id);

        if(!$affiliateProgram) {
            App::abort(404);
        }

        AffiliateProgram::find($id)->delete();

        return Redirect::to('dashboard/affiliate_program');
    }

    /**
     * Attach a user account to a partner
     *
     * @param int $enc_affiliateProgram_id
     * @return Resposne
     */
    public function attachAffiliateProgramPartner($enc_affiliateProgram_id = null)
    {
        $affiliateProgram_id = base64_decode($enc_affiliateProgram_id);
        $affiliateProgram    = AffiliateProgram::find($affiliateProgram_id);
        $partner = Partner::find(self::sanitizeText(Input::get('partner_id')));

        if(!$affiliateProgram || !$partner) {
            App::abort(404);
        }

        if(!$affiliateProgram->partners->contains($partner)) {
            $affiliateProgram->partners()->attach($partner);
        }

        return $this->listAffiliateProgramPartners($enc_affiliateProgram_id);
    }
    
    /**
     * Unlink a partner from a affiliate
     *
     * @param int $enc_affiliateProgram_id
     * @param int $enc_partner_id
     * @return Response
     */
    public function unlinkAffiliateProgramPartner($enc_affiliateProgram_id = null, $enc_partner_id = null)
    {
        $affiliateProgram_id = base64_decode($enc_affiliateProgram_id);
        $partner_id = base64_decode($enc_partner_id);
        $affiliateProgram = AffiliateProgram::find($affiliateProgram_id);
        $partner = Partner::find($partner_id);

        if(!$affiliateProgram || !$partner) {
            App::abort(404);
        }

        $affiliateProgram->partners()->detach($partner_id);

        return $this->listAffiliateProgramPartners($enc_affiliateProgram_id);
    }
    
    /**
     * List partners
     *
     * @param int $enc_affiliateProgram_id
     * @return Response
     */
    public function listAffiliateProgramPartners($enc_affiliateProgram_id)
    {
        $affiliateProgram_id = base64_decode($enc_affiliateProgram_id);
        $affiliateProgram = AffiliateProgram::find($affiliateProgram_id);

        if(!$affiliateProgram) {
            App::abort(404);
        }

        return View::make('affiliate_program.partnerlist', array(
            'affiliateProgram' => $affiliateProgram
        ));
    }
} // EOC