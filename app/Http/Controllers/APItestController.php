<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use RewardHelper;
use SendGrid;
use View;
use Input;
use Request;
use File;
use Validator;
use App\CurrencyPricing as CurrencyPricing;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Auth;
use AuditHelper;
use PinCode;
use App\Reference as Reference;
use App\User as User;
use App\AffiliateProgramsMemberships as AffiliateProgramsMemberships;
use Meta;
use InputHelper;
use App\Admin as Admin;
use App\Transaction as Transaction;
use App\Partner as Partner;
use App\Language as Language;
use App\Country as Country;
use App\ProductPartnerReward as ProductPartnerReward;
use App\Currency as Currency;
use App\Tier as Tier;
use PointsHelper;
use App\Store as Store;
use App\Pos as Pos;
use UCID;
use App\Http\Controllers\NotificationController as NotificationController;

/**
 * API2 Controller
 *
 * @category   Controllers
 * @package    BLU
 * @author     Ralph Nader <adminit@bluloyalty.com>
 * @copyright  2016 BLU
 * @link       http://blupoints.com
 */
class APItestController extends BaseController
{
    private $networks;
    private $partner;
    private $user;

    /**
     * Initiate the controller and ensure the partner request is authenticated
     *
     * @return void
     */
    public function __construct(){
        $api_key = Input::get('api_key', null);

        if(empty($api_key)) {
            App::abort(401);
        }

        $this->user = Admin::where('api_key', "$api_key")->first();

        if(!$this->user) {
            App::abort(404);
        } else {
            Auth::loginUsingId($this->user->id);

            // Assign scopes

            $this->partner = $this->user->partners()->first();
            $this->network = $this->partner->firstNetwork();

            if(!$this->partner) {
                App::abort(403, "No partner associated to request $api_key");
            }
        }
    }

    /**
     * Generic response handler for API requests
     *
     * @param int $status
     * @param string $message
     * @param object $objects
     * @param string $class_name
     * @return Response
     */
    private static function respondWith($status, $message, $object = null, $class_name = ''){
        $response = array();

        if($message) {
            $response['message'] = $message;
        }

        if(!empty($object)) {
            if($object && is_object($object)) {
                if(get_class($object) != 'stdClass') {
                    $class_name = get_class($object);
                }

                if(method_exists($object, 'toArray')) {
                    $response[$class_name] = $object->toArray();
                } else {
                    $response[$class_name] = $object;
                }
            } else {
                $response[$class_name] = $object;
            }
        } else {
            $response[$class_name] = array();
        }

        $response['status'] = $status;

        if($status == 500) {
            Log::warning($message);
        }

        return Response::make($response, $status);
    }
    public function getVersion(){
        return "gJXHUGX&X+h@84jE";
    }
    

    /**
     * Perform Import Using api
     *
     * POST /api/import-file
     *
	 * @param POST file (file)
     * @return Response
     */
    public function postImportFile(){
        if(Input::hasFile('file')) {
            $ip = Request::getClientIp();
            $file = Input::file('file');
            $path = public_path('uploads');
            $ext  = $file->getClientOriginalExtension();
            $name1 = $file->getClientOriginalName();
            $name = md5(time()).".{$ext}";
            $result = FALSE;
            $errResult = array();
            $auth_user = $this->user;
            //-----------------------------------------------------BISBTransactions

            if(($ext == 'TXT' || $ext == 'txt') && strstr($name1,'BISBTransactions') && strcmp($this->user->api_key, "092c7dfffab8aef9449663c52504bbd7")==0 ){
                //Reference ID|PostingDate|Product ID|BonusPts|Billing Amount|Type|MCC|
                //  0               1           2           3       4           5   6 
                //Billing Currency|Transaction currency|Transaction Amount|Transaction Sign|Line number
                //        7               8                       9               10              11
                $tx = array();
                $count=0;
                $counterrors =0;
                $errBool = True;
                $errors = array();
                $params = array();
                $passes = 0;
//$counterrors = 0;
                Input::file('file')->move($path, $name);
                $inputFile = $path.'/'.$name;
                $contents = File::get($inputFile);
                $contents = nl2br($contents,FALSE);
                $contentLines = explode("<br>", $contents);
                $errors = array();
                
                
                $countContentLines = count($contentLines) - 1;
                $test = $contentLines[$countContentLines];
                $test1 = $contentLines[$countContentLines - 1 ];
                $ret_eof_error = true;
                if(trim($test) === "EOF"){
                    $ret_eof_error = false;
                }
                elseif(trim($test1) === "EOF"){
                    $ret_eof_error = false;
                }
                if($ret_eof_error == true){
                 return self::respondWith(
                        200, 'EOF NOT FOUND', $errors, 'errors'
                    );
                }
                $arrContent = array();
                foreach ($contentLines as $lineread){
                    if(trim($lineread) !=='EOF'){
                        $arrContent[] = explode("|", $lineread);
                    }
                }
                $partner= $this->partner;
                $network_id = $partner->firstNetwork()->id;
                $partner_id = $partner->id;
                $partners = $partner->managedObjects();
                $partners_children = array();
                foreach ($partners as $part) {
                    $partners_children[] = $part->id;
                }
                $loy_by_partner = RewardHelper::loyaltyByPartner($partners_children);

                $partner_prefix = $partner->prefix;
                $itemchosen = ProductPartnerReward::where('partner_id', $partner_id)->get();
                $arr_item_chosen = array();
                foreach ($itemchosen as $ic) {
                    $arr_item_chosen[$ic->partner_mapping_id] = $ic->product_id;
                }

                
                $transactions_file = Transaction::select('ref_number')->where('ref_number', 'like', $name1 . "%")->get();
                $arr_transactions_file = $transactions_file->toArray();

//                $ref_number = $name1."1";
//                echo "<pre>";
//                var_dump(array_search($ref_number, array_column($arr_transactions_file, 'ref_number')));
//                exit();
                $arr_currencies = array();
                $currencies = Currency::all();
                foreach ($currencies as $curr) {
                    $arr_currencies[$curr->short_code] = $curr->id;
                }
                if (count($arr_transactions_file) > 0) {
                    return self::respondWith(
                                    200, 'NOT OK File Already Exists', null, 'errors'
                    );
                }
                
                $store_res = $partner->stores->first();
                if($store_res){
                    $pos_id = $store_res->pointOfSales->first()->id;
                    $tx['store_id'] = $store_res->id;
                    $tx['pos_id'] = $pos_id;
                }
                
//                echo "<pre>";
//                var_dump($arrContent);
//                exit();
                foreach($arrContent as $row){
                    $lineNumber = 0;
                    $posted_at = date('Y-m-d H:i:s');
                    if($count != 0){
                        if(trim($row[0]) != '' && trim($row[0]) != 'EOF'){
                            
                            $unhandled_date = explode("/",$row[1]);

                            if(!isset($row[11])){
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-001";
                                $errors[] = $row;
//                                echo $row[0] . "001";
//                                var_dump($row);
//                                exit();
                                continue;
                            }
                            elseif(substr(trim($row[0]), 0, 5) !== "BISB-" || trim($row[0]) === "" || $row[0] === " "){
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-002";
                                $errors[] = $row;
//                                echo $row[0] . "002";
//                                var_dump($row);
//                                exit();
                                continue;
                            }///self::respondwith('400', ,null);
                            elseif($row[1]=== "" || $row[1] === " "  || !checkdate($unhandled_date[1], $unhandled_date[0], $unhandled_date[2])){
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-003";
                                $errors[] = $row;
//                                echo $row[0] . "003";
//                                var_dump($row);
//                                exit();
                                continue;
                            }
                            elseif($row[2] === "" || $row[2] === " " || !is_numeric($row[2])){
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-004";
                                $errors[] = $row;
//                                echo $row[0] . "004";
//                                var_dump($row);
//                                exit();
                                continue;
                            }
                            elseif($row[3] === "" || $row[3] === " " || is_numeric($row[3]) === FALSE){
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-005";
                                $errors[] = $row;
//                                echo $row[0] . "005";
//                                var_dump($row);
//                                exit();
                                continue;
                            }
                            elseif($row[4]=== "" || $row[4] === " " || !is_numeric($row[4])){
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-006";
                                $errors[] = $row;
//                                echo $row[0] . "006";
//                                var_dump($row);
//                                exit();
                                continue;
                            }
                            elseif($row[9]=== "" || $row[9] === " " || !is_numeric($row[9])){
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-006.1";
                                $errors[] = $row;
//                                echo $row[0] . "007";
//                                var_dump($row);
//                                exit();
                                continue;
                            }
                            elseif($row[7]=== "" || $row[7] === " " || strlen($row[7])!== 3 ){
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-007";
                                $errors[] = $row;
//                                echo $row[0] . "008";
//                                var_dump($row);
//                                exit();
                                continue;
                            }
                            elseif($row[8]=== "" || $row[8] === " " || strlen($row[8])!== 3 ){
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-007.2";
                                $errors[] = $row;
//                                echo $row[0] . "009";
//                                var_dump($row);
//                                exit();
                                continue;
                            }
                            elseif($row[5]=== "" || $row[5] === " " || strlen($row[5])!== 2 ){
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-007.1";
                                $errors[] = $row;
//                                echo $row[0] . "010";
//                                var_dump($row);
//                                exit();
                                continue;
                            }
                            elseif($row[6]=== "" || $row[6] === " " ){
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-008";
                                $errors[] = $row;
//                                echo $row[0] . "011";
//                                var_dump($row);
//                                exit();
                                continue;
                            }
                            elseif(trim($row[10]) !=="C" && trim($row[10]) !=="D"){
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-010";
                                $errors[] = $row;
//                                echo $row[0] . "012";
//                                var_dump($row);
//                                exit();
                                continue;
                            }
                            
                            elseif(!is_numeric(trim($row[11]))){
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-009";
                                $errors[] = $row;
//                                echo $row[0] . "012";
//                                var_dump($row);
//                                exit();
                                continue;
                            }
                            
                            if(isset($row[11])){
                                $lineNumber = $row[11];
                            }
                            $ref_number = $name1 . $lineNumber;
                            $ref_number = rtrim($ref_number);
//                                echo "<pre>";
//                                echo $name1 . $lineNumber . "<br>";
//                                var_dump($row);
//                                var_dump($errors);
//                                exit();
//                            $transact = Transaction::where('ref_number',$ref_number)->first();
                            
                            if(array_search($ref_number, array_column($arr_transactions_file, 'ref_number')) === FALSE){
                                $coefficient = 1;
                                if($row[10] == 'C'){
                                    $coefficient = intval(-1);
                                }
                                $row[0] = trim($row[0]);
                                $tx['id']			= 'NULL';
                                $tx['ref_number']		= $ref_number;
                                
                                $tx['original_total_amount']    = $row[9] * $coefficient;
                                $tx['partner_amount']           = $row[4] * $coefficient;
                                $tx['partner_trxtype']          = $row[5];
                                $tx['mcc']                      = $row[6];
                                
                                
                                $tx['user_id']			= 1;
                                $tx['points_redeemed']	= 0;
                                $tx['points_rewarded']	= 0;
                                $tx['partner_id']		= $partner->id;
                                $tx['network_id']		= $network_id;
                                $tx['trx_reward']		= 1;

                                $reference = Reference::where('number', $row[0])->first();
                                if(isset($reference->user_id)){
                                    $user = $reference->user_id;
                                }
                                else{
                                    $errBool = true;
                                    $counterrors = $counterrors + 1;
                                    $errors[] = $row;
                                    continue;
                                }
                                
                                $tx['user_id']=$user;
                                
                                $userInfo = User::find($user);
                                if($userInfo->status == 'closed'){
                                    $errBool = true;
                                    $counterrors = $counterrors + 1;
                                    $errors[] = $row;
                                    continue;
                                }
//                                $unhandledDate = $row[1];
//                                $arrunhandledDate = explode("/",$unhandledDate);
                                $date_unhandled = $unhandled_date[1] . "/" . $unhandled_date[0] . "/" . $unhandled_date[2];
                                //$date_unhandled = $row[1];
                                $date = strtotime($date_unhandled);
                                $new_date = date('Y-m-d H:i:s', $date);

                                $tx['created_at']      = $new_date;
                                $tx['updated_at']      = $new_date;
                                $tx['posted_at']       = $posted_at;
                                // Map Currency
                                
                                

//                                $loyalty = Loyalty::where('partner_id',$tx['partner_id'])->first();
                                
                                //$itemchosen = ProductPartnerReward::where('partner_mapping_id',$partner_prefix.$row[2])->first();
                                if(isset($arr_item_chosen[$partner_prefix.$row[2]])){
                                    $tx['product_id'] = $arr_item_chosen[$partner_prefix.$row[2]];
                                }
                                else{
                                    $tx['product_id'] = NULL;
                                }
                                
                                
                                

                                $validator = Validator::make($tx, array(
                                    //'id'         => 'required|numeric',
                                    'partner_id' => 'required|numeric',
                                    'store_id'   => 'required|numeric',
                                    'user_id'    => 'required|numeric'
                                ));

                                if(!$validator->fails()) {


                                    //DB::table('transaction')->insert($tx);
    //                                echo $passes . "<pre>";
    //                                var_dump($params);
    //                                var_dump($arr_result);
    //                                die();
                                    //ReferenceID|Postingdate|Card type|Bonuspts|Amount|Currency
                                    //  0           1           2           3       4       5

                                    if(is_numeric($row[3])){
                                        $txbonus = array(
                                            'partner_id' => $tx['partner_id'],
                                            'store_id' => $tx['store_id'],
                                            'pos_id' => $tx['pos_id'],
                                            'user_id' => $tx['user_id'],
                                            'ref_number' => "Bisb ONLINE Bonus POINTS",
                                            'total_amount' => 0,
                                            'points_rewarded' => round($row[3],0,PHP_ROUND_HALF_UP) * $coefficient,
                                            'amt_reward' => PointsHelper::pointsToAmount(round($row[3],0,PHP_ROUND_HALF_UP),"BHD" ,$network_id) * $coefficient,
                                            'trx_reward' => 1,
                                            
                                            'currency_id' => 6,
                                            'exchange_rate' => 1,
                                            'country_id' => 8,
                                            'quantity' => 0,
                                            'notes' => 'Bonus Points',
                                            'trans_class' => "Amount",
                                            'network_id' => $network_id,
                                            'created_at' => $new_date,
                                            'updated_at' => $new_date,
                                            'posted_at' => $posted_at
                                        );

                                        if($txbonus['points_rewarded'] != 0){
                                            DB::table('transaction')->insert($txbonus);
                                            $user = User::find($txbonus['user_id']);
                                            $user->updateBalance($row[3], $txbonus['network_id'], 'add');
                                            $partner  = Partner::find($txbonus['partner_id']);
                                        }
                                        //$template = View::make('sms.bonus-points', array('bonusPoints' => $row[3]))->render();

                                        //SMSHelper::send($template, $user, $partner);
                                    }
                                    
                                        $passes++;

                                        if(is_numeric($tx['points_rewarded'])  ){
//                                            $params = array(
//                                                'user_id'		=> $tx['user_id'],
//                                                'amount'		=> floatval($tx['partner_amount']),
//                                                'currency'		=> $tx['partner_currency_id'],
//                                                'partner'		=> $tx['partner_id'],
//                                                'store_id'		=> $tx['store_id'], // Optional
//                                                'pos_id'		=> $tx['pos_id'], // Optional
//                                                //'notes'		=> $tx['notes'], // Optional
//                                                'created_at'	=> $tx['created_at'],
//                                                'updated_at'	=> $tx['updated_at']
//                                            );
                                            $params = array(
                                                'user_id'		=> $tx['user_id'],
                                                'partner'		=> $tx['partner_id'],
                                                'pos_id'		=> $tx['pos_id'],
                                                'store_id'	=> $tx['store_id'],
                                                'amount'		=> floatval($tx['partner_amount']),
                                                'currency'	=> $row[8],
                                                //'notes'		=> 'Reward By Product',
                                                'type'		=> 'reward',
                                                'reference'	=> $tx['ref_number'],
                                                'network_id'	=> $partner->firstNetwork()->id,
                                                'product_id'    => $tx['product_id'],
                                                'partner_currency' => $row[7],
                                                'original_transaction_amount' => floatval($tx['original_total_amount']),
                                                'use_partner_currency'=> True,
                                                'mcc'=> $tx['mcc'],
                                                'partner_trxtype'=> $tx['partner_trxtype'],
                                                'created_at' => $new_date,
                                                'updated_at' => $new_date
                                              );
                                            
//                                            $user = User::find($tx['user_id']);
//                                            APIController::postSendEmailJson(json_encode($params),"BISB Params - ". $tx['ref_number']);
                                            try{
                                                $result_do_reward = RewardHelper::doRewardNewLoyalty($params, 'Amount',$loy_by_partner);
                                            } catch (Exception $ex) {
                                                //$resp = array($ex);
                                                $json_data = json_encode(array("Exception" => $ex));
                                                APIController::postSendEmailJson($json_data);
                                            }
                                            
//                                            $json_data = $result_do_reward;
//                                                APIController::postSendEmailJson($json_data);
                                            //$trx_id = DB::table('transaction')->insertGetId($tx);
                                            //$trx_inserted = Transaction::find($trx_id);
                                            
//                                            try{
//                                                $trx_after_loyalty = Loyalty::processMatchingRewardExceptionRule($trx_inserted);
//                                            } catch (Exception $ex) {
//                                                $resp= array();
////                                                $resp[] = $trx_after_loyalty;
//                                                $resp[] = $ex;
//                                                $json_data = json_encode($resp);
//                                                APIController::postSendEmailJson($json_data);
//
//                                            }
//                                                $json_data = json_encode($trx_inserted->toArray());
//                                                APIController::postSendEmailJson($json_data);
                                            
//                                                $arr_trx_after_loyalty = $trx_after_loyalty->toArray();
//                                                DB::table('transaction')->where('id',$trx_id)->update($arr_trx_after_loyalty);
                                            
                                            
//                                            $user->updateBalance($trx_after_loyalty->points_rewarded, $trx_after_loyalty->network_id, 'add');
//                                            $tx['points_balance'] = $user->balance($tx['network_id']);
//                                            $partner  = Partner::find(4298);
//                                            //$template = View::make('sms.reward', array('bonusPoints' => $row[3]))->render();
//                                            if(!empty($rewardsms) &&  floatval($tx['points_rewarded']) > 0){
//                                                $sms_body = $rewardsms;
//                                                $sms_body = str_ireplace('{title}', $user->title, $sms_body);
//                                                $sms_body = str_ireplace('{first_name}', $user->first_name, $sms_body);
//                                                $sms_body = str_ireplace('{last_name}', $user->last_name, $sms_body);
//                                                $sms_body = str_ireplace('{balance}', $user->balance($tx['network_id']), $sms_body);
//
//
//                                                    $sms_body = str_ireplace('{points}', $tx['points_rewarded'], $sms_body);
//                                                    //SMSHelper::send($sms_body, $user, $partner);
//                                            }

                                        }
                                        //$arr_result = Transaction::doReward($params, 'Amount');

                                }
                            }
                        }

                    }

                    $count++;
                }

                if($counterrors > 0){
                    $imploded_transactions = array();
                    foreach($errors as $err1Id){
                        $imploded_transactions[] = implode(',',$err1Id);
                    }
                    $imploded_arr_transactions = implode("\r\n", $imploded_transactions);
                    $path = public_path('uploads');
                    $file = '/BisbTransactions' . date("1ymd") . '.csv';
                    $filepath = $path . $file;
                    $myfile = fopen($filepath, "w") or die("Unable to open file!");
                    fwrite($myfile, $imploded_arr_transactions);
                    fclose($myfile);

                    $filepath1 = url("/uploads". $file);
                    $html = "Transaction File Imported with Errors";
                    //$response1 =  self::emailattachement('adminit@bluloyalty.com',$filepath,'Cashback FNB',$html);
                     $data = array();
                     $data['html'] = $html;

                     $response1 = Mail::send('emails.fnbemail', $data, function($message1) use ($imploded_arr_transactions, $filepath,$file, $counterrors) {
                            $filename = str_replace('/','',$file);
                            //$message1->attach(new Swift_Message_Attachment(file_get_contents($filepath), $filename, 'application/vnd.ms-excel'));
                            $message1->attachData($imploded_arr_transactions,$filename);
                            $message1->to(array('adminit@bluloyalty.com'))->subject("LIVE TEST Transaction Import BISB Bank - " . $counterrors);
                        });

                    return self::respondWith(
                        200, 'NOT OK', $errors, 'errors'
                    );
                     //with('success','BISB Customers Imported Successfully!');
                }
                else{
                    NotificationController::sendEmailNotification(json_encode("Transaction File Import successful" . $count), 'BLU Points', Config::get('blu.email_from'), array(null), array('adminit@bluloyalty.com'), "LIVE TEST Transaction Import Bisb");
                    return self::respondWith(
                                200, "OK", $errors, 'errors'
                            );
                }
            
            }
            elseif(($ext == 'TXT' || $ext == 'txt') && strstr($name1,'BISBCustomers') && strcmp($this->user->api_key, "092c7dfffab8aef9449663c52504bbd7cccc")==0  ){
                //Reference|email|mobile|dob|gender|marital_status|status
                //echo "ralph";
                AuditHelper::record(
                        4,$auth_user->name  ." started uploading file: " . $name1
                );
                $tx = array();
                Input::file('file')->move($path, $name);
                $inputFile = $path.'/'.$name;
                $contents = File::get($inputFile);
                $contents = nl2br($contents,FALSE);
                $contentLines = explode("<br>", $contents);
                
                $countContentLines = count($contentLines);
                $test = $contentLines[$countContentLines - 1];
                $test1 = $contentLines[$countContentLines - 2 ];
                $ret_eof_error = true;
                if(trim($test) === "EOF"){
                    $ret_eof_error = false;
                }
                elseif(trim($test1) === "EOF"){
                    $ret_eof_error = false;
                }
                if($ret_eof_error == true){
                 return self::respondWith(
                        200, 'EOF NOT FOUND', $errors, 'errors'
                    );
                }
                $arrContent = array();
                foreach ($contentLines as $lineread){
                    $arrContent[] = explode("|", $lineread);
                }
                $count=0;
                $counterrors =0;
                $errBool = True;
                $errors = array();
                $errCount = 0;
                $errIDs = array();
                $genUsers = array();
                foreach($arrContent as $row){
                    if($count != 0){
                        if(trim($row[0]) != '' && trim($row[0]) !== 'EOF'){
                            
                            $ref_id = "";
                            if (substr(trim($row[0]), 0, 5) === "BISB-") {
                                $ref_id = trim($row[0]);
                            } else {
                                $errCount = $errCount + 1;
                                $errIDs[] = "NO REFERENCE". " - " . $count;
                                $genUsers[] = $row;
                                continue;
                            }
                            $email = "";
                            if (isset($row[1])) {
                                $email = $row[1];
                                $user_email = User::where('email', $email)->count();

                                if (empty($email) || strtolower($email) == 'nil' || intval($user_email) > 0) {
                                    $email = UCID::generateUniqueEmail($ref_id, $this->partner->id);
                                }
//                                if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
//                                    $errCount = $errCount + 1;
//                                    $errIDs[] = $ref_id;
//                                    $genUsers[] = $row;
//                                    continue;
//                                }
                            } else {
                                $email = UCID::generateUniqueEmail($ref_id, $this->partner->id);
                            }

                            $mobile = "";
                            if (isset($row[2])) {
                                $mobile = $row[2];
                            } else {
                                $errCount = $errCount + 1;
                                $errIDs[] = $ref_id;
                                $genUsers[] = $row;
                                continue;
                            }


                            $dob = "01/01/3000";
                            if (isset($row[3])) {
                                $dob = $row[3];
                            }

                            $gender = "Not Selected";
                            if (in_array($row[4],array('f','m'))) {
                                $gender = $row[4];
                            }

                            $marital_status = "Not Selected";
                            if (in_array($row[5],array('Single','Married','Widowed','Divorced',"Other"))) {
                                $marital_status = $row[5];
                            }

                            if (in_array($row[6], array('S','I'))) {
                                $status = $row[6];
                            } else {
                                $status = "A";
                            }

                            $importedUsers = array();
                            $count = 0;
                            $partner = $this->partner;
                            $partner_name = $partner->name;
                            $errCount = 0;


                            $user = array();
                            $user['id'] = 'NULL';


                            $fname = "na";
                            $lname = "na";
                            $removed_prefix = str_replace($partner->prefix, '', trim($ref_id));
                            $user['first_name'] = $fname;
                            $user['last_name'] = $lname;
                            //$mobile = $partner->id . "000000000" . trim($removed_prefix);
                            if (substr($mobile, 0, 3) === "973") {
                                $user['mobile'] = substr($mobile, 3);
                                $user['normalized_mobile'] = '+' . $mobile;
                                $user['telephone_code'] = '+973';
                            } else {
                                $user['mobile'] = $mobile;
                                $user['normalized_mobile'] = '+973' . $mobile;
                                $user['telephone_code'] = '+973';
                            }
                            //$email = str_replace('-', '',$partner->prefix).$removed_prefix.'@'. str_replace(' ', '',$partner->name) .'.com';

                            $user['email'] = $email;


                            $date = strtotime($dob);
                            $new_date = date('Y-m-d H:i:s', $date);
                            $user['dob'] = $new_date;
                            $user['gender'] = $gender;
                            $user['marital_status'] = $marital_status;
                            $user['balance'] = 0;
                            $user['confirm_passcode'] = false;

                            $user['passcode'] = PinCode::newCode();

                            $user['draft'] = false;
                            $user['verified'] = 1;
                            $user['status'] = 'active';
                            if ($status == 'A') {
                                $user['status'] = 'active';
                            } elseif ($status == 'S') {
                                $user['status'] = 'suspended';
                            } elseif ($status == 'I') {
                                $user['status'] = 'closed';
                            }

                            $user['created_at'] = date("Y-m-d H:i:s");
                            $user['updated_at'] = date("Y-m-d H:i:s");
                            $user['ucid'] = UCID::newCustomerID();
                            $user['country_id'] = "2";
                            $user['area_id'] = "14";
                            $user['city_id'] = "95";

                            // Validate dob
                            if ($user['dob'] == null || $user['dob'] == '') {
                                $user['dob'] = date("Y-m-d H:i:s", strtotime("1 January 3000"));
                            }

                            //set the registration source POS
                            $storeDetails = Store::where('partner_id', $partner->id)->where('name', 'Real-Time Registration')->first();
                            if ($storeDetails) {
                                $posDetails = Pos::where('store_id', $storeDetails->id)->where('name', 'System Generated References POS')->first();
                                if ($posDetails) {
                                    $user['registration_source_pos'] = $posDetails->id;
                                }
                            }

                            $validator = Validator::make($user, array(
                                        'first_name' => 'required',
                                        'last_name' => 'required',
                                        'email' => 'required|email|unique:users',
                                        'mobile' => 'required|numeric',
                                        'dob' => 'required',
                                        'balance' => 'required|numeric'
                            ));


                            if (!$validator->fails()) {


                                // Create the user
                                $last_id = DB::table('users')->insertGetId($user);
                                //$last_id = DB::table('users')->getPdo()->lastInsertId();
                                // Assign the role
                                DB::table('role_user')->insert(array(
                                    'role_id' => 4,
                                    'user_id' => $last_id
                                ));
                                DB::table('reference')->insert(array(
                                    'number' => trim($ref_id),
                                    'partner_id' => trim($partner->id),
                                    'user_id' => $last_id,
                                    'created_at' => date('Y-m-d H:i:s', time()),
                                    'updated_at' => date('Y-m-d H:i:s', time())
                                ));
                                DB::table('partner_user')->insert(array(
                                    'partner_id' => trim($partner->id),
                                    'user_id' => $last_id
                                ));
                                DB::table('partner_user')->insert(array(
                                    'partner_id' => 1,
                                    'user_id' => $last_id
                                ));


                                DB::table('balance')->insert(array(
                                    'network_id' => trim($partner->firstNetwork()->id),
                                    'user_id' => $last_id,
                                    'balance' => $user['balance']
                                ));
                                DB::table('segment_user')->insert(array(
                                    'segment_id' => $partner->segments->first()->id,
                                    'user_id' => $last_id
                                ));

                                //Default Tier affiliation to Member
                                $defaultTier = Tier::where('partner_id', trim($partner->id))->where('name', 'like', 'Default Tier%')->first();
                                if (!empty($defaultTier)) {
                                    $defaultTierCriteria = Tier::where('tier_id', $defaultTier->id)->where('name', 'like', 'Default Tier Criteria%')->first();
                                    if (!empty($defaultTierCriteria)) {
                                        DB::table('tiercriteria_user')->insert(array(
                                            'tiercriteria_id' => $defaultTierCriteria->id,
                                            'user_id' => $last_id
                                        ));
                                    }
                                }



                                $userSMS = User::find($last_id);
                                $importedUsers[$count]['Blu_id'] = $last_id;
                                $importedUsers[$count]['Middle_id'] = $ref_id;

                                //$resSMS = self::getSendsms($userSMS,3,"new_user");
                                $count++;
                                $userSMS->sendWelcomeMessageNew($partner->id);
                                //$userSMS->sendAccountPin();
                                $errBool = False;
                                //DB::table('transaction')->insert($tx);
                            } else {

                                $errCount = $errCount + 1;
                                $errIDs[] = $ref_id;
                                $genUsers[] = $user;
                                continue;
                            }
                        }

                    }
                    $count++;
                }

                if($errCount > 0){
                    $imploded_transactions = array();
                    
                    $imploded_transactions[] = implode(',',$errIDs);
                    
                    $imploded_arr_transactions = implode("\r\n", $imploded_transactions);
                    $path = public_path('uploads');
                    $file = '/BISBCustomer' . date("1ymd") . '.csv';
                    $filepath = $path . $file;
                    $myfile = fopen($filepath, "w") or die("Unable to open file!");
                    fwrite($myfile, implode("\r\n", $imploded_transactions));
                    fclose($myfile);

                    $filepath1 = url("/uploads". $file);
                    $html = "BISB Customer File Imported with Errors";
                    //$response1 =  self::emailattachement('adminit@bluloyalty.com',$filepath,'Cashback FNB',$html);
                     $data = array();
                     $data['html'] = $html;

                     $response1 = Mail::send('emails.fnbemail', $data, function($message1) use ($imploded_arr_transactions, $filepath,$file, $counterrors) {
                            $filename = str_replace('/','',$file);
                            //$message1->attach(new Swift_Message_Attachment(file_get_contents($filepath), $filename, 'application/vnd.ms-excel'));
                            $message1->attachData($imploded_arr_transactions,$filename);
                            $message1->to(array('adminit@bluloyalty.com'))->subject("Customer Import BISB Bank - " . $counterrors);
                        });

                    AuditHelper::record(
                            4, $name1 ." was uploaded with errors by " . $auth_user->name
                    );
                    return self::respondWith(
                        200, 'NOT OK', $genUsers, 'errors'
                    );
                     //with('success','ABB Customers Imported Successfully!');
                }
                else{
                    NotificationController::sendEmailNotification("BISB Customer File Import successful", 'BLU Points', Config::get('blu.email_from'), array(null), array('adminit@bluloyalty.com'), "Customer Import BISB Bank");
                    AuditHelper::record(
                            4, $name1 ." was uploaded by " . $auth_user->name
                    );
                    return self::respondWith(
                                200, "OK", $genUsers, 'errors'
                            );
                }


            }
            elseif(($ext == 'TXT' || $ext == 'txt') && strstr($name1,'ABBCustomer') && strcmp($this->user->api_key, "f96d7e8fbe3dfc4da3a50e0c131f3038cccc")==0  ){
                //ReferenceID|Title|FirstName|LastName|Email|Mobile|DOB|Gender|MaritalStatus|Occupation|Status
                //0             1   2           3       4       5    6     7        8           9         10
                echo "ralph";
                AuditHelper::record(
                        4,$auth_user->name  ." started uploading file: " . $name1
                );
                $tx = array();
                Input::file('file')->move($path, $name);
                $inputFile = $path.'/'.$name;
                $contents = File::get($inputFile);
                $contents = nl2br($contents,FALSE);
                $contentLines = explode("<br>", $contents);

                $arrContent = array();
                foreach ($contentLines as $lineread){
                    $arrContent[] = explode("|", $lineread);
                }
                $count=0;
                $counterrors =0;
                $errBool = True;
                $errors = array();
                foreach($arrContent as $row){
                    if($count != 0){
                        if(trim($row[0]) != '' && trim($row[0]) !== 'EOF'){
                            $row[0] = trim($row[0]);
                            $errBool = True;
//                            $fullname = explode(' ', $row[2]);
//                            $fname = $fullname[0];
//                            $fullname[0]='';
//                            $lname = implode(' ', $fullname);
                            $user                     = array();
                            $user['id']               = 'NULL';
                            $user['first_name']       = $row[2];
                            $user['last_name']        = $row[3];
                            if(empty($row[3])){
                                $user['last_name'] = "Empty";
                            }
                            $mobile = $row[5];
                            $mobile = ltrim($mobile,'0');
                            $tomobile = str_replace('ABB-','',$row[0]);
                            //echo $mobile . "---<br>";
                            if(!is_numeric($mobile) || strlen($mobile) <= 8){
                                $mobile = "4208". $tomobile;
                                $user['mobile']           = $mobile;
                                $user['normalized_mobile']= '+973'.$mobile;
                                $user['telephone_code']   = '+973';
                            }
                            elseif(InputHelper::isPhone($mobile)){
                                $countc = InputHelper::isPhone($mobile);
                                $mobile = str_replace($countc,"",$mobile);
                                $user['mobile']           = $mobile;
                                $user['normalized_mobile']= '+'.$countc.$mobile;
                                $user['telephone_code']   = '+'.$countc;
                            }
                            else{
                                $user['mobile']           = "";
                                $user['normalized_mobile']= '';
                                $user['telephone_code']   = '';
                            }

                            //echo "****" . InputHelper::isPhone($mobile) . "+++<br>" . $mobile . "====<br>";
                            $email1 = trim($row[4]);
                            $email3 = '';
                            if($email1 == '' || !InputHelper::isEmail($email1)){
                                $email3 = $row[0].'@ABBCust.com';
                            }
                            else{
                                $email3 = $email1;
                            }

                            $user['email']            = $email3;

                            $unhandledDate = $row[6];
                            if($unhandledDate == '' || $unhandledDate == ' '){
                                $unhandledDate = "01/01/1902";
                            }

                            $arrunhandledDate = explode("/",$unhandledDate);
                            $date_unhandled = $arrunhandledDate[1] . "/" . $arrunhandledDate[0] . "/" . $arrunhandledDate[2];

                            $date = strtotime($date_unhandled);
                            $new_date = date('Y-m-d H:i:s', $date);
                            $user['dob']              = $new_date;

                            $user['balance']          = 0;
                            $user['marital_status']   = $row[8];
                            $user['confirm_passcode'] = false;
                            $user['passcode']         = $arrunhandledDate[0] . $arrunhandledDate[1];//PinCode::newCode();
                            $user['title']            = $row[1];
                            $user['draft']            = false;
                            $user['verified']         = 1;
                            $user['status']           = 'active';
//                            if($row[10] == 'D'){
//                                $user['status'] = "inactive";
//                            }
//                            elseif($row[10]== 'C'){
//                                $user['status'] = "suspended";
//                            }

                            $user['created_at']       = date("Y-m-d H:i:s");
                            $user['updated_at']       = date("Y-m-d H:i:s");
                            $user['ucid']             = UCID::newCustomerID();
                            $user['country_id']       = 6;
                            //$user['password']       = Hash::make($row[22]);

                            // Validate gender
                            switch($row[7]) {
                                case "Male":
                                    $user['gender'] = 'm';
                                    break;
                                case "Female":
                                    $user['gender'] = 'f';
                                    break;
                                default:
                                    $user['gender'] = 'm';
                            }

                            // Validate balance
                            if($user['balance'] == null) {
                                $user['balance'] = 0;
                            }

                            // Validate dob
                            if($user['dob'] == null) {
                                $user['dob'] = date("Y-m-d H:i:s", strtotime("1 January 1902"));
                            }

                            //set the registration source POS
                            $storeDetails = Store::where('partner_id', 4208)->where('name', 'Registration through Customer File')->first();
                            $posDetails   = Pos::where('store_id', $storeDetails->id)->where('name', 'Registration through Customer File POS')->first();
                            $user['registration_source_pos']         = $posDetails->id;
                            
                            $validator = Validator::make($user, array(
                                'first_name' => 'required',
                                'last_name'  => 'required',
                                'email'      => 'required|email|unique:users',
                                'mobile'     => 'required|numeric',
                                'dob'        => 'required',
                                'balance'    => 'required|numeric'
                            ));
                            
                            $skipstatus= array();
                            $skipstatus[] = 'inactive';
                            $skipstatus[] = 'closed';
                            $countmobileunique = DB::table('users')->where('normalized_mobile', $user['normalized_mobile'])->whereNotIn('status', $skipstatus)->count();
                            
                            $count = DB::table('users')->where('email', $user['email'])->where('draft', false)->count();
                            $countmobile = DB::table('users')->where('normalized_mobile', $user['normalized_mobile'])->count();


                            $reference = Reference::where('number', $row[0])->first();
                            if(!$validator->fails() && $countmobileunique <= 0) {

                                if(!isset($reference)){
                                    // Create the user
                                    $last_id = DB::table('users')->insertGetId($user);
                                    //$last_id = DB::table('users')->getPdo()->lastInsertId();

                                    // Assign the role
                                    DB::table('role_user')->insert(array(
                                        'role_id' => 4,
                                        'user_id' => $last_id
                                    ));
                                    DB::table('reference')->insert(array(
                                       'number' => $row[0],
                                        'partner_id' => 4208,
                                        'user_id'=>$last_id,
                                        'created_at' => date('Y-m-d H:i:s', time()),
                                        'updated_at' => date('Y-m-d H:i:s', time())
                                    ));
                                    DB::table('partner_user')->insert(array(
                                        'partner_id' => 4208,
                                        'user_id' => $last_id
                                    ));
                                    DB::table('partner_user')->insert(array(
                                        'partner_id' => 1,
                                        'user_id' => $last_id
                                    ));
                                    DB::table('segment_user')->insert(array(
                                        'segment_id' => Partner::find(4208)->segments->first()->id,
                                        'user_id' => $last_id
                                    ));
                                    
                                    //create default balance
                                    $createdUser = User::find($last_id);
                                    $createdUser->updateBalance(0, $this->partner->firstNetwork()->id ,'add');
                                    //Default Tier affiliation to Member
                                    $defaultTier = Tier::where('partner_id', 4208)->where('name', 'like', 'Default Tier%')->first();
                                    if(!empty($defaultTier)){
                                        $defaultTierCriteria = Tier::where('tier_id', $defaultTier->id)->where('name', 'like', 'Default Tier Criteria%')->first();
                                        if(!empty($defaultTierCriteria)){
                                            DB::table('tiercriteria_user')->insert(array(
                                                'tiercriteria_id' => $defaultTierCriteria->id,
                                                'user_id' => $last_id
                                            ));
                                        }
                                    }
        
                                    $userSMS = User::find($last_id);
                                    $custRef = $row[0];
                                    $template    = View::make('sms.new-user-aib', array('ref' => $custRef, 'user' => $userSMS))->render();
                                    $partner = Partner::find('4208');
                                    SMSHelper::send($template, $userSMS, $partner);
                                    //$userSMS->sendWelcomeMessageNew('4208');
                                    //$userSMS->sendAccountPin();
                                    $errBool = False;
                                //DB::table('transaction')->insert($tx);
                            }
                                elseif(isset($reference)){

                                        $user1 = $reference->user_id;
    //
    //                                    $userChange = array();
    //                                    if($row[10] == 'A'){
    //                                        $userChange['status'] = 'active';
    //                                    }
    //                                    elseif($row[10] == 'D'){
    //                                        $userChange['status'] = "inactive";
    //                                    }
    //                                    elseif($row[10]== 'C'){
    //                                        $userChange['status'] = "suspended";
    //                                    }
    //                                    User::where('id',$user)->update($userChange);
    //                                    $errBool = False;
                                        unset($user['id']);
                                        unset($user['passcode']);
                                        DB::table('partner_user')->insert(array(
                                            'partner_id' => 4208,
                                            'user_id' => $user1
                                        ));

                                        User::where('id',$user1)->update($user);
                                        $errBool = False;
                                    }
                            }

                            else{
                                //$reference = Reference::where('number', $row[0])->first();
                                if(isset($reference) && $countmobileunique <= 0){

                                    $user1 = $reference->user_id;
//
//                                    $userChange = array();
//                                    if($row[10] == 'A'){
//                                        $userChange['status'] = 'active';
//                                    }
//                                    elseif($row[10] == 'D'){
//                                        $userChange['status'] = "inactive";
//                                    }
//                                    elseif($row[10]== 'C'){
//                                        $userChange['status'] = "suspended";
//                                    }
//                                    User::where('id',$user)->update($userChange);
//                                    $errBool = False;
                                    unset($user['id']);
                                    unset($user['passcode']);
                                    DB::table('partner_user')->insert(array(
                                        'partner_id' => 4208,
                                        'user_id' => $user1
                                    ));
                                    User::where('id',$user1)->update($user);
                                    $errBool = False;
                                }
                                else{
                                    if($countmobileunique <= 0){
                                        $count = DB::table('users')->where('email', $user['email'])->where('draft', false)->count();
                                        $countmobile = DB::table('users')->where('normalized_mobile', $user['normalized_mobile'])->where('draft', false)->count();

                                        if($count == 0 && empty($user['email'])){
                                            $email3 = $row[0].'@ABBCust.com';
                                            $user['email'] = $email3;
                                            $validator = Validator::make($user, array(
                                                'first_name' => 'required',
                                                'last_name'  => 'required',
                                                'email'      => 'required|email|unique:users',
                                                'mobile'     => 'required|numeric',
                                                'dob'        => 'required',
                                                'balance'    => 'required|numeric'
                                            ));
                                            if(!$validator->fails()){
                                                $last_id = DB::table('users')->insertGetId($user);
                                                //$last_id = DB::table('users')->getPdo()->lastInsertId();

                                                // Assign the role
                                                DB::table('role_user')->insert(array(
                                                    'role_id' => 4,
                                                    'user_id' => $last_id
                                                ));
                                                DB::table('reference')->insert(array(
                                                   'number' => $row[0],
                                                    'partner_id' => 4208,
                                                    'user_id'=>$last_id,
                                                    'created_at' => date('Y-m-d H:i:s', time()),
                                                    'updated_at' => date('Y-m-d H:i:s', time())
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => 4208,
                                                    'user_id' => $last_id
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => 1,
                                                    'user_id' => $last_id
                                                ));
                                                DB::table('segment_user')->insert(array(
                                                    'segment_id' => Partner::find(4208)->segments->first()->id,
                                                    'user_id' => $last_id
                                                ));
                                                
                                                //Default Tier affiliation to Member
                                                $defaultTier = Tier::where('partner_id', 4208)->where('name', 'like', 'Default Tier%')->first();
                                                if(!empty($defaultTier)){
                                                    $defaultTierCriteria = Tier::where('tier_id', $defaultTier->id)->where('name', 'like', 'Default Tier Criteria%')->first();
                                                    if(!empty($defaultTierCriteria)){
                                                        DB::table('tiercriteria_user')->insert(array(
                                                            'tiercriteria_id' => $defaultTierCriteria->id,
                                                            'user_id' => $last_id
                                                        ));
                                                    }
                                                }
        
                                                $userSMS = User::find($last_id);

                                                //$userSMS->sendWelcomeMessage();
                                                //$userSMS->sendAccountPin();
                                                $errBool = False;
                                            }
                                            elseif($validator->fails()){
                                                if($countmobile == 0 || empty($user['normalized_mobile'])){
                                                    $mobile = "4208". $tomobile;
                                                    $user['mobile']           = $mobile;
                                                    $user['normalized_mobile']= '+973'.$mobile;
                                                    $user['telephone_code']   = '+973';
                                                    $validator = Validator::make($user, array(
                                                        'first_name' => 'required',
                                                        'last_name'  => 'required',
                                                        'email'      => 'required|email|unique:users',
                                                        'mobile'     => 'required|numeric',
                                                        'dob'        => 'required',
                                                        'balance'    => 'required|numeric'
                                                    ));
                                                    if(!$validator->fails()){
                                                        $last_id = DB::table('users')->insertGetId($user);
                                                        //$last_id = DB::table('users')->getPdo()->lastInsertId();

                                                        // Assign the role
                                                        DB::table('role_user')->insert(array(
                                                            'role_id' => 4,
                                                            'user_id' => $last_id
                                                        ));
                                                        DB::table('reference')->insert(array(
                                                           'number' => $row[0],
                                                             'partner_id' => 4208,
                                                            'user_id'=>$last_id,
                                                            'created_at' => date('Y-m-d H:i:s', time()),
                                                            'updated_at' => date('Y-m-d H:i:s', time())
                                                        ));
                                                        DB::table('partner_user')->insert(array(
                                                            'partner_id' => 4208,
                                                            'user_id' => $last_id
                                                        ));
                                                        DB::table('partner_user')->insert(array(
                                                            'partner_id' => 1,
                                                            'user_id' => $last_id
                                                        ));
                                                        DB::table('segment_user')->insert(array(
                                                            'segment_id' => Partner::find(4208)->segments->first()->id,
                                                            'user_id' => $last_id
                                                        ));
                                                        
                                                        //Default Tier affiliation to Member
                                                        $defaultTier = Tier::where('partner_id', 4208)->where('name', 'like', 'Default Tier%')->first();
                                                        if(!empty($defaultTier)){
                                                            $defaultTierCriteria = Tier::where('tier_id', $defaultTier->id)->where('name', 'like', 'Default Tier Criteria%')->first();
                                                            if(!empty($defaultTierCriteria)){
                                                                DB::table('tiercriteria_user')->insert(array(
                                                                    'tiercriteria_id' => $defaultTierCriteria->id,
                                                                    'user_id' => $last_id
                                                                ));
                                                            }
                                                        }
                                                        $userSMS = User::find($last_id);

                                                        //$userSMS->sendWelcomeMessage();
                                                        //$userSMS->sendAccountPin();
                                                        $errBool = False;
                                                    }
                                                }

                                            }
                                        }
                                        elseif($count > 0){

                                            $userByEmail = DB::table('users')->where('email', $user['email'])->where('draft', false)->first();
                                            $userRefAvailable = User::where('id', $userByEmail->id)->where('draft', false)->whereHas('references', function($q)  {
                                                                        $q->where('number', 'like', "%ABB-%");
                                                                })->count();
                                            if($userRefAvailable == 0){
                                                DB::table('reference')->insert(array(
                                                   'number' => $row[0],
                                                     'partner_id' => 4208,
                                                    'user_id'=>$userByEmail->id,
                                                    'created_at' => date('Y-m-d H:i:s', time()),
                                                    'updated_at' => date('Y-m-d H:i:s', time())
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => 4208,
                                                    'user_id' => $userByEmail->id
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => 1,
                                                    'user_id' => $last_id
                                                ));
                                                $errBool = False;

                                            }
                                            else{
                                                $email3 = $row[0].'@ABBCust.com';
                                                $user['email'] = $email3;
                                                $last_id = DB::table('users')->insertGetId($user);
                                                DB::table('role_user')->insert(array(
                                                    'role_id' => 4,
                                                    'user_id' => $last_id
                                                ));
                                                DB::table('reference')->insert(array(
                                                   'number' => $row[0],
                                                     'partner_id' => 4208,
                                                    'user_id'=>$last_id,
                                                    'created_at' => date('Y-m-d H:i:s', time()),
                                                    'updated_at' => date('Y-m-d H:i:s', time())
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => 4208,
                                                    'user_id' => $last_id
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => 1,
                                                    'user_id' => $last_id
                                                ));
                                                DB::table('segment_user')->insert(array(
                                                    'segment_id' => Partner::find(4208)->segments->first()->id,
                                                    'user_id' => $last_id
                                                ));
                                                
                                                //Default Tier affiliation to Member
                                                $defaultTier = Tier::where('partner_id', 4208)->where('name', 'like', 'Default Tier%')->first();
                                                if(!empty($defaultTier)){
                                                    $defaultTierCriteria = Tier::where('tier_id', $defaultTier->id)->where('name', 'like', 'Default Tier Criteria%')->first();
                                                    if(!empty($defaultTierCriteria)){
                                                        DB::table('tiercriteria_user')->insert(array(
                                                            'tiercriteria_id' => $defaultTierCriteria->id,
                                                            'user_id' => $last_id
                                                        ));
                                                    }
                                                }
                                                $errBool = False;
                                            }

                                        }
                                        elseif($countmobile > 0){
                                            $userByMobile = DB::table('users')->where('normalized_mobile', $user['normalized_mobile'])->where('draft', false)->first();
                                            $userRefAvailable = User::where('id', $userByMobile->id)->where('draft', false)->whereHas('references', function($q)  {
                                                                        $q->where('number', 'like', "%ABB-%");
                                                                })->count();
                                            if($userRefAvailable == 0){
                                                DB::table('reference')->insert(array(
                                                   'number' => $row[0],
                                                     'partner_id' => 4208,
                                                    'user_id'=>$userByMobile->id,
                                                    'created_at' => date('Y-m-d H:i:s', time()),
                                                    'updated_at' => date('Y-m-d H:i:s', time())
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => 4208,
                                                    'user_id' => $userByMobile->id
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => 1,
                                                    'user_id' => $last_id
                                                ));
                                                $errBool = False;
                                            }
                                            else{
                                                $tomobile = str_replace('ABB-','',$row[0]);
                                                $mobile = "4208". $tomobile;
                                                $user['mobile']           = $mobile;
                                                $user['normalized_mobile']= '+973'.$mobile;
                                                $user['telephone_code']   = '+973';
                                                $last_id = DB::table('users')->insertGetId($user);
                                                DB::table('role_user')->insert(array(
                                                    'role_id' => 4,
                                                    'user_id' => $last_id
                                                ));
                                                DB::table('reference')->insert(array(
                                                   'number' => $row[0],
                                                     'partner_id' => 4208,
                                                    'user_id'=>$last_id,
                                                    'created_at' => date('Y-m-d H:i:s', time()),
                                                    'updated_at' => date('Y-m-d H:i:s', time())
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => 4208,
                                                    'user_id' => $last_id
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => 1,
                                                    'user_id' => $last_id
                                                ));
                                                DB::table('segment_user')->insert(array(
                                                    'segment_id' => Partner::find(4208)->segments->first()->id,
                                                    'user_id' => $last_id
                                                ));
                                                //Default Tier affiliation to Member
                                                $defaultTier = Tier::where('partner_id', 4208)->where('name', 'like', 'Default Tier%')->first();
                                                if(!empty($defaultTier)){
                                                    $defaultTierCriteria = Tier::where('tier_id', $defaultTier->id)->where('name', 'like', 'Default Tier Criteria%')->first();
                                                    if(!empty($defaultTierCriteria)){
                                                        DB::table('tiercriteria_user')->insert(array(
                                                            'tiercriteria_id' => $defaultTierCriteria->id,
                                                            'user_id' => $last_id
                                                        ));
                                                    }
                                                }
                                                        
                                                $errBool = False;
                                            }
                                        }
                                    }
                                    else{
                                            $userByMobile = DB::table('users')->where('normalized_mobile', $user['normalized_mobile'])->where('draft', false)->first();
                                            $userRefAvailable = User::where('id', $userByMobile->id)->where('draft', false)->whereHas('references', function($q)  {
                                                                        $q->where('number', 'like', "%ABB-%");
                                                                })->count();
                                            if($userRefAvailable == 0){
                                                DB::table('reference')->insert(array(
                                                   'number' => $row[0],
                                                     'partner_id' => 4208,
                                                    'user_id'=>$userByMobile->id,
                                                    'created_at' => date('Y-m-d H:i:s', time()),
                                                    'updated_at' => date('Y-m-d H:i:s', time())
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => 4208,
                                                    'user_id' => $userByMobile->id
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => 1,
                                                    'user_id' => $last_id
                                                ));
                                                $errBool = False;
                                            }
                                            else{
                                                $tomobile = str_replace('ABB-','',$row[0]);
                                                $mobile = "4208". $tomobile;
                                                $user['mobile']           = $mobile;
                                                $user['normalized_mobile']= '+973'.$mobile;
                                                $user['telephone_code']   = '+973';
                                                $last_id = DB::table('users')->insertGetId($user);
                                                DB::table('role_user')->insert(array(
                                                    'role_id' => 4,
                                                    'user_id' => $last_id
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => 4208,
                                                    'user_id' => $last_id
                                                ));
                                                DB::table('partner_user')->insert(array(
                                                    'partner_id' => 1,
                                                    'user_id' => $last_id
                                                ));
                                                DB::table('segment_user')->insert(array(
                                                    'segment_id' => Partner::find(4208)->segments->first()->id,
                                                    'user_id' => $last_id
                                                ));
                                                //Default Tier affiliation to Member
                                                $defaultTier = Tier::where('partner_id', 4208)->where('name', 'like', 'Default Tier%')->first();
                                                if(!empty($defaultTier)){
                                                    $defaultTierCriteria = Tier::where('tier_id', $defaultTier->id)->where('name', 'like', 'Default Tier Criteria%')->first();
                                                    if(!empty($defaultTierCriteria)){
                                                        DB::table('tiercriteria_user')->insert(array(
                                                            'tiercriteria_id' => $defaultTierCriteria->id,
                                                            'user_id' => $last_id
                                                        ));
                                                    }
                                                }
                                                $errBool = False;
                                                $counterrors = $counterrors +1;
                                                $errors[] = $row;
                                            }
                                        }
                                }
                                if($errBool == True){
                                    $counterrors = $counterrors +1;
                                    $errors[] = $row;
//                                    echo "<pre>1";
//                                    var_dump($errors);
//                                    exit();
                                }
                            }
                        }

                    }
                    $count++;
                }

                if($counterrors > 0){
                    $imploded_transactions = array();
                    foreach($errors as $err1Id){
                        $imploded_transactions[] = implode(',',$err1Id);
                    }
                    $imploded_arr_transactions = implode("\r\n", $imploded_transactions);
                    $path = public_path('uploads');
                    $file = '/ABBCustomer' . date("1ymd") . '.csv';
                    $filepath = $path . $file;
                    $myfile = fopen($filepath, "w") or die("Unable to open file!");
                    fwrite($myfile, implode("\r\n", $imploded_transactions));
                    fclose($myfile);

                    $filepath1 = url("/uploads". $file);
                    $html = "al Baraka Customer File Imported with Errors";
                    //$response1 =  self::emailattachement('adminit@bluloyalty.com',$filepath,'Cashback FNB',$html);
                     $data = array();
                     $data['html'] = $html;
                     
                     $response1 = Mail::send('emails.fnbemail', $data, function($message1) use ($imploded_arr_transactions, $filepath,$file, $counterrors) {
                            $filename = str_replace('/','',$file);
                            //$message1->attach(new Swift_Message_Attachment(file_get_contents($filepath), $filename, 'application/vnd.ms-excel'));
                            $message1->attachData($imploded_arr_transactions,$filename);
                            $message1->to(array('adminit@bluloyalty.com'))->subject("DEV Customer Import Al Baraka Bank - " . $counterrors);
                        });

                    AuditHelper::record(
                            4, $name1 ." was uploaded with errors by " . $auth_user->name
                    );
                    return self::respondWith(
                        200, 'NOT OK', $errors, 'errors'
                    );
                     //with('success','ABB Customers Imported Successfully!');
                }
                else{
                    NotificationController::sendEmailNotification("Al Baraka Customer File Import successful", 'BLU Points', Config::get('blu.email_from'), array(null), array('adminit@bluloyalty.com'), "DEV Customer Import Al Baraka Bank");
                    AuditHelper::record(
                            4, $name1 ." was uploaded by " . $auth_user->name
                    );
                    return self::respondWith(
                                200, "OK", $errors, 'errors'
                            );
                }


            }
            elseif(($ext == 'TXT' || $ext == 'txt') && strstr($name1,'ABBTransactions') && strcmp($this->user->api_key, "f96d7e8fbe3dfc4da3a50e0c131f3038")==0 ){
                //Reference ID|PostingDate|Product ID|BonusPts|Billing Amount|Type|MCC|
                //  0               1           2           3       4           5   6 
                //Billing Currency|Transaction currency|Transaction Amount|Transaction Sign|Line number
                //        7               8                       9               10              11
                $tx = array();
                $count=0;
                $counterrors =0;
                $errBool = True;
                $errors = array();
                $params = array();
                $passes = 0;
                //$counterrors = 0;
                Input::file('file')->move($path, $name);
                $inputFile = $path.'/'.$name;
                $contents = File::get($inputFile);
                $contents = nl2br($contents,FALSE);
                $contentLines = explode("<br>", $contents);
                $errors = array();
                
                
                $countContentLines = count($contentLines) - 1;
                $test = $contentLines[$countContentLines];
                $test1 = $contentLines[$countContentLines - 1 ];
                $ret_eof_error = true;
                if(trim($test) === "EOF"){
                    $ret_eof_error = false;
                }
                elseif(trim($test1) === "EOF"){
                    $ret_eof_error = false;
                }
                if($ret_eof_error == true){
                 return self::respondWith(
                        200, 'EOF NOT FOUND', $errors, 'errors'
                    );
                }
                $arrContent = array();
                foreach ($contentLines as $lineread){
                    if(trim($lineread) !=='EOF'){
                        $arrContent[] = explode("|", $lineread);
                    }
                }
                $partner= $this->partner;
                $network_id = $partner->firstNetwork()->id;
                $partner_id = $partner->id;
                $partners = $partner->managedObjects();
                $partners_children = array();
                foreach ($partners as $part) {
                    $partners_children[] = $part->id;
                }
                $loy_by_partner = RewardHelper::loyaltyByPartner($partners_children);

                $partner_prefix = $partner->prefix;
                $itemchosen = ProductPartnerReward::where('partner_id', $partner_id)->get();
                $arr_item_chosen = array();
                foreach ($itemchosen as $ic) {
                    $arr_item_chosen[$ic->partner_mapping_id] = $ic->product_id;
                }

                
                $transactions_file = Transaction::select('ref_number')->where('ref_number', 'like', $name1 . "%")->get();
                $arr_transactions_file = $transactions_file->toArray();
                $arr_currencies = array();
                $currencies = Currency::all();
                foreach ($currencies as $curr) {
                    $arr_currencies[$curr->short_code] = $curr->id;
                }
                if (count($arr_transactions_file) > 0) {
                    return self::respondWith(
                                    200, 'NOT OK File Already Exists', null, 'errors'
                    );
                }
                
                $store_res = $partner->stores->first();
                if($store_res){
                    $pos_id = $store_res->pointOfSales->first()->id;
                    $tx['store_id'] = $store_res->id;
                    $tx['pos_id'] = $pos_id;
                }
                
                foreach($arrContent as $row){
                    $lineNumber = 0;
                    $posted_at = date('Y-m-d H:i:s');

                    if($count != 0){
                        if(trim($row[0]) != '' && trim($row[0]) != 'EOF'){
                            
                            $unhandled_date = explode("/",$row[1]);

                            if(!isset($row[11])){
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-001";
                                $errors[] = $row;
//                                echo $row[0] . "001";
//                                var_dump($row);
//                                exit();
                                continue;
                            }
                            elseif(substr(trim($row[0]), 0, 4) !== "ABB-" || trim($row[0]) === "" || $row[0] === " "){
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-002";
                                $errors[] = $row;
//                                echo $row[0] . "002";
//                                var_dump($row);
//                                exit();
                                continue;
                            }///self::respondwith('400', ,null);
                            elseif($row[1]=== "" || $row[1] === " "  || !checkdate($unhandled_date[1], $unhandled_date[0], $unhandled_date[2])){
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-003";
                                $errors[] = $row;
//                                echo $row[0] . "003";
//                                var_dump($row);
//                                exit();
                                continue;
                            }
                            elseif($row[2] === "" || $row[2] === " " || !is_numeric($row[2])){
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-004";
                                $errors[] = $row;
//                                echo $row[0] . "004";
//                                var_dump($row);
//                                exit();
                                continue;
                            }
                            elseif($row[3] === "" || $row[3] === " " || is_numeric($row[3]) === FALSE){
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-005";
                                $errors[] = $row;
//                                echo $row[0] . "005";
//                                var_dump($row);
//                                exit();
                                continue;
                            }
                            elseif($row[4]=== "" || $row[4] === " " || !is_numeric($row[4])){
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-006";
                                $errors[] = $row;
//                                echo $row[0] . "006";
//                                var_dump($row);
//                                exit();
                                continue;
                            }
                            elseif($row[9]=== "" || $row[9] === " " || !is_numeric($row[9])){
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-006.1";
                                $errors[] = $row;
//                                echo $row[0] . "007";
//                                var_dump($row);
//                                exit();
                                continue;
                            }
                            elseif($row[7]=== "" || $row[7] === " " || strlen($row[7])!== 3 ){
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-007";
                                $errors[] = $row;
//                                echo $row[0] . "008";
//                                var_dump($row);
//                                exit();
                                continue;
                            }
                            elseif($row[8]=== "" || $row[8] === " " || strlen($row[8])!== 3 ){
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-007.2";
                                $errors[] = $row;
//                                echo $row[0] . "009";
//                                var_dump($row);
//                                exit();
                                continue;
                            }
                            elseif($row[5]=== "" || $row[5] === " " || strlen($row[5])!== 2 ){
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-007.1";
                                $errors[] = $row;
//                                echo $row[0] . "010";
//                                var_dump($row);
//                                exit();
                                continue;
                            }
                            elseif($row[6]=== "" || $row[6] === " " ){
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-008";
                                $errors[] = $row;
//                                echo $row[0] . "011";
//                                var_dump($row);
//                                exit();
                                continue;
                            }
                            elseif(trim($row[10]) !=="C" && trim($row[10]) !=="D"){
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-010";
                                $errors[] = $row;
//                                echo $row[0] . "012";
//                                var_dump($row);
//                                exit();
                                continue;
                            }
                            
                            elseif(!is_numeric(trim($row[11]))){
                                $errBool = true;
                                $counterrors = $counterrors + 1;
                                $row['errornum'] = $counterrors . "-009";
                                $errors[] = $row;
//                                echo $row[0] . "012";
//                                var_dump($row);
//                                exit();
                                continue;
                            }
                            
                            if(isset($row[11])){
                                $lineNumber = $row[11];
                            }
                            $ref_number = $name1 . $lineNumber;
                            $ref_number = rtrim($ref_number);
//                                echo "<pre>";
//                                echo $name1 . $lineNumber . "<br>";
//                                var_dump($row);
//                                var_dump($errors);
//                                exit();
//                            $transact = Transaction::where('ref_number',$ref_number)->first();
                            
                            if(array_search($ref_number, array_column($arr_transactions_file, 'ref_number')) === FALSE){
                                $coefficient = 1;
                                if($row[10] == 'C'){
                                    $coefficient = intval(-1);
                                }
                                $row[0] = trim($row[0]);
                                $tx['id']			= 'NULL';
                                $tx['ref_number']		= $ref_number;
                                
                                $tx['original_total_amount']    = $row[9] * $coefficient;
                                $tx['partner_amount']           = $row[4] * $coefficient;
                                $tx['partner_trxtype']          = $row[5];
                                $tx['mcc']                      = $row[6];
                                
                                
                                $tx['user_id']			= 1;
                                $tx['points_redeemed']	= 0;
                                $tx['points_rewarded']	= 0;
                                $tx['partner_id']		= $partner->id;
                                $tx['network_id']		= $network_id;
                                $tx['trx_reward']		= 1;

                                $reference = Reference::where('number', $row[0])->first();
                                if(isset($reference->user_id)){
                                    $user = $reference->user_id;
                                }
                                else{
                                    $errBool = true;
                                    $counterrors = $counterrors + 1;
                                    $errors[] = $row;
                                    continue;
                                }
                                
                                $tx['user_id']=$user;
                                
                                $userInfo = User::find($user);
                                if($userInfo->status == 'closed'){
                                    $errBool = true;
                                    $counterrors = $counterrors + 1;
                                    $errors[] = $row;
                                    continue;
                                }
//                                $unhandledDate = $row[1];
//                                $arrunhandledDate = explode("/",$unhandledDate);
                                $date_unhandled = $unhandled_date[1] . "/" . $unhandled_date[0] . "/" . $unhandled_date[2];
                                //$date_unhandled = $row[1];
                                $date = strtotime($date_unhandled);
                                $new_date = date('Y-m-d H:i:s', $date);

                                $tx['created_at']      = $new_date;
                                $tx['updated_at']      = $new_date;
                                $tx['posted_at']       = $posted_at;
                                // Map Currency
                                
                                

//                                $loyalty = Loyalty::where('partner_id',$tx['partner_id'])->first();
                                
                                //$itemchosen = ProductPartnerReward::where('partner_mapping_id',$partner_prefix.$row[2])->first();
                                if(isset($arr_item_chosen[$partner_prefix.$row[2]])){
                                    $tx['product_id'] = $arr_item_chosen[$partner_prefix.$row[2]];
                                }
                                else{
                                    $tx['product_id'] = NULL;
                                }
                                
                                $validator = Validator::make($tx, array(
                                    //'id'         => 'required|numeric',
                                    'partner_id' => 'required|numeric',
                                    'store_id'   => 'required|numeric',
                                    'user_id'    => 'required|numeric'
                                ));

                                if(!$validator->fails()) {




                                    if(is_numeric($row[3])){
                                        $txbonus = array(
                                            'partner_id' => $tx['partner_id'],
                                            'store_id' => $tx['store_id'],
                                            'pos_id' => $tx['pos_id'],
                                            'user_id' => $tx['user_id'],
                                            'ref_number' => "Al Baraka Bank ONLINE Bonus POINTS",
                                            'total_amount' => 0,
                                            'points_rewarded' => round($row[3],0,PHP_ROUND_HALF_UP) * $coefficient,
                                            'amt_reward' => PointsHelper::pointsToAmount(round($row[3],0,PHP_ROUND_HALF_UP),"BHD" ,$network_id) * $coefficient,
                                            'trx_reward' => 1,
                                            'currency_id' => 6,
                                            'exchange_rate' => 1,
                                            'country_id' => 8,
                                            'quantity' => 0,
                                            'notes' => 'Bonus Points',
                                            'trans_class' => "Amount",
                                            'network_id' => $network_id,
                                            'created_at' => $new_date,
                                            'updated_at' => $new_date,
                                            'posted_at' => $posted_at
                                        );

                                        if($txbonus['points_rewarded'] != 0){
                                            DB::table('transaction')->insert($txbonus);
                                            $user = User::find($txbonus['user_id']);
                                            $user->updateBalance($row[3], $txbonus['network_id'], 'add');
                                            $partner  = Partner::find($txbonus['partner_id']);
                                        }

                                    }
                                    
                                        $passes++;

                                        if(is_numeric($tx['points_rewarded'])  ){

                                            $params = array(
                                                'user_id'		=> $tx['user_id'],
                                                'partner'		=> $tx['partner_id'],
                                                'pos_id'		=> $tx['pos_id'],
                                                'store_id'	=> $tx['store_id'],
                                                'amount'		=> floatval($tx['partner_amount']),
                                                'currency'	=> $row[8],
                                                //'notes'		=> 'Reward By Product',
                                                'type'		=> 'reward',
                                                'reference'	=> $tx['ref_number'],
                                                'network_id'	=> $partner->firstNetwork()->id,
                                                'product_id'    => $tx['product_id'],
                                                'partner_currency' => $row[7],
                                                'original_transaction_amount' => floatval($tx['original_total_amount']),
                                                'use_partner_currency'=> True,
                                                'mcc'=> $tx['mcc'],
                                                'partner_trxtype'=> $tx['partner_trxtype'],
                                                'created_at' => $new_date,
                                                'updated_at' => $new_date,
                                                'posted_at' => $posted_at
                                                
                                              );
                                            
                                            $user = User::find($tx['user_id']);
                                            try{
                                                $result_do_reward = RewardHelper::doRewardNewLoyalty($params, 'Amount',$loy_by_partner);
                                            } catch (Exception $ex) {
                                                //$resp = array($ex);
                                                $json_data = json_encode(array("Exception" => $ex));
                                                APIController::postSendEmailJson($json_data);
                                            }
                                        }

                                }
                            }
                        }

                    }

                    $count++;
                }

                if($counterrors > 0){
                    $imploded_transactions = array();
                    foreach($errors as $err1Id){
                        $imploded_transactions[] = implode(',',$err1Id);
                    }
                    $imploded_arr_transactions = implode("\r\n", $imploded_transactions);
                    $path = public_path('uploads');
                    $file = '/ABBTransactions' . date("1ymd") . '.csv';
                    $filepath = $path . $file;
                    $myfile = fopen($filepath, "w") or die("Unable to open file!");
                    fwrite($myfile, $imploded_arr_transactions);
                    fclose($myfile);

                    $filepath1 = url("/uploads". $file);
                    $html = "Transaction File Imported with Errors";
                    //$response1 =  self::emailattachement('adminit@bluloyalty.com',$filepath,'Cashback FNB',$html);
                     $data = array();
                     $data['html'] = $html;
                     $sendgrid = new SendGrid(
                        Config::get('sendgrid.username'), Config::get('sendgrid.password')
                    );
                    
                    $mail = new SendGrid\Email();

                    $mail->setTos(array('adminit@bluloyalty.com'));
                    $mail->setFrom(Config::get('blu.email_from'));
                    $mail->setFromName('BLU Points');
                    $mail->setSubject("Transaction Import Al Baraka - Error Count: " . $counterrors);
                    $mail->setHtml($html);
                    
                    //$returned_error_file = "Errors_". $name1;
                    $mail->setAttachment(addslashes($imploded_arr_transactions), str_replace("/", "", $file));

                    $sendgrid->send($mail);
//                     $response1 = Mail::send('emails.fnbemail', $data, function($message1) use ($imploded_arr_transactions, $filepath,$file, $counterrors) {
//                            $filename = str_replace('/','',$file);
//                            //$message1->attach(new Swift_Message_Attachment(file_get_contents($filepath), $filename, 'application/vnd.ms-excel'));
//                            $message1->attachData($imploded_arr_transactions,$filename);
//                            $message1->to(array('adminit@bluloyalty.com'))->subject("Transaction Import Al Baraka - Error Count: " . $counterrors);
//                        });


                    return self::respondWith(
                        200, 'NOT OK', $errors, 'errors'
                    );
                     //with('success','BISB Customers Imported Successfully!');
                }
                else{
                    $sendgrid = new SendGrid(
                        Config::get('sendgrid.username'), Config::get('sendgrid.password')
                    );
                    $mail = new SendGrid\Email();

                    $mail->setTos(array('adminit@bluloyalty.com'));
                    $mail->setFrom(Config::get('blu.email_from'));
					$mail->setFromName('BLU Points');
                    $mail->setSubject("Transaction Import Al Baraka");
                    $mail->setHtml("Transaction File Import successful - number of transactions: " . $count);

                    $sendgrid->send($mail);
                    return self::respondWith(
                                200, "OK", $errors, 'errors'
                            );
                }
            
            }
            elseif(($ext == 'TXT' || $ext == 'txt') && strstr($name1,'ABBChanges') && strcmp($this->user->api_key, "f96d7e8fbe3dfc4da3a50e0c131f3038cccc")==0 ){
//                RefID|New Mobile Number|New Email|New Status
//                  0  |        1        |    2    |    3
                
                $auth_user = Auth::User();
                AuditHelper::record(
                        4,$auth_user->name  ." started uploading file: " . $name1
                );
                
                $tx = array();
                Input::file('file')->move($path, $name);
                $inputFile = $path.'/'.$name;
                $contents = File::get($inputFile);
                $contents = nl2br($contents,FALSE);
                $contentLines = explode("<br>", $contents);
                $arrContent = array();
                foreach ($contentLines as $lineread){
                    $arrContent[] = explode("|", $lineread);
                }
                $errIDs = array();
                $count=0;
                $errCount = 0;
                foreach($arrContent as $row){
                    if($count != 0){
                        if(trim($row[0]) != '' && $row[0] != "EOF"){
                            $row[0] = trim($row[0]);
                            $reference = Reference::where('number', $row[0])->first();
                            if(isset($reference)){
                            $user = $reference->user_id;
                            $mobile = $row[1];
                            $userChange = array();
                            if($mobile != 'Not Changed' && is_numeric($mobile)){
                                $arr_countries = array("441624", "441534", "441481", "35818", "1876", "1869", "1868", "1809", "1787", "1784", "1767", "1758", "1684", "1671", "1670", "1664", "1649", "1473", "1441", "1345", "1340", "1284", "1268", "1264", "1246", "1242", "998", "996", "995", "994", "993", "992", "977", "976", "975", "974", "973", "972", "971", "970", "968", "967", "966", "965", "964", "963", "962", "961", "960", "886", "880", "870", "856", "855", "853", "852", "850", "692", "691", "690", "689", "688", "687", "686", "685", "683", "682", "681", "680", "679", "678", "677", "676", "675", "674", "673", "672", "670", "599", "599", "599", "598", "597", "596", "595", "594", "593", "592", "591", "590", "590", "590", "509", "508", "507", "506", "505", "504", "503", "502", "501", "500", "423", "421", "420", "389", "387", "386", "385", "382", "381", "380", "379", "378", "377", "376", "375", "374", "373", "372", "371", "370", "359", "358", "357", "356", "355", "354", "353", "352", "351", "350", "299", "298", "297", "291", "290", "269", "268", "267", "266", "265", "264", "263", "262", "262", "261", "260", "258", "257", "256", "255", "254", "253", "252", "251", "250", "249", "248", "246", "245", "244", "243", "242", "241", "240", "239", "238", "237", "236", "235", "234", "233", "232", "231", "230", "229", "228", "227", "226", "225", "224", "223", "222", "221", "220", "218", "216", "213", "212", "212", "211", "98", "95", "94", "93", "92", "91", "90", "86", "84", "82", "81", "66", "65", "64", "63", "62", "61", "61", "61", "60", "58", "57", "56", "55", "54", "53", "52", "51", "49", "48", "47", "47", "46", "45", "44", "43", "41", "40", "39", "36", "34", "33", "32", "31", "30", "27", "20", "7", "7", "1", "1", "1");
                                foreach($arr_countries as $ctry){
                                    if(substr($mobile, 0, strlen($ctry)) === $ctry){
                                        $mobile = substr($mobile,strlen($ctry));
                                        $ismobileUnique = User::where('mobile',$mobile)->where('id', '!=', $user)->count();
                                        if($ismobileUnique > 0){
                                            $errCount = $errCount +1;
                                            $errIDs[] = $row;
                                            continue;
                                        }
                                        $userChange['mobile']           = $mobile;
                                        $userChange['normalized_mobile']= "+" . $ctry . $mobile;
                                        $userChange['telephone_code']   = '+' . $ctry;
                                        break;
                                    }
                                }
                            }
                            
                            if(!isset($userChange['mobile']) && $mobile != 'Not Changed' ){
                                $errCount = $errCount +1;
                                $errIDs[] = $row;
                            }
                            
                            if($row[2] != 'Not Changed'){
                                $userChange['email']            = $row[2];
                            }
                            
                            $userOb = User::find($user);
                            
                            if($row[3] != '' && $row[3] != 'Not Changed'){
                                if($row[3] == 'C'){
                                    $userChange['status'] = "closed";
                                }
                                elseif($row[3]== 'S'){
                                    $userChange['status'] = "suspended";
                                }
                                elseif($row[3]== 'A'){
                                    $userChange['status'] = "active";
                                }
                            }else{
                                $userChange['status'] = $userOb->status;
                            }
                              
                            if( $userChange['status'] != $userOb->status && $row[3] != 'Not Changed'){
                                $closeUserAllowed = true;
                                foreach($userOb->partners as $part){
                                    // can not set status as closed for a user that has another partners
                                    if($part->id != 4208 && $part->id != 1){
                                        $closeUserAllowed =false;
                                    }
                                }  
                                if($userOb->status == "closed"){
                                    $closeUserAllowed =false;
                                }
//                                    $errIDs[] ="Partner is not allowed to change the status of this user as it is active for other partners"; 
//                                $errIDs[] = $row;
                                
                                if(!$closeUserAllowed ){
                                    $errIDs[] = $row;
                                }else{
                                    $updateChange = User::where('id',$user)->update($userChange);
                                }
                            }else{
                                 $updateChange = User::where('id',$user)->update($userChange);
                            }
                        }else{
                            $errCount = $errCount +1;
                            $errIDs[] = $row;
                        }
                    }

                    }
                    $count++;
                }

                AuditHelper::record(
                        4, $name1 ." was uploaded by " . $auth_user->name
                );
                return self::respondWith(
                    200, "OK", $errIDs, 'errors'
                ); //with('success','ABB Customers Imported Successfully!');

            }
            elseif (($ext == 'TXT' || $ext == 'txt') && strstr($name1, 'FNB1') && strcmp($this->user->api_key, "a4e5575828c097cadca66a40e8a32f75") == 0) {

                AuditHelper::record(
                        4, $auth_user->name . " started uploading file: " . $name1
                );
                $tx = array();
                $count = 0;
                $errCount = 0;
                $errBool = True;
                $errIDs = array();
                $params = array();
                $passes = 0;
                Input::file('file')->move($path, $name);
                $inputFile = $path . '/' . $name;
                $contents = File::get($inputFile);
                $contents = nl2br($contents, FALSE);
                $contentLines = explode("<br>", $contents);

                $countContentLines = count($contentLines) - 1;
                $test = $contentLines[$countContentLines];
                $test1 = $contentLines[$countContentLines - 1];
                $ret_eof_error = true;
                if (trim($test) === "EOF") {
                    $ret_eof_error = false;
                } elseif (trim($test1) === "EOF") {
                    $ret_eof_error = false;
                }
                $err_res = array();
                $err_res[] = "File Not Uploaded Entirely, Please upload it again.";
                if ($ret_eof_error === true) {
                    return self::respondWith(
                                    402, null, $err_res, 'error_res'
                    );
                }
                $arrContent = array();
                foreach ($contentLines as $lineread) {
                    $arrContent[] = explode(",", $lineread);
                }
                $partner_id = 2020;
                $partner = Partner::find($partner_id);
                $partners = $partner->managedObjects();
                $partners_children = array();
                foreach ($partners as $part) {
                    $partners_children[] = $part->id;
                }
                $loy_by_partner = RewardHelper::loyaltyByPartner($partners_children);

                $partner_prefix = $partner->prefix;
                $itemchosen = ProductPartnerReward::where('partner_id', $partner_id)->get();
                $arr_item_chosen = array();
                foreach ($itemchosen as $ic) {
                    $arr_item_chosen[$ic->partner_mapping_id] = $ic->product_id;
                }

                $network_id = $partner->firstNetwork()->id;
                $transactions_file = Transaction::select('ref_number')->where('ref_number', 'like', $name1 . "%")->get();
                $arr_transactions_file = $transactions_file->toArray();

//                $ref_number = $name1."1";
//                echo "<pre>";
//                var_dump(array_search($ref_number, array_column($arr_transactions_file, 'ref_number')));
//                exit();
                if (count($arr_transactions_file) > 0) {
                    return self::respondWith(
                                    200, 'NOT OK File Already Exists', null, 'errors'
                    );
                }

                if (!empty($arrContent[0][0])) {
                    $lineNumber = 0;
                    foreach ($arrContent as $row) {
                        if (trim($row[0]) != '' && trim($row[0]) != 'EOF') {
                            if (isset($row[8])) {
                                $lineNumber = intval($row[8]);
                            }
                            $ref_number = $name1 . $lineNumber;
                            $ref_number = rtrim($ref_number);

                            //$transact = Transaction::where('ref_number', $ref_number)->count();


                            if (array_search($ref_number, array_column($arr_transactions_file, 'ref_number')) === FALSE) {
                                $tx['id'] = 'NULL';
                                $tx['ref_number'] = $ref_number;
                                $tx['total_amount'] = $row[2];
                                $tx['store_id'] = '4446';
                                $tx['pos_id'] = '1016';
                                $product_id = trim($row[4]);
                                $tx['product_id'] = $arr_item_chosen[$partner_prefix . $product_id];
                                $ralphrow = array();
                                $ralphrow['store'] = rtrim($row[5]);
                                $ralphrow['city'] = rtrim($row[6]);
                                $ralphrow['iso'] = $row[7];
                                $ralphrow['subject'] = "Points Received";
                                $store_name = $ralphrow['store'] . " -- " . $ralphrow['city'];
                                $count_stores = Store::where('name', $store_name)->count();
                                //                                echo "$count_stores";
                                //                                exit();
                                $tx['notes'] = json_encode($ralphrow);
                                $refnum1 = ltrim($row[0], '0');
                                $refnum2 = trim($refnum1, ' ');
                                $refnum = str_replace("\r\n", "", $refnum2);
                                $ralphrow1 = array();
                                $ralphrow1['id'] = $refnum;
                                $ralphrow1['currency'] = $row[1];
                                $ralphrow1['amount'] = $row[2];
                                $ralphrow1['dateralph'] = $row[3];
                                $ralphrow1['product'] = $row[4];
                                $ralphrow1['branch'] = $row[5];
                                $ralphrow1['city'] = $row[6];
                                $ralphrow1['iso'] = $row[7];

                                $user = 0;
                                //echo is_numeric($refnum);
                                if (is_numeric($refnum)) {
                                    $userObj = User::where('id', $refnum)->first();
                                    if (!$userObj) {
                                        $errResult['001'][] = $ralphrow1;
                                        //                                        echo "<pre> 1: ";
                                        //                                        var_dump($errResult);
                                        //                                        exit();
                                        continue;
                                    }
                                    $user = $userObj->id;
                                } else {
                                    $reference = Reference::where('number', $refnum)->first();
                                    if (isset($reference->user_id)) {
                                        $user = $reference->user_id;
                                    } else {
                                        $errResult['002'][] = $ralphrow1;
                                        //                                        echo "<pre> 2: ";
                                        //                                        var_dump($errResult);
                                        //                                        //exit();
                                        continue;
                                    }
                                }

                                $tx['user_id'] = $user;
                                $tx['points_redeemed'] = 0;
                                $tx['partner_id'] = $partner_id;
                                $tx['trx_reward'] = 1;

                                $unhandledDate = $row[3];
                                $arrunhandledDate = explode("/", $unhandledDate);
                                $date_unhandled = $arrunhandledDate[1] . "/" . $arrunhandledDate[0] . "/" . $arrunhandledDate[2];

                                $date = strtotime($date_unhandled);
                                $new_date = date('Y-m-d H:i:s', $date);

                                $tx['created_at'] = $new_date;
                                $tx['updated_at'] = $new_date;
                                $tx['posted_at'] = date('Y-m-d H:i:s');
                                $country_id = Country::where('iso_code', $row[7])->first();
                                $tx['country_id'] = $country_id->id;
                                // Map Currency
                                if ($row[1]) {
                                    $currency = Currency::where('short_code', $row[1])->first();

                                    if ($currency) {
                                        $tx['currency_id'] = $currency->id;
                                    }

                                    $currencyRate = CurrencyPricing::where('currency_id', $row[1])->first();
                                    if ($currencyRate) {
                                        $tx['exchange_rate'] = floatval($currencyRate->rate);
                                    } else {
                                        $tx['exchange_rate'] = 1;
                                    }
                                }
                                if ($count_stores > 0) {
                                    $tx['total_amount'] = floatval($tx['total_amount']) * 2;
                                }





                                $validator = Validator::make($tx, array(
                                            //'id'         => 'required|numeric',
                                            'partner_id' => 'required|numeric',
                                            'store_id' => 'required|numeric',
                                            'user_id' => 'required|numeric'
                                ));
                                if (!$validator->fails()) {

                                    $params = array(
                                        'user_id' => $tx['user_id'],
                                        'amount' => $tx['total_amount'],
                                        'currency' => $row[1],
                                        'partner' => $tx['partner_id'],
                                        'partner_currency' => $row[1],
                                        'store_id' => $tx['store_id'], // Optional
                                        'pos_id' => $tx['pos_id'], // Optional
                                        'notes' => $tx['notes'], // Optional
                                        'created_at' => $tx['created_at'],
                                        'updated_at' => $tx['posted_at'],
                                        'posted_at' => $tx['posted_at'],
                                        'reference' => $tx['ref_number'],
                                        'product_id' => $tx['product_id'],
                                        'country_id' => $tx['country_id']
                                    );
//                                    self::postSendEmailJson(json_encode($params), 'FNB Params - ' . $tx['ref_number']);
                                    $output_ralph = RewardHelper::doRewardNewLoyalty($params, 'Amount', $loy_by_partner);
                                } else {
                                    $errResult['003'][] = $ralphrow1;
                                    //                                    echo "<pre> 3: ";
                                    //                                        var_dump($errResult);
                                    //                                        exit();
                                }


                                //                        if(!$validator->fails()) {
                                //                            DB::table('transaction')->insert($tx);
                                //                        }
                            }
//                    echo "<pre>";
//                    var_dump($tx);
//                    exit();
                        }
                    }
                    AuditHelper::record(
                            4, $name1 . " was uploaded by " . $auth_user->name
                    );

                    return self::respondWith(
                                    200, "OK", $errResult, 'errors'
                    );
                }

                AuditHelper::record(
                        4, $name1 . " was uploaded with errors by " . $auth_user->name
                );
                return self::respondWith(
                                401, "File Empty", $errResult, 'errors'
                );
            }
            elseif (($ext == 'TXT' || $ext == 'txt') && strstr($name1, 'CEDTransactions') && strcmp($this->user->api_key, "529480800b15bb51906fd0773e06a731") == 0) {
                ignore_user_abort(TRUE);
                AuditHelper::record(
                        4, $auth_user->name . " started uploading file: " . $name1
                );
                $tx = array();
                $count = 0;
                $errCount = 0;
                $errBool = True;
                $errIDs = array();
                $params = array();
                $passes = 0;
                Input::file('file')->move($path, $name);
                $inputFile = $path . '/' . $name;
                $contents = File::get($inputFile);
                $contents = nl2br($contents, FALSE);
                $contentLines = explode("<br>", $contents);

                $countContentLines = count($contentLines) - 1;
                $test = $contentLines[$countContentLines];
                $test1 = $contentLines[$countContentLines - 1];
                $ret_eof_error = true;
                if (trim($test) === "EOF") {
                    $ret_eof_error = false;
                } elseif (trim($test1) === "EOF") {
                    $ret_eof_error = false;
                }
                if ($ret_eof_error == true) {
                    return self::respondWith(
                                    200, 'EOF NOT FOUND', $errIDs, 'errors'
                    );
                }
                $arrContent = array();
                foreach ($contentLines as $lineread) {
                    $arrContent[] = explode("|", $lineread);
                }
                $partner_id = 4173;
                $partner = Partner::find($partner_id);
                $partners = $partner->managedObjects();
                $partners_children = array();
                foreach ($partners as $part) {
                    $partners_children[] = $part->id;
                }
                $loy_by_partner = RewardHelper::loyaltyByPartner($partners_children);

                $partner_prefix = $partner->prefix;
                $itemchosen = ProductPartnerReward::where('partner_id', '4173')->get();
                $arr_item_chosen = array();
                foreach ($itemchosen as $ic) {
                    $arr_item_chosen[$ic->partner_mapping_id] = $ic->product_id;
                }
                $network_id = $partner->firstNetwork()->id;
                $transactions_file = Transaction::select('ref_number')->where('ref_number', 'like', $name1 . "%")->get();
                $arr_transactions_file = $transactions_file->toArray();

//                $ref_number = $name1."1";
//                echo "<pre>";
//                var_dump(array_search($ref_number, array_column($arr_transactions_file, 'ref_number')));
//                exit();
                if (count($arr_transactions_file) > 0) {
                    return self::respondWith(
                                    200, 'NOT OK File Already Exists', null, 'errors'
                    );
                }

                foreach ($arrContent as $row) {
                    $lineNumber = 0;
                    if ($count != 0) {
                        if (trim($row[0]) != '' && trim($row[0]) != 'EOF') {

                            if (isset($row[6])) {
                                $lineNumber = $row[6];
                            }
                            $ref_number = $name1 . $lineNumber;
                            $ref_number = rtrim($ref_number);

                            //$transact = Transaction::where('ref_number', $ref_number)->count();


                            if (array_search($ref_number, array_column($arr_transactions_file, 'ref_number')) === FALSE) {

                                $row[0] = trim($row[0]);
                                $product_id = $row[2];
                                $tx['id'] = 'NULL';
                                $tx['ref_number'] = $ref_number;
                                $tx['total_amount'] = $row[4];
                                $tx['original_total_amount'] = $row[4];
                                $tx['partner_amount'] = $row[4];
                                $tx['store_id'] = '4898';
                                $tx['pos_id'] = '1005';
                                $tx['user_id'] = 1;
                                $tx['points_redeemed'] = 0;
                                $tx['points_rewarded'] = 0;
                                $tx['partner_id'] = '4173';
                                $tx['network_id'] = 1;
                                $tx['trx_reward'] = 1;

                                $reference = Reference::where('number', $row[0])->first();
                                if (isset($reference->user_id)) {
                                    $user = $reference->user_id;
                                } else {
                                    $errBool = true;
                                    $errCount = $errCount + 1;
                                    $errIDs[] = $row;
                                    continue;
                                }


                                if (isset($arr_item_chosen[$partner_prefix . $product_id])) {
                                    $tx['product_id'] = $arr_item_chosen[$partner_prefix . $product_id];
                                } else {
                                    $tx['product_id'] = NULL;
                                }

                                $tx['user_id'] = $user;

                                $userInfo = User::find($user);
                                if ($userInfo->status == 'closed') {
                                    $errBool = true;
                                    $errCount = $errCount + 1;
                                    $errIDs[] = $row;
                                    continue;
                                }

                                $unhandledDate = $row[1];
                                $arrunhandledDate = explode("/", $unhandledDate);
                                $date_unhandled = $arrunhandledDate[1] . "/" . $arrunhandledDate[0] . "/" . $arrunhandledDate[2];

                                $date = strtotime($date_unhandled);
                                $new_date = date('Y-m-d H:i:s', $date);

                                $tx['created_at'] = $new_date;
                                $tx['updated_at'] = $new_date;
                                $tx['posted_at'] = date('Y-m-d H:i:s');
                                // Map Currency
                                if ($row[5]) {
                                    $currency = Currency::where('short_code', $row[5])->first();
                                    $tx['currency_id'] = 6;
                                    if ($currency) {
                                        $tx['currency_id'] = $currency->id;
                                        $tx['partner_currency_id'] = $currency->id;
                                    }

                                    $currencyRate = CurrencyPricing::where('currency_id', $tx['currency_id'])->orderBy('id', 'DESC')->first();
                                    if ($currencyRate) {
                                        $tx['exchange_rate'] = floatval($currencyRate->rate);
                                        $tx['total_amount'] = $tx['total_amount'] / $tx['exchange_rate'];
                                    } else {
                                        $tx['exchange_rate'] = 1;
                                    }
                                }





                                $validator = Validator::make($tx, array(
                                            //'id'         => 'required|numeric',
                                            'partner_id' => 'required|numeric',
                                            'store_id' => 'required|numeric',
                                            'user_id' => 'required|numeric'
                                ));

                                if (!$validator->fails()) {

                                    //ReferenceID|Postingdate|Card type|Bonuspts|Amount|Currency
                                    //  0           1           2           3       4       5

                                    if (is_numeric($row[3])) {
                                        $txbonus = array(
                                            'partner_id' => $tx['partner_id'],
                                            'store_id' => $tx['store_id'],
                                            'pos_id' => $tx['pos_id'],
                                            'user_id' => $tx['user_id'],
                                            'ref_number' => "CEDRUS ONLINE Bonus POINTS",
                                            'total_amount' => 0,
                                            'points_rewarded' => round($row[3], 0, PHP_ROUND_HALF_UP),
                                            'trx_reward' => 1,
                                            'rule_id' => 5171,
                                            'currency_id' => 6,
                                            'exchange_rate' => 1,
                                            'country_id' => 6,
                                            'quantity' => 0,
                                            'notes' => 'Bonus Points',
                                            'trans_class' => "Amount",
                                            'network_id' => 1,
                                            'created_at' => $new_date,
                                            'updated_at' => $new_date,
                                            'posted_at' => $tx['posted_at']
                                        );
                                        if ($txbonus['points_rewarded'] != 0) {
                                            DB::table('transaction')->insert($txbonus);

                                            $userInfo->updateBalance($row[3], $txbonus['network_id'], 'add');
                                            $partner = Partner::find($txbonus['partner_id']);

//                                            if (Transaction::where('user_id', $tx['user_id'])->where('ref_number', 'NOT LIKE', 'Transfer%')->count() == 1) {
//                                                //$userSMS = User::find($tx['user_id']);
//                                                $custRef = $row[0];
//                                                $template = View::make('sms.new-user-cedrus', array('ref' => $custRef))->render();
//                                                //$partner = Partner::find('4173');
//                                                SMSHelper::send($template, $userInfo, $partner);
//                                            }
                                        }
                                    }
                                    //$partner = Partner::find('4173');
                                    $params = array(
                                        'user_id' => $tx['user_id'],
                                        'amount' => $tx['original_total_amount'],
                                        'currency' => $row[5],
                                        'partner_currency' => $row[5],
                                        'partner' => $tx['partner_id'],
                                        'store_id' => $tx['store_id'], // Optional
                                        'pos_id' => $tx['pos_id'], // Optional
                                        'notes' => "Points Received", // Optional
                                        'created_at' => $tx['created_at'],
                                        'network_id' => $network_id,
                                        'updated_at' => $tx['updated_at'],
                                        'posted_at' => $tx['posted_at'],
                                        'reference' => $tx['ref_number'],
                                        'product_id' => $tx['product_id']
                                    );

                                    //Transaction::doRewardNewLoyalty($params, 'Amount');
                                    $output_ralph = RewardHelper::doRewardNewLoyalty($params, 'Amount', $loy_by_partner);
                                    $passes++;

                                    if (Transaction::where('user_id', $tx['user_id'])->where('ref_number', 'NOT LIKE', 'Transfer%')->count() == 1) {
                                        //$userSMS = User::find($tx['user_id']);
                                        $custRef = $row[0];
                                        $template = View::make('sms.new-user-cedrus', array('ref' => $custRef))->render();
                                        //$partner = Partner::find('4173');
                                        SMSHelper::send($template, $userInfo, $partner);
                                    }
                                    //$arr_result = Transaction::doReward($params, 'Amount');
                                }
                            }
                        }
                    }

                    $count++;
                }

                if ($errCount > 0) {
                    $imploded_transactions = array();
                    foreach ($errIDs as $err1Id) {
                        $imploded_transactions[] = implode(',', $err1Id);
                    }
                    $imploded_arr_transactions = implode("\r\n", $imploded_transactions);
                    $path = public_path('uploads');
                    $file = '/CEDTransactions' . date("1ymd") . '.csv';
                    $filepath = $path . $file;
                    $myfile = fopen($filepath, "w") or die("Unable to open file!");
                    fwrite($myfile, $imploded_arr_transactions);
                    fclose($myfile);

                    $filepath1 = url("/uploads" . $file);
                    $html = "Transaction File Imported with Errors";
                    //$response1 =  self::emailattachement('adminit@bluloyalty.com',$filepath,'Cashback FNB',$html);
//                     $data = array();
//                     $data['html'] = $html;
//                     $response1 = Mail::send('emails.fnbemail', $data, function($message1) use ($imploded_arr_transactions, $filepath,$file, $errCount) {
//                            $filename = str_replace('/','',$file);
//                            //$message1->attach(new Swift_Message_Attachment(file_get_contents($filepath), $filename, 'application/vnd.ms-excel'));
//                            $message1->attachData($imploded_arr_transactions,$filename);
//                            $message1->to(array('adminit@bluloyalty.com'))->subject("Transaction Import Cedrus Bank - " . $errCount);
//                        });
//
//                    var_dump($imploded_arr_transactions);
//                    exit();
                    $sendgrid = new SendGrid(
                            Config::get('sendgrid.username'), Config::get('sendgrid.password')
                    );

                    $mail = new SendGrid\Email();

                    $mail->setTos(array('adminit@bluloyalty.com'));
                    $mail->setFrom(Config::get('blu.email_from'));
                    $mail->setFromName('BLU Points');
                    $mail->setSubject("Transaction Import Cedrus Bank - " . $errCount);
                    $mail->setHtml("Transaction File Imported with Errors");

                    //$returned_error_file = "Errors_". $name1;
                    $mail->setAttachment(addslashes($imploded_arr_transactions), str_replace("/", "", $file));

                    $sendgrid->send($mail);

                    AuditHelper::record(
                            4, $name1 . " was uploaded with errors by " . $auth_user->name
                    );
                    return self::respondWith(
                                    200, 'NOT OK', $errIDs, 'errors'
                    );
                    //with('success','CED Customers Imported Successfully!');
                } else {
                    $sendgrid = new SendGrid(
                            Config::get('sendgrid.username'), Config::get('sendgrid.password')
                    );
                    $mail = new SendGrid\Email();

                    $mail->setTos(array('adminit@bluloyalty.com'));
                    $mail->setFrom(Config::get('blu.email_from'));
                    $mail->setFromName('BLU Points');
                    $mail->setSubject("Transaction Import Cedrus Bank");
                    $mail->setHtml("Transaction File Import successful");

                    $sendgrid->send($mail);

                    AuditHelper::record(
                            4, $name1 . " was uploaded by " . $auth_user->name
                    );
                    return self::respondWith(
                                    200, "OK", $errIDs, 'errors'
                    );
                }
                ignore_user_abort(False);
            }

        }
            
            
    }
    
    public function getTest(){
        $test = Input::get('test');//$cpt,$username, $pin,$site
        $url = "https://bluprd.alfransi.com.sa/BLU/api.php?method=sensitiveInfo&api_key=1be83d735510848b88a08e43c2e114d9";
        $post_fields = 'user_ids='. $test ;
        
        $curl = curl_init();
        // Set some options - we are passing in a useragent too here
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $post_fields,
            CURLOPT_SSL_VERIFYPEER => False,
            CURLOPT_USERAGENT => 'Testing Sensitive Info'
        ));
        // Send the request & save response to $resp
        $resp = curl_exec($curl);
        
        if(curl_errno($curl)){
            var_dump(curl_error($curl));
            exit();
        }
	//curl_setopt($ch, CURLOPT_HEADER, 1);
        //var_dump($post_fields);
        echo "<pre>";
        var_dump($resp);
        exit();
    }
    
    public function postMiddlewareUploadFile()
    {
        $partner_id_parent = $this->partner->id;
        $ip = Request::getClientIp();
        $file = Input::file('file');
        $path = public_path('uploads');
        $ext  = $file->getClientOriginalExtension();
        $name1 = $file->getClientOriginalName();
        $name = md5(time()).".{$ext}";
        $result = FALSE;
        if(($ext == 'TXT' || $ext == 'txt') && strstr($name1,'BSFTransaction') && strcmp($this->user->api_key, "af08ea2bd3afbd900a29abbc8c818c4f") == 0){
            Input::file('file')->move($path, $name);
            $inputFile = $path.'/'.$name;
            $contents = File::get($inputFile);
            $lines = explode("\n", $contents);
            $count = 0;
            $errors = array();
            $regex = '/^[0-9]{1,2}\/[0-9]{1,2}\/[0-9]{4}$/';
            $counterrors = 0;
            $arr_imported = array();
            $arr_imported_bonus = array();
            $all_tx = array();
            $all_tx_bonus = array();
            $arr_csv_file = array();
            $partner = $this->partner;
            $partners = $partner->managedObjects();
            $partners_children = array();
            $partners_by_mapping_id = array();
            foreach ($partners as $part) {
                $partners_children[] = $part->id;
                $partners_by_mapping_id[$part->parent_partner_mapping_id]['partner_id'] = $part->id;
                $partners_by_mapping_id[$part->parent_partner_mapping_id]['store'] = $part->stores->first();
                $partners_by_mapping_id[$part->parent_partner_mapping_id]['store_id'] = $partners_by_mapping_id[$part->parent_partner_mapping_id]['store']->id;
                $partners_by_mapping_id[$part->parent_partner_mapping_id]['pos_id'] = $partners_by_mapping_id[$part->parent_partner_mapping_id]['store']->pointOfSales->first()->id;
            }
            $loy_by_partner = RewardHelper::loyaltyByPartner($partners_children);
            //self::postSendEmailJson(json_encode(array($partners_children,$loy_by_partner)));
            $partner_prefix = $partner->prefix;
            $itemchosen = ProductPartnerReward::whereIn('partner_id', $partners_children)->get();
            $arr_item_chosen = array();
            foreach ($itemchosen as $ic) {
                $arr_item_chosen[$ic->partner_mapping_id] = $ic->product_id;
            }
            $network_id = $partner->firstNetwork()->id;
            $transactions_file = Transaction::select('ref_number')->where('ref_number', 'like', $name1 . "%")->get();
            $arr_transactions_file = $transactions_file->toArray();
            $arr_currencies = array();
            $currencies = Currency::all();
            foreach ($currencies as $curr) {
                $arr_currencies[$curr->short_code] = $curr->id;
            }

            foreach ($lines as $line) {
                $content = explode("|",$line);
                if(trim($content[0]) !== "EOF" && $count !== 0){
                    if(!isset($content[1])){
                        continue;
                    }
                    $unhandled_date = explode('/', $content[1] );

                    if(!isset($content[7])){
                        $errBool = true;
                        $counterrors = $counterrors + 1;
                        $content['errornum'] = $counterrors . "-001";
                        $errors[] = $content;
                        $arr_csv_file[] = implode(',',$content);
                        continue;
                    }
                    elseif(substr(trim($content[0]), 0, 4) !== "BSF-" || trim($content[0]) === "" || $content[0] === " "){
                        $errBool = true;
                        $counterrors = $counterrors + 1;
                        $content['errornum'] = $counterrors . "-002";
                        $errors[] = $content;
                        $arr_csv_file[] = implode(',',$content);
                        continue;
                    }///self::respondwith('400', ,null);
                    elseif($content[1]=== "" || $content[1] === " " || !preg_match($regex, $content[1]) || !checkdate($unhandled_date[0], $unhandled_date[1], $unhandled_date[2])){
                        $errBool = true;
                        $counterrors = $counterrors + 1;
                        $content['errornum'] = $counterrors . "-003";
                        $errors[] = $content;
                        $arr_csv_file[] = implode(',',$content);
                        continue;
                    }
                    elseif($content[2] === "" || $content[2] === " " || !is_numeric($content[2])){
                        $errBool = true;
                        $counterrors = $counterrors + 1;
                        $content['errornum'] = $counterrors . "-004";
                        $errors[] = $content;
                        $arr_csv_file[] = implode(',',$content);
                        continue;
                    }
                    elseif($content[3] === "" || $content[3] === " " || is_numeric($content[3]) === FALSE){
                        $errBool = true;
                        $counterrors = $counterrors + 1;
                        $content['errornum'] = $counterrors . "-005";
                        $errors[] = $content;
                        $arr_csv_file[] = implode(',',$content);
                        continue;
                    }
                    elseif($content[4]=== "" || $content[4] === " " || !is_numeric($content[4])){
                        $errBool = true;
                        $counterrors = $counterrors + 1;
                        $content['errornum'] = $counterrors . "-006";
                        $errors[] = $content;
                        $arr_csv_file[] = implode(',',$content);
                        continue;
                    }
                    elseif($content[5]=== "" || $content[5] === " " || strlen($content[5])!== 3 ){
                        $errBool = true;
                        $counterrors = $counterrors + 1;
                        $content['errornum'] = $counterrors . "-007";
                        $errors[] = $content;
                        $arr_csv_file[] = implode(',',$content);
                        continue;
                    }
                    elseif($content[6]=== "" || $content[6] === " " || !is_numeric($content[6])){
                        $errBool = true;
                        $counterrors = $counterrors + 1;
                        $content['errornum'] = $counterrors . "-008";
                        $errors[] = $content;
                        $arr_csv_file[] = implode(',',$content);
                        continue;
                    }
                    elseif(!is_numeric(trim($content[7]))){
                        $errBool = true;
                        $counterrors = $counterrors + 1;
                        $content['errornum'] = $counterrors . "-009";
                        $errors[] = $content;
                        $arr_csv_file[] = implode(',',$content);
                        continue;
                    }
                    $lineNumber =0;
                    if(trim($content[0]) != '' && trim($content[0]) != 'EOF'){

                        if(isset($content[7])){
                            $lineNumber = $content[7];
                        }
                        $ref_number = $name1 . $lineNumber;
                        $ref_number = rtrim($ref_number);
                        $reference = trim($content[0]);

                        $tx['id']              = 'NULL';
                        $tx['ref_number']      = $ref_number;
                        $tx['total_amount']    = $content[4];
                        $tx['original_total_amount'] = $content[4];
                        $tx['store_id']        = trim($content[6]);

                        $tx['points_redeemed'] = 0;
                        $tx['product_id'] = $content[2];
                        $tx['country_id'] = '1';
                        $tx['network_id']      = '8';
                        $tx['trx_reward']      = 1;
                        $tx['rule_id'] = 5257 ;
                        $tx['currency_short_code'] = $content[5];
                        $tx['ref_number'] = $ref_number;

                        //Check REFERENCE
                        $reference = Reference::where('number', $reference)->first();
                                
                        if(!isset($reference->user_id) || is_null($reference)){
                            $errBool = true;
                            $counterrors = $counterrors + 1;
                            $content['errornum'] = "400010";
                            $content['message'] = 'The user has no BLU ID';
                            $content['message_ar'] = 'لا يوجد رقم BLU';
                            $errors[] = $content;
                            $arr_csv_file[] = implode(',',$content);
                            continue;
                        } else {
                            $tx['user_id'] = $reference->user_id;
                        }

                        $unhandledDate = $content[1];

                        $date = strtotime($unhandledDate);
                        $new_date = date('Y-m-d H:i:s', $date);

                        $tx['created_at']      = $new_date;
                        $tx['updated_at']      = $new_date;

                        //Bonus points
                        if(is_numeric($content[3]) != 0){
                            $txbonus = array(
                                //'partner_id' => $tx['partner_id'],
                                'store_id' => $tx['store_id'],
                                //'pos_id'   => $tx['pos_id'],
                                //'user_ref_id' => $content[0],
                                'ref_number' => "Banque Saudi Fransi Bonus POINTS - " . $ref_number,
                                'total_amount' => 0,
                                'points_rewarded' => round($content[3],0,PHP_ROUND_HALF_UP),
                                'trx_reward' => 1,
                                'rule_id' => 5257,
                                'currency_id' => 17,
                                'exchange_rate' => 1,
                                'currency_short_code' => 'SAR',
                                'country_id' => '1',
                                'quantity' => 0,
                                'user_id' => $tx['user_id'],
                                'notes' => 'Bonus Points',
                                'trans_class' => "Points",
                                'network_id' => '8',
                                'created_at' => $new_date,
                                'product_id' => $tx['product_id'],
                                'updated_at' => $new_date
                            );

                            if ($txbonus['points_rewarded'] != 0) {
                                $all_tx_bonus[$count] = $txbonus;
                                $json_contents = json_encode($txbonus);
                                $response = self::postMiddlewareTransactionDatafromfile2($txbonus, 1, $arr_item_chosen, $loy_by_partner, $partners_by_mapping_id, $partner_prefix, $network_id, $arr_currencies, $arr_transactions_file);
                                $response1 = json_decode($response,True);
                                if($response1['status'] == 200){
                                    $arr_imported_bonus[] = $txbonus;
                                } else {
                                    $counterrors +=1;
                                    $content['errornum'] = "400011";
                                    $content['message'] = 'Bonus points were not imported';
                                    $content['message_ar'] = 'لم يتم تحميل النقاط الإضافية ';
                                    $errors[]= $content;
                                    $arr_csv_file[] = implode(',',$content);
                                }
                            }

                            //$template = View::make('sms.bonus-points', array('bonusPoints' => $content[3]))->render();
                            //SMSHelper::send($template, $user, $partner);
                        }

                        if($content[4] != 0) {
                            $all_tx[$count] = $tx;
                            $json_contents = json_encode($tx);
                            $pointsRewarded = round($content[3],0,PHP_ROUND_HALF_UP);
                            $response = self::postMiddlewareTransactionDatafromfile2($tx,0, $arr_item_chosen, $loy_by_partner, $partners_by_mapping_id, $partner_prefix, $network_id, $arr_currencies, $arr_transactions_file);
                            $response1 = json_decode($response,True);

                            if($response1['status'] == 200){
                                $arr_imported[] = $tx;
                            } else {
                                $counterrors +=1;
                                $content['errornum'] = "400012";
                                $content['message'] = 'Transaction was not imported';
                                $content['message_ar'] = 'لم يتم تحميل المعاملة ';
                                $errors[]= $content;
                                $arr_csv_file[] = implode(',',$content);
                            }
                        }
                    }
//                    else {
//                        //REFID|POSTINGDATE|PRODUCT_ID|BONUS_PTS|AMOUNT|CURRENCY|STORE_ID|LINENUMBER
//                        if(trim($lineread) !== "EOF" && $count_lines != 0){
//                            $counterrors +=1;
//                            $content['errornum'] = "400013";
//                            $content['message'] = 'There is an error with the REF ID or the file is empty';
//                            $content['message_ar'] = 'هناك خطأ في رمز الإحالة أو الملف فارغ ';
//                            $errors[]= $content;
//                            $arr_csv_file[] = implode(',',$content);
//                        }
//                    }
			    }

                $count++;
            }
            $arr_response['tx_bonus'] = $arr_imported_bonus;
            $arr_response['tx'] = $arr_imported;
            $arr_response['errors'] = $errors;
            $arr_response['counterrors'] = $counterrors;
            $json_data = implode("\n",$arr_csv_file);

            APIController::postSendEmail(addslashes($json_data),"LIVE BLU Error File from middleware - " . $counterrors);

            if($counterrors > 0){
                return self::respondWith(
                    400, 'NOT OK', $arr_response , "ImportedAndErrors"
                );
            } else {
                return self::respondWith(
                    200, 'OK', $arr_response , "ImportedAndErrors"
                );
            }
        }
        else{
            return self::respondWith(400, "Not Found");
        }
                
    }
    
    public static function postMiddlewareTransactionDatafromfile2($transactions, $is_bonus, $arr_item_chosen, $loy_by_partner, $partners_by_mapping_id, $partner_prefix, $network_id, $arr_currencies, $arr_transactions_file)
    {
        $errors = array();
        if (!isset($transactions)) {

            $importedTransactionsandErrors = array();
            $importedTransactionsandErrors['errors'] = $transactions['ref_number'];
            $importedTransactionsandErrors['imported'] = "";
            $importedTransactionsandErrors['status'] = 400;
            return json_encode($importedTransactionsandErrors);
        }

        $importedTransactions = array();
        $count = 0;
        $errCount = 0;
        $errIDs = array();
        $obj_transactions = $transactions;

        //BSF is using store_id to refer to the partner/profit center
        $partner_id = $obj_transactions['store_id'];
        if (isset($partners_by_mapping_id[$partner_id])) {
            $partner = $partners_by_mapping_id[$partner_id];
            if (isset($partners_by_mapping_id[$partner_id]['store_id'])) {
                $store_id = $partners_by_mapping_id[$partner_id]['store_id'];
                $obj_transactions['store_id'] = $store_id;

                if (isset($partners_by_mapping_id[$partner_id]['pos_id'])) {
                    $pos_id = $partners_by_mapping_id[$partner_id]['pos_id'];
                }

            } else {

                $errCount = $errCount +1;
                $errIDs[] = $obj_transactions;

                NotificationController::sendEmailNotification(json_encode($errIDs), 'BLU Points', Config::get('blu.email_from'), array(null), array('adminit@bluloyalty.com'), "Transactions Received with Errors Middle Ware - Partner: " . $partner->id ." - " . $errCount);

                $importedTransactionsandErrors = array();
                $importedTransactionsandErrors['errors'] = array_values($errIDs);
                $importedTransactionsandErrors['imported'] = array_values($importedTransactions);
                $importedTransactionsandErrors['status'] = 400;
                return json_encode($importedTransactionsandErrors);
            }

            $now_time = date("Y-m-d H:i:s");

            //$partner_prefix = $partner->prefix;
            $obj_transactions['partner_id'] = $partners_by_mapping_id[$partner_id]['partner_id'];
            $obj_transactions['pos_id'] = $pos_id;
            $obj_transactions['posted_at'] = $now_time;
            $partner_name = '';

            if ($is_bonus == 0) {
                $product_id = $obj_transactions['product_id'];
                if (isset($arr_item_chosen[$partner_prefix . $product_id])) {
                    $obj_transactions['product_id'] = $arr_item_chosen[$partner_prefix . $product_id];
                } else {
                    $obj_transactions['product_id'] = NULL;
                }
                if ($obj_transactions["currency_short_code"]) {
                    $currency_id = 0;
                    if (isset($arr_currencies[$obj_transactions["currency_short_code"]])) {
                        $currency_id = $arr_currencies[$obj_transactions["currency_short_code"]];
                    }
                    //$currency = Currency::where('short_code', $obj_transactions["currency_short_code"])->first();
                    else {
                        $errCount = $errCount +1;
                        $errIDs[] = $obj_transactions;
                        $importedTransactionsandErrors = array();
                        $importedTransactionsandErrors['errors'] = array_values($errIDs);
                        $importedTransactionsandErrors['imported'] = array_values($importedTransactions);
                        $importedTransactionsandErrors['status'] = 400;

                        return json_encode($importedTransactionsandErrors);
                    }

                    if ($currency_id !== 0) {
                        $obj_transactions['currency_id'] = $currency_id;
                        $obj_transactions['partner_currency_id'] = $currency_id;
                    } else {
                        $obj_transactions['currency_id'] = 6;
                        $obj_transactions['partner_currency_id'] = 6;
                    }
                }
            }

            $short_code = $obj_transactions['currency_short_code'];
            $points_rewarded = PointsHelper::rewardPoints($obj_transactions["original_total_amount"], $obj_transactions["user_id"], $obj_transactions['store_id'], [], $obj_transactions["currency_short_code"]);
            if ($points_rewarded) {
                $obj_transactions['points_rewarded'] = round($points_rewarded);
            } else {
                $obj_transactions['points_rewarded'] = 0;
            }

            unset($obj_transactions['currency_short_code']);
            $validator = Validator::make($obj_transactions, array(
                'partner_id' => 'required|numeric',
                'store_id'   => 'required|numeric',
                'user_id'    => 'required|numeric'
            ));

            if(!$validator->fails()) {
                $trx_exists =  array_search($obj_transactions['ref_number'], array_column($arr_transactions_file, 'ref_number'));

                if ($is_bonus == 0 && $trx_exists === FALSE) {
                    $obj_transactions['notes'] = "";
                    $params = array(
                            'user_id'		=> $obj_transactions['user_id'],
                            'amount'		=> $obj_transactions['total_amount'],
                            'original_transaction_amount' => $obj_transactions['original_total_amount'],
                            'currency'		=> $short_code,
                            'partner'		=> $obj_transactions['partner_id'],
                            'partner_currency'  => $short_code,
                            'store_id'		=> $obj_transactions['store_id'], // Optional
                            'pos_id'		=> $obj_transactions['pos_id'], // Optional
                            'notes'		=> $obj_transactions['notes'], // Optional
                            'created_at'	=> $obj_transactions['created_at'],
                            'updated_at'	=> $obj_transactions['updated_at'],
                            'posted_at'         => $obj_transactions['posted_at'],
                            'reference'		=> $obj_transactions['ref_number'],
                            'product_id'	=> $obj_transactions['product_id'],
                            'use_partner_currency' => True
                        );

                    $outputTest = RewardHelper::doRewardNewLoyalty($params, 'Amount', $loy_by_partner);

                } else {

                    if ($obj_transactions['points_rewarded'] != 0 && $trx_exists == FALSE) {
                        DB::table('transaction')->insert($obj_transactions);
                        $user = User::find($obj_transactions['user_id']);
                        $user->updateBalance($obj_transactions['points_rewarded'], $obj_transactions['network_id'], 'add');
                        $partner = Partner::find($obj_transactions['partner_id']);
                    }
                }

                    //$resSMS = self::getSendsms($userSMS,3,"new_user");
                    //$userSMS->sendWelcomeMessage();
                    //$userSMS->sendAccountPin();
                    $errBool = False;
            } else {
                $errCount = $errCount +1;
                $errIDs[] = $obj_transactions;
            }

            if ($errCount > 0) {
                $importedTransactionsandErrors = array();
                $importedTransactionsandErrors['errors'] = array_values($errIDs);
                $importedTransactionsandErrors['imported'] = array_values($importedTransactions);
                $importedTransactionsandErrors['status'] = 400;

//                NotificationController::sendEmailNotification(
//                    json_encode($importedTransactionsandErrors),
//                    'BLU Points',
//                    Config::get('blu.email_from'),
//                    array(null),
//                    array('adminit@bluloyalty.com'),
//                    "DEV Transactions Received with Errors Middle Ware - Partner: " . $partner_name ." - " . $errCount
//                );

                return json_encode($importedTransactionsandErrors);
            } else {

//                NotificationController::sendEmailNotification(
//                    json_encode($importedTransactions),
//                    'BLU Points',
//                    Config::get('blu.email_from'),
//                    array(null),
//                    array('adminit@bluloyalty.com'),
//                    "DEV Transactions Processed successfully - Partner: " . $partner_name
//                );

                $importedTransactionsandErrors = array();
                $importedTransactionsandErrors['errors'] = array_values($errIDs);
                $importedTransactionsandErrors['imported'] = array_values($importedTransactions);
                $importedTransactionsandErrors['status'] = 200;

                return json_encode($importedTransactionsandErrors);
            }
        }
    }

public function getProductCategories() {
        $partner_id = $this->partner->id;
        $categories = CategoryHelper::hierarcy1($partner_id);
        
        if (Input::has('lang')) {
            $lang = Input::get('lang', 'en');
            $languageData = Language::where('short_code', $lang)->first();
            foreach ($categories as $key => $cat) {

                $catTrans = Cattranslation::where('category_id', $cat->id)->where('lang_id', $languageData->id)->first();
                if (isset($catTrans)) {
                    $categories[$key]->name = $catTrans->name;
                }
                foreach ($cat->children as $key1 => $cat1) {

                    $catTrans1 = Cattranslation::where('category_id', $cat1->id)->where('lang_id', $languageData->id)->first();
                    if (isset($catTrans1)) {

                        $categories[$key]->children[$key1]->name = $catTrans1->name;
                    }
//                var_dump($cat1);
//                exit();
                    if ($cat1->children) {
                        foreach ($cat1->children as $key2 => $cat2) {
                            $catTrans2 = Cattranslation::where('category_id', $cat2->id)->where('lang_id', $languageData->id)->first();
                            if (isset($catTrans2)) {
                                $categories[$key]->children[$key1]->children[$key2]->name = $catTrans2->name;
                            }
                        }
                    }
                }
            }
        }
        return self::respondWith(
                        200, null, $categories, 'categories'
        );
    }
    public function postUpdateUser($user_id) {
        $partner = $this->partner;
        if($partner->id != 4251){
            $validator = Validator::make(Input::all(), array(
                        'first_name' => 'required|min:2',
                        'last_name' => 'required|min:2',
                        'email' => 'required|email',
                        'dob_day' => 'required',
                        'dob_month' => 'required',
                        'dob_year' => 'required',
                        'city_id' => 'required',
                        'area_id' => 'required'
            ));
            if ($validator->fails()) {
                return self::respondWith(
                                400, null, $validator->messages()->all(), 'errors'
                );
            }
        }

        $dob = date("Y-m-d", strtotime(
                        Input::get('dob_day') . '-' . Input::get('dob_month') . '-' . Input::get('dob_year')
        ));

        $user = User::find($user_id);
        if (!$user->draft) {
            $allow_profile = False;
            $partner_id = $partner->id;
            foreach ($user->partners as $part) {
                if ($partner_id == $part->id) {
                    $allow_profile = true;
                }
            }
            if (!$user || $allow_profile == FALSE) {
                return self::respondWith(
                                400, "NOT OK"
                );
            }
        }
//        $old_user = $user;
//        unset($old_user->passcode);
//        unset($old_user->username);
//        unset($old_user->api_key);
//        unset($old_user->password);
//
//        $oldUserRalph = var_export($old_user,TRUE);
        if (Input::has('pref_lang')) {
            $pref_lang = Input::get('pref_lang');
        }

        //update the user in middleware
        if ($partner->has_middleware == 1) {
            $first_name = Input::get("first_name");
            $last_name = Input::get("last_name");
            $email = Input::get("email");

            $url = $partner->link_middleware_api . '?method=updateProfile&api_key=' . $partner->middleware_api_key;

            // Set some options - we are passing in a useragent too here
            $string_param = "user_id=" . $user_id . "&first_name=" . $first_name;
            $string_param .= "&last_name=" . $last_name . "&email=" . $email;
            if (!empty($pref_lang)) {
                $string_param .= "&pref_lang=" . $pref_lang;
            }
            $ch1 = curl_init();

            curl_setopt($ch1, CURLOPT_URL, $url);
            curl_setopt($ch1, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch1, CURLOPT_TIMEOUT, 45);
            curl_setopt($ch1, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch1, CURLOPT_POST, 1);
            curl_setopt($ch1, CURLOPT_POSTFIELDS, $string_param);
            $results = curl_exec($ch1);
            $resp_curl = json_decode($results);
            var_dump($results);
            exit();
            if (!$resp_curl) {
                return self::respondWith(
                                400, 'msg-profile-not-updated-middleware'
                );
            } elseif (isset($resp_curl->status) && !empty($resp_curl->status) && $resp_curl->status != 200) {
                return self::respondWith(
                                400, 'msg-email-already-exist'
                );
            }
        }

        if (Input::has('telephone_code')) {
            $user->telephone_code = Input::get('telephone_code');
        }

        if (Input::has('mobile')) {
            $user->mobile = Input::get('mobile');
        }

        if (Input::has('country_id')) {
            $user->country_id = Input::get('country_id');
        }

        if (Input::has('nationality')) {
            $user->nationality = self::sanitizeText(Input::get('nationality'));
        }

        $social_media = UserSocialMedia::where('user_id', $user_id)->where('social_media', 'Facebook')->first();
        if (Input::has('facebook')) {
            if (!$social_media) {
                $user_social_media = new UserSocialMedia();
                $user_social_media->social_media = 'Facebook';
                $user_social_media->account = Input::get('facebook');
                $user_social_media->user_id = $user_id;
                $user_social_media->save();
            } else {
                $social_media->account = Input::get('facebook');
                $social_media->save();
            }
        } else {
            if ($social_media) {
                UserSocialMedia::where('user_id', $user_id)->where('social_media', 'Facebook')->delete();
            }
        }

        $social_media = UserSocialMedia::where('user_id', $user_id)->where('social_media', 'Instagram')->first();
        if (Input::has('instagram')) {
            if (!$social_media) {
                $user_social_media = new UserSocialMedia();
                $user_social_media->social_media = 'Instagram';
                $user_social_media->account = Input::get('instagram');
                $user_social_media->user_id = $user_id;
                $user_social_media->save();
            } else {
                $social_media->account = Input::get('instagram');
                $social_media->save();
            }
        } else {
            if ($social_media) {
                UserSocialMedia::where('user_id', $user_id)->where('social_media', 'Instagram')->delete();
            }
        }

        if ($partner->has_middleware != 1) {
            $user->first_name = Input::get('first_name');
            $user->last_name = Input::get('last_name');
            $user->email = Input::get('email');
        }
        $user->gender = Input::get('gender', 'm');
        $user->area_id = Input::get('area_id');
        $user->city_id = Input::get('city_id');
        $user->marital_status = Input::get('marital_status', 'single');
        $user->num_children = Input::get('num_children', 0);
        $user->income_bracket = Input::get('income_bracket', '$500 - $1,000');
        $user->occupation_id = Input::get('occupation_id', 0);
        $user->dob = $dob;
        $user->status = 'active';

        $user->normalized_mobile = InputHelper::normalizeMobileNumber($user->mobile, $user->telephone_code);
        if (Input::hasFile('image')) {
            $file = Input::file('image');
            $path = public_path('uploads');
            $ext = strtolower($file->getClientOriginalExtension());
            $size = $file->getSize();
            $type = $file->getMimeType();
            $name = md5(time()) . ".{$ext}";

            if (in_array($ext, Meta::allowedImageExts())) {
                Input::file('image')->move($path, $name);

                $f = new Media();
                $f->name = $name;
                $f->size = $size;
                $f->type = $type;
                $f->ext = $ext;
                $f->save();

                $user->profile_image = $f->id;
            }
        }
        if ($user->draft) {
            if (!$user->verified) {
                if ($this->partner->id == '4215') {
                    $user->sendWelcomeMessage($this->partner->id, 'new-user-m2');
                } else {
                    $user->sendWelcomeMessage();
                }
            }
            $user->draft = false;

            // Automatically associate to partners

            /* BLU */
            DB::table('partner_user')->insert(array(
                'partner_id' => 1,
                'user_id' => $user_id
            ));

            /* WHITELABEL PARTNER */
            if ($this->partner->id != 1) {
                DB::table('partner_user')->insert(array(
                    'partner_id' => $this->partner->id,
                    'user_id' => $user_id
                ));
            }
            DB::table('role_user')->insert(array(
                'role_id' => 4,
                'user_id' => $user_id
            ));

            $partnerUsed = Partner::find($this->partner->id);

            DB::table('segment_user')->insert(array(
                'segment_id' => $partnerUsed->segments->first()->id,
                'user_id' => $user_id
            ));
            //$userSentRalph = var_export($user);
            AuditHelper::record(
                    $this->partner->id, "'{$user->id} / {$user->email}' Member profile updated " //{$userSentRalph}"
            );
        } else {
            AuditHelper::record(
                    $this->partner->id, "'{$user->id} / {$user->email}' Member profile updated " //{$oldUserRalph}
            );
        }
        if (Input::has('verified')) {
            $user_verified = Input::get('verified');
            if (strtolower($user_verified) == "true") {
                $user_verified = 1;
            }
            $user->verified = $user_verified;
        }
        $user->ref_account = false;

        $user->save();

        if(Input::has('sirius')){
            $membership = AffiliateProgramsMemberships::where('user_id', $user_id)->where('program_id', 12)->first();
            $sirius = AffiliateProgramsMemberships::where('membership_number', Input::get('sirius'))->where('user_id', '<>', $user_id)->first();
            if ($sirius) {
                return self::respondWith(
                                401, "Sirius number already exists"
                );
            }
            if(!$membership){
                $affiliateProgramMembership = new AffiliateProgramsMemberships();
                $affiliateProgramMembership->user_id = $user->id;
                $affiliateProgramMembership->membership_number = Input::get('sirius');
                $affiliateProgramMembership->program_id = 12;
                $affiliateProgramMembership->save();
            }else{
                $membership->membership_number = Input::get('sirius');
                $membership->save();
            }
        }

        if (!empty($pref_lang)) {
            $user->preferred_language = $pref_lang;
        }
        if (!empty($first_name)) {
            $user->first_name = $first_name;
        }
        if (!empty($last_name)) {
            $user->last_name = $last_name;
        }
        if (!empty($email)) {
            $user->email = $email;
        }
        $balance = $user->balance($this->partner->firstNetwork()->id);

        $user1 = $user->toArray();

        $user1['balance'] = (string) $balance;
        if($partner_id == 4251){
            $tier_user = MembershiptierUser::select('membershiptier.*', 'membershiptier_user.created_at as member_since', 'membershiptier_user.updated_at as membership_expiry')
                    ->leftJoin('membershiptier', 'membershiptier_user.membershiptier_id', '=', 'membershiptier.id')
                    ->where('membershiptier_user.user_id', $user->id)->first();
            if($tier_user->tier_group != 0){
                $tier = Membershiptier::where('id', $tier_user->tier_group)->first();
            } else {
                $tier = $tier_user;
            }
            $user1['tier']['id'] = $tier_user->id;
            $user1['tier']['name'] = $tier->name;
            $social_media = UserSocialMedia::where('user_id', $user->id)->pluck('account', 'social_media');
            $user1['social_media'] = (object)$social_media;
            $user1['member_since'] = $tier_user->member_since;
            $user1['membership_expiry'] = $tier_user->membership_expiry;
        }

        // update user to be verified upon updating the user profile <fboukarroum>


        unset($user1['passcode']);
        return self::respondWith(
                        200, null, $user1, 'user'
        );
    }
}