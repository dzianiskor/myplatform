<?php
namespace App\Http\Controllers;
use App\Brandtranslation as Brandtranslation;
use View;
use Input;
use Validator;
use App\Language;
use Response;
/**
 * Brand Translation Controller
 * 
 * @brand   Controllers
 * @package    BLU
 * @author     Ralph Nader <ralph@bluloyalty.com>
 * @copyright  2015 BLU
 * @link       http://blupoints.com
 */
class BrandtranslationController extends BaseController
{
    /**
     * Fetch the select drop down data for the brandtranslation popup
     *
     * @return array
     */
    private function getSelectData()
    {
        return array(
            'languages'  => Language::orderBy('name')->pluck('name','id')
            );
    }

    /**
     * Create and return a default draft brandtranslation
     *
     * @param int $brand_id
     * @return Result
     */
    private function getBrandtranslationDraft($enc_brand_id)
    {
        $brand_id = base64_decode($enc_brand_id);
        $brandtranslation = new Brandtranslation();

        $brandtranslation->name       = 'New Brandtranslation';
        $brandtranslation->brand_id = $brand_id;

        $brandtranslation->save();

        return $brandtranslation;
    }

    /**
     * Display the new brandtranslation page
     *
     * @return View
     */
    public function newBrandtranslation($enc_brand_id = null)
    {
        $brand_id = base64_decode($enc_brand_id);
        $data = array(
            'brandtranslation'   => $this->getBrandtranslationDraft($enc_brand_id)
        );

        return View::make("brandtranslation.form", array_merge(
            $data, $this->getSelectData()
        ));
    }

    /**
     * Display the edit brandtranslation page
     *
     * @return View
     */
    public function editBrandtranslation($enc_brandtranslation_id = null)
    {
        $brandtranslation_id = base64_decode($enc_brandtranslation_id);
        $data = array(
            'brandtranslation'   => Brandtranslation::find($brandtranslation_id)
        );

        return View::make("brandtranslation.form", array_merge(
            $data, $this->getSelectData()
        ));
    }

    /**
     * Create a new brandtranslation resource
     *
     * @param int $brandtranslation_id
     * @return void
     */
    public function updateBrandtranslation($enc_brandtranslation_id = null)
    {
        $brandtranslation_id = base64_decode($enc_brandtranslation_id);
        $rules = array(
            'brandtranslation_name' => 'required|min:1'
        );
        $input = Input::all();
        foreach($input as $k=>$i){
            $input[$k] = self::sanitizeText($i);
        }
        $validator = Validator::make($input, $rules);

        if($validator->fails()) {
            return Response::json(array(
                'failed' => true,
                'errors' => "Please ensure that you provide a valid brandtranslation name"
            ));
        }

        $brandtranslation = Brandtranslation::find($brandtranslation_id);

        // Details
        $brandtranslation->name       = self::sanitizeText(Input::get('brandtranslation_name'));
        $brandtranslation->draft      = false;
        $brandtranslation->lang_id        = self::sanitizeText(Input::get('brandtranslation_lang'));
        

        $brandtranslation->push();
        $brandTranslationBrandId = base64_encode($brandtranslation->brand_id);
        return Response::json(array(
            'failed' => false,
            'view_url' => url('dashboard/brandtranslation/list/'.$brandTranslationBrandId)
        ));
    }

    /**
     * Delete the specified brandtranslation
     *
     * @return View
     */
    public function deleteBrandtranslation($enc_brandtranslation_id = null)
    {
        $brandtranslation_id = base64_decode($enc_brandtranslation_id);
        Brandtranslation::find($brandtranslation_id)->delete();

        return Response::json(array(
            'failed' => false
        ));
    }

    /**
     * Render a list of brandtranslations relevant to the Partner
     *
     * @param int $brand_id
     * @return View
     */
    public function listBrandtranslations($enc_brand_id = null)
    {
        $brand_id = base64_decode($enc_brand_id);
        $data = array(
            'brandtranslations' => Brandtranslation::where('brand_id', $brand_id)->where('draft', false)->get()
        );

        return View::make("brandtranslation.list", $data);
    }

} // EOC