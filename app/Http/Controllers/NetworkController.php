<?php
namespace App\Http\Controllers;
use View;
use Input;
use Validator;
use I;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Config;
use App\Network as Network;
use App\Currency as Currency;
use App\Media as Media;
use Meta;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Network Controller
 *
 * @category   Controllers
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class NetworkController extends BaseController
{
    protected $layout = "layouts.dashboard";

    /**
     * Return all the data needed for the network screens
     *
     * @return array
     */
    private function getAllLists()
    {
        return array(
            'statuses'  => Meta::networkStatuses()
        );
    }

    /**
     * Return the validations for the network screen
     *
     * @return array
     */
    private function getValidations()
    {
        return array(
            'name'              => 'required',
            'currency'          => 'required',
            'affiliate_name'    => 'required',
            'affiliate_country' => 'required|exists:country,id',
            'affiliate_city'    => 'required|exists:city,id',
            'affiliate_area'    => 'required|exists:area,id',
            'value_in_currency' => 'numeric|min:0',
        );
    }

    /**
     * Display a list of the private networks
     *
     * @return void
     */
    public function getIndex()
    {
        if(!I::can('view_network')){
            return Redirect::to(url("dashboard"));
        }

        if (I::am('BLU Admin')){
            $network = Network::select('network.*')->where('draft', false)->with('country');
        } else {
            $partner = Auth::User()->getTopLevelPartner()->id;
            $network = Network::select('network.*');
            $networks = $network->whereHas('Partners', function($q) use($partner){
                $q->where('partner_id', $partner);
            });
        }

        if(Input::has('q')) {
            $q = self::sanitizeText(Input::get('q'));
            $networks = $network->where('network.name', 'like', '%'.$q.'%');
        }

        $networks = $network->paginate(15);

        return View::make("network.list", array(
            'networks' => $networks
        ));
    }

    /**
     * get fields for new private network
     *
     * @return void
     */
    public function getNew()
    {
        if(!I::can('create_network')){
            return Redirect::to(url("dashboard"));
        }
        $network                    = new Network();
        $network->parent_network_id = 1;
        $network->draft             = true;
        $network->name              = "New Network";
        $network->affiliate_country = 9;
        $network->affiliate_city    = 672;
        $network->affiliate_area    = 76;
        $network->save();
        $networkId = base64_encode($network->id);
        return Redirect::to(url("dashboard/network/view/{$networkId}"));
    }

    /**
     * View a single private network entry
     *
     * @param int $id
     * @return void
     */
    public function getView($enc_id = null)
    {
        $id = base64_decode($enc_id);
        if(!I::can('view_network')){
            return Redirect::to(url("dashboard"));
        }
        $network = Network::find($id);

        if(!$network) {
            \App::abort(404);
        }
        $managedPartners = Auth::User()->managedPartners();
        $can_view_network = false;
        foreach($managedPartners as $manP){
            foreach($network->partners as $n){
                //var_dump($p->id);
                if($manP->id == $n->id){
                    $can_view_network = true;
                    break;
                }
            }
            
        }
        if( !(I::am('BLU Admin')) && ($can_view_network== false && $network->draft == false)){
            return Redirect::to(url("dashboard"));
        }

        return View::make("network.view", array_merge(
            $this->getAllLists(), array('network' => $network)
        ));
    }

    /**
     * Delete a private network instance
     *
     * @param int $id
     * @return void
     */
    public function getDelete($enc_id = null)
    {
        $network = Network::find(base64_decode($enc_id));
        if(!I::can('create_network')){
            return Redirect::to(url("dashboard"));
        }

        if(!$network) {
            \App::abort(404);
        }

        $network->delete();

        return Redirect::to('dashboard/network');
    }

    /**
     * update a private network instance
     *
     * @param int $id
     * @return void
     */
    public function postUpdate($enc_id = null)
    {
        $id = base64_decode($enc_id);
        if(!I::can('create_network')){
            return Redirect::to(url("dashboard"));
        }
        $network = Network::find($id);

        if(!$network) {
            \App::abort(404);
        }
        $input = Input::all();
        foreach($input as $k=>$i){
            $input[$k] = self::sanitizeText($i);
        }
        $validator = Validator::make($input, $this->getValidations());

        if($validator->fails()) {
            return Redirect::to("dashboard/network/view/{$enc_id}")->withErrors($validator)->withInput();
        }

        // Basic details
        $network->draft                       = false;
        $network->name                        = self::sanitizeText(Input::get('name'));
        $network->status                      = self::sanitizeText(Input::get('status'));
        $network->description                 = self::sanitizeText(Input::get('description'));
        $network->currency                    = self::sanitizeText(Input::get('currency'));
        $network->do_points_conversion        = self::sanitizeText(Input::get('do_points_conversion'));
//        $network->network_point               = self::sanitizeText(Input::get('network_point');
//        $network->blu_point                   = self::sanitizeText(Input::get('blu_point');
        $network->value_in_currency           = self::sanitizeText(Input::get('value_in_currency'));
        $network->currency_id                 = self::sanitizeText(Input::get('currencies'));

		// calculate points price
		$currency			= Currency::where('id', $network->currency_id)->first();
        $priceInUSD			= $network->value_in_currency  / $currency->latestRate();
		$priceInBluPoints	= round($priceInUSD / Config::get('blu.points_rate'), 3);
		$numberOfDigits		= strlen(substr(strrchr($priceInBluPoints, "."), 1));
		$priceInBluPoints	= filter_var($priceInBluPoints, FILTER_SANITIZE_NUMBER_INT);
		$priceInNetworkPoints	= pow(10, $numberOfDigits);

		$network->network_point               = $priceInNetworkPoints;
        $network->blu_point                   = $priceInBluPoints;


        // Affiliate Organization
        $network->affiliate_name              = self::sanitizeText(Input::get('affiliate_name'));
        $network->affiliate_telephone         = self::sanitizeText(Input::get('affiliate_telephone'));
        $network->affiliate_address_1         = self::sanitizeText(Input::get('affiliate_address_1'));
        $network->affiliate_address_2         = self::sanitizeText(Input::get('affiliate_address_2'));
        $network->affiliate_country           = self::sanitizeText(Input::get('affiliate_country'));
        $network->affiliate_city              = self::sanitizeText(Input::get('affiliate_city'));
        $network->affiliate_area              = self::sanitizeText(Input::get('affiliate_area'));
        $network->affiliate_main_telephone    = self::sanitizeText(Input::get('affiliate_main_telephone'));
        $network->affiliate_email             = self::sanitizeText(Input::get('affiliate_email'));

        // Affiliate Contact Details
        $network->affiliate_contact_name      = self::sanitizeText(Input::get('affiliate_contact_name'));
        $network->affiliate_contact_telephone = self::sanitizeText(Input::get('affiliate_contact_telephone'));
        $network->affiliate_contact_ext       = self::sanitizeText(Input::get('affiliate_contact_ext'));
        $network->affiliate_contact_mobile    = self::sanitizeText(Input::get('affiliate_contact_mobile'));
        $network->affiliate_contact_email     = self::sanitizeText(Input::get('affiliate_contact_email'));

        if(Input::hasFile('image')) {

            $file = Input::file('image');
            $path = public_path('uploads');
            $ext  = strtolower($file->getClientOriginalExtension());
            $size = $file->getSize();
            $type = $file->getMimeType();
            $name = md5(time()).".{$ext}";

            if(in_array( $ext, Meta::allowedImageExts() )) {
                Input::file('image')->move($path, $name);

                $f = new Media();
                $f->name = $name;
                $f->size = $size;
                $f->type = $type;
                $f->ext  = $ext;
                $f->save();

                $network->image = $f->id;
            }
        }

        $network->save();

        return Redirect::to(Input::get('redirect_to', 'dashboard/network'));
    }

} // EOC