<?php
namespace App\Http\Controllers;
/**
 * Product Model Translation Controller
 * 
 * @category   Controllers
 * @package    BLU
 * @author     Ralph Nader <ralph@bluloyalty.com>
 * @copyright  2015 BLU
 * @link       http://blupoints.com
 */
class ProdmodeltranslationController extends BaseController
{
    /**
     * Fetch the select drop down data for the prodmodeltranslation popup
     *
     * @return array
     */
    private function getSelectData()
    {
        return array(
            'languages'  => Language::orderBy('name')->pluck('name','id')
            );
    }

    /**
     * Create and return a default draft prodmodeltranslation
     *
     * @param int $prodmodel_id
     * @return Result
     */
    private function getProdmodeltranslationDraft($prodmodel_id)
    {
        $prodmodeltranslation = new Prodmodeltranslation();

        $prodmodeltranslation->name       = 'New Prodmodeltranslation';
        $prodmodeltranslation->prodmodel_id = $prodmodel_id;

        $prodmodeltranslation->save();

        return $prodmodeltranslation;
    }

    /**
     * Display the new prodmodeltranslation page
     *
     * @return View
     */
    public function newProdmodeltranslation($prodmodel_id = null)
    {
        $data = array(
            'prodmodeltranslation'   => $this->getProdmodeltranslationDraft($prodmodel_id)
        );

        return View::make("prodmodeltranslation.form", array_merge(
            $data, $this->getSelectData()
        ));
    }

    /**
     * Display the edit prodmodeltranslation page
     *
     * @return View
     */
    public function editProdmodeltranslation($prodmodeltranslation_id = null)
    {
        $data = array(
            'prodmodeltranslation'   => Prodmodeltranslation::find($prodmodeltranslation_id)
        );

        return View::make("prodmodeltranslation.form", array_merge(
            $data, $this->getSelectData()
        ));
    }

    /**
     * Create a new prodmodeltranslation resource
     *
     * @param int $prodmodeltranslation_id
     * @return void
     */
    public function updateProdmodeltranslation($prodmodeltranslation_id = null)
    {
        $rules = array(
            'prodmodeltranslation_name' => 'required|min:1'
        );

        $validator = Validator::make(Input::all(), $rules);

        if($validator->fails()) {
            return Response::json(array(
                'failed' => true,
                'errors' => "Please ensure that you provide a valid prodmodeltranslation name"
            ));
        }

        $prodmodeltranslation = Prodmodeltranslation::find($prodmodeltranslation_id);

        // Details
        $prodmodeltranslation->name       = self::sanitizeText(Input::get('prodmodeltranslation_name'));
        $prodmodeltranslation->draft      = false;
        $prodmodeltranslation->lang_id        = self::sanitizeText(Input::get('prodmodeltranslation_lang'));
        

        $prodmodeltranslation->push();

        return Response::json(array(
            'failed' => false,
            'view_url' => url('dashboard/prodmodeltranslation/list/'.$prodmodeltranslation->prodmodel_id)
        ));
    }

    /**
     * Delete the specified prodmodeltranslation
     *
     * @return View
     */
    public function deleteProdmodeltranslation($prodmodeltranslation_id = null)
    {
        Prodmodeltranslation::find($prodmodeltranslation_id)->delete();

        return Response::json(array(
            'failed' => false
        ));
    }

    /**
     * Render a list of prodmodeltranslations relevant to the Partner
     *
     * @param int $prodmodel_id
     * @return View
     */
    public function listProdmodeltranslations($prodmodel_id = null)
    {
        $data = array(
            'prodmodeltranslations' => Prodmodeltranslation::where('prodmodel_id', $prodmodel_id)->where('draft', false)->get()
        );

        return View::make("prodmodeltranslation.list", $data);
    }

} // EOC