<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Mail;
use View;
use Input;
use Validator;
use Session;
use Security;
use Illuminate\Support\Facades\Response;
use I;
use Illuminate\Support\Facades\Auth;
use App\BluCollection as BluCollection;
use AuditHelper;
use Illuminate\Support\Facades\Redirect;
use Transaction;
use App\Partner as Partner;
use Notification;
use Ticket;
use Language;
use Localizationkey;
use LangKey;
use ManagedObjectsHelper;
use User;
use App\Country;
use App\Admin as Admin;
use App\Role as Role;
use App\City as City;
use App\Area as Area;
use Meta;
use App\ProductPartnerRedemption as ProductPartnerRedemption;
use App\ProductPartnerReward as ProductPartnerReward;
use App\Category as Category;
use App\Brand as Brand;
use App\Supplier as Supplier;
use App\Prodmodel as Prodmodel;
use App\ProdmodelPartner as ProdmodelPartner;
use App\Network as Network;
use App\Currency as Currency;
use App\Product as Product;
use App\Segment as Segment;
use App\Loyalty;
use App\LoyaltyRule as LoyaltyRule;
use App\LoyaltyCashback as LoyaltyCashback;
use App\ProductRedemptionCountries as ProductRedemptionCountries;
use Illuminate\Support\Facades\DB;
use App\LoyaltyExceptionrule as LoyaltyExceptionrule;
use Illuminate\Support\Facades\Config;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Loyalty Controller
 *
 * @category   Controllers
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class LoyaltyController extends BaseController
{
    private $auth_user = null;

    public function __construct()
    {
        parent::__construct();

        $this->auth_user = Auth::User();
    }

    /**
     * Fetch the validations
     *
     * @return array
     */
    private function getValidations()
    {
        return array(
            'name' => 'required',
//            'reward_usd' => 'required|numeric',
            'original_reward' => 'required|numeric|min:0',
            'reward_pts' => 'required|integer|min:0',
            'valid_to' => 'required|date|after:valid_from'

        );
    }

    /**
     * Fetch all contextual list data
     *
     * @return array
     */
    private function getAllLists()
    {
        return array(
            'partners' => Auth::User()->managedPartners()->sortBy('name'),
            'partnersList' => ManagedObjectsHelper::managedPartners()->sortBy('name')->pluck('name', 'id'),
            'segments' => Auth::User()->managedSegments(),
            'stores' => Auth::User()->managedStores(),
//            'items'      => Auth::User()->managedItems(),
            'items' => array(),
            'categories' => DB::table('category')->where('draft', false)->orderBy('name')->get(),
            'brands' => DB::table('brand')->where('draft', false)->orderBy('name')->get(),
            'countries' => DB::table('country')->orderBy('name')->get(),
            'networks' => ManagedObjectsHelper::managedNetworks()->sortBy('name')->pluck('name', 'id'),
            'currencies' => Currency::where('draft', false)->orderBy('name')->pluck('name', 'id'),
            'mcccodes' => DB::table('mcccodes')->orderBy('id')->get(),
            'trxTypes' => DB::table('transaction_types')->orderBy('id')->get(),
        );
    }

    /**
     * Get the next priority number
     *
     * @return int
     */
    private function getNextPriorityNumber($partner_id = null)
    {
        $priority = DB::table('loyalty')->where('draft', false);

        $priority->where('partner_id', $partner_id);

        return ($priority->max('priority') + 1);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getIndex()
    {
        if (!I::can('view_loyalty')) {
            return Redirect::to(url("dashboard"));
        }

        $programs = [];
        if (I::am('BLU Admin')) {
            $programs = Loyalty::where('draft', false);
        } else {
            $partnerIds = Auth::User()->managedPartnerIDs();
            $programs = Loyalty::where('draft', false)->whereIn('partner_id', $partnerIds);
        }

        if (Input::has('q') && Input::get('q') != '') {
            $programs = $programs->where('name', 'like', '%'.self::sanitizeText(Input::get('q')).'%');
        }

        if (Input::has('partner_loyalty_list') && Input::has('partner_loyalty_list') != '') {
            $partnerLoyaltyIds = explode(',', Input::get('partner_loyalty_list'));
            $programs = $programs->whereIn('partner_id', $partnerLoyaltyIds);
        }

        $programs = $programs->orderBy('created_at')->get();

        if (Input::has('network_loyalty_list') && Input::has('network_loyalty_list') != '') {
            $networkLoyaltyIds = explode(',', Input::get('network_loyalty_list'));
            $programs = $programs->load('partner.networks')->filter(function($program) use ($networkLoyaltyIds) {
                return $program->partner->networks->first() && in_array($program->partner->networks->first()->id, $networkLoyaltyIds);
            });
        }

        $page = Input::get('page', Paginator::resolveCurrentPage() ?: 1);
        $programs = new LengthAwarePaginator($programs->forPage($page, 15), $programs->count(), 15, $page);

        return View::make("loyalty.list", [
            'programs' => $programs,
            'lists' => $this->getAllLists()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getCreate()
    {
        if (!I::can('create_loyalty')) {
            return Redirect::to(url("dashboard"));
        }

        $program = new Loyalty();

        $program->draft = true;
        $program->approved = 1;
        $program->partner_id = Auth::User()->firstPartner()->id;
        $program->valid_from = date('Y-m-d');
        $program->valid_to = date('Y-m-d', strtotime("+15 Years"));
        $program->valid_from_time = '09:00';
        $program->valid_to_time = '17:00';
        $program->cashback_validity = 0;
        $program->priority = $this->getNextPriorityNumber($program->id);
        $program->save();

        return Redirect::to(url("dashboard/loyalty/edit/" . base64_encode($program->id)));
    }

    /**
     * Show the loyalty program edit page
     *
     * @param string $enc_id
     * @return Response
     */
    public function getEdit($enc_id = null)
    {
        if (!I::can('view_loyalty')) {
            return Redirect::to(url("dashboard"));
        }

        $program = Loyalty::find(base64_decode($enc_id));

        if (!$program) {
            \App::abort(404);
        }

        $managedPartners = Auth::User()->managedPartners();
        $can_view_loyalty = false;
        foreach ($managedPartners as $manP) {
            if ($manP->id == $program->partner_id) {
                $can_view_loyalty = true;
                break;
            }
        }
        if ($can_view_loyalty == false) {
            return Redirect::to(url("dashboard"));
        }

        $local_data = [
            'program' => $program,
            'programs' => DB::table('loyalty')
                            ->where('draft', false)
                            ->where('partner_id', $program->partner_id)
                            ->orderBy('priority')
                            ->get(),
        ];

        return View::make("loyalty.view", array_merge(
            $local_data, $this->getAllLists()
        ));
    }

    /**
     * Delete the program
     *
     * @param string $enc_id
     * @return void
     */
    public function getDelete($enc_id = null)
    {
        if (!I::can('delete_loyalty')) {
            return Redirect::to(url("dashboard"));
        }
        $program = Loyalty::find(base64_decode($enc_id));

        if (!$program) {
            \App::abort(404);
        }

        //AuditHelper::record(
        //    Auth::User()->firstPartner()->id, "Loyalty program '{$program->name}' deleted by '{Auth::User()->name}'"
        //);

        if (!empty(Auth::User()->parentID())) {
            $dataToBeApproved = array();
            $dataToBeApproved['approval_type'] = 'delete loyalty';
            $dataToBeApproved['loyalty_id'] = $program->id;
            $dataToBeApproved['user_id'] = Auth::User()->id;
            $dataToBeApproved['name'] = $program->name;
            $dataToBeApproved['user_id'] = Auth::user()->id;

            $jsonData = json_encode($dataToBeApproved);
            $userToApprove = Auth::User()->parentID();
            $parentUser = Admin::find($userToApprove);

            $module = 'Loyalty';
            $approval = DB::table('approvals')->insert(array(
                'module' => $module,
                'data' => $jsonData,
                'approve_by' => $userToApprove
            ));
            $lastApproval = DB::table('approvals')->orderBy('id', 'desc')->first();
            $lastID = $lastApproval->id;

            $params = array();
            $params['last_id'] = $lastID;
            $params['parent'] = $parentUser;
            $params['approve_action'] = 'approveloyaltydelete';
            $params['decline_action'] = 'declineloyaltydelete';
            $params['data'] = $dataToBeApproved;
            $params['module'] = $module;
            $params['type'] = 'delete_loyalty';
            $params['subject'] = 'Loyalty Approval Request';
            $params['to'] = $parentUser->email;
            self::sendApprovalEmail($params);

            DB::table('loyalty')->where('id', $dataToBeApproved['loyalty_id'])->update(array(
                'approved' => 0
            ));

            return Redirect::to(url("dashboard/loyalty"));
        } else {
            $program->delete();
        }
        return Redirect::to(url("dashboard/loyalty"));
    }

    /**
     * Show the loyalty program edit page
     *
     * @param string $enc_id
     * @return Response
     */
    public function postUpdate($enc_id = null)
    {
        if (!I::can('edit_loyalty') && !I::can('create_loyalty')) {
            return Redirect::to(url("dashboard"));
        }
        $program = Loyalty::find(base64_decode($enc_id));

        if (!$program) {
            \App::abort(404);
        }

        if ($program->approved != 1) {
            return Redirect::to("dashboard/loyalty/edit/{$enc_id}")->withErrors('The last changes are not approved yet')
                           ->withInput();
        }
        $input = Input::all();
        foreach ($input as $k => $i) {
            $input[$k] = self::sanitizeText($i);
        }
        $validator = Validator::make($input, $this->getValidations());

        if ($validator->fails()) {
            return Redirect::to("dashboard/loyalty/edit/{$enc_id}")->withErrors($validator)->withInput();
        }

        // Audit trail
        if (!$program->draft) {
            $auth_user = Auth::User();

            if ($program->valid_from != date("Y-m-d", strtotime(self::sanitizeText(Input::get('valid_from'))))) {

                $valid_from = date("Y-m-d", strtotime(self::sanitizeText(Input::get('valid_from'))));

//                AuditHelper::record(
//                    Auth::User()->firstPartner()->id, "Loyalty program '{$program->name}' validity period (Valid from) changed to {$valid_from} by '{Auth::User()->name}'"
//                );
            }

            if ($program->valid_to != date("Y-m-d", strtotime(self::sanitizeText(Input::get('valid_to'))))) {

                $valid_to = date("Y-m-d", strtotime(self::sanitizeText(Input::get('valid_to'))));

//                AuditHelper::record(
//                    Auth::User()->firstPartner()->id, "Loyalty program '{$program->name}' validity period (Valid to) changed to {$valid_to} by '{Auth::User()->name}'"
//                );
            }

            if ($program->valid_from_time != date("H:i:s", strtotime(self::sanitizeText(Input::get('valid_from_time'))))) {

                $valid_from_time = date("H:i:s", strtotime(self::sanitizeText(Input::get('valid_from_time'))));

//                AuditHelper::record(
//                    Auth::User()->firstPartner()->id, "Loyalty program '{$program->name}' validity period (Valid from time) changed to {$valid_from_time} by '{Auth::User()->name}'"
//                );
            }

            if ($program->valid_to_time != date("H:i:s", strtotime(self::sanitizeText(Input::get('valid_to_time'))))) {

                $valid_to_time = date("H:i:s", strtotime(self::sanitizeText(Input::get('valid_to_time'))));

//                AuditHelper::record(
//                    Auth::User()->firstPartner()->id, "Loyalty program '{$program->name}' validity period (Valid to time) changed to {$valid_to_time} by '{Auth::User()->name}'"
//                );
            }

            if ($program->cashback_validity != intval(self::sanitizeText(Input::get('cashback_validity')))) {

                $cashback_validity = self::sanitizeText(Input::get('cashback_validity'));

//                AuditHelper::record(
//                    Auth::User()->firstPartner()->id, "Loyalty program '{$program->name}' cashback validity changed to {$cashback_validity} by '{Auth::User()->name}'"
//                );
            }

            if ($program->reward_sms != self::sanitizeText(Input::get('reward_sms'))) {
//                AuditHelper::record(
//                    Auth::User()->firstPartner()->id, "Loyalty program '{$program->name}' reward SMS content changed by '{Auth::User()->name}'"
//                );
            }

            if ($program->redemption_sms != self::sanitizeText(Input::get('redemption_sms'))) {
//                AuditHelper::record(
//                    Auth::User()->firstPartner()->id, "Loyalty program '{$program->name}' redemption SMS content changed by '{Auth::User()->name}'"
//                );
            }

            if ($program->status != self::sanitizeText(Input::get('status'))) {

                $new_status = self::sanitizeText(Input::get('status'));

//                AuditHelper::record(
//                    Auth::User()->firstPartner()->id, "Loyalty program '{$program->name}' status changed to {$new_status} by '{Auth::User()->name}'"
//                );
            }
        }

        //Saving the original price
        $originalReward = self::sanitizeText(Input::get('original_reward', 1));
        $currencyId = self::sanitizeText(Input::get('currency_id', 6));
        $program->currency_id = $currencyId;
        $program->original_reward = $originalReward;
        $currency = Currency::where('id', $currencyId)->first();
        if ($originalReward > 0) {
            $program->reward_usd = $originalReward / $currency->latestRate();
        } else {
            $program->reward_usd = self::sanitizeText(Input::get('reward_usd', 1));
        }

        if (!empty(Auth::User()->parentID())) {

            $dataToBeApproved = array();
            $dataToBeApproved['id'] = base64_decode($enc_id);
            $dataToBeApproved['name'] = self::sanitizeText(Input::get('name'));
            $dataToBeApproved['partner_id'] = self::sanitizeText(Input::get('partner_id'));
            $dataToBeApproved['description'] = self::sanitizeText(Input::get('description'));
            $dataToBeApproved['reward_sms'] = self::sanitizeText(Input::get('reward_sms'));
            $dataToBeApproved['redemption_sms'] = self::sanitizeText(Input::get('redemption_sms'));
            $dataToBeApproved['reward_pts'] = self::sanitizeText(Input::get('reward_pts', 0));
            $dataToBeApproved['status'] = self::sanitizeText(Input::get('status'));
            $dataToBeApproved['valid_from'] = date("Y-m-d", strtotime(self::sanitizeText(Input::get('valid_from'))));
            $dataToBeApproved['valid_to'] = date("Y-m-d", strtotime(self::sanitizeText(Input::get('valid_to'))));
            $dataToBeApproved['valid_from_time'] = date("H:i:s", strtotime(self::sanitizeText(Input::get('valid_from_time'))));
            $dataToBeApproved['valid_to_time'] = date("H:i:s", strtotime(self::sanitizeText(Input::get('valid_to_time'))));
            $dataToBeApproved['cashback_validity'] = self::sanitizeText(Input::get('cashback_validity'));
            $dataToBeApproved['draft'] = false;
            $dataToBeApproved['currency_id'] = self::sanitizeText(Input::get('currency_id', 6));
            $dataToBeApproved['original_reward'] = self::sanitizeText(Input::get('original_reward', 1));
            $dataToBeApproved['reward_usd'] = $program->reward_usd;
            $dataToBeApproved['user_id'] = Auth::user()->id;
            $dataToBeApproved['priority'] = $program->priority;

            $jsonData = json_encode($dataToBeApproved);
            $userToApprove = Auth::User()->parentID();
            $parentUser = Admin::find($userToApprove);
            $parentEmail = $parentUser->email;

            $module = 'Loyalty';
            $approval = DB::table('approvals')->insert(array(
                'module' => $module,
                'data' => $jsonData,
                'approve_by' => $userToApprove
            ));
            $lastApproval = DB::table('approvals')->orderBy('id', 'desc')->first();
            $lastID = $lastApproval->id;


            $params = array();
            $params['last_id'] = $lastID;
            $params['parent'] = $parentUser;
            $params['approve_action'] = 'approvechanges';
            $params['decline_action'] = 'declinechanges';
            $params['data'] = $dataToBeApproved;
            $params['module'] = $module;
            $params['type'] = 'update_loyalty';
            $params['subject'] = 'Loyalty Approval Request';
            $params['to'] = $parentUser->email;
            self::sendApprovalEmail($params);

            DB::table('loyalty')->where('id', $program->id)->update(array(
                'approved' => 0
            ));

            return Redirect::to(Input::get('redirect_to', 'dashboard/loyalty'));

        } else {

            $program->name = self::sanitizeText(Input::get('name'));
            $program->partner_id = self::sanitizeText(Input::get('partner_id'));
            $program->description = self::sanitizeText(Input::get('description'));
            $program->reward_sms = self::sanitizeText(Input::get('reward_sms'));
            $program->redemption_sms = self::sanitizeText(Input::get('redemption_sms'));
            $program->reward_pts = self::sanitizeText(Input::get('reward_pts', 0));
            $program->status = self::sanitizeText(Input::get('status'));
            $program->valid_from = date("Y-m-d", strtotime(self::sanitizeText(Input::get('valid_from'))));
            $program->valid_to = date("Y-m-d", strtotime(self::sanitizeText(Input::get('valid_to'))));
            $program->valid_from_time = date("H:i:s", strtotime(self::sanitizeText(Input::get('valid_from_time'))));
            $program->valid_to_time = date("H:i:s", strtotime(self::sanitizeText(Input::get('valid_to_time'))));
            $program->cashback_validity = self::sanitizeText(Input::get('cashback_validity'));
            $program->draft = false;

            $program->save();

            return Redirect::to(Input::get('redirect_to', 'dashboard/loyalty'));
        }

    }

    /**
     * Save a new loyalty exception rule
     *
     * @param string $enc_id
     * @return void
     */
    public function postRule($enc_id = null)
    {
        if (!I::can('edit_loyalty')) {
            return Redirect::to(url("dashboard"));
        }
        $program = Loyalty::find(base64_decode($enc_id));

        if (!$program) {
            \App::abort(404);
        }
        $ruleTypes = self::sanitizeText(Input::get('rule_type'));
        $ruleValues = self::sanitizeText(Input::get('rule_value'));
        $ruleValuesCurrencies = self::sanitizeText(Input::get('rule_value_currency'));
        $ruleComparison = self::sanitizeText(Input::get('rule_comparison'));
        $ruleOperators = self::sanitizeText(Input::get('rule_operator'));
        $ruleStatus = self::sanitizeText(Input::get('rule_status'));
        $arr_ruleTypes = json_decode($ruleTypes, TRUE);
        $arr_ruleValues = json_decode($ruleValues, TRUE);
        $arr_ruleValuesCurrencies = json_decode($ruleValuesCurrencies, TRUE);
        $arr_ruleOperators = json_decode($ruleOperators, TRUE);
        $arr_ruleOperators[0] = 'and';

        $arr_ruleComparison = json_decode($ruleComparison, TRUE);
        $arr_send = array();
        $arr_send['rule_types'] = $arr_ruleTypes;
        $arr_send['rule_values'] = $arr_ruleValues;
        $arr_send['rule_comparison'] = $arr_ruleComparison;
        $json_data = json_encode($arr_send);

//        APIController::postSendEmailJson($json_data);

        $rule = new LoyaltyRule();

        //Saving the original price
        $originalReward = self::sanitizeText(Input::get('original_reward', 0));
        $currencyId = self::sanitizeText(Input::get('currency_id', 6));
        $rule->currency_id = $currencyId;
        $rule->original_reward = $originalReward;
        $rule->status = $ruleStatus;
        $currency = Currency::where('id', $currencyId)->first();
        if ($originalReward > 0) {
            $rule->reward_usd = $originalReward / $currency->latestRate();
        } else {
            $rule->reward_usd = self::sanitizeText(Input::get('reward_usd', 1));
        }

        if (Auth::User() && !empty(Auth::User()->parentID())) {
            $dataToBeApproved = array();
            $dataToBeApproved['id'] = base64_decode($enc_id);
            $dataToBeApproved['rule_type'] = self::sanitizeText(Input::get('rule_type'));
            $dataToBeApproved['rule_value'] = self::sanitizeText(Input::get('rule_value'));
            $dataToBeApproved['type'] = self::sanitizeText(Input::get('type'));
            $dataToBeApproved['reward_pts'] = self::sanitizeText(Input::get('reward_pts'));
            $dataToBeApproved['rule_status'] = self::sanitizeText(Input::get('rule_status'));
            $dataToBeApproved['loyalty_id'] = $program->id;
            $dataToBeApproved['currency_id'] = self::sanitizeText(Input::get('currency_id', 6));
            $dataToBeApproved['original_reward'] = self::sanitizeText(Input::get('original_reward', 0));
            $dataToBeApproved['reward_usd'] = $program->reward_usd;
            $dataToBeApproved['user_id'] = Auth::user()->id;

            $jsonData = json_encode($dataToBeApproved);
            $userToApprove = Auth::User()->parentID();
            $parentUser = Admin::find($userToApprove);
            $parentEmail = $parentUser->email;

            $module = 'Loyalty Rules';
            $approval = DB::table('approvals')->insert(array(
                'module' => $module,
                'data' => $jsonData,
                'approve_by' => $userToApprove
            ));
            $lastApproval = DB::table('approvals')->orderBy('id', 'desc')->first();
            $lastID = $lastApproval->id;

            // Rule Details
            switch ($dataToBeApproved['rule_type']) {
                case 'Segment':
                    $dataToBeApproved['rule_value'] = Segment::find($dataToBeApproved['rule_value'])->name;
                    break;

                case 'Country':
                    $dataToBeApproved['rule_value'] = Country::find($dataToBeApproved['rule_value'])->name;
                    break;

                case 'Store':
                    $dataToBeApproved['rule_value'] = Store::find($dataToBeApproved['rule_value'])->name;
                    break;

                case 'Item':
                    $dataToBeApproved['rule_value'] = Product::find($dataToBeApproved['rule_value'])->name;
                    break;

                case 'Category':
                    $dataToBeApproved['rule_value'] = Category::find($dataToBeApproved['rule_value'])->name;
                    break;

                case 'Brand':
                    $dataToBeApproved['rule_value'] = Brand::find($dataToBeApproved['rule_value'])->name;
                    break;

                default:
                    break;
            }

            $params = array();
            $params['last_id'] = $lastID;
            $params['parent'] = $parentUser;
            $params['approve_action'] = 'approveruleschanges';
            $params['decline_action'] = 'declineruleschanges';
            $params['data'] = $dataToBeApproved;
            $params['module'] = $module;
            $params['type'] = 'update_loyalty_rule';
            $params['subject'] = 'Loyalty Rule Request';
            $params['to'] = $parentUser->email;
            self::sendApprovalEmail($params);

            DB::table('loyalty')->where('id', $dataToBeApproved['loyalty_id'])->update(array(
                'approved' => 0
            ));

            return View::make("loyalty.row", array(
                'rule' => $rule
            ));

        } else {

            $rule->rule_type = self::sanitizeText(Input::get('rule_type'));
            $rule->rule_value = self::sanitizeText(Input::get('rule_value'));
            $rule->type = self::sanitizeText(Input::get('type'));
            $rule->reward_pts = self::sanitizeText(Input::get('reward_pts', 1));
            $rule->currency_id = $currencyId;
            $rule->status = $ruleStatus;
            //        $rule->reward_usd = self::sanitizeText(Input::get('reward_usd', 1);
            $rule->loyalty_id = $program->id;

            $rule->save();

            foreach ($arr_ruleValues as $k => $v) {

                if (!empty($arr_ruleValues[$k])) {

                    $exception_rule = new LoyaltyExceptionrule();
                    $exception_rule->loyalty_rule_id = $rule->id;
                    $exception_rule->operator = $arr_ruleOperators[0];
                    $exception_rule->type = $arr_ruleTypes[$k];
                    $exception_rule->rule_value = $arr_ruleValues[$k];
                    $exception_rule->comparison_operator = $arr_ruleComparison[$k];
                    if (!empty($arr_ruleValuesCurrencies[$k])) {
                        $exception_rule->rule_value_currency = $arr_ruleValuesCurrencies[$k];
                    }
                    $exception_rule->save();

                }
            }
//			AuditHelper::record(
//				Auth::User()->firstPartner()->id, "Loyalty program '{$program->name}' exception rule({$rule->rule_type} / {$rule->rule_value}) added by '{Auth::User()->name}'"
//			);

            return View::make("loyalty.row", array(
                'rule' => $rule
            ));
        }
    }

    /**
     * Display the edit exception rule
     *
     * @param $enc_rule_id
     * @return mixed
     */
    public function getEditRule($enc_rule_id)
    {
        $rule = LoyaltyRule::find(base64_decode($enc_rule_id));
        $currencies = Currency::where('draft', false)->orderBy('name')->pluck('name', 'id');
        $exception_rules_obj = LoyaltyExceptionrule::where('loyalty_rule_id', $rule->id)->first();

        return View::make("loyalty.editloyaltyrule", compact('rule', 'currencies', 'exception_rules_obj'));
    }

    /**
     * Update a loyalty exception rule
     *
     * @param string $enc_id
     * @return void
     */
    public function postUpdateRule($enc_id = null)
    {
        if (!I::can('edit_loyalty')) {
            return Redirect::to(url("dashboard"));
        }

        $ruleStatus = self::sanitizeText(Input::get('rule_status'));
        $rule = LoyaltyRule::find(base64_decode($enc_id));
        if (!$rule) {
            return Response::json(array(
                'failed' => true
            ));
        }

        if (Auth::User() && !empty(Auth::User()->parentID())) {
            $module = 'Loyalty Rules';
            $parentUser = Admin::find(Auth::User()->parentID());
            $dataToBeApproved = [
                'id' => $rule->id,
                'status' => $ruleStatus,
                'user_id' => Auth::User()->id,
                'loyalty_id' => $rule->loyalty_id,
                'type' => $rule->type,
                'reward_pts' => $rule->reward_pts,
                'original_reward' => $rule->original_reward,
                'currency_id' => $rule->currency_id,
            ];

            $filter = [];
            foreach ($rule->exception_rules()->get() as $exceptionRule) {
                if (!empty($filter)) {
                    $filter[] = $exceptionRule->operator;
                }
                $filter[] = $exceptionRule->type;
                $filter[] = LoyaltyExceptionrule::$comparisonOperator[$exceptionRule->comparison_operator];
                $filter[] = $exceptionRule->rule_value;
            }
            $dataToBeApproved['condition'] = implode(' ', $filter);

            DB::table('approvals')->insert(array(
                'module' => $module,
                'data' => json_encode($dataToBeApproved),
                'approve_by' => $parentUser->id,
            ));
            $lastApproval = DB::table('approvals')->orderBy('id', 'desc')->first();
            $lastID = $lastApproval->id;

            $params = array();
            $params['last_id'] = $lastID;
            $params['parent'] = $parentUser;
            $params['data'] = $dataToBeApproved;
            $params['rule'] = $rule;
            $params['module'] = $module;
            $params['approve_action'] = 'approverulestatuschanges';
            $params['decline_action'] = 'declinerulestatuschanges';
            $params['type'] = 'update_loyalty_rule_status';
            $params['subject'] = 'Update Loyalty Rule Status Request';
            $params['to'] = $parentUser->email;
            self::sendApprovalEmail($params);

            return Response::json([
                'id' => $rule->id,
                'status' => 'test',
                'failed' => false
            ]);
        } else {
            $rule->status = $ruleStatus;
            $rule->update();

            AuditHelper::record(
                1, "'" . Auth::User()->name . "' has changed exception status to: '" . $ruleStatus . "' for rule: '" . $rule->id . "' for loyalty: '" . $rule->loyalty_id . "' at: " . date("Y-m-d H:i:s", strtotime("now"))
            );

            return Response::json([
                'id' => $rule->id,
                'status' => $rule->status,
                'failed' => false
            ]);
        }
    }

    /**
     * Unlink an exception rule from a loyalty program
     *
     * @param string $enc_id
     * @return void
     */
    public function getUnlink($enc_id = null)
    {
        if (!I::can('delete_loyalty')) {
            return Redirect::to(url("dashboard"));
        }
        $rule = LoyaltyRule::find(base64_decode($enc_id));
        $program = Loyalty::find($rule->loyalty_id);
        if (!$rule) {
            \App::abort(404);
        }

//        AuditHelper::record(
//            Auth::User()->firstPartner()->id, "Loyalty program '{$program->name}' exception rule({$rule->rule_type} / {$rule->rule_value}) deleted by '{Auth::User()->name}'"
//        );
        if (!empty(Auth::User()->parentID())) {
            $dataToBeApproved = array();
            $dataToBeApproved['approval_type'] = 'delete';
            $dataToBeApproved['id'] = base64_decode($enc_id);
            $dataToBeApproved['loyalty_id'] = $program->id;
            $dataToBeApproved['name'] = $program->name;
            $dataToBeApproved['user_id'] = Auth::User()->id;
            $dataToBeApproved['type'] = $rule->type;
            $dataToBeApproved['rule_type'] = $rule->rule_type;
            $dataToBeApproved['rule_value'] = $rule->rule_value;
            $dataToBeApproved['original_reward'] = $rule->original_reward;
            $dataToBeApproved['reward_pts'] = $rule->reward_pts;
            $dataToBeApproved['currency_id'] = $rule->currency_id;
            $dataToBeApproved['currency'] = Currency::find($rule->currency_id)->short_code;
            $dataToBeApproved['user_id'] = Auth::user()->id;

            $jsonData = json_encode($dataToBeApproved);
            $userToApprove = Auth::User()->parentID();
            $parentUser = Admin::find($userToApprove);
            $parentEmail = $parentUser->email;

            $module = 'Loyalty Rules';
            $approval = DB::table('approvals')->insert(array(
                'module' => $module,
                'data' => $jsonData,
                'approve_by' => $userToApprove
            ));
            $lastApproval = DB::table('approvals')->orderBy('id', 'desc')->first();
            $lastID = $lastApproval->id;

            $params = array();
            $params['last_id'] = $lastID;
            $params['parent'] = $parentUser;
            $params['admin'] = array();
            $params['approve_action'] = 'approverulesdelete';
            $params['decline_action'] = 'declinerulesdelete';
            $params['data'] = $dataToBeApproved;
            $params['module'] = $module;
            $params['type'] = 'delete_loyalty_rule';
            $params['subject'] = 'Loyalty Rule Request';
            $params['to'] = $parentUser->email;
            self::sendApprovalEmail($params);

            DB::table('loyalty')->where('id', $dataToBeApproved['loyalty_id'])->update(array(
                'approved' => 0
            ));

            return View::make("loyalty.row", array(
                'rule' => $rule
            ));

        } else {

//            $rule->delete();
            DB::table('loyalty_rule')->where('id', $rule->id)->update(array(
                'deleted' => 1,
                'deleted_at' => date('Y-m-d H:i:s'),
                'deleted_by' => Auth::user()->id
            ));

            DB::table('loyalty_exceptionrule')->where('loyalty_rule_id', $rule->id)->update(array(
                'deleted' => 1,
                'deleted_at' => date('Y-m-d H:i:s'),
                'deleted_by' => Auth::user()->id
            ));
        }

        return Response::json(array(
            'failed' => false
        ));
    }

    /**
     * @param string $enc_id
     *
     * @return mixed
     */
    public function getRulesHistory($enc_id = null)
    {
        $id = base64_decode($enc_id);
        $rules = LoyaltyRule::withTrashed()->where('loyalty_id', $id)->where('deleted', 1)->get();

        return View::make("loyalty.rules-history", array(
            'rules' => $rules
        ));
    }

    /**
     * Handle the priority updates
     *
     * @return void
     */
    public function postPrioritize()
    {
        $sort = self::sanitizeText(Input::get('sort', array()));
        if (!empty(Auth::User()->parentID())) {

            $dataToBeApproved = array();
            $dataToBeApproved['approval_type'] = 'change_rank';
            $dataToBeApproved['sort'] = json_encode($sort);
            $dataToBeApproved['user_id'] = Auth::user()->id;


            $jsonData = json_encode($dataToBeApproved);
            $userToApprove = Auth::User()->parentID();
            $parentUser = Admin::find($userToApprove);
//            $parentEmail				= $parentUser->email;

            $module = 'Ranking';
            $lastID = DB::table('approvals')->insertGetId(array(
                'module' => $module,
                'data' => $jsonData,
                'approve_by' => $userToApprove
            ));

            $params = array();
            $params['last_id'] = $lastID;
            $params['parent'] = $parentUser;
            $params['admin'] = array();
            $params['approve_action'] = 'approveprioritychanges';
            $params['decline_action'] = 'declineprioritychanges';
            $params['data'] = $dataToBeApproved;
            $params['module'] = $module;
            $params['type'] = 'update_loyalty_rank';
            $params['subject'] = 'Loyalty Rank Approval Request';
            $params['to'] = $parentUser->email;
            self::sendApprovalEmail($params);

//            DB::table('loyalty')->where('id', $dataToBeApproved['id'])->update(array(
//            'approved' => 0
//            ));

        } else {
            foreach ($sort as $pair) {
                DB::table('loyalty')->where('id', $pair['id'])->update(array('priority' => $pair['p']));
            }
        }


        return Response::json(array(
            'failed' => false
        ));
    }


    public function getApproveprioritychanges($ApprovalId)
    {
        if (Auth::check() == false) {
            Auth::logout();
            return Redirect::to("auth/login")->with(
                "flash_error", "Please Login!"
            );
        }
        $approvalData = DB::table('approvals')->where('id', $ApprovalId)->first();
        if (!$approvalData) {
            return Redirect::to("dashboard/loyalty?msg=Your request has been already processed");
        }
        $toBeApprovedBy = $approvalData->approve_by;
        $data = json_decode($approvalData->data, true);
        $module = 'Ranking';
        $admin = Admin::find($data['user_id']);
        if (Auth::user()->id != $toBeApprovedBy) {
            return Redirect::to("dashboard/loyalty?msg=You don't have enough permissions to decline");
        }

//        DB::table('loyalty')->where('id', $data['loyalty_id'])->update(array(
//                'approved' => 1,
//                'approved_by' => Auth::user()->id
//        ));

        foreach (json_decode($data['sort'], true) as $pair) {
            DB::table('loyalty')->where('id', $pair['id'])->update(array('priority' => $pair['p']));
        }

//        AuditHelper::record(
//                Auth::User()->firstPartner()->id, "Loyalty program ranking approved by '{Auth::User()->name}'"
//        );

        $params = array();
        $params['last_id'] = 0;
        $params['parent'] = $this->auth_user;
        $params['admin'] = $admin;
        $params['approve_action'] = '';
        $params['decline_action'] = '';
        $params['data'] = $data;
        $params['module'] = $module;
        $params['type'] = 'loyalty_rank_approved';
        $params['subject'] = 'Loyalty Rank Request Approved';
        $params['to'] = $admin->email;
        self::sendApprovalEmail($params);

        return Redirect::to(url("dashboard/loyalty"));
    }


    public function getDeclineprioritychanges($ApprovalId)
    {
        if (Auth::check() == false) {
            Auth::logout();
            return Redirect::to("auth/login")->with(
                "flash_error", "Please Login!"
            );
        }
        $approvalData = DB::table('approvals')->where('id', $ApprovalId)->first();
        if (!$approvalData) {
            return Redirect::to("dashboard/loyalty?msg=Your request has been already processed");
        }
        $toBeApprovedBy = $approvalData->approve_by;
        $data = json_decode($approvalData->data, true);
        $module = 'Ranking';

        if (Auth::user()->id != $toBeApprovedBy) {
            return Redirect::to("dashboard/loyalty?msg=You don't have enough permissions to decline");
        }

//        DB::table('loyalty')->where('id', $data['loyalty_id'])->update(array(
//                'approved' => 1,
//                'approved_by' => Auth::user()->id
//        ));

        DB::table('approvals')->where('id', $ApprovalId)->delete();


//        $program	= Loyalty::find($data['loyalty_id']);
//
//        AuditHelper::record(
//                Auth::User()->firstPartner()->id, "Loyalty program ranking declined by '{Auth::User()->name}'"
//        );

        $admin = Admin::find($data['user_id']);

        $params = array();
        $params['last_id'] = 0;
        $params['parent'] = $this->auth_user;
        $params['admin'] = $admin;
        $params['approve_action'] = '';
        $params['decline_action'] = '';
        $params['data'] = $data;
        $params['module'] = $module;
        $params['type'] = 'loyalty_rank_declined';
        $params['subject'] = 'Loyalty Rank Request Declined';
        $params['to'] = $admin->email;
        self::sendApprovalEmail($params);

        return Redirect::to(url("dashboard/loyalty"));
    }


    /**
     * Return a specified priority for a loyalty program
     *
     * @param int $program_id
     * @return response
     */
    public function getPriority($program_id)
    {
        return Response::json(array(
            'failed' => false,
            'priority' => DB::table('loyalty')->where('id', $program_id)->pluck('priority')
        ));
    }


    /**
     * Approve changes made by another Admin
     * and Apply the new changes
     *
     * @param int $program_id
     * @return response
     */
    public function getApprovechanges($ApprovalId)
    {
        if (Auth::check() == false) {
            Auth::logout();
            return Redirect::to("auth/login")->with(
                "flash_error", "Please Login!"
            );
        }


        $approvalData = DB::table('approvals')->where('id', $ApprovalId)->first();
        if (!$approvalData) {
            return Redirect::to("dashboard/loyalty?msg=Your request has been already processed");
        }
        $toBeApprovedBy = $approvalData->approve_by;
        $data = json_decode($approvalData->data, true);
        $module = 'loyalty';
        if (Auth::user()->id != $toBeApprovedBy) {
            return Redirect::to("dashboard/loyalty?msg=You don't have enough permissions to approve");
        }

        $program = Loyalty::find($data['id']);
        $program->name = $data['name'];
        $program->partner_id = $data['partner_id'];
        $program->description = $data['description'];
        $program->reward_sms = $data['reward_sms'];
        $program->redemption_sms = $data['redemption_sms'];
        $program->reward_pts = $data['reward_pts'];
        $program->status = $data['status'];
        $program->reward_usd = $data['reward_usd'];
        $program->original_reward = $data['original_reward'];
        $program->currency_id = $data['currency_id'];
        $program->valid_from = $data['valid_from'];
        $program->valid_to = $data['valid_to'];
        $program->valid_from_time = $data['valid_from_time'];
        $program->valid_to_time = $data['valid_to_time'];
        $program->cashback_validity = $data['cashback_validity'];
        $program->draft = $data['draft'];


        $program->save();

        DB::table('approvals')->where('id', $ApprovalId)->delete();

        DB::table('loyalty')->where('id', $data['id'])->update(array(
            'approved' => 1,
            'approved_by' => Auth::user()->id
        ));

//        AuditHelper::record(
//                Auth::User()->firstPartner()->id, "Loyalty program '{$program->name}' approved by '{Auth::User()->name}'"
//        );

        $admin = Admin::find($data['user_id']);

        $params = array();
        $params['last_id'] = 0;
        $params['parent'] = $this->auth_user;
        $params['admin'] = $admin;
        $params['approve_action'] = '';
        $params['decline_action'] = '';
        $params['data'] = $data;
        $params['module'] = $module;
        $params['type'] = 'approved_loyalty';
        $params['subject'] = 'Loyalty Request Approved';
        $params['to'] = $admin->email;
        self::sendApprovalEmail($params);

        return Redirect::to("dashboard/loyalty/edit/" . base64_encode($data['id']));
    }


    /**
     * Approve changes made by another Admin
     * and Apply the new changes
     *
     * @param int $approvalId
     * @return response
     */
    public function getApproveruleschanges($ApprovalId)
    {
        if (Auth::check() == false) {
            Auth::logout();
            return Redirect::to("auth/login")->with(
                "flash_error", "Please Login!"
            );
        }


        $approvalData = DB::table('approvals')->where('id', $ApprovalId)->first();
        if (!$approvalData) {
            return Redirect::to("dashboard/loyalty?msg=Your request has been already processed");
        }
        $toBeApprovedBy = $approvalData->approve_by;
        $data = json_decode($approvalData->data, true);
        $module = 'loyalty';
        if (Auth::user()->id != $toBeApprovedBy) {
            return Redirect::to("dashboard/loyalty?msg=You don't have enough permissions to approve");
        }

        $rule = new LoyaltyRule();
        $rule->currency_id = $data['currency_id'];
        $rule->original_reward = $data['original_reward'];
        $rule->reward_usd = $data['reward_usd'];
        $rule->rule_type = $data['rule_type'];
        $rule->rule_value = $data['rule_value'];
        $rule->type = $data['type'];
        $rule->reward_pts = $data['reward_pts'];
        $rule->loyalty_id = $data['loyalty_id'];

        $rule->save();

//		AuditHelper::record(
//			Auth::User()->firstPartner()->id, "Loyalty program '{$program->name}' exception rule({$rule->rule_type} / {$rule->rule_value}) added by '{Auth::User()->name}'"
//		);


        DB::table('approvals')->where('id', $ApprovalId)->delete();

        DB::table('loyalty')->where('id', $data['loyalty_id'])->update(array(
            'approved' => 1,
            'approved_by' => Auth::user()->id
        ));

        $program = Loyalty::find($data['loyalty_id']);

//        AuditHelper::record(
//                Auth::User()->firstPartner()->id, "Loyalty program '{$program->name}' exception rule({$rule->rule_type}) approved by '{Auth::User()->name}'"
//        );

        $admin = Admin::find($data['user_id']);

        $params = array();
        $params['last_id'] = 0;
        $params['parent'] = $this->auth_user;
        $params['admin'] = $admin;
        $params['approve_action'] = '';
        $params['decline_action'] = '';
        $params['data'] = $data;
        $params['module'] = $module;
        $params['type'] = 'approved_loyalty_rule';
        $params['subject'] = 'Loyalty Rule Request Approved';
        $params['to'] = $admin->email;
        self::sendApprovalEmail($params);

        return Redirect::to("dashboard/loyalty/edit/" . base64_encode($data['loyalty_id']));
    }

    /**
     * Decline changes made by another Admin
     * and Apply the new changes
     *
     * @param int $program_id
     * @return response
     */
    public function getDeclinechanges($ApprovalId)
    {
        if (Auth::check() == false) {
            Auth::logout();
            return Redirect::to("auth/login")->with(
                "flash_error", "Please Login!"
            );
        }


        $approvalData = DB::table('approvals')->where('id', $ApprovalId)->first();
        if (!$approvalData) {
            return Redirect::to("dashboard/loyalty?msg=Your request has been already processed");
        }
        $data = json_decode($approvalData->data, true);
        $program = Loyalty::find($data['id']);
        $module = 'loyalty';
        $toBeApprovedBy = $approvalData->approve_by;
        if (Auth::user()->id != $toBeApprovedBy) {
            return Redirect::to("dashboard/loyalty?msg=You don't have enough permissions to approve");
        }
        DB::table('loyalty')->where('id', $data['id'])->update(array(
            'approved' => 1,
            'approved_by' => Auth::user()->id
        ));

        DB::table('approvals')->where('id', $ApprovalId)->delete();

//            AuditHelper::record(
//                    Auth::User()->firstPartner()->id, "Loyalty program '{$program->name}' Declined by '{Auth::User()->name}'"
//            );

        $admin = Admin::find($data['user_id']);

        $params = array();
        $params['last_id'] = 0;
        $params['parent'] = $this->auth_user;
        $params['admin'] = $admin;
        $params['approve_action'] = '';
        $params['decline_action'] = '';
        $params['data'] = $data;
        $params['module'] = $module;
        $params['type'] = 'declined_loyalty';
        $params['subject'] = 'Loyalty Request Declined';
        $params['to'] = $admin->email;
        self::sendApprovalEmail($params);

        return Redirect::to("dashboard/loyalty/edit/" . base64_encode($data['id']));
    }

    /**
     * Decline changes made by another Admin
     * and Apply the new changes
     *
     * @param int $approvalId
     * @return response
     */
    public function getDeclineruleschanges($ApprovalId)
    {
        if (Auth::check() == false) {
            Auth::logout();
            return Redirect::to("auth/login")->with(
                "flash_error", "Please Login!"
            );
        }

        $approvalData = DB::table('approvals')->where('id', $ApprovalId)->first();
        if (!$approvalData) {
            return Redirect::to("dashboard/loyalty?msg=Your request has been already processed");
        }
        $toBeApprovedBy = $approvalData->approve_by;
        $data = json_decode($approvalData->data, true);
        $module = 'Loyalty Rule';

        if (Auth::user()->id != $toBeApprovedBy) {
            return Redirect::to("dashboard/loyalty?msg=You don't have enough permissions to decline");
        }

        DB::table('loyalty')->where('id', $data['loyalty_id'])->update(array(
            'approved' => 1,
            'approved_by' => Auth::user()->id
        ));

        DB::table('approvals')->where('id', $ApprovalId)->delete();


        $program = Loyalty::find($data['loyalty_id']);

//        AuditHelper::record(
//                Auth::User()->firstPartner()->id, "Loyalty program '{$program->name}' exception rule({$data['rule_type']}) declined by '{Auth::User()->name}'"
//        );

        $admin = Admin::find($data['user_id']);

        $params = array();
        $params['last_id'] = 0;
        $params['parent'] = $this->auth_user;
        $params['admin'] = $admin;
        $params['approve_action'] = '';
        $params['decline_action'] = '';
        $params['data'] = $data;
        $params['module'] = $module;
        $params['type'] = 'declined_loyalty_rule';
        $params['subject'] = 'Loyalty Rule Request Declined';
        $params['to'] = $admin->email;
        self::sendApprovalEmail($params);

//	return Redirect::to("dashboard/loyalty?msg=". $module ." Declined");
        return Redirect::to("dashboard/loyalty/edit/" . base64_encode($data['loyalty_id']));
    }

    /**
     * Approve changes made by another Admin
     * and Apply the new changes
     *
     * @param int $approvalId
     * @return response
     */
    public function getApproverulesdelete($ApprovalId)
    {
        if (Auth::check() == false) {
            Auth::logout();
            return Redirect::to("auth/login")->with(
                "flash_error", "Please Login!"
            );
        }


        $approvalData = DB::table('approvals')->where('id', $ApprovalId)->first();
        if (!$approvalData) {
            return Redirect::to("dashboard/loyalty?msg=Your request has been already processed");
        }
        $toBeApprovedBy = $approvalData->approve_by;
        $data = json_decode($approvalData->data, true);
        $module = 'Loyalty Rule';
        if (Auth::user()->id != $toBeApprovedBy) {
            return Redirect::to("dashboard/loyalty?msg=You don't have enough permissions to approve");
        }

        $rule = LoyaltyRule::find($data['id']);
//        $rule->delete();
        DB::table('loyalty_rule')->where('id', $rule->id)->update(array(
            'deleted' => 1,
            'deleted_at' => date('Y-m-d H:i:s'),
            'deleted_by' => Auth::user()->id
        ));
        DB::table('loyalty_exceptionrule')->where('loyalty_rule_id', $rule->id)->update(array(
            'deleted' => 1,
            'deleted_at' => date('Y-m-d H:i:s'),
            'deleted_by' => Auth::user()->id
        ));

        DB::table('approvals')->where('id', $ApprovalId)->delete();

        DB::table('loyalty')->where('id', $data['loyalty_id'])->update(array(
            'approved' => 1,
            'approved_by' => Auth::user()->id
        ));

        $program = Loyalty::find($data['loyalty_id']);

//        AuditHelper::record(
//                Auth::User()->firstPartner()->id, "Loyalty program '{$program->name}' exception rule({$rule->rule_type}) delete approved by '{Auth::User()->name}'"
//        );

        $admin = Admin::find($data['user_id']);

        $params = array();
        $params['last_id'] = 0;
        $params['parent'] = $this->auth_user;
        $params['admin'] = $admin;
        $params['approve_action'] = '';
        $params['decline_action'] = '';
        $params['data'] = $data;
        $params['module'] = $module;
        $params['type'] = 'approved_loyalty_rule_delete';
        $params['subject'] = 'Loyalty Rule Deletion Approved';
        $params['to'] = $admin->email;
        self::sendApprovalEmail($params);

//	return Redirect::to("dashboard/loyalty?msg=". $module ." Approved");
        return Redirect::to("dashboard/loyalty/edit/" . base64_encode($data['loyalty_id']));
    }

    /**
     * Approve changes made by another Admin
     * and Apply the new changes
     *
     * @param int $approvalId
     * @return response
     */
    public function getDeclinerulesdelete($ApprovalId)
    {
        if (Auth::check() == false) {
            Auth::logout();
            return Redirect::to("auth/login")->with(
                "flash_error", "Please Login!"
            );
        }
        $approvalData = DB::table('approvals')->where('id', $ApprovalId)->first();

        if (!$approvalData) {
            return Redirect::to("dashboard/loyalty?msg=Your request has been already processed");
        }
        $data = json_decode($approvalData->data, true);
        $module = 'Loyalty Rule';
        $toBeApprovedBy = $approvalData->approve_by;
        $rule = LoyaltyRule::find($data['id']);
        if (Auth::user()->id != $toBeApprovedBy) {
            return Redirect::to("dashboard/loyalty?msg=You don't have enough permissions to approve");
        }

        DB::table('approvals')->where('id', $ApprovalId)->delete();

        DB::table('loyalty')->where('id', $data['loyalty_id'])->update(array(
            'approved' => 1,
            'approved_by' => Auth::user()->id
        ));

        $program = Loyalty::find($data['loyalty_id']);

//        AuditHelper::record(
//                Auth::User()->firstPartner()->id, "Loyalty program '{$program->name}' exception rule({$rule->rule_type}) delete approved by '{Auth::User()->name}'"
//        );

        $admin = Admin::find($data['user_id']);
        $params = array();
        $params['last_id'] = 0;
        $params['parent'] = $this->auth_user;
        $params['admin'] = $admin;
        $params['approve_action'] = '';
        $params['decline_action'] = '';
        $params['data'] = $data;
        $params['module'] = $module;
        $params['type'] = 'declined_loyalty_rule_delete';
        $params['subject'] = 'Loyalty Rule Deletion Declined';
        $params['to'] = $admin->email;
        self::sendApprovalEmail($params);

//        return Redirect::to("dashboard/loyalty?msg=". $module ." Approved");
        return Redirect::to("dashboard/loyalty/edit/" . base64_encode($data['loyalty_id']));
    }

    /**
     * Approve changes made by another Admin
     * and Apply the new changes
     *
     * @param int $approvalId
     * @return response
     */
    public function getApproveloyaltydelete($ApprovalId)
    {
        if (Auth::check() == false) {
            Auth::logout();
            return Redirect::to("auth/login")->with(
                "flash_error", "Please Login!"
            );
        }

        $approvalData = DB::table('approvals')->where('id', $ApprovalId)->first();

        if (!$approvalData) {
            return Redirect::to("dashboard/loyalty?msg=Your request has been already processed");
        }
        $toBeApprovedBy = $approvalData->approve_by;
        $data = json_decode($approvalData->data, true);
        $module = 'Loyalty';
        if (Auth::user()->id != $toBeApprovedBy) {
            return Redirect::to("dashboard/loyalty?msg=You don't have enough permissions to approve");
        }

        $program = Loyalty::find($data['loyalty_id']);
        $programName = $program->name;
        $program->delete();

        DB::table('approvals')->where('id', $ApprovalId)->delete();

        DB::table('loyalty')->where('id', $data['loyalty_id'])->update(array(
            'approved' => 1,
            'approved_by' => Auth::user()->id
        ));

//        AuditHelper::record(
//            Auth::User()->firstPartner()->id, "Loyalty program '{$programName}' delete approved by '{Auth::User()->name}'"
//        );

        $admin = Admin::find($data['user_id']);

        $params = array();
        $params['last_id'] = 0;
        $params['parent'] = $this->auth_user;
        $params['admin'] = $admin;
        $params['approve_action'] = '';
        $params['decline_action'] = '';
        $params['data'] = $data;
        $params['module'] = $module;
        $params['type'] = 'approved_loyalty_delete';
        $params['subject'] = 'Loyalty Deletion Request Approved';
        $params['to'] = $admin->email;
        self::sendApprovalEmail($params);

        return Redirect::to("dashboard/loyalty?msg=" . $module . " Approved");
    }

    /**
     * Approve changes made by another Admin
     * and Apply the new changes
     *
     * @param int $approvalId
     * @return response
     */
    public function getDeclineloyaltydelete($ApprovalId)
    {
        if (Auth::check() == false) {
            Auth::logout();
            return Redirect::to("auth/login")->with(
                "flash_error", "Please Login!"
            );
        }
        $approvalData = DB::table('approvals')->where('id', $ApprovalId)->first();

        if (!$approvalData) {
            return Redirect::to("dashboard/loyalty?msg=Your request has been already processed");
        }
        $toBeApprovedBy = $approvalData->approve_by;
        $data = json_decode($approvalData->data, true);
        $module = 'Loyalty';
        if (Auth::user()->id != $toBeApprovedBy) {
            return Redirect::to("dashboard/loyalty?msg=You don't have enough permissions to approve");
        }

        DB::table('approvals')->where('id', $ApprovalId)->delete();

        DB::table('loyalty')->where('id', $data['loyalty_id'])->update(array(
            'approved' => 1,
            'approved_by' => Auth::user()->id
        ));

        $program = Loyalty::find($data['loyalty_id']);
        $programName = $program->name;
        AuditHelper::record(
            Auth::User()
                ->firstPartner()->id, "Loyalty program '{$programName}' delete approved by '{Auth::User()->name}'"
        );

        $admin = Admin::find($data['user_id']);
        $params = array();
        $params['last_id'] = 0;
        $params['parent'] = $this->auth_user;
        $params['admin'] = $admin;
        $params['approve_action'] = '';
        $params['decline_action'] = '';
        $params['data'] = $data;
        $params['module'] = $module;
        $params['type'] = 'declined_loyalty_delete';
        $params['subject'] = 'Loyalty Deletion Request Declined';
        $params['to'] = $admin->email;
        self::sendApprovalEmail($params);

        return Redirect::to("dashboard/loyalty?msg=" . $module . " Declined");
    }

    public static function sendApprovalEmail($params)
    {
        if (empty($params['admin'])) {
            $params['admin'] = array();
        }

        $emails = array($params['to']);
        $from = Config::get('mail.from.address');
        $emailData = [
            'parent' => $params['parent'],
            'admin' => $params['admin'],
            'dataToBeApproved' => $params['data'],
            'lastID' => $params['last_id'],
            'approveAction' => $params['approve_action'],
            'declineAction' => $params['decline_action'],
            'module' => $params['module']
        ];
        Mail::send("emails.{$params['type']}", $emailData, function ($message) use ($emails, $from, $params) {
            $message->to($emails)->subject($params['subject'])->from($from, 'BLU Points');
        });
    }

    /**
     * Perform a basic search for managed members under the current user context
     *
     * @return view
     */
    public function getProducts()
    {
        $term = self::sanitizeText(Input::get('term'));

        if (!$term) {
            exit;
        }

        // We're manually converting to JSON to control attributes
        $buffer = [];
        $partner = Auth::User()->getTopLevelPartner()->id;

        $all_products = Product::select('product.*')
                               ->whereHas('Partners', function ($q) use ($partner) {
                                   $q->where('partner_id', $partner);
                               })
                               ->where('draft', false)
                               ->where(function ($query) use ($term) {
                                   $query->orWhere('product.name', 'like', "{$term}%")
                                         ->orWhere('product.brand_id', 'like', "{$term}%")
                                         ->orWhere('product.partner_mapping_id', 'like', "{$term}%")
                                         ->orWhere('product.reference', 'like', "{$term}%")->get();
                               })->get();


        foreach ($all_products as $product) {
            $p = new \stdClass();

            $p->label = $product->name;
            $p->value = $product->name;
            $p->id = $product->id;

            $buffer[] = $p;
        }

        return Response::json($buffer);
    }

    /**
     * Perform a basic search for managed members under the current user context
     *
     * @return view
     */
    public function getMcccodes()
    {
        $term = self::sanitizeText(Input::get('term'));

        if (!$term) {
            exit;
        }

        // We're manually converting to JSON to control attributes

        $buffer = [];
        $all_mcccodes = DB::table('mcccodes')->where(function ($query) use ($term) {
            $query->orWhere('mcc', 'like', "%{$term}%")
                  ->orWhere('edited_description', 'like', "%{$term}%")
                  ->orWhere('combined_description', 'like', "%{$term}%")
                  ->orWhere('usda_description', 'like', "%{$term}%")
                  ->orWhere('irs_description', 'like', "%{$term}%")->get();
        })->orderBy('id')->get();

        foreach ($all_mcccodes as $mcc) {
            $p = new \stdClass();

            $p->label = $mcc->edited_description . ' - ' . $mcc->mcc;
            $p->value = $mcc->edited_description . ' - ' . $mcc->mcc;
            $p->id = $mcc->id;

            $buffer[] = $p;
        }

        return Response::json($buffer);
    }


    /**
     * Save a new loyalty cashback eligible
     *
     * @param string $enc_id
     * @return void
     */
    public function postCashbackeligible($enc_id = null)
    {
        if (!I::can('edit_loyalty')) {
            return Redirect::to(url("dashboard"));
        }
        $program = Loyalty::find(base64_decode($enc_id));

        if (!$program) {
            \App::abort(404);
        }

        //Saving the original price
        $product_id = self::sanitizeText(Input::get('rule_value_items'));

        if (!empty(Auth::User()->parentID())) {
            $dataToBeApproved = array();
            $dataToBeApproved['loyalty_id'] = $program->id;
            $dataToBeApproved['product_id'] = $product_id;
            $dataToBeApproved['user_id'] = Auth::user()->id;


            $jsonData = json_encode($dataToBeApproved);
            $userToApprove = Auth::User()->parentID();
            $parentUser = Admin::find($userToApprove);
            $module = 'Cashback Eligible Products';

            $approval = DB::table('approvals')->insert(array(
                'module' => $module,
                'data' => $jsonData,
                'approve_by' => $userToApprove
            ));
            $lastApproval = DB::table('approvals')->orderBy('id', 'desc')->first();
            $lastID = $lastApproval->id;

            $params = array();
            $params['last_id'] = $lastID;
            $params['parent'] = $parentUser;
            $params['approve_action'] = 'approvecashbackchanges';
            $params['decline_action'] = 'declinecashbackchanges';
            $params['data'] = $dataToBeApproved;
            $params['module'] = $module;
            $params['type'] = 'update_cashback_eligible';
            $params['subject'] = 'Loyalty Cashback Rule Request';
            $params['to'] = $parentUser->email;
            self::sendApprovalEmail($params);

            DB::table('loyalty')->where('id', $dataToBeApproved['loyalty_id'])->update(array(
                'approved' => 0
            ));

            return View::make("loyalty.rowcashback", array(
                'cashback' => $cashback
            ));

        } else {
            $cashback = new LoyaltyCashback();


            $cashback->product_id = $product_id;
            $cashback->loyalty_id = $program->id;

            $cashback->save();

        }
//        AuditHelper::record(
//                Auth::User()->firstPartner()->id, "Loyalty program '{$program->name}' Cashback eligible product ({$cashback->product_id}) added by '{Auth::User()->name}'"
//        );

        return View::make("loyalty.rowcashback", array(
            'cashback' => $cashback
        ));

    }

    /**
     * Approve changes made by another Admin
     * and Apply the new changes
     *
     * @param int $approvalId
     * @return response
     */
    public function getApprovecashbackchanges($ApprovalId)
    {
        if (Auth::check() == false) {
            Auth::logout();
            return Redirect::to("auth/login")->with(
                "flash_error", "Please Login!"
            );
        }


        $approvalData = DB::table('approvals')->where('id', $ApprovalId)->first();
        if (!$approvalData) {
            return Redirect::to("dashboard/loyalty?msg=Your request has been already processed");
        }
        $toBeApprovedBy = $approvalData->approve_by;
        $data = json_decode($approvalData->data, true);
        $module = 'Cashback Eligible Products';
        if (Auth::user()->id != $toBeApprovedBy) {
            return Redirect::to("dashboard/loyalty?msg=You don't have enough permissions to approve");
        }

        $cashback = new LoyaltyCashback();
        $cashback->product_id = $data['product_id'];
        $cashback->loyalty_id = $data['loyalty_id'];
        $cashback->save();


        DB::table('approvals')->where('id', $ApprovalId)->delete();

        DB::table('loyalty')->where('id', $data['loyalty_id'])->update(array(
            'approved' => 1,
            'approved_by' => Auth::user()->id
        ));

        $program = Loyalty::find($data['loyalty_id']);

//        AuditHelper::record(
//                Auth::User()->firstPartner()->id, "Loyalty program '{$program->name}' cashback ({$cashback->id}) approved by '{Auth::User()->name}'"
//        );

        $admin = Admin::find($data['user_id']);

        $params = array();
        $params['last_id'] = 0;
        $params['parent'] = $this->auth_user;
        $params['admin'] = $admin;
        $params['approve_action'] = '';
        $params['decline_action'] = '';
        $params['data'] = $data;
        $params['module'] = $module;
        $params['type'] = 'approved_loyalty_cashback_rule';
        $params['subject'] = 'Loyalty Cashback Rule Approved';
        $params['to'] = $admin->email;
        self::sendApprovalEmail($params);

        return Redirect::to("dashboard/loyalty/edit/" . base64_encode($data['loyalty_id']));

    }

    /**
     * Decline changes made by another Admin
     * and Apply the new changes
     *
     * @param int $approvalId
     * @return response
     */
    public function getDeclinecashbackchanges($ApprovalId)
    {
        if (Auth::check() == false) {
            Auth::logout();
            return Redirect::to("auth/login")->with(
                "flash_error", "Please Login!"
            );
        }

        $approvalData = DB::table('approvals')->where('id', $ApprovalId)->first();
        if (!$approvalData) {
            return Redirect::to("dashboard/loyalty?msg=Your request has been already processed");
        }
        $toBeApprovedBy = $approvalData->approve_by;
        $data = json_decode($approvalData->data, true);
        $module = 'Cashback Eligible Products';

        if (Auth::user()->id != $toBeApprovedBy) {
            return Redirect::to("dashboard/loyalty?msg=You don't have enough permissions to decline");
        }

        DB::table('loyalty')->where('id', $data['loyalty_id'])->update(array(
            'approved' => 1,
            'approved_by' => Auth::user()->id
        ));

        DB::table('approvals')->where('id', $ApprovalId)->delete();


        $program = Loyalty::find($data['loyalty_id']);

//        AuditHelper::record(
//                Auth::User()->firstPartner()->id, "Loyalty program '{$program->name}' Cashback rule declined by '{Auth::User()->name}'"
//        );

        $admin = Admin::find($data['user_id']);

        $params = array();
        $params['last_id'] = 0;
        $params['parent'] = $this->auth_user;
        $params['admin'] = $admin;
        $params['approve_action'] = '';
        $params['decline_action'] = '';
        $params['data'] = $data;
        $params['module'] = $module;
        $params['type'] = 'declined_loyalty_cashback_rule';
        $params['subject'] = 'Loyalty Cashback Rule Declined';
        $params['to'] = $admin->email;
        self::sendApprovalEmail($params);

//	return Redirect::to("dashboard/loyalty?msg=". $module ." Declined");
        return Redirect::to("dashboard/loyalty/edit/" . base64_encode($data['loyalty_id']));
    }

    public function getUnlinkcashback($enc_id = null)
    {
        if (!I::can('edit_loyalty')) {
            return Redirect::to(url("dashboard"));
        }
        $cashback = LoyaltyCashback::find(base64_decode($enc_id));
        $program = Loyalty::find($cashback->loyalty_id);
        if (!$cashback) {
            \App::abort(404);
        }

        if (!empty(Auth::User()->parentID())) {
            $dataToBeApproved = array();
            $dataToBeApproved['approval_type'] = 'delete';
            $dataToBeApproved['id'] = base64_decode($enc_id);
            $dataToBeApproved['loyalty_id'] = $cashback->loyalty_id;
            $dataToBeApproved['product_id'] = $cashback->product_id;
            $dataToBeApproved['name'] = $program->name;
            $dataToBeApproved['user_id'] = Auth::user()->id;

            $jsonData = json_encode($dataToBeApproved);
            $userToApprove = Auth::User()->parentID();
            $parentUser = Admin::find($userToApprove);

            $module = 'Loyalty Rules';
            $approval = DB::table('approvals')->insert(array(
                'module' => $module,
                'data' => $jsonData,
                'approve_by' => $userToApprove
            ));
            $lastApproval = DB::table('approvals')->orderBy('id', 'desc')->first();
            $lastID = $lastApproval->id;

            $params = array();
            $params['last_id'] = $lastID;
            $params['parent'] = $parentUser;
            $params['admin'] = array();
            $params['approve_action'] = 'approvecashbackdelete';
            $params['decline_action'] = 'declinecashbackdelete';
            $params['data'] = $dataToBeApproved;
            $params['module'] = $module;
            $params['type'] = 'delete_loyalty_cashback_rule';
            $params['subject'] = 'Loyalty Cashback Rule Deletion Approval';
            $params['to'] = $parentUser->email;
            self::sendApprovalEmail($params);

            DB::table('loyalty')->where('id', $dataToBeApproved['loyalty_id'])->update(array(
                'approved' => 0
            ));

            return Response::json(array(
                'failed' => false
            ));
        } else {
//                AuditHelper::record(
//                    Auth::User()->firstPartner()->id, "Loyalty program '{$program->name}' Cashback Eligible product({$cashback->product_id}) deleted by '{Auth::User()->name}'"
//                );

            $cashback->delete();
        }
        return Response::json(array(
            'failed' => false
        ));
    }

    /**
     * Approve changes made by another Admin
     * and Apply the new changes
     *
     * @param int $approvalId
     * @return response
     */
    public function getApprovecashbackdelete($ApprovalId)
    {
        if (Auth::check() == false) {
            Auth::logout();
            return Redirect::to("auth/login")->with(
                "flash_error", "Please Login!"
            );
        }


        $approvalData = DB::table('approvals')->where('id', $ApprovalId)->first();
        if (!$approvalData) {
            return Redirect::to("dashboard/loyalty?msg=Your request has been already processed");
        }
        $toBeApprovedBy = $approvalData->approve_by;
        $data = json_decode($approvalData->data, true);
        $module = 'Cashback Eligible Products';
        if (Auth::user()->id != $toBeApprovedBy) {
            return Redirect::to("dashboard/loyalty?msg=You don't have enough permissions to approve");
        }

        $cashback = LoyaltyCashback::find($data['id']);
        $cashback->delete();

        DB::table('approvals')->where('id', $ApprovalId)->delete();

        DB::table('loyalty')->where('id', $data['loyalty_id'])->update(array(
            'approved' => 1,
            'approved_by' => Auth::user()->id
        ));

        $program = Loyalty::find($data['loyalty_id']);

//        AuditHelper::record(
//                Auth::User()->firstPartner()->id, "Loyalty program '{$program->name}' cashback ({$cashback->id}) approved by '{Auth::User()->name}'"
//        );

        $admin = Admin::find($data['user_id']);

        $params = array();
        $params['last_id'] = 0;
        $params['parent'] = $this->auth_user;
        $params['admin'] = $admin;
        $params['approve_action'] = '';
        $params['decline_action'] = '';
        $params['data'] = $data;
        $params['module'] = $module;
        $params['type'] = 'approved_loyalty_cashback_rule_delete';
        $params['subject'] = 'Loyalty Cashback Rule Deletion Approved';
        $params['to'] = $admin->email;
        self::sendApprovalEmail($params);

        return Redirect::to("dashboard/loyalty/edit/" . base64_encode($data['loyalty_id']));

    }


    /**
     * Decline changes made by another Admin
     * and Apply the new changes
     *
     * @param int $approvalId
     * @return response
     */
    public function getDeclinecashbackdelete($ApprovalId)
    {
        if (Auth::check() == false) {
            Auth::logout();
            return Redirect::to("auth/login")->with(
                "flash_error", "Please Login!"
            );
        }

        $approvalData = DB::table('approvals')->where('id', $ApprovalId)->first();
        if (!$approvalData) {
            return Redirect::to("dashboard/loyalty?msg=Your request has been already processed");
        }
        $toBeApprovedBy = $approvalData->approve_by;
        $data = json_decode($approvalData->data, true);
        $module = 'Cashback Eligible Products';

        if (Auth::user()->id != $toBeApprovedBy) {
            return Redirect::to("dashboard/loyalty?msg=You don't have enough permissions to decline");
        }

        DB::table('loyalty')->where('id', $data['loyalty_id'])->update(array(
            'approved' => 1,
            'approved_by' => Auth::user()->id
        ));

        DB::table('approvals')->where('id', $ApprovalId)->delete();


        $program = Loyalty::find($data['loyalty_id']);

//        AuditHelper::record(
//                Auth::User()->firstPartner()->id, "Loyalty program '{$program->name}' Cashback rule declined by '{Auth::User()->name}'"
//        );

        $admin = Admin::find($data['user_id']);

        $params = array();
        $params['last_id'] = 0;
        $params['parent'] = $this->auth_user;
        $params['admin'] = $admin;
        $params['approve_action'] = '';
        $params['decline_action'] = '';
        $params['data'] = $data;
        $params['module'] = $module;
        $params['type'] = 'declined_loyalty_cashback_rule_delete';
        $params['subject'] = 'Loyalty Cashback Rule Deletion Declined';
        $params['to'] = $admin->email;
        self::sendApprovalEmail($params);

//	return Redirect::to("dashboard/loyalty?msg=". $module ." Declined");
        return Redirect::to("dashboard/loyalty/edit/" . base64_encode($data['loyalty_id']));
    }


    /**
     * Perform a basic search for Transaction Types
     *
     * @return view
     */
    public function getTrxtypes()
    {
        $term = self::sanitizeText(Input::get('term'));

        if (!$term) {
            exit;
        }

        // We're manually converting to JSON to control attributes

        $buffer = [];
        $all_types = DB::table('transaction_types')->where(function ($query) use ($term) {
            $query->orWhere('trx_code', 'like', "%{$term}%")
                  ->orWhere('name', 'like', "%{$term}%")->get();
        })->orderBy('id')->get();

        foreach ($all_types as $trx_type) {
            $p = new \stdClass();

            $p->label = $trx_type->name . ' - ' . $trx_type->trx_code;
            $p->value = $trx_type->name . ' - ' . $trx_type->trx_code;
            $p->id = $trx_type->id;

            $buffer[] = $p;
        }

        return Response::json($buffer);
    }
} // EOC
