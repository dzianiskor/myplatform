<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\View;
use stdClass;
use I;
use Illuminate\Support\Facades\Auth;
use App\Transaction as Transaction;
use App\Notification as Notification;
use App\Ticket as Ticket;
use App\Language as Language;
use App\Localizationkey as Localizationkey;

use App\LangKey as LangKey;
use ManagedObjectsHelper;
use Illuminate\Support\Facades\DB;

/**
 * Dashboard Controller
 *
 * @category   Controllers
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class DashboardController extends BaseController
{
    protected $layout = "layouts.dashboard";

    /**
     * Display the dashboard index page
     *
     * @return string
     */
    public function showIndex()
    {
        //initialise preselected values
        $networkId = $_COOKIE['dashboard_network_id'] ?? null;

        $selectedPartnerIds = (collect(explode(',', $_COOKIE['dashboard_partner_id'] ?? null))
            ->filter(function ($value) {
                return ((int)$value != 0);
            })->toArray());

        //get all managed networks
        $managedNetworks = ManagedObjectsHelper::managedNetworks()->sortBy('name')->pluck('name', 'id');

        //preselected network must be accessible
        if (!$networkId || !isset($managedNetworks[$networkId]))
        {
            $networkId = Auth::User()->firstPartner()->firstNetwork()->id;
            setcookie('dashboard_network_id', $networkId);
        }

        //get all partners of network id
        $partners = Auth::User()->partnersOfNetworkId($networkId)->orderBy('name')->pluck('name', 'id');

        $networkPartnerIds = $partners->keys()->toArray();
        //preselected partners must be exists in selected network
        if ($selectedPartnerIds && ($selectedPartnerIds = array_intersect($selectedPartnerIds, $networkPartnerIds)))
        {
            $partnerIds = $selectedPartnerIds;
        }else
        {
            $partnerIds = $networkPartnerIds;
        }

        $transaction_overview = $this->transactionOverview($partnerIds);
        $redeemers_earners = $this->redeemersEarners($partnerIds);
        $data = array(
            'open_tickets' => $this->tickets($partnerIds),
            'sumPointsRewarded' => number_format($transaction_overview->sum('points_rewarded')),
            'sumPointsRedeemed' => number_format($transaction_overview->sum('points_redeemed')),
            'countRewardTransactions' => number_format($transaction_overview->sum('trx_reward')),
            'accounts' => $this->countMembers($partnerIds),
            'sms_notifications' => $this->countSMSNotifications($partnerIds),
            'email_notifications' => $this->countEmailNotifications($partnerIds),
            'transaction_overview' => $transaction_overview,
            'top_redeemers' => $redeemers_earners->sortByDesc('points_redeemed')->take(2),
            'top_earners' => $redeemers_earners->sortByDesc('points_rewarded')->take(2),
            'top_level_partner' => Auth::User()->getTopLevelPartner(),
            'networks' => $managedNetworks,
            'partners' => $partners,
            'network_id_selected' => $networkId,
            'partner_ids_selected' => $selectedPartnerIds,
        );

        return View::make('dashboard.index', $data);
    }

    /**
     * Return MySQL formatted date for current time
     *
     * @return date
     */
    private function now()
    {
        return date("Y-m-d H:i:s", time());
    }

    /**
     * Return MySQL formatted date for current time - 1 day
     *
     * @return date
     */
    private function yesterdate($days = 1)
    {
        return date("Y-m-d H:i:s", strtotime("-$days days", time()));
    }

    /**
     * Sum reward transactions
     *
     * @return int
     */
    private function sumPointsRewarded()
    {
        $partnersArray = [];
        if (!empty($_COOKIE['dashboard_partner_id'])) {
            $partnersCookie = $_COOKIE['dashboard_partner_id'];
            $partnersArray = explode(',', $partnersCookie);
        }
        if (!empty($partnersArray) && $partnersCookie != 'null') {
            $q = Transaction::whereIn('partner_id', $partnersArray);
            if (!empty($_COOKIE['dashboard_network_id'])) {
                $dashboardNetworkId = $_COOKIE['dashboard_network_id'];
                $q->where('network_id', $dashboardNetworkId);
            }
        } else {
            $user = Auth::User();
            $q = Transaction::whereIn('partner_id', $user->managedPartnerIDs());
        }

        $q->where('trx_reward', 1);
        $q->where('created_at', '>=', $this->yesterdate(30));
        $q->where('created_at', '<=', $this->now());

        return number_format($q->sum('points_rewarded'));
    }

    /**
     * Sum redemption transactions
     *
     * @return int
     */
    private function sumPointsRedeemed()
    {
        $partnersArray = [];
        if (!empty($_COOKIE['dashboard_partner_id'])) {
            $partnersCookie = $_COOKIE['dashboard_partner_id'];
            $partnersArray = explode(',', $partnersCookie);
        }
        if (!empty($partnersArray) && $partnersCookie != 'null') {
            $q = Transaction::whereIn('partner_id', $partnersArray);
            if (!empty($_COOKIE['dashboard_network_id'])) {
                $dashboardNetworkId = $_COOKIE['dashboard_network_id'];
                $q->where('network_id', $dashboardNetworkId);
            }
        } else {
            $q = Transaction::whereIn('partner_id', Auth::User()->managedPartnerIDs());
        }

        $q->where('trx_redeem', 1);
        $q->where('created_at', '>=', $this->yesterdate(30));
        $q->where('created_at', '<=', $this->now());

        return number_format($q->sum('points_redeemed'));
    }

    /**
     * Count reward transactions
     *
     * @return int
     */
    private function countRewardTransactions()
    {
        $partnersArray = [];
        if (!empty($_COOKIE['dashboard_partner_id'])) {
            $partnersCookie = $_COOKIE['dashboard_partner_id'];
            $partnersArray = explode(',', $partnersCookie);
        }
        if (!empty($partnersArray) && $partnersCookie != 'null') {
            $q = Transaction::whereIn('partner_id', $partnersArray);
            if (!empty($_COOKIE['dashboard_network_id'])) {
                $dashboardNetworkId = $_COOKIE['dashboard_network_id'];
                $q->where('network_id', $dashboardNetworkId);
            }
        } else {
            $q = Transaction::whereIn('partner_id', Auth::User()->managedPartnerIDs());
        }

        $q->where('trx_reward', 1);
        $q->where('created_at', '>=', $this->yesterdate(30));
        $q->where('created_at', '<=', $this->now());

        return number_format($q->count());
    }

    /**
     * Count member accounts
     *
     * @param array $partnerIds
     *
     * @return int
     */
    private function countMembers($partnerIds)
    {
        return $partnerIds ?
                $count = DB::table('users')
                   ->leftJoin('partner_user', 'partner_user.user_id', '=', 'users.id')
                   ->whereIn('partner_user.partner_id', $partnerIds)
                   ->where('users.draft', false)
                   ->where('users.id', '>', 1)
                   ->count() : 0;

        /*} else {
            if (I::am('BLU Admin')) {
                $count = User::where('draft', false)->where('id', '>', 1)->count();
            } else {
                $count = Auth::User()->partnerHierarchyMembers()->count();
            }
        }*/
    }

    /**
     * Count SMS Notifications sent
     *
     * @param array $partnerIds
     *
     * @return int
     */
    private function countSMSNotifications($partnerIds)
    {
        return Notification::where('type', 'SMS')
            ->whereIn('partner_id', $partnerIds)
            ->where('sent', true)
            ->where('created_at', '>=', $this->yesterdate(30))
            ->where('created_at', '<=', $this->now())
            ->count();
    }

    /**
     * Count email notifications sent
     *
     * @param array $partnerIds
     *
     * @return int
     */
    private function countEmailNotifications($partnerIds)
    {
        return Notification::where('type', 'Email')
            ->whereIn('partner_id', $partnerIds)
            ->where('sent', true)
            ->where('created_at', '>=', $this->yesterdate(30))
            ->where('created_at', '<=', $this->now())
            ->count();
    }

    /**
     * Fetch a list of support tickets to display
     *
     * @param array $partnersIds
     *
     * @return array
     */
    private function tickets($partnersIds)
    {
        if (I::can('view_tickets'))
            return Ticket::where('draft', '0')
                ->where('status', 'In Progress')
                ->whereIn('partner_id', $partnersIds)
                ->orderBy('created_at')
                ->limit(5)
                ->get();
    }

    /**
     * Generate a report series for reward transactions
     *
     * @param array $partner_ids
     * @return Collection
     */
    private function transactionOverview($partner_ids = [1])
    {
        $results = DB::table('transaction_dates')
                     ->where('transaction_date', '>=', Carbon::today()->subDays(30))
                     ->whereIn('partner_id', $partner_ids)
                     ->groupBy('transaction_date')
                     ->orderBy('transaction_date')
                     ->get()
                     ->each(function ($transaction) {
                         $transaction->date = Carbon::createFromFormat('Y-m-d', $transaction->transaction_date)
                                                    ->format('d/m/Y');
                     });

        return $results;
    }

    /**
     * Fetch a list of the top redeemers
     *
     * @return Collection
     */
    private function topRedeemers()
    {
        $results = Transaction::where('voided', false);
        $results->select('user_id', DB::raw('SUM(points_redeemed) AS points_redeemed'));

        $partnersArray = [];
        if (!empty($_COOKIE['dashboard_partner_id'])) {
            $partnersCookie = $_COOKIE['dashboard_partner_id'];
            $partnersArray = explode(',', $partnersCookie);
        }
        if (!empty($partnersArray) && $partnersCookie != 'null') {
            $results->whereIn('partner_id', $partnersArray);
            if (!empty($_COOKIE['dashboard_network_id'])) {
                $dashboardNetworkId = $_COOKIE['dashboard_network_id'];
                $results->where('network_id', $dashboardNetworkId);
            }
        } else {
            $results->whereIn('partner_id', Auth::User()->managedPartnerIDs());
        }

        $results->where('points_redeemed', '>', 0);
        $results->where('created_at', '>=', $this->yesterdate(30));
        $results->where('created_at', '<=', $this->now());
        $results->groupBy('user_id');
        $results->orderBy('points_redeemed', 'DESC');
        $results->limit(2);

        return $results;
    }

    /**
     * Fetch a list of the top earners
     *
     * @return Collection
     */
    private function topEarners()
    {
        $results = Transaction::where('voided', false);

        $results->select(
            'user_id',
            DB::raw('SUM(points_rewarded) AS points_rewarded')
        );

        $partnersArray = [];
        if (!empty($_COOKIE['dashboard_partner_id'])) {
            $partnersCookie = $_COOKIE['dashboard_partner_id'];
            $partnersArray = explode(',', $partnersCookie);
        }
        if (!empty($partnersArray) && $partnersCookie != 'null') {
            $results->whereIn('partner_id', $partnersArray);
            if (!empty($_COOKIE['dashboard_network_id'])) {
                $dashboardNetworkId = $_COOKIE['dashboard_network_id'];
                $results->where('network_id', $dashboardNetworkId);
            }
        } else {
            $results->whereIn('partner_id', Auth::User()->managedPartnerIDs());
        }

        $results->where('points_rewarded', '>', 0);
        $results->where('created_at', '>=', $this->yesterdate(30));
        $results->where('created_at', '<=', $this->now());
        $results->groupBy('user_id');
        $results->orderBy('points_rewarded', 'DESC');
        $results->limit(2);

        return $results;
    }

    /**
     * Fetch a list of all earners and redeemers
     *
     * @param array $partner_ids
     * @return Collection
     */
    private function redeemersEarners($partner_ids = [1])
    {
        $results = DB::table('transaction_points')
                     ->where('date_from', '>=', Carbon::today()->subDays(30))
                     ->whereIn('partner_id', $partner_ids)
                     ->groupBy('user_id')
                     ->get();

        return $results;
    }

    public static function platformLanguageKeys()
    {
        $lang = Auth::User()->pref_lang;
        $languageObj = new Language();
        $languageData = Language::where('short_code', $lang)->first();
        $languageId = $languageData->id;
        $language = $languageObj::find($languageId);

        $languageVars = $language->keys()->get();
        $countKeys = 0;
        $arr_keys = [];
        foreach ($languageVars as $l_var) {
            if ($l_var->key['is_platform_key'] == 1) {
                //$arr_keys[$countKeys]['key'] = $l_var->key['key'];
                $arr_keys[$l_var->key['key']] = trim($l_var->value);
                $countKeys += 1;
            }
        }

        return $arr_keys;
    }

    public static function platformTranslateKeys($key)
    {
        $lang = Auth::User()->pref_lang;

        $languageData = Language::where('short_code', $lang)->first();
        $languageId = $languageData->id;
        $key_id = Localizationkey::where('key', $key)->where('is_platform_key', 1)->first();
        if ($key_id) {
            $lang_key = LangKey::where('lang_id', $languageId)->where('key_id', $key_id->id)->first();
        } else {
            $lang_key = "";
        }

        $lang_key_value = '';
        if (is_object($lang_key)) {
            $lang_key_value = $lang_key->value;
        } else {
            $lang_key_value = $key;
        }
        return $lang_key_value;
    }

    public static function platformTranslateKeysForLanguage($key, $language)
    {
        $query = Cache::remember('platformTranslateKeysForLanguage', 30, function() {
            return DB::table('localization_language')
              ->select(
                  DB::raw('LOWER(CONCAT(localization_language.short_code, "_", localization_keys.key)) as translation_key'),
                  'localization_lang_keys.value'
              )
              ->join('localization_lang_keys', 'localization_lang_keys.lang_id', '=', 'localization_language.id')
              ->join('localization_keys', 'localization_lang_keys.key_id', '=', 'localization_keys.id')
              ->where('localization_keys.is_platform_key', 1)
              ->pluck('localization_lang_keys.value', 'translation_key');
        });

        $translationKey = strtolower($language . '_' . $key);

        return isset($query[$translationKey]) ? $query[$translationKey] : '';
    }

    /**
     * Performs search for user's partners of network
     *
     * @param int $networkId
     *
     * @return view
     */
    public function getNetworkPartners($networkId)
    {
        return Response::json(
            Auth::User()
                ->partnersOfNetworkId($networkId)->sortBy('name')
                ->transform(function ($partner) {
                    $transformed = new stdClass();
                    $transformed->label = $partner->name;
                    $transformed->value = $partner->name;
                    $transformed->id = $partner->id;

                    return $transformed;
                })
        );
    }
}
