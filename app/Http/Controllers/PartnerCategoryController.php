<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\App;
use View;
use Input;
use Validator;
use I;
use Illuminate\Support\Facades\Redirect;
use \App\PartnerCategory;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Partner Category Controller
 * 
 * @category   Controllers
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class PartnerCategoryController extends BaseController
{
    protected $layout = "layouts.dashboard";

    /**
     * Return the category validations
     *
     * @return array
     */
    private function getValidations() 
    {
        return array(
            'name' => 'required|min:2'
        );
    }

    /**
     * Display a list of the Categories
     *
     * @return void
     */
    public function getIndex()
    {
        if(!I::can('view_categories')){
            return Redirect::to(url("dashboard"));
        }
        $this->ormClass      = 'App\PartnerCategory';
        $this->search_fields = array('name');

        $categories = $this->getList();

        $page = Input::get('page');
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $categories = new LengthAwarePaginator($categories->forPage($page, 15), $categories->count(), 15, $page);

        return View::make("partnercategory.list", array(
            'categories' => $categories
        ));
    }

    /**
     * Fetch previous categories for the new category form
     *
     * @return void
     */
    public function getNew()
    {
        if(!I::can('create_categories')){
            return Redirect::to(url("dashboard"));
        }
        $category = new PartnerCategory();
        $category->draft = true;
        $category->name = "New Category";
        $category->save();
        $categoryId = base64_encode($category->id);
        return Redirect::to(url("dashboard/partnercategory/view/{$categoryId}"));

    }

    /**
     * View a single Category resource
     *
     * @param int $id
     * @return void
     */
    public function getView($enc_id = null)
    {
        $id = base64_decode($enc_id);
        if(!I::can('view_categories')){
            return Redirect::to(url("dashboard"));
        }
        $category = PartnerCategory::find($id);

        if(!$category) {
            App::abort(404);
        }

        return View::make("partnercategory.view", array(
            'category' => $category,
        ));
    }

    /**
     * Delete a Category instance
     *
     * @param int $id
     * @return void
     */
    public function getDelete($enc_id = null)
    {
        $id = base64_decode($enc_id);
        if(!I::can('delete_categories')){
            return Redirect::to(url("dashboard"));
        }
        $category = PartnerCategory::find($id);

        if(!$category) {
            App::abort(404);
        }

        $category->delete();

        return Redirect::to('dashboard/partnercategory');
    }

    /**
     * Update a category resource
     *
     * @param int $id
     * @return void
     */
    public function postSave($enc_id = null)
    {
        $id = base64_decode($enc_id);
        if(!I::can('edit_categories')){
            return Redirect::to(url("dashboard"));
        }
        $category = PartnerCategory::find($id);

        if(!$category) {
            App::abort(404);
        }

        $category->draft = false;
        $input = Input::all();
        unset($input['redirect_to']);
        foreach($input as $k=>$i){
            $input[$k] = self::sanitizeText($i);
        }
        $validator = Validator::make($input, $this->getValidations());

        if($validator->fails()) {
            return Redirect::to('dashboard/partnercategory/view/' . $enc_id)->withErrors($validator)->withInput();
        }

        $category->update($input);

        return Redirect::to(Input::get('redirect_to', 'dashboard/partnercategory'));
    }


} // EOC