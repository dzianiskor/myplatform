<?php
namespace App\Http\Controllers;
use App\Reference;
use I;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;

/**
 * Reference Controller
 * 
 * @category   Controllers
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class ReferenceController extends BaseController
{
	/**
	 * Fetch a list of items assigned to the particular user
	 * 
	 * @param $int $user_id
	 * @return View
	 */
	private function listItems($user_id)
	{
            if(!I::can('edit_members')){
                return Redirect::to(url("dashboard"));
            }
        return View::make('reference.userlist', array(
            'items' => DB::table('reference')->where('user_id', $user_id)->whereIn('partner_id', Auth::User()->managedPartnerIDs())->get()
        ));
	}

	/**
	 * Link a reference ID to a user account 
	 * 
	 * @param int $user_id
	 * @return Response
	 */	
	public function postLink($user_id = null)
	{
            if(!I::can('assign_ref_ids')){
                return Redirect::to(url("dashboard"));
            }
//            $validator = Validator::make(
//                Input::all(), array('reference_number' => 'required|digits:9')
//            );
//
//            if ($validator->fails()) {
//                $errors = $validator->messages()->toArray();
//
//                return Response::json(array(
//                    'fails'   => true,
//                    'message' => implode("\n", $errors['reference_number'])
//                ));
//            }

            /*
                    Only allow reference cards that do not already have a user 
                    assigned to it
             */
            $reference = Reference::where('number', self::sanitizeText(Input::get('reference_number')))->first();

            if(!$reference) {
                return Response::json(array(
                    'fails'   => true,
                    'message' => "Reference number is not valid."
                ));
            }        

            if($reference->user_id == $user_id) {
                return Response::json(array(
                    'fails'   => true,
                    'message' => "Reference number already assigned to the specified member."
                ));
            }

            if($reference->user_id != 0) {
                return Response::json(array(
                    'fails'   => true,
                    'message' => "Reference number already assigned to a member."
                ));
            }

            // Assign
            $partner_id = Auth::User()->getTopLevelPartner()->id;
            $reference->partner_id =$partner_id;
            $reference->user_id = $user_id;
            $reference->save();

            return $this->listItems($user_id);
	}

	/**
	 * Unlink a reference ID from a user account 
	 * 
	 * @param int $user_id
	 * @param int $reference_id
	 * @return Response
	 */
	public function getUnlink($user_id = null, $reference_id = null)
	{
            if(!I::can('assign_ref_ids')){
                return Redirect::to(url("dashboard"));
            }
		$reference = Reference::find($reference_id)->where('user_id', $user_id)->first();

		if($reference) {
			$reference->delete();
		}

		return $this->listItems($user_id);		
	}
	
} // EOC