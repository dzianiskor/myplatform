<?php
namespace App\Http\Controllers;
use App\Country as Country;
use App\Area as Area;
use App\City as City;
use ManagedObjectsHelper;
use Illuminate\Support\Facades\Redirect;
use App\Admin as Admin;
use I;
use Illuminate\Support\Facades\Auth;
use View;
use Input;
use Illuminate\Support\Facades\Session;
use App\Role as Role;
use Illuminate\Support\Facades\Hash;
use Validator;
use Meta;

/**
 * Admin Controller
 *
 * @category   Controllers
 * @package    BLU
 * @author     Magid Mroueh <team@wixelhq.com>
 * @copyright  2017 BLU
 * @link       http://blupoints.com
 */
class AffiliatesController extends BaseController
{
    protected $layout = "layouts.dashboard";

    private $auth_user = null;

    public function __construct()
    {
        parent::__construct();

        $this->auth_user = Auth::User();
    }

   /**
     * Get all list data
     *
     * @return array
     */
    private function getAllLists()
    {
        return array(
            'partners'         => ManagedObjectsHelper::managedPartners()->sortBy('name')->pluck('name', 'id'),
            'countries'        => Country::orderBy('name')->pluck('name', 'id'),
            'areas'        => Area::orderBy('name')->pluck('name', 'id'),
            'cities'        => City::orderBy('name')->pluck('name', 'id'),
        );
    }


    /**
     * List all Affiliates
     *
     * @return void
     */
    public function listAffiliates()
    {
        if(!I::can('view_affiliates')){
            return Redirect::to(url("dashboard"));
        }
        $this->ormClass      = 'App\Affiliates';
        $this->search_fields = array('name', 'id');
        $this->defaultSort   = 'id';

        return View::make('affiliates.list', array(
            'affiliates' => $this->getList(),
            'lists' => $this->getAllLists()
        ));
    }

    /**
     * Create a new admin draft and redirect to it
     *
     * @return Response
     */
    public function newAdmin()
    {
        if(!I::can('create_admins')){
            return Redirect::to(url("dashboard"));
        }
        $user = Admin::createNewUser();
        $userId = base64_encode($user->id);
        return Redirect::to(url("dashboard/admins/view/{$userId}"));
    }

    /**
     * Display the admin view page
     *
     * @param int $id
     * @return View
     */
    public function viewAdmin($enc_id = null)
    {
        $id = base64_decode($enc_id);
        if(!I::can('view_admins')){
            return Redirect::to(url("dashboard"));
        }
        $admin = Admin::find($id);

        if(!$admin) {
            \App::abort(404);
        }
        $bool_ismember_partner = false;
        if(I::am("BLU Admin")){
            $bool_ismember_partner = true;
        }
        $mem_partners = $admin->partners()->get();

        foreach($mem_partners as $mem_part){
            if(in_array($mem_part->id,Auth::User()->managedPartnerIDs())){
                $bool_ismember_partner = true;
                break;
            }
        }

        if($bool_ismember_partner == false){
            return Redirect::to(url("dashboard"));
        }

        $roles   = Session::get('user_role_ids');
        $role_id = min($roles);
        $roles   = Role::where('id', '>=', $role_id)->orderBy('name')->pluck('name', 'id');

        $view_data = array(
            'admin'          => $admin,
            'roles'           => $roles,
            'partners'        => ManagedObjectsHelper::managedPartners()->pluck('name', 'id'),
            'managedPartners' => Auth::User()->managedParentPartnerIDs()
        );

        return View::make('admin.view', array_merge($view_data, $this->getData()));
    }

    /**
     * Update a admin account
     *
     * @param int $id
     * @return Response
     */
    public function updateAdmin($enc_id = null)
    {
        $id = base64_decode($enc_id);
        if(!I::can('edit_admins')){
            return Redirect::to(url("dashboard"));
        }
        $user = Admin::find($id);

        if(!$user) {
            \App::abort(404);
        }

        $validator = Validator::make(Input::all(), array(
            'first_name' => 'required|min:2',
            'last_name'  => 'required|min:2',
        ));

        if($validator->fails()) {
            return Redirect::to(url("dashboard/admins/view/$enc_id"))->withErrors($validator);
        }

        // Format DOB
        $dob = strtotime(
            self::sanitizeText(Input::get('day')).'-'.self::sanitizeText(Input::get('month')).'-'.self::sanitizeText(Input::get('year'))
        );

        if(!$user->draft) {
            $auth_user = Auth::User();

            AuditHelper::record(
                Auth::User()->firstPartner()->id, "Admin '{$user->id} / {$user->email}' profile updated by '{Auth::User()->name}'"
            );
        }

        $user->parent_id      = self::sanitizeText(Input::get('parent_id'));
        $user->status         = self::sanitizeText(Input::get('status'));
        $user->title          = self::sanitizeText(Input::get('title'));
        $user->first_name     = self::sanitizeText(Input::get('first_name'));
        $user->middle_name    = self::sanitizeText(Input::get('middle_name'));
        $user->last_name      = self::sanitizeText(Input::get('last_name'));
        $user->email          = self::sanitizeText(Input::get('email'));
        $user->gender         = self::sanitizeText(Input::get('gender'));
        $user->tag            = self::sanitizeText(Input::get('tag'));
        $user->country_id     = self::sanitizeText(Input::get('country_id'));
        $user->area_id        = self::sanitizeText(Input::get('area_id'));
        $user->city_id        = self::sanitizeText(Input::get('city_id'));
        $user->dob            = date("Y-m-d H:i:s", $dob);
        $user->pref_lang      = self::sanitizeText(Input::get('pref_lang'));


        if(Input::hasFile('image')) {
            $file = Input::file('image');
            $path = public_path('uploads');
            $ext  = strtolower($file->getClientOriginalExtension());
            $size = $file->getSize();
            $type = $file->getMimeType();
            $name = md5(time()).".{$ext}";

            if(in_array($ext, Meta::allowedImageExts() )) {
                Input::file('image')->move($path, $name);

                $f = new Media();
                $f->name = $name;
                $f->size = $size;
                $f->type = $type;
                $f->ext  = $ext;
                $f->save();

                $user->profile_image = $f->id;
            }
        }
        $user->draft = '0';
        if (Input::has('username')) {
            $user->username = self::sanitizeText(Input::get('username'));
        }

        if(Input::has('password')) {
            $password = self::sanitizeText(Input::get('password'));
            $bool_valid_pass = self::valid_pass($password);
            if($bool_valid_pass == TRUE){
                $user->password = Hash::make(self::sanitizeText(Input::get('password')));
            }
            else{
                //['you have to choose if this banner is for logged in/out users.']
                return Redirect::to(url("dashboard/admins/view/$enc_id"))->withErrors(['The Password should be at least 8 characters with 1 capital letter, 1 number, 1 symbol and 1 lower case letter'])->withInput();
            }
        }
         if(Input::has('static_ip')) {
             $user->static_ip = self::sanitizeText(Input::get('static_ip'));
         }

        $user->save();

        return Redirect::to('dashboard/admins');
    }
    public static function valid_pass($candidate) {
        if (!preg_match_all('$\S*(?=\S{8,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])(?=\S*[\W])\S*$', $candidate)) {
            return FALSE;
        }
        return TRUE;
    }

    /**
     * Delete a admin account
     *
     * @param int $id
     * @return Response
     */
    public function deleteAdmin($enc_id = null)
    {
        $id = base64_decode($enc_id);
        if(!I::can('delete_admins')){
            return Redirect::to(url("dashboard"));
        }
        if (!I::am('BLU Admin')) {
            \App::abort(400);
        }

        Admin::find($id)->delete();

        return Redirect::to('dashboard/admins');
    }


    /**
     * Update admin preffered language
     *
     * @param int $userId
     * @param string $lang
     * 
     * @return Response
     */
    public function updatePrefLang($enc_userId = null)
    {
        $userId = base64_decode($enc_userId);
        $lang = self::sanitizeText(Input::get('language', 'en'));
        $user = Admin::find($userId);
        
        if(!$user) {
            \App::abort(404);
        }
        
        $user->pref_lang      = $lang;
        $user->save();

        return Redirect::back()->with('message','Preferred language changed.');
    }

} // EOC