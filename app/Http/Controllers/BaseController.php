<?php

/**
 * Base Controller
 *
 * @category   Controllers
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use I;
use Input;
use Illuminate\Support\Facades\DB;
use View;
use Network;
use BluCollection;
use Illuminate\Pagination\LengthAwarePaginator;
use App\MobileGame as MobileGame;
use App\MobileGameAds as MobileGameAds;
use App\MobilePush as MobilePush;
use App\PartnerCategory as PartnerCategory;
use Illuminate\Database\Eloquent;
use App\Country as Country;

class BaseController extends Controller
{
    protected $ormClass;
    protected $search_fields;
    protected $defaultSort = 'id';
    protected $constraint;
    protected $order = 'asc';

    public function __construct()
    {
        /* Regular members should never see the platform dashboard */

        if (!$this instanceof AuthController && I::am('Member')) {
            Auth::logout();

            Session::forget('user_roles');

            Session::forget('user_permissions');

            Session::forget('user_role_ids');

            Redirect::to("auth/login");
        }
    }

    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */
    protected function setupLayout()
    {
        if (!is_null($this->layout)) {
            $this->layout = View::make($this->layout);
        }
    }

    /**
     * Paginate the query results
     *
     * @param Objects $results
     * @return Array
     */
    protected function paginate($results)
    {
        $paginator_results = new LengthAwarePaginator($results, $results->count(), 20);
        return $paginator_results;
    }

    /**
     * Sanitize input
     *
     * @param $input_text
     * @return Text
     */
    public static function sanitizeText($input_text)
    {
        $allowed_tags = "<a><p><span><div><h1><h2><h3><h4><h5><h6><img><map><area><hr><br><br/><ul><ol><li><dl><dt><dd><table><tbody><th><tr><td><em><b><u><i><strong><del><ins><sub><sup><quote><blockquote><pre><address><code><cite><strike><caption>";
        if (is_string($input_text)) {
            $return_input = strip_tags($input_text, $allowed_tags);
        } else {
            $return_input = $input_text;
        }
        return $return_input;
    }

    /**
     * Generic method to handle listing of the resource
     *
     * @return Result
     */
    protected function getList()
    {
        $ormClass = $this->ormClass;

        // Sort
        if (Input::has('sort')) {
            $sort = self::sanitizeText(Input::get('sort'));
        } else {
            $sort = $this->defaultSort;
        }

        $order  = $this->order;

        $managed = true;

        switch ($ormClass) {
            case 'App\Partner':
                $ormClass = Auth::User()->managedPartners();
                break;
            case 'App\Product':
                $ormClass = Auth::User()->managedItems();
                break;
            case 'App\Offer':
                $ormClass = Auth::User()->managedOffers();
                break;
            case 'App\Estatement':
                $ormClass = Auth::User()->managedEstatements();
                break;
            case 'App\EstatementHistory':
                $ormClass = Auth::User()->managedEstatementHistory();
                break;
            case 'App\Segment':
                $ormClass = Auth::User()->managedSegments();
                break;
            case 'App\Store':
                $ormClass = Auth::User()->managedStores();
                break;
            case 'App\Ticket':
                $ormClass = Auth::User()->managedTickets();
                break;
            case 'App\User':
                $ormClass = Auth::User()->managedMembers();
                break;
            case 'App\Admin':
                $ormClass = Auth::User()->managedAdmins();
                break;
            case 'App\Loyalty':
                $ormClass = Auth::User()->managedLoyalty();
                break;
            case 'App\BatchCoupons':
                $ormClass = Auth::User()->managedCoupons();
                break;
            case 'App\Notification':
                $ormClass = Auth::User()->managedNotifications();
                break;
            case 'App\Banner':
                $ormClass = Auth::User()->managedBanners();
                break;
            case 'App\Cardprint':
                $ormClass = Auth::User()->managedCardprint();
                break;
            case 'App\Affiliate':
                $ormClass = Auth::User()->managedAffiliates();
                break;
            case 'App\AffiliateProgram':
                $ormClass = Auth::User()->managedAffiliatePrograms();
                break;
            default:
                $managed = false;
                break;
        }

        if ($managed) {
            $results = $ormClass->orderBy($sort, $order);
        } else {
            $results = $ormClass::orderBy($sort, $order);
        }

        // Contextual constraints
        if (!empty($this->constraint)) {
            $results = $results->where(
                $this->constraint->leftv,
                $this->constraint->eq,
                $this->constraint->rightv
            );
        }

        $model = $results->first();

        if ($model) {
            $columns = array_keys($model->attributesToArray());

            if (in_array('draft', $columns)) {
                $results = $results->FieldEqualsValue('draft', 0);
            }
            if (in_array('ref_account', $columns)) {
                $results = $results->FieldEqualsValue('ref_account', 0);
            }
        }

        $class = get_class($results);

        // Convert the builder to a Collection

        if ($class == "Illuminate\\Database\\Eloquent\\Builder") {
            $results->getQuery()->limit = null;

//            if ($ormClass != 'App\Partner') {
                $results = $results->get();
//            } else {
//                $results;
//            }
        }

        $newResults = new \App\BluCollection();
        $newResPartner = new \App\BluCollection();
        $newResParent = new \App\BluCollection();
        $newResCategory = new \App\BluCollection();
        $newResSupplier = new \App\BluCollection();
        $newResCountry = new \App\BluCollection();
        $newResNetwork = new \App\BluCollection();
        $newResSegment = new \App\BluCollection();
        $newResMemCountry = new \App\BluCollection();
        $newResMemPartner = new \App\BluCollection();
        $newResDisplaychannel = new \App\BluCollection();
        $newResBrand = new \App\BluCollection();
        $newResBannerPartner = new \App\BluCollection();
        $newResBannerStatus = new \App\BluCollection();
        $newResBannerMobStatus = new \App\BluCollection();
        $newResCardPrint = new \App\BluCollection();
        $newResEstatementPartner = new \App\BluCollection();
        $newResEstatementStatus = new \App\BluCollection();
        $newResLoyaltyPartner = new \App\BluCollection();
        $newResLoyaltyNetwork = new \App\BluCollection();
        $newResOffersDisplaychannel = new \App\BluCollection();
        $newResOfferStatus = new \App\BluCollection();
        $newResSegmentMembers = new \App\BluCollection();
        $newResAffiliatePartners = new \App\BluCollection();
        $newResAffiliateCountries = new \App\BluCollection();
        $newResAffiliateCategories = new \App\BluCollection();
        $newResAffiliateProgramAffiliates = new \App\BluCollection();
        $newResAffiliateProgramChannels = new \App\BluCollection();
        // Search
        if (Input::has('q') && Input::get('q') != '') {
            $q = self::sanitizeText(Input::get('q'));

            foreach ($this->search_fields as $k) {
                $filtered = $results->FieldLikeValue($k, $q);

                foreach ($filtered as $item) {
                    if (!$newResults->contains($item)) {
                        $newResults->add($item);
                    }
                }
            }
        } else {
            $newResults = $results;
        }

        // filter partner_id
        if (Input::has('partner_list')) {
            $q = self::sanitizeText(Input::get('partner_list'));

            if (!is_array($q)) {
                $q = explode(',', $q);
            }

            if (sizeof($q) == 1) {
                $q  = urldecode($q[0]);
                $q = explode(',', $q);
            }

            foreach ($newResults as $res1) {
                if ($res1->partners) {
                    foreach ($res1->partners as $respartner) {
                        if (in_array($respartner->id, $q)) {
                            if (!$newResPartner->contains($res1)) {
                                $newResPartner->add($res1);
                            }
                        }
                    }
                }
                if ($res1->partner) {
                    if (in_array($res1->partner->id, $q)) {
                        if (!$newResPartner->contains($res1)) {
                            $newResPartner->add($res1);
                        }
                    }
                }
            }
        } else {
            $newResPartner = $newResults;
        }

        // filter parent
        if (Input::has('parent_list')) {
            $q = self::sanitizeText(Input::get('parent_list'));

            if (!is_array($q)) {
                $q = explode(',', $q);
            }

            if (sizeof($q) == 1) {
                $q  = urldecode($q[0]);
                $q = explode(',', $q);
            }

            foreach ($newResPartner as $res1) {
                if ($res1->parent) {
                    if (in_array($res1->parent->id, $q)) {
                        $newResParent->add($res1);
                    }
                }
            }
        } else {
            $newResParent = $newResPartner;
        }

        // filter category
        if (Input::has('category_list')) {
            $q = self::sanitizeText(Input::get('category_list'));

            if (!is_array($q)) {
                $q = explode(',', $q);
            }

            if (sizeof($q) == 1) {
                $q  = urldecode($q[0]);
                $q = explode(',', $q);
            }

            foreach ($newResParent as $res1) {
                if ($res1->category) {
                    if (in_array($res1->category->id, $q)) {
                        $newResCategory->add($res1);
                    }
                }
            }
        } else {
            $newResCategory = $newResParent;
        }

        // filter partner_estatement_list
        if (Input::has('partner_estatement_list')) {
            $q = self::sanitizeText(Input::get('partner_estatement_list'));

            if (!is_array($q)) {
                $q = explode(',', $q);
            }

            if (sizeof($q) == 1) {
                $q  = urldecode($q[0]);
                $q = explode(',', $q);
            }

            foreach ($newResCategory as $res1) {
                if (in_array($res1->partner_id, $q)) {
                        $newResEstatementPartner->add($res1);
                }
            }
        } else {
            $newResEstatementPartner = $newResCategory;
        }

        // filter estatement_status
        if (Input::has('estatement_status')) {
            $q = self::sanitizeText(Input::get('estatement_status'));

            if (!is_array($q)) {
                $q = explode(',', $q);
            }

            if (sizeof($q) == 1) {
                $q  = urldecode($q[0]);
                $q = explode(',', $q);
            }

            foreach ($newResults as $res1) {
                if (in_array($res1->status, $q)) {
                        $newResEstatementStatus->add($res1);
                }
            }
        } else {
            $newResEstatementStatus = $newResEstatementPartner;
        }

        // filter partner_loyalty_id
        if (Input::has('partner_loyalty_list')) {
            $q = self::sanitizeText(Input::get('partner_loyalty_list'));

            if (!is_array($q)) {
                $q = explode(',', $q);
            }

            if (sizeof($q) == 1) {
                $q  = urldecode($q[0]);
                $q = explode(',', $q);
            }

            foreach ($newResEstatementStatus as $res1) {
                if (in_array($res1->partner_id, $q)) {
                        $newResLoyaltyPartner->add($res1);
                }
            }
        } else {
            $newResLoyaltyPartner = $newResEstatementStatus;
        }

        // filter network_loyalty_list
        if (Input::has('network_loyalty_list')) {
            $q = self::sanitizeText(Input::get('network_loyalty_list'));

            if (!is_array($q)) {
                $q = explode(',', $q);
            }

            if (sizeof($q) == 1) {
                $q  = urldecode($q[0]);
                $q = explode(',', $q);
            }

            foreach ($newResLoyaltyPartner as $res1) {
                $partner = \App\Partner::find($res1->partner_id);
                $firstNetwork = $partner->firstNetwork();
                if ($firstNetwork) {
                    if (in_array($firstNetwork->id, $q)) {
                        $newResLoyaltyNetwork->add($res1);
                    }
                }
            }
        } else {
            $newResLoyaltyNetwork = $newResLoyaltyPartner;
        }

        // filter supplier_id
        if (Input::has('supplier_list')) {
            $q = self::sanitizeText(Input::get('supplier_list'));

            if (!is_array($q)) {
                $q = explode(',', $q);
            }

            if (sizeof($q) == 1) {
                $q  = urldecode($q[0]);
                $q = explode(',', $q);
            }

            foreach ($q as $p1) {
                foreach ($newResLoyaltyNetwork as $res2) {
                    if ($res2->supplier_id == $p1) {
                        if (!$newResSupplier->contains($res2)) {
                            $newResSupplier->add($res2);
                        }
                    }
                }
            }
        } else {
            $newResSupplier = $newResLoyaltyNetwork;
        }

        // filter partner_member_list
        if (Input::has('partner_member_list')) {
            $q1 = self::sanitizeText(Input::get('partner_member_list'));

            if (!is_array($q)) {
                $q = explode(',', $q);
            }

            if (sizeof($q) == 1) {
                $q  = urldecode($q[0]);
                $q = explode(',', $q);
            }

            $integerIDs = array_map('intval', $q1);
            $results111 = DB::table('partner_user')->whereIn('partner_id', $integerIDs)
                    ->groupBy('user_id')->get();

            //$allmembers = Partner::whereIn('id',$q)->members;
            $q = array();
            foreach ($results111 as $res111) {
                $q[] = $res111->user_id;
            }

            foreach ($newResSupplier as $res1) {
                if (in_array($res1->id, $q)) {
                    $newResMemPartner->add($res1);
                }
            }
        } else {
            $newResMemPartner = $newResSupplier;
        }

        // filter country_id
        if (Input::has('country_list')) {
            $q = self::sanitizeText(Input::get('country_list'));

            if (!is_array($q)) {
                $q = explode(',', $q);
            }

            if (sizeof($q) == 1) {
                $q  = urldecode($q[0]);
                $q = explode(',', $q);
            }
            foreach ($q as $p1) {
                foreach ($newResMemPartner as $res3) {
                    foreach ($res3->countries as $rescountries) {
                        if ($rescountries->id == $p1) {
                            if (!$newResCountry->contains($res3)) {
                                $newResCountry->add($res3);
                            }
                        }
                    }
                }
            }
        } else {
            $newResCountry = $newResMemPartner;
        }

        // filter country member
        if (Input::has('country_member_list')) {
            $q = self::sanitizeText(Input::get('country_member_list'));

            if (!is_array($q)) {
                $q = explode(',', $q);
            }

            if (sizeof($q) == 1) {
                $q  = urldecode($q[0]);
                $q = explode(',', $q);
            }

            foreach ($newResCountry as $res6) {
                if (in_array($res6->country_id, $q)) {
                    $newResMemCountry->add($res6);
                }
            }
        } else {
            $newResMemCountry = $newResCountry;
        }

        // filter network_id  && self::sanitizeText(Input::get('partner_id') != '0'
        if (Input::has('network_list')) {
            $q = self::sanitizeText(Input::get('network_list'));
            if (!is_array($q)) {
                $q = explode(',', $q);
            }
            if (sizeof($q) == 1) {
                $q  = urldecode($q[0]);
                $q = explode(',', $q);
            }
            foreach ($q as $p1) {
                foreach ($newResMemCountry as $res4) {
                    foreach ($res4->networks as $resnetworks) {
                        if ($resnetworks->id == $p1) {
                            if (!$newResNetwork->contains($res4)) {
                                $newResNetwork->add($res4);
                            }
                        }
                    }
                }
            }
        } else {
            $newResNetwork = $newResMemCountry;
        }
        // filter segment_id  && self::sanitizeText(Input::get('partner_id') != '0'
        if (Input::has('segment_list')) {
            $q = self::sanitizeText(Input::get('segment_list'));

            if (!is_array($q)) {
                $q = explode(',', $q);
            }

            if (sizeof($q) == 1) {
                $q  = urldecode($q[0]);
                $q = explode(',', $q);
            }

            $integerIDs = array_map('intval', $q);
            $results111 = DB::table('segment_user')->whereIn('segment_id', $integerIDs)
                    ->groupBy('user_id')->get();
            //$allmembers = Partner::whereIn('id',$q)->members;
            $q1 = array();
            foreach ($results111 as $res111) {
                $q1[] = $res111->user_id;
            }

            foreach ($newResNetwork as $res5) {
                if (array_search($res5->id, $q1) !== false) {
                    $newResSegment->add($res5);
                }
            }
        } else {
            $newResSegment = $newResNetwork;
        }

        // filter segment_id  && self::sanitizeText(Input::get('partner_id')) != '0'
        if (Input::has('displaychannel_list')) {
            $q = self::sanitizeText(Input::get('displaychannel_list'));

            if (!is_array($q)) {
                $q = explode(',', $q);
            }

            if (sizeof($q) == 1) {
                $q  = urldecode($q[0]);
                $q = explode(',', $q);
            }

            $integerIDs = array_map('intval', $q);
            $results111 = DB::table('channel_product')->whereIn('channel_id', $integerIDs)
                    ->groupBy('product_id')->get();
            //$allmembers = Partner::whereIn('id',$q)->members;
            $q1 = array();
            foreach ($results111 as $res111) {
                $q1[] = $res111->product_id;
            }

            foreach ($newResSegment as $res5) {
                if (array_search($res5->id, $q1) !== false) {
                    $newResDisplaychannel->add($res5);
                }
            }
        } else {
            $newResDisplaychannel = $newResSegment;
        }

        // filter supplier_id
        if (Input::has('brand_list')) {
            $q = self::sanitizeText(Input::get('brand_list'));

            if (!is_array($q)) {
                $q = explode(',', $q);
            }

            if (sizeof($q) == 1) {
                $q  = urldecode($q[0]);
                $q = explode(',', $q);
            }

            foreach ($q as $p1) {
                foreach ($newResDisplaychannel as $res2) {
                    if ($res2->brand_id == $p1) {
                        if (!$newResBrand->contains($res2)) {
                            $newResBrand->add($res2);
                        }
                    }
                }
            }
        } else {
            $newResBrand = $newResDisplaychannel;
        }

        // filter partner_banner_list
        if (Input::has('partner_banner_list')) {
            $q1 = self::sanitizeText(Input::get('partner_banner_list'));

            if (!is_array($q)) {
                $q = explode(',', $q);
            }

            if (sizeof($q) == 1) {
                $q  = urldecode($q[0]);
                $q = explode(',', $q);
            }

            $integerIDs = array_map('intval', $q1);
            $results111 = DB::table('banner')->whereIn('partner_id', $integerIDs)->get();

            //$allmembers = Partner::whereIn('id',$q)->members;
            $q = array();
            foreach ($results111 as $res111) {
                $q[] = $res111->id;
            }

            foreach ($newResBrand as $res1) {
                if (in_array($res1->id, $q)) {
                    $newResBannerPartner->add($res1);
                }
            }
        } else {
            $newResBannerPartner = $newResBrand;
        }

            // filter partner_cardprint_list
        if (Input::has('partner_cardprint_list')) {
            $q1 = self::sanitizeText(Input::get('partner_cardprint_list'));

            if (!is_array($q1)) {
                $q1 = explode(',', $q1);
            }

            if (sizeof($q1) == 1) {
                $q1  = urldecode($q1[0]);
                $q1 = explode(',', $q1);
            }

            $integerIDs = array_map('intval', $q1);
            $results111 = DB::table('cardprint')->whereIn('partner_id', $integerIDs)->get();

            //$allmembers = Partner::whereIn('id',$q)->members;
            $q = array();
            foreach ($results111 as $res111) {
                $q[] = $res111->id;
            }

            foreach ($newResBannerPartner as $res1) {
                if (in_array($res1->id, $q)) {
                    $newResCardPrint->add($res1);
                }
            }
        } else {
            $newResCardPrint = $newResBannerPartner;
        }

        // filter status_banner_list
        if (Input::has('status_banner_list')) {
            $q1 = self::sanitizeText(Input::get('status_banner_list'));

            if (!is_array($q)) {
                $q = explode(',', $q);
            }

            if (sizeof($q) == 1) {
                $q  = urldecode($q[0]);
                $q = explode(',', $q);
            }

            $integerIDs = array_map('intval', $q1);
            $results111 = DB::table('banner')->whereIn('status', $q1)->get();

            //$allmembers = Partner::whereIn('id',$q)->members;
            $q = array();
            foreach ($results111 as $res111) {
                $q[] = $res111->id;
            }

            foreach ($newResCardPrint as $res1) {
                if (in_array($res1->id, $q)) {
                    $newResBannerStatus->add($res1);
                }
            }
        } else {
            $newResBannerStatus = $newResCardPrint;
        }

        // filter mobile_status_banner_list
        if (Input::has('mobile_status_banner_list')) {
            $q1 = self::sanitizeText(Input::get('mobile_status_banner_list'));

            if (!is_array($q)) {
                $q = explode(',', $q);
            }

            if (sizeof($q) == 1) {
                $q  = urldecode($q[0]);
                $q = explode(',', $q);
            }

            $integerIDs = array_map('intval', $q1);
            $results111 = DB::table('banner')->whereIn('mob_status', $q1)->get();

            //$allmembers = Partner::whereIn('id',$q)->members;
            $q = array();
            foreach ($results111 as $res111) {
                $q[] = $res111->id;
            }

            foreach ($newResBannerStatus as $res1) {
                if (in_array($res1->id, $q)) {
                    $newResBannerMobStatus->add($res1);
                }
            }
        } else {
            $newResBannerMobStatus = $newResBannerStatus;
        }

        // filter Offers by display channel
        if (Input::has('offer_displaychannel_list')) {
            $q = self::sanitizeText(Input::get('offer_displaychannel_list'));

            if (!is_array($q)) {
                $q = explode(',', $q);
            }

            if (sizeof($q) == 1) {
                $q  = urldecode($q[0]);
                $q = explode(',', $q);
            }

            $integerIDs = array_map('intval', $q);
            $results111 = DB::table('channel_offer')->whereIn('channel_id', $integerIDs)
                    ->groupBy('offer_id')->get();
            //$allmembers = Partner::whereIn('id',$q)->members;
            $q1 = array();
            foreach ($results111 as $res111) {
                $q1[] = $res111->offer_id;
            }

            foreach ($newResBannerMobStatus as $res5) {
                if (array_search($res5->id, $q1) !== false) {
                    $newResOffersDisplaychannel->add($res5);
                }
            }
        } else {
            $newResOffersDisplaychannel = $newResBannerMobStatus;
        }

        // filter estatement_status
        if (Input::has('offer_status')) {
            $q = self::sanitizeText(Input::get('offer_status'));

            if (!is_array($q)) {
                $q = explode(',', $q);
            }

            if (sizeof($q) == 1) {
                $q  = urldecode($q[0]);
                $q = explode(',', $q);
            }

            foreach ($newResOffersDisplaychannel as $res1) {
                if (in_array($res1->status, $q)) {
                        $newResOfferStatus->add($res1);
                }
            }
        } else {
            $newResOfferStatus = $newResOffersDisplaychannel;
        }

        // filter segments by partner
        if (Input::has('partner_segment_list')) {
            $q = self::sanitizeText(Input::get('partner_segment_list'));

            if (!is_array($q)) {
                $q = explode(',', $q);
            }

            if (sizeof($q) == 1) {
                $q  = urldecode($q[0]);
                $q = explode(',', $q);
            }

            foreach ($newResOfferStatus as $res1) {
                if (in_array($res1->partner_id, $q)) {
                        $newResSegmentMembers->add($res1);
                }
            }
        } else {
            $newResSegmentMembers = $newResOfferStatus;
        }

        // filter affiliates by partner
        if (Input::has('affiliate_partner_list')) {
            $q = self::sanitizeText(Input::get('affiliate_partner_list'));

            if (!is_array($q)) {
                $q = explode(',', $q);
            }

            if (sizeof($q) == 1) {
                $q  = urldecode($q[0]);
                $q = explode(',', $q);
            }

            foreach ($newResSegmentMembers as $res1) {
                foreach ($res1->partners as $respartner) {
                    if (in_array($respartner->id, $q)) {
                        if (!$newResAffiliatePartners->contains($res1)) {
                            $newResAffiliatePartners->add($res1);
                        }
                    }
                }
            }
        } else {
            $newResAffiliatePartners = $newResSegmentMembers;
        }

        // filter affiliates by country
        if (Input::has('affiliate_country_list')) {
            $q = self::sanitizeText(Input::get('affiliate_country_list'));

            if (!is_array($q)) {
                $q = explode(',', $q);
            }

            if (sizeof($q) == 1) {
                $q  = urldecode($q[0]);
                $q = explode(',', $q);
            }

            foreach ($newResAffiliatePartners as $res1) {
                if (in_array($res1->country_id, $q)) {
                    if (!$newResAffiliateCountries->contains($res1)) {
                        $newResAffiliateCountries->add($res1);
                    }
                }
            }
        } else {
            $newResAffiliateCountries = $newResAffiliatePartners;
        }

        // filter affiliates by category
        if (Input::has('affiliate_category_list')) {
            $q = self::sanitizeText(Input::get('affiliate_category_list'));

            if (!is_array($q)) {
                $q = explode(',', $q);
            }

            if (sizeof($q) == 1) {
                $q  = urldecode($q[0]);
                $q = explode(',', $q);
            }

            foreach ($newResAffiliateCountries as $res1) {
                if (in_array($res1->category_id, $q)) {
                    if (!$newResAffiliateCategories->contains($res1)) {
                        $newResAffiliateCategories->add($res1);
                    }
                }
            }
        } else {
            $newResAffiliateCategories = $newResAffiliateCountries;
        }

        // filter affiliates Programs by channel
        if (Input::has('affiliate_program_channel_list')) {
            $q = self::sanitizeText(Input::get('affiliate_program_channel_list'));

            if (!is_array($q)) {
                $q = explode(',', $q);
            }

            if (sizeof($q) == 1) {
                    $q  = urldecode($q[0]);
                    $q = explode(',', $q);
            }

            foreach ($newResAffiliateCategories as $res1) {
                foreach ($res1->partners as $respartner) {
                    if (in_array($respartner->id, $q)) {
                        if (!$newResAffiliateProgramChannels->contains($res1)) {
                            $newResAffiliateProgramChannels->add($res1);
                        }
                    }
                }
            }
        } else {
            $newResAffiliateProgramChannels = $newResAffiliateCategories;
        }

        // filter affiliates Programs by affiliates
        if (Input::has('affiliate_list')) {
            $q = self::sanitizeText(Input::get('affiliate_list'));

            if (!is_array($q)) {
                $q = explode(',', $q);
            }

            if (sizeof($q) == 1) {
                    $q  = urldecode($q[0]);
                    $q = explode(',', $q);
            }

            foreach ($newResAffiliateProgramChannels as $res1) {
                if (in_array($res1->affiliate_id, $q)) {
                    if (!$newResAffiliateProgramAffiliates->contains($res1)) {
                        $newResAffiliateProgramAffiliates->add($res1);
                    }
                }
            }
        } else {
            $newResAffiliateProgramAffiliates = $newResAffiliateProgramChannels;
        }

//        return $this->paginate($newResAffiliateProgramAffiliates);
        if ($ormClass != 'App\Partner') {
            return $this->paginate($newResAffiliateProgramAffiliates);
        } else {
            $newResAffiliateProgramAffiliates;
        }
    }
} // EOC
