<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\App;
use View;
use Input;
use Validator;
use I;
use Illuminate\Support\Facades\Redirect;
use App\Brand as Brand;
use Response;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Brand Controller
 * 
 * @category   Controllers
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class BrandController extends BaseController
{
    protected $layout = "layouts.dashboard";

    /**
     * Return the validation rules
     *
     * @return array
     */
    private function getValidations() 
    {
        return array(
            'name' => 'required|min:1'
        );
    }

    /**
     * Display a list of the Brand
     *
     * @return void
     */
    public function listBrands()
    {
        if(!I::can('view_brands')){
            return Redirect::to(url("dashboard"));
        }
        $this->ormClass      = 'App\Brand';
        $this->search_fields = array('name');

        $brands = $this->getList();

        $page = Input::get('page');
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $brands = new LengthAwarePaginator($brands->forPage($page, 15), $brands->count(), 15, $page);

        return View::make("brand.list", array(
            'brands' => $brands
        ));
    }

    /**
     * Display the new brand window
     *
     * @return void
     */
    public function newBrand()
    {
        if(!I::can('create_brands')){
            return Redirect::to(url("dashboard"));
        }
        return View::make("brand.new", array(
            'brands' => Brand::where('draft', false)->orderBy('name')->pluck('name', 'id')
        ));
    }

    /**
     * Create a new brand resource
     *
     * @return void
     */
    public function createNewBrand()
    {
        if(!I::can('create_brands')){
            return Redirect::to(url("dashboard"));
        }
        $input = Input::all();
        foreach($input as $k=>$i){
            $input[$k] = self::sanitizeText($i);
        }
        $validator = Validator::make($input, $this->getValidations());

        if($validator->fails()) {
            return Redirect::to('dashboard/brands/new')->withErrors($validator)->withInput();
        }

        $brand        = new Brand();
        $brand->name  = self::sanitizeText(Input::get('name'));
        $brand->draft = false;

        $brand->save();

        return Redirect::to('dashboard/brands');
    }

    /**
     * View a single Brand entry
     *
     * @param int $id
     * @return void
     */
    public function viewBrand($enc_id = null)
    {
        $id = base64_decode($enc_id);
        if(!I::can('view_brands')){
            return Redirect::to(url("dashboard"));
        }
        $brand = Brand::find($id);

        if(!$brand) {
            \App::abort(404);
        }

        return View::make("brand.view", array(
            'brand'  => $brand,
            'brands' => Brand::where('draft', false)->orderBy('name')->pluck('name', 'id'),
        ));
    }

    /**
     * Delete a Brand instance
     *
     * @param int $id
     * @return void
     */
    public function deleteBrand($enc_id = null)
    {
        $id = base64_decode($enc_id);
        if(!I::can('delete_brands')){
            return Redirect::to(url("dashboard"));
        }
        $brand = Brand::find($id);

        if(!$brand) {
            App::abort(404);
        }

        $brand->delete();

        return Redirect::to('dashboard/brands');
    }

    /**
     * update a Brand instance
     *
     * @param int $id
     * @return void
     */
    public function updateBrand($enc_id = null)
    {
        $id = base64_decode($enc_id);
        if(!I::can('edit_brands')){
            return Redirect::to(url("dashboard"));
        }
        return View::make("brand.update", array(
            'id' => $id
        ));
    }

    /**
     * update a Brand instance
     *
     * @param int $id
     * @return void
     */
    public function saveBrand($enc_id = null)
    {
        $id = base64_decode($enc_id);
        if(!I::can('edit_brands')){
            return Redirect::to(url("dashboard"));
        }
        $brand = Brand::find($id);

        if(!$brand) {
            App::abort(404);
        }
        $input = Input::all();
        foreach($input as $k=>$i){
            $input[$k] = self::sanitizeText($i);
        }
        $validator = Validator::make($input, $this->getValidations());

        if($validator->fails()) {
            return Response::json(array(
                'failed' => true,
                'errors' => "Please ensure that you provide a valid brand name"
            ));
        }

        $brand->name = self::sanitizeText(Input::get('name'));
        $brand->draft = false;
        $brand->save();

        return Redirect::to(Input::get('redirect_to', 'dashboard/brands'));
    }

    /**
     * get fields for new Brand
     *
     * @return void
     */
    public function newPopoverBrand()
    {
        if(!I::can('create_brands')){
            return Redirect::to(url("dashboard"));
        }
        $brand       = new Brand();
        $brand->name = "New Brand";

        $brand->save();
        
        return View::make("brand.new_popover",
            array('brand' => $brand));
    }

    /**
     * update a Brand instance
     *
     * @param int $id
     * @return void
     */
    public function savePopoverBrand($enc_id = null)
    {
        $id = base64_decode($enc_id);
        if(!I::can('edit_brands')){
            return Redirect::to(url("dashboard"));
        }
        $brand = Brand::find($id);

        if(!$brand) {
            \App::abort(404);
        }
        $input = Input::all();
        foreach($input as $k=>$i){
            $input[$k] = self::sanitizeText($i);
        }
        $validator = Validator::make($input, $this->getValidations());

        if($validator->fails()) {
            return Response::json(array(
                'failed' => true,
                'errors' => "Please ensure that you provide a valid brand name"
            ));
        }

        $brand->name  = self::sanitizeText(Input::get('name'));
        $brand->draft = false;
        $brand->save();

        return Response::json(array(
            'failed' => false,
            'view_url' => url('dashboard/brands/getList/')
        ));
    }

    /**
     * return a html select list of all brands
     *
     * @return void
     */
    public function getBrandsList()
    {
        if(!I::can('view_brands')){
            return Redirect::to(url("dashboard"));
        }
        return View::make("brand.selectList",
            array('brands' => Brand::where('draft', false)->orderBy('name')->pluck('name', 'id'))
        );
    }
} // EOC