<?php
namespace App\Http\Controllers;
use View;
use Input;
use Validator;

use I;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Offer as Offer;
use App\Tier as Tier;
use ManagedObjectsHelper;
use Meta;
use App\Partner as Partner;
use App\Segment as Segment;

/**
 * Offer Controller
 *
 * @category   Controllers
 * @package    BLU
 * @author     Magid Mroueh <magid.mroueh@bluloyalty.com>
 * @copyright  2016 BLU
 * @link       http://blupoints.com
 */
class TriggersController extends BaseController {

    protected $layout = "layouts.dashboard";

    /**
     * Get all list data
     *
     * @return array
     */
    private function getAllLists() {
        $partner    = Auth::User()->getTopLevelPartner()->id;
        return array(
            'partners' => ManagedObjectsHelper::managedPartners()->sortBy('name')->pluck('name', 'id'),
            'segments' => ManagedObjectsHelper::managedSegments()->sortBy('name')->pluck('name', 'id'),
            'tiers'  => Tier::where('draft',false)->whereHas('Partner', function($q) use($partner){
                            $q->where('partner_id', $partner);
                        })->orderBy('name')->pluck('name','id'),
            'statuses' => Meta::offerStatuses(),
        );
    }

    /**
     * Return the offer validations
     *
     * @return array
     */
    private function getValidations() {
        return array(
            'name' => 'required',
        );
    }

    /**
     * Display a list of the offers
     *
     * @return void
     */
    public function listTriggers() {
        $this->ormClass = 'App\Offer';
        $this->defaultSort = 'id';
        $this->search_fields = array('name');
        $offers = $this->getList();

        return View::make("triggers.list", array(
                    'offers' => $offers,
                    'lists' => $this->getAllLists()
        ));
    }

    /**
     * get fields for new offer
     *
     * @return void
     */
    public function newTrigger() {
        $offer = new Offer();

        $offer->name = '';
        $offer->start_date = NULL;
        $offer->end_date = NULL;
        $offer->draft = TRUE;
        $offer->status = 'Active';
        $offer->logged_in = 0;
        $offer->logged_out = 1;

        $offer->save();

        // Link to top level partner automtically

        $partner = Auth::User()->getTopLevelPartner();

        $offer->partners()->attach($partner->id);
        $offer_id = base64_encode($offer->id);
        return Redirect::to(url("dashboard/triggers/view/1"));
    }

    /**
     * View a single Offer entry
     *
     * @param int $id
     * @return void
     */
    public function viewTrigger($enc_id = null) {
        $id = base64_decode($enc_id);

        return View::make("triggers.view", 
            $this->getAllLists()
        );
    }

    /**
     * Delete a offer instance
     *
     * @param int $id
     * @return void
     */
    public function deleteTrigger($enc_id = null) {
        $id = base64_decode($enc_id);
        $offer = Offer::find($id);

        if (!$offer) {
            App::abort(404);
        }

//        $offer->delete();
        $offer->deleted = 1;
        $offer->deleted_at = date('Y-m-d H:i:s');
        $offer->save();

        return Redirect::to('dashboard/triggers');
    }

    /**
     * Update a offer
     *
     * @param int $id
     * @return void
     */
    public function saveTrigger($enc_id) {
        $id = base64_decode($enc_id);
        $offer = Offer::find($id);

        
        if (!$offer) {
            App::abort(404);
        }

        $validator = Validator::make(Input::all(), $this->getValidations());

        if ($validator->fails()) {
            return Redirect::to(url("dashboard/triggers/view/{$enc_id}"))->withErrors($validator)->withInput();
        }

        $input = Input::all();

        foreach($input as $k=>$i){
            $input[$k] = self::sanitizeText($i);
        }

        foreach ($input as $k => $v) {
            if ($v == '') {
                unset($input[$k]);
            }
        }

        // Display Control
        if (!Input::has('logged_in')) {
            $input['logged_in'] = false;
        }
        if($input['logged_in'] == 1 && count($offer->segments) < 1){ //check if not selected segments
            return Redirect::to(url("dashboard/triggers/view/$enc_id"))->withErrors(['you have to select at least one segment if the offer is for Logged In users.'])->withInput();
        }
        
        // Display offline
        if (!Input::has('logged_out')) {
            $input['logged_out'] = false;
        }
        
        // Date
        if (Input::has('validity_start_date')) {
            if(strtotime(self::sanitizeText(Input::get('validity_start_date'))) > strtotime(self::sanitizeText(Input::get('validity_end_date')))){ //check if not selected segments
                return Redirect::to(url("dashboard/triggers/view/$enc_id"))->withErrors(['the valid from date should be earlier than the valid to.'])->withInput();
            }
            $input['validity_start_date'] = date('Y-m-d', strtotime(self::sanitizeText(Input::get('validity_start_date'))));
        }
        // Date
        if (Input::has('validity_end_date')) {
            if(strtotime(self::sanitizeText(Input::get('validity_start_date'))) > strtotime(self::sanitizeText(Input::get('validity_end_date')))){ //check if not selected segments
                return Redirect::to(url("dashboard/triggers/view/$enc_id"))->withErrors(['the valid from date should be earlier than the valid to.'])->withInput();
            }
            $input['validity_end_date'] = date('Y-m-d', strtotime(self::sanitizeText(Input::get('validity_end_date'))));
        }
        // Date
        if (Input::has('start_date')) {
            if(strtotime(self::sanitizeText(Input::get('start_date'))) > strtotime(self::sanitizeText(Input::get('end_date')))){ //check if not selected segments
                return Redirect::to(url("dashboard/triggers/view/$enc_id"))->withErrors(['the start date should be earlier than the end date.'])->withInput();
            }
            $input['start_date'] = date('Y-m-d', strtotime(self::sanitizeText(Input::get('start_date'))));
        }
        // Date
        if (Input::has('end_date')) {
            if(strtotime(self::sanitizeText(Input::get('start_date'))) > strtotime(self::sanitizeText(Input::get('end_date')))){ //check if not selected segments
                return Redirect::to(url("dashboard/triggers/view/$enc_id"))->withErrors(['the valid from should be earlier than the end date.'])->withInput();
            }
            $input['end_date'] = date('Y-m-d', strtotime(self::sanitizeText(Input::get('end_date'))));
        }

        $offer->draft = false;
        $offer->update($input);

        return Redirect::to('dashboard/triggers');
    }

    /**
     * Create a small LI list with offer partners
     *
     * @param int $user_id
     * @return Response
     */
    private function listPartners($enc_offer_id) {
        $offer_id = base64_decode($enc_offer_id);
        $offer = Offer::find($offer_id);
        return View::make('triggers.list.partnerList', array(
                    'offer' => $offer,
        ));
    }

    /**
     * link a partner to a offer
     *
     * @param int $offer id
     * @return void
     */
    public function postLink_partner($enc_id) {
        $id = base64_decode($enc_id);
        $partner_id = self::sanitizeText(Input::get('partner_id'));
        $offer = Offer::find($id);
        $partner = Partner::find($partner_id);

        if (!$offer->partners->contains($partner)) {
            $offer->partners()->attach($partner);
        }
        return $this->listPartners($enc_id);
    }

    /**
     * unlink a partner from offer
     *
     * @param int $offer id
     * @return void
     */
    public function getUnlink_partner($enc_offer_id, $enc_partner_id) {
        $offer_id = base64_decode($enc_offer_id);
        $partner_id = base64_decode($enc_partner_id);
        $offer = Offer::find($offer_id);

        $offer->partners()->detach($partner_id);
        return $this->listPartners($enc_offer_id);
    }

    /**
     * Create a small LI list with offer segments
     *
     * @param int $offer_id
     * @return Response
     */
    private function listSegments($enc_offer_id) {
        $offer_id = base64_decode($enc_offer_id);
        $offer = Offer::find($offer_id);
        return View::make('triggers.list.segmentList', array(
                    'offer' => $offer,
        ));
    }

    /**
     * link a segment to a offer
     *
     * @param int $offer id
     * @return void
     */
    public function postLink_segment($enc_id) {
        $id = base64_decode($enc_id);
        $segment_id = self::sanitizeText(Input::get('segment_id'));
        $offer = Offer::find($id);
        $segment = Segment::find($segment_id);

        if (!$offer->segments->contains($segment)) {
            $offer->segments()->attach($segment);
        }
        return $this->listSegments($enc_id);
    }

    /**
     * unlink a segment from a offer
     *
     * @param int $offer id, $segment_id
     * @return void
     */
    public function getUnlink_segment($enc_offer_id, $enc_segment_id) {
        $offer_id = base64_decode($enc_offer_id);
        $segment_id = base64_decode($enc_segment_id);
        $partner_id = self::sanitizeText(Input::get('partner_id'));
        $offer = Offer::find($offer_id);

        $offer->segments()->detach($segment_id);
        return $this->listSegments($enc_offer_id);
    }

    /**
     * Load a list of Tiers associated to the specified offer
     *
     * @param int $offer_id
     * @return View
     */
    private function listTiers($enc_offer_id) {
        $offer_id = base64_decode($enc_offer_id);
        $offer = Offer::find($offer_id);

        return View::make('triggers.list.tierList', array(
                    'offer' => $offer,
        ));
    }

    /**
     * Link the offer to a partner tier
     *
     * @param int $offer
     * @return View
     */
    public function postLink_tier($enc_offer_id) {
        $offer_id = base64_decode($enc_offer_id);
        $tier_id = self::sanitizeText(Input::get('tier_id'));

        return $this->listTiers($enc_offer_id);
    }

    /**
     * unlink a Tier from a offer
     *
     * @param int $offer id
     * @param int $tier_id
     * @return View
     */
    public function getUnlink_tier($enc_offer_id, $enc_tier_id) {
        $offer_id = base64_decode($enc_offer_id);
        $tier_id = base64_decode($enc_tier_id);

        return $this->listTiers($enc_offer_id);
    }

}

// EOC