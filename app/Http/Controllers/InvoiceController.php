<?php
namespace App\Http\Controllers;
use I;
use View;
use Input;
use Illuminate\Support\Facades\Redirect;
use ManagedObjectsHelper;
use Illuminate\Support\Facades\DB;
use App\UserInvoice as UserInvoice;
use Response;
/**
 * Invoice Controller
 * 
 * @category   Controllers
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class InvoiceController extends BaseController
{
    

   

    /**
     * Display the edit userinvoice page
     *
     * @return View
     */
    public function editInvoice($enc_userinvoice_id = null)
    {
        $userinvoice_id = base64_decode($enc_userinvoice_id);
        if(!I::can('view_userinvoice')){
            return Redirect::to(url("dashboard"));
        }
        $data = array(
            'userinvoice'   => UserInvoice::find($userinvoice_id)

        );

        return View::make("userinvoice.form", $data );
    }

    /**
     * Create a new userinvoice resource
     *
     * @param int $userinvoice_id
     * @return void
     */
    public function updateInvoice($enc_userinvoice_id = null)
    {
        $userinvoice_id = base64_decode($enc_userinvoice_id);
        if(!I::can('view_userinvoice')){
            return Redirect::to(url("dashboard"));
        }

        $userinvoice = UserInvoice::find($userinvoice_id);
        // Details
        $userinvoice->inserted       = self::sanitizeText(Input::get('inserted'));

        $userinvoice->push();

        return Response::json(array(
            'failed' => false,
            'view_url' => url('dashboard/userinvoice/list/')
        ));
    }

    

    /**
     * Render a list of userinvoices relevant to the Partner
     *
     * @param int $partner_id
     * @return View
     */
    public function listInvoices()
    {
        if(!I::can('view_userinvoice')){
            return Redirect::to(url("dashboard"));
        }
        $partner_id = ManagedObjectsHelper::managedPartners()->pluck('id');
        
        $data = array(
            'userinvoices' => DB::table('user_invoice')->whereIn('partner_id', $partner_id)->where('draft', false)->orderBy('inserted','asc')->orderBy('id','desc')->get()
        );

        return View::make("userinvoice.list", $data);
    }

} // EOC