<?php
namespace App\Http\Controllers;
use App\Media;
use App\Partner;
use Illuminate\Support\Facades\App;
use View;
use Input;
use Validator;
use Session;
use Security;
use I;
use Illuminate\Support\Facades\Auth;
use App\BluCollection as BluCollection;
use AuditHelper;
use Illuminate\Support\Facades\Redirect;
use Transaction;
use Notification;
use Ticket;
use Language;
use Localizationkey;
use LangKey;
use Network;
use ManagedObjectsHelper;
use User;
use App\Country;
use App\Admin as Admin;
use App\Role as Role;
use App\City as City;
use App\Area as Area;
use Meta;
use Illuminate\Support\Facades\DB;
use App\Category as Category;
use App\Affiliate as Affiliate;
use UCID;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Affiliate Controller
 *
 * @category   Controllers
 * @package    BLU
 * @author     Magid Mroueh <team@wixelhq.com>
 * @copyright  2017 BLU
 * @link       http://blupoints.com
 */
class AffiliateController extends BaseController
{
    protected $layout = "layouts.dashboard";

    private $auth_user = null;

    public function __construct()
    {
        parent::__construct();

        $this->auth_user = Auth::User();
    }

    /**
     * Get all list data
     *
     * @return array
     */
    private function getAllLists()
    {
        return array(
            'partners' => ManagedObjectsHelper::managedPartners()->sortBy('name')->pluck('name', 'id'),
            'countries' => Country::orderBy('name')->pluck('name', 'id'),
            'areas' => Area::orderBy('name')->pluck('name', 'id'),
            'cities' => City::orderBy('name')->pluck('name', 'id'),
            'statuses' => array('enabled'=>'Enabled', 'disabled'=>'Disabled'),
            'categories' => Category::with('children')->where('parent_category_id', 1)->where('draft', false)->orderBy('name', 'asc')->get(),
            'filter_categories' => Category::with('children')->where('parent_category_id', 1)->where('draft', false)->orderBy('name', 'asc')->pluck('name', 'id'),
        );
    }


    /**
     * List all Affiliates
     *
     * @return void
     */
    public function listAffiliates()
    {
        if(!I::can('view_affiliates')){
            return Redirect::to(url("dashboard"));
        }
        $this->ormClass      = 'App\Affiliate';
        $this->search_fields = array('name');

        $affiliates = $this->getList();

        $page = Input::get('page');
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $affiliates = new LengthAwarePaginator($affiliates->forPage($page, 15), $affiliates->count(), 15, $page);

        return View::make('affiliates.list', array(
            'affiliates' => $affiliates,
            'lists' => $this->getAllLists()
        ));
    }

    /**
     * Create a new Affiliate draft and redirect to it
     *
     * @return Response
     */
    public function newAffiliate()
    {
        if(!I::can('add_affiliates')){
            return Redirect::to(url("dashboard"));
        }
        $affiliate = new Affiliate();
        $affiliate->ucid            = UCID::newAffiliateID();
        $affiliate->category_id     = 1;
        $affiliate->status          = 'enabled';
        $affiliate->name            = 'New Name';
        $affiliate->description     = 'New Description';
        $affiliate->contact_name    = 'New Contact Name';
        $affiliate->contact_email  = 'New Contact Email';
        $affiliate->contact_number  = '961';
        $affiliate->draft           = 1;
        $affiliate->save();
        
        $affiliateId = base64_encode($affiliate->id);
        return Redirect::to(url("dashboard/affiliates/view/{$affiliateId}"));
    }

    /**
     * Display the Affiliate view page
     *
     * @param int $id
     * @return View
     */
    public function viewAffiliate($enc_id = null)
    {
        $id = base64_decode($enc_id);
        if(!I::can('view_affiliates')){
            return Redirect::to(url("dashboard"));
        }
        $affiliate = Affiliate::find($id);

        if(!$affiliate) {
            \App::abort(404);
        }

        $view_data = array(
            'affiliate'       => $affiliate,
            'partners'        => ManagedObjectsHelper::managedPartners()->pluck('name', 'id'),
            'managedPartners' => Auth::User()->managedParentPartnerIDs()
        );

        return View::make('affiliates.view', array_merge($view_data, $this->getAllLists()));
    }

    /**
     * Update a Affiliate account
     *
     * @param int $id
     * @return Response
     */
    public function updateAffiliate($enc_id = null)
    {
        $id = base64_decode($enc_id);
        if (!I::can('add_affiliates') && !I::can('edit_affiliates')) {
            return Redirect::to(url("dashboard"));
        }
        $affiliate = Affiliate::find($id);

        if(!$affiliate) {
            App::abort(404);
        }

        $validator = Validator::make(Input::all(), array(
            'name' => 'required|min:2',
            'contact_email' => 'nullable|email',
            'country_id'  => 'required',
        ));

        if($validator->fails()) {
            return Redirect::to(url("dashboard/affiliates/view/$enc_id"))->withErrors($validator);
        }

        if(!$affiliate->draft) {
            AuditHelper::record(
                Auth::User()->firstPartner()->id, "Affiliate '{$affiliate->id} ' profile updated by '{Auth::User()->name}'"
            );
        }

        $affiliate->status          = self::sanitizeText(Input::get('status'));
        $affiliate->name            = self::sanitizeText(Input::get('name'));
        
        $affiliate->description     = self::sanitizeText(Input::get('description'));
        if(Input::has('contact_name')){
        $affiliate->contact_name    = self::sanitizeText(Input::get('contact_name'));
        }
        if(Input::has('contact_email')){
        $affiliate->contact_email   = self::sanitizeText(Input::get('contact_email'));
        }
        if(Input::has('contact_number')){
        $affiliate->contact_number  = self::sanitizeText(Input::get('contact_number'));
        }
        if(Input::has('contact_website')){
        $affiliate->contact_website = self::sanitizeText(Input::get('contact_website'));
        }
        $affiliate->category_id     = self::sanitizeText(Input::get('category_id'));
        $affiliate->country_id      = self::sanitizeText(Input::get('country_id'));
        $affiliate->area_id         = self::sanitizeText(Input::get('area_id'));
        $affiliate->city_id         = self::sanitizeText(Input::get('city_id'));
        if(Input::has('address_1')){
        $affiliate->address_1       = self::sanitizeText(Input::get('address_1'));
        }
        if(Input::has('address_2')){
        $affiliate->address_2       = self::sanitizeText(Input::get('address_2'));
        }

        if(Input::hasFile('image')) {
            $file = Input::file('image');
            $path = public_path('uploads');
            $ext  = strtolower($file->getClientOriginalExtension());
            $size = $file->getSize();
            $type = $file->getMimeType();
            $name = md5(time()).".{$ext}";

            if(in_array($ext, Meta::allowedImageExts() )) {
                Input::file('image')->move($path, $name);

                $f = new Media();
                $f->name = $name;
                $f->size = $size;
                $f->type = $type;
                $f->ext  = $ext;
                $f->save();

                $affiliate->image = $f->id;
            }
        }
        $affiliate->draft = '0';
        $affiliate->save();

        return Redirect::to('dashboard/affiliates');
    }

    /**
     * Delete a Affiliate account
     *
     * @param int $enc_id
     * @return Response
     */
    public function deleteAffiliate($enc_id = null)
    {
        $id = base64_decode($enc_id);
        
        if(!I::can('delete_affiliates')){
            return Redirect::to(url("dashboard"));
        }

        $affiliate = Affiliate::find($id);

        if(!$affiliate) {
            App::abort(404);
        }

        Affiliate::find($id)->delete();

        return Redirect::to('dashboard/affiliates');
    }

    /**
     * Attach a user account to a partner
     *
     * @param int $user_id
     * @return Resposne
     */
    public function attachAffiliatePartner($enc_affiliate_id = null)
    {
        $affiliate_id = base64_decode($enc_affiliate_id);
        $affiliate    = Affiliate::find($affiliate_id);
        $partner = Partner::find(self::sanitizeText(Input::get('partner_id')));

        if(!$affiliate || !$partner) {
            App::abort(404);
        }

        if(!$affiliate->partners->contains($partner)) {
            $affiliate->partners()->attach($partner);
        }

        return $this->listAffiliatePartners($enc_affiliate_id);
    }
    
    /**
     * Unlink a partner from a affiliate
     *
     * @param int $enc_affiliate_id
     * @param int $enc_partner_id
     * @return Response
     */
    public function unlinkAffiliatePartner($enc_affiliate_id = null, $enc_partner_id = null)
    {
        $affiliate_id = base64_decode($enc_affiliate_id);
        $partner_id = base64_decode($enc_partner_id);
        $affiliate = Affiliate::find($affiliate_id);
        $partner = Partner::find($partner_id);

        if(!$affiliate || !$partner) {
            App::abort(404);
        }

        $affiliate->partners()->detach($partner);

        return $this->listAffiliatePartners($enc_affiliate_id);
    }
    
    /**
     * List partners
     *
     * @param int $enc_affiliate_id
     * @return Response
     */
    public function listAffiliatePartners($enc_affiliate_id)
    {
        $affiliate_id = base64_decode($enc_affiliate_id);
        $affiliate = Affiliate::find($affiliate_id);

        if(!$affiliate) {
            \App::abort(404);
        }

        return View::make('affiliates.partnerlist', array(
            'affiliate' => $affiliate
        ));
    }

} // EOC