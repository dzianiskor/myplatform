<?php
namespace App\Http\Controllers;
use App\Media;
use I;
use Meta;
use View;
use ManagedObjectsHelper;
use Illuminate\Support\Facades\Auth;
use Validator;
use Input;
use \App\MobileGameAds as MobileGameAds;
use Illuminate\Support\Facades\Redirect;

/**
 * Mobile Game Controller
 *
 * @category   Controllers
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class MobileGameAdsController extends BaseController
{
    protected $layout = "layouts.dashboard";

    private function getValidations()
    {
        return array(
            'title'       => 'required',
            'description' => 'required',
            'url' => 'required',
        );
    }
    /**
     * Fetch the contextual lists
     *
     * @return array
     */
    private function getAllLists()
    {
        return array(
            'partners'  => ManagedObjectsHelper::managedPartners()->sortBy('name')->pluck('name', 'id')->toArray()
        );
    }

    /**
     * Display a list of the currencies
     *
     * @return void
     */
    public function getIndex()
    {
        if(!I::can('view_mobile_game_ads')){
            return Redirect::to(url("dashboard"));
        }
        $this->ormClass      = 'App\MobileGameAds';
        $this->search_fields = array('title');

		$allMobileGamesAds		= $this->getList();
		$allManagedPartners		= ManagedObjectsHelper::managedPartners()->sortBy('name')->pluck('id')->toArray();
            $allowedMobileGamesAds	= array();
            $uniqueMobileGamesAds	= array();
            foreach ($allMobileGamesAds as $mobileGameAd) {
                    if(in_array($mobileGameAd->partner_id,$allManagedPartners)){
                                    $allowedMobileGamesAds[] = $mobileGameAd;
                    }
            }

        return View::make("mobilegameads.list", array(
            'mobilegameads' => $allowedMobileGamesAds
        ));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getCreate()
    {
        if (!I::can('view_mobile_game_ads') && !I::can('create_mobile_game')) {
            return Redirect::to(url("dashboard"));
        }
        $mobilegamead        = new MobileGameAds();
        $mobilegamead->title = "New Mobile Game Ad";
        $mobilegamead->draft = true;
        $mobilegamead->save();

        $mobileGameAdId = base64_encode($mobilegamead->id);
        return Redirect::to(url("dashboard/mobilegameads/edit/$mobileGameAdId"));
    }

    /**
     * Show the currency edit page
     *
     * @param integer $currency_id
     * @return Response
     */
    public function getEdit($enc_id)
    {
        $id = base64_decode($enc_id);
        if (!I::can('view_mobile_game_ads') && !I::can('edit_mobile_game_ads')) {
            return Redirect::to(url("dashboard"));
        }
        $mobilegamead = MobileGameAds::find($id);

        if(!$mobilegamead) {
            App::abort(404);
        }
        
        $managedPartners = Auth::User()->managedPartners();
        if(isset($mobilegamead->partner_id) && $mobilegamead->partner_id != 0 ){
            $can_view_game_ad = false;
            foreach($managedPartners as $manP){
                    if($manP->id == $mobilegamead->partner_id){
                        $can_view_game_ad = true;
                        break;
                    }
            }
            if($can_view_game_ad== false){
                return Redirect::to(url("dashboard"));
            }
        }
        return View::make("mobilegameads.view", array_merge(
           array('mobilegamead'=> $mobilegamead), $this->getAllLists()
        ));
    }

    /**
     * Delete the currency
     *
     * @param integer $currency_id
     * @return void
     */
    public function getDelete($enc_id)
    {
        $id = base64_decode($enc_id);
        if (!I::can('delete_mobile_game_ads')) {
            return Redirect::to(url("dashboard"));
        }
        $mobilegamead = MobileGameAds::find($id);

        if(!$mobilegamead) {
            App::abort(404);
        }

        $mobilegamead->delete();

        return Redirect::to(url("dashboard/mobilegameads"));
    }

    /**
     * apply changes
     *
     * @param integer $currency_id
     * @return Response
     */
    public function postUpdate($enc_id)
    {
        $id = base64_decode($enc_id);
        if (!I::can('edit_mobile_game_ads')) {
            return Redirect::to(url("dashboard"));
        }
        $mobilegamead = MobileGameAds::find($id);

        if(!$mobilegamead) {
            App::abort(404);
        }
        
        $managedPartners = Auth::User()->managedPartners();
         if(isset($mobilegamead->partner_id) && $mobilegamead->partner_id != 0 ){
            $can_view_game_ad = false;
            foreach($managedPartners as $manP){
                if($manP->id == $mobilegamead->partner_id){
                    $can_view_game_ad = true;
                    break;
                }
            }
            if($can_view_game_ad== false && $mobilegamead->draft == false){
                return Redirect::to(url("dashboard"));
            }
         }
         $input = Input::all();
        foreach($input as $k=>$i){
            $input[$k] = self::sanitizeText($i);
        }
        $validator = Validator::make($input, $this->getValidations());

        if($validator->fails()) {
            return Redirect::to("dashboard/mobilegameads/edit/$id")->withErrors($validator)->withInput();
        }

        $mobilegamead->title           = self::sanitizeText(Input::get('title'));
        $mobilegamead->description           = self::sanitizeText(Input::get('description'));
        $mobilegamead->url           = self::sanitizeText(Input::get('url'));
        $mobilegamead->partner_id           = self::sanitizeText(Input::get('partner_id'));
        $mobilegamead->draft = false;
        if(Input::hasFile('image')) {
            $file = Input::file('image');
            $path = public_path('uploads');
            $ext  = strtolower($file->getClientOriginalExtension());
            $size = $file->getSize();
            $type = $file->getMimeType();
            $name = md5(time()).".{$ext}";

            if(in_array($ext, Meta::allowedImageExts() )) {
                Input::file('image')->move($path, $name);

                $f = new Media();
                $f->name = $name;
                $f->size = $size;
                $f->type = $type;
                $f->ext  = $ext;
                $f->save();

                $mobilegamead->media_id = $f->id;
            }
        }

        //$mobilegame->media_id       = self::sanitizeText(Input::get('media_id'));

        $mobilegamead->save();

        return Redirect::to(Input::get('redirect_to', 'dashboard/mobilegameads'));
    }
} // EOC