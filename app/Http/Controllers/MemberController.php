<?php
namespace App\Http\Controllers;

use View;
use Input;
use Validator;
use Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use I;
use Illuminate\Support\Facades\Auth;
use App\BluCollection;
use AuditHelper;
use Illuminate\Support\Facades\Redirect;
use ManagedObjectsHelper;
use App\User as User;
use App\Role as Role;
use App\AffiliateProgram as AffiliateProgram;
use App\AffiliateProgramsMemberships as AffiliateProgramsMemberships;
use App\City as City;
use App\Area as Area;
use App\Occupation as Occupation;
use Meta;
use InputHelper;
use App\Balance as Balance;
use Illuminate\Support\Facades\Hash;
use App\Media as Media;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Member Controller
 *
 * @category   Controllers
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class MemberController extends BaseController
{
    protected $layout = "layouts.dashboard";

    private $auth_user = null;

    public function __construct()
    {
        parent::__construct();

        $this->auth_user = Auth::User();
    }

    /**
     * Fetch the data for the partner view
     *
     * @return array
     */
    private function getData()
    {
        return array(
            'countries'   => \App\Country::orderBy('name')->pluck('name', 'id'),
            'cities'      => City::orderBy('name')->pluck('name', 'id'),
            'areas'       => Area::orderBy('name')->pluck('name', 'id'),
            'occupations' => Occupation::orderBy('name')->pluck('name', 'id'),
            'statuses'    => Meta::memberStatuses(),
            'affiliatePrograms' => ManagedObjectsHelper::managedAffiliatePrograms()->sortBy('name')->pluck('name', 'id')
        );
    }

    /**
     * Get all list data
     *
     * @return array
     */
    private function getAllLists($partnerIds = null)
    {
        return array(
            'partners'  => ManagedObjectsHelper::managedPartners()->sortBy('name')->pluck('name', 'id'),
            'countries' => \App\Country::orderBy('name')->pluck('name', 'id'),
            'segments'  => ManagedObjectsHelper::managedSegments()->sortBy('name')->pluck('name', 'id'),
        );
    }

    /**
     * Verify that the values passed are unique
     *
     * @param string $field
     * @param string $value
     * @param int $exclusion_id
     * @return Response
     */
    public function verifyUniqueField($field = null, $value = null, $exclusion_id = null)
    {
        if (!in_array($field, array('email', 'username', 'mobile'))) {
            App::abort(400);
        }

        $exclusion_id = base64_decode($exclusion_id);

        $row = DB::table('users')->where($field, '=', $value)->first();

        if (!$row) {
            return Response::json(array('unique' => true));
        }

        if ($row->id == $exclusion_id) {
            return Response::json(array('unique' => true));
        } else {
            return Response::json(array('unique' => false));
        }
    }

    /**
     * List all members
     *
     * @return void
     */
    public function listMembers()
    {
        if (!I::can('view_members')) {
            return Redirect::to(url("dashboard"));
        }
        $admin_partner = Auth::User()->getTopLevelPartner();
        if($admin_partner->has_middleware == '1'){
            $users = Auth::User()->managedMembers(false);
            $temp_users = $users->get();
            $user_ids_used = array();
            if(Input::has('q')){
                $term = self::sanitizeText(Input::get('q'));
            }
            else{
                $term = '';
            }
            foreach($temp_users as $temp_u){
                $user_ids_used[] = $temp_u->id;
            }
            
            $i = 0;
            $temp_ids = [];
            $response = [];

            while($i < count($user_ids_used)){
                array_push($temp_ids, $user_ids_used[$i]);
                if($i !== 0  && ($i == count($user_ids_used) - 1 || $i % 30000 == 0)) {      
                    if($term == ''){
                        $url = $admin_partner->link_middleware_api . '?method=sensitiveInfo&api_key='.$admin_partner->middleware_api_key;
                        $json_ids = json_encode($temp_ids);
                        //$url .= "&user_ids=" . $json_ids . "&term='" . $term . "'";
                        $post_fields = "user_ids=" . $json_ids ;
                    }
                    else{
                        $url = $admin_partner->link_middleware_api . '?method=searchSensitive&api_key='.$admin_partner->middleware_api_key;
                        $json_ids = json_encode($temp_ids);
                        //$url .= "&user_ids=" . $json_ids . "&term='" . $term . "'";
                        $post_fields = "term=" . $term . "&user_ids=" . $json_ids;
                    }
                    $curl = curl_init();
                    // Set some options - we are passing in a useragent too here
                    curl_setopt_array($curl, array(
                        CURLOPT_RETURNTRANSFER => 1,
                        CURLOPT_URL => $url,
                        CURLOPT_POST => true,
                        CURLOPT_POSTFIELDS => $post_fields,
                        CURLOPT_SSL_VERIFYPEER => False,
                        CURLOPT_USERAGENT => 'Testing Sensitive Info'
                    ));
                    // Send the request & save response to $resp
                    $resp = curl_exec($curl);
                    if(curl_errno($curl)){
                        var_dump(curl_error($curl));
                        exit();
                    }
                    $resp_curl1 = json_decode($resp);
                    $response = array_merge($response, $resp_curl1);
                    $temp_ids = [];
                   
                }
                $i++;
            }
            $resp_curl = $response;
            
            //var_dump($resp_curl);
            if ($resp_curl) {
                $Users_info = $resp_curl;
                $buffer = new BluCollection();

                foreach ($Users_info as $member) {
                    if (in_array($member->blu_id, $user_ids_used)) {
                        $u = new User();
                        $u->user_id = $member->blu_id;
                        $u->first_name = $member->first_name;
                        $u->last_name = $member->last_name;
                        $u->email = $member->email;
                        $u->normalized_mobile = $member->mobile;
                        $user = User::find($member->blu_id);
                        $u->country_id = $user->country_id;
                        $u->status = $user->status;
                        $u->marital_status = strtolower($user->marital_status);
                        $buffer->add($u);
                    }
                }
                
                $buffer = $buffer->paginateCollection($buffer)->appends(Input::except('page'));

                return View::make('member.search', array(
                            'members' => $buffer,
                            'lists' => $this->getAllLists(),
                            'countMembers' => count($resp_curl)
                ));
            } else {
//                var_dump($url);
//                var_dump($post_fields);
            }
        }

        $partnerIds = Auth::User()->managedPartnerIDs();

        $query = User::where('users.draft', false)->with('country')->with('networks');

        if (Input::has('q') && Input::has('q') != '')
        {
            $name = self::sanitizeText(Input::get('q'));
            $query->where(function ($query) use ($name, $partnerIds) {
                $query->whereHas('references', function ($query) use ($name, $partnerIds) {
                    $query->where('number', $name)
                        ->whereIn('partner_id', $partnerIds);
                })
                ->orWhere('users.username', 'like', "{$name}%")
                      ->orWhere(DB::raw('CONCAT_WS(" ", users.first_name, users.last_name)'), 'like', "{$name}%")
                      ->orWhere('users.first_name', 'like', "{$name}%")
                      ->orWhere('users.middle_name', 'like', "{$name}%")
                      ->orWhere('users.last_name', 'like', "{$name}%")
                      ->orWhere('users.email', 'like', "{$name}%")
                      ->orWhere('users.mobile', 'like', "{$name}%")
                      ->orWhere('users.normalized_mobile', 'like', "{$name}%");
            });
        }

        $search_query = '';

        if (Input::has('country') && Input::has('country') != '') {
            $countryIds = explode(',', Input::get('country'));
            foreach ($countryIds as $countryId) {
                $search_query .= ($search_query == '') ? '?' : '&';
                $search_query .= 'country[]=' . $countryId;
            }
            $query = $query->whereIn('users.country_id', $countryIds);
        }

        if (Input::has('segment') && Input::has('segment') != '') {
            $segmentIds = explode(',', Input::get('segment'));
            foreach ($segmentIds as $segmentId) {
                $search_query .= ($search_query == '') ? '?' : '&';
                $search_query .= 'segment[]=' . $segmentId;
            }
            $query = $query->whereHas('segments', function ($u) use ($segmentIds) {
                $u->whereIn('segment_id', $segmentIds);
            });
        }

        if (Input::has('partner') && Input::has('partner') != '') {
            //partners must be accessible - array_intersect
            $partnerIds = array_intersect(explode(',', Input::get('partner')), $partnerIds);
            $query = $query->whereHas('partners', function ($u) use ($partnerIds) {
                $u->whereIn('partner_id', $partnerIds);
            });
        } else {
            $query = $query->whereHas('partners', function ($u) use ($partnerIds) {
                $u->whereIn('partner_id', $partnerIds);
            });
        }

        $query = $query->orderBy('users.id');

        return View::make('member.list', array(
            'members' => $query->paginate(15)->appends(Input::except('page')),
            'lists' => $this->getAllLists($partnerIds),
            'search_query' => $search_query
        ));
    }

    /**
     * Create a new member draft and redirect to it
     *
     * @return Response
     */
    public function newMember()
    {
        if (!I::can('create_members')) {
            return Redirect::to(url("dashboard"));
        }
        $user = User::createNewUser();

        $userId = base64_encode($user->id);
        return Redirect::to(url("dashboard/members/view/{$userId}"));
    }

    /**
     * Display the member view page
     *
     * @param int $id
     * @return View
     */
    public function viewMember($enc_id = null)
    {
        $id = base64_decode($enc_id);
        if (!I::can('view_members')) {
            return Redirect::to(url("dashboard"));
        }
        $member = User::find($id);
        if (!$member) {
            \App::abort(404);
        }
        $bool_ismember_partner = false;
        if (I::am("BLU Admin")) {
            $bool_ismember_partner = true;
        }
        $mem_partners = $member->partners()->get();

        foreach ($mem_partners as $mem_part) {
            if (in_array($mem_part->id, Auth::User()->managedPartnerIDs())) {
                $bool_ismember_partner = true;
                break;
            }
        }
        $user_ids_used = array($id);
        $term = '';
        if ($bool_ismember_partner == false) {
            return Redirect::to(url("dashboard"));
        }
        $admin_partner = Auth::User()->getTopLevelPartner();
        if ($admin_partner->has_middleware == '1') {
            $url = $admin_partner->link_middleware_api . '?method=sensitiveInfo&api_key='.$admin_partner->middleware_api_key;
            $json_ids = json_encode($user_ids_used);
            $url .= "&user_ids=" . $json_ids ;

            $curl = curl_init();
            // Set some options - we are passing in a useragent too here
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => $url,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_USERAGENT => 'Testing Sensitive Info'
            ));
            // Send the request & save response to $resp
            $resp = curl_exec($curl);
            $resp_curl = json_decode($resp);
            if ($resp_curl) {
                $Users_info = $resp_curl;
                foreach ($Users_info as $member1) {
                    if (in_array($member1->blu_id, $user_ids_used)) {
                        $member->first_name = $member1->first_name;
                        $member->last_name = $member1->last_name;
                        $member->email = $member1->email;
                        $member->mobile = $member1->mobile;
                        $member->title = $member1->title;
                    }
                }
            }
        }

        $roles   = Session::get('user_role_ids');
        $role_id = min($roles);
        $roles   = Role::where('id', '>=', $role_id)->orderBy('name')->pluck('name', 'id');
        $member->marital_status = strtolower($member->marital_status);
        $view_data = array(
            'member'          => $member,
            'roles'           => $roles,
            'partners'        => ManagedObjectsHelper::managedPartners()->sortBy('name')->pluck('name', 'id'),
            'segments'        => ManagedObjectsHelper::managedSegments()->pluck('name', 'id'),
            'managedPartners' => Auth::User()->managedParentPartnerIDs(),
            'allAffiliatePrograms' => AffiliateProgram::where('draft', 0)->pluck('name', 'id'),
        );

        return View::make('member.view', array_merge($view_data, $this->getData()));
    }

    /**
     * Update a member account
     *
     * @param int $id
     * @return Response
     */
    public function updateMember($enc_id = null)
    {
        $id = base64_decode($enc_id);
        if (!I::can('edit_members')) {
            return Redirect::to(url("dashboard"));
        }
        $user = User::find($id);

        if (!$user) {
            \App::abort(404);
        }
        $input = Input::all();
        foreach ($input as $k => $i) {
            $input[$k] = self::sanitizeText($i);
        }
        $validator = Validator::make($input, array(
            'first_name' => 'required|min:2',
            'last_name'  => 'required|min:2',
            'email'  => 'nullable|email',
            'mobile'  => 'required'
        ));

        if ($validator->fails()) {
            return Redirect::to(url("dashboard/members/view/$enc_id"))->withErrors($validator);
        }

        // Format DOB
        $dob = strtotime(
            self::sanitizeText(Input::get('day')).'-'.self::sanitizeText(Input::get('month')).'-'.self::sanitizeText(Input::get('year'))
        );

        if (!$user->draft) {
            //$auth_user = Auth::User();

            //AuditHelper::record(
            //    Auth::User()->firstPartner()->id, "Member '{$user->id} / {$user->email}' profile updated by '{Auth::User()->name}'"
            //);
        }

        $user->status         = self::sanitizeText(Input::get('status'));
        $user->title          = self::sanitizeText(Input::get('title'));
        $user->first_name     = self::sanitizeText(Input::get('first_name'));
        $user->middle_name    = self::sanitizeText(Input::get('middle_name'));
        $user->last_name      = self::sanitizeText(Input::get('last_name'));
        $user->mobile         = InputHelper::sanitizeMobile(self::sanitizeText(Input::get('mobile')));
        $user->email          = self::sanitizeText(Input::get('email'));
        $user->marital_status = self::sanitizeText(Input::get('marital_status'));
        $user->gender         = self::sanitizeText(Input::get('gender'));
        $user->occupation_id  = self::sanitizeText(Input::get('occupation_id'));
        $user->tag            = self::sanitizeText(Input::get('tag'));
        $user->income_bracket = self::sanitizeText(Input::get('income_bracket'));
        $user->num_children   = self::sanitizeText(Input::get('num_children'));
        $user->country_id     = self::sanitizeText(Input::get('country_id'));
        $user->area_id        = self::sanitizeText(Input::get('area_id'));
        $user->city_id        = self::sanitizeText(Input::get('city_id'));
        $user->address_1      = self::sanitizeText(Input::get('address_1'));
        $user->address_2      = self::sanitizeText(Input::get('address_2'));
        $user->dob            = date("Y-m-d H:i:s", $dob);
        $user->telephone_code = self::sanitizeText(Input::get('telephone_code'));
        $user->normalized_mobile = InputHelper::normalizeMobileNumber(InputHelper::sanitizeMobile(self::sanitizeText(Input::get('mobile'))), self::sanitizeText(Input::get('telephone_code'))) ;

        // Normalize phone number
        $user->normalized_mobile = InputHelper::normalizeMobileNumber($user->mobile, $user->telephone_code);

        if (Input::hasFile('image')) {
            $file = Input::file('image');
            $path = public_path('uploads');
            $ext  = strtolower($file->getClientOriginalExtension());
            $size = $file->getSize();
            $type = $file->getMimeType();
            $name = md5(time()).".{$ext}";

            if (in_array($ext, Meta::allowedImageExts())) {
                Input::file('image')->move($path, $name);

                $f = new Media();
                $f->name = $name;
                $f->size = $size;
                $f->type = $type;
                $f->ext  = $ext;
                $f->save();

                $user->profile_image = $f->id;
            }
        }

        if (Input::has('pin')) {
            $user->passcode = self::sanitizeText(Input::get('pin'));
        }

        if (Input::has('username')) {
            $user->username = self::sanitizeText(Input::get('username'));
        }

        if (Input::has('password')) {
            $user->password = Hash::make(self::sanitizeText(Input::get('password')));
        }

        $user->sendWelcomeMessage();
        $user->save();

        return Redirect::to(Input::get('redirect_to', 'dashboard/members'));
    }

    /**
     * Delete a member account
     *
     * @param int $id
     * @return Response
     */
    public function deleteMember($enc_id = null)
    {
        $id = base64_decode($enc_id);
        if (!I::can('delete_members')) {
            return Redirect::to(url("dashboard"));
        }
        if (!I::am('BLU Admin')) {
//            App::abort(400);
            return Redirect::to('dashboard/members')->with(
                "flash_error",
                "You don't have enough permissions to delete members."
            );
        }

        User::find($id)->delete();

        return Redirect::to('dashboard/members');
    }

    /**
     * Resend a member pin number via SMS
     *
     * @param int $id
     * @return Response
     */
    public function getSendpin($enc_id = null)
    {
        $id = base64_decode($enc_id);
        if (!I::can("resend_pin")) {
            App::abort(400);
        }

        $user = User::find($id);

        if (!$user) {
            \App::abort(404);
        }

        $partner  = Auth::User()->getTopLevelPartner();

        $resSms = APIController::getSendsms($user, $partner->id, "pin_resend");


            //AuditHelper::record(
            //    Auth::User()->firstPartner()->id, "PIN re-sent to member '{$user->id} / {$user->email}' by '{Auth::User()->name}'"
            //);

            return Response::json(array(
            'message' => 'The Pin Code has been sent to the member via SMS.'
            ));
    }

    /**
     * Fetch the user balances for the various networks
     *
     * @param int $id
     * @return View
     */
    public function getBalances($enc_id = null)
    {
        $id = base64_decode($enc_id);
        $balances   = array();
        $user = User::where('id', $id)->first();

        if (!I::am('BLU Admin')) {
            $admin_partner = Auth::User()->getTopLevelPartner();
            $balances   = Balance::where('network_id', $admin_partner->firstNetwork()->id)->where('user_id', $user->id)->with('network')->first();
        }
        return View::make('member.ejs.balances', array(
            'user' => $user,
            'balance' => $balances
        ));
    }


    /**
     * Fetch the user balances for the various networks
     *
     * @param int $id
     * @return View
     */
    public function getSendsms($enc_user_id, $enc_partner_id, $smstype = "new_user")
    {
        $user_id = base64_decode($enc_user_id);
        $partner_id = base64_decode($enc_partner_id);


        $resSms = APIController::getSendsms($user_id, $partner_id, $smstype);

        $arr_response = array();

        if ($resSms == true) {
            $arr_response['status'] = '200';
            $arr_response['message'] ='Your SMS has been sent successfully';
        }
        return json_encode($arr_response);
    }

    /**
     * Attach a user account to a partner
     *
     * @param int $enc_member_id
     * @return Resposne
     */
    public function createAffiliateProgramMembership($enc_member_id = null)
    {
        $member_id = base64_decode($enc_member_id);
        $affiliateProgram    = AffiliateProgram::find(Input::get('program_id'));

        if (!$affiliateProgram) {
            \App::abort(404);
        }
        $affiliateProgramMembership = new AffiliateProgramsMemberships();
        $affiliateProgramMembership->user_id = $member_id;
        $affiliateProgramMembership->membership_number = Input::get('membership_number');
        $affiliateProgramMembership->program_id = $affiliateProgram->id;
        $affiliateProgramMembership->save();

        return $this->listAffiliateProgramMemberships($enc_member_id);
    }

    /**
     * Unlink a partner from a affiliate
     *
     * @param int $enc_affiliateProgram_id
     * @param int $enc_partner_id
     * @return Response
     */
    public function deleteAffiliateProgramMembership($enc_member_id = null, $enc_apm_id = null)
    {
        $apm_id = base64_decode($enc_apm_id);
        $affiliateProgramMembership = AffiliateProgramsMemberships::find($apm_id);

        if (!$affiliateProgramMembership) {
            \App::abort(404);
        }

        $affiliateProgramMembership->delete();

        return $this->listAffiliateProgramMemberships($enc_member_id);
    }

    /**
     * List partners
     *
     * @param int $enc_member_id
     * @return Response
     */
    public function listAffiliateProgramMemberships($enc_member_id)
    {
        $member_id = base64_decode($enc_member_id);
        $member = User::find($member_id);

        if (!$member) {
            \App::abort(404);
        }

        return View::make('member.membershipslist', array(
            'member' => $member
        ));
    }

    /**
     * View a partner from a affiliate
     *
     * @param int $enc_affiliateProgram_id
     * @param int $enc_partner_id
     * @return Response
     */
    public function viewAffiliateProgramMembership($enc_apm_id = null)
    {
        $apm_id = base64_decode($enc_apm_id);
        $affiliateProgramMembership = AffiliateProgramsMemberships::find($apm_id);

        if (!$affiliateProgramMembership) {
            \App::abort(404);
        }

        return View::make('member.viewmembership', array(
            'affiliateProgramMembership' => $affiliateProgramMembership,
            'affiliatePrograms' => ManagedObjectsHelper::managedAffiliatePrograms()->sortBy('name')->pluck('name', 'id')
        ));
    }

    /**
     * Attach a user account to a partner
     *
     * @param int $enc_member_id
     * @return Resposne
     */
    public function updateAffiliateProgramMembership($enc_apm_id = null, $enc_member_id)
    {
        $apm_id = base64_decode($enc_apm_id);
        $affiliateProgramMembership    = AffiliateProgramsMemberships::find($apm_id);

        if (!$affiliateProgramMembership) {
            \App::abort(404);
        }

        $affiliateProgramMembership->membership_number = Input::get('membership_number');
        $affiliateProgramMembership->program_id = Input::get('program_id');
        $affiliateProgramMembership->save();

        return $this->listAffiliateProgramMemberships($enc_member_id);
    }
} // EOC
