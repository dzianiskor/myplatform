<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\RepositoryUser;
use App\Partner as Partner;
use App\User as User;


class BsfMiddlewareController extends Controller
{

    private static $useMockData = false;

    private static $bsfPartnerId = 4206;


    private static $bsfMockUserData = [
        [
            "id" => 60243,
            "blu_id" => 60243,
            "first_name" => 'AlGwaiz',
            "last_name" => 'AlGwaiz',
            "pass_code" => '3344',
            "blu_pass_code" => '2222'
        ],
        [
            "id" => 136632,
            "blu_id" => 136632,
            "first_name" => 'IBRAHIM',
            "last_name" => 'ALHASHMI',
            "pass_code" => '5054',
            "blu_pass_code" => '5054'
        ],
        [
            "id" => 152368,
            "blu_id" => 152368,
            "first_name" => 'Elias',
            "last_name" => 'Sakr',
            "pass_code" => '1111',
            "blu_pass_code" => '1111'
        ],
        [
            "id" => 152460,
            "blu_id" => 152460,
            "first_name" => 'Loyalty',
            "last_name" => 'Dh',
            "pass_code" => '8520',
            "blu_pass_code" => '8520'
        ],
    ];

    /**
     * GET api/bsf-middleware/get-all-users
     * @param RepositoryUser $repositoryUser
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllUsers(RepositoryUser $repositoryUser)
    {
        $userIds = $repositoryUser->allIds(self::$bsfPartnerId)->toArray();
        return response()->json(self::getUsersByIdFromMiddleware($userIds));
    }

    /**
     * GET api/bsf-middleware/get-all-user-ids
     * @param RepositoryUser $repositoryUser
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllUserIds(RepositoryUser $repositoryUser)
    {
        $userIds = $repositoryUser->allIds(self::$bsfPartnerId)->toArray();
        return response()->json([
            "count" => count($userIds),
            "ids"   => $userIds
        ]);
    }

    /**
     * POST api/bsf-middleware/get-users-by-id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse $users
     */
    public function getUsersById(Request $request){
        $userIds = $request->input('user_ids');

        if(!isset($userIds)){
            return response()->json([
                "status" => 400,
                "message" => "No user ID's provided."
            ]);
        }

        $userIdsArray = explode(',', $userIds);

        return response()->json(self::getUsersByIdFromMiddleware($userIdsArray));
    }


    /**
     * GET api/bsf-middleware/get-range-of-users
     * @param Request $request
     * @param RepositoryUser $repositoryUser
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getRangeOfUsers(Request $request, RepositoryUser $repositoryUser)
    {
        $offset = $request->input('offset', 0);
        $limit = $request->input('limit', 1000);

        $users = $repositoryUser->listByPartnerAndRange(self::$bsfPartnerId, $offset, $limit);
        $userIds = $users->pluck('id')->toArray();

        return response()->json(self::getUsersByIdFromMiddleware($userIds));
    }



    /**
     * @param array $userIds
     *
     * @return array
     */
    public function getUsersByIdFromMiddleware($userIds)
    {
        $partner = Partner::find(self::$bsfPartnerId);

        $bsfMiddlewareUrl = $partner->link_middleware_api . '?method=sensitiveInfo&api_key=' . $partner->middleware_api_key;
        $usersQuery = "user_ids=[" . implode(",",$userIds) . "]";

        $curlResponse = self::curlPostRequest($bsfMiddlewareUrl, $usersQuery);

        if($curlResponse['status'] = 200){
            $curlResponse['total_users_from_bsf_db'] = count($curlResponse['response']);
        }

        return $curlResponse;
    }


    /**
     * GET api/bsf-middleware/get-users-with-different-pins
     * @param Request $request
     * @param \App\Repositories\RepositoryUser $repositoryUser
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUsersWithDifferentPinCodes(Request $request, RepositoryUser $repositoryUser)
    {

        $offset = $request->input('offset', 0);
        $limit = $request->input('limit', 100);

        $users = $repositoryUser->listByPartnerAndRange(self::$bsfPartnerId, $offset, $limit);

        $membersWithDifferentPinCodes = self::usersWithDifferentPinCodes($users);
        return response()->json($membersWithDifferentPinCodes);
    }

    /**
     * GET api/bsf-middleware/update-users-with-different-pins
     * @param Request $request
     * @param \App\Repositories\RepositoryUser $repositoryUser
     *
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function updateUsersWithDifferentPinCodes(Request $request, RepositoryUser $repositoryUser)
    {

        $offset = $request->input('offset', 0);
        $limit = $request->input('limit', 100);

        $users = $repositoryUser->listByPartnerAndRange(self::$bsfPartnerId, $offset, $limit);

        $membersToUpdateRequest = self::usersWithDifferentPinCodes($users);

        if($membersToUpdateRequest["status"] !== 200){
            return $membersToUpdateRequest;
        }

        $membersToUpdate = $membersToUpdateRequest["members_with_different_pins"];
        $failedUserUpdates = [];
        $numberUsersUpdated = 0;
        foreach ($membersToUpdate as $member) {
            try  {
                $user = User::find($member['blu_id']);
                $user->passcode = $member['pin_codes']['bsf'];
                $user->save();
                $numberUsersUpdated ++;
            } catch (\Exception $e){
                $failedUserUpdates[] = $member['blu_id'];
            }
        }

        return response()->json([
            "status" => 200,
            "message" => $numberUsersUpdated. " users pin's updated",
            "errors" => [
                "failed_user_updates_by_id" => $failedUserUpdates,
                "failed_to_compare_pins" => $membersToUpdateRequest["errors"]["failed_to_compare_pins"]
            ]
        ]);
    }

    /**
     * @param array $userIds
     * @param $users
     *
     * @return array $users
     */
    public function usersWithDifferentPinCodes($users)
    {

        // Get all BSF members data from BSF middleware DB
        $userIds = $users->pluck('id')->toArray();
        $bsfDbMembersRequest = self::getUsersByIdFromMiddleware($userIds);

        if($bsfDbMembersRequest["status"] !== 200){
            return $bsfDbMembersRequest;
        }

        $bsfDbMembers = $bsfDbMembersRequest["response"];

        // create HashMap of bsfDbMembers so can easily access their values by
        // blu user_id
        $bsfDbMembersHashMap = [];
        foreach ($bsfDbMembers as $member){
            $bsfDbMembersHashMap[$member->blu_id] = $member;
        }

        // now need to compare the pin codes of each of the members between the
        // value on the blu db and the bsf db, and if it is different,
        // we need to update it.
        $membersWithDifferentPinCodes = [];
        $errors = [];

        foreach ($users as $member){
            $bluDbPin = $member->passcode;

            if(isset($bsfDbMembersHashMap[$member->id])){
                $bsfMember = $bsfDbMembersHashMap[$member->id];

                if(!self::$useMockData){
                    $bsfDbPin = base64_decode($bsfMember->pass_code);
                } else {
                    $bsfDbPin = $bsfMember->pass_code;
                }

                if($bluDbPin != $bsfDbPin){
                    $membersWithDifferentPinCodes[] = [
                        'blu_id' => $bsfMember->blu_id,
                        'first_name' => $bsfMember->first_name,
                        'last_name' => $bsfMember->last_name,
                        'pin_codes' => [
                            'blu' => $bluDbPin,
                            'bsf' => $bsfDbPin
                        ],
                    ];
                }
            } else {
                $errors[] = [
                    'blu_id' => $member->id,
                    'error' => 'User with this ID does not exist in BSF db'
                ];
            }

        }

        return [
            "status" => 200,
            "total_members_with_different_pins" => sizeof($membersWithDifferentPinCodes),
            "members_with_different_pins" => $membersWithDifferentPinCodes,
            "errors" => [
                "failed_to_compare_pins" => $errors
            ],
        ];

    }

    /**
     * POST /api/bsf-middleware/upload-change-file
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function postUploadFileChange(Request $request)
    {

        try{
            $fileName = basename($_FILES['file']['name']);
            $fileType = $_FILES['file']['type'];

            // temporary file location on server for processing request
            $tempFileLocation = $_FILES['file']['tmp_name'];

            // Create a CURLFile object to send file via curl.
            $curlFile = curl_file_create($tempFileLocation, $fileType, $fileName);

        } catch (\Exception $e){
            return response()->json([
                "message" => "Please make sure a file has been uploaded, and that it using the key 'file'.",
                "error" => $e
            ]);
        }

        try {

            $postData = [
                "method" => "uploadFile",
                "file" => $curlFile,
            ];

            $targetUrl = $request->input('target_url', 'https://bluuat.alfransi.com.sa/BLU/api.php');
            $apiKey = $request->input('api_key', 'af08ea2bd3afbd900a29abbc8c818c4f');
            $finalTargetUrl = $targetUrl . '?api_key=' . $apiKey;

            $curlResponse = self::curlPostRequest($targetUrl, $postData);

            return response()->json([
                    "final_target_url" => $finalTargetUrl,
                    "target_response" => $curlResponse
                ]
            );

        } catch (\Exception $e){
            return response()->json([
                "message" => "Error while processing request.",
                "error" => $e
            ]);
        }

    }


    /**
     * Simulate receiving a file change request on the middleware.
     * Checks a file had been received
     *
     * POST /api/bsf-middleware/simulation/receive-change-file
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function postReceiveFileChange()
    {

        $fileName = basename($_FILES['file']['name']);
        $fileType = $_FILES['file']['type'];

        return response()->json([
            "method"   => $_POST['method'],
            "fileName" => $fileName,
            "fileType" => $fileType,
            "status"   => 200
        ]);
    }

    /**
     * @param string $targetUrl
     * @param array $postData
     *
     * @return array $response
     */
    public function curlPostRequest($targetUrl, $postData)
    {

        if(self::$useMockData){
            return [
                "status" => 200,
                // turn array into object, since the curl json response would
                // also be an object.
                "response" =>  json_decode(json_encode(self::$bsfMockUserData), FALSE)
            ];
        }

        // Curl request
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $targetUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 300);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, False);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
        $curlResponse = json_decode(curl_exec($ch));

        if (curl_errno($ch)) {
            $response = [
                "status" => 400,
                "curl_errno" => curl_errno($ch)
            ];
        } else {
            $response = [
                "status" => 200,
                "response" => $curlResponse
            ];
        }

        curl_close($ch);

        return $response;
    }


}
