<?php
namespace App\Http\Controllers;
use App\Language as Language;
use App\Cattranslation as Cattranslation;
use View;
use Response;
use Validator;
use Input;

/**
 * Category Translation Controller
 *
 * @category   Controllers
 * @package    BLU
 * @author     Ralph Nader <ralph@bluloyalty.com>
 * @copyright  2015 BLU
 * @link       http://blupoints.com
 */
class CattranslationController extends BaseController
{
    /**
     * Fetch the select drop down data for the cattranslation popup
     *
     * @return array
     */
    private function getSelectData()
    {
        return array(
            'languages'  => Language::orderBy('name')->pluck('name','id')
        );
    }

    /**
     * Create and return a default draft cattranslation
     *
     * @param int $category_id
     * @return Result
     */
    private function getCattranslationDraft($enc_category_id)
    {
        $category_id = base64_decode($enc_category_id);
        $cattranslation = new Cattranslation();

        $cattranslation->name       = 'New Cattranslation';
        $cattranslation->category_id = $category_id;
        $cattranslation->lang_id = 1;
        $cattranslation->save();

        return $cattranslation;
    }

    /**
     * Display the new cattranslation page
     *
     * @return View
     */
    public function newCattranslation($enc_category_id = null)
    {
        $data = array(
            'cattranslation'   => $this->getCattranslationDraft($enc_category_id)
        );

        return View::make("cattranslation.form", array_merge(
            $data, $this->getSelectData()
        ));
    }

    /**
     * Display the edit cattranslation page
     *
     * @return View
     */
    public function editCattranslation($enc_cattranslation_id = null)
    {
        $cattranslation_id = base64_decode($enc_cattranslation_id);
        $data = array(
            'cattranslation'   => Cattranslation::find($cattranslation_id)
        );

        return View::make("cattranslation.form", array_merge(
            $data, $this->getSelectData()
        ));
    }

    /**
     * Create a new cattranslation resource
     *
     * @param int $cattranslation_id
     * @return void
     */
    public function updateCattranslation($enc_cattranslation_id = null)
    {
        $cattranslation_id = base64_decode($enc_cattranslation_id);
        $rules = array(
            'cattranslation_name' => 'required|min:1'
        );
        $input = Input::all();
        foreach($input as $k=>$i){
            $input[$k] = self::sanitizeText($i);
        }
        $validator = Validator::make($input, $rules);

        if($validator->fails()) {
            return Response::json(array(
                'failed' => true,
                'errors' => "Please ensure that you provide a valid cattranslation name"
            ));
        }

        $cattranslation = Cattranslation::find($cattranslation_id);

        // Details
        $cattranslation->name       = self::sanitizeText(Input::get('cattranslation_name'));
        $cattranslation->draft      = false;
        $cattranslation->lang_id        = self::sanitizeText(Input::get('cattranslation_lang'));


        $cattranslation->push();
        $categoryId = base64_encode($cattranslation->category_id);
        return Response::json(array(
            'failed' => false,
            'view_url' => url('dashboard/cattranslation/list/'.$categoryId)
        ));
    }

    /**
     * Delete the specified cattranslation
     *
     * @return View
     */
    public function deleteCattranslation($enc_cattranslation_id = null)
    {
        $cattranslation_id = base64_decode($enc_cattranslation_id);
        Cattranslation::find($cattranslation_id)->delete();

        return Response::json(array(
            'failed' => false
        ));
    }

    /**
     * Render a list of cattranslations relevant to the Partner
     *
     * @param int $category_id
     * @return View
     */
    public function listCattranslations($enc_category_id = null)
    {
        $category_id = base64_decode($enc_category_id);
        $data = array(
            'cattranslations' => Cattranslation::where('category_id', $category_id)->where('draft', false)->get()
        );

        return View::make("cattranslation.list", $data);
    }

} // EOC