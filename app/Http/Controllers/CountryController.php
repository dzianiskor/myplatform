<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\App;
use View;
use Input;
use Validator;
use I;
use Illuminate\Support\Facades\Redirect;
use App\Country as Country;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Country Controller
 * 
 * @category   Controllers
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class CountryController extends BaseController
{
    protected $layout = "layouts.dashboard";

    /**
     * generate a reusable array with all the validations needed
     * for the Country screen
     *
     * @return array
     */
    private function getValidations() {
        $rules = array(
            'name'           => 'required|min:2',
            'telephone_code' => 'required'
        );

        return $rules;
    }

    /**
     * Display a list of the countries
     *
     * @return void
     */
    public function listCountries()
    {
        if(!I::can('view_countries')){
            return Redirect::to(url("dashboard"));
        }
        $this->ormClass      = 'App\Country';
        $this->search_fields = array('name');

        $countries = $this->getList();

        $page = Input::get('page');
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $countries = new LengthAwarePaginator($countries->forPage($page, 15), $countries->count(), 15, $page);

        return View::make("country.list", array(
            'countries' => $countries
        ));

    }

    /**
     * get fields for new Country
     *
     * @return void
     */
    public function newCountry()
    {
        if(!I::can('create_countries')){
            return Redirect::to(url("dashboard"));
        }
        return View::make("country.new");
    }

    /**
     * get fields for new Country
     *
     * @return void
     */
    public function createNewCountry()
    {
        if(!I::can('create_countries')){
            return Redirect::to(url("dashboard"));
        }
        $input = Input::all();
        foreach($input as $k=>$i){
            $input[$k] = self::sanitizeText($i);
        }
        $validator = Validator::make($input, $this->getValidations());

        if($validator->fails()) {
            return Redirect::to('dashboard/countries/new')->withErrors($validator)->withInput();
        }

        Country::create($input);
        return Redirect::to('dashboard/countries');
    }

    /**
     * View a single Country entry
     *
     * @param int $id
     * @return void
     */
    public function viewCountry($enc_id = null)
    {
        $id = base64_decode($enc_id);
        if(!I::can('view_countries')){
            return Redirect::to(url("dashboard"));
        }
        $country = Country::find($id);

        if(!$country) {
            App::abort(404);
        }

        return View::make("country.view", array(
            'country' => $country
        ));
    }

    /**
     * Delete a Country instance
     *
     * @param int $id
     * @return void
     */
    public function deleteCountry($enc_id = null)
    {
        $id = base64_decode($enc_id);
        if(!I::can('delete_countries')){
            return Redirect::to(url("dashboard"));
        }
        $country = Country::find($id);

        if(!$country) {
            App::abort(404);
        }

        $country->delete();

        return Redirect::to('dashboard/countries');
    }

    /**
     * update a Country instance
     *
     * @param int $id
     * @return void
     */
    public function updateCountry($enc_id = null)
    {
        $id = base64_decode($enc_id);
        if(!I::can('edit_countries')){
            return Redirect::to(url("dashboard"));
        }
        return View::make("country.update", array(
            'id' => $id
        ));
    }

    /**
     * update a Country instance
     *
     * @param int $id
     * @return void
     */
    public function saveCountry($enc_id = null)
    {
        $id = base64_decode($enc_id);
        if(!I::can('edit_countries')){
            return Redirect::to(url("dashboard"));
        }
        $country = Country::find($id);

        if(!$country) {
            App::abort(404);
        }
        $input = Input::all();
        unset($input['redirect_to']);
        foreach($input as $k=>$i){
            $input[$k] = self::sanitizeText($i);
        }
        $validator = Validator::make($input, $this->getValidations());

        if($validator->fails()) {
            return Redirect::to('dashboard/countries/view/' . $enc_id)->withErrors($validator)->withInput();
        }

        $country->update($input);

        return Redirect::to(Input::get('redirect_to', 'dashboard/countries'));
    }

} // EOC