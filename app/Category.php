<?php
namespace App;
use App\BluCollection;
/**
 * Category Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class Category extends BaseModel 
{
    public $table      = 'category';
    protected $guarded = array();

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------
    
    public function cattranslations()
    {
        return $this->hasMany('App\Cattranslation')->where('draft', false);
    }
    
    public function parentCategory() 
    { 
    	return $this->belongsTo('App\category'); 
   	}
   	
    public function childCategories() 
    { 
    	return $this->hasMany('App\category');   
    }

    public static function orderedList()
    {
        return Category::orderBy('name')->get();
    }

    public function parent() {
        return $this->belongsToOne('App\Category', 'parent_category_id');
    }

    public function children() {
        return $this->hasMany('App\Category', 'parent_category_id')->orderBy('name', 'asc');
    }

} // EOC