<?php
namespace App;
use App\BluCollection;
use Eloquent;
/**
 * Banner Page Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class BannerPage extends Eloquent 
{
    public $table = 'banner_page';

	public $timestamps = false;

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------
    
    public function banner() 
    { 
    	return $this->belongsTo('App\Banner', 'banner_id');  
   	}

} // EOC