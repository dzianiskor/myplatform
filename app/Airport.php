<?php
namespace App;
use App\BluCollection;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Product Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class Airport extends BaseModel 
{
    protected $table = 'airports';
    protected $fillable = [
        'name',
        'code',
        'city',
        'state',
        'country',
        'lat',
        'lon',
        'woeid',
        'tz',
        'phone',
        'type',
        'email',
        'url',
        'runway_length',
        'elev',
        'icao',
        'direct_flights',
        'carriers'
    ];
} // EOC