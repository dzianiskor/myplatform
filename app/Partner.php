<?php

namespace App;

//use Illuminate\Database\Eloquent\Model;
use App\BaseModel as BaseModel;
/**
 * Partner Model
 *
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class Partner extends BaseModel
{
    public $table      = 'partner';
    protected $guarded = array();

    # --------------------------------------------------------------------------
    #
    # Relationships
    #
    # --------------------------------------------------------------------------

    public function stores()
    {
        return $this->hasMany('App\Store')->where('draft', false);
    }

    public function transactions()
    {
        return $this->hasMany('App\Transaction')->orderBy('created_at', 'desc');
    }

    public function networks()
    {
        return $this->belongsToMany('App\Network')->distinct();
    }

    public function firstNetwork()
    {
        return $this->belongsToMany('App\Network')->first();
    }

    public function loyaltyPrograms()
    {
        return $this->hasMany('App\Loyalty')->where('draft', false);
    }

    public function products()
    {
        return $this->belongsToMany('App\Product');
    }

    public function removedProducts()
    {
        return $this->belongsToMany('App\Product')->onlyTrashed();
    }
    
    public function offers()
    {
        return $this->belongsToMany('App\Offer', 'partner_offer');
    }

    public function removedOffers()
    {
        return $this->belongsToMany('App\Offer')->onlyTrashed();
    }

    public function admins()
    {
        return $this->belongsToMany('App\User');
    }

    public function notifications()
    {
        return $this->hasMany('App\Notification');
    }

    public function segments()
    {
        return $this->hasMany('App\Segment')->where('draft', false);
    }

    public function batches()
    {
        return $this->hasMany('App\Batch')->where('type', '!=', 'Coupons');
    }

    public function coupons()
    {
        return $this->hasMany('App\Batch')->where('type', 'Coupons');
    }

    public function members()
    {
        return $this->belongsToMany('App\User');
    }

    public function removedMembers()
    {
        return $this->belongsToMany('App\User')->onlyTrashed();
    }
    public function mobbanners()
    {
        return $this->hasMany('App\Banner')->where('draft', false)->where('mob_status', 'Online')->orderBy('id','DESC');
    }

    public function parent()
    {
        return $this->belongsTo('App\Partner', 'parent_id');
    }

    public function roles()
    {
        return $this->hasMany('App\Role');
    }

    public function category()
    {
        return $this->belongsTo('App\PartnerCategory', 'category_id');
    }

    public function country()
    {
        return $this->belongsTo('App\Country');
    }

    public function children()
    {
        return $this->hasMany('App\Partner', 'parent_id');
    }

    public function banners()
    {
        return $this->hasMany('App\Banner')->where('draft', false)->where('status', 'Online')->orderBy('id','DESC');
    }
	
    public function allbanners()
    {
        return $this->hasMany('App\Banner')->where('draft', false)->orderBy('id','DESC');
    }

    public function allcardprint()
    {
        return $this->hasMany('App\CardPrint')->where('draft', false)->orderBy('id','DESC');
    }
    public function channels()
    {
        return $this->hasMany('App\PartnerChannel', 'partner_id')->distinct();
    }
    
    public function agreports()
    {
        return $this->hasMany('App\AutoGeneratedReportPartner', 'partner_id')->distinct();
    }

    public function partnerlinked()
    {
        return $this->hasMany('App\PartnerLinked', 'partner_id')->distinct();
    }

    public function tickets()
    {
        return $this->hasMany('App\Ticket', 'partner_id');
    }
    public function suppliers()
    {
        return $this->belongsToMany('App\Supplier');
    }
	public function countries()
    {
        return $this->hasMany('App\PartnerCountry', 'partner_id')->distinct();
    }
    public function estatements()
    {
        return $this->hasMany('App\Estatement', 'partner_id');
    }
    public function estatementhistory()
    {
        return $this->hasMany('App\EstatementHistory', 'partner_id');
    }
    public function affiliates()
    {
        return $this->belongsToMany('App\Affiliate');
    }
    public function affiliateprograms()
    {
        return $this->belongsToMany('App\AffiliateProgram');
    }
    public function emailwhitelabelings()
    {
        return $this->hasMany('App\EmailWhitelabeling', 'partner_id');
    }
    public function childs() {
        return $this->hasMany('App\Partner', 'parent_id', 'id');
    }

# --------------------------------------------------------------------------
    #
    # Attributes
    #
    # --------------------------------------------------------------------------

    /**
     * Fetch all media in the partner hierarchy
     *
     * @return array
     */
    public function uploads()
    {
        return Media::whereIn('id', $this->childHierarchy());
    }

    # --------------------------------------------------------------------------
    #
    # Helpers
    #
    # --------------------------------------------------------------------------

    /**
     * Fetch a list of all hierarchical child partners of the current partner
     *
     * @return array
     */
    public function childHierarchy()
    {
        $nodes = $this->clildNewHierarchy();

        return $nodes;
    }

    public function clildNewHierarchy()
    {
        $nodes = $tree = [];
        $partners = Partner::select('id', 'parent_id')->where('draft', false)->get()->toArray();
        $nodes = $tree = $this->buildTree($partners, $this->id);

        while (!empty($tree)) {
            $results = $this->buildTree($partners, array_shift($tree));
            foreach($results as $item) {
                if (!in_array($item, $nodes)) {
                    $nodes[] = $item;
                }
                if (!in_array($item, $tree)) {
                    $tree[]  = $item;
                }
            }

        };

        array_unshift($nodes, $this->id);

        return $nodes;
    }

    public function buildTree($elements, $parentId)
    {
        $childIds = [];

        foreach ($elements as $element) {
            if ($element['parent_id'] == $parentId) {
                $childIds[] = $element['id'];
            }
        }

        return $childIds;
    }

    /**
     * Return the partner objects of of the parent hierarchy structure
     *
     * @return array
     */
    public function managedObjects()
    {
        $partners = self::whereIn('id', $this->childHierarchy());
        $partners = $partners->get();

        return $partners;
    }

    /**
     * Fetches the top level parent
     *
     * @return array
     */
    public function topLevelPartner()
    {
        if ($this->parent) {
            $parent = $this->parent;

            while ($parent->parent) {
                if($parent->is_top_level_partner){
                    return $parent;
                }
                elseif($parent == $this->parent){
                  $parent = $this->parent;  
                  break;
                }else{
                   $parent = $this->parent;   
                }
                
            }

            return $parent;
        } else {
            return $this;
        }
    }


    public function productsAndChildProducts() {
        $products = new BluCollection();
        $childPartners = $this->managedObjects();
        foreach ($childPartners as $childPartner) {
            $childProducts = $childPartner->products;
            foreach ($childProducts as $childProduct) {
                $products->add($childProduct);
            }
        }
        return $products;
    }

} // EOC