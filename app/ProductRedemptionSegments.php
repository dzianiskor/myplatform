<?php
namespace App;
use App\BluCollection;
use Eloquent;
/**
 * Product Channel Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class ProductRedemptionSegments extends Eloquent 
{
    public $table = 'product_redemption_segments';
    public $timestamps = false;

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------

    public function segment() 
    { 
        return $this->belongsTo('App\Segment', 'segment_id'); 
    }        

} // EOC