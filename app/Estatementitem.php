<?php
namespace App;
use App\BluCollection;
use Eloquent;
/**
 * Store Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Magid Mroueh <magid.mroueh@bluloyalty.com>
 * @copyright  2016 BLU
 * @link       http://blupoints.com
 */
class Estatementitem extends Eloquent 
{   
    public $timestamps = false;
    public $table = 'estatement_items';

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------
    
    public function estatement()
    {
        return $this->belongsTo('App\Estatement');
    }

    

    # --------------------------------------------------------------------------
    # 
    # Helpers
    # 
    # --------------------------------------------------------------------------
    
    /**
     * Return a list of stores for the specified partner
     * 
     * @param int $estatement_id
     * @return array
     */
    public static function estatementItems($estatement_id)     
    { 
        return Estatementitem::where('estatement_id', $estatement_id)->orderBy('id')->get();
    }

} // EOC