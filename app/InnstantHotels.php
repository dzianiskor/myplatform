<?php
namespace App;
use App\BluCollection;
/**
 * Banner Model
 *
 * @category   Models
 * @package    BLU
 * @author     Magid Mroueh <magid.mroueh@bluloyalty.com>
 * @copyright  2015 BLU
 * @link       http://blupoints.com
 */
class InnstantHotels extends BaseModel
{
	public $table = 'innstant_hotels';

    public function cities()
    {
        return $this->belongsTo('App\InnstantCities');
    }

    public function description()
    {
        return $this->has('InnstantDescriptions', 'hotel_id');
    }

    public function facilities()
    {
        return $this->hasMany('App\InnstantHotelFacilities', 'hotel_id');
    }

    public function Images()
    {
        return $this->hasMany('App\InnstantImages', 'hotel_id');
    }

} // EOC