<?php
namespace App;
use App;
use Illuminate\Container\Container;
use Illuminate\Pagination\Paginator;
use \Illuminate\Pagination\LengthAwarePaginator;

/**
 * BluCollection Model
 *
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class BluCollection extends \Illuminate\Database\Eloquent\Collection
{
    public function __construct($collection = array())
    {
        if (is_a($collection, 'array')) {
            parent::__construct($collection);
        } else {
            parent::__construct(array());
        }
        foreach ($collection as $item) {
            $this->add($item);
        }
    }

    /**
     * FieldEqualsValue Scope Query
     *
     * @param string $field
     * @param string $value
     * @return Object
     */
    public function FieldEqualsValue($field, $value)
    {
        return $this->filter(function($element) use ($field, $value) {
           return $element->$field == $value;
        });
    }

    /**
     * FieldLikeValue Scope Query
     *
     * @param string $field
     * @param string $value
     * @return Object
     */
    public function FieldLikeValue($field, $value)
    {
        return $this->filter(function($element) use ($field, $value) {

            $haystack = $element->$field;
            $toRet    = stripos($haystack, $value);
            $toRet    = $toRet !== false;

            return $toRet;
        });
    }

    /**
     * FieldNotEqualsValue Scope Query
     *
     * @param string $field
     * @param string $value
     * @return Object
     */
    public function FieldNotEqualsValue($field, $value)
    {
        return $this->filter(function($element) use ($field, $value) {
           return $element->$field != $value;
        });
    }

    /**
     * FieldGreaterOrEqualsValue Scope Query
     *
     * @param string $field
     * @param string $value
     * @return Object
     */
    public function FieldGreaterOrEqualsValue($field, $value)
    {
        return $this->filter(function($element) use ($field, $value) {
           return $element->$field >= $value;
        });
    }

    /**
     * FieldLessOrEqualsValue Scope Query
     *
     * @param string $field
     * @param string $value
     * @return Object
     */
    public function FieldLessOrEqualsValue($field, $value)
    {
        return $this->filter(function($element) use ($field, $value) {
           return $element->$field <= $value;
        });
    }

    /**
   	 * Get a paginator for a collection.
   	 *
     * @param int  $perPage
     *
     * @return \Illuminate\Pagination\AbstractPaginator
   	 */
    public function paginate($perPage)
    {
        $page = Paginator::resolveCurrentPage() ?: 1;

        return Container::getInstance()->makeWith(LengthAwarePaginator::class, [
            'items' => $this->forPage($page, $perPage),
            'total' => $this->count(),
            'perPage' => $perPage,
            'currentPage' => $page
        ]);
    }

    /**
     * @deprecated, use paginate() method
     */
    public function paginateCollection($items, $perPage = 15, $page = null, $options = [])
    {
	    $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
	    $items = $items instanceof BluCollection ? $items : BluCollection::make($items);
	    return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }

    /**
     * OrderBy Constraint
     *
     * @param string $attribute
     * @param string $order
     * @return Object
     */
    public function orderBy($attribute, $order = 'asc')
    {
        $this->sortBy(function($model) use ($attribute) {
            return $model->{$attribute};
        });

        if($order == 'desc') {
            $this->items = array_reverse($this->items);
        }

        return $this;
    }

} // EOC