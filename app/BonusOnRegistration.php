<?php
namespace App;
use App\BluCollection;
/**
 * Report Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class BonusOnRegistration extends BaseModel 
{
	public $table = 'bonus_on_registration';
	
} // EOC