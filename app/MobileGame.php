<?php
namespace App;
use App\BaseModel as BaseModel;
use App\BluCollection;
/**
 * Mobile Game Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class MobileGame extends BaseModel 
{
    public $table = 'mobile_game_preferences';   
    
    public function image()
    {
        $image = $this->belongsTo('App\Media', 'media_id');
        $image = $image->first();

        return $image;
    }
    public function countries()   
    {        
        return $this->belongsToMany('App\Country'); 
    }
    public function partners()    
    {        
        return $this->belongsToMany('App\Partner'); 
    }

} // EOC