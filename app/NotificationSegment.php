<?php
namespace App;
use App\BluCollection;
use Eloquent;
/**
 * Notification Segment Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class NotificationSegment extends Eloquent 
{
    public $table = 'notification_segment';
 
} // EOC