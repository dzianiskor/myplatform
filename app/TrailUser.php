<?php
namespace App;
use App\BluCollection;
use Eloquent;
/**
 * Audit Trail Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class TrailUser extends Eloquent 
{
    public $table = 'audit_trail_users';

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------
    
    public function partner()
    {
        return $this->belongsTo('App\Partner');
    }   
    public function user()
    {
        return $this->belongsTo('App\User');
    }

} // EOC