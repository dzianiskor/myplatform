<?php
namespace App;
use App\BluCollection;
use Eloquent;
/**
 * BannerSegment model
 * 
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class BannerSegment extends Eloquent 
{
    public $table = 'banner_segment';

	public $timestamps = false;

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------
    
    public function banner() 
    { 
    	return $this->belongsTo('App\Banner', 'banner_id');  
   	}
   	
    public function segment() 
    { 
    	return $this->belongsTo('App\Segment', 'segment_id'); 
   	}    

} // EOC