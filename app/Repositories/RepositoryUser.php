<?php

namespace App\Repositories;

use App\User;
use Illuminate\Database\Eloquent\Builder;
use InputHelper;

class RepositoryUser
{

    public function findByMobileAndPinCode($mobile, $pinCode, $partnerId, $draft = 0)
    {
        return $this->queryFindByMobile($mobile, $partnerId, $draft)
                    ->where('passcode', $pinCode)
                    ->first();
    }

    public function findByMobile($mobile, $partnerId, $draft = 0)
    {
        return $this->queryFindByMobile($mobile, $partnerId, $draft)
                    ->first();
    }

    public function findByNormalizedMobileNumber($normalizedNumber, $partnerId)
    {
        return $this->queryLimitByPartner($partnerId)
                    ->where('normalized_mobile', $normalizedNumber)
                    ->first();
    }

    /**
     * @param string $mobile
     * @param int $partnerId
     * @param int $draft
     *
     * @return Builder
     */
    protected function queryFindByMobile($mobile, $partnerId, $draft = 0)
    {
        $query = $this->queryLimitByPartner($partnerId, $draft);

        if ('+' == $mobile[0])
            $query->where('normalized_mobile', $mobile);
        else
            $query->where(function($query) use ($mobile){
                $query->where('mobile', InputHelper::sanitizeMobile($mobile))
                      ->orWhere('mobile', $mobile);
            });

        return $query;
    }

    /**
     * @param int|array $partnerId
     * @param int $draft
     *
     * @return Builder
     */
    protected function queryLimitByPartner($partnerId, $draft = 0)
    {
        return
            $query = User::select(['users.*'])
                         ->where('draft', $draft)
                         ->where('verified', 1) //not sure exactly about this condition
                         ->whereIn('users.id', function($query) use ($partnerId){
                    $query->from('partner_user')
                          ->select(['user_id'])
                          ->whereIn('partner_id', is_array($partnerId) ? $partnerId : [$partnerId]);
                });
    }

    /**
     * @param string $searchPhrase
     * @param array $partnerIds
     *
     * @return array
     */
    public function listBySearchPhrase($searchPhrase, $partnerIds, $limit = null)
    {
        return $searchPhrase ?
            $this->queryLimitByPartner($partnerIds)
                 ->where(function (Builder $query) use ($searchPhrase, $partnerIds) {
                     $query->orWhere('username', 'like', '%' . $searchPhrase . '%')
                           ->orWhere('first_name', 'like', '%' . $searchPhrase . '%')
                           ->orWhere('middle_name', 'like', '%' . $searchPhrase . '%')
                           ->orWhere('last_name', 'like', '%' . $searchPhrase . '%')
                           ->orWhere('email', 'like', '%' . $searchPhrase . '%')
                           ->orWhere('mobile', 'like', '%' . $searchPhrase . '%')
                           ->orWhere('normalized_mobile', 'like', '%' . $searchPhrase . '%')
                           ->orWhereHas('references',
                               function ($query) use ($searchPhrase, $partnerIds) {
                                   return $query->where('reference.number', 'like', '%' . $searchPhrase . '%')
                                                ->whereIn('partner_id', $partnerIds);
                               });
                 })->limit($limit)->get()

            : [];
    }

    /**
     * @param string $searchPhrase
     * @param array $partnerIds
     *
     * @return array
     */
    public function listSearchByReference($searchPhrase, $partnerIds, $limit = null)
    {
        return $searchPhrase ?
            $this->queryLimitByPartner($partnerIds)
                 ->where(function (Builder $query) use ($searchPhrase, $partnerIds) {
                     $query->orWhereHas('references',
                         function ($query) use ($searchPhrase, $partnerIds) {
                             return $query->where('reference.number', 'like', '%' . $searchPhrase . '%')
                                          ->whereIn('partner_id', $partnerIds);
                         });
                 })->limit($limit)->get()

            : [];
    }

    /**
     * @param string $searchPhrase
     * @param array $partnerIds
     *
     * @return array
     */
    public function listSearchByUserData($searchPhrase, $partnerIds, $limit = null)
    {
        return $searchPhrase ?
            $this->queryLimitByPartner($partnerIds)
                 ->where(function (Builder $query) use ($searchPhrase, $partnerIds) {
                     $query->orWhere('username', 'like', '%' . $searchPhrase . '%')
                           ->orWhere('first_name', 'like', '%' . $searchPhrase . '%')
                           ->orWhere('middle_name', 'like', '%' . $searchPhrase . '%')
                           ->orWhere('last_name', 'like', '%' . $searchPhrase . '%')
                           ->orWhere('email', 'like', '%' . $searchPhrase . '%')
                           ->orWhere('mobile', 'like', '%' . $searchPhrase . '%')
                           ->orWhere('normalized_mobile', 'like', '%' . $searchPhrase . '%');
                 })->limit($limit)->get()

            : [];
    }

    /**
     * @param array $partnerIds
     *
     * @param int $offset
     * @param int $limit
     * @return array
     */
    public function listByPartnerAndRange($partnerIds, $offset=0, $limit = 100)
    {
        return $this->queryLimitByPartner($partnerIds)
            ->offset($offset)
            ->limit($limit)
            ->get();
    }

    /**
     * @param array $partnerIds
     *
     * @return array
     */
    public function allIds($partnerIds)
    {
        return $partnerIds ?
            $this->queryLimitByPartner($partnerIds)->pluck('id')
            : [];
    }
}


