<?php

namespace App\Repositories\Traits;

trait WithMemberFilter
{
    protected $memberId;

    public function memberId($memberId)
    {
        $this->memberId = $memberId;

        return $this;
    }
}