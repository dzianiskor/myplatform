<?php

namespace App\Repositories\Traits;

trait WithPartnerFilter
{
    protected $partnerIds;

    public function partnerId($partnerIds)
    {
        $this->partnerIds = $partnerIds;

        return $this;
    }
}