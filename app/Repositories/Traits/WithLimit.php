<?php

namespace App\Repositories\Traits;

trait WithLimit
{
    protected $limit;
    protected $offset;

    public function limit($limit)
    {
        $this->limit = (int) $limit;

        return $this;
    }

    public function offset($offset)
    {
        $this->offset = (int) $offset;

        return $this;
    }
}