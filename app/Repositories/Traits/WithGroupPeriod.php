<?php

namespace App\Repositories\Traits;

trait WithGroupPeriod
{
    protected $groupPeriod;

    public function groupPeriod($groupPeriod)
    {
        $this->groupPeriod = $groupPeriod;

        return $this;
    }
}