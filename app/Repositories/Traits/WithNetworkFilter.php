<?php

namespace App\Repositories\Traits;

trait WithNetworkFilter
{
    protected $networkId;

    public function networkId($networkId)
    {
        $this->networkId = $networkId;

        return $this;
    }
}