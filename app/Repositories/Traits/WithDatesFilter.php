<?php

namespace App\Repositories\Traits;

use Illuminate\Support\Carbon;

trait WithDatesFilter
{
    protected $startDate, $endDate;

    public function startDate($startDate)
    {
        $this->startDate = Carbon::parse($startDate);

        return $this;
    }

    public function endDate($endDate)
    {
        $this->endDate = Carbon::parse($endDate);

        return $this;
    }
}