<?php

namespace App\Repositories\Report;

use App\Repositories\Traits\WithDatesFilter;
use App\Repositories\Traits\WithGroupPeriod;
use App\Repositories\Traits\WithLimit;
use App\Repositories\Traits\WithNetworkFilter;
use App\Repositories\Traits\WithPartnerFilter;
use App\Transaction;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;

class RepositoryReport26
{
    protected $query;

    use WithDatesFilter, WithNetworkFilter, WithGroupPeriod, WithPartnerFilter;

    public function getData()
    {
        return $this->queryBase()
                    ->get();
    }

    /**
     * @return Builder
     */
    protected function queryBase()
    {
        if ($this->query) return $this->query;

        $this->query = DB::table('transaction_deduction as td')
                         ->join('partner as p', 'td.partner_id', '=', 'p.id')
                         ->selectRaw('sum(td.points_used) ptsused, p.name as partner_name')
                         ->whereBetween('td.created_at', [$this->startDate, $this->endDate])
                         ->whereIn('td.partner_id', $this->partnerIds)
                         ->orderBy('creation', 'DESC')
                         ->groupBy('p.name');

        if ('yearly' == $this->groupPeriod || 'monthly' == $this->groupPeriod)
        {
            $this->query->selectRaw('YEAR(td.created_at) creation')
                        ->groupBy(DB::raw('YEAR(td.created_at)'));

            if ('monthly' == $this->groupPeriod)
            {
                $this->query->selectRaw('MONTH(td.created_at) crmonth')
                            ->groupBy(DB::raw('MONTH(td.created_at)'))
                            ->orderBy('crmonth', 'DESC');
            }
        } else {
            $this->query->selectRaw('DATE(td.created_at) creation')
                        ->groupBy(DB::raw('DATE(td.created_at)'));
        }

        return $this->query;
    }

    public function total()
    {
        return $this->queryBase()->getCountForPagination();
    }
}