<?php

namespace App\Repositories\Report;

use App\Repositories\Traits\WithDatesFilter;
use App\Repositories\Traits\WithGroupPeriod;
use App\Repositories\Traits\WithLimit;
use App\Repositories\Traits\WithNetworkFilter;
use App\Repositories\Traits\WithPartnerFilter;
use App\Transaction;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;

class RepositoryReport25
{
    protected $query;

    use WithLimit, WithDatesFilter, WithNetworkFilter, WithGroupPeriod, WithPartnerFilter;

    public function getData()
    {
        return $this->queryBase()
            ->when($this->limit, function(Builder $query){
                $query->offset($this->offset)
                    ->limit($this->limit);
            })
            ->orderBy('partner_name')
            ->orderBy('network_name')
            ->get();
    }

    /**
     * @return Builder
     */
    protected function queryBase()
    {
        if ($this->query) return $this->query;

        /** @var Builder $query */
        //this is very fast query used index presorting for grouping, in the future need to make calculated STORED created_date (sql type DATE) column = DATE(cheated_at) and use it as first column in Covered index
        $query = Transaction::selectRaw('DATE(created_at) as created_date, partner_id, network_id, 
        SUM(points_rewarded) as preward, SUM(points_redeemed) as predeem, SUM(trx_reward) as trx_rew, SUM(trx_redeem) as trx_red')
            ->whereBetween('created_at', [$this->startDate, $this->endDate])
            ->whereIn('partner_id', $this->partnerIds)
            ->groupBy('created_at', 'network_id', 'partner_id')
            ->getQuery();

        if (!empty($defaults['network_ids'])) {
            $query->whereIn('network_id', $this->networkId);
        }

        //query in the middle we need for performance optimisation because, this query wrap very fast grouping by query.
        $query = DB::query()->fromSub($query, 'pre_group')
            ->selectRaw('partner_id, network_id, SUM(preward) as preward, SUM(predeem) as predeem, SUM(trx_rew) as trx_rew, SUM(trx_red) as trx_red');

        if ('yearly' == $this->groupPeriod || 'monthly' == $this->groupPeriod)
        {
            $query->selectRaw('YEAR(created_date) as creation')
                ->groupBy('creation');

            if ('monthly' == $this->groupPeriod)
                $query->selectRaw('MONTH(created_date) as crmonth')
                    ->groupBy('crmonth');
        }else
        {
            $query->selectRaw('created_date as creation')
                ->groupBy('creation');
        }
        //don't change the order of grouping field, this take effect on performance
        $query->groupBy('network_id', 'partner_id');

        //join partner's and network's names
        $query = DB::query()->fromSub($query, 'tr')
            ->select(['partner.name as partner_name', 'network.name as network_name', 'preward', 'predeem', 'trx_rew', 'trx_red'])
            ->join('partner', 'partner_id', '=', 'partner.id')
            ->join('network', 'network_id', '=', 'network.id');

        if ('yearly' == $this->groupPeriod)
        {
            $query->addSelect('creation')
                ->orderBy('creation', 'DESC');
        }elseif('monthly' == $this->groupPeriod)
        {
            $query->addSelect(['creation', 'crmonth'])
                ->orderBy('creation', 'DESC')
                ->orderBy('crmonth', 'DESC');
        }else
        {
            $query->addSelect('creation')
                ->orderBy('creation', 'DESC');
        }

        return $this->query = $query;
    }

    public function total()
    {
        return $this->queryBase()->getCountForPagination();
    }
}