<?php

namespace App\Repositories\Report;

use App\Repositories\Traits\WithDatesFilter;
use App\Repositories\Traits\WithLimit;
use App\Repositories\Traits\WithMemberFilter;
use App\Repositories\Traits\WithNetworkFilter;
use App\Repositories\Traits\WithPartnerFilter;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;

class RepositoryReport40
{
    use WithLimit, WithDatesFilter, WithPartnerFilter, WithNetworkFilter, WithMemberFilter;

    public function getData()
    {
        //join tables to sub query result
        //return DB::query()->fromSub($subQuery->offset($offset)->limit($limit), 't') Laravel 5.6
        return DB::query()->from(DB::raw('(' . $this->querySub() . ' ORDER BY t2.id DESC, ti2.id DESC' . ($this->limit ?   ' limit ' . $this->limit . ' offset ' . $this->offset : '') . ') t '))
                 ->join('transaction as t1', 't1.id', '=', 't.id')
                 ->join('users as u1', 't1.user_id', '=', 'u1.id')
                 ->join('partner as p1', 't1.partner_id', '=', 'p1.id')
                 ->leftJoin('transaction_item as ti1', 't.transaction_item_id', '=', 'ti1.id')
                 ->orderBy('t1.id', 'desc')
                 ->orderBy('ti1.id', 'desc')
                 ->select([
                     't1.id as trx_id',
                     't1.user_id as user_id',
                     't1.created_at as date',
                     't1.trans_class as category',
                     'ti1.product_id as item',
                     'ti1.quantity as quantity',
                     'ti1.price_in_points as points_redeemed',
                     'ti1.paid_in_points as paid_in_points',
                     'ti1.paid_in_cash as paid_in_cash',
                     //'ti1.delivery_cost as delivery_cost',
                     'ti1.total_amount as amount_spent',
                     't1.points_redeemed as trx_pts_redeemed',
                     't1.total_amount as trx_total_amount',
                     't1.quantity as trx_quantity',
                     't1.notes as notes',
                     't1.ref_number as ref_number',
                     't1.reversed as reversed',
                     't1.trx_reward as trx_reward',
                     't1.network_id as network_id',
                     'ti1.currency_id as ti_currency_id',
                     'ti1.original_total_amount as ti_original_total_amount',
                     't1.currency_id as t_currency_id',
                     't1.original_total_amount as t_original_total_amount',
                     'u1.first_name',
                     'u1.last_name',
                     'u1.normalized_mobile',
                     't1.delivery_cost as delivery_cost',
                     't1.cash_payment as cash_payment',
                 ])
                 ->selectRaw('(SELECT number from reference as r where r.user_id = u1.id and r.partner_id = t1.partner_id order by r.id limit 1) as refID')
                 ->selectRaw('(SELECT number from reference as r where r.user_id = u1.id and r.partner_id = p1.parent_id order by r.id limit 1) as parentRefID')
                 ->get();
    }

    /**
     * @return Builder
     */
    protected function queryBase()
    {
        return DB::table('transaction as t2')
                 ->whereBetween(DB::raw('t2.created_at'), [$this->startDate, $this->endDate->addDay()])
                 ->when($this->networkId, function (Builder $query) {
                     $query->where('t2.network_id', $this->networkId);
                 })
                 ->when($this->partnerIds, function (Builder $query) {
                     $query->whereIn('t2.partner_id', $this->partnerIds);
                 })
                 ->when($this->memberId, function (Builder $query) {
                     $query->where('t2.user_id', $this->memberId);
                 })
                 ->where(function (Builder $query) {
                     $query->where('trx_redeem', 1)
                           ->orWhere(function (Builder $query) {
                               $query->where('trx_reward', 1)
                                     ->where('reversed', 1);
                           });
                 });
    }

    protected function querySub()
    {
        /*
         *  don't delete, this code compatible with Laravel version 5.6
         * /*/
        //build sub query optimized for ordering
        /*$subQuery = DB::table('transaction as t2')->select(['t2.id', 'ti2.id as transaction_item_id'])
            ->join('users as u2', 't2.user_id', '=', 'u2.id')
            ->leftJoin('transaction_item as ti2', 't2.id', '=', 'ti2.transaction_id')
            ->whereBetween('t2.created_at', [$defaults ['start_date'], $defaults['end_date']])
            ->where('t2.network_id', $networkId)
            ->orderBy('t2.id', 'desc')
            ->orderBy('ti2.id', 'desc');

        if ($this->>memberId)
        {
            $subQuery->where('user_id', $this->memberId);
        } else
        {
            $subQuery->whereIn('partner_id', $defaults['entities']);
        }

        $subQuery->where(function ($subQuery){
            $subQuery->where('t2.trx_redeem', 1)
                ->orWhere(function ($subQuery) {
                    $subQuery->where('t2.trx_reward', 1)
                        ->where('t2.reversed', 1);
                });
        });

        //join tables to sub query result
        $query = DB::query()->fromSub($subQuery->offset($offset)->limit($limit), 't')
            ->join('transaction as t1', 't1.id', '=', 't.id')
            ->join('users as u1', 't1.user_id', '=', 'u1.id')
            ->leftJoin('transaction_item as ti1', 't.transaction_item_id', '=', 'ti1.id')
            ->orderBy('t1.id', 'desc')
            ->orderBy('ti1.id', 'desc');*/

        return 'SELECT t2.id, ti2.id AS transaction_item_id FROM transaction AS t2 JOIN users AS u2 ON t2.user_id = u2.id LEFT JOIN transaction_item AS ti2 ON t2.id = ti2.transaction_id
        WHERE t2.created_at BETWEEN \'' . $this->startDate->format('Ymd') . '\' AND \'' . $this->endDate->addDay()->format('Ymd') . '\'
        AND t2.network_id = ' . (int) $this->networkId .
            ($this->memberId ? ' AND user_id = ' . $this->memberId : '') .
            ($this->partnerIds ? ' AND  partner_id in (' . implode(',', $this->partnerIds) . ')' : '') .
            ' AND (t2.trx_redeem = 1 OR (t2.trx_reward = 1 AND t2.reversed = 1))';
    }

    public function getTotals()
    {
        return $this->queryBase()->selectRaw('
            SUM(points_redeemed) as total_points_redeemed,
            SUM(cash_payment) as total_cash_payment,
            SUM(delivery_cost) as total_delivery_cost,
            SUM(amt_redeem) as total_amt_redeem
        ')->first();
    }

    public function total()
    {
        //take count from sub query
        //$totals = $subQuery->getCountForPagination(); Laravel 5.6
        return DB::query()->from(DB::raw('(' . $this->querySub() . ') t'))->count();
    }
}