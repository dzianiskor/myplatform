<?php
namespace App;
use App\BluCollection;
/**
 * CardPrint Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Ralph Nader <ralph@bluloyalty.com>
 * @copyright  2016 BLU
 * @link       http://blupoints.com
 */
class CardPrint extends BaseModel
{
    public $table = 'cardprint';
	
    public function partner()
    {
        return $this->belongsTo('App\Partner');
    }
    
    public function image()
    {
        $image = $this->belongsTo('App\Media', 'media_id');
        $image = $image->first();

        return $image;
    }
    
    

} // EOC