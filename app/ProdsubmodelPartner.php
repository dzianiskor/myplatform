<?php
namespace App;
use App\BluCollection;
use Eloquent;
/**
 * Prodsubmodel Partner Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Ralph Nader <ralph@bluloyalty.com>
 * @copyright  2016 BLU
 * @link       http://blupoints.com
 */
class ProdsubmodelPartner extends Eloquent 
{
    public $table = 'prodsubmodel_partner';

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------

    public function partner() 
    { 
        return $this->belongsTo('App\Partner'); 
    }        

} // EOC