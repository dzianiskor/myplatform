<?php
namespace App;
use App\BluCollection;
use Eloquent;
/**
 * Cart Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class Cart extends Eloquent 
{
    public $table = 'cart';

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------
    
    public function user() 
    { 
    	return $this->belongsTo('App\User');  
   	}
   	
    public function product() 
    { 
    	return $this->belongsTo('App\Product'); 
   	}    

} // EOC