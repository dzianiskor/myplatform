<?php
namespace App;
use App\BluCollection;
/**
 * Area Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Magid Mroueh <magid.mroueh@wixelhq.com>
 * @copyright  2017 BLU
 * @link       http://blupoints.com
 */
class Affiliate extends BaseModel 
{
    public $table      = 'affiliate';

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------
    
    public function Partners() 
    { 
    	return $this->belongsToMany('App\Partner');
    }


} // EOC