<?php

namespace App;

use DateTime;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;
use UCID;
use PinCode;
use Illuminate\Support\Facades\DB;
use View;
use SMSHelper;

// Don't Remove
if (!defined('BLU')) {
    define("BLU", 1);
}

/**
 * User Model
 *
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class User extends BaseModel {

    //Frontend thumbnail sizes
    const TN_FE_PROFILE_IMAGE_WIDTH = 46;
    const TN_FE_PROFILE_IMAGE_HEIGHT = 46;

    protected $table = 'users';
    protected $hidden = array('password');
    protected $guarded = array('password', 'ucid', 'passcode');
    protected $dates = ['deleted_at'];
    protected $softDelete = true;

    # --------------------------------------------------------------------------
    #
    # Relationships
    #
    # --------------------------------------------------------------------------
    // Return roles
    public function roles() {
        return $this->belongsToMany('App\Role', 'role_user', 'user_id', 'role_id');
    }

    // Return associated cards
    public function invoices() {
        return $this->hasMany('App\UserInvoice', 'user_id');
    }

    // Return associated cards
    public function cards() {
        return $this->hasMany('App\Card', 'user_id');
    }

    // Return associated social media accoutns
    public function socialMedia() {
        return $this->hasMany('UserSocialMedia');
    }

    // Return associated balances
    public function userbalances() {
        return $this->hasMany('App\Balance', 'user_id');
    }

    // Return partners entities
    public function partners() {
        return $this->belongsToMany('App\Partner', 'partner_user')->distinct();
    }

    // Return first partner entity
    public function firstPartner() {
        return $this->belongsToMany('App\Partner', 'partner_user', 'user_id', 'partner_id')->first();
    }

    //Return profile image media
    public function profileImageMedia() {
        return $this->belongsTo(Media::class, 'profile_image');
    }

    // Return segments
    public function segments() {
        return $this->belongsToMany('App\Segment', 'segment_user', 'user_id', 'segment_id');
    }

    // Return tiercriterion
    public function tiercriteria() {
        return $this->belongsToMany('App\Tiercriteria', 'tiercriteria_user', 'user_id', 'tiercriteria_id');
    }

    // Return address
    public function address() {
        return $this->hasOne('App\Address', 'mapping_key');
    }

    // Return address
    public function deviceuser() {
        return $this->hasMany('DeviceUser', 'user_id');
    }

    // Return transactions
    public function transactions() {
        return $this->hasMany('App\Transaction', 'user_id')->orderBy('created_at', 'desc');
        ;
    }

    public function firstTransaction() {
        return $this->hasOne(Transaction::class);
    }

    // Return favorites
    public function favorites() {
        return $this->hasMany('App\Favorite', 'user_id');
    }

    // Return networks
    public function networks() {
        return $this->belongsToMany('App\Network');
    }

    // Return first network
    public function getNetworkAttribute() {
        return $this->networks->first();
    }

    // Return first network
    public function firstNetwork() {
        return $this->belongsToMany('App\Network')->first();
    }

    // Return references
    public function references() {
        return $this->hasMany('App\Reference');
    }

    public function firstReference() {
        return $this->hasOne('App\Reference', 'user_id', 'id');
    }

    // Return country
    public function country() {
        return $this->belongsTo('App\Country');
    }

    // Return city
    public function city() {
        return $this->belongsTo('App\City');
    }

    // Return area
    public function area() {
        return $this->belongsTo('App\Area');
    }

    // Return cart items
    public function cartItems() {
        return $this->hasMany('App\Cart');
    }

    // Return cart items
    public function affiliatePrograms() {
        return $this->hasMany('App\AffiliateProgram');
    }

    // Return first partner entity
    public function affliateProgramsMemberships() {
        return $this->hasMany('App\AffiliateProgramsMemberships');
    }

    // Return highest assigned role ID
    public function highestRoleID() {
        return DB::table('role_user')->where('user_id', $this->id)->max('role_id');
    }

    /**
     * Check if the curren user has a network balance
     *
     * @param int $network
     * @return bool
     */
    public function hasNetworkBalance($network_id = 1) {
        $count = Balance::where('network_id', $network_id)->where('user_id', $this->id)->count();

        if ($count > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Determine if the user has enough network balance to
     *
     * @param int $network_id
     * @param float $amount
     * @return bool
     */
    public function hasEnoughNetworkBalance($network_id, $amount) {
        $balance = $this->balance($network_id);

        if ($balance >= $amount) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Determine if the user has enough combined BLU & Network balance
     *
     * @param int $network_id
     * @param int $amount
     * @return bool
     */
    public function hasEnoughCombinedPoints($network_id, $amount) {
        $blu_balance = $this->balance(1);
        $net_balance = $this->balance($network_id);

        if (($blu_balance + $net_balance) >= $amount) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Fetch a list of all the users balances
     *
     * @return array
     */
    public function balances() {
        return Balance::where('user_id', $this->id)->with('network')->orderBy('balance.updated_at', 'ASC')->get();
    }

    /**
     * Update a users balance on a specific network
     *
     * @param int $amt
     * @param int $network_id
     * @return int
     */
    public function updateBalance($amt, $network_id = 1, $op_type = 'add') {
        $balance = Balance::where('network_id', $network_id)->where('user_id', $this->id)->first();

        if (!$balance) {
            $balance = new Balance();
            $balance->user_id = $this->id;
            $balance->network_id = $network_id;
            $balance->balance = 0;
        }

        switch ($op_type) {
            case "add":
                $balance->balance += $amt;
                break;
            case "subtract":
                //if($balance->balance > 0) {
                $balance->balance -= $amt;
                //}
                //if($balance->balance < 0) {
                //    $balance->balance = 0;
                //}
                break;
        }

        $balance->save();

        return $balance->balance;
    }

    /**
     * Fetch the users balance for the provided network
     *
     * @param int $network_id
     * @return int
     */
    public function balance($network_id = 1) {
        $balance = Balance::where('network_id', $network_id)
                ->where('user_id', $this->id)
                ->select('balance')
                ->first();

        if (!$balance) {
            $balance = new Balance();

            $balance->user_id = $this->id;
            $balance->network_id = $network_id;
            $balance->balance = 0;

            $balance->save();
        }

        return $balance->balance;
    }

    /**
     * Deduct the maximum possible points amount from the network
     *
     * @param int $points
     * @param int $transaction_network1
     * @param int $attempt_network1
     * @return int
     */
    public function deductMaxPoints($points, $transaction_network1, $attempt_network1) {
        if ($points == 0) {
            return 0;
        }

        $network_fields = array(
            'id', 'network_point', 'blu_point', 'name', 'do_points_conversion'
        );

        $network_balance = $this->balance($attempt_network1);
        $transaction_network = Network::where('id', '=', $transaction_network1)->select($network_fields)->take(1)->first();
        $attempt_network = Network::where('id', '=', $attempt_network1)->select($network_fields)->take(1)->first();
        $deducted_amount = 0;
        //$points_value        = 0;
        # Is this a cross network transaction?
        $cross_network = function() use($attempt_network, $transaction_network) {
            return $attempt_network->id != $transaction_network->id;
        };

        # Convert network points to BLU points
        $to_blu_points = function($amount, $network) {
            return ceil(($network->blu_point / $network->network_point) * $amount);
        };

        # Convert BLU points to network points
        $from_blu_points = function($amount, $network) {
            return ceil(($network->network_point / $network->blu_point) * $amount);
        };

        # Calculate the maximum deductable amount
        $calculate_deductable = function($points) use($network_balance) {
            if ($network_balance >= $points) {
                return $points;
            } else {
                return $network_balance;
            }
        };

        // Calculate deductable amount

        $points_value = $points;
        //$deducted_amount = $cross_network();
        $not_blu_attempt = false;
        $not_blu_trx = false;
        if ($cross_network() && $attempt_network->id == BLU) {
            if ($transaction_network->do_points_conversion) {
                $points_value = $to_blu_points($points, $transaction_network);
                $deducted_amount = $calculate_deductable($points_value);
                //$deducted_amount = $from_blu_points($deducted_amount, $transaction_network);
                $not_blu_trx = true;
            }
        } elseif ($cross_network() && $transaction_network->id == BLU) {
            if ($attempt_network->do_points_conversion) {
                $points_value = $from_blu_points($points, $attempt_network);
                $deducted_amount = $calculate_deductable($points_value);
                $not_blu_attempt = true;
                //$deducted_amount = $to_blu_points($deducted_amount, $attempt_network);
            }
        } elseif ($attempt_network->id == $transaction_network->id) {

            $deducted_amount = $calculate_deductable($points_value);
        } elseif ($attempt_network->id != $transaction_network->id) {
            $points_val_blu = $to_blu_points($points_value, $transaction_network);
            $points_value = $from_blu_points($points_val_blu, $attempt_network);
            $deducted_amount = $calculate_deductable($points_value);
            $not_blu_attempt = true;
            $not_blu_trx = true;
        }

        $this->updateBalance($deducted_amount, $attempt_network->id, 'subtract');
        if ($not_blu_attempt == true) {
            $deducted_amount = $to_blu_points($deducted_amount, $attempt_network);
        } elseif ($not_blu_trx == true) {
            $deducted_amount = $from_blu_points($deducted_amount, $transaction_network);
        } elseif ($not_blu_attempt && $not_blu_trx) {
            $deducted_amount1 = $to_blu_points($deducted_amount, $attempt_network);
            $deducted_amount = $from_blu_points($deducted_amount1, $transaction_network);
        }
        return $deducted_amount;
    }

    # --------------------------------------------------------------------------
    #
    # Helpers
    #
    # --------------------------------------------------------------------------

    /**
     * Pretty print full gender
     *
     * @return mixed
     */
    public function fullGender() {
        $default = 'Male';

        switch ($this->gender) {
            case "f":
                $default = 'Female';
                break;
        }

        return $default;
    }

    /**
     * Calcuate the user's age
     *
     * @return int
     */
    public function age() {
        $date = new DateTime($this->dob);
        $now = new DateTime();

        $interval = $now->diff($date);

        return $interval->y;
    }

    /**
     * Get the unique identifier for the user.
     *
     * @return mixed
     */
    public function getAuthIdentifier() {
        return $this->getKey();
    }

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword() {
        return $this->password;
    }

    /**
     * Get the e-mail address where password reminders are sent.
     *
     * @return string
     */
    public function getReminderEmail() {
        return $this->email;
    }

    /**
     * Concat the various roles into one string
     *
     * @return string
     */
    public function roleString() {
        $roles = $this->roles()->get();
        $buffer = [];

        foreach ($roles as $r) {
            $buffer[] = $r->name;
        }

        echo implode(', ', $buffer);
    }

    /**
     * Return the full normalised contact number
     *
     * @return string
     */
    public function normalizedContactNumber() {
        if (strpos($this->mobile, '+') !== false) {
            return $this->mobile;
        }

        $num = ltrim(trim((string) $this->mobile), 0);

        if (empty($num)) {
            return "N/A";
        }

        return "{$this->telephone_code}{$num}";
    }

    /**
     * Return the top most partner
     *
     * @return Result
     */
    public function getTopLevelPartner() {
        $firstLevelChildren = array();
        $highestLevelParents = array();
        $sameLevelPartners = array();

        foreach ($this->partners as $partner) {
            $partners[] = $partner->id;
        }

        foreach ($partners as $partner_id) {
            $partner = Partner::find($partner_id);
            $partnerParent = $partner->parent_id;
            if ($partner->is_top_level_partner) {
                return Partner::find($partner_id);
            } elseif (empty($partnerParent)) {
                return Partner::find($partner_id);
            } else {
                if (in_array($partnerParent, $partners)) {
                    $highestLevelParents[$partnerParent][] = $partner_id;
                    //				$firstLevelParents[] = $partner_id;
                }
                if ($partnerParent == 1) {
                    $firstLevelChildren[] = $partner_id;
                } else {
                    $sameLevelPartners[] = $partner_id;
                }
            }
        }

        if (!empty($firstLevelChildren)) {
            sort($firstLevelChildren);
            return Partner::find($firstLevelChildren[0]);
        } elseif (!empty($highestLevelParents)) {
            $highestParents = array_keys($highestLevelParents);
            sort($highestParents);
            return Partner::find($highestParents[0]);
        } else {
            sort($sameLevelPartners);
            return Partner::find($sameLevelPartners[0]);
        }

//        $partner = $this->partners->first();
//
//        if (!$partner) {
//            $partner = Partner::find(1); //return blu partner by default
//        }
//
//        return $partner;
    }

    /**
     * Fetch a name attribute string
     *
     * @return string
     */
    public function getNameAttribute() {
        return $this->first_name . chr(32) . $this->last_name;
    }

    /**
     * Return a full name string
     *
     * @return string
     */
    public function getTitleAndFullName() {
        $title = ($this->title) ? $this->title : '';

        return $title . chr(32) . $this->getNameAttribute();
    }

    /**
     * Send out the users welcome message SMS
     *
     * @return void
     */
    public function sendWelcomeMessage($partnerId = 1, $templateName = 'new-user') {
        if ($this->draft) {
            $this->draft = false;
            $partner = Partner::find($partnerId);
            $template = View::make('sms.' . $templateName, array('user' => $this))->render();

            SMSHelper::send($template, $this, $partner);
        }
    }

    /**
     * Send out the users welcome message SMS
     *
     * @return void
     */
    public function sendWelcomeMessageNew($partnerId = 1) {
        if ($this->draft || $partnerId == 1 || $partnerId == 4209 || $partnerId == 4188 || $partnerId == 4245 || $partnerId == 4235 || $partnerId == 4246) {
            $this->draft = false;
            $partner = Partner::find($partnerId);
            if (empty($partner->welcome_sms)) {
                $partner = Partner::find(1);
            }

            $template = $partner->welcome_sms;
            $ref = Reference::where('user_id', $this->id)->first();
            if (isset($ref)) {
                $values = array("{ref}", "{passcode}");
                $replace = array($ref->number, $this->passcode);
                $template = str_ireplace($values, $replace, $template);
            } else {
                $template = str_ireplace("{passcode}", $this->passcode, $template);
            }

            SMSHelper::send($template, $this, $partner);
        }
    }

    /**
     * Re-send the account PIN code to the user
     *
     * @return void
     */
    public function sendAccountPin() {

        $partner = Partner::find(1);
        $template = View::make('sms.pin-resend', array('user' => $this))->render();

        SMSHelper::send($template, $this, $partner);
    }

    /**
     * Helper to create a new user account
     *
     * @return User
     */
    public static function createNewUser($status = 'active') {

        $partner = Auth::User()->getTopLevelPartner();

        $user = new User();
        $user->ucid = UCID::newCustomerID();
        $user->passcode = PinCode::newCode();
        $user->country_id = $partner->country_id;
        $user->area_id = $partner->area_id;
        $user->city_id = $partner->city_id;
        $user->status = $status;
        $user->dob = "1975-01-01";
        $user->api_key = md5($user->ucid);
        $user->email = "";
        $user->save();

        //link the new partner to current user top level partner
        $user->partners()->attach($partner->id);

        // Assign a default role
        $role = Role::where('name', '=', 'Member')->first();

        if ($role) {
            $user->roles()->attach($role->id);
        }

        DB::table('segment_user')->insert(array(
            'segment_id' => $partner->segments->first()->id,
            'user_id' => $user->id
        ));

        //Default Tier affiliation to Member
        $defaultTier = Tier::where('partner_id', $partner->id)->where('name', 'like', 'Default Tier%')->first();
        if (!empty($defaultTier)) {
            $defaultTierCriteria = Tier::where('tier_id', $defaultTier->id)->where('name', 'like', 'Default Tier Criteria%')->first();
            if (!empty($defaultTierCriteria)) {
                DB::table('tiercriteria_user')->insert(array(
                    'tiercriteria_id' => $defaultTierCriteria->id,
                    'user_id' => $user->id
                ));
            }
        }
        return $user;
    }

    # --------------------------------------------------------------------------
    #
    # Managed Objects
    #
    # --------------------------------------------------------------------------

    /**
     * Fetch a list of all partners managed by this user - in scope
     *
     * @return array
     */
    public function managedPartners() {
        $partners = $this->partners()->where('draft', false)->get();
        $collection = new BluCollection();

        foreach ($partners as $partner) {
            $childPartners = $partner->managedObjects();

            foreach ($childPartners as $childPartner) {
                if (!$collection->contains($childPartner)) {
                    $collection->add($childPartner);
                }
            }
        }

        return $collection;
    }

    /**
     * Return a list of only managed parent partner ID's
     *
     * @return array
     */
    public function managedParentPartnerIDs() {
        $parents = DB::table('partner')->select('parent_id')->whereIn('parent_id', $this->managedPartnerIDs())->distinct()->orderBy('name')->get();
        $buffer = array();

        foreach ($parents as $p) {
            $buffer[] = $p->parent_id;
        }

        return $buffer;
    }

    /**
     * Return a list of managed parent partners
     *
     * @return array
     */
    public function managedParentPartners() {
        return Partner::where('draft', false)->whereIn('id', $this->managedParentPartnerIDs())->orderBy('name');
    }

    /**
     * Fetch a list of ID's belonging to managed partners
     *
     * @return array
     */
    public function managedPartnerIDs() {
        $partners = $this->managedPartners();
        $buffer = array();

        foreach ($partners as $p) {
            if (!in_array($p->id, $buffer)) {
                $buffer[] = $p->id;
            }
        }

        return $buffer;
    }

    /**
     * Generic to fetch a managed property
     *
     * @param string $property
     * @return Collection
     */
    private function managedProperty($property) {
        $collection = new BluCollection();

        foreach ($this->managedPartners() as $partner) {
            foreach ($partner->$property as $p) {
                if (!$collection->contains($p)) {
                    $collection->add($p);
                }
            }
        }

        return $collection;
    }

    /**
     * Fetch a list of segments managed by the current user partner
     *
     * @return Collection
     */
    public function managedSegments() {
        if (I::am('BLU Admin')) {
            return Segment::where('draft', false)->orderBy('name')->get();
        } else {
            return $this->managedProperty('segments');
        }
    }

    /**
     * Fetch a list of stores managed by the current user
     *
     * @return Collection
     */
    public function managedStores() {
        if (I::am('BLU Admin')) {
            return Store::where('draft', false)->orderBy('name')->get();
        } else {
            return $this->managedProperty('stores');
        }
    }

    /**
     * Fetch a list of products/services managed by the current user
     *
     * @return Collection
     */
    public function managedItems() {
        if (I::am('BLU Admin')) {
            return Product::where('draft', false)->orderBy('name')->get();
        } else {
            return $this->managedProperty('products');
        }
    }

    /**
     * Fetch a list of Offers managed by the current user
     *
     * @return Collection
     */
    public function managedOffers() {
        if (I::am('BLU Admin')) {
            return Offer::where('draft', false)->orderBy('name')->get();
        } else {
            return $this->managedProperty('offers');
        }
    }

    /**
     * Fetch a list of Offers managed by the current user
     *
     * @return Collection
     */
    public function managedEstatements() {
        if (I::am('BLU Admin')) {
            return App\Estatement::where('draft', false)->orderBy('name')->get();
        } else {
            return $this->managedProperty('estatement');
        }
    }

    /**
     * Fetch all notifications managed by this user on behalf of partners
     *
     * @return Collection
     */
    public function managedNotifications() {
        if (I::am('BLU Admin')) {
            return Notification::where('draft', false)->orderBy('name')->get();
        } else {
            return $this->managedProperty('notifications');
        }
    }

    /**
     * Fetch a list of tickets managed by the current user
     *
     * @return Collection
     */
    public function managedTickets($limit = null) {
        if (I::am('BLU Admin') || I::am('Support Center Agent') || I::am('BLU Admin 2') || I::am('Partner Admin Level 3')) {
            if (empty($limit)) {
                return Ticket::where('draft', false)->whereIn('partner_id', $this->managedPartnerIDs())->orderBy('created_at')->get();
            } else {
                return Ticket::where('draft', false)->whereIn('partner_id', $this->managedPartnerIDs())->orderBy('created_at')->limit($limit)->get();
            }
        } else {
            return new BluCollection();
        }
    }

    /**
     * Fetch a list of coupons managed by the current user
     *
     * @return Collection
     */
    public function managedCoupons() {
        if (I::am('BLU Admin')) {
            return Batch::where('draft', false)->where('type', 'Coupons')->orderBy('id')->get();
        } else {
            return $this->managedProperty('coupons');
        }
    }

    /**
     * Fetch a list of batches (Cards + Refs) managed by the current user
     *
     * @return Collection
     */
    public function managedBatches() {
        if (I::am('BLU Admin')) {
            return Batch::where('draft', false)->where('type', '!=', 'Coupons')->orderBy('id')->get();
        } else {
            return $this->managedProperty('batches');
        }
    }

    /**
     * Fetch a list of members managed by the current user
     *
     * @param $eager_load Boolean value indicating if we should eager load the results
     * @param $query String value to use as a search basis
     * @return Collection
     */
    public function managedMembers($eager_load = true, $query = null) {
        $collection = User::where('draft', false)->orderBy('created_at');

        if (I::am('BLU Admin')) {
            if (!empty($query)) {
                $query = explode(chr(32), $query);

                $collection->where(function($q) use($query) {
                    foreach ($query as $key => $value) {
                        $q->where('first_name', 'LIKE', '%' . $value . '%');
                        $q->orWhere('last_name', 'LIKE', '%' . $value . '%');
                    }
                });
            }
            // Eager loading could sometimes be a bad thing, so use conditionally
            if ($eager_load) {
                return $collection->get();
            } else {
                return $collection;
            }
        } else {
            if ($eager_load) {
                return $this->partnerHierarchyMembers($query)->get();
            } else {
                return $this->partnerHierarchyMembers($query);
            }
        }
    }

    /**
     * Re-sampled method to return memberships of all partners in hierarchy
     *
     * @return collection
     */
    public function partnerHierarchyMembers($query = null, $eager_load = false) {
        $ids = array();

        foreach ($this->managedPartners() as $p) {
            if (!in_array($p->id, $ids)) {
                $ids[] = $p->id;
            }
        }

        $results = User::distinct('users.id')->select(
                        'users.id', 'users.email', 'users.username', 'users.verified', 'users.title', 'users.balance', 'users.first_name', 'users.middle_name', 'users.last_name', 'users.mobile', 'users.normalized_mobile', 'users.telephone_code', 'users.profile_image', 'users.status', 'users.country_id', 'users.city_id', 'users.area_id'
                )->join('partner_user', 'users.id', '=', 'partner_user.user_id')
                ->whereIn('partner_user.partner_id', $ids);

        if (!empty($query)) {
            $query = explode(chr(32), $query);

            $results->where(function($q) use($query) {
                foreach ($query as $key => $value) {
                    $q->where('first_name', 'LIKE', '%' . $value . '%');
                    $q->orWhere('last_name', 'LIKE', '%' . $value . '%');
                }
            });
        }


        unset($ids);

        return $results;
    }

    /**
     * Fetch a list of removed members managed by the current user
     *
     * @return Collection
     */
    public function managedRemovedMembers() {
        if (I::am('BLU Admin')) {
            return User::onlyTrashed()->where('draft', false)->orderBy('created_at');
        } else {
            return $this->managedProperty('removedMembers');
        }
    }

    /**
     * Fetch a list of removed members managed by the current user
     *
     * @return Collection
     */
    public function managedRemovedProducts() {
        if (I::am('BLU Admin')) {
            return Product::onlyTrashed()->where('draft', false)->orderBy('created_at');
        } else {
            return $this->managedProperty('removedProducts');
        }
    }

    /**
     * Fetch a list of roles managed by the current user
     *
     * @return Collection
     */
    public function managedRoles() {
        if (I::am('BLU Admin')) {
            return Role::where('draft', false)->orderBy('created_at')->get();
        } else {
            return $this->managedProperty('roles');
        }
    }

    /**
     * Fetch a list of loyalty programs managed by the current user
     *
     * @return Collection
     */
    public function managedLoyalty() {
        if (I::am('BLU Admin')) {
            return Loyalty::where('draft', false)->orderBy('created_at')->get();
        } else {
            return $this->managedProperty('loyaltyPrograms');
        }
    }

    /**
     * Fetch a list of POS managed by the current user
     *
     * @return Collection
     */
    public function managedPointOfSales() {
        if (I::am('BLU Admin')) {
            $buffer = new BluCollection();

            foreach (Pos::orderBy('created_at')->get() as $pos) {
                $buffer->add($pos);
            }

            return $buffer;
        } else {
            $stores = $this->managedStores();
            $buffer = new BluCollection();

            foreach ($stores as $store) {

                $storePoses = $store->pointOfSales;

                if ($storePoses) {
                    foreach ($storePoses as $storePos) {
                        if (!$buffer->contains($storePos)) {
                            $buffer->add($storePos);
                        }
                    }
                }
            }

            return $buffer;
        }
    }

    /**
     * Fetch a list of banners managed by the current user
     *
     * @return Collection
     */
    public function managedBanners() {
        if (I::am('BLU Admin')) {
            return Banner::where('draft', false)->orderBy('created_at')->get();
        } else {
            return $this->managedProperty('banners');
        }
    }

    /**
     * Return a list of only managed networks ID's
     *
     * @return array
     */
    public function managedNetworks() {
        if (I::am('BLU Admin')) {
            return Network::where('draft', false)->orderBy('name')->get();
        } else {
            return $this->managedProperty('networks');
        }
    }

    /**
     * Fetch a list of suppliers managed by the current user <fboukarroum>
     *
     * @return Collection
     */
//    public function managedSuppliers()
//    {
//    	if(I::am('BLU Admin')) {
//			return Supplier::where('draft', false)->orderBy('name')->get();
//    	} else {
//            return $this->managedProperty('suppliers');
//    	}
//    }
    # --------------------------------------------------------------------------
    #
    # Query Scopes
    #
    # --------------------------------------------------------------------------

    public function scopeWomen($query) {
        return $query->whereGender('f');
    }

    public function scopeMen($query) {
        return $query->whereGender('m');
    }

    public function scopeActive($query) {
        return $query->whereStatus('active');
    }

    public function scopeSuspended($query) {
        return $query->whereStatus('suspended');
    }

    public function scopeInactive($query) {
        return $query->whereStatus('inactive');
    }

    public function scopeVerified($query) {
        return $query->whereVerified(1);
    }

    public function scopeValid($query) {
        return $query->whereVerified(true)->whereDraft(false);
    }

    /**
     * Return the top most partner
     *
     * @return Result
     */
    public function getTopHierarchyPartnerIds() {
        $firstLevelChildren = array();
        $highestLevelParents = array();
        $sameLevelPartners = array();

        foreach ($this->partners as $partner) {
            $partners[] = $partner->id;
        }

        foreach ($partners as $partner_id) {
            $partner = Partner::find($partner_id);
            $partnerParent = $partner->parent_id;
            if ($partner->is_top_level_partner) {
                return $partner_id;
            } elseif (empty($partnerParent)) {
                return Partner::find($partner_id);
            } else {
                if (in_array($partnerParent, $partners)) {
                    $highestLevelParents[$partnerParent][] = $partner_id;
                }
                if ($partnerParent == 1) {
                    $firstLevelChildren[] = $partner_id;
                } else {
                    $sameLevelPartners[] = $partner_id;
                }
            }
        }

        if (!empty($firstLevelChildren)) {
            sort($firstLevelChildren);
            return $firstLevelChildren;
        } elseif (!empty($highestLevelParents)) {
            $highestParents = array_keys($highestLevelParents);
            sort($highestParents);
            return $highestParents;
        } else {
            sort($sameLevelPartners);
            return $sameLevelPartners;
        }
    }

    public function getProfileImagePath() {
        if (!$this->profileImageMedia) {
            throw new \Exception('Profile image is missing');
        }

        return public_path("/uploads/{$this->profileImageMedia->name}");
    }

    public function generateProfileImageThumbnail($width = self::TN_FE_PROFILE_IMAGE_WIDTH, $height = self::TN_FE_PROFILE_IMAGE_HEIGHT) {
        if (!$this->profileImageMedia || !file_exists($this->getProfileImagePath())) {
            return $this;
        }
        $thumbnailName = "tn_{$width}x{$height}_{$this->profileImageMedia->name}";
        $image = Image::make($this->getProfileImagePath())->resize($width, $height)->save(public_path("/uploads/{$thumbnailName}"));

        return $this;
    }

}

// EOC
