<?php
namespace App;
use App\BluCollection;
use Eloquent;
/**
 * Point of Sale Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class Pos extends Eloquent 
{
    public $table = 'pos';

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------
    
    public function store()
    {
        return $this->belongsTo('App\Store');
    }

} // EOC