<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class SystemCronLog extends Model
{
    protected $table = 'system_cron_logs';

    public $timestamps = false;

    /**
     * @return Carbon
     */
    public function lastRunDate()
    {
        return new Carbon($this->last_run_date);
    }

    /**
     * @param Carbon $date
     *
     * @return SystemCronLog
     */
    public function setLastRunDate(Carbon $date)
    {
        $this->last_run_date = $date->format('Y-m-d H:i:s');

        return $this;
    }
}
