<?php

/**
 * Store Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class StoreFnb extends BaseModel {

    public $table = 'store_fnb';

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------

    public function store() {
        return $this->belongsTo('Store');
    }
}

// EOC