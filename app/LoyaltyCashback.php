<?php
namespace App;

/**
 * Loyalty Rule Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Ralph Nader <ralph@bluloyalty.com>
 * @copyright  2016 BLU
 * @link       http://blupoints.com
 */
class LoyaltyCashback extends BaseModel
{
    public $table = 'loyalty_cashback';

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------
    
    public function loyalty_program()
    {
        return $this->belongsTo('App\Loyalty');
    }

} // EOC