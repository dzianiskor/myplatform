<?php
namespace App;
use App\BluCollection;
use Eloquent;
/**
 * Mobile Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class MobileUserGames extends Eloquent
{
    public $table = 'mobile_user_games';

    public function game()
    {
        return $this->belongsTo('App\MobileGame');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
} // EOC