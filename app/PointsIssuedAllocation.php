<?php
namespace App;
use App\BluCollection;
use Eloquent;
/**
 * Product Channel Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Magid Mroueh <magid.mroueh@bluloyalty.com>
 * @copyright  2017 BLU
 * @link       http://blupoints.com
 */
class PointsIssuedAllocation extends Eloquent 
{
    public $table = 'points_issued_allocation';
    public $timestamps = false;

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------

    public function transactionitem()
    { 
        return $this->belongsTo('App\TransactionItem', 'trx_item_id'); 
    }  
    
    public function transactiondeduction()
    { 
        return $this->belongsTo('App\transaction_deduction', 'trx_deduction_id'); 
    }        

} // EOC