<?php

/**
 * Notification Segment Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Magid Mroueh <magid.mroueh@bluloyalty.com>
 * @copyright  2017 BLU
 * @link       http://blupoints.com
 */
class PreferenceUser extends BaseModel {
    public $table = 'preference_user';
}



// EOC