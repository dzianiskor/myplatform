<?php
namespace App;
use App\BluCollection;
use Eloquent;
use Illuminate\Support\Facades\Hash;
/**
 * Area Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class PasswordHistory extends Eloquent 
{
    public $table      = 'admin_password_history';

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------
    
    public function admin() 
    { 
    	return $this->belongsTo('App\Admin'); 
    }

    /**
     * checks the admin's old passwords 
     * @return true if the password equals any of the last 4 passwords
     * @return false if the password doesn't equal any of the last 4 passwords
     */
    public static function isEqualOldPassowrds($password, $user_id) {
        $oldPasswords = PasswordHistory::where('admin_id', $user_id)->limit(4)->orderBy('created_at','DESC')->get();
        foreach($oldPasswords as $oldPassword) {
            if(Hash::check($password, $oldPassword->password)) {
               return true;
            }
        }
        return false;
    }

    public static function savePassword($user) {

        $passwordHistory = new PasswordHistory();
        $passwordHistory->admin_id = $user->id;
        $passwordHistory->password = $user->password;
        $passwordHistory->created_at = date('Y-m-d H:i:s');
        $passwordHistory->save();
    }
} // EOC