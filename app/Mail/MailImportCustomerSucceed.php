<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MailImportCustomerSucceed extends Mailable
{
    use Queueable, SerializesModels;
    private $subjectSuffix;

    /**
     * Create a new message instance.
     *
     * @param string $subjectSuffix
     */
    public function __construct($subjectSuffix)
    {
        $this->subjectSuffix = $subjectSuffix;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->to(env('BLU_ADMIN_EMAIL', 'adminit@bluloyalty.com'))
                    ->subject('Customer Import ' . $this->subjectSuffix)
                    ->view('emails.import.customer-succeed');
    }
}
