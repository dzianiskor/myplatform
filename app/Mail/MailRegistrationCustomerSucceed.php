<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MailRegistrationCustomerSucceed extends Mailable
{
    use Queueable, SerializesModels;
    private $subjectSuffix;
    public $users;

    /**
     * Create a new message instance.
     *
     * @param string $subjectSuffix
     */
    public function __construct($subjectSuffix, $users)
    {
        $this->subjectSuffix = $subjectSuffix;
        $this->users = $users;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->to(env('BLU_ADMIN_EMAIL', 'adminit@bluloyalty.com'))
                    ->subject($this->subjectSuffix)
                    ->view('emails.registration.customer-registration-succeed');
    }
}