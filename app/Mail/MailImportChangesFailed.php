<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MailImportChangesFailed extends Mailable
{
    use Queueable, SerializesModels;
    private $filePath;
    private $subjectSuffix;

    /**
     * Create a new message instance.
     *
     * @param string $filePath
     * @param string $subjectSuffix
     */
    public function __construct($filePath, $subjectSuffix)
    {
        $this->filePath = $filePath;
        $this->subjectSuffix = $subjectSuffix;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->to(env('BLU_ADMIN_EMAIL', 'adminit@bluloyalty.com'))
                    ->subject('Changes Import ' . $this->subjectSuffix)
                    ->view('emails.import.changes-failed')
                    ->attach($this->filePath);
    }
}
