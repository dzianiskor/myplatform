<?php
namespace App;
use App\BluCollection;
/**
 * Loyalty Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class StampReward extends BaseModel 
{
    public $table = 'stamp_reward';

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------
    
    public function partner() 
    {
        return $this->belongsTo('App\Partner');
    }

    

    # --------------------------------------------------------------------------
    # 
    # Attributes
    # 
    # --------------------------------------------------------------------------    

	

    # --------------------------------------------------------------------------
    # 
    # Helpers
    # 
    # --------------------------------------------------------------------------

    
} // EOC