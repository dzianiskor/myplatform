<?php
namespace App;

use App\Notifications\MailResetPasswordToken;
use App\Partner as Partner;
use I;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use UCID;
use Illuminate\Support\Facades\Auth as Auth;
// Don't Remove
if(!defined('BLU')) {
    define("BLU", 1);
}

/**
 * User Model
 *
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class Admin extends Authenticatable
{
    use Notifiable;

    protected $table      = 'admins';
    protected $hidden     = array('password');
    protected $guarded    = array('password', 'ucid');
    protected $dates      = ['deleted_at'];
    protected $softDelete = true;

    # --------------------------------------------------------------------------
    #
    # Relationships
    #
    # --------------------------------------------------------------------------

    // Return roles
    public function roles()
    {
        return $this->belongsToMany('App\Role', 'role_admin', 'admin_id', 'role_id');
    }

    // Return partners entities
    public function partners()
    {
        return $this->belongsToMany('App\Partner', 'partner_admin')->distinct();
    }

    // Return first partner entity
    public function firstPartner()
    {
        return $this->belongsToMany('App\Partner', 'partner_admin', 'admin_id', 'partner_id')->first();
    }
    
    // Return address
    public function address()
    {
        return $this->hasOne('App\Address', 'mapping_key');
    }



    // Return country
    public function country()
    {
        return $this->belongsTo('App\Country');
    }

    // Return city
    public function city()
    {
        return $this->belongsTo('App\City');
    }

    // Return area
    public function area()
    {
        return $this->belongsTo('App\Area');
    }



    // Return highest assigned role ID
    public function highestRoleID()
    {
        return DB::table('role_admin')->where('admin_id', $this->id)->max('role_id');
    }

    // Return Admin parent ID
    public function parentID()
    {
		$admin	= DB::table('admins')->where('id', $this->id)->first();
        return $admin->parent_id;
    }



    # --------------------------------------------------------------------------
    #
    # Helpers
    #
    # --------------------------------------------------------------------------

    /**
   	 * Pretty print full gender
   	 *
   	 * @return mixed
   	 */
    public function fullGender()
    {
        $default = 'Male';

        switch($this->gender) {
            case "f":
                $default = 'Female';
                break;
        }

        return $default;
    }

    /**
   	 * Calcuate the user's age
   	 *
   	 * @return int
   	 */
    public function age()
    {
        $date = new DateTime($this->dob);
        $now  = new DateTime();

        $interval = $now->diff($date);

        return $interval->y;
    }

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}

    /**
     * Concat the various roles into one string
     *
     * @return string
     */
    public function roleString()
    {
        $roles  = $this->roles()->get();
        $buffer = [];

        foreach($roles as $r) {
            $buffer[] = $r->name;
        }

        echo implode(', ', $buffer);
    }

    /**
     * Return the full normalised contact number
     *
     * @return string
     */
    public function normalizedContactNumber()
    {
        if (strpos($this->mobile, '+') !== false) {
            return $this->mobile;
        }

        $num = ltrim(trim((string)$this->mobile), 0);

        if(empty($num)) {
            return "N/A";
        }

		return "{$this->telephone_code}{$num}";
    }

    /**
     * Return the top most partner
     *
     * @return Result
     */
    public function getTopLevelPartner()
    {
        $firstLevelChildren	= array();
        $highestLevelParents	= array();
        $sameLevelPartners	= array();

        foreach ($this->partners as $partner){
                $partners[] = $partner->id;
        }

        foreach ($partners as $partner_id){
                $partner	= Partner::find($partner_id);
                $partnerParent	= $partner->parent_id;
                if($partner->is_top_level_partner){
                    return Partner::find($partner_id);
                }
                elseif(empty($partnerParent)){
                 return Partner::find($partner_id);
                }else {
                        if(in_array($partnerParent, $partners)){
                           $highestLevelParents[$partnerParent][] = $partner_id;
//				$firstLevelParents[] = $partner_id;
                        }
                        if($partnerParent == 1){
                                $firstLevelChildren[]	= $partner_id;
                        }else{
                                $sameLevelPartners[]	= $partner_id;
                        }
           }
        }

        if(!empty($firstLevelChildren)){
           sort($firstLevelChildren);
           return Partner::find($firstLevelChildren[0]);
        }
        elseif(!empty($highestLevelParents)){
           $highestParents	= array_keys($highestLevelParents);
           sort($highestParents);
           return Partner::find($highestParents[0]);
        }
        else{
           sort($sameLevelPartners);
           return Partner::find($sameLevelPartners[0]);
        }
//        $partner = $this->partners->first();
//
//        if (!$partner) {
//            $partner = Partner::find(1); //return blu partner by default
//        }
//
//        return $partner;
    }

    /**
     * Fetch a name attribute string
     *
     * @return string
     */
    public function getNameAttribute()
    {
        return $this->first_name.chr(32).$this->last_name;
    }

    /**
     * Return a full name string
     *
     * @return string
     */
    public function getTitleAndFullName()
    {
        $title = ($this->title)? $this->title : '';

        return $title.chr(32).$this->getNameAttribute();
    }





    /**
     * Helper to create a new user account
     *
     * @return User
     */
    public static function createNewUser()
	{
        $user             = new Admin();
        $user->ucid       = UCID::newCustomerID();
        $user->country_id = 9;
        $user->area_id    = 76;
        $user->city_id    = 672;
        $user->status     = "active";
        $user->dob        = "1975-01-01";
	$user->api_key	  = md5($user->ucid);
        $user->verified   = '1';
        $user->pref_lang  = 'en';
        $user->email      ="";
        $user->username   = "";
        $user->password   ="";
        $user->save();

        //link the new partner to current user top level partner
        $partner = Auth::User()->getTopLevelPartner();

        $user->partners()->attach($partner->id);

//        // Assign a default role
//        $role = Role::where('name', '=', 'Member')->first();
//
//        if($role) {
//            $user->roles()->attach($role->id);
//        }

        return $user;
    }

    # --------------------------------------------------------------------------
    #
    # Managed Objects
    #
    # --------------------------------------------------------------------------

	/**
	 * Fetch a list of all partners managed by this user - in scope
	 *
	 * @return array
	 */
    public function managedPartners()
    {
        $partners   = $this->partners()->where('draft', false)->get();

        $collection = new BluCollection();

        foreach($partners as $partner) {
            $childPartners = $partner->managedObjects();

            foreach ($childPartners as $childPartner) {
                if (!$collection->contains($childPartner)) {
                    $collection->add($childPartner);
                }
            }
        }

        return $collection;
    }
    
    public function managedPartnerFirst(){
        $partner   = $this->partners()->where('draft', false)->first();
        return $partner;
    }

    /**
     * Return a list of only managed parent partner ID's
     *
     * @return array
     */
    public function managedParentPartnerIDs()
    {
        $parents = DB::table('partner')->select('parent_id')->whereIn('parent_id', $this->managedPartnerIDs())->distinct()->orderBy('name')->get();
        $buffer  = array();

        foreach($parents as $p) {
            $buffer[] = $p->parent_id;
        }

        return $buffer;
    }

    /**
     * Return a list of managed parent partners
     *
     * @return array
     */
    public function managedParentPartners()
    {
        return Partner::where('draft', false)->whereIn('id', $this->managedParentPartnerIDs())->orderBy('name');
    }

    /**
     * Fetch a list of ID's belonging to managed partners
     *
     * @return array
     */
    public function managedPartnerIDs()
    {
        $partners = $this->managedPartners();
        $buffer   = array();

        foreach($partners as $p) {
            if(!in_array($p->id, $buffer)) {
                $buffer[] = $p->id;
            }
        }

        return $buffer;
    }

    /**
     * Generic to fetch a managed property
     *
     * @param string $property
     * @return Collection
     */
    private function managedProperty($property)
    {
        $collection = new BluCollection();

        foreach($this->managedPartners() as $partner) {
            foreach ($partner->$property as $p) {
                if (!$collection->contains($p)) {
                    $collection->add($p);
                }
            }
        }

		return $collection;
    }

    /**
	 * Fetch a list of invoices managed by the current user
	 *
	 * @return Collection
	 */
    public function managedInvoices()
    {
    	if(I::am('BLU Admin')) {
    		return UserInvoice::orderBy('created_at')->get();
    	} else {
            return $this->managedProperty('invoices');
    	}
    }

	/**
	 * Fetch a list of stores managed by the current user
	 *
	 * @return Collection
	 */
    public function managedStores()
    {
    	if(I::am('BLU Admin')) {
    		return Store::where('draft', false)->orderBy('name')->get();
    	} else {
            return $this->managedProperty('stores');
    	}
    }

	/**
	 * Fetch a list of products/services managed by the current user
	 *
	 * @return Collection
	 */
    public function managedItems()
    {
    	if(I::am('BLU Admin')) {
            return Product::where('draft', false)->orderBy('name')->get();
    	} else {
            return $this->managedProperty('products');
    	}
    }
    
    /**
     * Fetch a list of Offers managed by the current user
     *
     * @return Collection
     */
    public function managedOffers()
    {
    	if(I::am('BLU Admin')) {
		return Offer::where('draft', false)->orderBy('name')->get();
    	} else {
            return $this->managedProperty('offers');
    	}
    }
    /**
     * Fetch a list of Estatements managed by the current user
     *
     * @return Collection
     */
    public function managedEstatements()
    {
    	if(I::am('BLU Admin')) {
		return Estatement::where('draft', false)->where('deleted', false)->orderBy('title')->get();
    	} else {
            return $this->managedProperty('estatements');
    	}
    }
    /**
     * Fetch a list of Estatement history managed by the current user
     *
     * @return Collection
     */
    public function managedEstatementHistory()
    {
    	if(I::am('BLU Admin')) {
		return EstatementHistory::where('draft', false)->orderBy('title')->get();
    	} else {
            return $this->managedProperty('estatementhistory'); 
    	}
    }
    

	/**
	 * Fetch all notifications managed by this user on behalf of partners
	 *
	 * @return Collection
	 */
    public function managedNotifications()
    {
        if(I::am('BLU Admin')) {
            return Notification::where('draft', false)->orderBy('name')->get();
        } else {
            return $this->managedProperty('notifications');
        }
    }

    /**
   	 * Fetch a list of tickets managed by the current user
   	 *
   	 * @return Collection
   	 */
    public function managedTickets($limit = null)
    {
        if(I::can('view_tickets')) {
            if(empty($limit)){
                return Ticket::where('draft', false)->whereIn('partner_id', $this->managedPartnerIDs())->orderBy('created_at')->get();
            } else {
                return Ticket::where('draft', false)->whereIn('partner_id', $this->managedPartnerIDs())->orderBy('created_at')->limit($limit)->get();
            }
        } else {
            return new BluCollection();
        }
    }

    /**
     * Fetch a list of coupons managed by the current user
     *
     * @return Collection
     */
    public function managedCoupons()
    {
        if(I::am('BLU Admin')) {
            return Batch::where('draft', false)->where('type', 'Coupons')->orderBy('id')->get();
        } else {
            return $this->managedProperty('coupons');
        }
    }

    /**
     * Fetch a list of batches (Cards + Refs) managed by the current user
     *
     * @return Collection
     */
    public function managedBatches()
    {
        if(I::am('BLU Admin')) {
            return Batch::where('draft', false)->where('type', '!=', 'Coupons')->orderBy('id')->get();
        } else {
            return $this->managedProperty('batches');
        }
    }

    /**
     * Fetch a list of members managed by the current user
     *
     * @param $eager_load Boolean value indicating if we should eager load the results
     * @param $query String value to use as a search basis
     * @return Collection
     */
    public function managedMembers($eager_load = true, $query = null)
    {
        $collection = User::where('draft', false)->orderBy('users.created_at');

        if(I::am('BLU Admin')) {
            if(!empty($query)) {
                $query = explode(chr(32), $query);

                $collection->where(function($q) use($query){
                    foreach ($query as $key => $value) {
                        $q->where('first_name', 'LIKE', '%'. $value .'%');
                        $q->orWhere('last_name', 'LIKE', '%'. $value .'%');
                    }
                });
            }
            // Eager loading could sometimes be a bad thing, so use conditionally
            if($eager_load) {
                return $collection->get();
            } else {
                return $collection;
            }
        } else {
            if($eager_load) {
                return $this->partnerHierarchyMembers($query)->get();
            } else {
                return $this->partnerHierarchyMembers($query);
            }
        }
    }

    /**
     * Fetch a list of members managed by the current user
     *
     * @param $eager_load Boolean value indicating if we should eager load the results
     * @param $query String value to use as a search basis
     * @return Collection
     */
    public function managedAdmins($eager_load = true, $query = null)
    {
        $collection = Admin::where('draft', false)->orderBy('created_at');

        if(I::am('BLU Admin')) {
            if(!empty($query)) {
                $query = explode(chr(32), $query);

                $collection->where(function($q) use($query){
                    foreach ($query as $key => $value) {
                        $q->where('first_name', 'LIKE', '%'. $value .'%');
                        $q->orWhere('last_name', 'LIKE', '%'. $value .'%');
                    }
                });
            }
            // Eager loading could sometimes be a bad thing, so use conditionally
            if($eager_load) {
                return $collection->get();
            } else {
                return $collection;
            }
        } else {
            if($eager_load) {
                return $this->partnerHierarchyAdmins($query)->get();
            } else {
                return $this->partnerHierarchyAdmins($query);
            }
        }
    }

    /**
     * Re-sampled method to return memberships of all partners in hierarchy
     *
     * @return collection
     */
    public function partnerHierarchyMembers($query = null, $eager_load = false)
    {
        $ids = array();

        foreach($this->managedPartners() as $p) {
            if(!in_array($p->id, $ids)) {
                $ids[] = $p->id;
            }
        }

        $results = User::distinct('users.id')->select(
            'users.id',
            'users.email',
            'users.username',
            'users.verified',
            'users.title',
            'users.balance',
            'users.first_name',
            'users.middle_name',
            'users.last_name',
            'users.mobile',
            'users.normalized_mobile',
            'users.telephone_code',
            'users.profile_image',
            'users.status',
            'users.country_id',
            'users.city_id',
            'users.area_id'
        )->join('partner_user', 'users.id', '=', 'partner_user.user_id')
         ->whereIn('partner_user.partner_id', $ids);

        if(!empty($query)) {
            $query = explode(chr(32), $query);

            $results->where(function($q) use($query){
                foreach ($query as $key => $value) {
                    $q->where('first_name', 'LIKE', '%'. $value .'%');
                    $q->orWhere('last_name', 'LIKE', '%'. $value .'%');
                }
            });
        }


        unset($ids);

        return $results;
    }

    /**
     * Re-sampled method to return memberships of all partners in hierarchy
     *
     * @return collection
     */
    public function partnerHierarchyAdmins($query = null, $eager_load = false)
    {
        $ids = array();

        foreach($this->managedPartners() as $p) {
            if(!in_array($p->id, $ids)) {
                $ids[] = $p->id;
            }
        }

        $results = Admin::distinct('admins.id')->select(
            'admins.id',
            'admins.email',
            'admins.username',
            'admins.verified',
            'admins.title',
            'admins.first_name',
            'admins.middle_name',
            'admins.last_name',
            'admins.profile_image',
            'admins.status',
            'admins.country_id',
            'admins.city_id',
            'admins.area_id'
        )->join('partner_admin', 'admins.id', '=', 'partner_admin.admin_id')
         ->whereIn('partner_admin.partner_id', $ids);

        if(!empty($query)) {
            $query = explode(chr(32), $query);

            $results->where(function($q) use($query){
                foreach ($query as $key => $value) {
                    $q->where('first_name', 'LIKE', '%'. $value .'%');
                    $q->orWhere('last_name', 'LIKE', '%'. $value .'%');
                }
            });
        }


        unset($ids);

        return $results;
    }

    /**
     * Fetch a list of removed members managed by the current user
     *
     * @return Collection
     */
    public function managedRemovedMembers()
    {
        if(I::am('BLU Admin')) {
            return Admin::onlyTrashed()->where('draft', false)->orderBy('created_at');
        } else {
            return $this->managedProperty('removedMembers');
        }
    }

    /**
     * Fetch a list of removed members managed by the current user
     *
     * @return Collection
     */
    public function managedRemovedProducts()
    {
        if(I::am('BLU Admin')) {
            return Product::onlyTrashed()->where('draft', false)->orderBy('created_at');
        } else {
            return $this->managedProperty('removedProducts');
        }
    }

    /**
	 * Fetch a list of segments managed by the current user partner
	 *
	 * @return Collection
	 */
    public function managedSegments()
    {
    	if(I::am('BLU Admin')) {
    		return Segment::where('draft', false)->orderBy('name')->get();
    	} else {
            return $this->managedProperty('segments');
    	}
    }


    /**
     * Fetch a list of roles managed by the current user
     *
     * @return Collection
     */
    public function managedRoles()
    {
        if(I::am('BLU Admin')) {
            return Role::all();
        } else {
            return $this->managedProperty('roles');
        }
    }

    /**
     * Fetch a list of loyalty programs managed by the current user
     *
     * @return Collection
     */
    public function managedLoyalty()
    {
        if(I::am('BLU Admin')) {
            return Loyalty::where('draft', false)->orderBy('created_at')->get();
        } else {
            return $this->managedProperty('loyaltyPrograms');
        }
    }

    /**
     * Fetch a list of POS managed by the current user
     *
     * @return Collection
     */
    public function managedPointOfSales()
    {
        if(I::am('BLU Admin')) {
            $buffer = new BluCollection();

            foreach(Pos::orderBy('created_at')->get() as $pos) {
                $buffer->add($pos);
            }

            return $buffer;
        } else {
            $stores = $this->managedStores();
            $buffer = new BluCollection();

            foreach($stores as $store) {

                $storePoses = $store->pointOfSales;

                if($storePoses) {
                    foreach ($storePoses as $storePos) {
                        if(!$buffer->contains($storePos)) {
                            $buffer->add($storePos);
                        }
                    }
                }
            }

            return $buffer;
        }
    }

    /**
     * Fetch a list of banners managed by the current user
     *
     * @return Collection
     */
    public function managedBanners()
    {
        if(I::am('BLU Admin')) {
            return Banner::where('draft', false)->orderBy('created_at')->get();
        } else {
            return $this->managedProperty('allbanners');
        }
    }
    
    /**
     * Fetch a list of cardprint managed by the current user
     *
     * @return Collection
     */
    public function managedCardprint()
    {
        if(I::am('BLU Admin')) {
            return CardPrint::where('draft', false)->orderBy('created_at')->get();
        } else {
            return $this->managedProperty('allcardprint');
        }
    }
    
    /**
     * Fetch a list of Affiliates managed by the current user
     *
     * @return Collection
     */
    public function managedAffiliates()
    {
        if(I::am('BLU Admin')) {
            return Affiliate::where('draft', false)->orderBy('id')->get();
        } else {
            return $this->managedProperty('affiliates');
        }
    }
    
    /**
     * Fetch a list of Affiliates managed by the current user
     *
     * @return Collection
     */
    public function managedAffiliatePrograms()
    {
        if(I::am('BLU Admin')) {
            return AffiliateProgram::where('draft', false)->orderBy('id')->get();
        } else {
            return $this->managedProperty('affiliateprograms');
        }
    }


    /**
     * Return a list of only managed networks ID's
     *
     * @return array
     */
    public function managedNetworks()
    {
        if(I::am('BLU Admin')) {
            return $this->adminManagedNetworks();
    	} else {
            return $this->managedProperty('networks');
    	}
    }

    /**
     * Return list of networks connected to all partners of admin, including all child partners
     *
     * @return array|Collection
     */
    protected function adminManagedNetworks()
    {
        $adminPartners = array_flip($this->partners()->where('draft', false)->pluck('partner.id')->toArray());

        $allPartners = Partner::join('network_partner', 'partner.id', '=', 'partner_id')
            ->where('draft', false)->getQuery()
            ->get(['id', 'parent_id', 'network_id']);

        $networksIds = [];

        //collect all child partners and its networks
        foreach ($allPartners as $partner)
        {
            if (isset($adminPartners[$partner->id]) || isset($adminPartners[$partner->parent_id]))
            {
                $adminPartners[$partner->id] = 1;
                $networksIds[$partner->network_id] = 1;
            }
        }

        return $networksIds ? Network::whereIn('id', array_keys($networksIds))->where('draft', false)->orderBy('name')->get() : [];
    }

    public function partnersOfNetworkId($networkId)
    {
        if (!$network = Network::find($networkId))
            return collect();

        $networkPartners = array_flip($network->partners()->pluck('id')->toArray());
        $managedPartners = $this->managedPartners();

        foreach ($managedPartners as $key => $partner)
            if (!isset($networkPartners[$partner->id]))
                unset($managedPartners[$key]);

        return $managedPartners ?: collect();
    }

    # --------------------------------------------------------------------------
    #
    # Query Scopes
    #
    # --------------------------------------------------------------------------

    public function scopeWomen($query)
    {
        return $query->whereGender('f');
    }

    public function scopeMen($query)
    {
        return $query->whereGender('m');
    }

    public function scopeActive($query)
    {
        return $query->whereStatus('active');
    }

    public function scopeSuspended($query)
    {
        return $query->whereStatus('suspended');
    }

    public function scopeInactive($query)
    {
        return $query->whereStatus('inactive');
    }

    public function scopeVerified($query)
    {
        return $query->whereVerified(1);
    }

    public function scopeValid($query)
    {
        return $query->whereVerified(true)->whereDraft(false);
    }
    
    /**
     * Return the top most partner
     *
     * @return Result
     */
    public function getTopHierarchyPartnerIds()
    {
        $firstLevelChildren		= array();
        $highestLevelParents	= array();
        $sameLevelPartners		= array();

        foreach ($this->partners as $partner){
            $partners[] = $partner->id;
        }

        foreach ($partners as $partner_id){
            $partner	= Partner::find($partner_id);
            $partnerParent	= $partner->parent_id;
            if($partner->is_top_level_partner){
                return array($partner_id);
            }elseif(empty($partnerParent)){
                return array($partner_id);
            }else {
                if(in_array($partnerParent, $partners)){
                   $highestLevelParents[$partnerParent][] = $partner_id;
                }
                if($partnerParent == 1){
                        $firstLevelChildren[]	= $partner_id;
                }else{
                        $sameLevelPartners[]	= $partner_id;
                }
           }
        }

        if(!empty($firstLevelChildren)){
           sort($firstLevelChildren);
           return $firstLevelChildren;
        }
        elseif(!empty($highestLevelParents)){
           $highestParents	= array_keys($highestLevelParents);
           sort($highestParents);
           return $highestParents;
        }
        else{
           sort($sameLevelPartners);
           return $sameLevelPartners;
        }

    }
    public function getRememberToken()
    {
        return $this->remember_token;
    }

    public function setRememberToken($value)
    {
        $this->remember_token = $value;
    }

    public function getRememberTokenName()
    {
        return 'remember_token';
    }

    public function getAuthIdentifierName() {
        return "id";
    }

    public function getEmailForPasswordReset() {
        return $this->email;
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new MailResetPasswordToken($token));
    }

    public function getFullName()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function availableRoles()
    {
        return $this->managedRoles()->merge($this->roles)->unique();
    }

    public function canSeePartner($partnerId)
    {
        if ($this->roles->contains('name', 'BLU Admin')) {
            return true;
        }

        return $this->managedPartners()->contains('id', $partnerId);
    }


} // EOC