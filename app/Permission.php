<?php
namespace App;
use App\BluCollection;
use Eloquent;
/**
 * Permission Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class Permission extends Eloquent 
{
    public $table      = 'permission';
    public $timestamps = false;
    protected $guarded = array();

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------
    
    public function roles()
    {
        return $this->belongsToMany('App\Role');
    }

} // EOC