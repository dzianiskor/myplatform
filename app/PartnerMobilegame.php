<?php
namespace App;
use App\BluCollection;
use Eloquent;
/**
 * Partner Channel Model
 *
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class PartnerMobilegame extends Eloquent
{
    public $table = 'mobile_game_partner';

    # --------------------------------------------------------------------------
    #
    # Relationships
    #
    # --------------------------------------------------------------------------

    public function partner()
    {
    	return $this->belongsTo('App\Partner');
   	}

    public function mobilegames()
    {
    	return $this->belongsTo('App\MobileGame', 'mobile_game_id');
   	}

} // EOC