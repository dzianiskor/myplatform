<?php
namespace App;
use App\BluCollection;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Product Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Magid Mroueh <magid.mroueh@bluloyalty.com>
 * @copyright  2017 BLU
 * @link       http://blupoints.com
 */
class EmailWhitelabeling extends BaseModel 
{
    public $table         = 'email_whitelabeling';
} // EOC