<?php

/**
 * Area Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Rabih Sbaity
 * @copyright  2017 BLU
 * @link       http://blupoints.com
 */
class RewardTypes extends BaseModel {

    public $table = 'reward_types';

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------

    public function partner() {
        return $this->belongsTo('Partner');
    }
}

// EOC