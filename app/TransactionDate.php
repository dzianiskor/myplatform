<?php

namespace App;

class TransactionDate extends BaseModel
{
    public $table = 'transaction_dates';
    public $timestamps = false;
}
