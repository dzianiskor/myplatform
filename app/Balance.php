<?php
namespace App;
use App\BluCollection;
use Eloquent;
/**
 * Balance Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class Balance extends Eloquent 
{
    public $table = 'balance';

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------
    
    public function user() 
    { 
    	return $this->belongsTo('App\User'); 
    }

    public function network() 
    { 
    	return $this->belongsTo('App\Network'); 
    }

} // EOC