<?php
namespace App;
use App\BluCollection;
/**
 * Banner Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Magid Mroueh <magid.mroueh@bluloyalty.com>
 * @copyright  2016 BLU
 * @link       http://blupoints.com
 */
class RedemptionOrder extends BaseModel
{
    public $table   = 'redemption_order';
    
    # --------------------------------------------------------------------------
    #
    # Relationships
    #
    # --------------------------------------------------------------------------
    
    public function Transaction()
    {
        return $this->belongsTo('App\Transaction');
    }
    
    public function TransactionItem()
    {
        return $this->belongsTo('App\TransactionItem');
    }

    /**
     * functio to create a new redemption order
     * 
     * @param array $params
     * array(
     * trx_id => '',
     * trx_item_id => '',
     * item_id => '',
     * item_name => '',
     * item_model => '',
     * item_sub_model => '',
     * full_price_in_usd => '',
     * supplier_id => '',
     * notes => '',
     * passenger_name => '',
     * company => '',
     * start_booking_date => '',
     * start_booking_date => '',
     * end_booking_date => '',
     * airline_fees => '',
     * cost => '',
     * cost_currency => '',
     * delivery_cost => '',
     * delivery_cost_currency => '',
     * )
     * @param type $type
     */
    public static function createRedemptionOrder(array $params = NULL, $type = "Item")
    {
        if (is_array($params) && !empty($params)) {
            $redemptionTrx = new RedemptionOrder();
            $passengerName  = '';
            $company  = '';
            $bookingStartDate  = '';
            $bookingEndDate  = '';
            $airlineFees  = '';

            $trxId      = $params['trx_id'];
            $trxItemId  = $params['trx_item_id'];

            if($type == 'Item'){
                $itemId         = $params['product_id'];
                if(empty($params['product_name']) || empty($params['product_model']) || empty($params['product_sub_model'])){
                    if(empty($params['country_id']) || empty($params['channel_id'])){
                        $product = Product::find($params['product_id']);
                    }else{
                        $product = ProductsHelper::productRedemptionDetails($params['product_id'], $params['country_id'], $params['channel_id']);
                    }
                    $itemName       = $product->name;
                    $itemModel      = $product->model;
                    $itemSubModel   = $product->sub_model;
                }else{
                    $itemName       = $params['product_name'];
                    $itemModel      = $params['product_model'];
                    $itemSubModel   = $params['product_sub_model'];
                }
            }

            $fullPriceInUsd = $params['full_price_in_usd'];
            $supplierId     = $params['supplier_id'];

            if($type == 'Travel'){
                if(!empty($params['notes'])){
                    $notes          = $params['notes'];
                }
                $notesArray = json_decode($notes, True);
                switch ($params['trans_class']) {
                    case 'hotel':
                        $passengerName  = $notesArray['book_first_name'] . ' ' . $notesArray['book_last_name'];
                        $company  = $notesArray['hotelName'];
                        $bookingStartDate  = $notesArray['arrivalDate'];
                        $bookingEndDate  = $notesArray['departureDate'];
                        break;
                    case 'flight':
                        $user = User::find($params['user_id']);
                        $userName   = $user->first_name . ' ' . $user->last_name;
                        $passengerName  = $userName;
                        $company  = $notesArray['out_carrier_name'];
                        $bookingStartDate  = $notesArray['out_depart_time'];
                        $bookingEndDate  = 'N/A';
                        if(!empty($notesArray['in_arrive_time'])){
                            $bookingEndDate  = $notesArray['in_arrive_time'];
                        }
                        break;
                    case 'car':
                        $user = User::find($params['user_id']);
                        $userName   = $user->first_name . ' ' . $user->last_name;
                        $passengerName  = $userName;
                        $company  = $notesArray['pickup_location_name'];
                        $bookingStartDate  = $notesArray['pickup'];
                        $bookingEndDate  = $notesArray['dropoff'];
                        break;
                    default:
                        break;
                }
                if(!empty($params['passenger_name'])){
                    $passengerName  = $params['passenger_name'];
                }
                if(!empty($params['company'])){
                    $company  = $params['company'];
                }
                if(!empty($params['start_booking_date'])){
                    $bookingStartDate  = $params['start_booking_date'];
                }
                if(!empty($params['end_booking_date'])){
                    $bookingEndDate  = $params['end_booking_date'];
                }

                if(!empty($params['airline_fees'])){
                    $airlineFees  = $params['airline_fees'];
                }
            }

            $cost                   = $params['cost'];
            $costCurrency           = $params['cost_currency'];
            $deliveryCost           = $params['delivery_cost'];
            $deliveryCostCurrency   = $params['delivery_cost_currency'];
//        $paymentGatewayCost     = $params['payment_gateway_cost'];
            $qty                    = $params['quantity'];
            $userId                 = $params['user_id'];
            $cashPaid = 0;
            if(!empty($params['cash_paid'])){
                $cashPaid                = $params['cash_paid'];
            }
            $redemptionTrx->trx_id  = $trxId;
            $redemptionTrx->trx_item_id  = $trxItemId;
            $redemptionTrx->type  = $type;

            if($type == 'Item'){
                $redemptionTrx->product_id = $itemId;
                $redemptionTrx->product_name = $itemName;
                $redemptionTrx->product_model = $itemModel;
                $redemptionTrx->product_sub_model = $itemSubModel;
            }

            $redemptionTrx->full_price_in_usd   = $fullPriceInUsd;
            $redemptionTrx->supplier_id         = $supplierId;

            if($type == 'Travel'){
                $redemptionTrx->notes               = $notes;
                $redemptionTrx->passenger_name      = $passengerName;
                $redemptionTrx->company             = $company;
                $redemptionTrx->start_booking_date  = $bookingStartDate;
                $redemptionTrx->end_booking_date    = $bookingEndDate;
                $redemptionTrx->airline_fees        = $airlineFees;
            }

            $redemptionTrx->cost                    = $cost;
            $redemptionTrx->cost_currency           = $costCurrency;
            $redemptionTrx->delivery_cost           = $deliveryCost;
            $redemptionTrx->delivery_cost_currency  = $deliveryCostCurrency;
            $redemptionTrx->quantity                = $qty;
            $redemptionTrx->user_id                 = $userId;
            $redemptionTrx->cash_payment               = $cashPaid;
//        $redemptionTrx->payment_gateway_cost    = $paymentGatewayCost;

            $redemptionTrx->save();
        }
    }

    /**
     * function to reverse redemption order
     * 
     * @param array $params
     * array(
     * trx_id => '',
     * trx_item_id => '',
     * qty => '',
     * )
     * @param type $type
     */
    public static function reverseRedemptionOrder(array $params, $type = "Full") // types: Full, Item, Delivery
    {
        try{
            
        
        //APIController::postSendEmailJson(json_encode($params),'Reverse Redemption Order 001  ');
        $newRedemptionTrx = new RedemptionOrder();
        if($type == "Full"){
            $redemptionTrxs = RedemptionOrder::where('trx_id', $params['trx_id'])->get();
            
            foreach ($redemptionTrxs as $redemptionTrx) {
                $newRedemptionTrx = $redemptionTrx->replicate();
                
                $newRedemptionTrx->full_price_in_usd = $redemptionTrx->full_price_in_usd * (-1);
                $newRedemptionTrx->cost = $redemptionTrx->cost * (-1);
                $newRedemptionTrx->delivery_cost = $redemptionTrx->delivery_cost * (-1);
                $newRedemptionTrx->payment_gateway_cost = $redemptionTrx->payment_gateway_cost * (-1);
                $newRedemptionTrx->cash_payment = $redemptionTrx->cash_payment * (-1);
                $newRedemptionTrx->airline_fees = $redemptionTrx->airline_fees * (-1);
                $newRedemptionTrx->quantity = $redemptionTrx->quantity * (-1);
                $newRedemptionTrx->save();
                
                $redemptionTrx->reversed = 1;
                $redemptionTrx->reversed_item = 1;
                $redemptionTrx->reversed_delivery = 1;
                $redemptionTrx->reversed_at = date('Y-m-d');
                $redemptionTrx->save();
                
            }
        }elseif($type == "Item"){
            try {
                
            
            $redemptionTrxs = RedemptionOrder::where('trx_id', $params['trx_id'])->where('trx_item_id', $params['trx_item_id'])->get();
//            APIController::postSendEmailJson(json_encode($params),'Reverse Redemption Order 002  ');
            foreach ($redemptionTrxs as $redemptionTrx) {
                $priceByItemInUsd = $redemptionTrx->full_price_in_usd / $redemptionTrx->quantity;
                $costByItem = $redemptionTrx->cost / $redemptionTrx->quantity;
                $paymentGatewayCostByItem = $redemptionTrx->payment_gateway_cost / $redemptionTrx->quantity;
                $cashPaymentByItem = $redemptionTrx->cash_payment / $redemptionTrx->quantity;
                $airlineFeesByItem= $redemptionTrx->airline_fees / $redemptionTrx->quantity;
//                $json_data = json_encode(array($redemptionTrx->toArray()));
//                APIController::postSendEmailJson($json_data, "DEV Redemption ORDER 001 ");
                $newRedemptionTrx = $redemptionTrx->replicate();
                
                $newRedemptionTrx->trx_item_id = $params['trx_reversed_item_id'];
                $newRedemptionTrx->trx_id = $params['reversal_trx_id'];
                $newRedemptionTrx->full_price_in_usd = $params['full_price_in_usd'];
                $newRedemptionTrx->cost = $costByItem * $params['qty'] * (-1);
                $newRedemptionTrx->delivery_cost = 0;
                $newRedemptionTrx->reversed_delivery = 0;
                $newRedemptionTrx->payment_gateway_cost = $paymentGatewayCostByItem * $params['qty'] * (-1);
                $newRedemptionTrx->cash_payment = $cashPaymentByItem * $params['qty'] * (-1);
                $newRedemptionTrx->airline_fees = $airlineFeesByItem * $params['qty'] * (-1);
                $newRedemptionTrx->quantity = $params['qty'] * (-1);
                $newRedemptionTrx->save();
//                $json_data = json_encode(array($newRedemptionTrx->toArray()));
//                APIController::postSendEmailJson($json_data, "DEV Redemption ORDER 002 ");
                $redemptionTrx->reversed_item = 1;
                $redemptionTrx->reversed_at = date('Y-m-d');
                $redemptionTrx->save();
                
            }
            } catch (Exception $ex) {
                $json_data = json_encode(array($ex->getMessage(), $ex->getLine()));
                APIController::postSendEmailJson($json_data, "DEV automate cashback Try Catch 001 ");
            }
            //APIController::postSendEmailJson(json_encode($params),'Reverse Redemption Order 003  ');
        }elseif($type == "Delivery"){
            $redemptionTrxs = RedemptionOrder::where('trx_id', $params['trx_id'])->where('trx_item_id', $params['trx_item_id'])->get();
            //APIController::postSendEmailJson(json_encode($params),'Reverse Redemption Order 004  ');
            
            foreach ($redemptionTrxs as $redemptionTrx) {
                $product_temp = Product::find($redemptionTrx->product_id);
                if($product_temp->is_voucher == 1){
                    $deliveryCostByItem = $redemptionTrx->delivery_cost;
                }
                else{
                    $deliveryCostByItem = $redemptionTrx->delivery_cost / $redemptionTrx->quantity;
                }
                
                
                $newRedemptionTrx = $redemptionTrx->replicate();
                $newRedemptionTrx->trx_id = $params['reversal_trx_id'];
                $newRedemptionTrx->full_price_in_usd = $params['full_price_in_usd'];
                $newRedemptionTrx->cost = 0;
                $newRedemptionTrx->delivery_cost = $deliveryCostByItem * $params['qty'] * (-1);
                $newRedemptionTrx->payment_gateway_cost = 0;
                $newRedemptionTrx->cash_payment = 0;
                $newRedemptionTrx->airline_fees = 0;
                $newRedemptionTrx->quantity = 0;
                $newRedemptionTrx->save();
                
                $redemptionTrx->reversed_delivery = 1;
                $redemptionTrx->reversed_at = date('Y-m-d');
                $redemptionTrx->save();
            }
            //APIController::postSendEmailJson(json_encode($params),'Reverse Redemption Order 005  ');
        }
        
            return $newRedemptionTrx;
        } catch (Exception $ex) {
            $json_data = json_encode(array($ex->getMessage(), $ex->getLine()));
            APIController::postSendEmailJson($json_data, "DEV automate cashback Try Catch 001 ");
        }
    }
}// EOC
