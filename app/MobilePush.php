<?php
namespace App;
use App\BluCollection;
/**
 * Mobile Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class MobilePush extends BaseModel
{
    public $table = 'mobile_push_notification';
    public function image()
    {
        $image = $this->belongsTo('App\Media', 'media_id');
        $image = $image->first();

        return $image;
    }
} // EOC