<?php
namespace App;
use App\BluCollection;
use Eloquent;
/**
 * Reference Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class MediaInvoice extends Eloquent 
{
    public $table = 'media_invoice';

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------

    public function invoice() 
    {
        return $this->belongsTo('App\UserInvoice');
    }   
         
} // EOC