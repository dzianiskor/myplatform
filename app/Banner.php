<?php
namespace App;
use App\BluCollection;
/**
 * Banner Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class Banner extends BaseModel
{
	public $table = 'banner';
	
    public function partner()
    {
        return $this->belongsTo('App\Partner');
    }
    public function language()
    {
        return $this->belongsTo('App\Language','lang_id');
    }

    public function image()
    {
        $image = $this->belongsTo('App\Media', 'media_id');
        $image = $image->first();

        return $image;
    }
    
    public function mobimage()
    {
        $mobimage = $this->belongsTo('App\Media', 'mob_media_id');
        $mobimage = $mobimage->first();

        return $mobimage;
    }
	
    public function countries()
    { 
        return $this->hasMany('App\BannerCountry', 'banner_id')->with('Country');
    }

    public function pages()
    { 
        return $this->hasMany('App\BannerPage', 'banner_id');
    }
    
    public function segments()
    { 
        return $this->hasMany('App\BannerSegment', 'banner_id')->with('Segment');
    }

} // EOC