<?php
namespace App;
use App\BluCollection;
/**
 * Batch Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class Batch extends BaseModel 
{
    public $table      = 'batch';
    protected $guarded = array();

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------

    public function coupons() 
    { 
        return $this->hasMany('App\Coupon');    
    }

    public function cards() 
    { 
        return $this->hasMany('App\Card');      
    }    

    public function references() 
    { 
        return $this->hasMany('App\Reference'); 
    } 

    public function partner() 
    { 
        return $this->BelongsTo('App\Partner'); 
    }

    public function country() 
    { 
        return $this->BelongsTo('App\Country'); 
    }

    public function segment() 
    { 
        return $this->BelongsTo('App\Segment'); 
    }

    # --------------------------------------------------------------------------
    # 
    # Attributes
    # 
    # --------------------------------------------------------------------------
    
    public function redeemedCoupons() 
    {
        return $this->coupons()->where('transaction_id', '!=', 'null');
    }

} // EOC