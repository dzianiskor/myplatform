<?php
namespace App;
use App\BluCollection;
/**
 * Partner Category Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class PartnerCategory extends BaseModel 
{
	public $table      = 'partnercategory';
	protected $guarded = array();

} // EOC