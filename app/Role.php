<?php
namespace App;
use App\BluCollection;
use Eloquent;

/**
 * Role Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class Role extends Eloquent 
{
    public $table      = 'role';
    public $timestamps = false;
    protected $guarded = array();

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------
    
    public function users()
    {
        return $this->belongsToMany('App\User');
    }

    public function partner()
    {
        return $this->belongsTo('App\Partner');
    }

    public function permissions()
    {
        return $this->belongsToMany('App\Permission');
    }

} // EOC