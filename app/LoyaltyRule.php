<?php
namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;
/**
 * Loyalty Rule Model
 *
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class LoyaltyRule extends BaseModel
{
    use SoftDeletes;

    public $table = 'loyalty_rule';
    protected $dates = ['deleted_at'];
    protected $softDelete = true;

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------

    public function loyalty_program()
    {
        return $this->belongsTo('App\Loyalty');
    }

    public function exception_rules()
    {
        return $this->hasMany('App\LoyaltyExceptionrule');
    }

} // EOC