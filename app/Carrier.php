<?php
namespace App;
use App\BluCollection;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Product Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class Carrier extends BaseModel 
{
    protected $table = 'carriers';
    protected $fillable = [
        'name',
        'alternative_name',
        'iata_code',
        'icao_code',
        'callsign',
        'country',
        'is_active',
    ];
} // EOC