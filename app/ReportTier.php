<?php
namespace App;
use App\BluCollection;
/**
 * Report Tier Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class ReportTier extends BaseModel 
{
    public $table = 'report_tier';
    
} // EOC