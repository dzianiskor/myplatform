<?php
namespace App;
use App\BluCollection;
use Eloquent;
/**
 * Store Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class Prodmodeltranslation extends Eloquent 
{
    public $table = 'prodmodel_translation';

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------
    
    public function prodmodel()
    {
        return $this->belongsTo('App\Prodmodel');
    }

    

    # --------------------------------------------------------------------------
    # 
    # Helpers
    # 
    # --------------------------------------------------------------------------
    
    /**
     * Return a list of stores for the specified partner
     * 
     * @param int $prodmodel_id
     * @return array
     */
    public static function prodmodelTranslations($prodmodel_id)     
    { 
        return Prodmodeltranslation::where('prodmodel_id', $prodmodel_id)->orderBy('name')->get();
    }

} // EOC