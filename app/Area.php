<?php
namespace App;
use App\BluCollection;
use Eloquent;
/**
 * Area Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class Area extends Eloquent 
{
    public $table      = 'area';
    public $timestamps = false;

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------
    
    public function country() 
    { 
    	return $this->belongsTo('App\Country'); 
    }

    public function cities() 
    { 
    	return $this->hasMany('App\City');      
    }

} // EOC