<?php
namespace App;
use App\BluCollection;
use Eloquent;
/**
 * Store Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class Prodtranslation extends BaseModel
{
    public $table = 'product_translation';

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------
    
    public function product()
    {
        return $this->belongsTo('App\Product');
    }

    

    # --------------------------------------------------------------------------
    # 
    # Helpers
    # 
    # --------------------------------------------------------------------------
    
    /**
     * Return a list of stores for the specified partner
     * 
     * @param int $product_id
     * @return array
     */
    public static function productTranslations($product_id)     
    { 
        return Prodtranslation::where('product_id', $product_id)->orderBy('name')->get();
    }

} // EOC