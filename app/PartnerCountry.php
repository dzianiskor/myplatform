<?php
namespace App;
use App\BluCollection;
use Eloquent;
/**
 * Partner Channel Model
 *
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class PartnerCountry extends Eloquent
{
    public $table = 'country_partner';

    # --------------------------------------------------------------------------
    #
    # Relationships
    #
    # --------------------------------------------------------------------------

    public function partner()
    {
    	return $this->belongsTo('App\Partner');
   	}

    public function countries()
    {
    	return $this->belongsTo('App\Country', 'country_id');
   	}

} // EOC