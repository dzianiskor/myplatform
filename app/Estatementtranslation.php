<?php
namespace App;
use App\BluCollection;
use Eloquent;
/**
 * Store Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Magid Mroueh <magid.mroueh@bluloyalty.com>
 * @copyright  2016 BLU
 * @link       http://blupoints.com
 */
class Estatementtranslation extends Eloquent 
{
    public $table = 'estatement_translation';

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------
    
    public function estatement()
    {
        return $this->belongsTo('App\Estatement');
    }

    

    # --------------------------------------------------------------------------
    # 
    # Helpers
    # 
    # --------------------------------------------------------------------------
    
    /**
     * Return a list of stores for the specified partner
     * 
     * @param int $estatement_id
     * @return array
     */
    public static function estatementTranslations($estatement_id)     
    { 
        return Estatementtranslation::where('estatement_id', $estatement_id)->orderBy('name')->get();
    }

    public function lang()
    {
        return $this->belongsTo('App\Language', 'lang_id');
    }

} // EOC