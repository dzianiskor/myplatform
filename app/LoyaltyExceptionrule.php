<?php
namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Loyalty Rule Model
 *
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class LoyaltyExceptionrule extends BaseModel
{
    use SoftDeletes;

    public $table = 'loyalty_exceptionrule';

    protected $dates = ['deleted_at'];

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------

    public function loyalty_rule_program()
    {
        return $this->belongsTo('App\LoyaltyRule', 'loyalty_rule_id');
    }

} // EOC