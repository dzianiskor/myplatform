<?php
namespace App;
use App\BluCollection;
use Eloquent;
/**
 * Store Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class Store extends Eloquent 
{
    public $table = 'store';

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------
    
    public function partner()
    {
        return $this->belongsTo('App\Partner');
    }

    public function address()
    {
        return $this->hasOne('App\Address', 'mapping_key');
    }

    public function pointOfSales()
    {
        return $this->hasMany('App\Pos');
    }

    public function transactions()
    {
        return $this->hasMany('App\Transaction');
    }

    # --------------------------------------------------------------------------
    # 
    # Helpers
    # 
    # --------------------------------------------------------------------------
    
    /**
     * Return a list of stores for the specified partner
     * 
     * @param int $partner_id
     * @return array
     */
    public static function partnerStores($partner_id)     
    { 
        return Store::where('partner_id', $partner_id)->orderBy('name')->get();
    }

} // EOC