<?php
namespace App;
use App\BluCollection;
use Eloquent;
/**
 * Product Channel Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class ProductPartnerReward extends Eloquent 
{
    public $table = 'product_partner_reward';
    public $timestamps = false;

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------

    public function partner() 
    { 
        return $this->belongsTo('App\Partner', 'partner_id'); 
    }        

} // EOC