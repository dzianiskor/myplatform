<?php
namespace App;
use App\BluCollection;
use Eloquent;
/**
 * Product Channel Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class ProductRedemptionCountries extends Eloquent 
{
    public $table = 'product_redemption_countries';
    public $timestamps = false;

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------

    public function country() 
    { 
        return $this->belongsTo('App\Country', 'country_id'); 
    }        

} // EOC