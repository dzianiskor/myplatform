<?php
namespace App;
use App\BluCollection;
/**
 * Currency Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class Currency extends BaseModel {

    public $table = 'currency';

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------    

    public function latestPrice() {
        return $this->latestPriceRelation;
    }

    public function latestRate() {
        return $this->latestPrice()->rate;
    }

    public function latestPriceByDate($date) {
        return $this->prices->where('date', $date)->first();
    }

    public function rateByDate($date) {
        if (empty($this->latestPriceByDate($date))) {
            return 1;
        }
        return $this->latestPriceByDate($date)->rate;
    }

    public function currencyTranslations() {
        return $this->hasMany('App\CurrencyTranslation')->where('draft', false);
    }

    public static function getDefault()
    {
        return self::where('short_code', 'USD')->first();
    }

    public function prices()
    {
        return $this->hasMany('App\CurrencyPricing');
    }

    public function latestPrices()
    {
        return $this->hasMany('App\CurrencyPricing')->latest();
    }

    public function latestPriceRelation() {
        return $this->hasOne('App\CurrencyPricing')->latest();
    }

}

// EOC