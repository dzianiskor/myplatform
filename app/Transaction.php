<?php

namespace App;

use App\Http\Controllers\APIController;
use App\Http\Controllers\TempTransactionController;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use BluCollection;
use AuditHelper;
use ProductsHelper;
use CurrencyHelper;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\App;
use Input;
use Session;
use Request;
use File;
use Eloquent;
use I;
use Meta;
use InputHelper;
use SMSHelper;
use Validator;
use PointsHelper;
use Illuminate\Support\Facades\Config;
use App\Http\Controllers\NotificationController as NotificationController;

/**
 * Transaction Model
 *
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class Transaction extends Eloquent {

    const TRANS_SUCCESS = 200;
    const TRANS_FAILED = 500;
    const REWARD_TRANS = 'reward';
    const REDEEM_TRANS = 'redeem';
    const REVERSE_TRANS = 'reverse';

    public $table = 'transaction';
    private static $notify = true;

    # --------------------------------------------------------------------------
    #
    # Relationships
    #
    # --------------------------------------------------------------------------

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function store() {
        return $this->belongsTo('App\Store');
    }

    public function partner() {
        return $this->belongsTo('App\Partner');
    }

    public function network() {
        return $this->belongsTo('App\Network');
    }

    public function coupon() {
        return $this->belongsTo('App\Coupon');
    }

    public function references()
    {
        return $this->hasMany(Reference::class, 'user_id', 'user_id');
    }

    public function loyalty() {
        return $this->belongsTo('App\Loyalty', 'rule_id');
    }

    public function loyaltyexception() {
        return $this->belongsTo('App\LoyaltyRule', 'loyalty_exception_id');
    }

    public function items() {
        return $this->hasMany('App\TransactionItem');
    }

    public function currency() {
        return $this->belongsTo('App\Currency', 'currency_id');
    }

    public function country() {
        return $this->belongsTo('App\Country', 'country_id');
    }

    public function category() {
        return $this->belongsTo('App\Category', 'category_id');
    }

    public function pos() {
        return $this->belongsTo('App\Pos', 'pos_id');
    }

    public function staff() {
        return $this->belongsTo('App\Admin', 'auth_staff_id');
    }

    public function staffRole() {
        return $this->belongsTo('App\Role', 'auth_role_id');
    }

    public function segment() {
        return $this->belongsTo('App\Segment', 'segment_id');
    }

    # --------------------------------------------------------------------------
    #
    # Transaction Logic
    #
    # --------------------------------------------------------------------------

    /**
     * Generic response handler for calling transactions
     *
     * @param int $status
     * @param string $message
     * @param object $transaction
     * @return object
     */
    private static function respondWith($status, $message, $transaction) {
        $response = new \stdClass();

        $response->status = $status;
        $response->message = $message;
        $response->transaction = $transaction;

        if ($status == Transaction::TRANS_FAILED) {
            Log::warning($message);
        }

        return $response;
    }

    /**
     * Fetch and return a valid stff member only
     *
     * @param int $user_id
     * @return mixed
     */
    private static function validStaffMember($user_id) {
        if (empty($user_id)) {
            return false;
        }

        $staff_member = User::where('id', $user_id)->where('status', 'active')->first();

        if (!$staff_member) {
            return false;
        }

        // Can't be a regular member
        if ($staff_member->roles()->where('role_id', 4)->count() == 0) {
            return false;
        }

        // Can't have no roles
        if ($staff_member->roles()->count() == 0) {
            return false;
        }

        return $staff_member;
    }

    /**
     * Perform the reward transaction
     *
     * Item Transaction:
     *
     *   $params = array(
     *       'user_id'   => 1,
     *       'partner'   => 1,
     *       'items'     => array(stdClass('id', 'qty')),
     *       'source'    => '', // Optional
     *       'reference' => '', // Optional
     *       'notes'     => '', // Optional
     *       'store_id'  => '', // Optional
     *       'pos_id'    => '', // Optional
     *       'staff_id'  => ''  // Optional
     *   );
     *
     *   Transaction::doReward($params, 'Items');
     *
     * Amount Transaction:
     *
     *   $params = array(
     *       'user_id'   => 1,
     *       'partner'   => 1,
     *       'amount'    => 100,
     *       'currency'  => 'USD'
     *       'source'    => '', // Optional
     *       'reference' => '', // Optional
     *       'notes'     => '', // Optional
     *       'store_id'  => '', // Optional
     *       'pos_id'    => '', // Optional
     *       'staff_id'  => ''  // Optional
     *   );
     *
     *   Transaction::doReward($params, 'Amount');
     *
     * Points Transaction:
     *
     *   $params = array(
     *       'user_id'   => 1,
     *       'partner'   => 1,
     *       'points'    => 1000,
     *       'source'    => '', // Optional
     *       'reference' => '', // Optional
     *       'notes'     => '', // Optional
     *       'store_id'  => '', // Optional
     *       'pos_id'    => '', // Optional
     *       'staff_id'  => ''  // Optional
     *   );
     *
     *   Transaction::doReward($params, 'Amount');
     *
     * @param array $params
     * @param string $trans_class (Items, Amount)
     * @return void
     */
    public static function doReward(array $params, $trans_class = "Items") {
        $validator = Validator::make(
                        $params, array(
                    'user_id' => 'required|integer'
                        )
        );

        if ($validator->fails()) {
            return self::respondWith(
                            Transaction::TRANS_FAILED, "Validation failed, ensure that all required fields are present", null
            );
        }

        DB::beginTransaction();

        $transaction = new Transaction();
        $transaction->trx_reward = 1;
        $transaction->trx_redeem = 0;
        $transaction->trans_class = $trans_class;

        $user = User::where('id', $params['user_id'])->first();

        if (!$user) {
            return self::respondWith(
                            Transaction::TRANS_FAILED, "Transaction aborted: User '{$params['user_id']}' does not exist.", null
            );
        }

        // only active and suspended users can be rewarded FS427 <fboukarroum>
        if ($user->status != 'active' && $user->status != 'suspended' && $user->status != 'inactive') {
            return self::respondWith(
                            Transaction::TRANS_FAILED, "Transaction aborted: User account is " . $user->status, null
            );
        }
        // Fetch partner data

        if (!isset($params['partner'])) {
            $params['partner'] = $user->getTopLevelPartner();
        } else {
            $params['partner'] = Partner::find($params['partner']);
        }

//		$networkId	= $params['network_id'];
        if (empty($params['network_id'])) {
            $params['network'] = $params['partner']->firstNetwork();
        } else {
            $params['network'] = Network::find($params['network_id']);
        }
        if ($params['network']->status != 'Active') {
            return self::respondWith(
                            Transaction::TRANS_FAILED, "Transaction aborted: Network '{$params['network']->name}' is not currently active.", null
            );
        }
        if (isset($params['created_at'])) {
            $transaction->created_at = $params['created_at'];
        }
        if (isset($params['updated_at'])) {
            $transaction->updated_at = $params['updated_at'];
        }
        if (isset($params['posted_at'])) {
            $transaction->posted_at = $params['posted_at'];
        } else {
            $transaction->posted_at = date('Y-m-d H:i:s');
        }
        $transaction->quantity = 0;
        $transaction->trans_class = $trans_class;
        $transaction->user_id = array_get($params, 'user_id');
        $transaction->country_id = $user->country_id;
        $transaction->partner_id = $params['partner']->id;
        $transaction->source = array_get($params, 'source');
        $transaction->ref_number = array_get($params, 'reference');
        $transaction->notes = array_get($params, 'notes');
        $transaction->store_id = array_get($params, 'store_id');
        $transaction->pos_id = array_get($params, 'pos_id');
        $transaction->auth_staff_id = array_get($params, 'staff_id');
        $transaction->points_rewarded = 0;
        $transaction->rule_id = null;
        $transaction->network_id = $params['network']->id;
        $transaction->points_balance = $user->balance($transaction->network_id);
        if (isset($params['product_id'])) {
            $transaction->product_id = $params['product_id'];
        }
        $transaction->save();

        // Item only reward transaction

        if (isset($params['items'])) {

            $params['currency'] = Currency::where('short_code', 'USD')->first();

            $transaction->currency_id = $params['currency']->id;
            $transaction->partner_currency_id = $params['currency']->id;
            $transaction->exchange_rate = $params['currency']->latestPrice()->rate;

            foreach ($params['items'] as $product) {

                $p = Product::find($product->id);

                if ($p) {
                    if (!isset($product->qty)) {
                        $product->qty = 1;
                    }

                    $originalPriceInNetworkCurrency = $p->original_price;
//                    if($p->currency_id != $transaction->currency_id){
//                        $originalPriceInNetworkCurrency = $p->price_in_usd * $transaction->exchange_rate;
//                    }
//                    $originalPriceInNetworkCurrency		= $produsctOriginalPrice * $transaction->exchange_rate;
                    $originalPrice = $originalPriceInNetworkCurrency + ($p->original_sales_tax * $originalPriceInNetworkCurrency / 100);
                    $PriceInUsd = $p->price_in_usd + ($p->original_sales_tax * $p->price_in_usd / 100);
                    $item = new TransactionItem();

                    $item->total_amount = ($PriceInUsd * $product->qty);
                    $item->original_total_amount = ($originalPrice * $product->qty);
                    $item->currency_id = $p->currency_id;
                    $item->transaction_id = $transaction->id;
                    $item->product_id = $p->id;
                    $item->product_model = $p->model;
                    $item->product_brand_id = $p->brand_id;
                    $item->product_sub_model = $p->sub_model;
                    $item->product_category_id = $p->category_id;
                    $item->delivery_option = $p->delivery_options;
                    $item->quantity = $product->qty;

                    $item->save();

                    $transaction->total_amount += ($PriceInUsd * $product->qty);
                }
            }

            $transaction->quantity = count($params['items']);
            $transaction->original_total_amount = floatval($transaction->total_amount);
            $transaction->partner_amount = floatval($transaction->total_amount);
            $transaction->total_amount = (float) ($transaction->total_amount / $transaction->exchange_rate);
        }

        // Amount only reward transaction

        if (isset($params['amount'])) {
            $params['currency'] = Currency::where('short_code', array_get($params, 'currency'))->first();

            if (!$params['currency']) {
                return self::respondWith(
                                Transaction::TRANS_FAILED, "Transaction aborted: Currency chosen does not exist.", null
                );
            }

            $transaction->currency_id = $params['currency']->id;
            $transaction->partner_currency_id = $params['currency']->id;
            $transaction->exchange_rate = $params['currency']->latestPrice()->rate;
            $transaction->total_amount = (float) (array_get($params, 'amount') / $transaction->exchange_rate);
            $transaction->original_total_amount = floatval(array_get($params, 'amount'));
            $transaction->partner_amount = floatval(array_get($params, 'amount'));
        }

        if (isset($params['points']) && !isset($params['amount'])) {
            $params['currency'] = Currency::where('short_code', 'USD')->first();
            $transaction->currency_id = $params['currency']->id;
            $transaction->partner_currency_id = $params['currency']->id;
            $transaction->exchange_rate = $params['currency']->latestPrice()->rate;

            $transaction->total_amount = PointsHelper::pointsToAmount($params['points'], 'USD', $params['network']->id);
            $transaction->points_rewarded = round($params['points']);
        } { // if transfer set total amount to zero
            if (isset($params['type']) && $params['type'] == 'transfer') {
                $transaction->total_amount = 0;
            }
        } { // if bonus set total amount to zero
            if (isset($params['type']) && ($params['type'] == 'bonus' || $params['type'] == 'bonus_on_registration')) {
                $transaction->total_amount = 0;
                $transaction->original_total_amount = 0;
                $transaction->partner_amount = 0;
                $transaction->points_rewarded = $params['points'];
            }
        }
        // Process coupons

        if (!empty($params['coupon_code'])) {
            $coupon = App\Coupon::where('coupon_code', $params['coupon_code'])->first();
            $couponType = $coupon->coupon_type;
            $couponDetails = CouponHelper::CouponPrice($params['coupon_code'], $transaction->total_amount, $transaction->partner_id);
            $couponPrice = $couponDetails['price'];
            $couponCurrencyId = $couponDetails['currency'];
            $couponSegment = $coupon->segmentId();
            if (!empty($couponCurrencyId)) {
                $couponCurrencyDetails = Currency::where('id', $couponCurrencyId)->first();
                $couponPriceInUsd = $couponPrice / $couponCurrencyDetails->latestPrice()->rate;
                $couponCurrencyShortCode = $couponCurrencyDetails->short_code;
            } else {
                $couponPriceInUsd = $couponPrice;
                $couponCurrencyShortCode = 'USD';
            }

            $coupnValidFrom = $coupon->valid_from();
            $coupnValidTo = $coupon->valid_to();
            $couponPartner = $coupon->partner_id;
            $currentDate = date('Y-m-d');
            if (strtotime($coupnValidFrom) <= strtotime($currentDate) && strtotime($currentDate) <= strtotime($coupnValidTo)) {
                ;
            } else {
                DB::rollback();

                return self::respondWith(
                                Transaction::TRANS_FAILED, "The coupon is Expired!", $transaction
                );
            }

            if ($couponPartner != $transaction->partner_id) {
                DB::rollback();

                return self::respondWith(
//						500, "This coupon is issued by " . Partner::find($couponPartner)->name, $transaction
                                500, "This coupon is issued by another partner", $transaction
                );
            }

            //validate segment
            $user = User::find($transaction->user_id);
            foreach ($user->segments as $segment) {
                $userSegments[] = $segment->id;
            }

            if ($couponSegment > 0 && !in_array($couponSegment, $userSegments)) {
                return self::respondWith(
                                500, "The user is not in the corresponding segment. ", $transaction
                );
            }

            if (empty($coupon->transaction_id)) {
//				if($couponType == 'value'){
////					$couponPriceInPoints		= PointsHelper::amountToPoints($couponPrice, $couponCurrencyShortCode,$params['network']->id);
//					$user->coupon_price			= $couponPrice;
//					$transaction->points_rewarded	= ($transaction->points_rewarded - $couponPrice);
//				}
//				else{
//					$couponActualPrice			= ( $params['amount'] * $couponPriceInUsd ) / 100;
////					$couponPriceInPoints		= PointsHelper::amountToPoints($couponActualPrice,$params['currency'],$params['network']->id);
//					$user->coupon_price			= $couponActualPrice;
//					$transaction->points_rewarded	= $transaction->points_rewarded - $couponActualPrice;
//				}
//
                $transaction->coupon_id = (int) $coupon->id;
//
//                if($transaction->points_rewarded < 0) {
//                    $transaction->points_rewarded = 0;
//                }
                // Assign the coupon
                DB::table('coupon')->where('coupon_code', $params['coupon_code'])->update(array(
                    'transaction_id' => $transaction->id
                ));
            } else {
                DB::rollback();

                return self::respondWith(
                                500, "The coupon is already used for another transaction!", $transaction
                );
            }
        }

        // Process the loyalty programs against this transaction for amount or items, not points
        $bonus_pts_on_registration = 'reward';
        if (isset($params['type']) && $params['type'] == 'bonus_on_registration') {
            $bonus_pts_on_registration = 'bonus_on_registration';
        }
//        $sendgrid1 = new SendGrid(
//                        Config::get('sendgrid.username'), Config::get('sendgrid.password')
//                    );
//                    $arr_email1 = array();
//                    $arr_email1['params'] = json_encode($transaction->toArray());
//                    $mail1 = new SendGrid\Email();
//
//                    $mail1->setTos(array('adminit@bluloyalty.com'));
//                    $mail1->setFrom(Config::get('blu.email_from'));
//                                $mail1->setFromName('BLU Points');
//                    $mail1->setSubject("Bonus points on registration 1  ". $transaction->id);
//                    $mail1->setHtml(json_encode($arr_email1));
//
//                    $sendgrid1->send($mail1);
        if (!isset($params['points'])) {
            if ($bonus_pts_on_registration !== 'bonus_on_registration') {
                //APIController::postSendEmailJson(json_encode(array($transaction->partner_id)),"params doReward 002");
                $transaction = Loyalty::processMatchingRewardExceptionRule($transaction, $bonus_pts_on_registration, true);
            }
        }

        // Set the transaction location

        if ($transaction->store_id) {

            $params['store'] = Store::find($transaction->store_id);

            if ($params['store']) {
                if ($params['store']->address) {
                    $transaction->country_id = $params['store']->address->country_id;
                } else {
                    $transaction->country_id = $user->country_id;
                }
            }
        }

        // Assign staff member

        if ($transaction->auth_staff_id) {
            $staff_member = self::validStaffMember($transaction->auth_staff_id);

            if ($staff_member) {
                $transaction->auth_staff_id = $staff_member->id;
                $transaction->auth_role_id = $staff_member->roles()->first()->id;
            }
        }

        //get network currency
        $networkCurrencyId = $params['network']->currency_id;
        $networkCurrency = Currency::where('id', $networkCurrencyId)->first();
        $networkCurrencyShortCode = $networkCurrency->short_code;
        $transaction->amt_reward = PointsHelper::pointsToAmount($transaction->points_rewarded, $networkCurrencyShortCode, $params['network']->id);

        $transaction->save();

        // Update User Balance

        $user->updateBalance($transaction->points_rewarded, $transaction->network_id, 'add');
        DB::commit();
        // Send notifications
        $outputRalph = '';
        if ($transaction->points_rewarded > 0) {
            // commented manually
///removed for FNB testing////////////////////////////////////////////////////////////////////////////////////////////////////

            if (isset($params['type']) && ($params['type'] == 'bonus' || $params['type'] == 'bonus_on_registration' && $params['partner']->id === 4206)) {
                // Don't send email if its a bonus on registration for BSF
            } else {
                $outputRalph = self::sendNotifications($user, $transaction, Transaction::REWARD_TRANS);
            }

        }

        return self::respondWith(
                        Transaction::TRANS_SUCCESS, $outputRalph, $transaction->toArray()
        );
    }

    public static function doRewardNewLoyalty(array $params, $trans_class = "Items") {
        $validator = Validator::make(
                        $params, array(
                    'user_id' => 'required|integer'
                        )
        );

        if ($validator->fails()) {
            return self::respondWith(
                            Transaction::TRANS_FAILED, "Validation failed, ensure that all required fields are present", null
            );
        }

        DB::beginTransaction();

        $transaction = new Transaction();
        $transaction->trx_reward = 1;
        $transaction->trx_redeem = 0;
        $transaction->trans_class = $trans_class;

        $user = User::where('id', $params['user_id'])->first();

        if (!$user) {
            return self::respondWith(
                            Transaction::TRANS_FAILED, "Transaction aborted: User '{$params['user_id']}' does not exist.", null
            );
        }

        // only active and suspended users can be rewarded FS427 <fboukarroum>
        if ($user->status != 'active' && $user->status != 'suspended' && $user->status != 'inactive') {

            return self::respondWith(
                            Transaction::TRANS_FAILED, "Transaction aborted: User account is " . $user->status, null
            );
        }
        // Fetch partner data

        if (!isset($params['partner'])) {
            $params['partner'] = $user->getTopLevelPartner();
        } else {
            $params['partner'] = Partner::find($params['partner']);
        }

//		$networkId	= $params['network_id'];
        if (empty($params['network_id'])) {
            $params['network'] = $params['partner']->firstNetwork();
        } else {
            $params['network'] = Network::find($params['network_id']);
        }
        if ($params['network']->status != 'Active') {
            return self::respondWith(
                            Transaction::TRANS_FAILED, "Transaction aborted: Network '{$params['network']->name}' is not currently active.", null
            );
        }
        if (isset($params['created_at'])) {
            $transaction->created_at = $params['created_at'];
        }
        if (isset($params['updated_at'])) {
            $transaction->updated_at = $params['updated_at'];
        }
        if (isset($params['posted_at'])) {
            $transaction->posted_at = $params['posted_at'];
        } else {
            $transaction->posted_at = date('Y-m-d H:i:s');
        }
        if (isset($params['country_id'])) {
            $transaction->country_id = $params['country_id'];
        } else {
            $transaction->country_id = $user->country_id;
        }
        $transaction->quantity = 0;
        $transaction->trans_class = $trans_class;
        $transaction->user_id = array_get($params, 'user_id');
        $transaction->partner_id = $params['partner']->id;
        $transaction->source = array_get($params, 'source');
        $transaction->ref_number = array_get($params, 'reference');
        $transaction->notes = array_get($params, 'notes');
        $transaction->store_id = array_get($params, 'store_id');
        $transaction->pos_id = array_get($params, 'pos_id');
        $transaction->auth_staff_id = array_get($params, 'staff_id');
        $transaction->partner_trxtype = array_get($params, 'partner_trxtype');
        $transaction->points_rewarded = 0;
        $transaction->rule_id = null;
        $transaction->network_id = $params['network']->id;
        $transaction->points_balance = $user->balance($transaction->network_id);
        if (isset($params['mcc'])) {
            $transaction->mcc = $params['mcc'];
        }
        if (isset($params['product_id'])) {
            $transaction->product_id = $params['product_id'];
        }
        $transaction->save();

        // Item only reward transaction

        if (isset($params['items'])) {

            $params['currency'] = Currency::where('short_code', 'USD')->first();

            $transaction->currency_id = $params['currency']->id;
            $transaction->partner_currency_id = $params['currency']->id;
            $transaction->exchange_rate = $params['currency']->latestPrice()->rate;

            foreach ($params['items'] as $product) {

                $p = Product::find($product->id);

                if ($p) {
                    if (!isset($product->qty)) {
                        $product->qty = 1;
                    }

                    $originalPriceInNetworkCurrency = $p->original_price;
//                    if($p->currency_id != $transaction->currency_id){
//                        $originalPriceInNetworkCurrency = $p->price_in_usd * $transaction->exchange_rate;
//                    }
//                    $originalPriceInNetworkCurrency		= $produsctOriginalPrice * $transaction->exchange_rate;
                    $originalPrice = $originalPriceInNetworkCurrency + ($p->original_sales_tax * $originalPriceInNetworkCurrency / 100);
                    $PriceInUsd = $p->price_in_usd + ($p->original_sales_tax * $p->price_in_usd / 100);
                    $item = new TransactionItem();

                    $item->total_amount = ($PriceInUsd * $product->qty);
                    $item->original_total_amount = ($originalPrice * $product->qty);
                    $item->currency_id = $p->currency_id;
                    $item->transaction_id = $transaction->id;
                    $item->product_id = $p->id;
                    $item->product_model = $p->model;
                    $item->product_brand_id = $p->brand_id;
                    $item->product_sub_model = $p->sub_model;
                    $item->product_category_id = $p->category_id;
                    $item->delivery_option = $p->delivery_options;
                    $item->quantity = $product->qty;

                    $item->save();

                    $transaction->total_amount += ($PriceInUsd * $product->qty);
                }
            }

            $transaction->quantity = count($params['items']);
            $transaction->original_total_amount = floatval($transaction->total_amount);
            $transaction->partner_amount = floatval($transaction->total_amount);
            $transaction->total_amount = (float) ($transaction->total_amount / $transaction->exchange_rate);
        }

        // Amount only reward transaction

        if (isset($params['amount'])) {
            $params['currency'] = Currency::where('short_code', array_get($params, 'currency'))->first();
            $partner_currency = Currency::where('short_code', $params['partner_currency'])->first();
            if (!$params['currency']) {
                return self::respondWith(
                                Transaction::TRANS_FAILED, "Transaction aborted: Currency chosen does not exist.", null
                );
            }
            if (isset($params['use_partner_currency']) && $params['use_partner_currency'] == TRUE) {
                $transaction->currency_id = $params['currency']->id;
                $transaction->partner_currency_id = $partner_currency->id;
                $transaction->exchange_rate = $params['currency']->latestPrice()->rate;
                $transaction->total_amount = (float) (array_get($params, 'original_transaction_amount') / $transaction->exchange_rate);
                $transaction->original_total_amount = floatval(array_get($params, 'original_transaction_amount'));
                $transaction->partner_amount = floatval(array_get($params, 'amount'));
            } elseif (isset($params['original_transaction_amount'])) {
                $transaction->currency_id = $params['currency']->id;
                $transaction->partner_currency_id = $params['currency']->id;
                $transaction->exchange_rate = $params['currency']->latestPrice()->rate;
                $transaction->total_amount = (float) array_get($params, 'amount');
                $transaction->original_total_amount = floatval(array_get($params, 'original_transaction_amount'));
                $transaction->partner_amount = floatval(array_get($params, 'original_transaction_amount'));
            } else {
                $transaction->currency_id = $params['currency']->id;
                $transaction->partner_currency_id = $params['currency']->id;
                $transaction->exchange_rate = $params['currency']->latestPrice()->rate;
                $transaction->total_amount = (float) (array_get($params, 'amount') / $transaction->exchange_rate);
                $transaction->original_total_amount = floatval(array_get($params, 'amount'));
                $transaction->partner_amount = floatval(array_get($params, 'amount'));
            }
        }

        if (isset($params['points']) && !isset($params['amount'])) {
            $params['currency'] = Currency::where('short_code', 'USD')->first();
            $transaction->currency_id = $params['currency']->id;
            $transaction->partner_currency_id = $params['currency']->id;
            $transaction->exchange_rate = $params['currency']->latestPrice()->rate;

            $transaction->total_amount = PointsHelper::pointsToAmount($params['points'], 'USD', $params['network']->id);
            $transaction->points_rewarded = round($params['points']);
        } { // if transfer set total amount to zero
            if (isset($params['type']) && $params['type'] == 'transfer') {
                $transaction->total_amount = 0;
            }
        } { // if bonus set total amount to zero
            if (isset($params['type']) && ($params['type'] == 'bonus' || $params['type'] == 'bonus_on_registration')) {
                $transaction->total_amount = 0;
                $transaction->original_total_amount = 0;
                $transaction->partner_amount = 0;
                $transaction->points_rewarded = $params['points'];
            }
        }
        // Process coupons

        if (!empty($params['coupon_code'])) {
            $coupon = App\Coupon::where('coupon_code', $params['coupon_code'])->first();
            $couponType = $coupon->coupon_type;
            $couponDetails = CouponHelper::CouponPrice($params['coupon_code'], $transaction->total_amount, $transaction->partner_id);
            $couponPrice = $couponDetails['price'];
            $couponCurrencyId = $couponDetails['currency'];
            $couponSegment = $coupon->segmentId();
            if (!empty($couponCurrencyId)) {
                $couponCurrencyDetails = Currency::where('id', $couponCurrencyId)->first();
                $couponPriceInUsd = $couponPrice / $couponCurrencyDetails->latestPrice()->rate;
                $couponCurrencyShortCode = $couponCurrencyDetails->short_code;
            } else {
                $couponPriceInUsd = $couponPrice;
                $couponCurrencyShortCode = 'USD';
            }

            $coupnValidFrom = $coupon->valid_from();
            $coupnValidTo = $coupon->valid_to();
            $couponPartner = $coupon->partner_id;
            $currentDate = date('Y-m-d');
            if (strtotime($coupnValidFrom) <= strtotime($currentDate) && strtotime($currentDate) <= strtotime($coupnValidTo)) {
                ;
            } else {
                DB::rollback();

                return self::respondWith(
                                Transaction::TRANS_FAILED, "The coupon is Expired!", $transaction
                );
            }

            if ($couponPartner != $transaction->partner_id) {
                DB::rollback();

                return self::respondWith(
//						500, "This coupon is issued by " . Partner::find($couponPartner)->name, $transaction
                                500, "This coupon is issued by another partner", $transaction
                );
            }

            //validate segment
            $user = User::find($transaction->user_id);
            foreach ($user->segments as $segment) {
                $userSegments[] = $segment->id;
            }

            if ($couponSegment > 0 && !in_array($couponSegment, $userSegments)) {
                return self::respondWith(
                                500, "The user is not in the corresponding segment. ", $transaction
                );
            }

            if (empty($coupon->transaction_id)) {
//				if($couponType == 'value'){
////					$couponPriceInPoints		= PointsHelper::amountToPoints($couponPrice, $couponCurrencyShortCode,$params['network']->id);
//					$user->coupon_price			= $couponPrice;
//					$transaction->points_rewarded	= ($transaction->points_rewarded - $couponPrice);
//				}
//				else{
//					$couponActualPrice			= ( $params['amount'] * $couponPriceInUsd ) / 100;
////					$couponPriceInPoints		= PointsHelper::amountToPoints($couponActualPrice,$params['currency'],$params['network']->id);
//					$user->coupon_price			= $couponActualPrice;
//					$transaction->points_rewarded	= $transaction->points_rewarded - $couponActualPrice;
//				}
//
                $transaction->coupon_id = (int) $coupon->id;
//
//                if($transaction->points_rewarded < 0) {
//                    $transaction->points_rewarded = 0;
//                }
                // Assign the coupon
                DB::table('coupon')->where('coupon_code', $params['coupon_code'])->update(array(
                    'transaction_id' => $transaction->id
                ));
            } else {
                DB::rollback();

                return self::respondWith(
                                500, "The coupon is already used for another transaction!", $transaction
                );
            }
        }

        // Process the loyalty programs against this transaction for amount or items, not points
        $bonus_pts_on_registration = 'reward';
        if (isset($params['type']) && $params['type'] == 'bonus_on_registration') {
            $bonus_pts_on_registration = 'bonus_on_registration';
        }
//        $sendgrid1 = new SendGrid(
//                        Config::get('sendgrid.username'), Config::get('sendgrid.password')
//                    );
//                    $arr_email1 = array();
//                    $arr_email1['params'] = json_encode($transaction->toArray());
//                    $mail1 = new SendGrid\Email();
//
//                    $mail1->setTos(array('adminit@bluloyalty.com'));
//                    $mail1->setFrom(Config::get('blu.email_from'));
//                                $mail1->setFromName('BLU Points');
//                    $mail1->setSubject("Bonus points on registration 1  ". $transaction->id);
//                    $mail1->setHtml(json_encode($arr_email1));
//
//                    $sendgrid1->send($mail1);
//        $resp = array();
//                                                $resp[] = "doreward new";
//                                                //$resp[] = $params;
//                                                $resp[] = $transaction->toArray();
//                                                $json_data = json_encode($resp);
//                                                APIController::postSendEmailJson($json_data);
        if (!isset($params['points'])) {
            if ($bonus_pts_on_registration !== 'bonus_on_registration') {
                try {
                    if (isset($params['use_partner_currency']) && $params['use_partner_currency'] == True) {
                        $transaction = Loyalty::processMatchingRewardExceptionRule($transaction, $bonus_pts_on_registration, True);
                    } else {
                        $transaction = Loyalty::processMatchingRewardExceptionRule($transaction, $bonus_pts_on_registration);
                    }
                } catch (Exception $ex) {
                    $resp = array("dorewardnew Process Matching Reward Exception Rule", $ex);
                    $json_data = json_encode($resp);
                    //APIController::postSendEmailJson($json_data);
                }
            }
        }
        //APIController::postSendEmailJson(json_encode($transaction->toArray()));
        // Set the transaction location

        if ($transaction->store_id) {

            $params['store'] = Store::find($transaction->store_id);

            if ($params['store'] && !isset($params['country_id'])) {
                if ($params['store']->address) {
                    $transaction->country_id = $params['store']->address->country_id;
                } else {
                    $transaction->country_id = $user->country_id;
                }
            }
        }
//        APIController::postSendEmailJson(json_encode(array($transaction->country_id,$params['country_id'])),"test 0002".array_get($params, 'reference'));
        // Assign staff member

        if ($transaction->auth_staff_id) {
            $staff_member = self::validStaffMember($transaction->auth_staff_id);

            if ($staff_member) {
                $transaction->auth_staff_id = $staff_member->id;
                $transaction->auth_role_id = $staff_member->roles()->first()->id;
            }
        }
//        $sendgrid1 = new SendGrid(
//                        Config::get('sendgrid.username'), Config::get('sendgrid.password')
//                    );
//                    $arr_email1 = array();
//                    $arr_email1['params'] = json_encode($transaction->toArray());
//                    $mail1 = new SendGrid\Email();
//
//                    $mail1->setTos(array('adminit@bluloyalty.com'));
//                    $mail1->setFrom(Config::get('blu.email_from'));
//                                $mail1->setFromName('BLU Points');
//                    $mail1->setSubject("Bonus points on registration ". $transaction->id);
//                    $mail1->setHtml(json_encode($arr_email1));
//
//                    $sendgrid1->send($mail1);
        //get network currency
        $networkCurrencyId = $params['network']->currency_id;
        $networkCurrency = Currency::where('id', $networkCurrencyId)->first();
        $networkCurrencyShortCode = $networkCurrency->short_code;
        $transaction->amt_reward = PointsHelper::pointsToAmount($transaction->points_rewarded, $networkCurrencyShortCode, $params['network']->id);

        $transaction->save();

        // Update User Balance

        $user->updateBalance($transaction->points_rewarded, $transaction->network_id, 'add');
        DB::commit();
        // Send notifications
        $outputRalph = '';
        if ($transaction->points_rewarded > 0) {

///removed for FNB testing////////////////////////////////////////////////////////////////////////////////////////////////////
            //$outputRalph = self::sendNotifications($user, $transaction, Transaction::REWARD_TRANS);
        }


//$resp = array();
//                                                $resp[] = "44444444444444444444 doreward new";
//                                                //$resp[] = $params;
//                                                $resp[] = $transaction->toArray();
//                                                $json_data = json_encode($resp);
//                                                APIController::postSendEmailJson($json_data);
        return self::respondWith(
                        Transaction::TRANS_SUCCESS, $outputRalph, $transaction->toArray()
        );
    }

    /**
     * Perform a redemption transaction
     *
     * Product Redemption:
     *
     *   $params = array(
     *       'user_id'   => 1,
     *       'partner'   => 1,
     *       'items'     => array(stdClass('id', 'qty')),
     *       'source'    => '', // Optional
     *       'reference' => '', // Optional
     *       'notes'     => '', // Optional
     *       'store_id'  => '', // Optional
     *       'pos_id'    => '', // Optional
     *       'staff_id'  => ''  // Optional
     *   );
     *
     * Points Redemption:
     *
     *   $params = array(
     *       'user_id'   => 1,
     *       'partner'   => 1,
     *       'points'    => 10000
     *   );
     *
     * Amount Redemption (USD Implied):
     *
     *   $params = array(
     *       'user_id' => 1,
     *       'partner' => 1,
     *       'amount'  => 100
     *   );
     *
     * Amount Redemption (Custom Currency):
     *
     *   $params = array(
     *       'user_id'  => 1,
     *       'partner'  => 1,
     *       'amount'   => 100,
     *       'currency' => 'USD'
     *   );
     *
     * @param array $params
     * @param string $trans_class ['Points', 'Amount', 'Items', 'Flight', 'Hotel', 'Car']
     * @return stdClass
     */
    public static function doRedemption(array $params, $trans_class = "Items", $points_type = null) {
        // ---------------------------------------------------------------------
        //
        // Transaction Field Validation Process
        //
        // ---------------------------------------------------------------------
        $remainingPoints = 0;
        $cashPayment = 0;
        $paidCash = 0;
        $networkId = 0;

        $transaction = new Transaction();

        $validator = Validator::make(
                        $params, array(
                    'user_id' => 'required|integer'
                        )
        );

        $user = User::where('id', $params['user_id'])->first();

        if ($validator->fails()) {
            TempTransactionController::deleteTempTransaction($user->id);
            return self::respondWith(
                            Transaction::TRANS_FAILED, "Validation failed, ensure that all required fields are present", null
            );
        }

        if (!empty($params['from_merchant_app']) && $params['from_merchant_app'] == 1) {
            $userDet = User::find($params['user_id']);
            $userDet->verified = true;
            $userDet->save();
        }

        if (isset($params['country'])) {
            $user->country_id = $params['country'];
        }
        if (isset($params['area'])) {
            $user->area_id = $params['area'];
        }
        if (isset($params['city'])) {
            $user->city_id = $params['city'];
        }
        if (isset($params['address_1'])) {
            $user->address_1 = $params['address_1'];
        }
        if (isset($params['address_2'])) {
            $user->address_2 = $params['address_2'];
        }
        if (isset($params['email'])) {
            $user->email = $params['email'];
        }
        if (isset($params['postal_code'])) {
            $user->postal_code = $params['postal_code'];
        }
        if (isset($params['remaining'])) {
            $remainingPoints = ceil($params['remaining']);
        }
        if (isset($params['cash_payment'])) {
            $cashPayment = $params['cash_payment'];
        }
        if (isset($params['paid_cash'])) {
            $paidCash = $params['paid_cash'];
        }
        if (isset($params['network_id'])) {
            $networkId = $params['network_id'];
        }
        if (isset($params['e_ticket'])) {
            $eticket = $params['e_ticket'];
        } else {
            $eticket = '0';
        }
        if (isset($params['send_hitchhiker_emails'])) {
            $sendHitchhikerEmails = $params['send_hitchhiker_emails'];
        } else {
            $sendHitchhikerEmails = 0;
        }
        if (isset($params['posted_at'])) {
            $transaction->posted_at = $params['posted_at'];
        } else {
            $transaction->posted_at = date('Y-m-d H:i:s');
        }

        if (!$user) {
            TempTransactionController::deleteTempTransaction($user->id);
            return self::respondWith(
                            Transaction::TRANS_FAILED, "Transaction aborted: User '{$params['user_id']}' does not exist.", null
            );
        }

        if ($user->verified != true) {
            TempTransactionController::deleteTempTransaction($user->id);
            return self::respondWith(
                            Transaction::TRANS_FAILED, "Transaction aborted: User '{$params['user_id']}' exists but is not yet verified.", null
            );
        }

        if ($user->balances()->count() == 0) {
            TempTransactionController::deleteTempTransaction($user->id);
            return self::respondWith(
                            Transaction::TRANS_FAILED, "Transaction aborted: The member has no usable points balances.", null
            );
        }

        if ($user->status != 'active') {
            TempTransactionController::deleteTempTransaction($user->id);
//            if(!empty($params['from_merchant_app']) && $params['from_merchant_app'] == 1 && $user->status == 'inactive'){
            if ($user->status == 'inactive') {
                ;
            } else {
                return self::respondWith(
                                Transaction::TRANS_FAILED, "Transaction aborted: User account is " . $user->status, null
                );
            }
        }

        // Fetch partner data
        if (!isset($params['partner'])) {
            $params['partner'] = $user->getTopLevelPartner();
        } else {
            $params['partner'] = Partner::find($params['partner']);
        }

        if (!$params['partner']) {
            TempTransactionController::deleteTempTransaction($user->id);
            return self::respondWith(
                            Transaction::TRANS_FAILED, "No valid partner entity could be located for this transaction.", null
            );
        }
        // check if user is a middlware partner and then if exists in middleware
        $partner = $params['partner'];
        if ($partner->has_middleware == 1) {
            $user_id = $params['user_id'];
            $user_ids = array();
            $user_ids[] = $user_id;
            $url = $partner->link_middleware_api . '?method=sensitiveInfo&api_key=' . $partner->middleware_api_key;
            $json_user_ids = json_encode($user_ids);
            $url = $url . "&user_ids=" . $json_user_ids;
            //$user = User::where('id', $user_id)->with('country', 'city', 'area', 'cards')->first();
            $curl = curl_init();
            // Set some options - we are passing in a useragent too here
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => $url,
                CURLOPT_SSL_VERIFYPEER => False,
                CURLOPT_USERAGENT => 'Sensitive Info'
            ));
            // Send the request & save response to $resp
            $resp = curl_exec($curl);
            $resp_curl = json_decode($resp);
            $userCPT = $resp_curl[0]->CPT;
            if (!$resp_curl) {
                TempTransactionController::deleteTempTransaction($user->id);
                return self::respondWith(
                                500, "Could not locate user with id {$user_id} in database", null
                );
            }
        }
        // Fetch the primary partner network for this transaction
        if ($networkId == 0) {
            $params['network'] = $params['partner']->firstNetwork();
        } else {
            $params['network'] = Network::find($networkId);
        }

        if ($params['network']->status != 'Active') {
            TempTransactionController::deleteTempTransaction($user->id);
            return self::respondWith(
                            Transaction::TRANS_FAILED, "Transaction aborted: Network '{$params['network']->name}' is not currently active.", null
            );
        }

        // ---------------------------------------------------------------------
        //
        // Transaction Loading Process
        //
        // ---------------------------------------------------------------------

        DB::beginTransaction();

        // Let's set the defaults for this transaction

        $transaction->trx_reward = 0;
        $transaction->trx_redeem = 1;
        $transaction->quantity = 0;
        $transaction->trans_class = $trans_class;
        $transaction->user_id = array_get($params, 'user_id');
        $transaction->country_id = $user->country_id;
        $transaction->partner_id = $params['partner']->id;
        $transaction->source = array_get($params, 'source');
        $transaction->ref_number = array_get($params, 'reference');
        $transaction->notes = array_get($params, 'notes');
        $transaction->store_id = array_get($params, 'store_id');
        $transaction->pos_id = array_get($params, 'pos_id');
        $transaction->auth_staff_id = array_get($params, 'staff_id');
        $transaction->network_id = $params['network']->id;
        $transaction->rule_id = null;
        $transaction->cash_payment = $cashPayment / 100;
        $transaction->delivery_cost = 0;
        $transaction->currency_id = $params['network']->currency_id;
        $currency = Currency::where('id', $transaction->currency_id)->first();
        $transaction->exchange_rate = $currency->latestPrice()->rate;

        if (isset($params['created_at'])) {
            $transaction->created_at = $params['created_at'];
        }
        if (isset($params['updated_at'])) {
            $transaction->updated_at = $params['updated_at'];
        }
        $testTempTransactionInsert = TempTransactionController::createTempTransaction($transaction);
        if ($testTempTransactionInsert) {
            $transaction->save();
        } else {
            TempTransactionController::deleteTempTransaction($user->id);
            return self::respondWith(
//                Transaction::TRANS_FAILED, "There is another transaction running at the moment.", $testTempTransactionInsert
                            Transaction::TRANS_FAILED, "Another redemption transaction is currently being processed for the same account. Please try again.", $testTempTransactionInsert
            );
        }

        // Process the payload

        $trans_price = 0;
        $trans_points = 0;
        $trans_delivery = 0;

        $userBalance = $user->balance($transaction->network_id);

        // Are we redeeming products?
        if (isset($params['items'])) {
            $sum_item_price_in_points = 0;
            foreach ($params['items'] as $p) {
                $product = Product::find($p->id);
                $orginalPriceinNetworkCurrency = $product->original_price;
//                if($product->currency_id != $transaction->currency_id){
//                    $orginalPriceinNetworkCurrency = $product->price_in_usd * $transaction->exchange_rate;
//                }
                $originalPrice = $orginalPriceinNetworkCurrency + ($product->original_sales_tax * $orginalPriceinNetworkCurrency / 100);
                $PriceInUsd = $product->price_in_usd + ($product->original_sales_tax * $product->price_in_usd / 100);
                $PriceInPoints = $product->price_in_points + ($product->original_sales_tax * $product->price_in_points / 100);
                $originalcurrencyId = $product->currency_id;
                $deliveryPrice = $product->delivery_charges;
                $deliveryCurrecy = $product->delivery_currency_id;
                $productPriceWithTax = $product->original_price + ($product->original_sales_tax * $product->original_price / 100);
                $originalPriceInPoints = PointsHelper::productPriceInPoints($PriceInPoints, $productPriceWithTax, $originalcurrencyId, $networkId);
                $sum_item_price_in_points += ($originalPriceInPoints * $p->qty);
            }

            foreach ($params['items'] as $p) {
                if (!isset($p->qty) || $p->qty == null) {
                    $p->qty = 1;
                }

                $product = Product::find($p->id);

                if ($product) {

                    if ($networkId == 0) {
                        $networkId = $params['network']->id;
                    }
//                    $orginalPriceinNetworkCurrency  = $product->price_in_usd * $transaction->exchange_rate;
                    $orginalPriceinNetworkCurrency = $product->original_price;
//                    if($product->currency_id != $transaction->currency_id){
//                        $orginalPriceinNetworkCurrency = $product->price_in_usd * $transaction->exchange_rate;
//                    }
                    $originalPrice = $orginalPriceinNetworkCurrency + ($product->original_sales_tax * $orginalPriceinNetworkCurrency / 100);
                    $PriceInUsd = $product->price_in_usd + ($product->original_sales_tax * $product->price_in_usd / 100);
                    $PriceInPoints = $product->price_in_points + ($product->original_sales_tax * $product->price_in_points / 100);
                    $originalcurrencyId = $product->currency_id;
                    $deliveryPrice = $product->delivery_charges;
                    $deliveryCurrecy = $product->delivery_currency_id;
                    $productPriceWithTax = $product->original_price + ($product->original_sales_tax * $product->original_price / 100);
//                    $originalPriceInPoints1	= PointsHelper::productPriceInPoints($PriceInPoints, $productPriceWithTax, $originalcurrencyId, $networkId);
                    $originalPriceInPoints = PointsHelper::productPriceInPoints($PriceInPoints, $productPriceWithTax, $originalcurrencyId, $networkId);
                    $deliveryPriceInPoints = ceil(PointsHelper::deliveryPriceInPoints($deliveryPrice, $deliveryCurrecy, $networkId));

                    $transaction->total_amount += ($PriceInUsd * $p->qty);
                    $transaction->original_total_amount += ($originalPrice * $p->qty);
                    $transaction->partner_amount += ($originalPrice * $p->qty);
                    $transaction->points_redeemed += ($originalPriceInPoints * $p->qty);

                    if ($product->is_voucher == 1) {
                        $transaction->delivery_cost += intval($deliveryPriceInPoints);
                    } else {
                        $transaction->delivery_cost += intval($deliveryPriceInPoints * intval($p->qty));
                    }



                    $transaction->quantity += $p->qty;

                    // Record TransactionItem

                    $ti = new TransactionItem();

                    $ti->total_amount = $PriceInUsd * floatval($p->qty);
                    $ti->original_total_amount = $originalPrice * $p->qty;
                    $ti->currency_id = $product->currency_id;
                    $ti->product_id = $product->id;
                    $ti->product_model = $product->model;
                    $ti->product_brand_id = $product->brand_id;
                    $ti->product_sub_model = $product->sub_model;
                    $ti->product_category_id = $product->category_id;
                    $ti->delivery_option = $product->delivery_options;
                    $ti->price_in_points = $originalPriceInPoints * floatval($p->qty);
                    $ti->network_id = $networkId;
//                    $ti->delivery_cost		= $deliveryPriceInPoints * floatval($p->qty);
                    if ($product->is_voucher == 1) {
                        $ti->delivery_cost = $deliveryPriceInPoints;
                    } else {
                        $ti->delivery_cost = $deliveryPriceInPoints * floatval($p->qty);
                    }
                    $ti->quantity = $p->qty;
                    $ti->transaction_id = $transaction->id;

                    $ti->save();

                    // Saving redemption order
                    // calculate the cash payment by item
                    $totalPaidInCash = $transaction->cash_payment;
                    $itemPriceInPoints = $ti->price_in_points;
                    $ratio = $itemPriceInPoints / $sum_item_price_in_points;
                    $cash_paid_by_item = $totalPaidInCash * $ratio;
                    $itemFullPriceInUsd = $ti->total_amount + $cash_paid_by_item;
                    if (!empty($ti->amt_delivery)) {
                        $redOrderDeliveryCost = $ti->amt_delivery;
                    } else {
                        $redOrderDeliveryCost = 0;
                    }
                    $redemptionOrederParams = array(
                        'trx_id' => $transaction->id,
                        'trx_item_id' => $ti->id,
                        'product_id' => $product->id,
                        'product_name' => $product->name,
                        'product_model' => $product->model,
                        'product_sub_model' => $product->sub_model,
                        'full_price_in_usd' => $itemFullPriceInUsd,
                        'supplier_id' => $product->supplier_id,
                        'cost' => $itemFullPriceInUsd,
                        'cost_currency' => 6, //$tid->currency_id,
                        'delivery_cost' => $redOrderDeliveryCost,
                        'delivery_cost_currency' => $ti->currency_id,
                        'quantity' => $ti->quantity,
                        'user_id' => $transaction->user_id,
                        'cash_paid' => $cash_paid_by_item
                    );
                    RedemptionOrder::createRedemptionOrder($redemptionOrederParams, 'Item');
                    // Check stock levels
                    if (($product->qty - $p->qty) < 0) {
                        $product->qty = 0;
                    } else {
                        $product->qty = (int) ($product->qty - $p->qty);
                    }

                    // Update product

                    $product->save();

                    if ($product->qty <= Config::get('blu.stock_level_warning')) {
                        $product->sendStockLevelWarning();
                    }
                }
            }
            //add delivery cost in usd to total_amount
            $networkCurrency = Currency::where('id', $transaction->currency_id)->first();
            $deliveryCostInPoints = ceil($transaction->delivery_cost);
            $deliveryCostInUSD = PointsHelper::pointsToAmount($deliveryCostInPoints, 'USD', $params['network']->id);
            $deliveryCostInOriginalCurrency = PointsHelper::pointsToAmount($deliveryCostInPoints, $networkCurrency->short_code, $params['network']->id);
//            $dev_cost = intval($transaction->delivery_cost)/100;
            $transaction->total_amount = $transaction->total_amount + $deliveryCostInUSD;
            $transaction->original_total_amount = $transaction->original_total_amount + $deliveryCostInOriginalCurrency;

            // Add Delivery Cost to Points Redeemed
            $transaction->points_redeemed += ceil($transaction->delivery_cost);

            // Remove points paid by cash from Points
            if ($remainingPoints > 0) {
                $transaction->points_redeemed -= $remainingPoints;
            }

            if ($transaction->points_redeemed > $userBalance && $paidCash == 0) {
                DB::rollback();
                TempTransactionController::deleteTempTransaction($user->id);

                return self::respondWith(
                    Transaction::TRANS_FAILED, "User doesn't have enough amount of points. Please try again.", $testTempTransactionInsert
                );
            }
        }

        // Are we redeeming points?
        if (isset($params['points'])) {
            $networkCurrency = Currency::where('id', $transaction->currency_id)->first();
            $transaction->points_redeemed = ceil($params['points']);
            $partner = $params['partner'];

            if ($transaction->points_redeemed > $userBalance && $paidCash == 0) {
                DB::rollback();
                TempTransactionController::deleteTempTransaction($user->id);

                return self::respondWith(
                    Transaction::TRANS_FAILED, "User doesn't have enough amount of points. Please try again.", $testTempTransactionInsert
                );
            }

            if ($points_type == 'cashback') {

                $transaction->total_amount = PointsHelper::pointsToAmount($params['points'], 'USD', $params['network']->id, 'cashback', $partner->id);
                $transaction->original_total_amount = PointsHelper::pointsToAmount($params['points'], $networkCurrency->short_code, $params['network']->id, 'cashback', $partner->id);
                $transaction->partner_amount = PointsHelper::pointsToAmount($params['points'], $networkCurrency->short_code, $params['network']->id, 'cashback', $partner->id);
            } elseif ($points_type == 'miles') {

                $transaction->total_amount = 0; //PointsHelper::pointsToAmount($params['points'], 'USD', $params['network']->id,'miles',$partner->id);
                $transaction->original_total_amount = 0; //PointsHelper::pointsToAmount($params['points'], $networkCurrency->short_code, $params['network']->id,'miles',$partner->id);
                $transaction->partner_amount = 0; //PointsHelper::pointsToAmount($params['points'], $networkCurrency->short_code, $params['network']->id,'miles',$partner->id);
            } else {
                $transaction->total_amount = PointsHelper::pointsToAmount($params['points'], 'USD', $params['network']->id, null, $partner->id);
                $transaction->original_total_amount = PointsHelper::pointsToAmount($params['points'], $networkCurrency->short_code, $params['network']->id, null, $partner->id);
                $transaction->partner_amount = PointsHelper::pointsToAmount($params['points'], $networkCurrency->short_code, $params['network']->id, null, $partner->id);
            } { // if transfer set total amount to zero
                if (isset($params['type']) && $params['type'] == 'transfer') {
                    $transaction->total_amount = 0;
                }
            }
        }

        // Are we redeeming an amount?

        if (isset($params['amount'])) {
            if (isset($params['redeem_points'])) {
                $transaction->points_redeemed = $params['redeem_points'];
            } else {
                $transaction->points_redeemed = ceil(PointsHelper::amountToPoints($params['amount'], $params['currency'], $networkId));
            }

            if ($transaction->points_redeemed > $userBalance && $paidCash == 0) {
                DB::rollback();
                TempTransactionController::deleteTempTransaction($user->id);

                return self::respondWith(
                    Transaction::TRANS_FAILED, "User doesn't have enough amount of points. Please try again.", $testTempTransactionInsert
                );
            }

            $currency = Currency::where('short_code', $params['currency'])->first();
            $transaction->currency_id = $currency->id;
            $transaction->exchange_rate = $currency->latestPrice()->rate;
            $transaction->original_total_amount = $params['amount'];
            $transaction->total_amount = floatval($params['amount']) / floatval($transaction->exchange_rate);
        }

        // Are we redeeming a travel ?
//		 if( isset($params['travel_type']) && ( $params['travel_type'] == 'car' ) ){
//			 $transaction->points_redeemed = $params['points'];
//		 }

        if ($transaction->auth_staff_id) {

            $staff_member = self::validStaffMember($transaction->auth_staff_id);

            if ($staff_member) {
                $transaction->auth_staff_id = $staff_member->id;
                $transaction->auth_role_id = $staff_member->roles()->first()->id;
            }
        }

        // Process coupons

        if (!empty($params['coupon_code']) && trim($params['coupon_code']) !== "") {
            $coupon = App\Coupon::where('coupon_code', $params['coupon_code'])->first();
            $couponType = $coupon->coupon_type;

            $couponDetails = CouponHelper::CouponPrice($params['coupon_code'], $transaction->total_amount, $transaction->partner_id);
            $couponPrice = $couponDetails['price'];
            $couponCurrencyId = $couponDetails['currency'];

            if (!empty($couponCurrencyId)) {
                $couponCurrencyDetails = Currency::where('id', $couponCurrencyId)->first();
                $couponPriceInUsd = $couponPrice / $couponCurrencyDetails->latestPrice()->rate;
                $couponCurrencyShortCode = $couponCurrencyDetails->short_code;
            } else {
                $couponPriceInUsd = $couponPrice;
                $couponCurrencyShortCode = 'USD';
            }
            $coupnValidFrom = $coupon->valid_from();
            $coupnValidTo = $coupon->valid_to();
            $couponPartner = $coupon->partner_id;
            $currentDate = date('Y-m-d');
            if (strtotime($coupnValidFrom) <= strtotime($currentDate) && strtotime($currentDate) <= strtotime($coupnValidTo)) {
                ;
            } else {
                DB::rollback();
                TempTransactionController::deleteTempTransaction($user->id);
                return self::respondWith(
                                Transaction::TRANS_FAILED, "The coupon is Expired!", $transaction
                );
            }

            if ($couponPartner != $transaction->partner_id) {
                DB::rollback();
                TempTransactionController::deleteTempTransaction($user->id);
                return self::respondWith(
                                500, "This coupon is issued by " . Partner::find($couponPartner)->name, $transaction
                );
            }

            if (empty($coupon->transaction_id)) {
                if ($couponType == 'value') {
                    $couponPriceInPoints = PointsHelper::amountToPoints($couponPrice, $couponCurrencyShortCode, $networkId);
                    $user->coupon_price = $couponPriceInPoints;
                    $transaction->points_redeemed = ($transaction->points_redeemed - $couponPriceInPoints);
                } else {
//					$couponActualPrice				= ( $params['amount'] * $couponPriceInUsd ) / 100;
                    $couponActualPrice = $couponPriceInUsd;
                    $couponPriceInPoints = PointsHelper::amountToPoints($couponActualPrice, $params['currency'], $networkId);
                    $user->coupon_price = $couponActualPrice;
                    $transaction->points_redeemed = $transaction->points_redeemed - $couponPriceInPoints;
                }

                $transaction->coupon_id = (int) $coupon->id;

                if ($transaction->points_redeemed < 0) {
                    $transaction->points_redeemed = 0;
                }

                if ($transaction->points_redeemed > $userBalance && $paidCash == 0) {
                    DB::rollback();
                    TempTransactionController::deleteTempTransaction($user->id);

                    return self::respondWith(
                        Transaction::TRANS_FAILED, "User doesn't have enough amount of points. Please try again.", $testTempTransactionInsert
                    );
                }

                // Assign the coupon
                DB::table('coupon')->where('coupon_code', $params['coupon_code'])->update(array(
                    'transaction_id' => $transaction->id
                ));
            } else {
                DB::rollback();
                TempTransactionController::deleteTempTransaction($user->id);
                return self::respondWith(
                                500, "The coupon is already used for another transaction!", $transaction
                );
            }
        }

        // ---------------------------------------------------------------------
        //
        // Network Points Conversion Process
        //
        // ---------------------------------------------------------------------
        $user_balances = $user->balances();
        $networks = array();
        $trxNewtork = Network::where('id', '=', $transaction->network_id)->take(1)->first();
        // Is this transaction on a BLU network?
        if ($transaction->network_id == '1') {
            $networks[] = '1';
        } else {

            if ($trxNewtork->do_points_conversion) {
                $networks[] = $transaction->network_id;
                $networks[] = '1';
            } else {
                $networks[] = $transaction->network_id;
            }
        }

        /*
         * Here we construct a queue of network IDS in order of priority to try
         * transact against. The user balances are already ordered by date
         * updated to prioritize the oldest ones
         */
        if ($trxNewtork->do_points_conversion) {
            //echo "<pre> User Balances <br> ";
//            var_dump($user_balances);
//            exit();
            foreach ($user_balances as $balance) {
                $trxNewtork1 = Network::where('id', '=', $balance->network_id)->take(1)->first();
//                echo "<pre>Transaction";
//                var_dump($balance->network_id);
//                echo "Networks";
//                var_dump($trxNewtork1->do_points_conversion);
//                exit();
                if (!in_array($balance->network_id, $networks) && $balance->network_id != '0' && $trxNewtork1->do_points_conversion == '1') {
                    array_push($networks, $balance->network_id);
                }
            }
        }



        $total_rundown = $transaction->points_redeemed;

        /*
         * Here we attempt to deduct the maximum amount of points from a member
         * on each network that they belong to by proving the network where the
         * transaction occurred and then the network the transaction is
         * attempting to run against
         */
        $count_networks = 0;
        $rundown_ralph = array();
        $transaction_network = Network::where('id', '=', $transaction->network_id)->take(1)->first();

        foreach ($networks as $balance_network) {
            $attempt_network = Network::where('id', '=', $balance_network)->take(1)->first();
            if ($attempt_network->do_points_conversion || $attempt_network->id == $transaction_network->id) {
                $total_rundown -= $user->deductMaxPoints($total_rundown, $transaction->network_id, $balance_network);

                $rundown_ralph[$count_networks]['balance_network'] = $balance_network;
                $rundown_ralph[$count_networks]['trx_net_id'] = $transaction->network_id;
                $rundown_ralph[$count_networks]['total_rundown'] = $total_rundown;
                $count_networks += 1;
            }
            if ($total_rundown <= 0) {
                break;
            }
        }


        if ($paidCash == 1) {
            // Remove points paid by cash from Points
            if ($remainingPoints > 0) {
                $transaction->points_redeemed -= $remainingPoints;
                $temp_view_pts_redeemed = $transaction->points_redeemed;
                //$transaction->points_redeemed	 = abs($transaction->points_redeemed);
                $total_rundown = 0;
            }
        }
        /*
         * We check to see if the total required points could not collectively be
         * deduced from the networks the member belong to. If successful, there
         * should be no points remaining in the rundown
         */
        if ($total_rundown > 0) {
            DB::rollback();
            TempTransactionController::deleteTempTransaction($user->id);
            return self::respondWith(
                            Transaction::TRANS_FAILED, "The member has an insufficient networks points balance for this transaction.", $rundown_ralph
            );
        }

        // Update the transaction
        $networkCurrencyId = $params['network']->currency_id;
        $networkCurrency = Currency::where('id', $networkCurrencyId)->first();
        $networkCurrencyShortCode = $networkCurrency->short_code;
        $transaction->amt_redeem = number_format(PointsHelper::pointsToAmount($transaction->points_redeemed, $networkCurrencyShortCode, $params['network']->id), 2);

        $transaction->save();
        DB::commit();

        $url_trx_api = env('APP_URL').'/api/deduct-transaction-max?api_key=c4ca4238a0b923820dcc509a6f75849b&transaction_id=' . $transaction->id;
        $url = url($url_trx_api);

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, False);
        $result = curl_exec($ch);
        curl_close($ch);

        // Commented manually
//        $sendgrid = new SendGrid(
//                Config::get('sendgrid.username'), Config::get('sendgrid.password')
//        );
//        $arr_email = array();
//        $arr_email["network"] = $networkCurrencyId;
//        $arr_email["partner"] = $partner->id;
//        $arr_email['amt_rede'] = json_encode($transaction->toArray());
//        $arr_email['returnMaxDeduction'] = $returnMaxDeduction;
//        $arr_email['user'] = $user->id;
//        $arr_email['balance'] = json_encode($user_balances);
//        $mail = new SendGrid\Email();
//
//        $mail->setTos(array('adminit@bluloyalty.com'));
//        $mail->setFrom(Config::get('blu.email_from'));
//        $mail->setFromName('BLU Points');
//        $mail->setSubject("Return Max Deduction" . $transaction->id);
//        $mail->setHtml(json_encode($arr_email) . "xxxxx" . $returnMaxDeduction);
//
//        $sendgrid->send($mail);
        // Process the loyalty program

        $transaction = Loyalty::processMatchingRedemptionRule($transaction);

        // Create the order for reference information

        $order = new Order();

        $order->transaction_id = $transaction->id;
        $order->user_id = $transaction->user_id;

        $order->save();

        // Send the notifications
        $outputRalph = '';
        if (strtolower($trans_class) != "flight") {
            if ($transaction->points_redeemed > 0 || $paidCash == 1) {
                //Audit Log for Redemption
                AuditHelper::record($transaction->partner_id, "Redemption | Member '{$user->id} / {$user->email}' | Transaction Id: '{$transaction->id}'" . " on " . date('Y-m-d H:i:s'));
//            if($transaction->partner_id !== '3'){
                if (!strpos($transaction->ref_number, "-- Card Deduction")) {
                    try {
                        if ($eticket == '0') {
                            // commented manually
                            $outputRalph = self::sendNotifications($user, $transaction, Transaction::REDEEM_TRANS);
                        }
                    } catch (Exception $ex) {

//                    Commented manually
//                    $sendgrid1 = new SendGrid(
//                            Config::get('sendgrid.username'), Config::get('sendgrid.password')
//                    );
//                    $arr_email1 = array();
//                    $arr_email1["network"] = $networkCurrencyId;
//                    $arr_email1["partner"] = $partner->id;
//                    $arr_email1['amt_rede'] = $transaction->amt_redeem;
//                    $arr_email1['returnMaxDeduction'] = $returnMaxDeduction;
//                    $arr_email1['user'] = $user->id;
//                    $arr_email1['balance'] = json_encode($user_balances);
//                    $arr_email1['exception'] = $ex;
//                    $mail1 = new SendGrid\Email();
//
//                    $mail1->setTos(array('adminit@bluloyalty.com'));
//                    $mail1->setFrom(Config::get('blu.email_from'));
//                    $mail1->setFromName('BLU Points');
//                    $mail1->setSubject("doRedemption Send Notification " . $transaction->id);
//                    $mail1->setHtml(json_encode($arr_email1) . "xxxxx" . $returnMaxDeduction);
//
//                    $sendgrid1->send($mail1);

                        NotificationController::sendEmailNotification(json_encode($arr_email1) . "xxxxx", 'BLU Points', Config::get('blu.email_from'), array(null), array('adminit@bluloyalty.com'), "doRedemption Send Notification " . $transaction->id);
                    }
                }
//            }
            }
        }

        // Send confirmation to BLU Admins
//        if($transaction->partner_id !== '3'){
        if ($eticket == '0' || $sendHitchhikerEmails == 1) {
            if (isset($userCPT) && !empty($userCPT)) {
                $user->CPT = $userCPT;
            }
            try {
                if ($sendHitchhikerEmails == 1) {
                    /* Send Order Notification to BLU Admin */
                    self::sendOrderNotification($transaction, $user, "", "", "order", "v3");
                    self::sendNotifications($user, $transaction, Transaction::REDEEM_TRANS, "v3", "booking-confirmation");
                } else {
                    self::sendOrderNotification($transaction, $user);
                }
            } catch (Exception $ex) {

//                Commented manually
//                $sendgrid1 = new SendGrid(
//                        Config::get('sendgrid.username'), Config::get('sendgrid.password')
//                );
//                $arr_email1 = array();
//                $arr_email1["network"] = $networkCurrencyId;
//                $arr_email1["partner"] = $partner->id;
//                $arr_email1['amt_rede'] = $transaction->amt_redeem;
//                $arr_email1['returnMaxDeduction'] = $returnMaxDeduction;
//                $arr_email1['user'] = $user->id;
//                $arr_email1['balance'] = json_encode($user_balances);
//                $arr_email1['exception'] = $ex;
//                $mail1 = new SendGrid\Email();
//
//                $mail1->setTos(array('adminit@bluloyalty.com'));
//                $mail1->setFrom(Config::get('blu.email_from'));
//                $mail1->setFromName('BLU Points');
//                $mail1->setSubject("doRedemption Order Nofication " . $transaction->id);
//                $mail1->setHtml(json_encode($arr_email1) . "xxxxx" . $returnMaxDeduction);
//
//                $sendgrid1->send($mail1);

                NotificationController::sendEmailNotification(json_encode($arr_email1) . "xxxxx", 'BLU Points', Config::get('blu.email_from'), array(null), array('adminit@bluloyalty.com', Config::get('blu.admin_email')), "doRedemption Order Nofication " . $transaction->id);
            }
        }
//        }
        AuditHelper::record($transaction->partner_id, "Redemption 112 | Member '{$user->id} / {$user->email}' | Transaction Id: '{$transaction->id}'" . " on " . date('Y-m-d H:i:s'));
        $result_ralph = array();
        $result_ralph['trx'] = $transaction->toArray();
        //$result_ralph['ralph'] = $output;\
        //deduct max transactions
//        Commented manually
//        $sendgrid1 = new SendGrid(
//                Config::get('sendgrid.username'), Config::get('sendgrid.password')
//        );
        $arr_email1 = array();
        $arr_email1["network"] = $networkCurrencyId;
        $arr_email1["partner"] = $partner->id;
        $arr_email1['amt_rede'] = $transaction->amt_redeem;
        // $arr_email1['returnMaxDeduction'] = $returnMaxDeduction;
        $arr_email1['user'] = $user->id;
        $arr_email1['balance'] = json_encode($user_balances);

//        Commented manually
//        $mail1 = new SendGrid\Email();
//
//        $mail1->setTos(array('adminit@bluloyalty.com'));
//        $mail1->setFrom(Config::get('blu.email_from'));
//        $mail1->setFromName('BLU Points');
//        $mail1->setSubject("End doRedemption Return Max Deduction" . $transaction->id);
//        $mail1->setHtml(json_encode($arr_email1) . "xxxxx" . $returnMaxDeduction);
//
//        $sendgrid1->send($mail1);


        NotificationController::sendEmailNotification(json_encode($arr_email1) . "xxxxx", 'BLU Points', Config::get('blu.email_from'), array(null), array('adminit@bluloyalty.com'), "End doRedemption Return Max Deduction" . $transaction->id);


        // $result_ralph['ralph'] = $returnMaxDeduction;
        // Send confirmation to BLU Admins
        TempTransactionController::deleteTempTransaction($user->id);
        return self::respondWith(
                        Transaction::TRANS_SUCCESS, "success", $result_ralph
        );
    }




    public static function doRedemptionTest(array $params, $trans_class = "Items", $points_type = null) {
        // ---------------------------------------------------------------------
        //
        // Transaction Field Validation Process
        //
        // ---------------------------------------------------------------------
        $remainingPoints = 0;
        $cashPayment = 0;
        $paidCash = 0;
        $networkId = 0;

        $transaction = new Transaction();

        $validator = Validator::make(
                        $params, array(
                    'user_id' => 'required|integer'
                        )
        );

        $user = User::where('id', $params['user_id'])->first();

        if ($validator->fails()) {
            TempTransactionController::deleteTempTransaction($user->id);
            return self::respondWith(
                            Transaction::TRANS_FAILED, "Validation failed, ensure that all required fields are present", null
            );
        }

        if (!empty($params['from_merchant_app']) && $params['from_merchant_app'] == 1) {
            $userDet = User::find($params['user_id']);
            $userDet->verified = true;
            $userDet->save();
        }

        if (isset($params['country'])) {
            $user->country_id = $params['country'];
        }
        if (isset($params['area'])) {
            $user->area_id = $params['area'];
        }
        if (isset($params['city'])) {
            $user->city_id = $params['city'];
        }
        if (isset($params['address_1'])) {
            $user->address_1 = $params['address_1'];
        }
        if (isset($params['address_2'])) {
            $user->address_2 = $params['address_2'];
        }
        if (isset($params['email'])) {
            $user->email = $params['email'];
        }
        if (isset($params['postal_code'])) {
            $user->postal_code = $params['postal_code'];
        }
        if (isset($params['remaining'])) {
            $remainingPoints = ceil($params['remaining']);
        }
        if (isset($params['cash_payment'])) {
            $cashPayment = $params['cash_payment'];
        }
        if (isset($params['paid_cash'])) {
            $paidCash = $params['paid_cash'];
        }
        if (isset($params['network_id'])) {
            $networkId = $params['network_id'];
        }
        if (isset($params['e_ticket'])) {
            $eticket = $params['e_ticket'];
        } else {
            $eticket = '0';
        }
        if (isset($params['send_hitchhiker_emails'])) {
            $sendHitchhikerEmails = $params['send_hitchhiker_emails'];
        } else {
            $sendHitchhikerEmails = 0;
        }
        if (isset($params['posted_at'])) {
            $transaction->posted_at = $params['posted_at'];
        } else {
            $transaction->posted_at = date('Y-m-d H:i:s');
        }

        if (!$user) {
            TempTransactionController::deleteTempTransaction($user->id);
            return self::respondWith(
                            Transaction::TRANS_FAILED, "Transaction aborted: User '{$params['user_id']}' does not exist.", null
            );
        }

        if ($user->verified != true) {
            TempTransactionController::deleteTempTransaction($user->id);
            return self::respondWith(
                            Transaction::TRANS_FAILED, "Transaction aborted: User '{$params['user_id']}' exists but is not yet verified.", null
            );
        }

        if ($user->balances()->count() == 0) {
            TempTransactionController::deleteTempTransaction($user->id);
            return self::respondWith(
                            Transaction::TRANS_FAILED, "Transaction aborted: The member has no usable points balances.", null
            );
        }

        if ($user->status != 'active') {
            TempTransactionController::deleteTempTransaction($user->id);
//            if(!empty($params['from_merchant_app']) && $params['from_merchant_app'] == 1 && $user->status == 'inactive'){
            if ($user->status == 'inactive') {
                ;
            } else {
                return self::respondWith(
                                Transaction::TRANS_FAILED, "Transaction aborted: User account is " . $user->status, null
                );
            }
        }

        // Fetch partner data
        if (!isset($params['partner'])) {
            $params['partner'] = $user->getTopLevelPartner();
        } else {
            $params['partner'] = Partner::find($params['partner']);
        }

        if (!$params['partner']) {
            TempTransactionController::deleteTempTransaction($user->id);
            return self::respondWith(
                            Transaction::TRANS_FAILED, "No valid partner entity could be located for this transaction.", null
            );
        }
        // check if user is a middlware partner and then if exists in middleware
        $partner = $params['partner'];
        if ($partner->has_middleware == 1) {
            $user_id = $params['user_id'];
            $user_ids = array();
            $user_ids[] = $user_id;
            $url = $partner->link_middleware_api . '?method=sensitiveInfo&api_key=' . $partner->middleware_api_key;
            $json_user_ids = json_encode($user_ids);
            $url = $url . "&user_ids=" . $json_user_ids;
            //$user = User::where('id', $user_id)->with('country', 'city', 'area', 'cards')->first();
            $curl = curl_init();
            // Set some options - we are passing in a useragent too here
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => $url,
                CURLOPT_SSL_VERIFYPEER => False,
                CURLOPT_USERAGENT => 'Sensitive Info'
            ));
            // Send the request & save response to $resp
            $resp = curl_exec($curl);
            $resp_curl = json_decode($resp);
            $userCPT = $resp_curl[0]->CPT;
            if (!$resp_curl) {
                TempTransactionController::deleteTempTransaction($user->id);
                return self::respondWith(
                                500, "Could not locate user with id {$user_id} in database", null
                );
            }
        }
        // Fetch the primary partner network for this transaction
        if ($networkId == 0) {
            $params['network'] = $params['partner']->firstNetwork();
        } else {
            $params['network'] = Network::find($networkId);
        }

        if ($params['network']->status != 'Active') {
            TempTransactionController::deleteTempTransaction($user->id);
            return self::respondWith(
                            Transaction::TRANS_FAILED, "Transaction aborted: Network '{$params['network']->name}' is not currently active.", null
            );
        }

        // ---------------------------------------------------------------------
        //
        // Transaction Loading Process
        //
        // ---------------------------------------------------------------------

        DB::beginTransaction();

        // Let's set the defaults for this transaction

        $transaction->trx_reward = 0;
        $transaction->trx_redeem = 1;
        $transaction->quantity = 0;
        $transaction->trans_class = $trans_class;
        $transaction->user_id = array_get($params, 'user_id');
        $transaction->country_id = $user->country_id;
        $transaction->partner_id = $params['partner']->id;
        $transaction->source = array_get($params, 'source');
        $transaction->ref_number = array_get($params, 'reference');
        $transaction->notes = array_get($params, 'notes');
        $transaction->store_id = array_get($params, 'store_id');
        $transaction->pos_id = array_get($params, 'pos_id');
        $transaction->auth_staff_id = array_get($params, 'staff_id');
        $transaction->network_id = $params['network']->id;
        $transaction->rule_id = null;
        $transaction->cash_payment = $cashPayment / 100;
        $transaction->delivery_cost = 0;
        $transaction->currency_id = $params['network']->currency_id;
        $currency = Currency::where('id', $transaction->currency_id)->first();
        $transaction->exchange_rate = $currency->latestPrice()->rate;

        if (isset($params['created_at'])) {
            $transaction->created_at = $params['created_at'];
        }
        if (isset($params['updated_at'])) {
            $transaction->updated_at = $params['updated_at'];
        }
        $testTempTransactionInsert = TempTransactionController::createTempTransaction($transaction);
        if ($testTempTransactionInsert) {
            $transaction->save();
        } else {
            TempTransactionController::deleteTempTransaction($user->id);
            return self::respondWith(
//                Transaction::TRANS_FAILED, "There is another transaction running at the moment.", $testTempTransactionInsert
                            Transaction::TRANS_FAILED, "Another redemption transaction is currently being processed for the same account. Please try again.", $testTempTransactionInsert
            );
        }

        // Process the payload

        $trans_price = 0;
        $trans_points = 0;
        $trans_delivery = 0;

        // Are we redeeming products?

        if (isset($params['items'])) {
            $sum_item_price_in_points = 0;
            foreach ($params['items'] as $p) {
                $product = Product::find($p->id);
                $orginalPriceinNetworkCurrency = $product->original_price;
//                if($product->currency_id != $transaction->currency_id){
//                    $orginalPriceinNetworkCurrency = $product->price_in_usd * $transaction->exchange_rate;
//                }
                $originalPrice = $orginalPriceinNetworkCurrency + ($product->original_sales_tax * $orginalPriceinNetworkCurrency / 100);
                $PriceInUsd = $product->price_in_usd + ($product->original_sales_tax * $product->price_in_usd / 100);
                $PriceInPoints = $product->price_in_points + ($product->original_sales_tax * $product->price_in_points / 100);
                $originalcurrencyId = $product->currency_id;
                $deliveryPrice = $product->delivery_charges;
                $deliveryCurrecy = $product->delivery_currency_id;
                $productPriceWithTax = $product->original_price + ($product->original_sales_tax * $product->original_price / 100);
                $originalPriceInPoints = PointsHelper::productPriceInPoints($PriceInPoints, $productPriceWithTax, $originalcurrencyId, $networkId);
                $sum_item_price_in_points += ($originalPriceInPoints * $p->qty);
            }

            foreach ($params['items'] as $p) {
                if (!isset($p->qty) || $p->qty == null) {
                    $p->qty = 1;
                }

                $product = Product::find($p->id);

                if ($product) {

                    if ($networkId == 0) {
                        $networkId = $params['network']->id;
                    }
//                    $orginalPriceinNetworkCurrency  = $product->price_in_usd * $transaction->exchange_rate;
                    $orginalPriceinNetworkCurrency = $product->original_price;
//                    if($product->currency_id != $transaction->currency_id){
//                        $orginalPriceinNetworkCurrency = $product->price_in_usd * $transaction->exchange_rate;
//                    }
                    $originalPrice = $orginalPriceinNetworkCurrency + ($product->original_sales_tax * $orginalPriceinNetworkCurrency / 100);
                    $PriceInUsd = $product->price_in_usd + ($product->original_sales_tax * $product->price_in_usd / 100);
                    $PriceInPoints = $product->price_in_points + ($product->original_sales_tax * $product->price_in_points / 100);
                    $originalcurrencyId = $product->currency_id;
                    $deliveryPrice = $product->delivery_charges;
                    $deliveryCurrecy = $product->delivery_currency_id;
                    $productPriceWithTax = $product->original_price + ($product->original_sales_tax * $product->original_price / 100);
//                    $originalPriceInPoints1	= PointsHelper::productPriceInPoints($PriceInPoints, $productPriceWithTax, $originalcurrencyId, $networkId);
                    $originalPriceInPoints = PointsHelper::productPriceInPoints($PriceInPoints, $productPriceWithTax, $originalcurrencyId, $networkId);
                    $deliveryPriceInPoints = ceil(PointsHelper::deliveryPriceInPoints($deliveryPrice, $deliveryCurrecy, $networkId));

                    $transaction->total_amount += ($PriceInUsd * $p->qty);
                    $transaction->original_total_amount += ($originalPrice * $p->qty);
                    $transaction->partner_amount += ($originalPrice * $p->qty);
                    $transaction->points_redeemed += ($originalPriceInPoints * $p->qty);

                    if ($product->is_voucher == 1) {
                        $transaction->delivery_cost += intval($deliveryPriceInPoints);
                    } else {
                        $transaction->delivery_cost += intval($deliveryPriceInPoints * intval($p->qty));
                    }



                    $transaction->quantity += $p->qty;

                    // Record TransactionItem

                    $ti = new TransactionItem();

                    $ti->total_amount = $PriceInUsd * floatval($p->qty);
                    $ti->original_total_amount = $originalPrice * $p->qty;
                    $ti->currency_id = $product->currency_id;
                    $ti->product_id = $product->id;
                    $ti->product_model = $product->model;
                    $ti->product_brand_id = $product->brand_id;
                    $ti->product_sub_model = $product->sub_model;
                    $ti->product_category_id = $product->category_id;
                    $ti->delivery_option = $product->delivery_options;
                    $ti->price_in_points = $originalPriceInPoints * floatval($p->qty);
                    $ti->network_id = $networkId;
//                    $ti->delivery_cost		= $deliveryPriceInPoints * floatval($p->qty);
                    if ($product->is_voucher == 1) {
                        $ti->delivery_cost = $deliveryPriceInPoints;
                    } else {
                        $ti->delivery_cost = $deliveryPriceInPoints * floatval($p->qty);
                    }
                    $ti->quantity = $p->qty;
                    $ti->transaction_id = $transaction->id;

                    $ti->save();

                    // Saving redemption order
                    // calculate the cash payment by item
                    $totalPaidInCash = $transaction->cash_payment;
                    $itemPriceInPoints = $ti->price_in_points;
                    $ratio = $itemPriceInPoints / $sum_item_price_in_points;
                    $cash_paid_by_item = $totalPaidInCash * $ratio;
                    $itemFullPriceInUsd = $ti->total_amount + $cash_paid_by_item;
                    if (!empty($ti->amt_delivery)) {
                        $redOrderDeliveryCost = $ti->amt_delivery;
                    } else {
                        $redOrderDeliveryCost = 0;
                    }
                    $redemptionOrederParams = array(
                        'trx_id' => $transaction->id,
                        'trx_item_id' => $ti->id,
                        'product_id' => $product->id,
                        'product_name' => $product->name,
                        'product_model' => $product->model,
                        'product_sub_model' => $product->sub_model,
                        'full_price_in_usd' => $itemFullPriceInUsd,
                        'supplier_id' => $product->supplier_id,
                        'cost' => $itemFullPriceInUsd,
                        'cost_currency' => 6, //$tid->currency_id,
                        'delivery_cost' => $redOrderDeliveryCost,
                        'delivery_cost_currency' => $ti->currency_id,
                        'quantity' => $ti->quantity,
                        'user_id' => $transaction->user_id,
                        'cash_paid' => $cash_paid_by_item
                    );
                    RedemptionOrder::createRedemptionOrder($redemptionOrederParams, 'Item');
                    // Check stock levels
                    if (($product->qty - $p->qty) < 0) {
                        $product->qty = 0;
                    } else {
                        $product->qty = (int) ($product->qty - $p->qty);
                    }

                    // Update product

                    $product->save();

                    if ($product->qty <= Config::get('blu.stock_level_warning')) {
                        $product->sendStockLevelWarning();
                    }
                }
            }
            //add delivery cost in usd to total_amount
            $networkCurrency = Currency::where('id', $transaction->currency_id)->first();
            $deliveryCostInPoints = ceil($transaction->delivery_cost);
            $deliveryCostInUSD = PointsHelper::pointsToAmount($deliveryCostInPoints, 'USD', $params['network']->id);
            $deliveryCostInOriginalCurrency = PointsHelper::pointsToAmount($deliveryCostInPoints, $networkCurrency->short_code, $params['network']->id);
//            $dev_cost = intval($transaction->delivery_cost)/100;
            $transaction->total_amount = $transaction->total_amount + $deliveryCostInUSD;
            $transaction->original_total_amount = $transaction->original_total_amount + $deliveryCostInOriginalCurrency;

            // Add Delivery Cost to Points Redeemed
            $transaction->points_redeemed += ceil($transaction->delivery_cost);

            // Remove points paid by cash from Points
            if ($remainingPoints > 0) {
                $transaction->points_redeemed -= $remainingPoints;
            }
        }

        // Are we redeeming points?
        if (isset($params['points'])) {
            $networkCurrency = Currency::where('id', $transaction->currency_id)->first();
            $transaction->points_redeemed = ceil($params['points']);
            $partner = $params['partner'];

            if ($points_type == 'cashback') {

                $transaction->total_amount = PointsHelper::pointsToAmount($params['points'], 'USD', $params['network']->id, 'cashback', $partner->id);
                $transaction->original_total_amount = PointsHelper::pointsToAmount($params['points'], $networkCurrency->short_code, $params['network']->id, 'cashback', $partner->id);
                $transaction->partner_amount = PointsHelper::pointsToAmount($params['points'], $networkCurrency->short_code, $params['network']->id, 'cashback', $partner->id);
            } elseif ($points_type == 'miles') {

                $transaction->total_amount = 0; //PointsHelper::pointsToAmount($params['points'], 'USD', $params['network']->id,'miles',$partner->id);
                $transaction->original_total_amount = 0; //PointsHelper::pointsToAmount($params['points'], $networkCurrency->short_code, $params['network']->id,'miles',$partner->id);
                $transaction->partner_amount = 0; //PointsHelper::pointsToAmount($params['points'], $networkCurrency->short_code, $params['network']->id,'miles',$partner->id);
            } else {
                $transaction->total_amount = PointsHelper::pointsToAmount($params['points'], 'USD', $params['network']->id, null, $partner->id);
                $transaction->original_total_amount = PointsHelper::pointsToAmount($params['points'], $networkCurrency->short_code, $params['network']->id, null, $partner->id);
                $transaction->partner_amount = PointsHelper::pointsToAmount($params['points'], $networkCurrency->short_code, $params['network']->id, null, $partner->id);
            } { // if transfer set total amount to zero
                if (isset($params['type']) && $params['type'] == 'transfer') {
                    $transaction->total_amount = 0;
                }
            }
        }

        // Are we redeeming an amount?

        if (isset($params['amount'])) {
            if (isset($params['redeem_points'])) {
                $transaction->points_redeemed = $params['redeem_points'];
            } else {
                $transaction->points_redeemed = ceil(PointsHelper::amountToPoints($params['amount'], $params['currency'], $networkId));
            }

            $currency = Currency::where('short_code', $params['currency'])->first();
            $transaction->currency_id = $currency->id;
            $transaction->exchange_rate = $currency->latestPrice()->rate;
            $transaction->original_total_amount = $params['amount'];
            $transaction->total_amount = floatval($params['amount']) / floatval($transaction->exchange_rate);
        }

        // Are we redeeming a travel ?
//		 if( isset($params['travel_type']) && ( $params['travel_type'] == 'car' ) ){
//			 $transaction->points_redeemed = $params['points'];
//		 }
//
//

        if ($transaction->auth_staff_id) {

            $staff_member = self::validStaffMember($transaction->auth_staff_id);

            if ($staff_member) {
                $transaction->auth_staff_id = $staff_member->id;
                $transaction->auth_role_id = $staff_member->roles()->first()->id;
            }
        }

        // Process coupons

        if (!empty($params['coupon_code']) && trim($params['coupon_code']) !== "") {
            $coupon = App\Coupon::where('coupon_code', $params['coupon_code'])->first();
            $couponType = $coupon->coupon_type;

            $couponDetails = CouponHelper::CouponPrice($params['coupon_code'], $transaction->total_amount, $transaction->partner_id);
            $couponPrice = $couponDetails['price'];
            $couponCurrencyId = $couponDetails['currency'];

            if (!empty($couponCurrencyId)) {
                $couponCurrencyDetails = Currency::where('id', $couponCurrencyId)->first();
                $couponPriceInUsd = $couponPrice / $couponCurrencyDetails->latestPrice()->rate;
                $couponCurrencyShortCode = $couponCurrencyDetails->short_code;
            } else {
                $couponPriceInUsd = $couponPrice;
                $couponCurrencyShortCode = 'USD';
            }
            $coupnValidFrom = $coupon->valid_from();
            $coupnValidTo = $coupon->valid_to();
            $couponPartner = $coupon->partner_id;
            $currentDate = date('Y-m-d');
            if (strtotime($coupnValidFrom) <= strtotime($currentDate) && strtotime($currentDate) <= strtotime($coupnValidTo)) {
                ;
            } else {
                DB::rollback();
                TempTransactionController::deleteTempTransaction($user->id);
                return self::respondWith(
                                Transaction::TRANS_FAILED, "The coupon is Expired!", $transaction
                );
            }

            if ($couponPartner != $transaction->partner_id) {
                DB::rollback();
                TempTransactionController::deleteTempTransaction($user->id);
                return self::respondWith(
                                500, "This coupon is issued by " . Partner::find($couponPartner)->name, $transaction
                );
            }

            if (empty($coupon->transaction_id)) {
                if ($couponType == 'value') {
                    $couponPriceInPoints = PointsHelper::amountToPoints($couponPrice, $couponCurrencyShortCode, $networkId);
                    $user->coupon_price = $couponPriceInPoints;
                    $transaction->points_redeemed = ($transaction->points_redeemed - $couponPriceInPoints);
                } else {
//					$couponActualPrice				= ( $params['amount'] * $couponPriceInUsd ) / 100;
                    $couponActualPrice = $couponPriceInUsd;
                    $couponPriceInPoints = PointsHelper::amountToPoints($couponActualPrice, $params['currency'], $networkId);
                    $user->coupon_price = $couponActualPrice;
                    $transaction->points_redeemed = $transaction->points_redeemed - $couponPriceInPoints;
                }

                $transaction->coupon_id = (int) $coupon->id;

                if ($transaction->points_redeemed < 0) {
                    $transaction->points_redeemed = 0;
                }

                // Assign the coupon
                DB::table('coupon')->where('coupon_code', $params['coupon_code'])->update(array(
                    'transaction_id' => $transaction->id
                ));
            } else {
                DB::rollback();
                TempTransactionController::deleteTempTransaction($user->id);
                return self::respondWith(
                                500, "The coupon is already used for another transaction!", $transaction
                );
            }
        }

        // ---------------------------------------------------------------------
        //
        // Network Points Conversion Process
        //
        // ---------------------------------------------------------------------
        $user_balances = $user->balances();
        $networks = array();
        $trxNewtork = Network::where('id', '=', $transaction->network_id)->take(1)->first();
        // Is this transaction on a BLU network?
        if ($transaction->network_id == '1') {
            $networks[] = '1';
        } else {

            if ($trxNewtork->do_points_conversion) {
                $networks[] = $transaction->network_id;
                $networks[] = '1';
            } else {
                $networks[] = $transaction->network_id;
            }
        }

        /*
         * Here we construct a queue of network IDS in order of priority to try
         * transact against. The user balances are already ordered by date
         * updated to prioritize the oldest ones
         */
        if ($trxNewtork->do_points_conversion) {
            //echo "<pre> User Balances <br> ";
//            var_dump($user_balances);
//            exit();
            foreach ($user_balances as $balance) {
                $trxNewtork1 = Network::where('id', '=', $balance->network_id)->take(1)->first();
//                echo "<pre>Transaction";
//                var_dump($balance->network_id);
//                echo "Networks";
//                var_dump($trxNewtork1->do_points_conversion);
//                exit();
                if (!in_array($balance->network_id, $networks) && $balance->network_id != '0' && $trxNewtork1->do_points_conversion == '1') {
                    array_push($networks, $balance->network_id);
                }
            }
        }



        $total_rundown = $transaction->points_redeemed;

        /*
         * Here we attempt to deduct the maximum amount of points from a member
         * on each network that they belong to by proving the network where the
         * transaction occurred and then the network the transaction is
         * attempting to run against
         */
        $count_networks = 0;
        $rundown_ralph = array();
        $transaction_network = Network::where('id', '=', $transaction->network_id)->take(1)->first();

        foreach ($networks as $balance_network) {
            $attempt_network = Network::where('id', '=', $balance_network)->take(1)->first();
            if ($attempt_network->do_points_conversion || $attempt_network->id == $transaction_network->id) {
                $total_rundown -= $user->deductMaxPoints($total_rundown, $transaction->network_id, $balance_network);

                $rundown_ralph[$count_networks]['balance_network'] = $balance_network;
                $rundown_ralph[$count_networks]['trx_net_id'] = $transaction->network_id;
                $rundown_ralph[$count_networks]['total_rundown'] = $total_rundown;
                $count_networks += 1;
            }
            if ($total_rundown <= 0) {
                break;
            }
        }


        if ($paidCash == 1) {
            // Remove points paid by cash from Points
            if ($remainingPoints > 0) {
                $transaction->points_redeemed -= $remainingPoints;
                $temp_view_pts_redeemed = $transaction->points_redeemed;
                //$transaction->points_redeemed	 = abs($transaction->points_redeemed);
                $total_rundown = 0;
            }
        }
        /*
         * We check to see if the total required points could not collectively be
         * deduced from the networks the member belong to. If successful, there
         * should be no points remaining in the rundown
         */
        if ($total_rundown > 0) {
            DB::rollback();
            TempTransactionController::deleteTempTransaction($user->id);
            return self::respondWith(
                            Transaction::TRANS_FAILED, "The member has an insufficient networks points balance for this transaction.", $rundown_ralph
            );
        }

        // Update the transaction
        $networkCurrencyId = $params['network']->currency_id;
        $networkCurrency = Currency::where('id', $networkCurrencyId)->first();
        $networkCurrencyShortCode = $networkCurrency->short_code;
        $transaction->amt_redeem = number_format(PointsHelper::pointsToAmount($transaction->points_redeemed, $networkCurrencyShortCode, $params['network']->id), 2);

        $transaction->save();
        DB::commit();

        $url_trx_api = env('APP_URL').'/api/deduct-transaction-max?api_key=c4ca4238a0b923820dcc509a6f75849b&transaction_id=' . $transaction->id;
        $url = url($url_trx_api);

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, False);
        $result = curl_exec($ch);
        curl_close($ch);

        // Commented manually
//        $sendgrid = new SendGrid(
//                Config::get('sendgrid.username'), Config::get('sendgrid.password')
//        );
//        $arr_email = array();
//        $arr_email["network"] = $networkCurrencyId;
//        $arr_email["partner"] = $partner->id;
//        $arr_email['amt_rede'] = json_encode($transaction->toArray());
//        $arr_email['returnMaxDeduction'] = $returnMaxDeduction;
//        $arr_email['user'] = $user->id;
//        $arr_email['balance'] = json_encode($user_balances);
//        $mail = new SendGrid\Email();
//
//        $mail->setTos(array('adminit@bluloyalty.com'));
//        $mail->setFrom(Config::get('blu.email_from'));
//        $mail->setFromName('BLU Points');
//        $mail->setSubject("Return Max Deduction" . $transaction->id);
//        $mail->setHtml(json_encode($arr_email) . "xxxxx" . $returnMaxDeduction);
//
//        $sendgrid->send($mail);
        // Process the loyalty program

        $transaction = Loyalty::processMatchingRedemptionRule($transaction);

        // Create the order for reference information

        $order = new Order();

        $order->transaction_id = $transaction->id;
        $order->user_id = $transaction->user_id;

        $order->save();

        // Send the notifications
        $outputRalph = '';
        if (strtolower($trans_class) != "flight") {
            if ($transaction->points_redeemed > 0 || $paidCash == 1) {
                //Audit Log for Redemption
                AuditHelper::record($transaction->partner_id, "Redemption | Member '{$user->id} / {$user->email}' | Transaction Id: '{$transaction->id}'" . " on " . date('Y-m-d H:i:s'));
//            if($transaction->partner_id !== '3'){
                if (!strpos($transaction->ref_number, "-- Card Deduction")) {
                    try {
                        if ($eticket == '0') {
                            // commented manually
                            $outputRalph = self::sendNotifications($user, $transaction, Transaction::REDEEM_TRANS);
                        }
                    } catch (Exception $ex) {

//                    Commented manually
//                    $sendgrid1 = new SendGrid(
//                            Config::get('sendgrid.username'), Config::get('sendgrid.password')
//                    );
//                    $arr_email1 = array();
//                    $arr_email1["network"] = $networkCurrencyId;
//                    $arr_email1["partner"] = $partner->id;
//                    $arr_email1['amt_rede'] = $transaction->amt_redeem;
//                    $arr_email1['returnMaxDeduction'] = $returnMaxDeduction;
//                    $arr_email1['user'] = $user->id;
//                    $arr_email1['balance'] = json_encode($user_balances);
//                    $arr_email1['exception'] = $ex;
//                    $mail1 = new SendGrid\Email();
//
//                    $mail1->setTos(array('adminit@bluloyalty.com'));
//                    $mail1->setFrom(Config::get('blu.email_from'));
//                    $mail1->setFromName('BLU Points');
//                    $mail1->setSubject("doRedemption Send Notification " . $transaction->id);
//                    $mail1->setHtml(json_encode($arr_email1) . "xxxxx" . $returnMaxDeduction);
//
//                    $sendgrid1->send($mail1);

                        NotificationController::sendEmailNotification(json_encode($arr_email1) . "xxxxx", 'BLU Points', Config::get('blu.email_from'), array(null), array('adminit@bluloyalty.com', Config::get('blu.admin_email')), "doRedemption Send Notification " . $transaction->id);
                    }
                }
//            }
            }
        }

        // Send confirmation to BLU Admins
//        if($transaction->partner_id !== '3'){
        if ($eticket == '0' || $sendHitchhikerEmails == 1) {
            if (isset($userCPT) && !empty($userCPT)) {
                $user->CPT = $userCPT;
            }
            try {
                if ($sendHitchhikerEmails == 1) {
                    /* Send Order Notification to BLU Admin */
                    self::sendOrderNotification($transaction, $user, "", "", "order", "v3");
                    self::sendNotifications($user, $transaction, Transaction::REDEEM_TRANS, "v3", "booking-confirmation");
                  
                } else {
                    $response = self::sendOrderNotification($transaction, $user);
                    return $response;
                }
            } catch (Exception $ex) {

//                Commented manually
//                $sendgrid1 = new SendGrid(
//                        Config::get('sendgrid.username'), Config::get('sendgrid.password')
//                );
//                $arr_email1 = array();
//                $arr_email1["network"] = $networkCurrencyId;
//                $arr_email1["partner"] = $partner->id;
//                $arr_email1['amt_rede'] = $transaction->amt_redeem;
//                $arr_email1['returnMaxDeduction'] = $returnMaxDeduction;
//                $arr_email1['user'] = $user->id;
//                $arr_email1['balance'] = json_encode($user_balances);
//                $arr_email1['exception'] = $ex;
//                $mail1 = new SendGrid\Email();
//
//                $mail1->setTos(array('adminit@bluloyalty.com'));
//                $mail1->setFrom(Config::get('blu.email_from'));
//                $mail1->setFromName('BLU Points');
//                $mail1->setSubject("doRedemption Order Nofication " . $transaction->id);
//                $mail1->setHtml(json_encode($arr_email1) . "xxxxx" . $returnMaxDeduction);
//
//                $sendgrid1->send($mail1);

                NotificationController::sendEmailNotification(json_encode($arr_email1) . "xxxxx", 'BLU Points', Config::get('blu.email_from'), array(null), array('adminit@bluloyalty.com', Config::get('blu.admin_email')), "doRedemption Order Nofication " . $transaction->id);
            }
        }
//        }
        AuditHelper::record($transaction->partner_id, "Redemption 112 | Member '{$user->id} / {$user->email}' | Transaction Id: '{$transaction->id}'" . " on " . date('Y-m-d H:i:s'));
        $result_ralph = array();
        $result_ralph['trx'] = $transaction->toArray();
        //$result_ralph['ralph'] = $output;\
        //deduct max transactions
//        Commented manually
//        $sendgrid1 = new SendGrid(
//                Config::get('sendgrid.username'), Config::get('sendgrid.password')
//        );
        $arr_email1 = array();
        $arr_email1["network"] = $networkCurrencyId;
        $arr_email1["partner"] = $partner->id;
        $arr_email1['amt_rede'] = $transaction->amt_redeem;
        // $arr_email1['returnMaxDeduction'] = $returnMaxDeduction;
        $arr_email1['user'] = $user->id;
        $arr_email1['balance'] = json_encode($user_balances);

//        Commented manually
//        $mail1 = new SendGrid\Email();
//
//        $mail1->setTos(array('adminit@bluloyalty.com'));
//        $mail1->setFrom(Config::get('blu.email_from'));
//        $mail1->setFromName('BLU Points');
//        $mail1->setSubject("End doRedemption Return Max Deduction" . $transaction->id);
//        $mail1->setHtml(json_encode($arr_email1) . "xxxxx" . $returnMaxDeduction);
//
//        $sendgrid1->send($mail1);


        NotificationController::sendEmailNotification(json_encode($arr_email1) . "xxxxx", 'BLU Points', Config::get('blu.email_from'), array(null), array('adminit@bluloyalty.com'), "End doRedemption Return Max Deduction" . $transaction->id);


        // $result_ralph['ralph'] = $returnMaxDeduction;
        // Send confirmation to BLU Admins
        TempTransactionController::deleteTempTransaction($user->id);
        return self::respondWith(
                        Transaction::TRANS_SUCCESS, "success", $result_ralph
        );
    }

    /*
     * Reverse transactions using the trans_class reversal
     *
     * @params trx_id
     * @params trans_class
     *
     * return success
     */

    public static function doReversal($trx_id, $trans_class = "Reversal") {
        try {
            if (!isset($trx_id) || empty($trx_id)) {
                return self::respondWith(
                                6001, "Failed", null
                );
            }

            $transaction = Transaction::find($trx_id);
            $trans_reversal = new Transaction();
            DB::beginTransaction();
            $trans_reversal->user_id = $transaction->user_id;
            $trans_reversal->country_id = $transaction->country_id;
            $trans_reversal->partner_id = $transaction->partner_id;
            $trans_reversal->trans_class = $trans_class;
            $trans_reversal->source = $transaction->source;
            $trans_reversal->ref_number = $trx_id;
            $trans_reversal->notes = $transaction->notes;
            $trans_reversal->store_id = $transaction->store_id;
            $trans_reversal->pos_id = $transaction->pos_id;
            $trans_reversal->auth_staff_id = Auth::user()->id;
            $trans_reversal->network_id = $transaction->network_id;
            $trans_reversal->rule_id = $transaction->rule_id;
            $trans_reversal->total_amount = $transaction->total_amount * (-1);
            $trans_reversal->original_total_amount = $transaction->original_total_amount * (-1);
            $trans_reversal->partner_amount = $transaction->partner_amount * (-1);
            $trans_reversal->points_rewarded = $transaction->points_rewarded * (-1);
            $trans_reversal->points_redeemed = $transaction->points_redeemed * (-1);
            $trans_reversal->points_balance = null;
            $trans_reversal->trx_reward = $transaction->trx_reward;
            $trans_reversal->trx_redeem = $transaction->trx_redeem;
            $trans_reversal->amt_reward = $transaction->amt_reward * (-1);
            $trans_reversal->amt_redeem = $transaction->amt_redeem * (-1);
            $trans_reversal->currency_id = $transaction->currency_id;
            $trans_reversal->partner_currency_id = $transaction->partner_currency_id;
            $trans_reversal->exchange_rate = $transaction->exchange_rate;
            $trans_reversal->product_id = $transaction->product_id;
            $trans_reversal->product_model = $transaction->product_model;
            $trans_reversal->product_brand_id = $transaction->product_brand_id;
            $trans_reversal->product_sub_model = $transaction->product_sub_model;
            $trans_reversal->product_category_id = $transaction->product_category_id;
            $trans_reversal->city_id = $transaction->city_id;
            $trans_reversal->area_id = $transaction->area_id;
            $trans_reversal->segment_id = $transaction->segment_id;
            $trans_reversal->category_id = $transaction->category_id;
            $trans_reversal->quantity = $transaction->quantity * (-1);
            $trans_reversal->auth_role_id = $transaction->auth_role_id;
            // set the created at and updated as new default values not as old transaction
            //        $trans_reversal->created_at				= $transaction->created_at;
            //        $trans_reversal->updated_at				= $transaction->updated_at;
            $trans_reversal->delivery_cost = $transaction->delivery_cost * (-1);
            $trans_reversal->source = $transaction->source;
            $trans_reversal->cash_payment = $transaction->cash_payment * (-1);
            $trans_reversal->reversed = 0;
            $trans_reversal->posted_at = date('Y-m-d H:i:s');

            $user = User::where('id', $trans_reversal->user_id)->first();
            if ($trans_reversal->trx_reward == '1') {
                if ($user->balance($trans_reversal->network_id) >= $transaction->points_rewarded) {
                    DB::table('transaction')->where('id', $trx_id)->update(array('reversed' => 1));
                    $user->updateBalance($trans_reversal->points_rewarded, $trans_reversal->network_id, 'add');
                    $trans_reversal->save();
                } else {
                    return self::respondWith(
                                    600, "Failed", null
                    );
                }
            } elseif ($trans_reversal->trx_redeem == '1') {
                $points_to_deduct = $transaction->points_redeemed * (-1);
                DB::table('transaction')->where('id', $trx_id)->update(array('reversed' => 1));
                $user->updateBalance($trans_reversal->points_redeemed, $trans_reversal->network_id, 'subtract');
                $trans_reversal->save();
                $last_inserted_reversal = $trans_reversal->id;
                //            $trx_rewards = Transaction::where('user_id',$transaction->user_id)->where('trx_reward','1')->where('network_id',$transaction->network_id)->where("voided",0)->orderBy('id','ASC')->get();
                //            $trx_rewards_arr = array();
                //            foreach($trx_rewards as $t){
                //                $trx_rewards_arr[] = $t->id;
                //            }
                $trx_used_deduction = DB::table('transaction_deduction')->select(DB::raw('DISTINCT(trx_id)'))
                                ->where('trx_ref_id', $trx_id)->get();
                $trx_used_deduction_arr = array();
                $got_first = false;

                //            foreach($trx_used_deduction as $val){
                //                if($got_first == false){
                //                    $first_trx_used_deduction = $val;
                //                    $got_first = true;
                //                }
                //                $trx_used_deduction_arr[] = $val->trx_id;
                //            }
                //            if(empty($trx_used_deduction_arr)){
                //                $trx_used_deduction_arr[] = 0;
                //            }
                //$trx_used_deduction_arr = $trx_used_deduction->toArray();
                //$trx_unused_deduction = Transaction::whereIn('id',$trx_rewards_arr)->whereNotIn('id', $trx_used_deduction_arr)->orderBy('id','asc')->get();
                $change_last_trx_unused = true;
                foreach ($trx_used_deduction as $tud) {
                    //                echo "<pre>";
                    //                var_dump($tud);
                    //                exit();
                    $trx_deduction = new TransactionDeduction();
                    $trx_deduction->trx_ref_id = $last_inserted_reversal;
                    $trx_deduction_temp = TransactionDeduction::where('trx_id', $tud->trx_id)->where('trx_used', 0)->orderBy('id', 'DESC')->first();
                    $trx_deduction_temp_used = TransactionDeduction::where('trx_id', $tud->trx_id)->where('trx_used', 1)->orderBy('id', 'DESC')->first();
                    $trx_deduction_redemption = TransactionDeduction::where('trx_id', $tud->trx_id)->where('trx_ref_id', $trx_id)->orderBy('id', 'DESC')->first();

                    if ($trx_deduction_temp && !$trx_deduction_temp_used) {
                        $change_last_trx_unused = false;
                        $trx_int_deduction_pts_remaining = intval($trx_deduction_temp->points_remaining);
                        $trx_int_deduction_pts_used = intval($trx_deduction_redemption->points_used) * (-1);
                        $trx_deduction->points_remaining = $trx_int_deduction_pts_remaining - $trx_int_deduction_pts_used;
                        $trx_deduction->points_used = $trx_deduction_redemption->points_used * (-1);
                        $trx_deduction->trx_id = $tud->trx_id;
                        $trx_deduction->trx_used = 0; //0
                        $trx_deduction->used_date = date("Y-m-d H:i:s");
                        $trx_deduction->partner_id = Transaction::find($tud->trx_id)->partner_id;
                        $trx_deduction->save();
                    } elseif ($trx_deduction_temp_used) {
                        $change_last_trx_unused = false;
                        //$trx_int_deduction_pts_remaining = intval($trx_deduction_temp->points_remaining);
                        $trx_int_deduction_pts_used = intval($trx_deduction_redemption->points_used);
                        $trx_deduction->points_remaining = $trx_int_deduction_pts_used;
                        $trx_deduction->points_used = $trx_deduction_redemption->points_used * (-1);
                        $trx_deduction->trx_id = $tud->trx_id;
                        $trx_deduction->trx_used = 0; //0
                        $trx_deduction->used_date = date("Y-m-d H:i:s");
                        $trx_deduction->partner_id = Transaction::find($tud->trx_id)->partner_id;
                        $trx_deduction->save();
                        $trx_deduction_temp_used->trx_used = 0;
                        $trx_deduction_temp_used->updated_at = date("Y-m-d H:i:s");
                        $trx_deduction_temp_used->save();
                    }
                }

                //DB::table('transaction_deduction')->where('trx_ref_id', $trx_id)->;

                $trx_items = DB::table('transaction_item')->where('transaction_id', $trx_id)->get();
                foreach ($trx_items as $t_item) {
                    //                $product = Product::find($t_item->product_id);
                    $product = ProductsHelper::productRedemptionDetails($t_item->product_id, $transaction->country_id, $transaction->partner_id);
                    $product->qty = $product->qty + $t_item->quantity;
                    $product->save();

                    //insert reversed items
                    $reversed_item = new TransactionItem();
                    $reversed_item->total_amount = $t_item->total_amount * -1;
                    $reversed_item->original_total_amount = $t_item->original_total_amount * (-1);
                    $reversed_item->currency_id = $t_item->currency_id;
                    $reversed_item->product_id = $t_item->product_id;
                    $reversed_item->product_model = $t_item->product_model;
                    $reversed_item->product_brand_id = $t_item->product_brand_id;
                    $reversed_item->product_sub_model = $t_item->product_sub_model;
                    $reversed_item->product_category_id = $t_item->product_category_id;
                    $reversed_item->delivery_option = $t_item->delivery_option;
                    $reversed_item->quantity = $t_item->quantity * -1;
                    $reversed_item->transaction_id = $last_inserted_reversal;
                    $reversed_item->price_in_points = $t_item->price_in_points * -1;
                    $reversed_item->network_id = $t_item->network_id;
                    $reversed_item->paid_in_points = $t_item->paid_in_points * -1;
                    $reversed_item->paid_in_cash = $t_item->paid_in_cash * -1;

                    $reversed_item->save();

                    $trx_item_temp = TransactionItem::where('id', $t_item->id)->first();
                    $trx_item_temp->reversed = 1;
                    $trx_item_temp->reversed_at = date("Y-m-d H:i:s");
                    $trx_item_temp->save();
                    $trxs_delivery = TransactionItemDelivery::where('trx_item_id', $t_item->id)->first();
                    $reversed_item_delivery = new TransactionItemDelivery();
                    $reversed_item_delivery->trx_id = $last_inserted_reversal;
                    $reversed_item_delivery->trx_item_id = $t_item->id;
                    $reversed_item_delivery->delivery_cost = $trxs_delivery->delivery_cost * (-1);
                    $reversed_item_delivery->network_id = $t_item->network_id;
                    $reversed_item_delivery->amt_delivery = $trxs_delivery->amt_delivery * (-1); //$transactionDelivery->network_id;
                    $reversed_item_delivery->currency_id = $t_item->currency_id;
                    $reversed_item_delivery->reversed = 0;
                    $reversed_item_delivery->quantity = $trxs_delivery->quantity * (-1);
                    $reversed_item_delivery->ref_number = $trxs_delivery->id;

                    $reversed_item_delivery->save();
                    //$trx_item_delivery_temp = TransactionItemDelivery::where('id',$trxs_delivery->id)->first();
                    $trxs_delivery->reversed = 1;
                    $trxs_delivery->reversed_at = date("Y-m-d H:i:s");
                    $trxs_delivery->save();

                    //reverse points issued allocation
                    $pointsIssuedAllocationItems = PointsIssuedAllocation::where('trx_item_id', $t_item->id)->get();
                    foreach ($pointsIssuedAllocationItems as $pointsIssuedAllocationItem) {
                        $pointsIssuedAllocation = array();
                        $pointsIssuedAllocation['trx_deduction_id'] = $pointsIssuedAllocationItem->trx_deduction_id;
                        $pointsIssuedAllocation['trx_item_id'] = $pointsIssuedAllocationItem->trx_item_id;
                        $pointsIssuedAllocation['partner_id'] = $pointsIssuedAllocationItem->partner_id;
                        $pointsIssuedAllocation['created_at'] = date("Y-m-d H:i:s");
                        $pointsIssuedAllocation['updated_at'] = date("Y-m-d H:i:s");
                        $pointsIssuedAllocation['points_used'] = $pointsIssuedAllocationItem->points_used * (-1);
                        DB::table('points_issued_allocation')->insert($pointsIssuedAllocation);
                    }
                    //					DB::table('transaction_item')->where('transaction_id', $trx_id)->update(array( 'total_amount' => ( $t_item->total_amount*-1 ) ));
                }

                //            DB::table('transaction_item')->where('transaction_id', $trx_id)->delete();
            }
            //reverse Full redemption order
            $params = array();
            $params['trx_id'] = $trx_id;
            RedemptionOrder::reverseRedemptionOrder($params, 'Full');

            DB::commit();
            // check if user is a middlware partner and then if exists in middleware

            $partner = Partner::find($transaction->partner_id);
            /* if($partner->has_middleware == 1){
              $user_ids = array();
              $user_ids[] =  $transaction->user_id;
              $url = $partner->link_middleware_api . '?method=sensitiveInfo&api_key='.$partner->middleware_api_key;
              $json_user_ids = json_encode($user_ids);
              $url = $url."&user_ids=" . $json_user_ids;
              //$user = User::where('id', $user_id)->with('country', 'city', 'area', 'cards')->first();

              $curl = curl_init();
              // Set some options - we are passing in a useragent too here
              curl_setopt_array($curl, array(
              CURLOPT_RETURNTRANSFER => 1,
              CURLOPT_URL => $url,
              CURLOPT_USERAGENT => 'Sensitive Info'
              ));
              // Send the request & save response to $resp
              $resp = curl_exec($curl);

              $resp_curl = json_decode($resp);
              $user->CPT = $resp_curl[0]->CPT;
              } */
            try {
                // self::sendOrderNotification($trans_reversal, $user);
            } catch (Exception $ex) {

                //            Commented manually
                //            $sendgrid1 = new SendGrid(
                //                    Config::get('sendgrid.username'), Config::get('sendgrid.password')
                //            );
                $arr_email1 = array();

                $arr_email1["partner"] = $partner->id;
                $arr_email1['amt_rede'] = $transaction->amt_redeem;
                $arr_email1['user'] = $user->id;
                $arr_email1['exception'] = $ex;

                //            Commented manually
                //            $mail1 = new SendGrid\Email();
                //
    //            $mail1->setTos(array('adminit@bluloyalty.com'));
                //            $mail1->setFrom(Config::get('blu.email_from'));
                //            $mail1->setFromName('BLU Points');
                //            $mail1->setSubject("doReversal Send order Confirmation " . $transaction->id);
                //            $mail1->setHtml(json_encode($arr_email1));
                //
    //            $sendgrid1->send($mail1);
            }

            try {
                //            Commented manually
                //            self::sendNotifications($user, $trans_reversal, Transaction::REVERSE_TRANS);
            } catch (Exception $ex) {

                //            Commented manually
                //            $sendgrid1 = new SendGrid(
                //                    Config::get('sendgrid.username'), Config::get('sendgrid.password')
                //            );
                $arr_email1 = array();

                $arr_email1["partner"] = $partner->id;
                $arr_email1['amt_rede'] = $transaction->amt_redeem;
                $arr_email1['user'] = $user->id;
                $arr_email1['exception'] = $ex;

                //            Commented manually
                //            $mail1 = new SendGrid\Email();
                //
    //            $mail1->setTos(array('adminit@bluloyalty.com'));
                //            $mail1->setFrom(Config::get('blu.email_from'));
                //            $mail1->setFromName('BLU Points');
                //            $mail1->setSubject("doReversal Send Notification " . $transaction->id);
                //            $mail1->setHtml(json_encode($arr_email1));
                //
    //            $sendgrid1->send($mail1);
            }
            //                        self::sendOrderNotification($trans_reversal, $user);
            //                        self::sendNotifications($user, $trans_reversal, Transaction::REVERSE_TRANS);
            //		 if($transaction->points_redeemed > 0 || $paidCash == 1) {
            //            $outputM = self::sendNotifications($user, $transaction, Transaction::REVERSE_TRANS);
            //        }


            $result_ralph = array();
            $result_ralph['trx_reversed'] = $trx_id;
            $result_ralph['cash'] = abs($trans_reversal->cash_payment);
            $result_ralph['currency'] = 'USD';
            return self::respondWith(
                            Transaction::TRANS_SUCCESS, "success", $result_ralph
            );
        } catch (Exception $ex) {
            $json_data = json_encode(array($ex->getMessage(), $ex->getLine()));
            APIController::postSendEmailJson($json_data, "DEV doreversal Try Catch 001 ");
        }
    }

    /**
     * Deduct maximum points from transactions
     *
     * return true
     */
    public static function deductTransactionMax($transaction) {
        $trx_rewards = Transaction::select('id')
                ->where('user_id', $transaction->user_id)
                ->where('trx_reward', '1')
                ->where('network_id', $transaction->network_id)
                ->where("voided", 0)
                ->where("expired", 0)
                ->orderBy('id', 'ASC')
                ->get();

        $arr_trx_rewards = $trx_rewards->toArray();

        $trx_rewards_arr = [];
        foreach ($arr_trx_rewards as $t) {
            $trx_rewards_arr[] = $t['id'];
        }
        if (empty($trx_rewards_arr)) {
            $trx_rewards_arr[] = 0;
        }

        $trx_used_deduction = DB::table('transaction_deduction')
                ->select(DB::raw('DISTINCT(trx_id)'))
                ->whereIn('trx_id', $trx_rewards_arr)
                ->where('trx_used', 1)
                ->get();

        $trx_used_deduction_arr = [];
        foreach ($trx_used_deduction as $val) {
            $trx_used_deduction_arr[] = $val->trx_id;
        }
        if (empty($trx_used_deduction_arr)) {
            $trx_used_deduction_arr[] = 0;
        }

        $trx_unused_deduction = Transaction::select('id', 'points_rewarded', 'partner_id')
                ->whereIn('id', $trx_rewards_arr)
                ->whereNotIn('id', $trx_used_deduction_arr)
                ->get();

        $trx_deductions = TransactionDeduction::select('trx_id', 'points_remaining')
                ->whereIn('trx_id', $trx_rewards_arr)
                ->whereNotIn('trx_id', $trx_used_deduction_arr)
                ->get();

        $deduction_transaction_data = [];
        foreach ($trx_deductions as $item) {
            $deduction_transaction_data[$item->trx_id]['trx_id'] = $item->trx_id;
            $deduction_transaction_data[$item->trx_id]['points_remaining'] = $item->points_remaining;
        }

        $tot_amount = $transaction->points_redeemed;
        $sum_unused_points = 0;

        foreach ($trx_unused_deduction as $tnu) {
            if (isset($deduction_transaction_data[$tnu->id]) && !empty($deduction_transaction_data[$tnu->id])) {
                $sum_unused_points += intval($deduction_transaction_data[$tnu->id]['points_remaining']);
            }

            if ($tnu) {
                $sum_unused_points += intval($tnu->points_rewarded);
            }
        }

        if ($sum_unused_points >= $tot_amount) {
            foreach ($trx_unused_deduction as $tnu) {
                if ($tot_amount > 0) {

                    $trx_deduction = new TransactionDeduction();
                    $trx_deduction->trx_ref_id = $transaction->id;

                    $trx_deduction_temp = TransactionDeduction::where('trx_id', $tnu->id)
                            ->where('trx_used', 0)
                            ->orderBy('id', 'DESC')
                            ->first();

                    $trx_deduction_temp_count = TransactionDeduction::where('trx_id', $tnu->id)
                            ->where('trx_used', 1)
                            ->count();
                    $bool_passed = false;

                    if ($trx_deduction_temp && $trx_deduction_temp_count == 0) {
                        $trx_int_deduction_pts_remaining = intval($trx_deduction_temp->points_remaining);

                        if ($tot_amount >= $trx_int_deduction_pts_remaining) {
                            $tot_amount -= $trx_int_deduction_pts_remaining;
                            $trx_deduction->points_remaining = 0;
                            $trx_deduction->points_used = $trx_int_deduction_pts_remaining;
                            $trx_deduction->trx_id = $tnu->id;
                            $trx_deduction->trx_used = 1;
                            $trx_deduction->used_date = date("Y-m-d H:i:s");
                            $trx_deduction->created_at = date("Y-m-d H:i:s");
                            $trx_deduction->updated_at = date("Y-m-d H:i:s");
                            $trx_deduction->partner_id = $tnu->partner_id; //Transaction::find($tnu->id)->partner_id;
                        } else {
                            $trx_deduction->points_remaining = $trx_int_deduction_pts_remaining - $tot_amount;
                            $trx_deduction->points_used = $tot_amount;
                            $trx_deduction->trx_id = $tnu->id;
                            $trx_deduction->trx_used = 0; //0
                            $trx_deduction->used_date = date("Y-m-d H:i:s");
                            $trx_deduction->created_at = date("Y-m-d H:i:s");
                            $trx_deduction->updated_at = date("Y-m-d H:i:s");
                            $trx_deduction->partner_id = $tnu->partner_id; //Transaction::find($tnu->id)->partner_id;
                            $tot_amount = 0;
                        }
                        $bool_passed = true;
                    } elseif ($trx_deduction_temp_count == 0) {
                        if ($tnu) {
                            $trx_int_deduction_pts_remaining = intval($tnu->points_rewarded);
                            if ($tot_amount >= $trx_int_deduction_pts_remaining) {
                                $tot_amount -= $trx_int_deduction_pts_remaining;
                                $trx_deduction->points_remaining = 0;
                                $trx_deduction->points_used = $trx_int_deduction_pts_remaining;
                                $trx_deduction->trx_id = $tnu->id;
                                $trx_deduction->trx_used = 1; //1
                                $trx_deduction->used_date = date("Y-m-d H:i:s");
                                $trx_deduction->created_at = date("Y-m-d H:i:s");
                                $trx_deduction->updated_at = date("Y-m-d H:i:s");
                                $trx_deduction->partner_id = $tnu->partner_id; //Transaction::find($tnu->id)->partner_id;
                            } else {
                                $trx_deduction->points_remaining = $trx_int_deduction_pts_remaining - $tot_amount;
                                $trx_deduction->points_used = $tot_amount;
                                $trx_deduction->trx_id = $tnu->id;
                                $trx_deduction->trx_used = 0; //0
                                $trx_deduction->used_date = date("Y-m-d H:i:s");
                                $trx_deduction->created_at = date("Y-m-d H:i:s");
                                $trx_deduction->updated_at = date("Y-m-d H:i:s");
                                $trx_deduction->partner_id = $tnu->partner_id; //Transaction::find($tnu->id)->partner_id;
                                $tot_amount = 0;
                            }
                        }
                        $bool_passed = true;
                    }

                    if ($bool_passed == true) {
                        $trx_deduction->save();
                    }
                }
            }

            $trxDeductions = TransactionDeduction::where('trx_ref_id', $transaction->id)->pluck('points_used', 'id');
            $trxItems = TransactionItem::where('transaction_id', $transaction->id)->get();

            $trxDeductionPtsUsedRemaining = 0;
            $lastIndex = 0;

            foreach ($trxDeductions as $trxDeductionId => $trxDeductionPtsUsed) {
                $trxDeductionDetails = TransactionDeduction::where('id', $trxDeductionId)->first();

                while ($trxDeductionPtsUsed > 0 && !$trxItems->isEmpty()) {

                    foreach ($trxItems as $k => $trxItem) {
                        if ($trxItem->paid_in_points == 0) {
                            $trxDeductionPtsUsed = 0;
                            continue;
                        }
                        $trxItemFullPaidInPoints = $trxItem->paid_in_points + $trxItem->delivery_cost;
                        if ($lastIndex > $k) {
                            continue;
                        }
                        $pointsIssuedAllocation = array();
                        $pointsIssuedAllocation['trx_deduction_id'] = $trxDeductionId;
                        $pointsIssuedAllocation['trx_item_id'] = $trxItem->id;
                        $pointsIssuedAllocation['partner_id'] = $trxDeductionDetails->partner_id;
                        $pointsIssuedAllocation['created_at'] = date("Y-m-d H:i:s");
                        $pointsIssuedAllocation['updated_at'] = date("Y-m-d H:i:s");

                        if ($trxDeductionPtsUsedRemaining < 0) {
                            if ($trxDeductionPtsUsed == abs($trxDeductionPtsUsedRemaining)) {
                                $pointsIssuedAllocation['points_used'] = abs($trxDeductionPtsUsedRemaining);
                                $trxDeductionPtsUsed = 0;
                            } elseif ($trxDeductionPtsUsed < abs($trxDeductionPtsUsedRemaining)) {
                                $pointsIssuedAllocation['points_used'] = $trxDeductionPtsUsed;
                                $trxDeductionPtsUsed = $trxDeductionPtsUsed - abs($trxDeductionPtsUsedRemaining);
                            } else {
                                $pointsIssuedAllocation['points_used'] = abs($trxDeductionPtsUsedRemaining);
                                $trxDeductionPtsUsed = $trxDeductionPtsUsed - $pointsIssuedAllocation['points_used'];
                                $trxDeductionPtsUsedRemaining = 0;
                            }

                            $trxDeductionPtsUsedRemaining = $trxDeductionPtsUsed + $trxDeductionPtsUsedRemaining;

                            DB::table('points_issued_allocation')->insert($pointsIssuedAllocation);
                            if ($trxDeductionPtsUsed < 0) {
                                $lastIndex = $k;
                                unset($trxItems->$k);
                                $trxDeductionPtsUsedRemaining = $trxDeductionPtsUsed;
                                continue 3;
                            }
                            $lastIndex = $k;
                            unset($trxItems->$k);
                            continue;
                        }

                        if ($trxDeductionPtsUsed == $trxItemFullPaidInPoints) {
                            $pointsIssuedAllocation['points_used'] = $trxItemFullPaidInPoints;
                            $trxDeductionPtsUsed = 0;
                        } elseif ($trxDeductionPtsUsed < $trxItemFullPaidInPoints) {
                            $pointsIssuedAllocation['points_used'] = $trxDeductionPtsUsed;
                            $trxDeductionPtsUsed = $trxDeductionPtsUsed - $trxItemFullPaidInPoints;
                        } else {
                            $pointsIssuedAllocation['points_used'] = $trxItemFullPaidInPoints;
                            $trxDeductionPtsUsed = $trxDeductionPtsUsed - $trxItemFullPaidInPoints;
                            $trxDeductionPtsUsedRemaining = 0;
                        }

                        DB::table('points_issued_allocation')->insert($pointsIssuedAllocation);

                        if ($trxDeductionPtsUsed < 0) {
                            $lastIndex = $k;
                            unset($trxItems->$k);
                            $trxDeductionPtsUsedRemaining = $trxDeductionPtsUsed;
                            continue 3;
                        }
                    }
                }
            }
            return $tot_amount;
        } else {
            return $tot_amount;
        }
    }

    /**
     * Send the order notification email to BLU admins
     *
     * @param $transaction
     * @param $user
     * @return void
     */
    public static function sendOrderNotification($transaction, $user, $countryId = '', $channelId = '', $emailType = "order", $version = "v2") {
        if (!self::$notify) {
            return false;
        }

        if ($emailType == "") {
            $emailType = "order";
        }
        if ($version == "") {
            $version = "v2";
        }

        $email_template = "";
        $tosArray = array(null);
        $subject = "";
        $fromName = "";
        $fromEmail = "";
//		Commented manually
//		$sendgrid = new SendGrid(
//			Config::get('sendgrid.username'), Config::get('sendgrid.password')
//		);

        if ($transaction->partner_id == 4173 && $version != "v3") {
            $email_template = View::make("emails." . $emailType . "-confirmation-cedrus", array(
                        'transaction' => $transaction,
                        'user' => $user
                    ))->render();
//			Commented manually
//			$mail = new SendGrid\Email();
            // $mail->addTo(Config::get('blu.admin_email'));
            // $mail->addTo("rewards@cedrusbank.com");
            //$tosArray = array(Config::get('blu.admin_email'));


            if(App::environment('production')){
                $tosArray = array('rewards@cedrusbank.com', Config::get('blu.admin_email'));
            } else {
                $tosArray = array(Config::get('blu.admin_email'));
            }

            $fromEmail = Config::get('blu.email_from');
            $fromName = 'BLU Points';
            if (strtolower($transaction->trans_class) == 'reversal') {
                $subject = "Cedrus Bank Reversal Transaction Order Confirmation";
            } else {
                $subject = "Cedrus Bank Customer Order Confirmation";
            }
//			$mail->setHtml($email_template);
        } else if ($transaction->partner_id == 4215 && $version != "v3") {
            $email_template = View::make("emails." . $emailType . "-confirmation-m2", array(
                        'transaction' => $transaction,
                        'user' => $user
                    ))->render();
//			Commented manually
//			$mail = new SendGrid\Email();
//
            // $mail->addTo(Config::get('blu.admin_email'));
            // $mail->addTo("m2loyalty@m2.com.lb");

            if(App::environment('production')){
                $tosArray = array('m2loyalty@m2.com.lb', Config::get('blu.admin_email'));
            } else {
                $tosArray = array(Config::get('blu.admin_email'));
            }

            //$tosArray = array( Config::get('blu.admin_email'));
            $fromEmail = Config::get('blu.email_from');
            $fromName = 'BLU Points';
            if (strtolower($transaction->trans_class) == 'reversal') {
                $subject = "M2 Reversal Transaction Order Confirmation";
            } else {
                $subject = "M2 Customer Order Confirmation";
            }
//			$mail->setHtml($email_template);
        } else if ($transaction->partner_id == 3 && $version != "v3") { // OM
            if (strpos($transaction->notes, 'Card Deduction') !== false) {
                return true;
            }

            $email_template = View::make("emails." . $emailType . "-confirmation-om", array(
                        'transaction' => $transaction,
                        'user' => $user
                    ))->render();
//			Commented manually
//            $mail = new SendGrid\Email();
//			$mail->addTo(Config::get('blu.admin_email'));
//			$mail->addTo('originalandme@lifcosal.com');
            //$tosArray = array('originalandme@lifcosal.com', Config::get('blu.admin_email')); // testing emails
            $tosArray = array(Config::get('blu.admin_email'));
            $fromEmail = Config::get('blu.email_from');
            $fromName = 'BLU Points';
            if (strtolower($transaction->trans_class) == 'reversal') {
                $subject = "Original & Me Reversal Transaction Order Confirmation";
            } else {
                $subject = "Original & Me Customer Order Confirmation";
            }
//			$mail->setHtml($email_template);
        } else if ($transaction->partner_id == 4206 && $version != "v3") {
            $user_id = $user->id;
            $partner = Partner::find($transaction->partner_id);

            if ($partner->has_middleware == 1) {
                $user_ids = array();
                $user_ids[] = $user_id;
                $url = $partner->link_middleware_api . '?method=sensitiveInfo&api_key=' . $partner->middleware_api_key;
                $json_user_ids = json_encode($user_ids);
                $url = $url . "&user_ids=" . $json_user_ids;
//				$user			= User::where('id', $user_id)->with('country', 'city', 'area', 'cards')->first();

                $curl = curl_init();
                // Set some options - we are passing in a useragent too here
                curl_setopt_array($curl, array(
                    CURLOPT_RETURNTRANSFER => 1,
                    CURLOPT_URL => $url,
                    CURLOPT_SSL_VERIFYPEER => False,
                    CURLOPT_USERAGENT => 'Sensitive Info'
                ));

                // Send the request & save response to $resp
                $resp = curl_exec($curl);
                $resp_curl = json_decode($resp);

                if (!$resp_curl) {
                    return self::respondWith(
                                    500, "Could not locate user with id {$user_id} in database", null
                    );
                }

                $user->CPT = $resp_curl[0]->CPT;
                $user->first_name = $resp_curl[0]->first_name;
                $user->last_name = $resp_curl[0]->last_name;
                $user->email = $resp_curl[0]->email;
                $user->mobile = $resp_curl[0]->mobile;
                $user->normalized_mobile = $resp_curl[0]->mobile;
            }

            $email_template = View::make("emails." . $emailType . "-confirmation-bsf", array(
                        'transaction' => $transaction,
                        'user' => $user
                    ))->render();
//			Commented manually
//			$mail = new SendGrid\Email();
//			$mail->addTo(Config::get('blu.admin_email'));
            $tosArray = array(Config::get('blu.admin_email'));
            $fromEmail = 'fransijanasupport@alfransi.com.sa';
            $fromName = 'JANA Rewards';
            if (strtolower($transaction->trans_class) == 'reversal') {
                $subject = "[BSF] Reversal Transaction Order Confirmation";
            } else {
                $subject = "BSF Customer Order Confirmation";
            }
//			$mail->setHtml($email_template);
        } else if ($transaction->partner_id == 4209 && $version != "v3") {
            $partner = Partner::find($transaction->partner_id);
            $emailWhitelabelingData = $partner->emailwhitelabelings;

            foreach ($emailWhitelabelingData as $emailWhitelabeling) {
                $emailWhiteLabel[$emailWhitelabeling->field][$emailWhitelabeling->lang] = $emailWhitelabeling->value;
            }
            $emailWhiteLabel['partner']['primary'] = $partner->color;
            $emailWhiteLabel['partner']['secondary'] = $partner->secondary_color;

            $email_template = View::make("emails." . $emailType . "-confirmation-bisb", array(
                        'transaction' => $transaction,
                        'user' => $user
                    ))->render();
//			Commented manually
//			$mail = new SendGrid\Email();
//			//$mail->addTo("contactcentregroup@bisb.com");
			// $mail->addTo(Config::get('blu.admin_email'));

            if(App::environment('production')){
                $tosArray = array('contactcentregroup@bisb.com', Config::get('blu.admin_email'));
            } else {
                $tosArray = array(Config::get('blu.admin_email'));
            }

            $fromEmail = "contactcentregroup@bisb.com";
            $fromName = 'BisB Rewards';
            if (strtolower($transaction->trans_class) == 'reversal') {
                $subject = "[BisB] Reversal Transaction Order Confirmation";
            } else {
                $subject = "BisB Customer Order Confirmation";
            }
//			$mail->setHtml($email_template);
        } else {
            $emailWhiteLabel = array();
            $blu_partner = Partner::find(1);
            $bluEmailWhitelabelingData = $blu_partner->emailwhitelabelings;
            $partner = Partner::find($transaction->partner_id);
            $emailWhitelabelingData = $partner->emailwhitelabelings;

            foreach ($emailWhitelabelingData as $emailWhitelabeling) {
                $emailWhiteLabel[$emailWhitelabeling->field][$emailWhitelabeling->lang] = $emailWhitelabeling->value;
            }

            foreach ($bluEmailWhitelabelingData as $emailWhitelabeling) {
                if (array_key_exists($emailWhitelabeling->field, $emailWhiteLabel) && array_key_exists($emailWhitelabeling->lang, $emailWhiteLabel[$emailWhitelabeling->field])) {
                    continue;
                } else {
                    $emailWhiteLabel[$emailWhitelabeling->field][$emailWhitelabeling->lang] = $emailWhitelabeling->value;
                }
            }

            if (!empty($partner->color)) {
                $emailWhiteLabel['partner']['primary'] = $partner->color;
                $emailWhiteLabel['partner']['secondary'] = $partner->secondary_color;
            } else {
                $emailWhiteLabel['partner']['primary'] = $blu_partner->color;
                $emailWhiteLabel['partner']['secondary'] = $blu_partner->secondary_color;
            }

            // $template_view_name = "emails." . $emailType . "-confirmation-" . $version;


            if($transaction->partner_id == 4246):
                $template_view_name = "emails." . $emailType . "-confirmation-bsl-" . $version;
            else:
                $template_view_name = "emails." . $emailType . "-confirmation-" . $version;
            endif;

            if ($emailType == "booking") {
                $email_template = View::make($template_view_name, array(
                            'user' => $user,
                            'transaction' => $transaction,
                            'emailPartnerData' => $emailWhiteLabel/* ,
                                  'record_locator'	=> $record_locator,
                                  'pnr'				=> $response_decode_pnr,
                                  'out_segments'		=> $out_segments,
                                  'in_segments'		=> $in_segments,
                                  'adult_mobile'		=> $adult_mobile[0] */
                        ))->render();
            } else {
                if (!empty($countryId) && !empty($channelId)) {
                    $email_template = View::make($template_view_name, array(
                                'user' => $user,
                                'transaction' => $transaction,
                                'countryId' => $countryId,
                                'channelId' => $channelId,
                                'emailPartnerData' => $emailWhiteLabel
                            ))->render();
                } else {
                    $email_template = View::make($template_view_name, array(
                                'transaction' => $transaction,
                                'user' => $user,
                                'emailPartnerData' => $emailWhiteLabel
                            ))->render();
                }
            }
//			Commented manually
//			$mail = new SendGrid\Email();
            if ($transaction->partner_id == 4245) {
                // $mail->setTos(['salwa.zoubian@bml.com.lb', 'wissam.nohra@telesupport-int.com', 'PDEV@bml.com.lb', 'jannette.fawaz@bml.com.lb', Config::get('blu.admin_email')]);

                if(App::environment('production')){
                    $tosArray = array(
                        'wissam.nohra@telesupport-int.com',
                        'Lea.michael@telesupport-int.com',
                        'PDEV@bml.com.lb',
                        Config::get('blu.admin_email')
                    );
                } else {
                    $tosArray = array(Config::get('blu.admin_email'));
                }

                //$tosArray = array(Config::get('blu.admin_email'));
            } else {
                if ($emailType == "booking") {
                    $notes = $transaction->notes;
                    $json_notes = json_decode($notes);
                    $email = !empty($json_notes->email) ? $json_notes->email : $user->email;
                } else {
                    $email = Config::get('blu.admin_email');
                }
                // $mail->addTo($email);
                $tosArray = array($email);
            }

            if ($transaction->partner_id == 1) {
                $fromEmail = Config::get('blu.email_from');
                $fromName = 'BLU Points';
            } else {
                $fromEmail = $emailWhiteLabel['support_email']['en'];
                $fromName = $emailWhiteLabel['program_name']['en'] . " Rewards";
            }

            if (strtolower($transaction->trans_class) == 'reversal') {
                $subject = "[" . $emailWhiteLabel['partner_name_subject']['en'] . "] Reversal Transaction Order Confirmation";
            } else {
                $subject = "[" . $emailWhiteLabel['partner_name_subject']['en'] . "] Customer Order Confirmation";
            }

            // $mail->setHtml($email_template);
        }

        NotificationController::sendEmailNotification($email_template, $fromName, $fromEmail, array(null), $tosArray, $subject);
        
        // if($transaction->user_id == 40655) {
        //     return array('response ' => $response,
        //     'fromName ' => $fromName, 
        //     'fromEmail ' => $fromEmail, 
        //     'tosArray ' => $tosArray, 
        //     'subject ' => $subject, 
        //     'email_template ' => $email_template);
        // }

//		Commented manually
//		$sendgrid->send($mail);
    }

    /**
     * Handle the transaction notifications
     *
     * @param $user
     * @param $transaction
     * @param $type
     * @return void
     */
    public static function sendNotifications($user, $transaction, $type1, $version = "v2", $emailType = "", $countryId = '', $channelId = '') {
        if (!self::$notify) {
            return false;
        }

        /* SMS Notifications */
        $outputRalph = 'ccc';
        $rule_id_Ralph = Loyalty::getDefaultProgram($transaction->partner_id);

        //$rule_id_Ralph = json_decode($rule_id_Ralph);
        if ($rule_id_Ralph) {
            $transaction->rule_id = $rule_id_Ralph->id;
        }



        if (!empty($transaction->rule_id) && $type1 != 'reverse' && $transaction->notes != 'Bonus Points') {
            $sms_body = null;

            if ($type1 == Transaction::REDEEM_TRANS) {
                $sms_body = $transaction->loyalty->redemption_sms;
            } else {
                $sms_body = $transaction->loyalty->reward_sms;
            }

            //$outputRalph = $resultRalph;
            if (!empty($sms_body)) {
                $sms_body = str_ireplace('{title}', $user->title, $sms_body);
                $sms_body = str_ireplace('{first_name}', $user->first_name, $sms_body);
                $sms_body = str_ireplace('{last_name}', $user->last_name, $sms_body);
                $sms_body = str_ireplace('{balance}', $user->balance($transaction->network_id), $sms_body);
                $sms_body = str_ireplace('{date}', Carbon::parse($transaction->created_at)->format('Y-m-d'), $sms_body);
                $sms_body = str_ireplace('{time}', Carbon::parse($transaction->created_at)->format('H:i:s'), $sms_body);

                if ($type1 == Transaction::REDEEM_TRANS) {
                    $sms_body = str_ireplace('{points}', number_format($transaction->points_redeemed), $sms_body);
                    $sms_body = str_ireplace('{amount}', $transaction->amt_redeem, $sms_body);
                }

                if ($type1 == Transaction::REWARD_TRANS) {
                    $sms_body = str_ireplace('{points}', number_format($transaction->points_rewarded), $sms_body);
                    $sms_body = str_ireplace('{amount}', $transaction->amt_reward, $sms_body);
                }

                if ($transaction->store) {
                    $sms_body = str_ireplace('{store}', $transaction->store->name, $sms_body);
                } else {
                    $sms_body = str_ireplace('{store}', '', $sms_body);
                }

                // Send the SMS

                $outputRalph = SMSHelper::send($sms_body, $user, $transaction->partner);
            }
        }

        /* Email Notification */
//		$sendgrid = new SendGrid(
//				Config::get('sendgrid.username'), Config::get('sendgrid.password')
//		);



        $ddd = array("partner_id" => $transaction->partner_id,
            "version" => $version,
            "user" => $user);


        if ($transaction->partner_id == 4173 && $version != "v3") {
            $type = strtolower($type1);
//			Commented manually
//			$mail = new SendGrid\Email();
            $email_template = View::make("emails.{$type}-cedrus", array(
                        'user' => $user,
                        'transaction' => $transaction
                    ))->render();
//
//			$mail->addTo($user->email);
//			$mail->setFrom(Config::get('blu.email_from'));
//			$mail->setFromName('BLU Points');
            if (strtolower($transaction->trans_class) == "flight") {
                $subject = "Cedrus Bank Flight Confirmation";
            } else if (strtolower($transaction->trans_class) == "hotel") {
                $subject = "Cedrus Bank Hotel Confirmation";
            } else if (strtolower($transaction->trans_class) == "car") {
                $subject = "Cedrus Bank Car Rental Confirmation";
            } else if ($transaction->trans_class == "Points") {
                $subject = "Cedrus Bank Transfer Confirmation";
            } else if ($type == Transaction::REDEEM_TRANS) {
                $subject = "Cedrus Bank Redemption Confirmation";
            } else if ($transaction->trans_class == "Items") {
                $subject = "Cedrus Bank Redemption Confirmation";
            } else if (strtolower($transaction->trans_class) == 'reversal') {
                $subject = "Cedrus Bank Reversal Transaction Order Confirmation";
            } else {
                $subject = "Cedrus Bank " . ucfirst($type) . " Transaction Notification";
            }
//			$mail->setHtml($email_template);
//
//			$sendgrid->send($mail);

            NotificationController::sendEmailNotification($email_template, 'BLU Points', Config::get('blu.email_from'), array(null), array($user->email), $subject);
        } else if ($transaction->partner_id == 4215 && $version != "v3") {
            $type = strtolower($type1);
//			Commented manually
//			$mail = new SendGrid\Email();
            $email_template = View::make("emails.{$type}-m2", array(
                        'user' => $user,
                        'transaction' => $transaction
                    ))->render();
//
//			$mail->addTo($user->email);
//			$mail->setFrom(Config::get('blu.email_from'));
//			$mail->setFromName('BLU Points');
            if (strtolower($transaction->trans_class) == "flight") {
                $subject = "M2 Flight Confirmation";
            } else if (strtolower($transaction->trans_class) == "hotel") {
                $subject = "M2 Hotel Confirmation";
            } else if (strtolower($transaction->trans_class) == "car") {
                $subject = "M2 Car Rental Confirmation";
            } else if ($transaction->trans_class == "Points") {
                $subject = "M2 Transfer Confirmation";
            } else if ($type == Transaction::REDEEM_TRANS) {
                $subject = "M2 Redemption Confirmation";
            } else if ($transaction->trans_class == "Items") {
                $subject = "M2 Redemption Confirmation";
            } else if (strtolower($transaction->trans_class) == 'reversal') {
                $subject = "M2 Reversal Transaction Order Confirmation";
            } else {
                $subject = "M2 " . ucfirst($type) . " Transaction Notification";
            }
//			$mail->setHtml($email_template);
//
//			$sendgrid->send($mail);

            NotificationController::sendEmailNotification($email_template, 'BLU Points', Config::get('blu.email_from'), array(null), array($user->email), $subject);
        } else if ($transaction->partner_id == 3 && $version != "v3") { // OM
            if (strpos($transaction->notes, 'Card Deduction') !== false) {
                return true;
            }
            $type = strtolower($type1);
//			Commented manually
//			$mail = new SendGrid\Email();
            $email_template = View::make("emails.{$type}-om", array(
                        'user' => $user,
                        'transaction' => $transaction
                    ))->render();
//
//			$mail->addTo($user->email);
//			$mail->setFrom(Config::get('blu.email_from'));
//			$mail->setFromName('BLU Points');
            if (strtolower($transaction->trans_class) == "flight") {
                $subject = "Original & Me Flight Confirmation";
            } else if (strtolower($transaction->trans_class) == "hotel") {
                $subject = "Original & Me Hotel Confirmation";
            } else if (strtolower($transaction->trans_class) == "car") {
                $subject = "Original & Me Car Rental Confirmation";
            } else if ($transaction->trans_class == "Points") {
                $subject = "Original & Me Transfer Confirmation";
            } else if ($type == Transaction::REDEEM_TRANS) {
                $subject = "Original & Me Redemption Confirmation";
            } else if ($transaction->trans_class == "Items") {
                $subject = "Original & Me Redemption Confirmation";
            } else if (strtolower($transaction->trans_class) == 'reversal') {
                $subject = "Original & Me Reversal Transaction Order Confirmation";
            } else {
                $subject = "Original & Me " . ucfirst($type) . " Transaction Notification";
            }
//			$mail->setHtml($email_template);
//
//			$sendgrid->send($mail);

            NotificationController::sendEmailNotification($email_template, 'BLU Points', Config::get('blu.email_from'), array(null), array($user->email), $subject);
        } else if ($transaction->partner_id == 4209 && $version != "v3") {
            $type = strtolower($type1);
//			Commented manually
//			$mail = new SendGrid\Email();
            $email_template = View::make("emails.{$type}-bisb", array(
                        'user' => $user,
                        'transaction' => $transaction
                    ))->render();
//
//			$mail->addTo($user->email);
//			$mail->setFrom("contactcentregroup@bisb.com");
//			$mail->setFromName('BisB Rewards');
            if (strtolower($transaction->trans_class) == "flight") {
                $subject = "تأكيد حجز الرحلة الجوية \ BisB Flight Confirmation";
            } else if (strtolower($transaction->trans_class) == "hotel") {
                $subject = "تأكيد حجز فندقي \ BisB Hotel Confirmation";
            } else if (strtolower($transaction->trans_class) == "car") {
                $subject = "تأكيد حجز سيارة \ BisB Car Rental Confirmation";
            } else if ($transaction->trans_class == "Points") {
                if (strtolower($transaction->ref_number) == "cashback") {
                    $card_type = json_encode($transaction->ref_number);
                    $subject = "تأكيد سحب نقاط BisB نقداً \ BisB Cashback Confirmation";
                } else if (strpos(strtolower($transaction->ref_number), "miles points") > 0) {
                    $subject = "تأكيد تحويل أميال \ BisB Miles Conversion Confirmation";
                } else {
                    $subject = "تأكيد تحويل نقاط \ BisB Transfer Confirmation";
                }
            } else if ($type == Transaction::REDEEM_TRANS) {
                $subject = "تأكيد استخدام النقاط \ BisB Redemption Confirmation";
            } else if ($transaction->trans_class == "Items") {
                $subject = "تأكيد استخدام النقاط \ BisB Redemption Confirmation";
            } else if (strtolower($transaction->trans_class) == 'reversal') {
                $subject = "BisB Reversal Transaction Order Confirmation  \ تأكيد الغاء معاملة";
            } else {
                $subject = "BisB " . ucfirst($type) . " Transaction Notification \ إشعار بمكافأتك نقاط";
            }
//			$mail->setHtml($email_template);
//
//			$sendgrid->send($mail);

            NotificationController::sendEmailNotification($email_template, 'BisB Rewards', 'contactcentregroup@bisb.com', array(null), array($user->email), $subject);
        } else if ($transaction->partner_id == 4206 && $version != "v3") {
            $type = strtolower($type1);
//			Commented manually
//			$mail = new SendGrid\Email();
            $user_id = $user->id;
            $partner = Partner::find($transaction->partner_id);
            if ($partner->has_middleware == 1) {
                $user_ids = array();
                $user_ids[] = $user_id;
                $url = $partner->link_middleware_api . '?method=sensitiveInfo&api_key=' . $partner->middleware_api_key;
                $json_user_ids = json_encode($user_ids);
                $url = $url . "&user_ids=" . $json_user_ids;
                //$user = User::where('id', $user_id)->with('country', 'city', 'area', 'cards')->first();
                $curl = curl_init();
                // Set some options - we are passing in a useragent too here
                curl_setopt_array($curl, array(
                    CURLOPT_RETURNTRANSFER => 1,
                    CURLOPT_URL => $url,
                    CURLOPT_SSL_VERIFYPEER => False,
                    CURLOPT_USERAGENT => 'Sensitive Info'
                ));
                // Send the request & save response to $resp
                $resp = curl_exec($curl);
                $resp_curl = json_decode($resp);

                if (!$resp_curl) {
                    return self::respondWith(
                                    500, "Could not locate user with id {$user_id} in database", null
                    );
                }
                $user->CPT = $resp_curl[0]->CPT;
                $user->first_name = $resp_curl[0]->first_name;
                $user->last_name = $resp_curl[0]->last_name;
                $user->email = $resp_curl[0]->email;
                $user->mobile = $resp_curl[0]->mobile;
                $user->normalized_mobile = $resp_curl[0]->mobile;
            }
            $email_template = View::make("emails.{$type}-bsf", array(
                        'user' => $user,
                        'transaction' => $transaction
                    ))->render();
//
//			$mail->addTo($user->email);
//			$mail->addTo("Yasmina.sayegh@hotmail.com");
//	//            $mail->setFrom(Config::get('blu.email_from'));
//			$mail->setFrom('fransijanasupport@alfransi.com.sa');
//			$mail->setFromName('JANA Rewards');
//	//            $mail->setFromName('BLU Points');
            if (strtolower($transaction->trans_class) == "flight") {
                $subject = "تأكيد حجز الرحلة الجوية \ BSF Flight Confirmation";
            } else if (strtolower($transaction->trans_class) == "hotel") {
                $subject = "تأكيد حجز فندقي \ BSF Hotel Confirmation";
            } else if (strtolower($transaction->trans_class) == "car") {
                $subject = "تأكيد حجز سيارة \ BSF Car Rental Confirmation";
            } else if ($transaction->trans_class == "Points") {
                if (strtolower($transaction->notes) == "cashback") {
                    $subject = "تأكيد سحب نقاط BSF نقداً \ BSF Cashback Confirmation";
                } else {
                    $subject = "تأكيد تحويل نقاط \ BSF Transfer Confirmation";
                }
            } else if ($type == Transaction::REDEEM_TRANS) {
                $subject = "تأكيد استخدام النقاط \ BSF Redemption Confirmation";
            } else if ($transaction->trans_class == "Items") {
                $subject = "تأكيد استخدام النقاط \ BSF Redemption Confirmation";
            } else if (strtolower($transaction->trans_class) == 'reversal') {
                $subject = "BSF Reversal Transaction Order Confirmation  \ تأكيد الغاء معاملة";
            } else {
                $subject = "BSF " . ucfirst($type) . " Transaction Notification \ إشعار بمكافأتك نقاط";
            }
//			$mail->setHtml($email_template);
//
//			$sendgrid->send($mail);

            NotificationController::sendEmailNotification($email_template, 'JANA Rewards', 'fransijanasupport@alfransi.com.sa', array(null), array($user->email), $subject);
        } else {
            $emailWhiteLabel = array();
            $blu_partner = Partner::find(1);
            $bluEmailWhitelabelingData = $blu_partner->emailwhitelabelings;
            $partner = Partner::find($transaction->partner_id);
            $emailWhitelabelingData = $partner->emailwhitelabelings;

            foreach ($emailWhitelabelingData as $emailWhitelabeling) {
                $emailWhiteLabel[$emailWhitelabeling->field][$emailWhitelabeling->lang] = $emailWhitelabeling->value;
            }

            foreach ($bluEmailWhitelabelingData as $emailWhitelabeling) {
                if (array_key_exists($emailWhitelabeling->field, $emailWhiteLabel) && array_key_exists($emailWhitelabeling->lang, $emailWhiteLabel[$emailWhitelabeling->field])) {
                    continue;
                } else {
                    $emailWhiteLabel[$emailWhitelabeling->field][$emailWhitelabeling->lang] = $emailWhitelabeling->value;
                }
            }
            if (!empty($partner->color)) {
                $emailWhiteLabel['partner']['primary'] = $partner->color;
                $emailWhiteLabel['partner']['secondary'] = $partner->secondary_color;
            } else {
                $emailWhiteLabel['partner']['primary'] = $blu_partner->color;
                $emailWhiteLabel['partner']['secondary'] = $blu_partner->secondary_color;
            }

            $type = strtolower($type1);
//			$mail = new SendGrid\Email();
//
            // $template_view_name = "emails.{$type}-" . $version;


            if($transaction->partner_id == 4246):
                $template_view_name = "emails.{$type}-bsl";
            else:
                $template_view_name = "emails.{$type}-" . $version;
            endif;
            
            if ($emailType != "") {
                // $template_view_name = "emails." . $emailType . "-" . $version;
                if($transaction->partner_id == 4246):
                    $template_view_name = "emails." . $emailType . "-bsl-". $version;
                else:
                    $template_view_name = "emails." . $emailType . "-" . $version;
                endif;
            }
//
            if (!empty($countryId) && !empty($channelId)) {
                $email_template = View::make($template_view_name, array(
                            'user' => $user,
                            'transaction' => $transaction,
                            'countryId' => $countryId,
                            'channelId' => $channelId,
                            'emailPartnerData' => $emailWhiteLabel
                        ))->render();
            } else {
                $email_template = View::make($template_view_name, array(
                            'user' => $user,
                            'transaction' => $transaction,
                            'emailPartnerData' => $emailWhiteLabel
                        ))->render();
            }
//
//            $mail->addTo($user->email);
            if ($transaction->partner_id == 1) {
                $fromEmail = Config::get('blu.email_from');
                $fromName = 'BLU Points';
            } else {
                $fromEmail = $emailWhiteLabel['support_email']['en'];
                $fromName = $emailWhiteLabel['program_name']['en'] . " Rewards";
            }
            if (strtolower($transaction->trans_class) == "flight") {
                $subject = "تأكيد حجز الرحلة الجوية \ " . $emailWhiteLabel['partner_name_subject']['en'] . " Flight Confirmation";
            } else if (strtolower($transaction->trans_class) == "hotel") {
                $subject = "تأكيد حجز فندقي \ " . $emailWhiteLabel['partner_name_subject']['en'] . " Hotel Confirmation";
            } else if (strtolower($transaction->trans_class) == "car") {
                $subject = "تأكيد حجز سيارة \ " . $emailWhiteLabel['partner_name_subject']['en'] . " Car Rental Confirmation";
            } else if ($transaction->trans_class == "Points") {
                if (strtolower($transaction->notes) == "cashback") {
                    $subject = "تأكيد سحب نقاط نقدا\ " . $emailWhiteLabel['partner_name_subject']['en'] . " Cashback Confirmation";
                } else if (strtolower($transaction->notes) == "points redemption") {
                    $subject = "تأكيد استخدام النقاط \ " . $emailWhiteLabel['partner_name_subject']['en'] . " Redemption Confirmation";
                } else {
                    $subject = "تأكيد تحويل نقاط \ " . $emailWhiteLabel['partner_name_subject']['en'] . " Transfer Confirmation";
                }
            } else if ($type == Transaction::REDEEM_TRANS) {
                $subject = "تأكيد استخدام النقاط \ " . $emailWhiteLabel['partner_name_subject']['en'] . " Redemption Confirmation";
            } else if ($transaction->trans_class == "Items") {
                $subject = "تأكيد استخدام النقاط \ " . $emailWhiteLabel['partner_name_subject']['en'] . " Redemption Confirmation";
            } else if (strtolower($transaction->trans_class) == 'reversal') {
                $subject = "تأكيد الغاء معاملة \ " . $emailWhiteLabel['partner_name_subject']['en'] . " Reversal Transaction Order Confirmation";
            } else {
                $subject = " إشعار بمكافأتك نقاط\ " . $emailWhiteLabel['partner_name_subject']['en'] . " " . ucfirst($type) . " Transaction Notification";
            }
//            $mail->setHtml($email_template);
//
//            $sendgrid->send($mail);

            NotificationController::sendEmailNotification($email_template, $fromName, $fromEmail, array(null), array($user->email), $subject);
        }

        return $outputRalph;
    }

    /**
     * Void a transaction
     *
     * @param int $transaction_od
     * @return void
     */
    public static function voidTransaction($transaction_id) {
        $transaction = Transaction::find($transaction_id);

        if (!$transaction) {
            return false;
        }

        $transaction->voided = true;
        $transaction->save();

        return true;
    }

    /**
     * Perform a redemption transaction based the New Product Pricing Module
     *
     * Product Redemption:
     *
     *   $params = array(
     *       'user_id'   => 1,
     *       'partner'   => 1,
     *       'items'     => array(stdClass('id', 'qty')),
     *       'source'    => '', // Optional
     *       'reference' => '', // Optional
     *       'notes'     => '', // Optional
     *       'store_id'  => '', // Optional
     *       'pos_id'    => '', // Optional
     *       'staff_id'  => ''  // Optional
     *   );
     *
     * Points Redemption:
     *
     *   $params = array(
     *       'user_id'   => 1,
     *       'partner'   => 1,
     *       'points'    => 10000
     *   );
     *
     * Amount Redemption (USD Implied):
     *
     *   $params = array(
     *       'user_id' => 1,
     *       'partner' => 1,
     *       'amount'  => 100
     *   );
     *
     * Amount Redemption (Custom Currency):
     *
     *   $params = array(
     *       'user_id'  => 1,
     *       'partner'  => 1,
     *       'amount'   => 100,
     *       'currency' => 'USD'
     *   );
     *
     * @param array $params
     * @param string $trans_class ['Points', 'Amount', 'Items', 'Flight', 'Hotel', 'Car']
     * @return stdClass
     */
    public static function doRedemptionNew(array $params, $trans_class = "Items", $points_type = null) {
        // ---------------------------------------------------------------------
        //
        // Transaction Field Validation Process
        //
        // ---------------------------------------------------------------------
        $remainingPoints = 0;
        $cashPayment = 0;
        $paidCash = 0;
        $networkId = 0;

        $transaction = new Transaction();

        $validator = Validator::make(
                        $params, array(
                    'user_id' => 'required|integer'
                        )
        );

        if ($validator->fails()) {
            return self::respondWith(
                            Transaction::TRANS_FAILED, "Validation failed, ensure that all required fields are present", null
            );
        }

        if (!empty($params['from_merchant_app']) && $params['from_merchant_app'] == 1) {
            $userDet = User::find($params['user_id']);
            $userDet->verified = true;
            $userDet->save();
        }

        $user = User::where('id', $params['user_id'])->first();

        if (isset($params['country'])) {
            $user->country_id = $params['country'];
        }
        if (isset($params['area'])) {
            $user->area_id = $params['area'];
        }
        if (isset($params['city'])) {
            $user->city_id = $params['city'];
        }
        if (isset($params['address_1'])) {
            $user->address_1 = $params['address_1'];
        }
        if (isset($params['address_2'])) {
            $user->address_2 = $params['address_2'];
        }
        if (isset($params['email'])) {
            $user->email = $params['email'];
        }
        if (isset($params['postal_code'])) {
            $user->postal_code = $params['postal_code'];
        }
        if (isset($params['remaining'])) {
            $remainingPoints = ceil($params['remaining']);
        }
        if (isset($params['cash_payment'])) {
            $cashPayment = $params['cash_payment'];
        }
        if (isset($params['paid_cash'])) {
            $paidCash = $params['paid_cash'];
        }
        if (isset($params['network_id'])) {
            $networkId = $params['network_id'];
        }
        if (isset($params['e_ticket'])) {
            $eticket = $params['e_ticket'];
        } else {
            $eticket = '0';
        }
        if (isset($params['posted_at'])) {
            $transaction->posted_at = $params['posted_at'];
        } else {
            $transaction->posted_at = date('Y-m-d H:i:s');
        }

        if (!$user) {
            //TempTransactionController::deleteTempTransaction($user->id);
            return self::respondWith(
                            Transaction::TRANS_FAILED, "Transaction aborted: User '{$params['user_id']}' does not exist.", null
            );
        }

        if ($user->verified != true) {
            TempTransactionController::deleteTempTransaction($user->id);
            return self::respondWith(
                            Transaction::TRANS_FAILED, "Transaction aborted: User '{$params['user_id']}' exists but is not yet verified.", null
            );
        }

        if ($user->balances()->count() == 0) {
            TempTransactionController::deleteTempTransaction($user->id);
            return self::respondWith(
                            Transaction::TRANS_FAILED, "Transaction aborted: The member has no usable points balances.", null
            );
        }

        // Fetch partner data
        if (!isset($params['partner'])) {
            $params['partner'] = $user->getTopLevelPartner();
        } else {
            $params['partner'] = Partner::find($params['partner']);
        }

        if (!$params['partner']) {
            TempTransactionController::deleteTempTransaction($user->id);
            return self::respondWith(
                            Transaction::TRANS_FAILED, "No valid partner entity could be located for this transaction.", null
            );
        }

        if ($user->status != 'active' && $params['partner']->id != 4173) {
            TempTransactionController::deleteTempTransaction($user->id);
            if (!empty($params['from_merchant_app']) && $params['from_merchant_app'] == 1 && $user->status == 'inactive') {
                ;
            } else {
                return self::respondWith(
                                Transaction::TRANS_FAILED, "Transaction aborted: User account is " . $user->status, null
                );
            }
        }

        // validate Country and channel
        if (empty($params['country'])) {
            return self::respondWith(
                            Transaction::TRANS_FAILED, "No valid country entity could be located for this transaction.", null
            );
        } else {
            $country = Country::find($params['country']);
            if (!$country) {
                return self::respondWith(
                                Transaction::TRANS_FAILED, "No valid country entity could be located for this transaction.", null
                );
            } else {
                $countryId = $country->id;
            }
        }

        if (empty($params['channel'])) {
            return self::respondWith(
                            Transaction::TRANS_FAILED, "No valid channel entity could be located for this transaction.", null
            );
        } else {
            $channel = Partner::find($params['channel']);
            if (!$channel) {
                return self::respondWith(
                                Transaction::TRANS_FAILED, "No valid channel entity could be located for this transaction.", null
                );
            } else {
                $channelId = $channel->id;
            }
        }

        // check if user is a middlware partner and then if exists in middleware
        $partner = $params['partner'];
        if ($partner->has_middleware == 1) {
            $user_id = $params['user_id'];
            $user_ids = array();
            $user_ids[] = $user_id;
            $url = $partner->link_middleware_api . '?method=sensitiveInfo&api_key=' . $partner->middleware_api_key;
            $json_user_ids = json_encode($user_ids);
            $url = $url . "&user_ids=" . $json_user_ids;
            $curl = curl_init();
            // Set some options - we are passing in a useragent too here
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => $url,
                CURLOPT_SSL_VERIFYPEER => False,
                CURLOPT_USERAGENT => 'Sensitive Info'
            ));
            // Send the request & save response to $resp
            $resp = curl_exec($curl);
            $resp_curl = json_decode($resp);
            $userCPT = $resp_curl[0]->CPT;
            if (!$resp_curl) {
                TempTransactionController::deleteTempTransaction($user->id);
                return self::respondWith(
                                500, "Could not locate user with id {$user_id} in database", null
                );
            }
        }

        // Fetch the primary partner network for this transaction
        if ($networkId == 0) {
            $params['network'] = $params['partner']->firstNetwork();
        } else {
            $params['network'] = Network::find($networkId);
        }

        if ($params['network']->status != 'Active') {
            TempTransactionController::deleteTempTransaction($user->id);
            return self::respondWith(
                            Transaction::TRANS_FAILED, "Transaction aborted: Network '{$params['network']->name}' is not currently active.", null
            );
        }

        // ---------------------------------------------------------------------
        //
        // Transaction Loading Process
        //
        // ---------------------------------------------------------------------

        DB::beginTransaction();

        // Let's set the defaults for this transaction
        $transaction->trx_reward = 0;
        $transaction->trx_redeem = 1;
        $transaction->quantity = 0;
        $transaction->trans_class = $trans_class;
        $transaction->user_id = array_get($params, 'user_id');
        $transaction->country_id = $user->country_id;
        $transaction->partner_id = $params['partner']->id;
        $transaction->source = array_get($params, 'source');
        $transaction->ref_number = array_get($params, 'reference');
        $transaction->notes = array_get($params, 'notes');
        $transaction->store_id = array_get($params, 'store_id');
        $transaction->pos_id = array_get($params, 'pos_id');
        $transaction->auth_staff_id = array_get($params, 'staff_id');
        $transaction->network_id = $params['network']->id;
        $transaction->rule_id = null;
        $transaction->cash_payment = $cashPayment / 100;
        $transaction->delivery_cost = 0;
        $transaction->currency_id = $params['network']->currency_id;
        $transaction->partner_currency_id = $params['network']->currency_id;

        $currency = Currency::where('id', $transaction->currency_id)->first();
        $transaction->exchange_rate = $currency->latestPrice()->rate;
        $testTempTransactionInsert = TempTransactionController::createTempTransaction($transaction);

        if ($testTempTransactionInsert) {
            $transaction->save();
        } else {
            TempTransactionController::deleteTempTransaction($user->id);
            return self::respondWith(
                            Transaction::TRANS_FAILED, "Another redemption transaction is currently being processed for the same account. Please try again.", $testTempTransactionInsert
            );
        }
        // Process the payload

        $trans_price = 0;
        $trans_points = 0;
        $trans_delivery = 0;

        $userBalance = $user->balance($transaction->network_id);

        // Are we redeeming products?
        if (isset($params['items'])) {
            $sum_retail_price_in_usd = 0;
            $sum_item_price_in_points = 0;
            $pointsRedeemed = 0;
            $arr_products = array();
            $arr_ps = array();
            $countItems = 0;

            foreach ($params['items'] as $p) {
                $product = ProductsHelper::productRedemptionDetails($p->id, $countryId, $channelId);
                $arr_products[$countItems] = $product;
                $arr_ps[$countItems] = $p;
                $itemRetailPriceWithTaxes = $product->retail_price_in_usd + ($product->original_sales_tax * $product->retail_price_in_usd / 100);
                $sum_retail_price_in_usd += ($itemRetailPriceWithTaxes * $p->qty);

                $orginalPriceinNetworkCurrency = $product->original_price;
                $originalPrice = $orginalPriceinNetworkCurrency + ($product->original_sales_tax * $orginalPriceinNetworkCurrency / 100);
                $PriceInUsd = $product->price_in_usd + ($product->original_sales_tax * $product->price_in_usd / 100);
                $PriceInPoints = $product->price_in_points + ($product->original_sales_tax * $product->price_in_points / 100);
                $originalcurrencyId = $product->currency_id;
                $deliveryPrice = $product->delivery_charges;
                $deliveryCurrecy = $product->delivery_currency_id;
                $productPriceWithTax = $product->original_price + ($product->original_sales_tax * $product->original_price / 100);
                $originalPriceInPoints = PointsHelper::productPriceInPoints($PriceInPoints, $productPriceWithTax, $originalcurrencyId, $networkId);
                $deliveryPriceInPoints = round(PointsHelper::deliveryPriceInPoints($deliveryPrice, $deliveryCurrecy, $networkId));
                if ($product->is_voucher == 1) {
                    $deliveryCost = intval($deliveryPriceInPoints);
                } else {
                    $deliveryCost = intval($deliveryPriceInPoints * intval($p->qty));
                }
                $pointsRedeemed += $originalPriceInPoints * $p->qty;
                $countItems++;
            }

            $i = 0;
            $pointsPaidByItemsTotal = 0;
            $cashPaidByItemsTotal = 0;
            if ($paidCash == 1) {
                // Remove points paid by cash from Points
                if ($remainingPoints > 0) {
                    $pointsRedeemed = $pointsRedeemed - $remainingPoints;
                }
            }

            foreach ($arr_ps as $k => $p) {
                if (!isset($p->qty) || $p->qty == null) {
                    $p->qty = 1;
                }

                $product = $arr_products[$k];

                if ($product) {
                    if ($networkId == 0) {
                        $networkId = $params['network']->id;
                    }

                    $orginalPriceinNetworkCurrency = $product->original_price;
                    $originalPrice = $orginalPriceinNetworkCurrency + ($product->original_sales_tax * $orginalPriceinNetworkCurrency / 100);
                    $PriceInUsd = $product->price_in_usd + ($product->original_sales_tax * $product->price_in_usd / 100);
                    $PriceInPoints = $product->price_in_points + ($product->original_sales_tax * $product->price_in_points / 100);
                    $originalcurrencyId = $product->currency_id;
                    $deliveryPrice = $product->delivery_charges;
                    $deliveryCurrecy = $product->delivery_currency_id;
                    $productPriceWithTax = $product->original_price + ($product->original_sales_tax * $product->original_price / 100);
                    $originalPriceInPoints = PointsHelper::productPriceInPoints($PriceInPoints, $productPriceWithTax, $originalcurrencyId, $networkId);
                    $deliveryPriceInPoints = round(PointsHelper::deliveryPriceInPoints($deliveryPrice, $deliveryCurrecy, $networkId));

                    $transaction->total_amount += ($PriceInUsd * $p->qty);
                    $transaction->original_total_amount += ($originalPrice * $p->qty);
                    $transaction->partner_amount += ($originalPrice * $p->qty);
                    $transaction->points_redeemed += (($originalPriceInPoints * $p->qty) - $product->delivery_options);

                    if ($product->is_voucher == 1) {
                        $transaction->delivery_cost += intval($deliveryPriceInPoints);
                    } else {
                        $transaction->delivery_cost += intval($deliveryPriceInPoints * intval($p->qty));
                    }

                    $transaction->quantity += $p->qty;

                    // calculate the cash payment by item
                    if ($transaction->cash_payment > 0) {
                        $totalPaidInCash = $transaction->cash_payment;
                        $totalPaidInPoints = $pointsRedeemed;
                        $itemRetailPriceWithTaxes = $product->retail_price_in_usd + ($product->original_sales_tax * $product->retail_price_in_usd / 100);
                        $itemRetailPrice = $itemRetailPriceWithTaxes * $p->qty;
                        $ratio = $itemRetailPrice / $sum_retail_price_in_usd;
                        $cash_paid_by_item = $totalPaidInCash * $ratio;
                        $points_paid_by_item = round($totalPaidInPoints * $ratio);
                        $cashPaidByItemsTotal += number_format($cash_paid_by_item, 2);
                        $pointsPaidByItemsTotal += $points_paid_by_item;
                        $last_item = end($params['items']);

                        if ($p->id === $last_item->id) {
                            if ($transaction->cash_payment != $cashPaidByItemsTotal) {
                                $otherProductsCashPayment = $cashPaidByItemsTotal - $cash_paid_by_item;
                                $cash_paid_by_item = $transaction->cash_payment - $otherProductsCashPayment;
                            }

                            if ($pointsRedeemed != $pointsPaidByItemsTotal) {
                                $otherProductsPointsPayment = $pointsPaidByItemsTotal - $points_paid_by_item;
                                $points_paid_by_item = round($pointsRedeemed - $otherProductsPointsPayment);
                                $arr_results = array();
                                $arr_results['points_paid_by_item'] = $points_paid_by_item;
                                $arr_results['points_redeemed'] = $pointsRedeemed;
                                $arr_results['points_paid_by_item_total'] = $pointsPaidByItemsTotal;
                                $arr_results['other_products_points_payment'] = $otherProductsPointsPayment;
                                $json_data1 = json_encode($arr_results);
                                APIController::postSendEmailJson($json_data1);
                            }
                        }
                    } else {
                        if ($product->is_voucher == 1) {
                            $tiDelivery = $deliveryPriceInPoints;
                        } else {
                            $tiDelivery = $deliveryPriceInPoints * floatval($p->qty);
                        }

                        $cash_paid_by_item = 0;
                        $points_paid_by_item = ($originalPriceInPoints * floatval($p->qty)); // + $tiDelivery;
                    }

                    // Record TransactionItem
                    $ti = new TransactionItem();
                    $ti->total_amount = $PriceInUsd * floatval($p->qty);
                    $ti->original_total_amount = $originalPrice * $p->qty;
                    $ti->currency_id = $product->currency_id;
                    $ti->product_id = $product->id;
                    $ti->product_model = $product->model;
                    $ti->product_brand_id = $product->brand_id;
                    $ti->product_sub_model = $product->sub_model;
                    $ti->product_category_id = $product->category_id;
                    $ti->delivery_option = $product->delivery_options;
                    $ti->price_in_points = $originalPriceInPoints * floatval($p->qty);
                    $ti->network_id = $networkId;
                    $ti->paid_in_points = $points_paid_by_item;
                    $ti->paid_in_cash = $cash_paid_by_item;
                    $ti->cash_currency_id = '6';

                    if ($product->is_voucher == 1) {
                        $ti->delivery_cost = $deliveryPriceInPoints;
                    } else {
                        $ti->delivery_cost = $deliveryPriceInPoints * floatval($p->qty);
                    }
                    $ti->quantity = $p->qty;
                    $ti->transaction_id = $transaction->id;
                    $ti->save();

                    //  Save transcation item delivery
                    $tid = new TransactionItemDelivery();
                    $tid->trx_id = $transaction->id;
                    $tid->trx_item_id = $ti->id;
                    if ($product->is_voucher == 1) {
                        $deliveryCost = $deliveryPriceInPoints;
                        $tid->quantity = 1;
                        $tid->delivery_cost = $deliveryCost;
                    } else {
                        $deliveryCost = $deliveryPriceInPoints * floatval($p->qty);
                        $tid->quantity = $p->qty;
                        $tid->delivery_cost = $deliveryCost;
                    }
                    $tid->network_id = $networkId;
                    $tidCurrency = Currency::where('id', $transaction->currency_id)->first();

                    $tid->amt_delivery = PointsHelper::pointsToAmount($deliveryCost, $tidCurrency->short_code, $networkId);
                    $tid->currency_id = $transaction->currency_id;
                    $tid->save();

                    // Saving redemption order
                    if (!empty($tid->amt_delivery)) {
                        $redOrderDeliveryCost = $tid->amt_delivery;
                    } else {
                        $redOrderDeliveryCost = 0;
                    }

                    if ($product->is_voucher == 1) {
                        $productDelivery = $product->delivery_charges;
                    } else {
                        $productDelivery = $product->delivery_charges * floatval($p->qty);
                    }

                    $redOrderDeliveryCurrency = Currency::where('id', $tid->currency_id)->first();

                    $redOrderDeliveryCostInUsd = CurrencyHelper::convertToUSD($redOrderDeliveryCost, $redOrderDeliveryCurrency->short_code);
                    $paidinPointsValueInUsd = PointsHelper::pointsToAmount($points_paid_by_item, 'USD', $transaction->network_id, null, $transaction->partner_id);
                    $itemFullPriceInUsd = $paidinPointsValueInUsd + $cash_paid_by_item + $redOrderDeliveryCostInUsd['amount'];
                    $redemptionOrederParams = array(
                        'trx_id' => $transaction->id,
                        'trx_item_id' => $ti->id,
                        'product_id' => $product->id,
                        'product_name' => $product->name,
                        'product_model' => $product->model,
                        'product_sub_model' => $product->sub_model,
                        'full_price_in_usd' => $itemFullPriceInUsd,
                        'supplier_id' => $product->supplier_id,
                        'cost' => ($product->original_price + ($product->original_sales_tax * $product->original_price / 100)) * floatval($p->qty),
                        'cost_currency' => $product->currency_id,
                        'delivery_cost' => $productDelivery,
                        'delivery_cost_currency' => $product->delivery_currency_id,
                        'quantity' => $ti->quantity,
                        'user_id' => $transaction->user_id,
                        'cash_paid' => $cash_paid_by_item,
                        'country_id' => $countryId,
                        'channel_id' => $channelId
                    );
                    $json_data = json_encode($redemptionOrederParams);
                    $url_trx_api = env('APP_URL').'/api/create-redemption-order?api_key=c4ca4238a0b923820dcc509a6f75849b';
                    $url = url($url_trx_api);
                    $string_param = 'redemption_order=' . $json_data;
                    $ch1 = curl_init();

                    curl_setopt($ch1, CURLOPT_URL, $url);
                    curl_setopt($ch1, CURLOPT_HEADER, 1);
                    curl_setopt($ch1, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch1, CURLOPT_TIMEOUT, 1);
                    curl_setopt($ch1, CURLOPT_POST, 1);
                    curl_setopt($ch1, CURLOPT_POSTFIELDS, $string_param);
                    curl_setopt($ch1, CURLOPT_SSL_VERIFYPEER, False);
                    $result = curl_exec($ch1);

                    curl_close($ch1);
                    $json_data = "";

                    //RedemptionOrder::createRedemptionOrder($redemptionOrederParams, 'Item');
                    // Check stock levels
                    $productRedemption = ProductPartnerRedemption::find($product->product_redemption_id);

                    if (($productRedemption->qty - $p->qty) < 0) {
                        $productRedemption->qty = 0;
                    } else {
                        $productRedemption->qty = (int) ($product->qty - $p->qty);
                    }

                    // Update product

                    $productRedemption->save();

                    if ($productRedemption->qty <= Config::get('blu.stock_level_warning')) {
                        $productRedemption->sendStockLevelWarning();
                    }
                }
                $i++;
            }

            //add delivery cost in usd to total_amount
            $networkCurrency = Currency::where('id', $transaction->currency_id)->first();
            $deliveryCostInPoints = ceil($transaction->delivery_cost);
            $deliveryCostInUSD = PointsHelper::pointsToAmount($deliveryCostInPoints, 'USD', $params['network']->id);
            $deliveryCostInOriginalCurrency = PointsHelper::pointsToAmount($deliveryCostInPoints, $networkCurrency->short_code, $params['network']->id);
            $transaction->total_amount = $transaction->total_amount + $deliveryCostInUSD;

            $transaction->original_total_amount = $transaction->original_total_amount + $deliveryCostInOriginalCurrency;
            $transaction->partner_amount = $transaction->original_total_amount + $deliveryCostInOriginalCurrency;

            // Add Delivery Cost to Points Redeemed
            $transaction->points_redeemed += ceil($transaction->delivery_cost);

            if ($transaction->points_redeemed > $userBalance && $paidCash == 0) {
                DB::rollback();
                TempTransactionController::deleteTempTransaction($user->id);

                return self::respondWith(
                    Transaction::TRANS_FAILED, "User doesn't have enough amount of points. Please try again.", $testTempTransactionInsert
                );
            }
        }

        // Are we redeeming points?
        if (isset($params['points'])) {
            $networkCurrency = Currency::where('id', $transaction->currency_id)->first();
            $transaction->points_redeemed = ceil($params['points']);

            if ($transaction->points_redeemed > $userBalance && $paidCash == 0) {
                DB::rollback();
                TempTransactionController::deleteTempTransaction($user->id);

                return self::respondWith(
                    Transaction::TRANS_FAILED, "User doesn't have enough amount of points. Please try again.", $testTempTransactionInsert
                );
            }

            $partner = $params['partner'];
            if ($points_type == 'cashback') {

                $transaction->total_amount = PointsHelper::pointsToAmount($params['points'], 'USD', $params['network']->id, 'cashback', $partner->id);
                $transaction->original_total_amount = PointsHelper::pointsToAmount($params['points'], $networkCurrency->short_code, $params['network']->id, 'cashback', $partner->id);
                $transaction->partner_amount = PointsHelper::pointsToAmount($params['points'], $networkCurrency->short_code, $params['network']->id, 'cashback', $partner->id);
            } elseif ($points_type == 'miles') {

                $transaction->total_amount = 0; //PointsHelper::pointsToAmount($params['points'], 'USD', $params['network']->id,'miles',$partner->id);
                $transaction->original_total_amount = 0; //PointsHelper::pointsToAmount($params['points'], $networkCurrency->short_code, $params['network']->id,'miles',$partner->id);
                $transaction->partner_amount = 0; //PointsHelper::pointsToAmount($params['points'], $networkCurrency->short_code, $params['network']->id,'miles',$partner->id);
            } else {
                $transaction->total_amount = PointsHelper::pointsToAmount($params['points'], 'USD', $params['network']->id, null, $partner->id);
                $transaction->original_total_amount = PointsHelper::pointsToAmount($params['points'], $networkCurrency->short_code, $params['network']->id, null, $partner->id);
                $transaction->partner_amount = PointsHelper::pointsToAmount($params['points'], $networkCurrency->short_code, $params['network']->id, null, $partner->id);
            } { // if transfer set total amount to zero
                if (isset($params['type']) && $params['type'] == 'transfer') {
                    $transaction->total_amount = 0;
                }
            }
        }

        // Are we redeeming an amount?
        if (isset($params['amount'])) {
            $transaction->points_redeemed = ceil(PointsHelper::amountToPoints($params['amount'], $params['currency'], $networkId));

            if ($transaction->points_redeemed > $userBalance && $paidCash == 0) {
                DB::rollback();
                TempTransactionController::deleteTempTransaction($user->id);

                return self::respondWith(
                    Transaction::TRANS_FAILED, "User doesn't have enough amount of points. Please try again.", $testTempTransactionInsert
                );
            }

            $currency = Currency::where('short_code', $params['currency'])->first();
            $transaction->currency_id = $currency->id;
            $transaction->exchange_rate = $currency->latestPrice()->rate;
            $transaction->original_total_amount = $params['amount'];
            $transaction->partner_amount = $params['amount'];
            $transaction->total_amount = floatval($params['amount']) / floatval($transaction->exchange_rate);
        }

        if ($transaction->auth_staff_id) {
            $staff_member = self::validStaffMember($transaction->auth_staff_id);

            if ($staff_member) {
                $transaction->auth_staff_id = $staff_member->id;
                $transaction->auth_role_id = $staff_member->roles()->first()->id;
            }
        }

        // Process coupons
        if (!empty($params['coupon_code'])) {
            $coupon = App\Coupon::where('coupon_code', $params['coupon_code'])->first();
            $couponType = $coupon->coupon_type;

            $couponDetails = CouponHelper::CouponPrice($params['coupon_code'], $transaction->total_amount, $transaction->partner_id);
            $couponPrice = $couponDetails['price'];
            $couponCurrencyId = $couponDetails['currency'];

            if (!empty($couponCurrencyId)) {
                $couponCurrencyDetails = Currency::where('id', $couponCurrencyId)->first();
                $couponPriceInUsd = $couponPrice / $couponCurrencyDetails->latestPrice()->rate;
                $couponCurrencyShortCode = $couponCurrencyDetails->short_code;
            } else {
                $couponPriceInUsd = $couponPrice;
                $couponCurrencyShortCode = 'USD';
            }
            $coupnValidFrom = $coupon->valid_from();
            $coupnValidTo = $coupon->valid_to();
            $couponPartner = $coupon->partner_id;
            $couponSegment = $coupon->segmentId();
            $currentDate = date('Y-m-d');
            if (strtotime($coupnValidFrom) <= strtotime($currentDate) && strtotime($currentDate) <= strtotime($coupnValidTo)) {
                ;
            } else {
                DB::rollback();
                TempTransactionController::deleteTempTransaction($user->id);
                return self::respondWith(
                                Transaction::TRANS_FAILED, "The coupon is Expired!", $transaction
                );
            }

            if ($couponPartner != $transaction->partner_id) {
                DB::rollback();
                TempTransactionController::deleteTempTransaction($user->id);
                return self::respondWith(
//						500, "This coupon is issued by " . Partner::find($couponPartner)->name, $transaction
                                500, "This coupon is issued by another partner", $transaction
                );
            }

            //validate segment
//            $user = User::find($transaction->user_id);
            foreach ($user->segments as $segment) {
                $userSegments[] = $segment->id;
            }

            if ($couponSegment > 0 && !in_array($couponSegment, $userSegments)) {
                TempTransactionController::deleteTempTransaction($user->id);
                return self::respondWith(
                                500, "The user is not in the corresponding segment. ", $transaction
                );
            }

            if (empty($coupon->transaction_id)) {
                if ($couponType == 'value') {
                    $couponPriceInPoints = PointsHelper::amountToPoints($couponPrice, $couponCurrencyShortCode, $networkId);
                    $user->coupon_price = $couponPriceInPoints;
                    $transaction->points_redeemed = ($transaction->points_redeemed - $couponPriceInPoints);
                } else {
//					$couponActualPrice				= ( $params['amount'] * $couponPriceInUsd ) / 100;
                    $couponActualPrice = $couponPriceInUsd;
                    $couponPriceInPoints = PointsHelper::amountToPoints($couponActualPrice, $params['currency'], $networkId);
                    $user->coupon_price = $couponActualPrice;
                    $transaction->points_redeemed = $transaction->points_redeemed - $couponPriceInPoints;
                }

                $transaction->coupon_id = (int) $coupon->id;

                if ($transaction->points_redeemed < 0) {
                    $transaction->points_redeemed = 0;
                }

                if ($transaction->points_redeemed > $userBalance && $paidCash == 0) {
                    DB::rollback();
                    TempTransactionController::deleteTempTransaction($user->id);

                    return self::respondWith(
                        Transaction::TRANS_FAILED, "User doesn't have enough amount of points. Please try again.", $testTempTransactionInsert
                    );
                }

                // Assign the coupon
                DB::table('coupon')->where('coupon_code', $params['coupon_code'])->update(array(
                    'transaction_id' => $transaction->id
                ));
            } else {
                DB::rollback();
                TempTransactionController::deleteTempTransaction($user->id);
                return self::respondWith(
                                500, "The coupon is already used for another transaction!", $transaction
                );
            }
        }

        // ---------------------------------------------------------------------
        //
        // Network Points Conversion Process
        //
        // ---------------------------------------------------------------------
        $user_balances = $user->balances();
        $networks = array();
        $trxNewtork = Network::where('id', '=', $transaction->network_id)->take(1)->first();
        // Is this transaction on a BLU network?
        if ($transaction->network_id == '1') {
            $networks[] = '1';
        } else {

            if ($trxNewtork->do_points_conversion) {
                $networks[] = $transaction->network_id;
                $networks[] = '1';
            } else {
                $networks[] = $transaction->network_id;
            }
        }

        /*
         * Here we construct a queue of network IDS in order of priority to try
         * transact against. The user balances are already ordered by date
         * updated to prioritize the oldest ones
         */
        if ($trxNewtork->do_points_conversion) {
            foreach ($user_balances as $balance) {
                $trxNewtork1 = Network::where('id', '=', $balance->network_id)->take(1)->first();

                if (!in_array($balance->network_id, $networks) && $balance->network_id != '0' && $trxNewtork1->do_points_conversion == '1') {
                    array_push($networks, $balance->network_id);
                }
            }
        }

        $total_rundown = $transaction->points_redeemed;

        /*
         * Here we attempt to deduct the maximum amount of points from a member
         * on each network that they belong to by proving the network where the
         * transaction occurred and then the network the transaction is
         * attempting to run against
         */
        $count_networks = 0;
        $rundown_ralph = array();
        $transaction_network = Network::where('id', '=', $transaction->network_id)->take(1)->first();
        $temp_view_pts_redeemed = 0;
        foreach ($networks as $balance_network) {
            $attempt_network = Network::where('id', '=', $balance_network)->take(1)->first();
            if ($attempt_network->do_points_conversion || $attempt_network->id == $transaction_network->id) {
                $total_rundown -= $user->deductMaxPoints($total_rundown, $transaction->network_id, $balance_network);

                $rundown_ralph[$count_networks]['balance_network'] = $balance_network;
                $rundown_ralph[$count_networks]['trx_net_id'] = $transaction->network_id;
                $rundown_ralph[$count_networks]['total_rundown'] = $total_rundown;
                $count_networks += 1;
            }
            if ($total_rundown <= 0) {
                break;
            }
        }

        if ($paidCash == 1) {
            // Remove points paid by cash from Points
            if ($remainingPoints > 0) {
                $transaction->points_redeemed -= $remainingPoints;
                $temp_view_pts_redeemed = $transaction->points_redeemed;
                $total_rundown = 0;
            }
        }

        /*
         * We check to see if the total required points could not collectively be
         * deduced from the networks the member belong to. If successful, there
         * should be no points remaining in the rundown
         */
        if ($total_rundown > 0) {
            DB::rollback();
            TempTransactionController::deleteTempTransaction($user->id);
            return self::respondWith(
                            Transaction::TRANS_FAILED, "The member has an insufficient networks points balance for this transaction", $rundown_ralph
            );
        }

        //get network currency
        $networkCurrencyId = $params['network']->currency_id;
        $networkCurrency = Currency::where('id', $networkCurrencyId)->first();
        $networkCurrencyShortCode = $networkCurrency->short_code;
        $transaction->amt_redeem = number_format(PointsHelper::pointsToAmount($transaction->points_redeemed, $networkCurrencyShortCode, $params['network']->id), 2);

        $transaction->save();
        DB::commit();

        $url_trx_api = env('APP_URL').'/api/deduct-transaction-max?api_key=c4ca4238a0b923820dcc509a6f75849b&transaction_id=' . $transaction->id;
        $url = url($url_trx_api);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, False);
        curl_exec($ch);
        curl_close($ch);

        $order = new Order();
        $order->transaction_id = $transaction->id;
        $order->user_id = $transaction->user_id;
        $order->save();

        // save redemption order
        if ($trans_class == 'flight' || $trans_class == 'hotel' || $trans_class == 'car') {
            $pointsRedeemedInUsd = PointsHelper::pointsToAmount($transaction->points_redeemed, 'USD', $transaction->network_id);
            $travelFullPriceInUsd = $pointsRedeemedInUsd + $transaction->cash_payment;
            $redemptionOrederParams = array(
                'trx_id' => $transaction->id,
                'trx_item_id' => 0,
                'notes' => $transaction->notes,
                'full_price_in_usd' => $travelFullPriceInUsd,
                'supplier_id' => 0,
                'cost' => $travelFullPriceInUsd,
                'cost_currency' => $transaction->currency_id,
                'delivery_cost' => 0,
                'delivery_cost_currency' => 0,
                'quantity' => 1,
                'user_id' => $transaction->user_id,
                'trans_class' => $trans_class,
                'cash_paid' => $cashPayment
            );
            RedemptionOrder::createRedemptionOrder($redemptionOrederParams, 'Travel');
        }

        // Send the notifications
        $url_trx_api = env('APP_URL').'/api/send-notifications-transaction?api_key=c4ca4238a0b923820dcc509a6f75849b';
        $url1 = url($url_trx_api);
        if (!isset($userCPT) || empty($userCPT)) {
            $userCPT = "";
        }
        $string_param = 'channel_id=' . $channelId
                . '&country_id=' . $countryId
                . '&user_cpt=' . $userCPT
                . '&user_id=' . $user->id
                . '&paid_cash=' . $paidCash
                . '&eticket=' . $eticket
                . '&transaction_id=' . $transaction->id;

        $ch2 = curl_init();

        curl_setopt($ch2, CURLOPT_URL, $url1);
        curl_setopt($ch2, CURLOPT_HEADER, 1);
        curl_setopt($ch2, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch2, CURLOPT_TIMEOUT, 1);
        curl_setopt($ch2, CURLOPT_POST, 1);
        curl_setopt($ch2, CURLOPT_POSTFIELDS, $string_param);
        curl_setopt($ch2, CURLOPT_SSL_VERIFYPEER, False);
        curl_exec($ch2);

        curl_close($ch2);


        $result_magid = array();
        $result_magid['trx'] = $transaction->toArray();

        TempTransactionController::deleteTempTransaction($user->id);


        return self::respondWith(
                        Transaction::TRANS_SUCCESS, "success", $result_magid
        );
    }

    public function getHotelImageUrl($domain = "http://images.travelnow.com")
    {
        $from_array = array('_b', '_l', '_t', '_n', '_g', '_e', '_d', '_y', '_z', '_w');
        $imageUrl = str_replace($from_array, '_s', json_decode($this->notes)->img ?? '');
        $hostName = parse_url($imageUrl, PHP_URL_HOST);

        return isset($hostName) ? $imageUrl : $domain . $imageUrl;
    }

}

// EOC
