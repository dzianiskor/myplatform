<?php

namespace App\Console;

use App\Console\Commands\AlinmaDailyCommand;
use App\Console\Commands\CatalogueSyncing;
use App\Console\Commands\BSF2Command;
use App\Console\Commands\BSFCommand;
use App\Console\Commands\NotificationCommand;
use App\Console\Commands\EstatementCommand;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Console\Commands\SegmentCommand;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\TransactionDashboardCommand::class,
        SegmentCommand::class,
        Commands\CurrencyCommand::class,
        CatalogueSyncing::class,
        AlinmaDailyCommand::class,
        BSF2Command::class,
        BSFCommand::class,
        NotificationCommand::class,
        EstatementCommand::class,
        //\Themsaid\RoutesPublisher\RoutesPublisherCommand::class
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('transaction:dashboard')->daily();
        $schedule->command('segment:sync')->hourly();
        $schedule->command('currency:sync')->daily();
        $schedule->command('transaction:daily mis file')->dailyAt('18:00');
        // $schedule->command('inspire')
        //          ->hourly();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
