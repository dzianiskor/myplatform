<?php

namespace App\Console\Commands;

use App\Tier;
use App\Tiercriteria;
use App\Transaction;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;


class TierCommand extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tiers:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync member tiers based on criteria';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->info("Running Tiering");

        $tiers = Tier::where('draft', false)->get();

        foreach ($tiers as $tier)
        {

            $this->info("- Running Tier #{$tier->id} : {$tier->name}");
            $tier_promotion_in_months = intval($tier->promotion_in_months);
            $tier_demotion_in_months = intval($tier->demotion_in_months);
            $tier_start_month = intval($tier->start_month);
            $partner_id = $tier->partner_id;
            $now_month = date("m");
            $now_date = date("Y-m-d");
            $temp = intval($now_month) + 12 - $tier_start_month;
            $run_promotion = $temp % $tier_promotion_in_months;
            $run_demotion = $temp % $tier_demotion_in_months;
            $previous_promotion = 0;
            if ($run_promotion == 0)
            {
                $tempString = "-" . $tier_promotion_in_months . ' months';
                $new_timestamp = strtotime($tempString, strtotime($now_date));
                $previous_promotion = date("Y-m-d", $new_timestamp);
            }

            $previous_demotion = 0;
            if ($run_demotion == 0)
            {
                $tempString = "-" . $tier_demotion_in_months . ' months';
                $new_timestamp = strtotime($tempString, strtotime($now_date));
                $previous_demotion = date("Y-m-d", $new_timestamp);
            }

            $str_time_demotion = strtotime($previous_demotion);
            $str_time_promotion = strtotime($previous_promotion);
            $promotion_older_demotion = false;
            if ($str_time_demotion > $str_time_promotion)
            {
                $promotion_older_demotion = true;
            }

            $tiercriteria = Tiercriteria::where('draft', false)->where('tier_id', $tier->id)->get();

            $arr_unordered_tiercriterion = [];
            $count = 0;
            foreach ($tiercriteria as $tiercriterion)
            {
                $arr_unordered_tiercriterion[$count][1] = $tiercriterion->id;
                $arr_unordered_tiercriterion[$count][0] = $tiercriterion->start_value;
                $arr_unordered_tiercriterion[$count][2] = $tiercriterion->end_value;
                $count++;
            }
            asort($arr_unordered_tiercriterion);
            $arr_ordered_tiercriterion = [];
            $count = 0;
            $arr_of_tiercriteria_ids = [];
            foreach ($arr_unordered_tiercriterion as $unOrd)
            {
                $arr_ordered_tiercriterion[$count][0] = $unOrd[0];//START VALUE
                $arr_ordered_tiercriterion[$count][1] = $unOrd[1];//ID
                $arr_ordered_tiercriterion[$count][2] = $unOrd[2];//END VALUE
                $arr_of_tiercriteria_ids = $unOrd[1];
                $count++;
            }
            $users = User::where('draft', false);
            $users->whereHas('partners', function ($q) use ($partner_id) {
                $q->where('partner.id', $partner_id);
            });
            $obj_users = $users->get();
            $current_tier_key = -1;
            $sum_pointsRewarded = 0;
            $new_tier_key = -1;
            foreach ($obj_users as $obj_user)
            {
                $tc_user = $obj_user->tiercriteria;
                $this->info('previous demotion: ' . $previous_demotion);
                $sum_pointsRewarded = Transaction::select(DB::raw('SUM(points_rewarded) AS trx_reward, user_id'))->where('user_id', $obj_user->id)->where('partner_id', $partner_id)
                    ->where('created_at', '>', $previous_demotion)->get();
                //$this->info("- Running user #{$obj_user->id} : {$sum_pointsRewarded}");
                if (is_null($sum_pointsRewarded[0]->trx_reward))
                {
                    $sum_pointsRewarded[0]->trx_reward = 0;
                }
                $this->info("- Running user #{$obj_user->id} : {$sum_pointsRewarded}");
                if ($promotion_older_demotion == True)
                {
                    $sum_pointsRewarded = Transaction::select(DB::raw('SUM(points_rewarded) AS trx_reward'))->where('user_id', $obj_user->id)->where('partner_id', $partner_id)
                        ->where('created_at', '>', $previous_promotion)->get();
                }

                foreach ($tc_user as $tcu)
                {

                    if (isset($tcu->tiercriteria_id))
                    {
                        $arr_key = array_search($tcu->tiercriteria_id, array_column($arr_ordered_tiercriterion, '1'));
                        if ($arr_key !== FALSE)
                        {
                            $current_tier_key = $arr_key;
                        }
                    }

                }
                $new_tier_key = 0;
//                            var_dump($sum_pointsRewarded[0]->trx_reward);
//                            exit();
                foreach ($arr_ordered_tiercriterion as $ordered_key => $orderedCriterion)
                {
                    if ($orderedCriterion[0] <= $sum_pointsRewarded[0]->trx_reward && $orderedCriterion[2] >= $sum_pointsRewarded[0]->trx_reward)
                    {
                        $new_tier_key = $orderedCriterion[1];
                        break;
                    }
                }
                $buffer = [];
                if ($new_tier_key > $current_tier_key && $run_promotion == 0 && $new_tier_key != 0)
                {
                    $buffer[] = [
                        'tiercriteria_id' => $new_tier_key,
                        'user_id'         => $obj_user->id,
                        'created_at'      => date("Y-m-d H:i:s"),
                        'updated_at'      => date("Y-m-d H:i:s"),
                    ];
                } elseif ($new_tier_key < $current_tier_key && $run_demotion == 0 && $new_tier_key != 0)
                {
                    $buffer[] = [
                        'tiercriteria_id' => $new_tier_key,
                        'user_id'         => $obj_user->id,
                        'created_at'      => date("Y-m-d H:i:s"),
                        'updated_at'      => date("Y-m-d H:i:s"),
                    ];
                }
//                            var_dump($buffer);
//                            exit();
                if (!empty($buffer))
                {
                    DB::table('tiercriteria_user')->insert($buffer);
                }

            }
//                        foreach($tiercriteria as $tiercriterion){
//                            //DB::table('tiercriteria_user')->where('tiercriteria_id', $tiercriterion->id)->delete();
//
////                            $arr_user_ids = array();
////                            foreach($users->get() as $usr){
////                                $arr_user_ids[] = $usr->id;
////                            }
////                            $current_tier = DB::table('tiercriteria_user')->where('user_id', $tiercriterion->user_id)->get();
//
//                            switch($tiercriterion->criterion) {
//                                case "RewardedPoints":
//                                    $min        = (int)$tiercriterion->start_value;
//                                    $max        = (int)$tiercriterion->end_value;
//                                    if($min > $max && $max == 0){
//                                        $max = $min + 1000000000;
//                                    }
//
//                                    if($max < $min) {
//                                            $max = ($min + 1);
//                                    }
//
//
//
//
//                                    $users->whereHas('transactions', function($q) use($params, $start_date, $end_date, $min, $max) {
//                                            $q->where('transaction.created_at', '>=', $date_previous_promotion);
//                                            $q->where('transaction.created_at', '<=', $date_promotion_months_ago);
//                                            $q->havingRaw("SUM(transaction.points_rewarded) >= $min");
//                                            $q->havingRaw("SUM(transaction.points_rewarded) <= $max");
//                                            $q->groupBy('transaction.user_id');
//                                    });
//                                    $buffer = array();
//
//                                    foreach($users->get()->pluck('id') as $k => $v) {
//                                            if($v !== 0) {
//                                                    $buffer[] = array(
//                                                            'segment_id' => $segment->id,
//                                                            'user_id'	 => $v
//                                                    );
//                                            }
//                                    }
//
//                                    if(!empty($buffer)) {
//                                            DB::table('segment_user')->insert($buffer);
//
//                                            $segment->last_run = date('Y-m-d H:i:s', time());
//                                            $segment->save();
//                                    }
//                                    break;
//                            }
//
//
//
//                        }


            // Re-map the segments & users


        }
    }
}
