<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;


class TestCommand extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:comms';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Test the outgoing communication mechanisms - email & SMS';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        // Send test email and SMS's
    }
}
