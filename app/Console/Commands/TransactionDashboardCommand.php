<?php

namespace App\Console\Commands;


use Illuminate\Console\Command;
use App\Transaction;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class TransactionDashboardCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'transaction:dashboard';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate daily data for dashboard.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->window = 30;
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        // transactionOverview()
        $results = Transaction::select(
            'partner_id',
            DB::raw("DATE_FORMAT(created_at, '%e/%m/%Y') AS date"),
            DB::raw('SUM(trx_reward) AS trx_reward'),
            DB::raw('SUM(points_rewarded) AS points_rewarded'),
            DB::raw('SUM(points_redeemed) AS points_redeemed')
        );

        $results->where('created_at', '>=', Carbon::now()->subDays($this->window)->toDateTimeString());
        $results->groupBy(DB::raw('DATE(created_at)'));
        $results->groupBy('partner_id');
        $results->orderBy('created_at', 'ASC');

        $entries = $results->get();

        $transaction_dates = [];
        $transaction_dates_db = DB::table('transaction_dates')->where('transaction_date', '>=', Carbon::now()->subDays($this->window)->toDateString())
            ->select('transaction_date', 'partner_id')->get();
        foreach ($transaction_dates_db as $transaction_date_db) {
            $transaction_dates[] = $transaction_date_db->transaction_date . '-' . $transaction_date_db->partner_id;
        }

        foreach ($entries as $entry) {
            $entry->date = Carbon::createFromFormat('d/m/Y', $entry->date)->toDateString();
            if (! in_array($entry->date . '-' . $entry->partner_id, $transaction_dates)) {
                DB::insert('insert into transaction_dates (partner_id, transaction_date, trx_reward, points_rewarded, points_redeemed) values (?, ?, ?, ?, ?)', [
                    $entry->partner_id,
                    $entry->date,
                    $entry->trx_reward,
                    $entry->points_rewarded,
                    $entry->points_redeemed
                ]);
            } else {
                DB::update('update transaction_dates set trx_reward = ?, points_rewarded = ?, points_redeemed = ? where transaction_date = ? and partner_id = ?', [
                    $entry->trx_reward,
                    $entry->points_rewarded,
                    $entry->points_redeemed,
                    $entry->date,
                    $entry->partner_id
                ]);
            }
        }

        // redeemersEarners()
        $results = Transaction::where('voided', false);

        $results->select(
            'user_id',
            'partner_id',
            DB::raw('SUM(points_redeemed) AS points_redeemed'),
            DB::raw('SUM(points_rewarded) AS points_rewarded')
        );
        $results->where('created_at', '>=', Carbon::now()->subDays($this->window)->toDateTimeString());
        $results->groupBy('user_id');
        $results->groupBy('partner_id');

        $entries = $results->get();

        $transaction_points = [];
        $transaction_points_db = DB::table('transaction_points')->where('date_from', '>=', Carbon::now()->subDays($this->window)->toDateString())
            ->select('date_from', 'date_to', 'partner_id')->get();
        foreach ($transaction_points_db as $transaction_point_db) {
            $transaction_points[] = $transaction_point_db->date_from . '-' . $transaction_point_db->date_to . '-' .$transaction_point_db->partner_id;
        }

        foreach ($entries as $entry) {
            $date_from = Carbon::today()->subDays($this->window)->toDateString();
            $date_to = Carbon::today()->toDateString();
            if (! in_array($date_from.'-'.$date_to.'-'.$entry->partner_id, $transaction_points)) {
                DB::insert('insert into transaction_points (date_from, date_to, user_id, partner_id, points_redeemed, points_rewarded) values (?, ?, ?, ?, ?, ?)', [
                    $date_from,
                    $date_to,
                    $entry->user_id,
                    $entry->partner_id,
                    $entry->points_redeemed,
                    $entry->points_rewarded
                ]);
            } else {
                DB::update('update transaction_points set points_rewarded = ?, points_redeemed = ? where date_from = ? and date_to = ? and user_id = ? and partner_id = ?', [
                    $entry->points_rewarded,
                    $entry->points_redeemed,
                    $date_from,
                    $date_to,
                    $entry->user_id,
                    $entry->partner_id
                ]);
            }
        }
    }
}
