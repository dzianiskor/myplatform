<?php
namespace App\Console\Commands;

use App\Balance;
use App\Partner;
use App\Product;
use App\Segment;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class SegmentCommand extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'segments:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync member segments based on criteria';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("Running Segment Mapping");

        $segments = Segment::where('draft', false)->where('manual', false)->get();

        foreach($segments as $segment) {

            $this->info("- Running Segment #{$segment->id} : {$segment->name}");

            // First remove existing mapping
            DB::table('segment_user')->where('segment_id', $segment->id)->delete();

            // Start with all users
            $users = User::where('draft', false);

            // Limit to partner hierarchy?
            $partner_constrained = false;

            // Buffer for JSON payloads
            $params = null;

            // Construct mapping query
            foreach($segment->attributes as $a) {

                $this->info("-- Segmentation Attribute {$a->name}: #{$a->id}");

                $params = json_decode($a->json_data);

                switch($a->name) {

                    // User based intent
                    case "user.email":
                        $users->where('email', 'like', "%{$params->email}%");
                        break;
                    case "user.role":
                        $users->whereHas('roles', function($q) use($params) {
                            $q->where('role.id', $params->role);
                        });
                        break;
                    case "user.verified":
                        $verfied = ($params->verification == true)? 1 : 0;
                        $users->where('verified', $verfied);
                        break;
                    case "user.title":
                        $users->where('title', $params->title);
                        break;
                    case "user.status":
                        $users->where('status', $params->status);
                        break;
                    case "user.first_name":
                        $users->where('first_name', 'like', "%{$params->first_name}%");
                        break;
                    case "user.last_name":
                        $users->where('last_name', 'like', "%{$params->last_name}%");
                        break;
                    case "user.gender":
                        $users->where('gender', '=', $params->gender);
                        break;
                    case "user.marital_status":
                        $users->where('marital_status', '=', $params->marital_status);
                        break;
                    case "user.age":
                        $min = (int)$params->min_age;
                        $max = (int)$params->max_age;

                        if($min < 1)    { $min = 1;    }
                        if($max < $min) { $max = $min; }
                        if($max > 100)  { $max = 100;  }

                        $min = date("Y-m-d", strtotime("-$min years", time()));
                        $max = date("Y-m-d", strtotime("-$max years", time()));

                        $users->where('dob', '>=', $max);
                        $users->where('dob', '<=', $min);

                        //$users->whereBetween('dob', array($max, $min));
                        break;
                    case "user.country":
                        $users->where('country_id', $params->country);
                        break;
                    case "user.city":
                        $users->where('city_id', $params->city);
                        break;
                    case "user.area":
                        $users->where('area_id', $params->area);
                        break;
                    case "user.income_bracket":
                        $users->where('income_bracket', $params->income_bracket);
                        break;
                    case "user.occupation":
                        $users->where('occupation_id', $params->occupation);
                        break;
                    case "user.tags":
                        $tags = explode(',', $params->tags);

                        $users->where(function($query) use($tags) {
                            foreach($tags as $t) {
                                $t = trim($t);
                                $query->orWhere('tag', 'like', "%{$t}%");
                            }
                        });
                        break;
                    case "user.num_children":
                        $users->where('num_children', $params->num_children);
                        break;

                    case "user.balance":
                        $minBalance = $params->min_balance;
                        $maxBalance = $params->max_balance;

                        $partnerId = $segment->partner_id;
                        //echo "Partner_id = " . $partnerId;
                        // get the partner
                        $partner = Partner::find($partnerId);
                        //var_dump($partner);
                        // get the network id of the partner
                        $network = $partner->firstNetwork();
                        $networkId = $network->id;
                        $partner_users = $partner->members;
                        $arr_partner_users = array();
                        foreach($partner_users as $p_user){
                            $arr_partner_users[] = $p_user->id;
                        }
                        if(count($arr_partner_users) == 0){
                            $arr_partner_users[] = "-1";
                        }
                        //var_dump($networkId);
                        // get all user ids  with balances for this network with balance greater than the param min balance and less than max balance
                        $balanceNetworkUsers = Balance::select('balance.user_id')
                            ->where('balance.network_id', $networkId)
                            ->where("balance.balance", '>=',$minBalance)
                            ->where("balance.balance", '<=',$maxBalance)
                            ->whereIn("balance.user_id",$arr_partner_users)->get();
                        //$csvBalanceNetworkUsers = implode(',',$balanceNetworkUsers->toArray());
//						var_dump($balanceNetworkUsers);
                        $balanceUsers = array(-1);
                        foreach($balanceNetworkUsers as $balanceNetworkUser){
                            $balanceUsers[] = $balanceNetworkUser->user_id;
                        }
                        if(count($balanceUsers) == 0){
                            $balanceUsers[] = "-1";
                        }
                        //get the user for this partner and the user id is in list of user that belongs to this network with
                        //balance greater than param min balance and less than max balance from balance table
                        //var_dump(implode(',',$balanceUsers));
                        $users->whereHas('partners', function($q) use($partnerId) {
                            $q->where('partner.id', $partnerId);
                        })->whereIn("users.id", $balanceUsers);

                        break;

                    // Partner based intent [@TODO: This could clash with managed partners]
                    case "partner.name":
                        $partner_constrained = true;

                        $users->whereHas('partners', function($q) use($params) {
                            $q->where('partner.id', $params->partner);
                        });
                        break;

                    // Transaction based intent
                    case "transactions.points_earned":
                        $start_date = date("Y-m-d H:i:s", strtotime($params->earn_start_date));
                        $end_date   = date("Y-m-d H:i:s", strtotime($params->earn_end_date));
                        $min        = (int)$params->min_points_earn;
                        $max        = (int)$params->max_points_earn;

                        if($max < $min) {
                            $max = ($min + 1);
                        }

                        $users->whereHas('transactions', function($q) use($params, $start_date, $end_date, $min, $max) {
                            $q->where('transaction.created_at', '>=', $start_date);
                            $q->where('transaction.created_at', '<=', $end_date);
                            $q->havingRaw("SUM(transaction.points_rewarded) >= $min");
                            $q->havingRaw("SUM(transaction.points_rewarded) <= $max");
                            $q->groupBy('transaction.user_id');
                        });
                        break;

                    case "transactions.points_redeemed":
                        $start_date = date("Y-m-d H:i:s", strtotime($params->redeem_start_date));
                        $end_date   = date("Y-m-d H:i:s", strtotime($params->redeem_end_date));
                        $min        = (int)$params->min_points_redeem;
                        $max        = (int)$params->max_points_redeem;

                        if($max < $min) {
                            $max = ($min + 1);
                        }

                        $users->whereHas('transactions', function($q) use($params, $start_date, $end_date, $min, $max) {
                            $q->where('transaction.created_at', '>=', $start_date);
                            $q->where('transaction.created_at', '<=', $end_date);
                            $q->havingRaw("SUM(transaction.points_redeemed) >= $min");
                            $q->havingRaw("SUM(transaction.points_redeemed) <= $max");
                            $q->groupBy('transaction.user_id');
                        });
                        break;

                    case "transactions.spend":
                        $start_date = date("Y-m-d H:i:s", strtotime($params->spend_start_date));
                        $end_date   = date("Y-m-d H:i:s", strtotime($params->spend_end_date));
                        $min        = (int)$params->min_spend;
                        $max        = (int)$params->max_spend;

                        if($max < $min) {
                            $max = ($min + 1);
                        }

                        $users->whereHas('transactions', function($q) use($params, $start_date, $end_date, $min, $max) {
                            $q->where('transaction.created_at', '>=', $start_date);
                            $q->where('transaction.created_at', '<=', $end_date);
                            $q->havingRaw("SUM(transaction.total_amount) >= $min");
                            $q->havingRaw("SUM(transaction.total_amount) <= $max");
                            $q->groupBy('transaction.user_id');
                        });
                        break;

                    case "transactions.product.name":
                        $products = Product::select('id')->where("name", 'like', "%{$params->product_name}%")->get();
//						foreach($products as $product){
//                                                    $product_id = $product->id;
//                                                     $users->whereHas('transactions', function($query) use($product_id) {
//                                                          $query->whereHas('items', function($q) use($product_id) {
//                                                              $q->where("product_id", $product_id);
//                                                               });
//
//                                                    });
//                                                }
                        /*
                         * the below query gets the users that have one of the products
                         * where as the above get users with all products ( users that have 4 products at same time
                         * for example )
                         */
                        $product_ids = array();
                        foreach($products as $product){
                            $product_ids[] = $product->id;
                        }
                        if(count($product_ids) == 0){
                            $product_ids[] = "-1";
                        }
                        if(isset($product_ids) && !empty($product_ids) ){
                            $users->whereHas('transactions', function($query) use($product_ids) {
                                //$query->where("product_name", 'like', "%{$params->product_name}%");
//                                                        $query->whereIn("transaction.product_id", $product_ids);
                                $query->whereHas('items', function($q) use($product_ids) {
                                    $q->whereIn("product_id", $product_ids);
                                });

                            });
                        }
                        break;
                    case "transactions.product.brand":
                        $users->whereHas('transactions', function($query) use($params) {
//							$query->where("product_brand_id", $params->product_brand);
                            $query->whereHas('items', function($q) use($params) {
                                $q->where("product_brand_id", $params->product_brand);
                            });
                        });
                        break;
                    case "transactions.product.model":
//						$users->whereHas('transactions', function($query) use($params) {
//							$query->where("product_model", $params->product_model);
//						});
                        $users->whereHas('transactions', function($query) use($params) {
                            $query->whereHas('items', function($q) use($params) {
                                $q->where("product_model",'like', "%{$params->product_model}%");
                            });
                        });
                        break;
                    case "transactions.location.city":
                        $users->whereHas('transactions', function($query) use($params) {
                            $query->where("city_id", $params->transaction_city);
                        });
                        break;
                    case "transactions.location.area":
                        $users->whereHas('transactions', function($query) use($params) {
                            $query->where("area_id", $params->transaction_area);
                        });
                        break;
                    case "transactions.location.country":
                        $users->whereHas('transactions', function($query) use($params) {
                            $query->where("country_id", $params->transaction_country);
                        });
                        break;
                }
            }

            // Constrain to segment partner hierarchy
            if(!$partner_constrained) {

                $segment_partner_id = $segment->partner_id;

                $users->whereHas('partners', function($q) use($segment_partner_id) {
                    $q->where('partner.id', $segment_partner_id);
                });
            }

            // Re-map the segments & users

            $buffer = array();

            foreach($users->get()->pluck('id') as $k => $v) {
                if($v !== 0) {
                    $buffer[] = array(
                        'segment_id' => $segment->id,
                        'user_id'	 => $v
                    );
                    if(count($buffer)>500){
                        DB::table('segment_user')->insert($buffer);

                        $segment->last_run = date('Y-m-d H:i:s', time());
                        $segment->save();
                        unset($buffer);
                        $buffer = array();

                    }
                }
            }

            if(!empty($buffer)) {
                DB::table('segment_user')->insert($buffer);

                $segment->last_run = date('Y-m-d H:i:s', time());
                $segment->save();
            }
        }
    }
}
