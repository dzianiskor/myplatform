<?php

namespace App\Console\Commands;

use App\Http\Controllers\API2Controller;
use Illuminate\Console\Command;


class BsfCommand extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bsf:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'BSF Report generation and send';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $yesterday = date("Y-m-d", strtotime('-1 days'));
        $today = date('Y-m-d');
        // Send test email and SMS's
        $response = API2Controller::getBsfReports($yesterday, $today, '4249');
        $this->info($response);
    }
}
