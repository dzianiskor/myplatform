<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class BsfwwwCommand extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bsfwww:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'BSF Report generation and send';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $yesterday = date("Y-m-d", strtotime( '-1 days' ) );
        $today = date('Y-m-d');
        // Send test email and SMS's
        //$response = API2Controller::getBsfwwwReports($yesterday,$today,'4206');
        $response = "skipped";
        $this->info($response);
    }
}
