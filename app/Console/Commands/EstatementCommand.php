<?php

namespace App\Console\Commands;

use App\Http\Controllers\APIController;
use App\Http\Controllers\EstatementController;
use Illuminate\Console\Command;
use App\Estatement;


class EstatementCommand extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'estatement:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'run estatements for users';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $day_of_the_month = date("d");
        $time_of_the_day = date("H:i:s",strtotime("-1 hours"));
        $now_time= date("H:i:s");
        $int_timeofday = intval(str_replace(':', '', $time_of_the_day));
        $int_nowtime = intval(str_replace(':','',$now_time));
        if($int_timeofday > $int_nowtime && $int_timeofday > 231000){
            $time_of_the_day = "00:00:00";
        }
        elseif($int_timeofday > $int_nowtime && $int_nowtime <= 231000){
            $now_time = "23:59:59";
        }
        $estatements = Estatement::where('delivery_date',$day_of_the_month)->where('draft',0)->where('time','>',$time_of_the_day)->where('time','<=',$now_time)->get();
        //var_dump($estatements);
        if($estatements->count() == 0){
            $this->info("No Estatements");
            exit();
        }
        foreach($estatements as $e){
            $json_data = json_encode(array("Estatement" => $e->id));
            APIController::postSendEmailJson($json_data);
//                $response = EstatementController::getSendOutEstatement(base64_encode($e->id));
            $response = EstatementController::fireBulkEstatement(base64_encode($e->id));
        }
    }
}
