<?php
namespace App\Console\Commands;

use App\Classes\PathHelper;
use App\Classes\ReportExportHelper;
use App\Mail\MailExportReportSucceed;
use App\Transaction;
use AuditHelper;
use Illuminate\Console\Command;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class AlinmaDailyCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'transaction:daily_mis_file';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate daily data for Alinma.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->window = 15;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('- Start Alinma Daily Cron Job -');

        $partnerId = 4385;
        $results = Transaction::select('transaction.user_id as us_id',
                                       'reference.number as ref_number',
                                       DB::raw('sum(points_rewarded) pts_rew'),
                                       DB::raw('sum(points_redeemed) pts_red'),
                                       DB::raw('(SELECT SUM(points_rewarded) FROM `transaction` WHERE ref_number LIKE \'Periodic Expired Points\' and user_id = us_id) as ptx_exp'))
                                       ->join('reference', 'transaction.user_id', '=', 'reference.user_id')
                                       ->where('transaction.created_at', '>=', Carbon::today()->subDays(16))
                                       ->where('transaction.partner_id', $partnerId)
                                       ->groupBy('transaction.user_id')
                                       ->orderBy('transaction.created_at')
                                       ->get();

        $x = 0;
        $data = [];
        if (!$results->isEmpty()) {
            foreach ($results as $key => $item) {
                ++$x;
                if ($item->pts_rew == "0.00" && $item->pts_red == "0.00") {
                    continue;
                }
                $data[$key]['Reference ID']    = $item->ref_number;
                $data[$key]['Rewarded Points'] = $item->pts_rew;
                $data[$key]['Redeemed Points'] = $item->pts_red;
                $data[$key]['Expired Points']  = (is_numeric($item->ptx_exp)) ? $item->ptx_exp : '0.00';
                $data[$key]['Line Number']     = $x;
            }
            $data[] = 'EOF';
        }

        $fileNameGenerated = 'AlinmaMisFile' . '_' . Carbon::now()->format('Ymd');

        return ($data) ?
            $this->exportReportSucceed($data, $fileNameGenerated, 'Alinma Daily')
            : self::respondWith( 200, "empty result", [], 'errors');

        $this->info('- Done -');
    }

    protected function exportReportSucceed($data, $baseFileName, $subjectSuffix)
    {
        Mail::send(
            new MailExportReportSucceed(
                $realFileName = ReportExportHelper::makeSuccessReportFile(
                    $data,
                    $baseFileName,
                    $folder = 'misc'
                ),
                $subjectSuffix
            )
        );

        return $this->responseExportReportSucceed($baseFileName);
    }

    protected function responseExportReportSucceed($baseFileName)
    {
        AuditHelper::record(
            4, 'Finished processing ' . $baseFileName . '.txt with 0 errors.'
        );

        return self::respondWith(
            200, "OK", [], 'errors'
        );
    }
}
