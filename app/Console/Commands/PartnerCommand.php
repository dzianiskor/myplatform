<?php

namespace App\Console\Commands;

use App\Partner;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;


class PartnerCommand extends Command {

    const DYNAMIC_BIND = 'dynamic';
    const STATIC_BIND  = 'static';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'partners:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync members to partners based on award history';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->info("Running Partner -> User Mapping");

        Partner::where('draft', false)->chunk(200, function($partners) {

            $now = date('Y-m-d H:i:s', time());
            $t12 = date('Y-m-d H:i:s', strtotime("-12 months"));
            $t24 = date('Y-m-d H:i:s', strtotime("-24 months"));

            foreach($partners as $partner) {

                // Delete dynamic user bindings only

                DB::table('partner_user')->where('partner_id', $partner->id)
                    ->where('type', PartnerCommand::DYNAMIC_BIND)
                    ->delete();

                // Aggregate the users points from the last 24 months

                $transactions = DB::table('transaction');
                $transactions->where('partner_id', $partner->id);
                $transactions->where('created_at', '>=', $t24);
                $transactions->select(DB::raw('SUM(points_rewarded) AS points_rewarded, user_id'));
                $transactions->groupBy('user_id');

                $buffer = array();

                foreach($transactions->get() as $t) {

                    if($t->points_rewarded >= (int)$partner->threshold_24_months) {

                        $buffer[] = array(
                            'partner_id' => $partner->id,
                            'user_id'    => $t->user_id,
                            'type'		 => PartnerCommand::DYNAMIC_BIND
                        );
                    }
                    /*
                    if($t->points_rewarded >= (int)$partner->threshold_12_months) {

                        $buffer[] = array(
                            'partner_id' => $partner->id,
                            'user_id'    => $t->user_id,
                            'type'		 => PartnerCommand::DYNAMIC_BIND
                        );
                    }
                    */
                }

                if(count($buffer) > 0) {
                    DB::table('partner_user')->insert($buffer);
                }

                $this->info("- Syncing Partner #{$partner->id} #{$partner->name}. Count:".count($buffer));
            }
        });
    }
}
