<?php

namespace App\Console\Commands;

use App\CurrencyPricing;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class CurrencyCommand extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'currency:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync currencies with the latest exchange rates.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $selectedCurrencies = DB::table('currency')->get();

        $rates = file_get_contents('https://openexchangerates.org/api/latest.json?app_id=' . Config::get('oer.app_id'));
        $rates = json_decode($rates);
        $rates = (array)$rates->rates;

        // Only update currencies present in the data from OER

        foreach($selectedCurrencies as $currency) {

            $this->info("Updating: {$currency->short_code}");

            if(array_key_exists($currency->short_code, $rates)) {
                $c              = new CurrencyPricing();
                $c->currency_id = $currency->id;
                $c->date        = date('Y-m-d');
                $c->rate        = $rates[$currency->short_code];

                $c->save();

                $this->info(" -> {$c->rate}");
            }
        }

        $this->info('- Done -');
    }
}
