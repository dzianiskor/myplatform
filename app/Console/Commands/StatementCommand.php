<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;


class StatementCommand extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'statements:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'run statements for users';

    private function sendStatement($user) {
        $start = date("Y-m-01", time());
        $end   = date("Y-m-t", time()); //t = number of days in a given month

        $transactions = $user->transactions()
            ->where('created_at', '>=', $start)
            ->where('created_at', '<=', $end)
            ->orderBy('created_at');
        if ($transactions->count() > 0) {
            $transactions = $transactions->get();
            $openingBalance = $user->transactions()
                ->where('created_at', '<', $start)
                ->get(array(
                        DB::raw('SUM((0-points_redeemed) + points_rewarded) as total')
                    )
                )
                ->first()
                ->total;
            $statement = View::make("emails.statement",
                array(
                    'user'          => $user,
                    'openingBalance'=> $openingBalance,
                    'transactions'  => $transactions,
                    'start'         => $start,
                    'end'           => $end,
                )
            )
                ->render();

            $mail = new SendGrid\Email();
            $mail->addTo($user->email);
            $mail->setFrom(Config::get('blu.email_from'));
            $mail->setFromName('BLU Points');
            $mail->setSubject("Blu Statement : {$start} - {$end}");
            $mail->setHtml($statement);

            $userName = Config::get('sendgrid.username');
            $password = Config::get('sendgrid.password');
            $sendGrid = new SendGrid($userName, $password);
            $sendGrid->send($mail);
        }
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        User::where('draft', 0)
            ->orderBy('id', 'desc')
            ->where('verified', 1)
            ->where('email', '!=', '')
            ->chunk(200, function($users) {
                foreach ($users as $user) {
                    $this->sendStatement($user);
                }
            });
    }
}
