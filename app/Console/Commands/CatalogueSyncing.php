<?php
namespace App\Console\Commands;

use App\Brand;
use App\Media;
use App\Product;
use App\Supplier;
use App\SystemCronLog;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CatalogueSyncing extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'catalogue:syncing';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Syncing catalog';

    /**
     * @var int
     */
    protected $partnerId = 4247;

    /**
     * @var int
     */
    protected $currencyId = 6;

    /**
     * @var string
     */
    protected $commandName = 'catalogue_syncing';

    /**
     * @var string
     */
    protected $apiUrl = 'https://blutest.bankofbeirut.com/api/catalogue-syncing/';

    /**
     * @var string
     */
    protected $apiKey = '4825f8d06c87b98a6386323a18b1d246';

    /**
     * @var string
     */
    protected $filenameSeparator = '|,';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $cronInfo = $this->getCronInfo();
        $this->info('Last cron run date: '. $cronInfo->lastRunDate());

        $brands = $this->getBrandsList($cronInfo->lastRunDate());
        $this->info('Found brands: '. $brands->count());

        $suppliers = $this->getSuppliersList($cronInfo->lastRunDate());
        $this->info('Found suppliers: '. $suppliers->count());

        $products = $this->getProductsList($cronInfo->lastRunDate());
        $this->info('Found products: '. $products->count());

        if ($brands->count() > 0) {
            $this->info('Uploading brands...');
            $this->send('brands', json_encode($brands));
        }

        if ($suppliers->count() > 0) {
            $this->info('Uploading suppliers...');
            $this->send('suppliers', json_encode($suppliers));
        }

        if ($products->count() > 0) {
            $this->info('Uploading products...');
            $this->send('products', json_encode($products));
        }

//        $this->updateCronLogDate($cronInfo);
        $this->info('Success');
    }

    /**
     * @param Carbon $startDate
     *
     * @return Product[]
     */
    protected function getProductsList(Carbon $startDate)
    {
        $products = DB::table('product')
                 ->select(
                     'product.*',
                     'cover_media.name as cover',
                     DB::raw('GROUP_CONCAT(media.name SEPARATOR "' . $this->filenameSeparator . '") as images')
                 )
                 ->leftJoin('product_partner_redemption', 'product_partner_redemption.product_id', '=', 'product.id')
                 ->leftJoin('product_redemption_countries', 'product_redemption_countries.product_id', '=', 'product.id')
                 ->leftJoin('media_product', 'media_product.product_id', '=', 'product.id')
                 ->leftJoin('media', 'media_product.media_id', '=', 'media.id')
                 ->leftJoin('media as cover_media', 'product.cover_image', '=', 'cover_media.id')
                 ->join('partner_product', 'product.id', '=', 'partner_product.product_id')
//                 ->where('partner_product.partner_id', $this->partnerId)
                 ->where('product.draft', 0)
//                 ->where('product_partner_redemption.currency_id', $this->currencyId)
//                 ->where('product_redemption_countries.currency_id', $this->currencyId)
                 ->where(function ($query) use ($startDate) {
                     $query->whereDate('product.created_at', '>', $startDate->toDateString())
                           ->orWhereDate('product.updated_at', '>', $startDate->toDateString());
                 })
            ->groupBy('product.id')
                 ->get();

        foreach ($products as $key => $product) {
            $media = [
                'cover' => null,
                'images' => [],
            ];

            if ($content = $this->getBinaryMedia($product->cover)) {
                $media['cover'] = [
                    'filename' => $product->cover,
                    'content' => $content,
                ];
            }

            $images = explode($this->filenameSeparator, $product->images);
            foreach ($images as $image) {
                if ($content = $this->getBinaryMedia($image)) {
                    $media['images'][$image] = $content;
                }
            }

            $product->media = $media;
        }

        return $products;
    }

    /**
     * @param string $filename
     *
     * @return bool|string
     */
    protected function getBinaryMedia($filename)
    {
        $media = new Media();
        $media->name = $filename;

        if (!$filename || !$media->fileExists()) {
            return false;
        }

        return base64_encode(file_get_contents($media->getFilePath()));
    }

    /**
     * @param Carbon $startDate
     *
     * @return Brand[]
     */
    protected function getBrandsList(Carbon $startDate)
    {
        return DB::table('brand')
                 ->select(
                     'brand.id',
                     'brand.name'
                 )
                 ->join('brand_partner', 'brand.id', '=', 'brand_partner.brand_id')
                 ->where('brand_partner.partner_id', $this->partnerId)
                 ->where('brand.draft', 0)
                 ->where(function ($query) use ($startDate) {
                     $query->whereDate('brand.created_at', '>', $startDate->toDateString())
                           ->orWhereDate('brand.updated_at', '>', $startDate->toDateString());
                 })
                 ->get();
    }

    /**
     * @param Carbon $startDate
     *
     * @return Supplier[]
     */
    protected function getSuppliersList(Carbon $startDate)
    {
        return DB::table('supplier')
                 ->select(
                     'supplier.id',
                     'supplier.ucid',
                     'supplier.name',
                     'supplier.image',
                     'supplier.description',
                     'supplier.email',
                     'supplier.website',
                     'supplier.phone_number',
                     'supplier.contact_name',
                     'supplier.contact_email',
                     'supplier.contact_number',
                     'supplier.address_1',
                     'supplier.address_2',
                     'supplier.country_id',
                     'supplier.city_id',
                     'supplier.area_id',
                     'supplier.status',
                     'supplier.parent_id',
                     'supplier.category_id'
                 )
                 ->join('supplier_partner', 'supplier.id', '=', 'supplier_partner.supplier_id')
                 ->where('supplier_partner.partner_id', $this->partnerId)
                 ->where('supplier.draft', 0)
                 ->where(function ($query) use ($startDate) {
                     $query->whereDate('supplier.created_at', '>', $startDate->toDateString())
                           ->orWhereDate('supplier.updated_at', '>', $startDate->toDateString());
                 })
                 ->get();
    }

    /**
     * @return SystemCronLog
     */
    protected function getCronInfo()
    {
        $cronInfo = SystemCronLog::where('partner_id', $this->partnerId)
                                 ->where('command', $this->commandName)
                                 ->first();

        if (!$cronInfo) {
            $cronInfo = new SystemCronLog();
            $cronInfo->command = $this->commandName;
            $cronInfo->partner_id = $this->partnerId;
            $cronInfo->setLastRunDate(Carbon::now());
        }

        return $cronInfo;
    }

    /**
     * @param SystemCronLog $cronLog
     *
     * @return SystemCronLog
     */
    protected function updateCronLogDate(SystemCronLog $cronLog)
    {
        $cronLog->setLastRunDate(Carbon::now());
        $cronLog->save();

        return $cronLog;
    }

    /**
     * @param string $type
     * @param string $data
     *
     * @return mixed
     */
    protected function send($type, $data)
    {
        try {
            $ch = curl_init($this->apiUrl . $type . '?api_key=' . $this->apiKey);

            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($data))
            );

            $result = curl_exec($ch);

            return $result;
        } catch (\Error $e) {
            Log::error('Error while sending ' . $type);
        }
    }
}
