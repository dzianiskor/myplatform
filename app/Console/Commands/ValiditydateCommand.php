<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;


class ValiditydateCommand extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'validity:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updates entries that their validity dates are expired';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $date = date('Y-m-d');

        $this->info('- Updating out of validity Products');

        DB::table('product')->where('end_date', '<', $date)->Where('end_date', '!=', '1970-01-01')->Where('end_date', '!=', '0000-00-00')->WhereNotNull('end_date')->update(['display_online' => 0]);

        $this->info('- Updating out of validity Products on the new product pricing module');

        DB::table('product_partner_redemption')->where('end_date', '<', $date)->Where('end_date', '!=', '1970-01-01')->Where('end_date', '!=', '0000-00-00')->WhereNotNull('end_date')->update(['display_online' => 0]);


        $this->info('- Updating out of validity Banners');

        DB::table('banner')->where('end_date', '<', $date)->Where('end_date', '!=', '1970-01-01')->Where('end_date', '!=', '0000-00-00')->WhereNotNull('end_date')->update(['status' => 'Offline']);
    }
}
