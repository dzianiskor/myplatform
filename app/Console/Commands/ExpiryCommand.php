<?php

namespace App\Console\Commands;

use App\Tier;
use App\Tiercriteria;
use App\Transaction;
use App\TransactionDeduction;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;


class ExpiryCommand extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'expiry:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync Expiry of points';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->info("Running Expiry of Points");
        $tiercriteria = Tiercriteria::where('draft',false)->get();



        foreach($tiercriteria as $tiercriterion) {

            $this->info("- Running Tier Criterion #{$tiercriterion->id} : {$tiercriterion->name}");
            $tier = Tier::where('id',$tiercriterion->tier_id)->first();
            $partner_id = $tier->partner_id;



            // Start with all users
            $users_ids = DB::table('tiercriteria_user')->select(DB::raw('user_id'))->where('tiercriteria_id', $tiercriterion->id)->get();
            $now_date = date("Y-m-d");
            $tempString = "-" . $tiercriterion->expiry_in_months . ' months';
            $new_timestamp = strtotime($tempString, strtotime($now_date));
            $date_of_expiry = date("Y-m-d",$new_timestamp);
            foreach($users_ids as $user){

                $this->info("Transactions for user " . $user->user_id);
                //$update_to_balance = 0;
                $user_trx_expiring = Transaction::where('created_at','<=',$date_of_expiry)->where('expired','0')->where('user_id',$user->user_id)->where('partner_id',$partner_id)->get();
                if(isset($user_trx_expiring)){
                    foreach($user_trx_expiring as $trx){
                        $this->info("Transaction For Expiry ID: ". $trx->id);
                        $trx_deduct = TransactionDeduction::where('trx_id',$trx->id)->orderBy('id','desc')->first();
                        $run_expiry = true;
                        if(isset($trx_deduct)){
                            if($trx_deduct->trx_used == 1){
                                $run_expiry = false;
                            }
                            elseif($trx_deduct->trx_used == 0){
                                $trx_deduct->trx_used = 1;
                                $trx_deduct->save();
                                $trx->expired = 1;
                                $trx->expired_at= $now_date;
                                $trx->save();
                                $this->info("Used Transaction ". $run_expiry );
                                //$update_to_balance += $trx->points_rewarded;
                                $user = User::find($trx->user_id);
                                $user->updateBalance($trx_deduct->points_remaining, $trx->network_id, 'subtract');
                                $run_expiry = false;
                            }
                        }
                        $this->info("Run Expiry ". $run_expiry );
                        if($run_expiry == true){
                            $trx->expired = 1;
                            $trx->expired_at= $now_date;
                            $trx->save();
                            //$update_to_balance += $trx->points_rewarded;
                            $user = User::find($trx->user_id);
                            $user->updateBalance($trx->points_rewarded, $trx->network_id, 'subtract');
                        }
                    }
                }
                else{
                    $this->info("NO transactions for user " . $user->user_id);
                }


            }

        }
    }
}
