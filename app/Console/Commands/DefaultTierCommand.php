<?php

namespace App\Console\Commands;

use App\Tier;
use App\Tiercriteria;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;


class DefaultTierCommand extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tiers:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync member tiers based on criteria';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->info("Running Default Tier affiliation");
        $criteriaByPartner = [];
        //get All default tiers
        $tiers = Tier::where('draft', false)->where('name', 'like', '%Default Tier -%')->get();

        foreach ($tiers as $tier)
        {
            $defaultTiercriteria = Tiercriteria::where('tier_id', $tier->id)->where('name', 'like', '%Default Tier Criteria -%')->first();
            if (!$defaultTiercriteria)
            {
                continue;
            }
            $criteriaByPartner[$tier->partner_id] = $defaultTiercriteria->id;
        }

        //get All Users
        $users = User::where('draft', false)->get();
        $buffer = [];
        foreach ($users as $user)
        {
            $userTierCriteriaArray = [];
            $userTierCriterias = $user->tiercriteria;
            foreach ($userTierCriterias as $userTierCriteria)
            {
                $userTierCriteriaArray[] = $userTierCriteria->id;
            }
            foreach ($user->partners as $userPartner)
            {
                if (!empty($criteriaByPartner[$userPartner->id]))
                {
                    $tierCriteria = $criteriaByPartner[$userPartner->id];
                    if (!in_array($tierCriteria, $userTierCriteriaArray))
                    {
                        $buffer[] = [
                            'tiercriteria_id' => $tierCriteria,
                            'user_id'         => $user->id,
                            'created_at'      => date("Y-m-d H:i:s"),
                            'updated_at'      => date("Y-m-d H:i:s"),
                        ];
                    }
                }
            }
        }
        if (!empty($buffer))
        {
            DB::table('tiercriteria_user')->insert($buffer);
        }
    }
}
