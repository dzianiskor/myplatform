<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CleanupCommand extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cleanup:drafts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cleanup draft entries in the BLU Database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $date = date('Y-m-d H:i:s', strtotime('-1 day'));

        $this->info('- Cleaning up Parter Drafts');

        DB::table('partner')->where('draft', true)->where('created_at','<=', $date)->delete();


        $this->info('- Cleaning up Store Drafts');

        DB::table('store')->where('draft', true)->where('created_at','<=', $date)->delete();


        $this->info('- Cleaning up Segment Drafts');

        DB::table('segment')->where('draft', true)->where('created_at','<=', $date)->delete();


        $this->info('- Cleaning up Batch Drafts');

        DB::table('batch')->where('draft', true)->where('created_at','<=', $date)->delete();


        $this->info('- Cleaning up Brand Drafts');

        DB::table('brand')->where('draft', true)->where('created_at','<=', $date)->delete();


        $this->info('- Cleaning up Category Drafts');

        DB::table('category')->where('draft', true)->where('created_at','<=', $date)->delete();

        $this->info('- Cleaning up Partner Category Drafts');

        DB::table('partnercategory')->where('draft', true)->where('created_at','<=', $date)->delete();

        $this->info('- Cleaning up Product Drafts');

        DB::table('product')->where('draft', true)->where('created_at','<=', $date)->delete();


        $this->info('- Cleaning up Segment Drafts');

        DB::table('segment')->where('draft', true)->where('created_at','<=', $date)->delete();


        $this->info('- Cleaning up Support Ticket Drafts');

        DB::table('ticket')->where('draft', true)->where('created_at','<=', $date)->delete();


        $this->info('- Cleaning up Member/User Drafts');

        DB::table('users')->where('draft', true)->where('created_at','<=', $date)->delete();
    }
}
