<?php

namespace App\Console\Commands;

use App\Notification;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Config;
use SendGrid;
use SMSHelper;


class NotificationCommand extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notifications:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send all SMS and email notifications';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Sending all notification schedules');

        $end = date("Y-m-d H:i:s", strtotime("+60 Minutes"));
        $start = date("Y-m-d H:i:s", time());

        $q = Notification::where('draft', false);
        $q->where('invoked', true);
        $q->where('sent', false);
        $q->where('start_date', '>=', $start);
        $q->where('start_date', '<=', $end);

        /*
            We're keeping these numbers low because
             each notification has its own segments that could
             potentially contain 1000's of users
         */

        $q->chunk(10, function($notifications) {

            foreach($notifications as $n) {

                $this->info("- Notification: {$n->name} / {$n->type}");

                $targets = array();

                foreach ($n->segments as $segment) {
                    foreach($segment->users as $u) {

                        if($n->type == 'Email') {
                            if(!in_array($u->email, $targets)) {
                                $targets[] = $u->email;
                            }
                        }

                        if($n->type == 'SMS') {
                            $number = $u->normalizedContactNumber();

                            if(!in_array($number, $targets)) {
                                $targets[] = $u;
                            }
                        }

                    }
                }

                foreach ($n->users as $user) {
                    if($n->type == 'Email') {
                        if(!in_array($user->email, $targets)) {
                            $targets[] = $user->email;
                        }
                    }

                    if($n->type == 'SMS') {
                        $number = $user->normalizedContactNumber();

                        if(!in_array($number, $targets)) {
                            $targets[] = $user;
                        }
                    }
                }
                $this->info("- Target count: ".count($targets));

                $partner = $n->partner;
                $parent  = $partner->topLevelPartner();

                // @TODO: We need to batch these recipients more efficiently

                if($partner) {

                    switch($n->type) {
                        case "Email":
                            $sendgrid = new SendGrid(
                                Config::get('sendgrid.username'), Config::get('sendgrid.password')
                            );

                            $mail = new SendGrid\Email();

                            $mail->setTos($targets);
                            $mail->setFrom(Config::get('blu.email_from'));
                            $mail->setFromName('BLU Points');
                            $mail->setSubject($n->subject);
                            $mail->setHtml($n->body);

                            $sendgrid->send($mail);
                            break;
                        case "SMS":
                            foreach($targets as $t) {
                                if($t === reset($targets)){
                                    $n->sent  = true;
                                    $n->save();
                                }
                                $balance   = User::find($t->id)->balance();
                                $smsString = $n->body;
                                $smsString = str_replace("{title}", $t->title, $smsString);
                                $smsString = str_replace("{first_name}", $t->first_name, $smsString);
                                $smsString = str_replace("{last_name}", $t->last_name, $smsString);
                                $smsString = str_replace("{pin_code}", $t->passcode, $smsString);
                                $smsString = str_replace("{balance}", $balance, $smsString);
                                $output = SMSHelper::send(htmlentities($smsString), $t, $partner);
                            }
                            break;
                    }

                }

                $n->count = count($targets);
                $n->sent  = true;
                $n->save();
            }

        });
    }
}
