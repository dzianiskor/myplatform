<?php
namespace App;
use App\BluCollection;
/**
 * Supplier Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Ralph Nader <ralph@bluloyalty.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class Tier extends BaseModel 
{
    public $table      = 'tier';
    protected $guarded = array();

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------

    public function criteria()
    { 
        return $this->hasMany('App\Tiercriteria')->where('draft', false);             
    }

    public function partner()
    {
        return $this->belongsTo('App\Partner');
    }

} // EOC