<?php
namespace App;
use App\BluCollection;
use Eloquent;
/**
 * Partner Channel Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class PartnerChannel extends Eloquent 
{
    public $table = 'channel_partner';

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------
    
    public function partner() 
    { 
    	return $this->belongsTo('App\Partner');  
   	}
   	
    public function channel() 
    { 
    	return $this->belongsTo('App\Partner', 'channel_id'); 
   	}    

} // EOC