<?php
namespace App;
use App\BluCollection;
use Eloquent;
/**
 * Reference Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class Reference extends BaseModel
{
    public $table = 'reference';

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------
    
    public function batch() 
    {
        return $this->belongsTo('App\Batch');
    }    

    public function user() 
    {
        return $this->belongsTo('App\User');
    }   
         
} // EOC