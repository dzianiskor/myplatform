<?php
namespace App;

/**
 * SegmentUser Model
 *
 */

class SegmentUser extends BaseModel
{
    public $table = 'segment_user';
    public $timestamps = false;
    public $sequence = 'seq_segment_user';

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------
    
    public function user() 
    { 
    	return $this->belongsTo('App\User'); 
    }

    public function segment()
    {
        return $this->belongsTo('App\Segment');
    }

} // EOC