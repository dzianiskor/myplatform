<?php
namespace App;
use App\BluCollection;
/**
 * Prodmodel Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Ralph Nader <ralph@bluloyalty.com>
 * @copyright  2016 BLU
 * @link       http://blupoints.com
 */
class Prodmodel extends BaseModel 
{
    public $table      = 'prodmodel';
    protected $guarded = array();

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------
    
    public function prodmodeltranslations()
    {
        return $this->hasMany('App\Prodmodeltranslation')->where('draft', false);
    }
    


    public static function orderedList()
    {
        return Prodmodel::orderBy('name')->get();
    }    

} // EOC