<?php
namespace App;
/**
 * Country Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class LangKey extends BaseModel 
{
    public $table      = 'localization_lang_keys';
    
    
    public function lang() 
    { 
    	return $this->belongsTo('App\Language', 'lang_id');  
   	}
   	
    public function key() 
    { 
    	return $this->belongsTo('App\Localizationkey', 'key_id'); 
   	} 
} // EOC