<?php
namespace App;
use App\BluCollection;
use Eloquent;
/**
 * Product Channel Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class ProductRedemption extends Eloquent 
{
    public $table = 'product_redemption';

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------
   
    public function channels()
    {
        return $this->hasMany('App\product_redemption_display_channel', 'product_redemption_id');
    }
    
    public function segments()
    {
        return $this->belongsToMany('App\Segment');
    }

    public function countries()
    {
        return $this->belongsToMany('App\Country');
    }
    
} // EOC