<?php
namespace App;
use App\BluCollection;
use Eloquent;
/**
 * Address Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class Address extends Eloquent 
{
	public $table = 'address';
	
} // EOC