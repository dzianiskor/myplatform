<?php
namespace App;
use Eloquent;
use App\BluCollection;
/**
 * Base Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class BaseModel extends Eloquent 
{
    /**
     * FieldEqualsValue Scope Query
     * 
     * @param Object $query
     * @param String $field
     * @param String $value
     * @return void
     */
    public function scopeFieldEqualsValue($query, $field, $value)
    {
        return $query->where($field, '=', $value);
    }

    /**
     * FieldLikeValue Scope Query
     * 
     * @param Object $query
     * @param string $field
     * @param string $value
     * @return Object
     */
    public function scopeFieldLikeValue($query, $field, $value)
    {
        return $query->where($field, 'LIKE', $value);
    }

    /**
     * Override the default newCollection() method
     * 
     * @param array $models
     * @return BluCollection
     */
    public function newCollection(array $models = array())
    {
        return new BluCollection($models);
    }

} // EOC