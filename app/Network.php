<?php
namespace App;
/**
 * Network Model
 *
 * Private Networks always take priority over BLU Network.
 * Hence, if a Private Network has enabled the conversion of points from its
 * Private Network points to BLU Points, their points currency will
 * ALWAYS take precedence when redeeming within the Private Network’s shops.
 * In addition, if a Partner is part of a Private Network but the conversion
 * of points has not been enabled by the latter, then the Partner can only
 * issue and redeem the Private Network’s currency.
 * This applies even if BLU is always the Parent Network.
 *
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class Network extends BaseModel
{
    public $table = 'network';
    protected $guarded = array();

    # --------------------------------------------------------------------------
    #
    # Relationships
    #
    # --------------------------------------------------------------------------

    public function parentNetwork()
    {
        return $this->belongsTo('App\Network');
    }

    public function partners()
    {
        return $this->belongsToMany('App\Partner');
    }

    public function networkCurrency()
    {
        return $this->belongsTo(Currency::class, 'currency_id', 'id');
    }

    public function members()
    {
        return $this->belongsToMany('App\User');
    }

    public function products()
    {
        return $this->belongsToMany('App\Product');
    }

    public function country()
    {
        return $this->belongsTo('App\Country', 'affiliate_country');
    }

    # --------------------------------------------------------------------------
    #
    # Attributes
    #
    # --------------------------------------------------------------------------

    public function childHierarchy()
    {
        $fifo  = array($this->id);
        $nodes = array();

        do {
            $results = self::select('id', 'parent_network_id')->where('parent_network_id', array_shift($fifo))->where('draft', 0)->get();

            foreach($results as $p) {

                if(!in_array($p->id, $nodes)) {
                    $nodes[] = $p->id;
                    $fifo[]  = $p->id;
                }
            }

        } while(!empty($fifo));

        return $nodes;
    }

} // EOC