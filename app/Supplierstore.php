<?php
namespace App;
use App\BluCollection;
use Eloquent;
/**
 * Store Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Ralph Nader <ralph@bluloyalty.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class Supplierstore extends BaseModel
{
    public $table = 'supplier_store';

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------
    
    public function supplier()
    {
        return $this->belongsTo('App\Supplier');
    }

    public function address()
    {
        return $this->hasOne('App\Address', 'mapping_key');
    }


    # --------------------------------------------------------------------------
    # 
    # Helpers
    # 
    # --------------------------------------------------------------------------
    
    /**
     * Return a list of supplierstores for the specified partner
     * 
     * @param int $supplier_id
     * @return array
     */
    public static function supplierStores($supplier_id)
    { 
        return Supplierstore::where('supplier_id', $supplier_id)->orderBy('name')->get();
    }

} // EOC