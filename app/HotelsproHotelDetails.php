<?php
namespace App;
use App\BaseModel;

/**
 * Supplier Model
 *
 * @category   Models
 * @package    BLU
 * @author     Ralph Nader <ralph@bluloyalty.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class HotelsproHotelDetails extends BaseModel
{
    public $table      = 'hotelspro_hotel_details';
    protected $guarded = array();

} // EOC