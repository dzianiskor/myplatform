<?php
namespace App;
use App\BluCollection;
use Eloquent;
/**
 * Store Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Magid Mroueh <magid.mroueh@bluloyalty.com>
 * @copyright  2016 BLU
 * @link       http://blupoints.com
 */
class Offertranslation extends Eloquent 
{
    public $table = 'offer_translation';

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------
    
    public function offer()
    {
        return $this->belongsTo('App\Offer');
    }

    

    # --------------------------------------------------------------------------
    # 
    # Helpers
    # 
    # --------------------------------------------------------------------------
    
    /**
     * Return a list of stores for the specified partner
     * 
     * @param int $offer_id
     * @return array
     */
    public static function offerTranslations($offer_id)     
    { 
        return Offertranslation::where('offer_id', $offer_id)->orderBy('name')->get();
    }

} // EOC