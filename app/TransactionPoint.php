<?php

namespace App;

class TransactionPoint extends BaseModel
{
    public $table = 'transaction_points';
    public $timestamps = false;
}
