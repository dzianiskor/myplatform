<?php
namespace App;
use App\BluCollection;
use Eloquent;
/**
 * Ticket Activity Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class TicketActivity extends Eloquent 
{
    public $table = 'ticket_activity';

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------
    
    public function user()      
    { 
    	return $this->hasOne('App\User', 'id'); 
    }

} // EOC