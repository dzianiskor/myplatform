<?php
namespace App;
use App\BluCollection;
/**
 * Area Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Magid Mroueh <magid.mroueh@wixelhq.com>
 * @copyright  2017 BLU
 * @link       http://blupoints.com
 */
class AffiliateProgramsMemberships extends BaseModel 
{
    public $table      = 'affiliate_programs_memberships';

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------
    
    public function Partners() 
    { 
    	return $this->belongsToMany('App\Partner');
    }


} // EOC