<?php

namespace App;
use Eloquent;
use Illuminate\Support\Facades\View; 
use SendGrid;
use Config;
use App\Http\Controllers\NotificationController as NotificationController;

/**
 * Product Channel Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class ProductPartnerRedemption extends Eloquent 
{
    public $table = 'product_partner_redemption';
    protected $guarded    = array();
    public $timestamps = false;

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------

    public function partner() 
    { 
        return $this->belongsTo('App\Partner', 'partner_id'); 
    }        

    public function channels()
    {
        return $this->hasMany('App\ProductRedemptionChannel', 'product_redemption_id');
    }
    
    public function segments()
    {
        return $this->belongsToMany('App\Segment', 'product_redemption_segments', 'product_redemption_id');
    }

    public function countries()
    {
        return $this->hasMany('App\ProductRedemptionCountries', 'product_redemption_id');
    }
    
    
    
    /**
     * Send a stock warning notification to BLU admins
     *
     * @return void
     */
    public function sendStockLevelWarning()
    {

        $email_template = View::make("emails.stock-warning", array(
            'product' => $this
        ))->render();

        NotificationController::sendEmailNotification($email_template, 'BLU Points', Config::get('blu.email_from'), array(null), array(Config::get('blu.admin_email')), "[BLU] Stock Level Warning");
    }
} // EOC