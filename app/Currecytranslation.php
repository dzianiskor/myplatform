<?php
namespace App;
use App\BluCollection;
use Eloquent;
/**
 * Store Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Magid Mroueh <magid.mroueh@bluloyalty.com>
 * @copyright  2016 BLU
 * @link       http://blupoints.com
 */
class CurrencyTranslation extends Eloquent 
{
    public $table = 'currency_translation';

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------
    
    public function currency()
    {
        return $this->belongsTo('App\Currency');
    }

    

    # --------------------------------------------------------------------------
    # 
    # Helpers
    # 
    # --------------------------------------------------------------------------
    
    /**
     * Return a list of translations of a specific currency
     * 
     * @param int $currency_id
     * @return array
     */
    public static function currencyTranslations($currency_id)     
    { 
        return CurrencyTranslation::where('currency_id', $currency_id)->orderBy('name')->get();
    }

} // EOC