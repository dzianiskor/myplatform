<?php
namespace App;
use User;
/**
 * Ticket Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class Ticket extends BaseModel 
{
    public $table      = 'ticket';
    protected $guarded = array();

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------
    
    public function type()
    {
        return $this->belongsTo('App\TicketType');
    }

    public function activity()
    {
        return $this->hasMany('App\TicketActivity', 'ticket_id'); 
    }

    public function admin()
    {
        return $this->belongsTo('App\Admin', 'user_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'source_id');
    }

    public function partner()
    {
        return $this->belongsTo('App\Partner', 'source_id');
    }

    # --------------------------------------------------------------------------
    # 
    # Attributes
    # 
    # --------------------------------------------------------------------------
    
    public function sourceInfo() 
    {
        $toret = null;
        
        if (isset($this->querySource)) {
            $toret = $this->belongsTo($this->querySource, 'source_id');
        } else {
            $toret = $this->belongsTo('App\Member', 'source_id');
        }

        return $toret;
    }

} // EOC