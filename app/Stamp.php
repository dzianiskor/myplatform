<?php
namespace App;
use App\BluCollection;
/**
 * Loyalty Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class Stamp extends BaseModel 
{
    public $table = 'stamp';

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------
    
    public function partner() 
    {
        return $this->belongsTo('App\Partner');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    # --------------------------------------------------------------------------
    # 
    # Attributes
    # 
    # --------------------------------------------------------------------------    

	

    # --------------------------------------------------------------------------
    # 
    # Helpers
    # 
    # --------------------------------------------------------------------------

    
} // EOC