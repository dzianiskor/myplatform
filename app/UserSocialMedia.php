<?php
namespace App;
/**
 * Favorite Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class UserSocialMedia extends BaseModel {

    public $table = 'user_social_media';

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------

    public function User() 
    { 
    	return $this->belongsTo('User');
    }
}

// EOC