<?php
namespace App;
use App\BluCollection;
use Eloquent;
/**
 * Mobile Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class MobileInstallations extends Eloquent
{
    public $table = 'mobile_installations';
} // EOC