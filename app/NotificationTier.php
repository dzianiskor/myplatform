<?php
namespace App;
use App\BluCollection;
use Eloquent;
/**
 * Notification Tier Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Magid Mroueh <magid.mroueh@bluloyalty.com>
 * @copyright  2017 BLU
 * @link       http://blupoints.com
 */
class NotificationTier extends Eloquent 
{
    public $table = 'notification_tier';
 
} // EOC