<?php
namespace App;
use App\BluCollection;
use Eloquent;
/**
 * Favorite Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class Favorite extends Eloquent 
{
    public $table = 'favorite';

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------
    
    public function user() 
    { 
    	return $this->belongsTo('App\User'); 
    }

    public function product() 
    { 
    	return $this->belongsTo('App\Product'); 
    }

} // EOC