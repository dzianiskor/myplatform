<?php
/**
 * EDS SMS gateway integration
 *
 * Usage Example:
 * 
 * $sms = new EDS('username', 'pin', 'sender ID');
 * $sms->send('number', 'message');
 * 
 * @category   Helper Classes
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class EDS
{
	private $endpoint = 'http://api.edsfze.com/http/sendsms.aspx?';
        private $apikey = '5498bc02-8ee3-4727-a4c2-2ba0d34d47c9200913';
	private $params = array();

	/**
	 * Create the new instance
	 * 
	 * @param
	 * @return void
	 */
	public function __construct($senderid, $msgtype)
	{
            $senderid= rawurlencode($senderid);
            $msgtype= rawurlencode($msgtype);
            // Msgtype = 0 for English = 8 for Arabic , DIr = 0 disabled or 1 enabled Request for delivery report for the respective message.
            $this->endpoint = $this->endpoint . "apikey=" . $this->apikey ."&Sid=" .$senderid ."&Msgtype=". $msgtype."&Dlr=1";
	}

	/**
	 * Perform the send request 
	 *
	 * @param int $number
	 * @param string $message
	 * @return string
	 */
	public function send($number, $message)
	{
		$this->params['message'] = urlencode($message) ;
		$this->params['mnumber'] = $number;

                $ch = curl_init();
                // URL encode message body
                $messageBody = urlencode($message);
                $URI = $this->endpoint;
                $URI .= "&Mobiles=" . $number;
                $URI .= "&msg=" . $messageBody;
                // Set URL to connect to
                curl_setopt($ch, CURLOPT_URL, $URI);
                // Set header supression
                curl_setopt($ch, CURLOPT_HEADER, 0);
                // Disable SSL peer verification
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                // Indicate that the message should be returned to a variable
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                // Make request
                $output = curl_exec($ch);
		curl_close($ch);	
//                $output = $URI;
		return $output; 		
	}	

} // EOC