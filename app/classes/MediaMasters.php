<?php

/**
 * MediaMasters SMS Helper
 *
 *	Usage Example:
 *
 *	$sms = new MediaMasters('username', 'password', 'sender ID');
 *	$sms->send('number', 'message');
 *	
 * @category   Helper Classes
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class MediaMasters
{
	private $endpoint = 'http://107.20.199.106/api/sendsms/plain';

	private $username;
	private $password;
	private $sender_id;

	/**
	 * Create the new instance
	 * 
	 * @param string $username
	 * @param string $password
	 * @param string $sender_id
	 * @return void
	 */
	public function __construct($username, $password, $sender_id)
	{
		$this->username  = $username;
		$this->password  = $password;
		$this->sender_id = $sender_id;
	}

	/**
	 * Determine what the SMS language is 
	 * 
	 * @param string $message
	 * @return int
	 */
	private function language($message)
	{
		if(Language::isArabic($message)) {
			return 8;
		} else {
			return 0;
		}
	}

	/**
	 * Send the text message
	 * 
	 * @param int $number
	 * @param string $message
	 * @return string
	 */
	public function send($number, $message)
	{
		if($this->language($message) == 8) {
			$message = substr($message, 0, 69);
		} else {
			$message = substr($message, 0, 159);
		}

		$message .= "OPTOUT 1130";

		$params = array(
			'user'       => $this->username,
			'password'   => $this->password,
			'sender'     => $this->sender_id,
			'SMSText'    => $message,
			'GSM'        => $number,
			'DataCoding' => $this->language($message)
		);

		$url = $this->endpoint."?".implode('&', $params);

		$ch = curl_init($url);
		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, 0);

		$data = curl_exec($ch);

		curl_close($ch);		

		return $data;
	}

} // EOC