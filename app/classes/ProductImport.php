<?php

/**
 * Product Import
 *
 * @category   Helper Classes
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class ProductImport extends ExcelImport
{
    /**
     * Perform the transaction import action
     *
     * @param object $transactionSheet
     * @return void
     */
    public static function import($productSheet)
    {

        if(!$productSheet) {
            return;
        }

        $numRows = $productSheet->getHighestRow();
        $cells   = $productSheet->rangeToArray('A1:H'.$numRows);

        // Skip the headers
        for ($i = 1; $i < $numRows; $i++) {

            $row = null;

            if(isset($cells[$i])) {
                $row = $cells[$i];
            }

            if(!$row) {
                continue;
            }

//            $product					= array();
//            $product['name']			= $row[0];
//            $product['brand_id']		= $row[1];
//            $product['model']			= $row[2];
//            $product['sub_model']		= $row[3];
//            $product['description']		= $row[4];
//            $product['category_id']		= $row[5];
//            $product['price_in_usd']	= $row[6];
//            $product['reference']		= $row[7];
//            $product['draft']			= 0;
//            $product['hot_deal']		= 0;
//            $product['display_online']	= 0;
//            $product['ucid']			= UCID::newProductID();
//			echo '<pre>';
//			print_r($row[0]);
//			exit;
				$product					= new Product();
				$product->ucid				= UCID::newProductID();
				$product->start_date		= null;
				$product->end_date			= null;
				$product->status			= 'active';
				$product->draft				= false;
				$product->name				= "$row[0]";
				$product->brand_id			= $row[1];
				$product->model				= $row[2];
				$product->sub_model			= $row[3];
				$product->description		= "$row[4]";
				$product->category_id		= $row[5];
				$product->price_in_usd		= $row[6];
				$product->reference			= "$row[7]";
				$product->hot_deal			= 0;
				$product->display_online	= 0;


				$product->save();

			// Link to top level partner automtically
				$partner = Auth::User()->getTopLevelPartner();
				$product->partners()->attach($partner->id);
//
//            $validator = Validator::make($product, array(
//                'name'			=> 'required',
//                'brand_id'		=> 'required|numeric',
//                'model'			=> 'required',
//                'sub_model'		=> 'required',
//                'description'   => 'required',
//                'category_id'   => 'required|numeric',
//                'price_in_usd'  => 'required',
//                'reference'		=> 'required',
//            ));

            //exit();
//            if(!$validator->fails()) {
//               $res = DB::table('product')->insert($product);
//            }

        }
    }

} // EOC