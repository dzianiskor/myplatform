<?php

use App\Currency;
use App\CurrencyPricing;

/**
 * Currency Helper
 * 
 * @category   Helper Classes
 * @package    BLU
 * @author     Ralph Nader <ralph@bluloyalty.com>
 * @copyright  2016 BLU
 * @link       http://blupoints.com
 */
class CurrencyHelper 
{
    /**
     * Return the converted amount to USD
     *
     * @return array
     */
    public static function convertToUSD($amount, $currency_shortcode = 'USD'){
        $currency = Currency::where('short_code', $currency_shortcode)->first();
        $currency_id = 6;
        if($currency) {
            $currency_id = $currency->id;
        }

        $currencyRate = CurrencyPricing::where('currency_id',$currency_id)->orderBy('created_at','DESC')->first();
        $exchange_rate = 1;
        if($currencyRate){
            $exchange_rate = floatval($currencyRate->rate);
        }
        
        $converted_amount = $amount / $exchange_rate;
        $arr_results['amount'] = $converted_amount;
        $arr_results['exchange_rate'] = $exchange_rate;
	return $arr_results;
    }
    
    /**
     * Return the converted amount From USD
     *
     * @return array
     */
    public static function convertFromUSD($amount, $currency_shortcode = 'USD'){
        $currency = Currency::where('short_code', $currency_shortcode)->first();
        $currency_id = 6;
        if($currency) {
            $currency_id = $currency->id;
        }

        $currencyRate = CurrencyPricing::where('currency_id',$currency_id)->orderBy('created_at','DESC')->first();
        $exchange_rate = 1;
        if($currencyRate){
            $exchange_rate = floatval($currencyRate->rate);
        }
        
        $converted_amount = $amount * $exchange_rate;
	$arr_results['amount'] = $converted_amount;
        $arr_results['exchange_rate'] = $exchange_rate;
	return $arr_results;
    }

   

} // EOC