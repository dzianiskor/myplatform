<?php

use App\Area;
use App\City;
use App\Country as Country;
use App\BluCollection as BluCollection;
use App\TicketType as TicketType;
use Illuminate\Support\Facades\Auth;

/**
 * Meta Helper
 *
 * A Container class for all the meta data that's repeatable.
 *
 * @category   Helper Classes
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class Meta
{
    /**
     * Return a list of all supported countries
     *
     * @return array
     */
    public static function countries()
    {
        return DB::table('country')->orderBy('name', 'asc')->get();
    }

    /**
     * Return a list of all supported cities
     *
     * @return array
     */
    public static function cities()
    {
        $result = $country = $area = $city = [];
        $countries = Country::select('id', 'name')->where('id', '>', 0)->get()->toArray();
        foreach ($countries as $item) {
            $country[$item['id']] = $item;
        }
        $areas = Area::select('id', 'country_id', 'name')->whereIn('country_id', $countries)->get()->toArray();
        foreach ($areas as $item) {
            $area[$item['id']] = $item;
        }

        $cities = City::select('id', 'area_id', 'name')->whereIn('area_id', $areas)->orderBy('name', 'ASC')->get()->toArray();
        foreach ($cities as $item) {
            $countryItem = $areaItem = [];
            $retItem = new \stdClass();
            $retItem->id = $item['id'];

            $areaItem = $area[$item['area_id']];
            if ($areaItem) {
                $countryItem = $country[$areaItem['country_id']];
            }

            if ($countryItem && $areaItem) {
                $retItem->name = $item['name'] . ', ' . $areaItem['name'] . ', ' . $countryItem['name'];
            } else if ($areaItem) {
                $retItem->name = $item['name'] . ', ' . $areaItem['name'];
            } else {
                $retItem->name = $item['name'];
            }

            $result[] = $retItem;
        }

        $result = new BluCollection($result);
        $result = $result->orderBy("name");
        return $result;
    }

    /**
     * Return a list of all supported areas
     *
     * @param boolean $hide_empty_values
     * @return array
     */
    public static function areas($hide_empty_values = false)
    {
        if($hide_empty_values) {
            return DB::table('area')->where('name','!=', 'Other')->select('id', 'name')->orderBy('name')->get();
        } else {
            return DB::table('area')->select('id', 'name')->orderBy('name')->get();
        }
    }

    /**
     * Fetch a role name using its ID only
     *
     * @param int $id
     * @return string
     */
    public static function role_name_from_id($id)
    {
        return DB::table('role')->where('id', $id)->pluck('name');
    }

    /**
     * Fetch a country name using its ID only
     *
     * @param int $id
     * @return string
     */
    public static function country_name_from_id($id)
    {
        return DB::table('country')->where('id', $id)->pluck('name');
    }

    /**
     * Fetch an area name using its ID only
     *
     * @param int $id
     * @return string
     */
    public static function area_name_from_id($id)
    {
        return DB::table('area')->where('id', $id)->pluck('name');
    }

    /**
     * Fetch a city name using its ID only
     *
     * @param int $id
     * @return string
     */
    public static function city_name_from_id($id)
    {
        return DB::table('city')->where('id', $id)->pluck('name');
    }

    /**
     * Fetch a partner name using its ID only
     *
     * @param int $id
     * @return string
     */
    public static function partner_name_from_id($id)
    {
        return DB::table('partner')->where('id', $id)->pluck('name');
    }

    /**
     * Fetch a brand name using its ID only
     *
     * @param int $id
     * @return string
     */
    public static function brand_name_from_id($id)
    {
        return DB::table('brand')->where('id', $id)->pluck('name');
    }

    /**
     * Fetch a segment name using its ID only
     *
     * @param int $id
     * @return string
     */
    public static function segment_name_from_id($id)
    {
        return DB::table('segment')->where('id', $id)->pluck('name');
    }

    /**
     * Fetch a store name using its ID only
     *
     * @param int $id
     * @return string
     */
    public static function store_name_from_id($id)
    {
        return DB::table('store')->where('id', $id)->pluck('name');
    }

    /**
     * Fetch a product/item name using its ID only
     *
     * @param int $id
     * @return string
     */
    public static function item_name_from_id($id)
    {
        return DB::table('product')->where('id', $id)->pluck('name');
    }

    /**
     * Fetch a category name using its ID only
     *
     * @param int $id
     * @return string
     */
    public static function category_name_from_id($id)
    {
        return DB::table('category')->where('id', $id)->pluck('name');
    }

    /**
     * Fetch an occupation name using its ID only
     *
     * @param int $id
     * @return string
     */
    public static function occupation_name_from_id($id)
    {
        return DB::table('occupation')->where('id', $id)->pluck('name');
    }

    /**
     * Fetch an Currency name using its ID only
     *
     * @param int $id
     * @return string
     */
    public static function currency_name_from_id($id)
    {
        return DB::table('currency')->where('id', $id)->pluck('name');
    }

    /**
     * Fetch an Merchant Category Code name using its ID only
     *
     * @param int $id
     * @return string
     */
    public static function mcc_name_from_id($id)
    {
        return DB::table('mcccodes')->where('id', $id)->pluck('edited_description');
    }

    /**
     * Fetch an Merchant Category Code name using its ID only
     *
     * @param int $id
     * @return string
     */
    public static function trxtype_name_from_id($id)
    {
        return DB::table('transaction_types')->where('id', $id)->pluck('name');
    }

    /**
     * Fetch an Merchant Category Code name using its ID only
     *
     * @param int $id
     * @return string
     */
    public static function mcc_code_from_id($id)
    {
        return DB::table('mcccodes')->where('id', $id)->pluck('mcc');
    }

    /**
     * Return an array of Months
     *
     * @return array
     */
    public static function monthArray()
    {
        return array(
            '1'  => 'January',
            '2'  => 'February',
            '3'  => 'March',
            '4'  => 'April',
            '5'  => 'May',
            '6'  => 'June',
            '7'  => 'July',
            '8'  => 'August',
            '9'  => 'September',
            '10' => 'October',
            '11' => 'November',
            '12' => 'December',
        );
    }

    /**
     * Return an array of days for a month
     *
     * @return array
     */
    public static function dayArray()
    {
        $buffer = array();

        for($i = 1; $i <= 31; $i++) {
            $buffer[$i] = $i;
        }

        return $buffer;
    }

    /**
     * Return an array of years from 1930 to current
     *
     * @return array
     */
    public static function yearArray()
    {
        $buffer = array();

        for($i = 1930; $i <= date('Y'); $i++) {
            $buffer[$i] = $i;
        }

        return $buffer;
    }

    /**
     * Return the value of a specified component of a date
     *
     * @param string $date
     * @param string $component
     * @return string
     */
    public static function dateComponent($date, $component = 'year')
    {
        if(empty($date)) {
            return null;
        }

        $ts = strtotime($date);

        switch($component) {
            case 'year':
                return date('Y', $ts);
                break;
            case 'month':
                return date('n', $ts);
                break;
            case 'day':
                return date('j', $ts);
                break;
            default:
                return null;
        }
    }

    /**
     * Return a list of all supported marital statuses
     *
     * @return array
     */
    public static function maritalStatuses()
    {
        return array(
            'single'   => 'Single',
            'married'  => 'Married',
            'widowed'  => 'Widowed',
            'divorced' => 'Divorced',
            'other'    =>"Other",
            'Not Selected' => "Not Selected"
        );
    }

    /**
     * Return a list of all supported genders
     *
     * @return array
     */
    public static function genderList()
    {
        return array(
            'm' => 'Male',
            'f' => 'Female',
            'not selected' => 'Not Selected'
        );
    }

    /**
     * Return a list of all salutation titles
     *
     * @return array
     */
    public static function titleList()
    {
        return array(
            'Mr.'   => 'Mr.',
            'Mrs.'  => 'Mrs.',
            'Ms.'   => 'Ms.',
            'Prof.' => 'Prof.',
            'Dr.'   => 'Dr.',
            'Gen.'  => 'Gen.',
            'Sen.'  => 'Sen.',
            'HRH.'  => 'HRH.'
        );
    }

    /**
     * Return a list of all income brackets
     *
     * @return array
     */
    public static function incomeList()
    {
        return array(
            '$0 - $500'         => '$0 - $500',
            '$500 - $1,000'     => '$500 - $1,000',
            '$1,000 - $2,000'   => '$1,000 - $2,000',
            '$2,000 - $3,000'   => '$2,000 - $3,000',
            '$3,000 - $4,000'   => '$3,000 - $4,000',
            '$4,000 - $5,000'   => '$4,000 - $5,000',
            '$5,000 - $8,000'   => '$5,000 - $8,000',
            '$8,000 - $12,000'  => '$8,000 - $12,000',
            '$12,000 - $15,000' => '$12,000 - $15,000',
            'More than $15,000' => 'More than $15,000',
        );
    }

    /**
     * Return a list of all occupations
     *
     * @return array
     */
    public static function occupationList()
    {
        return DB::table('occupation')->orderBy('name', 'asc')->pluck('name', 'id');
    }

    /**
      * Return network specific statuses
      *
      * @return array
      */
    public static function networkStatuses()
     {
         return array(
             'active'    => 'Active',
             'inactive'  => 'Inactive',
             'suspended' => 'Suspended'
         );
     }

    /**
      * Return member specific statuses
      *
      * @return array
      */
    public static function memberStatuses()
     {
         return array(
             'active'    => 'Active',
             'inactive'  => 'Inactive',
             'suspended' => 'Suspended',
             'closed' => 'Closed'
         );
     }

    /**
      * Return banner specific statuses
      *
      * @return array
      */
    public static function bannerStatuses()
     {
         return array(
             'online'    => 'Online',
             'offline'   => 'Offline'
         );
     }

     /**
      * Return Tier specific statuses
      *
      * @return array
      */
    public static function tierCriteriaOptions()
     {
         return array(
             'RewardedPoints'    => 'Rewarded Points',
             'NumTransactions'   => 'Number of Transactions',
             'NumVisits'         => 'Number of Visits',
             'AmtSpent'          => 'Amount Spent'
         );
     }

    /**
      * Return partner specific statuses
      *
      * @return array
      */
    public static function partnerStatuses()
     {
         return array(
             'enabled'   => 'Enabled',
             'disabled'  => 'Disabled',
             'suspended' => 'Suspended'
         );
     }

     /**
      * Return supplier specific statuses
      *
      * @return array
      */
    public static function supplierStatuses()
     {
         return array(
             'Enabled'   => 'Enabled',
             'Disabled'  => 'Disabled',
             'Suspended' => 'Suspended'
         );
     }

    /**
     * Return partner specific statuses
     *
     * @return array
     */
    public static function productStatuses()
    {
        return array(
            'active'   => 'active',
            'inactive' => 'Inactive'
        );
    }

    /**
     * Return offer specific statuses
     *
     * @return array
     */
    public static function offerStatuses()
    {
        return array(
            'Active'   => 'Active',
            'Inactive' => 'Inactive'
        );
    }

    /**
     * Return Item type
     *
     * @return array
     */
    public static function itemType()
    {
        return array(
            'product'   => 'Product',
            'offer' => 'Offer'
        );
    }

    /**
     * Return days of month
     *
     * @return array
     */
    public static function monthDays()
    {
        return array(
            '1'   => '1st day of the month',
            '2'   => '2nd day of the month',
            '3'   => '3rd day of the month',
            '4'   => '4th day of the month',
            '5'   => '5th day of the month',
            '6'   => '6th day of the month',
            '7'   => '7th day of the month',
            '8'   => '8th day of the month',
            '9'   => '9th day of the month',
            '10'   => '10th day of the month',
            '11'   => '11th day of the month',
            '12'   => '12th day of the month',
            '13'   => '13th day of the month',
            '14'   => '14th day of the month',
            '15'   => '15th day of the month',
            '16'   => '16th day of the month',
            '17'   => '17th day of the month',
            '18'   => '18th day of the month',
            '19'   => '19th day of the month',
            '20'   => '20th day of the month',
            '21'   => '21st day of the month',
            '22'   => '22nd day of the month',
            '23'   => '23rd day of the month',
            '24'   => '24th day of the month',
            '25'   => '25th day of the month',
            '26'   => '26th day of the month',
            '27'   => '27th day of the month',
            '28'   => '28th day of the month',
            '29'   => '29th day of the month',
            '30'   => '30th day of the month',
            '31'   => '31st day of the month',
        );
    }

    /**
      * Return product delivery options
      *
      * @return array
      */
    public static function deliveryOptions()
     {
         return array(
             'Delivery by BLU Only'            => 'Delivery by BLU Only',
             'Delivery by Merchant Only'       => 'Delivery by Merchant Only',
             'Pick-up Only'                    => 'Pick-up Only',
             'Pick-up Or Delivery by BLU'      => 'Pick-up Or Delivery by BLU',
             'Pick-up Or Delivery by merchant' => 'Pick-up Or Delivery by merchant'
         );
     }

    /**
     * Return a list of all allowed image file types for upload
     *
     * @return array
     */
     public static function allowedImageExts()
     {
        return array('png','jpg','jpeg','gif');
     }

    /**
     * Return a list of standard ticket types
     *
     * @return array
     */
     public static function ticketTypes()
     {
        return TicketType::orderBy('name')->pluck('name', 'id');
     }

    /**
     * Return a list of the standard ticket sources
     *
     * @return array
     */
     public static function ticketSources()
     {
        return array('Member', 'Partner');
     }

    /**
     * Return a list of loyalty statuses
     *
     * @return array
     */
     public static function loyaltyStatuses()
     {
        return array('Enabled', 'Disabled');
     }

    /**
     * Return loyalty program rule types
     *
     * @return array
     */
     public static function loyaltyRuleTypes()
     {
        return array('Points', 'Price');
     }

    /**
     * Return the types of exception loyalty rules
     *
     * @return array
     */
     public static function loyaltyExceptionRuleTypes()
     {
        return array('Segment',
            'Country',
            'Store',
            'Item',
            'Category',
            'Brand',
            'Currency',
            'Merchant Category Code',
            'Transaction Types',
            'Amount');
//        return array('Segment', 'Country', 'Store', 'Item', 'Category', 'Brand', 'Currency');
     }

    /**
     * Return a list of batch types
     *
     * @return array
     */
     public static function batchTypes()
     {
        return array('Coupons', 'Cards', 'References');
     }

    /**
     * List of all call center ticket statuses
     *
     * @return array
     */
     public static function ticketStatuses()
     {
        return array(
            'In Progress' => 'In Progress',
            'Closed'      => 'Closed',
            'Escalated'   => 'Escalated'
        );
     }

    /**
     * Return a list of active partner names and ID's
     *
     * @return array
     */
     public static function listPartners()
     {
        $partners = Auth::User()->managedPartners();
        //$partners = Partner::where('draft', false)->where('status', 'Enabled')->orderBy('name')->pluck('name', 'id');
        //$partners = array('0'=>"Please select a partner") + $partners;

        return $partners;
     }

    /**
     * Return the available notification types
     *
     * @return array
     */
     public static function notificationTypes()
     {
        return array('Email', 'SMS');
     }

    /**
     * Return the available notification types
     *
     * @return array
     */
	 public static function transactionClasses()
 	 {
		return array('Points', 'Amount', 'Items', 'Flight', 'Hotel', 'Car', 'Reversal', 'Reversal Item', 'Reversal Delivery', 'Coupon', 'Discount');
	 }

    /**
     * Return the available social media types
     *
     * @return array
     */
    public static function socialMediaTypes() {
        return array('Facebook', 'Twitter', 'Instagram', 'Snapchat', 'LinkedIn');
    }

	 /**
     * Return the diffrence days between two dates
     *
     * @return array
     */
	public static function days_diff($start, $end)
	{
	  $daylen = 60*60*24;

	   return (strtotime($start)-strtotime($end))/$daylen;
	}

    /**
    * Return a list of all availabe Merchant Apps
    *
    * @return array
    */
    public static function merchantApps()
    {
        return array(
            'default'   => 'default',
            '3-steps barcode'  => '3-Steps Barcode'
        );
    }

    /**
    * Return a list of all availabe Merchant Apps
    *
    * @return array
    */
    public static function orderStatuses()
    {
        return array(
            'Not Processed'   => 'Not Processed',
            'Ordered'  => 'Ordered',
            'Delivered'  => 'Delivered',
            'Cancelled'  => 'Cancelled'
        );
    }
} // EOC