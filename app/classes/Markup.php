<?php

/**
 * Markup Helper
 * 
 * @category   Helper Classes
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class Markup 
{
    /**
     * Return an array from a set of object results
     *
     * @param array $result
     * @return array
     */
    public static function arrayFromObjects($result, $extract)
    {
        $buffer = array();

        foreach($result as $i => $val) {
            foreach($extract as $k => $v) {
                if(isset($val->$k)) {
                    $buffer["{$val->$k}"] = $val->$v;
                }
            }
        }

        return $buffer;
    }

    /**
     * Generate a drop down element with defaults
     *
     * @param string $name
     * @param array $data
     * @param string $selected_value
     * @param array $extract
     * @param string $class
     * @return void
     */
    public static function select($name, $data, $selected_value = null, $attributes=array('class'=>"pure-input-1-3"), $empty_title = "None")
    {
        if (!isset($attributes['class'])) {
            $attributes['class'] = "pure-input-1-3";
        }

        echo '<select name="'.$name.'"';
        
        foreach ($attributes as $attribute => $value) {
            echo $attribute . '="'.$value.'"';
        }

        echo '>';
        echo '<option value="NULL">'.$empty_title.'</option>';

        foreach($data as $k => $v) {
            if($k == $selected_value) {
                echo '<option value="'.$k.'" selected>'.$v.'</option>';
            } else {
                echo '<option value="'.$k.'">'.$v.'</option>';
            }
        }

        echo '</select>';
    }

    /**
     * A simpler select element from data results
     *
     * Eg: Markup::simple_select('role', $roles, array('class' => 'pure-input-1'))
     * 
     * @param string $name
     * @param array $data
     * @param array $attrs
     * @param mixed $default_value
     * @param array $extract
     * @return void
     */ 
    public static function simple_select($name, $results, $attrs = array(), $default_value = null, $extract = array('id', 'name'))
    {
        echo '<select name="'.$name.'" id="'.$name.'" ';

        foreach($attrs as $k => $v) {
            echo $k.' = "'.$v.'" ';
        }

        echo '>';

        $key = $extract[0];
        $val = $extract[1];

        foreach($results as $r) {
            if(isset($r->$key)) {
                if($r->$val == $default_value) {
                    echo '<option value="'.$r->$key.'" selected>';
                } else {
                    echo '<option value="'.$r->$key.'">';    
                }
            } else {
                if($r->$val == $default_value) {
                    echo '<option value="'.$r->$val.'" selected>';
                } else {
                    echo '<option value="'.$r->$val.'">';    
                }
            }

            echo $r->$val."</option>";
        }

        echo '</select>';
    }

    /**
     * Generate the country select HTML element
     *
     * {{ Markup::country_select($name, $id, array('class' => 'pure-input-1-3')) }}
     *
     * @param string $name
     * @param int $selected_id
     * @param array $attrs
     * @return void
     */
    public static function country_select($name = 'country_id', $selected_id = null, $attrs = array())
    {
        echo '<select data-country="true" name="'.$name.'" id="'.$name.'" '.self::attributes_from_array($attrs).'>';

        $countries = DB::table('country')->orderBy('name', 'asc')->get();

        foreach($countries as $c) {
            if($c->id == $selected_id) {
                echo '<option value="'.$c->id.'" selected>'.$c->name.'</option>';
            } else {
                echo '<option value="'.$c->id.'">'.$c->name.'</option>';
            }
        }

        echo '</select>';
    }

    /**
     * Generate the area select HTML element
     *
     * {{ Markup::area_select($name, $country_id, $area_id, array('class' => 'pure-input-1-3')) }}
     *
     * @param string $name
     * @param int $country_id
     * @param int $area_id
     * @param array $attrs
     * @return void
     */
    public static function area_select($name = 'area_id', $country_id = null, $area_id = null, $attrs = array())
    {
        echo '<select data-area="true" name="'.$name.'" id="'.$name.'" '.self::attributes_from_array($attrs).'>';

        if(!empty($country_id)) {
            $areas = DB::table('area')->where('country_id', $country_id)->orderBy('name', 'asc')->get();

            foreach($areas as $a) {
                if($a->id == $area_id) {
                    echo '<option value="'.$a->id.'" selected>'.$a->name.'</option>';
                } else {
                    echo '<option value="'.$a->id.'">'.$a->name.'</option>';
                }
            }
        } else {
            echo "<option>Select country first</option>";    
        }

        echo '</select>';
    }  

    /**
     * Generate the city select HTML element
     *
     * {{ Markup::city_select($name, $area_id, $city_id, array('class' => 'form-1')) }}
     * 
     * @param string $name
     * @param int $area_id
     * @param int $city_id
     * @param array $attrs
     * @return void
     */
    public static function city_select($name = 'city_id', $area_id = null, $city_id = null, $attrs = array())
    {
        echo '<select data-city="true" name="'.$name.'" id="'.$name.'" '.self::attributes_from_array($attrs).'>';

        if(!empty($area_id)) {
            $cities = DB::table('city')->where('area_id', $area_id)->orderBy('name', 'asc')->get();

            foreach($cities as $c) {
                if($c->id == $city_id) {
                    echo '<option value="'.$c->id.'" selected>'.$c->name.'</option>';
                } else {
                    echo '<option value="'.$c->id.'">'.$c->name.'</option>';
                }
            }
        } else {
            echo "<option>Select area first</option>";    
        }

        echo '</select>';
    }      

    /**
     * Generate a string of attributes from an array
     * 
     * @param array $attrs
     * @return string
     */
    private static function attributes_from_array(array $attrs)
    {
        $s = '';

        foreach($attrs as $k => $v) {
            $s .= $k.' = "'.$v.'" ';
        }        

        return $s;        
    }  

    /**
     * Generate a checkbox wrapped inside a label for Pure.io Forms
     *
     * @param string $name
     * @param string $label
     * @param bool $is_checked
     * @param int $value
     * @return void
     */
    public static function checkbox($name, $label, $is_checked = false, $value = 1, $is_disabled = '')
    {
        $checked = "";
        $disabled = '';

        if($is_checked) {
            $checked = 'checked="checked"';
        }
        if($is_disabled) {
            $disabled = 'disabled="disabled"';
        }

        echo '<label for="'.$name.'" class="pure-checkbox checkbox-label">';
        echo '<input type="checkbox" name="'.$name.'" id="'.$name.'" value="'.$value.'" '.$checked.' '. $disabled .' /> '.$label;
        echo '</label>';
    }

} // EOC