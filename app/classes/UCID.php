<?php
use Illuminate\Support\Facades\DB;
use App\Partner as Partner;

/**
 * UCID Helper
 * 
 * @category   Helper Classes
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class UCID 
{
    /**
     * Create a new unique customer ID as per business requirement
     *
     * @return int
     */
    public static function newCustomerID()
    {
        $unique = false;

        do {
            $ucid = self::generateUCID();

            $users_count   = DB::table('users')->where('ucid', $ucid)->count();
            $partner_count = DB::table('partner')->where('ucid', $ucid)->count();
            $product_count = DB::table('product')->where('ucid', $ucid)->count();
            $max_supplier = DB::table('supplier')->where('ucid', $ucid)->count();
            $max_admin = DB::table('admins')->where('ucid', $ucid)->count();
            $max_affiliate = DB::table('affiliate')->where('ucid', $ucid)->count();
            $max_affiliate_program = DB::table('affiliate_program')->where('ucid', $ucid)->count();
            
            if($users_count == 0 && $partner_count == 0 && $product_count == 0 && $max_admin == 0 && $max_supplier == 0 && $max_affiliate == 0 && $max_affiliate_program == 0) {
                $unique = true;
            }
        } while ($unique == false);

        return $ucid;
    }

    /**
     * Create a new unique product ID as per business requirement
     *
     * @return int
     */
    public static function newProductID()
    {
        $unique = false;

        do {
            $ucid = self::generateUCID();

            $users_count   = DB::table('users')->where('ucid', $ucid)->count();
            $partner_count = DB::table('partner')->where('ucid', $ucid)->count();
            $product_count = DB::table('product')->where('ucid', $ucid)->count();
            $max_supplier = DB::table('supplier')->where('ucid', $ucid)->count();
            $max_admin = DB::table('admins')->where('ucid', $ucid)->count();
            $max_affiliate = DB::table('affiliate')->where('ucid', $ucid)->count();
            $max_affiliate_program = DB::table('affiliate_program')->where('ucid', $ucid)->count();
            
            if($users_count == 0 && $partner_count == 0 && $product_count == 0 && $max_admin == 0 && $max_supplier == 0 && $max_affiliate == 0 && $max_affiliate_program == 0) {
                $unique = true;
            }
        } while ($unique == false);

        return $ucid;
    }

    /**
     * Create a new unique partner ID as per business requirement
     *
     * @return int
     */
    public static function newPartnerID()
    {
        $unique = false;

        do {
            $ucid = self::generateUCID();

            $users_count   = DB::table('users')->where('ucid', $ucid)->count();
            $partner_count = DB::table('partner')->where('ucid', $ucid)->count();
            $product_count = DB::table('product')->where('ucid', $ucid)->count();
            $max_supplier = DB::table('supplier')->where('ucid', $ucid)->count();
            $max_admin = DB::table('admins')->where('ucid', $ucid)->count();
            $max_affiliate = DB::table('affiliate')->where('ucid', $ucid)->count();
            $max_affiliate_program = DB::table('affiliate_program')->where('ucid', $ucid)->count();
            
            if($users_count == 0 && $partner_count == 0 && $product_count == 0 && $max_admin == 0 && $max_supplier == 0 && $max_affiliate == 0 && $max_affiliate_program == 0) {
                $unique = true;
            }
        } while ($unique == false);

        return $ucid;
    }

    /**
     * Create a new unique affiliate ID as per business requirement
     *
     * @return int
     */
    public static function newAffiliateID()
    {
        $unique = false;

        do {
            $ucid = self::generateUCID();

            $users_count   = DB::table('users')->where('ucid', $ucid)->count();
            $partner_count = DB::table('partner')->where('ucid', $ucid)->count();
            $product_count = DB::table('product')->where('ucid', $ucid)->count();
            $max_supplier = DB::table('supplier')->where('ucid', $ucid)->count();
            $max_admin = DB::table('admins')->where('ucid', $ucid)->count();
            $max_affiliate = DB::table('affiliate')->where('ucid', $ucid)->count();
            $max_affiliate_program = DB::table('affiliate_program')->where('ucid', $ucid)->count();

            if($users_count == 0 && $partner_count == 0 && $product_count == 0 && $max_admin == 0 && $max_supplier == 0 && $max_affiliate == 0 && $max_affiliate_program == 0) {
                $unique = true;
            }
        } while ($unique == false);

        return $ucid;
    }
    
    /**
     * Create a new unique affiliate Program ID as per business requirement
     *
     * @return int
     */
    public static function newAffiliateProgramID()
    {
        $unique = false;

        do {
            $ucid = self::generateUCID();

            $users_count   = DB::table('users')->where('ucid', $ucid)->count();
            $partner_count = DB::table('partner')->where('ucid', $ucid)->count();
            $product_count = DB::table('product')->where('ucid', $ucid)->count();
            $max_supplier = DB::table('supplier')->where('ucid', $ucid)->count();
            $max_admin = DB::table('admins')->where('ucid', $ucid)->count();
            $max_affiliate = DB::table('affiliate')->where('ucid', $ucid)->count();
            $max_affiliate_program = DB::table('affiliate_program')->where('ucid', $ucid)->count();

            if($users_count == 0 && $partner_count == 0 && $product_count == 0 && $max_admin == 0 && $max_supplier == 0 && $max_affiliate == 0 && $max_affiliate_program == 0) {
                $unique = true;
            }
        } while ($unique == false);

        return $ucid;
    }
    
    /**
     * Create a unique variable length digit
     *
     * @return int
     */
    private static function generateUCID()
    {
        $max_member  = DB::table('users')->max('ucid');
        $max_partner = DB::table('partner')->max('ucid');
        $max_product = DB::table('product')->max('ucid');
        $max_supplier = DB::table('supplier')->max('ucid');
        $max_admin = DB::table('admins')->max('ucid');
        $max_affiliate = DB::table('affiliate')->max('ucid');
        $max_affiliate_program = DB::table('affiliate_program')->max('ucid');

        if($max_member == 0 && $max_partner == 0 && $max_product == 0 && $max_supplier == 0 && $max_admin == 0 && $max_affiliate == 0 && $max_affiliate_program == 0) {
            return 100000000; // start with
        } else {
            return (max($max_partner, $max_member, $max_product, $max_admin, $max_supplier, $max_affiliate, $max_affiliate_program) + 1);
        }
    }
    
    /**
     * Create a unique variable length digit
     *
     * @return int
     */
    public static function generateUniqueEmail($reference, $partnerId = 1)
    {
        $uniqueEmail = '';
        if(!$reference){
            return '';
        }
        $partner = Partner::find($partnerId);
        $partnerName = $partner->name;

        $uniqueEmail = $reference . '@' . trim($partnerName) . '.com';
        
        return $uniqueEmail;
    }

} // EOC