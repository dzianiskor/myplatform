<?php
use App\Language as Language;
/**
 * Libancall SMS gateway integration
 *
 * Usage Example:
 * 
 * $sms = new LibanCall('login', 'password', 'sender ID');
 * $sms->send('number', 'message');
 * 
 * @category   Helper Classes
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class LibanCall 
{

	private $endpoint    = 'http://web.libancall.net:2010/BlueSolution.aspx';
	private $contentType = 'text/XML';

	public $login;
	public $pass;
	public $description;
	public $senderid;
	public $country;
	public $client_name;
	public $mapid;

	/**
	 * Create the new instance
	 * 
	 * @param
	 * @return void
	 */
	public function __construct($login, $password, $senderid, $name = "BLU", $description = "BLU", $country = "LBN")
	{
		$this->login       = $login;
		$this->pass        = $password;
		$this->senderid    = $senderid;
		$this->client_name = htmlentities($name);
		$this->description = htmlentities($description);
		$this->country     = $country;
	}

	/**
	 * Return a default date format for the gateway
	 * 
	 * @return string
	 */
	public static function dateAttr()
	{
		return date("j/m/Y G:i:s", time());
	}

	/**
	 * Determine the language of the message to send 
	 * 
	 * @return string
	 */
	public function language($message)
	{
		if(Language::isArabic($message)) {
			return 'ar';
		} else {
			return 'en';
		}
	}

	/**
	 * Generate the XML Request structure
	 *
	 * @param int $number
	 * @param string $message_text
	 * @return void
	 */
	public function generateXML($number, $message_text)
	{
		$xml = new DOMDocument( "1.0", "UTF-8");
                $xml->xmlStandalone = false;

		$session = $xml->createElement("SESSION");
		$session->setAttribute('login', $this->login);
		$session->setAttribute('pass',  $this->pass);

		$create = $xml->createElement('CREATE');

		$map_ID = rand(100, 1000000);

		$campaign = $xml->createElement('CAMPAIGN');
		$campaign->setAttribute('type', '3');
		$campaign->appendChild($xml->createElement('NAME', $this->client_name));
		$campaign->appendChild($xml->createElement('TPOA', $this->senderid));
		$campaign->appendChild($xml->createElement('MAPID', $map_ID));
		$campaign->appendChild($xml->createElement('DESCRIPTION', $this->description));
		$campaign->appendChild($xml->createElement('START', LibanCall::dateAttr()));
		$campaign->appendChild($xml->createElement('END', LibanCall::dateAttr()));
		$campaign->appendChild($xml->createElement('COUNTRY', $this->country));
		$campaign->appendChild($xml->createElement('CLIENT_NAME', $this->client_name));
		$campaign->appendChild($xml->createElement('LANG', $this->language($message_text)));

		$perso   = $xml->createElement('PERSO');
		$message = $xml->createElement('MESSAGE');
		$message->appendChild($xml->createElement('NUMBER', $number));

		/*if($this->language($message_text) == 'ar') {
			$message_text = substr($message_text, 0, 69);
		} else {
			$message_text = substr($message_text, 0, 159);
		}*/

		$message->appendChild($xml->createElement('WORDING', $message_text));

		$perso->appendChild($message);
		$campaign->appendChild($perso);
		$create->appendChild($campaign);
		$session->appendChild($create);
		$xml->appendChild($session);
                
		return $xml->saveXML();
	}

	/**
	 * Perform the send request 
	 *
	 * @param int $number
	 * @param string $message
	 * @return string
	 */
	public function send($number, $message)
	{
            $randmapID  = rand(100,1000000);
            
                $xmlRalph = $this->generateXML($number, $message);
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $this->endpoint);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
		curl_setopt($ch, CURLOPT_POSTFIELDS, str_replace("&amp;", "&",$xmlRalph));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		
		$output[0] = curl_exec($ch);
                $output[1] = $xmlRalph;
		curl_close($ch);	

		return $output;
	}

} // EOC