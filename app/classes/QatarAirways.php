<?php

/**
 * Qatar Airways file Generation
 *
 * Usage Example:
 * 
 * $qatarAir = new QatarAirways('filename', 'partner_header', 'points_value_of_mile', 'footer_identifier', 'currency_code');
 * $qatarAir->generateFile('partner_id');
 * 
 * @category   Helper Classes
 * @package    BLU
 * @author     Ralph Nader <ralph@bluloyalty.com>
 * @copyright  2016 BLU
 * @link       http://blupoints.com
 */
class QatarAirways 
{

	public $filename;
	public $partner_header;
	public $point_value_of_mile;
	public $footer_identifier;
	public $currency_code;


	/**
	 * Create the new instance
	 * 
	 * @param
	 * @return void
	 */
	public function __construct($filename, $partner_header, $point_value_of_mile, $footer_identifier, $currency_code)
	{
		$this->filename                 = $filename;
		$this->partner_header           = $partner_header;
		$this->point_value_of_mile      = $point_value_of_mile;
		$this->footer_identifier        = $footer_identifier;
		$this->currency_code            = $currency_code;
	}

	/**
	 * Perform the generateFile request 
	 *
	 * @param int $partner
	 * @param string $filename
	 * @return string
	 */
	public function generateFile($partner)
	{
            $arr_lines= array();
            $arr_lines[] = $this->partner_header;
            $transactions = Transaction::where('partner_id',$partner)->get();
            foreach($transactions as $trx){
                $decoded_json = json_decode($trx->notes);
                
                $privilege_num = $decoded_json->privileged_number;
                $unhandled_date = explode(" ", $trx->created_at);
                $trx_date = str_replace('-',"",$unhandled_date[0]);
                $trx_type = str_pad($decoded_json->trx_type,10," ");
                $unique_id = str_pad($trx->id,14," ",STR_PAD_LEFT);
                $trx_location = str_pad($decoded_json->trx_location,10," ",STR_PAD_LEFT);
                $trx_filler = str_pad("",16," ",STR_PAD_LEFT);
                $num_units = str_pad($decoded_json->units,2,"0",STR_PAD_LEFT);//as in the example always 0
                $milage_rate = str_pad($decoded_json->milage_rate,12,"0",STR_PAD_LEFT);//Do you want to include the milage rate
                $total_spend_transaction = str_pad($decoded_json->total_spend,12,"0",STR_PAD_LEFT);// Do you want to include the amount
                $currency_code = str_pad($decoded_json->currency_code,2," ",STR_PAD_LEFT);
                $member_name = str_pad($decoded_json->member_name,20," ",STR_PAD_RIGHT);
                $base_miles_issued = str_pad($decoded_json->base_miles_issued,9,"0",STR_PAD_LEFT);
                $bonus_miles_issued = str_pad($decoded_json->bonus_miles_issued,9,"0",STR_PAD_LEFT);
                
                $str_line = $privilege_num . $trx_date . $trx_type . $unique_id . $trx_location . $trx_filler . $num_units . $milage_rate . $total_spend_transaction . $currency_code . $member_name . $base_miles_issued . $bonus_miles_issued;
                $arr_lines[] = $str_line;
            }
            
            $arr_lines[] = $this->footer_identifier;
            $imploded_arr_lines = implode("\r\n", $arr_lines);
            $path = public_path('uploads');
            $file = '/' . $this->filename . "_" . date("Ymd") . '.txt';
            $filepath = $path . $file;
            $myfile = fopen($filepath, "w") or die("Unable to open file!");
            fwrite($myfile, $imploded_arr_lines);
            fclose($myfile);
            

		return $output;
	}

} // EOC