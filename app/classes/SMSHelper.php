<?php

use App\Language as Language;
use LibanCall;
use Kalaam;
use A2A;

/**
 * SMS Helper
 * 
 * @category   Helper Classes
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
require(app_path() . '/classes/Unifonic/Autoload.php');

use \Unifonic\API\Client;

class SMSHelper {

    /**
     * Send out SMS's using available services per partner
     * 
     * @param string $template
     * @param mixed $user_or_string
     * @param Object $partner
     * @return void
     */
    public static function send($template, $user_or_string, $partner = null) {
        // @TODO Remove from live system

        /*
          $url = 'http://107.170.71.154/service.php';

          $fields = array();

          if(is_string($user_or_string)) {
          $fields = array(
          'number'  => urlencode($user_or_string),
          'message' => urlencode($template),
          );
          } else {
          $fields = array(
          'number'  => urlencode($user_or_string->normalizedContactNumber()),
          'message' => urlencode($template),
          );
          }

          $fields_string = '';

          foreach($fields as $key=>$value) {
          $fields_string .= $key.'='.$value.'&';
          }

          rtrim($fields_string, '&');

          $ch = curl_init();

          curl_setopt($ch,CURLOPT_URL, $url);
          curl_setopt($ch,CURLOPT_POST, count($fields));
          curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

          $result = curl_exec($ch);

          curl_close($ch);
         */

        $message = null;
        $number = null;

        if (is_string($user_or_string)) {
            $number = $user_or_string;
            $message = $template;
        } else {
            $number = $user_or_string->normalizedContactNumber();
            $message = $template;
        }
        /*
          $sms = new LibanCall(
          "Blue Solution", "Bl0ue", 525, "Test", "Test"
          );

          $resp = $sms->send($number, $message);
         */

        if ($partner->smsgate == '1') {
            // Msgtype = 0 for English = 8 for Arabic            
            if (Language::isArabic($message)) {
                $msgType = 8;
            } else {
                $msgType = 0;
            }
            $sms = new EDS(
                    $partner->smsgate_number, $msgType
            );
            // Comma delimited mobile numbers prefixed with country code 
            $number = str_replace('+', '', $number);
            $outputralph = $sms->send($number, $message);
            return $outputralph;
        } elseif ($partner->smsgate == '2') {
            $number = str_replace('+', '', $number);
            $sms = new LibanCall(
                    $partner->smsgate_username, $partner->smsgate_password, $partner->smsgate_number, $partner->name, $partner->description
            );

            $outputralph = $sms->send($number, $message);
            return $outputralph;
        } elseif ($partner->smsgate == '3') {
            // $msgType = N for English = OL for Arabic            
            if (Language::isArabic($message)) {
                $msgType = "OL";
            } else {
                $msgType = "N";
            }
            $sms = new Kalaam(
                    $partner->smsgate_username, $partner->smsgate_password, $partner->smsgate_number, $msgType
            );

            $number = str_replace('+', '', $number);
            $outputralph = $sms->send($number, $message);
            return $outputralph;
        } elseif ($partner->smsgate == '4') {
            $sms = new Client();
            $number = str_replace('+', '', $number);
            $outputralph = $sms->Messages->Send($number, $message, 'abdulwahed');
            return $outputralph;
        } elseif ($partner->smsgate == '5') {
            $sms = new A2A($partner->smsgate_username, $partner->smsgate_password);
            $number = str_replace('+', '', $number);
            $outputralph = $sms->send($number, $message);
            return $outputralph;
        } elseif ($partner->smsgate == '6') {
            $number = str_replace('+', '', $number);
            $sms = new ElBarid(
                    $partner->smsgate_username, $partner->smsgate_password, $partner->smsgate_number, $partner->name, $partner->description
            );

            $outputralph = $sms->send($number, $message);
            return $outputralph;
        }
    }

}

// EOC