<?php

/**
 * Security Helper
 * 
 * @category   Helper Classes
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class Security 
{
    /**
     * Take all permissions and roles a user has and store them in the session
     *
     * @return void
     */
    public static function configureRoles() 
    {
        $permissions = array();
        $roles       = array();
        $role_ids    = array();

        foreach(Auth::user()->roles as $role) {
            $roles[]    = $role->name;
            $role_ids[] = $role->id;

            foreach($role->permissions as $p) {
                $permissions[] = $p->slug;
            }
        }

        Session::put('user_permissions', $permissions);
        Session::put('user_roles', $roles);
        Session::put('user_role_ids', $role_ids);
    }

} // EOC