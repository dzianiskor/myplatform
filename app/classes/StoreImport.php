<?php

/**
 * Store Import Helper
 * 
 * @category   Helper Classes
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class StoreImport extends ExcelImport 
{
    /**
     * Perform the store import action
     * 
     * @param object $userSheet
     * @return void
     */
    public static function import($storeSheet) 
    {
        if (!$storeSheet) {
            return;
        }

        $numRows = $storeSheet->getHighestRow();
        $cells   = $storeSheet->rangeToArray('A1:G'.$numRows);

        // Skip the headers
        for ($i = 1; $i < $numRows; $i++) {
            
            $row = null;

            if(isset($cells[$i])) {
                $row = $cells[$i];
            }

            if(!$row) {
                continue;
            }

            $store               = array();
            $store['id']         = $row[0];
            $store['name']       = $row[1];
            $store['partner_id'] = $row[3];
            $store['lat']        = $row[5];
            $store['lng']        = $row[6];
            $store['created_at'] = date("Y-m-d H:i:s");
            $store['updated_at'] = date("Y-m-d H:i:s");

            if(!$store['lat']) {
                $store['lat'] = 25.0738579;
            }

            if(!$store['lng']) {
                $store['lng'] = 55.2298444;
            }            

            $validator = Validator::make($store, array(
                'id'         => 'required|numeric',
                'name'       => 'required',
                'partner_id' => 'required|numeric'
            ));

            if(!$validator->fails()) {
                DB::table('store')->insert($store);
            }
        }

    }

} // EOC