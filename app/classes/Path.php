<?php

namespace App\Classes;

use Illuminate\Support\Carbon;

class PathHelper
{
    public static function incoming($path)
    {
        return storage_path('app/daily-files/incoming/' . mb_strtolower(trim($path,'\\/')));
    }

    public static function outgoing($path)
    {
        return storage_path('app/daily-files/outgoing/' . mb_strtolower(trim($path,'\\/')));
    }

    public static function generateFileBaseName($fileName, $ext = null)
    {
        return mb_strtolower(basename($fileName, $ext ? '.' . $ext : $ext) . '_' . Carbon::now()->format('Ymd_H-i-s_u'));
    }

    public static function generateFileBaseNameFnb($fileName, $ext = null)
    {
        return basename($fileName, $ext ? '.' . $ext : $ext) . '' . Carbon::now()->format('ymd');
    }
}