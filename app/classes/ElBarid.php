<?php
/**
 * El Barid SMS gateway integration
 *
 * 
 * @category   Helper Classes
 * @package    Ralph Nader
 * @author     BLU Team <ralph@bluloyalty.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class ElBarid 
{

	private $endpoint    = 'http://198.101.210.203/elbarid.wsdl';
	private $contentType = 'text/XML';

	public $login;
	public $pass;
	public $description;
	public $senderid;
	public $country;
	public $client_name;
	public $mapid;

	/**
	 * Create the new instance
	 * 
	 * @param
	 * @return void
	 */
	public function __construct($login, $password, $senderid, $name = "BLU", $description = "BLU", $country = "LBN")
	{
		$this->login       = $login;
		$this->pass        = $password;
		$this->senderid    = $senderid;
		$this->client_name = $name;
		$this->description = $description;
		$this->country     = $country;
	}

	/**
	 * Return a default date format for the gateway
	 * 
	 * @return string
	 */
	public static function dateAttr()
	{
		return date("j/m/Y G:i:s", time());
	}

	/**
	 * Determine the language of the message to send 
	 * 
	 * @return string
	 */
	public function language($message)
	{
		if(Language::isArabic($message)) {
			return 'ar';
		} else {
			return 'en';
		}
	}

	/**
	 * Generate the XML Request structure
	 *
	 * @param int $number
	 * @param string $message_text
	 * @return void
	 */
	public function generateObject($number, $message_text)
	{
		$sms_object = new \stdClass();
                $sms_object->phoneNumber = $number;
                $sms_object->message = $message_text;
                $sms_object->unicodeMessage = '';
                $sms_object->sms_type_id = '1';
                $sms_object->notify ='0';
                $sms_object->senderId = $this->senderid;
                $sms_object->priority ='1';
                $sms_object->vbApp = 'SoapRequest';
                $sms_object->vbIdTime = time();
                
		return $sms_object;
	}

	/**
	 * Perform the send request 
	 *
	 * @param int $number
	 * @param string $message
	 * @return string
	 */
	public function send($number, $message)
	{
            
                $xmlRalph = $this->generateObject($number, $message);
                $loginAuth['username'] = $this->login;
                $loginAuth['password'] = $this->pass;
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $this->endpoint);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $loginAuth);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlRalph);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		
		$output[0] = curl_exec($ch);
                $output[1] = $xmlRalph;
		curl_close($ch);	

		return $output;
	}

} // EOC