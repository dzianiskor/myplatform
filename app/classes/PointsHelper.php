<?php

use App\Currency;
use App\LoyaltyExceptionrule;
use App\LoyaltyRule;
use App\Product;
use App\Segment;
use App\Store;
use App\Loyalty;
use App\Network;
use App\Partner;
/**
 * Poitns Helper
 *
 * @category   Helper Classes
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class PointsHelper
{
    public static function rewardPoints($amount, $user_id, $store_id, $items = array(), $currency_shortcode = 'USD', $couponPrice = 0, $couponCurrencyId = '6')
    {
        $amountEvent = $amount;
        $store		= Store::find($store_id);
        $program	= Loyalty::getDefaultProgram($store->partner_id);

        $currency	= Currency::where('short_code', $currency_shortcode)->first();
        if(!$currency){
            $currency = Currency::where('short_code','USD')->first();
        }
        $partner	= Partner::find($store->partner_id);
        $network	= $partner->firstNetwork();
        $networkId	= $network->id;
        $networkPointPrice	= $network->value_in_currency;
        $networkCurrencyId	= $network->currency_id;
        $networkRewardPts	= $network->reward_pts;

        if($networkCurrencyId == $currency->id){
                $amountInPoints	= $amount / $networkPointPrice;
                $amountInNetworkCurrency	= $amount;
                $currencyOfTheAmount            = $currency->id;

        }else{
                $networkCurrency		= Currency::where('id', $networkCurrencyId)->first();
                $amountInUsd			= $amount / $currency->latestRate();
                $amountInNetworkCurrency	= $amountInUsd * $networkCurrency->latestRate();
                $amountInPoints			= $amountInNetworkCurrency / $networkPointPrice;
                $amount				= $amountInNetworkCurrency;
                $currencyOfTheAmount            = $networkCurrency->id;
        }

//        $amount		= $amount / $currency->latestRate();

        if(!empty($couponCurrencyId)){
        $couponCurrencyDetails	= Currency::where('id', $couponCurrencyId)->first();
        $couponPrice_usd = $couponPrice / $couponCurrencyDetails->latestRate();

        $couponPoints_rewarded	= $couponPrice_usd;
        }
        else{
                $couponPoints_rewarded	= $couponPrice;
                $couponPrice_usd		= $couponPrice;
        }

        if(!$program) {
            return 0;
        } else {
            $rule_matched = false; // Have we matched an exception rule?

            $arr_matched_rules = [];

            if($program->rules()->count() > 0) {

                $reward_usd = 0;     // The reward USD amount
                $reward_pts = 0;     // The reward points amount
                $reward_tp  = null;  // The reward type

                foreach($program->rules as $exception_rule) {
                    if($rule_matched) {
                        break;
                    }
                    $exception_rule_obj = LoyaltyExceptionrule::where('loyalty_rule_id',$exception_rule->id)->where('deleted',0)->get();
//                    dd($exception_rule_obj);
                    $arr_except_rule = array();
                    $except_rule_checked = false;
                    $except_rule_matched = 0;
                    $ors_arr_match = array();
                    foreach($exception_rule_obj as $except_rule){
                        switch($except_rule->type) {
                            case "Segment":
                                if(Compare::is(Segment::hasUser($user_id, $except_rule->rule_value), 1, $except_rule->comparison_operator)) {
//                                    $transaction->segment_id = $except_rule->rule_value;
                                    $arr_except_rule['segment'][] = $except_rule->rule_value;
                                    $except_rule_checked = true;
                                    $except_rule_matched = $exception_rule->id;
                                    if($except_rule->operator == 'or'){
                                        $ors_arr_match[] = true;
                                    }
                                }
                                else{
                                    $except_rule_checked = false;
                                    if($except_rule->operator == 'and'){
                                        break 2;
                                    }else{
                                        $ors_arr_match[] = false;
                                    }
                                }
                                break;
                            case "Country":
                                if($store) {
                                    if(Compare::is($store->address->country_id, $exception_rule->rule_value, $except_rule->comparison_operator)) {
                                        $arr_except_rule['country'][] = $except_rule->rule_value;
                                        $except_rule_checked = true;
                                        $except_rule_matched = $exception_rule->id;
//                                        $transaction->country_id = $exception_rule->rule_value;
                                        if($except_rule->operator == 'or'){
                                            $ors_arr_match[] = true;
                                        }
                                    }
                                    else{
                                        $except_rule_checked = false;
                                        if($except_rule->operator == 'and'){
                                            break 2;
                                        }else{
                                            $ors_arr_match[] = false;
                                        }
                                    }
                                }
                                break;
                            case "Store":
//                                $exception_rule_objects = LoyaltyExceptionrule::where('loyalty_rule_id',$exception_rule->id)->get();
//                                dd($exception_rule_objects->toArray(), 1);
                                if($store) {
                                    $store = Store::where('id', $store->id)->first();
                                    if(Compare::is($store->id,$except_rule->rule_value, $except_rule->comparison_operator)) {
                                        $arr_except_rule['store'][] = $except_rule->rule_value;
                                        $except_rule_checked = true;
                                        $except_rule_matched = $exception_rule->id;
                                        if($except_rule->operator == 'or'){
                                            $ors_arr_match[] = true;
                                        }
                                    }
                                    else{
                                        $except_rule_checked = false;
                                        if($except_rule->operator == 'and'){
                                            break 2;
                                        }else{
                                            $ors_arr_match[] = false;
                                        }
                                    }
                                }
                                break;
                            case "Item":
                                if(count($items) > 0) {
                                    foreach($items as $i) {
                                        if(Compare::is($i->product_id, $except_rule->rule_value,$except_rule->comparison_operator)) {
                                            $arr_except_rule['item'][] = $except_rule->rule_value;
                                            $except_rule_checked = true;
                                            $except_rule_matched = $exception_rule->id;
                                            if($except_rule->operator == 'or'){
                                                $ors_arr_match[] = true;
                                            }
                                        }
                                        else{
                                            $except_rule_checked = false;
                                            if($except_rule->operator == 'and'){
                                                break 2;
                                            }else{
                                                $ors_arr_match[] = false;
                                            }
                                        }
                                    }
                                }

                            case "Category":
                                if(count($items) > 0) {
                                    foreach($items as $i) {
                                        $product = Product::find($i->product_id);
                                        if($product->category) {
                                            $category = $product->category;
                                            if(Compare::is($category->id, $exception_rule->rule_value, $except_rule->comparison_operator)) {
                                                $exception_id = $exception_rule->id;
                                            }
                                            if($category->parentCategory) {
                                                $parent = $category->parentCategory;
                                                if(Compare::is($parent->id, $exception_rule->rule_value, $except_rule->comparison_operator)) {
                                                    $exception_id = $exception_rule->id;

                                                }
                                            }
                                        }

                                        if($rule_matched) {
//                                            $transaction->category_id = $except_rule->rule_value;
                                            $arr_except_rule['category'][] = $except_rule->rule_value;
                                            $except_rule_checked = true;
                                            $except_rule_matched = $exception_rule->id;
                                            if($except_rule->operator == 'or'){
                                                $ors_arr_match[] = true;
                                            }
                                        }
                                        else{
                                            $except_rule_checked = false;
                                            if($except_rule->operator == 'and'){
                                                break 2;
                                            }else{
                                                $ors_arr_match[] = false;
                                            }
                                        }
                                    }
                                }
                                break;
                            case "Brand":
                                if(!empty($items)) {
                                    foreach($items as $i) {
                                        $product = Product::find($i);
                                        if($product){
                                            if(Compare::is($product->brand_id, $except_rule->rule_value, $except_rule->comparison_operator)) {
                                                $arr_except_rule['brand'][] = $except_rule->rule_value;
                                                $except_rule_checked = true;
                                                $except_rule_matched = $exception_rule->id;
                                                if($except_rule->operator == 'or'){
                                                    $ors_arr_match[] = true;
                                                }
                                            }
                                            else{
                                                $except_rule_checked = false;
                                                if($except_rule->operator == 'and'){
                                                    break 2;
                                                }else{
                                                    $ors_arr_match[] = false;
                                                }
                                            }
                                        }
                                    }
                                }

                                break;
                            case "Currency":
                                if($currency) {
                                    if(Compare::is($currency->id, $except_rule->rule_value, $except_rule->comparison_operator)) {
                                        $arr_except_rule['currency'][] = $except_rule->rule_value;
                                        $except_rule_checked = true;
                                        $except_rule_matched = $exception_rule->id;
                                        if($except_rule->operator == 'or'){
                                            $ors_arr_match[] = true;
                                        }
                                    }
                                    else{
                                        $except_rule_checked = false;
                                        if($except_rule->operator == 'and'){
                                            break 2;
                                        }else{
                                            $ors_arr_match[] = false;
                                        }
                                    }
                                }
                                break;
                        }
                    }
                    if(count($ors_arr_match) > 0){
                        if(in_array(true, $ors_arr_match)){
                            $arr_matched_rules[] = $except_rule_matched;
                        }
                    }else{
                        if($except_rule_checked == True ){
                            $arr_matched_rules[] = $except_rule_matched;
                        }
                    }
                }
            }
//            dd($arr_matched_rules);
            if(count($arr_matched_rules) >= 1){
                $ratio = 0;
                $points_to_reward = 0;
                foreach($arr_matched_rules as $except_rule_matched) {
                    //$exception_id = $except_rule_matched;
                    $exception_rule = LoyaltyRule::find($except_rule_matched);

                    switch($exception_rule->type) {
                        case "Points":
                            if($exception_rule->reward_pts / $exception_rule->reward_usd > $ratio){
                                $reward_pts = $exception_rule->reward_pts;
                                $exception_id = $except_rule_matched;
                                $reward_usd = $exception_rule->reward_usd;
                                $original_reward = $exception_rule->original_reward;
                                $exception_currency_id = $exception_rule->currency_id;
                                $reward_tp  = 'Points';
                                $ratio = $exception_rule->reward_pts / $exception_rule->reward_usd;
                            }
                            break;
                        case "Price":
                            if($exception_rule->reward_pts > $points_to_reward){
                                $reward_pts = $exception_rule->reward_pts;
                                $original_reward = $exception_rule->original_reward;
                                $points_to_reward = $reward_pts;
                                $exception_id = $except_rule_matched;
                                $reward_tp  = 'Price';
                            }
                            break;
                        case "Event":
                            $reward_pts = $exception_rule->reward_pts;
                            $reward_num_events = $exception_rule->original_reward;
                            $reward_tp  = 'Event';
                            $exception_id = $except_rule_matched;
                            break;
                    }

                    $rule_matched = true;
                }
            }


            if ($rule_matched) {
                if($reward_tp == 'Points') {
                    $amountInNetworkCurrency = (float) $amountEvent;
                    $points_rewarded	= round(($amountInNetworkCurrency / $reward_usd) *  $reward_pts);
                    $couponPoints_rewarded	= round(($couponPrice_usd / $reward_usd) *  $reward_pts);
                }
                elseif($reward_tp == 'Event'){
                    $points_rewarded = round(($amountEvent / $reward_num_events) *  $reward_pts);
                }
                else {
                    $points_rewarded = $reward_pts;
                }
            } else {
                if($currencyOfTheAmount != $program->currency_id){
                    //convert the amount to the rule currency
                    $amountCurrency   = Currency::where('id', $currencyOfTheAmount)->first();
                    $programCurrency   = Currency::where('id', $program->currency_id)->first();
                    $programCurrencyRate   = $programCurrency->latestRate();
                    $amountCurrencyRate   = $amountCurrency->latestRate();
                    $amountInNetworkCurrency = ($amountInNetworkCurrency / $amountCurrencyRate) * $programCurrencyRate;
                }
                $points_rewarded = round(($amountInNetworkCurrency / $program->original_reward) *  $program->reward_pts);
//                $points_rewarded = round($amountInPoints / $program->reward_pts);
                $couponPoints_rewarded = round(($couponPrice_usd / $program->reward_usd) *  $program->reward_pts);
            }

            return $points_rewarded - $couponPoints_rewarded;
        }
    }

    public static function redeemPoints($amount, $currency = 'USD', $store_id, $couponPrice = 0, $couponCurrencyId= '6')
    {
//		$partnerId	= Auth::User()->getTopLevelPartner()->id;
		$store		= Store::find($store_id);
		$partnerId	= $store->partner_id;
        $currencyDetails = Currency::where('short_code', $currency)->first();

		$partner	= Partner::find($partnerId);
        $network	= $partner->firstNetwork();
		$networkId	= $network->id;
		$networkPointPrice	= $network->value_in_currency;
		$networkCurrencyId	= $network->currency_id;

		if($networkCurrencyId == $currencyDetails->id){
			$amount						= $amount / $networkPointPrice;
		}else{
			$networkCurrency			= Currency::where('id', $networkCurrencyId)->first();
			$amountInUsd				= $amount / $currencyDetails->latestRate();
			$amountInNetworkCurrency	= $amountInUsd * $networkCurrency->latestRate();
			$amount						= $amountInNetworkCurrency / $networkPointPrice;
		}

        $redeem_points = $amount;
		$couponCurrencyDetails	= Currency::where('id', $couponCurrencyId)->first();

		if( $partnerId != 3){
			$couponPrice_usd = $couponPrice / $couponCurrencyDetails->latestRate();
			$couponPoints = $couponPrice_usd / Config::get('blu.points_rate');

			if( $redeem_points == 0 ){
				$redeem_points = abs($redeem_points - $couponPoints);
			}else{
				$redeem_points = $redeem_points - $couponPoints;
			}
		}

        return $redeem_points;
    }

    public static function programName($store_id)
        {
            $store = Store::find($store_id);
            $program = Loyalty::getDefaultProgram($store->partner_id);
            if ($program) {
                return $program->name;
            } else {
                return "N/A";
            }
        }

    /**
     * Convert points to a monetary amount
     *
     * @param int $points
     * @param string $currency
     * @return float
     */
    public static function pointsToAmount($points, $currency_shortcode = 'USD', $networkId = 1, $points_type = null,$partner_id=1)
    {
        $currency		= Currency::where('short_code', $currency_shortcode)->first();
        $network                = Network::find($networkId);
        $networkPointPrice	= $network->value_in_currency;
        $networkCurrencyId	= $network->currency_id;
        $networkCurrency	= Currency::where('id', $networkCurrencyId)->first();
        if($currency && $networkId == 1) {
            return (($points * Config::get('blu.points_rate')) * $currency->latestRate());
        }
        elseif ($currency && $networkId != 1) {
            $partner = Partner::find($partner_id);
            $cashback_value_in_currency = $partner->cashback_value_in_currency;
            $miles_value_in_currency = $partner->miles_value_in_currency;
            if($points_type == 'cashback' && !empty($cashback_value_in_currency) && $cashback_value_in_currency != 0.000){
                $networkPointPrice = $cashback_value_in_currency;
            }
            if($points_type == 'miles' && !empty($miles_value_in_currency) && $miles_value_in_currency != 0.000){
                $networkPointPrice = $miles_value_in_currency;
            }
            if($networkCurrencyId == $currency->id){
                return ($points * $networkPointPrice );
            }else{
                return (($points * $networkPointPrice ) / $networkCurrency->latestRate());
            }
        }else {
            return 0;
        }
    }

    /**
     * Convert a monetary amount into  a points amount
     *
     * @param float $amount
     * @param string $input_currency
     * @return int
     */
    public static function amountToPoints($amount, $input_currency = 'USD', $networkId = 1)
    {
		$currency = Currency::where('short_code', $input_currency)->first();
		if(empty($currency)){
			$currency = Currency::where('id', $input_currency)->first();
		}

		if($networkId == 1 || empty($networkId)){
                    
			return round(($amount / $currency->latestRate()) / Config::get('blu.points_rate'));
		}

		$network			= Network::find($networkId);
		$networkPointPrice	= $network->value_in_currency;
		$networkCurrencyId	= $network->currency_id;
                if(empty($networkCurrencyId)){
                    $networkCurrencyId = 6;
                }
                
                if(empty($networkPointPrice)){
                    $networkPointPrice = 0.1;
                }

		if($networkCurrencyId == $currency->id){
			$amountInPoints	= $amount / $networkPointPrice;
		}else{
			$networkCurrency			= Currency::where('id', $networkCurrencyId)->first();
			$amountInUsd				= $amount / $currency->latestRate();
			$amountInNetworkCurrency	= $amountInUsd * $networkCurrency->latestRate();
			$amountInPoints				= $amountInNetworkCurrency / $networkPointPrice;
		}

        return round($amountInPoints);

    }



    /**
     * Convert a delivery amount into  a points amount
     *
     * @param float $amount
     * @param string $input_currency
     * @return int
     */
    public static function productPriceInPoints($priceInPoints, $amount, $input_currency = 'USD', $networkId = 1, $network = null, $currency = null)
    {
		if($networkId == 1){
			return round($priceInPoints);
		}
        if (! $currency) {
            $currency = Currency::where('short_code', $input_currency)->first();
            if (empty($currency)) {
                $currency = Currency::where('id', $input_currency)->first();
            }
        }


        if (! $network) {
            $network = Network::find($networkId);
        }
		$networkPointPrice	= $network->value_in_currency;
		$networkCurrencyId	= $network->currency_id;

		if($networkCurrencyId == $currency->id){
			$amountInPoints	= $amount / $networkPointPrice;
		}else{
			$networkCurrency			= $network->networkCurrency;
			$amountInUsd				= $amount / $currency->latestRate();
			$amountInNetworkCurrency	= $amountInUsd * $networkCurrency->latestRate();
			$amountInPoints				= $amountInNetworkCurrency / $networkPointPrice;
		}
//        return ceil($amountInPoints);
        return round($amountInPoints);
    }

    /**
     * Convert a delivery amount into  a points amount
     *
     * @param float $amount
     * @param string $input_currency
     * @return int
     */
    public static function deliveryPriceInPoints($amount, $input_currency = 'USD', $networkId = 1, $network = null, $currency = null)
    {
        if (! $currency) {
            $currency = Currency::where('short_code', $input_currency)->first();
            if (empty($currency)) {
                $currency = Currency::where('id', $input_currency)->first();
            }
        }

        if (! $network) {
            $network = Network::find($networkId);
        }
        $networkPointPrice = $network->value_in_currency;
        $networkCurrencyId = $network->currency_id;

        if ($networkCurrencyId == $currency->id) {
            $amountInPoints = $amount / $networkPointPrice;
        } else {
            $networkCurrency = $network->networkCurrency;
            $amountInUsd = $amount / $currency->latestRate();
            $amountInNetworkCurrency = $amountInUsd * $networkCurrency->latestRate();
            $amountInPoints = $amountInNetworkCurrency / $networkPointPrice;
        }

        return ceil($amountInPoints);
    }

    /**
     * returns the value of points in network
	 * currency
     *
     * @param float $amount
     * @param string $input_currency
     * @return int
     */
    public static function partnerPointPrice($partnerId)
	{
		$partner					= Partner::find($partnerId);
		$network					= $partner->firstNetwork();
		$networkId					= $network->id;
		$networkPointPrice			= $network->value_in_currency;
		$networkCurrencyId			= $network->currency_id;

		$response['value']			= $networkPointPrice;
		$response['currency_id']	= $networkCurrencyId;


		return $response;
	}
    /**
     * Convert a monetary amount into  a points amount by specific date
     *
     * @param float $amount
     * @param string $input_currency
     * @param string $date
     * @return int
     */
    public static function amountToPointsByDate($amount, $input_currency = 'USD', $networkId = 1, $date)
    {
		$currency = Currency::where('short_code', $input_currency)->first();
		if(empty($currency)){
			$currency = Currency::where('id', $input_currency)->first();
		}

		if($networkId == 1 || empty($networkId)){
                    
			return round(($amount / $currency->rateByDate($date)) / Config::get('blu.points_rate'));
		}

		$network			= Network::find($networkId);
		$networkPointPrice	= $network->value_in_currency;
		$networkCurrencyId	= $network->currency_id;

		if($networkCurrencyId == $currency->id){
			$amountInPoints	= $amount / $networkPointPrice;
		}else{
			$networkCurrency			= Currency::where('id', $networkCurrencyId)->first();
			$amountInUsd				= $amount / $currency->rateByDate($date);
			$amountInNetworkCurrency	= $amountInUsd * $networkCurrency->rateByDate($date);
			$amountInPoints				= $amountInNetworkCurrency / $networkPointPrice;
		}

        return round($amountInPoints);

    }



    /**
     * Convert product price to points amount by specific date
     *
     * @param float $amount
     * @param string $input_currency
     * @param string $date
     * @return int
     */
    public static function productPriceInPointsByDate($priceInPoints, $amount, $input_currency = 'USD', $networkId = 1, $date, $network = null, $currency = null)
    {
		if($networkId == 1){
			return round($priceInPoints);
		}
        if (! $currency) {
            $currency = Currency::where('short_code', $input_currency)->first();
            if (empty($currency)) {
                $currency = Currency::where('id', $input_currency)->first();
            }
        }

        if (! $network) {
            $network = Network::find($networkId);
        }
		$networkPointPrice	= $network->value_in_currency;
		$networkCurrencyId	= $network->currency_id;

		if($networkCurrencyId == $currency->id){
			$amountInPoints	= $amount / $networkPointPrice;
		}else{
			$networkCurrency			= $network->networkCurrency;
			$amountInUsd				= $amount / $currency->rateByDate($date);
			$amountInNetworkCurrency	= $amountInUsd * $networkCurrency->rateByDate($date);
			$amountInPoints				= $amountInNetworkCurrency / $networkPointPrice;
		}
//        return ceil($amountInPoints);
        return round($amountInPoints);
    }
    
    public static function round_up( $value, $precision ) { 
        $pow = pow( 10, $precision ); 
        return ( ceil( $pow * $value ) + ceil( $pow * $value - ceil( $pow * $value ) ) ) / $pow; 
    }
} // EOC