<?php
use Illuminate\Support\Facades\Auth;
/**
 * Roles/Permissions Helper
 *
 *   Breakdown of default roles:
 *
 *   1 - BLU Admin
 *   2 - Partner Admin
 *   3 - Member
 *
 *   These ID's will always remain 1,2,3 depsite editing the roles otherwise
 * 
 * @category   Helper Classes
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class I 
{
    /**
     * Check if a user has access to the specified permission slug
     *
     * Eg: if (I::can('view_partners')) { ... }
     *
     * @param string $permission_slug
     * @return bool
     */
    public static function can($permission_slug)
    {
        $permissions = Session::get('user_permissions');

        if(!$permissions) {
            return false;
        }

        $permission_slug = strtolower(trim($permission_slug));

        if(in_array($permission_slug, $permissions)) {
            return true;
        }

        return false;
    }

    /**
     * Check if a user has access to the specified permission slug
     *
     * Eg: if (I::can('view_partners')) { ... }
     *
     * @param string $permission_slug
     * @return bool
     */
    public static function can_edit($permission_slug, $object)
    {
        $permissions = Session::get('user_permissions');

        if(!$permissions) {
            return false;
        }

        $edit_permission = 'edit_' . strtolower(trim($permission_slug));
        $create_permission = 'create_' . strtolower(trim($permission_slug));

        if(in_array($create_permission, $permissions)) {
            if (property_exists($object, 'draft')) {
                if ($object->draft == true) {
                    return true;
                }
            } else {
                return true;
            }
        }

        if(in_array($edit_permission, $permissions)) {
            return true;
        }

        return false;
    }

    /**
     * Check if a user has a specific role assigned {Not guaranteed}
     *
     * Eg: if (I::am('BLU Admin')) { ... }
     *
     * @param string $role_name
     * @return bool
     */
    public static function am($role_name)
    {
        $roles = Session::get('user_roles');

        if(!$roles) {
            return false;
        }

        if(in_array($role_name, $roles)) {
            return true;
        }

        return false;
    }

    /**
     * Granular way to determine role membership
     * 
     * @param int $id
     * @return boolean
     */
    public static function have_role_id($id)
    {
        $roles = Session::get('user_role_ids');

        if(!$roles) {
            return false;
        }

        if(in_array($id, $roles)) {
            return true;
        }

        return false;        
    }
    
    /**
     * Check if a user belongs to a specific partner
     *
     * Eg: if (I::am('BLU Admin')) { ... }
     *
     * @param string $partner_id
     * @return bool
     */
    public static function belong($partner_id)
    {
        $partner = App\Partner::find($partner_id);
        $belong = Auth::User()->partners->contains($partner);

        if(!$belong) {
            return false;
        }

        if($belong) {
            return true;
        }

        return false;
    }

} // EOC