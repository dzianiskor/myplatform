<?php
/**
 * Export Helper
 * 
 * @category   Helper Classes
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class ExportHelper 
{
    /**
     * Generate the download URL for exports
     *
     * @param string $type
     * @return string
     */
    public static function generateDownloadPath($type = 'xls') 
    {
        if(strpos($_SERVER['REQUEST_URI'], '?') === false) {
            return $_SERVER['REQUEST_URI']."?export={$type}";
        } else {
            return $_SERVER['REQUEST_URI']."&export={$type}";
        }   
    }

} // EOC