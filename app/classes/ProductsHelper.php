<?php

use App\Product;

/**
 * Poitns Helper
 *
 * @category   Helper Classes
 * @package    BLU
 * @author     Magid Mroueh <magid.mroueh@bluloyalty.com>
 * @copyright  2017 BLU
 * @link       http://blupoints.com
 */
class ProductsHelper {

    /**
     * Return a Product details
     *
     * @param int $productId, $countryId, $channelId
     * @return string
     */
    public static function productRedemptionDetails($productId, $countryId, $channelId) {
        $product = Product::select('product.id', 'product.name', 'product.brand_id', 'product.cover_image', 'product.is_voucher', 'product.model', 'product.sub_model', 'product.description', 'product.category_id', 'product_partner_redemption.id as product_redemption_id', 'product_partner_redemption.price_in_points', 'product_partner_redemption.price_in_usd', 'product_partner_redemption.retail_price_in_usd', 'product_partner_redemption.supplier_id', 'product_partner_redemption.currency_id', 'product_partner_redemption.original_price', 'product_partner_redemption.original_retail_price', 'product_partner_redemption.hot_deal', 'product_partner_redemption.qty', 'product_partner_redemption.display_online', 'product_partner_redemption.redemption_limit', 'product_redemption_countries.delivery_charges', 'product_redemption_countries.custom_tarrif_taxes as original_sales_tax', 'product_redemption_countries.currency_id as delivery_currency_id')
                ->join('product_partner_redemption', 'product_partner_redemption.product_id', '=', 'product.id')
                ->join('product_redemption_countries', function($join) {
                    $join->on('product_redemption_countries.product_id', '=', 'product.id');
                    $join->on('product_redemption_countries.product_redemption_id', '=', 'product_partner_redemption.id');
                    $join->on('product_redemption_countries.partner_id', '=', 'product_partner_redemption.partner_id');
                })
                ->Join('product_redemption_display_channel', function($join) {
                    $join->on('product_redemption_display_channel.product_id', '=', 'product.id');
                    $join->on('product_redemption_display_channel.product_redemption_id', '=', 'product_partner_redemption.id');
                })
                ->where('product_partner_redemption.product_id', $productId)
                ->where('product_redemption_countries.country_id', $countryId)
                ->where('product_redemption_display_channel.partner_id', $channelId)
                ->with('category')
                ->with('brand')
                ->first();

        return $product;
    }
    
    /**
     * Return a Product details
     *
     * @param int $productId, $countryId, $channelId
     * @return string
     */
    public static function productsRedemptionDetails($productIds, $countryId, $channelId) {
        $results = Product::select('product.id', 'product.name', 'product.cover_image', 'product.is_voucher', 'product.brand_id', 'product.model', 'product.sub_model', 'product.description', 'product.category_id', 'product_partner_redemption.price_in_points', 'product_partner_redemption.price_in_usd', 'product_partner_redemption.retail_price_in_usd', 'product_partner_redemption.currency_id', 'product_partner_redemption.original_price', 'product_partner_redemption.original_retail_price', 'product_partner_redemption.hot_deal', 'product_partner_redemption.qty', 'product_partner_redemption.display_online', 'product_redemption_countries.delivery_charges', 'product_redemption_countries.custom_tarrif_taxes as original_sales_tax', 'product_redemption_countries.currency_id as delivery_currency_id');
        $results->Join('product_partner_redemption', 'product_partner_redemption.product_id', '=','product.id');
        $results->Join('product_redemption_countries', function($join)
                {
                  $join->on('product_redemption_countries.product_id', '=', 'product.id');
                  $join->on('product_redemption_countries.product_redemption_id', '=', 'product_partner_redemption.id');
                  $join->on('product_redemption_countries.partner_id', '=', 'product_partner_redemption.partner_id');

                });
        $results->Join('product_redemption_display_channel', function($join)
                {
                  $join->on('product_redemption_display_channel.product_id', '=', 'product.id');
                  $join->on('product_redemption_display_channel.product_redemption_id', '=', 'product_partner_redemption.id');

                });
        $results->whereIn('product.id', $productIds);
        $results->where('product.draft', false);
        $results->where('product_partner_redemption.display_online', true);
        $results->where('product_redemption_countries.status', 'active');
        $results->where('product_partner_redemption.qty',">", 0);
        $results->where('product_redemption_display_channel.partner_id', $channelId);
        $results->where('product_redemption_countries.country_id', $countryId);
        $result = $results->get();

        return $result;
    }
    
    /**
     * Return a Product details
     *
     * @param int $productId, $countryId, $channelId
     * @return string
     */
    public static function searchProducts($search, $countryId, $channelId)
    {
        $results = Product::select('product.id', 'product.name', 'product.cover_image', 'product.brand_id', 'product.model', 'product.sub_model', 'product.description', 'product.category_id', 'product_partner_redemption.price_in_points', 'product_partner_redemption.price_in_usd', 'product_partner_redemption.retail_price_in_usd', 'product_partner_redemption.currency_id', 'product_partner_redemption.original_price', 'product_partner_redemption.original_retail_price', 'product_partner_redemption.hot_deal', 'product_partner_redemption.qty', 'product_partner_redemption.display_online', 'product_redemption_countries.delivery_charges', 'product_redemption_countries.custom_tarrif_taxes as original_sales_tax', 'product_redemption_countries.currency_id as delivery_currency_id');
        $results->Join('product_partner_redemption', 'product_partner_redemption.product_id', '=','product.id');
        $results->Join('product_redemption_countries', function($join) {
                  $join->on('product_redemption_countries.product_id', '=', 'product.id');
                  $join->on('product_redemption_countries.product_redemption_id', '=', 'product_partner_redemption.id');
                  $join->on('product_redemption_countries.partner_id', '=', 'product_partner_redemption.partner_id');
                });
        $results->Join('product_redemption_display_channel', function($join) {
                  $join->on('product_redemption_display_channel.product_id', '=', 'product.id');
                  $join->on('product_redemption_display_channel.product_redemption_id', '=', 'product_partner_redemption.id');
                });
        $results->where(function($query) use ($search) {
                                $query->orWhere('ucid', $search)
                                      ->orWhere('model', 'like', "%$search%")
                                      ->orWhere('name', 'like', "%$search%")
                                      ->orWhereHas('brand', function($query) use ($search) {
                                        $query->where('name', 'like',"%$search%");
                                    });

                             });
        $results->where('product.draft', false);
        $results->where('product_partner_redemption.display_online', true);
        $results->where('product_redemption_countries.status', 'active');
        $results->where('product_partner_redemption.qty',">", 0);
        $results->where('product_redemption_display_channel.partner_id', $channelId);
        $results->where('product_redemption_countries.country_id', $countryId);
        $result = $results->get();

        return $result;
    }

    
}

// EOC