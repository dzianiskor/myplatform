<?php

/**
 * User Import Helper
 * 
 * @category   Helper Classes
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class UserImport extends ExcelImport 
{
    /**
     * Perform the user import action
     * 
     * @param object $userSheet
     * @return void
     */
    public static function import($userSheet) 
    {
        if (!$userSheet) {
            return;
        }

        $numRows = $userSheet->getHighestRow();
        $cells   = $userSheet->rangeToArray('A1:W'.$numRows);

        // Skip headers
        for ($i = 2; $i <= $numRows; $i++) {

            //reset php script time limits
            set_time_limit(30);
            
            $row = null;

            if(isset($cells[$i])) {
                $row = $cells[$i];
            }

            if(!$row) {
                continue;
            }
            
            $user                     = array();
            $user['id']               = $row[0];
            $user['first_name']       = $row[3];
            $user['last_name']        = $row[4];
            $user['mobile']           = $row[11];
            $user['email']            = $row[21];
            $user['dob']              = date("Y-m-d H:i:s", strtotime($row[6]));
            $user['balance']          = $row[7];
            $user['marital_status']   = $row[10];
            $user['comfirm_passcode'] = true;
            $user['passcode']         = $row[12];
            $user['title']            = null;
            $user['draft']            = false;
            $user['verified']         = 1;
            $user['status']           = 'active';
            $user['created_at']       = date("Y-m-d H:i:s");
            $user['updated_at']       = date("Y-m-d H:i:s");
            $user['ucid']             = UCID::newCustomerID();
            //$user['password']       = Hash::make($row[22]);

            // Validate gender
            switch($row[5]) {
                case "Male":
                    $user['gender'] = 'm';
                    break;
                case "Female":
                    $user['gender'] = 'f';
                    break;
                default:
                    $user['gender'] = 'm';
            }

            // Validate balance
            if($user['balance'] == null) {
                $user['balance'] = 0;
            }

            // Validate dob
            if($user['dob'] == null) {
                $user['dob'] = date("Y-m-d H:i:s", strtotime("1 January 1985"));
            }

            $validator = Validator::make($user, array(
                'id'         => 'required|numeric',
                'first_name' => 'required',
                'last_name'  => 'required',
                'email'      => 'required|email|unique:users',
                'mobile'     => 'required|numeric',
                'dob'        => 'required',
                'balance'    => 'required|numeric',
            ));

            if(!$validator->fails()) {

                // Create the user 
                DB::table('users')->insert($user);

                // Assign the role
                DB::table('role_user')->insert(array(
                    'role_id' => 4,
                    'user_id' => $user['id']
                ));
            }
        }
    }

} // EOC