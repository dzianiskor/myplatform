<?php

/**
 * Transaction Import
 *
 * @category   Helper Classes
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class TransactionImport extends ExcelImport
{
    /**
     * Perform the transaction import action
     *
     * @param object $transactionSheet
     * @return void
     */
    public static function import($transactionSheet)
    {
        if(!$transactionSheet) {
            return;
        }

        $numRows = $transactionSheet->getHighestRow();
        $cells   = $transactionSheet->rangeToArray('A1:Q'.$numRows);

        // Skip the headers
        for ($i = 1; $i < $numRows; $i++) {

            $row = null;

            if(isset($cells[$i])) {
                $row = $cells[$i];
            }

            if(!$row) {
                continue;
            }

            $tx								= array();
            $tx['id']						= $row[0];
            $tx['ref_number']				= $row[1];
            $tx['original_total_amount']    = $row[3];
            $tx['total_amount']				= $row[3];
            $tx['partner_amount']    = $row[3];

            $tx['store_id']					= $row[4];
            $tx['user_id']					= $row[5];
            $tx['points_rewarded']			= $row[6];
            $tx['points_redeemed']			= $row[16];
            $tx['partner_id']				= $row[9];
            $tx['exchange_rate']			= $row[14];
            $tx['created_at']				= date('Y-m-d H:i:s',PHPExcel_Shared_Date::ExcelToPHP($row[2]));
            $tx['updated_at']				= date('Y-m-d H:i:s',PHPExcel_Shared_Date::ExcelToPHP($row[2]));

            // Map Currency
            if($row[13]) {
                $currency = Currency::where('short_code', $row[13])->first();

                if($currency) {
                    $tx['currency_id']	= $currency->id;
                    $tx['partner_currency_id']	= $currency->id;
                    $tx['total_amount']	= $row[3] / $currency->latestRate();
                }
            }

            $validator = Validator::make($tx, array(
                'id'         => 'required|numeric',
                'partner_id' => 'required|numeric',
                'store_id'   => 'required|numeric',
                'user_id'    => 'required|numeric'
            ));

            if(!$validator->fails()) {
                DB::table('transaction')->insert($tx);
            }
        }

    }

} // EOC