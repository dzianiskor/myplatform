<?php

use Illuminate\Support\Facades\DB;
use Input;
use Illuminate\Support\Facades\Log;

/**
 * RefID Helper
 *
 * @category   Helper Classes
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class RefID {

    /**
     * Generate a random seeded start for reference ID's
     *
     * @return int
     */
    public static function randomSeededStart() {
        return mt_rand(100000, 999999);
    }

    /**
     * Create a new unique reference number
     *
     * @return int
     */
    public static function newNumber() {
        $max_number = self::getMaxRefNumber();

        return ($max_number + 1);
    }

    /**
     * Determine what the last used number is in the reference table
     *
     * @return int
     */
    public static function getMaxRefNumber() {
        $constraint_min = 100000000;
        $constraint_max = 999999999;

        $numbers = array();
        $prefix = Input::get('prefix') . '%';
        $references = DB::table('reference')->select('number')->where('number', 'like', $prefix)->get();

        foreach ($references as $reference) {
            $numbers[] = preg_replace("/[^0-9,.]/", "", $reference->number);
        }
        if (!empty($numbers)) {
            $max_number = max($numbers);
        } else {
            $max_number = 0;
        }

        if (!$max_number) {
            $max_number = $constraint_min;
        }

        if ($max_number >= $constraint_max) {
            Log::warning('Max range of Ref number slots have been processed.');
        }

        return $max_number;
    }

}

// EOC