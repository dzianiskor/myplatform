<?php

use App\BluCollection;
use App\Report;
use App\User;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\View;

/**
 * Report Helper
 *
 * @category   Helper Classes
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class ReportHelper
{
    const PAGINATION_COUNT = 40;

    /**
     * Helper method used to sum up the totals of the specified field
     *
     * @param Collection $results
     * @param string $field
     * @return int
     */
    public static function sum($results, $field)
    {
        $total = 0;

        foreach($results as $t) {
            if($t->$field) {
                $total += $t->$field;
            }
        }

        return $total;
    }

    /**
     * Generate & return a default set of dates
     *
     * @return Array
     */
    private static function dateDefaults()
    {
        $defaults = array('params' => 0);

        if(Input::has('start_date')) {
            $defaults['start_date'] = date("Y-m-d H:i:s", strtotime(Input::get('start_date')));
            $defaults['params'] = 1;
        } else {
            $defaults['start_date'] = date("Y-m-d H:i:s", strtotime("-1 month", time()) );
        }

        if(Input::has('end_date')) {
            $defaults['end_date'] = date("Y-m-d", strtotime(Input::get('end_date')));
            $defaults['params'] = 1;
        } else {
            $defaults['end_date'] = date("Y-m-d");
        }
        $defaults['end_date'] = $defaults['end_date'] . " 23:59:59";
        return $defaults;
    }

    /**
     * Create basic set of defaults with optional params
     *
     * @return array
     */
    public static function normalizeDefaults()
    {
        $dates    = self::dateDefaults();

        $defaults = array('params' => $dates['params']);
        if(Input::has('network_ids')){
            $defaults['network_ids'] = explode(',', Input::get('network_ids'));
            $defaults['params'] = 1;
        }
        // Fix for when no entities are passed
        if(empty($defaults['network_ids'])) {
            $defaults['network_ids'] = array(-1);
        }

        if(Input::has('network_currency')){
            $defaults['network_currency'] = Input::get('network_currency');
            $defaults['params'] = 1;
        }
        // Fix for when no entities are passed
        if(empty($defaults['network_currency'])) {
            $defaults['network_currency'] = 1;
        }
        if(Input::has('bypartner')){
            $defaults['bypartner'] = 'bypartner';
            $defaults['params'] = 1;
        }
        if(empty($defaults['bypartner'])){
            $defaults['bypartner'] = 'not';
        }
        if(empty($defaults['currency'])){
            $defaults['currency'] = 'not';
        }
        if(Input::has('zerobalance')){
            $defaults['zerobalance'] = 'zerobalance';
            $defaults['params'] = 1;
        }
        if(empty($defaults['zerobalance'])){
            $defaults['zerobalance'] = 'not';
        }
        if(Input::has('parent_ids')){
            $defaults['parent_ids'] = explode(',', Input::get('parent_ids'));
            $defaults['params'] = 1;
        }
        // Fix for when no entities are passed
        if(empty($defaults['parent_ids'])) {
            $defaults['parent_ids'] = array(-1);
        }

        if(Input::has('entity')) {
            $defaults['entities'] = explode(',', Input::get('entity'));
            $defaults['params'] = 1;
        }

        // Fix for when no entities are passed
        if(empty($defaults['entities'])) {
            $defaults['entities'] = array(-1);
        }

        if(Input::has('member_id')) {
            $defaults['member_id'] = explode(',', Input::get('member_id'));
            $defaults['params'] = 1;
        }

        // Fix for when no member passed
        if(empty($defaults['member_id'])) {
            $defaults['member_id'] = 0;
        }

        if(Input::has('filter1')) {
            $defaults['filter1'] = explode(',', Input::get('filter1'));
            $defaults['params'] = 1;
        }

        if(empty($defaults['filter1'])) {
            $defaults['filter1'] = array(-1);
        }

        if(Input::has('range')){
            $defaults['daterange'] = Input::get('range');
            $defaults['params'] = 1;
        }
        if(empty($defaults['daterange'])) {
            $defaults['daterange'] = 'daily';
        }
        if(Input::has('monyeardaterange')){
            $defaults['monyeardaterange'] = Input::get('monyeardaterange');
            $defaults['params'] = 1;
        }
        if(empty($defaults['monyeardaterange'])) {
            $defaults['monyeardaterange'] = 'monthly';
        }

        if(Input::has('trxtype')){
            $defaults['trxtype'] = Input::get('trxtype');
            $defaults['params'] = 1;
        }
        if(empty($defaults['trxtype'])) {
            $defaults['trxtype'] = 'reward';
        }

        if(Input::has('supplier')) {
            $defaults['suppliers'] = explode(',', Input::get('supplier'));
            $defaults['params'] = 1;
        }

        if(Input::has('currencies')) {
            $defaults['currency'] = Input::get('currencies');
            $defaults['params'] = 1;
        }

        if(empty($defaults['suppliers'])) {
            $defaults['suppliers'] = array(-1);
        }

        if(Input::has('travels')) {
            $defaults['travels'] = explode(',', Input::get('travels') );
            $defaults['params'] = 1;
        }
        if(empty($defaults['travels'])) {
            $defaults['travels'] = array(-1);
        }

        if(Input::has('redemptioncategories')) {
            $defaults['redemptioncategories'] = explode(',', Input::get('redemptioncategories') );
            $defaults['params'] = 1;
        }
        if(empty($defaults['redemptioncategories'])) {
            $defaults['redemptioncategories'] = array(-1);
        }

        if(Input::has('reference_number')) {
            $defaults['reference_number'] = Input::get('reference_number');
        }

        if(empty($defaults['reference_number'])) {
            $defaults['reference_number'] = null;
        }

        return array_merge($dates, $defaults);
    }

    /**
     * Determine what kind of response to send and deliver it
     *
     * @param Report $report
     * @param array $defaults
     * @param BluCollection $results
     * @return string
     */
    public static function sendView($report, $defaults, $results, $totals = array(), $sensitive_info = array(), $customData = [])
    {
        $raw_results = $results;
        $paginated = $results;

        if(!empty($results)){
            $paginated   = $results->paginate(ReportHelper::PAGINATION_COUNT);
        }

        return View::make("report.ejs.report_{$report->id}", array(
            'defaults'    => $defaults,
            'report'      => $report,
            'results'     => $paginated,
            'raw_results' => $raw_results,
            'sensitive'   => $sensitive_info,
            'totals'      => $totals,
            'customData'  => $customData,
        ));
    }

    /**
     * Generate and deliver a download response
     *
     * @param Report $report
     * @param array $buffer
     * @return Response
     */
    public static function sendDownload($report, $buffer)
    {
        $file = storage_path()."/generator/{$report->name}_".time().".csv";

        CSVHelper::writeToFile($buffer, $file);

        return Response::download($file);
    }

    /**
     * Create a valid Excel date string
     *
     * @param string $date
     * @return date
     */
    public static function xlsDate($date)
    {
        return date('F j, Y, g:i a', strtotime($date));
    }

    /**
     * Return and handle empty attributes
     *
     * @param Object $input
     * @param string $attr
     * @param string $default
     * @return string
     */
    public static function defaultEmpty($input, $attr, $default = "N/A")
    {
        if(!$input) {
            return $default;
        } else {
            return $input->$attr;
        }
    }

    /**
     * Return a string representation of the specified period
     *
     * @param array $defaults
     * @return string
     */
    public static function periodString($defaults)
    {
        return "{$defaults['start_date']} - {$defaults['end_date']}";
    }

    /**
     * Generate a simple string date representation for charts
     *
     * @param array $defaults
     * @return string
     */
    public static function chartDateString($defaults)
    {
        $start = date('M j, Y', strtotime($defaults['start_date']));
        $end   = date('M j, Y', strtotime($defaults['end_date']));

        return "{$start} - {$end}";
    }

    /**
     * Helper to generate a simple list of series data
     *
     * @param array $results
     * @param string $attribute
     * @return string
     */
    public static function generateSeriesData($results, $attribute)
    {
        $buffer = array();

        foreach ($results as $r) {
            $buffer[] = $r->$attribute;
        }

        return implode(',', $buffer);
    }

    /**
     * Simple helper to assist when pre-selecting checkboxes based on input
     *
     * @param string $attr
     * @param string $needle
     * @param array $haystack
     * @return string
     */
    public static function isSelected($attr, $needle, array $haystack)
    {
        if(in_array($needle, $haystack)) {
            return "$attr=\"$attr\"";
        }
    }

    /**
     * function to get the sensitive info for the managed users
     * from the middle ware
     *
     * @return array
     */
    public static function getMiddlewareSensitiveInfo($userIds){
        //$response = array();
        $admin_partner = Auth::User()->getTopLevelPartner();
        $user_ids_for_middleware = $userIds;
        $all_middleware_users = array();
        $url = $admin_partner->link_middleware_api . '?method=sensitiveInfo&api_key='.$admin_partner->middleware_api_key;
        $json_ids = json_encode($user_ids_for_middleware);
        $post_fields = "user_ids=" . $json_ids ;
        $curl = curl_init();
        // Set some options - we are passing in a useragent too here
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url,
            CURLOPT_SSL_VERIFYPEER => False,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $post_fields,
            CURLOPT_USERAGENT => 'Testing Sensitive Info'
        ));
        // Send the request & save response to $resp
        $resp = curl_exec($curl);
        $response = json_decode($resp, TRUE);

        return $response;
    }

    /**
     * function to get the sensitive info for the search user
     * from the middle ware
     *
     * @return array
     */
    public static function getMiddlewareSearchSensitiveInfo($term)
    {
        $response = array();
        $user_ids_used = array();
        $users = Auth::User()->managedMembers(false);
        $temp_users = $users->get();
        foreach($temp_users as $temp_u){
            $user_ids_used[] = $temp_u->id;
        }
        $admin_partner = Auth::User()->getTopLevelPartner();
        if($term == ''){
            $url = $admin_partner->link_middleware_api . '?method=sensitiveInfo&api_key='.$admin_partner->middleware_api_key;
            $json_ids = json_encode($user_ids_used);
            //$url .= "&user_ids=" . $json_ids . "&term='" . $term . "'";
            $post_fields = "user_ids=" . $json_ids ;
        }
        else{
            $url = $admin_partner->link_middleware_api . '?method=searchSensitive&api_key='.$admin_partner->middleware_api_key;
            $json_ids = json_encode($user_ids_used);
            //$url .= "&user_ids=" . $json_ids . "&term='" . $term . "'";
            $post_fields = "term=" . $term . "&user_ids=" . $json_ids ;
        }
        $curl = curl_init();
        // Set some options - we are passing in a useragent too here
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $post_fields,
            CURLOPT_SSL_VERIFYPEER => False,
            CURLOPT_USERAGENT => 'Testing Sensitive Info'
        ));
        // Send the request & save response to $resp
        $resp = curl_exec($curl);
        $resp_curl = json_decode($resp);
        if(curl_errno($curl)){
            var_dump(curl_error($curl));
            exit();
        }

        if($resp_curl){
            $Users_info = $resp_curl;
            $buffer = new BluCollection();

            foreach($Users_info as $member) {
                if(in_array($member->blu_id,$user_ids_used)){
                    $u = new \stdClass();
                    $u->user_id    = $member->blu_id;
                    $u->first_name = $member->first_name;
                    $u->last_name = $member->last_name;
                    $u->email = $member->email;
                    $u->normalized_mobile = $member->mobile;
                    $user = User::find($member->blu_id);
                    $u->country_id = $user->country_id;
                    $u->status = $user->status;
                    $u->marital_status = strtolower($user->marital_status);
                    $buffer->add($u);
                }
            }
            return $buffer;
        }

    }
}