<?xml version="1.0" encoding="utf-8"?>
<wsdl:definitions xmlns:tm="http://microsoft.com/wsdl/mime/textMatching/" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:mime="http://schemas.xmlsoap.org/wsdl/mime/" xmlns:tns="http://tempuri.org/" xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" xmlns:s1="http://microsoft.com/wsdl/types/" xmlns:s="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://schemas.xmlsoap.org/wsdl/soap12/" xmlns:http="http://schemas.xmlsoap.org/wsdl/http/" targetNamespace="http://tempuri.org/" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/">
  <wsdl:types>
    <s:schema elementFormDefault="qualified" targetNamespace="http://tempuri.org/">
      <s:import namespace="http://microsoft.com/wsdl/types/" />
      <s:element name="HelloWorld">
        <s:complexType />
      </s:element>
      <s:element name="HelloWorldResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="HelloWorldResult" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="getuserinfobycard">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="card" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="getuserinfobycardResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="getuserinfobycardResult" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="getBluUserInfobyMobile">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="mobile" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="getBluUserInfobyMobileResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="getBluUserInfobyMobileResult" type="tns:BluUser" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:complexType name="BluUser">
        <s:sequence>
          <s:element minOccurs="1" maxOccurs="1" name="BUserID" type="s:int" />
          <s:element minOccurs="1" maxOccurs="1" name="EntityId" type="s:int" />
          <s:element minOccurs="0" maxOccurs="1" name="ReferenceId" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="Cards" type="tns:ArrayOfString" />
          <s:element minOccurs="0" maxOccurs="1" name="FirstName" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="LastName" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="Occupation" type="s:string" />
          <s:element minOccurs="1" maxOccurs="1" name="DOB" type="s:dateTime" />
          <s:element minOccurs="0" maxOccurs="1" name="Email" type="s:string" />
          <s:element minOccurs="1" maxOccurs="1" name="RemainingPoints" type="s:int" />
          <s:element minOccurs="0" maxOccurs="1" name="Mobile" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="Phone" type="s:string" />
        </s:sequence>
      </s:complexType>
      <s:complexType name="ArrayOfString">
        <s:sequence>
          <s:element minOccurs="0" maxOccurs="unbounded" name="string" nillable="true" type="s:string" />
        </s:sequence>
      </s:complexType>
      <s:element name="getBluUserInfobyCard">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="card" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="getBluUserInfobyCardResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="getBluUserInfobyCardResult" type="tns:BluUser" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="CreateBluUser">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="bluuser" type="tns:BluUser" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="CreateBluUserResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="CreateBluUserResult" type="tns:BluUser" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="insertTransaction">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="blutrx" type="tns:BluTrx" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:complexType name="BluTrx">
        <s:sequence>
          <s:element minOccurs="1" maxOccurs="1" name="Id" type="s:int" />
          <s:element minOccurs="1" maxOccurs="1" name="TrxId" type="s:int" />
          <s:element minOccurs="0" maxOccurs="1" name="Batch" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="Reference" type="s:string" />
          <s:element minOccurs="1" maxOccurs="1" name="TrxDate" type="s:dateTime" />
          <s:element minOccurs="1" maxOccurs="1" name="TotalAmount" type="s:decimal" />
          <s:element minOccurs="1" maxOccurs="1" name="TotalPaid" type="s:decimal" />
          <s:element minOccurs="0" maxOccurs="1" name="Mapped_StoreId" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="Mapped_POSId" type="s:string" />
          <s:element minOccurs="1" maxOccurs="1" name="FK_BusinessUserId" type="s:int" />
          <s:element minOccurs="0" maxOccurs="1" name="Card" type="s:string" />
          <s:element minOccurs="1" maxOccurs="1" name="Discount" type="s:int" />
          <s:element minOccurs="1" maxOccurs="1" name="DiscountAmount" type="s:decimal" />
          <s:element minOccurs="1" maxOccurs="1" name="FK_RulesId" type="s:int" />
          <s:element minOccurs="0" maxOccurs="1" name="note" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="currency" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="CreatedBy" type="s:string" />
          <s:element minOccurs="1" maxOccurs="1" name="CreatedOn" type="s:dateTime" />
          <s:element minOccurs="1" maxOccurs="1" name="ModifiedOn" type="s:dateTime" />
          <s:element minOccurs="0" maxOccurs="1" name="ModifiedBy" type="s:string" />
          <s:element minOccurs="1" maxOccurs="1" name="FK_EntityId" type="s:int" />
          <s:element minOccurs="1" maxOccurs="1" name="FK_StatusId" type="s:int" />
        </s:sequence>
      </s:complexType>
      <s:element name="insertTransactionResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="insertTransactionResult" type="s:int" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="processbatch">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="batch" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="processbatchResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="processbatchResult" type="s:int" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="insertBulkTransactions">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="blutrxArray" type="tns:ArrayOfBluTrx" />
            <s:element minOccurs="1" maxOccurs="1" name="FK_entityid" type="s:int" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:complexType name="ArrayOfBluTrx">
        <s:sequence>
          <s:element minOccurs="0" maxOccurs="unbounded" name="BluTrx" nillable="true" type="tns:BluTrx" />
        </s:sequence>
      </s:complexType>
      <s:element name="insertBulkTransactionsResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="insertBulkTransactionsResult" type="s:int" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="getUserByMobile">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="mobile" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="getUserByMobileResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="getUserByMobileResult" type="tns:BusinessUser" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:complexType name="BusinessUser">
        <s:complexContent mixed="false">
          <s:extension base="tns:BaseClassDO">
            <s:sequence>
              <s:element minOccurs="1" maxOccurs="1" name="BusinessUserId" type="s:int" />
              <s:element minOccurs="0" maxOccurs="1" name="RemainingPoints" type="s:string" />
              <s:element minOccurs="0" maxOccurs="1" name="ImgName" type="s:string" />
              <s:element minOccurs="0" maxOccurs="1" name="Title" type="s:string" />
              <s:element minOccurs="1" maxOccurs="1" name="FK_userId2" type="s:int" />
              <s:element minOccurs="0" maxOccurs="1" name="O_User" type="tns:User" />
              <s:element minOccurs="0" maxOccurs="1" name="HouryRate" type="s:string" />
              <s:element minOccurs="0" maxOccurs="1" name="Region" type="s:string" />
              <s:element minOccurs="0" maxOccurs="1" name="Currency" type="s:string" />
              <s:element minOccurs="0" maxOccurs="1" name="UserName" type="s:string" />
              <s:element minOccurs="0" maxOccurs="1" name="FirstName" type="s:string" />
              <s:element minOccurs="0" maxOccurs="1" name="LastName" type="s:string" />
              <s:element minOccurs="0" maxOccurs="1" name="Position" type="s:string" />
              <s:element minOccurs="0" maxOccurs="1" name="Company" type="s:string" />
              <s:element minOccurs="0" maxOccurs="1" name="Phone" type="s:string" />
              <s:element minOccurs="0" maxOccurs="1" name="Mobile" type="s:string" />
              <s:element minOccurs="0" maxOccurs="1" name="O_Status" type="tns:Status" />
              <s:element minOccurs="0" maxOccurs="1" name="Industry" type="s:string" />
              <s:element minOccurs="0" maxOccurs="1" name="Gender" type="s:string" />
              <s:element minOccurs="1" maxOccurs="1" name="DateOfBirth" type="s:dateTime" />
              <s:element minOccurs="0" maxOccurs="1" name="MaritalStatus" type="s:string" />
              <s:element minOccurs="0" maxOccurs="1" name="Tag" type="s:string" />
              <s:element minOccurs="0" maxOccurs="1" name="Club" type="s:string" />
              <s:element minOccurs="0" maxOccurs="1" name="CreatedFrom" type="s:string" />
              <s:element minOccurs="0" maxOccurs="1" name="RunningStatus" type="s:string" />
              <s:element minOccurs="0" maxOccurs="1" name="Nationality" type="s:string" />
              <s:element minOccurs="0" maxOccurs="1" name="TSize" type="s:string" />
              <s:element minOccurs="0" maxOccurs="1" name="BSize" type="s:string" />
              <s:element minOccurs="0" maxOccurs="1" name="SSize" type="s:string" />
              <s:element minOccurs="0" maxOccurs="1" name="JSize" type="s:string" />
              <s:element minOccurs="1" maxOccurs="1" name="BloooomId" type="s:int" />
              <s:element minOccurs="0" maxOccurs="1" name="Pincode" type="s:string" />
              <s:element minOccurs="0" maxOccurs="1" name="FbookId" type="s:string" />
              <s:element minOccurs="0" maxOccurs="1" name="FbookLink" type="s:string" />
              <s:element minOccurs="0" maxOccurs="1" name="TwitterId" type="s:string" />
              <s:element minOccurs="0" maxOccurs="1" name="TwitterLink" type="s:string" />
              <s:element minOccurs="0" maxOccurs="1" name="Token" type="s:string" />
              <s:element minOccurs="0" maxOccurs="1" name="TokenType" type="s:string" />
            </s:sequence>
          </s:extension>
        </s:complexContent>
      </s:complexType>
      <s:complexType name="BaseClassDO">
        <s:sequence>
          <s:element minOccurs="0" maxOccurs="1" name="CreatedBy" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="ModifiedBy" type="s:string" />
          <s:element minOccurs="1" maxOccurs="1" name="CreatedOn" type="s:dateTime" />
          <s:element minOccurs="1" maxOccurs="1" name="ModifiedOn" type="s:dateTime" />
        </s:sequence>
      </s:complexType>
      <s:complexType name="User">
        <s:complexContent mixed="false">
          <s:extension base="tns:MembershipUser">
            <s:sequence>
              <s:element minOccurs="1" maxOccurs="1" name="Identity" type="s1:guid" />
              <s:element minOccurs="0" maxOccurs="1" name="O_MembershipUser" type="tns:MembershipUser" />
              <s:element minOccurs="1" maxOccurs="1" name="UserId" type="s1:guid" />
              <s:element minOccurs="1" maxOccurs="1" name="UserId2" type="s:int" />
              <s:element minOccurs="0" maxOccurs="1" name="UserName" type="s:string" />
              <s:element minOccurs="0" maxOccurs="1" name="ApplicationName" type="s:string" />
              <s:element minOccurs="0" maxOccurs="1" name="Password" type="s:string" />
              <s:element minOccurs="0" maxOccurs="1" name="PasswordQuestion" type="s:string" />
              <s:element minOccurs="0" maxOccurs="1" name="PasswordAnswer" type="s:string" />
              <s:element minOccurs="1" maxOccurs="1" name="LastLockoutDate" type="s:dateTime" />
              <s:element minOccurs="1" maxOccurs="1" name="LastPasswordChangedDate" type="s:dateTime" />
              <s:element minOccurs="1" maxOccurs="1" name="CreationDate" type="s:dateTime" />
              <s:element minOccurs="1" maxOccurs="1" name="IsOnline" type="s:boolean" />
              <s:element minOccurs="1" maxOccurs="1" name="IsLockedOut" type="s:boolean" />
              <s:element minOccurs="1" maxOccurs="1" name="LastLockedOutDate" type="s:dateTime" />
              <s:element minOccurs="1" maxOccurs="1" name="FailedPasswordAttemptCount" type="s:int" />
              <s:element minOccurs="1" maxOccurs="1" name="FailedPasswordAttemptWindowStart" type="s:dateTime" />
              <s:element minOccurs="1" maxOccurs="1" name="FailedPasswordAnswerAttemptCount" type="s:int" />
              <s:element minOccurs="1" maxOccurs="1" name="FailedPasswordAnswerAttemptWindowStart" type="s:dateTime" />
              <s:element minOccurs="0" maxOccurs="1" name="ProviderUserKey" />
              <s:element minOccurs="0" maxOccurs="1" name="ProviderName" type="s:string" />
            </s:sequence>
          </s:extension>
        </s:complexContent>
      </s:complexType>
      <s:complexType name="MembershipUser">
        <s:sequence>
          <s:element minOccurs="0" maxOccurs="1" name="Email" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="Comment" type="s:string" />
          <s:element minOccurs="1" maxOccurs="1" name="IsApproved" type="s:boolean" />
          <s:element minOccurs="1" maxOccurs="1" name="LastLoginDate" type="s:dateTime" />
          <s:element minOccurs="1" maxOccurs="1" name="LastActivityDate" type="s:dateTime" />
        </s:sequence>
      </s:complexType>
      <s:complexType name="Status">
        <s:complexContent mixed="false">
          <s:extension base="tns:ContentBaseClassDO">
            <s:sequence>
              <s:element minOccurs="1" maxOccurs="1" name="StatusId" type="s:int" />
              <s:element minOccurs="0" maxOccurs="1" name="StatusName" type="s:string" />
              <s:element minOccurs="0" maxOccurs="1" name="O_Module" type="tns:Module" />
            </s:sequence>
          </s:extension>
        </s:complexContent>
      </s:complexType>
      <s:complexType name="ContentBaseClassDO">
        <s:sequence>
          <s:element minOccurs="0" maxOccurs="1" name="CreatedBy" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="ModifiedBy" type="s:string" />
          <s:element minOccurs="1" maxOccurs="1" name="CreatedOn" type="s:dateTime" />
          <s:element minOccurs="1" maxOccurs="1" name="ModifiedOn" type="s:dateTime" />
        </s:sequence>
      </s:complexType>
      <s:complexType name="Module">
        <s:complexContent mixed="false">
          <s:extension base="tns:ContentBaseClassDO">
            <s:sequence>
              <s:element minOccurs="1" maxOccurs="1" name="ModuleId" type="s:int" />
              <s:element minOccurs="1" maxOccurs="1" name="ParentId" type="s:int" />
              <s:element minOccurs="0" maxOccurs="1" name="ModuleName" type="s:string" />
              <s:element minOccurs="0" maxOccurs="1" name="ShortDescription" type="s:string" />
            </s:sequence>
          </s:extension>
        </s:complexContent>
      </s:complexType>
      <s:element name="string" nillable="true" type="s:string" />
      <s:element name="BluUser" nillable="true" type="tns:BluUser" />
      <s:element name="int" type="s:int" />
      <s:element name="BusinessUser" nillable="true" type="tns:BusinessUser" />
    </s:schema>
    <s:schema elementFormDefault="qualified" targetNamespace="http://microsoft.com/wsdl/types/">
      <s:simpleType name="guid">
        <s:restriction base="s:string">
          <s:pattern value="[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}" />
        </s:restriction>
      </s:simpleType>
    </s:schema>
  </wsdl:types>
  <wsdl:message name="HelloWorldSoapIn">
    <wsdl:part name="parameters" element="tns:HelloWorld" />
  </wsdl:message>
  <wsdl:message name="HelloWorldSoapOut">
    <wsdl:part name="parameters" element="tns:HelloWorldResponse" />
  </wsdl:message>
  <wsdl:message name="getuserinfobycardSoapIn">
    <wsdl:part name="parameters" element="tns:getuserinfobycard" />
  </wsdl:message>
  <wsdl:message name="getuserinfobycardSoapOut">
    <wsdl:part name="parameters" element="tns:getuserinfobycardResponse" />
  </wsdl:message>
  <wsdl:message name="getBluUserInfobyMobileSoapIn">
    <wsdl:part name="parameters" element="tns:getBluUserInfobyMobile" />
  </wsdl:message>
  <wsdl:message name="getBluUserInfobyMobileSoapOut">
    <wsdl:part name="parameters" element="tns:getBluUserInfobyMobileResponse" />
  </wsdl:message>
  <wsdl:message name="getBluUserInfobyCardSoapIn">
    <wsdl:part name="parameters" element="tns:getBluUserInfobyCard" />
  </wsdl:message>
  <wsdl:message name="getBluUserInfobyCardSoapOut">
    <wsdl:part name="parameters" element="tns:getBluUserInfobyCardResponse" />
  </wsdl:message>
  <wsdl:message name="CreateBluUserSoapIn">
    <wsdl:part name="parameters" element="tns:CreateBluUser" />
  </wsdl:message>
  <wsdl:message name="CreateBluUserSoapOut">
    <wsdl:part name="parameters" element="tns:CreateBluUserResponse" />
  </wsdl:message>
  <wsdl:message name="insertTransactionSoapIn">
    <wsdl:part name="parameters" element="tns:insertTransaction" />
  </wsdl:message>
  <wsdl:message name="insertTransactionSoapOut">
    <wsdl:part name="parameters" element="tns:insertTransactionResponse" />
  </wsdl:message>
  <wsdl:message name="processbatchSoapIn">
    <wsdl:part name="parameters" element="tns:processbatch" />
  </wsdl:message>
  <wsdl:message name="processbatchSoapOut">
    <wsdl:part name="parameters" element="tns:processbatchResponse" />
  </wsdl:message>
  <wsdl:message name="insertBulkTransactionsSoapIn">
    <wsdl:part name="parameters" element="tns:insertBulkTransactions" />
  </wsdl:message>
  <wsdl:message name="insertBulkTransactionsSoapOut">
    <wsdl:part name="parameters" element="tns:insertBulkTransactionsResponse" />
  </wsdl:message>
  <wsdl:message name="getUserByMobileSoapIn">
    <wsdl:part name="parameters" element="tns:getUserByMobile" />
  </wsdl:message>
  <wsdl:message name="getUserByMobileSoapOut">
    <wsdl:part name="parameters" element="tns:getUserByMobileResponse" />
  </wsdl:message>
  <wsdl:message name="HelloWorldHttpGetIn" />
  <wsdl:message name="HelloWorldHttpGetOut">
    <wsdl:part name="Body" element="tns:string" />
  </wsdl:message>
  <wsdl:message name="getuserinfobycardHttpGetIn">
    <wsdl:part name="card" type="s:string" />
  </wsdl:message>
  <wsdl:message name="getuserinfobycardHttpGetOut">
    <wsdl:part name="Body" element="tns:string" />
  </wsdl:message>
  <wsdl:message name="getBluUserInfobyMobileHttpGetIn">
    <wsdl:part name="mobile" type="s:string" />
  </wsdl:message>
  <wsdl:message name="getBluUserInfobyMobileHttpGetOut">
    <wsdl:part name="Body" element="tns:BluUser" />
  </wsdl:message>
  <wsdl:message name="getBluUserInfobyCardHttpGetIn">
    <wsdl:part name="card" type="s:string" />
  </wsdl:message>
  <wsdl:message name="getBluUserInfobyCardHttpGetOut">
    <wsdl:part name="Body" element="tns:BluUser" />
  </wsdl:message>
  <wsdl:message name="processbatchHttpGetIn">
    <wsdl:part name="batch" type="s:string" />
  </wsdl:message>
  <wsdl:message name="processbatchHttpGetOut">
    <wsdl:part name="Body" element="tns:int" />
  </wsdl:message>
  <wsdl:message name="getUserByMobileHttpGetIn">
    <wsdl:part name="mobile" type="s:string" />
  </wsdl:message>
  <wsdl:message name="getUserByMobileHttpGetOut">
    <wsdl:part name="Body" element="tns:BusinessUser" />
  </wsdl:message>
  <wsdl:message name="HelloWorldHttpPostIn" />
  <wsdl:message name="HelloWorldHttpPostOut">
    <wsdl:part name="Body" element="tns:string" />
  </wsdl:message>
  <wsdl:message name="getuserinfobycardHttpPostIn">
    <wsdl:part name="card" type="s:string" />
  </wsdl:message>
  <wsdl:message name="getuserinfobycardHttpPostOut">
    <wsdl:part name="Body" element="tns:string" />
  </wsdl:message>
  <wsdl:message name="getBluUserInfobyMobileHttpPostIn">
    <wsdl:part name="mobile" type="s:string" />
  </wsdl:message>
  <wsdl:message name="getBluUserInfobyMobileHttpPostOut">
    <wsdl:part name="Body" element="tns:BluUser" />
  </wsdl:message>
  <wsdl:message name="getBluUserInfobyCardHttpPostIn">
    <wsdl:part name="card" type="s:string" />
  </wsdl:message>
  <wsdl:message name="getBluUserInfobyCardHttpPostOut">
    <wsdl:part name="Body" element="tns:BluUser" />
  </wsdl:message>
  <wsdl:message name="processbatchHttpPostIn">
    <wsdl:part name="batch" type="s:string" />
  </wsdl:message>
  <wsdl:message name="processbatchHttpPostOut">
    <wsdl:part name="Body" element="tns:int" />
  </wsdl:message>
  <wsdl:message name="getUserByMobileHttpPostIn">
    <wsdl:part name="mobile" type="s:string" />
  </wsdl:message>
  <wsdl:message name="getUserByMobileHttpPostOut">
    <wsdl:part name="Body" element="tns:BusinessUser" />
  </wsdl:message>
  <wsdl:portType name="BluWebServiceSoap">
    <wsdl:operation name="HelloWorld">
      <wsdl:input message="tns:HelloWorldSoapIn" />
      <wsdl:output message="tns:HelloWorldSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="getuserinfobycard">
      <wsdl:input message="tns:getuserinfobycardSoapIn" />
      <wsdl:output message="tns:getuserinfobycardSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="getBluUserInfobyMobile">
      <wsdl:input message="tns:getBluUserInfobyMobileSoapIn" />
      <wsdl:output message="tns:getBluUserInfobyMobileSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="getBluUserInfobyCard">
      <wsdl:input message="tns:getBluUserInfobyCardSoapIn" />
      <wsdl:output message="tns:getBluUserInfobyCardSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="CreateBluUser">
      <wsdl:input message="tns:CreateBluUserSoapIn" />
      <wsdl:output message="tns:CreateBluUserSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="insertTransaction">
      <wsdl:input message="tns:insertTransactionSoapIn" />
      <wsdl:output message="tns:insertTransactionSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="processbatch">
      <wsdl:input message="tns:processbatchSoapIn" />
      <wsdl:output message="tns:processbatchSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="insertBulkTransactions">
      <wsdl:input message="tns:insertBulkTransactionsSoapIn" />
      <wsdl:output message="tns:insertBulkTransactionsSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="getUserByMobile">
      <wsdl:input message="tns:getUserByMobileSoapIn" />
      <wsdl:output message="tns:getUserByMobileSoapOut" />
    </wsdl:operation>
  </wsdl:portType>
  <wsdl:portType name="BluWebServiceHttpGet">
    <wsdl:operation name="HelloWorld">
      <wsdl:input message="tns:HelloWorldHttpGetIn" />
      <wsdl:output message="tns:HelloWorldHttpGetOut" />
    </wsdl:operation>
    <wsdl:operation name="getuserinfobycard">
      <wsdl:input message="tns:getuserinfobycardHttpGetIn" />
      <wsdl:output message="tns:getuserinfobycardHttpGetOut" />
    </wsdl:operation>
    <wsdl:operation name="getBluUserInfobyMobile">
      <wsdl:input message="tns:getBluUserInfobyMobileHttpGetIn" />
      <wsdl:output message="tns:getBluUserInfobyMobileHttpGetOut" />
    </wsdl:operation>
    <wsdl:operation name="getBluUserInfobyCard">
      <wsdl:input message="tns:getBluUserInfobyCardHttpGetIn" />
      <wsdl:output message="tns:getBluUserInfobyCardHttpGetOut" />
    </wsdl:operation>
    <wsdl:operation name="processbatch">
      <wsdl:input message="tns:processbatchHttpGetIn" />
      <wsdl:output message="tns:processbatchHttpGetOut" />
    </wsdl:operation>
    <wsdl:operation name="getUserByMobile">
      <wsdl:input message="tns:getUserByMobileHttpGetIn" />
      <wsdl:output message="tns:getUserByMobileHttpGetOut" />
    </wsdl:operation>
  </wsdl:portType>
  <wsdl:portType name="BluWebServiceHttpPost">
    <wsdl:operation name="HelloWorld">
      <wsdl:input message="tns:HelloWorldHttpPostIn" />
      <wsdl:output message="tns:HelloWorldHttpPostOut" />
    </wsdl:operation>
    <wsdl:operation name="getuserinfobycard">
      <wsdl:input message="tns:getuserinfobycardHttpPostIn" />
      <wsdl:output message="tns:getuserinfobycardHttpPostOut" />
    </wsdl:operation>
    <wsdl:operation name="getBluUserInfobyMobile">
      <wsdl:input message="tns:getBluUserInfobyMobileHttpPostIn" />
      <wsdl:output message="tns:getBluUserInfobyMobileHttpPostOut" />
    </wsdl:operation>
    <wsdl:operation name="getBluUserInfobyCard">
      <wsdl:input message="tns:getBluUserInfobyCardHttpPostIn" />
      <wsdl:output message="tns:getBluUserInfobyCardHttpPostOut" />
    </wsdl:operation>
    <wsdl:operation name="processbatch">
      <wsdl:input message="tns:processbatchHttpPostIn" />
      <wsdl:output message="tns:processbatchHttpPostOut" />
    </wsdl:operation>
    <wsdl:operation name="getUserByMobile">
      <wsdl:input message="tns:getUserByMobileHttpPostIn" />
      <wsdl:output message="tns:getUserByMobileHttpPostOut" />
    </wsdl:operation>
  </wsdl:portType>
  <wsdl:binding name="BluWebServiceSoap" type="tns:BluWebServiceSoap">
    <soap:binding transport="http://schemas.xmlsoap.org/soap/http" />
    <wsdl:operation name="HelloWorld">
      <soap:operation soapAction="http://tempuri.org/HelloWorld" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getuserinfobycard">
      <soap:operation soapAction="http://tempuri.org/getuserinfobycard" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getBluUserInfobyMobile">
      <soap:operation soapAction="http://tempuri.org/getBluUserInfobyMobile" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getBluUserInfobyCard">
      <soap:operation soapAction="http://tempuri.org/getBluUserInfobyCard" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="CreateBluUser">
      <soap:operation soapAction="http://tempuri.org/CreateBluUser" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="insertTransaction">
      <soap:operation soapAction="http://tempuri.org/insertTransaction" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="processbatch">
      <soap:operation soapAction="http://tempuri.org/processbatch" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="insertBulkTransactions">
      <soap:operation soapAction="http://tempuri.org/insertBulkTransactions" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getUserByMobile">
      <soap:operation soapAction="http://tempuri.org/getUserByMobile" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
  </wsdl:binding>
  <wsdl:binding name="BluWebServiceSoap12" type="tns:BluWebServiceSoap">
    <soap12:binding transport="http://schemas.xmlsoap.org/soap/http" />
    <wsdl:operation name="HelloWorld">
      <soap12:operation soapAction="http://tempuri.org/HelloWorld" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getuserinfobycard">
      <soap12:operation soapAction="http://tempuri.org/getuserinfobycard" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getBluUserInfobyMobile">
      <soap12:operation soapAction="http://tempuri.org/getBluUserInfobyMobile" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getBluUserInfobyCard">
      <soap12:operation soapAction="http://tempuri.org/getBluUserInfobyCard" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="CreateBluUser">
      <soap12:operation soapAction="http://tempuri.org/CreateBluUser" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="insertTransaction">
      <soap12:operation soapAction="http://tempuri.org/insertTransaction" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="processbatch">
      <soap12:operation soapAction="http://tempuri.org/processbatch" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="insertBulkTransactions">
      <soap12:operation soapAction="http://tempuri.org/insertBulkTransactions" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getUserByMobile">
      <soap12:operation soapAction="http://tempuri.org/getUserByMobile" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
  </wsdl:binding>
  <wsdl:binding name="BluWebServiceHttpGet" type="tns:BluWebServiceHttpGet">
    <http:binding verb="GET" />
    <wsdl:operation name="HelloWorld">
      <http:operation location="/HelloWorld" />
      <wsdl:input>
        <http:urlEncoded />
      </wsdl:input>
      <wsdl:output>
        <mime:mimeXml part="Body" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getuserinfobycard">
      <http:operation location="/getuserinfobycard" />
      <wsdl:input>
        <http:urlEncoded />
      </wsdl:input>
      <wsdl:output>
        <mime:mimeXml part="Body" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getBluUserInfobyMobile">
      <http:operation location="/getBluUserInfobyMobile" />
      <wsdl:input>
        <http:urlEncoded />
      </wsdl:input>
      <wsdl:output>
        <mime:mimeXml part="Body" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getBluUserInfobyCard">
      <http:operation location="/getBluUserInfobyCard" />
      <wsdl:input>
        <http:urlEncoded />
      </wsdl:input>
      <wsdl:output>
        <mime:mimeXml part="Body" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="processbatch">
      <http:operation location="/processbatch" />
      <wsdl:input>
        <http:urlEncoded />
      </wsdl:input>
      <wsdl:output>
        <mime:mimeXml part="Body" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getUserByMobile">
      <http:operation location="/getUserByMobile" />
      <wsdl:input>
        <http:urlEncoded />
      </wsdl:input>
      <wsdl:output>
        <mime:mimeXml part="Body" />
      </wsdl:output>
    </wsdl:operation>
  </wsdl:binding>
  <wsdl:binding name="BluWebServiceHttpPost" type="tns:BluWebServiceHttpPost">
    <http:binding verb="POST" />
    <wsdl:operation name="HelloWorld">
      <http:operation location="/HelloWorld" />
      <wsdl:input>
        <mime:content type="application/x-www-form-urlencoded" />
      </wsdl:input>
      <wsdl:output>
        <mime:mimeXml part="Body" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getuserinfobycard">
      <http:operation location="/getuserinfobycard" />
      <wsdl:input>
        <mime:content type="application/x-www-form-urlencoded" />
      </wsdl:input>
      <wsdl:output>
        <mime:mimeXml part="Body" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getBluUserInfobyMobile">
      <http:operation location="/getBluUserInfobyMobile" />
      <wsdl:input>
        <mime:content type="application/x-www-form-urlencoded" />
      </wsdl:input>
      <wsdl:output>
        <mime:mimeXml part="Body" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getBluUserInfobyCard">
      <http:operation location="/getBluUserInfobyCard" />
      <wsdl:input>
        <mime:content type="application/x-www-form-urlencoded" />
      </wsdl:input>
      <wsdl:output>
        <mime:mimeXml part="Body" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="processbatch">
      <http:operation location="/processbatch" />
      <wsdl:input>
        <mime:content type="application/x-www-form-urlencoded" />
      </wsdl:input>
      <wsdl:output>
        <mime:mimeXml part="Body" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getUserByMobile">
      <http:operation location="/getUserByMobile" />
      <wsdl:input>
        <mime:content type="application/x-www-form-urlencoded" />
      </wsdl:input>
      <wsdl:output>
        <mime:mimeXml part="Body" />
      </wsdl:output>
    </wsdl:operation>
  </wsdl:binding>
  <wsdl:service name="BluWebService">
    <wsdl:port name="BluWebServiceSoap" binding="tns:BluWebServiceSoap">
      <soap:address location="http://bloooomserver.com/blu/webservices/bluwebservice.asmx" />
    </wsdl:port>
    <wsdl:port name="BluWebServiceSoap12" binding="tns:BluWebServiceSoap12">
      <soap12:address location="http://bloooomserver.com/blu/webservices/bluwebservice.asmx" />
    </wsdl:port>
    <wsdl:port name="BluWebServiceHttpGet" binding="tns:BluWebServiceHttpGet">
      <http:address location="http://bloooomserver.com/blu/webservices/bluwebservice.asmx" />
    </wsdl:port>
    <wsdl:port name="BluWebServiceHttpPost" binding="tns:BluWebServiceHttpPost">
      <http:address location="http://bloooomserver.com/blu/webservices/bluwebservice.asmx" />
    </wsdl:port>
  </wsdl:service>
</wsdl:definitions>