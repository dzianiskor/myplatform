<?php
use App\Admin as Admin;
use App\Balance;
use App\Partner as Partner;
use App\User as User;
use Illuminate\Support\Facades\Auth;
/**
 * Support Helper
 *
 * @category   Helper Classes
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class Support
{
    /**
     * Return a full user name from the ID in reverse
     *
     * @param int $id
     * @return string
     */
    public static function memberRevNameFromID($id)
    {
    	if(!$id) {
    		return null;
    	}

        $user = User::where('id', $id)->first();

        if(!$user) {
        	return null;
        }

        $memberName = "{$user->last_name}, {$user->first_name}";
        if($memberName == 'na, na' || (empty($user->last_name) && empty($user->first_name))){
          $memberName = 'N/A';  
        }

        return $memberName;
    }

    /**
     * Return a balance from the ID
     *
     * @param int $userId
     * @return string
     */
    public static function getBalanceByUserID($userId)
    {
        if(!$userId) {
            return null;
        }

        $balance = Balance::where('user_id', $userId)->first();

        if(!$balance) {
            return null;
        }

        return $balance->balance;
    }

    /**
     * Return a full user name from the ID
     *
     * @param int $id
     * @return string
     */
    public static function memberNameFromID($id)
    {
    	if(!$id) {
    		return null;
    	}
        
        $user            = new User();
        $user->first_name = 'N/A';
        $user->last_name = 'N/A';
        $admin_partner = Auth::User()->getTopLevelPartner();
        $user_ids_used = array($id);
        $term = '';
        if($admin_partner->has_middleware == '1'){
            $url = $admin_partner->link_middleware_api . '?method=searchSensitive&api_key='.$admin_partner->middleware_api_key;
            $json_ids = json_encode($user_ids_used);
            $url .= "&user_ids=" . $json_ids . "&term='" . $term . "'";
            $curl = curl_init();
            // Set some options - we are passing in a useragent too here
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => $url,
                CURLOPT_USERAGENT => 'Testing Sensitive Info'
            ));
            // Send the request & save response to $resp
            $resp = curl_exec($curl);
            $resp_curl = json_decode($resp);

            $Users_info = $resp_curl;
            if ($Users_info) foreach($Users_info as $member1) {
                if(in_array($member1->blu_id,$user_ids_used)){
                    $user            = new User();
                    $user->first_name = $member1->first_name;
                    $user->last_name = $member1->last_name;
                    $user->email = $member1->email;
                    $user->mobile = $member1->mobile;
                    $user->title = $member1->title;
                }
            }
        }
        else{
            $user = User::where('id', $id)->first();
        }
        if(!$user) {
        	return null;
        }

        $memberName = "{$user->last_name}, {$user->first_name}";
        if($memberName == 'na, na'){
          $memberName = 'N/A';  
        }
        return $memberName;
    }

    /**
     * Return a full partner entity name from its ID
     *
     * @param int $id
     * @return string
     */
    public static function partnerNameFromID($id)
    {
        if(!$id) {
            return null;
        }

        $partner = Partner::where('id', $id)->first();

        if(!$partner) {
            return null;
        }

        return $partner->name;
    }

    /**
     * Fetch a partner image from a partner ID
     *
     * @param int $id
     * @return int
     */
    public static function partnerImageFromID($id)
    {
        if(!$id) {
            return null;
        }

        $partner = Partner::where('id', $id)->first();

        if(!$partner) {
            return null;
        }

        return $partner->image;
    }

    /**
     * Create a 'time ago' string representation
     *
     * @param time $ptime
     * @return string
     */
    public static function timeElapsed($ptime)
    {
        $etime = time() - $ptime;

        if ($etime < 1)
        {
            return '0 seconds';
        }

        $a = array( 12 * 30 * 24 * 60 * 60  => 'year',
                    30 * 24 * 60 * 60       => 'month',
                    24 * 60 * 60            => 'day',
                    60 * 60                 => 'hour',
                    60                      => 'minute',
                    1                       => 'second'
                    );

        foreach ($a as $secs => $str)
        {
            $d = $etime / $secs;
            if ($d >= 1)
            {
                $r = round($d);
                return $r . ' ' . $str . ($r > 1 ? 's' : '') . ' ago';
            }
        }
    }

	/**
     * Return a full admin name from the ID in reverse
     *
     * @param int $id
     * @return string
     */
    public static function adminRevNameFromID($id)
    {
    	if(!$id) {
    		return null;
    	}

        $user = Admin::where('id', $id)->first();

        if(!$user) {
        	return null;
        }

        return "{$user->last_name}, {$user->first_name}";
    }

    /**
     * Return a full admin name from the ID
     *
     * @param int $id
     * @return string
     */
    public static function adminNameFromID($id)
    {
    	if(!$id) {
    		return null;
    	}

        $user = Admin::where('id', $id)->first();

        if(!$user) {
        	return null;
        }

        return "{$user->first_name} {$user->last_name}";
    }
} // EOC