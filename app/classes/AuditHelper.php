<?php
/**
 * Auth Trail Helper
 * 
 * @category   Helper Classes
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class AuditHelper 
{
	/**
	 * Record audit trail activity 
	 * 
	 * @param int $partner_id
	 * @param string $description
	 * @return void
	 */
	public static function record($partner_id, $detail)
	{
		$audit = new App\Trail();
		
		$audit->partner_id = $partner_id;
		$audit->detail     = $detail;

		$audit->save();
	}
        
        /**
	 * Record audit trail activity 
	 * 
	 * @param int $partner_id
	 * @param string $description
	 * @return void
	 */
	public static function recorduser($partner_id,$user_id, $detail)
	{
		$audit = new App\TrailUser();
		
		$audit->partner_id = $partner_id;
                $audit->user_id = $user_id;
		$audit->detail     = $detail;

		$audit->save();
	}

} // EOC