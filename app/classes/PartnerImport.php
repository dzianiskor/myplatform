<?php

/**
 * Partner Import Helper
 * 
 * @category   Helper Classes
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class PartnerImport extends ExcelImport 
{
    /**
     * Perform the partner import action
     * 
     * @param object $userSheet
     * @return void
     */
    public static function import($partnerSheet) 
    {
        if(!$partnerSheet) {
            return;
        }

        $numRows = $partnerSheet->getHighestRow();
        $cells   = $partnerSheet->rangeToArray('A1:U'.$numRows);

        // Skip the header row
        for ($i = 1; $i < $numRows; $i++) {

            $row = null;

            if(isset($cells[$i])) {
                $row = $cells[$i];
            }

            if(!$row) {
                continue;
            }

            if($row[0] == 1) {
                continue;
            }

            $partner                     = array();
            $partner['id']               = $row[0];
            $partner['name']             = $row[1];
            $partner['parent_id']        = $row[4];         
            $partner['smsgate_username'] = $row[5];
            $partner['smsgate_password'] = $row[6];
            $partner['smsgate']          = $row[7];
            $partner['smsgate_number']   = $row[16];
            $partner['contact_name']     = $row[10];
            $partner['contact_email']    = $row[11];
            $partner['contact_number']   = $row[12];
            $partner['contact_website']  = $row[13];
            $partner['draft']            = false;
            $partner['status']           = 'Enabled';
            $partner['created_at']       = date("Y-m-d H:i:s");
            $partner['updated_at']       = date("Y-m-d H:i:s");
            $partner['ucid']             = UCID::newPartnerID();

            // Validate parent
            if($partner['parent_id'] == 0) {
                $partner['parent_id'] = null;
            }       

            // Validate SMS Gateway
            switch($partner['smsgate']) {
                case "PMM":
                    $partner['smsgate'] = 'MediaMasters';
                    break;
            }
            
            $validator = Validator::make($partner, array(
                'id'   => 'required|numeric',
                'name' => 'required|not_in:BLU'
            ));

            if(!$validator->fails()) {

                // Create the user 
                DB::table('partner')->insert($partner);

                // Assign to the BLU network
                DB::table('network_partner')->insert(array(
                    'partner_id' => $partner['id'],
                    'network_id' => 1
                ));
            }           
        }
    }

} // EOC