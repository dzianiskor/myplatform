<?php
/**
 * BsfMembersMiddlewareHelper Helper
 *
 * @category   Helper Classes
 * @link       http://blupoints.com
 */


class BsfMembersMiddlewareHelper
{

    public static function getUser($userId, $admin_partner)
    {

        if($admin_partner->has_middleware != '1'){
            return array();
        }

        $userIds = array($userId);
        $term = '';

        $url = $admin_partner->link_middleware_api . '?method=sensitiveInfo&api_key='.$admin_partner->middleware_api_key;
        $json_ids = json_encode($userIds);
        $url .= "&user_ids=" . $json_ids . "&term='" . $term . "'";

        $curl = curl_init();
        // Set some options - we are passing in a useragent too here
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url,
            CURLOPT_SSL_VERIFYPEER => False,
            CURLOPT_USERAGENT => 'Testing Sensitive Info'
        ));

        // Send the request & save response to $resp
        $resp = curl_exec($curl);
        $usersInfo = json_decode($resp);

        return $usersInfo;

    }

    public static function search($queryTerm, $users, $admin_partner)
    {

        if($admin_partner->has_middleware != '1'){
            return array();
        }

        $temp_users = $users->get();
        $user_ids_used = array();

        foreach ($temp_users as $temp_u) {
            $user_ids_used[] = $temp_u->id;
        }

        $i = 0;
        $temp_ids = [];
        $response = [];

        while ($i < count($user_ids_used)) {
            array_push($temp_ids, $user_ids_used[$i]);
            if ($i !== 0 && ($i == count($user_ids_used) - 1 || $i % 30000 == 0)) {

                $url = $admin_partner->link_middleware_api . '?method=searchSensitive&api_key=' . $admin_partner->middleware_api_key;
                $json_ids = json_encode($temp_ids);
                $post_fields = "term=" . $queryTerm . "&user_ids=" . $json_ids;

                $curl = curl_init();
                // Set some options - we are passing in a useragent too here
                curl_setopt_array($curl, array(
                    CURLOPT_RETURNTRANSFER => 1,
                    CURLOPT_URL => $url,
                    CURLOPT_POST => true,
                    CURLOPT_POSTFIELDS => $post_fields,
                    CURLOPT_SSL_VERIFYPEER => False,
                    CURLOPT_USERAGENT => 'Testing Sensitive Info'
                ));

                // Send the request & save response to $resp
                $resp = curl_exec($curl);

                if (curl_errno($curl)) {
                    var_dump(curl_error($curl));
                    exit();
                }
                $resp_curl1 = json_decode($resp);
                $response = array_merge($response, $resp_curl1);
                $temp_ids = [];
            }
            $i++;
        }

        return $response;

    }

    public static function curlGetRequestData()
    {



    }


} // EOC