<?php

use App\Http\Controllers\APIController;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\User as User;
use App\Transaction as Transaction;
use App\Partner as Partner;
use App\Country as Country;
use App\Network as Network;
use App\Currency as Currency;
use App\Product as Product;
use App\Store as Store;
use App\Loyalty as Loyalty;
use App\LoyaltyExceptionrule as LoyaltyExceptionrule;
/**
 * Banner Helper
 *
 * @category   Helper Classes
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class RewardHelper
{
    public static function loyaltyByPartner($partner_ids){
        $arr_program_rules = array();
        $arr_program_exception_rules = array();
        $arr_all = array();

        foreach($partner_ids as $partner_id){
            //APIController::postSendEmailJson(json_encode($partner_id),"program partner id");
            $program = Loyalty::getDefaultProgram($partner_id);
            //APIController::postSendEmailJson(json_encode($program), "Program");
            try{
                if(!$program) {
                    continue;
                } else {

                    // Do exception rules exist?
                    if($program->rules()->count() > 0) {

                        $reward_usd = 0;     // The reward USD amount
                        $reward_pts = 0;     // The reward points amount
                        $reward_tp  = null;  // The reward type

                        foreach($program->rules as $exception_rule) {

                            $arr_program_rules[$partner_id]['program'] = $program->toArray();
                            $arr_program_rules[$partner_id][$exception_rule->id] = $exception_rule->toArray();

                            $exception_rule_obj = LoyaltyExceptionrule::where('loyalty_rule_id',$exception_rule->id)->get();

                            foreach($exception_rule_obj as $except_rule){
                                $arr_program_rules[$partner_id][$exception_rule->id]['exception_rules'][$except_rule->id] = $except_rule->toArray();
                            }
                        }
                    }
                }
            }
            catch (Exception $ex) {
                $json_data = json_encode(array($ex->getMessage(),$ex->getLine()));
                APIController::postSendEmailJson($json_data, "try catch exception 0005 - " . $program->id);
            }
        }
        return $arr_program_rules;
    }


    public static function rewardByTransaction($loyaltyByPartner , $transaction, $type = 'reward',$use_partner_currency = false){
        if(isset($loyaltyByPartner[$transaction->partner_id]['program']['id'])){
            $program = $loyaltyByPartner[$transaction->partner_id]['program'];
            $program_id = $loyaltyByPartner[$transaction->partner_id]['program']['id'];
            $exception_rules = $loyaltyByPartner[$transaction->partner_id];
        }

        $user = User::where('id', $transaction->user_id)->first();
        try{
            if(!$program) {
                $transaction->points_rewarded = 0;
                $transaction->rule_id         = null;
            }
            else {
                $networkId			= $transaction->network_id;
                $network			= Network::find($networkId);
                $currency	= Currency::where('id', $transaction->currency_id)->first();
                $networkPointPrice	= $network->value_in_currency;
                $programCurrencyId	= $program['currency_id'];
                //$networkRewardPts	= $network->reward_pts;
                $amount			= $transaction->original_total_amount;
                if($use_partner_currency == TRUE){
                    $currency	= Currency::where('id', $transaction->partner_currency_id)->first();
                    $amount =$transaction->partner_amount;
                }
                if($programCurrencyId == $currency->id){
                    $amountInPoints			= $amount / $networkPointPrice;
                    $amountInNetworkCurrency	= $amount;
                    $networkCurrency		= Currency::where('id', $programCurrencyId)->first();
                }else{
                    $networkCurrency		= Currency::where('id', $programCurrencyId)->first();
                    $amountInUsd			= CurrencyHelper::convertToUSD($amount, $currency->short_code);
                    $amountInNetworkCurrency	= $amountInUsd['amount'] * $networkCurrency->latestRate();
                    $amountInPoints			= $amountInNetworkCurrency / $networkPointPrice;
                    $amount				= $amountInNetworkCurrency;
                }
                //coupon process

                $couponPoints_rewarded	= 0;
                $couponPrice_usd		= 0;


                $rule_matched = false; // Have we matched an exception rule?
                $exception_id = 0;
                $arr_matched_rules = array();
                // Do exception rules exist?
                unset($exception_rules['program']);
                if(count($exception_rules) > 0) {

                    $reward_usd = 0;     // The reward USD amount
                    $reward_pts = 0;     // The reward points amount
                    $reward_tp  = null;  // The reward type

                    foreach($exception_rules as $key_rule => $exception_rule) {
                        //                    $json_data = json_encode($exception_rule->toArray());
                        //                    APIController::postSendEmailJson($json_data);
                        if($rule_matched) {
                            break;
                        }
                        //$loyalty_rule = LoyaltyRule::find($exception_rule['id']);

                        $exception_rule_obj = $exception_rule['exception_rules'];


                        $arr_except_rule = array();
                        $except_rule_checked = false;
                        $except_rule_matched = 0;
                        //                    $last_rule = end($exception_rule_obj);
                        $ors_arr_match = array();
                        $and_matched = false;
                        foreach($exception_rule_obj as $except_rule){
                            //                    $json_data = json_encode(array("trx_id" => $transaction->id ,"except_rule_type"=> $except_rule['type'], "except_rule id"=> $exception_rule['id']));
                            //                    APIController::postSendEmailJson($json_data);

                            switch($except_rule['type']) {
                                case "Segment":
                                    //                                if(Segment::hasUser($transaction->user_id, $except_rule['rule_value'])) {
                                    if(Compare::is(Segment::hasUser($transaction->user_id, $except_rule['rule_value']), 1, $except_rule['comparison_operator'])) {

                                        $transaction->segment_id = $except_rule['rule_value'];

                                        $arr_except_rule['segment'][] = $except_rule['rule_value'];
                                        $except_rule_checked = true;
                                        $except_rule_matched = $exception_rule['id'];
                                        //$arr_matched_rules[] = $except_rule_matched;
                                        if($except_rule['operator'] == 'or' && $and_matched == true){
                                            $ors_arr_match[] = true;
                                        }
                                        elseif($except_rule['operator'] == 'and'){
                                            $and_matched = true;
                                        }
                                    }
                                    else{
                                        $except_rule_checked = false;

                                        if($except_rule['operator'] == 'and'){
                                            $and_matched = false;
                                            break 2;
                                        }else{
                                            $ors_arr_match[] = false;
                                        }
                                    }
                                    break;
                                case "Country":

                                    if($transaction->country_id) {

                                        $country = Country::where('id', $transaction->country_id)->first();
                                        //                                    if($currency->id == $except_rule['rule_value']) {
                                        if(Compare::is($country->id, $except_rule['rule_value'], $except_rule['comparison_operator'])) {

                                            $arr_except_rule['country'][] = $except_rule['rule_value'];
                                            $except_rule_checked = true;
                                            $except_rule_matched = $exception_rule['id'];
                                            if($except_rule['operator'] == 'or' && $and_matched == true){
                                                $ors_arr_match[] = true;
                                            }
                                            elseif($except_rule['operator'] == 'and'){
                                                $and_matched = true;
                                            }
                                            //                                        APIController::postSendEmailJson(json_encode(array($transaction->ref_number,$arr_except_rule,$except_rule_matched)),"Check Currency 001");

                                        }
                                        else{
                                            $except_rule_checked = false;

                                            if($except_rule['operator'] == 'and'){
                                                $and_matched = false;
                                                break 2;
                                            }else{
                                                $ors_arr_match[] = false;
                                            }
                                        }
                                    }
                                    elseif($transaction->store_id) {

                                        $store = Store::where('id', $transaction->store_id)->first();

                                        //                                    if($store->address->country_id == $exception_rule['rule_value']) {
                                        if(Compare::is($store->address->country_id, $exception_rule['rule_value'], $except_rule['comparison_operator'])) {
                                            $arr_except_rule['country'][] = $except_rule['rule_value'];
                                            $except_rule_checked = true;
                                            $except_rule_matched = $exception_rule['id'];
                                            //$arr_matched_rules[] = $except_rule_matched;
                                            $transaction->country_id = $exception_rule['rule_value'];
                                            if($except_rule['operator'] == 'or' && $and_matched == true){
                                                $ors_arr_match[] = true;
                                            }
                                            elseif($except_rule['operator'] == 'and'){
                                                $and_matched = true;
                                            }
                                        }
                                        else{
                                            $except_rule_checked = false;
                                            if($except_rule['operator'] == 'and'){
                                                $and_matched = false;
                                                break 2;
                                            }else{
                                                $ors_arr_match[] = false;
                                            }
                                        }
                                    }
                                    break;
                                case "Store":
                                    if($transaction->store_id) {

                                        $store = Store::where('id', $transaction->store_id)->first();

                                        //                                    if($store->id == $except_rule['rule_value']) {
                                        if(Compare::is($store->id,$except_rule['rule_value'], $except_rule['comparison_operator'])) {
                                            $arr_except_rule['store'][] = $except_rule['rule_value'];
                                            $except_rule_checked = true;
                                            $except_rule_matched = $exception_rule['id'];
                                            //$arr_matched_rules[] = $except_rule_matched;
                                            if($except_rule['operator'] == 'or' && $and_matched == true){
                                                $ors_arr_match[] = true;
                                            }
                                            elseif($except_rule['operator'] == 'and'){
                                                $and_matched = true;
                                            }
                                        }
                                        else{
                                            $except_rule_checked = false;
                                            if($except_rule['operator'] == 'and'){
                                                $and_matched = false;
                                                break 2;
                                            }else{
                                                $ors_arr_match[] = false;
                                            }
                                        }
                                    }
                                    break;
                                case "Item":

                                    if($transaction->items()->count() > 0) {
                                        foreach($transaction->items as $i) {
                                            //                                        if($i->product_id == $except_rule['rule_value']) {
                                            if(Compare::is($i->product_id, $except_rule['rule_value'],$except_rule['comparison_operator'])) {
                                                $arr_except_rule['item'][] = $except_rule['rule_value'];
                                                $except_rule_checked = true;
                                                $except_rule_matched = $exception_rule['id'];
                                                //$arr_matched_rules[] = $except_rule_matched;
                                                if($except_rule['operator'] == 'or' && $and_matched == true){
                                                    $ors_arr_match[] = true;
                                                }
                                                elseif($except_rule['operator'] == 'and'){
                                                    $and_matched = true;
                                                }
                                            }
                                            else{
                                                $except_rule_checked = false;
                                                if($except_rule['operator'] == 'and'){
                                                    $and_matched = false;
                                                    break 2;
                                                }else{
                                                    $ors_arr_match[] = false;
                                                }
                                            }
                                        }
                                    }
                                    if(!empty($transaction->product_id) && !is_null($transaction->product_id)){

                                        //                                    if($transaction->product_id == $except_rule['rule_value']) {
                                        if(Compare::is($transaction->product_id, $except_rule['rule_value'], $except_rule['comparison_operator'])) {
//                                                            $json_data = json_encode(array("trx_id" => $transaction->ref_number ,"transaction_product "=>$transaction->product_id,'rule_value'=>$except_rule['rule_value'],'exception_rule'=> $exception_rule['id']));
//                    APIController::postSendEmailJson($json_data,"check product comparison");
                                            $arr_except_rule['item'][] = $except_rule['rule_value'];
                                            $except_rule_checked = true;
                                            $except_rule_matched = $exception_rule['id'];
                                            //$arr_matched_rules[] = $except_rule_matched;
                                            if($except_rule['operator'] == 'or' && $and_matched == true){
                                                $ors_arr_match[] = true;
                                            }
                                            elseif($except_rule['operator'] == 'and'){
                                                $and_matched = true;
                                            }
                                        }
                                        else{
                                            $except_rule_checked = false;
                                            if($except_rule['operator'] == 'and'){
                                                $and_matched = false;
                                                break 2;
                                            }else{
                                                $ors_arr_match[] = false;
                                            }
                                        }
                                    }
                                    else{
                                        $ors_arr_match[] = false;
                                    }
                                    if($except_rule['operator'] == 'and' && !Compare::is($transaction->product_id, $except_rule['rule_value'], $except_rule['comparison_operator'])){
                                        break 2;
                                    }else{
                                        break;
                                    }

                                case "Category":
                                    if($transaction->items()->count() > 0) {
                                        foreach($transaction->items as $i) {

                                            $product = Product::find($i->product_id);

                                            if($product->category) {
                                                $category = $product->category;

                                                //                                            if($category->id == $exception_rule['rule_value']) {
                                                if(Compare::is($category->id, $exception_rule['rule_value'], $except_rule['comparison_operator'])) {
                                                    $exception_id = $exception_rule['id'];

                                                }

                                                if($category->parentCategory) {
                                                    $parent = $category->parentCategory;

                                                    //                                                if($parent->id == $exception_rule['rule_value']) {
                                                    if(Compare::is($parent->id, $exception_rule['rule_value'], $except_rule['comparison_operator'])) {
                                                        $exception_id = $exception_rule['id'];

                                                    }
                                                }
                                            }

                                            if($rule_matched) {

                                                $transaction->category_id = $except_rule['rule_value'];
                                                $arr_except_rule['category'][] = $except_rule['rule_value'];
                                                $except_rule_checked = true;
                                                $except_rule_matched = $exception_rule['id'];
                                                //$arr_matched_rules[] = $except_rule_matched;
                                                if($except_rule['operator'] == 'or' && $and_matched == true){
                                                    $ors_arr_match[] = true;
                                                }
                                                elseif($except_rule['operator'] == 'and'){
                                                    $and_matched = true;
                                                }
                                            }
                                            else{
                                                $except_rule_checked = false;
                                                if($except_rule['operator'] == 'and'){
                                                    $and_matched = false;
                                                    break 2;
                                                }else{
                                                    $ors_arr_match[] = false;
                                                }
                                            }
                                        }
                                    }
                                    if(!empty($transaction->product_id) && !is_null($transaction->product_id)){
                                        $product = Product::find($transaction->product_id);

                                        if($product->category) {
                                            $category = $product->category;

//                                            if($category->id == $exception_rule['rule_value']) {
                                            if(Compare::is($category->id, $exception_rule['rule_value'], $except_rule['comparison_operator'])) {
                                                $exception_id = $exception_rule['id'];

                                            }

                                            if($category->parentCategory) {
                                                $parent = $category->parentCategory;

//                                                if($parent->id == $exception_rule['rule_value']) {
                                                if(Compare::is($parent->id, $exception_rule['rule_value'], $except_rule['comparison_operator'])) {
                                                    $exception_id = $exception_rule['id'];

                                                }
                                            }
                                        }

                                        if($rule_matched) {

                                            $transaction->category_id = $except_rule['rule_value'];
                                            $arr_except_rule['category'][] = $except_rule['rule_value'];
                                            $except_rule_checked = true;
                                            $except_rule_matched = $exception_rule['id'];
                                            //$arr_matched_rules[] = $except_rule_matched;
                                            if($except_rule['operator'] == 'or' && $and_matched == true){
                                                $ors_arr_match[] = true;
                                            }
                                            elseif($except_rule['operator'] == 'and'){
                                                $and_matched = true;
                                            }
                                        }
                                        else{
                                            $except_rule_checked = false;
                                            if($except_rule['operator'] == 'and'){
                                                $and_matched = false;
                                                break 2;
                                            }else{
                                                $ors_arr_match[] = false;
                                            }
                                        }
                                    }
                                    break;
                                case "Brand":
                                    $i = $transaction->product_id;
                                    $product = Product::find($i);
                                    //                                APIController::postSendEmailJson(array($i,$product->brand_id),"Test brand Loyalty");
                                    //                                        if($product->brand_id == $except_rule['rule_value']) {
                                    if($product){
                                        if(Compare::is($product->brand_id, $except_rule['rule_value'], $except_rule['comparison_operator'])) {
                                            $arr_except_rule['brand'][] = $except_rule['rule_value'];
                                            $except_rule_checked = true;
                                            $except_rule_matched = $exception_rule['id'];
                                            //$arr_matched_rules[] = $except_rule_matched;
                                            if($except_rule['operator'] == 'or' && $and_matched == true){
                                                $ors_arr_match[] = true;
                                            }
                                            elseif($except_rule['operator'] == 'and'){
                                                $and_matched = true;
                                            }
                                        }
                                        else{
                                            $except_rule_checked = false;
                                            if($except_rule['operator'] == 'and'){
                                                $and_matched = false;
                                                break 2;
                                            }else{
                                                $ors_arr_match[] = false;
                                            }
                                        }
                                    }

                                    break;
                                case "Currency":
                                    if($transaction->currency_id) {
                                        //$json_data = json_encode(array("trx_id" => $transaction->id ,"in currency arr_except _rule Checked ",$except_rule['rule_value'],$transaction->currency_id));
                                        //                APIController::postSendEmailJson($json_data);
                                        $currency = Currency::where('id', $transaction->currency_id)->first();
                                        //                                    if($currency->id == $except_rule['rule_value']) {
                                        if(Compare::is($currency->id, $except_rule['rule_value'], $except_rule['comparison_operator'])) {

                                            $arr_except_rule['currency'][] = $except_rule['rule_value'];
                                            $except_rule_checked = true;
                                            $except_rule_matched = $exception_rule['id'];
                                            if($except_rule['operator'] == 'or' && $and_matched == true){
                                                $ors_arr_match[] = true;
                                            }
                                            elseif($except_rule['operator'] == 'and'){
                                                $and_matched = true;
                                            }
                                            //                                        APIController::postSendEmailJson(json_encode(array($transaction->ref_number,$arr_except_rule,$except_rule_matched)),"Check Currency 001");

                                        }
                                        else{
                                            $except_rule_checked = false;
                                            if($except_rule['operator'] == 'and'){
                                                $and_matched = false;
                                                break 2;
                                            }else{
                                                $ors_arr_match[] = false;
                                            }
                                        }
                                    }
                                    break;

                                case "Merchant Category Code":
                                    //                    $json_data = json_encode(array("trx_id" => $transaction->id ,"001 except_rule_type"=> $except_rule['type'], "except_rule id"=> $exception_rule['id']));
                                    //                    APIController::postSendEmailJson($json_data);
                                    if(!empty($transaction->mcc) && !is_null($transaction->mcc)){
                                        $mcc_value = DB::table('mcccodes')->where('id',$except_rule['rule_value'])->first();
//                        $json_data = json_encode(array("trx_id" => $transaction->id ,"mcc_value"=> $mcc_value->mcc, $exception_rule['id']));
//                        APIController::postSendEmailJsonMagid($json_data);
                                        //                                    if($transaction->mcc == $mcc_value->mcc) {
                                        if(Compare::is($transaction->mcc, $mcc_value->mcc, $except_rule['comparison_operator'])) {
                                            $arr_except_rule['mcc'][] = $except_rule['rule_value'];
                                            $except_rule_checked = true;
                                            $except_rule_matched = $exception_rule['id'];
                                            //$arr_matched_rules[] = $except_rule_matched;
                                            if($except_rule['operator'] == 'or' && $and_matched == true){
                                                $ors_arr_match[] = true;
                                            }
                                            elseif($except_rule['operator'] == 'and'){
                                                $and_matched = true;
                                            }
                                        }
                                        else{
                                            $except_rule_checked = false;
                                            if($except_rule['operator'] == 'and'){
                                                $and_matched = false;
                                                break 2;
                                            }else{
                                                $ors_arr_match[] = false;
                                            }
                                        }
                                    }
                                    break;
                                case "Transaction Types":
                                    if(!empty($transaction->partner_trxtype) && !is_null($transaction->partner_trxtype)){
                                        $trxtypes_value = DB::table('transaction_types')->where('id',$except_rule['rule_value'])->first();
//                                        $json_data = json_encode(array($transaction->partner_trxtype ,$trxtypes_value->trx_code, $except_rule['rule_value']));
//                        APIController::postSendEmailJsonMagid($json_data);
                                        if(Compare::is($transaction->partner_trxtype, $trxtypes_value->trx_code, $except_rule['comparison_operator'])) {
                                            $arr_except_rule['transaction_type'][] = $except_rule['rule_value'];
                                            $except_rule_checked = true;
                                            $except_rule_matched = $exception_rule['id'];
                                            if($except_rule['operator'] == 'or' && $and_matched == true){
                                                $ors_arr_match[] = true;
                                            }
                                            elseif($except_rule['operator'] == 'and'){
                                                $and_matched = true;
                                            }
                                        }
                                        else{
                                            $except_rule_checked = false;
                                            if($except_rule['operator'] == 'and'){
                                                $and_matched = false;
                                                break 2;
                                            }else{
                                                $ors_arr_match[] = false;
                                            }
                                        }
                                    }
                                    break;
                                case "Amount":
                                    if(!empty($transaction->original_total_amount) && !is_null($transaction->original_total_amount)){
                                        //                                    APIController::postSendEmailJson($exception_rule['currency_id'], "exception rule currency ID");
                                        if(Compare::is($transaction->original_total_amount, $except_rule['rule_value'], $except_rule['comparison_operator']) && Compare::is($transaction->currency_id, $except_rule['rule_value_currency'], 'equal')) {
                                            $arr_except_rule['amount'][] = $except_rule['rule_value'];
                                            $except_rule_checked = true;
                                            $except_rule_matched = $exception_rule['id'];
                                            if($except_rule['operator'] == 'or' && $and_matched == true){
                                                $ors_arr_match[] = true;
                                            }
                                            elseif($except_rule['operator'] == 'and'){
                                                $and_matched = true;
                                            }
                                        }
                                        else{
                                            $except_rule_checked = false;
                                            if($except_rule['operator'] == 'and'){
                                                $and_matched = false;
                                                break 2;
                                            }else{
                                                $ors_arr_match[] = false;
                                            }
                                        }
                                    }
                                    if($except_rule['operator'] == 'and' && !(Compare::is($transaction->original_total_amount, $except_rule['rule_value'], $except_rule['comparison_operator']) && Compare::is($transaction->currency_id, $except_rule['rule_value_currency'], 'equal'))){
                                        break 2;
                                    }else{
                                        break;
                                    }
                            }


                            //                    $json_data = json_encode(array("trx_id" => $transaction->id ,"except_rule_matched"=> $except_rule_matched, 'except_rule_checked' => $except_rule_checked));
                            //                    APIController::postSendEmailJson($json_data,'Exception Rule Matched - ' . $transaction->id);



                        }
                        if(count($ors_arr_match) > 0){
                            if(in_array(true, $ors_arr_match)){
                                $arr_matched_rules[] = $except_rule_matched;
                            }
                        }else{
                            if($except_rule_checked == True ){
                                $arr_matched_rules[] = $except_rule_matched;
                            }
                        }
//                        $json_data = json_encode(array( "ors_arr_match"=>$ors_arr_match));
//                    APIController::postSendEmailJson($json_data,'end of each rule');
                    }
                }
                //            $json_data = json_encode(array("arr_ matched rules element 1",$arr_matched_rules[0]));
                //                APIController::postSendEmailJson($json_data);
//                try{
//                    $json_data = json_encode(array("trx_id" => $transaction->id ,"arr_ matched rules "=>$arr_matched_rules, "arr_except_rule"=> $arr_except_rule));
//                    APIController::postSendEmailJson($json_data, "transaction info 003");
//                } catch (Exception $ex) {
//                    $json_data = json_encode(array($ex->getLine(),$ex->getFile(),$ex->getMessage()));
//                    APIController::postSendEmailJson($json_data,'try catch exception001 ');
//                }

                if(count($arr_matched_rules) >= 1){

                    $ratio = 0;
                    $points_to_reward = 0;
                    foreach($arr_matched_rules as $except_rule_matched){
                        //$exception_id = $except_rule_matched;
                        $exception_rule = $exception_rules[$except_rule_matched];

                        switch($exception_rule['type']) {
                            case "Points":
                                if($exception_rule['reward_pts'] / $exception_rule['reward_usd'] > $ratio){
                                    $reward_pts = $exception_rule['reward_pts'];
                                    $exception_id = $except_rule_matched;
                                    //                                            $reward_usd = $exception_rule['reward_usd'];
                                    $reward_usd = $exception_rule['reward_usd'];
                                    $original_reward = $exception_rule['original_reward'];
                                    $exception_currency_id = $exception_rule['currency_id'];
                                    $reward_tp  = 'Points';
                                    $ratio = $exception_rule['reward_pts'] / $exception_rule['reward_usd'];
                                }
                                break;
                            case "Price":
                                if($exception_rule['reward_pts'] > $points_to_reward){
                                    $reward_pts = $exception_rule['reward_pts'];
                                    $original_reward = $exception_rule['original_reward'];
                                    $points_to_reward = $reward_pts;
                                    $exception_id = $except_rule_matched;
                                    $reward_tp  = 'Price';
                                }
                                break;
                            case "Event":
                                $reward_pts = $exception_rule['reward_pts'];
                                $reward_num_events = $exception_rule['original_reward'];
                                $reward_tp  = 'Event';
                                $exception_id = $except_rule_matched;
                                break;
                        }

                        $rule_matched = true;
                    }
                }
                if($rule_matched === TRUE) {

                    //                APIController::postSendEmailJson("went through" , "transaction - " . $reward_tp . " - " .$transaction->ref_number );
                    if($type == 'bonus_on_registration'){
                        if($exception_id != 0){
                            $transaction->loyalty_exception_id = $exception_id;
                        }
                        $transaction->rule_id         = (int)$program_id;
                        return $transaction;
                    }

                    if($reward_tp == 'Points') {
                        //                    APIController::postSendEmailJson(json_encode(array($exception_currency_id,$transaction->currency_id, $transaction->partner_currency_id)),"Points Testing 001 - " . $transaction->ref_number);
                        $currency_id = $transaction->currency_id;
                        $amountInNetworkCurrency = $transaction->original_total_amount;
                        if($use_partner_currency === TRUE){
                            $currency_id = $transaction->partner_currency_id;
                            $amountInNetworkCurrency = $transaction->partner_amount;
                        }

                        if(isset($exception_currency_id) && $exception_currency_id == $currency_id){
                            $transaction->points_rewarded = round(($amountInNetworkCurrency / $original_reward) *  $reward_pts);
                            $couponPoints_rewarded= round(($couponPrice_usd / $reward_usd) *  $reward_pts);
                            $transaction->points_rewarded = $transaction->points_rewarded - $couponPoints_rewarded;
                        }
                        elseif(isset($exception_currency_id)){
                            $ObjAmountCurrency = Currency::find($currency_id);
                            $amountInNetworkCurrencyUSD = CurrencyHelper::convertToUSD($amountInNetworkCurrency,$ObjAmountCurrency->short_code);
                            $exception_currency = Currency::find($exception_currency_id);
                            $amountInRewardCurrency = CurrencyHelper::convertFromUSD($amountInNetworkCurrencyUSD['amount'], $exception_currency->short_code);
                            //                            $arr_test = array("amountInNetworkCurrencyUSD"=>$amountInNetworkCurrencyUSD,
                            //                                "amountInNetworkCurrency"=>$amountInNetworkCurrency,
                            //                                "networkCurrency"=>$networkCurrency->short_code,
                            //                                "amountInRewardCurrency"=>$amountInRewardCurrency,
                            //                                "exception_currencyshort_code"=>$exception_currency->short_code
                            //                                );
                            //                            $json_data = json_encode($arr_test);
                            //                            APIController::postSendEmailJson($json_data, "Exception Currency ID");
                            $transaction->points_rewarded = round(($amountInRewardCurrency['amount'] / $original_reward) *  $reward_pts);
                            $couponPoints_rewarded= round(($couponPrice_usd / $reward_usd) *  $reward_pts);
                            $transaction->points_rewarded = $transaction->points_rewarded - $couponPoints_rewarded;
                        }
                        else{
                            $transaction->points_rewarded = round(($amountInNetworkCurrency / $original_reward) *  $reward_pts);
                            $couponPoints_rewarded= round(($couponPrice_usd / $reward_usd) *  $reward_pts);
                            $transaction->points_rewarded = $transaction->points_rewarded - $couponPoints_rewarded;
                        }



                        //                    $transaction->points_rewarded = round(($transaction->total_amount / $reward_usd) *  $reward_pts);
                        //                    $arr_test = array(
                        //                                "trx_ref" => $transaction->ref_number,
                        //                                "amountInNetworkCurrency"=>$amountInNetworkCurrency,
                        //                                "networkCurrency"=>$networkCurrency->short_code,
                        //                                "networkcurrencyid"=>$programCurrencyId,
                        //                                "transactioncurrency_id"=>$transaction->currency_id
                        //                                );
                        //                    $json_data = json_encode($arr_test);
                        //                    APIController::postSendEmailJson($json_data, "Exception Points 2"  . $transaction->ref_number);
                    }
                    elseif($reward_tp == 'Event'){
                        $transaction->points_rewarded = round(($transaction->original_total_amount / $reward_num_events) *  $reward_pts);
                        $transaction->total_amount = 0;
                        $transaction->event_total = $transaction->original_total_amount;
                        $transaction->original_total_amount = 0;
                        $transaction->partner_amount = 0;
                    }
                    else {
                        $transaction->points_rewarded = $reward_pts;
                    }

                    $transaction->points_balance  = $user->balance($transaction->network_id) + $transaction->points_rewarded;
                    if($exception_id != 0){
                        $transaction->loyalty_exception_id = $exception_id;
                    }
                    $transaction->rule_id         = (int)$program_id;

                }
                else {
                    if($use_partner_currency=== TRUE){
                        $currency_id = $transaction->partner_currency_id;
                        $amountInNetworkCurrency= $transaction->partner_amount;
                    }
                    else{
                        $currency_id = $transaction->currency_id;
                        $amountInNetworkCurrency= $transaction->original_total_amount;
                    }
                    //                $arr_exception = array('program'=>$program_->_id,"currencyOfTheAmount"=>$transaction->currency_id, 'program currency'=>$program->currency_id, 'reward_usd'=>$program->reward_usd ,'transaction_ref'=>$transaction->ref_number);
                    //                $json_d = json_encode($arr_exception);
                    //                APIController::postSendEmailJson($json_d, "Exception Matched 4");
                    if($currency_id != $program['currency_id']){
                        //convert the amount to the rule currency

                        $amountCurrency   = Currency::where('id', $currency_id)->first();
                        $programCurrency   = Currency::where('id', $program['currency_id'])->first();
                        $programCurrencyRate   = $programCurrency->latestRate();
                        $amountCurrencyRate   = $amountCurrency->latestRate();
                        //                    APIController::postSendEmailJson(json_encode(array($transaction->ref_number,$amountInNetworkCurrency,$amountCurrency->id,$amountCurrencyRate,$programCurrency->id,$programCurrencyRate)),'different currency than network');
                        $amountInNetworkCurrency = ($amountInNetworkCurrency / $amountCurrencyRate) * $programCurrencyRate;
                    }
                    //                $originalAmountInPoints	= PointsHelper::amountToPoints($transaction->original_total_amount, $transaction->currency_id, $transaction->network_id);
                    //                $transaction->points_rewarded = round(($transaction->total_amount / $program['reward_usd']) *  $program['reward_pts']);
                    //                APIController::postSendEmailJson(json_encode(array($transaction->ref_number,$amountInNetworkCurrency,$currency_id,$program['currency_id'])),'default rule 001');

                    $transaction->points_rewarded = round(($amountInNetworkCurrency / $program['original_reward']) *  $program['reward_pts']);
                    $couponPoints_rewarded = round(($couponPrice_usd / $program['reward_usd']) *  $program['reward_pts']);
                    $transaction->points_rewarded = $transaction->points_rewarded - $couponPoints_rewarded;
                    $transaction->points_balance  = $user->balance($transaction->network_id) + $transaction->points_rewarded;

                    if($exception_id != 0){
                        $transaction->loyalty_exception_id = $exception_id;
                    }
                    $transaction->rule_id         = (int)$program_id;
                }
            }
        } catch (Exception $ex) {
            $json_data = json_encode(array($ex->getMessage(),$ex->getLine()));
            APIController::postSendEmailJson($json_data, "try catch exception 0004 - " . $transaction->ref_number);
        }

        return $transaction;
    }

    public static function doRewardNewLoyalty(array $params, $trans_class = "Items", $loyaltyByPartner)
    {
        $validator = Validator::make(
            $params, array(
                'user_id' => 'required|integer'
            )
        );

        if($validator->fails()) {
            return array( $params['reference'] . "Validation failed, ensure that all required fields are present");
        }

        DB::beginTransaction();

        $transaction = new Transaction();
        $transaction->trx_reward  = 1;
        $transaction->trx_redeem  = 0;
        $transaction->trans_class = $trans_class;

        $user = User::where('id', $params['user_id'])->first();

        if(!$user) {
            return array("Transaction aborted: User '{$params['user_id']}' does not exist.");
        }

        // only active and suspended users can be rewarded FS427 <fboukarroum>
//        if(!$ignoreUserStatus && $user->status != 'active' && $user->status != 'suspended' && $user->status != 'inactive') {
//            return array( $params['reference'] . "Transaction aborted: User account is " . $user->status);
//        }
        // Fetch partner data

        if(!isset($params['partner'])) {
            $params['partner'] = $user->getTopLevelPartner();
        } else {
            $params['partner'] = Partner::find($params['partner']);
        }


        if(empty($params['network_id'])){
            $params['network'] = $params['partner']->firstNetwork();
        }
        else{
            $params['network'] = Network::find($params['network_id']);
        }
        if($params['network']->status != 'Active') {
            return array($params['reference'] . "Transaction aborted: Network '{$params['network']->name}' is not currently active.");
        }
        if(isset($params['created_at'])){
            $transaction->created_at = $params['created_at'];
        }
        if(isset($params['updated_at'])){
            $transaction->updated_at = $params['updated_at'];
        }
        if(isset($params['posted_at'])){
            $transaction->posted_at = $params['posted_at'];
        }
        else{
            $transaction->posted_at = date('Y-m-d H:i:s');
        }
        if(isset($params['country_id'])){
            $transaction->country_id = $params['country_id'];
        }else{
            $transaction->country_id      = $user->country_id;
        }
//        APIController::postSendEmailJson(json_encode(array($transaction->country_id,$params['country_id'])),"test 0001".array_get($params, 'reference'));
        $transaction->quantity        = 0;
        $transaction->trans_class     = $trans_class;
        $transaction->user_id         = array_get($params, 'user_id');
        $transaction->partner_id      = $params['partner']->id;
        $transaction->source          = array_get($params, 'source');
        $transaction->ref_number      = array_get($params, 'reference');
        $transaction->notes           = array_get($params, 'notes');
        $transaction->store_id        = array_get($params, 'store_id');
        $transaction->pos_id          = array_get($params, 'pos_id');
        $transaction->auth_staff_id   = array_get($params, 'staff_id');
        $transaction->partner_trxtype   = array_get($params, 'partner_trxtype');
        $transaction->points_rewarded = 0;
        $transaction->rule_id         = null;
        $transaction->network_id      = $params['network']->id;
        $transaction->points_balance  = $user->balance($transaction->network_id);
        if(isset($params['mcc'])){
            $transaction->mcc = $params['mcc'];
        }
        if(isset($params['product_id'])){
            $transaction->product_id = $params['product_id'];
        }
        //$transaction->save();

        // Amount only reward transaction
        if(isset($params['amount'])) {
            if(isset($params['currency'])){
                $params['currency'] = Currency::where('short_code', array_get($params, 'currency'))->first();
            }
            if(isset($params['partner_currency'])){
                $partner_currency = Currency::where('short_code',$params['partner_currency'])->first();
            }
            if(!isset($params['currency']) || !is_object($params['currency'])) {
                return array($params['reference'] . "Transaction aborted: Currency chosen does not exist.");
            }
            if(!isset($partner_currency) || !is_object($partner_currency)) {
                return array($params['reference'] . "Transaction aborted: Partner Currency chosen does not exist.");
            }
            if(isset($params['use_partner_currency']) && $params['use_partner_currency'] == TRUE){
                $transaction->currency_id   = $params['currency']->id;
                $transaction->partner_currency_id   = $partner_currency->id;
                $transaction->exchange_rate = $params['currency']->latestPrice()->rate;
                $transaction->total_amount  = (float)(array_get($params, 'original_transaction_amount') / $transaction->exchange_rate);
                $transaction->original_total_amount  = floatval(array_get($params, 'original_transaction_amount'));
                $transaction->partner_amount  = floatval(array_get($params, 'amount'));
            }
            elseif(isset($params['original_transaction_amount'])){
                $transaction->currency_id   = $params['currency']->id;
                $transaction->partner_currency_id   = $params['currency']->id;
                $transaction->exchange_rate = $params['currency']->latestPrice()->rate;
                $transaction->total_amount  = (float)array_get($params, 'amount');
                $transaction->original_total_amount  = floatval(array_get($params, 'original_transaction_amount'));
                $transaction->partner_amount  = floatval(array_get($params, 'original_transaction_amount'));
            }
            else{
                $transaction->currency_id   = $params['currency']->id;
                $transaction->partner_currency_id   = $params['currency']->id;
                $transaction->exchange_rate = $params['currency']->latestPrice()->rate;
                $transaction->total_amount  = (float)(array_get($params, 'amount') / $transaction->exchange_rate);
                $transaction->original_total_amount  = floatval(array_get($params, 'amount'));
                $transaction->partner_amount  = floatval(array_get($params, 'amount'));
            }

        }

        if(isset($params['points']) && !isset($params['amount'])){
            $params['currency'] = Currency::where('short_code', 'USD')->first();
            $transaction->currency_id   = $params['currency']->id;
            $transaction->partner_currency_id   = $params['currency']->id;
            $transaction->exchange_rate = $params['currency']->latestPrice()->rate;

            $transaction->total_amount = PointsHelper::pointsToAmount($params['points'], 'USD', $params['network']->id);
            $transaction->points_rewarded = round($params['points']);
        }

        { // if transfer set total amount to zero
            if( isset($params['type']) && $params['type'] == 'transfer'){
                $transaction->total_amount    = 0;
            }
        }

        { // if bonus set total amount to zero
            if( isset($params['type']) && ($params['type'] == 'bonus' || $params['type'] == 'bonus_on_registration')){
                $transaction->total_amount				= 0;
                $transaction->original_total_amount		= 0;
                $transaction->partner_amount                = 0;
                $transaction->points_rewarded			= $params['points'];

            }
        }

        // Process the loyalty programs against this transaction for amount or items, not points
        $bonus_pts_on_registration = 'reward';
        if(isset($params['type']) && $params['type'] == 'bonus_on_registration'){
            $bonus_pts_on_registration = 'bonus_on_registration';
        }
        if(!isset($params['points'])) {
            if($bonus_pts_on_registration !== 'bonus_on_registration'){
                try{
                    if(isset($params['use_partner_currency']) && $params['use_partner_currency'] == True){
                        $transaction = self::rewardByTransaction($loyaltyByPartner, $transaction, $bonus_pts_on_registration,True);
                    }
                    else{
                        $transaction = self::rewardByTransaction($loyaltyByPartner, $transaction, $bonus_pts_on_registration);
                    }

                } catch (Exception $ex) {
                    $resp = array("dorewardnew Process Matching Reward Exception Rule",$ex);
                    $json_data = json_encode($resp);
                    //APIController::postSendEmailJson($json_data);
                }

            }
        }
        //APIController::postSendEmailJson(json_encode($transaction->toArray()));
        // Set the transaction location

        if($transaction->store_id){

            $params['store'] = Store::find($transaction->store_id);

            if($params['store'] && !isset($params['country_id'])) {
                if($params['store']->address){
                    $transaction->country_id = $params['store']->address->country_id;
                } else {
                    $transaction->country_id = $user->country_id;
                }
            }
        }

        // Assign staff member

        if($transaction->auth_staff_id) {
            $staff_member = self::validStaffMember($transaction->auth_staff_id);

            if($staff_member) {
                $transaction->auth_staff_id = $staff_member->id;
                $transaction->auth_role_id  = $staff_member->roles()->first()->id;
            }
        }

        //get network currency
        $networkCurrencyId  = $params['network']->currency_id;
        $networkCurrency  = Currency::where('id', $networkCurrencyId)->first();
        $networkCurrencyShortCode  = $networkCurrency->short_code;

        $transaction->amt_reward = PointsHelper::pointsToAmount($transaction->points_rewarded, $networkCurrencyShortCode, $params['network']->id);

        $transaction->save();

        // Update User Balance

        $user->updateBalance($transaction->points_rewarded, $params['network']->id, 'add');
        DB::commit();
        // Send notifications
        $outputRalph = '';
        if($transaction->points_rewarded > 0) {

///removed for FNB testing////////////////////////////////////////////////////////////////////////////////////////////////////
            //$outputRalph = self::sendNotifications($user, $transaction, Transaction::REWARD_TRANS);
        }

        return array($outputRalph, $transaction->toArray());
    }


} // EOC