<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

/**
 * Card Security Helper
 *
 * @category   Helper Classes
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class CardSecurity {

    /**
     * Create a new unique card number
     *
     * @return int
     */
    public static function newNumber() {
        $max_number = self::getMaxCardNumber();

        return $max_number + 1;
    }

    /**
     * Determine what the last used number is in the cards table
     *
     * @return int
     */
    public static function getMaxCardNumber() {
        $constraint_min = 100000000;
        $constraint_max = 999999999;

        //$max_number = DB::table('card')->whereRaw('number regexp ?', array('^(0|[1-9][0-9]*)$'))->max('number');
        $cards = DB::table('card')->select('number')->where('number', 'not like', '%-%')->get();

        foreach ($cards as $card) {
            $numbers[] = $card->number;
        }
        if (!empty($numbers)) {
            $max_number = max($numbers);
        } else {
            $max_number = 0;
        }

        if (!$max_number) {
            $max_number = $constraint_min;
        }

        if ($max_number >= $constraint_max) {
            Log::warning('Max range of card number slots have been processed.');
        }

        return $max_number;
    }

}

// EOC
