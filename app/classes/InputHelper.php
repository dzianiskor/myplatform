<?php

use Illuminate\Support\Facades\Input;

/**
 * Input Helper
 * 
 * @category   Helper Classes
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class InputHelper 
{
    /**
     * unset null inputs
     *
     * @return int
     */
    public static function removeNulls()
    {
        $inputs = Input::all();
        
        foreach ($inputs as $k=>$v) {
            if ($inputs[$k] == 'NULL') {
                unset($inputs[$k]);
            }
        }
        return $inputs;
    }

    /**
     * Remove any extensional formatting from a mobile number
     *
	 * @param int $number
     * @return string
     */
	public static function sanitizeMobile($number)
	{
		$number = (string)$number;
		
		if(empty($number)) {
			return $number;
		}
		
		if($number[0] == '0') {
			$number = ltrim($number, '0');
		}		
		
		return (int)$number;
	}

    /**
     * Ensure that we have a correctly formatted country telephone code
     *
     * @param string/number $code
     * @return string
     */
    public static function sanitizeTelephoneCode($code)
    {
        $code = (string)trim($code);
        
        if(empty($code)) {
            return $code;
        }
        
        if($code[0] != '+') {
            $code = "+$code";
        }       
        
        return $code;
    }    
	
    /**
     * Normalize a mobile number according to country code
     *
	 * @param int $number
	 * @param string $country_code
     * @return string
     */
	public static function normalizeMobileNumber($number, $country_code)
	{
		$number = self::sanitizeMobile($number);
		
		if($country_code[0] == '+') {
			$country_code = ltrim($country_code, '+');
		}
		
		return "+{$country_code}{$number}";		
	}

    /**
     * Format jQuery dates
     *
     * @return int
     */
    public static function convertDate($date)
    {
        return $date;
    }

    /**
     * Return a default value if the implied value is empty
     *
	 * @param mixed $v
	 * @param mixed $default
     * @return mixed
     */
    public static function withDefault($v, $default = null)
    {
        if(empty($v) || !$v){
            return $default;
        }
    }

    /**
     * Determine if the input is most likely an email
     *
	 * @param string $input
     * @return bool
     */
	public static function isEmail($input)
	{
		if(strstr($input, '@') !== false) {
			return true;
		} else {
			return false;
		}
	}
    
    /**
     * Determine if the input is most likely an Phone number
     *
	 * @param string $input
     * @return bool
     */
	public static function isPhone($input)
	{
            $arr_country_codes = array(
                "1","7","20","27","30","31","32","33","34","36","39","40","41","43","44","45","46","47","48","49","51","52","53","54","55","56","57","58","60","61","62","63","64","65","66","81","82","84","86","90","91","92","93","94","95","98","211","212","213","216","218","220","221","222","223","224","225","226","227","228","229","230","231","232","233","234","235","236","237","238","239","240","241","242","243","244","245","246","248","249","250","251","252","253","254","255","256","257","258","260","261","262","263","264","265","266","267","268","269","290","291","297","298","299","350","351","352","353","354","355","356","357","358","359","370","371","372","373","374","375","376","377","378","379","380","381","382","383","385","386","387","389","420","421","423","500","501","502","503","504","505","506","507","508","509","590","591","592","593","595","597","598","599","670","672","673","674","675","676","677","678","679","680","681","682","683","685","686","687","688","689","690","691","692","850","852","853","855","856","880","886","960","961","962","963","964","965","966","967","968","970","971","972","973","974","975","976","977","992","993","994","995","996","998"
            );
            $res = '';
            foreach($arr_country_codes as $countc){
                if(strrpos($input, $countc, -strlen($input)) !== FALSE && strrpos($input, $countc, -strlen($input)) == 0){
                    $res = $countc;
                    break;
                    //return $countc;
                }
                else{
                    $res = FALSE;
                }
            }
            return $res;
	}
	
    /**
     * Determine if the input is most likely a reference code
     *
	 * @param string $input
     * @return bool
     */	
	public static function isReference($input)
	{
		if(preg_match('/^[a-zA-Z]+[a-zA-Z0-9\-]+$/', $input) || ctype_alnum($input)) {
			return true;
		} else {
			return false;
		}
	}

} // EOC