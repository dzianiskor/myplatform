<?php

namespace App\Classes;

class ReportExportHelper
{
    public static function makeSuccessReportFile($data, $baseFileName, $folder)
    {
        $fileName = mb_strtolower($baseFileName . '.txt');
        $filePath = PathHelper::outgoing($folder) . '/' . $fileName;

        if (!$file = fopen($filePath, "w"))
            throw new \RuntimeException(__METHOD__ . '(). Cannot open file ' . $filePath);

        $imploded_data = [];
        foreach ($data as $key => $item) {
            if ($key == 0) {
                $imploded_data[] = implode("|", array_keys($item));
            }
            if (is_array($item)) {
                $imploded_data[] = implode("|", array_values($item));
            } else {
                if ($item == 'EOF') {
                    $imploded_data[] = "EOF";
                }
            }
        }

        $imploded_data_end = implode("\r\n", $imploded_data);
        fwrite($file, $imploded_data_end);
        fclose($file);

        return $filePath;
    }
}