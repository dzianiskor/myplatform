<?php
/**
 * Card Security Helper
 *
 * @category   Helper Classes
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class CSVHelper
{
    /**
     * Create a new unique card number
     *
     * @return int
     */
    public static function writeToFile($arr_csv,$file)
    {
        $openfile = fopen($file,"w");

        foreach ($arr_csv as $line){
            fputcsv($openfile,$line);
        }

        fclose($openfile);
        return 1;
    }

} // EOC