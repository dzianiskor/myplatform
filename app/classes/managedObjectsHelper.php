<?php
use Illuminate\Support\Facades\Auth;
/*
    NOTE: This class will be made obsolete in the near future
 */
class ManagedObjectsHelper 
{
    /**
     * Return the managed partner collection
     * 
     * @return Collection
     */
    public static function managedPartners()
    {
        return Auth::User()->managedPartners();
    }
    
    /**
     * Return the managed networks collection
     * 
     * @return Collection
     */
    public static function managedNetworks()
    {
        return Auth::User()->managedNetworks();
    }

    /**
     * Return the managed segments collection
     * 
     * @return Collection
     */
    public static function managedSegments()
    {
        return Auth::User()->managedSegments();
    }

    /**
     * Return the managed items(products) collection
     * 
     * @return Collection
     */
    public static function managedItems()
    {
        return Auth::User()->managedItems();
    }

    /**
     * Return the managed stores collection
     * 
     * @return Collection
     */
    public static function managedStores()
    {
        return Auth::User()->managedStores();
    }

    /**
     * Return the managed tickets collection
     * 
     * @return Collection
     */
    public static function managedTickets()
    {
        return Auth::User()->managedTickets();
    }

    /**
     * Return the managed batches collection
     * 
     * @return Collection
     */
    public static function managedBatches()
    {
        return Auth::User()->managedBatches();
    }   

    /**
     * Return the managed coupons collection
     * 
     * @return Collection
     */
    public static function managedCoupons()
    {
        return Auth::User()->managedCoupons();
    }        

    /**
     * Return the managed members collection
     * 
     * @return Collection
     */
    public static function managedMembers()
    {
        return Auth::User()->managedMembers();
    }

    /**
     * Return the managed roles collection
     * 
     * @return Collection
     */
    public static function managedRoles()
    {
        return Auth::User()->managedRoles();
    }

    /**
     * Return the managed loyalty collection
     * 
     * @return Collection
     */
    public static function managedLoyalty()
    {
        return Auth::User()->managedLoyalty();
    }

    /**
     * Return the managed notifications collection
     * 
     * @return Collection
     */
    public static function managedNotifications()
    {
        return Auth::User()->managedNotifications();
    }

    /**
     * Return the managed POS collection
     * 
     * @return Collection
     */
    public static function managedPointOfSales()
    {
        return Auth::User()->managedPointOfSales();
    }

    /**
     * Return the managed banner collection
     * 
     * @return Collection
     */
    public static function managedBanners()
    {
        return Auth::User()->managedBanners();
    }
    /**
     * Return the managed Cardprint collection
     * 
     * @return Collection
     */
    public static function managedCardprint()
    {
        return Auth::User()->managedCardprint();
    }
    
    /**
     * Return the managed Affiliates collection
     * 
     * @return Collection
     */
    public static function managedAffiliates()
    {
        return Auth::User()->managedAffiliates();
    }
    /**
     * Return the managed Affiliates collection
     * 
     * @return Collection
     */
    public static function managedAffiliatePrograms()
    {
        return Auth::User()->managedAffiliatePrograms();
    }

} // EOC