<?php
use App\Category;
use App\CategoryPartner as CategoryPartner;

/**
 * Category Helper
 *
 * @category   Helper Classes
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class CategoryHelper
{
	/**
	 * Render a category hierarchy
	 *
	 * @param int $start
	 * @return array
	 */
	public static function hierarcy($start = null)
	{
        $categories = array();

        self::_list($start, $categories);

        return $categories;
	}

	/**
	 * Loop through categories hierarchically
	 *
	 * @param int $parent_id
	 * @param array &$return
	 * @return void
	 */
	private static function _list($parent_id = null, &$return)
	{
        $categories = Category::where('draft', false);
        $categories->whereNotIn('id', array(1352));
        $categories->where('parent_category_id', $parent_id);
        $categories->orderBy('name');

        foreach($categories->get() as $c) {
			$category       = new \stdClass();
			$category->id   = $c->id;
			$category->name = $c->name;

			self::_list($c->id, $category->children);

            $return[] = $category;
        }
	}

	public static function hierarcy1($partner_id,$start = null) {
        $categories = array();

        self::_list1($start,$partner_id, $categories);

        return $categories;
	}

	private static function _list1($parent_id = null,$partner_id, &$return) {
		$category_partner_ids = CategoryPartner::select('category_id')->where('partner_id',$partner_id)->get();
		$arr_category_partner_ids = array();//$category_partner_ids->toArray();
		foreach($category_partner_ids as $id){
			$arr_category_partner_ids[]  = $id->category_id;
		}


		$categories = Category::where('draft', false);
		$categories->whereNotIn('id', array(1352));
		$categories->where('parent_category_id', $parent_id);
		$categories->whereIn('id',$arr_category_partner_ids);
		$categories->orderBy('name');


		foreach($categories->get() as $c) {
			$category       = new stdClass();
			$category->id   = $c->id;
			$category->name = $c->name;
			self::_list1($c->id,$partner_id, $category->children);
			$return[] = $category;
		}
	}

} // EOC