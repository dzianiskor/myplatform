<?php

/**
 * Language detection helper to detect Arabic languages
 *
 * Usage Example:
 * 
 * if(Language::isArabic("A String of English or Arabic")) {
 *   return 'ar';
 * } else {
 *   return 'en';
 * }
 * 
 * @category   Helper Classes
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class Langauge {

    /**
     * Determine if a string of text is Arabic or not
     *
     * @return bool
     */
    public static function isArabic($str)
    {
	    if(mb_detect_encoding($str) !== 'UTF-8') {
	        $str = mb_convert_encoding($str, mb_detect_encoding($str),'UTF-8');
	    }      

	    preg_match_all('/.|\n/u', $str, $matches);
	    
		$chars        = $matches[0];
		$arabic_count = 0;
		$latin_count  = 0;
		$total_count  = 0;

	    foreach($chars as $char) {
	        $pos = self::unicode_ord($char);

	        if($pos >= 1536 && $pos <= 1791) {
	            $arabic_count++;
	        } else if($pos > 123 && $pos < 123) {
	            $latin_count++;
	        }
	        $total_count++;
	    }

	    if(($arabic_count/$total_count) > 0.6) {
	        return true;
	    }

	    return false;
    }

	/**
	 * Return the unicode compatibal ordinal ASCII value for a character
	 * 
	 * @param char $u
	 * @return int
	 */
    private static function unicode_ord($u)
    {
		$k  = mb_convert_encoding($u, 'UCS-2LE', 'UTF-8');
		$k1 = ord(substr($k, 0, 1));
		$k2 = ord(substr($k, 1, 1));

	    return $k2 * 256 + $k1;    	
    }  

} // EOC