<?php

/**
 * Kalaam SMS gateway integration
 *
 * Usage Example:
 * 
 * $sms = new Kalaam('username', 'pin', 'sender ID');
 * $sms->send('number', 'message');
 * 
 * @category   Helper Classes
 * @package    BLU
 * @copyright  2017 BLU
 * @link       http://blupoints.com
 */

class Kalaam {
    
    private $endpoint = 'http://api.smscountry.com/SMSCwebservice_bulk.aspx';
    public $user;
    public $password;
    public $senderid;
    public $msgtype;
    
    /**
    * Create the new instance
    * 
    * @param
    * @return void
    */
    public function __construct($user, $password, $senderid, $msgtype) {
        $senderid = rawurlencode($senderid);
        $msgtype = rawurlencode($msgtype);
        $this->user = $user;
        $this->password = $password;
        $this->senderid = $senderid;
        $this->msgtype = $msgtype;
    }

    /**
    * Perform the send request 
    *
    * @param int $number
    * @param string $message
    * @return string
    */
    public function send($number, $message) {
        $ch = curl_init();
        // URL encode message body
        $messageBody = urlencode($message);
        $url = "User=".$this->user."&passwd=".$this->password."&mobilenumber=".$number."&message=".$messageBody."&sid=".$this->senderid."&mtype=".$this->msgtype."&DR=Y";
        $ret = curl_setopt($ch, CURLOPT_URL, $this->endpoint);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $url);
        $ret = curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
	curl_close($ch);
	return $output;
    }
} // EOC

