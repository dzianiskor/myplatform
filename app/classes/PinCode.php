<?php

/**
 * PinCode Helper
 * 
 * @category   Helper Classes
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class PinCode 
{
    /**
     * Create a new randomized 4 digit pin code
     *
     * @return int
     */
    public static function newCode()
    {
    	// @TODO: Do we need additional security here?
    	
        return mt_rand(1000, 9999);
    }

} // EOC