<?php

use App\Http\Controllers\APIController;

/**
 * A2A SMS gateway integration
 *
 * Usage Example:
 * 
 * 
 * @category   Helper Classes
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */

require_once('nusoap/lib/nusoap.php');

class A2A
{
	private $endpoint = 'http://212.35.66.66:8888/wsSendSMS/Service.asmx?wsdl';
        public $bankCode;
        public $password;

	/**
	 * Create the new instance
	 * 
	 * @param
	 * @return void
	 */
	public function __construct($bankCode, $password)
	{
            $this->bankCode = $bankCode;
            $this->password = $password;
	}

	/**
	 * Perform the send request 
	 *
	 * @param int $number
	 * @param string $message
	 * @return string
	 */
	public function send($number, $message)
	{
            try{
                //$client = new SOAPClient($this->endpoint);
                $client = new nusoap_client($this->endpoint, true);
                $params = array(
                    'BankCode' => $this->bankCode,
                    'BankPWD' => $this->password,
                    'MobileNo' => $number,
                    'MsgText' => $message,
                    'SenderID' => 'AJIB'
                );
                //$result = $client->SendSMSMessage($params);
                $result = $client->call('SendSMSMessage', $params);
                return $result;
            } catch (Exception $ex) {
                $json_data = json_encode(array($ex->getMessage(), $ex->getLine()));
                APIController::postSendEmailJson($json_data, "LIVE A2A Class 001");
            }
            
	}	

} // EOC