<?php
/**
 * Banner Helper
 * 
 * @category   Helper Classes
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class BannerHelper 
{
    /**
     * Return an array of supported banner pages
     *
     * @return array
     */
    public static function availablePages()
    {
		return array(
			'home', 
			'earn_points', 
			'spend_points', 
			'hot_deals', 
			'blu_travel'
		);
    }

    /**
     * Return a friendly name for the specified page slug on the website
     *
     * @param string $page_slug
     * @return string
     */
	public static function pageName($page_slug)
	{
		switch($page_slug) {
			case "home":
				return "Home";
			case "earn_points":
				return "Earn Points";
			case "spend_points":
				return "Spend Points";
			case "hot_deals":
				return "Hot Deals";
			case "blu_travel";
				return "BLU Travel";
			default:
				return "Unknown Page";
		} 
	}

} // EOC