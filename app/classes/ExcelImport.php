<?php
/**
 * Excel Import Helper
 *
 * @category   Helper Classes
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class ExcelImport
{
    /**
     * Return sanitized values for their string equivalents when importing XLS
     *
     * @param string $value
     * @return string
     */
    protected static function getValue($value)
    {
        if (!$value || $value == 'NULL') {
            $value = '';
        }

        return $value;
    }

    /**
     * Perform the data import
     *
     * @param ExcelObject $excel
     * @return void
     */
    public static function import($excel)
    {
        // Import all users
        UserImport::Import($excel->getSheetByName('Users'));

        // Import all partner entities
        PartnerImport::Import($excel->getSheetByName('Entities'));

        // Import all stores
        StoreImport::Import($excel->getSheetByName('Stores'));

        // Import all transactions
        TransactionImport::Import($excel->getSheetByName('Transactions'));

        // Import all products
        ProductImport::Import($excel->getSheetByName('Products'));
    }

} // EOC