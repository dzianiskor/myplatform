<?php

namespace App\Classes;

class ReportImportHelper
{
    public static function makeErrorReportFile($errorList, $baseFileName, $folder)
    {
        $fileName = mb_strtolower($baseFileName . '.csv');
        $filePath = PathHelper::incoming($folder) . '/' . $fileName;

        /*if (!file_exists($fileFolder))
            if (!File::makeDirectory($fileFolder, 0775, true))
                throw new \RuntimeException(__METHOD__ . '(). Cannot create folder path ' . $fileFolder);*/

        if (!$file = fopen($filePath, "w"))
            throw new \RuntimeException(__METHOD__ . '(). Cannot open file ' . $filePath);

        foreach ($errorList as $error)
            fwrite($file, implode(',', $error) . "\r\n");

        fclose($file);

        return $filePath;
    }
}