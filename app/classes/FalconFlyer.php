<?php

use App\Classes\PathHelper;
use App\Transaction;

/**
 * Falcon Flyer file Generation
 *
 * Usage Example:
 * 
 * $falconFlyer = new FalconFlyer('filename', 'partner_header', 'points_value_of_mile', 'footer_identifier', 'currency_code');
 * $falconFlyer->generateFile('partner_id');
 * 
 * @category   Helper Classes
 * @package    BLU
 * @author     Ralph Nader <ralph@bluloyalty.com>
 * @copyright  2016 BLU
 * @link       http://blupoints.com
 */
class FalconFlyer 
{

	public $filename;
	public $partner_header;
	public $point_value_of_mile;
	public $footer_identifier;
	public $currency_code;


	/**
	 * Create the new instance
	 * 
	 * @param
	 * @return void
	 */
	public function __construct($filename, $partner_header, $point_value_of_mile, $footer_identifier, $currency_code)
	{
		$this->filename                 = $filename;
		$this->partner_header           = $partner_header;
		$this->point_value_of_mile      = $point_value_of_mile;
		$this->footer_identifier        = $footer_identifier;
		$this->currency_code            = $currency_code;
	}

	/**
	 * Perform the generateFile request 
	 *
	 * @param int $partner
	 * @param string $filename
	 * @return string
	 */
	public function generateFile($partner)
	{
            $arr_lines= array();
            $arr_lines[] = $this->partner_header;
            $transactions = Transaction::where('partner_id',$partner)->get();
            foreach($transactions as $trx){
                $decoded_json = json_decode($trx->notes);
                
                $privilege_num = $decoded_json->privileged_number;
                $unhandled_date = explode(" ", $trx->created_at);
                $trx_date = str_replace('-',"",$unhandled_date[0]);
                $class_number = $decoded_json->class_number;
                $first_initial = $decoded_json->first_initial;
                $last_name = $decoded_json->last_name;
                $base_miles_issued = $decoded_json->base_miles_issued;
                
                
                $str_line = $privilege_num . ","  . $class_number . "," . $trx_date . "," . $base_miles_issued . "," . $first_initial . "," . $last_name;
                $arr_lines[] = $str_line;
            }
            
            $arr_lines[] = $this->footer_identifier;
            $imploded_arr_lines = implode("\r\n", $arr_lines);

            $fileNameGenerated = PathHelper::generateFileBaseName($this->filename) . '.txt';
            $filePath = PathHelper::incoming('misc/' . $fileNameGenerated);

            $myfile = fopen($filePath, "w") or die("Unable to open file!");
            fwrite($myfile, $imploded_arr_lines);
            fclose($myfile);
            

		return $output;
	}

} // EOC