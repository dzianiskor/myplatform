<?php
use App\Coupon as Coupon;

/**
 * Coupon Helper
 *
 * @category   Helper Classes
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class CouponHelper
{
    /**
     * Generate a random string of ($length) characters long
     *
     * @param int $length
     * @return string
     */
    public static function generateRandomString($length = 5)
    {
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $buffer     = '';

        for ($i = 0; $i < $length; $i++) {
            $buffer .= $characters[rand(0, strlen($characters) - 1)];
        }

        return $buffer;
    }

    /**
     * Generate a random string of ($length) characters long
     *
     * @param int $length
     * @return string
     */
    public static function CouponPrice($couponCode, $amount = 0, $partnerId)
    {
        $couponPrice	= 0;
        $couponDetails	= array();
        $couponDetails['price']     = 0;
        $couponDetails['currency']  = 6;

        if($couponCode){
                $coupon			= Coupon::where('coupon_code', $couponCode)->first();
                $couponType		= $coupon->coupon_type;
                if($couponType == 'value'){
                        $couponPrice	= $coupon->price();
                }
                else{
                        if($partnerId == 3){
                                $couponPrice	= $coupon->price();
                                $couponDetails['price']	= $couponPrice;
                        }
                        else{
                                $couponPrice	= ($amount * $coupon->price()) / 100;
                                $couponDetails['price']	= $couponPrice;
                                $couponDetails['currency']	= $coupon->currency();
                        }
                }
        }
		
        return $couponDetails;
    }

    /**
     * get type of coupon
     *
     * @param string couponCode
     * @return string
     */
     public static function CouponType($couponCode)
    {
        $couponType	= 'value';

		if($couponCode){
			$coupon			= Coupon::where('coupon_code', $couponCode)->first();
			$couponType		= $coupon->coupon_type;
		}

        return $couponType;
    }

} // EOC