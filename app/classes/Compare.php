<?php
/**
 * ComparisonHelper
 *
 * 
 * @category   Helper Classes
 * @package    BLU
 * @author     Magid Mroueh <magid.mroueh@bluloyalty.com>
 * @copyright  2017 BLU
 * @link       http://blupoints.com
 */
class Compare 
{
    /**
     * this function will do th comparison
     *
     * @param string $op1, $op2, $operator
     * @return bool
     * 
     * $op1 is the current value
     * $op2 is the compared to value
     * $c is the operator string ex: equal, different, less_than, greater_than, less_or_equal, greater_or_equal.
     */
    public static function is($op1, $op2, $c)
    {
        $response = FALSE;
        if(empty($op1) || empty($op2) || empty($c)){
            return $response;
        }
        switch($c) {
            case "equal":
                $response = self::equal($op1, $op2);
                break;
            
            case "different":
                $response = self::different($op1, $op2);
                break;
            
            case "less_than":
                $response = self::less_than($op1, $op2);
                break;
            
            case "greater_than":
                $response = self::greater_than($op1, $op2);
                break;
            
            case "less_or_equal":
                $response = self::less_or_equal($op1, $op2);
                break;
            
            case "greater_or_equal":
                $response = self::greater_or_equal($op1, $op2);
                break;
            
            default :
                break;
            
        }
        
        return $response;
    }
    
    /**
    * Function will check if 2 options are equal
    *
    * @param    $op1, $op2
    * @access   private
    * @return   boolean
    */
    private static function equal($op1, $op2){
        if ($op1 == $op2) {
                return TRUE;
        }
        else{
            return FALSE;
        }
    }
    
    /**
    * Function will check if 2 options are different
    *
    * @param    $op1, $op2
    * @access   private
    * @return   boolean
    */
    private static function different($op1, $op2){
        if ($op1 != $op2) {
                return TRUE;
        }
        else{
            return FALSE;
        }
    }
    
    /**
    * Function will check if option is
    * less than another
    *
    * @param    $op1, $op2
    * @access   private
    * @return   boolean
    */
    private static function less_than($op1, $op2){
        if ($op1 < $op2) {
                return TRUE;
        }
        else{
            return FALSE;
        }
    }
    
    /**
    * Function will check if option is
    * greater than another
    *
    * @param    $op1, $op2
    * @access   private
    * @return   boolean
    */
    private static function greater_than($op1, $op2){
        if ($op1 > $op2) {
                return TRUE;
        }
        else{
            return FALSE;
        }
    }
    
    /**
    * Function will check if option is
    * less than or equal another
    *
    * @param    $op1, $op2
    * @access   private
    * @return   boolean
    */
    private static function less_or_equal($op1, $op2){
        if ($op1 <= $op2) {
                return TRUE;
        }
        else{
            return FALSE;
        }
    }
    
    /**
    * Function will check if option is
    * greater than or equal another
    *
    * @param    $op1, $op2
    * @access   private
    * @return   boolean
    */
    private static function greater_or_equal($op1, $op2){
        if ($op1 >= $op2) {
                return TRUE;
        }
        else{
            return FALSE;
        }
    }
} // EOC