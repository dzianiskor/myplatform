<?php
/**
 * UI Filtering/Sorting/Search Helper
 * 
 * @category   Helper Classes
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class Filter 
{
    /**
     * Create a base url using all url parameters for sorting
     *
     * @param string $component
     * @return string
     */
    public static function baseUrl($component)
    {
        return "?$component";

        /*
        if(!empty($_SERVER['QUERY_STRING'])){
            return "?{$_SERVER['QUERY_STRING']}&$component";
        } else {
            return "?$component";
        }
        */
    }

    /**
     * Return the search url for the current resource
     *
     * @return string
     */
    public static function searchUrl()
    {
        return array_key_exists('REQUEST_URI', $_SERVER) ? $_SERVER['REQUEST_URI'] : '';
    }

} // EOC