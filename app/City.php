<?php
namespace App;
use App\BluCollection;
use Eloquent;
/**
 * City Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class City extends Eloquent 
{
	public $table      = 'city';
	public $timestamps = false;

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------
    
    public function area() 
    { 
    	return $this->belongsTo('App\Area'); 
   	}

} // EOC