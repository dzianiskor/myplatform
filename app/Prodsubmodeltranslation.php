<?php
namespace App;
use App\BluCollection;
use Eloquent;
/**
 * Store Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class Prodsubmodeltranslation extends Eloquent 
{
    public $table = 'prodsubmodel_translation';

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------
    
    public function prodsubmodel()
    {
        return $this->belongsTo('App\Prodsubmodel');
    }

    

    # --------------------------------------------------------------------------
    # 
    # Helpers
    # 
    # --------------------------------------------------------------------------
    
    /**
     * Return a list of stores for the specified partner
     * 
     * @param int $prodsubmodel_id
     * @return array
     */
    public static function prodsubmodelTranslations($prodsubmodel_id)     
    { 
        return Prodsubmodeltranslation::where('prodsubmodel_id', $prodsubmodel_id)->orderBy('name')->get();
    }

} // EOC