<?php
namespace App;
use App\BluCollection;
/**
 * Occupation Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class Occupation extends BaseModel 
{
    public $table      = 'occupation';
    public $timestamps = false;
    protected $guarded = array();

    # --------------------------------------------------------------------------
    #
    # Relationships
    #
    # --------------------------------------------------------------------------
    
    public function users()
    {
        return $this->belongsToMany('App\User');
    }

} // EOC