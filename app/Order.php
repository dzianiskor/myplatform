<?php
namespace App;
use App\BluCollection;
use Eloquent;
/**
 * Order Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class Order extends Eloquent 
{
    public $table = 'order';

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------
    
    public function user()  
    { 
        return $this->belongsTo('App\User');             
    }

    public function transaction()  
    { 
        return $this->belongsTo('App\Transaction');             
    }

} // EOC