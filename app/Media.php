<?php
namespace App;
use App\BluCollection;
use Eloquent;
/**
 * Media Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class Media extends Eloquent 
{
    public $table = 'media';

    public function banners()
    {
        return $this->hasMany('App\Banner');
    }
    
    public function cardprints()
    {
        return $this->hasMany('App\CardPrint');
    }

    public function getThumbnailPath($width, $height)
    {
        return public_path("uploads/tn_{$width}x{$height}_{$this->name}");
    }

    public function getFilePath()
    {
        return public_path("uploads/{$this->name}");
    }

    public function fileExists()
    {
        return file_exists($this->getFilePath());
    }

    public static function getPlaceholderImagePath($size)
    {
        return "https://placehold.it/" . self::sanitizeText(Input::get('s'));
    }

    
} // EOC