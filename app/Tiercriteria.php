<?php
namespace App;
use App\BluCollection;
use Eloquent;
/**
 * Store Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Ralph Nader <ralph@bluloyalty.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class Tiercriteria extends Eloquent 
{
    public $table = 'tier_criteria';

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------
    
    public function tier()
    {
        return $this->belongsTo('App\Tier');
    }
    public function users()      
    { 
        return $this->belongsToMany('App\User'); 
    }   



    # --------------------------------------------------------------------------
    # 
    # Helpers
    # 
    # --------------------------------------------------------------------------
    
    /**
     * Return a list of supplierstores for the specified partner
     * 
     * @param int $tier_id
     * @return array
     */
    public static function TierCriterias($tier_id)     
    { 
        return Tiercriteria::where('tier_id', $tier_id)->orderBy('name')->get();
    }
    /**
     * Check to see if a tiercriteria contains a user
     * 
     * @param int $user_id
     * @return bool
     */
    public static function hasUser($user_id, $tiercriteria_id)
    {
        // Don't cache
        $count = DB::table('tiercriteria_user')->where('user_id', $user_id)->where('tiercriteria_id', $tiercriteria_id)->count();

        if($count > 0) {
            return true;
        } else {
            return false;
        }
    }

} // EOC