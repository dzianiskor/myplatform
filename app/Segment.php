<?php

namespace App;

use Illuminate\Support\Facades\DB;

/**
 * Segment Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class Segment extends BaseModel 
{
    public $table = 'segment';

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------    

    public function attributes() 
    { 
        return $this->hasMany('App\Attribute', 'mapping_key'); 
    }    

    public function users()      
    { 
        return $this->belongsToMany('App\User'); 
    }   

    public function products()   
    { 
        return $this->belongsToMany('App\Product'); 
    }

    public function partner()    
    { 
        return $this->belongsTo('App\Partner'); 
    }

    public function banners()
    {
        return $this->belongsToMany('App\Banner');
    }

    # --------------------------------------------------------------------------
    # 
    # Helpers
    # 
    # --------------------------------------------------------------------------    

    /**
     * Check to see if a segment contains a user
     * 
     * @param int $user_id
     * @return bool
     */
    public static function hasUser($user_id, $segment_id)
    {
        // Don't cache
        $count = DB::table('segment_user')->where('user_id', $user_id)->where('segment_id', $segment_id)->count();

        if($count > 0) {
            return true;
        } else {
            return false;
        }
    }
    
} // EOC