<?php

namespace App\Observers;

use App\User;

class UserObserver
{

    public function created(User $user)
    {
        $this->generateUserProfileImageThumbnailIfImageWasChanged($user);
    }

    public function updated(User $user)
    {
       $this->generateUserProfileImageThumbnailIfImageWasChanged($user);
    }

    protected function generateUserProfileImageThumbnailIfImageWasChanged($user)
    {
        if ($user->isDirty('profile_image')) {
            $user->generateProfileImageThumbnail();
        }
    }


}