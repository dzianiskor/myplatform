<?php
namespace App;

/**
 * Attribute Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class Attribute extends BaseModel
{
    public $table = 'attribute';
    public $timestamps = false;
    
} // EOC