<?php
namespace App;
use App\BluCollection;
use Eloquent;
/**
 * Area Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Magid Mroueh <magid.mroueh@wixelhq.com>
 * @copyright  2017 BLU
 * @link       http://blupoints.com
 */
class Affiliate extends Eloquent 
{
    public $table      = 'affiliates';

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------
    
    public function Partners() 
    { 
    	return $this->belongsToMany('App\Partner', 'affiliate_partner')->distinct();
    }


} // EOC