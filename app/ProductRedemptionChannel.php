<?php
namespace App;
use App\BluCollection;
use Eloquent;
/**
 * Product Channel Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class ProductRedemptionChannel extends Eloquent 
{
    public $table = 'product_redemption_display_channel';
    public $timestamps = false;

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------

    public function channel() 
    { 
        return $this->belongsTo('App\Partner', 'channel_id'); 
    }        

} // EOC