<?php
namespace App;
use App\BluCollection;
/**
 * Banner Model
 *
 * @category   Models
 * @package    BLU
 * @author     Magid Mroueh <magid.mroueh@bluloyalty.com>
 * @copyright  2015 BLU
 * @link       http://blupoints.com
 */
class InnstantDescriptions extends BaseModel
{
	public $table = 'innstant_descriptions';

    public function hotel()
    {
        return $this->belongsTo('App\InnstantHotels');
    }

} // EOC