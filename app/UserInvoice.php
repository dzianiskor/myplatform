<?php
namespace App;
use App\BluCollection;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Product Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class UserInvoice extends BaseModel 
{
    public $table         = 'user_invoice';
   

    # --------------------------------------------------------------------------
    #
    # Relationships
    #
    # --------------------------------------------------------------------------

    public function media()    
    {        
        return $this->belongsTo('App\Media');    
    }
    public function image()
    {
        $image = $this->hasMany('App\MediaInvoice', 'user_invoice_id');
        $image = $image->first();

        return $image;
    }
    public function partner(){
        $partner = Partner::find($this->partner_id);
        return $partner;
    }
    public function user(){
        $user = User::find($this->user_id);
        return $user;
    }

       

    # --------------------------------------------------------------------------
    # 
    # Attributes
    # 
    # --------------------------------------------------------------------------
    
    

} // EOC