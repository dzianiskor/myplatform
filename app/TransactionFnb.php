<?php

/**
 * Transaction Model
 *
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class TransactionFnb extends BaseModel {

    public $table = 'transaction_fnb';
    private static $notify = true;

    # --------------------------------------------------------------------------
    #
    # Relationships
    #
    # --------------------------------------------------------------------------

    public function transaction() {
        return $this->belongsTo('Transaction');
    }

}

// EOC
