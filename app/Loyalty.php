<?php

namespace App;
use CurrencyHelper;
use CouponHelper;
use PointsHelper;
use App\Http\Controllers\APIController;
use Compare;
use Illuminate\Database\Eloquent\Model;

class Loyalty extends Model
{
    public $table = 'loyalty';

    # --------------------------------------------------------------------------
    #
    # Relationships
    #
    # --------------------------------------------------------------------------

    public function partner()
    {
        return $this->belongsTo('App\Partner');
    }

    public function rules()
    {
        return $this->hasMany('App\LoyaltyRule')->where('deleted', '=' , 0);
    }

    public function cashbacks()
    {
        return $this->hasMany('App\LoyaltyCashback');
    }

    public function currency()
    {
        return $this->hasOne('App\currency', 'currency_id');
    }

    # --------------------------------------------------------------------------
    #
    # Attributes
    #
    # --------------------------------------------------------------------------

    /**
     * Fetch the default loyalty program for a partner
     *
     * @param int $partner_id
     * @return object
     */
    public static function getDefaultProgram($partner_id)
    {
        $now = date("Y-m-d H:i:s", time());

        $loyalty_program = Loyalty::where('partner_id', $partner_id);
        $loyalty_program->where('status', 'Enabled');
        $loyalty_program->where('draft', false);
        $loyalty_program->orderBy('priority', 'ASC');
        $loyalty_program->where('valid_from', '<=', $now);
        $loyalty_program->where('valid_to', '>=', $now);

        return $loyalty_program->first();
    }

    # --------------------------------------------------------------------------
    #
    # Helpers
    #
    # --------------------------------------------------------------------------

    public static function processMatchingRedemptionRule($transaction)
    {
        $program = Loyalty::getDefaultProgram($transaction->partner_id);

        if(!$program) {
            return $transaction;
        } else {

            $rule_matched = false; // Have we matched an exception rule?
            $exception_id = 0;

            // Do exception rules exist?
            if($program->rules()->count() > 0) {

                foreach($program->rules as $exception_rule) {

                    if($rule_matched) {
                        break;
                    }

                    switch($exception_rule->rule_type) {
                        case "Segment":
                            if(Segment::hasUser($transaction->user_id, $exception_rule->rule_value)) {
                                $transaction->segment_id = $exception_rule->rule_value;
                                $exception_id = $exception_rule->id;
                                $rule_matched = true;
                            }
                            break;
                        case "Country":
                            if($transaction->store) {
                                if($transaction->store->address->country_id == $exception_rule->rule_value) {
                                    $transaction->country_id = $exception_rule->rule_value;
                                    $exception_id = $exception_rule->id;
                                    $rule_matched = true;
                                }
                            }
                            break;
                        case "Store":
                            if($transaction->store) {
                                if($transaction->store->id == $exception_rule->rule_value) {
                                    $transaction->store_id = $exception_rule->rule_value;
                                    $exception_id = $exception_rule->id;
                                    $rule_matched = true;
                                }
                            }
                            break;
                        case "Category":
                            foreach($transaction->items as $item) {
                                if($item->product_category_id == $exception_rule->rule_value) {
                                    $transaction->category_id = $exception_rule->rule_value;
                                    $exception_id = $exception_rule->id;
                                    $matched = true;
                                }
                            }
                            break;
                        case "Currency":
                            if($transaction->currency_id) {
                                if($transaction->currency_id == $exception_rule->rule_value) {
                                    $transaction->currency_id = $exception_rule->rule_value;
                                    $exception_id = $exception_rule->id;
                                    $rule_matched = true;
                                }
                            }
                            break;
                    }
                }
            }

            if($rule_matched) {
                $transaction->rule_id = (int)$program->id;
                $transaction->loyalty_exception_id = (int)$exception_id;
                $transaction->save();
            }
        }

        return $transaction;
    }

    /**
     * Process the loyalty programs applicable to the specified transaction
     *
     * @param Transaction
     * @return Transaction
     */
    public static function processMatchingRewardRule($transaction, $type)
    {
        $program = Loyalty::getDefaultProgram($transaction->partner_id);

        $user = User::where('id', $transaction->user_id)->first();

        if(!$program) {
            $transaction->points_rewarded = 0;
            $transaction->rule_id         = null;
        } else {
            $networkId			= $transaction->network_id;
            $network			= Network::find($networkId);
            $currency	= Currency::where('id', $transaction->currency_id)->first();
            $networkPointPrice	= $network->value_in_currency;
            $networkCurrencyId	= $network->currency_id;
            $networkRewardPts	= $network->reward_pts;
            $amount			= $transaction->original_total_amount;
            if($networkCurrencyId == $currency->id){
                $amountInPoints			 = $amount / $networkPointPrice;
                $amountInNetworkCurrency = $amount;
                $currencyOfTheAmount     = $currency->id;
            } else {
                $networkCurrency		 = Currency::where('id', $networkCurrencyId)->first();
                $amountInUsd			 = $amount / $currency->latestRate();
                $amountInNetworkCurrency = $amountInUsd * $networkCurrency->latestRate();
                $amountInPoints			 = $amountInNetworkCurrency / $networkPointPrice;
                $amount				     = $amountInNetworkCurrency;
                $currencyOfTheAmount     = $networkCurrency->id;
            }

            //coupon process
            if(!empty($transaction->coupon_id)){
                $couponDetails			= Coupon::find($transaction->coupon_id);
                $coupon_code			= $couponDetails->coupon_code;;
                $couponDetails			= CouponHelper::CouponPrice($coupon_code, $transaction->total_amount, $transaction->partner_id);
                $couponPrice			= $couponDetails['price'];
                $couponCurrencyId		= $couponDetails['currency'];
                $couponCurrencyDetails	= Currency::where('id', $couponCurrencyId)->first();
                $couponPrice_usd		= $couponPrice / $couponCurrencyDetails->latestRate();
                $couponPoints_rewarded	= $couponPrice_usd;
            }
            else{
                $couponPoints_rewarded	= 0;
                $couponPrice_usd		= 0;
            }
            $rule_matched = false; // Have we matched an exception rule?
            $exception_id = 0;


            // Do exception rules exist?
            if($program->rules()->count() > 0) {

                $reward_usd = 0;     // The reward USD amount
                $reward_pts = 0;     // The reward points amount
                $reward_tp  = null;  // The reward type

                foreach($program->rules as $exception_rule) {

                    if($rule_matched) {
                        break;
                    }

                    switch($exception_rule->rule_type) {
                        case "Segment":
                            if(Segment::hasUser($transaction->user_id, $exception_rule->rule_value)) {

                                $transaction->segment_id = $exception_rule->rule_value;

                                switch($exception_rule->type) {
                                    case "Points":
                                        $reward_pts = $exception_rule->reward_pts;
//                                        $reward_usd = $exception_rule->reward_usd;
                                        $reward_usd = $exception_rule->original_reward;
                                        $reward_tp  = 'Points';
                                        break;
                                    case "Price":
                                        $reward_pts = $exception_rule->reward_pts;
                                        $reward_tp  = 'Price';
                                        break;
                                }
                                $exception_id = $exception_rule->id;
                                $rule_matched = true;
                            }
                            break;
                        case "Country":
                            if($transaction->store_id) {

                                $store = Store::where('id', $transaction->store_id)->first();

                                if($store->address->country_id == $exception_rule->rule_value) {

                                    $transaction->country_id = $exception_rule->rule_value;

                                    switch($exception_rule->type) {
                                        case "Points":
                                            $reward_pts = $exception_rule->reward_pts;
//                                            $reward_usd = $exception_rule->reward_usd;
                                            $reward_usd = $exception_rule->original_reward;
                                            $reward_tp  = 'Points';
                                            break;
                                        case "Price":
                                            $reward_pts = $exception_rule->reward_pts;
                                            $reward_tp  = 'Price';
                                            break;
                                    }
                                    $exception_id = $exception_rule->id;
                                    $rule_matched = true;
                                }
                            }
                            break;
                        case "Store":
                            if($transaction->store_id) {

                                $store = Store::where('id', $transaction->store_id)->first();

                                if($store->id == $exception_rule->rule_value) {
                                    switch($exception_rule->type) {
                                        case "Points":
                                            $reward_pts = $exception_rule->reward_pts;
//                                            $reward_usd = $exception_rule->reward_usd;
                                            $reward_usd = $exception_rule->original_reward;
                                            $reward_tp  = 'Points';
                                            break;
                                        case "Price":
                                            $reward_pts = $exception_rule->reward_pts;
                                            $reward_tp  = 'Price';
                                            break;
                                    }
                                    $exception_id = $exception_rule->id;
                                    $rule_matched = true;
                                }
                            }
                            break;
                        case "Item":
                            if($transaction->items()->count() > 0) {
                                foreach($transaction->items as $i) {
                                    if($i->product_id == $exception_rule->rule_value) {
                                        switch($exception_rule->type) {
                                            case "Points":
                                                $reward_pts = $exception_rule->reward_pts;
//                                                $reward_usd = $exception_rule->reward_usd;
                                                $reward_usd = $exception_rule->original_reward;
                                                $reward_tp  = 'Points';
                                                break;
                                            case "Price":
                                                $reward_pts = $exception_rule->reward_pts;
                                                $reward_tp  = 'Price';
                                                break;
                                            case "Event":
                                                $reward_pts = $exception_rule->reward_pts;
                                                $reward_num_events = $exception_rule->original_reward;
                                                $reward_tp  = 'Event';
                                                break;
                                        }
                                        $exception_id = $exception_rule->id;
                                        $rule_matched = true;
                                    }
                                }
                            }
                            if(!empty($transaction->product_id) && !is_null($transaction->product_id)){
                                if($transaction->product_id == $exception_rule->rule_value) {
                                    switch($exception_rule->type) {
                                        case "Points":
                                            $reward_pts = $exception_rule->reward_pts;
//                                                $reward_usd = $exception_rule->reward_usd;
                                            $reward_usd = $exception_rule->original_reward;
                                            $reward_tp  = 'Points';
                                            break;
                                        case "Price":
                                            $reward_pts = $exception_rule->reward_pts;
                                            $reward_tp  = 'Price';
                                            break;
                                        case "Event":
                                            $reward_pts = $exception_rule->reward_pts;
                                            $reward_num_events = $exception_rule->original_reward;
                                            $reward_tp  = 'Event';
                                            break;
                                    }
                                    $exception_id = $exception_rule->id;
                                    $rule_matched = true;
                                }
                            }
                            break;
                        case "Category":
                            if($transaction->items()->count() > 0) {
                                foreach($transaction->items as $i) {

                                    $product = Product::find($i->product_id);

                                    if($product->category) {
                                        $category = $product->category;

                                        if($category->id == $exception_rule->rule_value) {
                                            $exception_id = $exception_rule->id;
                                            $rule_matched = true;
                                        }

                                        if($category->parentCategory) {
                                            $parent = $category->parentCategory;

                                            if($parent->id == $exception_rule->rule_value) {
                                                $exception_id = $exception_rule->id;
                                                $rule_matched = true;
                                            }
                                        }
                                    }

                                    if($rule_matched) {

                                        $transaction->category_id = $exception_rule->rule_value;

                                        switch($exception_rule->type) {
                                            case "Points":
                                                $reward_pts = $exception_rule->reward_pts;
//                                                $reward_usd = $exception_rule->reward_usd;
                                                $reward_usd = $exception_rule->original_reward;
                                                $reward_tp  = 'Points';
                                                break;
                                            case "Price":
                                                $reward_pts = $exception_rule->reward_pts;
                                                $reward_tp  = 'Price';
                                                break;
                                        }
                                    }
                                }
                            }
                            break;
                        case "Brand":
                            if($transaction->items()->count() > 0) {
                                foreach($transaction->items as $i) {

                                    $product = Product::find($i);

                                    if($product->brand_id == $exception_rule->rule_value) {
                                        switch($exception_rule->type) {
                                            case "Points":
                                                $reward_pts = $exception_rule->reward_pts;
                                                //                                                $reward_usd = $exception_rule->reward_usd;
                                                $reward_usd = $exception_rule->original_reward;
                                                $reward_tp  = 'Points';
                                                break;
                                            case "Price":
                                                $reward_pts = $exception_rule->reward_pts;
                                                $reward_tp  = 'Price';
                                                break;
                                        }
                                        $exception_id = $exception_rule->id;
                                        $rule_matched = true;
                                    }
                                }
                            }
                            break;
                        case "Currency":
                            if($transaction->currency_id) {

                                $currency = Currency::where('id', $transaction->currency_id)->first();

                                if($currency->id == $exception_rule->rule_value) {
                                    switch($exception_rule->type) {
                                        case "Points":
                                            $reward_pts = $exception_rule->reward_pts;
//                                            $reward_usd = $exception_rule->reward_usd;
                                            $reward_usd = $exception_rule->original_reward;
                                            $reward_tp  = 'Points';
                                            break;
                                        case "Price":
                                            $reward_pts = $exception_rule->reward_pts;
                                            $reward_tp  = 'Price';
                                            break;
                                    }
                                    $exception_id = $exception_rule->id;
                                    $rule_matched = true;
                                }
                            }
                            break;
                        case "Merchant Category Code":
                            if(!empty($transaction->mcc) && !is_null($transaction->mcc)){
                                if($transaction->mcc == $exception_rule->rule_value) {
                                    switch($exception_rule->type) {
                                        case "Points":
                                            $reward_pts = $exception_rule->reward_pts;
//                                                $reward_usd = $exception_rule->reward_usd;
                                            $reward_usd = $exception_rule->original_reward;
                                            $reward_tp  = 'Points';
                                            break;
                                        case "Price":
                                            $reward_pts = $exception_rule->reward_pts;
                                            $reward_tp  = 'Price';
                                            break;
                                        case "Event":
                                            $reward_pts = $exception_rule->reward_pts;
                                            $reward_num_events = $exception_rule->original_reward;
                                            $reward_tp  = 'Event';
                                            break;
                                    }
                                    $exception_id = $exception_rule->id;
                                    $rule_matched = true;
                                }
                            }
                            break;
                    }
                }
            }

            if($rule_matched) {
                if($type == 'bonus_on_registration'){
                    if($exception_id != 0){
                        $transaction->loyalty_exception_id = $exception_id;
                    }
                    $transaction->rule_id         = (int)$program->id;
                    return $transaction;
                }

                if($reward_tp == 'Points') {
//                    $transaction->points_rewarded = round(($transaction->total_amount / $reward_usd) *  $reward_pts);
                    $transaction->points_rewarded = round(($amountInNetworkCurrency / $reward_usd) *  $reward_pts);
                    $couponPoints_rewarded= round(($couponPrice_usd / $reward_usd) *  $reward_pts);
                    $transaction->points_rewarded = $transaction->points_rewarded - $couponPoints_rewarded;
                }
                elseif($reward_tp == 'Event'){
                    $transaction->points_rewarded = round(($transaction->original_total_amount / $reward_num_events) *  $reward_pts);
                    $transaction->total_amount = 0;
                    $transaction->event_total = $transaction->original_total_amount;
                    $transaction->original_total_amount = 0;
                    $transaction->partner_amount = 0;
                }
                else {
                    $transaction->points_rewarded = $reward_pts;
                }

                $transaction->points_balance  = $user->balance($transaction->network_id) + $transaction->points_rewarded;
                if($exception_id != 0){
                    $transaction->loyalty_exception_id = $exception_id;
                }
                $transaction->rule_id         = (int)$program->id;
            } else {
                $originalAmountInPoints	= PointsHelper::amountToPoints($transaction->original_total_amount, $transaction->currency_id, $transaction->network_id);

                if($currencyOfTheAmount != $program->currency_id){
                    $amountCurrency  = Currency::where('id', $currencyOfTheAmount)->first();
                    $programCurrency = Currency::where('id', $program->currency_id)->first();

                    $programCurrencyRate = $programCurrency->latestRate();
                    $amountCurrencyRate  = $amountCurrency->latestRate();
                    $amountInNetworkCurrency = ($amountInNetworkCurrency / $amountCurrencyRate) * $programCurrencyRate;
                }

//                $transaction->points_rewarded = round(($transaction->total_amount / $program->reward_usd) *  $program->reward_pts);
                $transaction->points_rewarded = round(($amountInNetworkCurrency / $program->original_reward) *  $program->reward_pts);

                $couponPoints_rewarded = round(($couponPrice_usd / $program->reward_usd) *  $program->reward_pts);
                $transaction->points_rewarded = $transaction->points_rewarded - $couponPoints_rewarded;
                $transaction->points_balance  = $user->balance($transaction->network_id) + $transaction->points_rewarded;
                if($exception_id != 0){
                    $transaction->loyalty_exception_id = $exception_id;
                }
                $transaction->rule_id         = (int)$program->id;
            }
        }

        return $transaction;
    }

    /**
     * Process the loyalty programs applicable to the specified transaction
     *
     * @param Transaction
     * @return Transaction
     */
    public static function processMatchingRewardExceptionRule($transaction, $type = 'reward',$use_partner_currency = false)
    {
        $program = Loyalty::getDefaultProgram($transaction->partner_id);

        $user = User::where('id', $transaction->user_id)->first();
        try{


            if(!$program) {
                $transaction->points_rewarded = 0;
                $transaction->rule_id         = null;
            } else {
                $networkId			= $transaction->network_id;
                $network			= Network::find($networkId);
                $currency	= Currency::where('id', $transaction->currency_id)->first();
                $networkPointPrice	= $network->value_in_currency;
                $programCurrencyId	= $program->currency_id;
                //$networkRewardPts	= $network->reward_pts;
                $amount			= $transaction->original_total_amount;
                if($use_partner_currency == TRUE){
                    $currency	= Currency::where('id', $transaction->partner_currency_id)->first();
                    $amount =$transaction->partner_amount;
                }
                if($programCurrencyId == $currency->id){
                    $amountInPoints			= $amount / $networkPointPrice;
                    $amountInNetworkCurrency	= $amount;
                    $networkCurrency		= Currency::where('id', $programCurrencyId)->first();
                }else{
                    $networkCurrency		= Currency::where('id', $programCurrencyId)->first();
                    $amountInUsd			= CurrencyHelper::convertToUSD($amount, $currency->short_code);
                    $amountInNetworkCurrency	= $amountInUsd['amount'] * $networkCurrency->latestRate();
                    $amountInPoints			= $amountInNetworkCurrency / $networkPointPrice;
                    $amount				= $amountInNetworkCurrency;
                }
                //coupon process
                if(!empty($transaction->coupon_id)){
                    $couponDetails			= Coupon::find($transaction->coupon_id);
                    $coupon_code			= $couponDetails->coupon_code;
                    $couponDetails			= CouponHelper::CouponPrice($coupon_code, $transaction->total_amount, $transaction->partner_id);
                    $couponPrice			= $couponDetails['price'];
                    $couponCurrencyId		= $couponDetails['currency'];
                    $couponCurrencyDetails	= Currency::where('id', $couponCurrencyId)->first();
                    $couponPrice_usd		= $couponPrice / $couponCurrencyDetails->latestRate();
                    $couponPoints_rewarded	= $couponPrice_usd;
                }
                else{
                    $couponPoints_rewarded	= 0;
                    $couponPrice_usd		= 0;
                }
                $rule_matched = false; // Have we matched an exception rule?
                $exception_id = 0;
                $arr_matched_rules = array();
                // Do exception rules exist?
                if($program->rules()->count() > 0) {

                    $reward_usd = 0;     // The reward USD amount
                    $reward_pts = 0;     // The reward points amount
                    $reward_tp  = null;  // The reward type

                    foreach($program->rules as $exception_rule) {
//                    $json_data = json_encode($exception_rule->toArray());
//                    APIController::postSendEmailJson($json_data);
                        if($rule_matched) {
                            break;
                        }
                        //$loyalty_rule = LoyaltyRule::find($exception_rule->id);

                        $exception_rule_obj = LoyaltyExceptionrule::where('loyalty_rule_id',$exception_rule->id)->where('deleted',0)->get();
//                    $json_data = json_encode("trx_id" => $transaction->id ,$exception_rule_obj->toArray());
//                    APIController::postSendEmailJson($json_data);
                        $arr_except_rule = array();
                        $except_rule_checked = false;
                        $except_rule_matched = 0;
//                    $last_rule = end($exception_rule_obj);
                        $ors_arr_match = array();
                        foreach($exception_rule_obj as $except_rule){
//                    $json_data = json_encode(array("trx_id" => $transaction->id ,"except_rule_type"=> $except_rule->type, "except_rule id"=> $exception_rule->id));
//                    APIController::postSendEmailJson($json_data);

                            switch($except_rule->type) {
                                case "Segment":
//                                if(Segment::hasUser($transaction->user_id, $except_rule->rule_value)) {
                                    if(Compare::is(Segment::hasUser($transaction->user_id, $except_rule->rule_value), 1, $except_rule->comparison_operator)) {

                                        $transaction->segment_id = $except_rule->rule_value;

                                        $arr_except_rule['segment'][] = $except_rule->rule_value;
                                        $except_rule_checked = true;
                                        $except_rule_matched = $exception_rule->id;
                                        //$arr_matched_rules[] = $except_rule_matched;
                                        if($except_rule->operator == 'or'){
                                            $ors_arr_match[] = true;
                                        }
                                    }
                                    else{
                                        $except_rule_checked = false;
                                        if($except_rule->operator == 'and'){
                                            break 2;
                                        }else{
                                            $ors_arr_match[] = false;
                                        }
                                    }
                                    break;
                                case "Country":

                                    if($transaction->country_id) {

                                        $country = Country::where('id', $transaction->country_id)->first();
                                        //                                    if($currency->id == $except_rule['rule_value']) {
                                        if(Compare::is($country->id, $except_rule->rule_value, $except_rule->comparison_operator)) {

                                            $arr_except_rule['country'][] = $except_rule->rule_value;
                                            $except_rule_checked = true;
                                            $except_rule_matched = $exception_rule->id;
                                            if($except_rule->operator == 'or'){
                                                $ors_arr_match[] = true;
                                            }
                                        }
                                        else{
                                            $except_rule_checked = false;
                                            if($except_rule->operator == 'and'){
                                                break 2;
                                            }else{
                                                $ors_arr_match[] = false;
                                            }
                                        }
                                    }
                                    elseif($transaction->store_id) {

                                        $store = Store::where('id', $transaction->store_id)->first();

//                                    if($store->address->country_id == $exception_rule->rule_value) {
                                        if(Compare::is($store->address->country_id, $exception_rule->rule_value, $except_rule->comparison_operator)) {
                                            $arr_except_rule['country'][] = $except_rule->rule_value;
                                            $except_rule_checked = true;
                                            $except_rule_matched = $exception_rule->id;
                                            //$arr_matched_rules[] = $except_rule_matched;
                                            $transaction->country_id = $exception_rule->rule_value;
                                            if($except_rule->operator == 'or'){
                                                $ors_arr_match[] = true;
                                            }
                                        }
                                        else{
                                            $except_rule_checked = false;
                                            if($except_rule->operator == 'and'){
                                                break 2;
                                            }else{
                                                $ors_arr_match[] = false;
                                            }
                                        }
                                    }
                                    break;
                                case "Store":
                                    if($transaction->store_id) {

                                        $store = Store::where('id', $transaction->store_id)->first();

//                                    if($store->id == $except_rule->rule_value) {
                                        if(Compare::is($store->id,$except_rule->rule_value, $except_rule->comparison_operator)) {
                                            $arr_except_rule['store'][] = $except_rule->rule_value;
                                            $except_rule_checked = true;
                                            $except_rule_matched = $exception_rule->id;
                                            //$arr_matched_rules[] = $except_rule_matched;
                                            if($except_rule->operator == 'or'){
                                                $ors_arr_match[] = true;
                                            }
                                        }
                                        else{
                                            $except_rule_checked = false;
                                            if($except_rule->operator == 'and'){
                                                break 2;
                                            }else{
                                                $ors_arr_match[] = false;
                                            }
                                        }
                                    }
                                    break;
                                case "Item":

                                    if($transaction->items()->count() > 0) {
                                        foreach($transaction->items as $i) {
//                                        if($i->product_id == $except_rule->rule_value) {
                                            if(Compare::is($i->product_id, $except_rule->rule_value,$except_rule->comparison_operator)) {
                                                $arr_except_rule['item'][] = $except_rule->rule_value;
                                                $except_rule_checked = true;
                                                $except_rule_matched = $exception_rule->id;
                                                //$arr_matched_rules[] = $except_rule_matched;
                                                if($except_rule->operator == 'or'){
                                                    $ors_arr_match[] = true;
                                                }
                                            }
                                            else{
                                                $except_rule_checked = false;
                                                if($except_rule->operator == 'and'){
                                                    break 2;
                                                }else{
                                                    $ors_arr_match[] = false;
                                                }
                                            }
                                        }
                                    }
                                    if(!empty($transaction->product_id) && !is_null($transaction->product_id)){

//                                    if($transaction->product_id == $except_rule->rule_value) {
                                        if(Compare::is($transaction->product_id, $except_rule->rule_value, $except_rule->comparison_operator)) {
                                            $arr_except_rule['item'][] = $except_rule->rule_value;
                                            $except_rule_checked = true;
                                            $except_rule_matched = $exception_rule->id;
                                            //$arr_matched_rules[] = $except_rule_matched;
                                            if($except_rule->operator == 'or'){
                                                $ors_arr_match[] = true;
                                            }
                                        }
                                        else{
                                            $except_rule_checked = false;
                                            if($except_rule->operator == 'and'){
                                                break 2;
                                            }else{
                                                $ors_arr_match[] = false;
                                            }
                                        }
                                    }
                                    if($except_rule->operator == 'and' && !Compare::is($transaction->product_id, $except_rule->rule_value, $except_rule->comparison_operator)){
                                        break 2;
                                    }else{
                                        break;
                                    }

                                case "Category":
                                    if($transaction->items()->count() > 0) {
                                        foreach($transaction->items as $i) {

                                            $product = Product::find($i->product_id);

                                            if($product->category) {
                                                $category = $product->category;

//                                            if($category->id == $exception_rule->rule_value) {
                                                if(Compare::is($category->id, $exception_rule->rule_value, $except_rule->comparison_operator)) {
                                                    $exception_id = $exception_rule->id;

                                                }

                                                if($category->parentCategory) {
                                                    $parent = $category->parentCategory;

//                                                if($parent->id == $exception_rule->rule_value) {
                                                    if(Compare::is($parent->id, $exception_rule->rule_value, $except_rule->comparison_operator)) {
                                                        $exception_id = $exception_rule->id;

                                                    }
                                                }
                                            }

                                            if($rule_matched) {

                                                $transaction->category_id = $except_rule->rule_value;
                                                $arr_except_rule['category'][] = $except_rule->rule_value;
                                                $except_rule_checked = true;
                                                $except_rule_matched = $exception_rule->id;
                                                //$arr_matched_rules[] = $except_rule_matched;
                                                if($except_rule->operator == 'or'){
                                                    $ors_arr_match[] = true;
                                                }
                                            }
                                            else{
                                                $except_rule_checked = false;
                                                if($except_rule->operator == 'and'){
                                                    break 2;
                                                }else{
                                                    $ors_arr_match[] = false;
                                                }
                                            }
                                        }
                                    }
                                    break;
                                case "Brand":
                                    $i = $transaction->product_id;
                                    $product = Product::find($i);
//                                APIController::postSendEmailJson(array($i,$product->brand_id),"Test brand Loyalty");
//                                        if($product->brand_id == $except_rule->rule_value) {
                                    if($product){
                                        if(Compare::is($product->brand_id, $except_rule->rule_value, $except_rule->comparison_operator)) {
                                            $arr_except_rule['brand'][] = $except_rule->rule_value;
                                            $except_rule_checked = true;
                                            $except_rule_matched = $exception_rule->id;
                                            //$arr_matched_rules[] = $except_rule_matched;
                                            if($except_rule->operator == 'or'){
                                                $ors_arr_match[] = true;
                                            }
                                        }
                                        else{
                                            $except_rule_checked = false;
                                            if($except_rule->operator == 'and'){
                                                break 2;
                                            }else{
                                                $ors_arr_match[] = false;
                                            }
                                        }
                                    }

                                    break;
                                case "Currency":
                                    if($transaction->currency_id) {
//$json_data = json_encode(array("trx_id" => $transaction->id ,"in currency arr_except _rule Checked ",$except_rule->rule_value,$transaction->currency_id));
//                APIController::postSendEmailJson($json_data);
                                        $currency = Currency::where('id', $transaction->currency_id)->first();
                                        //                                    if($currency->id == $except_rule->rule_value) {
                                        if(Compare::is($currency->id, $except_rule->rule_value, $except_rule->comparison_operator)) {
                                            $arr_except_rule['currency'][] = $except_rule->rule_value;
                                            $except_rule_checked = true;
                                            $except_rule_matched = $exception_rule->id;
                                            if($except_rule->operator == 'or'){
                                                $ors_arr_match[] = true;
                                            }
                                        }
                                        else{
                                            $except_rule_checked = false;
                                            if($except_rule->operator == 'and'){
                                                break 2;
                                            }else{
                                                $ors_arr_match[] = false;
                                            }
                                        }
                                    }
                                    break;
                                case "Merchant Category Code":
//                    $json_data = json_encode(array("trx_id" => $transaction->id ,"001 except_rule_type"=> $except_rule->type, "except_rule id"=> $exception_rule->id));
//                    APIController::postSendEmailJson($json_data);
                                    if(!empty($transaction->mcc) && !is_null($transaction->mcc)){
                                        $mcc_value = DB::table('mcccodes')->where('id',$except_rule->rule_value)->first();
//                    $json_data = json_encode(array("trx_id" => $transaction->id ,"mcc_value"=> $mcc_value->mcc, $exception_rule->id));
//                    APIController::postSendEmailJsonMagid($json_data);
//                                    if($transaction->mcc == $mcc_value->mcc) {
                                        if(Compare::is($transaction->mcc, $mcc_value->mcc, $except_rule->comparison_operator)) {
                                            $arr_except_rule['mcc'][] = $except_rule->rule_value;
                                            $except_rule_checked = true;
                                            $except_rule_matched = $exception_rule->id;
                                            //$arr_matched_rules[] = $except_rule_matched;
                                            if($except_rule->operator == 'or'){
                                                $ors_arr_match[] = true;
                                            }
                                        }
                                        else{
                                            $except_rule_checked = false;
                                            if($except_rule->operator == 'and'){
                                                break 2;
                                            }else{
                                                $ors_arr_match[] = false;
                                            }
                                        }
                                    }
                                    break;
                                case "Transaction Types":
                                    if(!empty($transaction->partner_trxtype) && !is_null($transaction->partner_trxtype)){
                                        $trxtypes_value = DB::table('transaction_types')->where('id',$except_rule->rule_value)->first();
//                                    $json_data = json_encode(array($transaction->partner_trxtype ,$trxtypes_value->trx_code, $except_rule->rule_value));
//                    APIController::postSendEmailJsonMagid($json_data);
                                        if(Compare::is($transaction->partner_trxtype, $trxtypes_value->trx_code, $except_rule->comparison_operator)) {
                                            $arr_except_rule['transaction_type'][] = $except_rule->rule_value;
                                            $except_rule_checked = true;
                                            $except_rule_matched = $exception_rule->id;
                                            if($except_rule->operator == 'or'){
                                                $ors_arr_match[] = true;
                                            }
                                        }
                                        else{
                                            $except_rule_checked = false;
                                            if($except_rule->operator == 'and'){
                                                break 2;
                                            }else{
                                                $ors_arr_match[] = false;
                                            }
                                        }
                                    }
                                    break;
                                case "Amount":
                                    if(!empty($transaction->original_total_amount) && !is_null($transaction->original_total_amount)){
//                                    APIController::postSendEmailJson($exception_rule->currency_id, "exception rule currency ID");
                                        if(Compare::is($transaction->original_total_amount, $except_rule->rule_value, $except_rule->comparison_operator) && Compare::is($transaction->currency_id, $except_rule->rule_value_currency, 'equal')) {
                                            $arr_except_rule['amount'][] = $except_rule->rule_value;
                                            $except_rule_checked = true;
                                            $except_rule_matched = $exception_rule->id;
                                            if($except_rule->operator == 'or'){
                                                $ors_arr_match[] = true;
                                            }
                                        }
                                        else{
                                            $except_rule_checked = false;
                                            if($except_rule->operator == 'and'){
                                                break 2;
                                            }else{
                                                $ors_arr_match[] = false;
                                            }
                                        }
                                    }
                                    if($except_rule->operator == 'and' && !(Compare::is($transaction->original_total_amount, $except_rule->rule_value, $except_rule->comparison_operator) && Compare::is($transaction->currency_id, $except_rule->rule_value_currency, 'equal'))){
                                        break 2;
                                    }else{
                                        break;
                                    }
                            }


//                    $json_data = json_encode(array("trx_id" => $transaction->id ,"except_rule_matched"=> $except_rule_matched, 'except_rule_checked' => $except_rule_checked));
//                    APIController::postSendEmailJson($json_data,'Exception Rule Matched - ' . $transaction->id);



                        }
                        if(count($ors_arr_match) > 0){
                            if(in_array(true, $ors_arr_match)){
                                $arr_matched_rules[] = $except_rule_matched;
                            }
                        }else{
                            if($except_rule_checked == True ){
                                $arr_matched_rules[] = $except_rule_matched;
                            }
                        }
//                    $json_data = json_encode(array("trx_id" => $transaction->id ,"arr_except_rule"=>$arr_matched_rules));
//                APIController::postSendEmailJsonMagid($json_data);
                    }
                }
//            $json_data = json_encode(array("arr_ matched rules element 1",$arr_matched_rules[0]));
//                APIController::postSendEmailJson($json_data);
//            $json_data = json_encode(array("trx_id" => $transaction->id ,"arr_ matched rules "=>$arr_matched_rules, "arr_except_rule"=> $arr_except_rule));
//                APIController::postSendEmailJson($json_data, "transaction info 003");
                if(count($arr_matched_rules) >= 1){

                    $ratio = 0;
                    $points_to_reward = 0;
                    foreach($arr_matched_rules as $except_rule_matched){
                        //$exception_id = $except_rule_matched;
                        $exception_rule = LoyaltyRule::find($except_rule_matched);

                        switch($exception_rule->type) {
                            case "Points":
                                if($exception_rule->reward_pts / $exception_rule->reward_usd > $ratio){
                                    $reward_pts = $exception_rule->reward_pts;
                                    $exception_id = $except_rule_matched;
                                    //                                            $reward_usd = $exception_rule->reward_usd;
                                    $reward_usd = $exception_rule->reward_usd;
                                    $original_reward = $exception_rule->original_reward;
                                    $exception_currency_id = $exception_rule->currency_id;
                                    $reward_tp  = 'Points';
                                    $ratio = $exception_rule->reward_pts / $exception_rule->reward_usd;
                                }
                                break;
                            case "Price":
                                if($exception_rule->reward_pts > $points_to_reward){
                                    $reward_pts = $exception_rule->reward_pts;
                                    $original_reward = $exception_rule->original_reward;
                                    $points_to_reward = $reward_pts;
                                    $exception_id = $except_rule_matched;
                                    $reward_tp  = 'Price';
                                }
                                break;
                            case "Event":
                                $reward_pts = $exception_rule->reward_pts;
                                $reward_num_events = $exception_rule->original_reward;
                                $reward_tp  = 'Event';
                                $exception_id = $except_rule_matched;
                                break;
                        }

                        $rule_matched = true;
                    }
                }
//                if(isset($reward_tp)){
//                    $arr_exception = array('rule_matched'=>$rule_matched,"exception_id"=>$exception_id,'reward_tp'=>$reward_tp, 'reward_pts'=>$reward_pts, 'reward_usd'=>$reward_usd ,'amountinnetworkcurrency'=>$amountInNetworkCurrency,'transaction_ref'=>$transaction->ref_number);
//                    $json_d = json_encode($arr_exception);
//                    APIController::postSendEmailJson($json_d, "Exception Matched 9");
//                }

                if($rule_matched === TRUE) {
//                APIController::postSendEmailJson("went through" , "transaction - " . $reward_tp . " - " .$transaction->ref_number );
                    if($type == 'bonus_on_registration'){
                        if($exception_id != 0){
                            $transaction->loyalty_exception_id = $exception_id;
                        }
                        $transaction->rule_id         = (int)$program->id;
                        return $transaction;
                    }

                    if($reward_tp == 'Points') {
//                    APIController::postSendEmailJson(json_encode(array($exception_currency_id,$transaction->currency_id, $transaction->partner_currency_id)),"Points Testing 001 - " . $transaction->ref_number);
                        $currency_id = $transaction->currency_id;
                        $amountInNetworkCurrency = $transaction->original_total_amount;
                        if($use_partner_currency === TRUE){
                            $currency_id = $transaction->partner_currency_id;
                            $amountInNetworkCurrency = $transaction->partner_amount;
                        }

                        if(isset($exception_currency_id) && $exception_currency_id == $currency_id){
                            $transaction->points_rewarded = round(($amountInNetworkCurrency / $original_reward) *  $reward_pts);
                            $couponPoints_rewarded= round(($couponPrice_usd / $reward_usd) *  $reward_pts);
                            $transaction->points_rewarded = $transaction->points_rewarded - $couponPoints_rewarded;
                        }
                        elseif(isset($exception_currency_id)){
                            $ObjAmountCurrency = Currency::find($currency_id);
                            $amountInNetworkCurrencyUSD = CurrencyHelper::convertToUSD($amountInNetworkCurrency,$ObjAmountCurrency->short_code);
                            $exception_currency = Currency::find($exception_currency_id);
                            $amountInRewardCurrency = CurrencyHelper::convertFromUSD($amountInNetworkCurrencyUSD['amount'], $exception_currency->short_code);
                            //                            $arr_test = array("amountInNetworkCurrencyUSD"=>$amountInNetworkCurrencyUSD,
                            //                                "amountInNetworkCurrency"=>$amountInNetworkCurrency,
                            //                                "networkCurrency"=>$networkCurrency->short_code,
                            //                                "amountInRewardCurrency"=>$amountInRewardCurrency,
                            //                                "exception_currencyshort_code"=>$exception_currency->short_code
                            //                                );
                            //                            $json_data = json_encode($arr_test);
                            //                            APIController::postSendEmailJson($json_data, "Exception Currency ID");
                            $transaction->points_rewarded = round(($amountInRewardCurrency['amount'] / $original_reward) *  $reward_pts);
                            $couponPoints_rewarded= round(($couponPrice_usd / $reward_usd) *  $reward_pts);
                            $transaction->points_rewarded = $transaction->points_rewarded - $couponPoints_rewarded;
                        }
                        else{
                            $transaction->points_rewarded = round(($amountInNetworkCurrency / $original_reward) *  $reward_pts);
                            $couponPoints_rewarded= round(($couponPrice_usd / $reward_usd) *  $reward_pts);
                            $transaction->points_rewarded = $transaction->points_rewarded - $couponPoints_rewarded;
                        }


                    }
                    elseif($reward_tp == 'Event'){
                        $transaction->points_rewarded = round(($transaction->original_total_amount / $reward_num_events) *  $reward_pts);
                        $transaction->total_amount = 0;
                        $transaction->event_total = $transaction->original_total_amount;
                        $transaction->original_total_amount = 0;
                        $transaction->partner_amount = 0;
                    }
                    else {
                        $transaction->points_rewarded = $reward_pts;
                    }

                    $transaction->points_balance  = $user->balance($transaction->network_id) + $transaction->points_rewarded;
                    if($exception_id != 0){
                        $transaction->loyalty_exception_id = $exception_id;
                    }
                    $transaction->rule_id         = (int)$program->id;
                }
                else {
                    if($use_partner_currency=== TRUE){
                        $currency_id = $transaction->partner_currency_id;
                        $amountInNetworkCurrency= $transaction->partner_amount;
                    }
                    else{
                        $currency_id = $transaction->currency_id;
                        $amountInNetworkCurrency= $transaction->original_total_amount;
                    }
//                $arr_exception = array('program'=>$program->id,"currencyOfTheAmount"=>$transaction->currency_id, 'program currency'=>$program->currency_id, 'reward_usd'=>$program->reward_usd ,'transaction_ref'=>$transaction->ref_number);
//                $json_d = json_encode($arr_exception);
//                APIController::postSendEmailJson($json_d, "Exception Matched 4");
                    if($currency_id != $program->currency_id){
                        //convert the amount to the rule currency

                        $amountCurrency   = Currency::where('id', $currency_id)->first();
                        $programCurrency   = Currency::where('id', $program->currency_id)->first();
                        $programCurrencyRate   = $programCurrency->latestRate();
                        $amountCurrencyRate   = $amountCurrency->latestRate();
                        $amountInNetworkCurrency = ($amountInNetworkCurrency / $amountCurrencyRate) * $programCurrencyRate;
                    }
//                $originalAmountInPoints	= PointsHelper::amountToPoints($transaction->original_total_amount, $transaction->currency_id, $transaction->network_id);
//                $transaction->points_rewarded = round(($transaction->total_amount / $program->reward_usd) *  $program->reward_pts);
                    $transaction->points_rewarded = round(($amountInNetworkCurrency / $program->original_reward) *  $program->reward_pts);
                    $couponPoints_rewarded = round(($couponPrice_usd / $program->reward_usd) *  $program->reward_pts);
                    $transaction->points_rewarded = $transaction->points_rewarded - $couponPoints_rewarded;
                    $transaction->points_balance  = $user->balance($transaction->network_id) + $transaction->points_rewarded;

                    if($exception_id != 0){
                        $transaction->loyalty_exception_id = $exception_id;
                    }
                    $transaction->rule_id         = (int)$program->id;
                }
            }
        } catch (Exception $ex) {
            $json_data = json_encode(array($ex->getMessage(),$ex->getLine()));
            APIController::postSendEmailJson($json_data, "LIVE Try Catch 001 - " . $transaction->ref_number);
        }
        return $transaction;
    }

}
