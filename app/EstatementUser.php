<?php
namespace App;
use App\BluCollection;
use Eloquent;

/**
 * Notification Segment Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Magid Mroueh <magid.mroueh@bluloyalty.com>
 * @copyright  2017 BLU
 * @link       http://blupoints.com
 */
class EstatementUser extends Eloquent 
{
    public $table = 'estatement_user';
 
} // EOC