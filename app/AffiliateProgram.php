<?php
namespace App;
use App\BluCollection;
/**
 * Area Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Magid Mroueh <magid.mroueh@wixelhq.com>
 * @copyright  2017 BLU
 * @link       http://blupoints.com
 */
class AffiliateProgram extends BaseModel 
{
    public $table      = 'affiliate_program';

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------
    
    public function partners()
    { 
    	return $this->belongsToMany('App\Partner');
    }


} // EOC