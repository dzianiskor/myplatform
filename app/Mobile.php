<?php
namespace App;
use App\BluCollection;
use Eloquent;
/**
 * Mobile Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class Mobile extends Eloquent
{
    public $table = 'mobile';

    protected $fillable = array('uid', 'user_id');

    public function partner()
    {
        return $this->belongsTo('App\Partner');
    }

    public function store()
    {
        return $this->belongsTo('App\Store');
    }

    public function pos()
    {
        return $this->belongsTo('App\Pos');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
} // EOC