<?php
namespace App;
use App\BluCollection;
use Eloquent;
/**
 * Partner Channel Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class PartnerLinked extends Eloquent 
{
    public $table = 'partner_link';

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------
    
    public function partner() 
    { 
    	return $this->belongsTo('App\Partner');  
   	}
   	
    public function partnerlinked() 
    { 
    	return $this->belongsTo('App\Partner', 'partner_linked_id'); 
   	}    

} // EOC