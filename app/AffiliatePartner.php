<?php
namespace App;
use App\BluCollection;
use Eloquent;
/**
 * Area Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Magid Mroueh <magid.mroueh@wixelhq.com>
 * @copyright  2017 BLU
 * @link       http://blupoints.com
 */
class AffiliatePartner extends Eloquent 
{
    public $table      = 'affiliate_partner';

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------



} // EOC