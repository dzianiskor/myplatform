<?php

/**
 * Notification Segment Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Magid Mroueh <magid.mroueh@bluloyalty.com>
 * @copyright  2017 BLU
 * @link       http://blupoints.com
 */
class MobilePushUser extends Eloquent 
{
    public $table = 'mobile_push_notification_user';
 
} // EOC