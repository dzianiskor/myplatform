<?php
namespace App;
/**
 * Notification Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class Notification extends BaseModel
{
    public $table = 'notification';

    # --------------------------------------------------------------------------
    #
    # Relationships
    #
    # --------------------------------------------------------------------------
    
    public function partner()
    {
        return $this->belongsTo('App\Partner');
    }        

    public function segments()
    { 
        return $this->belongsToMany('App\Segment');
    }
    
    public function users(){
        return $this->belongsToMany('App\User');
    }

} // EOC