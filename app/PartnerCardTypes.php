<?php
namespace App;
use App\BluCollection;
use Eloquent;
/**
 * Partner Card Types Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Magid Mroueh <magid.mroueh@bluloyalty.com>
 * @copyright  2017 BLU
 * @link       http://blupoints.com
 */
class PartnerCardTypes extends Eloquent 
{
    public $table = 'partner_card_types';

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------
    
    public static function getCartTypeForNumber($card_id){
        $partnerCardTypes = PartnerCardTypes::where('id', $card_id)->first();
        return $partnerCardTypes->original;
    }


} // EOC