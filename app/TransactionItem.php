<?php
namespace App;
use App\BluCollection;
use Eloquent;
/**
 * Transaction Item Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class TransactionItem extends Eloquent 
{
    public $table = 'transaction_item';

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------
    
    public function product() 
    { 
        return $this->belongsTo('App\Product'); 
    }   

	public function transaction() 
	{ 
		return $this->belongsTo('App\Transaction'); 
	}   

} // EOC