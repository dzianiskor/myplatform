<?php
namespace App;
use App\BluCollection;
/**
 * Banner Model
 *
 * @category   Models
 * @package    BLU
 * @author     Magid Mroueh <magid.mroueh@bluloyalty.com>
 * @copyright  2015 BLU
 * @link       http://blupoints.com
 */
class InnstantCities extends BaseModel
{
	public $table = 'innstant_cities';

} // EOC