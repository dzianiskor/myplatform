<?php
namespace App;
use App\BluCollection;
use Eloquent;
/**
 * Store Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class Cattranslation extends Eloquent 
{
    public $table = 'category_translation';

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------
    
    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    

    # --------------------------------------------------------------------------
    # 
    # Helpers
    # 
    # --------------------------------------------------------------------------
    
    /**
     * Return a list of stores for the specified partner
     * 
     * @param int $category_id
     * @return array
     */
    public static function categoryTranslations($category_id)     
    { 
        return Cattranslation::where('category_id', $category_id)->orderBy('name')->get();
    }

} // EOC