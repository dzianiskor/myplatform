<?php
namespace App;
use App\BluCollection;
/**
 * Coupon Model
 *
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class Coupon extends BaseModel
{
    public $table = 'coupon';
    protected $guarded = array();

    # --------------------------------------------------------------------------
    #
    # Relationships
    #
    # --------------------------------------------------------------------------

    public function batch()
    {
        return $this->belongsTo('App\Batch');
    }

    public function transaction()
    {
        return $this->belongsTo('App\Transaction');
    }

    # --------------------------------------------------------------------------
    #
    # Attributes
    #
    # --------------------------------------------------------------------------

    /**
     * Return the valid_from date attribute
     *
     * @return Date
     */
    public function valid_from()
    {
        $batch = $this->batch;

        return $batch->valid_from;
    }

    /**
     * Return the valid_to date attribute
     *
     * @return Date
     */
    public function valid_to()
    {
        $batch = $this->batch;

        return $batch->valid_to;
    }

    /**
     * Return the price attribute
     *
     * @return Float
     */
    public function price()
    {
        $batch = $this->batch;

        return $batch->price;
    }

    /**
     * Return the currency attribute
     *
     * @return Float
     */
    public function currency()
    {
        $batch = $this->batch;

        return $batch->currency_id;
    }

    /**
     * Return the partner attribute
     *
     * @return Result
     */
    public function partner()
    {
        return $this->batch->partner;
    }

    /**
     * Return the country attribute
     *
     * @return Result
     */
    public function country()
    {
        return $this->batch->country;
    }

    /**
     * Return the segment attribute
     *
     * @return Result
     */
    public function segment()
    {
        return $this->batch->segment;
    }

    /**
     * Return the segment id attribute
     *
     * @return Result
     */
    public function segmentId()
    {
        return $this->batch->segment_id;
    }

    /**
     * Return the terms & conditions attribute
     *
     * @return String
     */
    public function terms()
    {
        return $this->batch->terms_and_conditions;
    }

} // EOC