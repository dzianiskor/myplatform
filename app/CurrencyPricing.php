<?php
namespace App;
use App\BluCollection;
use Eloquent;
/**
 * Currency Pricing Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class CurrencyPricing extends Eloquent 
{
    public $table = 'currency_pricing';

} // EOC