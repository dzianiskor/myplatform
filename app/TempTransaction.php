<?php
namespace App;
use App\BluCollection;
use Illuminate\Database\Eloquent\SoftDeletes;
use Eloquent;

/**
 * Product Model
 *
 * @category   Models
 * @package    BLU
 * @author     Magid Mroueh
 * @copyright  2016 BLU
 * @link       http://blupoints.com
 */
class TempTransaction extends Eloquent
{
    public $table         = 'temp_transaction';
} // EOC