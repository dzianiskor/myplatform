<?php
namespace App;
use App\BluCollection;
use App\Supplierstore as Supplierstore;
/**
 * Supplier Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Ralph Nader <ralph@bluloyalty.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class Supplier extends BaseModel 
{
    public $table      = 'supplier';
    protected $guarded = array();

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------

    public function stores()
    { 
        return $this->hasMany('App\Supplierstore')->where('draft', false);             
    }

    public function products()         
    { 
        return $this->belongsToMany('App\Product');                            
    }

    public function removedProducts()         
    { 
        return $this->belongsToMany('App\Product')->onlyTrashed();                                                       
    }    

    public function parent()           
    { 
        return $this->belongsTo('App\Supplier', 'parent_id');                   
    }

    public function category()         
    { 
        return $this->belongsTo('App\PartnerCategory', 'category_id');         
    }

    public function country()          
    { 
        return $this->belongsTo('App\Country');         
    }

    public function children()
    {
        return $this->hasMany('App\Supplier', 'parent_id');        
    }   
    // Return partners entities
    public function partners()     
    { 
        return $this->belongsToMany('App\Partner', 'supplier_partner')->distinct();   
    }
    public function networks()         
    { 
        return $this->belongsToMany('App\Network', 'supplier_network')->distinct();                            
    }

    public function firstNetwork()         
    { 
        return $this->belongsToMany('App\Network', 'supplier_network')->first();                            
    }

    # --------------------------------------------------------------------------
    # 
    # Attributes
    # 
    # --------------------------------------------------------------------------
    
    /**
     * Fetch all media in the partner hierarchy
     * 
     * @return array
     */
    public function uploads() 
    { 
        return Media::whereIn('id', $this->childHierarchy());
    }

    # --------------------------------------------------------------------------
    # 
    # Helpers
    # 
    # --------------------------------------------------------------------------
    
    /**
     * Fetch a list of all hierarchical child partners of the current partner
     * 
     * @return array
     */
    public function childHierarchy()
    {
        $fifo  = array($this->id);
        $nodes = array($this->id);

        do {
            $results = self::select('id', 'parent_id')->where('parent_id', array_shift($fifo))->where('draft', false)->get();

            foreach($results as $p) {

                if(!in_array($p->id, $nodes)) {
                    $nodes[] = $p->id;
                    $fifo[]  = $p->id;
                }
            }

        } while(!empty($fifo));

        return $nodes;
    }

    

    /**
     * Fetches the top level parent
     *
     * @return array
     */
    public function topLevelSupplier()
    {
        if ($this->parent) {
            $parent = $this->parent;            

            while ($parent->parent) {
                $parent = $this->parent;
            }

            return $parent;
        } else {
            return $this;
        }
    }


    public function productsAndChildProducts() {
        $products = new BluCollection();
        $childSuppliers = $this->managedObjects();
        foreach ($childSuppliers as $childSupplier) {
            $childProducts = $childSupplier->products;
            foreach ($childProducts as $childProduct) {
                $products->add($childProduct);
            }
        }
        return $products;
    }

} // EOC