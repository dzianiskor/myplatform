<?php

/**
 * Favorite Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class UserAdditionalFields extends BaseModel {

    public $table = 'user_additional_fields';

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------

    public function User() 
    { 
    	return $this->belongsTo('User');
    }
}

// EOC