<?php
namespace App;
use App\BluCollection;
/**
 * Brand Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class Brand extends BaseModel 
{
    public $table      = 'brand';
    protected $guarded = array();
        
    public function brandtranslations()
    {
        return $this->hasMany('App\Brandtranslation')->where('draft', false);
    }
	
} // EOC