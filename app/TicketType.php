<?php
namespace App;
use App\BluCollection;
use Eloquent;
/**
 * Ticket Type Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class TicketType extends Eloquent 
{
    public $table = 'ticket_type';

} // EOC