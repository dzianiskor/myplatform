<?php
namespace App;
use App\BluCollection;
use Eloquent;
/**
 * Card Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class Card extends Eloquent 
{
    public $table = 'card';

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------
    
    public function user() 
    { 
    	return $this->belongsTo('App\User');  
   	}
   	
    public function batch() 
    { 
    	return $this->belongsTo('App\Batch'); 
   	}    

} // EOC