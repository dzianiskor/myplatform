<?php
namespace App;
use App\BluCollection;
/**
 * Prodsubmodel Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Ralph Nader <ralph@bluloyalty.com>
 * @copyright  2016 BLU
 * @link       http://blupoints.com
 */
class Prodsubmodel extends BaseModel 
{
    public $table      = 'prodsubmodel';
    protected $guarded = array();

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------
    
    public function prodsubmodeltranslations()
    {
        return $this->hasMany('App\Prodsubmodeltranslation')->where('draft', false);
    }
    


    public static function orderedList()
    {
        return Prodsubmodel::orderBy('name')->get();
    }    

} // EOC