<?php
namespace App;
use App\BluCollection;
use Eloquent;
/**
 * BannerCountry model
 * 
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class BannerCountry extends Eloquent 
{
    public $table = 'banner_country';

	public $timestamps = false;

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------
    
    public function banner() 
    { 
    	return $this->belongsTo('App\Banner', 'banner_id');  
   	}
   	
    public function country() 
    { 
    	return $this->belongsTo('App\Country', 'country_id'); 
   	}    

} // EOC