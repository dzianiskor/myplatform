<?php
namespace App;
use App\BluCollection;
use Eloquent;
/**
 * Product Channel Model
 * 
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class ProductReward extends Eloquent 
{
    public $table = 'product_reward';

    # --------------------------------------------------------------------------
    # 
    # Relationships
    # 
    # --------------------------------------------------------------------------

} // EOC