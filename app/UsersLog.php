<?php
namespace App;
use App\BluCollection;
use Illuminate\Support\Facades\DB;


// Don't Remove
if(!defined('BLU')) {
    define("BLU", 1);
}

/**
 * User Model
 *
 * @category   Models
 * @package    BLU
 * @author     Wixel Team <team@wixelhq.com>
 * @copyright  2014 BLU
 * @link       http://blupoints.com
 */
class UsersLog extends BaseModel
{
    protected $table = 'users_log';

    
    public static function getNumberOfAttempts ($user_id) {
        $interval = date('Y-m-d H:i:s', strtotime('-15 minutes'));
        $count = DB::table('users_log')->where('user_id', $user_id)->where('log_time', '>',  $interval)->count();
        return $count;
    }

    public static function insertUserAttempt ($user_id, $ip, $store_id){
        DB::table('users_log')->insert(array(
            'user_id'       => $user_id,
            'ip'            => $ip,
            'description'   => 'Failed Login attempt from store ' . $store_id,
            'created_at'    => date('Y-m-d H:i:s', time()),
            'updated_at' => date('Y-m-d H:i:s', time())
        ));
    }
    
    public static function removeUserAttempts ($user_id){
        DB::table('users_log')->where('user_id', $user_id)->delete();
    }

} // EOC
